package SampleData
uses gw.api.databuilder.AccountBuilder
uses gw.api.databuilder.PersonBuilder

@Export
class Account {
  function create(currency : Currency,
                  name : String,
                  number : String,
                  billingPlan : BillingPlan,
                  delinquencyPlan : DelinquencyPlan,
                  address : Address,
                  segment : String): Account {
    var existing = gw.api.database.Query.make(Account).compare("AccountNumber", Equals, number).select()
    if (existing.Empty) {
      var person = new PersonBuilder()
            .withFirstName("Bill")
            .withLastName("Baker")
            .withAddress(address)
            .create()

      var contact = new AccountContact(person.Bundle)
      contact.setContact(person)
      contact.setPrimaryPayer(true)

      var contactRole = new AccountContactRole(contact.Bundle)
      contactRole.setRole(AccountRole.TC_INSURED)
      contact.addToRoles(contactRole)

      return new AccountBuilder()
        .withCurrency(currency)
        .withName(name)
        .withNumber(number)
        .withInvoiceDayOfMonth(1)
        .withInvoiceDeliveryType(InvoiceDeliveryMethod.TC_EMAIL)
        .withSegment(segment)
        .withBillingPlan(billingPlan)
        .withDelinquencyPlan(delinquencyPlan)
        .replaceContactsWith(contact)
        .createAndCommit()

    } else {
      return existing.AtMostOneRow
    }
  }
}
