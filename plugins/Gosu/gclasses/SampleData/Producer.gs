package SampleData

uses gw.api.databuilder.PersonBuilder
uses gw.api.databuilder.ProducerBuilder
uses gw.api.databuilder.ProducerContactBuilder

uses java.util.Date
uses gw.pl.currency.MonetaryAmount
uses java.math.BigDecimal

@Export
class Producer {
  function create(  currency : Currency,
                    publicID : String,
                    name : String,
                    address : Address,
                    tier : ProducerTier,
                    periodicity : Periodicity,
                    holdStatement : Boolean,
                    StatementHoldNegativeLimit: BigDecimal,
                    StatementHoldPositiveLimit: BigDecimal,
                    nextPaymentDate : Date): Producer {
    var existing = gw.api.database.Query.make(Producer).compare("Name", Equals, name).select()
    if (existing.Empty) {
      var primaryContact = new ProducerContactBuilder()
          .withContact(new PersonBuilder()
              .withAddress(address)
              .withFirstName("Bill")
              .withLastName("Baker")
              .withWorkPhone("650-357-9100")
              .withEmailAddress1("producer@guidewire.com")
              .create())
          .asPrimary()
          .createWithNullsAllowed()
      var producer = new ProducerBuilder()
          .withCurrency(currency)
          .withName(name)
          .withHoldStatement(holdStatement)
          .withStatementHoldNegativeLimit( new MonetaryAmount(StatementHoldNegativeLimit, currency))
          .withStatementHoldPositiveLimit( new MonetaryAmount(StatementHoldPositiveLimit, currency))
          .withTier(tier)
          .withRecurringProducerPayment(periodicity, 15)
          .withContact(primaryContact)
          .withPublicId(publicID)
          .createAndCommit()

      return producer
    }
    else {
      return existing.FirstResult
    }
  }
}
