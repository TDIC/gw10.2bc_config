package SampleData

uses gw.api.util.DateUtil
uses gw.api.databuilder.CommissionPlanBuilder
uses gw.pl.currency.MonetaryAmount

uses java.lang.Double
uses java.util.Date

@Export
class CommissionPlan {
  function create(
                  currency : Currency,
                  name : String,
                  description : String,
                  effectiveDate : Date,
                  expirationDate : Date,
                  goldTier : Boolean,
                  silverTier : Boolean,
                  bronzeTier : Boolean,
                  primary : Double,
                  secondary : Double,
                  referrer : Double,
                  payableCriteria : PayableCriteria) : CommissionPlan {
    return create( currency, name, description, effectiveDate, expirationDate, goldTier, silverTier, bronzeTier, primary, secondary, referrer, payableCriteria, true, null, null)
  }

  function create(
                  currency : Currency,
                  name : String,
                  description : String,
                  effectiveDate : Date,
                  expirationDate : Date,
                  goldTier : Boolean,
                  silverTier : Boolean,
                  bronzeTier : Boolean,
                  primary : Double,
                  secondary : Double,
                  referrer : Double,
                  payableCriteria : PayableCriteria,
                  premiumCommissionable : Boolean,
                  bonusPercentage : Double,
                  threshold : MonetaryAmount): CommissionPlan {
    var existing = gw.api.database.Query.make(CommissionPlan).compare("Name", Equals, name).select()
    if (existing.Empty) {
      var comissionPlanBuilder = new CommissionPlanBuilder()
              .withSingleCurrency(currency)
              .withName( name )
              .withDescription( description )
              .withEffectiveDate( DateUtil.currentDate() )
              .allowOnAllTiers()
              .withSuspendForDelinquency( true )
              .withPayableCriteria( payableCriteria )
              .withPrimaryRate( primary )
              .withSecondaryRate( secondary )
              .withReferrerRate( referrer )
      if (premiumCommissionable) {
        comissionPlanBuilder.withPremiumCommissionableItem()
      }
      if (bonusPercentage != null and threshold != null) {
        comissionPlanBuilder.withPremiumIncentive(bonusPercentage, threshold)
      }

      return comissionPlanBuilder.createAndCommit()
    } else {
      return existing.AtMostOneRow
    }
  }
}
