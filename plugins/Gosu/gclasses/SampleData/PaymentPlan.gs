package SampleData

uses gw.api.databuilder.PaymentPlanBuilder

uses java.lang.Double
uses java.util.Date

@Export
class PaymentPlan {
  function create(
                  currency : Currency,
                  publicId : String,
                  isReporting : boolean,
                  name : String,
                  description : String,
                  effectiveDate : Date,
                  expirationDate : Date,
                  deposit : Double,
                  numPayments : Double,
                  periodicity : Periodicity) : PaymentPlan {
    return create( currency, publicId, isReporting, name, description, effectiveDate, expirationDate, deposit, numPayments, periodicity,
      PaymentScheduledAfter.TC_CHARGEDATE, 0,
      PaymentScheduledAfter.TC_POLICYEFFECTIVEDATEPLUSONEPERIOD, 0)
  }

  function create(
                  currency : Currency,
                  publicId : String,
                  isReporting : boolean,
                  name : String,
                  description : String,
                  effectiveDate : Date,
                  expirationDate : Date,
                  deposit : Double,
                  numPayments : Double,
                  periodicity : Periodicity,
                  downPaymentAfter: PaymentScheduledAfter, daysFromReferenceDateToDownPayment: Double,
                  firstInstallmentAfter: PaymentScheduledAfter,
                  daysFromReferenceDateToFirstInstallment: Double) : PaymentPlan {
    return create( currency, publicId, isReporting, name, description, effectiveDate, expirationDate, deposit, numPayments, periodicity,
      downPaymentAfter, daysFromReferenceDateToDownPayment,
      firstInstallmentAfter, daysFromReferenceDateToFirstInstallment,
      PaymentScheduledAfter.TC_POLICYEFFECTIVEDATEPLUSONEPERIOD, 0, 0)
  }

  function create( currency : Currency, publicId : String, isReporting : boolean, name : String, description : String,
      effectiveDate : Date, expirationDate : Date, deposit : Double, numPayments : Double,
      periodicity : Periodicity, 
      downPaymentAfter: PaymentScheduledAfter, daysFromReferenceDateToDownPayment: Double,
      firstInstallmentAfter: PaymentScheduledAfter, daysFromReferenceDateToFirstInstallment: Double,
      oneTimeChargeAfter : PaymentScheduledAfter, daysFromReferenceDateToOneTimeCharge : Double,
      daysBeforePolicyExpirationForInvoicingBlackout : Double) : PaymentPlan {
    var paymentPlansWithGivenName = gw.api.database.Query.make(PaymentPlan).compare("Name", Equals, name).select()
    if (!paymentPlansWithGivenName.Empty) {
      return paymentPlansWithGivenName.AtMostOneRow
    }

    var paymentPlanBuilder = new PaymentPlanBuilder()
      .withSingleCurrency(currency)

    if (deposit.compareTo(0) == 0) {
      paymentPlanBuilder.withNoDownPayment()
    } else {
      paymentPlanBuilder.withDownPaymentPercent(deposit)
    }

    paymentPlanBuilder
      .withReportingFlag( isReporting )
      .withName(name)
      .withDescription( description )
      .withEffectiveDate( effectiveDate )
      .withExpirationDate( expirationDate )
      .withMaximumNumberOfInstallments(numPayments as int)
      .withPeriodicity(periodicity)
      .withDownPaymentAfter(downPaymentAfter)
      .withDaysFromReferenceDateToDownPayment(daysFromReferenceDateToDownPayment as int)
      .withFirstInstallmentAfter( firstInstallmentAfter )
      .withDaysFromReferenceDateToFirstInstallment(daysFromReferenceDateToFirstInstallment as int)
      .withOneTimeChargeAfter( oneTimeChargeAfter )
      .withDaysFromReferenceDateToOneTimeCharge(daysFromReferenceDateToOneTimeCharge as int)
      .withDaysBeforePolicyExpirationForInvoicingBlackout(daysBeforePolicyExpirationForInvoicingBlackout as int)

    if (isReporting) {
      paymentPlanBuilder.withEquityWarningsEnabled(false)
    }

    var paymentPlan = paymentPlanBuilder.create()
    if (publicId != null) {
      paymentPlan.PublicID = publicId
    }
    paymentPlan.Bundle.commit()
    return paymentPlan
  }

}
