package gw.search

uses java.io.Serializable
uses java.util.Date
uses gw.pl.currency.MonetaryAmount
uses gw.api.database.IQueryBeanResult
uses gw.entity.IEntityType
uses gw.api.database.Query
uses gw.api.util.DateUtil

@Export
class TransactionSearchCriteria implements Serializable {

  final static var SHOULD_IGNORE_CASE = true

  var _transactionNumber : String as TransactionNumber
  var _transactionType : typekey.Transaction as TransactionType
  var _minAmount : MonetaryAmount as MinAmount
  var _maxAmount : MonetaryAmount as MaxAmount
  var _earliestDate : Date as EarliestDate
  var _latestDate : Date as LatestDate
  var _currency : Currency as Currency

  function performSearch(): IQueryBeanResult<Transaction> {
    SearchHelper.checkForDateExceptions(EarliestDate, LatestDate);
    SearchHelper.checkForNumericExceptions(MinAmount, MaxAmount);
    var query = buildQuery() as Query<Transaction>
    return query.select()
  }

  function buildQuery() : Query {
    var query = Query.make(getSearchType() as Type<Transaction>)
    restrictSearchByTransactionNumber(query)
    restrictSearchByMinAndMaxAmount(query)
    restrictSearchByDate(query)
    restrictSearchByCurrency(query)
    return query
  }

  function restrictSearchByTransactionNumber(query : Query) {
    if (TransactionNumber.NotBlank) {
      query.startsWith("TransactionNumber", TransactionNumber, SHOULD_IGNORE_CASE)
    }
  }

  function restrictSearchByMinAndMaxAmount(query : Query) {
    if (MinAmount != null || MaxAmount != null) {
      query.between("Amount", MinAmount, MaxAmount)
    }
  }

  function restrictSearchByDate(query : Query) {
    if (EarliestDate != null || LatestDate != null) {
      // If a latest date has been entered, make sure it is set to the end of the day so that the query
      // will return results after midnight that morning
      var endOfLatestDate = LatestDate != null ? DateUtil.endOfDay(LatestDate) : LatestDate
      query.between("TransactionDate", EarliestDate, endOfLatestDate);
    }
  }

  function restrictSearchByCurrency(query : Query) {
    if(Currency != null) {
      query.compare("Currency", Equals, Currency)
    }
  }

  function getSearchType() : IEntityType {
    return TransactionType == null ? Transaction.TYPE.get() : TransactionType.getEntityType()
  }
}