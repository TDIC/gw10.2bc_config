package gw.search

uses java.io.Serializable
uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses gw.pl.persistence.core.Bundle

@Export
class ActivitySearchCriteria implements Serializable {

  var _activityPattern: ActivityPattern as ActivityPattern
  var _assignedToUser: User as AssignedToUser
  var _activityStatus: ActivityStatus as Status
  var _activitySubtype: typekey.Activity as Subtype_Ext
  var _assignedToQueue: AssignableQueue as AssignedToQueue_Ext
  var _activityApproved: Boolean as Approved_Ext

  function performSearch(): IQueryBeanResult<ActivitySearchView> {
    var query = Query.make(entity.ActivitySearchView)
    restrictSearchByActivityFields(query);
    return query.select()
  }

  function restrictSearchByActivityFields(query: Query) {
    if (ActivityPattern != null) {
      query.compare("ActivityPattern", Equals, ActivityPattern)
    }
    if (AssignedToUser != null) {
      query.compare("AssignedUser", Equals, AssignedToUser)
    }
    if (Status != null) {
      query.compare("Status", Equals, Status)
    }
    if (AssignedToQueue_Ext != null) {
      query.compare("AssignedQueue", Equals, AssignedToQueue_Ext)
    }
    if (Subtype_Ext != null) {
      query.compare("Subtype", Equals, Subtype_Ext)
    }
    if (Approved_Ext != null) {
      query.compare("Approved", Equals, Approved_Ext)
    }
  }

}