package gw.search

uses gw.api.database.Query

/**
 * Search criteria for Collateral Disbursements
 */
@Export
class CollDisbSearchCriteria extends gw.search.DisbSearchCriteria<CollDisbSearchView> {

  final static var SHOULD_IGNORE_CASE = true

  var _accountNumber : String as AccountNumber      /* The related account number */

  protected override function addQueryRestrictions(query : Query<CollDisbSearchView>) {
    super.addQueryRestrictions(query)
    restrictSearchByAccountNumber(query)
  }

  private function restrictSearchByAccountNumber(query : Query<CollDisbSearchView>) {
    if(AccountNumber.NotBlank) {
      query.join(CollateralDisbursement#Collateral)
          .join(Collateral#Account)
          .startsWith(Account#AccountNumber, AccountNumber, SHOULD_IGNORE_CASE)
    }
  }

  protected override function restrictSearchByUserSecurityZone(query: Query<CollDisbSearchView>) {
    if (perm.System.acctignoresecurityzone) {
      return // no need to consider security zones
    }
    var accountQuery = query.join(CollateralDisbursement#Collateral)
                            .join(Collateral#Account)
    accountQuery.or(\restriction -> {
      restriction.compare(Account#SecurityZone, Equals, null) // Everyone can see null SecurityZone
      restriction.compareIn(Account#SecurityZone, User.util.CurrentUser.GroupUsers*.Group*.SecurityZone)
    })
  }

}