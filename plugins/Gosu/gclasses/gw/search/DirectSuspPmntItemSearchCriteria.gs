package gw.search

uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses gw.api.util.DateUtil
uses gw.pl.currency.MonetaryAmount

uses java.io.Serializable
uses java.util.Date

@Export
class DirectSuspPmntItemSearchCriteria implements Serializable {
  var _currency : Currency as Currency
  var _earliestDate : Date as EarliestDate
  var _latestDate : Date as LatestDate
  var _minAmount : MonetaryAmount as MinAmount
  var _maxAmount : MonetaryAmount as MaxAmount

  function performSearch(): IQueryBeanResult<DirectSuspPmntItemSearchView> {
    SearchHelper.checkForDateExceptions(EarliestDate, LatestDate)
    SearchHelper.checkForNumericExceptions(MinAmount, MaxAmount)
    var query = buildQuery()
    return query.select()
  }

  function buildQuery() : Query<DirectSuspPmntItemSearchView> {
    var query = Query.make(entity.DirectSuspPmntItemSearchView)

    restrictSearchByOnlyExecutedSuspenseItemsThatHaveNotBeenReversedOrReleased(query)
    restrictSearchByUserSecurityZone(query)

    restrictSearchByCurrency(query)
    restrictSearchByDate(query)
    restrictSearchByMinAndMaxAmount(query)

    return query
  }

  function restrictSearchByOnlyExecutedSuspenseItemsThatHaveNotBeenReversedOrReleased(query : Query) {
    query.compare(BaseSuspDistItem#ReleasedDate, Equals, null)
    query.compare(BaseSuspDistItem#ReversedDate, Equals, null)
    query.compare(BaseSuspDistItem#ExecutedDate, NotEquals, null)
  }
  
  function restrictSearchByCurrency(query : Query) {
    if (Currency != null) {
      query.compare(DirectSuspPmntItem#Currency, Equals, Currency)
    }
  }

  function restrictSearchByDate(query : Query) {
    if (EarliestDate != null || LatestDate != null) {
      // If a latest date has been entered, make sure it is set to the end of the day so that the query
      // will return results after midnight that morning
      var endOfLatestDate = LatestDate != null ? DateUtil.endOfDay(LatestDate) : LatestDate
      query.between(DirectSuspPmntItem#ExecutedDate, EarliestDate, endOfLatestDate)
    }
  }

  function restrictSearchByMinAndMaxAmount(query : Query) {
    if (MinAmount != null || MaxAmount != null) {
      query.between(DirectSuspPmntItem#GrossAmountToApply, MinAmount, MaxAmount)
    }
  }

  private function restrictSearchByUserSecurityZone(query : Query) {
    if (perm.System.acctignoresecurityzone) {
      return // no need to consider security zones
    }
    var accountQuery = query
        .join(BaseSuspPmntItem#ActiveDist)
        .join(DirectBillPayment#BaseMoneyReceived)
        .join(DirectBillMoneyRcvd#Account)
    accountQuery.or(\ restriction -> {
      restriction.compare(Account#SecurityZone, Equals, null) // Everyone can see null SecurityZone
      restriction.compareIn(Account#SecurityZone, User.util.CurrentUser.GroupUsers*.Group*.SecurityZone)
    })
  }

}
