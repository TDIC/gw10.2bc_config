package gw.search

uses gw.api.database.IQueryBeanResult
uses gw.api.path.Paths
uses gw.api.restriction.RestrictionBuilder

uses java.io.Serializable

/**
 * Search criteria for reversible Charges
 */
@Export
class ReversibleChargeSearchCriteria extends ChargeSearchCriteria implements Serializable {

  override function performSearch() : IQueryBeanResult<Charge> {
    verifyPolicyPeriodIsOnAccount()
    var builder = RestrictionBuilder.make(Charge)

    if (PolicyPeriod != null) {
      builder.compare(Paths.make(Charge#TAccountContainer), Equals, PolicyPeriod)
    } else if (Account != null) {
      // The other case if if Account is set but target PolicyPeriod isn't.
      builder.compareSet(Paths.make(Charge#TAccountContainer),
          CompareIn, {Account, Account.Collateral})

      // include (through UNION) Account's PolicyPeriods' Charges...
      final var chargeAccountPoliciesRestrictionBuilder = RestrictionBuilder.make(entity.Charge)
      chargeAccountPoliciesRestrictionBuilder.compare(
          Paths.make(Charge#TAccountContainer,
              PolTAcctContainer#PolicyPeriod,
              entity.PolicyPeriod#Policy,
              entity.Policy#Account),
          Equals, Account)
      builder = builder.union(chargeAccountPoliciesRestrictionBuilder)
    }
    // base restriction should be called after ifelse because, it needs to be applied after union builder is created.
    addBaseRestrictions(builder)
    return builder.create().makeQuery().select()
  }

  /**
   * Base restrictions:<br/>
   * - Is not a reversal (does not have reversed charge)<br/>
   * - Not reversed<br/>
   * - Has reversible charge pattern (prorate/recapture charges are
   *   never reversible)<br/>
   * - Other restrictions included in the search criteria
   */
  private function addBaseRestrictions(builder : RestrictionBuilder<Charge>) {
    // not itself a reversal or not already reversed
    builder.compare(Paths.make(Charge#ReversedCharge), Equals, null)
    builder.compare(Paths.make(Charge#Reversed), Equals, false)

    // only Charges whose ChargePatterns are reversible
    final var chargePatternProperty = Charge#ChargePattern
    // Patterns that are configured as reversible
    builder.compare(
        Paths.make(chargePatternProperty, entity.ChargePattern#Reversible),
        Equals, true)
    // Pro-rata and recapture are never reversible...
    builder.compareSet(
        Paths.make(chargePatternProperty, entity.ChargePattern#Subtype),
        CompareNotIn,
            new ArrayList<typekey.ChargePattern>() {TC_PRORATACHARGE, TC_RECAPTURECHARGE})

    if (ChargePatterns != null) {
      final var chargePatternPath = Paths.make(chargePatternProperty)
      if (ChargePatterns.Count == 1) {
        builder.compare(chargePatternPath, Equals, ChargePattern)
      } else if (ChargePatterns.Count > 1) {
        builder.compareSet(chargePatternPath, CompareIn, ChargePatterns)
      }
    }
    if (Currency != null) {
      builder.compare(Paths.make(Charge#Currency), Equals, Currency)
    }
    if (EarliestDate != null || LatestDate != null) {
      builder.between(Paths.make(Charge#ChargeDate), EarliestDate, LatestDate)
    }
    if (MinAmount != null || MaxAmount != null) {
      builder.between(Paths.make(Charge#Amount), MinAmount, MaxAmount)
    }
  }
}