package gw.search

uses gw.api.database.Query

/**
 * Search criteria for Agency Disbursements
 */
@Export
class AgcyDisbSearchCriteria extends gw.search.DisbSearchCriteria<AgcyDisbSearchView> {

  final static var SHOULD_IGNORE_CASE = true

  var _producerName : String as ProducerName        /* The related producer name */

  protected override function addQueryRestrictions(query : Query<AgcyDisbSearchView>) {
    super.addQueryRestrictions(query)
    restrictSearchByProducerName(query)
  }

  private function restrictSearchByProducerName(query: Query<AgcyDisbSearchView>): void {
    if(ProducerName.NotBlank) {
      query.join(AgencyDisbursement#Producer)
          .startsWith(Producer#Name, ProducerName, SHOULD_IGNORE_CASE)
    }
  }

  protected override function restrictSearchByUserSecurityZone(query: Query<AgcyDisbSearchView>) {
    if (perm.System.prodignoresecurityzone) {
      return // no need to consider security zones
    }
    var accountQuery = query.join(AgencyDisbursement#Producer)
    accountQuery.or(\restriction -> {
      restriction.compare(Producer#SecurityZone, Equals, null) // Everyone can see null SecurityZone
      restriction.compareIn(Producer#SecurityZone, User.util.CurrentUser.GroupUsers*.Group*.SecurityZone)
    })
  }
}