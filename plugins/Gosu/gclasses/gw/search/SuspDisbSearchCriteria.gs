package gw.search

uses gw.api.database.Query

/**
 * Search criteria for Suspense Payment Disbursements
 */
@Export
class SuspDisbSearchCriteria extends gw.search.DisbSearchCriteria<SuspDisbSearchView> {

  protected override function addQueryRestrictions(query : Query<SuspDisbSearchView>) {
    super.addQueryRestrictions(query)
  }

  protected override function restrictSearchByUserSecurityZone(query: Query<SuspDisbSearchView>) {
    /*
    For now SuspenseDisbursements are not restricted by SecurityZone, because only an unapplied SuspensePayment can
    be disbursed, and if a SuspensePayment is unapplied then it is not linked to an entity with a SecurityZone, such
    as an Account, PolicyPeriod, or Producer. If in the future SuspensePayment has a SecurityZone field added to it,
    then this method will need to be implemented.
    */
  }
}