package gw.web.archive

uses gw.api.locale.DisplayKey

/**
 * Encapsulates the value and display logic used to change the value of the
 * {@link entity.Policy#isDoNotArchive() DoNotArchive} attribute of a {@link
 * entity.Policy Policy}.
 */
@Export
class PolicyDoNotArchiveHelper {
  var _policy : Policy

  construct(policy : Policy) {
    _policy = policy
  }

  /**
   * A text label that documents whether the {@link entity.Policy Policy} has
   * archiving enabled or disabled based on its {@link
   * entity.Policy#isDoNotArchive() DoNotArchive} attribute value.
   */
  property get DoNotArchiveLabel() : String {
    return _policy.DoNotArchive
        ? DisplayKey.get('Web.Archive.ArchiveDisabled')
        : DisplayKey.get('Web.Archive.ArchiveEnabled')
  }

  /**
   * The label for a field used to change (toggle) the {@link
   * entity.Policy#setDoNotArchive(java.lang.Boolean) DoNotArchive} value.
   */
  property get ChangeDoNotArchiveLabel() : String {
    return _policy.DoNotArchive
        ? DisplayKey.get('Web.Archive.EnableArchiving')
        : DisplayKey.get('Web.Archive.DisableArchiving')
  }

  /**
   * Whether a check box is available to toggle the {@link
   * entity.Policy#isDoNotArchive() DoNotArchive} attribute value of the
   * {@link entity.Policy Policy}.
   */
  property get ToggleAvailable() : boolean {
    return _policy.DoNotArchive
        ? perm.System.archiveenable : perm.System.archivedisable
  }

  /**
   * Toggle the {@link entity.Policy#isDoNotArchive() DoNotArchive} attribute
   * value of the {@link entity.Policy Policy}.
   *
   * @param change whether to toggle the {@code DoNotArchive} value
   * @param comment a comment documenting why the change was made
   */
  function toggleDoNotArchive(change : boolean, comment : String) {
    if (change) {
      _policy.setDoNotArchive(!_policy.DoNotArchive/*, \ -> comment*/)
    }
  }
}