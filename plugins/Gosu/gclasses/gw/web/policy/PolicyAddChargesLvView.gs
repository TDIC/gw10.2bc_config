package gw.web.policy

uses gw.api.domain.accounting.ChargePatternKey
uses gw.api.domain.charge.ChargeInitializer
uses gw.api.domain.invoice.InvoiceStreams
uses gw.api.locale.DisplayKey
uses gw.invoice.InvoicingOverridesView
uses entity.BillingInstruction

@Export
class PolicyAddChargesLvView {

  private var _chargeToInvoicingOverridesViewMap : Map<ChargeInitializer, InvoicingOverridesView>
  private var _policyPeriod : PolicyPeriod
  private var _billingInstruction : BillingInstruction

  construct(billingInstruction : BillingInstruction, policyPeriod : PolicyPeriod, chargeToInvoicingOverridesViewMap : Map<ChargeInitializer, InvoicingOverridesView>) {
    _billingInstruction = billingInstruction
    _policyPeriod = policyPeriod
    _chargeToInvoicingOverridesViewMap = chargeToInvoicingOverridesViewMap
  }

  function getEligibleInvoiceStreams(chargeInitializer : ChargeInitializer) : java.util.List<InvoiceStream> {
    return InvoiceStreams.getCompatibleInvoiceStreams(getPayerAccount(chargeInitializer), _policyPeriod.PaymentPlan)
        .sortBy(\invoiceStream -> invoiceStream.DisplayName)
  }

  function getPayerAccount(chargeInitializer : ChargeInitializer) : Account {
    var defaultPayer = _chargeToInvoicingOverridesViewMap.get(chargeInitializer).DefaultPayer
    if (defaultPayer != null) {
      return defaultPayer typeis Account ? defaultPayer : null
    } else {
      var policyPayer = _policyPeriod.DefaultPayer
      return policyPayer typeis Account ? policyPayer : null
    }
  }

  function isPayerAccountNull(chargeInitializer : ChargeInitializer) : boolean {
    return getPayerAccount(chargeInitializer) == null
  }

  function areAnyChargePayersAccounts() : boolean {
    for (var initializer in _billingInstruction.ChargeInitializers) {
      if (getPayerAccount(initializer) != null) {
        return true
      }
    }
    return false
  }

  function addInitializer() : ChargeInitializer {
    var initializer = _billingInstruction.buildCharge(0bd.ofCurrency(_billingInstruction.Currency), ChargePatternKey.PREMIUM.get())
    if (_chargeToInvoicingOverridesViewMap != null) {
      _chargeToInvoicingOverridesViewMap.put(initializer, new InvoicingOverridesView(initializer))
    }
    return initializer
  }

  function removeInitializer(initializer : ChargeInitializer) {
    initializer.BreakdownItems.forEach(\breakdownItem -> breakdownItem.Categories*.remove())
    _billingInstruction.removeChargeInitializer(initializer)
    if (_chargeToInvoicingOverridesViewMap != null) {
      _chargeToInvoicingOverridesViewMap.remove(initializer)
    }
  }

  function chargeAmountValidation(initializer : ChargeInitializer) : String {
    if (initializer.Amount.IsZero) {
      return DisplayKey.get("Web.Charge.ChargeAmountCannotBeZero")
    }
    if (isBreakdownAmountIncorrect(initializer)) {
      return DisplayKey.get("Java.ChargeInitializer.BreakdownTotalDoesNotMatchChargeAmount",
          initializer.getTotalBreakdownAmount(), initializer.getAmount())
    }
    return null
  }

  private function isBreakdownAmountIncorrect(initializer : ChargeInitializer) : boolean {
    return !initializer.getBreakdownItems().isEmpty()
        && initializer.getAmount() != initializer.getTotalBreakdownAmount()
  }

  function overridingInvoiceStreamValidation(initializer : ChargeInitializer) : String {
    var view = _chargeToInvoicingOverridesViewMap.get(initializer)

    if (view.OverridingInvoiceStream == null) {
      return null
    }

    if (!getEligibleInvoiceStreams(initializer).contains(view.OverridingInvoiceStream)) {
      return DisplayKey.get("Web.NewPolicyChargesLV.OverridingStreamValidation")
    }

    return null
  }

}