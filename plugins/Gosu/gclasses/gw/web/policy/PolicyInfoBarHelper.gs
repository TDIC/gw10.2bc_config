package gw.web.policy

uses com.google.common.base.Preconditions
uses gw.api.locale.DisplayKey
uses gw.web.util.BCIconHelper

/**
 * A helper class for the {@link pcf.PolicyInfoBar}.
 */
@Export
class PolicyInfoBarHelper {
  var _policyPeriod : PolicyPeriod as readonly PolicyPeriod

  construct(policyPeriod : PolicyPeriod) {
    Preconditions.checkNotNull(policyPeriod)
    _policyPeriod = policyPeriod
  }

  /**
   * The icon for the line-of-business of the {@link PolicyPeriod}.
   */
  property get LineOfBusinessIcon() : String {
    return BCIconHelper.getIcon(PolicyPeriod.Policy.LOBCode)
  }

  /**
   * The icon for the {@link PolicyPeriod#BillingMethod BillingMethod}
   * of the {@link PolicyPeriod}.
   */
  property get PolicyBillingMethodIcon() : String {
    return BCIconHelper.getIcon(PolicyPeriod.BillingMethod)
  }

  /**
   * Whether the {@link #_policyPeriod PolicyPeriod} or its {@link Policy}
   * has any active trouble tickets.
   */
  property get HasActiveTroubleTickets() : boolean {
    return PolicyPeriod.HasActiveTroubleTickets or PolicyPeriod.Policy.HasActiveTroubleTickets
  }

  /**
   * Returns the icon used to represent the status of the {@link PolicyPeriod}.
   */
  public property get InForceIcon() : String {
    return BCIconHelper.getPolicyPeriodInForceIcon(PolicyPeriod)
  }

  /**
   * Returns tooltip used to represent the status of the {@link PolicyPeriod}.
   */
  public property get InForceTooltip(): String {
    if (_policyPeriod.IsInForce) {
      return DisplayKey.get('Web.PolicyInfoBar.InForce');
    } else {
      return _policyPeriod.Archived ? DisplayKey.get('Web.PolicyInfoBar.Archived') : DisplayKey.get('Web.PolicyInfoBar.NotInForce');
    }
  }

}