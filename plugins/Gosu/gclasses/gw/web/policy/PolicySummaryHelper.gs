package gw.web.policy

uses gw.api.database.Query
uses gw.api.locale.DisplayKey
uses gw.web.summary.SummaryHelper
uses gw.web.summary.SummaryTimeSinceCalculator

uses java.util.Date

/**
 * Encapsulates the value and display logic used to display the information
 * for a {@link entity.PolicyPeriod PolicyPeriod} on a summary display.
 */
@Export
class PolicySummaryHelper extends SummaryHelper {
  var _policyPeriod : PolicyPeriod as readonly PolicyPeriod
  var _financialsHelper : PolicySummaryFinancialsHelper as readonly Financials

  construct(policyPeriod : PolicyPeriod) {
    _policyPeriod = policyPeriod
    _financialsHelper = new PolicySummaryFinancialsHelper(policyPeriod)
  }

  /**
   * When the {@link entity.PolicyPeriod PolicyPeriod} is {@link
   * entity.PolicyPeriod#isArchived() Archived} this must look in the
   * {@link entity.PolicyPeriod#getPolicyPeriodArchiveSummary() archive summary}
   * because the associated
   * {@link entity.DelinquencyProcess delinquencies} will be archived.
   *
   * {@inheritDoc}
   */
  override final property get DelinquenciesCount() : int {
    return PolicyPeriod.Archived
        ? PolicyPeriod.PolicyPeriodArchiveSummary.DelinquencyCount
        : super.DelinquenciesCount
  }

  override final property get HasActiveTarget() : boolean {
    // variant: not PolicyPeriod.Closed
    return _policyPeriod.ExpirationDate.after(Date.Today)
  }

  override property get RecentDelinquenciesTooltip() : String {
    if (_policyPeriod.hasActiveDelinquencyProcess()) return DisplayKey.get('Web.PolicyInfoBar.Delinquent')

    var recent = RecentDelinquenciesCount
    if (recent == 0) {
      return DisplayKey.get('Web.PolicySummary.Overview.NoRecentDelinquency')
    } else if (recent == 1) {
      return DisplayKey.get('Web.PolicySummary.Overview.RecentDelinquency')
    }

    return DisplayKey.get('Web.PolicySummary.Overview.RecentDelinquencies')
  }

  override property get InceptionDate() : Date {
    return PolicyPeriod.PolicyInitialInceptionDate
  }


  override property get TimeSinceInceptionDisplay() : String {
    return SummaryTimeSinceCalculator.calculateTimeSince(InceptionDate, PolicyPeriod.EndDate)
  }

  override protected property get DelinquencyProcesses() : DelinquencyProcess[] {
    return PolicyPeriod.DelinquencyProcesses
  }

  override function makeDelinquenciesQuery() : Query<DelinquencyProcess> {
    final var delinquenciesQuery = Query.make(PolicyDlnqProcess)
    delinquenciesQuery.compare(PolicyDlnqProcess#PolicyPeriod, Equals, _policyPeriod)
    return delinquenciesQuery
  }

  /**
   * The context for this query is the {@link entity.PolicyPeriod#getPolicy()
   * Policy} for the {@link entity.PolicyPeriod PolicyPeriod} target for this
   * helper so the delinquencies may not be on it directly.
   *
   * {@inheritDoc}
   */
  override function makeRecentDelinquenciesQuery() : Query<DelinquencyProcess> {
    final var recentDelinquenciesQuery = Query.make(PolicyDlnqProcess)
    final var policySubQuery = Query.make(entity.PolicyPeriod)
    policySubQuery.compare(entity.PolicyPeriod#Policy, Equals, PolicyPeriod.Policy)
    policySubQuery.compare(entity.PolicyPeriod#PolicyPerExpirDate,
        GreaterThanOrEquals, RecentPastStart)
    recentDelinquenciesQuery.subselect(PolicyDlnqProcess#PolicyPeriod, CompareIn,
        policySubQuery, entity.PolicyPeriod#ID)

    return recentDelinquenciesQuery
  }
}
