package gw.web.policy

uses com.google.common.collect.Lists
uses gw.api.domain.accounting.ChargePatternKey
uses gw.api.locale.DisplayKey
uses gw.api.web.color.GWColor
uses gw.pl.currency.MonetaryAmount
uses gw.web.summary.SummaryChartEntry
uses gw.web.summary.SummaryChartMonetaryEntry
uses gw.web.summary.SummaryChartPercentageEntry
uses gw.web.summary.SummaryFinancialsHelper
uses gw.web.summary.SummaryHelper

uses java.util.List

/**
 * Encapsulates the value and display logic used to display the
 * financials information for a {@link PolicyPeriod} on a summary
 * display.
 */
@Export
class PolicySummaryFinancialsHelper extends SummaryFinancialsHelper {
  var _policyPeriod : PolicyPeriod as readonly PolicyPeriod
  var _nextDueInvoices : Invoice[]
  var _lastPayment : BaseDist

  static var LateFeeChargePattern : ChargePattern = null

  construct(policyPeriod : PolicyPeriod) {
    _policyPeriod = policyPeriod
    _tAccountOwner = policyPeriod
  }

  property get FinancialsMode() : String {
    if (PolicyPeriod.BillingMethod == TC_AGENCYBILL) {
      return PolicyPeriod.BillingMethod.Code
    } else if (PolicyPeriod.Account.BillingLevel == TC_ACCOUNT) {
      return PolicyPeriod.Account.BillingLevel.Code
    }
    return "Policy"
  }

  property get NextDueInvoices() : Invoice[] {
    if (_nextDueInvoices == null) {
      _nextDueInvoices = PolicyPeriod.NextDueInvoices
    }
    return _nextDueInvoices
  }

  property get ExpectedPayment() : MonetaryAmount {
    if (PolicyPeriod.Status != TC_INFORCE) {
      return null
    }
    return OutstandingAmount
  }

  property get UnsettledLateFeesAmount() : MonetaryAmount {
    if (LateFeeChargePattern == null) {
      LateFeeChargePattern = ChargePatternKey.POLICYLATEFEE.get()
    }
    final var total = PolicyPeriod.Charges
        .where(\ charge -> charge.ChargePattern == LateFeeChargePattern
            and not (charge.Reversed or charge.Reversal))
        .toTypedArray()
        // unsettled should account for write-offs so just walk the items...
        .flatMap(\ lateCharge -> lateCharge.InvoiceItems)
        .sum(PolicyPeriod.Currency,
            \ lateFeeInvoiceItem -> lateFeeInvoiceItem.GrossUnsettledAmount)

    return total.IsNegative ? 0bd.ofCurrency(total.Currency) : total
  }

  property get LastPayment() : BaseDist {
    if (_lastPayment == null) {
      // ActualPayments are ordered from last to first...
      _lastPayment = PolicyPeriod.ActualPayments.first().BaseDist
    }
    return _lastPayment
  }

  override property get NextInvoiceDueLabel() : String {
    return this.NextDueInvoices.IsEmpty
        ? DisplayKey.get('Web.PolicySummary.Financials.NextDueInvoice.NoInvoices')
        : DisplayKey.get('Web.PolicySummary.Financials.NextDueInvoice',
                getBestDateFormatAccordingToLocale(ON_DATE_LABEL_FORMAT, this.NextDueInvoices.first().DueDate))
  }

  override property get LastPaymentReceivedLabel() : String {
    return this.LastPayment != null
        ? DisplayKey.get('Web.PolicySummary.Financials.LastPaymentReceived',
                getBestDateFormatAccordingToLocale(ON_DATE_LABEL_FORMAT, this.LastPayment.DistributedDate))
        : DisplayKey.get('Web.PolicySummary.Financials.LastPaymentReceived.NoPayment')
  }

  property get PolicyValuesChartEntries() : List<SummaryChartEntry> {
    var entries = Lists.newArrayList<SummaryChartEntry>()

    entries.add(new SummaryChartMonetaryEntry('Web.PolicySummary.Financials.PolicyValue.Unbilled',
        UnbilledAmount, GWColor.THEME_PROGRESS_UNSTARTED))
    entries.add(new SummaryChartMonetaryEntry('Web.PolicySummary.Financials.PolicyValue.Paid',
        PaidAmount, GWColor.THEME_PROGRESS_COMPLETE))
    entries.add(new SummaryChartMonetaryEntry('Web.PolicySummary.Financials.PolicyValue.WrittenOff',
        WrittenOffAmount, GWColor.THEME_PROGRESS_ABANDONDED))
    entries.add( new SummaryChartMonetaryEntry('Web.PolicySummary.Financials.PolicyValue.Billed',
        BilledAmount, GWColor.THEME_PROGRESS_INPROGRESS))
    entries.add(new SummaryChartMonetaryEntry('Web.PolicySummary.Financials.PolicyValue.PastDue',
        DueAmount, GWColor.THEME_PROGRESS_OVERDUE))
    return entries
  }

  @Deprecated(:value="This was never used OOTB and will probably be removed in the future.", :version="BC 10.0.0")
  property get PolicyEarningsChartEntries() : List<SummaryChartEntry> {
    final var totalPremium = PolicyPeriod.TotalEquityChargeValue
    final var earnedPremium = PolicyPeriod.EarnedAmount
    return { /* in legend order */
        new SummaryChartPercentageEntry('Web.PolicySummary.Financials.PolicyEarnings.EarnedPremium',
            earnedPremium, totalPremium, GWColor.THEME_NEW),
        new SummaryChartPercentageEntry('Web.PolicySummary.Financials.PolicyEarnings.Balance',
            totalPremium - earnedPremium, totalPremium, GWColor.THEME_ALERT_INFO)
    }
  }

  property get PaidThroughDate() : String {
    var paidThroughDate =  PolicyPeriod.PaidThroughDate
    return  paidThroughDate == null ?  "" : paidThroughDate.format(SummaryHelper.FullDateFormat)
  }
}