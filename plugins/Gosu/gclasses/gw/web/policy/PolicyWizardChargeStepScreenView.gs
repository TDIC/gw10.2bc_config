package gw.web.policy

uses entity.BillingInstruction
uses gw.api.database.Query
uses gw.api.domain.charge.ChargeInitializer
uses gw.util.Pair

uses java.util.function.BiConsumer
uses java.util.function.Consumer

@Export
class PolicyWizardChargeStepScreenView {

  private var _policyPeriod : PolicyPeriod
  private var _billingInstruction : BillingInstruction

  construct(policyPeriod : PolicyPeriod, billingInstruction : BillingInstruction) {
    _policyPeriod = policyPeriod
    _billingInstruction = billingInstruction
  }

  function isDiscountedPaymentValid() : Boolean {
    if (!_policyPeriod.EligibleForFullPayDiscount) {
      return true;
    }

    var totalChargeAmount = 0bd.ofCurrency(_policyPeriod.Currency)
    for (charge in _billingInstruction.ChargeInitializers) {
      totalChargeAmount = totalChargeAmount.add(charge.Amount)
    }
    return totalChargeAmount > _policyPeriod.DiscountedPaymentThreshold
  }

  function beforeCommit() {
    consolidateEquivalentBreakdownCategoriesIntoSingleInstance()
  }

  private function consolidateEquivalentBreakdownCategoriesIntoSingleInstance() {
    var categoryNameAndTypeToSingleCategoryInstance = computeSingleInstanceForEquivalentCategories()
    forEachCategoryInEachBreakdownItem(_billingInstruction.ChargeInitializers, (\category, breakdownItem -> {
      var singleCategoryInstance = categoryNameAndTypeToSingleCategoryInstance.get(idAndCategoryType(category))
      if (category != singleCategoryInstance) {
        breakdownItem.removeCategory(category)
        breakdownItem.addCategory(singleCategoryInstance)
        category.remove()
      }
    }))
  }

  /**
   * Two categories are equivalent if they have the same CategoryIdentifier and CategoryType
   */
  private function computeSingleInstanceForEquivalentCategories() : HashMap<Pair<String, ChargeBreakdownCategoryType>, ChargeBreakdownCategory> {
    var categoryIdAndTypeToSingleCategoryInstance = new HashMap<Pair<String, ChargeBreakdownCategoryType>, ChargeBreakdownCategory>()
    forEachCategory(_billingInstruction.ChargeInitializers, (\category ->
        categoryIdAndTypeToSingleCategoryInstance.computeIfAbsent(
            idAndCategoryType(category),
            \absentCategoryIdAndType -> existingCategoryWithIdentifier(
                absentCategoryIdAndType.First, absentCategoryIdAndType.Second) ?: category)))
    return categoryIdAndTypeToSingleCategoryInstance;
  }

  private function idAndCategoryType(category : ChargeBreakdownCategory) : Pair<String, ChargeBreakdownCategoryType> {
    return new Pair<String, ChargeBreakdownCategoryType>(category.CategoryIdentifier, category.CategoryType);
  }

  private function existingCategoryWithIdentifier(categoryIdentifier : String, categoryType : ChargeBreakdownCategoryType) : ChargeBreakdownCategory {
    return Query.make(entity.ChargeBreakdownCategory)
        .compare(ChargeBreakdownCategory#CategoryIdentifier, Equals, categoryIdentifier)
        .compare(ChargeBreakdownCategory#CategoryType, Equals, categoryType)
        .select()
        .getAtMostOneRow()
  }

  private function forEachCategory(initializers : List<ChargeInitializer>, consumer : Consumer<ChargeBreakdownCategory>) {
    forEachCategoryInEachBreakdownItem(initializers, \category, breakdownItem -> consumer.accept(category))
  }

  private function forEachCategoryInEachBreakdownItem(initializers : List<ChargeInitializer>,
                                                      consumer : BiConsumer<ChargeBreakdownCategory, ChargeBreakdownItem>) {
    initializers.forEach(\initializer ->
        initializer.BreakdownItems.forEach(\breakdownItem ->
            breakdownItem.Categories
                .forEach(\category -> consumer.accept(category, breakdownItem))))
  }

}