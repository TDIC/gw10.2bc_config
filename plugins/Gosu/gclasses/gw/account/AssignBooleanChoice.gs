package gw.account
/**
 * Class to help describe which boolean is which value for the AssignInvoiceItemsToNewPayerPopup
 */
@Export
enum AssignBooleanChoice {
  ASSIGN_ALL(true),
  ASSIGN_NOT_FULLY_PAID(false)

  private var _booleanValue: boolean as readonly BooleanValue

  private construct(booleanValue : boolean) {
    _booleanValue = booleanValue
  }

  static function parse(value : boolean) : AssignBooleanChoice {
    return value ? ASSIGN_ALL : ASSIGN_NOT_FULLY_PAID
  }
}