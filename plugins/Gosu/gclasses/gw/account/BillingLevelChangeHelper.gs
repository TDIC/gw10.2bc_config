package gw.account

/*
 * Helper class to contain data used by the AccountDetailSummary to change the billing level
 */
@Export
class BillingLevelChangeHelper {

  private var _invoiceBy : Boolean
  private var _separateIncomingFunds : Boolean
  private var _account: Account

  construct(account : Account){
    _account = account
    _invoiceBy = _account.BillingLevel == BillingLevel.TC_ACCOUNT
    _separateIncomingFunds = _account.BillingLevel != BillingLevel.TC_POLICYDESIGNATEDUNAPPLIED
  }

  property get isInvoiceBy() : boolean {
    return _invoiceBy
  }

  property get isSeparateIncomingFunds() : boolean {
    return _separateIncomingFunds
  }

  property set isInvoiceBy(invoiceBy : boolean) {
     _invoiceBy =  invoiceBy
  }

  property set isSeparateIncomingFunds(separateIncomingFunds : boolean) {
     _separateIncomingFunds = separateIncomingFunds
  }

  function updateBillingLevel() : String {
    if (_invoiceBy) {
      _account.BillingLevel = BillingLevel.TC_ACCOUNT
      _separateIncomingFunds = true
    } else if (_separateIncomingFunds) {
      _account.BillingLevel = BillingLevel.TC_POLICYDEFAULTUNAPPLIED
    } else {
      _account.BillingLevel = BillingLevel.TC_POLICYDESIGNATEDUNAPPLIED
    }
    return null;
  }

}
