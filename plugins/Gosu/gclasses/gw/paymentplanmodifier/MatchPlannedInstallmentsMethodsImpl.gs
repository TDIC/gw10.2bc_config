package gw.paymentplanmodifier
uses gw.api.domain.invoice.PaymentPlanModifierMethods

@Export
class MatchPlannedInstallmentsMethodsImpl implements PaymentPlanModifierMethods {

  private var _matchPlannedInstallments : MatchPlannedInstallments;

  construct (matchPlannedInstallments : MatchPlannedInstallments) {
    _matchPlannedInstallments = matchPlannedInstallments
  }

  /**
   * Modifies the PaymentPlan so that it will create new installments that match the planned installments for the ReferenceCharge.
   */
  override function modify(paymentPlan : PaymentPlan) {
    var billingInstructionUsingThisModifier = _matchPlannedInstallments.policyBillingInstruction as ExistingPlcyPeriodBI

    var plannedInstallments = _matchPlannedInstallments.ReferenceCharge.AllInvoiceItems.where(\ item ->
          item.Invoice.Planned
          and (item.EventDate == billingInstructionUsingThisModifier.ModificationDate or item.EventDate.after(billingInstructionUsingThisModifier.ModificationDate))
          and item.Installment)
    if (!plannedInstallments.IsEmpty) {
      paymentPlan.HasDownPayment = false;
      paymentPlan.MaximumNumberOfInstallments = plannedInstallments.Count;

      paymentPlan.FirstInstallmentAfter = PaymentScheduledAfter.TC_POLICYEFFECTIVEDATE
      var daysFromPolicyPeriodEffectiveDateToFirstInstallment = _matchPlannedInstallments.ReferenceCharge.PolicyPeriod.EffectiveDate
          .differenceInDays(plannedInstallments[0].EventDate)
      // note: daysFromPolicyPeriodEffectiveDateToFirstInstallment could be negative, which means the first installment date would be _before_ the effective date
      paymentPlan.DaysFromReferenceDateToFirstInstallment = daysFromPolicyPeriodEffectiveDateToFirstInstallment

      if (plannedInstallments.length > 1) {
        paymentPlan.HasSecondInstallment = true;
        paymentPlan.SecondInstallmentAfter = PaymentScheduledAfter.TC_POLICYEFFECTIVEDATE
        var daysFromPolicyPeriodEffectiveDateToSecondInstallment = _matchPlannedInstallments.ReferenceCharge.PolicyPeriod.EffectiveDate
            .differenceInDays(plannedInstallments[1].EventDate)
        // note: daysFromPolicyPeriodEffectiveDateToFirstInstallment could be negative, which means the first installment date would be _before_ the effective date
        paymentPlan.DaysFromReferenceDateToSecondInstallment = daysFromPolicyPeriodEffectiveDateToSecondInstallment
      }
    }
  }

}
