package gw.admin.paymentplan

uses gw.api.locale.DisplayKey
uses java.util.List

@Export
enum DownPaymentFrequency {
  //for NewPaymentPlan_SecondStepScreen
  EVERY_TERM(\-> DisplayKey.get("Web.InstallmentViewHelper.DownPaymentFrequency.EveryTerm")),
  FIRST_TERM_ONLY(\-> DisplayKey.get("Web.InstallmentViewHelper.DownPaymentFrequency.FirstTermOnly")),
  NONE(\-> DisplayKey.get("Web.InstallmentViewHelper.DownPaymentFrequency.None")),

  //for PaymentPlanDetailScreen
  NOT_OVERRIDDEN(\-> DisplayKey.get('Web.PaymentPlanDetailScreen.NotOverridden')),
  YES(\-> DisplayKey.get('Web.InstallmentViewHelper.DownPaymentFrequency.Yes')),
  NO(\-> DisplayKey.get('Web.InstallmentViewHelper.DownPaymentFrequency.No'))
  //
  private var _displayResolver: block(): String as DisplayResolver
  //
  static private var _valuesForNewPlan = {EVERY_TERM, FIRST_TERM_ONLY, NONE}
  static private var _valuesForExistingPlan = {YES, NO}
  static private var _valuesForOverrides = {NOT_OVERRIDDEN, YES, NO}
  //

  private construct(resolveDisplayKey(): String) {
    _displayResolver = resolveDisplayKey
  }

  override function toString(): String {
    return DisplayResolver()
  }

  static public function getValuesForWizardScreen(): List<DownPaymentFrequency> {
    return _valuesForNewPlan
  }

  static public function getValuesForDetailsScreen(): List<DownPaymentFrequency> {
    return _valuesForExistingPlan
  }

  static public function getValuesForDetailsScreenOverrides(): List<DownPaymentFrequency> {
    return _valuesForOverrides
  }
}