package gw.admin.paymentplan

uses java.math.BigDecimal

/**
 * Subclasses of this abstract helper class are used on both the NewPaymentPlanWizard and the PaymentPlanDetailScreen to
 * manage setting a PaymentPlan's installment treatment (i.e. whether the PaymentPlan has a down payment "Every Term,"
 * "First Term Only," or "Never.")
 *
 * Subclasses of this class wrap an entity such as a PaymentPlan or ChargeSlicingOverrides. This class primarily
 * enables the DaysFromReferenceDateToXXX fields to always appear as a positive number in the UI, even
 * though they can have a negative sign in the database. UI toggle fields with the values "Before" and "After"
 * assist in managing the true signs of the DaysFromReferenceDateToXXX fields in the database. This design decision was made
 * in order to support more intuitive functionality in the UI and to eliminate the need for any related upgrade.
 *
 */
@Export
abstract class InstallmentViewHelper {
  private var _frequencyOfDownPayment: DownPaymentFrequency as FrequencyOfDownPayment
  private var _whenDownPayment: When as BeforeAfterDownPayment
  private var _whenFirstInstallment: When as BeforeAfterFirstInstallment
  private var _whenSecondInstallment: When as BeforeAfterSecondInstallment
  private var _choiceOfSecondInstallment: SecondInstallmentChoice as SecondInstallmentChoice
  private var _whenOneTimeCharge: When as BeforeAfterOneTimeCharge
  private var _subViewHelper : InstallmentViewHelper as readonly SubViewHelper
  private var _requiredFieldCalculator : gw.admin.paymentplan.RequiredFieldCalculator as readonly RequiredFieldCalculator
  //

  /* constructor and initialization code ******************************************************************************/

  construct() {

  }

  final protected property set SubViewHelper(helper : InstallmentViewHelper) {
    _subViewHelper = helper
  }

  final protected function initPaymentScheduledAfterFields() {
    /* We unfortunately can't call this method from this class's constructor because the subclass's getter implementations will
    try to reference values that haven't yet been set. It must therefore be called from the subclass's constructor instead. */
    DownPaymentAfter = PaymentScheduledAfter.TC_CHARGEEFFECTIVEDATE
    FirstInstallmentAfter = PaymentScheduledAfter.TC_CHARGEEFFECTIVEDATEPLUSONEPERIOD
    SecondInstallmentAfter = PaymentScheduledAfter.TC_CHARGEEFFECTIVEDATEPLUSONEPERIOD
    OneTimeChargeAfter = PaymentScheduledAfter.TC_CHARGEEFFECTIVEDATE
  }

  final protected function initWhenFields() {
    /* We unfortunately can't call this method from this class's constructor because the subclass's getter implementations will
    try to reference values that haven't yet been set. It must therefore be called from the subclass's constructor instead. */
    initWhenDownPayment()
    initWhenFirstInstallment()
    initWhenSecondInstallment()
    initWhenOneTimeCharge()
  }

  private function initWhenDownPayment() {
    if (InternalDaysFromReferenceDateToDownPayment != null && InternalDaysFromReferenceDateToDownPayment > 0) {
      _whenDownPayment = AFTER
    } else {
      _whenDownPayment = BEFORE
    }
  }

  private function initWhenFirstInstallment() {
    if (InternalDaysFromReferenceDateToFirstInstallment == null || InternalDaysFromReferenceDateToFirstInstallment >= 0) {
      _whenFirstInstallment = AFTER
    } else {
      _whenFirstInstallment = BEFORE
    }
  }

  private function initWhenSecondInstallment() {
    if (InternalDaysFromReferenceDateToSecondInstallment == null || InternalDaysFromReferenceDateToSecondInstallment >= 0) {
      _whenSecondInstallment = AFTER
    } else {
      _whenSecondInstallment = BEFORE
    }
  }

  private function initWhenOneTimeCharge() {
    if (InternalDaysFromReferenceDateToOneTimeCharge != null && InternalDaysFromReferenceDateToOneTimeCharge > 0) {
      _whenOneTimeCharge = AFTER
    } else {
      _whenOneTimeCharge = BEFORE
    }
  }

  final protected function initDownPaymentFields() {
    // May be called when the down payment fields must be overridden to prevent an unexpected missing-required-field error.
    if (DownPaymentAfter == null) {
      DownPaymentAfter = TC_CHARGEEFFECTIVEDATE
    }
    if (InternalDaysFromReferenceDateToDownPayment == null) {
      DaysFromReferenceDateToDownPayment = 0 // setting DaysFromReferenceDateToDownPayment will set the "When" field properly
    }
  }

  final protected function clearDownPaymentFields() {
    DownPaymentPercent = null
    DownPaymentAfter = null
    InternalDaysFromReferenceDateToDownPayment = null
  }

  final protected function initSecondInstallmentFields() {
    SecondInstallmentAfter = TC_CHARGEEFFECTIVEDATEPLUSONEPERIOD
    InternalDaysFromReferenceDateToSecondInstallment = 0
  }

  final protected function clearSecondInstallmentFields() {
    SecondInstallmentAfter = null
    InternalDaysFromReferenceDateToSecondInstallment = null
  }

  /* abstract getters and setters (public) *************************************************************************/

  //// Down Payment and Maximum Number of Installment fields

  abstract property set HasDownPayment(value: Boolean)

  abstract property get HasDownPayment(): Boolean

  abstract property set DownPaymentPercent(value: BigDecimal)

  abstract property get DownPaymentPercent(): BigDecimal

  abstract property set MaximumNumberOfInstallments(value: Integer)

  abstract property get MaximumNumberOfInstallments(): Integer

  //// PaymentScheduledAfter fields

  abstract property set DownPaymentAfter(value: PaymentScheduledAfter)

  abstract property get DownPaymentAfter(): PaymentScheduledAfter

  abstract property set FirstInstallmentAfter(value: PaymentScheduledAfter)

  abstract property get FirstInstallmentAfter(): PaymentScheduledAfter

  abstract property set SecondInstallmentAfter(value: PaymentScheduledAfter)

  abstract property get SecondInstallmentAfter(): PaymentScheduledAfter

  abstract property set OneTimeChargeAfter(value: PaymentScheduledAfter)

  abstract property get OneTimeChargeAfter(): PaymentScheduledAfter

  //// Align InvoiceItem to Installments fields

  abstract property set AlignInstallmentsToInvoices(value: Boolean)

  abstract property get AlignInstallmentsToInvoices(): Boolean

  //// Off Sequence Installment fields

  abstract property set HasSecondInstallment(value: Boolean)

  abstract property get HasSecondInstallment(): Boolean

  /* abstract getters and setters (protected) *************************************************************************/

  //// callbacks for DaysFromReferenceDateToXXX fields

  abstract protected property get InternalDaysFromReferenceDateToDownPayment(): Integer

  abstract protected property set InternalDaysFromReferenceDateToDownPayment(value: Integer)

  abstract protected property get InternalDaysFromReferenceDateToFirstInstallment(): Integer

  abstract protected property set InternalDaysFromReferenceDateToFirstInstallment(value: Integer)

  abstract protected property get InternalDaysFromReferenceDateToSecondInstallment(): Integer

  abstract protected property set InternalDaysFromReferenceDateToSecondInstallment(value: Integer)

  abstract protected property get InternalDaysFromReferenceDateToOneTimeCharge(): Integer

  abstract protected property set InternalDaysFromReferenceDateToOneTimeCharge(value: Integer)

  //// properties which expose info about the base payment plan

  abstract property get BasePaymentPlanHasDownPayment() : Boolean

  abstract property get BasePaymentPlanHasSecondInstallment() : Boolean

  /* concrete getters and setters (public) ****************************************************************************/

  /// Before/After toggle fields

  property get BeforeAfterDownPayment(): When {
    return _whenDownPayment
  }

  property set BeforeAfterDownPayment(when: When) {
    _whenDownPayment = when
    DaysFromRefDateToDownPaymentWithCorrectSign = InternalDaysFromReferenceDateToDownPayment
  }

  property get BeforeAfterFirstInstallment(): When {
    return _whenFirstInstallment
  }

  property set BeforeAfterFirstInstallment(when: When) {
    _whenFirstInstallment = when
    DaysFromRefDateToFirstInstallmentWithCorrectSign = InternalDaysFromReferenceDateToFirstInstallment
  }

  property get BeforeAfterOneTimeCharge(): When {
    return _whenOneTimeCharge
  }

  property get BeforeAfterSecondInstallment(): When {
    return _whenSecondInstallment
  }

  property set BeforeAfterSecondInstallment(when: When) {
    _whenSecondInstallment = when
    DaysFromRefDateToSecondInstallmentWithCorrectSign = InternalDaysFromReferenceDateToSecondInstallment
  }

  property set BeforeAfterOneTimeCharge(when: When) {
    _whenOneTimeCharge = when
    DaysFromRefDateToOneTimeChargeWithCorrectSign = InternalDaysFromReferenceDateToOneTimeCharge
  }

  //// DaysFromReferenceDateToXXX fields

  property get DaysFromReferenceDateToDownPayment(): Integer {
    var days = InternalDaysFromReferenceDateToDownPayment
    return days == null? days : java.lang.Math.abs(days)
  }

  property set DaysFromReferenceDateToDownPayment(value: Integer) {
    DaysFromRefDateToDownPaymentWithCorrectSign = value
  }

  property get DaysFromReferenceDateToFirstInstallment(): Integer {
    var days = InternalDaysFromReferenceDateToFirstInstallment
    return days == null? days : java.lang.Math.abs(days)
  }

  property set DaysFromReferenceDateToFirstInstallment(value: Integer) {
    DaysFromRefDateToFirstInstallmentWithCorrectSign = value
  }

  property get DaysFromReferenceDateToSecondInstallment(): Integer {
    var days = InternalDaysFromReferenceDateToSecondInstallment
    return days == null? days : java.lang.Math.abs(days)
  }

  property set DaysFromReferenceDateToSecondInstallment(value: Integer) {
    DaysFromRefDateToSecondInstallmentWithCorrectSign = value
  }

  property get DaysFromReferenceDateToOneTimeCharge(): Integer {
    var days = InternalDaysFromReferenceDateToOneTimeCharge
    return days == null? days : java.lang.Math.abs(days)
  }

  property set DaysFromReferenceDateToOneTimeCharge(value: Integer) {
    DaysFromRefDateToOneTimeChargeWithCorrectSign = value
  }

  /* private helper functions *****************************************************************************************/

  private property set DaysFromRefDateToDownPaymentWithCorrectSign(newValue: Integer) {
    if (newValue == null) {
      InternalDaysFromReferenceDateToDownPayment = null
    } else {if (_whenDownPayment == BEFORE) {
      InternalDaysFromReferenceDateToDownPayment = java.lang.Math.abs(newValue) * - 1
    } else {
      InternalDaysFromReferenceDateToDownPayment = java.lang.Math.abs(newValue)
    }
    }
  }

  private property set DaysFromRefDateToFirstInstallmentWithCorrectSign(newValue: Integer) {
    if (newValue == null) {
      InternalDaysFromReferenceDateToFirstInstallment = null
    } else {
      if (_whenFirstInstallment == BEFORE) {
        InternalDaysFromReferenceDateToFirstInstallment = java.lang.Math.abs(newValue) * - 1
      } else {
        InternalDaysFromReferenceDateToFirstInstallment = java.lang.Math.abs(newValue)
      }
    }
  }

  private property set DaysFromRefDateToSecondInstallmentWithCorrectSign(newValue: Integer) {
    if (newValue == null) {
      InternalDaysFromReferenceDateToSecondInstallment = null
    } else {
      if (_whenSecondInstallment == BEFORE) {
        InternalDaysFromReferenceDateToSecondInstallment = java.lang.Math.abs(newValue) * - 1
      } else {
        InternalDaysFromReferenceDateToSecondInstallment = java.lang.Math.abs(newValue)
      }
    }
  }

  private property set DaysFromRefDateToOneTimeChargeWithCorrectSign(newValue: Integer) {
    if (newValue == null) {
      InternalDaysFromReferenceDateToOneTimeCharge = null
    } else {
      if (_whenOneTimeCharge == BEFORE) {
        InternalDaysFromReferenceDateToOneTimeCharge = java.lang.Math.abs(newValue) * - 1
      } else {
        InternalDaysFromReferenceDateToOneTimeCharge = java.lang.Math.abs(newValue)
      }
    }
  }

  /* concrete functions (public) **************************************************************************************/

  public function onDownPaymentFrequencyChange() {
    //no op
  }

  public function onHasSecondInstallmentChange() {
    switch(SecondInstallmentChoice) {
      case YES:
        HasSecondInstallment = true
        if (not HasDownPayment) {
          initSecondInstallmentFields()
        }
        break
      case NO:
        HasSecondInstallment = false
        clearSecondInstallmentFields()
        break
    }
  }

  public function validateDownPayment(): String {
    return null
  }

  public function validateDownPaymentFields(): String {
    return null
  }

  public function validateSecondInstallmentFields(): String {
    return null
  }

  property get DownPaymentMode() : String {
    return null
  }

  property get NoneSelectedLabel() : String {
    return null
  }

}