package gw.agencybill

uses gw.api.locale.DisplayKey

@Export
enhancement AgencyCyclePromiseEnhancement : AgencyCyclePromise {

  public property get Status() : String {
    if (this.New) {
      return DisplayKey.get("Java.AgencyCycleDist.Status.New")
    } else if (this.Reversed) {
      return DisplayKey.get("Java.AgencyCycleDist.Status.Reversed")
    } else if (!this.Executed) {
      return DisplayKey.get("Java.AgencyCycleDist.Status.Draft")
    } else if (this.Applied) {
      return DisplayKey.get("Java.AgencyCyclePromise.Status.Paid")
    } else if (this.Executed) {
      return DisplayKey.get("Java.AgencyCyclePromise.Status.Unpaid")
    }

    return DisplayKey.get("Java.AgencyCyclePromise.Status.NotApplicable")
  }

}
