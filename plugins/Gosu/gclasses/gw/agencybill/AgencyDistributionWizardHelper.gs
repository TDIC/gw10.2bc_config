package gw.agencybill

uses entity.BaseDistItem
uses entity.BaseMoneyReceived
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.domain.approval.Approvables
uses gw.api.financials.MonetaryAmounts
uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException
uses gw.api.web.producer.agencybill.AgencyBillMoneyReceivedSetup
uses gw.api.web.producer.agencybill.AgencyBillMoneyReceivedSetup.PolicyFilterOption
uses gw.api.web.producer.agencybill.AgencyBillMoneySetupFactory
uses gw.api.web.producer.agencybill.AgencyBillPaymentMoneySetup
uses gw.api.web.producer.agencybill.AgencyBillPromisedMoneySetup
uses gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView
uses gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistView
uses gw.pl.currency.MonetaryAmount
uses pcf.AgencyDistributionWizard_SavePopup
uses pcf.api.Location

@Export
class AgencyDistributionWizardHelper {

  static enum DistributeToEnum {
    DO_NOT_DISTRIBUTE_NOW(\ -> DisplayKey.get("Web.AgencyDistributionWizardHelper.DistributeTo.DoNotDistributeNow")),
    STATEMENTS_AND_POLICIES(\ -> DisplayKey.get("Web.AgencyDistributionWizardHelper.DistributeTo.StatementsAndPolicies")),
    PROMISE(\ -> DisplayKey.get("Web.AgencyDistributionWizardHelper.DistributeTo.Promise")),
    SAVED(\ -> DisplayKey.get("Web.AgencyDistributionWizardHelper.DistributeTo.Saved")),
    MODIFYING(\ -> DisplayKey.get("Web.AgencyDistributionWizardHelper.DistributeTo.Modifying"))

    private var _displayResolver : block() : String as DisplayResolver
    override function toString() : String {
      return DisplayResolver()
    }

    private construct(resolveDisplayKey() : String) {
      _displayResolver = resolveDisplayKey
    }
  }

  static enum DistributeAmountsEnum {
    EDIT_DISTRIBUTION(\ -> DisplayKey.get("Web.AgencyDistributionWizardHelper.DistributeAmounts.EditDistribution")),
    DISTRIBUTE_NET_OWED_AMOUNTS_AUTOMATICALLY(\ -> DisplayKey.get("Web.AgencyDistributionWizardHelper.DistributeAmounts.DistributeNetOwedAmountsAutomatically"))

    private var _displayResolver : block() : String as DisplayResolver
    override function toString() : String {
      return DisplayResolver()
    }

    private construct(resolveDisplayKey() : String) {
      _displayResolver = resolveDisplayKey
    }
  }

  static enum DistributeByEnum {
    ITEM(\ -> DisplayKey.get("Web.AgencyDistributionWizardHelper.DistributeBy.Item")),
    POLICY(\ -> DisplayKey.get("Web.AgencyDistributionWizardHelper.DistributeBy.Policy"))

    private var _displayResolver : block() : String as DisplayResolver
    override function toString() : String {
      return DisplayResolver()
    }

    private construct(resolveDisplayKey() : String) {
      _displayResolver = resolveDisplayKey
    }
  }

  static enum StatusEnum {
    OPEN(\ -> DisplayKey.get("Web.AgencyDistributionWizardHelper.Status.Open")),
    PLANNED(\ -> DisplayKey.get("Web.AgencyDistributionWizardHelper.Status.Planned")),

    private var _displayResolver : block() : String as DisplayResolver
    override function toString() : String {
      return DisplayResolver()
    }

    private construct(resolveDisplayKey() : String) {
      _displayResolver = resolveDisplayKey
    }
  }

  static enum DistributionDifferenceMethodEnum {
    USE_UNAPPLIED(\ -> DisplayKey.get("Web.AgencyDistributionWizardHelper.DistributionDifferenceMethod.UseUnapplied")),
    WRITE_OFF(\ -> DisplayKey.get("Web.AgencyDistributionWizardHelper.DistributionDifferenceMethod.WriteOff")),

    private var _displayResolver : block() : String as DisplayResolver
    override function toString() : String {
      return DisplayResolver()
    }

    private construct(resolveDisplayKey() : String) {
      _displayResolver = resolveDisplayKey
    }
  }

  static enum DistributionTypeEnum {
    PROMISE,
    PAYMENT,
    CREDIT_DISTRIBUTION
  }

  static function createMoneySetup(producer : Producer, location : Location, distributionType : DistributionTypeEnum, moneyToEdit : BaseMoneyReceived) : AgencyBillMoneyReceivedSetup {
    if (moneyToEdit != null) {
      if (moneyToEdit typeis AgencyBillMoneyRcvd) {
        return AgencyBillMoneySetupFactory.createEditingPaymentMoney(moneyToEdit, location)
      } else if (moneyToEdit typeis PromisedMoney) {
        return AgencyBillMoneySetupFactory.createEditingPromisedMoney(moneyToEdit, location)
      } else {
        throw new IllegalArgumentException(DisplayKey.get("Web.AgencyDistributionWizardHelper.Error.InvalidMoneyType", typeof moneyToEdit))
      }
    }

    switch (distributionType) {
      case DistributionTypeEnum.PAYMENT: return AgencyBillMoneySetupFactory.createNewPaymentMoney(producer, location)
      case DistributionTypeEnum.PROMISE : return AgencyBillMoneySetupFactory.createNewPromisedMoney(producer, location)
      case DistributionTypeEnum.CREDIT_DISTRIBUTION : return AgencyBillMoneySetupFactory.createNewZeroDollarMoney(producer, location)
      default: throw new IllegalArgumentException(DisplayKey.get("Web.AgencyDistributionWizardHelper.Error.UnknownDistributionType", distributionType))
    }
  }

  private var _moneySetup : AgencyBillMoneyReceivedSetup as MoneySetup
  private var _distributeTo : DistributeToEnum as DistributeTo
  private var _distributeAmounts : DistributeAmountsEnum as DistributeAmounts
  private var _distributeBy : DistributeByEnum as DistributeBy
  private var _agencyCycleDistView : AgencyCycleDistView as AgencyCycleDistView
  private var _distributionDifferenceMethod : DistributionDifferenceMethodEnum as DistributionDifferenceMethod
  private var _isSaving = false
  private var _isNew = true
  private var _warnNoStatementsSelected = true
  private var _wizard : pcf.api.Wizard

  construct(theMoneySetup : AgencyBillMoneyReceivedSetup) {
    _moneySetup = theMoneySetup
    _distributeTo = DistributeToValues.first()
    if (MoneySetup.SavedDistribution != null) {
      // if the money was instantiated with a saved distribution (for instance, we entered the wizard trying to continue with a saved distribution),
      // we need to reflect that on the screen
      _distributeTo = DistributeToEnum.SAVED
      _isNew = false
    }
    if (MoneySetup.EditingExecutedDistribution) {
      _isNew = false
    }
    _distributeAmounts = DistributeAmountsEnum.EDIT_DISTRIBUTION
    _distributeBy = DistributeByEnum.ITEM
    MoneySetup.Prefill = AgencyCycleDistPrefill.TC_UNPAID
    _distributionDifferenceMethod = DistributionDifferenceMethodEnum.USE_UNAPPLIED

    // initialize the distribute to statements to be populated with all open statements on the paying producer
    if (IsPayment) {
      MoneySetup.addDistributeToStatements(MoneySetup.Producer.AgencyBillCycles.map(\ abc -> abc.StatementInvoice).where(\ s -> s.Open))
    }
  }

  property set DistributeTo( value : DistributeToEnum) {
    if (value != DistributeToEnum.PROMISE && MoneySetup typeis AgencyBillPaymentMoneySetup) {
      MoneySetup.AppliedFromPromise = null
    }
    _distributeTo = value
  }

  property get DistributeToValues() : DistributeToEnum[] {
    if (IsContinuingSaved) {
      return {DistributeToEnum.SAVED}
    } else if (MoneySetup.EditingExecutedDistribution) {
      return {DistributeToEnum.MODIFYING}
    } else if (IsPayment) {
        return DistributeToEnum.values().subtract({DistributeToEnum.SAVED, DistributeToEnum.MODIFYING}).toArray(new DistributeToEnum[0])
    } else if (IsPromise || IsCreditDistribution) {
      return DistributeToEnum.values().subtract({DistributeToEnum.DO_NOT_DISTRIBUTE_NOW, DistributeToEnum.PROMISE, DistributeToEnum.SAVED, DistributeToEnum.MODIFYING})
        .toArray(new DistributeToEnum[0])
    } else {
      throw new IllegalStateException(DisplayKey.get("Web.AgencyDistributionWizardHelper.Error.UnknownDistributionType", ""))
    }
  }

  property get PolicyFilterValues() : PolicyFilterOption[] {
    return PolicyFilterOption.values()
  }

  property set PolicyFilter(option : PolicyFilterOption) {
    MoneySetup.PolicyFilterOption = option
  }

  property get PolicyFilter() : PolicyFilterOption {
    return MoneySetup.PolicyFilterOption
  }

  property get DistributeAmountsValues() : DistributeAmountsEnum[] {
    return DistributeAmountsEnum.values()
  }

  property get DistributeByValues() : DistributeByEnum[] {
    return DistributeByEnum.values()
  }

  property get DistributionDifferenceMethodValues() : DistributionDifferenceMethodEnum[] {
    return DistributionDifferenceMethodEnum.values()
  }

  property get ShowSpecificPolicies() : boolean {
    return MoneySetup.PolicyFilterOption == PolicyFilterOption.FILTER
  }

  property get DistributeToStatementsAndPolicies() : boolean {
    return DistributeTo == DistributeToEnum.STATEMENTS_AND_POLICIES
  }

  property get DistributeToPromise() : boolean {
    return DistributeTo == DistributeToEnum.PROMISE
  }

  property get DistributeToSaved() : boolean {
    return DistributeTo == DistributeToEnum.SAVED
  }

  property get DoNotDistributeNow() : boolean {
    return DistributeTo == DistributeToEnum.DO_NOT_DISTRIBUTE_NOW
  }

  property get DistributionInfo() : String {
   var info = new StringBuffer()
   info.append(MoneySetup.Producer.DisplayName + ":")

   if (MoneySetup.DistributeToStatements.Count == 0) {
     info.append(DisplayKey.get("Web.AgencyDistributionWizardHelper.DistributionInfo.NoStatements"))
     return info.toString()
   }

   if (ShowSpecificPolicies) {
     info.append(DisplayKey.get("Web.AgencyDistributionWizardHelper.DistributionInfo.SpecificPolicies"))
   } else  {
     info.append(DisplayKey.get("Web.AgencyDistributionWizardHelper.DistributionInfo.AllPolicies"))
   }
   if (MoneySetup.DistributeToStatements.Count == 1) {
     var statement = MoneySetup.DistributeToStatements.first()
     info.append(DisplayKey.get("Web.AgencyDistributionWizardHelper.DistributionInfo.OneStatement", 
                     statement.InvoiceNumber,
                     statement.EventDate.format("short"),
                     statement.DisplayStatus))
   } else {
     info.append(DisplayKey.get("Web.AgencyDistributionWizardHelper.DistributionHelper.MultipleStatements"))
   }
   return info.toString()
  }

  property get SelectedEditDistribution() : boolean {
    return DistributeAmounts == DistributeAmountsEnum.EDIT_DISTRIBUTION
  }

  property get SelectedDistributeNetOwedAmountsAutomatically() : boolean {
    return DistributeAmounts == DistributeAmountsEnum.DISTRIBUTE_NET_OWED_AMOUNTS_AUTOMATICALLY
  }

  property get AdjustProducerUnapplied() : boolean {
    return DistributionDifferenceMethod == DistributionDifferenceMethodEnum.USE_UNAPPLIED
  }

  property get WriteOffDifference() : boolean {
    return DistributionDifferenceMethod == DistributionDifferenceMethodEnum.WRITE_OFF
  }

  function validateFinishDistributionStep() {
    if (!_isSaving
        && AgencyCycleDistView.AgencyCycleDist.DistItems.IsEmpty
        && AgencyCycleDistView.AgencyCycleDist.BaseSuspDistItems.IsEmpty) {
      throw new DisplayableException(DisplayKey.get("Web.AgencyDistributionWizard.Step.Distribution.Error.NoDistribution"))
    }
    for (distItem in AgencyCycleDistView.AgencyCycleDist.DistItems) {
      if (isGrossAmountToApplyInvalid(distItem)) {
        throw new DisplayableException(DisplayKey.get("Java.Error.BaseDist.InvalidGrossDistributionAmount"))
      }
      if (isCommissionAmountToApplyInvalid(distItem)) {
        throw new DisplayableException(DisplayKey.get("Java.Error.BaseDist.InvalidCommissionDistributionAmount"))
      }
    }
  }

  function isGrossAmountToApplyInvalid(agencyDistItem : BaseDistItem) : Boolean {
    return areAmountsMismatched(agencyDistItem.InvoiceItem.Amount, agencyDistItem.GrossAmountToApply)
  }

  function isCommissionAmountToApplyInvalid(agencyDistItem : BaseDistItem) : Boolean {
    return agencyDistItem.ItemCommissionToTarget.CommissionAmount.IsZero &&  agencyDistItem.CommissionAmountToApply.IsNotZero
  }

  // Following are the validation conditions
  // If amountFromInvoiceItem > 0, then amountToApply must be >= 0
  // If amountFromInvoiceItem < 0, then amountToApply must be <= 0
  // If amountFromInvoiceItem == 0, then amountToApply must be equal to 0
  private function areAmountsMismatched(amountFromInvoiceItem : MonetaryAmount, amountToApply : MonetaryAmount) : Boolean {
    return (amountFromInvoiceItem.IsPositive && amountToApply.IsNegative)
      || (amountFromInvoiceItem.IsNegative && amountToApply.IsPositive)
      || (amountFromInvoiceItem.IsZero && amountToApply.IsNotZero)
  }

  function beforeCommit() {
    if (!_isSaving) { // do not execute payments if we have hit the save button
      if (DoNotDistributeNow) {
        executeMoneyOnly()
      } else {
        executeDistribution()
      }
    }
  }

  function onExitMoneyDetailsScreen() {
    if (DoNotDistributeNow || DistributeToPromise) {
        MoneySetup.clearDistributeToStatements()
    }
    if (SelectedDistributeNetOwedAmountsAutomatically) {
      MoneySetup.setPrefill(AgencyCycleDistPrefill.TC_UNPAID)
      if (MoneySetup.DistributeToStatements.IsEmpty && DistributeToStatementsAndPolicies) {
        throw new DisplayableException(DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.Error.NoStatementsForDistributingAutomatically"))
      }
    }
    if (!MoneySetup.HasStartedDistributedStep && !DoNotDistributeNow) {
      if (DistributeToStatementsAndPolicies &&  _warnNoStatementsSelected && MoneySetup.DistributeToStatements.IsEmpty) {
        _warnNoStatementsSelected = false // only warn the user once
        throw new DisplayableException(DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.Error.NoStatementsSelected"))
      }
      AgencyCycleDistView = gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistView.createView(MoneySetup.prepareDistribution())
    }
  }
  private function executeMoneyOnly() {
    if (AgencyCycleDistView != null) {
      throw new DisplayableException(DisplayKey.get("Web.AgencyDistributionWizardHelper.Error.DistributionFoundWhenTryingToExecuteMoneyOnly"))
    }
    MoneySetup.Money.execute()
  }

  private function executeDistribution() {
    if (WriteOffExpense.IsNotZero && WriteOffDifference && AgencyCycleDistView.AgencyCycleDist typeis AgencyCyclePayment) {
      var writeOffThreshold = MoneySetup.Producer.AgencyBillPlan.getProducerWriteoffThreshold(MoneySetup.getCurrency())
      if (WriteOffExpense.abs() > writeOffThreshold) {
        throw new gw.api.util.DisplayableException(DisplayKey.get("Web.AgencyDistributionWizardHelper.Error.OverWriteOffThreshold"))
      }
      AgencyCycleDistView.AgencyCycleDist.WriteOffAmount = WriteOffExpense
    }
    AgencyCycleDistView.AgencyCycleDist.execute()
  }

  /**
   * The amount of money we have left to distribute on this payment.
   * Negative if the payment is overdistributed, and positive if it is underdistributed
   *
   * Remaining amount does not apply for credit distributions, as it doesn't bring money into the system.
   */
  property get Remaining() : MonetaryAmount {
    if(IsCreditDistribution){
      return 0bd.ofCurrency(_moneySetup.Currency)
    }

    return AgencyCycleDistView.AgencyCycleDist.Amount - DistributedAndSuspense
  }

  /**
   * The amount of money we have "used up" on this distribution.
   * Total net amount to apply across all the dist items + total net amount to apply across suspense items
   */
  property get DistributedAndSuspense() : MonetaryAmount {
    return AgencyCycleDistView.AgencyCycleDist.DistributedAmountForUnexecutedDist + AgencyCycleDistView.AgencyCycleDist.NetSuspenseAmountForSavedOrExecuted
  }

  enum Disposition {
      Automatic,
      NonAutomtic
  }

  property get ExceptionDistItems() : BaseDistItem[] {
    var plugin = gw.plugin.Plugins.get(gw.plugin.invoice.IInvoiceItem)
    var dividedByDisposition = AgencyCycleDistView.AgencyCycleDist.DistItems
        .partition(\ distItem -> {
          if (distItem.Disposition == DistItemDisposition.TC_AUTOEXCEPTION ) {
            return Disposition.Automatic
          } else {
            return  Disposition.NonAutomtic
          }
        })

    var automaticDispositionItems = dividedByDisposition.get(Disposition.Automatic) == null ?
       new ArrayList<BaseDistItem>() :
       dividedByDisposition.get(Disposition.Automatic)
    var nonAutomaticDispositionItems = dividedByDisposition.get(Disposition.NonAutomtic) == null ?
       new ArrayList<BaseDistItem>() :
       dividedByDisposition.get(Disposition.NonAutomtic)

    var distItemsToReturn = plugin.agencyDistributionItemsAboutToCauseExceptions(automaticDispositionItems)
    distItemsToReturn.addAll(nonAutomaticDispositionItems)

    return sortExceptionDistItems(distItemsToReturn)
  }

  private function sortExceptionDistItems(baseDistItems : Set<BaseDistItem>) : BaseDistItem[] {
    var sortedBaseDistItems = new ArrayList<BaseDistItem>(baseDistItems)

    var partitionByPolicyPeriod = sortedBaseDistItems.partition(\ distItem -> distItem.PolicyPeriod )
    var totalDifferenceForPolicyPeriod = new HashMap<PolicyPeriod, MonetaryAmount>()
    partitionByPolicyPeriod.eachKeyAndValue(\ key, value ->
      totalDifferenceForPolicyPeriod.put(key, value.sum(MoneySetup.Currency, \ distItem -> distItem.NetDifferenceAmount))
    )

    var distItemComparator = \ d1 : BaseDistItem, d2 : BaseDistItem -> {
      var policyTotalDifferenceOrder = totalDifferenceForPolicyPeriod.get(d1.PolicyPeriod).compareTo(totalDifferenceForPolicyPeriod.get(d2.PolicyPeriod))
      if(policyTotalDifferenceOrder != 0) {
        return -1 * policyTotalDifferenceOrder; //display items grouped by policies with highest total differences first (descending sort)
      } else {
        var statementDateOrder = d1.InvoiceItem.Invoice.EventDate.compareTo(d2.InvoiceItem.Invoice.EventDate)
        if (statementDateOrder != 0) {
          return statementDateOrder;
        } else {
          var netDifferenceAmountOrder = d1.NetDifferenceAmount.compareTo(d2.NetDifferenceAmount)
          if (netDifferenceAmountOrder != 0) {
            return -1 * netDifferenceAmountOrder //display items with highest NetDifferenceAmount first (descending sort)
          } else {
            return d1.InvoiceItem.DisplayNameAsItemType.compareTo(d2.InvoiceItem.DisplayNameAsItemType)
          }
        }
      }
    } as Comparator<BaseDistItem>
    Collections.sort(sortedBaseDistItems, distItemComparator)
    return sortedBaseDistItems.toArray(new BaseDistItem[0])
  }

  function getExceptionsDescription( exceptions : BaseDistItem[] ) : String {
    var numAuto = NumberOfAutomaticExceptions(exceptions)
    var moneyAuto = MoneyInAutomaticExceptions(exceptions)
    var numWriteOff = NumberOfWriteOffExceptions(exceptions)
    var moneyWriteOff = MoneyInWriteOffExceptions(exceptions)
    var numCarried = NumberOfCarriedForwardExceptions(exceptions)
    var numForced = NumberOfForcedExceptions(exceptions)
    var isApprovalRequired = requiresApproval(exceptions)
    if (numAuto > 0 && numWriteOff > 0 && isApprovalRequired) {
      return DisplayKey.get("Web.AgencyDistributionWizardHelper.ExceptionsDescription.AutoAndWriteOffsWithApprovalRequest", numAuto, moneyAuto, numWriteOff, moneyWriteOff, numCarried, numForced)
    } else if (numAuto > 0 && numWriteOff > 0) {
      return DisplayKey.get("Web.AgencyDistributionWizardHelper.ExceptionsDescription.AutoAndWriteOffs", numAuto, moneyAuto, numWriteOff, moneyWriteOff, numCarried, numForced)
    } else if (numAuto > 0) {
      return DisplayKey.get("Web.AgencyDistributionWizardHelper.ExceptionsDescription.AutoOnly", numAuto, moneyAuto, numWriteOff, numCarried, numForced)
    } else if (numWriteOff > 0 && isApprovalRequired) {
      return DisplayKey.get("Web.AgencyDistributionWizardHelper.ExceptionsDescription.WriteOffOnlyWithApprovalRequest", numAuto, numWriteOff, moneyWriteOff, numCarried, numForced)
    } else if (numWriteOff > 0) {
      return DisplayKey.get("Web.AgencyDistributionWizardHelper.ExceptionsDescription.WriteOffOnly", numAuto, numWriteOff, moneyWriteOff, numCarried, numForced)
    } else {
      return DisplayKey.get("Web.AgencyDistributionWizardHelper.ExceptionsDescription.NoAutoAndNoWriteOffs", numAuto, numWriteOff, numCarried, numForced)
    }
  }

  function save(wizard : pcf.api.Wizard) {
    _isSaving = true
    _wizard = wizard
    AgencyDistributionWizard_SavePopup.push(this)
  }

  function finishSave(popup : Location) {
    popup.commit()
    _wizard.finish()
  }

  function cancelSave(popup : Location) {
    popup.cancel()
    _isSaving = false
    _wizard = null
  }

  property get PaymentMoneySetup() : AgencyBillPaymentMoneySetup {
    if (IsPayment || IsCreditDistribution) {
      return MoneySetup as AgencyBillPaymentMoneySetup
    } else {
      return null
    }
  }

  property get PromiseMoneySetup() : AgencyBillPromisedMoneySetup {
    if (IsPromise) {
      return MoneySetup as AgencyBillPromisedMoneySetup
    } else {
      return null
    }
  }

  property get IsPayment() : boolean {
    return MoneySetup typeis gw.api.web.producer.agencybill.AgencyBillPaymentMoneySetup && !MoneySetup.CreditDistribution
  }

  property get IsPromise() : boolean {
    return MoneySetup typeis gw.api.web.producer.agencybill.AgencyBillPromisedMoneySetup
  }

  property get IsCreditDistribution() : boolean {
    return MoneySetup typeis gw.api.web.producer.agencybill.AgencyBillPaymentMoneySetup && MoneySetup.CreditDistribution
  }

  property get IsNewDistAndNotFromPromise() : boolean {
    if (MoneySetup typeis gw.api.web.producer.agencybill.AgencyBillPaymentMoneySetup) {
      return _isNew && MoneySetup.AppliedFromPromise == null
    } else {
      return _isNew
    }
  }

  /**
   * Returns true if there exists a saved distribution of the same type as this helper's AgencyCycleDist that
   * has the same producer and amount (for promises) and payment instrument (for payments)
   */
  property get SimilarSavedDistributionExists() : boolean {
    var similarSavedDistributionsQuery: Query<AgencyCycleDist>
    if (IsPayment || IsCreditDistribution) {
      similarSavedDistributionsQuery = Query.make(AgencyCyclePayment)
      similarSavedDistributionsQuery.cast(AgencyCyclePayment.Type)
    } else if (IsPromise) {
      similarSavedDistributionsQuery = Query.make(AgencyCyclePromise)
      similarSavedDistributionsQuery.cast(AgencyCyclePromise.Type)
    } else {
      throw new IllegalStateException(DisplayKey.get("Web.AgencyDistributionWizardHelper.Error.UnknownDistributionType", ""))
    }

    similarSavedDistributionsQuery
      .compare("DistributedDate", Equals, null) // saved
      .compare("ID", NotEquals, AgencyCycleDistView.AgencyCycleDist.ID)  // don't include the one we are currently working on
    var moneyReceivedTable = similarSavedDistributionsQuery.join("BaseMoneyReceived")

    if (IsPayment) {
      moneyReceivedTable
        .cast(AgencyBillMoneyRcvd.Type)
        .compare("Producer", Equals, MoneySetup.Producer) // same producer
        .compare("PaymentInstrument", Equals, PaymentMoneySetup.Money.PaymentInstrument) // same payment instrument
    } else if (IsCreditDistribution) {
      moneyReceivedTable
        .cast(ZeroDollarAMR.Type)
        .compare("Producer", Equals, MoneySetup.Producer) // same producer
    } else {
      moneyReceivedTable
        .cast(PromisedMoney.Type)
        .compare("PromisingProducer", Equals, MoneySetup.Producer) // same producer
   }
    moneyReceivedTable.compare("Amount", Equals, MoneySetup.Money.Amount) // same amount

    return !similarSavedDistributionsQuery.select().Empty
  }

  private function NumberOfAutomaticExceptions( exceptions : BaseDistItem[] ) : int {
    return exceptions.countWhere(\ e -> e.Disposition == DistItemDisposition.TC_AUTOEXCEPTION)
  }

  private function MoneyInAutomaticExceptions( exceptions : BaseDistItem[] ) : String {
    return render(exceptions.sum(
      MoneySetup.Currency,
      \ e -> e.Disposition == DistItemDisposition.TC_AUTOEXCEPTION ? e.NetDifferenceAmount : 0bd.ofCurrency(MoneySetup.Currency)))
  }

  private function NumberOfWriteOffExceptions( exceptions : BaseDistItem[] ) : int {
    return exceptions.countWhere(\ e -> e.Disposition == DistItemDisposition.TC_WRITEOFF)
  }

  private function MoneyInWriteOffExceptions( exceptions : BaseDistItem[] ) : String {
    return render(exceptions.sum(
      MoneySetup.Currency,
      \ e -> e.Disposition == DistItemDisposition.TC_WRITEOFF ? e.NetDifferenceAmount :  0bd.ofCurrency(MoneySetup.Currency)))
  }

  private function NumberOfCarriedForwardExceptions( exceptions : BaseDistItem[] ) : int {
    return exceptions.countWhere(\ e -> e.Disposition == DistItemDisposition.TC_CARRYFORWARD)
  }

  private function NumberOfForcedExceptions( exceptions : BaseDistItem[] ) : int {
    return exceptions.countWhere(\e -> e.Disposition == DistItemDisposition.TC_EXCEPTION)
  }

  private function render( amount : MonetaryAmount ) : String {
    return amount.render()
  }

  property get IsNewCreditDistribution() : boolean {
    return (IsCreditDistribution && !MoneySetup.EditingExecutedDistribution)
  }

  property get CreditAvailableForDistribution() : MonetaryAmount {
    if (IsCreditDistribution) {
      if (MoneySetup.EditingExecutedDistribution) {
        return ModifiedDistributedOrInSuspenseAmount
      } else {
        return MoneySetup.Producer.UnappliedAmount
      }
    } else {
      return 0bd.ofCurrency(MoneySetup.Currency)
    }
  }

  property get OtherUnappliedAvailableForDistribution() : MonetaryAmount {
    if (IsCreditDistribution) {
      if (MoneySetup.EditingExecutedDistribution) {
        return MoneySetup.Producer.UnappliedAmount
      } else {
        return 0bd.ofCurrency(MoneySetup.Currency)
      }
    } else {
      return MoneySetup.Producer.UnappliedAmount - ModifiedUnappliedAmount
    }
  }

  property get TotalAmountAvailableForDistribution() : MonetaryAmount {
    if (IsCreditDistribution) {
      return CreditAvailableForDistribution + OtherUnappliedAvailableForDistribution
    } else {
      return MoneySetup.Money.Amount + OtherUnappliedAvailableForDistribution
    }
  }

  property get ModifiedDistributedOrInSuspenseAmount() : MonetaryAmount {
      if (MoneySetup.EditingExecutedDistribution) {
        //When the new payment is executed, the previous unapplied write-offs are reversed, and so we shouldn't
        //consider the written-off amount as money that is available for distribution
        return MoneySetup.PreviouslyExecutedDistribution.NetDistributedToInvoiceItems
            .add(MoneySetup.PreviouslyExecutedDistribution.NetInSuspense)
            .subtract(ModifiedWrittenOff)
      } else {
        return 0bd.ofCurrency(MoneySetup.Currency)
      }
  }

  property get ModifiedUnappliedAmount() : MonetaryAmount {
    if (AgencyCycleDistView.AgencyCycleDist.BaseMoneyReceived.Modifying && (IsPayment || IsCreditDistribution)) {
      if (MoneySetup.EditingExecutedDistribution) {
        return MoneySetup.PreviouslyExecutedDistribution.NetDistributedToInvoiceItems
          .add(MoneySetup.PreviouslyExecutedDistribution.NetInSuspense)
          .subtract((MoneySetup.PreviouslyExecutedDistribution.Amount)
          .add(ProducerAmountWrittenOff)).negate()
      }
      else {
        return AgencyCycleDistView.AgencyCycleDist.BaseMoneyReceived.MoneyBeingModified.Amount
      }
    }
    else return 0bd.ofCurrency(MoneySetup.Currency)
  }

  property get ModifiedWrittenOff() : MonetaryAmount {
    if(MoneySetup.PreviouslyExecutedDistribution typeis AgencyCyclePayment){
      var writtenOff = MoneySetup.PreviouslyExecutedDistribution.WriteOffAmount
      return MonetaryAmounts.zeroIfNull(writtenOff, MoneySetup.Currency)
    }
    return 0bd.ofCurrency(MoneySetup.Currency)
  }

  property get ProducerAmountWrittenOff() : MonetaryAmount {
    var query = gw.api.database.Query.make(ProducerWriteoff)

    var contextTable = query.subselect("ID", CompareIn, entity.ProducerContext, "Transaction")

    var prodWriteoffContext = contextTable.cast(ProdWriteoffContext)
    prodWriteoffContext.compare("AgencyCyclePayment", Relop.Equals, MoneySetup.PreviouslyExecutedDistribution)

    var writeoffs = query.select()

    if (writeoffs.Count == 1) {
      return writeoffs.single().Amount
    }

    return 0bd.ofCurrency(MoneySetup.Currency)
  }

  property get IsContinuingSaved() : boolean {
    return MoneySetup.SavedDistribution != null
  }

  property get AllowChangeOfDistribution() : boolean {
    return !IsContinuingSaved && !MoneySetup.EditingExecutedDistribution && !MoneySetup.HasStartedDistributedStep
  }

  property get UnappliedBalanceBefore(): MonetaryAmount {
    return MoneySetup.Producer.UnappliedAmount - ModifiedUnappliedAmount
  }

  property get UnappliedBalanceChange(): MonetaryAmount {
    if (AdjustProducerUnapplied) {
      if(IsCreditDistribution){
        return DistributedAndSuspense.negate()
      } else {
        return Remaining
      }
    }

    return 0bd.ofCurrency(MoneySetup.Producer.Currency)
  }

  property get UnappliedBalanceAfter(): MonetaryAmount {
    if (AdjustProducerUnapplied) {
      if (IsCreditDistribution) {
        return UnappliedBalanceBefore + UnappliedBalanceChange
      } else {
        return UnappliedBalanceBefore + Remaining
      }
    }

    return UnappliedBalanceBefore
  }

  property get WriteOffExpense(): MonetaryAmount {
    if(AdjustProducerUnapplied){
      return 0bd.ofCurrency(MoneySetup.Producer.Currency)
    }

    if(IsCreditDistribution){
      return DistributedAndSuspense
    } else {
      return Remaining.negate()
    }
  }

  property get DistributionTypeName(): String {
    return MoneySetup.DistributionTypeName;
  }

  property get DistributionDifferenceMethodLabel(): String {
    var moneySetupTypeName = DistributionTypeName
    var balanceChange: MonetaryAmount
    if (IsCreditDistribution) {
      balanceChange = DistributedAndSuspense.negate()
    } else {
      balanceChange = Remaining
    }

    return balanceChange.IsNegative ?
        DisplayKey.get("Web.AgencyDistributionWizard.Step.Dispositions.WriteOffDV.DistributionDifferenceMethod.DepleteUnapplied", moneySetupTypeName, balanceChange.negate().render()) :
        DisplayKey.get("Web.AgencyDistributionWizard.Step.Dispositions.WriteOffDV.DistributionDifferenceMethod.LeaveInUnapplied", moneySetupTypeName, balanceChange.render())
  }

  function isNetAmountToApplyInvalid(agencyDistItem : BaseDistItem) : Boolean {
    return isGrossAmountToApplyInvalid(agencyDistItem) || isCommissionAmountToApplyInvalid(agencyDistItem)
  }

  function hasFrozenDistItem(chargeOwnerView: AgencyCycleDistChargeOwnerView) : boolean {
    return !chargeOwnerView.AgencyDistItems.where( \ agencyDistItem -> agencyDistItem.Frozen).Empty
  }

  property get IsDitributingDistWithFrozenDistItemsByPolicy() : boolean {
    return this.DistributeBy == DistributeByEnum.POLICY and this.AgencyCycleDistView.AgencyCycleDist.hasFrozenDistItem()
  }

  function distItemsExcludingFrozen(chargeOwnerView: AgencyCycleDistChargeOwnerView) : List<BaseDistItem> {
    return chargeOwnerView.AgencyDistItems.where(\distItem -> !distItem.Frozen)
  }

  function amountAvailableToDistributeExcludingFrozen(availableAmount: MonetaryAmount, chargeOwnerView: AgencyCycleDistChargeOwnerView) :MonetaryAmount {
    return availableAmount.subtract(frozenDistributedAmount(chargeOwnerView))
  }

  function frozenDistributedAmount(chargeOwnerView: AgencyCycleDistChargeOwnerView) :MonetaryAmount {
    return  chargeOwnerView.AgencyDistItems
              .where(\distItem -> distItem.Frozen)
              .sum(MoneySetup.Currency, \distItem -> distItem.NetAmountToApply)
  }

  function remainingAmount(availableAmount: MonetaryAmount, chargeOwnerView: AgencyCycleDistChargeOwnerView) :MonetaryAmount {
    return availableAmount
            .subtract(chargeOwnerView.TotalNetAmountToDistribute)
  }

  function requiresApproval(exceptionDistItems : BaseDistItem[]):Boolean {
    for (exceptionItem in exceptionDistItems) {
      if (exceptionItem.Disposition == TC_WRITEOFF and lacksWriteoffAuthority(exceptionItem)) {
        return true
      }
    }
    return false
  }

  private function lacksWriteoffAuthority(exceptionDistItem : BaseDistItem) : boolean {
    var currentUser = User.util.CurrentUser
    var itemGrossWriteOffAmount = exceptionDistItem.GrossAmountOwed.subtract(exceptionDistItem.GrossAmountToApply)
    var itemCommissionWriteOffAmount = exceptionDistItem.CommissionAmountOwed.subtract(exceptionDistItem.CommissionAmountToApply)
    var grossItemApproval = Approvables.requiresApproval(currentUser, TC_WRITOEFFITEMGROSS, itemGrossWriteOffAmount)
    var commissionItemApproval = Approvables.requiresApproval(currentUser, TC_WRITEOFFITEMCOMMISSION, itemCommissionWriteOffAmount)
    return not (grossItemApproval.HasAuthority and commissionItemApproval.HasAuthority)
  }
}
