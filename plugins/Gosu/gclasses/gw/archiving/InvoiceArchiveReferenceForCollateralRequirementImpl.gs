package gw.archiving

@Export
class InvoiceArchiveReferenceForCollateralRequirementImpl implements ArchiveReferenceByChargeOwner {
  var _invoiceArchiveReferenceForCollateralRequirement: InvoiceArchiveReferenceForCollateralRequirement

  construct(invoiceArchiveReferenceForCollateralRequirement: InvoiceArchiveReferenceForCollateralRequirement) {
    _invoiceArchiveReferenceForCollateralRequirement = invoiceArchiveReferenceForCollateralRequirement
  }

  override property get ChargeTAccountOwner() : TAccountOwner {
    return _invoiceArchiveReferenceForCollateralRequirement.CollateralRequirement
  }
}