package gw.archiving

/**
 * Defines classes that capture reference data for {@link Freezable}s when
 * they are frozen by a {@link PolicyPeriod} being archived.
 */
@Export
structure ArchiveReferenceByChargeOwner {

  /**
   * The {@link TAccountOwner} of the {@link Charge}s for this archive
   * reference.
   */
  property get ChargeTAccountOwner() : TAccountOwner
}