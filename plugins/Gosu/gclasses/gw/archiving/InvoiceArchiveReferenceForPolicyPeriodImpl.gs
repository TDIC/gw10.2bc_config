package gw.archiving

@Export
class InvoiceArchiveReferenceForPolicyPeriodImpl implements ArchiveReferenceByChargeOwner {
  var _invoiceArchiveReferenceForPolicyPeriod: InvoiceArchiveReferenceForPolicyPeriod

  construct(invoiceArchiveReferenceForPolicyPeriod: InvoiceArchiveReferenceForPolicyPeriod) {
    _invoiceArchiveReferenceForPolicyPeriod = invoiceArchiveReferenceForPolicyPeriod
  }

  override property get ChargeTAccountOwner() : TAccountOwner {
    return _invoiceArchiveReferenceForPolicyPeriod.PolicyPeriod
  }
}