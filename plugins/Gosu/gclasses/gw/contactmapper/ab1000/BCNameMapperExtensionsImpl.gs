package gw.contactmapper.ab1000

uses gw.webservice.contactapi.NameMapper

@Export
class BCNameMapperExtensionsImpl implements IBCNameMapperExtensions {

  override property get Instance(): NameMapper {
    return gw.contactmapper.ab1000.BCNameMapper.Instance
  }
}