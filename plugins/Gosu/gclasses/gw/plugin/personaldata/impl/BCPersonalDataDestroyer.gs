package gw.plugin.personaldata.impl

uses com.google.common.collect.Lists
uses gw.api.database.Query
uses gw.api.personaldata.PersonalDataDestroyer
uses org.apache.commons.lang.NotImplementedException
uses entity.Contact

@Export
class BCPersonalDataDestroyer implements PersonalDataDestroyer {

  override function destroyContact(purgeRequest: PersonalDataContactDestructionRequest): ContactDestructionStatus {
    throw new NotImplementedException("Implement me to fully enable obfuscation")
  }

  override function translateABUIDToPublicIDs(addressBookUID: String): List<String> {
    var contact = Query.make(Contact)
        .compare(Contact#AddressBookUID, Equals, addressBookUID)
        .withFindRetired(true)
        .select()
        .AtMostOneRow

    return contact == null ? Lists.newArrayList<String>() : {contact.PublicID}
  }

  override function doesContactWithPublicIDExist(publicID: String): boolean {
    return loadContactByPublicID(publicID) != null
  }

  override function translatePublicIDtoABUID(publicID: String): String {
    return loadContactByPublicID(publicID).AddressBookUID
  }

  private function loadContactByPublicID(publicID : String) : Contact {
    return Query.make(Contact)
        .compare(Contact#PublicID, Equals, publicID)
        .withFindRetired(true)
        .select()
        .AtMostOneRow
  }
}