package gw.plugin.document.impl

uses gw.plugin.document.IDocumentMetadataSource
/**
 *
 * IMPORTANT: This implementation is for Demo purpose only. Please do not modify it. Use it as an example for your
 * IDocumentMetadataSource implementation and define it in the plugin-gosu for your
 * IDocumentMetadataSource.gwp.
 *
 * IDocumentMetadataSource implementation for document management.
 *
 * This plugin assumes that the validations for the original fields of the Document entity
 * are done at the UI level outside of the plugin.
 */
@Export
class LocalDocumentMetadataSource extends BaseLocalDocumentMetadataSource implements IDocumentMetadataSource {
  construct() {
  }

  protected override function documentMatchesCriteria( doc : Document, criteria : DocumentSearchCriteria) : boolean  {
    if (not super.documentMatchesCriteria( doc, criteria )) {
      return false;
    }
    if (criteria.getAccount() != null) {
      if (criteria.getAccount() != doc.getAccount()) {
        return false;
      }
    }
    if (criteria.getPolicy() != null) {
      if (criteria.getPolicy() != doc.getPolicy()) {
        return false;
      }
    }
    if (criteria.getProducer() != null) {
      if (criteria.getProducer() != doc.getProducer()) {
        return false;
      }
    }
    return true
  }
}
