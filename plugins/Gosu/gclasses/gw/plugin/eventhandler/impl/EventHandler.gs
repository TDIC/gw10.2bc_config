package gw.plugin.eventhandler.impl;

uses gw.api.database.Query
uses gw.api.database.QuerySelectColumns
uses gw.api.domain.delinquency.DelinquencyTarget
uses gw.api.locale.DisplayKey
uses gw.api.path.Paths
uses gw.api.system.BCLoggerCategory
uses gw.api.util.DateUtil
uses gw.pl.currency.MonetaryAmount
uses gw.plugin.eventhandler.IEventHandler
uses entity.Disbursement

@Export
class EventHandler implements IEventHandler {

  private var _logger = BCLoggerCategory.DELINQUENCY_PROCESS;

  construct() {
  }

  public override function canClosePolicyPeriod(policyPeriod: PolicyPeriod) : Boolean {
    if (_logger.DebugEnabled ) _logger.debug(
          "EVENT HANDLED: PolicyPeriod [" + policyPeriod.PolicyNumber + "] about to be closed");
    return true;
  }

  public override function canStartDelinquencyProcess(target : DelinquencyTarget) : Boolean {
    if (_logger.DebugEnabled ) _logger.debug(
          "EVENT HANDLED: canStartDelinquencyProcess on [" + target.TargetDisplayName + "] ");
    return true;
  }

  public override function calculateAccountDelinquentAmountOverrideValue(account : Account) : MonetaryAmount {
    return null;
  }

  public override function calculateInvoiceDelinquentAmountOverrideValue(accountInvoice : AccountInvoice) : MonetaryAmount {
    /*
     * calculateInvoiceDelinquentAmountOverrideValue(AccountInvoice accountInvoice) is newly added in 9.0.1, customer
     * may not be aware of this plugin method and implement their logic only on
     * calculateAccountDelinquentAmountOverrideValue(Account account). However, the way this was implemented, will cause
     * calculateAccountDelinquentAmountOverrideValue(Account account) not to be called anymore in certain scenarios and
     * calculateInvoiceDelinquentAmountOverrideValue(AccountInvoice accountInvoice) will be called instead (which doesn't
     * contain custom code). This means customer's logic will no longer run, the behavior is gonna change and it will
     * take them a while to figure out why.
     *
     * In order to avoid this, we call calculateAccountDelinquentAmountOverrideValue(Account account) inside the default
     * implementation of calculateInvoiceDelinquentAmountOverrideValue(AccountInvoice accountInvoice). If customer wasn't
     * aware of the newly added plugin method, we would like keep their logic in consistency.
     *
     * Customer could change any of those implementation to fulfill their purpose.
     * */
    return calculateAccountDelinquentAmountOverrideValue(accountInvoice.Account)
  }

  public override function calculateAccountPoliciesDelinquentAmountOverrideValue(account : Account) : MonetaryAmount {
    return null;
  }

  public override function calculateAccountRawDelinquentAmountOverrideValue(account : Account) : MonetaryAmount {
    return null;
  }

  public override function calculatePolicyPeriodDelinquentAmountOverrideValue(policyPeriod : PolicyPeriod) : MonetaryAmount {
    return null;
  }

  public override function calculateCollateralDelinquentAmountOverrideValue(collateral : Collateral) : MonetaryAmount {
    return null;
  }

  public override function policyPeriodTransferredToNewProducer(policyPeriod : PolicyPeriod,
                                                       oldProducerCode : ProducerCode,
                                                       newProducerCode : ProducerCode) {
    if (_logger.DebugEnabled ) _logger.debug(
          "EVENT HANDLED: PolicyPeriod [" + policyPeriod.DisplayName + "] transferred from [" +
          oldProducerCode.Code + "] producer code to [" + newProducerCode.Code + "] producer code");
  }

  public override function validateManualDisbursementAmount(amount : MonetaryAmount, unapplied : MonetaryAmount,
                                pendingDisbursement : MonetaryAmount, disbursement : Disbursement) : Boolean {
    if (_logger.DebugEnabled ) _logger.debug(
          "EVENT HANDLED: validateManualDisbursementAmount  [" + disbursement + "]");
    return amount <= (unapplied - pendingDisbursement);
  }

  public override function rejectDisbursementBehavior(disbursementToReject : Disbursement): Boolean {
    return true;
  }

  public override function updateDisbursementStatusAndAmount(disbursement : Disbursement,
                                                    isAboutToExecuteDisbursement : boolean) {

    if (_logger.DebugEnabled ) {
      _logger.debug("EVENT HANDLED: updateDisbursementStatusAndAmountAsNecessary  [" + disbursement + "]");
    }
    
    // Update status and amount only if we are dealing with an AccountDisbursement or an AgencyDisbursement
    if (!(disbursement typeis AccountDisbursement || disbursement typeis AgencyDisbursement)) {
      return
    }

    if(not (disbursement.Status == TC_APPROVED or disbursement.Status == TC_AWAITINGAPPROVAL)) {
      return
    }
         
    var availableForDisbursement = disbursement typeis AccountDisbursement ? disbursement.UnappliedFund.Balance
                                                                           : disbursement.DisbursementTarget.UnappliedAmount

    // If amount in unapplied fund is enough to cover the full disbursement amount, no need to update anything
    if (availableForDisbursement.IsPositive and availableForDisbursement >= disbursement.Amount) {
      return
    }

    // Disbursement is larger than available funds, so we will adjust disbursement and post an explanatory note. 
    // Get the formatted values that will be put into strings that go into note subject and body.
    var formattedDisbursementAmount = disbursement.Amount.render()
    var formattedAvailableFunds = availableForDisbursement.render()
    var formattedDate = DateUtil.currentDate().format("short")
    var entityTypeDisplayName = (disbursement typeis AccountDisbursement) ? Account.Type.TypeInfo.Name : Producer.Type.TypeInfo.Name
          
    // Create new Note and hook it to the entity targeted by this disbursement
    var note = new Note(disbursement.Bundle);
    (disbursement.DisbursementTarget as NoteContainer).addNote(note);

    // Based on whether there were some funds in the unapplied fund but not quite enough to cover the disbursement, or whether
    // there were simply no money available at all, create an appropriate Note subject and body and adjust the disbursement
    if (availableForDisbursement.IsPositive) {
      
      // Some funds were available for disbursement, but not enough to cover the full disbursement amount.
      // Post note that says disbursement was altered due to not enough funds and new amount was posted.
      note.Subject = DisplayKey.get("Java.DisbursementManualNote.AmountChanged.Subject");
      note.Body = DisplayKey.get("Java.DisbursementManualNote.AmountChanged.Body", formattedDisbursementAmount,
                      formattedAvailableFunds, entityTypeDisplayName, formattedDate);

      // Adjust disbursement amount downward to be equal to the amount actually available to disburse
      disbursement.Amount = availableForDisbursement;

    } else {
      
      // No funds at all were available for disbursement. Post note that says disbursement was canceled due to 
      // no available unapplied fund amount to disburse
      note.Subject = DisplayKey.get("Java.DisbursementManualNote.Canceled.Subject");
      note.Body = DisplayKey.get("Java.DisbursementManualNote.Canceled.Body", formattedDisbursementAmount,
                      formattedDate, entityTypeDisplayName);

      // Mark the disbursement as closed
      disbursement.Status = DisbursementStatus.TC_REAPPLIED;
      disbursement.CloseDate = DateUtil.currentDate();           
    }
    
  }

  public override function matchAgencyBillSuspensePolicyPeriod(agencySuspDistItem : BaseSuspDistItem): PolicyPeriod {
    // if there's no PolicyNumber specified, then just return null.
    if (agencySuspDistItem.PolicyNumber == null) {
      return null
    }
    // Query for a policy period that matches the string policy period field on the passed-in AgencySuspDistItem
    var matchingPolicyPeriods = Query.make(PolicyPeriod)
                                .compare(PolicyPeriod#PolicyNumber, Equals, agencySuspDistItem.PolicyNumber)
                                .compare(PolicyPeriod#ArchiveState, Equals, null)
                                .select()
                                .orderByDescending(QuerySelectColumns.path(Paths.make(PolicyPeriod#PolicyPerEffDate))); // to return the  latest in case there is more than one term

    // Return the policy period that was found (if the query did not find one, we will return null)
    return matchingPolicyPeriods.getFirstResult()
  }

  public override function processAgencyBillSuspenseItemAfterPolicyMatch(agencySuspDistItem : BaseSuspDistItem) {
    // Out of the box, do nothing
  }
}
