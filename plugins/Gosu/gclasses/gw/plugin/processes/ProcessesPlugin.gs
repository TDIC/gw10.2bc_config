package gw.plugin.processes

uses gw.plugin.processing.IProcessesPlugin
uses gw.processes.BatchProcess
uses gw.processes.UpgradeProducerStatementHoldLimits

/**
 * Map out any Gosu batch process implementation for batch process types
 */
@Export
class ProcessesPlugin implements IProcessesPlugin {

    construct() {
    }

    override function createBatchProcess(type : BatchProcessType, arguments : Object[]) : BatchProcess {
      switch(type) {
        case BatchProcessType.TC_UPGRADEPRODUCERSTATEMENTHOLDLIMITS:
          return new UpgradeProducerStatementHoldLimits();
        default:
          return null
      }
    }

}