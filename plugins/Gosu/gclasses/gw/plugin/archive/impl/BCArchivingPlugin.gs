package gw.plugin.archive.impl

uses gw.entity.ILinkPropertyInfo
uses gw.plugin.archive.IBCArchivingPlugin

uses java.util.Set

/**
 * Defines the default plugin for {@link IBCArchivingPlugin}.
 */
@Export
class BCArchivingPlugin implements IBCArchivingPlugin {

  override function safeLinksIntoDomain() : Set<ILinkPropertyInfo> {
    return {}
  }

  override function safeCrossDomainLinks() : Set<ILinkPropertyInfo> { return {} }
}