package gw.plugin.approval.impl

uses entity.Disbursement
uses entity.NegativeWriteoff
uses entity.Writeoff
uses gw.api.database.Queries
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.domain.approval.Approvables
uses gw.api.locale.DisplayKey
uses gw.api.system.BCLoggerCategory
uses gw.api.util.DisplayableException
uses gw.bc.approval.ActivityApprovable
uses gw.i18n.DateTimeFormat
uses gw.plugin.approval.IActivityApprovablePlugin
uses entity.Transaction

@Export
class ActivityApprovablePlugin implements IActivityApprovablePlugin {

  //reminder: ActivityApprovable can also be cast to Approvable
  override function onRejected(activityApprovable: ActivityApprovable, reasonForRejection: String) {
    /*
    Note that the deprecated method BCApprovalHandlerUtil.onRejected() erroneously exposes the internal class
    BCApprovalHandler. This plugin method replaces it and is instead passed a publicly-available ActivityApprovable entity.
     */
    //noinspection GosuDeprecatedAPIUsage
    Approvables.call_BCApprovalHandlerUtil_onRejected(activityApprovable, reasonForRejection)
  }

  override function updateDisbursementAuthorityEvent(approvableBean: Disbursement) {
    var authorityEvent = approvableBean.AuthorityEvent
    if (authorityEvent != null) {
      authorityEvent.User = User.util.CurrentUser
    }
  }

  override function isCanApproveAdvanceCmsnPayment(advanceCmsnPayment: AdvanceCmsnPayment, isCurrentUserCanApproveAction: Boolean): Boolean {
    return isCurrentUserCanApproveAction
  }

  override function isCanApproveBonusCmsnPayment(bonusCmsnPayment: BonusCmsnPayment, isCurrentUserCanApproveAction: Boolean): Boolean {
    return isCurrentUserCanApproveAction
  }

  override function isCanApproveChargeReversal(chargeReversal: ChargeReversal, isCurrentUserCanApproveAction: Boolean): Boolean {
    return isCurrentUserCanApproveAction
  }

  override function isCanApproveDisbursement(disbursement: Disbursement, isCurrentUserCanApproveAction: Boolean): Boolean {
    /*
    if ( disbursement.Reason == Reason.TC_AUTOMATIC ) {
      return true
    }
    */
    return isCurrentUserCanApproveAction
  }

  override function isCanApproveFundsTransfer(fundsTransfer: FundsTransfer, isCurrentUserCanApproveAction: Boolean): Boolean {
    return isCurrentUserCanApproveAction
  }

  override function isCanApproveFundsTransferReversal(fundsTransferReversal: FundsTransferReversal, isCurrentUserCanApproveAction: Boolean): Boolean {
    return isCurrentUserCanApproveAction
  }

  override function isCanApproveNegativeWriteoff(negativeWriteoff: NegativeWriteoff, isCurrentUserCanApproveAction: Boolean): Boolean {
    return isCurrentUserCanApproveAction
  }

  override function isCanApproveWriteoff(writeoff: Writeoff, isCurrentUserCanApproveAction: Boolean): Boolean {
    return isCurrentUserCanApproveAction
  }

  override function isCanApproveWriteoffReversal(writeoffReversal: WriteoffReversal, isCurrentUserCanApproveAction: Boolean): Boolean {
    return isCurrentUserCanApproveAction
  }

  override function isCanApproveNegativeWriteoffRev(negativeWriteoffReversal: NegativeWriteoffRev, isCurrentUserCanApproveAction: Boolean): Boolean {
    return isCurrentUserCanApproveAction
  }

  override function isCanApproveCredit(credit: Credit, isCurrentUserCanApproveAction: Boolean): Boolean {
    return isCurrentUserCanApproveAction
  }

  override function isCanApproveCreditReversal(creditReversal: CreditReversal, isCurrentUserCanApproveAction: Boolean): Boolean {
    return isCurrentUserCanApproveAction
  }

  override function isCanApproveProducerPayableTransfer(transfer: ProducerPayableTransfer, isCurrentUserCanApproveAction: Boolean): Boolean {
    return isCurrentUserCanApproveAction
  }

  override function assignActivityCreatedByAppr(activity: Activity) {
    var currentUser = User.util.CurrentUser

    if (currentUser.GroupUsers.length == 0) {
      throw new DisplayableException(DisplayKey.get('Java.Approval.Required.NoSupervisor'))
    }

    var supervisorGroup = currentUser.GroupUsers[0].Group
    var supervisor = supervisorGroup.Supervisor
    activity.assign(supervisorGroup, supervisor)
  }

  override function reassignActivityCreatedByApprToUserWithSufficientAuthority(activity: Activity) {
    activity.assign(activity.AssignedGroup, activity.AssignedGroup.Supervisor )
  }

  public function addHistoryEventForRejectedAdvanceCmsnPayment(advanceCmsnPayment: AdvanceCmsnPayment) {
    addHistoryEvent(
        advanceCmsnPayment.Producer,
        HistoryEventType.TC_ADVANCECMSNPAYMENT,
        \-> {
          return DisplayKey.get("Java.HistoryEvent.RejectedApproval.AdvanceCmsnPayment",
              advanceCmsnPayment.Amount,
              advanceCmsnPayment.RequestingUser,
              advanceCmsnPayment.CreateTime.formatDate(DateTimeFormat.SHORT))
        })
  }

  public function addHistoryEventForRejectedBonusCmsnPayment(bonusCmsnPayment: BonusCmsnPayment) {
    addHistoryEvent(
        bonusCmsnPayment.Producer,
        HistoryEventType.TC_BONUSCMSNPAYMENT,
        \-> {
          return DisplayKey.get("Java.HistoryEvent.RejectedApproval.BonusCmsnPayment",
              bonusCmsnPayment.Amount,
              bonusCmsnPayment.PolicyCommission.ProducerCode,
              bonusCmsnPayment.RequestingUser,
              bonusCmsnPayment.CreateTime.formatDate(DateTimeFormat.SHORT))
        })
  }

  public function addHistoryEventForRejectedChargeReversal(chargeReversal: ChargeReversal) {
    addHistoryEvent(
        chargeReversal.Charge.TAccountOwner,
        HistoryEventType.TC_CHARGEREVERSED,
        \-> {
          return DisplayKey.get("Java.HistoryEvent.RejectedApproval.ChargeReversal",
              chargeReversal.Charge.Amount,
              chargeReversal.RequestingUser,
              //the following values make it possible to locate the Charge on the AccountDetailCharges page
              chargeReversal.CreateTime.formatDate(DateTimeFormat.SHORT),
              chargeReversal.Charge.ChargeDate.formatDate(DateTimeFormat.SHORT),
              chargeReversal.Charge,
              chargeReversal.Charge.BillingInstruction != null ? chargeReversal.Charge.BillingInstruction : " ",
              chargeReversal.Charge.ChargeGroup.NotBlank ? chargeReversal.Charge.ChargeGroup : " "
          )
        })
  }

  public function addHistoryEventForRejectedCredit(credit: Credit) {
    addHistoryEvent(
        credit.Account,
        HistoryEventType.TC_ACCOUNTCREDITED,
        \-> {
          return DisplayKey.get("Java.HistoryEvent.RejectedApproval.Credit",
              credit.Amount,
              credit.RequestingUser,
              credit.CreateTime.formatDate(DateTimeFormat.SHORT),
              credit.CreditType.DisplayName)
        })
  }

  public function addHistoryEventForRejectedCreditReversal(creditReversal: CreditReversal) {
    var query: Query<CreditTransaction> = Queries.createQuery(CreditTransaction.TYPE.get())
    query.join(CreditTransaction.CREDITCONTEXT_PROP.get()).compare(CreditContext.CREDIT_PROP.get(), Relop.Equals, creditReversal.Credit)
    var transaction = query.select().getAtMostOneRow()

    addHistoryEvent(
        creditReversal.Credit.Account,
        HistoryEventType.TC_ACCOUNTCREDITREVERSED,
        \-> {
          return DisplayKey.get("Java.HistoryEvent.RejectedApproval.CreditReversal",
              creditReversal.Credit.Amount,
              creditReversal.RequestingUser,
              creditReversal.CreateTime.formatDate(DateTimeFormat.SHORT),
              creditReversal.Credit.CreditType.DisplayName,
              transaction.TransactionDate.formatDate(DateTimeFormat.SHORT),
              transaction.TransactionNumber)
        })
  }

  public function addHistoryEventForRejectedDisbursement(disbursement: Disbursement) {
    addHistoryEvent(
        disbursement.DisbursementTarget,
        HistoryEventType.TC_DISBURSEMENT,
        \-> {
          return DisplayKey.get("Java.HistoryEvent.RejectedApproval.Disbursement",
              disbursement.Amount,
              disbursement.RequestingUser,
              disbursement.CreateTime.formatDate(DateTimeFormat.SHORT),
              disbursement.Reason)
        })
  }

  public function addHistoryEventForRejectedDisbursement(collateralDisbursement: CollateralDisbursement) {
    addHistoryEvent(
        (collateralDisbursement.DisbursementTarget as Collateral).Account,
        HistoryEventType.TC_DISBURSEMENT,
        \-> {
          return DisplayKey.get("Java.HistoryEvent.RejectedApproval.CollateralDisbursement",
              collateralDisbursement.Amount,
              collateralDisbursement.RequestingUser,
              collateralDisbursement.CreateTime.formatDate(DateTimeFormat.SHORT),
              collateralDisbursement.Reason)
        })
  }

  public function addHistoryEventForRejectedFundsTransfer(fundsTransfer: FundsTransfer) {
    var sourceTAccountOwner: TAccountOwner
    var historyEventType: HistoryEventType
    if (fundsTransfer.SourceUnapplied != null) {
      sourceTAccountOwner = fundsTransfer.SourceUnapplied.Account
      historyEventType = HistoryEventType.TC_ACCOUNTFUNDTRANSFER
    } else {
      sourceTAccountOwner = fundsTransfer.SourceProducer
      historyEventType = HistoryEventType.TC_PRODUCERFUNDTRANSFER
    }
    addHistoryEvent(
        sourceTAccountOwner,
        historyEventType,
        \-> {
          var description: String
          if (fundsTransfer.TargetUnapplied.Account != null) {
            description = DisplayKey.get("Java.HistoryEvent.RejectedApproval.FundsTransferToAccount",
                fundsTransfer.Amount,
                fundsTransfer.TargetUnapplied.Account,
                fundsTransfer.RequestingUser,
                fundsTransfer.CreateTime.formatDate(DateTimeFormat.SHORT))
          } else if (fundsTransfer.TargetProducer != null) {
            description = DisplayKey.get("Java.HistoryEvent.RejectedApproval.FundsTransferToProducer",
                fundsTransfer.Amount,
                fundsTransfer.TargetProducer,
                fundsTransfer.RequestingUser,
                fundsTransfer.CreateTime.formatDate(DateTimeFormat.SHORT))
          }
          return description
        })
  }

  public function addHistoryEventForRejectedFundsTransferReversal(fundsTransferReversal: FundsTransferReversal) {
    final var fundsTransfer = fundsTransferReversal.FundsTransferTransaction.FundsTransfer
    var sourceTAccountOwner: TAccountOwner
    var historyEventType: HistoryEventType
    if (fundsTransfer.SourceUnapplied != null) {
      sourceTAccountOwner = fundsTransfer.SourceUnapplied.Account
      historyEventType = HistoryEventType.TC_ACCOUNTFUNDTRANSFERREV
    } else {
      sourceTAccountOwner = fundsTransfer.SourceProducer
      historyEventType = HistoryEventType.TC_PRODUCERFUNDTRANSFERREV
    }
    addHistoryEvent(
        sourceTAccountOwner,
        historyEventType,
        \-> {
          var description: String
          if (fundsTransfer.TargetUnapplied.Account != null) {
            description = DisplayKey.get("Java.HistoryEvent.RejectedApproval.FundsTransferToAccountReversal",
                fundsTransfer.Amount,
                fundsTransfer.TargetUnapplied.Account,
                fundsTransferReversal.RequestingUser,
                fundsTransfer.CreateTime.formatDate(DateTimeFormat.SHORT),
                fundsTransfer.TransferTransaction.TransactionDate.formatDate(DateTimeFormat.SHORT),
                fundsTransfer.TransferTransaction.TransactionNumber
            )
          } else if (fundsTransfer.TargetProducer != null) {
            description = DisplayKey.get("Java.HistoryEvent.RejectedApproval.FundsTransferToProducerReversal",
                fundsTransfer.Amount,
                fundsTransfer.TargetProducer,
                fundsTransferReversal.RequestingUser,
                fundsTransfer.CreateTime.formatDate(DateTimeFormat.SHORT),
                fundsTransfer.TransferTransaction.TransactionDate.formatDate(DateTimeFormat.SHORT),
                fundsTransfer.TransferTransaction.TransactionNumber
            )
          }
          return description
        })
  }

  public function addHistoryEventForRejectedNegativeWriteoff(negativeWriteoff: NegativeWriteoff) {
    addHistoryEvent(
        negativeWriteoff.TAccountOwner,
        HistoryEventType.TC_NEGATIVEWRITEOFF,
        \-> {
          return DisplayKey.get("Java.HistoryEvent.RejectedApproval.NegativeWriteoff",
              negativeWriteoff.Amount,
              negativeWriteoff.RequestingUser,
              negativeWriteoff.CreateTime.formatDate(DateTimeFormat.SHORT))
        })
  }

  public function addHistoryEventForRejectedNegativeWriteoffReversal(negativeWriteoffRev: NegativeWriteoffRev) {
    var transaction: Transaction
    if (negativeWriteoffRev.NegativeWriteoff typeis AcctNegativeWriteoff) {
      var query: Query<AccountNegativeWriteoffTxn> = Queries.createQuery(AccountNegativeWriteoffTxn.TYPE.get())
      var contextTable = query.join(AccountNegativeWriteoffTxn.ACCOUNTCONTEXT_PROP.get())
      contextTable.cast(AccountNegativeWriteoffContext.TYPE.get())
      contextTable.compare(AccountNegativeWriteoffContext.ACCTNEGATIVEWRITEOFF_PROP.get(), Relop.Equals, negativeWriteoffRev.NegativeWriteoff)
      transaction = query.select().getAtMostOneRow()
    } else if (negativeWriteoffRev.NegativeWriteoff typeis ProdNegativeWriteoff) {
      var query: Query<ProducerNegWriteoffTxn> = Queries.createQuery(ProducerNegWriteoffTxn.TYPE.get())
      var contextTable = query.join(ProducerNegWriteoffTxn.PRODUCERCONTEXT_PROP.get())
      contextTable.cast(NegWriteoffProdContext.TYPE.get())
      contextTable.compare(NegWriteoffProdContext.PRODNEGATIVEWRITEOFF_PROP.get(), Relop.Equals, negativeWriteoffRev.NegativeWriteoff)
      transaction = query.select().getAtMostOneRow()
    }

    addHistoryEvent(
        negativeWriteoffRev.NegativeWriteoff.TAccountOwner,
        HistoryEventType.TC_NEGATIVEWRITEOFFREV,
        \-> {
          return DisplayKey.get("Java.HistoryEvent.RejectedApproval.NegativeWriteoffReversal",
              negativeWriteoffRev.NegativeWriteoff.Amount,
              negativeWriteoffRev.RequestingUser,
              negativeWriteoffRev.CreateTime.formatDate(DateTimeFormat.SHORT),
              transaction.TransactionDate.formatDate(DateTimeFormat.SHORT),
              transaction.TransactionNumber)
        })
  }

  public function addHistoryEventForRejectedProducerPayableTransfer(producerPayableTransfer: ProducerPayableTransfer) {
    addHistoryEvent(
        producerPayableTransfer.DebitsPayableOf,
        HistoryEventType.TC_PRODUCERPAYABLETRANSFER,
        \-> {
          return DisplayKey.get("Java.HistoryEvent.RejectedApproval.ProducerPayableTransfer",
              producerPayableTransfer.Amount,
              producerPayableTransfer.CreditsPayableOf,
              producerPayableTransfer.RequestingUser,
              producerPayableTransfer.CreateTime.formatDate(DateTimeFormat.SHORT))
        })
  }

  public function addHistoryEventForRejectedWriteoff(writeoff: Writeoff) {
    var tAccountOwner = getTAccountOwnerForWriteoff(writeoff)

    var historyEventType: HistoryEventType
    switch (typeof tAccountOwner) {
      case Account:
        historyEventType = HistoryEventType.TC_ACCOUNTWRITEOFF
        break
      case PolicyPeriod:
        historyEventType = HistoryEventType.TC_POLICYWRITEOFF
        break
      case Producer:
        historyEventType = HistoryEventType.TC_PRODUCERWRITEOFF
        break
      default:
        throw new IllegalArgumentException(tAccountOwner.Class.SimpleName)
    }

    addHistoryEvent(
        tAccountOwner,
        historyEventType,
        \-> {
          return DisplayKey.get("Java.HistoryEvent.RejectedApproval.Writeoff",
              writeoff.WriteOffType,
              writeoff.Amount,
              writeoff.RequestingUser,
              writeoff.CreateTime.formatDate(DateTimeFormat.SHORT),
              writeoff.Reason)
        })
  }

  public function addHistoryEventForRejectedWriteoffReversal(writeoffReversal: WriteoffReversal) {
    addHistoryEvent(
        getTAccountOwnerForWriteoff(writeoffReversal.Writeoff),
        HistoryEventType.TC_ACCOUNTWRITEOFFREV,
        \-> {
          return DisplayKey.get("Java.HistoryEvent.RejectedApproval.WriteoffReversal",
              writeoffReversal.Writeoff.Amount,
              writeoffReversal.RequestingUser,
              writeoffReversal.CreateTime.formatDate(DateTimeFormat.SHORT),
              writeoffReversal.Writeoff.WriteoffTxns.FirstResult.TransactionDate.formatDate(DateTimeFormat.SHORT),
              writeoffReversal.Writeoff.WriteoffTxns.FirstResult.TransactionNumber)
        })
  }

  // helper methods

  private function getTAccountOwnerForWriteoff(writeoff: Writeoff): TAccountOwner {
    var tAccountOwner: TAccountOwner
    switch (typeof writeoff) {
      case ChargeCommissionWriteoff:
        tAccountOwner = writeoff.ChargeCommission.PolicyCommission.ProducerCode.Producer
        break
      case ItemCommissionWriteoff:
        tAccountOwner = writeoff.ItemCommission.PolicyCommission.ProducerCode.Producer
        break
      default:
        tAccountOwner = writeoff.TAccountOwner
    }
    return tAccountOwner
  }

  private function addHistoryEvent(tAccountOwner: TAccountOwner, historyEventType: HistoryEventType, createDescription: block(): String) {
    if (tAccountOwner typeis HistoryEventContainer) {
      tAccountOwner.addHistoryFromGosu(gw.api.util.DateUtil.currentDate(), historyEventType, createDescription(), null, null, false)
    } else {
      BCLoggerCategory.PLUGIN.error(DisplayKey.get("Java.HistoryEvent.RejectedApproval.InvalidContainerException", tAccountOwner, tAccountOwner.IntrinsicType, historyEventType, createDescription()))
    }
  }
}