package gw.plugin.contact.impl

uses gw.plugin.contact.IContactConfigPlugin

@Export
class ContactConfigPlugin implements IContactConfigPlugin {
    override function minimumCriteriaSet(searchCriteria : ContactSearchCriteria) : boolean {
      //US122, 01/05/2015 Vicente, Validate Company Name if Company type
      //or First or Last Contact or ADA # if Person Type
      return nameCriteriaSet(searchCriteria) or adaNumberCriteriaSet_TDIC(searchCriteria)

    }

   private function nameCriteriaSet(searchCriteria : ContactSearchCriteria) : boolean {
    // Keyword functions as LastName for Persons, and Name for all other contact types
    if (searchCriteria.ContactSubtype == typekey.Contact.TC_PERSON) {
      return (searchCriteria.FirstName.HasContent or searchCriteria.Keyword.HasContent
              or searchCriteria.FirstNameKanji.HasContent or searchCriteria.KeywordKanji.HasContent)
    } else {
      return searchCriteria.Keyword.HasContent or searchCriteria.KeywordKanji.HasContent
    }
  }

  private function taxIdCriteriaSet(searchCriteria : ContactSearchCriteria) : boolean {
    return searchCriteria.TaxID.HasContent
  }

  private function addressCriteriaSet(searchCriteria : ContactSearchCriteria) : boolean {
    var address = searchCriteria.Address
    return (address.City.HasContent and address.State != null)
  }

  /**
   * US122
   * 01/14/2015 Vicente
   *
   * Returns true if the contact type is Person and the ADA Number is not empty
   */
  @Returns("Returns true if the contact type is Person and the ADA Number is not empty")
  private function adaNumberCriteriaSet_TDIC(searchCriteria : ContactSearchCriteria) : boolean {
    return (searchCriteria.ContactSubtype == typekey.Contact.TC_PERSON and searchCriteria.ADANumber_TDIC != null)
  }
}
