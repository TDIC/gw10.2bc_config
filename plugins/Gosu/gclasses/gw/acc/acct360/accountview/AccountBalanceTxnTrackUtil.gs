package gw.acc.acct360.accountview

uses gw.api.util.DateUtil
uses gw.pl.persistence.core.Bundle
uses org.slf4j.LoggerFactory
uses java.lang.invoke.MethodHandles

/**
 * Account 360 Accelerator
 * This class is used for Account Balance Summary Transaction calculation.
 * It contains all the functions necessary to create the tracking entities
 *
 **/
class AccountBalanceTxnTrackUtil {

  static function createChargeBilledRecordsWithBundle(bundle : Bundle, chrgBilledTxn: ChargeBilled) {
    var chargeBilledCtx = chrgBilledTxn.ChargeInstanceCtx
    var plcyPeriod = chrgBilledTxn.InvoiceItem?.PolicyPeriod
    var invoiceObj : Invoice
    var txnDate = chrgBilledTxn.TransactionDate
    var acctObj = chrgBilledTxn.Charge.OwnerAccount

    if (chrgBilledTxn.InvoiceItem.ReversedInvoiceItem != null) {
      invoiceObj = chrgBilledTxn.InvoiceItem.ReversedInvoiceItem.Invoice
    } else {
      invoiceObj = chrgBilledTxn.InvoiceItem.Invoice
    }

    logInfoDataTransaction(acctObj, plcyPeriod, chrgBilledTxn)

    if (plcyPeriod != null) {
      var existingSummaryRecord = acctObj.AccountBalanceTxns_Ext.where(\tran -> tran.New && tran.PolicyPeriod == plcyPeriod
          and tran.TransactionDate!= null and tran.TransactionDate.compareIgnoreTime(txnDate) == 0
          and tran typeis AccountBalanceTxnBilled_ext and (tran as AccountBalanceTxnBilled_ext).Invoice == invoiceObj).first()

      if (existingSummaryRecord == null ) {
        var newSummaryRecord = bundle.newBeanInstance(AccountBalanceTxnBilled_ext) as AccountBalanceTxnBilled_ext
        newSummaryRecord.Account = acctObj
        newSummaryRecord.PolicyPeriod = plcyPeriod
        newSummaryRecord.TransactionDate = txnDate
        newSummaryRecord.Invoice = invoiceObj
        newSummaryRecord.addToTransactions(chargeBilledCtx)
        newSummaryRecord.DueAmount = chrgBilledTxn.InvoiceItem.Amount
        newSummaryRecord.PaidAmount = 0bd.ofCurrency(chrgBilledTxn.Currency)
        newSummaryRecord.BalanceAmount = 0bd.ofCurrency(acctObj.Currency)
        acctObj.addToAccountBalanceTxns_Ext(newSummaryRecord)
      } else if (existingSummaryRecord != null){
        existingSummaryRecord = bundle.add(existingSummaryRecord)
        existingSummaryRecord.DueAmount = existingSummaryRecord.DueAmount + chrgBilledTxn.InvoiceItem.Amount
        existingSummaryRecord.addToTransactions(chargeBilledCtx)
      } // end policy period
    } else {

      var moveItemRecord = acctObj.AccountBalanceTxns_Ext.where(\tran -> tran.New
          and tran.TransactionDate!= null and tran.TransactionDate.compareIgnoreTime(txnDate) == 0
          and tran typeis AccountBalanceItemMoved_ext and (tran as AccountBalanceTxnBilled_ext).Invoice == invoiceObj).first()
          as AccountBalanceItemMoved_ext

      if (moveItemRecord != null) {
        moveItemRecord.DueAmount = chrgBilledTxn.InvoiceItem.Amount
        moveItemRecord.PaidAmount = 0bd.ofCurrency(chrgBilledTxn.Currency)
        moveItemRecord.addToTransactions(chargeBilledCtx)
        moveItemRecord.TransactionDate = txnDate
        moveItemRecord.Invoice = invoiceObj
        moveItemRecord.BalanceAmount = 0bd.ofCurrency(acctObj.Currency)
      } else {
        var objectCreated = bundle.newBeanInstance(AccountBalanceTxnBilled_ext)
        var newSummaryRecord = objectCreated as AccountBalanceTxnBilled_ext
        newSummaryRecord.Account = acctObj
        newSummaryRecord.TransactionDate = txnDate
        newSummaryRecord.Invoice = invoiceObj
        newSummaryRecord.addToTransactions(chargeBilledCtx)
        newSummaryRecord.DueAmount = chrgBilledTxn.InvoiceItem.Amount
        newSummaryRecord.PaidAmount =  0bd.ofCurrency(chrgBilledTxn.Currency)
        newSummaryRecord.BalanceAmount = 0bd.ofCurrency(acctObj.Currency)
        acctObj.addToAccountBalanceTxns_Ext(newSummaryRecord)
      }

    }
  }

  /**
   * Description: This method is used for inserting/updating records into AccountBalanceTxn
   * entity for ChargePaidFromAccount and ChargePaidFromProducer transactions
   *
   * @param bundle Bundle
   * @param txn ChargePaidFromAccount Transaction
   */
  static function createChargePaidFromAccountRecords(bundle : Bundle, chrgPaidFromAccountTxn : ChargePaidFromUnapplied) {
    var chargePaidCtx = chrgPaidFromAccountTxn.ChargeInstanceCtx
    var plcyPeriod = chrgPaidFromAccountTxn.PaymentItem.PolicyPeriod
    var txnDate = chrgPaidFromAccountTxn.TransactionDate
    var acctObj = chrgPaidFromAccountTxn.Charge.OwnerAccount
    var amountPaidFromUnapplied = 0bd.ofCurrency(chrgPaidFromAccountTxn.Currency)
    if (chrgPaidFromAccountTxn typeis ChargePaidFromAccount) {
      amountPaidFromUnapplied = (!chrgPaidFromAccountTxn.Reversal) ? (chrgPaidFromAccountTxn.PaymentItem.BaseDist.BaseMoneyReceived as DirectBillMoneyRcvd).AmountDistributedFromUnapplied
          : (chrgPaidFromAccountTxn.PaymentItem.BaseDist.BaseMoneyReceived as DirectBillMoneyRcvd).OriginalUnappliedAmt_Ext
    }
    var affectBalanceAmount = ((chrgPaidFromAccountTxn.PaymentItem.BaseDist.BaseMoneyReceived typeis ZeroDollarDMR ||
        chrgPaidFromAccountTxn.PaymentItem.BaseDist.BaseMoneyReceived typeis ZeroDollarAMR) &&
        !amountPaidFromUnapplied.IsPositive)?false:true

    logInfoDataTransaction(acctObj, plcyPeriod, chrgPaidFromAccountTxn)

    if (plcyPeriod != null && plcyPeriod.AgencyBill) {
      // does not affect the balance
      return
    }

    var paymentMethod = (chrgPaidFromAccountTxn.PaymentItem.BaseDist.BaseMoneyReceived as PaymentMoneyReceived).PaymentInstrument.PaymentMethod
    var refNumber = (chrgPaidFromAccountTxn.PaymentItem.BaseDist.BaseMoneyReceived as PaymentMoneyReceived).RefNumber

    if (chrgPaidFromAccountTxn.PaymentItem.BaseDist.BaseMoneyReceived typeis ZeroDollarDMR ||
        chrgPaidFromAccountTxn.PaymentItem.BaseDist.BaseMoneyReceived typeis ZeroDollarAMR) {
      refNumber = "Credit"
    }

    // Make a change to only exclude invoice offsets and include charge
    // reversals.
    //
    if (chrgPaidFromAccountTxn.PaymentItem.BaseDist.BaseMoneyReceived typeis ZeroDollarReversal
        && !chrgPaidFromAccountTxn.Charge.Reversed) {
      /* these are reversal that are a result of a offset of a billed invoice. These do not change the
       * outstanding balance. Ingore these
       */
      return
    }

    var paidAmount = 0bd.ofCurrency(chrgPaidFromAccountTxn.Currency)
    var paidOutstandingAmount = 0bd.ofCurrency(chrgPaidFromAccountTxn.Currency)

    /* Calculate the paid amount based on what type of payment it is and
               if it is a reversal or not.
    */

    if (!chrgPaidFromAccountTxn.Reversal
        && chrgPaidFromAccountTxn.Amount.IsPositive) {
      paidAmount = chrgPaidFromAccountTxn.Amount.negate()
      if (chrgPaidFromAccountTxn.InvoiceItem.Invoice.BilledOrDue) {
        paidOutstandingAmount = chrgPaidFromAccountTxn.Amount.negate()
      }
    } else if (chrgPaidFromAccountTxn.Reversal
        && chrgPaidFromAccountTxn.Amount.IsNegative) {
      paidAmount = chrgPaidFromAccountTxn.Amount.negate()
      if (chrgPaidFromAccountTxn.InvoiceItem.Invoice.BilledOrDue) {
        paidOutstandingAmount = chrgPaidFromAccountTxn.Amount.negate()
      }
    }

    if (chrgPaidFromAccountTxn.Reversal){
      chrgPaidFromAccountTxn.ChargeInstanceCtx.AccountBalanceTxn_ext = null
      txnDate = DateUtil.currentDate()
    }

    if (plcyPeriod != null) {
      var existingSummaryRecord = acctObj.AccountBalanceTxns_Ext.where(\tran -> tran.New and tran.Subtype == typekey.AccountBalanceTxn_ext.TC_ACCOUNTBALTXNPAYMENT_EXT
          and tran.PolicyPeriod == plcyPeriod and tran.TransactionDate!= null and tran.TransactionDate.compareIgnoreTime(txnDate) == 0
          and (tran as AccountBalTxnPayment_ext).PaymentInstrumentMethod == paymentMethod
          and (tran as AccountBalTxnPayment_ext).CheckRefNumber == refNumber ).first()
      if (existingSummaryRecord == null) {
        var newSummaryRecord = bundle.newBeanInstance(AccountBalTxnPayment_ext) as AccountBalTxnPayment_ext
        newSummaryRecord.Account = acctObj
        newSummaryRecord.PolicyPeriod = plcyPeriod
        newSummaryRecord.TransactionDate = txnDate
        newSummaryRecord.PaymentInstrumentMethod = paymentMethod
        newSummaryRecord.CheckRefNumber = refNumber
        newSummaryRecord.addToTransactions(chargePaidCtx)
        newSummaryRecord.PaidAmount = paidAmount
        newSummaryRecord.DueAmount = paidOutstandingAmount
        newSummaryRecord.ReferenceNumber = refNumber
        newSummaryRecord.BalanceAmount = (affectBalanceAmount)?paidAmount:0bd.ofCurrency(chrgPaidFromAccountTxn.Currency)
        acctObj.addToAccountBalanceTxns_Ext(newSummaryRecord)
      } else if (existingSummaryRecord != null){
        existingSummaryRecord.PaidAmount = existingSummaryRecord.PaidAmount + paidAmount
        existingSummaryRecord.DueAmount = existingSummaryRecord.DueAmount + paidOutstandingAmount
        existingSummaryRecord.BalanceAmount = (affectBalanceAmount)?existingSummaryRecord.BalanceAmount + paidAmount:existingSummaryRecord.BalanceAmount
        existingSummaryRecord.addToTransactions(chargePaidCtx)
      }
    } else {
      var newSummaryRecord = bundle.newBeanInstance(AccountBalTxnPayment_ext) as AccountBalTxnPayment_ext
      newSummaryRecord.Account = acctObj
      newSummaryRecord.TransactionDate = txnDate
      newSummaryRecord.PolicyPeriod = plcyPeriod
      newSummaryRecord.PaymentInstrumentMethod = paymentMethod
      newSummaryRecord.CheckRefNumber = refNumber
      newSummaryRecord.addToTransactions(chargePaidCtx)
      newSummaryRecord.PaidAmount = paidAmount
      newSummaryRecord.DueAmount = paidOutstandingAmount
      newSummaryRecord.ReferenceNumber = refNumber
      newSummaryRecord.BalanceAmount = (affectBalanceAmount)?paidAmount:0bd.ofCurrency(chrgPaidFromAccountTxn.Currency)
      acctObj.addToAccountBalanceTxns_Ext(newSummaryRecord)
    }
  }

  /**
   * Description: This method is used for inserting/updating records into AccountBalanceTxn
   * entity for ChargeWritten off  transactions
   *
   * @param bundle Bundle
   * @param txn ChargePaidFromAccount Transaction
   */
  static function createChargeWriteoffRecords(bundle : Bundle, chrgWriteOffTxn : ChargeWrittenOff)  {
    var chrgWriteOffCtx = chrgWriteOffTxn.ChargeInstanceCtx
    var plcyPeriod = chrgWriteOffTxn.InvoiceItem.PolicyPeriod
    var txnDate = chrgWriteOffTxn.TransactionDate
    var acctObj = chrgWriteOffTxn.Charge.OwnerAccount

    logInfoDataTransaction(acctObj, plcyPeriod, chrgWriteOffTxn)

    if (plcyPeriod != null && plcyPeriod.AgencyBill) {
      // does not affect the balance
      return
    }

    if (chrgWriteOffTxn.Reversal){
      chrgWriteOffTxn.ChargeInstanceCtx.AccountBalanceTxn_ext = null
      txnDate = DateUtil.currentDate()
    }

    if (plcyPeriod != null) {
      var existingSummaryRecord = acctObj.AccountBalanceTxns_Ext.where(\tran -> tran.PolicyPeriod == plcyPeriod
          and tran.TransactionDate.compareIgnoreTime(txnDate) == 0
          and tran typeis AccountBalTxnWriteoff_ext
          and tran.WriteOff == chrgWriteOffTxn.Writeoff
          and tran.New ).first()
      if (existingSummaryRecord == null) {
        var newSummaryRecord = bundle.newBeanInstance(AccountBalTxnWriteoff_ext) as  AccountBalTxnWriteoff_ext
        newSummaryRecord.Account = acctObj
        newSummaryRecord.PolicyPeriod = plcyPeriod
        newSummaryRecord.TransactionDate = txnDate
        newSummaryRecord.addToTransactions(chrgWriteOffCtx)
        newSummaryRecord.WriteOff = chrgWriteOffTxn.Writeoff
        newSummaryRecord.PaidAmount = chrgWriteOffTxn.Amount.negate()
        newSummaryRecord.BalanceAmount = chrgWriteOffTxn.Amount.negate()
        acctObj.addToAccountBalanceTxns_Ext(newSummaryRecord)
      } else if (existingSummaryRecord != null){
        existingSummaryRecord.PaidAmount = existingSummaryRecord.PaidAmount + chrgWriteOffTxn.Amount.negate()
        existingSummaryRecord.BalanceAmount = existingSummaryRecord.BalanceAmount + chrgWriteOffTxn.Amount.negate()
        existingSummaryRecord.addToTransactions(chrgWriteOffCtx)
      }
    } else {
      var newSummaryRecord = bundle.newBeanInstance(AccountBalTxnWriteoff_ext) as AccountBalTxnWriteoff_ext
      newSummaryRecord.Account = acctObj
      newSummaryRecord.TransactionDate = txnDate
      newSummaryRecord.PolicyPeriod = plcyPeriod
      newSummaryRecord.WriteOff = chrgWriteOffTxn.Writeoff
      newSummaryRecord.addToTransactions(chrgWriteOffCtx)
      newSummaryRecord.PaidAmount = chrgWriteOffTxn.Amount.negate()
      newSummaryRecord.BalanceAmount = chrgWriteOffTxn.Amount.negate()
      acctObj.addToAccountBalanceTxns_Ext(newSummaryRecord)
    }
  }

  /**
   * Description: This method is used for inserting/updating records into AccountBalanceTxn
   * entity for Intial Charge transactions
   *
   * @param bundle Bundle
   * @param txn InitialChargeTxn Transaction
   */
  static function createChargeBillingInstructionRecords(bundle : Bundle, initChargeTxn: InitialChargeTxn)  {
    var initChargeTxnCtx = initChargeTxn.ChargeInstanceCtx
    var plcyPeriod = initChargeTxn.Charge.PolicyPeriod
    var txnDate = initChargeTxn.TransactionDate
    var acctObj = initChargeTxn.Charge.OwnerAccount

    logInfoDataTransaction(acctObj, plcyPeriod, initChargeTxn)

    var billingInstruction = initChargeTxn.Charge.BillingInstruction

    if (plcyPeriod != null) {
      var existingSummaryRecord = acctObj.AccountBalanceTxns_Ext.where(\tran -> tran.PolicyPeriod == plcyPeriod
          and tran.TransactionDate.compareIgnoreTime(txnDate) == 0
          and tran typeis AccountBalanceBillInst_ext
          and tran.BillingInstruction == billingInstruction
          and tran.New ).first()
      if (existingSummaryRecord == null) {
        var newSummaryRecord = bundle.newBeanInstance(AccountBalanceBillInst_ext) as AccountBalanceBillInst_ext
        newSummaryRecord.Account = acctObj
        newSummaryRecord.PolicyPeriod = plcyPeriod
        newSummaryRecord.TransactionDate = txnDate
        newSummaryRecord.addToTransactions(initChargeTxnCtx)
        newSummaryRecord.BillingInstruction = billingInstruction
        newSummaryRecord.DueAmount = 0bd.ofCurrency(initChargeTxn.Currency)
        newSummaryRecord.BalanceAmount = initChargeTxn.Charge.BillingInstruction.Charges.sum(\c -> c.Amount)
        acctObj.addToAccountBalanceTxns_Ext(newSummaryRecord)
      } else if (existingSummaryRecord != null){
        existingSummaryRecord.addToTransactions(initChargeTxnCtx)
      }
    } else {
      var newSummaryRecord = bundle.newBeanInstance(AccountBalanceBillInst_ext) as AccountBalanceBillInst_ext
      newSummaryRecord.Account = acctObj
      newSummaryRecord.TransactionDate = txnDate
      newSummaryRecord.PolicyPeriod = plcyPeriod
      newSummaryRecord.BillingInstruction = billingInstruction
      newSummaryRecord.addToTransactions(initChargeTxnCtx)
      newSummaryRecord.DueAmount = 0bd.ofCurrency(initChargeTxn.Currency)
      newSummaryRecord.BalanceAmount = initChargeTxn.Charge.BillingInstruction.Charges.sum(\c -> c.Amount)
      acctObj.addToAccountBalanceTxns_Ext(newSummaryRecord)
    }
  }

  /**
   * Description: This function is used to create a non balance affecting transaction.
   *
   * @param acctObj Account
   * @param polPeriod PolicyPeriod
   * @param description String
   *
   */

  public static function createNonFinancialTransaction (acctObj : Account, polPeriod : PolicyPeriod, tranType : AcctBalNonBalType_Ext,description : String) {
    var newSummaryRecord = new AccountBalTxnNonBal_ext ()
    if (acctObj == null) {
      // can be null for policy level documents only.
      acctObj = polPeriod.Account
    }
    newSummaryRecord.Account = acctObj
    newSummaryRecord.PolicyPeriod = polPeriod
    newSummaryRecord.TransactionDate = gw.api.util.DateUtil.currentDate()
    newSummaryRecord.TransactionType = tranType
    newSummaryRecord.TransactionDescription = description
    newSummaryRecord.BalanceAmount = 0bd.ofCurrency(acctObj.Currency)
    newSummaryRecord.PaidAmount = 0bd.ofCurrency(acctObj.Currency)
    newSummaryRecord.DueAmount = 0bd.ofCurrency(acctObj.Currency)
  }

  /**
   * Description: This method is used log data when the info log level is set.
   *
   * @param acct Account
   * @param polPeriod PolicyPeriod
   * @param trans Transaction
   *
   */
  private static function logInfoDataTransaction (acct : Account, polPeriod : PolicyPeriod, trans : Transaction) {
    var _logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())
    var acctString = (acct == null)?"none":acct.DisplayName
    var polString = (polPeriod == null)?"none":polPeriod.DisplayName
    _logger.info("Processing transaction. Transaction: " + trans.TransactionNumber + " Transaction Type: " + trans.Subtype
        + " Amount: " + trans.Amount)
    _logger.info("Processing Account: " + acctString
        + " Policy Period: " + polString )
  }
}