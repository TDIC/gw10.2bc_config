package gw.acc.acct360.accountview.plugin
/**
 * Account 360 Accelerator
 * This class is used for Account Balance Summary Transactions.
 * It is called late in the cycle after all the transactions are generated.
 * It is used to create the Account 360 records.
 *
 **/
uses gw.pl.persistence.core.Bundle
uses gw.transaction.AbstractBundleTransactionCallback
uses gw.acc.acct360.accountview.AccountBalanceTxnTrackUtil
uses gw.acc.acct360.accountview.AccountBalanceTxnUtil
uses java.lang.Exception
uses java.lang.invoke.MethodHandles

uses org.slf4j.LoggerFactory
uses gw.api.upgrade.Coercions

class TransactionCatcherCallback extends AbstractBundleTransactionCallback {

  public static function getOrCreateCallbackFor(bundle: Bundle): TransactionCatcherCallback {
    for (var callback in bundle.BundleTransactionCallbacks) {
      if (callback typeis TransactionCatcherCallback) {
        return callback
      }
    }
    var newCallback = new TransactionCatcherCallback();
    bundle.addBundleTransactionCallback(newCallback);
    return newCallback;
  }

  /**
   * WARNING: this hooks in *very* late in the bundle commit process. Be very, very careful what kinds of code you
   * put in here. Guidewire has reviewed this code. Refrain from adding additional code.
   */
  override function afterSearchDenormObjects(bundle: Bundle) {
      super.afterSearchDenormObjects(bundle)
      var transactions = bundle.InsertedBeans.where(\ib -> (ib typeis ChargeBilled ||
                                                           ib typeis ChargePaidFromAccount ||
                                                           ib typeis ChargeWrittenOff ||
                                                           ib typeis ChargePaidFromProducer ||
                                                           ib typeis InitialChargeTxn) &&
                                                           ib typeis Transaction &&
                                                           !ib.Reversed)
      if (transactions.Count > 0) {
         processAcct360Transactions(bundle , Coercions.makeArray<entity.Transaction>(transactions))
      }
  }

  private function processAcct360Transactions (bundle: Bundle, transactions : Transaction[])  {
    var trans : Transaction
    try {
      for (txn in transactions) {
        trans = txn
        if (trans typeis ChargeBilled &&
           !trans.InvoiceItem.AgencyBill) {
          AccountBalanceTxnTrackUtil.createChargeBilledRecordsWithBundle(bundle, trans)
        } else if (trans typeis ChargePaidFromAccount) {
          AccountBalanceTxnTrackUtil.createChargePaidFromAccountRecords(bundle, trans)
        } else if (trans typeis ChargeWrittenOff) {
          AccountBalanceTxnTrackUtil.createChargeWriteoffRecords(bundle, trans)
        } else if (trans typeis ChargePaidFromProducer) {
          AccountBalanceTxnTrackUtil.createChargePaidFromAccountRecords(bundle, trans)
        } else if (trans typeis InitialChargeTxn &&
            !trans.Charge.AgencyBill) {
          AccountBalanceTxnTrackUtil.createChargeBillingInstructionRecords(bundle, trans)
        }
      }

    } catch (e : Exception) {
      var t = trans as ChargeInstanceTxn
      var _logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())
      _logger.error("Exception encountered in Account 360 processing. transaction catcher callback")
      AccountBalanceTxnUtil.logExceptionData(t.Charge.OwnerAccount, t.Charge.PolicyPeriod, e)
      throw e
    }
  }
}