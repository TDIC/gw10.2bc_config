package gw.acc.acct360.accountview

uses java.util.Date

uses gw.pl.currency.MonetaryAmount
/**
 * Account 360 Accelerator
 * This class is used to represent a Account Balance Summary detail in the 360 account view screen.
 *
 **/
class Acct360TranDetail {

  private var _transactionDate: Date as TransactionDate
  private var _effectiveDate : Date as EffectiveDate
  private var _policyPeriod: PolicyPeriod as PolicyPeriod
  private var _policyNumber: String as PolicyNumber
  private var _invoiceNumber: String as InvoiceNumber
  private var _invoice: Invoice as Invoice
  private var _description: String as Description
  private var _checkref:String as CheckRef
  private var _txnSubtype:typekey.Transaction as TransactionType
  private var _outstandingBalance: MonetaryAmount as OutstandingBalance
  private var _dueAmount: MonetaryAmount as DueAmount
  private var _paidAmount: MonetaryAmount as PaidAmount
  private var _paymentMethod:PaymentMethod as PaymentMethod
  private var _balanceAmount : MonetaryAmount as BalanceAmount
  private var _netAmount : MonetaryAmount as NetAmount
  private var _payment : PaymentMoneyReceived as Payment
  private var _billInstruction : BillingInstruction as BillInstruction
  private var _nonBalanceTran : boolean as NonBalanceTran
  private var _acctBalanceTran : AccountBalanceTxn_ext as AcctBalanceTran
}