package gw.acc.activityenhancement.subtypes

uses gw.acc.activityenhancement.IApprovalActivity
uses com.guidewire.bc.domain.approval.BCApprovalHandler

class ProducerPayableTransferApprovalActivityApprovalMethods
    implements IApprovalActivity {

  var _activity : ProducerPayableTransferApprovalActivity

  construct(activity : ProducerPayableTransferApprovalActivity) {
    _activity = activity
  }

  override function associateRelatedEntities() {
    // Producer -> Policies -> Transfer Policy
    // Policy -> Commissions -> Transfer Policy
    // no approval activity was fired in the above cases, what's the corresponding use case?
    var producerTransfer = _activity.ProducerPayableTransfer
    _activity.Producer_Ext = producerTransfer.DebitsPayableOf
  }

  override property get ApprovalHandler() : BCApprovalHandler {
    // ProducerPayableTransferApprovalActivity is not supported in BC v8
    // and has no associated ApprovalHandler
    return null
  }
}