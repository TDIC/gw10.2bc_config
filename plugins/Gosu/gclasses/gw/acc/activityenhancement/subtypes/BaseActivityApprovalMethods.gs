package gw.acc.activityenhancement.subtypes

uses gw.acc.activityenhancement.IApprovalActivity
uses org.slf4j.LoggerFactory
uses com.guidewire.bc.domain.approval.BCApprovalHandler

class BaseActivityApprovalMethods implements IApprovalActivity {

  construct(unused : ActivityCreatedByAppr) {
    LoggerFactory.getLogger(BaseActivityApprovalMethods)
        .warn("No implementsInterface defined for " + unused.Subtype)
  }

  override function associateRelatedEntities() {
    // No additional action to take
  }

  override property get ApprovalHandler() : BCApprovalHandler {
    return null
  }
}