package gw.acc.activityenhancement.subtypes

uses com.guidewire.bc.domain.approval.BCApprovalHandler

class CreditReversalApprActivityApprovalMethods
    extends CreditActivityApprovalMethods <CreditReversalApprActivity> {

  construct(activity: CreditReversalApprActivity) {
    super(activity)
  }

  override property get Credit() : Credit {
    return Activity.CreditReversal.Credit
  }

  override property get ApprovalHandler() : BCApprovalHandler {
    return Activity.ApprovalHandler
  }
}