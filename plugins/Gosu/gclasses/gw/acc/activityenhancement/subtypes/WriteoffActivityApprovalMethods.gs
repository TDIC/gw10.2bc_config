package gw.acc.activityenhancement.subtypes

uses gw.acc.activityenhancement.IApprovalActivity

abstract class WriteoffActivityApprovalMethods<T extends ActivityCreatedByAppr>
    implements IApprovalActivity {

  private var _activity : T as Activity

  protected construct(activity : T) {
    _activity = activity
  }

  protected abstract property get Writeoff() : Writeoff

  final override function associateRelatedEntities() {
    var writeoff = this.Writeoff
    if (writeoff typeis ChargeCommissionWriteoff) {
      // Producer -> Actions -> Writeoff Commission
      var chargeCommission = writeoff.ChargeCommission
      _activity.Producer_Ext =
          chargeCommission.PolicyCommission.ProducerCode.Producer
      /*
       * even though the commission could be associated with a specific policy,
       * the activity should not be related with the policy period since there
       * is no policy money involved
      */
    } else if (writeoff typeis ItemCommissionWriteoff) {
      // Agency Bill Commission Mismatch Writeoff
      if (writeoff.TAccountOwner typeis PolicyPeriod) {
        var policyPeriod = writeoff.TAccountOwner
        // assuming that only the primary producer's commission items can be
        // written off
        _activity.Producer_Ext = policyPeriod.ProducerCodeRoleEntries.firstWhere(
            \ p -> p.Role == TC_PRIMARY
        ).Producer
        /*
         * even though the commission could be associated with a specific policy,
         * the activity should not be related with the policy period since there
         * is no policy money involved
        */
      }
    } else if (writeoff typeis ItemGrossWriteoff) {
      // Agency Bill Gross Mismatch Writeoff
      if (writeoff.TAccountOwner typeis PolicyPeriod) {
        var policyPeriod = writeoff.TAccountOwner
        _activity.PolicyPeriod = policyPeriod
        // Account should be associated even though policy is paid by Producer/AB
        _activity.Account = policyPeriod.Account
        // assuming that only the primary producer's items can be written off
        _activity.Producer_Ext = policyPeriod.ProducerCodeRoleEntries.firstWhere(
            \ p -> p.Role == TC_PRIMARY
        ).Producer
      }
    } else if (writeoff typeis ChargeGrossWriteoff) {
      if (writeoff.TAccountOwner typeis Account) {
        // Account -> Actions -> New Transaction -> Writeoff (no policy target selected)
        _activity.Account = writeoff.TAccountOwner
      } else if (writeoff.TAccountOwner typeis PolicyPeriod) {
        // Account -> Actions -> New Transaction -> Writeoff (policy target selected)
        _activity.PolicyPeriod = writeoff.TAccountOwner
        _activity.Account = _activity.PolicyPeriod.Account
      } else if (writeoff.TAccountOwner typeis Producer) {
        // ??? might not be a use case
        _activity.Producer_Ext = writeoff.TAccountOwner
      }
    } else if (writeoff typeis ProdWriteoffContainer) {
      // Producer -> Actions -> Writeoff
      _activity.Producer_Ext = writeoff.Producer
    }
  }
}