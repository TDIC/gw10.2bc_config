package gw.acc.activityenhancement.subtypes

uses gw.acc.activityenhancement.IApprovalActivity

abstract class CreditActivityApprovalMethods<T extends ActivityCreatedByAppr>
    implements IApprovalActivity {

  private var _activity : T as Activity

  protected construct(activity : T) {
    _activity = activity
  }

  protected abstract property get Credit() : Credit

  final override function associateRelatedEntities() {
    // Account -> Actions -> New Transaction -> Credit / Credit Reversal
    var credit = this.Credit
    _activity.Account = credit.Account
  }
}