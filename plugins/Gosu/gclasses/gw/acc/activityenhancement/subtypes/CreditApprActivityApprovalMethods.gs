package gw.acc.activityenhancement.subtypes

uses com.guidewire.bc.domain.approval.BCApprovalHandler

class CreditApprActivityApprovalMethods
    extends CreditActivityApprovalMethods <CreditApprActivity> {

  construct(activity : CreditApprActivity) {
    super(activity)
  }

  override property get Credit() : Credit {
    return Activity.Credit
  }

  override property get ApprovalHandler(): BCApprovalHandler {

    return Activity.ApprovalHandler
  }
}