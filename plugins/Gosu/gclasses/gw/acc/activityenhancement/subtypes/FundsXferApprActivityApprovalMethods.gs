package gw.acc.activityenhancement.subtypes

uses gw.acc.activityenhancement.IApprovalActivity
uses com.guidewire.bc.domain.approval.BCApprovalHandler

class FundsXferApprActivityApprovalMethods
    implements IApprovalActivity {

  var _activity : FundsXferApprActivity

  construct(activity : FundsXferApprActivity) {
    _activity = activity
  }

  override function associateRelatedEntities() {
    var fundsTransfer = _activity.FundsTransfer
    // Account -> Actions -> New Transaction -> Transfer
    // Producer -> Actions -> Transfer Funds

    // this will be either Unapplied or Producer, and the other will be null
    _activity.Account = fundsTransfer.SourceUnapplied.Account
    if (fundsTransfer.SourceUnapplied.Policy != null) {
      var effectivePolicyPeriod =
          fundsTransfer.SourceUnapplied.Policy.PolicyPeriods
              .firstWhere(\period -> period.Status == TC_INFORCE)
      _activity.PolicyPeriod = effectivePolicyPeriod
    }
    _activity.Producer_Ext = fundsTransfer.SourceProducer
  }

  override property get ApprovalHandler() : BCApprovalHandler {
    return _activity.ApprovalHandler
  }
}