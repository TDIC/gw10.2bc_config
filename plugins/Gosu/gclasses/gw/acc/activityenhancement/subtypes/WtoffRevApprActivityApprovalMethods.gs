package gw.acc.activityenhancement.subtypes

uses com.guidewire.bc.domain.approval.BCApprovalHandler

class WtoffRevApprActivityApprovalMethods
    extends WriteoffActivityApprovalMethods <WtoffRevApprActivity> {

  construct(activity : WtoffRevApprActivity) {
    super(activity)
  }

  override property get Writeoff() : Writeoff {
    return Activity.WriteoffReversal.Writeoff
  }

  override property get ApprovalHandler() : BCApprovalHandler {
    return Activity.ApprovalHandler
  }
}