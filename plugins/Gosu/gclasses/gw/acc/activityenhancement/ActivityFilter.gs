package gw.acc.activityenhancement

uses com.guidewire.pl.system.database.Query
uses com.guidewire.pl.system.filters.QueryBasedQueryFilter
uses gw.entity.IQueryColumn
uses com.guidewire.commons.util.Relop
uses java.util.Date

class ActivityFilter implements QueryBasedQueryFilter {

  var _status:ActivityStatus
  var _priority:Priority
  var _escalated:Boolean
  var _openedSince:Date
  var _closedSince:Date
  var _name:String
  
  var _statusColumn = Activity.Type.TypeInfo.getProperty( "Status" ) as IQueryColumn
  var _priorityColumn = Activity.Type.TypeInfo.getProperty( "Priority" ) as IQueryColumn
  var _escalatedColumn = Activity.Type.TypeInfo.getProperty( "Escalated" ) as IQueryColumn
  var _createDateColumn = Activity.Type.TypeInfo.getProperty( "CreateTime" ) as IQueryColumn
  var _closeDataColumn = Activity.Type.TypeInfo.getProperty( "CloseDate" ) as IQueryColumn

  construct(name:String,status:ActivityStatus, priority:Priority, escalated:Boolean, openedSince:Date, closedSince:Date) {
    _name = name
    _status = status
    _priority = priority
    _escalated = escalated
    _openedSince = openedSince
    _closedSince = closedSince
  }
  
  override function applyFilter(obj:Object):boolean {
    if (obj typeis Activity) {
      var result = true
      if (_status != null) result = result and _status.equals(obj.Status)
      if (_priority != null) result = result and _priority.equals(obj.Priority)
      if (_escalated != null) result = result and _escalated.equals(obj.Escalated)
      if (_openedSince != null) result = result and _openedSince.beforeOrEqual(obj.CreateTime)
      if (_closedSince != null) {
        if (obj.CloseDate != null) {
          result = result and _closedSince.beforeOrEqual(obj.CloseDate)
        } else result = false // no close date means this activity is not closed yet so no match
      }
      return result
    } else return false
  }
  
  override function filterQuery(query:Query):Query {    
    if (_status != null) query.PrimaryTableObject.Restriction.compareEquals(_statusColumn, _status, true)
    if (_priority != null) query.PrimaryTableObject.Restriction.compareEquals(_priorityColumn, _priority, true)
    if (_escalated != null) query.PrimaryTableObject.Restriction.compareEquals(_escalatedColumn, _escalated, true)
    if (_openedSince != null) query.PrimaryTableObject.Restriction.compare(_createDateColumn, Relop.GE_RELOP, _openedSince, true)
    if (_closedSince != null) query.PrimaryTableObject.Restriction.compare(_closeDataColumn, Relop.GE_RELOP, _closedSince, true)
    return query
  }
  
  override function toString():String {
    return _name
  }

}