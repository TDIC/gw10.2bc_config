package gw.webservice.policycenter

uses gw.api.locale.DisplayKey

@Export
enhancement InvoiceStreamPCEnhancement : entity.InvoiceStream {
  
  property get PCDisplayName() : String {
    if (this.PaymentInstrument.PaymentMethod == TC_RESPONSIVE) {
      return this.Periodicity.DisplayName + ", " + DisplayKey.get("PaymentInstrument.API.Responsive.PCDisplayName")
    } else {
      return this.Periodicity.DisplayName + ", " + this.PaymentInstrument.DisplayName
    }
  }
}
