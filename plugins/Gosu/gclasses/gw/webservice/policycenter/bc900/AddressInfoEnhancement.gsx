package gw.webservice.policycenter.bc900

uses gw.webservice.policycenter.bc900.entity.types.complex.AddressInfo

@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
enhancement AddressInfoEnhancement : AddressInfo {
  function toAddress() : Address {
    var address = new Address()
    address.AddressBookUID = this.AddressBookUID
    address.AddressLine1 = this.AddressLine1
    address.AddressLine1Kanji = this.AddressLine1Kanji
    address.AddressLine2 = this.AddressLine2
    address.AddressLine2Kanji = this.AddressLine2Kanji
    address.City = this.City
    address.CityKanji = this.CityKanji
    address.State = typekey.State.get(this.State)
    address.PostalCode = this.PostalCode
    address.Country = typekey.Country.get(this.Country)
    address.CEDEX= this.CEDEX
    address.CEDEXBureau = this.CEDEXBureau
    return address
  }
}
