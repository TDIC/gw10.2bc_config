package gw.webservice.policycenter.bc900

uses gw.webservice.bc.bc900.PaymentInstrumentRecord
uses gw.webservice.bc.bc900.PaymentInstruments
uses gw.xml.ws.annotation.WsiExportable

@WsiExportable( "http://guidewire.com/bc/ws/gw/webservice/policycenter/bc900/AccountBillingSettings" )
@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
final class AccountBillingSettings {
  var _InvoiceDeliveryMethod : String as InvoiceDeliveryMethod
  var _PaymentInstrumentRecord : PaymentInstrumentRecord as PaymentInstrumentRecord
  
  construct()
  {
  }
  
  construct(account : Account) {
    this.InvoiceDeliveryMethod = account.InvoiceDeliveryType.Code
    this.PaymentInstrumentRecord = PaymentInstruments.toRecord(account.DefaultPaymentInstrument)
  }
  
}
