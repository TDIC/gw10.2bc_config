package gw.webservice.policycenter.bc900
uses gw.xml.ws.annotation.WsiExportable

@WsiExportable( "http://guidewire.com/bc/ws/gw/webservice/policycenter/bc900/BCAccountSearchResult" )
@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
final class BCAccountSearchResult {
  public var AccountNumber : String
  public var AccountName : String
  public var AccountNameKanji : String
  public var PrimaryPayer : String
  public var Currency : Currency
  public var isListBill : boolean

  construct() {
  }
  
  construct(account : Account) {
    this.AccountName = account.AccountName
    this.AccountNameKanji = account.AccountNameKanji
    this.AccountNumber = account.AccountNumber
    this.PrimaryPayer = account.PrimaryPayer.DisplayName
    this.Currency = account.Currency
    this.isListBill = account.isListBill()
  }
}
