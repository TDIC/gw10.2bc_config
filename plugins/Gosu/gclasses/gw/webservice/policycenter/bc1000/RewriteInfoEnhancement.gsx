package gw.webservice.policycenter.bc1000

uses gw.api.locale.DisplayKey
uses gw.api.web.policy.NewPolicyUtil
uses gw.api.webservice.exception.BadIdentifierException
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCNewAccountInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.RewriteInfo

@Export
enhancement RewriteInfoEnhancement : RewriteInfo {
  function executeRewriteBI() : BillingInstruction {
    return executedRewriteBISupplier()
  }

  /**
   * WARNING: If the owner/payer accounts don't exist in BC yet, this method will attempt to create
   * invoice previews based off tmp accounts with the system's default settings. This might result
   * in invoices that are slightly different than the ones produced once the policy is actually
   * issued towards the real accounts.
   */
  function toRewriteForPreview() : Rewrite {
    return toRewriteForPreview(null, null)
  }

  function toRewriteForPreview(ownerAccountInfo : PCNewAccountInfo, overridingPayerInfo : PCNewAccountInfo) : Rewrite {
    //Must create accounts prior to initialization code, if they don't exist yet
    this.setupAccountForPreview(this.AccountNumber, ownerAccountInfo)
    this.setupAccountForPreview(this.AltBillingAccountNumber, overridingPayerInfo)

    return executedRewriteBISupplier()
  }

  private function createRewriteBI() : Rewrite {
    final var priorPolicyPeriod = this.findPriorPolicyPeriod()
    if (priorPolicyPeriod == null) {
      throw new BadIdentifierException(
          DisplayKey.get("Webservice.Error.CannotFindMatchingPolicyPeriod",
              this.PriorPolicyNumber?:this.PolicyNumber,
              this.PriorTermNumber?:(this.TermNumber - 1)))
    }
    final var bi = (priorPolicyPeriod.Currency != this.CurrencyValue)
        ? NewPolicyUtil.createCurrencyChangeRewrite(this.findOwnerAccount(), priorPolicyPeriod)
        : NewPolicyUtil.createRewrite(priorPolicyPeriod)
    this.initPolicyPeriodBIInternal(bi) // must precede populate when prior period exists...
    this.populateIssuanceInfo(bi.NewPolicyPeriod)
    return bi
  }

  function executedRewriteBISupplier(): Rewrite {
    var bi = createRewriteBI()
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }
}