package gw.webservice.policycenter.bc1000

uses gw.api.database.Query
uses gw.api.util.LocaleUtil
uses gw.pl.persistence.core.Bundle
uses gw.webservice.policycenter.bc1000.entity.types.complex.BreakdownCriteriaInfo

@Export
enhancement BreakdownCriteriaInfoEnhancement : BreakdownCriteriaInfo {

  /**
   * A ChargeBreakdownCategoryType will be created if none can be found using the 'Code'. If a ChargeBreakdownCategoryType is found,
   * then the names of that category type will be updated using the localized names from the BreakdownCriteriaInfo.
   * @param bundle the {@link Bundle} that is used to update the category type
   */
  function toChargeBreakdownCategoryType(bundle : Bundle) : ChargeBreakdownCategoryType {
    var categoryType = Query.make(ChargeBreakdownCategoryType).compare(ChargeBreakdownCategoryType#Code, Equals, this.Code).select().AtMostOneRow

    if (categoryType == null) {
      categoryType = new ChargeBreakdownCategoryType()
      categoryType.Code = this.Code
    }

    bundle.add(categoryType)
    categoryType.Name = this.Name

    this.TranslatedNames.each(\translatedName -> {
      var language = LocaleUtil.toLanguage(LanguageType.get(translatedName.LanguageCode))
      if (language != null) {
        LocaleUtil.runAsCurrentLanguage(language, \-> categoryType.setName(translatedName.Translation))
      }
    })

    return categoryType
  }
}