package gw.webservice.policycenter.bc1000

uses gw.webservice.policycenter.bc1000.entity.types.complex.ContactAddressInfo

@Export
enhancement ContactAddressInfoEnhancement : ContactAddressInfo {
  function toContactAddress() : ContactAddress {
    var contactAddress = new ContactAddress()
    contactAddress.AddressBookUID = this.AddressBookUID
    contactAddress.Address = this.Address.$TypeInstance.toAddress()
    return contactAddress
  }
}