package gw.webservice.policycenter.bc1000

uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.BreakdownItemInfo_Categories
uses gw.webservice.policycenter.bc1000.entity.types.complex.BreakdownCategoryInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.BreakdownItemInfo

@Export
enhancement BreakdownItemInfoEnhancement : BreakdownItemInfo {

  function addCategoryInfo(breakdownCategoryInfo : BreakdownCategoryInfo) {
    var categoryInfo = new BreakdownItemInfo_Categories()
    categoryInfo.$TypeInstance = breakdownCategoryInfo
    this.Categories.add(categoryInfo)
  }

}
