package gw.webservice.bc.bc1000

uses gw.pl.currency.MonetaryAmount
uses gw.xml.ws.annotation.WsiExportable

@Export
@WsiExportable("http://guidewire.com/bc/ws/gw/webservice/bc/bc1000/BatchPaymentEntryDTO")
final class BatchPaymentEntryDTO {

  private var _publicID: String as PublicID
  private var _paymentType: MultiPaymentType as PaymentType
  private var _paymentInstrument: PaymentInstrumentRecord as PaymentInstrument
  private var _refNumber: String as RefNumber
  private var _amount: MonetaryAmount as Amount
  private var _paymentDate: Date as PaymentDate
  private var _accountNumber: String as AccountNumber
  private var _policyNumber: String as PolicyNumber
  private var _accountID: String as AccountID
  private var _policyID: String as PolicyID
  private var _invoiceID: String as InvoiceID
  private var _producerID: String as ProducerID
  private var _description: String as Description

}
