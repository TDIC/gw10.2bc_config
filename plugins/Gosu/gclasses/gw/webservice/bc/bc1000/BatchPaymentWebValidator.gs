package gw.webservice.bc.bc1000

uses gw.api.webservice.exception.DataConversionException
uses gw.payment.batch.BatchPaymentEntryValidator
uses gw.payment.batch.BatchPaymentValidator
uses gw.payment.batch.ValidationOutcome

/**
 * Validator specific for batchPayment validation that occurs in SOAP API
 * It uses DataConversionException to send the negative response when validation is not positive
 */
@Export
class BatchPaymentWebValidator {

  private static final var _instance = new BatchPaymentWebValidator()

  private construct() {
  }

  public static property get Instance(): BatchPaymentWebValidator {
    return _instance
  }

  private static final var batchPaymentValidator = new BatchPaymentValidator()
  private static final var batchPaymentEntryValidator = new BatchPaymentEntryValidator()

  /**
   * Method used to validate specified batchPayment and its subsequent payment entries
   *
   * @param batchPayment
   * @throws gw.api.webservice.exception.DataConversionException - if specified batchPayment or
   *                                                             any of the subsequent payment entry is not valid entity
   */
  @Throws(DataConversionException, "If specified batchPayment or any of the subsequent payment entry is not valid entity")
  public function validate(batchPayment: BatchPayment) {

    processValidationOutcome(batchPaymentValidator.validate(batchPayment))

    batchPayment.Payments.each(
        \entry -> processValidationOutcome(batchPaymentEntryValidator.validate(entry))
    )
  }

  private function processValidationOutcome(validationOutcome: ValidationOutcome) {
    if (validationOutcome.isFailed()) {
      throw new DataConversionException(validationOutcome.getValidationErrorMessageIfFail().get())
    }
  }
}