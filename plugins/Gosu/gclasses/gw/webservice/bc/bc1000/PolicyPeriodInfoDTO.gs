package gw.webservice.bc.bc1000

uses gw.bc.archive.ImpactedByArchiving
uses gw.pl.currency.MonetaryAmount
uses gw.xml.ws.annotation.WsiExportable

/**
 * Data Transfer Object ("DTO") to represent a summary of data from a
 * {@link PolicyPeriod PolicyPeriod} for use by a WS-I webservice.
 * <p>
 * The specific mappings for a {@code PolicyPeriodInfoDTO} are as follows:
 * <table border="1">
 *   <tr><td><b>Field</b></td><td><b>Maps to</b></td></tr>
 *   <tr><td>AccountPublicID</td><td>PolicyPeriod.Account.PublicID</td></tr>
 *   <tr><td>AmountBilled</td><td>{@link PolicyPeriod#getBilledAmount
 *       PolicyPeriod.BilledAmount}</td></tr>
 *   <tr><td>AmountDue</td><td>{@link PolicyPeriod#getDelinquentAmount()}
 *       PolicyPeriod.DueAmount}</td></tr>
 *   <tr><td>AmountInUnappliedFund</td><td>The balance in the designated
 *       unapplied fund of the policy-level billed
 *       {@code PolicyPeriod}</td></tr>
 *   <tr><td>AmountPaid</td><td>{@link PolicyPeriod#getPaidAmount
 *        PolicyPeriod.PaidAmount}</td></tr>
 *   <tr><td>AmountUnbilled</td><td>{@link PolicyPeriod#getUnbilledAmount
 *       PolicyPeriod.UnbilledAmount}</td></tr>
 *   <tr><td>AmountWrittenOff</td><td>Amount written off for the
 *       {@code PolicyPeriod}</td></tr>
 *   <tr><td>Delinquent</td><td>{@link PolicyPeriod#isDelinquent
 *       PolicyPeriod.Delinquent}</td></tr>
 *   <tr><td>EffectiveDate</td><td>{@link PolicyPeriod#getEffectiveDate
 *       PolicyPeriod.EffectiveDate}</td></tr>
 *   <tr><td>ExpirationDate</td><td>{@link PolicyPeriod#getExpirationDate
 *       PolicyPeriod.ExpirationDate}</td></tr>
 *   <tr><td>InfoDate</td><td>Date and Time this information was generated</td></tr>
 *   <tr><td>{@link PolicyPeriodInfoDTO#getLastInvoiceAmount LastInvoiceAmount}</td>
 *     <td>Amount on the last billed/due invoice</td></tr>
 *   <tr><td>{@link PolicyPeriodInfoDTO#getLastInvoiceDueDate LastInvoiceDueDate}</td>
 *      <td>Due date of the last billed/due invoice</td></tr>
 *   <tr><td>{@link PolicyPeriodInfoDTO#getLastPaymentAmount LastPaymentAmount}</td>
 *       <td>Amount of the last payment applied to the {@code PolicyPeriod}</td></tr>
 *   <tr><td>{@link PolicyPeriodInfoDTO#getLastPaymentReceivedDate
 *       LastPaymentReceivedDate}</td><td>Date the last payment applied to the
 *       {@code PolicyPeriod} was received</td></tr>
 *   <tr><td>NextInvoiceAmount</td><td>Amount on the next planned invoice</td></tr>
 *   <tr><td>NextInvoiceDueDate</td><td>Due date of the next planned invoice</td></tr>
 *   <tr><td>PrimaryInsuredName</td><td>Name of the primary insured on the
 *       {@code PolicyPeriod}</td></tr>
 *   <tr><td>PublicID</td><td>PublicID of the {@code PolicyPeriod}</td></tr>
 *   <tr><td>Archived</td><td>Whether the {@code PolicyPeriod} is archived</td></tr>
 *   <tr><td>Retrieved</td><td>Whether the {@code PolicyPeriod} was archived and
 *       has now been retrieved from archive</td></tr>
 * </table></p><p>
 * Customer configuration: modify this file by adding a property that should be displayed in the summary.
 */
@Export
@WsiExportable("http://guidewire.com/bc/ws/gw/webservice/bc/bc1000/PolicyPeriodInfoDTO")
final class PolicyPeriodInfoDTO {
  var _accountPublicID          : String              as AccountPublicID
  var _amountBilled             : MonetaryAmount      as AmountBilled
  var _amountDue                : MonetaryAmount      as AmountDue
  var _amountInUnappliedFund    : MonetaryAmount      as AmountInUnappliedFund
  var _amountPaid               : MonetaryAmount      as AmountPaid
  var _amountUnbilled           : MonetaryAmount      as AmountUnbilled
  /**
   * The amount written off for the {@link PolicyPeriod PolicyPeriod}.
   */
  var _amountWrittenOff         : MonetaryAmount      as AmountWrittenOff
  var _delinquent               : boolean             as Delinquent
  var _effectiveDate            : Date                as EffectiveDate
  var _expirationDate           : Date                as ExpirationDate
  var _infoDate                 : Date                as InfoDate
  var _lastInvoiceAmount        : MonetaryAmount      as LastInvoiceAmount
  var _lastInvoiceDueDate       : Date                as LastInvoiceDueDate
  var _lastPaymentAmount        : MonetaryAmount      as LastPaymentAmount
  var _lastPaymentReceivedDate  : Date                as LastPaymentReceivedDate
  var _nextInvoiceDueDate       : Date                as NextInvoiceDueDate
  var _nextInvoiceAmount        : MonetaryAmount      as NextInvoiceAmount
  var _primaryInsuredName       : String              as PrimaryInsuredName
  var _publicID                 : String              as PublicID

  /**
   * Whether the {@link PolicyPeriod PolicyPeriod} is archived.
   */
  var _archived                 : boolean             as Archived

  /**
   * Whether the {@link PolicyPeriod PolicyPeriod} was archived and
   * is now retrieved from archive.
   */
  var _retrieved                : boolean             as Retrieved

  /**
   * The total amount of the last completed {@link Invoice Invoice} for
   * the {@link PolicyPeriod PolicyPeriod}.
   */
  @ImpactedByArchiving
  property get LastInvoiceAmount() : MonetaryAmount {
    return _lastInvoiceAmount
  }

  /**
   * The due date of the last completed {@link Invoice Invoice} for
   * the {@link PolicyPeriod PolicyPeriod}.
   */
  @ImpactedByArchiving
  property get LastInvoiceDueDate() : Date {
    return _lastInvoiceDueDate
  }

  /**
   * The amount of the last {@link BaseMoneyReceived MoneyReceived} for
   * the {@link PolicyPeriod PolicyPeriod}.
   */
  @ImpactedByArchiving
  property get LastPaymentAmount() : MonetaryAmount {
    return _lastPaymentAmount
  }

  /**
   * The date on which the last {@link BaseMoneyReceived MoneyReceived}
   * for the {@link PolicyPeriod PolicyPeriod} was received.
   */
  @ImpactedByArchiving  property get LastPaymentReceivedDate() : Date {
    return _lastPaymentReceivedDate
  }

  /**
   * Creates a new PolicyPeriodInfoDTO that represents the current snapshot state of the supplied PolicyPeriod.
   * @param that The PolicyPeriod to be represented.
   */
  static function valueOf(that : PolicyPeriod) : PolicyPeriodInfoDTO {
    return new PolicyPeriodInfoDTO().readFrom(that)
  }

  /**
   * Set the fields in this DTO using the supplied PolicyPeriod
   * @param that The PolicyPeriod to copy from.
   */
  final function readFrom(that : PolicyPeriod) : PolicyPeriodInfoDTO {
    var lastInvoice         = that.CompletedInvoicesSortedByEventDate.last()
    var nextPlannedInvoice  = that.NextPlannedInvoice
    var lastMoneyReceived   = that.LastPayment.BaseDist.BaseMoneyReceived

    AccountPublicID         = that.Account.PublicID
    AmountBilled            = that.BilledAmount
    /*
     * DueAmount and UnbilledAmount should be zero by the time the PolicyPeriod
     * is archived.
     */
    AmountDue               = that.DueAmount
    AmountInUnappliedFund   = that.Policy.getDesignatedUnappliedFund(that.Account).Balance
    AmountPaid              = that.PaidAmount
    if (that.Archived) {
      AmountPaid = that.PolicyPeriodArchiveSummary.Paid
    }
    AmountUnbilled          = that.UnbilledAmount
    AmountWrittenOff        = that.PaidOrWrittenOffAmount - that.PaidAmount
    if (that.Archived) {
      AmountWrittenOff = that.PolicyPeriodArchiveSummary.WrittenOff
    }
    Delinquent              = that.Delinquent
    EffectiveDate           = that.EffectiveDate
    ExpirationDate          = that.ExpirationDate
    InfoDate                = Date.Now
    /*
     * The lastInvoice is null for an archived PolicyPeriod because
     * they cannot be associated when the latter is archived.
     */
    LastInvoiceDueDate      = lastInvoice.PaymentDueDate
    LastInvoiceAmount       = lastInvoice.Amount
    LastPaymentAmount       = lastMoneyReceived.Amount
    LastPaymentReceivedDate = lastMoneyReceived.ReceivedDate
    /*
     * There should be no nextPlannedInvoice for an archived (or closed)
     * PolicyPeriod so it will be null.
     */
    NextInvoiceDueDate      = nextPlannedInvoice.PaymentDueDate
    NextInvoiceAmount       = nextPlannedInvoice.Amount
    PrimaryInsuredName      = that.PrimaryInsured.Contact.DisplayName
    PublicID                = that.PublicID
    Archived = that.Archived
    Retrieved = that.Retrieved
    return this
  }

}