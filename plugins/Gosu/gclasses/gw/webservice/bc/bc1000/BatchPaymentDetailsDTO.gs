package gw.webservice.bc.bc1000

uses gw.pl.currency.MonetaryAmount
uses gw.xml.ws.annotation.WsiExportable

@Export
@WsiExportable("http://guidewire.com/bc/ws/gw/webservice/bc/bc1000/BatchPaymentDetailsDTO")
final class BatchPaymentDetailsDTO {

   private var _publicId: String as PublicID
   private var _batchNumber: String as BatchNumber
   private var _batchStatus: BatchPaymentsStatus as BatchStatus
   private var _amount: MonetaryAmount as Amount
   private var _remainingAmount: MonetaryAmount as RemainingAmount
   private var _createdDate: Date as CreationDate
   private var _createdBy: String as CreatedBy
   private var _lastEditedBy: String as LastEditedBy
   private var _lastEditDate: Date as LastEditDate
   private var _postedDate: Date as PostedDate
   private var _postedBy: String as PostedBy
   private var _reversalDate: Date as ReversalDate
   private var _reversedBy: String as ReversedBy

   private var _payments: BatchPaymentEntryDTO[] as Payments
}