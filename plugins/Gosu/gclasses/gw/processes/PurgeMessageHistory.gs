package gw.processes

uses gw.api.admin.MessagingUtil
uses gw.api.system.BCConfigParameters
uses gw.api.upgrade.Coercions
uses gw.api.util.DateUtil

/**
 * Purges the MessageHistory by deleting the completed messages where the sendtime is before given date.
 */
@Export
class PurgeMessageHistory extends BatchProcessBase {

  var _ageInDays : int

  construct() {
    this({BCConfigParameters.KeepCompletedMessagesForDays.Value})
  }

  construct(arguments : Object[]) {
    super(BatchProcessType.TC_PURGEMESSAGEHISTORY)
    if (arguments.length == 1 and arguments[0] typeis Integer) {
      _ageInDays = Coercions.makeIntFrom(arguments[0])
    } else {
      _ageInDays = BCConfigParameters.KeepCompletedMessagesForDays.Value
    }
  }

  override function doWork() {
    var before = DateUtil.currentDate().addDays(-_ageInDays)
    MessagingUtil.removeMessageHistory(before)
  }

}