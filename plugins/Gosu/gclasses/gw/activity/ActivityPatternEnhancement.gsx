package gw.activity

uses gw.api.database.Query
uses java.lang.Exception

@Export
enhancement ActivityPatternEnhancement : ActivityPattern {
  static property get Notification() : ActivityPattern {
    return findActivityPattern("notification")
  }

  static property get Approval() : ActivityPattern {
    return findActivityPattern("approval")
  }

// HermiaK US86: Create Activity. add APWActivity, Reinstatement, CreditCardInquiries,
// StopPaymentDisbursementReissue, FundsTransfer, IncomingCheckInquiries.

  static property get FundsTransfer () : ActivityPattern {
    return findActivityPattern("fundstransfer")
  }

  // SujitN - disabling the Pending Cancellation, Name (PNI), Offered Payment Plan Activitypattern
  /*static property get PendingCancellation () : ActivityPattern {
    return findActivityPattern("pendingcancellation")
  }

  static property get NameActivity() : ActivityPattern {
    return findActivityPattern("nameactivity")
  }

  static public property get OfferedPaymentPlanMismatchActivity_TDIC() : ActivityPattern {
    return findActivityPattern("OfferedPaymentPlanMismatch_TDIC");
  }*/

  private static function findActivityPattern(code : String) : ActivityPattern {
    var activityPattern = Query.make(ActivityPattern).compare("Code", Equals, code).select().AtMostOneRow;
    if (activityPattern == null) {
      throw new Exception ("Failed to find activity pattern: " + code);
    }
    return activityPattern;
  }
}
