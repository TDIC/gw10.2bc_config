package gw.payment

uses gw.api.locale.DisplayKey
uses gw.api.util.BCBigDecimalUtil

@Export
class DBPaymentHelper {
  private var _warningMessage : String

  construct(){
    _warningMessage = ""
  }

  function processWarningMessage() : String {
    return _warningMessage
  }

  function isNegative(moneyReceived : DirectBillMoneyRcvd) : Boolean{
    if(moneyReceived == null){
      return false
    }
    var unditribuedAmount = moneyReceived.Amount - moneyReceived.NetAmountDistributed - moneyReceived.NetSuspenseAmount
    var account = moneyReceived.Account
    if(BCBigDecimalUtil.lessThan(moneyReceived.getUnappliedFund().getBalance(), unditribuedAmount)) {
      _warningMessage = DisplayKey.get("Web.PaymentReversalConfirmation.NegativeBalanceWarning")
      return true
    }
    return false
  }
}