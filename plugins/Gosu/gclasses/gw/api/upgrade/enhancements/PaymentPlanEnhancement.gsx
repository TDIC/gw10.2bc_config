package gw.api.upgrade.enhancements

uses gw.api.util.CurrencyUtil
uses gw.pl.currency.MonetaryAmount

/**
 * File created by Guidewire Upgrade Tools.
 */
enhancement PaymentPlanEnhancement : PaymentPlan {

  @DeprecatedAndRestoredByUpgrade
  property get InstallmentFee() : MonetaryAmount {
    return this.getInstallmentFee(CurrencyUtil.DefaultCurrency)
  }

  @DeprecatedAndRestoredByUpgrade
  property set InstallmentFee(amount : MonetaryAmount) : void {
    this.setInstallmentFeeForCurrency(CurrencyUtil.DefaultCurrency, amount)
  }
}
