package gw.api.upgrade.enhancements

uses gw.api.databuilder.AgencyBillPlanBuilder
uses gw.api.util.CurrencyUtil
uses gw.pl.currency.MonetaryAmount

/**
 * File created by Guidewire Upgrade Tools.
 */
enhancement AgencyBillPlanBuilderEnhancement : AgencyBillPlanBuilder {

  @DeprecatedAndRestoredByUpgrade
  function withNetThresholdForSuppressingStatement(amount : MonetaryAmount) : AgencyBillPlanBuilder {
    this.withNetThresholdForSuppressingStatementForCurrency(CurrencyUtil.DefaultCurrency, amount)
    return this;
  }

  @DeprecatedAndRestoredByUpgrade
  function withClearCommissionThreshold(amount : MonetaryAmount) : AgencyBillPlanBuilder {
    this.withClearCommissionThresholdForCurrency(CurrencyUtil.DefaultCurrency, amount)
    return this;
  }

  @DeprecatedAndRestoredByUpgrade
  function withClearGrossThreshold(amount : MonetaryAmount) : AgencyBillPlanBuilder {
    this.withClearGrossThresholdForCurrency(CurrencyUtil.DefaultCurrency, amount)
    return this;
  }

  @DeprecatedAndRestoredByUpgrade
  function withProducerWriteoffThreshold(amount : MonetaryAmount) : AgencyBillPlanBuilder {
    this.withProducerWriteoffThresholdForCurrency(CurrencyUtil.DefaultCurrency, amount)
    return this;
  }

  @DeprecatedAndRestoredByUpgrade
  function withCurrency(amount : MonetaryAmount) : AgencyBillPlanBuilder {
      this.withSingleCurrency(amount.Currency)
      return this;
    }
}
