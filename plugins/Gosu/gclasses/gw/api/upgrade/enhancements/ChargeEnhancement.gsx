package gw.api.upgrade.enhancements

enhancement ChargeEnhancement: entity.Charge {

  @DeprecatedAndRestoredByUpgrade
      property get SingleInvoiceItem(): InvoiceItem {
    if (this.InvoiceItems.length != 1) {
      throw new IllegalStateException("This charge has more than one invoice item");
    }
    return this.InvoiceItems[0]
  }

}
