package gw.api.upgrade.enhancements

uses gw.api.databuilder.PaymentPlanBuilder
uses gw.api.util.CurrencyUtil
uses gw.pl.currency.MonetaryAmount

/**
 * File created by Guidewire Upgrade Tools.
 */
enhancement PaymentPlanBuilderEnhancement : PaymentPlanBuilder {

  @DeprecatedAndRestoredByUpgrade
  function withInstallmentFee(amount : MonetaryAmount) : PaymentPlanBuilder {
    this.withInstallmentFeeForCurrency(CurrencyUtil.DefaultCurrency, amount)
    return this;
  }

  @DeprecatedAndRestoredByUpgrade
  function withCurrency(amount : MonetaryAmount) : PaymentPlanBuilder {
    this.withSingleCurrency(amount.Currency)
    return this;
  }

}
