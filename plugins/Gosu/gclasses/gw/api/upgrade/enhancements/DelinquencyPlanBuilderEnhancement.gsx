package gw.api.upgrade.enhancements

uses gw.api.databuilder.DelinquencyPlanBuilder
uses gw.api.util.CurrencyUtil
uses gw.pl.currency.MonetaryAmount

/**
 * File created by Guidewire Upgrade Tools.
 */
enhancement DelinquencyPlanBuilderEnhancement : DelinquencyPlanBuilder {

  @DeprecatedAndRestoredByUpgrade
  function withCancellationThreshold(amount : MonetaryAmount) : DelinquencyPlanBuilder {
    this.withCancellationThresholdForCurrency(CurrencyUtil.DefaultCurrency, amount)
    return this;
  }

  @DeprecatedAndRestoredByUpgrade
  function withExitDelinquencyThreshold(amount : MonetaryAmount) : DelinquencyPlanBuilder {
    this.withExitDelinquencyThresholdForCurrency(CurrencyUtil.DefaultCurrency, amount)
    return this;
  }

  @DeprecatedAndRestoredByUpgrade
  function withLateFeeAmount(amount : MonetaryAmount) : DelinquencyPlanBuilder {
    this.withLateFeeAmountForCurrency(CurrencyUtil.DefaultCurrency, amount)
    return this;
  }

  @DeprecatedAndRestoredByUpgrade
  function withPolEnterDelinquencyThreshold(amount : MonetaryAmount) : DelinquencyPlanBuilder {
    this.withPolicyEnterDelinquencyThresholdForCurrency(CurrencyUtil.DefaultCurrency, amount)
    return this;
  }

  @DeprecatedAndRestoredByUpgrade
  function withReinstatementFeeAmount(amount : MonetaryAmount) : DelinquencyPlanBuilder {
    this.withReinstatementFeeAmountForCurrency(CurrencyUtil.DefaultCurrency, amount)
    return this;
  }

  @DeprecatedAndRestoredByUpgrade
  function withWriteoffThreshold(amount : MonetaryAmount) : DelinquencyPlanBuilder {
    this.withWriteoffThresholdForCurrency(CurrencyUtil.DefaultCurrency, amount)
    return this;
  }

  @DeprecatedAndRestoredByUpgrade
  function withCurrency(amount : MonetaryAmount) : DelinquencyPlanBuilder {
    this.withSingleCurrency(amount.Currency)
    return this;
  }
}
