package gw.api.databuilder.webservice.policycenter.bc1000

uses gw.api.databuilder.IssuanceBuilder
uses gw.webservice.policycenter.bc1000.entity.types.complex.RenewalInfo

@Export
abstract class RenewalInfoBuilderBase<T extends RenewalInfo, B extends RenewalInfoBuilderBase<T, B>> extends IssuePolicyInfoBuilderBase<T, B> {

  function withPriorPolicyPeriod(policyPeriod : PolicyPeriod) : B {
    _policyInfo.PriorPolicyNumber = policyPeriod.PolicyNumber
    _policyInfo.PriorTermNumber = policyPeriod.TermNumber
    return this as B
  }

  function withPriorPolicyNumber(policyNumber : String) : B {
    _policyInfo.PriorPolicyNumber = policyNumber
    return this as B
  }

  function withPriorPolicyTerm(termNumber : int) : B {
    _policyInfo.PriorTermNumber = termNumber
    return this as B
  }

  @Override
  function create() : T {
    if(_policyInfo.AccountNumber == null and _policyInfo.PriorPolicyNumber == null and _policyInfo.PriorTermNumber == null){

      var priorPeriod = createDefaultPriorPolicyPeriod()
      _policyInfo.PriorPolicyNumber = priorPeriod.PolicyNumber
      _policyInfo.PriorTermNumber = priorPeriod.TermNumber
      _policyInfo.AccountNumber = priorPeriod.Account.AccountNumber
    }

    super.create()
    return _policyInfo
  }

  private function createDefaultPriorPolicyPeriod() : PolicyPeriod {
    return new IssuanceBuilder()
      .createAndCommit()
  }
}