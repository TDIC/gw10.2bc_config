package gw.api.databuilder.webservice.policycenter.bc1000

uses gw.api.databuilder.AccountBuilder
uses gw.api.databuilder.BCDataBuilder
uses gw.api.databuilder.PaymentPlanBuilder
uses gw.api.domain.accounting.ChargePatternKey
uses gw.api.util.DateUtil
uses gw.pl.currency.MonetaryAmount
uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.BillingInstructionInfo_ChargeInfos
uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.IssuePolicyInfo_DefaultPolicyInvoiceStreamOverrides
uses gw.webservice.policycenter.bc1000.entity.types.complex.ChargeInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.IssuePolicyInfo
uses typekey.Currency

@Export
abstract class IssuePolicyInfoBuilderBase<T extends IssuePolicyInfo, B extends IssuePolicyInfoBuilderBase> {

  private var _currency : Currency
  private var _defaultDate = DateUtil.currentDate()

  protected var _policyInfo : T

  construct(currency : Currency){
    _currency = currency
    _policyInfo = new T()
    withBillingMethod(PolicyPeriodBillingMethod.TC_DIRECTBILL)
    withLOBCode(LOBCode.TC_PERSONALAUTO)
    withJurisdiction(Jurisdiction.TC_CA)
    withAssignedRisk(false)

    withEffectiveDate(_defaultDate)
    withModelDate(_defaultDate)
    withPeriodStart(_defaultDate)
    withPeriodEnd(_defaultDate.addYears(1))

    withPolicyNumber("PP-" + BCDataBuilder.createRandomWordPair())
    withTermNumber(1)

    _policyInfo.ChargeInfos = new ArrayList<BillingInstructionInfo_ChargeInfos>()
    addPremiumCharge(1000bd.ofCurrency(_currency))
  }

  construct(){
    this(BCDataBuilder.getDefaultBuilderCurrency())
  }

  public function withBillingMethod(billingMethod : PolicyPeriodBillingMethod) : B {
    _policyInfo.BillingMethodCode = billingMethod.Code
    return this as B
  }

  public function withLOBCode(lobCode : LOBCode) : B {
    _policyInfo.ProductCode = lobCode.Code
    return this as B
  }

  public function withJurisdiction(jurisdiction : Jurisdiction) : B {
    _policyInfo.JurisdictionCode = jurisdiction.Code
    return this as B
  }

  public function withAssignedRisk(assignedRisk : boolean) : B {
    _policyInfo.AssignedRisk = assignedRisk
    return this as B
  }

  public function withEffectiveDate(effectiveDate : Date) : B {
    _policyInfo.EffectiveDate = effectiveDate.XmlDateTime
    return this as B
  }

  public function withModelDate(modelDate : Date) : B {
    _policyInfo.ModelDate = modelDate.XmlDateTime
    return this as B
  }

  public function withPeriodStart(periodStartDate : Date) : B {
    _policyInfo.PeriodStart = periodStartDate.XmlDateTime
    return this as B
  }

  public function withPeriodEnd(periodEndDate : Date) : B {
    _policyInfo.PeriodEnd = periodEndDate.XmlDateTime
    return this as B
  }

  public function withTermNumber(termNumber : int) : B {
    _policyInfo.TermNumber = termNumber
    return this as B
  }

  public function withPolicyNumber(policyNumber : String) : B {
    _policyInfo.PolicyNumber = policyNumber
    return this as B
  }

  public function withOwnerAccount(account : Account) : B {
    _policyInfo.AccountNumber = account.AccountNumber
    return this as B
  }

  public function withAccountNumber(accountNumber : String) : B {
    _policyInfo.AccountNumber = accountNumber
    return this as B
  }

  public function withOverridingPayer(account : Account) : B {
    withOverridingPayer(account.AccountNumber)
    return this as B
  }

  function withDefaultOverridingPayer() : B {
    _policyInfo.AltBillingAccountNumber = createDefaultAccount().AccountNumber
    return this as B
  }

  public function withOverridingPayer(accountNumber : String) : B {
    _policyInfo.AltBillingAccountNumber = accountNumber
    return this as B
  }

  public function addPremiumCharge(amount : MonetaryAmount) : B {
    addCharge(amount, ChargePatternKey.PREMIUM)
    return this as B
  }

  public function replaceChargesWithPremiumCharge(amount : MonetaryAmount) : B {
    withNoCharges()
    addPremiumCharge(amount)
    return this as B
  }

  public function addCharge(amount : MonetaryAmount, chargePattern : ChargePatternKey) : B {
    var chargeInfo = new ChargeInfo()
    chargeInfo.Amount = amount.toString()
    chargeInfo.ChargePatternCode = chargePattern.get().ChargeCode
    chargeInfo.WrittenDate = _defaultDate.XmlDateTime

    addChargeInfo(chargeInfo)
    return this as B
  }

  public function addChargeInfo(chargeInfo : ChargeInfo) : B {
    _policyInfo.addChargeInfo(chargeInfo)
    return this as B
  }

  public function withNoCharges() : B {
    _policyInfo.ChargeInfos = new ArrayList<BillingInstructionInfo_ChargeInfos>()
    return this as B
  }

  public function withPaymentPlan(paymentPlan : PaymentPlan) : B {
    _policyInfo.PaymentPlanPublicId = paymentPlan.PublicID
    return this as B
  }

  public function withDefaultAnchorDateOverride(anchorDate : Date) : B {
    if(_policyInfo.DefaultPolicyInvoiceStreamOverrides == null){
      _policyInfo.DefaultPolicyInvoiceStreamOverrides = new IssuePolicyInfo_DefaultPolicyInvoiceStreamOverrides()
    }

    _policyInfo.DefaultPolicyInvoiceStreamOverrides.AnchorDate = anchorDate.XmlDateTime
    return this as B
  }

  public function withDefaultFirstDayOfMonth(firstDayOfMonth : int) : B {
    if(_policyInfo.DefaultPolicyInvoiceStreamOverrides == null){
      _policyInfo.DefaultPolicyInvoiceStreamOverrides = new IssuePolicyInfo_DefaultPolicyInvoiceStreamOverrides()
    }

    _policyInfo.DefaultPolicyInvoiceStreamOverrides.FirstDayOfMonth = firstDayOfMonth
    return this as B
  }

  function withDepositRequirement(monetaryAmount : MonetaryAmount) : B {
    _policyInfo.DepositRequirement = monetaryAmount.toString()
    return this as B
  }

  public function create() : T {
    _policyInfo.Currency = _currency.Code

    if(_policyInfo.AccountNumber == null){
      _policyInfo.AccountNumber = createDefaultAccount().AccountNumber
    }

    if(_policyInfo.PaymentPlanPublicId == null){
      _policyInfo.PaymentPlanPublicId = createDefaultPaymentPlan().PublicID
    }

    return _policyInfo as T
  }

  private function createDefaultAccount() : Account {
    return new AccountBuilder()
      .withCurrency(_currency)
      .createAndCommit()
  }

  private function createDefaultPaymentPlan() : PaymentPlan {
    return new PaymentPlanBuilder()
      .withSingleCurrency(_currency)
      .withPeriodicity(Periodicity.TC_MONTHLY)
      .withDownPaymentPercent(10)
      .createAndCommit()
  }
}