package gw.api.address

uses java.util.*

/**
 * A class that contains the defined Field ID constants
 */
/**
 * US424 - Define Users
 * 3/5/2015 - HermiaK
 * Include "STATE" as REQUIRED_FIELDS
 */
@Export
class BCAddressOwnerFieldId extends AddressOwnerFieldId {
  private construct(aName:String) {
    super(aName)
  }

  public final static var REQUIRED_FIELDS : Set<AddressOwnerFieldId> =  new HashSet<AddressOwnerFieldId>()
      { ADDRESSLINE1, CITY, STATE, COUNTRY }.freeze()

}