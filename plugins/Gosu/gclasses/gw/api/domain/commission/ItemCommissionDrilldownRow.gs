package gw.api.domain.commission

uses gw.api.util.Percentage
uses gw.pl.currency.MonetaryAmount

uses java.util.Collections

@Export
public class ItemCommissionDrilldownRow implements CommissionDrilldownRow {

  public var _itemCommission: ItemCommission

  var _reserve : MonetaryAmount
  var _earned : MonetaryAmount
  var _paid : MonetaryAmount
  var _writtenOff : MonetaryAmount
  var _total : MonetaryAmount

  var _commissionableGross : MonetaryAmount

  construct(itemCommission : ItemCommission) {
    _itemCommission = itemCommission

    _reserve = 0bd.ofCurrency(itemCommission.Currency)
    _earned = 0bd.ofCurrency(itemCommission.Currency)
    _paid = 0bd.ofCurrency(itemCommission.Currency)
    _writtenOff = 0bd.ofCurrency(itemCommission.Currency)
    _total = 0bd.ofCurrency(itemCommission.Currency)
    _commissionableGross = 0bd.ofCurrency(itemCommission.Currency)
  }

  public function addCommissionAmounts(itemCommission : ItemCommission) {
    _reserve += itemCommission.CommissionReserve
    _earned += itemCommission.DirectBillEarned
    _paid += itemCommission.AgencyBillRetained
    _writtenOff += itemCommission.WrittenOffCommission
    _total += itemCommission.CommissionAmount
    _commissionableGross += itemCommission.InvoiceItem.Amount
  }

  override property get Name(): String {
    return "Invoice Item - " + _itemCommission.InvoiceItem.EventDate
  }

  /**
   * Get gross for the associated invoice item
  */
  override property get CommissionableGross(): String {
    return _commissionableGross.renderWithZeroDash()
  }

  override property get Rate(): String {
    var percentAsBD = _itemCommission.InvoiceItem.getInvoiceItemCommissionOverride(_itemCommission.PolicyCommission.Role)
    return percentAsBD == null ? "-" : Percentage.fromPoints(percentAsBD).toString()
  }

  override property get Active(): Boolean {
    return _itemCommission.Active
  }

  override property get Children(): List <CommissionDrilldownRow> {
    return Collections.emptyList();
  }

  override property get Reserve(): String {
    return _reserve.renderWithZeroDash()
  }

  override property get Earned(): String {
    return _earned.renderWithZeroDash()
  }

  override property get Paid(): String {
    return _paid.renderWithZeroDash()
  }

  override property get WrittenOff(): String {
    return _writtenOff.renderWithZeroDash()
  }

  override property get Total(): String {
    return _total.renderWithZeroDash()
  }

  override property get RowType(): String {
    return "Item"
  }

  override public function toString():String {
    return Name;
  }
}