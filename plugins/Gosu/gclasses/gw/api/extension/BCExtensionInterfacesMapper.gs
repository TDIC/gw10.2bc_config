package gw.api.extension



/**
 * Extension interfaces mapper implementation.
 * <p>
 * WARNING: Customers must never modify this mapping class file. Treat this file as read-only, despite the @Export
 * annotation that is in this file for internal reasons. However, you can edit the writable implementation classes
 * that this file references.
 */
@Export
class BCExtensionInterfacesMapper implements ExtensionInterfacesMapper {

  override function bindImplementations(config: ExtensionInterfacesConfig) {
    config.setImplementation<gw.contactmapper.ab1000.IBCNameMapperExtensions>(new gw.contactmapper.ab1000.BCNameMapperExtensionsImpl())
    config.setImplementation<gw.contactmapper.ab900.IBCNameMapperExtensions>(new gw.contactmapper.ab900.BCNameMapperExtensionsImpl())
  }
}
