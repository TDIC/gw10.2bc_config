package perm

uses javax.annotation.Generated

@Generated("SystemPermissionType.ActivityEnhancement.ttx;SystemPermissionType.inbound.tix;SystemPermissionType.outbound.tix;SystemPermissionType.properties.tix;SystemPermissionType.tix;SystemPermissionType.tti;SystemPermissionType.ttx", "", "com.guidewire.pl.permissions.codegen.SystemPermissionsGenerator")
class System {
  static property get StorageUpdate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.STORAGEUPDATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get TransferNonTermLevelUnapplied_TDIC () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TRANSFERNONTERMLEVELUNAPPLIED_TDIC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get abcreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ABCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get abcreatepref () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ABCREATEPREF.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get abdelete () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ABDELETE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get abdeletepref () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ABDELETEPREF.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get abedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ABEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get abeditpref () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ABEDITPREF.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get abview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ABVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get abviewsearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ABVIEWSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctbillall () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTBILLALL.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctbillplanedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTBILLPLANEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctchargesedit_TDIC () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTCHARGESEDIT_TDIC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctchargesview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTCHARGESVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctclose () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTCLOSE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctcntcreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTCNTCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctcntdelete () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTCNTDELETE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctcntedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTCNTEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctcollview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTCOLLVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctcontview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTCONTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctcreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctdelview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTDELVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctdetedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTDETEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctdisbview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTDISBVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctdocview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTDOCVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctevalview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTEVALVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctfundstrackingview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTFUNDSTRACKINGVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get accthistview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTHISTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctignoresecurityzone () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTIGNORESECURITYZONE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctinvcview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTINVCVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctjournalview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTJOURNALVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctledgerview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTLEDGERVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctnoteview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTNOTEVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctplcytx () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTPLCYTX.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctplcyview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTPLCYVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctpmntview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTPMNTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctsearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctsummview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTSUMMVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get accttabview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTTABVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctttktview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTTTKTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get accttxnview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTTXNVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctwo () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACCTWO.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actapproveany () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTAPPROVEANY.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actcreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acteditunowned () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTEDITUNOWNED.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actmakemand () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTMAKEMAND.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actown () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTOWN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actpatcreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTPATCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actpatdelete () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTPATDELETE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actpatedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTPATEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actpatview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTPATVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actquenext_Ext () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTQUENEXT_EXT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actquepick_Ext () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTQUEPICK_EXT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actqueueassign () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTQUEUEASSIGN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actqueuenext () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTQUEUENEXT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actqueuepick () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTQUEUEPICK.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actraown () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTRAOWN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actraunown () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTRAUNOWN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actreviewassign () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTREVIEWASSIGN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actsearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get actviewallqueues () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ACTVIEWALLQUEUES.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get adhoccredit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ADHOCCREDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get admincmdline () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ADMINCMDLINE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get admindatachangeexec () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ADMINDATACHANGEEXEC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get admindatachangeview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ADMINDATACHANGEVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get admintabview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ADMINTABVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get agencybillplancreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.AGENCYBILLPLANCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get agencybillplanedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.AGENCYBILLPLANEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get agencybillplanview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.AGENCYBILLPLANVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get agencymoneyrecdsearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.AGENCYMONEYRECDSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get agencywo () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.AGENCYWO.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get alpmanage () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ALPMANAGE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get alpview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ALPVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get anytagcreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ANYTAGCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get anytagdelete () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ANYTAGDELETE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get anytagedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ANYTAGEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get anytagview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ANYTAGVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get archive () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ARCHIVE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get archivedisable () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ARCHIVEDISABLE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get archiveenable () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ARCHIVEENABLE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get archiveretrieve () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ARCHIVERETRIEVE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get attrmanage () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ATTRMANAGE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get attrview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ATTRVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get batchpmntedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.BATCHPMNTEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get batchpmntproc () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.BATCHPMNTPROC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get batchpmntview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.BATCHPMNTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get billplancreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.BILLPLANCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get billplanedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.BILLPLANEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get billplanview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.BILLPLANVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get businessadmin () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.BUSINESSADMIN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get buswkmanage () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.BUSWKMANAGE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get buswkview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.BUSWKVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get cashfromprodtx () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CASHFROMPRODTX.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get cashtoprodtx () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CASHTOPRODTX.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get cashtx () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CASHTX.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get cashunreltx () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CASHUNRELTX.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get changecontactsubtype () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CHANGECONTACTSUBTYPE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get chargebreakdowncategorytypecreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CHARGEBREAKDOWNCATEGORYTYPECREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get chargebreakdowncategorytypeedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CHARGEBREAKDOWNCATEGORYTYPEEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get chargebreakdowncategorytypeview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CHARGEBREAKDOWNCATEGORYTYPEVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get chargeholdcreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CHARGEHOLDCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get chargeholdrelease () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CHARGEHOLDRELEASE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get chargepatterncreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CHARGEPATTERNCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get chargepatternedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CHARGEPATTERNEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get chargepatternview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CHARGEPATTERNVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get chrgcmsnrateoverride () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CHRGCMSNRATEOVERRIDE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get colagencycreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.COLAGENCYCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get colagencydelete () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.COLAGENCYDELETE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get colagencyedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.COLAGENCYEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get colagencyview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.COLAGENCYVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get collateraldisburse () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.COLLATERALDISBURSE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get collateraldrawdown () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.COLLATERALDRAWDOWN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get collateralreqedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.COLLATERALREQEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get commpercoride () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.COMMPERCORIDE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get commplanassnedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.COMMPLANASSNEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get commplancreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.COMMPLANCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get commplanedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.COMMPLANEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get commplanoride () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.COMMPLANORIDE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get commplanview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.COMMPLANVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get confdoccreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CONFDOCCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get confdocdelete () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CONFDOCDELETE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get confdocedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CONFDOCEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get confdocview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CONFDOCVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get confnotecreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CONFNOTECREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get confnotedelete () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CONFNOTEDELETE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get confnoteedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CONFNOTEEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get confnoteview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CONFNOTEVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get ctccreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CTCCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get ctcedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CTCEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get ctcview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.CTCVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get debugtools () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DEBUGTOOLS.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get deleteproperties () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DELETEPROPERTIES.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get deliqedit_TDIC () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DELIQEDIT_TDIC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get delmednote () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DELMEDNOTE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get delplancreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DELPLANCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get delplancustedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DELPLANCUSTEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get delplanedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DELPLANEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get delplanview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DELPLANVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get delprivnote () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DELPRIVNOTE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get delsearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DELSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get delsensdoc () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DELSENSDOC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get delsensnote () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DELSENSNOTE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get disbauccreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DISBAUCCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get disbcreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DISBCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get disbpayeeedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DISBPAYEEEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get disbreview_TDIC () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DISBREVIEW_TDIC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get disbsearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DISBSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get doccreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DOCCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get docdelete () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DOCDELETE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get docedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DOCEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get docmodifyall () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DOCMODIFYALL.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get docview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DOCVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get docviewall () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.DOCVIEWALL.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get editSensMCMdetails () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.EDITSENSMCMDETAILS.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get editSensSIUdetails () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.EDITSENSSIUDETAILS.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get editinboundfiles () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.EDITINBOUNDFILES.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get editmednote () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.EDITMEDNOTE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get editobfuscatedusercontact () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.EDITOBFUSCATEDUSERCONTACT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get editoutboundfiles () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.EDITOUTBOUNDFILES.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get editprivnote () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.EDITPRIVNOTE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get editproperties () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.EDITPROPERTIES.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get editrefdata () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.EDITREFDATA.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get editsensdoc () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.EDITSENSDOC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get editsensnote () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.EDITSENSNOTE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get editsubrodetails () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.EDITSUBRODETAILS.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get equitywarningsedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.EQUITYWARNINGSEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get eventmessageview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.EVENTMESSAGEVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get exportproperties () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.EXPORTPROPERTIES.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get gentxn () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.GENTXN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get globalopview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.GLOBALOPVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get glocalopedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.GLOCALOPEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get groupcreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.GROUPCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get groupdelete () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.GROUPDELETE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get groupedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.GROUPEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get grouptreeview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.GROUPTREEVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get groupview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.GROUPVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get grpacctsearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.GRPACCTSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get hidecontsearch_TDIC () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.HIDECONTSEARCH_TDIC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get hideprodactivt_TDIC () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.HIDEPRODACTIVT_TDIC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get holidaymanage () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.HOLIDAYMANAGE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get holidayview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.HOLIDAYVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get importproperties () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.IMPORTPROPERTIES.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get intdoccreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INTDOCCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get intdocdelete () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INTDOCDELETE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get intdocedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INTDOCEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get intdocview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INTDOCVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get integadmin () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INTEGADMIN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get internaltools () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INTERNALTOOLS.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get intnotecreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INTNOTECREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get intnotedelete () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INTNOTEDELETE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get intnoteedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INTNOTEEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get intnoteview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INTNOTEVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get invccreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INVCCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get invcdateedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INVCDATEEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get invcmssgview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INVCMSSGVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get invcremove () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INVCREMOVE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get invcresend () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INVCRESEND.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get invcsearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INVCSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get invcstrmedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INVCSTRMEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get invmssgedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.INVMSSGEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get lvprint () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.LVPRINT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get manageldfctrs () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.MANAGELDFCTRS.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get manualDlnq_TDIC () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.MANUALDLNQ_TDIC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get modifyinvitem () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.MODIFYINVITEM.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get monitoradmin () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.MONITORADMIN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get moveinvitem () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.MOVEINVITEM.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get myagencyitemsview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.MYAGENCYITEMSVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get mydelinquenciesview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.MYDELINQUENCIESVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get mydisbview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.MYDISBVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get myttktview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.MYTTKTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get notecreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.NOTECREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get notedelete () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.NOTEDELETE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get noteedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.NOTEEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get noteeditbody () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.NOTEEDITBODY.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get noteview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.NOTEVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get orgcreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ORGCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get orgdelete () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ORGDELETE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get orgeditbasic () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ORGEDITBASIC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get orgsearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ORGSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get orgviewbasic () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ORGVIEWBASIC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get outboundfiles () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.OUTBOUNDFILES.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get outpmntsearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.OUTPMNTSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get ownsensclaim () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.OWNSENSCLAIM.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get ownsensclaimsub () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.OWNSENSCLAIMSUB.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get payallocplancreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PAYALLOCPLANCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get payallocplanedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PAYALLOCPLANEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get payallocplanview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PAYALLOCPLANVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcychange () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYCHANGE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcychargesedit_TDIC () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYCHARGESEDIT_TDIC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcychargesview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYCHARGESVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcycntcreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYCNTCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcycntdelete () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYCNTDELETE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcycntedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYCNTEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcycommview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYCOMMVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcycontview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYCONTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcycreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcydelview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYDELVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcydocview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYDOCVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcydtlsscrnedit_TDIC () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYDTLSSCRNEDIT_TDIC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcyhistview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYHISTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcyignoresecurityzone () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYIGNORESECURITYZONE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcyjournalview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYJOURNALVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcyledgerview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYLEDGERVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcynoteview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYNOTEVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcypmntview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYPMNTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcyprodtx () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYPRODTX.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcyrenew () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYRENEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcyreopen () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYREOPEN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcyreopen_TDIC () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYREOPEN_TDIC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcysearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcysummview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYSUMMVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcytabview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYTABVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcyttktview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYTTKTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcytxnview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYTXNVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcywo () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PLCYWO.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntdetpayedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PMNTDETPAYEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntdistedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PMNTDISTEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntmanmultproc () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PMNTMANMULTPROC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntmanproc () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PMNTMANPROC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntmanproc_tdic () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PMNTMANPROC_TDIC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntmethodedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PMNTMETHODEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntmultiagency () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PMNTMULTIAGENCY.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntplancreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PMNTPLANCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntplanedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PMNTPLANEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntplanview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PMNTPLANVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntreqonetimecreate_TDIC () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PMNTREQONETIMECREATE_TDIC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntreqsearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PMNTREQSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntreverse () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PMNTREVERSE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntscheedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PMNTSCHEEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntscheplcyedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PMNTSCHEPLCYEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntsearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PMNTSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodabcyclesview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODABCYCLESVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodabexceptionsview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODABEXCEPTIONSVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodabopenitemsview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODABOPENITEMSVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodabstmtresend () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODABSTMTRESEND.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodabstmtview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODABSTMTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodabtxnview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODABTXNVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodcntcreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODCNTCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodcntdelete () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODCNTDELETE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodcntedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODCNTEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodcontview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODCONTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodcreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get proddbstmtresend () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODDBSTMTRESEND.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get proddbstmtview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODDBSTMTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get proddbtxnview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODDBTXNVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get proddetedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODDETEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get proddocview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODDOCVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodhistview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODHISTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodignoresecurityzone () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODIGNORESECURITYZONE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodjournalview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODJOURNALVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodledgerview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODLEDGERVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodnoteview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODNOTEVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodplcyview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODPLCYVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodpmntadvman () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODPMNTADVMAN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodpmntbonusman () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODPMNTBONUSMAN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodpmntedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODPMNTEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodpmntprocman () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODPMNTPROCMAN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodpmntstndexman () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODPMNTSTNDEXMAN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodpmntstndman () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODPMNTSTNDMAN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodpmntview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODPMNTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodpromedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODPROMEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodpromview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODPROMVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodrevcashtopayablexfer () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODREVCASHTOPAYABLEXFER.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodrevpayablexfer () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODREVPAYABLEXFER.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodsearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodsummview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODSUMMVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodsuspitemsedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODSUSPITEMSEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodsuspitemsview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODSUSPITEMSVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodtabview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODTABVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodtieredit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODTIEREDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodttktview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODTTKTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodwo () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODWO.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodwriteoffsedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODWRITEOFFSEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodwriteoffsview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PRODWRITEOFFSVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get purge () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.PURGE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get regionmanage () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.REGIONMANAGE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get regionview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.REGIONVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get report_manager () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.REPORT_MANAGER.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get report_user () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.REPORT_USER.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get reporting_view () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.REPORTING_VIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get requestcontactdestruction () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.REQUESTCONTACTDESTRUCTION.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get resyncmessage () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.RESYNCMESSAGE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get retpremplancreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.RETPREMPLANCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get retpremplanedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.RETPREMPLANEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get retpremplanview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.RETPREMPLANVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get retrymessage () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.RETRYMESSAGE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get revtxn () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.REVTXN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get riedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.RIEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get riview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.RIVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get rolemanage () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ROLEMANAGE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get roleview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ROLEVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get ruleadmin () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.RULEADMIN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get scrprmmanage () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.SCRPRMMANAGE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get scrprmview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.SCRPRMVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get seczonemanage () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.SECZONEMANAGE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get sharedactcreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.SHAREDACTCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get sharedactedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.SHAREDACTEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get sharedactview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.SHAREDACTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get skipmessage () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.SKIPMESSAGE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get soapadmin () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.SOAPADMIN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get subrtxn () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.SUBRTXN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get susppmntedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.SUSPPMNTEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get susppmntproc () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.SUSPPMNTPROC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get susppmntview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.SUSPPMNTVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get toolsBatchProcessedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TOOLSBATCHPROCESSEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get toolsBatchProcessview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TOOLSBATCHPROCESSVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get toolsCacheinfoview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TOOLSCACHEINFOVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get toolsClusteredit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TOOLSCLUSTEREDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get toolsClusterview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TOOLSCLUSTERVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get toolsInfoview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TOOLSINFOVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get toolsJMXBeansEdit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TOOLSJMXBEANSEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get toolsJMXBeansview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TOOLSJMXBEANSVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get toolsJProfileredit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TOOLSJPROFILEREDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get toolsLogedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TOOLSLOGEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get toolsLogview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TOOLSLOGVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get toolsPluginedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TOOLSPLUGINEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get toolsPluginview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TOOLSPLUGINVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get toolsProfileredit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TOOLSPROFILEREDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get toolsProfilerview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TOOLSPROFILERVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get toolsWorkQueueedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TOOLSWORKQUEUEEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get toolsWorkQueueview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TOOLSWORKQUEUEVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get transferreversal () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TRANSFERREVERSAL.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get ttktassignany () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TTKTASSIGNANY.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get ttktassignown () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TTKTASSIGNOWN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get ttktcreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TTKTCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get ttktown () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TTKTOWN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get ttktsearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TTKTSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get txnsearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.TXNSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get useractivate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.USERACTIVATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get useradmin () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.USERADMIN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get usercreate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.USERCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get userdeactivate () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.USERDEACTIVATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get userdelete () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.USERDELETE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get useredit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.USEREDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get usereditattrs () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.USEREDITATTRS.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get usereditlang () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.USEREDITLANG.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get usergrantauth () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.USERGRANTAUTH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get usergrantroles () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.USERGRANTROLES.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get usersearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.USERSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get userview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.USERVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get userviewall () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.USERVIEWALL.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get utilitiesadmin () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.UTILITIESADMIN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewAdminHistory_ext () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWADMINHISTORY_EXT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewSensMCMdetails () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWSENSMCMDETAILS.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewSensSIUdetails () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWSENSSIUDETAILS.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewactcal () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWACTCAL.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewdesktop () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWDESKTOP.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewinboundfiles () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWINBOUNDFILES.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewmednote () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWMEDNOTE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewoutboundfiles () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWOUTBOUNDFILES.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewpolicysystem () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWPOLICYSYSTEM.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewprivnote () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWPRIVNOTE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewproperties () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWPROPERTIES.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewrefdata () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWREFDATA.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewsearch () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWSEARCH.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewsensdoc () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWSENSDOC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewsensnote () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWSENSNOTE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewsubrodetails () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWSUBRODETAILS.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewteam () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWTEAM.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get viewworkload () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.VIEWWORKLOAD.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get workflowmanage () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.WORKFLOWMANAGE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get workflowview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.WORKFLOWVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get wsdatachangeedit () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.WSDATACHANGEEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get zonemanage () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ZONEMANAGE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get zoneview () : boolean {
    return com.guidewire._generated.security.SystemPermKeys.ZONEVIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  
}