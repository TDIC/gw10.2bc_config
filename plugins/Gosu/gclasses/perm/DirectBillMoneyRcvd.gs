package perm

uses javax.annotation.Generated

@Generated("security-config-app.xml;security-config-pl.xml;security-config.xml", "", "com.guidewire.pl.permissions.codegen.EntityPermissionsGenerator")
class DirectBillMoneyRcvd {
  static property get pmntdistedit () : boolean {
    return com.guidewire._generated.security.DirectBillMoneyRcvdPermKeys.PMNTDISTEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntmanmultproc () : boolean {
    return com.guidewire._generated.security.DirectBillMoneyRcvdPermKeys.PMNTMANMULTPROC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntmanproc () : boolean {
    return com.guidewire._generated.security.DirectBillMoneyRcvdPermKeys.PMNTMANPROC.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntmethodedit () : boolean {
    return com.guidewire._generated.security.DirectBillMoneyRcvdPermKeys.PMNTMETHODEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntmultiagency () : boolean {
    return com.guidewire._generated.security.DirectBillMoneyRcvdPermKeys.PMNTMULTIAGENCY.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntreverse () : boolean {
    return com.guidewire._generated.security.DirectBillMoneyRcvdPermKeys.PMNTREVERSE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntscheedit () : boolean {
    return com.guidewire._generated.security.DirectBillMoneyRcvdPermKeys.PMNTSCHEEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  
}