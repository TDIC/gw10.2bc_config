package perm

uses javax.annotation.Generated

@Generated("security-config-app.xml;security-config-pl.xml;security-config.xml", "", "com.guidewire.pl.permissions.codegen.EntityPermissionsGenerator")
class Policy {
  static property get archivedisable () : boolean {
    return com.guidewire._generated.security.PolicyPermKeys.ARCHIVEDISABLE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get archiveenable () : boolean {
    return com.guidewire._generated.security.PolicyPermKeys.ARCHIVEENABLE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  
}