package perm

uses javax.annotation.Generated

@Generated("security-config-app.xml;security-config-pl.xml;security-config.xml", "", "com.guidewire.pl.permissions.codegen.EntityPermissionsGenerator")
class Account {
  static property get acctignoresecurityzone () : boolean {
    return com.guidewire._generated.security.AccountPermKeys.ACCTIGNORESECURITYZONE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get acctplcytx () : boolean {
    return com.guidewire._generated.security.AccountPermKeys.ACCTPLCYTX.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get chrgcmsnrateoverride () : boolean {
    return com.guidewire._generated.security.AccountPermKeys.CHRGCMSNRATEOVERRIDE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get close () : boolean {
    return com.guidewire._generated.security.AccountPermKeys.CLOSE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get collateraldisburse () : boolean {
    return com.guidewire._generated.security.AccountPermKeys.COLLATERALDISBURSE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get collateraldrawdown () : boolean {
    return com.guidewire._generated.security.AccountPermKeys.COLLATERALDRAWDOWN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get collateralreqedit () : boolean {
    return com.guidewire._generated.security.AccountPermKeys.COLLATERALREQEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get create () : boolean {
    return com.guidewire._generated.security.AccountPermKeys.CREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get edit () : boolean {
    return com.guidewire._generated.security.AccountPermKeys.EDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get invccreate () : boolean {
    return com.guidewire._generated.security.AccountPermKeys.INVCCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get invcdateedit () : boolean {
    return com.guidewire._generated.security.AccountPermKeys.INVCDATEEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get invcremove () : boolean {
    return com.guidewire._generated.security.AccountPermKeys.INVCREMOVE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get invcresend () : boolean {
    return com.guidewire._generated.security.AccountPermKeys.INVCRESEND.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get invcstrmedit () : boolean {
    return com.guidewire._generated.security.AccountPermKeys.INVCSTRMEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  
}