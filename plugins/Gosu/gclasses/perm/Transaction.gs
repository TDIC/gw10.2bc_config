package perm

uses javax.annotation.Generated

@Generated("security-config-app.xml;security-config-pl.xml;security-config.xml", "", "com.guidewire.pl.permissions.codegen.EntityPermissionsGenerator")
class Transaction {
  static property get acctwo () : boolean {
    return com.guidewire._generated.security.TransactionPermKeys.ACCTWO.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get cashtx () : boolean {
    return com.guidewire._generated.security.TransactionPermKeys.CASHTX.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get chargeholdcreate () : boolean {
    return com.guidewire._generated.security.TransactionPermKeys.CHARGEHOLDCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get chargeholdrelease () : boolean {
    return com.guidewire._generated.security.TransactionPermKeys.CHARGEHOLDRELEASE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get credit () : boolean {
    return com.guidewire._generated.security.TransactionPermKeys.CREDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get disbauccreate () : boolean {
    return com.guidewire._generated.security.TransactionPermKeys.DISBAUCCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get disbcreate () : boolean {
    return com.guidewire._generated.security.TransactionPermKeys.DISBCREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get disbpayeeedit () : boolean {
    return com.guidewire._generated.security.TransactionPermKeys.DISBPAYEEEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get gentxn () : boolean {
    return com.guidewire._generated.security.TransactionPermKeys.GENTXN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcywo () : boolean {
    return com.guidewire._generated.security.TransactionPermKeys.PLCYWO.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get revtxn () : boolean {
    return com.guidewire._generated.security.TransactionPermKeys.REVTXN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get subrtxn () : boolean {
    return com.guidewire._generated.security.TransactionPermKeys.SUBRTXN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  
}