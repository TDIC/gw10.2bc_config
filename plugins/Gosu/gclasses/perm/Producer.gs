package perm

uses javax.annotation.Generated

@Generated("security-config-app.xml;security-config-pl.xml;security-config.xml", "", "com.guidewire.pl.permissions.codegen.EntityPermissionsGenerator")
class Producer {
  static property get commpercoride () : boolean {
    return com.guidewire._generated.security.ProducerPermKeys.COMMPERCORIDE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get commplanassnedit () : boolean {
    return com.guidewire._generated.security.ProducerPermKeys.COMMPLANASSNEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get commplanoride () : boolean {
    return com.guidewire._generated.security.ProducerPermKeys.COMMPLANORIDE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get create () : boolean {
    return com.guidewire._generated.security.ProducerPermKeys.CREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get edit () : boolean {
    return com.guidewire._generated.security.ProducerPermKeys.EDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get proddbstmtresent () : boolean {
    return com.guidewire._generated.security.ProducerPermKeys.PRODDBSTMTRESENT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodignoresecurityzone () : boolean {
    return com.guidewire._generated.security.ProducerPermKeys.PRODIGNORESECURITYZONE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodpmntadvman () : boolean {
    return com.guidewire._generated.security.ProducerPermKeys.PRODPMNTADVMAN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodpmntbonusman () : boolean {
    return com.guidewire._generated.security.ProducerPermKeys.PRODPMNTBONUSMAN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodpmntstndman () : boolean {
    return com.guidewire._generated.security.ProducerPermKeys.PRODPMNTSTNDMAN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodrevcashtopayablexfer () : boolean {
    return com.guidewire._generated.security.ProducerPermKeys.PRODREVCASHTOPAYABLEXFER.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodrevpayablexfer () : boolean {
    return com.guidewire._generated.security.ProducerPermKeys.PRODREVPAYABLEXFER.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get prodtieredit () : boolean {
    return com.guidewire._generated.security.ProducerPermKeys.PRODTIEREDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  
}