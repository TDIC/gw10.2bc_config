package perm

uses java.lang.Object
uses javax.annotation.Generated

@Generated("security-config-app.xml;security-config-pl.xml;security-config.xml", "", "com.guidewire.pl.permissions.codegen.EntityPermissionsGenerator")
class TroubleTicket {
  static function assign (object :  Object) : boolean {
    return com.guidewire._generated.security.TroubleTicketPermKeys.ASSIGN.hasPermission(entity.User.util.CurrentUser, object)
  }
  
  static property get create () : boolean {
    return com.guidewire._generated.security.TroubleTicketPermKeys.CREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get own () : boolean {
    return com.guidewire._generated.security.TroubleTicketPermKeys.OWN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  
}