package perm

uses javax.annotation.Generated

@Generated("security-config-app.xml;security-config-pl.xml;security-config.xml", "", "com.guidewire.pl.permissions.codegen.EntityPermissionsGenerator")
class PolicyPeriod {
  static property get archiveretrieve () : boolean {
    return com.guidewire._generated.security.PolicyPeriodPermKeys.ARCHIVERETRIEVE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get create () : boolean {
    return com.guidewire._generated.security.PolicyPeriodPermKeys.CREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcychange () : boolean {
    return com.guidewire._generated.security.PolicyPeriodPermKeys.PLCYCHANGE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcyignoresecurityzone () : boolean {
    return com.guidewire._generated.security.PolicyPeriodPermKeys.PLCYIGNORESECURITYZONE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcyprodtx () : boolean {
    return com.guidewire._generated.security.PolicyPeriodPermKeys.PLCYPRODTX.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcyrenew () : boolean {
    return com.guidewire._generated.security.PolicyPeriodPermKeys.PLCYRENEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get plcyreopen () : boolean {
    return com.guidewire._generated.security.PolicyPeriodPermKeys.PLCYREOPEN.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get pmntscheplcyedit () : boolean {
    return com.guidewire._generated.security.PolicyPeriodPermKeys.PMNTSCHEPLCYEDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  
}