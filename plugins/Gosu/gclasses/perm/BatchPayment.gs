package perm

uses javax.annotation.Generated

@Generated("security-config-app.xml;security-config-pl.xml;security-config.xml", "", "com.guidewire.pl.permissions.codegen.EntityPermissionsGenerator")
class BatchPayment {
  static property get modify () : boolean {
    return com.guidewire._generated.security.BatchPaymentPermKeys.MODIFY.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get process () : boolean {
    return com.guidewire._generated.security.BatchPaymentPermKeys.PROCESS.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get view () : boolean {
    return com.guidewire._generated.security.BatchPaymentPermKeys.VIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  
}