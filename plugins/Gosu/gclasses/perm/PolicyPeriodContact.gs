package perm

uses javax.annotation.Generated

@Generated("security-config-app.xml;security-config-pl.xml;security-config.xml", "", "com.guidewire.pl.permissions.codegen.EntityPermissionsGenerator")
class PolicyPeriodContact {
  static property get create () : boolean {
    return com.guidewire._generated.security.PolicyPeriodContactPermKeys.CREATE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get delete () : boolean {
    return com.guidewire._generated.security.PolicyPeriodContactPermKeys.DELETE.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get edit () : boolean {
    return com.guidewire._generated.security.PolicyPeriodContactPermKeys.EDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  
}