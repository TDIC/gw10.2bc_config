package perm

uses javax.annotation.Generated

@Generated("security-config-app.xml;security-config-pl.xml;security-config.xml", "", "com.guidewire.pl.permissions.codegen.EntityPermissionsGenerator")
class SuspensePayment {
  static property get edit () : boolean {
    return com.guidewire._generated.security.SuspensePaymentPermKeys.EDIT.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get process () : boolean {
    return com.guidewire._generated.security.SuspensePaymentPermKeys.PROCESS.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  static property get view () : boolean {
    return com.guidewire._generated.security.SuspensePaymentPermKeys.VIEW.hasPermission(entity.User.util.CurrentUser, null)
  }
  
  
}