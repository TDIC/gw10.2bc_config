package acc.onbase.enhancement

uses gw.api.database.Query

/**
 * Hyland Build Version: 4.2.0-10-ga6a8ce6

 * <p>
 * Last Changes:
 * 10/17/2016 - Alicia Schroeder
 * * Initial implementation
 * * Methods to retrieve current Policy Period for a Policy and for a Policy Number
 */

enhancement PolicyPeriodFinderEnhancement: gw.api.domain.account.PolicyPeriodFinder {
  function getCurrentPolicyPeriodByPolicy(policy: Policy): PolicyPeriod {
    return policy.PolicyPeriods.firstWhere(\elt -> elt.Status == PolicyPeriodStatus.TC_INFORCE)
  }

  function getCurrentPolicyPeriodByNumber(policyNumber: String): PolicyPeriod {
    var policyPeriods = Query.make(PolicyPeriod).compare(PolicyPeriod#PolicyNumber, Equals, policyNumber).select()
    return policyPeriods.firstWhere(\elt -> elt.Status == PolicyPeriodStatus.TC_INFORCE)
  }
}