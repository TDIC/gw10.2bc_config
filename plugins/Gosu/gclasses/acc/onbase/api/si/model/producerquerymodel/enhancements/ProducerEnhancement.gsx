package acc.onbase.api.si.model.producerquerymodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ProducerEnhancement : acc.onbase.api.si.model.producerquerymodel.Producer {
  public static function create(object : entity.Producer) : acc.onbase.api.si.model.producerquerymodel.Producer {
    return new acc.onbase.api.si.model.producerquerymodel.Producer(object)
  }

  public static function create(object : entity.Producer, options : gw.api.gx.GXOptions) : acc.onbase.api.si.model.producerquerymodel.Producer {
    return new acc.onbase.api.si.model.producerquerymodel.Producer(object, options)
  }

}