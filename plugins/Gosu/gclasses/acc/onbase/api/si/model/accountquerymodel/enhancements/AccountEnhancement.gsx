package acc.onbase.api.si.model.accountquerymodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement AccountEnhancement : acc.onbase.api.si.model.accountquerymodel.Account {
  public static function create(object : entity.Account) : acc.onbase.api.si.model.accountquerymodel.Account {
    return new acc.onbase.api.si.model.accountquerymodel.Account(object)
  }

  public static function create(object : entity.Account, options : gw.api.gx.GXOptions) : acc.onbase.api.si.model.accountquerymodel.Account {
    return new acc.onbase.api.si.model.accountquerymodel.Account(object, options)
  }

}