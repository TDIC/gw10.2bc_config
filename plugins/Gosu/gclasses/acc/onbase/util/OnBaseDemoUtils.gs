package acc.onbase.util

/**
 * OnBaseDemoUtils - Code for OnBaseDemoPop.pcf
 * <p>
 * Hyland Build Version: 4.2.0-10-ga6a8ce6

 */
class OnBaseDemoUtils {

  public static function constructCustomQueryURL(account: Account): String {
    return "onbase://AE/Connector?Connection=Guidewire&Category=BillingCenter&Action=Custom+Query&Account+Number=" + UrlEncode(account.AccountNumber)
  }

  public static function constructFolderURL(account: Account): String {
    return "onbase://AE/Connector?Connection=Guidewire&Category=BillingCenter&Action=Billing+Folders&Account+Number=" + UrlEncode(account.AccountNumber)
  }

  public static function uploadDocURL(account: Account): String {
    return "onbase://AE/Connector?Connection=Guidewire&Category=BillingCenter&Action=Upload+Doc&Account+Number=" + UrlEncode(account.AccountNumber)
  }

  public static function unityFormURL(account: Account): String {
    return "onbase://AE/Connector?Connection=Guidewire&Category=BillingCenter&Action=Unity+Form&Account+Number=" + UrlEncode(account.AccountNumber)
  }

  public static function generateDocPacketURL(account: Account, templateName: String): String {
    return "onbase://AE/Connector?Connection=Guidewire&Category=BillingCenter&Action=Document+Packet&Template+Name=" + UrlEncode(templateName) + "&Account+Number=" + UrlEncode(account.AccountNumber)
  }

  private static function UrlEncode(urlParam: String): String {
    return java.net.URLEncoder.encode(urlParam, java.nio.charset.StandardCharsets.UTF_8.toString())
  }

}