package libraries
uses gw.api.util.CurrencyUtil
uses gw.pl.currency.MonetaryAmount
uses pcf.AgencyBillStatementDetailPopup

@Export
enhancement InvoiceItemEnhancement : entity.InvoiceItem {
  function StatementInvoiceDetailViewAction() {
    if (this.Invoice typeis StatementInvoice) {
      AgencyBillStatementDetailPopup.push(this.Invoice)
    }
  }

  /*
 * returns the outstanding amount of the invoiceItem by subtracting the paidAmount and the writtenoff amount.
 */
  property get OutstandingBalanceTDIC() : MonetaryAmount {
    var outstandingBalance = this.Amount?.subtract(this.PaidAmount)?.subtract(this.GrossAmountWrittenOff)
    if (outstandingBalance != null and outstandingBalance.IsPositive) {
      return outstandingBalance
    } else {
      return new MonetaryAmount(0bd, CurrencyUtil.DefaultCurrency)
    }
  }
}
