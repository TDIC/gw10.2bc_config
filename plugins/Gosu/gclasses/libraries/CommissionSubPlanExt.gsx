package libraries

@Export
enhancement CommissionSubPlanExt : CommissionSubPlan {
  property get PremiumIncentives() : PremiumIncentive[] {
    return this.Incentives.whereTypeIs(PremiumIncentive)
  }
}