package entity

@javax.annotation.Generated("com.guidewire.pl.metadata.codegen.Codegen", "", "DistArchiveReferenceByChargeOwner.eti;DistArchiveReferenceByChargeOwner.eix;DistArchiveReferenceByChargeOwner.etx")
enhancement GWDistArchiveReferenceByChargeOwnerEntityEnhancement : entity.DistArchiveReferenceByChargeOwner {
  property get ChargeTAccountOwner () : entity.TAccountOwner {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.archiving.ArchiveReferenceByChargeOwner") as gw.archiving.ArchiveReferenceByChargeOwner).ChargeTAccountOwner
  }
  
  
}