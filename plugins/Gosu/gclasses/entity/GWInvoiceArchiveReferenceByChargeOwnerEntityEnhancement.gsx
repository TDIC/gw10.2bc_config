package entity

@javax.annotation.Generated("com.guidewire.pl.metadata.codegen.Codegen", "", "InvoiceArchiveReferenceByChargeOwner.eti;InvoiceArchiveReferenceByChargeOwner.eix;InvoiceArchiveReferenceByChargeOwner.etx")
enhancement GWInvoiceArchiveReferenceByChargeOwnerEntityEnhancement : entity.InvoiceArchiveReferenceByChargeOwner {
  property get ChargeTAccountOwner () : entity.TAccountOwner {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.archiving.ArchiveReferenceByChargeOwner") as gw.archiving.ArchiveReferenceByChargeOwner).ChargeTAccountOwner
  }
  
  
}