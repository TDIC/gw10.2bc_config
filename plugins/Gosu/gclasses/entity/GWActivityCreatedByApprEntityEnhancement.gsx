package entity

@javax.annotation.Generated("com.guidewire.pl.metadata.codegen.Codegen", "", "ActivityCreatedByAppr.eti;ActivityCreatedByAppr.eix;ActivityCreatedByAppr.etx")
enhancement GWActivityCreatedByApprEntityEnhancement : entity.ActivityCreatedByAppr {
  function associateRelatedEntities () : void {
    (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.acc.activityenhancement.IApprovalActivity") as gw.acc.activityenhancement.IApprovalActivity).associateRelatedEntities()
  }
  
  property get ApprovalHandler () : com.guidewire.bc.domain.approval.BCApprovalHandler {
    return (com.guidewire.pl.persistence.code.EntityInternalAccess.getImplementation(this, "gw.acc.activityenhancement.IApprovalActivity") as gw.acc.activityenhancement.IApprovalActivity).ApprovalHandler
  }
  
  
}