package rules.Preupdate.PaymentRequestPreupdate_dir.TDIC_WC100000_dir

uses gw.api.system.database.SequenceUtil
uses gw.api.util.DateUtil
uses gw.api.system.BCLoggerCategory
uses gw.api.web.payment.DirectBillPaymentFactory
uses com.tdic.util.properties.PropertyUtil
uses com.tdic.util.misc.EmailUtil
uses org.slf4j.LoggerFactory

@gw.rules.RuleName("TDIC_PRPU110000 - Payment Request Status Drafted")
internal class TDIC_PRPU110000PaymentRequestStatusDrafted {
  static function doCondition(paymentRequest : entity.PaymentRequest) : boolean {
/*start00rule*/
return paymentRequest.isFieldChanged(PaymentRequest#Status)
    && DateUtil.compareIgnoreTime(DateUtil.currentDate(), paymentRequest.DraftDate) >= 0
/*end00rule*/
}

  static function doAction(paymentRequest : entity.PaymentRequest, actions : gw.rules.Action) {
/*start00rule*/
/**
 * US568 - Bank/EFT Integration
 * 10/14/2014 Alvin Lee
 *
 * In the TDIC Billing Plan, the Draft Date is set to 0 days after the Fix Date, unless the Draft Date is on a
 * non-business day.  Therefore, if the current date is the Draft Date, make the status jump directly to "Drafted" so
 * the integration will kick off.  This avoids the issue of the PaymentRequest batch job having to increment through
 * each status on a daily basis.  This Preupdate rule will also post the payment when the payment request's status is
 * "Drafted".  The reason for posting the payment here is to avoid creating an entity in the MessageTransport, as that
 * will not fire off the Event Fired rules for the transaction being created, which is needed for the GL Integration.
 */
if (paymentRequest.Status == PaymentRequestStatus.TC_CREATED
    || paymentRequest.Status == PaymentRequestStatus.TC_REQUESTED
    || paymentRequest.Status == PaymentRequestStatus.TC_FIXED) {
  LoggerFactory.getLogger("Rules").debug("TDIC_PRPU110000 - Payment Request Status Drafted - Payment Request for account '"
      + paymentRequest.Account + "' is in Created, Requested, or Fixed status, and current date is same as or past the Draft Date. Incrementing status to Drafted.")
  paymentRequest.Status = PaymentRequestStatus.TC_DRAFTED
}
  // Post Payment to Account
if (paymentRequest.Status == PaymentRequestStatus.TC_DRAFTED) {
  LoggerFactory.getLogger("Rules").debug("TDIC_PRPU110000 - Payment Request Status Drafted - Payment Request status for account '"
      + paymentRequest.Account + "'  is Drafted.")
  /**
   * US1158 - EFT Reversals
   * 3/12/2015 Alvin Lee
   *
   * Guard against duplicate payments to the same invoice.  Do not post payment and cancel the payment request if there
   * is no amount due on the invoice.
   */
  if(paymentRequest.IsOneTimePayment_TDIC){
    var dbmr = DirectBillPaymentFactory.createDirectBillMoneyRcvd(paymentRequest.UnappliedFund_TDIC, paymentRequest.PaymentInstrument, paymentRequest.Amount)
    dbmr.distribute()
    var refNumber = SequenceUtil.next(100000000, "One_Time_PaymentRequest") as String
    dbmr.RefNumber = refNumber
    paymentRequest.Referencenumber_TDIC = refNumber
  } else if (paymentRequest.Invoice.AmountDue.IsPositive) {  //TODO Code commented since we are not using the Invoice Logic, might be needed in future
    LoggerFactory.getLogger("Rules").debug("TDIC_PRPU110000 - Payment Request Status Drafted - Invoice has a positive amount due.  Posting Payment to account.")
    var dbmr = DirectBillPaymentFactory.createDirectBillMoneyRcvdWithInvoice(paymentRequest.Account, paymentRequest.PaymentInstrument, paymentRequest.Amount, paymentRequest.Invoice)
    dbmr.distribute()
    }
  else {
    LoggerFactory.getLogger("Rules").warn("TDIC_PRPU110000 - Payment Request Status Drafted - Payment Request with PublicID '"
        + paymentRequest.PublicID + "' in the amount of " + paymentRequest.Amount + " for Invoice "
        + paymentRequest.Invoice.InvoiceNumber + " and Account " + paymentRequest.Account
        + " is invalid since there is no amount due on the invoice. Possible duplicate payment request created by "
        + paymentRequest.CreateUser + ". Cancelling payment request and not posting payment.")
    paymentRequest.cancelRequest()
    //GWPS-1921 Suppressing the warning email
    /*var notificationEmail = PropertyUtil.getInstance().getProperty("PaymentNotificationEmail")
    if (notificationEmail == null) {
      LoggerFactory.getLogger("Rules").error("TDIC_PRPU110000 - Payment Request Status Drafted - Cannot retrieve notification email addresses with the key 'PaymentNotificationEmail' from integration database.")
    }
    EmailUtil.sendEmail(notificationEmail, "Bank/EFT Warning on server " + gw.api.system.server.ServerUtil.ServerId,
        "Payment Request in the amount of " + paymentRequest.Amount + " for Invoice "
        + paymentRequest.Invoice.InvoiceNumber + " and Account " + paymentRequest.Account
        + " is invalid since there is no amount due on the invoice. Possible duplicate payment request created by "
        + paymentRequest.CreateUser + ". Payment request has been cancelled, and payment has not been posted.")*/
  }
}
/*end00rule*/
  }
}
