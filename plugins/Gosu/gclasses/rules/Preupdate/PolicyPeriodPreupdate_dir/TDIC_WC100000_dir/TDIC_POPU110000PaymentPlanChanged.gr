package rules.Preupdate.PolicyPeriodPreupdate_dir.TDIC_WC100000_dir

uses gw.api.database.Query
uses gw.transaction.Transaction
uses gw.invoice.InvoiceItemFilter
uses gw.api.system.BCLoggerCategory
uses gw.api.domain.invoice.PaymentPlanChanger
uses java.util.Date
uses gw.api.locale.DisplayKey
uses org.slf4j.LoggerFactory

@gw.rules.RuleName("TDIC_POPU110000 - Payment Plan Changed")
internal class TDIC_POPU110000PaymentPlanChanged {
  static function doCondition(policyPeriod  :  entity.PolicyPeriod) : boolean {
/*start00rule*/
return not policyPeriod.New and policyPeriod.isFieldChanged(PolicyPeriod#PaymentPlan)
/*end00rule*/
}

  static function doAction(policyPeriod  :  entity.PolicyPeriod, actions : gw.rules.Action) {
/*start00rule*/
// Vicente, 08/19/2014, US65: If the Policy Period Payment Plan is "TDIC Monthly APW" payment and change to
// a different payment plan a responsive payment method is set
var tdicPaymentPlan = Query.make(PaymentPlan).compare(PaymentPlan#Name,Equals,"TDIC Monthly APW").select().first()
var responsivePaymentInstrument = policyPeriod.Account.PaymentInstruments.
  firstWhere( \ paymentInstrument -> paymentInstrument.PaymentMethod == typekey.PaymentMethod.TC_RESPONSIVE)
var _logger =  LoggerFactory.getLogger("Rules")
if(tdicPaymentPlan != null) {
  if(responsivePaymentInstrument != null){
    //Handle when payment plan is changed from "TDIC Monthly APW" to a non-APW payment plan
    if(policyPeriod.getOriginalValue("PaymentPlan") == tdicPaymentPlan.ID){
      _logger.debug("PolicyPeriodPreUpdate: Payment Plan changing from " + tdicPaymentPlan.Name + " to " + policyPeriod.PaymentPlan.Name)
      var account = policyPeriod.Account

      /**
       * US64 - Maintain Policy Payment Schedule
       * 09/11/2014 Alvin Lee
       *
       * Change Invoice Day Of Month to 1 day prior to Policy Effective Day.  Note that this is specific to Release A
       * where there is only 1 policy per account.  In Release B, the Invoice Day Of Month is no longer used and an
       * override will be created on each Invoice Stream.
       *
       * 5/1/2015 Alvin Lee
       * Update: Instead of setting to one day before policyEffectiveDay, Bill Date Billing is used
       * and the InvoiceDateOfMonth needs to be set to DaysFromReferenceDateToFirstInstallment (currently -46 days from
       * the Policy Effective Date).
       */
      var firstBillDate = policyPeriod.PolicyPerEffDate.addDays(policyPeriod.PaymentPlan.DaysFromReferenceDateToFirstInstallment)
      account.InvoiceDayOfMonth = firstBillDate.DayOfMonth
      account.BillDateOrDueDateBilling = BillDateOrDueDateBilling.TC_BILLDATEBILLING

      var newPaymentPlan = policyPeriod.PaymentPlan
      _logger.debug("Updating the Default Payment Instrument of Account '" + account + "' to have " + responsivePaymentInstrument + " Payment Instrument")
      policyPeriod.Account.DefaultPaymentInstrument = responsivePaymentInstrument

      //US1131, Vicente, 01/28/2015, All the Account's APW Payment Instruments that are not the Default Payment Instrument or responsive are removed
      _logger.debug("Removing all the Account's APW Payment Instruments that are not the default payment instrument or responsive of Account:" + account.AccountNumber )
      account.updateAccountPaymentInstruments()

      _logger.debug("Updating all Policy Periods under Account '" + account + "' to have " + newPaymentPlan + " Payment Plan")
      var accountPPeriods = account.AllPolicyPeriods
      var redistributePayments = gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange.ReverseAndRedistribute
      var itemFilterType = InvoiceItemFilterType.TC_PLANNEDITEMS
      var includeDownPaymentItems = true
      var bundle = Transaction.getCurrent()
      var changer : PaymentPlanChanger
      for(accountPolicyPeriod in accountPPeriods) {
        /**
         * US64 - Maintain Policy Payment Schedule
         * 09/11/2014 Vicente
         *
         * Change the payment plan for the Policy Periods that are not expired
         */
        if(not accountPolicyPeriod.IsExpired_TDIC){
          _logger.debug("Changing the payment plan of Policy Period '" + accountPolicyPeriod.PolicyNumberLong + "' to " + newPaymentPlan + " Payment Plan")
          //Add the Policy Period and Payment Plan to the current bundle in order to modify the Payment Plan
          accountPolicyPeriod = bundle.add(accountPolicyPeriod)
          newPaymentPlan = bundle.add(newPaymentPlan)
          accountPolicyPeriod.NewPaymentPlan_TDIC = newPaymentPlan
          changer = new PaymentPlanChanger(accountPolicyPeriod, newPaymentPlan, redistributePayments, InvoiceItemFilter.getFilter(itemFilterType, includeDownPaymentItems))
          changer.execute()
        } else {
          _logger.debug("The Policy Period '" + accountPolicyPeriod.PolicyNumberLong + "' will not change to " + newPaymentPlan + " Payment Plan because the Final Audit was completed and all Invoices have been billed.")
        }
      }
      /**
       * Hermia, US71 and US78 record event in History when ACH/EFT is added as Payment Instrument
       */
      account.addHistoryFromGosu(Date.CurrentDate, HistoryEventType.TC_PAYMENTINSTRUMENTADDED_TDIC,
          DisplayKey.get("TDIC.AccountHistory.APWRemoved"), null, null, false)
    }
    //Handle when payment plan is changed from a non-APW payment plan to "TDIC Monthly APW"
    else if (policyPeriod.PaymentPlan == tdicPaymentPlan) {
      _logger.debug("PolicyPeriodPreUpdate: Payment Plan changing to " + tdicPaymentPlan.Name)
      /**
       * US64 - Maintain Policy Payment Schedule
       * 09/11/2014 Alvin Lee
       *
       * Change Invoice Day Of Month to the default APW Invoice Day of Month, which is determined by the policy period
       * effective date.
       */
      var account = policyPeriod.Account
      account.InvoiceDayOfMonth = policyPeriod.APWInvoiceDayOfMonth_TDIC
      // 5/1/2015 Alvin Lee - Update: Set to Due Date Billing for APW.
      account.BillDateOrDueDateBilling = BillDateOrDueDateBilling.TC_DUEDATEBILLING
    }
  } else {
    _logger.error("There is not a Responsive Payment Instrument available.")
  }
} else {
  _logger.error("'TDIC Monthly APW Payment Plan' is not available.")
} 
/*end00rule*/
  }
}
