package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/activityenhancement/EdgeEntitiesInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class EdgeEntitiesInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($activity :  Activity) : void {
    __widgetOf(this, pcf.EdgeEntitiesInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$activity})
  }
  
  function refreshVariables ($activity :  Activity) : void {
    __widgetOf(this, pcf.EdgeEntitiesInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$activity})
  }
  
  
}