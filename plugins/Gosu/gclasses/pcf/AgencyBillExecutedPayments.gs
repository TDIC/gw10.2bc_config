package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExecutedPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyBillExecutedPayments extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (producer :  Producer) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPayments, {producer}, 0)
  }
  
  static function createDestination (producer :  Producer, initialSelectedMoney :  AgencyBillMoneyRcvd) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPayments, {producer, initialSelectedMoney}, 1)
  }
  
  static function drilldown (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPayments, {producer}, 0).drilldown()
  }
  
  static function drilldown (producer :  Producer, initialSelectedMoney :  AgencyBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPayments, {producer, initialSelectedMoney}, 1).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPayments, {producer}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (producer :  Producer, initialSelectedMoney :  AgencyBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPayments, {producer, initialSelectedMoney}, 1).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPayments, {producer}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (producer :  Producer, initialSelectedMoney :  AgencyBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPayments, {producer, initialSelectedMoney}, 1).goInMain()
  }
  
  static function printPage (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPayments, {producer}, 0).printPage()
  }
  
  static function printPage (producer :  Producer, initialSelectedMoney :  AgencyBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPayments, {producer, initialSelectedMoney}, 1).printPage()
  }
  
  static function push (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPayments, {producer}, 0).push()
  }
  
  static function push (producer :  Producer, initialSelectedMoney :  AgencyBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPayments, {producer, initialSelectedMoney}, 1).push()
  }
  
  
}