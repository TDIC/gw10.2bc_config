package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketInfoPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TroubleTicketInfoPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (troubleTicket :  TroubleTicket, assigneeHolder :  gw.api.assignment.Assignee[]) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TroubleTicketInfoPopup, {troubleTicket, assigneeHolder}, 0)
  }
  
  static function push (troubleTicket :  TroubleTicket, assigneeHolder :  gw.api.assignment.Assignee[]) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TroubleTicketInfoPopup, {troubleTicket, assigneeHolder}, 0).push()
  }
  
  
}