package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralManualDrawdownAmountScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CollateralManualDrawdownAmountScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($collateralDrawdown :  gw.api.web.collateral.CollateralDrawdownUtil) : void {
    __widgetOf(this, pcf.CollateralManualDrawdownAmountScreen, SECTION_WIDGET_CLASS).setVariables(false, {$collateralDrawdown})
  }
  
  function refreshVariables ($collateralDrawdown :  gw.api.web.collateral.CollateralDrawdownUtil) : void {
    __widgetOf(this, pcf.CollateralManualDrawdownAmountScreen, SECTION_WIDGET_CLASS).setVariables(true, {$collateralDrawdown})
  }
  
  
}