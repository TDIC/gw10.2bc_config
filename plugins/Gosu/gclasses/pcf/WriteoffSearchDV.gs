package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/WriteoffSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WriteoffSearchDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($writeoffSearch :  gw.search.WriteoffSearchCriteria, $accountEditable :  Boolean) : void {
    __widgetOf(this, pcf.WriteoffSearchDV, SECTION_WIDGET_CLASS).setVariables(false, {$writeoffSearch, $accountEditable})
  }
  
  function refreshVariables ($writeoffSearch :  gw.search.WriteoffSearchCriteria, $accountEditable :  Boolean) : void {
    __widgetOf(this, pcf.WriteoffSearchDV, SECTION_WIDGET_CLASS).setVariables(true, {$writeoffSearch, $accountEditable})
  }
  
  
}