package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentAdvanceDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewCommissionPaymentAdvanceDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($advancePayment :  AdvanceCmsnPayment, $canEdit :  Boolean) : void {
    __widgetOf(this, pcf.NewCommissionPaymentAdvanceDV, SECTION_WIDGET_CLASS).setVariables(false, {$advancePayment, $canEdit})
  }
  
  function refreshVariables ($advancePayment :  AdvanceCmsnPayment, $canEdit :  Boolean) : void {
    __widgetOf(this, pcf.NewCommissionPaymentAdvanceDV, SECTION_WIDGET_CLASS).setVariables(true, {$advancePayment, $canEdit})
  }
  
  
}