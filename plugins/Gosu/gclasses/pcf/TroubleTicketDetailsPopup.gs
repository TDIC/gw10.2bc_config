package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TroubleTicketDetailsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (troubleTicket :  TroubleTicket) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TroubleTicketDetailsPopup, {troubleTicket}, 0)
  }
  
  static function push (troubleTicket :  TroubleTicket) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TroubleTicketDetailsPopup, {troubleTicket}, 0).push()
  }
  
  
}