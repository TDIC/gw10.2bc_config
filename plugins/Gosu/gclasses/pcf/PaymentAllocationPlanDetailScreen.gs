package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/allocationplan/PaymentAllocationPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PaymentAllocationPlanDetailScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($paymentAllocationPlan :  PaymentAllocationPlan) : void {
    __widgetOf(this, pcf.PaymentAllocationPlanDetailScreen, SECTION_WIDGET_CLASS).setVariables(false, {$paymentAllocationPlan})
  }
  
  function refreshVariables ($paymentAllocationPlan :  PaymentAllocationPlan) : void {
    __widgetOf(this, pcf.PaymentAllocationPlanDetailScreen, SECTION_WIDGET_CLASS).setVariables(true, {$paymentAllocationPlan})
  }
  
  
}