package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/multicurrencyplans/PlanMultiCurrencyFeeThresholdInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PlanMultiCurrencyFeeThresholdInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($plan :  MultiCurrencyPlan, $feeThresholdDefaultPropertyInfo :  gw.lang.reflect.IPropertyInfo, $feeThresholdLabel :  String, $required :  boolean, $alwaysEditable :  Boolean, $validationFunc :  gw.lang.function.IBlock) : void {
    __widgetOf(this, pcf.PlanMultiCurrencyFeeThresholdInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$plan, $feeThresholdDefaultPropertyInfo, $feeThresholdLabel, $required, $alwaysEditable, $validationFunc})
  }
  
  function refreshVariables ($plan :  MultiCurrencyPlan, $feeThresholdDefaultPropertyInfo :  gw.lang.reflect.IPropertyInfo, $feeThresholdLabel :  String, $required :  boolean, $alwaysEditable :  Boolean, $validationFunc :  gw.lang.function.IBlock) : void {
    __widgetOf(this, pcf.PlanMultiCurrencyFeeThresholdInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$plan, $feeThresholdDefaultPropertyInfo, $feeThresholdLabel, $required, $alwaysEditable, $validationFunc})
  }
  
  
}