package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessesDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyProcessesDetailScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($target :  gw.api.domain.delinquency.DelinquencyTarget, $initialDelinquencyProcess :  DelinquencyProcess) : void {
    __widgetOf(this, pcf.DelinquencyProcessesDetailScreen, SECTION_WIDGET_CLASS).setVariables(false, {$target, $initialDelinquencyProcess})
  }
  
  function refreshVariables ($target :  gw.api.domain.delinquency.DelinquencyTarget, $initialDelinquencyProcess :  DelinquencyProcess) : void {
    __widgetOf(this, pcf.DelinquencyProcessesDetailScreen, SECTION_WIDGET_CLASS).setVariables(true, {$target, $initialDelinquencyProcess})
  }
  
  
}