package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/ActivityDetailToolbarButtonSet.disbursementapproval.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ActivityDetailToolbarButtonSet_disbursementapproval extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($activity :  Activity, $assigneeHolder :  gw.api.assignment.Assignee[]) : void {
    __widgetOf(this, pcf.ActivityDetailToolbarButtonSet_disbursementapproval, SECTION_WIDGET_CLASS).setVariables(false, {$activity, $assigneeHolder})
  }
  
  function refreshVariables ($activity :  Activity, $assigneeHolder :  gw.api.assignment.Assignee[]) : void {
    __widgetOf(this, pcf.ActivityDetailToolbarButtonSet_disbursementapproval, SECTION_WIDGET_CLASS).setVariables(true, {$activity, $assigneeHolder})
  }
  
  
}