package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadRewriteLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DataUploadRewriteLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($processor :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) : void {
    __widgetOf(this, pcf.DataUploadRewriteLV, SECTION_WIDGET_CLASS).setVariables(false, {$processor})
  }
  
  function refreshVariables ($processor :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) : void {
    __widgetOf(this, pcf.DataUploadRewriteLV, SECTION_WIDGET_CLASS).setVariables(true, {$processor})
  }
  
  
}