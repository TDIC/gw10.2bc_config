package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/MoveInvoiceItemsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class MoveInvoiceItemsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (invoiceItems :  InvoiceItem[], payer :  gw.api.domain.invoice.InvoicePayer) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.MoveInvoiceItemsPopup, {invoiceItems, payer}, 0)
  }
  
  function pickValueAndCommit (value :  Invoice) : void {
    __widgetOf(this, pcf.MoveInvoiceItemsPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (invoiceItems :  InvoiceItem[], payer :  gw.api.domain.invoice.InvoicePayer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.MoveInvoiceItemsPopup, {invoiceItems, payer}, 0).push()
  }
  
  
}