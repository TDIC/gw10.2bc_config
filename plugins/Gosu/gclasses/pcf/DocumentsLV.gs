package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/DocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DocumentsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($Documents :  gw.api.database.IQueryBeanResult<Document>, $documentSearchCriteria :  DocumentSearchCriteria, $activityReadonlyFlag :  boolean) : void {
    __widgetOf(this, pcf.DocumentsLV, SECTION_WIDGET_CLASS).setVariables(false, {$Documents, $documentSearchCriteria, $activityReadonlyFlag})
  }
  
  function refreshVariables ($Documents :  gw.api.database.IQueryBeanResult<Document>, $documentSearchCriteria :  DocumentSearchCriteria, $activityReadonlyFlag :  boolean) : void {
    __widgetOf(this, pcf.DocumentsLV, SECTION_WIDGET_CLASS).setVariables(true, {$Documents, $documentSearchCriteria, $activityReadonlyFlag})
  }
  
  
}