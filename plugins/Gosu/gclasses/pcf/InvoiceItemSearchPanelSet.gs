package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/InvoiceItemSearchPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class InvoiceItemSearchPanelSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($baseDist :  BaseDist, $initialPayerAccount :  Account, $initialPayerProducer :  Producer) : void {
    __widgetOf(this, pcf.InvoiceItemSearchPanelSet, SECTION_WIDGET_CLASS).setVariables(false, {$baseDist, $initialPayerAccount, $initialPayerProducer})
  }
  
  function refreshVariables ($baseDist :  BaseDist, $initialPayerAccount :  Account, $initialPayerProducer :  Producer) : void {
    __widgetOf(this, pcf.InvoiceItemSearchPanelSet, SECTION_WIDGET_CLASS).setVariables(true, {$baseDist, $initialPayerAccount, $initialPayerProducer})
  }
  
  
}