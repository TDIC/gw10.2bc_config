package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/NewActivityMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewActivityMenuItemSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($isShared :  Boolean, $troubleTicket :  TroubleTicket, $account :  Account, $policyPeriod :  PolicyPeriod) : void {
    __widgetOf(this, pcf.NewActivityMenuItemSet, SECTION_WIDGET_CLASS).setVariables(false, {$isShared, $troubleTicket, $account, $policyPeriod})
  }
  
  function refreshVariables ($isShared :  Boolean, $troubleTicket :  TroubleTicket, $account :  Account, $policyPeriod :  PolicyPeriod) : void {
    __widgetOf(this, pcf.NewActivityMenuItemSet, SECTION_WIDGET_CLASS).setVariables(true, {$isShared, $troubleTicket, $account, $policyPeriod})
  }
  
  
}