package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountNewNoteWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountNewNoteWorksheet extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (account :  Account) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AccountNewNoteWorksheet, {account}, 0)
  }
  
  static function createDestination (account :  Account, noteToEdit :  Note) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AccountNewNoteWorksheet, {account, noteToEdit}, 1)
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (account :  Account) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountNewNoteWorksheet, {account}, 0).goInWorkspace()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (account :  Account, noteToEdit :  Note) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountNewNoteWorksheet, {account, noteToEdit}, 1).goInWorkspace()
  }
  
  static function push (account :  Account) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountNewNoteWorksheet, {account}, 0).push()
  }
  
  static function push (account :  Account, noteToEdit :  Note) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountNewNoteWorksheet, {account, noteToEdit}, 1).push()
  }
  
  
}