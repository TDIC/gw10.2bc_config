package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ContactSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ContactSearchDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($searchCriteria :  ContactSearchCriteria) : void {
    __widgetOf(this, pcf.ContactSearchDV, SECTION_WIDGET_CLASS).setVariables(false, {$searchCriteria})
  }
  
  function refreshVariables ($searchCriteria :  ContactSearchCriteria) : void {
    __widgetOf(this, pcf.ContactSearchDV, SECTION_WIDGET_CLASS).setVariables(true, {$searchCriteria})
  }
  
  
}