package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducerMenuActions extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($producer :  Producer) : void {
    __widgetOf(this, pcf.ProducerMenuActions, SECTION_WIDGET_CLASS).setVariables(false, {$producer})
  }
  
  function refreshVariables ($producer :  Producer) : void {
    __widgetOf(this, pcf.ProducerMenuActions, SECTION_WIDGET_CLASS).setVariables(true, {$producer})
  }
  
  
}