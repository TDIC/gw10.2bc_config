package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/ProducersWithExceptionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducersWithExceptionsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($producers :  gw.api.web.invoice.ProducerWithExceptionsView[]) : void {
    __widgetOf(this, pcf.ProducersWithExceptionsLV, SECTION_WIDGET_CLASS).setVariables(false, {$producers})
  }
  
  function refreshVariables ($producers :  gw.api.web.invoice.ProducerWithExceptionsView[]) : void {
    __widgetOf(this, pcf.ProducersWithExceptionsLV, SECTION_WIDGET_CLASS).setVariables(true, {$producers})
  }
  
  
}