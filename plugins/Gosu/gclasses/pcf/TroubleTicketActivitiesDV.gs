package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketActivitiesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TroubleTicketActivitiesDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($troubleTicket :  TroubleTicket) : void {
    __widgetOf(this, pcf.TroubleTicketActivitiesDV, SECTION_WIDGET_CLASS).setVariables(false, {$troubleTicket})
  }
  
  function refreshVariables ($troubleTicket :  TroubleTicket) : void {
    __widgetOf(this, pcf.TroubleTicketActivitiesDV, SECTION_WIDGET_CLASS).setVariables(true, {$troubleTicket})
  }
  
  
}