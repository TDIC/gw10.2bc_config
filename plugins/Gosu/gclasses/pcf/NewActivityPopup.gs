package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/NewActivityPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewActivityPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (activityPattern :  ActivityPattern, isShared :  Boolean, troubleTicket :  TroubleTicket, account :  Account, policyPeriod :  PolicyPeriod) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewActivityPopup, {activityPattern, isShared, troubleTicket, account, policyPeriod}, 0)
  }
  
  static function push (activityPattern :  ActivityPattern, isShared :  Boolean, troubleTicket :  TroubleTicket, account :  Account, policyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewActivityPopup, {activityPattern, isShared, troubleTicket, account, policyPeriod}, 0).push()
  }
  
  
}