package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanEventsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyPlanEventsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($dlnqPlanReason :  DelinquencyPlanReason) : void {
    __widgetOf(this, pcf.DelinquencyPlanEventsLV, SECTION_WIDGET_CLASS).setVariables(false, {$dlnqPlanReason})
  }
  
  function refreshVariables ($dlnqPlanReason :  DelinquencyPlanReason) : void {
    __widgetOf(this, pcf.DelinquencyPlanEventsLV, SECTION_WIDGET_CLASS).setVariables(true, {$dlnqPlanReason})
  }
  
  
}