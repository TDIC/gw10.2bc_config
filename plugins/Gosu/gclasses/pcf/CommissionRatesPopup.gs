package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/accounting/CommissionRatesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CommissionRatesPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (charges :  Charge[]) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.CommissionRatesPopup, {charges}, 0)
  }
  
  static function push (charges :  Charge[]) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CommissionRatesPopup, {charges}, 0).push()
  }
  
  
}