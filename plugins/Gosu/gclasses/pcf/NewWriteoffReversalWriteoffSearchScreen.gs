package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalWriteoffSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewWriteoffReversalWriteoffSearchScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($reversal :  WriteoffReversal, $account :  Account) : void {
    __widgetOf(this, pcf.NewWriteoffReversalWriteoffSearchScreen, SECTION_WIDGET_CLASS).setVariables(false, {$reversal, $account})
  }
  
  function refreshVariables ($reversal :  WriteoffReversal, $account :  Account) : void {
    __widgetOf(this, pcf.NewWriteoffReversalWriteoffSearchScreen, SECTION_WIDGET_CLASS).setVariables(true, {$reversal, $account})
  }
  
  
}