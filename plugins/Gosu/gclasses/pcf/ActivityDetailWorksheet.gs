package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/ActivityDetailWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ActivityDetailWorksheet extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (activity :  Activity) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ActivityDetailWorksheet, {activity}, 0)
  }
  
  static function createDestination (activity :  Activity, fromApprovalReject :  Boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ActivityDetailWorksheet, {activity, fromApprovalReject}, 1)
  }
  
  static function createDestination (activity :  Activity, fromApprovalReject :  Boolean, editMode :  Boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ActivityDetailWorksheet, {activity, fromApprovalReject, editMode}, 2)
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (activity :  Activity) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ActivityDetailWorksheet, {activity}, 0).goInWorkspace()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (activity :  Activity, fromApprovalReject :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ActivityDetailWorksheet, {activity, fromApprovalReject}, 1).goInWorkspace()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (activity :  Activity, fromApprovalReject :  Boolean, editMode :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ActivityDetailWorksheet, {activity, fromApprovalReject, editMode}, 2).goInWorkspace()
  }
  
  static function push (activity :  Activity) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ActivityDetailWorksheet, {activity}, 0).push()
  }
  
  static function push (activity :  Activity, fromApprovalReject :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ActivityDetailWorksheet, {activity, fromApprovalReject}, 1).push()
  }
  
  static function push (activity :  Activity, fromApprovalReject :  Boolean, editMode :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ActivityDetailWorksheet, {activity, fromApprovalReject, editMode}, 2).push()
  }
  
  
}