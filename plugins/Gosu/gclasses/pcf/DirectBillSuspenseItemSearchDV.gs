package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DirectBillSuspenseItemSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DirectBillSuspenseItemSearchDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($searchCriteria :  gw.search.DirectSuspPmntItemSearchCriteria) : void {
    __widgetOf(this, pcf.DirectBillSuspenseItemSearchDV, SECTION_WIDGET_CLASS).setVariables(false, {$searchCriteria})
  }
  
  function refreshVariables ($searchCriteria :  gw.search.DirectSuspPmntItemSearchCriteria) : void {
    __widgetOf(this, pcf.DirectBillSuspenseItemSearchDV, SECTION_WIDGET_CLASS).setVariables(true, {$searchCriteria})
  }
  
  
}