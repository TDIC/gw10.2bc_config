package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/CommissionPlanSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CommissionPlanSearchScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($tier :  ProducerTier, $isClearBundle :  boolean) : void {
    __widgetOf(this, pcf.CommissionPlanSearchScreen, SECTION_WIDGET_CLASS).setVariables(false, {$tier, $isClearBundle})
  }
  
  function refreshVariables ($tier :  ProducerTier, $isClearBundle :  boolean) : void {
    __widgetOf(this, pcf.CommissionPlanSearchScreen, SECTION_WIDGET_CLASS).setVariables(true, {$tier, $isClearBundle})
  }
  
  
}