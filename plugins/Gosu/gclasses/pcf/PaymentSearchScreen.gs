package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/PaymentSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PaymentSearchScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter () : void {
    __widgetOf(this, pcf.PaymentSearchScreen, SECTION_WIDGET_CLASS).setVariables(false, {})
  }
  
  function refreshVariables () : void {
    __widgetOf(this, pcf.PaymentSearchScreen, SECTION_WIDGET_CLASS).setVariables(true, {})
  }
  
  
}