package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/CloneDelinquencyPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CloneDelinquencyPlan extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (delinquencyPlan :  DelinquencyPlan, includeSpecs :  boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.CloneDelinquencyPlan, {delinquencyPlan, includeSpecs}, 0)
  }
  
  static function drilldown (delinquencyPlan :  DelinquencyPlan, includeSpecs :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CloneDelinquencyPlan, {delinquencyPlan, includeSpecs}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (delinquencyPlan :  DelinquencyPlan, includeSpecs :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CloneDelinquencyPlan, {delinquencyPlan, includeSpecs}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (delinquencyPlan :  DelinquencyPlan, includeSpecs :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CloneDelinquencyPlan, {delinquencyPlan, includeSpecs}, 0).goInMain()
  }
  
  static function printPage (delinquencyPlan :  DelinquencyPlan, includeSpecs :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CloneDelinquencyPlan, {delinquencyPlan, includeSpecs}, 0).printPage()
  }
  
  static function push (delinquencyPlan :  DelinquencyPlan, includeSpecs :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CloneDelinquencyPlan, {delinquencyPlan, includeSpecs}, 0).push()
  }
  
  
}