package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessActivitiesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyProcessActivitiesLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($DelinquencyProcess :  DelinquencyProcess) : void {
    __widgetOf(this, pcf.DelinquencyProcessActivitiesLV, SECTION_WIDGET_CLASS).setVariables(false, {$DelinquencyProcess})
  }
  
  function refreshVariables ($DelinquencyProcess :  DelinquencyProcess) : void {
    __widgetOf(this, pcf.DelinquencyProcessActivitiesLV, SECTION_WIDGET_CLASS).setVariables(true, {$DelinquencyProcess})
  }
  
  
}