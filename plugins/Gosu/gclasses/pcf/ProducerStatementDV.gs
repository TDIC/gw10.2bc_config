package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerStatementDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducerStatementDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($producerStatement :  ProducerStatement) : void {
    __widgetOf(this, pcf.ProducerStatementDV, SECTION_WIDGET_CLASS).setVariables(false, {$producerStatement})
  }
  
  function refreshVariables ($producerStatement :  ProducerStatement) : void {
    __widgetOf(this, pcf.ProducerStatementDV, SECTION_WIDGET_CLASS).setVariables(true, {$producerStatement})
  }
  
  
}