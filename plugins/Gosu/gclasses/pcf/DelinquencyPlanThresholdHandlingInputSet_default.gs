package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanThresholdHandlingInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyPlanThresholdHandlingInputSet_default extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($delinquencyPlan :  DelinquencyPlan, $planNotInUse :  boolean) : void {
    __widgetOf(this, pcf.DelinquencyPlanThresholdHandlingInputSet_default, SECTION_WIDGET_CLASS).setVariables(false, {$delinquencyPlan, $planNotInUse})
  }
  
  function refreshVariables ($delinquencyPlan :  DelinquencyPlan, $planNotInUse :  boolean) : void {
    __widgetOf(this, pcf.DelinquencyPlanThresholdHandlingInputSet_default, SECTION_WIDGET_CLASS).setVariables(true, {$delinquencyPlan, $planNotInUse})
  }
  
  
}