package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultiplePolicyPeriodsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class SelectMultiplePolicyPeriodsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (policySearchCriteria :  gw.search.PolicySearchCriteria) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.SelectMultiplePolicyPeriodsPopup, {policySearchCriteria}, 0)
  }
  
  function pickValueAndCommit (value :  PolicyPeriod[]) : void {
    __widgetOf(this, pcf.SelectMultiplePolicyPeriodsPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (policySearchCriteria :  gw.search.PolicySearchCriteria) : pcf.api.Location {
    return __newDestinationForLocation(pcf.SelectMultiplePolicyPeriodsPopup, {policySearchCriteria}, 0).push()
  }
  
  
}