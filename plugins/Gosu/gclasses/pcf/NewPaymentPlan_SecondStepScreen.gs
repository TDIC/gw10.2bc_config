package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/payment/NewPaymentPlan_SecondStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewPaymentPlan_SecondStepScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($paymentPlanHelper :  gw.admin.paymentplan.PaymentPlanViewHelper) : void {
    __widgetOf(this, pcf.NewPaymentPlan_SecondStepScreen, SECTION_WIDGET_CLASS).setVariables(false, {$paymentPlanHelper})
  }
  
  function refreshVariables ($paymentPlanHelper :  gw.admin.paymentplan.PaymentPlanViewHelper) : void {
    __widgetOf(this, pcf.NewPaymentPlan_SecondStepScreen, SECTION_WIDGET_CLASS).setVariables(true, {$paymentPlanHelper})
  }
  
  
}