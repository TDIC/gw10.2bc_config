package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailContacts.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountDetailContacts extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (account :  Account) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AccountDetailContacts, {account}, 0)
  }
  
  static function createDestination (account :  Account, contactToSelectOnEnter :  AccountContact) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AccountDetailContacts, {account, contactToSelectOnEnter}, 1)
  }
  
  static function drilldown (account :  Account) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailContacts, {account}, 0).drilldown()
  }
  
  static function drilldown (account :  Account, contactToSelectOnEnter :  AccountContact) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailContacts, {account, contactToSelectOnEnter}, 1).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (account :  Account) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailContacts, {account}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (account :  Account, contactToSelectOnEnter :  AccountContact) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailContacts, {account, contactToSelectOnEnter}, 1).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (account :  Account) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailContacts, {account}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (account :  Account, contactToSelectOnEnter :  AccountContact) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailContacts, {account, contactToSelectOnEnter}, 1).goInMain()
  }
  
  static function printPage (account :  Account) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailContacts, {account}, 0).printPage()
  }
  
  static function printPage (account :  Account, contactToSelectOnEnter :  AccountContact) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailContacts, {account, contactToSelectOnEnter}, 1).printPage()
  }
  
  static function push (account :  Account) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailContacts, {account}, 0).push()
  }
  
  static function push (account :  Account, contactToSelectOnEnter :  AccountContact) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailContacts, {account, contactToSelectOnEnter}, 1).push()
  }
  
  
}