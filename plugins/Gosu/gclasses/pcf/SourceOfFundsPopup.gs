package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/SourceOfFundsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class SourceOfFundsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (fundsSourceTracker :  FundsSourceTracker) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.SourceOfFundsPopup, {fundsSourceTracker}, 0)
  }
  
  static function push (fundsSourceTracker :  FundsSourceTracker) : pcf.api.Location {
    return __newDestinationForLocation(pcf.SourceOfFundsPopup, {fundsSourceTracker}, 0).push()
  }
  
  
}