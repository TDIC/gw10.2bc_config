package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyPlanDetailDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($delinquencyPlan :  DelinquencyPlan) : void {
    __widgetOf(this, pcf.DelinquencyPlanDetailDV, SECTION_WIDGET_CLASS).setVariables(false, {$delinquencyPlan})
  }
  
  function refreshVariables ($delinquencyPlan :  DelinquencyPlan) : void {
    __widgetOf(this, pcf.DelinquencyPlanDetailDV, SECTION_WIDGET_CLASS).setVariables(true, {$delinquencyPlan})
  }
  
  
}