package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/OverrideCommissionPlanPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class OverrideCommissionPlanPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (PolicyPeriod :  PolicyPeriod) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.OverrideCommissionPlanPopup, {PolicyPeriod}, 0)
  }
  
  static function push (PolicyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.OverrideCommissionPlanPopup, {PolicyPeriod}, 0).push()
  }
  
  
}