package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/AccountInvoiceItemsLV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountInvoiceItemsLV_default extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($invoice :  AccountInvoice, $aggregation :  typekey.AggregationType) : void {
    __widgetOf(this, pcf.AccountInvoiceItemsLV_default, SECTION_WIDGET_CLASS).setVariables(false, {$invoice, $aggregation})
  }
  
  function refreshVariables ($invoice :  AccountInvoice, $aggregation :  typekey.AggregationType) : void {
    __widgetOf(this, pcf.AccountInvoiceItemsLV_default, SECTION_WIDGET_CLASS).setVariables(true, {$invoice, $aggregation})
  }
  
  
}