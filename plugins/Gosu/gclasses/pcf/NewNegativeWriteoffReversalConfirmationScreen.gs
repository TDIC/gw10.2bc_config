package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffReversalConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewNegativeWriteoffReversalConfirmationScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($negativeWriteoffToReverse :  NegativeWriteoffRev) : void {
    __widgetOf(this, pcf.NewNegativeWriteoffReversalConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(false, {$negativeWriteoffToReverse})
  }
  
  function refreshVariables ($negativeWriteoffToReverse :  NegativeWriteoffRev) : void {
    __widgetOf(this, pcf.NewNegativeWriteoffReversalConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(true, {$negativeWriteoffToReverse})
  }
  
  
}