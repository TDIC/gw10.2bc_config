package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducersMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducersMenuActions extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter () : void {
    __widgetOf(this, pcf.ProducersMenuActions, SECTION_WIDGET_CLASS).setVariables(false, {})
  }
  
  function refreshVariables () : void {
    __widgetOf(this, pcf.ProducersMenuActions, SECTION_WIDGET_CLASS).setVariables(true, {})
  }
  
  
}