package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ChargeSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ChargeSearchDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($chargeSearch :  gw.search.ReversibleChargeSearchCriteria, $accountEditable :  Boolean) : void {
    __widgetOf(this, pcf.ChargeSearchDV, SECTION_WIDGET_CLASS).setVariables(false, {$chargeSearch, $accountEditable})
  }
  
  function refreshVariables ($chargeSearch :  gw.search.ReversibleChargeSearchCriteria, $accountEditable :  Boolean) : void {
    __widgetOf(this, pcf.ChargeSearchDV, SECTION_WIDGET_CLASS).setVariables(true, {$chargeSearch, $accountEditable})
  }
  
  
}