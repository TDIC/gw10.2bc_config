package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/VoidDisbursementPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class VoidDisbursementPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (disbursement :  Disbursement) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.VoidDisbursementPopup, {disbursement}, 0)
  }
  
  function pickValueAndCommit (value :  Disbursement) : void {
    __widgetOf(this, pcf.VoidDisbursementPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (disbursement :  Disbursement) : pcf.api.Location {
    return __newDestinationForLocation(pcf.VoidDisbursementPopup, {disbursement}, 0).push()
  }
  
  
}