package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExceptionsConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyBillExceptionsConfirmationPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (exceptionItemViews :  gw.api.web.invoice.ExceptionItemView[], writeOffType :  AgencyWriteoffType) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyBillExceptionsConfirmationPopup, {exceptionItemViews, writeOffType}, 0)
  }
  
  static function push (exceptionItemViews :  gw.api.web.invoice.ExceptionItemView[], writeOffType :  AgencyWriteoffType) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExceptionsConfirmationPopup, {exceptionItemViews, writeOffType}, 0).push()
  }
  
  
}