package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/NewAccountContactPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewAccountContactPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (account :  Account, contactSubtype :  Type<Contact>) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewAccountContactPopup, {account, contactSubtype}, 0)
  }
  
  function pickValueAndCommit (value :  AccountContact) : void {
    __widgetOf(this, pcf.NewAccountContactPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (account :  Account, contactSubtype :  Type<Contact>) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewAccountContactPopup, {account, contactSubtype}, 0).push()
  }
  
  
}