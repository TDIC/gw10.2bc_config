package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopDisbursementsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DesktopDisbursementsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($disbursements :  gw.api.database.IQueryBeanResult<Disbursement>) : void {
    __widgetOf(this, pcf.DesktopDisbursementsLV, SECTION_WIDGET_CLASS).setVariables(false, {$disbursements})
  }
  
  function refreshVariables ($disbursements :  gw.api.database.IQueryBeanResult<Disbursement>) : void {
    __widgetOf(this, pcf.DesktopDisbursementsLV, SECTION_WIDGET_CLASS).setVariables(true, {$disbursements})
  }
  
  
}