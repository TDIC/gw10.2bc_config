package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AgencySuspenseItemsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencySuspenseItemsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (distribution :  AgencyCycleDist, editing :  boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencySuspenseItemsPopup, {distribution, editing}, 0)
  }
  
  static function push (distribution :  AgencyCycleDist, editing :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencySuspenseItemsPopup, {distribution, editing}, 0).push()
  }
  
  
}