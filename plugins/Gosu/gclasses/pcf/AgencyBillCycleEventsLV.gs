package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillCycleEventsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyBillCycleEventsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($statementInvoice :  StatementInvoice) : void {
    __widgetOf(this, pcf.AgencyBillCycleEventsLV, SECTION_WIDGET_CLASS).setVariables(false, {$statementInvoice})
  }
  
  function refreshVariables ($statementInvoice :  StatementInvoice) : void {
    __widgetOf(this, pcf.AgencyBillCycleEventsLV, SECTION_WIDGET_CLASS).setVariables(true, {$statementInvoice})
  }
  
  
}