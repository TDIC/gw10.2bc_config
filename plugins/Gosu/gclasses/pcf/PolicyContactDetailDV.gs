package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyContactDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyContactDetailDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyPeriodContact :  PolicyPeriodContact, $isNew :  Boolean) : void {
    __widgetOf(this, pcf.PolicyContactDetailDV, SECTION_WIDGET_CLASS).setVariables(false, {$policyPeriodContact, $isNew})
  }
  
  function refreshVariables ($policyPeriodContact :  PolicyPeriodContact, $isNew :  Boolean) : void {
    __widgetOf(this, pcf.PolicyContactDetailDV, SECTION_WIDGET_CLASS).setVariables(true, {$policyPeriodContact, $isNew})
  }
  
  
}