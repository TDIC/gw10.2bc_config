package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/AccountNewLOCPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountNewLOCPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (collateral :  Collateral) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AccountNewLOCPopup, {collateral}, 0)
  }
  
  static function push (collateral :  Collateral) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountNewLOCPopup, {collateral}, 0).push()
  }
  
  
}