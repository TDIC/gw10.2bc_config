package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/NewPaymentRequestAccountSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewPaymentRequestAccountSearchScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($tAccountOwnerReference :  gw.api.web.accounting.TAccountOwnerReference) : void {
    __widgetOf(this, pcf.NewPaymentRequestAccountSearchScreen, SECTION_WIDGET_CLASS).setVariables(false, {$tAccountOwnerReference})
  }
  
  function refreshVariables ($tAccountOwnerReference :  gw.api.web.accounting.TAccountOwnerReference) : void {
    __widgetOf(this, pcf.NewPaymentRequestAccountSearchScreen, SECTION_WIDGET_CLASS).setVariables(true, {$tAccountOwnerReference})
  }
  
  
}