package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewWriteoffWizard extends com.guidewire.pl.web.codegen.WizardBase {
  static function createDestination () : pcf.api.Destination {
    return __newDestinationForWizard(pcf.NewWriteoffWizard, {}, 0)
  }
  
  static function createDestination (passedInTarget :  entity.TAccountOwner) : pcf.api.Destination {
    return __newDestinationForWizard(pcf.NewWriteoffWizard, {passedInTarget}, 1)
  }
  
  static function drilldown () : pcf.api.Location {
    return __newDestinationForWizard(pcf.NewWriteoffWizard, {}, 0).drilldown()
  }
  
  static function drilldown (passedInTarget :  entity.TAccountOwner) : pcf.api.Location {
    return __newDestinationForWizard(pcf.NewWriteoffWizard, {passedInTarget}, 1).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go () : pcf.api.Location {
    return __newDestinationForWizard(pcf.NewWriteoffWizard, {}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (passedInTarget :  entity.TAccountOwner) : pcf.api.Location {
    return __newDestinationForWizard(pcf.NewWriteoffWizard, {passedInTarget}, 1).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain () : pcf.api.Location {
    return __newDestinationForWizard(pcf.NewWriteoffWizard, {}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (passedInTarget :  entity.TAccountOwner) : pcf.api.Location {
    return __newDestinationForWizard(pcf.NewWriteoffWizard, {passedInTarget}, 1).goInMain()
  }
  
  static function printPage () : pcf.api.Location {
    return __newDestinationForWizard(pcf.NewWriteoffWizard, {}, 0).printPage()
  }
  
  static function printPage (passedInTarget :  entity.TAccountOwner) : pcf.api.Location {
    return __newDestinationForWizard(pcf.NewWriteoffWizard, {passedInTarget}, 1).printPage()
  }
  
  static function push () : pcf.api.Location {
    return __newDestinationForWizard(pcf.NewWriteoffWizard, {}, 0).push()
  }
  
  static function push (passedInTarget :  entity.TAccountOwner) : pcf.api.Location {
    return __newDestinationForWizard(pcf.NewWriteoffWizard, {passedInTarget}, 1).push()
  }
  
  
}