package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyPoliciesScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TransferPolicyPoliciesScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyTransfer :  PolTransferByProdCode) : void {
    __widgetOf(this, pcf.TransferPolicyPoliciesScreen, SECTION_WIDGET_CLASS).setVariables(false, {$policyTransfer})
  }
  
  function refreshVariables ($policyTransfer :  PolTransferByProdCode) : void {
    __widgetOf(this, pcf.TransferPolicyPoliciesScreen, SECTION_WIDGET_CLASS).setVariables(true, {$policyTransfer})
  }
  
  
}