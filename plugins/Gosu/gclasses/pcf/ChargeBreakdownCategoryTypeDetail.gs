package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/chargepatterns/ChargeBreakdownCategoryTypeDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ChargeBreakdownCategoryTypeDetail extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (categoryType :  ChargeBreakdownCategoryType) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ChargeBreakdownCategoryTypeDetail, {categoryType}, 0)
  }
  
  static function drilldown (categoryType :  ChargeBreakdownCategoryType) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChargeBreakdownCategoryTypeDetail, {categoryType}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (categoryType :  ChargeBreakdownCategoryType) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChargeBreakdownCategoryTypeDetail, {categoryType}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (categoryType :  ChargeBreakdownCategoryType) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChargeBreakdownCategoryTypeDetail, {categoryType}, 0).goInMain()
  }
  
  static function printPage (categoryType :  ChargeBreakdownCategoryType) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChargeBreakdownCategoryTypeDetail, {categoryType}, 0).printPage()
  }
  
  static function push (categoryType :  ChargeBreakdownCategoryType) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChargeBreakdownCategoryTypeDetail, {categoryType}, 0).push()
  }
  
  
}