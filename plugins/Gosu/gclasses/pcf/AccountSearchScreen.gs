package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/AccountSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountSearchScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($showHyperlinks :  Boolean, $isClearBundle :  Boolean) : void {
    __widgetOf(this, pcf.AccountSearchScreen, SECTION_WIDGET_CLASS).setVariables(false, {$showHyperlinks, $isClearBundle})
  }
  
  function refreshVariables ($showHyperlinks :  Boolean, $isClearBundle :  Boolean) : void {
    __widgetOf(this, pcf.AccountSearchScreen, SECTION_WIDGET_CLASS).setVariables(true, {$showHyperlinks, $isClearBundle})
  }
  
  
}