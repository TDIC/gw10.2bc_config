package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/AccountView360Screen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountView360Screen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($account :  Account, $policyToDisplay :  Policy) : void {
    __widgetOf(this, pcf.AccountView360Screen, SECTION_WIDGET_CLASS).setVariables(false, {$account, $policyToDisplay})
  }
  
  function refreshVariables ($account :  Account, $policyToDisplay :  Policy) : void {
    __widgetOf(this, pcf.AccountView360Screen, SECTION_WIDGET_CLASS).setVariables(true, {$account, $policyToDisplay})
  }
  
  
}