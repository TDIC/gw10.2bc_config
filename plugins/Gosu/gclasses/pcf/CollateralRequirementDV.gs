package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralRequirementDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CollateralRequirementDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($requirement :  CollateralRequirement, $effectiveDateEditable :  Boolean) : void {
    __widgetOf(this, pcf.CollateralRequirementDV, SECTION_WIDGET_CLASS).setVariables(false, {$requirement, $effectiveDateEditable})
  }
  
  function refreshVariables ($requirement :  CollateralRequirement, $effectiveDateEditable :  Boolean) : void {
    __widgetOf(this, pcf.CollateralRequirementDV, SECTION_WIDGET_CLASS).setVariables(true, {$requirement, $effectiveDateEditable})
  }
  
  
}