package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicySummaryScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicySummaryScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($plcyPeriod :  PolicyPeriod) : void {
    __widgetOf(this, pcf.PolicySummaryScreen, SECTION_WIDGET_CLASS).setVariables(false, {$plcyPeriod})
  }
  
  function refreshVariables ($plcyPeriod :  PolicyPeriod) : void {
    __widgetOf(this, pcf.PolicySummaryScreen, SECTION_WIDGET_CLASS).setVariables(true, {$plcyPeriod})
  }
  
  
}