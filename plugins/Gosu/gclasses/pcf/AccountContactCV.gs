package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountContactCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountContactCV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($contact :  AccountContact, $isNew :  Boolean) : void {
    __widgetOf(this, pcf.AccountContactCV, SECTION_WIDGET_CLASS).setVariables(false, {$contact, $isNew})
  }
  
  function refreshVariables ($contact :  AccountContact, $isNew :  Boolean) : void {
    __widgetOf(this, pcf.AccountContactCV, SECTION_WIDGET_CLASS).setVariables(true, {$contact, $isNew})
  }
  
  
}