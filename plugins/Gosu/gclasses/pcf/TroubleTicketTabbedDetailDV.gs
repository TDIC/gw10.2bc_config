package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketTabbedDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TroubleTicketTabbedDetailDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($troubleTicket :  TroubleTicket, $CreateTroubleTicketHelper :  CreateTroubleTicketHelper) : void {
    __widgetOf(this, pcf.TroubleTicketTabbedDetailDV, SECTION_WIDGET_CLASS).setVariables(false, {$troubleTicket, $CreateTroubleTicketHelper})
  }
  
  function refreshVariables ($troubleTicket :  TroubleTicket, $CreateTroubleTicketHelper :  CreateTroubleTicketHelper) : void {
    __widgetOf(this, pcf.TroubleTicketTabbedDetailDV, SECTION_WIDGET_CLASS).setVariables(true, {$troubleTicket, $CreateTroubleTicketHelper})
  }
  
  
}