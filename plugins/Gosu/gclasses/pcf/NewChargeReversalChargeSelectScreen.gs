package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalChargeSelectScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewChargeReversalChargeSelectScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($Reversal :  ChargeReversal, $SearchCriteria :  gw.search.ReversibleChargeSearchCriteria) : void {
    __widgetOf(this, pcf.NewChargeReversalChargeSelectScreen, SECTION_WIDGET_CLASS).setVariables(false, {$Reversal, $SearchCriteria})
  }
  
  function refreshVariables ($Reversal :  ChargeReversal, $SearchCriteria :  gw.search.ReversibleChargeSearchCriteria) : void {
    __widgetOf(this, pcf.NewChargeReversalChargeSelectScreen, SECTION_WIDGET_CLASS).setVariables(true, {$Reversal, $SearchCriteria})
  }
  
  
}