package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerDetailGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducerDetailGroup extends com.guidewire.pl.web.codegen.LocationGroupBase {
  static function createDestination (producer :  Producer) : pcf.api.Destination {
    return __newDestinationForLocationGroup(pcf.ProducerDetailGroup, {producer}, 0)
  }
  
  static function drilldown (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocationGroup(pcf.ProducerDetailGroup, {producer}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocationGroup(pcf.ProducerDetailGroup, {producer}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocationGroup(pcf.ProducerDetailGroup, {producer}, 0).goInMain()
  }
  
  static function printPage (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocationGroup(pcf.ProducerDetailGroup, {producer}, 0).printPage()
  }
  
  static function push (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocationGroup(pcf.ProducerDetailGroup, {producer}, 0).push()
  }
  
  
}