package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/RecaptureDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class RecaptureDetailsScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($helper :  gw.accounting.NewRecaptureChargeHelper, $account :  Account) : void {
    __widgetOf(this, pcf.RecaptureDetailsScreen, SECTION_WIDGET_CLASS).setVariables(false, {$helper, $account})
  }
  
  function refreshVariables ($helper :  gw.accounting.NewRecaptureChargeHelper, $account :  Account) : void {
    __widgetOf(this, pcf.RecaptureDetailsScreen, SECTION_WIDGET_CLASS).setVariables(true, {$helper, $account})
  }
  
  
}