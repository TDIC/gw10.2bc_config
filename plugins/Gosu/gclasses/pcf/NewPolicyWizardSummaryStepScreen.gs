package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/NewPolicyWizardSummaryStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewPolicyWizardSummaryStepScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($account :  Account, $policyPeriod :  PolicyPeriod, $producerCodeRoleEntries :  ProducerCodeRoleEntry[], $invoicingOverridesView :  gw.invoice.InvoicingOverridesView, $issuance :  Issuance) : void {
    __widgetOf(this, pcf.NewPolicyWizardSummaryStepScreen, SECTION_WIDGET_CLASS).setVariables(false, {$account, $policyPeriod, $producerCodeRoleEntries, $invoicingOverridesView, $issuance})
  }
  
  function refreshVariables ($account :  Account, $policyPeriod :  PolicyPeriod, $producerCodeRoleEntries :  ProducerCodeRoleEntry[], $invoicingOverridesView :  gw.invoice.InvoicingOverridesView, $issuance :  Issuance) : void {
    __widgetOf(this, pcf.NewPolicyWizardSummaryStepScreen, SECTION_WIDGET_CLASS).setVariables(true, {$account, $policyPeriod, $producerCodeRoleEntries, $invoicingOverridesView, $issuance})
  }
  
  
}