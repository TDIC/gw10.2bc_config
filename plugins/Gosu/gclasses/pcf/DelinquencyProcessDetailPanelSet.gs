package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessDetailPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyProcessDetailPanelSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($delinquencyProcess :  DelinquencyProcess) : void {
    __widgetOf(this, pcf.DelinquencyProcessDetailPanelSet, SECTION_WIDGET_CLASS).setVariables(false, {$delinquencyProcess})
  }
  
  function refreshVariables ($delinquencyProcess :  DelinquencyProcess) : void {
    __widgetOf(this, pcf.DelinquencyProcessDetailPanelSet, SECTION_WIDGET_CLASS).setVariables(true, {$delinquencyProcess})
  }
  
  
}