package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/NewInvoice.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewInvoice extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (account :  Account, selectedInvoiceStream :  InvoiceStream) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewInvoice, {account, selectedInvoiceStream}, 0)
  }
  
  static function drilldown (account :  Account, selectedInvoiceStream :  InvoiceStream) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewInvoice, {account, selectedInvoiceStream}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (account :  Account, selectedInvoiceStream :  InvoiceStream) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewInvoice, {account, selectedInvoiceStream}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (account :  Account, selectedInvoiceStream :  InvoiceStream) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewInvoice, {account, selectedInvoiceStream}, 0).goInMain()
  }
  
  static function printPage (account :  Account, selectedInvoiceStream :  InvoiceStream) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewInvoice, {account, selectedInvoiceStream}, 0).printPage()
  }
  
  static function push (account :  Account, selectedInvoiceStream :  InvoiceStream) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewInvoice, {account, selectedInvoiceStream}, 0).push()
  }
  
  
}