package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/InvoiceSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class InvoiceSearchScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($isClearBundle :  boolean, $showHyperlinks :  boolean, $account :  Account) : void {
    __widgetOf(this, pcf.InvoiceSearchScreen, SECTION_WIDGET_CLASS).setVariables(false, {$isClearBundle, $showHyperlinks, $account})
  }
  
  function refreshVariables ($isClearBundle :  boolean, $showHyperlinks :  boolean, $account :  Account) : void {
    __widgetOf(this, pcf.InvoiceSearchScreen, SECTION_WIDGET_CLASS).setVariables(true, {$isClearBundle, $showHyperlinks, $account})
  }
  
  
}