package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/chargepatterns/ChargePatternDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ChargePatternDetail extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (chargePattern :  ChargePattern) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ChargePatternDetail, {chargePattern}, 0)
  }
  
  static function drilldown (chargePattern :  ChargePattern) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChargePatternDetail, {chargePattern}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (chargePattern :  ChargePattern) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChargePatternDetail, {chargePattern}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (chargePattern :  ChargePattern) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChargePatternDetail, {chargePattern}, 0).goInMain()
  }
  
  static function printPage (chargePattern :  ChargePattern) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChargePatternDetail, {chargePattern}, 0).printPage()
  }
  
  static function push (chargePattern :  ChargePattern) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChargePatternDetail, {chargePattern}, 0).push()
  }
  
  
}