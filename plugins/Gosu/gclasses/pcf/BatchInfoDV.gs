package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/BatchInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class BatchInfoDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($batchPaymentDetailsView :  gw.web.payment.batch.BatchPaymentDetailsView) : void {
    __widgetOf(this, pcf.BatchInfoDV, SECTION_WIDGET_CLASS).setVariables(false, {$batchPaymentDetailsView})
  }
  
  function refreshVariables ($batchPaymentDetailsView :  gw.web.payment.batch.BatchPaymentDetailsView) : void {
    __widgetOf(this, pcf.BatchInfoDV, SECTION_WIDGET_CLASS).setVariables(true, {$batchPaymentDetailsView})
  }
  
  
}