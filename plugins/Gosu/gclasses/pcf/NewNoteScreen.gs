package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/note/NewNoteScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewNoteScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($srcBean :  KeyableBean, $Note :  Note) : void {
    __widgetOf(this, pcf.NewNoteScreen, SECTION_WIDGET_CLASS).setVariables(false, {$srcBean, $Note})
  }
  
  function refreshVariables ($srcBean :  KeyableBean, $Note :  Note) : void {
    __widgetOf(this, pcf.NewNoteScreen, SECTION_WIDGET_CLASS).setVariables(true, {$srcBean, $Note})
  }
  
  
}