package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AssignInvoiceItemsToNewProducerPayerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AssignInvoiceItemsToNewProducerPayerPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (invoiceItems :  InvoiceItem[], chargesToAssign :  Charge[], currentAccount :  Account) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AssignInvoiceItemsToNewProducerPayerPopup, {invoiceItems, chargesToAssign, currentAccount}, 0)
  }
  
  static function push (invoiceItems :  InvoiceItem[], chargesToAssign :  Charge[], currentAccount :  Account) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AssignInvoiceItemsToNewProducerPayerPopup, {invoiceItems, chargesToAssign, currentAccount}, 0).push()
  }
  
  
}