package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyInfoBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyInfoBar extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyPeriod :  PolicyPeriod) : void {
    __widgetOf(this, pcf.PolicyInfoBar, SECTION_WIDGET_CLASS).setVariables(false, {$policyPeriod})
  }
  
  function refreshVariables ($policyPeriod :  PolicyPeriod) : void {
    __widgetOf(this, pcf.PolicyInfoBar, SECTION_WIDGET_CLASS).setVariables(true, {$policyPeriod})
  }
  
  
}