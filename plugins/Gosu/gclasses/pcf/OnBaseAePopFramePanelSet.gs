package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseAePopFramePanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class OnBaseAePopFramePanelSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($scrapeXml :  String[]) : void {
    __widgetOf(this, pcf.OnBaseAePopFramePanelSet, SECTION_WIDGET_CLASS).setVariables(false, {$scrapeXml})
  }
  
  function refreshVariables ($scrapeXml :  String[]) : void {
    __widgetOf(this, pcf.OnBaseAePopFramePanelSet, SECTION_WIDGET_CLASS).setVariables(true, {$scrapeXml})
  }
  
  
}