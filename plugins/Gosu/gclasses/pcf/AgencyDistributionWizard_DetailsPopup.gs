package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_DetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyDistributionWizard_DetailsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (chargeOwnerView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView, availableAmount :  gw.pl.currency.MonetaryAmount, wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyDistributionWizard_DetailsPopup, {chargeOwnerView, availableAmount, wizardState}, 0)
  }
  
  static function push (chargeOwnerView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView, availableAmount :  gw.pl.currency.MonetaryAmount, wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyDistributionWizard_DetailsPopup, {chargeOwnerView, availableAmount, wizardState}, 0).push()
  }
  
  
}