package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/InvoiceItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class InvoiceItemsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($invoiceItems :  InvoiceItem[], $charge :  Charge, $showInstallmentNumber :  Boolean) : void {
    __widgetOf(this, pcf.InvoiceItemsLV, SECTION_WIDGET_CLASS).setVariables(false, {$invoiceItems, $charge, $showInstallmentNumber})
  }
  
  function refreshVariables ($invoiceItems :  InvoiceItem[], $charge :  Charge, $showInstallmentNumber :  Boolean) : void {
    __widgetOf(this, pcf.InvoiceItemsLV, SECTION_WIDGET_CLASS).setVariables(true, {$invoiceItems, $charge, $showInstallmentNumber})
  }
  
  
}