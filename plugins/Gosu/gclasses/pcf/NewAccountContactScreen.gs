package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/NewAccountContactScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewAccountContactScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($contact :  AccountContact, $duplicateContactsPopupNavigator :  gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator) : void {
    __widgetOf(this, pcf.NewAccountContactScreen, SECTION_WIDGET_CLASS).setVariables(false, {$contact, $duplicateContactsPopupNavigator})
  }
  
  function refreshVariables ($contact :  AccountContact, $duplicateContactsPopupNavigator :  gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator) : void {
    __widgetOf(this, pcf.NewAccountContactScreen, SECTION_WIDGET_CLASS).setVariables(true, {$contact, $duplicateContactsPopupNavigator})
  }
  
  
}