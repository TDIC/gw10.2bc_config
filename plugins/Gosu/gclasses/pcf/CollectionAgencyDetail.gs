package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/collection/CollectionAgencyDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CollectionAgencyDetail extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (collectionAgency :  CollectionAgency) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.CollectionAgencyDetail, {collectionAgency}, 0)
  }
  
  static function drilldown (collectionAgency :  CollectionAgency) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CollectionAgencyDetail, {collectionAgency}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (collectionAgency :  CollectionAgency) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CollectionAgencyDetail, {collectionAgency}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (collectionAgency :  CollectionAgency) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CollectionAgencyDetail, {collectionAgency}, 0).goInMain()
  }
  
  static function printPage (collectionAgency :  CollectionAgency) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CollectionAgencyDetail, {collectionAgency}, 0).printPage()
  }
  
  static function push (collectionAgency :  CollectionAgency) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CollectionAgencyDetail, {collectionAgency}, 0).push()
  }
  
  
}