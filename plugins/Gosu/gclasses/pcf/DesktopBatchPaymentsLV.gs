package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopBatchPaymentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DesktopBatchPaymentsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($batchPayments :  gw.api.database.IQueryBeanResult<BatchPayment>) : void {
    __widgetOf(this, pcf.DesktopBatchPaymentsLV, SECTION_WIDGET_CLASS).setVariables(false, {$batchPayments})
  }
  
  function refreshVariables ($batchPayments :  gw.api.database.IQueryBeanResult<BatchPayment>) : void {
    __widgetOf(this, pcf.DesktopBatchPaymentsLV, SECTION_WIDGET_CLASS).setVariables(true, {$batchPayments})
  }
  
  
}