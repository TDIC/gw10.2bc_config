package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountDetailDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($account :  Account, $summaryFinancialsHelper :  gw.web.account.AccountSummaryFinancialsHelper) : void {
    __widgetOf(this, pcf.AccountDetailDV, SECTION_WIDGET_CLASS).setVariables(false, {$account, $summaryFinancialsHelper})
  }
  
  function refreshVariables ($account :  Account, $summaryFinancialsHelper :  gw.web.account.AccountSummaryFinancialsHelper) : void {
    __widgetOf(this, pcf.AccountDetailDV, SECTION_WIDGET_CLASS).setVariables(true, {$account, $summaryFinancialsHelper})
  }
  
  
}