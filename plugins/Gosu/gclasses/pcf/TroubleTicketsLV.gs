package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TroubleTicketsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($troubleTickets :  TroubleTicket[]) : void {
    __widgetOf(this, pcf.TroubleTicketsLV, SECTION_WIDGET_CLASS).setVariables(false, {$troubleTickets})
  }
  
  function refreshVariables ($troubleTickets :  TroubleTicket[]) : void {
    __widgetOf(this, pcf.TroubleTicketsLV, SECTION_WIDGET_CLASS).setVariables(true, {$troubleTickets})
  }
  
  
}