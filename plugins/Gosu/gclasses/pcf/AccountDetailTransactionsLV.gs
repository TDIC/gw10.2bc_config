package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailTransactionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountDetailTransactionsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($transactions :  Transaction[]) : void {
    __widgetOf(this, pcf.AccountDetailTransactionsLV, SECTION_WIDGET_CLASS).setVariables(false, {$transactions})
  }
  
  function refreshVariables ($transactions :  Transaction[]) : void {
    __widgetOf(this, pcf.AccountDetailTransactionsLV, SECTION_WIDGET_CLASS).setVariables(true, {$transactions})
  }
  
  
}