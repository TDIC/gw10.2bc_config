package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/activitypatterns/NewActivityPattern.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewActivityPattern extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (category :  ActivityCategory) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewActivityPattern, {category}, 0)
  }
  
  static function drilldown (category :  ActivityCategory) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewActivityPattern, {category}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (category :  ActivityCategory) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewActivityPattern, {category}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (category :  ActivityCategory) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewActivityPattern, {category}, 0).goInMain()
  }
  
  static function printPage (category :  ActivityCategory) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewActivityPattern, {category}, 0).printPage()
  }
  
  static function push (category :  ActivityCategory) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewActivityPattern, {category}, 0).push()
  }
  
  
}