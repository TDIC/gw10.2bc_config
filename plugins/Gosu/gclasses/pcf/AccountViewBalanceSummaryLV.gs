package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/AccountViewBalanceSummaryLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountViewBalanceSummaryLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($account :  Account, $transactions :  gw.acc.acct360.accountview.Acct360TranDetail[]) : void {
    __widgetOf(this, pcf.AccountViewBalanceSummaryLV, SECTION_WIDGET_CLASS).setVariables(false, {$account, $transactions})
  }
  
  function refreshVariables ($account :  Account, $transactions :  gw.acc.acct360.accountview.Acct360TranDetail[]) : void {
    __widgetOf(this, pcf.AccountViewBalanceSummaryLV, SECTION_WIDGET_CLASS).setVariables(true, {$account, $transactions})
  }
  
  
}