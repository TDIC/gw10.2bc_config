package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanCloneToolbarButtonSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyPlanCloneToolbarButtonSet_default extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($delinquencyPlan :  DelinquencyPlan) : void {
    __widgetOf(this, pcf.DelinquencyPlanCloneToolbarButtonSet_default, SECTION_WIDGET_CLASS).setVariables(false, {$delinquencyPlan})
  }
  
  function refreshVariables ($delinquencyPlan :  DelinquencyPlan) : void {
    __widgetOf(this, pcf.DelinquencyPlanCloneToolbarButtonSet_default, SECTION_WIDGET_CLASS).setVariables(true, {$delinquencyPlan})
  }
  
  
}