package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyConfirmationLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TransferPolicyConfirmationLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyTransfer :  PolicyTransfer) : void {
    __widgetOf(this, pcf.TransferPolicyConfirmationLV, SECTION_WIDGET_CLASS).setVariables(false, {$policyTransfer})
  }
  
  function refreshVariables ($policyTransfer :  PolicyTransfer) : void {
    __widgetOf(this, pcf.TransferPolicyConfirmationLV, SECTION_WIDGET_CLASS).setVariables(true, {$policyTransfer})
  }
  
  
}