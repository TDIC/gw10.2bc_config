package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/ItemEventPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ItemEventPanelSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($invoiceItem :  InvoiceItem) : void {
    __widgetOf(this, pcf.ItemEventPanelSet, SECTION_WIDGET_CLASS).setVariables(false, {$invoiceItem})
  }
  
  function refreshVariables ($invoiceItem :  InvoiceItem) : void {
    __widgetOf(this, pcf.ItemEventPanelSet, SECTION_WIDGET_CLASS).setVariables(true, {$invoiceItem})
  }
  
  
}