package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyProducerScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TransferPolicyProducerScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyTransfer :  PolTransferByProdCode, $producer :  Producer) : void {
    __widgetOf(this, pcf.TransferPolicyProducerScreen, SECTION_WIDGET_CLASS).setVariables(false, {$policyTransfer, $producer})
  }
  
  function refreshVariables ($policyTransfer :  PolTransferByProdCode, $producer :  Producer) : void {
    __widgetOf(this, pcf.TransferPolicyProducerScreen, SECTION_WIDGET_CLASS).setVariables(true, {$policyTransfer, $producer})
  }
  
  
}