package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralManualDrawdownWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CollateralManualDrawdownWizard extends com.guidewire.pl.web.codegen.WizardBase {
  static function createDestination (collateral :  Collateral) : pcf.api.Destination {
    return __newDestinationForWizard(pcf.CollateralManualDrawdownWizard, {collateral}, 0)
  }
  
  static function drilldown (collateral :  Collateral) : pcf.api.Location {
    return __newDestinationForWizard(pcf.CollateralManualDrawdownWizard, {collateral}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (collateral :  Collateral) : pcf.api.Location {
    return __newDestinationForWizard(pcf.CollateralManualDrawdownWizard, {collateral}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (collateral :  Collateral) : pcf.api.Location {
    return __newDestinationForWizard(pcf.CollateralManualDrawdownWizard, {collateral}, 0).goInMain()
  }
  
  static function printPage (collateral :  Collateral) : pcf.api.Location {
    return __newDestinationForWizard(pcf.CollateralManualDrawdownWizard, {collateral}, 0).printPage()
  }
  
  static function push (collateral :  Collateral) : pcf.api.Location {
    return __newDestinationForWizard(pcf.CollateralManualDrawdownWizard, {collateral}, 0).push()
  }
  
  
}