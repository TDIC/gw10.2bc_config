package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/CommissionReductionWizardPolicySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CommissionReductionWizardPolicySearchScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($tAccountOwnerReference :  gw.api.web.accounting.TAccountOwnerReference) : void {
    __widgetOf(this, pcf.CommissionReductionWizardPolicySearchScreen, SECTION_WIDGET_CLASS).setVariables(false, {$tAccountOwnerReference})
  }
  
  function refreshVariables ($tAccountOwnerReference :  gw.api.web.accounting.TAccountOwnerReference) : void {
    __widgetOf(this, pcf.CommissionReductionWizardPolicySearchScreen, SECTION_WIDGET_CLASS).setVariables(true, {$tAccountOwnerReference})
  }
  
  
}