package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/securityzones/SecurityZoneDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class SecurityZoneDetail extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (SecurityZone :  SecurityZone) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.SecurityZoneDetail, {SecurityZone}, 0)
  }
  
  static function drilldown (SecurityZone :  SecurityZone) : pcf.api.Location {
    return __newDestinationForLocation(pcf.SecurityZoneDetail, {SecurityZone}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (SecurityZone :  SecurityZone) : pcf.api.Location {
    return __newDestinationForLocation(pcf.SecurityZoneDetail, {SecurityZone}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (SecurityZone :  SecurityZone) : pcf.api.Location {
    return __newDestinationForLocation(pcf.SecurityZoneDetail, {SecurityZone}, 0).goInMain()
  }
  
  static function printPage (SecurityZone :  SecurityZone) : pcf.api.Location {
    return __newDestinationForLocation(pcf.SecurityZoneDetail, {SecurityZone}, 0).printPage()
  }
  
  static function push (SecurityZone :  SecurityZone) : pcf.api.Location {
    return __newDestinationForLocation(pcf.SecurityZoneDetail, {SecurityZone}, 0).push()
  }
  
  
}