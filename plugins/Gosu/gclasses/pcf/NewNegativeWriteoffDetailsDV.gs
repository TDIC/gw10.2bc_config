package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewNegativeWriteoffDetailsDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($uiWriteoff :  gw.api.web.accounting.UIWriteOffCreation) : void {
    __widgetOf(this, pcf.NewNegativeWriteoffDetailsDV, SECTION_WIDGET_CLASS).setVariables(false, {$uiWriteoff})
  }
  
  function refreshVariables ($uiWriteoff :  gw.api.web.accounting.UIWriteOffCreation) : void {
    __widgetOf(this, pcf.NewNegativeWriteoffDetailsDV, SECTION_WIDGET_CLASS).setVariables(true, {$uiWriteoff})
  }
  
  
}