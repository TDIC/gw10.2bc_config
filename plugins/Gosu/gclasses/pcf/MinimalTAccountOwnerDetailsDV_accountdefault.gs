package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/MinimalTAccountOwnerDetailsDV.accountdefault.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class MinimalTAccountOwnerDetailsDV_accountdefault extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($tAccountOwner :  TAccountOwner, $unapplied :  UnappliedFund) : void {
    __widgetOf(this, pcf.MinimalTAccountOwnerDetailsDV_accountdefault, SECTION_WIDGET_CLASS).setVariables(false, {$tAccountOwner, $unapplied})
  }
  
  function refreshVariables ($tAccountOwner :  TAccountOwner, $unapplied :  UnappliedFund) : void {
    __widgetOf(this, pcf.MinimalTAccountOwnerDetailsDV_accountdefault, SECTION_WIDGET_CLASS).setVariables(true, {$tAccountOwner, $unapplied})
  }
  
  
}