package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/LOCDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class LOCDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($loc :  LetterOfCredit) : void {
    __widgetOf(this, pcf.LOCDV, SECTION_WIDGET_CLASS).setVariables(false, {$loc})
  }
  
  function refreshVariables ($loc :  LetterOfCredit) : void {
    __widgetOf(this, pcf.LOCDV, SECTION_WIDGET_CLASS).setVariables(true, {$loc})
  }
  
  
}