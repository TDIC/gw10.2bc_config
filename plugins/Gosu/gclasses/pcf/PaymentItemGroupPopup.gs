package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/PaymentItemGroupPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PaymentItemGroupPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (paymentItemGroup :  PaymentItemGroup) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PaymentItemGroupPopup, {paymentItemGroup}, 0)
  }
  
  static function push (paymentItemGroup :  PaymentItemGroup) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentItemGroupPopup, {paymentItemGroup}, 0).push()
  }
  
  
}