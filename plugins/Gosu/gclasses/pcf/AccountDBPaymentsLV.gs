package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDBPaymentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountDBPaymentsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($account :  Account, $moneyReceiveds :  gw.api.database.IQueryBeanResult<PaymentMoneyReceived>, $showTargets :  boolean) : void {
    __widgetOf(this, pcf.AccountDBPaymentsLV, SECTION_WIDGET_CLASS).setVariables(false, {$account, $moneyReceiveds, $showTargets})
  }
  
  function refreshVariables ($account :  Account, $moneyReceiveds :  gw.api.database.IQueryBeanResult<PaymentMoneyReceived>, $showTargets :  boolean) : void {
    __widgetOf(this, pcf.AccountDBPaymentsLV, SECTION_WIDGET_CLASS).setVariables(true, {$account, $moneyReceiveds, $showTargets})
  }
  
  
}