package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/EditDBPaymentScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class EditDBPaymentScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($paymentView :  gw.api.web.payment.DirectBillPaymentView, $creditCardHandler :  tdic.bc.integ.services.creditcard.TDIC_CreditCardHandler) : void {
    __widgetOf(this, pcf.EditDBPaymentScreen, SECTION_WIDGET_CLASS).setVariables(false, {$paymentView, $creditCardHandler})
  }
  
  function refreshVariables ($paymentView :  gw.api.web.payment.DirectBillPaymentView, $creditCardHandler :  tdic.bc.integ.services.creditcard.TDIC_CreditCardHandler) : void {
    __widgetOf(this, pcf.EditDBPaymentScreen, SECTION_WIDGET_CLASS).setVariables(true, {$paymentView, $creditCardHandler})
  }
  
  
}