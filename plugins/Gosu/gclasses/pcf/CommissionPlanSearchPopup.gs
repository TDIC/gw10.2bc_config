package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/CommissionPlanSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CommissionPlanSearchPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination () : pcf.api.Destination {
    return __newDestinationForLocation(pcf.CommissionPlanSearchPopup, {}, 0)
  }
  
  static function createDestination (tier :  ProducerTier) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.CommissionPlanSearchPopup, {tier}, 1)
  }
  
  function pickValueAndCommit (value :  CommissionPlan) : void {
    __widgetOf(this, pcf.CommissionPlanSearchPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push () : pcf.api.Location {
    return __newDestinationForLocation(pcf.CommissionPlanSearchPopup, {}, 0).push()
  }
  
  static function push (tier :  ProducerTier) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CommissionPlanSearchPopup, {tier}, 1).push()
  }
  
  
}