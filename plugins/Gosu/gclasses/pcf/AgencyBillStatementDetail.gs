package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillStatementDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyBillStatementDetail extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (cycle :  AgencyBillCycle) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyBillStatementDetail, {cycle}, 0)
  }
  
  static function drilldown (cycle :  AgencyBillCycle) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillStatementDetail, {cycle}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (cycle :  AgencyBillCycle) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillStatementDetail, {cycle}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (cycle :  AgencyBillCycle) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillStatementDetail, {cycle}, 0).goInMain()
  }
  
  static function printPage (cycle :  AgencyBillCycle) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillStatementDetail, {cycle}, 0).printPage()
  }
  
  static function push (cycle :  AgencyBillCycle) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillStatementDetail, {cycle}, 0).push()
  }
  
  
}