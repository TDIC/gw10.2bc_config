package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/TransactionDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TransactionDetailPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (Transaction :  Transaction) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TransactionDetailPopup, {Transaction}, 0)
  }
  
  static function push (Transaction :  Transaction) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TransactionDetailPopup, {Transaction}, 0).push()
  }
  
  
}