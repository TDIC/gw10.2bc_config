package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/suspensepayment/ReverseSuspensePaymentsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ReverseSuspensePaymentsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (suspensePayments :  java.util.List<SuspensePayment>) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ReverseSuspensePaymentsPopup, {suspensePayments}, 0)
  }
  
  static function push (suspensePayments :  java.util.List<SuspensePayment>) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ReverseSuspensePaymentsPopup, {suspensePayments}, 0).push()
  }
  
  
}