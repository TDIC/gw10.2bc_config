package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/ChargeBreakdownItemsDisplayLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ChargeBreakdownItemsDisplayLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($charge :  entity.Charge) : void {
    __widgetOf(this, pcf.ChargeBreakdownItemsDisplayLV, SECTION_WIDGET_CLASS).setVariables(false, {$charge})
  }
  
  function refreshVariables ($charge :  entity.Charge) : void {
    __widgetOf(this, pcf.ChargeBreakdownItemsDisplayLV, SECTION_WIDGET_CLASS).setVariables(true, {$charge})
  }
  
  
}