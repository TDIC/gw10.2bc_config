package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillSuspenseItemPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyBillSuspenseItemPanelSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($suspenseItems :  BaseSuspDistItem[], $suspenseType :  gw.agencybill.AgencyBillSuspenseType) : void {
    __widgetOf(this, pcf.AgencyBillSuspenseItemPanelSet, SECTION_WIDGET_CLASS).setVariables(false, {$suspenseItems, $suspenseType})
  }
  
  function refreshVariables ($suspenseItems :  BaseSuspDistItem[], $suspenseType :  gw.agencybill.AgencyBillSuspenseType) : void {
    __widgetOf(this, pcf.AgencyBillSuspenseItemPanelSet, SECTION_WIDGET_CLASS).setVariables(true, {$suspenseItems, $suspenseType})
  }
  
  
}