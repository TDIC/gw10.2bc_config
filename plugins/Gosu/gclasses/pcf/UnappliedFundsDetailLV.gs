package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/UnappliedFundsDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class UnappliedFundsDetailLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($unappliedFund :  UnappliedFund) : void {
    __widgetOf(this, pcf.UnappliedFundsDetailLV, SECTION_WIDGET_CLASS).setVariables(false, {$unappliedFund})
  }
  
  function refreshVariables ($unappliedFund :  UnappliedFund) : void {
    __widgetOf(this, pcf.UnappliedFundsDetailLV, SECTION_WIDGET_CLASS).setVariables(true, {$unappliedFund})
  }
  
  
}