package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/email/SampleEmailWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class SampleEmailWorksheet extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (srcBean :  KeyableBean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.SampleEmailWorksheet, {srcBean}, 0)
  }
  
  static function createDestination (srcBean :  KeyableBean, docContainer :  DocumentContainer) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.SampleEmailWorksheet, {srcBean, docContainer}, 1)
  }
  
  static function createDestination (srcBean :  KeyableBean, docContainer :  DocumentContainer, emailTemplateStr :  String) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.SampleEmailWorksheet, {srcBean, docContainer, emailTemplateStr}, 2)
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (srcBean :  KeyableBean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.SampleEmailWorksheet, {srcBean}, 0).goInWorkspace()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (srcBean :  KeyableBean, docContainer :  DocumentContainer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.SampleEmailWorksheet, {srcBean, docContainer}, 1).goInWorkspace()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (srcBean :  KeyableBean, docContainer :  DocumentContainer, emailTemplateStr :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.SampleEmailWorksheet, {srcBean, docContainer, emailTemplateStr}, 2).goInWorkspace()
  }
  
  static function push (srcBean :  KeyableBean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.SampleEmailWorksheet, {srcBean}, 0).push()
  }
  
  static function push (srcBean :  KeyableBean, docContainer :  DocumentContainer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.SampleEmailWorksheet, {srcBean, docContainer}, 1).push()
  }
  
  static function push (srcBean :  KeyableBean, docContainer :  DocumentContainer, emailTemplateStr :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.SampleEmailWorksheet, {srcBean, docContainer, emailTemplateStr}, 2).push()
  }
  
  
}