package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/activityenhancement/PolicyPeriodActivitiesPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyPeriodActivitiesPage extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PolicyPeriodActivitiesPage, {plcyPeriod}, 0)
  }
  
  static function drilldown (plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyPeriodActivitiesPage, {plcyPeriod}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyPeriodActivitiesPage, {plcyPeriod}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyPeriodActivitiesPage, {plcyPeriod}, 0).goInMain()
  }
  
  static function printPage (plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyPeriodActivitiesPage, {plcyPeriod}, 0).printPage()
  }
  
  static function push (plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyPeriodActivitiesPage, {plcyPeriod}, 0).push()
  }
  
  
}