package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducerDetailPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (producer :  Producer) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ProducerDetailPopup, {producer}, 0)
  }
  
  static function push (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerDetailPopup, {producer}, 0).push()
  }
  
  
}