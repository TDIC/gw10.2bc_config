package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExceptionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyBillExceptionsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($exceptionItemViews :  gw.api.web.invoice.ExceptionItemView[], $ShowCheckboxes :  boolean, $ShowWriteoffReasonDropdowns :  boolean) : void {
    __widgetOf(this, pcf.AgencyBillExceptionsLV, SECTION_WIDGET_CLASS).setVariables(false, {$exceptionItemViews, $ShowCheckboxes, $ShowWriteoffReasonDropdowns})
  }
  
  function refreshVariables ($exceptionItemViews :  gw.api.web.invoice.ExceptionItemView[], $ShowCheckboxes :  boolean, $ShowWriteoffReasonDropdowns :  boolean) : void {
    __widgetOf(this, pcf.AgencyBillExceptionsLV, SECTION_WIDGET_CLASS).setVariables(true, {$exceptionItemViews, $ShowCheckboxes, $ShowWriteoffReasonDropdowns})
  }
  
  
}