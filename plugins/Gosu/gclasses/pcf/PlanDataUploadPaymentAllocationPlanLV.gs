package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadPaymentAllocationPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PlanDataUploadPaymentAllocationPlanLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($processor :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) : void {
    __widgetOf(this, pcf.PlanDataUploadPaymentAllocationPlanLV, SECTION_WIDGET_CLASS).setVariables(false, {$processor})
  }
  
  function refreshVariables ($processor :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) : void {
    __widgetOf(this, pcf.PlanDataUploadPaymentAllocationPlanLV, SECTION_WIDGET_CLASS).setVariables(true, {$processor})
  }
  
  
}