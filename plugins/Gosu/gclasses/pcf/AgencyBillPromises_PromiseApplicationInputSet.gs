package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillPromises_PromiseApplicationInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyBillPromises_PromiseApplicationInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($selectedPromise :  AgencyCyclePromise) : void {
    __widgetOf(this, pcf.AgencyBillPromises_PromiseApplicationInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$selectedPromise})
  }
  
  function refreshVariables ($selectedPromise :  AgencyCyclePromise) : void {
    __widgetOf(this, pcf.AgencyBillPromises_PromiseApplicationInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$selectedPromise})
  }
  
  
}