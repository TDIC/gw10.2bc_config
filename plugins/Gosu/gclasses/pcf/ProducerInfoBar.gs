package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerInfoBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducerInfoBar extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($Producer :  Producer) : void {
    __widgetOf(this, pcf.ProducerInfoBar, SECTION_WIDGET_CLASS).setVariables(false, {$Producer})
  }
  
  function refreshVariables ($Producer :  Producer) : void {
    __widgetOf(this, pcf.ProducerInfoBar, SECTION_WIDGET_CLASS).setVariables(true, {$Producer})
  }
  
  
}