package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewWriteoffReversalConfirmationScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($reversal :  WriteoffReversal) : void {
    __widgetOf(this, pcf.NewWriteoffReversalConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(false, {$reversal})
  }
  
  function refreshVariables ($reversal :  WriteoffReversal) : void {
    __widgetOf(this, pcf.NewWriteoffReversalConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(true, {$reversal})
  }
  
  
}