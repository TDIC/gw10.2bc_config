package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/ModifyDirectBillPaymentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ModifyDirectBillPaymentPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (account :  Account, originalMoney :  DirectBillMoneyRcvd, whenModifyingDirectBillMoney :  gw.api.web.payment.WhenModifyingDirectBillMoney) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ModifyDirectBillPaymentPopup, {account, originalMoney, whenModifyingDirectBillMoney}, 0)
  }
  
  static function push (account :  Account, originalMoney :  DirectBillMoneyRcvd, whenModifyingDirectBillMoney :  gw.api.web.payment.WhenModifyingDirectBillMoney) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ModifyDirectBillPaymentPopup, {account, originalMoney, whenModifyingDirectBillMoney}, 0).push()
  }
  
  
}