package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/AccountNewNegativeWriteoffWizardTargetStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountNewNegativeWriteoffWizardTargetStepScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($account :  Account, $uiWriteoff :  gw.api.web.accounting.UIWriteOffCreation) : void {
    __widgetOf(this, pcf.AccountNewNegativeWriteoffWizardTargetStepScreen, SECTION_WIDGET_CLASS).setVariables(false, {$account, $uiWriteoff})
  }
  
  function refreshVariables ($account :  Account, $uiWriteoff :  gw.api.web.accounting.UIWriteOffCreation) : void {
    __widgetOf(this, pcf.AccountNewNegativeWriteoffWizardTargetStepScreen, SECTION_WIDGET_CLASS).setVariables(true, {$account, $uiWriteoff})
  }
  
  
}