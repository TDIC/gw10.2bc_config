package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/authoritylimits/AuthorityLimitProfileDetailPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AuthorityLimitProfileDetailPage extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (authorityLimitProfile :  AuthorityLimitProfile) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AuthorityLimitProfileDetailPage, {authorityLimitProfile}, 0)
  }
  
  static function drilldown (authorityLimitProfile :  AuthorityLimitProfile) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AuthorityLimitProfileDetailPage, {authorityLimitProfile}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (authorityLimitProfile :  AuthorityLimitProfile) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AuthorityLimitProfileDetailPage, {authorityLimitProfile}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (authorityLimitProfile :  AuthorityLimitProfile) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AuthorityLimitProfileDetailPage, {authorityLimitProfile}, 0).goInMain()
  }
  
  static function printPage (authorityLimitProfile :  AuthorityLimitProfile) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AuthorityLimitProfileDetailPage, {authorityLimitProfile}, 0).printPage()
  }
  
  static function push (authorityLimitProfile :  AuthorityLimitProfile) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AuthorityLimitProfileDetailPage, {authorityLimitProfile}, 0).push()
  }
  
  
}