package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultiplePolicyPeriodsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class SelectMultiplePolicyPeriodsScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policySearchCriteria :  gw.search.PolicySearchCriteria) : void {
    __widgetOf(this, pcf.SelectMultiplePolicyPeriodsScreen, SECTION_WIDGET_CLASS).setVariables(false, {$policySearchCriteria})
  }
  
  function refreshVariables ($policySearchCriteria :  gw.search.PolicySearchCriteria) : void {
    __widgetOf(this, pcf.SelectMultiplePolicyPeriodsScreen, SECTION_WIDGET_CLASS).setVariables(true, {$policySearchCriteria})
  }
  
  
}