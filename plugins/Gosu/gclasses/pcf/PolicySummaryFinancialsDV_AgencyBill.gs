package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicySummaryFinancialsDV.AgencyBill.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicySummaryFinancialsDV_AgencyBill extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyPeriod :  PolicyPeriod, $summaryHelper :  gw.web.policy.PolicySummaryHelper) : void {
    __widgetOf(this, pcf.PolicySummaryFinancialsDV_AgencyBill, SECTION_WIDGET_CLASS).setVariables(false, {$policyPeriod, $summaryHelper})
  }
  
  function refreshVariables ($policyPeriod :  PolicyPeriod, $summaryHelper :  gw.web.policy.PolicySummaryHelper) : void {
    __widgetOf(this, pcf.PolicySummaryFinancialsDV_AgencyBill, SECTION_WIDGET_CLASS).setVariables(true, {$policyPeriod, $summaryHelper})
  }
  
  
}