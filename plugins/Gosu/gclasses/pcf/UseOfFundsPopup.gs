package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/UseOfFundsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class UseOfFundsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (fundsUseTracker :  FundsUseTracker) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.UseOfFundsPopup, {fundsUseTracker}, 0)
  }
  
  static function push (fundsUseTracker :  FundsUseTracker) : pcf.api.Location {
    return __newDestinationForLocation(pcf.UseOfFundsPopup, {fundsUseTracker}, 0).push()
  }
  
  
}