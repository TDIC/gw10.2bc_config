package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/PaymentInstrumentInputSet.creditcard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PaymentInstrumentInputSet_creditcard extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($paymentInstrument :  PaymentInstrument) : void {
    __widgetOf(this, pcf.PaymentInstrumentInputSet_creditcard, SECTION_WIDGET_CLASS).setVariables(false, {$paymentInstrument})
  }
  
  function refreshVariables ($paymentInstrument :  PaymentInstrument) : void {
    __widgetOf(this, pcf.PaymentInstrumentInputSet_creditcard, SECTION_WIDGET_CLASS).setVariables(true, {$paymentInstrument})
  }
  
  
}