package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/chargepatterns/ChargePatternDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ChargePatternDetailDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($chargePattern :  ChargePattern) : void {
    __widgetOf(this, pcf.ChargePatternDetailDV, SECTION_WIDGET_CLASS).setVariables(false, {$chargePattern})
  }
  
  function refreshVariables ($chargePattern :  ChargePattern) : void {
    __widgetOf(this, pcf.ChargePatternDetailDV, SECTION_WIDGET_CLASS).setVariables(true, {$chargePattern})
  }
  
  
}