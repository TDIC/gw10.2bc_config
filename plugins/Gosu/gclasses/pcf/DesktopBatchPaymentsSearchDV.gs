package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopBatchPaymentsSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DesktopBatchPaymentsSearchDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($batchSearchCriteria :  gw.search.BatchPaymentsSearchCriteria) : void {
    __widgetOf(this, pcf.DesktopBatchPaymentsSearchDV, SECTION_WIDGET_CLASS).setVariables(false, {$batchSearchCriteria})
  }
  
  function refreshVariables ($batchSearchCriteria :  gw.search.BatchPaymentsSearchCriteria) : void {
    __widgetOf(this, pcf.DesktopBatchPaymentsSearchDV, SECTION_WIDGET_CLASS).setVariables(true, {$batchSearchCriteria})
  }
  
  
}