package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/CommissionPayableReductionWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CommissionPayableReductionWizard extends com.guidewire.pl.web.codegen.WizardBase {
  static function createDestination (producerID :  gw.pl.persistence.core.Key) : pcf.api.Destination {
    return __newDestinationForWizard(pcf.CommissionPayableReductionWizard, {producerID}, 0)
  }
  
  static function drilldown (producerID :  gw.pl.persistence.core.Key) : pcf.api.Location {
    return __newDestinationForWizard(pcf.CommissionPayableReductionWizard, {producerID}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (producerID :  gw.pl.persistence.core.Key) : pcf.api.Location {
    return __newDestinationForWizard(pcf.CommissionPayableReductionWizard, {producerID}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (producerID :  gw.pl.persistence.core.Key) : pcf.api.Location {
    return __newDestinationForWizard(pcf.CommissionPayableReductionWizard, {producerID}, 0).goInMain()
  }
  
  static function printPage (producerID :  gw.pl.persistence.core.Key) : pcf.api.Location {
    return __newDestinationForWizard(pcf.CommissionPayableReductionWizard, {producerID}, 0).printPage()
  }
  
  static function push (producerID :  gw.pl.persistence.core.Key) : pcf.api.Location {
    return __newDestinationForWizard(pcf.CommissionPayableReductionWizard, {producerID}, 0).push()
  }
  
  
}