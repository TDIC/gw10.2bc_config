package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/ListBillInvoicingOverridesInvoiceItemsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ListBillInvoicingOverridesInvoiceItemsStepScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyPeriod :  PolicyPeriod, $listBillHelper :  gw.api.web.policy.ListBillInvoicingOverridesHelper) : void {
    __widgetOf(this, pcf.ListBillInvoicingOverridesInvoiceItemsStepScreen, SECTION_WIDGET_CLASS).setVariables(false, {$policyPeriod, $listBillHelper})
  }
  
  function refreshVariables ($policyPeriod :  PolicyPeriod, $listBillHelper :  gw.api.web.policy.ListBillInvoicingOverridesHelper) : void {
    __widgetOf(this, pcf.ListBillInvoicingOverridesInvoiceItemsStepScreen, SECTION_WIDGET_CLASS).setVariables(true, {$policyPeriod, $listBillHelper})
  }
  
  
}