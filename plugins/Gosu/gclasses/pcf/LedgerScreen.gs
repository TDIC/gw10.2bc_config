package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/accounting/LedgerScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class LedgerScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($tAccountOwner :  TAccountOwner, $relatedTAccountOwners :  TAccountOwner[]) : void {
    __widgetOf(this, pcf.LedgerScreen, SECTION_WIDGET_CLASS).setVariables(false, {$tAccountOwner, $relatedTAccountOwners})
  }
  
  function refreshVariables ($tAccountOwner :  TAccountOwner, $relatedTAccountOwners :  TAccountOwner[]) : void {
    __widgetOf(this, pcf.LedgerScreen, SECTION_WIDGET_CLASS).setVariables(true, {$tAccountOwner, $relatedTAccountOwners})
  }
  
  
}