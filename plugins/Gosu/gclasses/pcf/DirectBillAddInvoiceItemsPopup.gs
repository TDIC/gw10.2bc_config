package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/DirectBillAddInvoiceItemsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DirectBillAddInvoiceItemsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (directBillPaymentView :  gw.api.web.payment.DirectBillPaymentView, amountAvailableToDistribute :  gw.pl.currency.MonetaryAmount) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.DirectBillAddInvoiceItemsPopup, {directBillPaymentView, amountAvailableToDistribute}, 0)
  }
  
  static function push (directBillPaymentView :  gw.api.web.payment.DirectBillPaymentView, amountAvailableToDistribute :  gw.pl.currency.MonetaryAmount) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DirectBillAddInvoiceItemsPopup, {directBillPaymentView, amountAvailableToDistribute}, 0).push()
  }
  
  
}