package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CommissionPlanDetailScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($commissionPlan :  CommissionPlan) : void {
    __widgetOf(this, pcf.CommissionPlanDetailScreen, SECTION_WIDGET_CLASS).setVariables(false, {$commissionPlan})
  }
  
  function refreshVariables ($commissionPlan :  CommissionPlan) : void {
    __widgetOf(this, pcf.CommissionPlanDetailScreen, SECTION_WIDGET_CLASS).setVariables(true, {$commissionPlan})
  }
  
  
}