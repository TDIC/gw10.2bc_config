package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/TransferDetailsLV.Account.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TransferDetailsLV_account extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($fundsTransferUtil :  gw.api.web.transaction.FundsTransferUtil, $canEditPage :  boolean) : void {
    __widgetOf(this, pcf.TransferDetailsLV_account, SECTION_WIDGET_CLASS).setVariables(false, {$fundsTransferUtil, $canEditPage})
  }
  
  function refreshVariables ($fundsTransferUtil :  gw.api.web.transaction.FundsTransferUtil, $canEditPage :  boolean) : void {
    __widgetOf(this, pcf.TransferDetailsLV_account, SECTION_WIDGET_CLASS).setVariables(true, {$fundsTransferUtil, $canEditPage})
  }
  
  
}