package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/InvoiceSearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class InvoiceSearchResultsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($invoiceSearchViews :  gw.api.database.IQueryBeanResult<InvoiceSearchView>, $showHyperlinks :  boolean, $isWizard :  boolean) : void {
    __widgetOf(this, pcf.InvoiceSearchResultsLV, SECTION_WIDGET_CLASS).setVariables(false, {$invoiceSearchViews, $showHyperlinks, $isWizard})
  }
  
  function refreshVariables ($invoiceSearchViews :  gw.api.database.IQueryBeanResult<InvoiceSearchView>, $showHyperlinks :  boolean, $isWizard :  boolean) : void {
    __widgetOf(this, pcf.InvoiceSearchResultsLV, SECTION_WIDGET_CLASS).setVariables(true, {$invoiceSearchViews, $showHyperlinks, $isWizard})
  }
  
  
}