package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewProducerContactPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewProducerContactPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (producer :  Producer, contactSubtype :  Type<Contact>) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewProducerContactPopup, {producer, contactSubtype}, 0)
  }
  
  function pickValueAndCommit (value :  ProducerContact) : void {
    __widgetOf(this, pcf.NewProducerContactPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (producer :  Producer, contactSubtype :  Type<Contact>) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewProducerContactPopup, {producer, contactSubtype}, 0).push()
  }
  
  
}