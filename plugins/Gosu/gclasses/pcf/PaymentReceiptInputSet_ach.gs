package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/PaymentReceiptInputSet.ach.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PaymentReceiptInputSet_ach extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($paymentReceipt :  PaymentReceipt) : void {
    __widgetOf(this, pcf.PaymentReceiptInputSet_ach, SECTION_WIDGET_CLASS).setVariables(false, {$paymentReceipt})
  }
  
  function refreshVariables ($paymentReceipt :  PaymentReceipt) : void {
    __widgetOf(this, pcf.PaymentReceiptInputSet_ach, SECTION_WIDGET_CLASS).setVariables(true, {$paymentReceipt})
  }
  
  
}