package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountAddPaymentPlanPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountAddPaymentPlanPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (account :  Account, selectedPlans :  List<PaymentPlan>) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AccountAddPaymentPlanPopup, {account, selectedPlans}, 0)
  }
  
  function pickValueAndCommit (value :  PaymentPlan[]) : void {
    __widgetOf(this, pcf.AccountAddPaymentPlanPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (account :  Account, selectedPlans :  List<PaymentPlan>) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountAddPaymentPlanPopup, {account, selectedPlans}, 0).push()
  }
  
  
}