package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyDistributionReversalConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyDistributionReversalConfirmationPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (money :  AgencyBillMoneyRcvd) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyDistributionReversalConfirmationPopup, {money}, 1)
  }
  
  static function createDestination (agencyCycleDist :  AgencyCycleDist) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyDistributionReversalConfirmationPopup, {agencyCycleDist}, 0)
  }
  
  static function push (money :  AgencyBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyDistributionReversalConfirmationPopup, {money}, 1).push()
  }
  
  static function push (agencyCycleDist :  AgencyCycleDist) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyDistributionReversalConfirmationPopup, {agencyCycleDist}, 0).push()
  }
  
  
}