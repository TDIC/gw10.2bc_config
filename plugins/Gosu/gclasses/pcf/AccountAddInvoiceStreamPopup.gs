package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountAddInvoiceStreamPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountAddInvoiceStreamPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (account :  Account) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AccountAddInvoiceStreamPopup, {account}, 0)
  }
  
  function pickValueAndCommit (value :  InvoiceStream) : void {
    __widgetOf(this, pcf.AccountAddInvoiceStreamPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (account :  Account) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountAddInvoiceStreamPopup, {account}, 0).push()
  }
  
  
}