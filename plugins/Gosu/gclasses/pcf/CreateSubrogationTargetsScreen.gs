package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/CreateSubrogationTargetsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CreateSubrogationTargetsScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($subrogation :  Subrogation, $account :  gw.api.web.accounting.TAccountOwnerReference) : void {
    __widgetOf(this, pcf.CreateSubrogationTargetsScreen, SECTION_WIDGET_CLASS).setVariables(false, {$subrogation, $account})
  }
  
  function refreshVariables ($subrogation :  Subrogation, $account :  gw.api.web.accounting.TAccountOwnerReference) : void {
    __widgetOf(this, pcf.CreateSubrogationTargetsScreen, SECTION_WIDGET_CLASS).setVariables(true, {$subrogation, $account})
  }
  
  
}