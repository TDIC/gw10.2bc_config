package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/NewDocumentFromTemplateWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewDocumentFromTemplateWorksheet extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (documentContainer :  DocumentContainer) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewDocumentFromTemplateWorksheet, {documentContainer}, 0)
  }
  
  static function createDestination (documentContainer :  DocumentContainer, templateName :  String) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewDocumentFromTemplateWorksheet, {documentContainer, templateName}, 1)
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (documentContainer :  DocumentContainer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewDocumentFromTemplateWorksheet, {documentContainer}, 0).goInWorkspace()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (documentContainer :  DocumentContainer, templateName :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewDocumentFromTemplateWorksheet, {documentContainer, templateName}, 1).goInWorkspace()
  }
  
  static function push (documentContainer :  DocumentContainer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewDocumentFromTemplateWorksheet, {documentContainer}, 0).push()
  }
  
  static function push (documentContainer :  DocumentContainer, templateName :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewDocumentFromTemplateWorksheet, {documentContainer, templateName}, 1).push()
  }
  
  
}