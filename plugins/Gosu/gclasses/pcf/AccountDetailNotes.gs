package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailNotes.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountDetailNotes extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (account :  Account, isClearBundle :  Boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AccountDetailNotes, {account, isClearBundle}, 0)
  }
  
  static function createDestination (account :  Account, isClearBundle :  Boolean, latestNote :  Note) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AccountDetailNotes, {account, isClearBundle, latestNote}, 1)
  }
  
  static function drilldown (account :  Account, isClearBundle :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailNotes, {account, isClearBundle}, 0).drilldown()
  }
  
  static function drilldown (account :  Account, isClearBundle :  Boolean, latestNote :  Note) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailNotes, {account, isClearBundle, latestNote}, 1).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (account :  Account, isClearBundle :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailNotes, {account, isClearBundle}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (account :  Account, isClearBundle :  Boolean, latestNote :  Note) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailNotes, {account, isClearBundle, latestNote}, 1).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (account :  Account, isClearBundle :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailNotes, {account, isClearBundle}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (account :  Account, isClearBundle :  Boolean, latestNote :  Note) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailNotes, {account, isClearBundle, latestNote}, 1).goInMain()
  }
  
  static function printPage (account :  Account, isClearBundle :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailNotes, {account, isClearBundle}, 0).printPage()
  }
  
  static function printPage (account :  Account, isClearBundle :  Boolean, latestNote :  Note) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailNotes, {account, isClearBundle, latestNote}, 1).printPage()
  }
  
  static function push (account :  Account, isClearBundle :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailNotes, {account, isClearBundle}, 0).push()
  }
  
  static function push (account :  Account, isClearBundle :  Boolean, latestNote :  Note) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountDetailNotes, {account, isClearBundle, latestNote}, 1).push()
  }
  
  
}