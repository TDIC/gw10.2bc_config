package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralDisbursementDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CollateralDisbursementDetailScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($disbursementVar :  CollateralDisbursement) : void {
    __widgetOf(this, pcf.CollateralDisbursementDetailScreen, SECTION_WIDGET_CLASS).setVariables(false, {$disbursementVar})
  }
  
  function refreshVariables ($disbursementVar :  CollateralDisbursement) : void {
    __widgetOf(this, pcf.CollateralDisbursementDetailScreen, SECTION_WIDGET_CLASS).setVariables(true, {$disbursementVar})
  }
  
  
}