package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transfer/AccountTransferWizardTransferScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountTransferWizardTransferScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($accountTransfer :  gw.api.domain.account.AccountTransfer) : void {
    __widgetOf(this, pcf.AccountTransferWizardTransferScreen, SECTION_WIDGET_CLASS).setVariables(false, {$accountTransfer})
  }
  
  function refreshVariables ($accountTransfer :  gw.api.domain.account.AccountTransfer) : void {
    __widgetOf(this, pcf.AccountTransferWizardTransferScreen, SECTION_WIDGET_CLASS).setVariables(true, {$accountTransfer})
  }
  
  
}