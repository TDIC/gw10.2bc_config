package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/InvoiceForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class InvoiceForward extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (invoice :  Invoice) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.InvoiceForward, {invoice}, 0)
  }
  
  static function createDestination (invoice :  Invoice, account :  Account) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.InvoiceForward, {invoice, account}, 1)
  }
  
  static function drilldown (invoice :  Invoice) : pcf.api.Location {
    return __newDestinationForLocation(pcf.InvoiceForward, {invoice}, 0).drilldown()
  }
  
  static function drilldown (invoice :  Invoice, account :  Account) : pcf.api.Location {
    return __newDestinationForLocation(pcf.InvoiceForward, {invoice, account}, 1).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (invoice :  Invoice) : pcf.api.Location {
    return __newDestinationForLocation(pcf.InvoiceForward, {invoice}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (invoice :  Invoice, account :  Account) : pcf.api.Location {
    return __newDestinationForLocation(pcf.InvoiceForward, {invoice, account}, 1).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (invoice :  Invoice) : pcf.api.Location {
    return __newDestinationForLocation(pcf.InvoiceForward, {invoice}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (invoice :  Invoice, account :  Account) : pcf.api.Location {
    return __newDestinationForLocation(pcf.InvoiceForward, {invoice, account}, 1).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (invoice :  Invoice) : pcf.api.Location {
    return __newDestinationForLocation(pcf.InvoiceForward, {invoice}, 0).goInWorkspace()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (invoice :  Invoice, account :  Account) : pcf.api.Location {
    return __newDestinationForLocation(pcf.InvoiceForward, {invoice, account}, 1).goInWorkspace()
  }
  
  static function printPage (invoice :  Invoice) : pcf.api.Location {
    return __newDestinationForLocation(pcf.InvoiceForward, {invoice}, 0).printPage()
  }
  
  static function printPage (invoice :  Invoice, account :  Account) : pcf.api.Location {
    return __newDestinationForLocation(pcf.InvoiceForward, {invoice, account}, 1).printPage()
  }
  
  static function push (invoice :  Invoice) : pcf.api.Location {
    return __newDestinationForLocation(pcf.InvoiceForward, {invoice}, 0).push()
  }
  
  static function push (invoice :  Invoice, account :  Account) : pcf.api.Location {
    return __newDestinationForLocation(pcf.InvoiceForward, {invoice, account}, 1).push()
  }
  
  
}