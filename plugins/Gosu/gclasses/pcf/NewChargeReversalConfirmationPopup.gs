package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/accounting/NewChargeReversalConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewChargeReversalConfirmationPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (charge :  Charge) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewChargeReversalConfirmationPopup, {charge}, 0)
  }
  
  static function push (charge :  Charge) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewChargeReversalConfirmationPopup, {charge}, 0).push()
  }
  
  
}