package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewFundsTransferReversalConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewFundsTransferReversalConfirmationScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($transferFundsContext :  gw.transaction.TransferFundsReversalWizardContext) : void {
    __widgetOf(this, pcf.NewFundsTransferReversalConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(false, {$transferFundsContext})
  }
  
  function refreshVariables ($transferFundsContext :  gw.transaction.TransferFundsReversalWizardContext) : void {
    __widgetOf(this, pcf.NewFundsTransferReversalConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(true, {$transferFundsContext})
  }
  
  
}