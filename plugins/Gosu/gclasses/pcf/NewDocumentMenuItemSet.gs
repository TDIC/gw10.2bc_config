package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/NewDocumentMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewDocumentMenuItemSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($documentContainer :  DocumentContainer) : void {
    __widgetOf(this, pcf.NewDocumentMenuItemSet, SECTION_WIDGET_CLASS).setVariables(false, {$documentContainer})
  }
  
  function refreshVariables ($documentContainer :  DocumentContainer) : void {
    __widgetOf(this, pcf.NewDocumentMenuItemSet, SECTION_WIDGET_CLASS).setVariables(true, {$documentContainer})
  }
  
  
}