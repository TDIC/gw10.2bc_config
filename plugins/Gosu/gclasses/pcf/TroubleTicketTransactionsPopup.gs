package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketTransactionsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TroubleTicketTransactionsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (TroubleTicket :  TroubleTicket) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TroubleTicketTransactionsPopup, {TroubleTicket}, 0)
  }
  
  static function push (TroubleTicket :  TroubleTicket) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TroubleTicketTransactionsPopup, {TroubleTicket}, 0).push()
  }
  
  
}