package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffReversalSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewNegativeWriteoffReversalSearchScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($negativeWriteoffToReverse :  NegativeWriteoffRev, $account :  Account) : void {
    __widgetOf(this, pcf.NewNegativeWriteoffReversalSearchScreen, SECTION_WIDGET_CLASS).setVariables(false, {$negativeWriteoffToReverse, $account})
  }
  
  function refreshVariables ($negativeWriteoffToReverse :  NegativeWriteoffRev, $account :  Account) : void {
    __widgetOf(this, pcf.NewNegativeWriteoffReversalSearchScreen, SECTION_WIDGET_CLASS).setVariables(true, {$negativeWriteoffToReverse, $account})
  }
  
  
}