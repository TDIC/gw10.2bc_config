package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalChargesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewChargeReversalChargesLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($reversal :  ChargeReversal, $charges :  gw.api.database.IQueryBeanResult<Charge>) : void {
    __widgetOf(this, pcf.NewChargeReversalChargesLV, SECTION_WIDGET_CLASS).setVariables(false, {$reversal, $charges})
  }
  
  function refreshVariables ($reversal :  ChargeReversal, $charges :  gw.api.database.IQueryBeanResult<Charge>) : void {
    __widgetOf(this, pcf.NewChargeReversalChargesLV, SECTION_WIDGET_CLASS).setVariables(true, {$reversal, $charges})
  }
  
  
}