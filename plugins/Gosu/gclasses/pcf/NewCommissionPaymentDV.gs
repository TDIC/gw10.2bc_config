package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewCommissionPaymentDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($standardPayment :  StandardCmsnPayment) : void {
    __widgetOf(this, pcf.NewCommissionPaymentDV, SECTION_WIDGET_CLASS).setVariables(false, {$standardPayment})
  }
  
  function refreshVariables ($standardPayment :  StandardCmsnPayment) : void {
    __widgetOf(this, pcf.NewCommissionPaymentDV, SECTION_WIDGET_CLASS).setVariables(true, {$standardPayment})
  }
  
  
}