package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/Acct360NewNonBalanceTxnPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class Acct360NewNonBalanceTxnPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (acctObj :  Account) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.Acct360NewNonBalanceTxnPopup, {acctObj}, 0)
  }
  
  static function push (acctObj :  Account) : pcf.api.Location {
    return __newDestinationForLocation(pcf.Acct360NewNonBalanceTxnPopup, {acctObj}, 0).push()
  }
  
  
}