package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffWizardDetailsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewNegativeWriteoffWizardDetailsStepScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($uiWriteoff :  gw.api.web.accounting.UIWriteOffCreation) : void {
    __widgetOf(this, pcf.NewNegativeWriteoffWizardDetailsStepScreen, SECTION_WIDGET_CLASS).setVariables(false, {$uiWriteoff})
  }
  
  function refreshVariables ($uiWriteoff :  gw.api.web.accounting.UIWriteOffCreation) : void {
    __widgetOf(this, pcf.NewNegativeWriteoffWizardDetailsStepScreen, SECTION_WIDGET_CLASS).setVariables(true, {$uiWriteoff})
  }
  
  
}