package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanCloneToolbarButtonSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class BillingPlanCloneToolbarButtonSet_default extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($billingPlan :  BillingPlan) : void {
    __widgetOf(this, pcf.BillingPlanCloneToolbarButtonSet_default, SECTION_WIDGET_CLASS).setVariables(false, {$billingPlan})
  }
  
  function refreshVariables ($billingPlan :  BillingPlan) : void {
    __widgetOf(this, pcf.BillingPlanCloneToolbarButtonSet_default, SECTION_WIDGET_CLASS).setVariables(true, {$billingPlan})
  }
  
  
}