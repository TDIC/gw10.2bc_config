package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewProducerWizardBasicStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewProducerWizardBasicStepScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($producer :  Producer) : void {
    __widgetOf(this, pcf.NewProducerWizardBasicStepScreen, SECTION_WIDGET_CLASS).setVariables(false, {$producer})
  }
  
  function refreshVariables ($producer :  Producer) : void {
    __widgetOf(this, pcf.NewProducerWizardBasicStepScreen, SECTION_WIDGET_CLASS).setVariables(true, {$producer})
  }
  
  
}