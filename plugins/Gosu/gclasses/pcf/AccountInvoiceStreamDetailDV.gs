package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountInvoiceStreamDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountInvoiceStreamDetailDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($account :  Account, $invoiceStreamView :  gw.api.web.invoice.InvoiceStreamView) : void {
    __widgetOf(this, pcf.AccountInvoiceStreamDetailDV, SECTION_WIDGET_CLASS).setVariables(false, {$account, $invoiceStreamView})
  }
  
  function refreshVariables ($account :  Account, $invoiceStreamView :  gw.api.web.invoice.InvoiceStreamView) : void {
    __widgetOf(this, pcf.AccountInvoiceStreamDetailDV, SECTION_WIDGET_CLASS).setVariables(true, {$account, $invoiceStreamView})
  }
  
  
}