package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailCommissionsProducerInfoInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyDetailCommissionsProducerInfoInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyPeriod :  PolicyPeriod, $role :  PolicyRole) : void {
    __widgetOf(this, pcf.PolicyDetailCommissionsProducerInfoInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$policyPeriod, $role})
  }
  
  function refreshVariables ($policyPeriod :  PolicyPeriod, $role :  PolicyRole) : void {
    __widgetOf(this, pcf.PolicyDetailCommissionsProducerInfoInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$policyPeriod, $role})
  }
  
  
}