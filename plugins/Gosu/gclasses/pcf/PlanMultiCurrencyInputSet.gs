package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/multicurrencyplans/PlanMultiCurrencyInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PlanMultiCurrencyInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($plan :  MultiCurrencyPlan) : void {
    __widgetOf(this, pcf.PlanMultiCurrencyInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$plan})
  }
  
  function refreshVariables ($plan :  MultiCurrencyPlan) : void {
    __widgetOf(this, pcf.PlanMultiCurrencyInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$plan})
  }
  
  
}