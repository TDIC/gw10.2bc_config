package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessGracePeriodInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyProcessGracePeriodInputSet_default extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($delinquencyProcess :  entity.DelinquencyProcess) : void {
    __widgetOf(this, pcf.DelinquencyProcessGracePeriodInputSet_default, SECTION_WIDGET_CLASS).setVariables(false, {$delinquencyProcess})
  }
  
  function refreshVariables ($delinquencyProcess :  entity.DelinquencyProcess) : void {
    __widgetOf(this, pcf.DelinquencyProcessGracePeriodInputSet_default, SECTION_WIDGET_CLASS).setVariables(true, {$delinquencyProcess})
  }
  
  
}