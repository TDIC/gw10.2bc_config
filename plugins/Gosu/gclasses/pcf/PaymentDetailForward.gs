package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/PaymentDetailForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PaymentDetailForward extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (paymentReceipt :  PaymentReceipt) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PaymentDetailForward, {paymentReceipt}, 0)
  }
  
  static function drilldown (paymentReceipt :  PaymentReceipt) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentDetailForward, {paymentReceipt}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (paymentReceipt :  PaymentReceipt) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentDetailForward, {paymentReceipt}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (paymentReceipt :  PaymentReceipt) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentDetailForward, {paymentReceipt}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (paymentReceipt :  PaymentReceipt) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentDetailForward, {paymentReceipt}, 0).goInWorkspace()
  }
  
  static function printPage (paymentReceipt :  PaymentReceipt) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentDetailForward, {paymentReceipt}, 0).printPage()
  }
  
  static function push (paymentReceipt :  PaymentReceipt) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentDetailForward, {paymentReceipt}, 0).push()
  }
  
  
}