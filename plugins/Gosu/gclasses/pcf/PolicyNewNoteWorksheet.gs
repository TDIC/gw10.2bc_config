package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyNewNoteWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyNewNoteWorksheet extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (policyPeriod :  PolicyPeriod) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PolicyNewNoteWorksheet, {policyPeriod}, 0)
  }
  
  static function createDestination (policyPeriod :  PolicyPeriod, noteToEdit :  Note) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PolicyNewNoteWorksheet, {policyPeriod, noteToEdit}, 1)
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (policyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyNewNoteWorksheet, {policyPeriod}, 0).goInWorkspace()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (policyPeriod :  PolicyPeriod, noteToEdit :  Note) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyNewNoteWorksheet, {policyPeriod, noteToEdit}, 1).goInWorkspace()
  }
  
  static function push (policyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyNewNoteWorksheet, {policyPeriod}, 0).push()
  }
  
  static function push (policyPeriod :  PolicyPeriod, noteToEdit :  Note) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyNewNoteWorksheet, {policyPeriod, noteToEdit}, 1).push()
  }
  
  
}