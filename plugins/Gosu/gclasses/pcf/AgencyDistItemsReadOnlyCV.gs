package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyDistItemsReadOnlyCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyDistItemsReadOnlyCV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($agencyCycleDist :  AgencyCycleDist) : void {
    __widgetOf(this, pcf.AgencyDistItemsReadOnlyCV, SECTION_WIDGET_CLASS).setVariables(false, {$agencyCycleDist})
  }
  
  function refreshVariables ($agencyCycleDist :  AgencyCycleDist) : void {
    __widgetOf(this, pcf.AgencyDistItemsReadOnlyCV, SECTION_WIDGET_CLASS).setVariables(true, {$agencyCycleDist})
  }
  
  
}