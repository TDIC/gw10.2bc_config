package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyPaymentSplitConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyPaymentSplitConfirmationScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($agencySplitPayment :  gw.api.web.producer.agencybill.AgencySplitPayment, $alertBarMessageText :  String) : void {
    __widgetOf(this, pcf.AgencyPaymentSplitConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(false, {$agencySplitPayment, $alertBarMessageText})
  }
  
  function refreshVariables ($agencySplitPayment :  gw.api.web.producer.agencybill.AgencySplitPayment, $alertBarMessageText :  String) : void {
    __widgetOf(this, pcf.AgencyPaymentSplitConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(true, {$agencySplitPayment, $alertBarMessageText})
  }
  
  
}