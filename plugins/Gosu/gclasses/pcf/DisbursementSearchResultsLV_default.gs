package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DisbursementSearchResultsLV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DisbursementSearchResultsLV_default extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($disbursementSearchViews :  gw.api.database.IQueryBeanResult<DisbursementSearchView>) : void {
    __widgetOf(this, pcf.DisbursementSearchResultsLV_default, SECTION_WIDGET_CLASS).setVariables(false, {$disbursementSearchViews})
  }
  
  function refreshVariables ($disbursementSearchViews :  gw.api.database.IQueryBeanResult<DisbursementSearchView>) : void {
    __widgetOf(this, pcf.DisbursementSearchResultsLV_default, SECTION_WIDGET_CLASS).setVariables(true, {$disbursementSearchViews})
  }
  
  
}