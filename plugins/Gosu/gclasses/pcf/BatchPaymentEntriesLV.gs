package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/BatchPaymentEntriesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class BatchPaymentEntriesLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($batchPaymentDetailsView :  gw.web.payment.batch.BatchPaymentDetailsView) : void {
    __widgetOf(this, pcf.BatchPaymentEntriesLV, SECTION_WIDGET_CLASS).setVariables(false, {$batchPaymentDetailsView})
  }
  
  function refreshVariables ($batchPaymentDetailsView :  gw.web.payment.batch.BatchPaymentDetailsView) : void {
    __widgetOf(this, pcf.BatchPaymentEntriesLV, SECTION_WIDGET_CLASS).setVariables(true, {$batchPaymentDetailsView})
  }
  
  
}