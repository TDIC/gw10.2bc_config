package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/chargepatterns/NewChargePattern.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewChargePattern extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (patternType :  String) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewChargePattern, {patternType}, 0)
  }
  
  static function drilldown (patternType :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewChargePattern, {patternType}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (patternType :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewChargePattern, {patternType}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (patternType :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewChargePattern, {patternType}, 0).goInMain()
  }
  
  static function printPage (patternType :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewChargePattern, {patternType}, 0).printPage()
  }
  
  static function push (patternType :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewChargePattern, {patternType}, 0).push()
  }
  
  
}