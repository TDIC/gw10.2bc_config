package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/NewPolicyContactPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewPolicyContactPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (policyPeriod :  PolicyPeriod, contactSubtype :  Type<Contact>) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewPolicyContactPopup, {policyPeriod, contactSubtype}, 0)
  }
  
  function pickValueAndCommit (value :  PolicyPeriodContact) : void {
    __widgetOf(this, pcf.NewPolicyContactPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (policyPeriod :  PolicyPeriod, contactSubtype :  Type<Contact>) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewPolicyContactPopup, {policyPeriod, contactSubtype}, 0).push()
  }
  
  
}