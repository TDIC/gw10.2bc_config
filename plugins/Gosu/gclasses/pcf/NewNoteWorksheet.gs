package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/note/NewNoteWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewNoteWorksheet extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (srcBean :  KeyableBean, noteContainer :  NoteContainer) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewNoteWorksheet, {srcBean, noteContainer}, 0)
  }
  
  static function createDestination (srcBean :  KeyableBean, noteContainer :  NoteContainer, noteTemplateStr :  String) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewNoteWorksheet, {srcBean, noteContainer, noteTemplateStr}, 1)
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (srcBean :  KeyableBean, noteContainer :  NoteContainer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewNoteWorksheet, {srcBean, noteContainer}, 0).goInWorkspace()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (srcBean :  KeyableBean, noteContainer :  NoteContainer, noteTemplateStr :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewNoteWorksheet, {srcBean, noteContainer, noteTemplateStr}, 1).goInWorkspace()
  }
  
  static function push (srcBean :  KeyableBean, noteContainer :  NoteContainer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewNoteWorksheet, {srcBean, noteContainer}, 0).push()
  }
  
  static function push (srcBean :  KeyableBean, noteContainer :  NoteContainer, noteTemplateStr :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewNoteWorksheet, {srcBean, noteContainer, noteTemplateStr}, 1).push()
  }
  
  
}