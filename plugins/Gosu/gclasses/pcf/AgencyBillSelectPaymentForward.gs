package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillSelectPaymentForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyBillSelectPaymentForward extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (producer :  Producer, agencyMoneyReceived :  AgencyBillMoneyRcvd) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyBillSelectPaymentForward, {producer, agencyMoneyReceived}, 0)
  }
  
  static function drilldown (producer :  Producer, agencyMoneyReceived :  AgencyBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillSelectPaymentForward, {producer, agencyMoneyReceived}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (producer :  Producer, agencyMoneyReceived :  AgencyBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillSelectPaymentForward, {producer, agencyMoneyReceived}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (producer :  Producer, agencyMoneyReceived :  AgencyBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillSelectPaymentForward, {producer, agencyMoneyReceived}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (producer :  Producer, agencyMoneyReceived :  AgencyBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillSelectPaymentForward, {producer, agencyMoneyReceived}, 0).goInWorkspace()
  }
  
  static function printPage (producer :  Producer, agencyMoneyReceived :  AgencyBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillSelectPaymentForward, {producer, agencyMoneyReceived}, 0).printPage()
  }
  
  static function push (producer :  Producer, agencyMoneyReceived :  AgencyBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillSelectPaymentForward, {producer, agencyMoneyReceived}, 0).push()
  }
  
  
}