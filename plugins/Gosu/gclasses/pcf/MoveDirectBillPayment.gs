package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/MoveDirectBillPayment.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class MoveDirectBillPayment extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (account :  Account, originalMoney :  DirectBillMoneyRcvd) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.MoveDirectBillPayment, {account, originalMoney}, 0)
  }
  
  static function drilldown (account :  Account, originalMoney :  DirectBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.MoveDirectBillPayment, {account, originalMoney}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (account :  Account, originalMoney :  DirectBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.MoveDirectBillPayment, {account, originalMoney}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (account :  Account, originalMoney :  DirectBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.MoveDirectBillPayment, {account, originalMoney}, 0).goInMain()
  }
  
  static function printPage (account :  Account, originalMoney :  DirectBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.MoveDirectBillPayment, {account, originalMoney}, 0).printPage()
  }
  
  static function push (account :  Account, originalMoney :  DirectBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.MoveDirectBillPayment, {account, originalMoney}, 0).push()
  }
  
  
}