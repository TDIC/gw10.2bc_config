package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/systemAdmin/Acct360EditRowPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class Acct360EditRowPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (acctBalanceRow :  AccountBalanceTxn_ext) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.Acct360EditRowPopup, {acctBalanceRow}, 0)
  }
  
  static function push (acctBalanceRow :  AccountBalanceTxn_ext) : pcf.api.Location {
    return __newDestinationForLocation(pcf.Acct360EditRowPopup, {acctBalanceRow}, 0).push()
  }
  
  
}