package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultipleAccountsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class SelectMultipleAccountsScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter () : void {
    __widgetOf(this, pcf.SelectMultipleAccountsScreen, SECTION_WIDGET_CLASS).setVariables(false, {})
  }
  
  function refreshVariables () : void {
    __widgetOf(this, pcf.SelectMultipleAccountsScreen, SECTION_WIDGET_CLASS).setVariables(true, {})
  }
  
  
}