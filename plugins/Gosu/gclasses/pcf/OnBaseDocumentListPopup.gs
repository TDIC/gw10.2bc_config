package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseDocumentListPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class OnBaseDocumentListPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (Entity :  KeyableBean, LinkType :  acc.onbase.configuration.DocumentLinkType, EntityDescription :  java.lang.String, Beans :  KeyableBean[]) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.OnBaseDocumentListPopup, {Entity, LinkType, EntityDescription, Beans}, 0)
  }
  
  static function push (Entity :  KeyableBean, LinkType :  acc.onbase.configuration.DocumentLinkType, EntityDescription :  java.lang.String, Beans :  KeyableBean[]) : pcf.api.Location {
    return __newDestinationForLocation(pcf.OnBaseDocumentListPopup, {Entity, LinkType, EntityDescription, Beans}, 0).push()
  }
  
  
}