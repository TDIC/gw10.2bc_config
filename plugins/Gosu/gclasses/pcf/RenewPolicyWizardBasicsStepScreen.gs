package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/RenewPolicyWizardBasicsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class RenewPolicyWizardBasicsStepScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($prevPolicyPeriod :  PolicyPeriod, $newPolicyPeriod :  PolicyPeriod, $producerCodeRoleEntries :  ProducerCodeRoleEntry[], $invoicingOverridesView :  gw.invoice.InvoicingOverridesView, $renewal :  Renewal) : void {
    __widgetOf(this, pcf.RenewPolicyWizardBasicsStepScreen, SECTION_WIDGET_CLASS).setVariables(false, {$prevPolicyPeriod, $newPolicyPeriod, $producerCodeRoleEntries, $invoicingOverridesView, $renewal})
  }
  
  function refreshVariables ($prevPolicyPeriod :  PolicyPeriod, $newPolicyPeriod :  PolicyPeriod, $producerCodeRoleEntries :  ProducerCodeRoleEntry[], $invoicingOverridesView :  gw.invoice.InvoicingOverridesView, $renewal :  Renewal) : void {
    __widgetOf(this, pcf.RenewPolicyWizardBasicsStepScreen, SECTION_WIDGET_CLASS).setVariables(true, {$prevPolicyPeriod, $newPolicyPeriod, $producerCodeRoleEntries, $invoicingOverridesView, $renewal})
  }
  
  
}