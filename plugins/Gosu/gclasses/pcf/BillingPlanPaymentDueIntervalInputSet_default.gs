package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanPaymentDueIntervalInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class BillingPlanPaymentDueIntervalInputSet_default extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($billingPlan :  BillingPlan, $planNotInUse :  boolean) : void {
    __widgetOf(this, pcf.BillingPlanPaymentDueIntervalInputSet_default, SECTION_WIDGET_CLASS).setVariables(false, {$billingPlan, $planNotInUse})
  }
  
  function refreshVariables ($billingPlan :  BillingPlan, $planNotInUse :  boolean) : void {
    __widgetOf(this, pcf.BillingPlanPaymentDueIntervalInputSet_default, SECTION_WIDGET_CLASS).setVariables(true, {$billingPlan, $planNotInUse})
  }
  
  
}