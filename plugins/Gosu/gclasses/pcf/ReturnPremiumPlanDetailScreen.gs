package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/invoice/ReturnPremiumPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ReturnPremiumPlanDetailScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($returnPremiumPlan :  ReturnPremiumPlan) : void {
    __widgetOf(this, pcf.ReturnPremiumPlanDetailScreen, SECTION_WIDGET_CLASS).setVariables(false, {$returnPremiumPlan})
  }
  
  function refreshVariables ($returnPremiumPlan :  ReturnPremiumPlan) : void {
    __widgetOf(this, pcf.ReturnPremiumPlanDetailScreen, SECTION_WIDGET_CLASS).setVariables(true, {$returnPremiumPlan})
  }
  
  
}