package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillStatementTotalsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyBillStatementTotalsDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($agencyBillStatementView :  gw.api.web.invoice.AgencyBillStatementView) : void {
    __widgetOf(this, pcf.AgencyBillStatementTotalsDV, SECTION_WIDGET_CLASS).setVariables(false, {$agencyBillStatementView})
  }
  
  function refreshVariables ($agencyBillStatementView :  gw.api.web.invoice.AgencyBillStatementView) : void {
    __widgetOf(this, pcf.AgencyBillStatementTotalsDV, SECTION_WIDGET_CLASS).setVariables(true, {$agencyBillStatementView})
  }
  
  
}