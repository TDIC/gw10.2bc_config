package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalChargeSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewChargeReversalChargeSearchScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($reversal :  ChargeReversal) : void {
    __widgetOf(this, pcf.NewChargeReversalChargeSearchScreen, SECTION_WIDGET_CLASS).setVariables(false, {$reversal})
  }
  
  function refreshVariables ($reversal :  ChargeReversal) : void {
    __widgetOf(this, pcf.NewChargeReversalChargeSearchScreen, SECTION_WIDGET_CLASS).setVariables(true, {$reversal})
  }
  
  
}