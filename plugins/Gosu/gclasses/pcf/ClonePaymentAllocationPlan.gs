package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/allocationplan/ClonePaymentAllocationPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ClonePaymentAllocationPlan extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (paymentAllocationPlan :  PaymentAllocationPlan) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ClonePaymentAllocationPlan, {paymentAllocationPlan}, 0)
  }
  
  static function drilldown (paymentAllocationPlan :  PaymentAllocationPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ClonePaymentAllocationPlan, {paymentAllocationPlan}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (paymentAllocationPlan :  PaymentAllocationPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ClonePaymentAllocationPlan, {paymentAllocationPlan}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (paymentAllocationPlan :  PaymentAllocationPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ClonePaymentAllocationPlan, {paymentAllocationPlan}, 0).goInMain()
  }
  
  static function printPage (paymentAllocationPlan :  PaymentAllocationPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ClonePaymentAllocationPlan, {paymentAllocationPlan}, 0).printPage()
  }
  
  static function push (paymentAllocationPlan :  PaymentAllocationPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ClonePaymentAllocationPlan, {paymentAllocationPlan}, 0).push()
  }
  
  
}