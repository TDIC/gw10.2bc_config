package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/accounting/TransactionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TransactionsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($transactions :  gw.api.database.IQueryBeanResult<Transaction>, $showCheckboxes :  Boolean) : void {
    __widgetOf(this, pcf.TransactionsLV, SECTION_WIDGET_CLASS).setVariables(false, {$transactions, $showCheckboxes})
  }
  
  function refreshVariables ($transactions :  gw.api.database.IQueryBeanResult<Transaction>, $showCheckboxes :  Boolean) : void {
    __widgetOf(this, pcf.TransactionsLV, SECTION_WIDGET_CLASS).setVariables(true, {$transactions, $showCheckboxes})
  }
  
  
}