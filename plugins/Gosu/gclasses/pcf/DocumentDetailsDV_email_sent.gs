package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/DocumentDetailsDV.email_sent.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DocumentDetailsDV_email_sent extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($document :  Document) : void {
    __widgetOf(this, pcf.DocumentDetailsDV_email_sent, SECTION_WIDGET_CLASS).setVariables(false, {$document})
  }
  
  function refreshVariables ($document :  Document) : void {
    __widgetOf(this, pcf.DocumentDetailsDV_email_sent, SECTION_WIDGET_CLASS).setVariables(true, {$document})
  }
  
  
}