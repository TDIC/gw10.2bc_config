package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralRequirementSegregatePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CollateralRequirementSegregatePopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (collateralRequirement :  CollateralRequirement) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.CollateralRequirementSegregatePopup, {collateralRequirement}, 0)
  }
  
  static function push (collateralRequirement :  CollateralRequirement) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CollateralRequirementSegregatePopup, {collateralRequirement}, 0).push()
  }
  
  
}