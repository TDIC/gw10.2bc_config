package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalConfirmationDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewChargeReversalConfirmationDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($reversal :  ChargeReversal) : void {
    __widgetOf(this, pcf.NewChargeReversalConfirmationDV, SECTION_WIDGET_CLASS).setVariables(false, {$reversal})
  }
  
  function refreshVariables ($reversal :  ChargeReversal) : void {
    __widgetOf(this, pcf.NewChargeReversalConfirmationDV, SECTION_WIDGET_CLASS).setVariables(true, {$reversal})
  }
  
  
}