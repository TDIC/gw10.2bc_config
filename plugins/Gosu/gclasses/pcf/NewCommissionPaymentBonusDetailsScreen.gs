package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentBonusDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewCommissionPaymentBonusDetailsScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($bonusPayment :  BonusCmsnPayment, $producer :  Producer) : void {
    __widgetOf(this, pcf.NewCommissionPaymentBonusDetailsScreen, SECTION_WIDGET_CLASS).setVariables(false, {$bonusPayment, $producer})
  }
  
  function refreshVariables ($bonusPayment :  BonusCmsnPayment, $producer :  Producer) : void {
    __widgetOf(this, pcf.NewCommissionPaymentBonusDetailsScreen, SECTION_WIDGET_CLASS).setVariables(true, {$bonusPayment, $producer})
  }
  
  
}