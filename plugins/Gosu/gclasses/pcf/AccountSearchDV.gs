package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/AccountSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountSearchDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($searchCriteria :  gw.search.AccountSearchCriteria) : void {
    __widgetOf(this, pcf.AccountSearchDV, SECTION_WIDGET_CLASS).setVariables(false, {$searchCriteria})
  }
  
  function refreshVariables ($searchCriteria :  gw.search.AccountSearchCriteria) : void {
    __widgetOf(this, pcf.AccountSearchDV, SECTION_WIDGET_CLASS).setVariables(true, {$searchCriteria})
  }
  
  
}