package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/activityenhancement/desktop/ActivityLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ActivityLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($activities :  java.util.List<Activity>) : void {
    __widgetOf(this, pcf.ActivityLV, SECTION_WIDGET_CLASS).setVariables(false, {$activities})
  }
  
  function refreshVariables ($activities :  java.util.List<Activity>) : void {
    __widgetOf(this, pcf.ActivityLV, SECTION_WIDGET_CLASS).setVariables(true, {$activities})
  }
  
  
}