package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/InvoiceStreamAnchorDateInputSet.everyotherweek.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class InvoiceStreamAnchorDateInputSet_everyotherweek extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($invoiceDayChangeHelper :  gw.api.web.invoice.InvoiceDayChangeHelper, $isEditMode :  boolean) : void {
    __widgetOf(this, pcf.InvoiceStreamAnchorDateInputSet_everyotherweek, SECTION_WIDGET_CLASS).setVariables(false, {$invoiceDayChangeHelper, $isEditMode})
  }
  
  function refreshVariables ($invoiceDayChangeHelper :  gw.api.web.invoice.InvoiceDayChangeHelper, $isEditMode :  boolean) : void {
    __widgetOf(this, pcf.InvoiceStreamAnchorDateInputSet_everyotherweek, SECTION_WIDGET_CLASS).setVariables(true, {$invoiceDayChangeHelper, $isEditMode})
  }
  
  
}