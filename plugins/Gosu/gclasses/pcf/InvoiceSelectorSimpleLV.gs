package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/InvoiceSelectorSimpleLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class InvoiceSelectorSimpleLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($payer :  gw.api.domain.invoice.InvoicePayer) : void {
    __widgetOf(this, pcf.InvoiceSelectorSimpleLV, SECTION_WIDGET_CLASS).setVariables(false, {$payer})
  }
  
  function refreshVariables ($payer :  gw.api.domain.invoice.InvoicePayer) : void {
    __widgetOf(this, pcf.InvoiceSelectorSimpleLV, SECTION_WIDGET_CLASS).setVariables(true, {$payer})
  }
  
  
}