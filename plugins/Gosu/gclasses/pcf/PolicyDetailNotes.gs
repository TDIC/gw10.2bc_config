package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailNotes.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyDetailNotes extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (policyPeriod :  PolicyPeriod, isClearBundle :  Boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PolicyDetailNotes, {policyPeriod, isClearBundle}, 0)
  }
  
  static function createDestination (policyPeriod :  PolicyPeriod, isClearBundle :  Boolean, latestNote :  Note) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PolicyDetailNotes, {policyPeriod, isClearBundle, latestNote}, 1)
  }
  
  static function drilldown (policyPeriod :  PolicyPeriod, isClearBundle :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailNotes, {policyPeriod, isClearBundle}, 0).drilldown()
  }
  
  static function drilldown (policyPeriod :  PolicyPeriod, isClearBundle :  Boolean, latestNote :  Note) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailNotes, {policyPeriod, isClearBundle, latestNote}, 1).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (policyPeriod :  PolicyPeriod, isClearBundle :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailNotes, {policyPeriod, isClearBundle}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (policyPeriod :  PolicyPeriod, isClearBundle :  Boolean, latestNote :  Note) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailNotes, {policyPeriod, isClearBundle, latestNote}, 1).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (policyPeriod :  PolicyPeriod, isClearBundle :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailNotes, {policyPeriod, isClearBundle}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (policyPeriod :  PolicyPeriod, isClearBundle :  Boolean, latestNote :  Note) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailNotes, {policyPeriod, isClearBundle, latestNote}, 1).goInMain()
  }
  
  static function printPage (policyPeriod :  PolicyPeriod, isClearBundle :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailNotes, {policyPeriod, isClearBundle}, 0).printPage()
  }
  
  static function printPage (policyPeriod :  PolicyPeriod, isClearBundle :  Boolean, latestNote :  Note) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailNotes, {policyPeriod, isClearBundle, latestNote}, 1).printPage()
  }
  
  static function push (policyPeriod :  PolicyPeriod, isClearBundle :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailNotes, {policyPeriod, isClearBundle}, 0).push()
  }
  
  static function push (policyPeriod :  PolicyPeriod, isClearBundle :  Boolean, latestNote :  Note) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailNotes, {policyPeriod, isClearBundle, latestNote}, 1).push()
  }
  
  
}