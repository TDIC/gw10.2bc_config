package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/ProducerTransferDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducerTransferDetail extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (history :  ProducerPolicyHistory) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ProducerTransferDetail, {history}, 0)
  }
  
  static function drilldown (history :  ProducerPolicyHistory) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerTransferDetail, {history}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (history :  ProducerPolicyHistory) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerTransferDetail, {history}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (history :  ProducerPolicyHistory) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerTransferDetail, {history}, 0).goInMain()
  }
  
  static function printPage (history :  ProducerPolicyHistory) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerTransferDetail, {history}, 0).printPage()
  }
  
  static function push (history :  ProducerPolicyHistory) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerTransferDetail, {history}, 0).push()
  }
  
  
}