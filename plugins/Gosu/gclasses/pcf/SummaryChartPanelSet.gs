package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/summary/SummaryChartPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class SummaryChartPanelSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($summaryChartEntries :  List<gw.web.summary.SummaryChartEntry>) : void {
    __widgetOf(this, pcf.SummaryChartPanelSet, SECTION_WIDGET_CLASS).setVariables(false, {$summaryChartEntries})
  }
  
  function refreshVariables ($summaryChartEntries :  List<gw.web.summary.SummaryChartEntry>) : void {
    __widgetOf(this, pcf.SummaryChartPanelSet, SECTION_WIDGET_CLASS).setVariables(true, {$summaryChartEntries})
  }
  
  
}