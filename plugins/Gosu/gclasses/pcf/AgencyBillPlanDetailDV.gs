package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/agencybill/AgencyBillPlanDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyBillPlanDetailDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($agencyBillPlan :  AgencyBillPlan) : void {
    __widgetOf(this, pcf.AgencyBillPlanDetailDV, SECTION_WIDGET_CLASS).setVariables(false, {$agencyBillPlan})
  }
  
  function refreshVariables ($agencyBillPlan :  AgencyBillPlan) : void {
    __widgetOf(this, pcf.AgencyBillPlanDetailDV, SECTION_WIDGET_CLASS).setVariables(true, {$agencyBillPlan})
  }
  
  
}