package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contact/ContactSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ContactSearchPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (isProducerContact :  boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ContactSearchPopup, {isProducerContact}, 0)
  }
  
  function pickValueAndCommit (value :  gw.plugin.contact.ContactResult) : void {
    __widgetOf(this, pcf.ContactSearchPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (isProducerContact :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ContactSearchPopup, {isProducerContact}, 0).push()
  }
  
  
}