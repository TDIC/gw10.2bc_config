package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_DispositionsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyDistributionWizard_DispositionsScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : void {
    __widgetOf(this, pcf.AgencyDistributionWizard_DispositionsScreen, SECTION_WIDGET_CLASS).setVariables(false, {$wizardState})
  }
  
  function refreshVariables ($wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : void {
    __widgetOf(this, pcf.AgencyDistributionWizard_DispositionsScreen, SECTION_WIDGET_CLASS).setVariables(true, {$wizardState})
  }
  
  
}