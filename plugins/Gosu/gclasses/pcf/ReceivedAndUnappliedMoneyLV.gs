package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/ReceivedAndUnappliedMoneyLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ReceivedAndUnappliedMoneyLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($unappliedPayments :  gw.api.database.IQueryBeanResult<PaymentMoneyReceived>) : void {
    __widgetOf(this, pcf.ReceivedAndUnappliedMoneyLV, SECTION_WIDGET_CLASS).setVariables(false, {$unappliedPayments})
  }
  
  function refreshVariables ($unappliedPayments :  gw.api.database.IQueryBeanResult<PaymentMoneyReceived>) : void {
    __widgetOf(this, pcf.ReceivedAndUnappliedMoneyLV, SECTION_WIDGET_CLASS).setVariables(true, {$unappliedPayments})
  }
  
  
}