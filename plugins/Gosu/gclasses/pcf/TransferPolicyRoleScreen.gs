package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyRoleScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TransferPolicyRoleScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyTransfer :  PolTransferByRole) : void {
    __widgetOf(this, pcf.TransferPolicyRoleScreen, SECTION_WIDGET_CLASS).setVariables(false, {$policyTransfer})
  }
  
  function refreshVariables ($policyTransfer :  PolTransferByRole) : void {
    __widgetOf(this, pcf.TransferPolicyRoleScreen, SECTION_WIDGET_CLASS).setVariables(true, {$policyTransfer})
  }
  
  
}