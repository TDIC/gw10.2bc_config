package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class BillingPlanDetailPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (billingPlan :  BillingPlan) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.BillingPlanDetailPopup, {billingPlan}, 0)
  }
  
  static function push (billingPlan :  BillingPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.BillingPlanDetailPopup, {billingPlan}, 0).push()
  }
  
  
}