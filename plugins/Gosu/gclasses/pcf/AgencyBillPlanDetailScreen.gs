package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/agencybill/AgencyBillPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyBillPlanDetailScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($agencyBillPlan :  AgencyBillPlan) : void {
    __widgetOf(this, pcf.AgencyBillPlanDetailScreen, SECTION_WIDGET_CLASS).setVariables(false, {$agencyBillPlan})
  }
  
  function refreshVariables ($agencyBillPlan :  AgencyBillPlan) : void {
    __widgetOf(this, pcf.AgencyBillPlanDetailScreen, SECTION_WIDGET_CLASS).setVariables(true, {$agencyBillPlan})
  }
  
  
}