package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/AccountInvoiceInformationInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountInvoiceInformationInputSet_default extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($invoice :  AccountInvoice) : void {
    __widgetOf(this, pcf.AccountInvoiceInformationInputSet_default, SECTION_WIDGET_CLASS).setVariables(false, {$invoice})
  }
  
  function refreshVariables ($invoice :  AccountInvoice) : void {
    __widgetOf(this, pcf.AccountInvoiceInformationInputSet_default, SECTION_WIDGET_CLASS).setVariables(true, {$invoice})
  }
  
  
}