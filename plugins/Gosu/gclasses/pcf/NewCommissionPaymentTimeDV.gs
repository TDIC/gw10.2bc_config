package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentTimeDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewCommissionPaymentTimeDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($delayedPayment :  DelayedCmsnPayment, $canEdit :  Boolean) : void {
    __widgetOf(this, pcf.NewCommissionPaymentTimeDV, SECTION_WIDGET_CLASS).setVariables(false, {$delayedPayment, $canEdit})
  }
  
  function refreshVariables ($delayedPayment :  DelayedCmsnPayment, $canEdit :  Boolean) : void {
    __widgetOf(this, pcf.NewCommissionPaymentTimeDV, SECTION_WIDGET_CLASS).setVariables(true, {$delayedPayment, $canEdit})
  }
  
  
}