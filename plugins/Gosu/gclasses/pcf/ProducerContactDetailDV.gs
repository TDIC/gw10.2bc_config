package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerContactDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducerContactDetailDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($producerContact :  ProducerContact, $isNew :  Boolean) : void {
    __widgetOf(this, pcf.ProducerContactDetailDV, SECTION_WIDGET_CLASS).setVariables(false, {$producerContact, $isNew})
  }
  
  function refreshVariables ($producerContact :  ProducerContact, $isNew :  Boolean) : void {
    __widgetOf(this, pcf.ProducerContactDetailDV, SECTION_WIDGET_CLASS).setVariables(true, {$producerContact, $isNew})
  }
  
  
}