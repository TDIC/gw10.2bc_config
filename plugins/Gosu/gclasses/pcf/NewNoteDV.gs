package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/note/NewNoteDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewNoteDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($Note :  Note) : void {
    __widgetOf(this, pcf.NewNoteDV, SECTION_WIDGET_CLASS).setVariables(false, {$Note})
  }
  
  function refreshVariables ($Note :  Note) : void {
    __widgetOf(this, pcf.NewNoteDV, SECTION_WIDGET_CLASS).setVariables(true, {$Note})
  }
  
  
}