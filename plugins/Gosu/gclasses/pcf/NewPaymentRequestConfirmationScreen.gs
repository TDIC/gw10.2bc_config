package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/NewPaymentRequestConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewPaymentRequestConfirmationScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($paymentRequest :  PaymentRequest) : void {
    __widgetOf(this, pcf.NewPaymentRequestConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(false, {$paymentRequest})
  }
  
  function refreshVariables ($paymentRequest :  PaymentRequest) : void {
    __widgetOf(this, pcf.NewPaymentRequestConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(true, {$paymentRequest})
  }
  
  
}