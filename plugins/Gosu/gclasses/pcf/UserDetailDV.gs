package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/users/UserDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class UserDetailDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($user :  User, $isNew :  Boolean) : void {
    __widgetOf(this, pcf.UserDetailDV, SECTION_WIDGET_CLASS).setVariables(false, {$user, $isNew})
  }
  
  function refreshVariables ($user :  User, $isNew :  Boolean) : void {
    __widgetOf(this, pcf.UserDetailDV, SECTION_WIDGET_CLASS).setVariables(true, {$user, $isNew})
  }
  
  
}