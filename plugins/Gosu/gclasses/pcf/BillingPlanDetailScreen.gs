package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class BillingPlanDetailScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($billingPlan :  BillingPlan) : void {
    __widgetOf(this, pcf.BillingPlanDetailScreen, SECTION_WIDGET_CLASS).setVariables(false, {$billingPlan})
  }
  
  function refreshVariables ($billingPlan :  BillingPlan) : void {
    __widgetOf(this, pcf.BillingPlanDetailScreen, SECTION_WIDGET_CLASS).setVariables(true, {$billingPlan})
  }
  
  
}