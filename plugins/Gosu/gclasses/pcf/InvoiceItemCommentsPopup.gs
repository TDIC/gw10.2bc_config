package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/InvoiceItemCommentsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class InvoiceItemCommentsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (item :  InvoiceItem, agencyBillStatementView :  gw.api.web.invoice.AgencyBillStatementView) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.InvoiceItemCommentsPopup, {item, agencyBillStatementView}, 0)
  }
  
  static function push (item :  InvoiceItem, agencyBillStatementView :  gw.api.web.invoice.AgencyBillStatementView) : pcf.api.Location {
    return __newDestinationForLocation(pcf.InvoiceItemCommentsPopup, {item, agencyBillStatementView}, 0).push()
  }
  
  
}