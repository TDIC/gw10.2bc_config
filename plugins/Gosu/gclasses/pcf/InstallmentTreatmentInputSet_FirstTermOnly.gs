package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/payment/InstallmentTreatmentInputSet.FirstTermOnly.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class InstallmentTreatmentInputSet_FirstTermOnly extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($viewHelper :  gw.admin.paymentplan.InstallmentViewHelper) : void {
    __widgetOf(this, pcf.InstallmentTreatmentInputSet_FirstTermOnly, SECTION_WIDGET_CLASS).setVariables(false, {$viewHelper})
  }
  
  function refreshVariables ($viewHelper :  gw.admin.paymentplan.InstallmentViewHelper) : void {
    __widgetOf(this, pcf.InstallmentTreatmentInputSet_FirstTermOnly, SECTION_WIDGET_CLASS).setVariables(true, {$viewHelper})
  }
  
  
}