package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/PolicyPeriodPanelLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyPeriodPanelLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyPeriods :  List<PolicyPeriod>) : void {
    __widgetOf(this, pcf.PolicyPeriodPanelLV, SECTION_WIDGET_CLASS).setVariables(false, {$policyPeriods})
  }
  
  function refreshVariables ($policyPeriods :  List<PolicyPeriod>) : void {
    __widgetOf(this, pcf.PolicyPeriodPanelLV, SECTION_WIDGET_CLASS).setVariables(true, {$policyPeriods})
  }
  
  
}