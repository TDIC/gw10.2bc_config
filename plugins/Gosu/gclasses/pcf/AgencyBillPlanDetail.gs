package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/agencybill/AgencyBillPlanDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyBillPlanDetail extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (agencyBillPlan :  AgencyBillPlan) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyBillPlanDetail, {agencyBillPlan}, 0)
  }
  
  static function drilldown (agencyBillPlan :  AgencyBillPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillPlanDetail, {agencyBillPlan}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (agencyBillPlan :  AgencyBillPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillPlanDetail, {agencyBillPlan}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (agencyBillPlan :  AgencyBillPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillPlanDetail, {agencyBillPlan}, 0).goInMain()
  }
  
  static function printPage (agencyBillPlan :  AgencyBillPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillPlanDetail, {agencyBillPlan}, 0).printPage()
  }
  
  static function push (agencyBillPlan :  AgencyBillPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillPlanDetail, {agencyBillPlan}, 0).push()
  }
  
  
}