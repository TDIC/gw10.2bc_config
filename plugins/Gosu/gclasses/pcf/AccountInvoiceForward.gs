package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountInvoiceForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountInvoiceForward extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (invoiceID :  String) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AccountInvoiceForward, {invoiceID}, 0)
  }
  
  static function drilldown (invoiceID :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountInvoiceForward, {invoiceID}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (invoiceID :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountInvoiceForward, {invoiceID}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (invoiceID :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountInvoiceForward, {invoiceID}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (invoiceID :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountInvoiceForward, {invoiceID}, 0).goInWorkspace()
  }
  
  static function printPage (invoiceID :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountInvoiceForward, {invoiceID}, 0).printPage()
  }
  
  static function push (invoiceID :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountInvoiceForward, {invoiceID}, 0).push()
  }
  
  
}