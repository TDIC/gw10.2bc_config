package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/RenewPolicyWizardChargeStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class RenewPolicyWizardChargeStepScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($renewal :  Renewal, $view :  gw.web.policy.PolicyWizardChargeStepScreenView) : void {
    __widgetOf(this, pcf.RenewPolicyWizardChargeStepScreen, SECTION_WIDGET_CLASS).setVariables(false, {$renewal, $view})
  }
  
  function refreshVariables ($renewal :  Renewal, $view :  gw.web.policy.PolicyWizardChargeStepScreenView) : void {
    __widgetOf(this, pcf.RenewPolicyWizardChargeStepScreen, SECTION_WIDGET_CLASS).setVariables(true, {$renewal, $view})
  }
  
  
}