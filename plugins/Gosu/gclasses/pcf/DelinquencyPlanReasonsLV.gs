package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanReasonsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyPlanReasonsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($dlnqPlan :  DelinquencyPlan, $planNotInUse :  boolean) : void {
    __widgetOf(this, pcf.DelinquencyPlanReasonsLV, SECTION_WIDGET_CLASS).setVariables(false, {$dlnqPlan, $planNotInUse})
  }
  
  function refreshVariables ($dlnqPlan :  DelinquencyPlan, $planNotInUse :  boolean) : void {
    __widgetOf(this, pcf.DelinquencyPlanReasonsLV, SECTION_WIDGET_CLASS).setVariables(true, {$dlnqPlan, $planNotInUse})
  }
  
  
}