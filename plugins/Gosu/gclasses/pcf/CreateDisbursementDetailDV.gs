package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/CreateDisbursementDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CreateDisbursementDetailDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($disbursementVar :  Disbursement, $isAmountEditable :  boolean) : void {
    __widgetOf(this, pcf.CreateDisbursementDetailDV, SECTION_WIDGET_CLASS).setVariables(false, {$disbursementVar, $isAmountEditable})
  }
  
  function refreshVariables ($disbursementVar :  Disbursement, $isAmountEditable :  boolean) : void {
    __widgetOf(this, pcf.CreateDisbursementDetailDV, SECTION_WIDGET_CLASS).setVariables(true, {$disbursementVar, $isAmountEditable})
  }
  
  
}