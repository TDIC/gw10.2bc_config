package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/collection/CollectionAgencyDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CollectionAgencyDetailPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (collectionAgency :  CollectionAgency) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.CollectionAgencyDetailPopup, {collectionAgency}, 0)
  }
  
  static function push (collectionAgency :  CollectionAgency) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CollectionAgencyDetailPopup, {collectionAgency}, 0).push()
  }
  
  
}