package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillDistArchiveWarningPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyBillDistArchiveWarningPanelSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($dist :  BaseDist) : void {
    __widgetOf(this, pcf.AgencyBillDistArchiveWarningPanelSet, SECTION_WIDGET_CLASS).setVariables(false, {$dist})
  }
  
  function refreshVariables ($dist :  BaseDist) : void {
    __widgetOf(this, pcf.AgencyBillDistArchiveWarningPanelSet, SECTION_WIDGET_CLASS).setVariables(true, {$dist})
  }
  
  
}