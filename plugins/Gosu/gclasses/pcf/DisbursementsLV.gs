package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/DisbursementsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DisbursementsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($disbursements :  List<Disbursement>, $initialSelectedDisbursement :  Disbursement) : void {
    __widgetOf(this, pcf.DisbursementsLV, SECTION_WIDGET_CLASS).setVariables(false, {$disbursements, $initialSelectedDisbursement})
  }
  
  function refreshVariables ($disbursements :  List<Disbursement>, $initialSelectedDisbursement :  Disbursement) : void {
    __widgetOf(this, pcf.DisbursementsLV, SECTION_WIDGET_CLASS).setVariables(true, {$disbursements, $initialSelectedDisbursement})
  }
  
  
}