package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/collection/CollectionAgencyDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CollectionAgencyDetailScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($collectionAgency :  CollectionAgency) : void {
    __widgetOf(this, pcf.CollectionAgencyDetailScreen, SECTION_WIDGET_CLASS).setVariables(false, {$collectionAgency})
  }
  
  function refreshVariables ($collectionAgency :  CollectionAgency) : void {
    __widgetOf(this, pcf.CollectionAgencyDetailScreen, SECTION_WIDGET_CLASS).setVariables(true, {$collectionAgency})
  }
  
  
}