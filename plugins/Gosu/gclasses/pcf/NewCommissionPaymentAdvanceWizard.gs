package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentAdvanceWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewCommissionPaymentAdvanceWizard extends com.guidewire.pl.web.codegen.WizardBase {
  static function createDestination (producer :  Producer) : pcf.api.Destination {
    return __newDestinationForWizard(pcf.NewCommissionPaymentAdvanceWizard, {producer}, 0)
  }
  
  static function drilldown (producer :  Producer) : pcf.api.Location {
    return __newDestinationForWizard(pcf.NewCommissionPaymentAdvanceWizard, {producer}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (producer :  Producer) : pcf.api.Location {
    return __newDestinationForWizard(pcf.NewCommissionPaymentAdvanceWizard, {producer}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (producer :  Producer) : pcf.api.Location {
    return __newDestinationForWizard(pcf.NewCommissionPaymentAdvanceWizard, {producer}, 0).goInMain()
  }
  
  static function printPage (producer :  Producer) : pcf.api.Location {
    return __newDestinationForWizard(pcf.NewCommissionPaymentAdvanceWizard, {producer}, 0).printPage()
  }
  
  static function push (producer :  Producer) : pcf.api.Location {
    return __newDestinationForWizard(pcf.NewCommissionPaymentAdvanceWizard, {producer}, 0).push()
  }
  
  
}