package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contact/DuplicateContactsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DuplicateContactsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (duplicateContactsPopupNavigator :  gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.DuplicateContactsPopup, {duplicateContactsPopupNavigator}, 0)
  }
  
  static function push (duplicateContactsPopupNavigator :  gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DuplicateContactsPopup, {duplicateContactsPopupNavigator}, 0).push()
  }
  
  
}