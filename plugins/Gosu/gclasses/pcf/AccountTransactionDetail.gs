package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountTransactionDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountTransactionDetail extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (Account :  Account, Transaction :  Transaction) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AccountTransactionDetail, {Account, Transaction}, 0)
  }
  
  static function drilldown (Account :  Account, Transaction :  Transaction) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountTransactionDetail, {Account, Transaction}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (Account :  Account, Transaction :  Transaction) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountTransactionDetail, {Account, Transaction}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (Account :  Account, Transaction :  Transaction) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountTransactionDetail, {Account, Transaction}, 0).goInMain()
  }
  
  static function printPage (Account :  Account, Transaction :  Transaction) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountTransactionDetail, {Account, Transaction}, 0).printPage()
  }
  
  static function push (Account :  Account, Transaction :  Transaction) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountTransactionDetail, {Account, Transaction}, 0).push()
  }
  
  
}