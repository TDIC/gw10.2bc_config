package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailContacts.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyDetailContacts extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PolicyDetailContacts, {plcyPeriod}, 0)
  }
  
  static function createDestination (plcyPeriod :  PolicyPeriod, contactToSelectOnEnter :  PolicyPeriodContact) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PolicyDetailContacts, {plcyPeriod, contactToSelectOnEnter}, 1)
  }
  
  static function drilldown (plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailContacts, {plcyPeriod}, 0).drilldown()
  }
  
  static function drilldown (plcyPeriod :  PolicyPeriod, contactToSelectOnEnter :  PolicyPeriodContact) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailContacts, {plcyPeriod, contactToSelectOnEnter}, 1).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailContacts, {plcyPeriod}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (plcyPeriod :  PolicyPeriod, contactToSelectOnEnter :  PolicyPeriodContact) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailContacts, {plcyPeriod, contactToSelectOnEnter}, 1).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailContacts, {plcyPeriod}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (plcyPeriod :  PolicyPeriod, contactToSelectOnEnter :  PolicyPeriodContact) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailContacts, {plcyPeriod, contactToSelectOnEnter}, 1).goInMain()
  }
  
  static function printPage (plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailContacts, {plcyPeriod}, 0).printPage()
  }
  
  static function printPage (plcyPeriod :  PolicyPeriod, contactToSelectOnEnter :  PolicyPeriodContact) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailContacts, {plcyPeriod, contactToSelectOnEnter}, 1).printPage()
  }
  
  static function push (plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailContacts, {plcyPeriod}, 0).push()
  }
  
  static function push (plcyPeriod :  PolicyPeriod, contactToSelectOnEnter :  PolicyPeriodContact) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailContacts, {plcyPeriod, contactToSelectOnEnter}, 1).push()
  }
  
  
}