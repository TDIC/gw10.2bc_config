package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/AccountDisbursementConfirmDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountDisbursementConfirmDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($accountDisbursement :  AccountDisbursement) : void {
    __widgetOf(this, pcf.AccountDisbursementConfirmDV, SECTION_WIDGET_CLASS).setVariables(false, {$accountDisbursement})
  }
  
  function refreshVariables ($accountDisbursement :  AccountDisbursement) : void {
    __widgetOf(this, pcf.AccountDisbursementConfirmDV, SECTION_WIDGET_CLASS).setVariables(true, {$accountDisbursement})
  }
  
  
}