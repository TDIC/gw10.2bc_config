package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TransferPolicyConfirmationScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyTransfer :  PolicyTransfer) : void {
    __widgetOf(this, pcf.TransferPolicyConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(false, {$policyTransfer})
  }
  
  function refreshVariables ($policyTransfer :  PolicyTransfer) : void {
    __widgetOf(this, pcf.TransferPolicyConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(true, {$policyTransfer})
  }
  
  
}