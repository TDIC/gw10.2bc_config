package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyItemBalanceInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyItemBalanceInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($invoiceItem :  InvoiceItem) : void {
    __widgetOf(this, pcf.AgencyItemBalanceInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$invoiceItem})
  }
  
  function refreshVariables ($invoiceItem :  InvoiceItem) : void {
    __widgetOf(this, pcf.AgencyItemBalanceInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$invoiceItem})
  }
  
  
}