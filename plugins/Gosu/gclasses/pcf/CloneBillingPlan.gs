package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/billing/CloneBillingPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CloneBillingPlan extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (billingPlan :  BillingPlan, includeSpecs :  boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.CloneBillingPlan, {billingPlan, includeSpecs}, 0)
  }
  
  static function drilldown (billingPlan :  BillingPlan, includeSpecs :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CloneBillingPlan, {billingPlan, includeSpecs}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (billingPlan :  BillingPlan, includeSpecs :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CloneBillingPlan, {billingPlan, includeSpecs}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (billingPlan :  BillingPlan, includeSpecs :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CloneBillingPlan, {billingPlan, includeSpecs}, 0).goInMain()
  }
  
  static function printPage (billingPlan :  BillingPlan, includeSpecs :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CloneBillingPlan, {billingPlan, includeSpecs}, 0).printPage()
  }
  
  static function push (billingPlan :  BillingPlan, includeSpecs :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CloneBillingPlan, {billingPlan, includeSpecs}, 0).push()
  }
  
  
}