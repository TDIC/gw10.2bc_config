package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/suspensepayment/EditSuspensePaymentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class EditSuspensePaymentPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (suspensePayment :  SuspensePayment) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.EditSuspensePaymentPopup, {suspensePayment}, 0)
  }
  
  static function push (suspensePayment :  SuspensePayment) : pcf.api.Location {
    return __newDestinationForLocation(pcf.EditSuspensePaymentPopup, {suspensePayment}, 0).push()
  }
  
  
}