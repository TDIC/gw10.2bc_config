package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/payment/PaymentPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PaymentPlanDetailScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($paymentPlan :  PaymentPlan, $isClone :  Boolean) : void {
    __widgetOf(this, pcf.PaymentPlanDetailScreen, SECTION_WIDGET_CLASS).setVariables(false, {$paymentPlan, $isClone})
  }
  
  function refreshVariables ($paymentPlan :  PaymentPlan, $isClone :  Boolean) : void {
    __widgetOf(this, pcf.PaymentPlanDetailScreen, SECTION_WIDGET_CLASS).setVariables(true, {$paymentPlan, $isClone})
  }
  
  
}