package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class BillingPlanDetailDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($billingPlan :  BillingPlan) : void {
    __widgetOf(this, pcf.BillingPlanDetailDV, SECTION_WIDGET_CLASS).setVariables(false, {$billingPlan})
  }
  
  function refreshVariables ($billingPlan :  BillingPlan) : void {
    __widgetOf(this, pcf.BillingPlanDetailDV, SECTION_WIDGET_CLASS).setVariables(true, {$billingPlan})
  }
  
  
}