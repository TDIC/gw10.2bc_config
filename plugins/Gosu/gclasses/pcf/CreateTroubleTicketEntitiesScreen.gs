package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/CreateTroubleTicketEntitiesScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CreateTroubleTicketEntitiesScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($TroubleTicket :  TroubleTicket, $AssigneeHolder :  gw.api.assignment.Assignee[], $CreateTroubleTicketHelper :  CreateTroubleTicketHelper) : void {
    __widgetOf(this, pcf.CreateTroubleTicketEntitiesScreen, SECTION_WIDGET_CLASS).setVariables(false, {$TroubleTicket, $AssigneeHolder, $CreateTroubleTicketHelper})
  }
  
  function refreshVariables ($TroubleTicket :  TroubleTicket, $AssigneeHolder :  gw.api.assignment.Assignee[], $CreateTroubleTicketHelper :  CreateTroubleTicketHelper) : void {
    __widgetOf(this, pcf.CreateTroubleTicketEntitiesScreen, SECTION_WIDGET_CLASS).setVariables(true, {$TroubleTicket, $AssigneeHolder, $CreateTroubleTicketHelper})
  }
  
  
}