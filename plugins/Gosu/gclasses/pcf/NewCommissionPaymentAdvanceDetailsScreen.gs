package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentAdvanceDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewCommissionPaymentAdvanceDetailsScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($advancePayment :  AdvanceCmsnPayment) : void {
    __widgetOf(this, pcf.NewCommissionPaymentAdvanceDetailsScreen, SECTION_WIDGET_CLASS).setVariables(false, {$advancePayment})
  }
  
  function refreshVariables ($advancePayment :  AdvanceCmsnPayment) : void {
    __widgetOf(this, pcf.NewCommissionPaymentAdvanceDetailsScreen, SECTION_WIDGET_CLASS).setVariables(true, {$advancePayment})
  }
  
  
}