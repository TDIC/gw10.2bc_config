package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/AccountNewWriteoffWizardTargetStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountNewWriteoffWizardTargetStepScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($account :  Account, $targetOfWriteoff :  gw.api.web.accounting.TAccountOwnerReference) : void {
    __widgetOf(this, pcf.AccountNewWriteoffWizardTargetStepScreen, SECTION_WIDGET_CLASS).setVariables(false, {$account, $targetOfWriteoff})
  }
  
  function refreshVariables ($account :  Account, $targetOfWriteoff :  gw.api.web.accounting.TAccountOwnerReference) : void {
    __widgetOf(this, pcf.AccountNewWriteoffWizardTargetStepScreen, SECTION_WIDGET_CLASS).setVariables(true, {$account, $targetOfWriteoff})
  }
  
  
}