package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadConfirmation.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DataUploadConfirmation extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (processor :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.DataUploadConfirmation, {processor}, 0)
  }
  
  static function drilldown (processor :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DataUploadConfirmation, {processor}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (processor :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DataUploadConfirmation, {processor}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (processor :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DataUploadConfirmation, {processor}, 0).goInMain()
  }
  
  static function printPage (processor :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DataUploadConfirmation, {processor}, 0).printPage()
  }
  
  static function push (processor :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DataUploadConfirmation, {processor}, 0).push()
  }
  
  
}