package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/DBPaymentReversalConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DBPaymentReversalConfirmationPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (moneyReceived :  DirectBillMoneyRcvd) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.DBPaymentReversalConfirmationPopup, {moneyReceived}, 0)
  }
  
  static function push (moneyReceived :  DirectBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DBPaymentReversalConfirmationPopup, {moneyReceived}, 0).push()
  }
  
  
}