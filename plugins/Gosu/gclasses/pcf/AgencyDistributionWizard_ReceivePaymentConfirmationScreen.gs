package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_ReceivePaymentConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyDistributionWizard_ReceivePaymentConfirmationScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($money :  AgencyBillMoneyRcvd) : void {
    __widgetOf(this, pcf.AgencyDistributionWizard_ReceivePaymentConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(false, {$money})
  }
  
  function refreshVariables ($money :  AgencyBillMoneyRcvd) : void {
    __widgetOf(this, pcf.AgencyDistributionWizard_ReceivePaymentConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(true, {$money})
  }
  
  
}