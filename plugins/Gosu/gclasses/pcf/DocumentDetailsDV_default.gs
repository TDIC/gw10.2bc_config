package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/DocumentDetailsDV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DocumentDetailsDV_default extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($document :  Document) : void {
    __widgetOf(this, pcf.DocumentDetailsDV_default, SECTION_WIDGET_CLASS).setVariables(false, {$document})
  }
  
  function refreshVariables ($document :  Document) : void {
    __widgetOf(this, pcf.DocumentDetailsDV_default, SECTION_WIDGET_CLASS).setVariables(true, {$document})
  }
  
  
}