package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/SuspenseCreateDisbursementWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class SuspenseCreateDisbursementWizard extends com.guidewire.pl.web.codegen.WizardBase {
  static function createDestination (suspensePayment :  SuspensePayment) : pcf.api.Destination {
    return __newDestinationForWizard(pcf.SuspenseCreateDisbursementWizard, {suspensePayment}, 0)
  }
  
  static function drilldown (suspensePayment :  SuspensePayment) : pcf.api.Location {
    return __newDestinationForWizard(pcf.SuspenseCreateDisbursementWizard, {suspensePayment}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (suspensePayment :  SuspensePayment) : pcf.api.Location {
    return __newDestinationForWizard(pcf.SuspenseCreateDisbursementWizard, {suspensePayment}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (suspensePayment :  SuspensePayment) : pcf.api.Location {
    return __newDestinationForWizard(pcf.SuspenseCreateDisbursementWizard, {suspensePayment}, 0).goInMain()
  }
  
  static function printPage (suspensePayment :  SuspensePayment) : pcf.api.Location {
    return __newDestinationForWizard(pcf.SuspenseCreateDisbursementWizard, {suspensePayment}, 0).printPage()
  }
  
  static function push (suspensePayment :  SuspensePayment) : pcf.api.Location {
    return __newDestinationForWizard(pcf.SuspenseCreateDisbursementWizard, {suspensePayment}, 0).push()
  }
  
  
}