package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/DisbursementConfirmPanelSet.AccountDisbursement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DisbursementConfirmPanelSet_AccountDisbursement extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($disbursement :  Disbursement) : void {
    __widgetOf(this, pcf.DisbursementConfirmPanelSet_AccountDisbursement, SECTION_WIDGET_CLASS).setVariables(false, {$disbursement})
  }
  
  function refreshVariables ($disbursement :  Disbursement) : void {
    __widgetOf(this, pcf.DisbursementConfirmPanelSet_AccountDisbursement, SECTION_WIDGET_CLASS).setVariables(true, {$disbursement})
  }
  
  
}