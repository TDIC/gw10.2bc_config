package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailDocuments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyDetailDocuments extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (policy :  Policy, plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PolicyDetailDocuments, {policy, plcyPeriod}, 0)
  }
  
  static function drilldown (policy :  Policy, plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailDocuments, {policy, plcyPeriod}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (policy :  Policy, plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailDocuments, {policy, plcyPeriod}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (policy :  Policy, plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailDocuments, {policy, plcyPeriod}, 0).goInMain()
  }
  
  static function printPage (policy :  Policy, plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailDocuments, {policy, plcyPeriod}, 0).printPage()
  }
  
  static function push (policy :  Policy, plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailDocuments, {policy, plcyPeriod}, 0).push()
  }
  
  
}