package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ContactCriteriaInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ContactCriteriaInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($contactCriteria :  gw.search.ContactCriteria, $displayADA :  boolean) : void {
    __widgetOf(this, pcf.ContactCriteriaInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$contactCriteria, $displayADA})
  }
  
  function refreshVariables ($contactCriteria :  gw.search.ContactCriteria, $displayADA :  boolean) : void {
    __widgetOf(this, pcf.ContactCriteriaInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$contactCriteria, $displayADA})
  }
  
  
}