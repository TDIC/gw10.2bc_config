package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountPaymentDistributionItemsCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountPaymentDistributionItemsCV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($selectedMoney :  DirectBillMoneyRcvd) : void {
    __widgetOf(this, pcf.AccountPaymentDistributionItemsCV, SECTION_WIDGET_CLASS).setVariables(false, {$selectedMoney})
  }
  
  function refreshVariables ($selectedMoney :  DirectBillMoneyRcvd) : void {
    __widgetOf(this, pcf.AccountPaymentDistributionItemsCV, SECTION_WIDGET_CLASS).setVariables(true, {$selectedMoney})
  }
  
  
}