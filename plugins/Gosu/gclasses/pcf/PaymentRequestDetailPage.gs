package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/PaymentRequestDetailPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PaymentRequestDetailPage extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (oldPaymentRequest :  PaymentRequest, shouldStartInEditMode :  Boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PaymentRequestDetailPage, {oldPaymentRequest, shouldStartInEditMode}, 0)
  }
  
  static function drilldown (oldPaymentRequest :  PaymentRequest, shouldStartInEditMode :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentRequestDetailPage, {oldPaymentRequest, shouldStartInEditMode}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (oldPaymentRequest :  PaymentRequest, shouldStartInEditMode :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentRequestDetailPage, {oldPaymentRequest, shouldStartInEditMode}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (oldPaymentRequest :  PaymentRequest, shouldStartInEditMode :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentRequestDetailPage, {oldPaymentRequest, shouldStartInEditMode}, 0).goInMain()
  }
  
  static function printPage (oldPaymentRequest :  PaymentRequest, shouldStartInEditMode :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentRequestDetailPage, {oldPaymentRequest, shouldStartInEditMode}, 0).printPage()
  }
  
  static function push (oldPaymentRequest :  PaymentRequest, shouldStartInEditMode :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentRequestDetailPage, {oldPaymentRequest, shouldStartInEditMode}, 0).push()
  }
  
  
}