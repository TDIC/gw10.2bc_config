package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/TroubleTicketSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TroubleTicketSearchDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($searchCriteria :  gw.search.TroubleTicketSearchCriteria) : void {
    __widgetOf(this, pcf.TroubleTicketSearchDV, SECTION_WIDGET_CLASS).setVariables(false, {$searchCriteria})
  }
  
  function refreshVariables ($searchCriteria :  gw.search.TroubleTicketSearchCriteria) : void {
    __widgetOf(this, pcf.TroubleTicketSearchDV, SECTION_WIDGET_CLASS).setVariables(true, {$searchCriteria})
  }
  
  
}