package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/CreditReversalConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CreditReversalConfirmationScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($account :  Account, $creditReversal :  CreditReversal) : void {
    __widgetOf(this, pcf.CreditReversalConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(false, {$account, $creditReversal})
  }
  
  function refreshVariables ($account :  Account, $creditReversal :  CreditReversal) : void {
    __widgetOf(this, pcf.CreditReversalConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(true, {$account, $creditReversal})
  }
  
  
}