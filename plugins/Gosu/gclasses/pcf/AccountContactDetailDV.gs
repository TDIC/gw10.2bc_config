package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountContactDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountContactDetailDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($accountContact :  AccountContact, $isNew :  Boolean) : void {
    __widgetOf(this, pcf.AccountContactDetailDV, SECTION_WIDGET_CLASS).setVariables(false, {$accountContact, $isNew})
  }
  
  function refreshVariables ($accountContact :  AccountContact, $isNew :  Boolean) : void {
    __widgetOf(this, pcf.AccountContactDetailDV, SECTION_WIDGET_CLASS).setVariables(true, {$accountContact, $isNew})
  }
  
  
}