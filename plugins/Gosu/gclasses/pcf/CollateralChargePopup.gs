package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralChargePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CollateralChargePopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (chargePatternOwnerPair :  com.google.gdata.util.common.base.Pair<ChargePattern, TAccountOwner>, collateralUtil :  gw.api.web.account.CollateralUtil) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.CollateralChargePopup, {chargePatternOwnerPair, collateralUtil}, 0)
  }
  
  static function push (chargePatternOwnerPair :  com.google.gdata.util.common.base.Pair<ChargePattern, TAccountOwner>, collateralUtil :  gw.api.web.account.CollateralUtil) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CollateralChargePopup, {chargePatternOwnerPair, collateralUtil}, 0).push()
  }
  
  
}