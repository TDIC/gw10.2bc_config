package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/NewDocumentFromTemplateScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewDocumentFromTemplateScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($documentCreationInfo :  gw.document.DocumentCreationInfo, $documentContainer :  DocumentContainer) : void {
    __widgetOf(this, pcf.NewDocumentFromTemplateScreen, SECTION_WIDGET_CLASS).setVariables(false, {$documentCreationInfo, $documentContainer})
  }
  
  function refreshVariables ($documentCreationInfo :  gw.document.DocumentCreationInfo, $documentContainer :  DocumentContainer) : void {
    __widgetOf(this, pcf.NewDocumentFromTemplateScreen, SECTION_WIDGET_CLASS).setVariables(true, {$documentCreationInfo, $documentContainer})
  }
  
  
}