package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExecutedPromises.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyBillExecutedPromises extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (producer :  Producer) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPromises, {producer}, 0)
  }
  
  static function createDestination (producer :  Producer, initialSelectedPromise :  AgencyCyclePromise) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPromises, {producer, initialSelectedPromise}, 1)
  }
  
  static function drilldown (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPromises, {producer}, 0).drilldown()
  }
  
  static function drilldown (producer :  Producer, initialSelectedPromise :  AgencyCyclePromise) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPromises, {producer, initialSelectedPromise}, 1).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPromises, {producer}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (producer :  Producer, initialSelectedPromise :  AgencyCyclePromise) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPromises, {producer, initialSelectedPromise}, 1).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPromises, {producer}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (producer :  Producer, initialSelectedPromise :  AgencyCyclePromise) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPromises, {producer, initialSelectedPromise}, 1).goInMain()
  }
  
  static function printPage (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPromises, {producer}, 0).printPage()
  }
  
  static function printPage (producer :  Producer, initialSelectedPromise :  AgencyCyclePromise) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPromises, {producer, initialSelectedPromise}, 1).printPage()
  }
  
  static function push (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPromises, {producer}, 0).push()
  }
  
  static function push (producer :  Producer, initialSelectedPromise :  AgencyCyclePromise) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillExecutedPromises, {producer, initialSelectedPromise}, 1).push()
  }
  
  
}