package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/PolicySearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicySearchPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination () : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PolicySearchPopup, {}, 0)
  }
  
  static function createDestination (policyForRelatedLookup :  boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PolicySearchPopup, {policyForRelatedLookup}, 1)
  }
  
  function pickValueAndCommit (value :  PolicyPeriod) : void {
    __widgetOf(this, pcf.PolicySearchPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push () : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicySearchPopup, {}, 0).push()
  }
  
  static function push (policyForRelatedLookup :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicySearchPopup, {policyForRelatedLookup}, 1).push()
  }
  
  
}