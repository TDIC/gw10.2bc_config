package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/ApprovalActivityDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ApprovalActivityDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($approvalActivity :  Activity) : void {
    __widgetOf(this, pcf.ApprovalActivityDV, SECTION_WIDGET_CLASS).setVariables(false, {$approvalActivity})
  }
  
  function refreshVariables ($approvalActivity :  Activity) : void {
    __widgetOf(this, pcf.ApprovalActivityDV, SECTION_WIDGET_CLASS).setVariables(true, {$approvalActivity})
  }
  
  
}