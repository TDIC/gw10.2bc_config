package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyPlanDetailPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (delinquencyPlan :  DelinquencyPlan) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.DelinquencyPlanDetailPopup, {delinquencyPlan}, 0)
  }
  
  static function push (delinquencyPlan :  DelinquencyPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DelinquencyPlanDetailPopup, {delinquencyPlan}, 0).push()
  }
  
  
}