package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/OutgoingPaymentDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class OutgoingPaymentDetailPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (outgoingPayment :  OutgoingPayment) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.OutgoingPaymentDetailPopup, {outgoingPayment}, 0)
  }
  
  static function push (outgoingPayment :  OutgoingPayment) : pcf.api.Location {
    return __newDestinationForLocation(pcf.OutgoingPaymentDetailPopup, {outgoingPayment}, 0).push()
  }
  
  
}