package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountContactDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountContactDetailPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (accountContact :  AccountContact) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AccountContactDetailPopup, {accountContact}, 0)
  }
  
  function pickValueAndCommit (value :  AccountContact) : void {
    __widgetOf(this, pcf.AccountContactDetailPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (accountContact :  AccountContact) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountContactDetailPopup, {accountContact}, 0).push()
  }
  
  
}