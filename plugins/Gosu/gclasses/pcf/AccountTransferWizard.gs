package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transfer/AccountTransferWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountTransferWizard extends com.guidewire.pl.web.codegen.WizardBase {
  static function createDestination (accountID :  gw.pl.persistence.core.Key) : pcf.api.Destination {
    return __newDestinationForWizard(pcf.AccountTransferWizard, {accountID}, 0)
  }
  
  static function drilldown (accountID :  gw.pl.persistence.core.Key) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AccountTransferWizard, {accountID}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (accountID :  gw.pl.persistence.core.Key) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AccountTransferWizard, {accountID}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (accountID :  gw.pl.persistence.core.Key) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AccountTransferWizard, {accountID}, 0).goInMain()
  }
  
  static function printPage (accountID :  gw.pl.persistence.core.Key) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AccountTransferWizard, {accountID}, 0).printPage()
  }
  
  static function push (accountID :  gw.pl.persistence.core.Key) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AccountTransferWizard, {accountID}, 0).push()
  }
  
  
}