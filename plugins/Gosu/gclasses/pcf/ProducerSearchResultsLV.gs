package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ProducerSearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducerSearchResultsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($producerSearchViews :  gw.api.database.IQueryBeanResult<ProducerSearchView>, $tAccountOwnerReference :  gw.api.web.accounting.TAccountOwnerReference, $isWizard :  boolean, $showHyperlinks :  boolean, $showCheckboxes :  Boolean) : void {
    __widgetOf(this, pcf.ProducerSearchResultsLV, SECTION_WIDGET_CLASS).setVariables(false, {$producerSearchViews, $tAccountOwnerReference, $isWizard, $showHyperlinks, $showCheckboxes})
  }
  
  function refreshVariables ($producerSearchViews :  gw.api.database.IQueryBeanResult<ProducerSearchView>, $tAccountOwnerReference :  gw.api.web.accounting.TAccountOwnerReference, $isWizard :  boolean, $showHyperlinks :  boolean, $showCheckboxes :  Boolean) : void {
    __widgetOf(this, pcf.ProducerSearchResultsLV, SECTION_WIDGET_CLASS).setVariables(true, {$producerSearchViews, $tAccountOwnerReference, $isWizard, $showHyperlinks, $showCheckboxes})
  }
  
  
}