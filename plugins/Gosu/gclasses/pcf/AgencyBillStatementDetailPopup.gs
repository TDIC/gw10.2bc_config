package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillStatementDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyBillStatementDetailPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (statement :  StatementInvoice) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyBillStatementDetailPopup, {statement}, 0)
  }
  
  static function push (statement :  StatementInvoice) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillStatementDetailPopup, {statement}, 0).push()
  }
  
  
}