package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillStatementDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyBillStatementDetailScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($cycle :  AgencyBillCycle) : void {
    __widgetOf(this, pcf.AgencyBillStatementDetailScreen, SECTION_WIDGET_CLASS).setVariables(false, {$cycle})
  }
  
  function refreshVariables ($cycle :  AgencyBillCycle) : void {
    __widgetOf(this, pcf.AgencyBillStatementDetailScreen, SECTION_WIDGET_CLASS).setVariables(true, {$cycle})
  }
  
  
}