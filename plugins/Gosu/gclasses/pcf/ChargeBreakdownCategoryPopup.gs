package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/ChargeBreakdownCategoryPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ChargeBreakdownCategoryPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (chargeBreakdownItem :  ChargeBreakdownItem, view :  gw.web.policy.ChargeBreakdownItemsLvView) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ChargeBreakdownCategoryPopup, {chargeBreakdownItem, view}, 0)
  }
  
  static function push (chargeBreakdownItem :  ChargeBreakdownItem, view :  gw.web.policy.ChargeBreakdownItemsLvView) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChargeBreakdownCategoryPopup, {chargeBreakdownItem, view}, 0).push()
  }
  
  
}