package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/DBUndistributePaymentConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DBUndistributePaymentConfirmationPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (payment :  DirectBillPayment) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.DBUndistributePaymentConfirmationPopup, {payment}, 0)
  }
  
  static function push (payment :  DirectBillPayment) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DBUndistributePaymentConfirmationPopup, {payment}, 0).push()
  }
  
  
}