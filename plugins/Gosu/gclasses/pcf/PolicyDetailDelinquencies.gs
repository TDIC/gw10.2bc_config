package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailDelinquencies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyDetailDelinquencies extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PolicyDetailDelinquencies, {plcyPeriod}, 0)
  }
  
  static function createDestination (plcyPeriod :  PolicyPeriod, selectedDelinquencyProcessOnEnter :  DelinquencyProcess) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PolicyDetailDelinquencies, {plcyPeriod, selectedDelinquencyProcessOnEnter}, 1)
  }
  
  static function drilldown (plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailDelinquencies, {plcyPeriod}, 0).drilldown()
  }
  
  static function drilldown (plcyPeriod :  PolicyPeriod, selectedDelinquencyProcessOnEnter :  DelinquencyProcess) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailDelinquencies, {plcyPeriod, selectedDelinquencyProcessOnEnter}, 1).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailDelinquencies, {plcyPeriod}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (plcyPeriod :  PolicyPeriod, selectedDelinquencyProcessOnEnter :  DelinquencyProcess) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailDelinquencies, {plcyPeriod, selectedDelinquencyProcessOnEnter}, 1).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailDelinquencies, {plcyPeriod}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (plcyPeriod :  PolicyPeriod, selectedDelinquencyProcessOnEnter :  DelinquencyProcess) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailDelinquencies, {plcyPeriod, selectedDelinquencyProcessOnEnter}, 1).goInMain()
  }
  
  static function printPage (plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailDelinquencies, {plcyPeriod}, 0).printPage()
  }
  
  static function printPage (plcyPeriod :  PolicyPeriod, selectedDelinquencyProcessOnEnter :  DelinquencyProcess) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailDelinquencies, {plcyPeriod, selectedDelinquencyProcessOnEnter}, 1).printPage()
  }
  
  static function push (plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailDelinquencies, {plcyPeriod}, 0).push()
  }
  
  static function push (plcyPeriod :  PolicyPeriod, selectedDelinquencyProcessOnEnter :  DelinquencyProcess) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailDelinquencies, {plcyPeriod, selectedDelinquencyProcessOnEnter}, 1).push()
  }
  
  
}