package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyPeriodForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyPeriodForward extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (policyNumber :  String, termNumber :  int) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PolicyPeriodForward, {policyNumber, termNumber}, 0)
  }
  
  static function drilldown (policyNumber :  String, termNumber :  int) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyPeriodForward, {policyNumber, termNumber}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (policyNumber :  String, termNumber :  int) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyPeriodForward, {policyNumber, termNumber}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (policyNumber :  String, termNumber :  int) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyPeriodForward, {policyNumber, termNumber}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (policyNumber :  String, termNumber :  int) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyPeriodForward, {policyNumber, termNumber}, 0).goInWorkspace()
  }
  
  static function printPage (policyNumber :  String, termNumber :  int) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyPeriodForward, {policyNumber, termNumber}, 0).printPage()
  }
  
  static function push (policyNumber :  String, termNumber :  int) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyPeriodForward, {policyNumber, termNumber}, 0).push()
  }
  
  
}