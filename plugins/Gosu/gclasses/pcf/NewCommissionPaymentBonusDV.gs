package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentBonusDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewCommissionPaymentBonusDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($bonusPayment :  BonusCmsnPayment, $producer :  Producer, $canEdit :  Boolean) : void {
    __widgetOf(this, pcf.NewCommissionPaymentBonusDV, SECTION_WIDGET_CLASS).setVariables(false, {$bonusPayment, $producer, $canEdit})
  }
  
  function refreshVariables ($bonusPayment :  BonusCmsnPayment, $producer :  Producer, $canEdit :  Boolean) : void {
    __widgetOf(this, pcf.NewCommissionPaymentBonusDV, SECTION_WIDGET_CLASS).setVariables(true, {$bonusPayment, $producer, $canEdit})
  }
  
  
}