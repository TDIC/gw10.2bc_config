package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadImportScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PlanDataUploadImportScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($processor :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) : void {
    __widgetOf(this, pcf.PlanDataUploadImportScreen, SECTION_WIDGET_CLASS).setVariables(false, {$processor})
  }
  
  function refreshVariables ($processor :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) : void {
    __widgetOf(this, pcf.PlanDataUploadImportScreen, SECTION_WIDGET_CLASS).setVariables(true, {$processor})
  }
  
  
}