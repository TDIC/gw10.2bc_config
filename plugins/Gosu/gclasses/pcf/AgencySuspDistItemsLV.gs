package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/AgencySuspDistItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencySuspDistItemsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($suspenseDistItems :  gw.api.database.IQueryBeanResult<BaseSuspDistItem>) : void {
    __widgetOf(this, pcf.AgencySuspDistItemsLV, SECTION_WIDGET_CLASS).setVariables(false, {$suspenseDistItems})
  }
  
  function refreshVariables ($suspenseDistItems :  gw.api.database.IQueryBeanResult<BaseSuspDistItem>) : void {
    __widgetOf(this, pcf.AgencySuspDistItemsLV, SECTION_WIDGET_CLASS).setVariables(true, {$suspenseDistItems})
  }
  
  
}