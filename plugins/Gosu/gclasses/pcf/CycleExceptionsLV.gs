package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/CycleExceptionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CycleExceptionsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($agencyCyclesWithException :  AgencyCycleProcess[], $isLatePayments :  boolean) : void {
    __widgetOf(this, pcf.CycleExceptionsLV, SECTION_WIDGET_CLASS).setVariables(false, {$agencyCyclesWithException, $isLatePayments})
  }
  
  function refreshVariables ($agencyCyclesWithException :  AgencyCycleProcess[], $isLatePayments :  boolean) : void {
    __widgetOf(this, pcf.CycleExceptionsLV, SECTION_WIDGET_CLASS).setVariables(true, {$agencyCyclesWithException, $isLatePayments})
  }
  
  
}