package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/PolicyActivityLV.policylevel.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyActivityLV_policylevel extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($agencyBillStatementView :  gw.api.web.invoice.AgencyBillStatementView, $viewOption :  InvoiceItemViewOption) : void {
    __widgetOf(this, pcf.PolicyActivityLV_policylevel, SECTION_WIDGET_CLASS).setVariables(false, {$agencyBillStatementView, $viewOption})
  }
  
  function refreshVariables ($agencyBillStatementView :  gw.api.web.invoice.AgencyBillStatementView, $viewOption :  InvoiceItemViewOption) : void {
    __widgetOf(this, pcf.PolicyActivityLV_policylevel, SECTION_WIDGET_CLASS).setVariables(true, {$agencyBillStatementView, $viewOption})
  }
  
  
}