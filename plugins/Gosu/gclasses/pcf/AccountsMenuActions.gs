package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountsMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountsMenuActions extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter () : void {
    __widgetOf(this, pcf.AccountsMenuActions, SECTION_WIDGET_CLASS).setVariables(false, {})
  }
  
  function refreshVariables () : void {
    __widgetOf(this, pcf.AccountsMenuActions, SECTION_WIDGET_CLASS).setVariables(true, {})
  }
  
  
}