package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerStatementPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducerStatementPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (policyNumber :  String, producerStatement :  ProducerStatement) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ProducerStatementPopup, {policyNumber, producerStatement}, 0)
  }
  
  static function push (policyNumber :  String, producerStatement :  ProducerStatement) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerStatementPopup, {policyNumber, producerStatement}, 0).push()
  }
  
  
}