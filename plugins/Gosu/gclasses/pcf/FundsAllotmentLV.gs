package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/FundsAllotmentLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class FundsAllotmentLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($fundsTracker :  FundsTracker) : void {
    __widgetOf(this, pcf.FundsAllotmentLV, SECTION_WIDGET_CLASS).setVariables(false, {$fundsTracker})
  }
  
  function refreshVariables ($fundsTracker :  FundsTracker) : void {
    __widgetOf(this, pcf.FundsAllotmentLV, SECTION_WIDGET_CLASS).setVariables(true, {$fundsTracker})
  }
  
  
}