package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountContactCorrespondenceDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountContactCorrespondenceDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($accountContact :  AccountContact) : void {
    __widgetOf(this, pcf.AccountContactCorrespondenceDV, SECTION_WIDGET_CLASS).setVariables(false, {$accountContact})
  }
  
  function refreshVariables ($accountContact :  AccountContact) : void {
    __widgetOf(this, pcf.AccountContactCorrespondenceDV, SECTION_WIDGET_CLASS).setVariables(true, {$accountContact})
  }
  
  
}