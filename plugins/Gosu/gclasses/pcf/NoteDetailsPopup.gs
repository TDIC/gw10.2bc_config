package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/note/NoteDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NoteDetailsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (Note :  Note) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NoteDetailsPopup, {Note}, 0)
  }
  
  static function push (Note :  Note) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NoteDetailsPopup, {Note}, 0).push()
  }
  
  
}