package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/Acct360ViewTransactionsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class Acct360ViewTransactionsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (chargeInsttransactions :  ChargeInstanceCtx[]) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.Acct360ViewTransactionsPopup, {chargeInsttransactions}, 0)
  }
  
  static function push (chargeInsttransactions :  ChargeInstanceCtx[]) : pcf.api.Location {
    return __newDestinationForLocation(pcf.Acct360ViewTransactionsPopup, {chargeInsttransactions}, 0).push()
  }
  
  
}