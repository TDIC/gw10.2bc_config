package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/suspensepayment/SuspensePaymentTransactionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class SuspensePaymentTransactionsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($suspensePayment :  SuspensePayment) : void {
    __widgetOf(this, pcf.SuspensePaymentTransactionsLV, SECTION_WIDGET_CLASS).setVariables(false, {$suspensePayment})
  }
  
  function refreshVariables ($suspensePayment :  SuspensePayment) : void {
    __widgetOf(this, pcf.SuspensePaymentTransactionsLV, SECTION_WIDGET_CLASS).setVariables(true, {$suspensePayment})
  }
  
  
}