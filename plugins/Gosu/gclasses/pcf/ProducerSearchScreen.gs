package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ProducerSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducerSearchScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($showHyperlinks :  Boolean, $isClearBundle :  boolean) : void {
    __widgetOf(this, pcf.ProducerSearchScreen, SECTION_WIDGET_CLASS).setVariables(false, {$showHyperlinks, $isClearBundle})
  }
  
  function refreshVariables ($showHyperlinks :  Boolean, $isClearBundle :  boolean) : void {
    __widgetOf(this, pcf.ProducerSearchScreen, SECTION_WIDGET_CLASS).setVariables(true, {$showHyperlinks, $isClearBundle})
  }
  
  
}