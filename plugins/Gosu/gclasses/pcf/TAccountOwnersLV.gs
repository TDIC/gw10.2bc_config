package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/accounting/TAccountOwnersLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TAccountOwnersLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($primaryTAccountOwner :  TAccountOwner, $relatedTAccountOwners :  TAccountOwner[]) : void {
    __widgetOf(this, pcf.TAccountOwnersLV, SECTION_WIDGET_CLASS).setVariables(false, {$primaryTAccountOwner, $relatedTAccountOwners})
  }
  
  function refreshVariables ($primaryTAccountOwner :  TAccountOwner, $relatedTAccountOwners :  TAccountOwner[]) : void {
    __widgetOf(this, pcf.TAccountOwnersLV, SECTION_WIDGET_CLASS).setVariables(true, {$primaryTAccountOwner, $relatedTAccountOwners})
  }
  
  
}