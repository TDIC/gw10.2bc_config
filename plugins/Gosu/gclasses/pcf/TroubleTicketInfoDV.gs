package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TroubleTicketInfoDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($ticket :  TroubleTicket, $AssigneeHolder :  gw.api.assignment.Assignee[]) : void {
    __widgetOf(this, pcf.TroubleTicketInfoDV, SECTION_WIDGET_CLASS).setVariables(false, {$ticket, $AssigneeHolder})
  }
  
  function refreshVariables ($ticket :  TroubleTicket, $AssigneeHolder :  gw.api.assignment.Assignee[]) : void {
    __widgetOf(this, pcf.TroubleTicketInfoDV, SECTION_WIDGET_CLASS).setVariables(true, {$ticket, $AssigneeHolder})
  }
  
  
}