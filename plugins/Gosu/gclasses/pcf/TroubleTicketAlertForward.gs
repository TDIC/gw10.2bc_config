package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketAlertForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TroubleTicketAlertForward extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (entity :  HoldRelatedEntity) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TroubleTicketAlertForward, {entity}, 0)
  }
  
  static function createDestination (entity :  HoldRelatedEntity, policyPeriod :  PolicyPeriod) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TroubleTicketAlertForward, {entity, policyPeriod}, 1)
  }
  
  static function drilldown (entity :  HoldRelatedEntity) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TroubleTicketAlertForward, {entity}, 0).drilldown()
  }
  
  static function drilldown (entity :  HoldRelatedEntity, policyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TroubleTicketAlertForward, {entity, policyPeriod}, 1).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (entity :  HoldRelatedEntity) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TroubleTicketAlertForward, {entity}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (entity :  HoldRelatedEntity, policyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TroubleTicketAlertForward, {entity, policyPeriod}, 1).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (entity :  HoldRelatedEntity) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TroubleTicketAlertForward, {entity}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (entity :  HoldRelatedEntity, policyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TroubleTicketAlertForward, {entity, policyPeriod}, 1).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (entity :  HoldRelatedEntity) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TroubleTicketAlertForward, {entity}, 0).goInWorkspace()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (entity :  HoldRelatedEntity, policyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TroubleTicketAlertForward, {entity, policyPeriod}, 1).goInWorkspace()
  }
  
  static function printPage (entity :  HoldRelatedEntity) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TroubleTicketAlertForward, {entity}, 0).printPage()
  }
  
  static function printPage (entity :  HoldRelatedEntity, policyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TroubleTicketAlertForward, {entity, policyPeriod}, 1).printPage()
  }
  
  static function push (entity :  HoldRelatedEntity) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TroubleTicketAlertForward, {entity}, 0).push()
  }
  
  static function push (entity :  HoldRelatedEntity, policyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TroubleTicketAlertForward, {entity, policyPeriod}, 1).push()
  }
  
  
}