package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/DocumentSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DocumentSearchDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($documentSearchCriteria :  DocumentSearchCriteria) : void {
    __widgetOf(this, pcf.DocumentSearchDV, SECTION_WIDGET_CLASS).setVariables(false, {$documentSearchCriteria})
  }
  
  function refreshVariables ($documentSearchCriteria :  DocumentSearchCriteria) : void {
    __widgetOf(this, pcf.DocumentSearchDV, SECTION_WIDGET_CLASS).setVariables(true, {$documentSearchCriteria})
  }
  
  
}