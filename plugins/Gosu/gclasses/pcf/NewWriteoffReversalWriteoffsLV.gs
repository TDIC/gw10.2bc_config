package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalWriteoffsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewWriteoffReversalWriteoffsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($reversal :  WriteoffReversal, $writeOffs :  gw.api.database.IQueryBeanResult<Writeoff>) : void {
    __widgetOf(this, pcf.NewWriteoffReversalWriteoffsLV, SECTION_WIDGET_CLASS).setVariables(false, {$reversal, $writeOffs})
  }
  
  function refreshVariables ($reversal :  WriteoffReversal, $writeOffs :  gw.api.database.IQueryBeanResult<Writeoff>) : void {
    __widgetOf(this, pcf.NewWriteoffReversalWriteoffsLV, SECTION_WIDGET_CLASS).setVariables(true, {$reversal, $writeOffs})
  }
  
  
}