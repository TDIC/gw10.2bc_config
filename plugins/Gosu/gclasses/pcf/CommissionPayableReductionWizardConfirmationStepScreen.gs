package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/CommissionPayableReductionWizardConfirmationStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CommissionPayableReductionWizardConfirmationStepScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($chargeCommissions :  ChargeCommission[], $currency :  Currency, $showApprovalRequiredAlertBar :  Boolean) : void {
    __widgetOf(this, pcf.CommissionPayableReductionWizardConfirmationStepScreen, SECTION_WIDGET_CLASS).setVariables(false, {$chargeCommissions, $currency, $showApprovalRequiredAlertBar})
  }
  
  function refreshVariables ($chargeCommissions :  ChargeCommission[], $currency :  Currency, $showApprovalRequiredAlertBar :  Boolean) : void {
    __widgetOf(this, pcf.CommissionPayableReductionWizardConfirmationStepScreen, SECTION_WIDGET_CLASS).setVariables(true, {$chargeCommissions, $currency, $showApprovalRequiredAlertBar})
  }
  
  
}