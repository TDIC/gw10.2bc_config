package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/PolicySearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicySearchDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($searchCriteria :  gw.search.PolicySearchCriteria, $allowsArchiveInclusion :  boolean) : void {
    __widgetOf(this, pcf.PolicySearchDV, SECTION_WIDGET_CLASS).setVariables(false, {$searchCriteria, $allowsArchiveInclusion})
  }
  
  function refreshVariables ($searchCriteria :  gw.search.PolicySearchCriteria, $allowsArchiveInclusion :  boolean) : void {
    __widgetOf(this, pcf.PolicySearchDV, SECTION_WIDGET_CLASS).setVariables(true, {$searchCriteria, $allowsArchiveInclusion})
  }
  
  
}