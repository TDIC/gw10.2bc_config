package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DisbursementSearchScreen.CollateralDisbursement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DisbursementSearchScreen_CollateralDisbursement extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($disbursementSubtypeHolder :  typekey.Disbursement[]) : void {
    __widgetOf(this, pcf.DisbursementSearchScreen_CollateralDisbursement, SECTION_WIDGET_CLASS).setVariables(false, {$disbursementSubtypeHolder})
  }
  
  function refreshVariables ($disbursementSubtypeHolder :  typekey.Disbursement[]) : void {
    __widgetOf(this, pcf.DisbursementSearchScreen_CollateralDisbursement, SECTION_WIDGET_CLASS).setVariables(true, {$disbursementSubtypeHolder})
  }
  
  
}