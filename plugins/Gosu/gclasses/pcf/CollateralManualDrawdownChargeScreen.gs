package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralManualDrawdownChargeScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CollateralManualDrawdownChargeScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($collateralDrawdown :  gw.api.web.collateral.CollateralDrawdownUtil) : void {
    __widgetOf(this, pcf.CollateralManualDrawdownChargeScreen, SECTION_WIDGET_CLASS).setVariables(false, {$collateralDrawdown})
  }
  
  function refreshVariables ($collateralDrawdown :  gw.api.web.collateral.CollateralDrawdownUtil) : void {
    __widgetOf(this, pcf.CollateralManualDrawdownChargeScreen, SECTION_WIDGET_CLASS).setVariables(true, {$collateralDrawdown})
  }
  
  
}