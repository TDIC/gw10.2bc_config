package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/PaymentSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PaymentSearchDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($searchCriteria :  gw.search.PaymentSearchCriteria) : void {
    __widgetOf(this, pcf.PaymentSearchDV, SECTION_WIDGET_CLASS).setVariables(false, {$searchCriteria})
  }
  
  function refreshVariables ($searchCriteria :  gw.search.PaymentSearchCriteria) : void {
    __widgetOf(this, pcf.PaymentSearchDV, SECTION_WIDGET_CLASS).setVariables(true, {$searchCriteria})
  }
  
  
}