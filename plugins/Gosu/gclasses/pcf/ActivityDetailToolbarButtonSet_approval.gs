package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/ActivityDetailToolbarButtonSet.approval.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ActivityDetailToolbarButtonSet_approval extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($activity :  Activity, $assigneeHolder :  gw.api.assignment.Assignee[]) : void {
    __widgetOf(this, pcf.ActivityDetailToolbarButtonSet_approval, SECTION_WIDGET_CLASS).setVariables(false, {$activity, $assigneeHolder})
  }
  
  function refreshVariables ($activity :  Activity, $assigneeHolder :  gw.api.assignment.Assignee[]) : void {
    __widgetOf(this, pcf.ActivityDetailToolbarButtonSet_approval, SECTION_WIDGET_CLASS).setVariables(true, {$activity, $assigneeHolder})
  }
  
  
}