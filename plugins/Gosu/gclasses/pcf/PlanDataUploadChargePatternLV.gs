package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadChargePatternLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PlanDataUploadChargePatternLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($processor :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) : void {
    __widgetOf(this, pcf.PlanDataUploadChargePatternLV, SECTION_WIDGET_CLASS).setVariables(false, {$processor})
  }
  
  function refreshVariables ($processor :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) : void {
    __widgetOf(this, pcf.PlanDataUploadChargePatternLV, SECTION_WIDGET_CLASS).setVariables(true, {$processor})
  }
  
  
}