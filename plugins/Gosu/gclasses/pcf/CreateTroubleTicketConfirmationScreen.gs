package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/CreateTroubleTicketConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CreateTroubleTicketConfirmationScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($troubleTicket :  TroubleTicket, $assigneeHolder :  gw.api.assignment.Assignee[], $createTroubleTicketHelper :  CreateTroubleTicketHelper) : void {
    __widgetOf(this, pcf.CreateTroubleTicketConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(false, {$troubleTicket, $assigneeHolder, $createTroubleTicketHelper})
  }
  
  function refreshVariables ($troubleTicket :  TroubleTicket, $assigneeHolder :  gw.api.assignment.Assignee[], $createTroubleTicketHelper :  CreateTroubleTicketHelper) : void {
    __widgetOf(this, pcf.CreateTroubleTicketConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(true, {$troubleTicket, $assigneeHolder, $createTroubleTicketHelper})
  }
  
  
}