package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/InvoiceSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class InvoiceSearchDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($searchCriteria :  gw.search.InvoiceSearchCriteria) : void {
    __widgetOf(this, pcf.InvoiceSearchDV, SECTION_WIDGET_CLASS).setVariables(false, {$searchCriteria})
  }
  
  function refreshVariables ($searchCriteria :  gw.search.InvoiceSearchCriteria) : void {
    __widgetOf(this, pcf.InvoiceSearchDV, SECTION_WIDGET_CLASS).setVariables(true, {$searchCriteria})
  }
  
  
}