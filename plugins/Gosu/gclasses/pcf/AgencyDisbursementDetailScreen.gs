package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyDisbursementDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyDisbursementDetailScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($disbursementVar :  AgencyDisbursement, $payCommissionReference :  boolean[], $invoiceItems :  List<InvoiceItem>, $fromCreditItems :  boolean) : void {
    __widgetOf(this, pcf.AgencyDisbursementDetailScreen, SECTION_WIDGET_CLASS).setVariables(false, {$disbursementVar, $payCommissionReference, $invoiceItems, $fromCreditItems})
  }
  
  function refreshVariables ($disbursementVar :  AgencyDisbursement, $payCommissionReference :  boolean[], $invoiceItems :  List<InvoiceItem>, $fromCreditItems :  boolean) : void {
    __widgetOf(this, pcf.AgencyDisbursementDetailScreen, SECTION_WIDGET_CLASS).setVariables(true, {$disbursementVar, $payCommissionReference, $invoiceItems, $fromCreditItems})
  }
  
  
}