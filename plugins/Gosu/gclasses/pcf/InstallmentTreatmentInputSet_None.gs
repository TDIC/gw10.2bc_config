package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/payment/InstallmentTreatmentInputSet.None.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class InstallmentTreatmentInputSet_None extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($viewHelper :  gw.admin.paymentplan.InstallmentViewHelper) : void {
    __widgetOf(this, pcf.InstallmentTreatmentInputSet_None, SECTION_WIDGET_CLASS).setVariables(false, {$viewHelper})
  }
  
  function refreshVariables ($viewHelper :  gw.admin.paymentplan.InstallmentViewHelper) : void {
    __widgetOf(this, pcf.InstallmentTreatmentInputSet_None, SECTION_WIDGET_CLASS).setVariables(true, {$viewHelper})
  }
  
  
}