package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyCycleExceptionCommentsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyCycleExceptionCommentsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (agencyCyclesWithException :  AgencyCycleProcess[], isLatePayments :  boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyCycleExceptionCommentsPopup, {agencyCyclesWithException, isLatePayments}, 0)
  }
  
  static function push (agencyCyclesWithException :  AgencyCycleProcess[], isLatePayments :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyCycleExceptionCommentsPopup, {agencyCyclesWithException, isLatePayments}, 0).push()
  }
  
  
}