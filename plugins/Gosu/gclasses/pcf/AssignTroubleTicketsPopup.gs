package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/AssignTroubleTicketsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AssignTroubleTicketsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (assignmentPopup :  gw.api.web.troubleticket.TroubleTicketAssignmentPopup, selectedTroubleTickets :  TroubleTicket[]) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AssignTroubleTicketsPopup, {assignmentPopup, selectedTroubleTickets}, 0)
  }
  
  function pickValueAndCommit (value :  gw.api.assignment.Assignee) : void {
    __widgetOf(this, pcf.AssignTroubleTicketsPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (assignmentPopup :  gw.api.web.troubleticket.TroubleTicketAssignmentPopup, selectedTroubleTickets :  TroubleTicket[]) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AssignTroubleTicketsPopup, {assignmentPopup, selectedTroubleTickets}, 0).push()
  }
  
  
}