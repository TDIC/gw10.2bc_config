package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadUserLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AdminDataUploadUserLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($processor :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) : void {
    __widgetOf(this, pcf.AdminDataUploadUserLV, SECTION_WIDGET_CLASS).setVariables(false, {$processor})
  }
  
  function refreshVariables ($processor :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) : void {
    __widgetOf(this, pcf.AdminDataUploadUserLV, SECTION_WIDGET_CLASS).setVariables(true, {$processor})
  }
  
  
}