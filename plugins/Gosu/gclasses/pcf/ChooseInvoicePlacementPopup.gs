package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/ChooseInvoicePlacementPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ChooseInvoicePlacementPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry, payer :  gw.api.domain.invoice.InvoicePayer) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ChooseInvoicePlacementPopup, {entry, payer}, 0)
  }
  
  function pickValueAndCommit (value :  Invoice) : void {
    __widgetOf(this, pcf.ChooseInvoicePlacementPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry, payer :  gw.api.domain.invoice.InvoicePayer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChooseInvoicePlacementPopup, {entry, payer}, 0).push()
  }
  
  
}