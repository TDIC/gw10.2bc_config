package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/AccountSearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountSearchResultsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($accountSearchViews :  gw.api.database.IQueryBeanResult<AccountSearchView>, $tAccountOwnerReference :  gw.api.web.accounting.TAccountOwnerReference, $isWizard :  boolean, $showHyperlinks :  boolean, $showCheckboxes :  Boolean) : void {
    __widgetOf(this, pcf.AccountSearchResultsLV, SECTION_WIDGET_CLASS).setVariables(false, {$accountSearchViews, $tAccountOwnerReference, $isWizard, $showHyperlinks, $showCheckboxes})
  }
  
  function refreshVariables ($accountSearchViews :  gw.api.database.IQueryBeanResult<AccountSearchView>, $tAccountOwnerReference :  gw.api.web.accounting.TAccountOwnerReference, $isWizard :  boolean, $showHyperlinks :  boolean, $showCheckboxes :  Boolean) : void {
    __widgetOf(this, pcf.AccountSearchResultsLV, SECTION_WIDGET_CLASS).setVariables(true, {$accountSearchViews, $tAccountOwnerReference, $isWizard, $showHyperlinks, $showCheckboxes})
  }
  
  
}