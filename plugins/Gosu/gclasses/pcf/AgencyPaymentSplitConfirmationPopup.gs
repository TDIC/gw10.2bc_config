package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyPaymentSplitConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyPaymentSplitConfirmationPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (moneyReceived :  AgencyBillMoneyRcvd) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyPaymentSplitConfirmationPopup, {moneyReceived}, 0)
  }
  
  static function push (moneyReceived :  AgencyBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyPaymentSplitConfirmationPopup, {moneyReceived}, 0).push()
  }
  
  
}