package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/WizardsStep1AccountPolicySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class WizardsStep1AccountPolicySearchScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($targetOfWriteoff :  gw.api.web.accounting.TAccountOwnerReference, $possibleSourceType :  TAccountOwnerType, $showProducerAsSource :  boolean, $showPolicyAsSource :  boolean) : void {
    __widgetOf(this, pcf.WizardsStep1AccountPolicySearchScreen, SECTION_WIDGET_CLASS).setVariables(false, {$targetOfWriteoff, $possibleSourceType, $showProducerAsSource, $showPolicyAsSource})
  }
  
  function refreshVariables ($targetOfWriteoff :  gw.api.web.accounting.TAccountOwnerReference, $possibleSourceType :  TAccountOwnerType, $showProducerAsSource :  boolean, $showPolicyAsSource :  boolean) : void {
    __widgetOf(this, pcf.WizardsStep1AccountPolicySearchScreen, SECTION_WIDGET_CLASS).setVariables(true, {$targetOfWriteoff, $possibleSourceType, $showProducerAsSource, $showPolicyAsSource})
  }
  
  
}