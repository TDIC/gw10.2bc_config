package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerCodesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducerCodesLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($producer :  Producer, $isNew :  Boolean) : void {
    __widgetOf(this, pcf.ProducerCodesLV, SECTION_WIDGET_CLASS).setVariables(false, {$producer, $isNew})
  }
  
  function refreshVariables ($producer :  Producer, $isNew :  Boolean) : void {
    __widgetOf(this, pcf.ProducerCodesLV, SECTION_WIDGET_CLASS).setVariables(true, {$producer, $isNew})
  }
  
  
}