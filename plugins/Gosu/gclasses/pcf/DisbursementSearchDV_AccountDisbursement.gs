package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DisbursementSearchDV.AccountDisbursement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DisbursementSearchDV_AccountDisbursement extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($searchCriteria :  gw.search.DisbSearchCriteria, $disbursementSubtypeHolder :  typekey.Disbursement[]) : void {
    __widgetOf(this, pcf.DisbursementSearchDV_AccountDisbursement, SECTION_WIDGET_CLASS).setVariables(false, {$searchCriteria, $disbursementSubtypeHolder})
  }
  
  function refreshVariables ($searchCriteria :  gw.search.DisbSearchCriteria, $disbursementSubtypeHolder :  typekey.Disbursement[]) : void {
    __widgetOf(this, pcf.DisbursementSearchDV_AccountDisbursement, SECTION_WIDGET_CLASS).setVariables(true, {$searchCriteria, $disbursementSubtypeHolder})
  }
  
  
}