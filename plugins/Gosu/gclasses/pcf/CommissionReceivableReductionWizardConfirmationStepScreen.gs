package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/CommissionReceivableReductionWizardConfirmationStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CommissionReceivableReductionWizardConfirmationStepScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($helper :  gw.producer.CommissionReceivableReductionHelper) : void {
    __widgetOf(this, pcf.CommissionReceivableReductionWizardConfirmationStepScreen, SECTION_WIDGET_CLASS).setVariables(false, {$helper})
  }
  
  function refreshVariables ($helper :  gw.producer.CommissionReceivableReductionHelper) : void {
    __widgetOf(this, pcf.CommissionReceivableReductionWizardConfirmationStepScreen, SECTION_WIDGET_CLASS).setVariables(true, {$helper})
  }
  
  
}