package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyChangeWizardBasicsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyChangeWizardBasicsStepScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyChange :  PolicyChange) : void {
    __widgetOf(this, pcf.PolicyChangeWizardBasicsStepScreen, SECTION_WIDGET_CLASS).setVariables(false, {$policyChange})
  }
  
  function refreshVariables ($policyChange :  PolicyChange) : void {
    __widgetOf(this, pcf.PolicyChangeWizardBasicsStepScreen, SECTION_WIDGET_CLASS).setVariables(true, {$policyChange})
  }
  
  
}