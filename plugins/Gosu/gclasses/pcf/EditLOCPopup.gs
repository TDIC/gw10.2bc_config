package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/EditLOCPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class EditLOCPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (loc :  LetterOfCredit) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.EditLOCPopup, {loc}, 0)
  }
  
  static function push (loc :  LetterOfCredit) : pcf.api.Location {
    return __newDestinationForLocation(pcf.EditLOCPopup, {loc}, 0).push()
  }
  
  
}