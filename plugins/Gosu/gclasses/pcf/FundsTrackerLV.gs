package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/FundsTrackerLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class FundsTrackerLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($account :  Account, $type :  gw.entity.IEntityType) : void {
    __widgetOf(this, pcf.FundsTrackerLV, SECTION_WIDGET_CLASS).setVariables(false, {$account, $type})
  }
  
  function refreshVariables ($account :  Account, $type :  gw.entity.IEntityType) : void {
    __widgetOf(this, pcf.FundsTrackerLV, SECTION_WIDGET_CLASS).setVariables(true, {$account, $type})
  }
  
  
}