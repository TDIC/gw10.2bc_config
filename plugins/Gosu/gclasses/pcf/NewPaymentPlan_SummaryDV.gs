package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/payment/NewPaymentPlan_SummaryDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewPaymentPlan_SummaryDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($paymentPlanHelper :  gw.admin.paymentplan.PaymentPlanViewHelper) : void {
    __widgetOf(this, pcf.NewPaymentPlan_SummaryDV, SECTION_WIDGET_CLASS).setVariables(false, {$paymentPlanHelper})
  }
  
  function refreshVariables ($paymentPlanHelper :  gw.admin.paymentplan.PaymentPlanViewHelper) : void {
    __widgetOf(this, pcf.NewPaymentPlan_SummaryDV, SECTION_WIDGET_CLASS).setVariables(true, {$paymentPlanHelper})
  }
  
  
}