package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcess.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyProcess extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (account :  Account, delinquencyProcess :  DelinquencyProcess) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.DelinquencyProcess, {account, delinquencyProcess}, 0)
  }
  
  static function drilldown (account :  Account, delinquencyProcess :  DelinquencyProcess) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DelinquencyProcess, {account, delinquencyProcess}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (account :  Account, delinquencyProcess :  DelinquencyProcess) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DelinquencyProcess, {account, delinquencyProcess}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (account :  Account, delinquencyProcess :  DelinquencyProcess) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DelinquencyProcess, {account, delinquencyProcess}, 0).goInMain()
  }
  
  static function printPage (account :  Account, delinquencyProcess :  DelinquencyProcess) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DelinquencyProcess, {account, delinquencyProcess}, 0).printPage()
  }
  
  static function push (account :  Account, delinquencyProcess :  DelinquencyProcess) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DelinquencyProcess, {account, delinquencyProcess}, 0).push()
  }
  
  
}