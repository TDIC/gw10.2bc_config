package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/payment/PaymentPlanDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PaymentPlanDetail extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (paymentPlan :  PaymentPlan) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PaymentPlanDetail, {paymentPlan}, 0)
  }
  
  static function drilldown (paymentPlan :  PaymentPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentPlanDetail, {paymentPlan}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (paymentPlan :  PaymentPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentPlanDetail, {paymentPlan}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (paymentPlan :  PaymentPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentPlanDetail, {paymentPlan}, 0).goInMain()
  }
  
  static function printPage (paymentPlan :  PaymentPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentPlanDetail, {paymentPlan}, 0).printPage()
  }
  
  static function push (paymentPlan :  PaymentPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentPlanDetail, {paymentPlan}, 0).push()
  }
  
  
}