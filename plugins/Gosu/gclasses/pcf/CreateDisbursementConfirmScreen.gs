package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/CreateDisbursementConfirmScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CreateDisbursementConfirmScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($disbursement :  Disbursement) : void {
    __widgetOf(this, pcf.CreateDisbursementConfirmScreen, SECTION_WIDGET_CLASS).setVariables(false, {$disbursement})
  }
  
  function refreshVariables ($disbursement :  Disbursement) : void {
    __widgetOf(this, pcf.CreateDisbursementConfirmScreen, SECTION_WIDGET_CLASS).setVariables(true, {$disbursement})
  }
  
  
}