package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/InvoiceDayChangerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class InvoiceDayChangerPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (invoiceStream :  entity.InvoiceStream) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.InvoiceDayChangerPopup, {invoiceStream}, 0)
  }
  
  static function push (invoiceStream :  entity.InvoiceStream) : pcf.api.Location {
    return __newDestinationForLocation(pcf.InvoiceDayChangerPopup, {invoiceStream}, 0).push()
  }
  
  
}