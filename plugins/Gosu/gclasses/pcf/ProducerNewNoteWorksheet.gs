package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerNewNoteWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducerNewNoteWorksheet extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (producer :  Producer) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ProducerNewNoteWorksheet, {producer}, 0)
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerNewNoteWorksheet, {producer}, 0).goInWorkspace()
  }
  
  static function push (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerNewNoteWorksheet, {producer}, 0).push()
  }
  
  
}