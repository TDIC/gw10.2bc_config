package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/AgencyCommissionPaymentWizard_ConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyCommissionPaymentWizard_ConfirmationScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($incomingProducerPayment :  IncomingProducerPayment) : void {
    __widgetOf(this, pcf.AgencyCommissionPaymentWizard_ConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(false, {$incomingProducerPayment})
  }
  
  function refreshVariables ($incomingProducerPayment :  IncomingProducerPayment) : void {
    __widgetOf(this, pcf.AgencyCommissionPaymentWizard_ConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(true, {$incomingProducerPayment})
  }
  
  
}