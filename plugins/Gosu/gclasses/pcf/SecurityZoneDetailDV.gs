package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/securityzones/SecurityZoneDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class SecurityZoneDetailDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($securityZone :  SecurityZone) : void {
    __widgetOf(this, pcf.SecurityZoneDetailDV, SECTION_WIDGET_CLASS).setVariables(false, {$securityZone})
  }
  
  function refreshVariables ($securityZone :  SecurityZone) : void {
    __widgetOf(this, pcf.SecurityZoneDetailDV, SECTION_WIDGET_CLASS).setVariables(true, {$securityZone})
  }
  
  
}