package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountEvaluationInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountEvaluationInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($account :  Account, $contextDelinquencyProcess :  DelinquencyProcess) : void {
    __widgetOf(this, pcf.AccountEvaluationInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$account, $contextDelinquencyProcess})
  }
  
  function refreshVariables ($account :  Account, $contextDelinquencyProcess :  DelinquencyProcess) : void {
    __widgetOf(this, pcf.AccountEvaluationInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$account, $contextDelinquencyProcess})
  }
  
  
}