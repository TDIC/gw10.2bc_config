package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyProcessDetailPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (delinquencyProcess :  DelinquencyProcess) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.DelinquencyProcessDetailPopup, {delinquencyProcess}, 0)
  }
  
  static function push (delinquencyProcess :  DelinquencyProcess) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DelinquencyProcessDetailPopup, {delinquencyProcess}, 0).push()
  }
  
  
}