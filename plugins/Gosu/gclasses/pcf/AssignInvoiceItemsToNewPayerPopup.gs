package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AssignInvoiceItemsToNewPayerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AssignInvoiceItemsToNewPayerPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (invoiceItems :  InvoiceItem[], currentAccount :  Account, chargesToAssign :  Charge[]) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AssignInvoiceItemsToNewPayerPopup, {invoiceItems, currentAccount, chargesToAssign}, 0)
  }
  
  static function push (invoiceItems :  InvoiceItem[], currentAccount :  Account, chargesToAssign :  Charge[]) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AssignInvoiceItemsToNewPayerPopup, {invoiceItems, currentAccount, chargesToAssign}, 0).push()
  }
  
  
}