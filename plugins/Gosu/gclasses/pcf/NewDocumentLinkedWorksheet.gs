package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/NewDocumentLinkedWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewDocumentLinkedWorksheet extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (documentContainer :  DocumentContainer) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewDocumentLinkedWorksheet, {documentContainer}, 0)
  }
  
  static function createDestination (policyPeriod :  PolicyPeriod) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewDocumentLinkedWorksheet, {policyPeriod}, 1)
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (documentContainer :  DocumentContainer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewDocumentLinkedWorksheet, {documentContainer}, 0).goInWorkspace()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (policyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewDocumentLinkedWorksheet, {policyPeriod}, 1).goInWorkspace()
  }
  
  static function push (documentContainer :  DocumentContainer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewDocumentLinkedWorksheet, {documentContainer}, 0).push()
  }
  
  static function push (policyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewDocumentLinkedWorksheet, {policyPeriod}, 1).push()
  }
  
  
}