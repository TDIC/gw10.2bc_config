package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/ProducerPaymentReversalConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducerPaymentReversalConfirmationPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (payment :  ProducerPaymentRecd) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ProducerPaymentReversalConfirmationPopup, {payment}, 0)
  }
  
  static function push (payment :  ProducerPaymentRecd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerPaymentReversalConfirmationPopup, {payment}, 0).push()
  }
  
  
}