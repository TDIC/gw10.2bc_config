package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/MultiPaymentEntryWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class MultiPaymentEntryWizard extends com.guidewire.pl.web.codegen.WizardBase {
  static function createDestination (currency :  Currency) : pcf.api.Destination {
    return __newDestinationForWizard(pcf.MultiPaymentEntryWizard, {currency}, 0)
  }
  
  static function drilldown (currency :  Currency) : pcf.api.Location {
    return __newDestinationForWizard(pcf.MultiPaymentEntryWizard, {currency}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (currency :  Currency) : pcf.api.Location {
    return __newDestinationForWizard(pcf.MultiPaymentEntryWizard, {currency}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (currency :  Currency) : pcf.api.Location {
    return __newDestinationForWizard(pcf.MultiPaymentEntryWizard, {currency}, 0).goInMain()
  }
  
  static function printPage (currency :  Currency) : pcf.api.Location {
    return __newDestinationForWizard(pcf.MultiPaymentEntryWizard, {currency}, 0).printPage()
  }
  
  static function push (currency :  Currency) : pcf.api.Location {
    return __newDestinationForWizard(pcf.MultiPaymentEntryWizard, {currency}, 0).push()
  }
  
  
}