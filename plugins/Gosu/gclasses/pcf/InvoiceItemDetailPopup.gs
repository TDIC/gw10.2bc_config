package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/InvoiceItemDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class InvoiceItemDetailPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (invoiceItem :  InvoiceItem) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.InvoiceItemDetailPopup, {invoiceItem}, 0)
  }
  
  static function push (invoiceItem :  InvoiceItem) : pcf.api.Location {
    return __newDestinationForLocation(pcf.InvoiceItemDetailPopup, {invoiceItem}, 0).push()
  }
  
  
}