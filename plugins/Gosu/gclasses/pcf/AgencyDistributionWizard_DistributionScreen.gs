package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_DistributionScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyDistributionWizard_DistributionScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : void {
    __widgetOf(this, pcf.AgencyDistributionWizard_DistributionScreen, SECTION_WIDGET_CLASS).setVariables(false, {$wizardState})
  }
  
  function refreshVariables ($wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : void {
    __widgetOf(this, pcf.AgencyDistributionWizard_DistributionScreen, SECTION_WIDGET_CLASS).setVariables(true, {$wizardState})
  }
  
  
}