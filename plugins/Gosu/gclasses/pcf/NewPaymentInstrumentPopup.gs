package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/NewPaymentInstrumentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewPaymentInstrumentPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (paymentMethodOptions :  java.util.List<PaymentMethod>, account :  Account, oneTimeIsEditable :  boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewPaymentInstrumentPopup, {paymentMethodOptions, account, oneTimeIsEditable}, 1)
  }
  
  static function createDestination (paymentMethodOptions :  java.util.List<PaymentMethod>, account :  Account, oneTimeIsEditable :  boolean, creditCardHandler :  tdic.bc.integ.services.creditcard.TDIC_CreditCardHandler) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewPaymentInstrumentPopup, {paymentMethodOptions, account, oneTimeIsEditable, creditCardHandler}, 3)
  }
  
  static function createDestination (paymentMethodOptions :  java.util.List<PaymentMethod>, paymentRequest :  PaymentRequest) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewPaymentInstrumentPopup, {paymentMethodOptions, paymentRequest}, 4)
  }
  
  static function createDestination (paymentMethodOptions :  java.util.List<PaymentMethod>, producer :  Producer, oneTimeIsEditable :  boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewPaymentInstrumentPopup, {paymentMethodOptions, producer, oneTimeIsEditable}, 2)
  }
  
  static function createDestination (paymentMethodOptions :  java.util.List<PaymentMethod>, oneTimeIsEditable :  boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewPaymentInstrumentPopup, {paymentMethodOptions, oneTimeIsEditable}, 0)
  }
  
  function pickValueAndCommit (value :  PaymentInstrument) : void {
    __widgetOf(this, pcf.NewPaymentInstrumentPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (paymentMethodOptions :  java.util.List<PaymentMethod>, account :  Account, oneTimeIsEditable :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewPaymentInstrumentPopup, {paymentMethodOptions, account, oneTimeIsEditable}, 1).push()
  }
  
  static function push (paymentMethodOptions :  java.util.List<PaymentMethod>, account :  Account, oneTimeIsEditable :  boolean, creditCardHandler :  tdic.bc.integ.services.creditcard.TDIC_CreditCardHandler) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewPaymentInstrumentPopup, {paymentMethodOptions, account, oneTimeIsEditable, creditCardHandler}, 3).push()
  }
  
  static function push (paymentMethodOptions :  java.util.List<PaymentMethod>, paymentRequest :  PaymentRequest) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewPaymentInstrumentPopup, {paymentMethodOptions, paymentRequest}, 4).push()
  }
  
  static function push (paymentMethodOptions :  java.util.List<PaymentMethod>, producer :  Producer, oneTimeIsEditable :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewPaymentInstrumentPopup, {paymentMethodOptions, producer, oneTimeIsEditable}, 2).push()
  }
  
  static function push (paymentMethodOptions :  java.util.List<PaymentMethod>, oneTimeIsEditable :  boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewPaymentInstrumentPopup, {paymentMethodOptions, oneTimeIsEditable}, 0).push()
  }
  
  
}