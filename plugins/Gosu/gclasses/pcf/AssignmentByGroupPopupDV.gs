package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/AssignmentByGroupPopupDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AssignmentByGroupPopupDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($AssignmentPopup :  gw.api.assignment.AssignmentPopup, $SearchResult :  gw.api.assignment.AssignmentSearchResult, $SearchCriteria :  gw.api.assignment.AssignmentSearchCriteria, $selectedActivities :  Activity[]) : void {
    __widgetOf(this, pcf.AssignmentByGroupPopupDV, SECTION_WIDGET_CLASS).setVariables(false, {$AssignmentPopup, $SearchResult, $SearchCriteria, $selectedActivities})
  }
  
  function refreshVariables ($AssignmentPopup :  gw.api.assignment.AssignmentPopup, $SearchResult :  gw.api.assignment.AssignmentSearchResult, $SearchCriteria :  gw.api.assignment.AssignmentSearchCriteria, $selectedActivities :  Activity[]) : void {
    __widgetOf(this, pcf.AssignmentByGroupPopupDV, SECTION_WIDGET_CLASS).setVariables(true, {$AssignmentPopup, $SearchResult, $SearchCriteria, $selectedActivities})
  }
  
  
}