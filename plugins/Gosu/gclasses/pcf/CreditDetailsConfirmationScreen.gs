package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/CreditDetailsConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CreditDetailsConfirmationScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($account :  Account, $credit :  Credit) : void {
    __widgetOf(this, pcf.CreditDetailsConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(false, {$account, $credit})
  }
  
  function refreshVariables ($account :  Account, $credit :  Credit) : void {
    __widgetOf(this, pcf.CreditDetailsConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(true, {$account, $credit})
  }
  
  
}