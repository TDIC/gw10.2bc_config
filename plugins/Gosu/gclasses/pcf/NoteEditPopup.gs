package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/note/NoteEditPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NoteEditPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (Note :  Note) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NoteEditPopup, {Note}, 0)
  }
  
  static function createDestination (Note :  Note, docContainer :  DocumentContainer) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NoteEditPopup, {Note, docContainer}, 1)
  }
  
  static function push (Note :  Note) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NoteEditPopup, {Note}, 0).push()
  }
  
  static function push (Note :  Note, docContainer :  DocumentContainer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NoteEditPopup, {Note, docContainer}, 1).push()
  }
  
  
}