package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/MinimalTAccountOwnerDetailsDV.producer.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class MinimalTAccountOwnerDetailsDV_producer extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($tAccountOwner :  TAccountOwner, $unapplied :  UnappliedFund) : void {
    __widgetOf(this, pcf.MinimalTAccountOwnerDetailsDV_producer, SECTION_WIDGET_CLASS).setVariables(false, {$tAccountOwner, $unapplied})
  }
  
  function refreshVariables ($tAccountOwner :  TAccountOwner, $unapplied :  UnappliedFund) : void {
    __widgetOf(this, pcf.MinimalTAccountOwnerDetailsDV_producer, SECTION_WIDGET_CLASS).setVariables(true, {$tAccountOwner, $unapplied})
  }
  
  
}