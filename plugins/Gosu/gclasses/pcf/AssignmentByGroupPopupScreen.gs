package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/AssignmentByGroupPopupScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AssignmentByGroupPopupScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($AssignmentPopup :  gw.api.assignment.AssignmentPopup, $selectedActivities :  Activity[]) : void {
    __widgetOf(this, pcf.AssignmentByGroupPopupScreen, SECTION_WIDGET_CLASS).setVariables(false, {$AssignmentPopup, $selectedActivities})
  }
  
  function refreshVariables ($AssignmentPopup :  gw.api.assignment.AssignmentPopup, $selectedActivities :  Activity[]) : void {
    __widgetOf(this, pcf.AssignmentByGroupPopupScreen, SECTION_WIDGET_CLASS).setVariables(true, {$AssignmentPopup, $selectedActivities})
  }
  
  
}