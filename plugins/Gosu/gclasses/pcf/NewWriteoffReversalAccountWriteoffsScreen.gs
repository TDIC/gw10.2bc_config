package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalAccountWriteoffsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewWriteoffReversalAccountWriteoffsScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($reversal :  WriteoffReversal, $accountID :  gw.pl.persistence.core.Key) : void {
    __widgetOf(this, pcf.NewWriteoffReversalAccountWriteoffsScreen, SECTION_WIDGET_CLASS).setVariables(false, {$reversal, $accountID})
  }
  
  function refreshVariables ($reversal :  WriteoffReversal, $accountID :  gw.pl.persistence.core.Key) : void {
    __widgetOf(this, pcf.NewWriteoffReversalAccountWriteoffsScreen, SECTION_WIDGET_CLASS).setVariables(true, {$reversal, $accountID})
  }
  
  
}