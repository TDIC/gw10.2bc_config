package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/PolicySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicySearchScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($showHyperlinks :  Boolean, $isClearBundle :  boolean, $allowsArchiveInclusion :  boolean) : void {
    __widgetOf(this, pcf.PolicySearchScreen, SECTION_WIDGET_CLASS).setVariables(false, {$showHyperlinks, $isClearBundle, $allowsArchiveInclusion})
  }
  
  function refreshVariables ($showHyperlinks :  Boolean, $isClearBundle :  boolean, $allowsArchiveInclusion :  boolean) : void {
    __widgetOf(this, pcf.PolicySearchScreen, SECTION_WIDGET_CLASS).setVariables(true, {$showHyperlinks, $isClearBundle, $allowsArchiveInclusion})
  }
  
  
}