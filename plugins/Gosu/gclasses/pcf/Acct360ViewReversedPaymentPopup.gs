package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/Acct360ViewReversedPaymentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class Acct360ViewReversedPaymentPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (payment :  DirectBillMoneyRcvd) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.Acct360ViewReversedPaymentPopup, {payment}, 0)
  }
  
  static function push (payment :  DirectBillMoneyRcvd) : pcf.api.Location {
    return __newDestinationForLocation(pcf.Acct360ViewReversedPaymentPopup, {payment}, 0).push()
  }
  
  
}