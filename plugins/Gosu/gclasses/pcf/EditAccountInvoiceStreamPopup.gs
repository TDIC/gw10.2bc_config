package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/EditAccountInvoiceStreamPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class EditAccountInvoiceStreamPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (account :  Account, invoiceStream :  InvoiceStream) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.EditAccountInvoiceStreamPopup, {account, invoiceStream}, 0)
  }
  
  static function push (account :  Account, invoiceStream :  InvoiceStream) : pcf.api.Location {
    return __newDestinationForLocation(pcf.EditAccountInvoiceStreamPopup, {account, invoiceStream}, 0).push()
  }
  
  
}