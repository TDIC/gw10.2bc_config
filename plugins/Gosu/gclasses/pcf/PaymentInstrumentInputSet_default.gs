package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/PaymentInstrumentInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PaymentInstrumentInputSet_default extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($paymentInstrument :  PaymentInstrument) : void {
    __widgetOf(this, pcf.PaymentInstrumentInputSet_default, SECTION_WIDGET_CLASS).setVariables(false, {$paymentInstrument})
  }
  
  function refreshVariables ($paymentInstrument :  PaymentInstrument) : void {
    __widgetOf(this, pcf.PaymentInstrumentInputSet_default, SECTION_WIDGET_CLASS).setVariables(true, {$paymentInstrument})
  }
  
  
}