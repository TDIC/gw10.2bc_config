package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/HoldDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class HoldDetailsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (hold :  Hold) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.HoldDetailsPopup, {hold}, 0)
  }
  
  static function push (hold :  Hold) : pcf.api.Location {
    return __newDestinationForLocation(pcf.HoldDetailsPopup, {hold}, 0).push()
  }
  
  
}