package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyChargeOwnerDetailForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyChargeOwnerDetailForward extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (chargeOwnerView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyChargeOwnerDetailForward, {chargeOwnerView}, 0)
  }
  
  static function drilldown (chargeOwnerView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyChargeOwnerDetailForward, {chargeOwnerView}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (chargeOwnerView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyChargeOwnerDetailForward, {chargeOwnerView}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (chargeOwnerView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyChargeOwnerDetailForward, {chargeOwnerView}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (chargeOwnerView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyChargeOwnerDetailForward, {chargeOwnerView}, 0).goInWorkspace()
  }
  
  static function printPage (chargeOwnerView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyChargeOwnerDetailForward, {chargeOwnerView}, 0).printPage()
  }
  
  static function push (chargeOwnerView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyChargeOwnerDetailForward, {chargeOwnerView}, 0).push()
  }
  
  
}