package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlanDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CommissionPlanDetailPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (commissionPlan :  CommissionPlan) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.CommissionPlanDetailPopup, {commissionPlan}, 0)
  }
  
  static function push (commissionPlan :  CommissionPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CommissionPlanDetailPopup, {commissionPlan}, 0).push()
  }
  
  
}