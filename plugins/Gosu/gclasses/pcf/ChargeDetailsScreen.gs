package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/ChargeDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ChargeDetailsScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($helper :  gw.accounting.NewGeneralSingleChargeHelper) : void {
    __widgetOf(this, pcf.ChargeDetailsScreen, SECTION_WIDGET_CLASS).setVariables(false, {$helper})
  }
  
  function refreshVariables ($helper :  gw.accounting.NewGeneralSingleChargeHelper) : void {
    __widgetOf(this, pcf.ChargeDetailsScreen, SECTION_WIDGET_CLASS).setVariables(true, {$helper})
  }
  
  
}