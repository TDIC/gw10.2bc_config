package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/PaymentRequestSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PaymentRequestSearchDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($searchCriteria :  gw.search.PaymentRequestSearchCriteria) : void {
    __widgetOf(this, pcf.PaymentRequestSearchDV, SECTION_WIDGET_CLASS).setVariables(false, {$searchCriteria})
  }
  
  function refreshVariables ($searchCriteria :  gw.search.PaymentRequestSearchCriteria) : void {
    __widgetOf(this, pcf.PaymentRequestSearchDV, SECTION_WIDGET_CLASS).setVariables(true, {$searchCriteria})
  }
  
  
}