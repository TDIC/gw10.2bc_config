package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralRequirementUnSegregatePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CollateralRequirementUnSegregatePopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (collateralRequirement :  CollateralRequirement) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.CollateralRequirementUnSegregatePopup, {collateralRequirement}, 0)
  }
  
  static function push (collateralRequirement :  CollateralRequirement) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CollateralRequirementUnSegregatePopup, {collateralRequirement}, 0).push()
  }
  
  
}