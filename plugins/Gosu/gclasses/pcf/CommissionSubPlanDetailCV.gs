package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/commission/CommissionSubPlanDetailCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CommissionSubPlanDetailCV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($commissionSubPlan :  CommissionSubPlan) : void {
    __widgetOf(this, pcf.CommissionSubPlanDetailCV, SECTION_WIDGET_CLASS).setVariables(false, {$commissionSubPlan})
  }
  
  function refreshVariables ($commissionSubPlan :  CommissionSubPlan) : void {
    __widgetOf(this, pcf.CommissionSubPlanDetailCV, SECTION_WIDGET_CLASS).setVariables(true, {$commissionSubPlan})
  }
  
  
}