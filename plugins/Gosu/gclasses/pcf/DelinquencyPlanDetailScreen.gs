package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyPlanDetailScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($dlnqPlan :  DelinquencyPlan) : void {
    __widgetOf(this, pcf.DelinquencyPlanDetailScreen, SECTION_WIDGET_CLASS).setVariables(false, {$dlnqPlan})
  }
  
  function refreshVariables ($dlnqPlan :  DelinquencyPlan) : void {
    __widgetOf(this, pcf.DelinquencyPlanDetailScreen, SECTION_WIDGET_CLASS).setVariables(true, {$dlnqPlan})
  }
  
  
}