package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketRelatedEntitiesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TroubleTicketRelatedEntitiesPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (troubleTicket :  TroubleTicket, createTroubleTicketHelper :  CreateTroubleTicketHelper) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TroubleTicketRelatedEntitiesPopup, {troubleTicket, createTroubleTicketHelper}, 0)
  }
  
  static function push (troubleTicket :  TroubleTicket, createTroubleTicketHelper :  CreateTroubleTicketHelper) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TroubleTicketRelatedEntitiesPopup, {troubleTicket, createTroubleTicketHelper}, 0).push()
  }
  
  
}