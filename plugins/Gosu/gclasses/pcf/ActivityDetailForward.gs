package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/ActivityDetailForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ActivityDetailForward extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (activity :  Activity) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ActivityDetailForward, {activity}, 0)
  }
  
  static function drilldown (activity :  Activity) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ActivityDetailForward, {activity}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (activity :  Activity) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ActivityDetailForward, {activity}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (activity :  Activity) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ActivityDetailForward, {activity}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (activity :  Activity) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ActivityDetailForward, {activity}, 0).goInWorkspace()
  }
  
  static function printPage (activity :  Activity) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ActivityDetailForward, {activity}, 0).printPage()
  }
  
  static function push (activity :  Activity) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ActivityDetailForward, {activity}, 0).push()
  }
  
  
}