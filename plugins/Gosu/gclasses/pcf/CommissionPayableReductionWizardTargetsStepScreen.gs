package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/CommissionPayableReductionWizardTargetsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CommissionPayableReductionWizardTargetsStepScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($chargeCommissions :  ChargeCommission[], $approvalRequiredForWriteoff :  List<Boolean>) : void {
    __widgetOf(this, pcf.CommissionPayableReductionWizardTargetsStepScreen, SECTION_WIDGET_CLASS).setVariables(false, {$chargeCommissions, $approvalRequiredForWriteoff})
  }
  
  function refreshVariables ($chargeCommissions :  ChargeCommission[], $approvalRequiredForWriteoff :  List<Boolean>) : void {
    __widgetOf(this, pcf.CommissionPayableReductionWizardTargetsStepScreen, SECTION_WIDGET_CLASS).setVariables(true, {$chargeCommissions, $approvalRequiredForWriteoff})
  }
  
  
}