package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/RenewPolicyWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class RenewPolicyWizard extends com.guidewire.pl.web.codegen.WizardBase {
  static function createDestination (prevPolicyPeriod :  PolicyPeriod) : pcf.api.Destination {
    return __newDestinationForWizard(pcf.RenewPolicyWizard, {prevPolicyPeriod}, 0)
  }
  
  static function drilldown (prevPolicyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForWizard(pcf.RenewPolicyWizard, {prevPolicyPeriod}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (prevPolicyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForWizard(pcf.RenewPolicyWizard, {prevPolicyPeriod}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (prevPolicyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForWizard(pcf.RenewPolicyWizard, {prevPolicyPeriod}, 0).goInMain()
  }
  
  static function printPage (prevPolicyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForWizard(pcf.RenewPolicyWizard, {prevPolicyPeriod}, 0).printPage()
  }
  
  static function push (prevPolicyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForWizard(pcf.RenewPolicyWizard, {prevPolicyPeriod}, 0).push()
  }
  
  
}