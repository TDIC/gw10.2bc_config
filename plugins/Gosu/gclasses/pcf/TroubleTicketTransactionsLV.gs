package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/accounting/TroubleTicketTransactionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TroubleTicketTransactionsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($transactions :  entity.Transaction[], $showCheckboxes :  Boolean) : void {
    __widgetOf(this, pcf.TroubleTicketTransactionsLV, SECTION_WIDGET_CLASS).setVariables(false, {$transactions, $showCheckboxes})
  }
  
  function refreshVariables ($transactions :  entity.Transaction[], $showCheckboxes :  Boolean) : void {
    __widgetOf(this, pcf.TroubleTicketTransactionsLV, SECTION_WIDGET_CLASS).setVariables(true, {$transactions, $showCheckboxes})
  }
  
  
}