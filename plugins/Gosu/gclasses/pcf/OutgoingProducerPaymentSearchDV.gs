package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/OutgoingProducerPaymentSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class OutgoingProducerPaymentSearchDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($searchCriteria :  gw.search.OutgoingProducerPmntSearchCriteria) : void {
    __widgetOf(this, pcf.OutgoingProducerPaymentSearchDV, SECTION_WIDGET_CLASS).setVariables(false, {$searchCriteria})
  }
  
  function refreshVariables ($searchCriteria :  gw.search.OutgoingProducerPmntSearchCriteria) : void {
    __widgetOf(this, pcf.OutgoingProducerPaymentSearchDV, SECTION_WIDGET_CLASS).setVariables(true, {$searchCriteria})
  }
  
  
}