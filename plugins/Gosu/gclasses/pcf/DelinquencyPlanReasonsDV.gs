package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanReasonsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyPlanReasonsDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($dlnqPlan :  DelinquencyPlan) : void {
    __widgetOf(this, pcf.DelinquencyPlanReasonsDV, SECTION_WIDGET_CLASS).setVariables(false, {$dlnqPlan})
  }
  
  function refreshVariables ($dlnqPlan :  DelinquencyPlan) : void {
    __widgetOf(this, pcf.DelinquencyPlanReasonsDV, SECTION_WIDGET_CLASS).setVariables(true, {$dlnqPlan})
  }
  
  
}