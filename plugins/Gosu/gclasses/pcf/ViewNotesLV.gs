package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/ViewNotesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ViewNotesLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($NoteList :  gw.api.database.IQueryBeanResult<Note>) : void {
    __widgetOf(this, pcf.ViewNotesLV, SECTION_WIDGET_CLASS).setVariables(false, {$NoteList})
  }
  
  function refreshVariables ($NoteList :  gw.api.database.IQueryBeanResult<Note>) : void {
    __widgetOf(this, pcf.ViewNotesLV, SECTION_WIDGET_CLASS).setVariables(true, {$NoteList})
  }
  
  
}