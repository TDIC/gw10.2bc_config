package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/Acct360ChargeViewPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class Acct360ChargeViewPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (billInstruction :  BillingInstruction) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.Acct360ChargeViewPopup, {billInstruction}, 0)
  }
  
  static function createDestination (plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.Acct360ChargeViewPopup, {plcyPeriod}, 1)
  }
  
  static function push (billInstruction :  BillingInstruction) : pcf.api.Location {
    return __newDestinationForLocation(pcf.Acct360ChargeViewPopup, {billInstruction}, 0).push()
  }
  
  static function push (plcyPeriod :  PolicyPeriod) : pcf.api.Location {
    return __newDestinationForLocation(pcf.Acct360ChargeViewPopup, {plcyPeriod}, 1).push()
  }
  
  
}