package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/ChargeTransactionDetailsForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ChargeTransactionDetailsForward extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (charge :  Charge) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ChargeTransactionDetailsForward, {charge}, 0)
  }
  
  static function drilldown (charge :  Charge) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChargeTransactionDetailsForward, {charge}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (charge :  Charge) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChargeTransactionDetailsForward, {charge}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (charge :  Charge) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChargeTransactionDetailsForward, {charge}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (charge :  Charge) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChargeTransactionDetailsForward, {charge}, 0).goInWorkspace()
  }
  
  static function printPage (charge :  Charge) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChargeTransactionDetailsForward, {charge}, 0).printPage()
  }
  
  static function push (charge :  Charge) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ChargeTransactionDetailsForward, {charge}, 0).push()
  }
  
  
}