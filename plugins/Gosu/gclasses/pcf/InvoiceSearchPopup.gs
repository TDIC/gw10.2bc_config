package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/InvoiceSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class InvoiceSearchPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination () : pcf.api.Destination {
    return __newDestinationForLocation(pcf.InvoiceSearchPopup, {}, 0)
  }
  
  static function createDestination (account :  Account) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.InvoiceSearchPopup, {account}, 1)
  }
  
  function pickValueAndCommit (value :  Invoice) : void {
    __widgetOf(this, pcf.InvoiceSearchPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push () : pcf.api.Location {
    return __newDestinationForLocation(pcf.InvoiceSearchPopup, {}, 0).push()
  }
  
  static function push (account :  Account) : pcf.api.Location {
    return __newDestinationForLocation(pcf.InvoiceSearchPopup, {account}, 1).push()
  }
  
  
}