package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessEventsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyProcessEventsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($DelinquencyProcess :  DelinquencyProcess) : void {
    __widgetOf(this, pcf.DelinquencyProcessEventsLV, SECTION_WIDGET_CLASS).setVariables(false, {$DelinquencyProcess})
  }
  
  function refreshVariables ($DelinquencyProcess :  DelinquencyProcess) : void {
    __widgetOf(this, pcf.DelinquencyProcessEventsLV, SECTION_WIDGET_CLASS).setVariables(true, {$DelinquencyProcess})
  }
  
  
}