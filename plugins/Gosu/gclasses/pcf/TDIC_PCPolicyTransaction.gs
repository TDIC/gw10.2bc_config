package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/exitpoints/TDIC_PCPolicyTransaction.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_PCPolicyTransaction extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (policyCenterSystemURL :  String, offerNumber :  String, jobIndex :  int) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_PCPolicyTransaction, {policyCenterSystemURL, offerNumber, jobIndex}, 0)
  }
  
  static function drilldown (policyCenterSystemURL :  String, offerNumber :  String, jobIndex :  int) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_PCPolicyTransaction, {policyCenterSystemURL, offerNumber, jobIndex}, 0).drilldown()
  }
  
  static function printPage (policyCenterSystemURL :  String, offerNumber :  String, jobIndex :  int) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_PCPolicyTransaction, {policyCenterSystemURL, offerNumber, jobIndex}, 0).printPage()
  }
  
  static function push (policyCenterSystemURL :  String, offerNumber :  String, jobIndex :  int) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_PCPolicyTransaction, {policyCenterSystemURL, offerNumber, jobIndex}, 0).push()
  }
  
  
}