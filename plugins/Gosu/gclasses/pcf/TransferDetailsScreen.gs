package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/TransferDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TransferDetailsScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($fundsTransferUtil :  gw.api.web.transaction.FundsTransferUtil) : void {
    __widgetOf(this, pcf.TransferDetailsScreen, SECTION_WIDGET_CLASS).setVariables(false, {$fundsTransferUtil})
  }
  
  function refreshVariables ($fundsTransferUtil :  gw.api.web.transaction.FundsTransferUtil) : void {
    __widgetOf(this, pcf.TransferDetailsScreen, SECTION_WIDGET_CLASS).setVariables(true, {$fundsTransferUtil})
  }
  
  
}