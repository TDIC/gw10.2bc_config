package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/BatchPaymentDetailsPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class BatchPaymentDetailsPage extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (batchPayment :  BatchPayment) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.BatchPaymentDetailsPage, {batchPayment}, 0)
  }
  
  static function drilldown (batchPayment :  BatchPayment) : pcf.api.Location {
    return __newDestinationForLocation(pcf.BatchPaymentDetailsPage, {batchPayment}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (batchPayment :  BatchPayment) : pcf.api.Location {
    return __newDestinationForLocation(pcf.BatchPaymentDetailsPage, {batchPayment}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (batchPayment :  BatchPayment) : pcf.api.Location {
    return __newDestinationForLocation(pcf.BatchPaymentDetailsPage, {batchPayment}, 0).goInMain()
  }
  
  static function printPage (batchPayment :  BatchPayment) : pcf.api.Location {
    return __newDestinationForLocation(pcf.BatchPaymentDetailsPage, {batchPayment}, 0).printPage()
  }
  
  static function push (batchPayment :  BatchPayment) : pcf.api.Location {
    return __newDestinationForLocation(pcf.BatchPaymentDetailsPage, {batchPayment}, 0).push()
  }
  
  
}