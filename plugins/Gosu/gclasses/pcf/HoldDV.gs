package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/HoldDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class HoldDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($hold :  Hold, $ShowRelatedEntities :  Boolean) : void {
    __widgetOf(this, pcf.HoldDV, SECTION_WIDGET_CLASS).setVariables(false, {$hold, $ShowRelatedEntities})
  }
  
  function refreshVariables ($hold :  Hold, $ShowRelatedEntities :  Boolean) : void {
    __widgetOf(this, pcf.HoldDV, SECTION_WIDGET_CLASS).setVariables(true, {$hold, $ShowRelatedEntities})
  }
  
  
}