package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/CreateDisbursementDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CreateDisbursementDetailScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($disbursementVar :  AccountDisbursement) : void {
    __widgetOf(this, pcf.CreateDisbursementDetailScreen, SECTION_WIDGET_CLASS).setVariables(false, {$disbursementVar})
  }
  
  function refreshVariables ($disbursementVar :  AccountDisbursement) : void {
    __widgetOf(this, pcf.CreateDisbursementDetailScreen, SECTION_WIDGET_CLASS).setVariables(true, {$disbursementVar})
  }
  
  
}