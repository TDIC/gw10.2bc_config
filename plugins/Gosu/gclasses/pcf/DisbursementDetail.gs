package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/DisbursementDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DisbursementDetail extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (disbursement :  Disbursement, startInEdit :  Boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.DisbursementDetail, {disbursement, startInEdit}, 0)
  }
  
  static function drilldown (disbursement :  Disbursement, startInEdit :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DisbursementDetail, {disbursement, startInEdit}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (disbursement :  Disbursement, startInEdit :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DisbursementDetail, {disbursement, startInEdit}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (disbursement :  Disbursement, startInEdit :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DisbursementDetail, {disbursement, startInEdit}, 0).goInMain()
  }
  
  static function printPage (disbursement :  Disbursement, startInEdit :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DisbursementDetail, {disbursement, startInEdit}, 0).printPage()
  }
  
  static function push (disbursement :  Disbursement, startInEdit :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DisbursementDetail, {disbursement, startInEdit}, 0).push()
  }
  
  
}