package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_SavePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyDistributionWizard_SavePopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyDistributionWizard_SavePopup, {wizardState}, 0)
  }
  
  static function push (wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyDistributionWizard_SavePopup, {wizardState}, 0).push()
  }
  
  
}