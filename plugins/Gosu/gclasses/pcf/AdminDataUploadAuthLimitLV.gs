package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadAuthLimitLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AdminDataUploadAuthLimitLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($processor :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) : void {
    __widgetOf(this, pcf.AdminDataUploadAuthLimitLV, SECTION_WIDGET_CLASS).setVariables(false, {$processor})
  }
  
  function refreshVariables ($processor :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) : void {
    __widgetOf(this, pcf.AdminDataUploadAuthLimitLV, SECTION_WIDGET_CLASS).setVariables(true, {$processor})
  }
  
  
}