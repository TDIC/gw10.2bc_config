package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyMultiPaymentSplitConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyMultiPaymentSplitConfirmationPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (originalPaymentEntry :  PaymentEntry) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyMultiPaymentSplitConfirmationPopup, {originalPaymentEntry}, 0)
  }
  
  static function push (originalPaymentEntry :  PaymentEntry) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyMultiPaymentSplitConfirmationPopup, {originalPaymentEntry}, 0).push()
  }
  
  
}