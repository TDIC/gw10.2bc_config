package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/agencybill/AgencyBillPlanDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyBillPlanDetailPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (agencyBillPlan :  AgencyBillPlan) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyBillPlanDetailPopup, {agencyBillPlan}, 0)
  }
  
  static function push (agencyBillPlan :  AgencyBillPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyBillPlanDetailPopup, {agencyBillPlan}, 0).push()
  }
  
  
}