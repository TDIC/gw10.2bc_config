package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/NewTemplateDocumentDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewTemplateDocumentDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($documentBCContext :  gw.document.DocumentBCContext, $documentCreationInfo :  gw.document.DocumentCreationInfo, $documentContainer :  DocumentContainer) : void {
    __widgetOf(this, pcf.NewTemplateDocumentDV, SECTION_WIDGET_CLASS).setVariables(false, {$documentBCContext, $documentCreationInfo, $documentContainer})
  }
  
  function refreshVariables ($documentBCContext :  gw.document.DocumentBCContext, $documentCreationInfo :  gw.document.DocumentCreationInfo, $documentContainer :  DocumentContainer) : void {
    __widgetOf(this, pcf.NewTemplateDocumentDV, SECTION_WIDGET_CLASS).setVariables(true, {$documentBCContext, $documentCreationInfo, $documentContainer})
  }
  
  
}