package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/AgencyCommissionPaymentWizard_SelectProducerScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyCommissionPaymentWizard_SelectProducerScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($incomingProducerPayment :  IncomingProducerPayment) : void {
    __widgetOf(this, pcf.AgencyCommissionPaymentWizard_SelectProducerScreen, SECTION_WIDGET_CLASS).setVariables(false, {$incomingProducerPayment})
  }
  
  function refreshVariables ($incomingProducerPayment :  IncomingProducerPayment) : void {
    __widgetOf(this, pcf.AgencyCommissionPaymentWizard_SelectProducerScreen, SECTION_WIDGET_CLASS).setVariables(true, {$incomingProducerPayment})
  }
  
  
}