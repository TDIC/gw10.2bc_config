package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyTargetDetailsForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyTargetDetailsForward extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (target :  gw.api.domain.delinquency.DelinquencyTarget) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.DelinquencyTargetDetailsForward, {target}, 0)
  }
  
  static function drilldown (target :  gw.api.domain.delinquency.DelinquencyTarget) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DelinquencyTargetDetailsForward, {target}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (target :  gw.api.domain.delinquency.DelinquencyTarget) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DelinquencyTargetDetailsForward, {target}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (target :  gw.api.domain.delinquency.DelinquencyTarget) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DelinquencyTargetDetailsForward, {target}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (target :  gw.api.domain.delinquency.DelinquencyTarget) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DelinquencyTargetDetailsForward, {target}, 0).goInWorkspace()
  }
  
  static function printPage (target :  gw.api.domain.delinquency.DelinquencyTarget) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DelinquencyTargetDetailsForward, {target}, 0).printPage()
  }
  
  static function push (target :  gw.api.domain.delinquency.DelinquencyTarget) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DelinquencyTargetDetailsForward, {target}, 0).push()
  }
  
  
}