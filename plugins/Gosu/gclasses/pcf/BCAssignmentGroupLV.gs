package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/groups/BCAssignmentGroupLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class BCAssignmentGroupLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($Groups :  gw.api.database.IQueryBeanResult<Group>) : void {
    __widgetOf(this, pcf.BCAssignmentGroupLV, SECTION_WIDGET_CLASS).setVariables(false, {$Groups})
  }
  
  function refreshVariables ($Groups :  gw.api.database.IQueryBeanResult<Group>) : void {
    __widgetOf(this, pcf.BCAssignmentGroupLV, SECTION_WIDGET_CLASS).setVariables(true, {$Groups})
  }
  
  
}