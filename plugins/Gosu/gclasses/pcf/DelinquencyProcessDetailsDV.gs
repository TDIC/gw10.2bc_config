package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyProcessDetailsDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($delinquencyProcess :  DelinquencyProcess) : void {
    __widgetOf(this, pcf.DelinquencyProcessDetailsDV, SECTION_WIDGET_CLASS).setVariables(false, {$delinquencyProcess})
  }
  
  function refreshVariables ($delinquencyProcess :  DelinquencyProcess) : void {
    __widgetOf(this, pcf.DelinquencyProcessDetailsDV, SECTION_WIDGET_CLASS).setVariables(true, {$delinquencyProcess})
  }
  
  
}