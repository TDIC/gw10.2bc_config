package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerContactDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducerContactDetailPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (contact :  ProducerContact) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ProducerContactDetailPopup, {contact}, 0)
  }
  
  function pickValueAndCommit (value :  ProducerContact) : void {
    __widgetOf(this, pcf.ProducerContactDetailPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (contact :  ProducerContact) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerContactDetailPopup, {contact}, 0).push()
  }
  
  
}