package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/TransferDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TransferDetailsDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($fundsTransferUtil :  gw.api.web.transaction.FundsTransferUtil, $canEditPage :  Boolean) : void {
    __widgetOf(this, pcf.TransferDetailsDV, SECTION_WIDGET_CLASS).setVariables(false, {$fundsTransferUtil, $canEditPage})
  }
  
  function refreshVariables ($fundsTransferUtil :  gw.api.web.transaction.FundsTransferUtil, $canEditPage :  Boolean) : void {
    __widgetOf(this, pcf.TransferDetailsDV, SECTION_WIDGET_CLASS).setVariables(true, {$fundsTransferUtil, $canEditPage})
  }
  
  
}