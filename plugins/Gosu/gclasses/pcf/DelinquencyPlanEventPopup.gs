package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanEventPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DelinquencyPlanEventPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (dlnqPlanReason :  DelinquencyPlanReason) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.DelinquencyPlanEventPopup, {dlnqPlanReason}, 0)
  }
  
  function pickValueAndCommit (value :  DelinquencyPlanEvent) : void {
    __widgetOf(this, pcf.DelinquencyPlanEventPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (dlnqPlanReason :  DelinquencyPlanReason) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DelinquencyPlanEventPopup, {dlnqPlanReason}, 0).push()
  }
  
  
}