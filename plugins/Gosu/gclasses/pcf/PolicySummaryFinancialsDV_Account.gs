package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicySummaryFinancialsDV.Account.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicySummaryFinancialsDV_Account extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyPeriod :  PolicyPeriod, $summaryHelper :  gw.web.policy.PolicySummaryHelper) : void {
    __widgetOf(this, pcf.PolicySummaryFinancialsDV_Account, SECTION_WIDGET_CLASS).setVariables(false, {$policyPeriod, $summaryHelper})
  }
  
  function refreshVariables ($policyPeriod :  PolicyPeriod, $summaryHelper :  gw.web.policy.PolicySummaryHelper) : void {
    __widgetOf(this, pcf.PolicySummaryFinancialsDV_Account, SECTION_WIDGET_CLASS).setVariables(true, {$policyPeriod, $summaryHelper})
  }
  
  
}