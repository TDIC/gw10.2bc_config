package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/ChargeBreakdownItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ChargeBreakdownItemsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($initializer :  gw.api.domain.charge.ChargeInitializer) : void {
    __widgetOf(this, pcf.ChargeBreakdownItemsLV, SECTION_WIDGET_CLASS).setVariables(false, {$initializer})
  }
  
  function refreshVariables ($initializer :  gw.api.domain.charge.ChargeInitializer) : void {
    __widgetOf(this, pcf.ChargeBreakdownItemsLV, SECTION_WIDGET_CLASS).setVariables(true, {$initializer})
  }
  
  
}