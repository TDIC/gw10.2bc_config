package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/NewMultiPaymentScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewMultiPaymentScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($newMultiPayment :  NewMultiPayment, $currency :  Currency, $editing :  boolean) : void {
    __widgetOf(this, pcf.NewMultiPaymentScreen, SECTION_WIDGET_CLASS).setVariables(false, {$newMultiPayment, $currency, $editing})
  }
  
  function refreshVariables ($newMultiPayment :  NewMultiPayment, $currency :  Currency, $editing :  boolean) : void {
    __widgetOf(this, pcf.NewMultiPaymentScreen, SECTION_WIDGET_CLASS).setVariables(true, {$newMultiPayment, $currency, $editing})
  }
  
  
}