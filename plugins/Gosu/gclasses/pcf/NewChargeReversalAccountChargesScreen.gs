package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalAccountChargesScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewChargeReversalAccountChargesScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($reversal :  ChargeReversal, $accountID :  gw.pl.persistence.core.Key) : void {
    __widgetOf(this, pcf.NewChargeReversalAccountChargesScreen, SECTION_WIDGET_CLASS).setVariables(false, {$reversal, $accountID})
  }
  
  function refreshVariables ($reversal :  ChargeReversal, $accountID :  gw.pl.persistence.core.Key) : void {
    __widgetOf(this, pcf.NewChargeReversalAccountChargesScreen, SECTION_WIDGET_CLASS).setVariables(true, {$reversal, $accountID})
  }
  
  
}