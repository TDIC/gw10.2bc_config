package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/Acct360InvoiceViewPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class Acct360InvoiceViewPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (invoice :  AccountInvoice) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.Acct360InvoiceViewPopup, {invoice}, 0)
  }
  
  static function push (invoice :  AccountInvoice) : pcf.api.Location {
    return __newDestinationForLocation(pcf.Acct360InvoiceViewPopup, {invoice}, 0).push()
  }
  
  
}