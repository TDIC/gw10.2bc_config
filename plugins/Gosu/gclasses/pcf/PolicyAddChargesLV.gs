package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyAddChargesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyAddChargesLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($billingInstruction :  BillingInstruction, $policyPeriod :  PolicyPeriod, $chargeToInvoicingOverridesViewMap :  java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>) : void {
    __widgetOf(this, pcf.PolicyAddChargesLV, SECTION_WIDGET_CLASS).setVariables(false, {$billingInstruction, $policyPeriod, $chargeToInvoicingOverridesViewMap})
  }
  
  function refreshVariables ($billingInstruction :  BillingInstruction, $policyPeriod :  PolicyPeriod, $chargeToInvoicingOverridesViewMap :  java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>) : void {
    __widgetOf(this, pcf.PolicyAddChargesLV, SECTION_WIDGET_CLASS).setVariables(true, {$billingInstruction, $policyPeriod, $chargeToInvoicingOverridesViewMap})
  }
  
  
}