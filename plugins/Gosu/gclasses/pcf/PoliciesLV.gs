package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/PoliciesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PoliciesLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policies :  gw.api.database.IQueryBeanResult<Policy>) : void {
    __widgetOf(this, pcf.PoliciesLV, SECTION_WIDGET_CLASS).setVariables(false, {$policies})
  }
  
  function refreshVariables ($policies :  gw.api.database.IQueryBeanResult<Policy>) : void {
    __widgetOf(this, pcf.PoliciesLV, SECTION_WIDGET_CLASS).setVariables(true, {$policies})
  }
  
  
}