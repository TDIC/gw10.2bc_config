package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/CommissionReceivableReductionWizardTargetsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CommissionReceivableReductionWizardTargetsStepScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($helper :  gw.producer.CommissionReceivableReductionHelper) : void {
    __widgetOf(this, pcf.CommissionReceivableReductionWizardTargetsStepScreen, SECTION_WIDGET_CLASS).setVariables(false, {$helper})
  }
  
  function refreshVariables ($helper :  gw.producer.CommissionReceivableReductionHelper) : void {
    __widgetOf(this, pcf.CommissionReceivableReductionWizardTargetsStepScreen, SECTION_WIDGET_CLASS).setVariables(true, {$helper})
  }
  
  
}