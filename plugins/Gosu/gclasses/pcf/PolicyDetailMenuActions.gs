package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyDetailMenuActions extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyPeriod :  PolicyPeriod) : void {
    __widgetOf(this, pcf.PolicyDetailMenuActions, SECTION_WIDGET_CLASS).setVariables(false, {$policyPeriod})
  }
  
  function refreshVariables ($policyPeriod :  PolicyPeriod) : void {
    __widgetOf(this, pcf.PolicyDetailMenuActions, SECTION_WIDGET_CLASS).setVariables(true, {$policyPeriod})
  }
  
  
}