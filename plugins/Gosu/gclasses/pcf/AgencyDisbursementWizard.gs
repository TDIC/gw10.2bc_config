package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyDisbursementWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyDisbursementWizard extends com.guidewire.pl.web.codegen.WizardBase {
  static function createDestination (producer :  Producer, fromCreditItems :  boolean) : pcf.api.Destination {
    return __newDestinationForWizard(pcf.AgencyDisbursementWizard, {producer, fromCreditItems}, 0)
  }
  
  static function drilldown (producer :  Producer, fromCreditItems :  boolean) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AgencyDisbursementWizard, {producer, fromCreditItems}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (producer :  Producer, fromCreditItems :  boolean) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AgencyDisbursementWizard, {producer, fromCreditItems}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (producer :  Producer, fromCreditItems :  boolean) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AgencyDisbursementWizard, {producer, fromCreditItems}, 0).goInMain()
  }
  
  static function printPage (producer :  Producer, fromCreditItems :  boolean) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AgencyDisbursementWizard, {producer, fromCreditItems}, 0).printPage()
  }
  
  static function push (producer :  Producer, fromCreditItems :  boolean) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AgencyDisbursementWizard, {producer, fromCreditItems}, 0).push()
  }
  
  
}