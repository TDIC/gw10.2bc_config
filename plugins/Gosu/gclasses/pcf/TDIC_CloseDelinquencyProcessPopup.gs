package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/TDIC_CloseDelinquencyProcessPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TDIC_CloseDelinquencyProcessPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (dlnqTargets :  gw.api.domain.delinquency.DelinquencyTarget[]) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TDIC_CloseDelinquencyProcessPopup, {dlnqTargets}, 0)
  }
  
  static function push (dlnqTargets :  gw.api.domain.delinquency.DelinquencyTarget[]) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TDIC_CloseDelinquencyProcessPopup, {dlnqTargets}, 0).push()
  }
  
  
}