package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultipleAccountsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class SelectMultipleAccountsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination () : pcf.api.Destination {
    return __newDestinationForLocation(pcf.SelectMultipleAccountsPopup, {}, 0)
  }
  
  function pickValueAndCommit (value :  Account[]) : void {
    __widgetOf(this, pcf.SelectMultipleAccountsPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push () : pcf.api.Location {
    return __newDestinationForLocation(pcf.SelectMultipleAccountsPopup, {}, 0).push()
  }
  
  
}