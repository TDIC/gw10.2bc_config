package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerDetailNotes.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducerDetailNotes extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (producer :  Producer, isClearBundle :  Boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ProducerDetailNotes, {producer, isClearBundle}, 0)
  }
  
  static function drilldown (producer :  Producer, isClearBundle :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerDetailNotes, {producer, isClearBundle}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (producer :  Producer, isClearBundle :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerDetailNotes, {producer, isClearBundle}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (producer :  Producer, isClearBundle :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerDetailNotes, {producer, isClearBundle}, 0).goInMain()
  }
  
  static function printPage (producer :  Producer, isClearBundle :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerDetailNotes, {producer, isClearBundle}, 0).printPage()
  }
  
  static function push (producer :  Producer, isClearBundle :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerDetailNotes, {producer, isClearBundle}, 0).push()
  }
  
  
}