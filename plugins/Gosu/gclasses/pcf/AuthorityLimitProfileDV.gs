package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/authoritylimits/AuthorityLimitProfileDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AuthorityLimitProfileDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($authorityLimitProfile :  AuthorityLimitProfile) : void {
    __widgetOf(this, pcf.AuthorityLimitProfileDV, SECTION_WIDGET_CLASS).setVariables(false, {$authorityLimitProfile})
  }
  
  function refreshVariables ($authorityLimitProfile :  AuthorityLimitProfile) : void {
    __widgetOf(this, pcf.AuthorityLimitProfileDV, SECTION_WIDGET_CLASS).setVariables(true, {$authorityLimitProfile})
  }
  
  
}