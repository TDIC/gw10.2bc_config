package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerStatement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducerStatement extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (producerStatement :  ProducerStatement) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ProducerStatement, {producerStatement}, 0)
  }
  
  static function drilldown (producerStatement :  ProducerStatement) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerStatement, {producerStatement}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (producerStatement :  ProducerStatement) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerStatement, {producerStatement}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (producerStatement :  ProducerStatement) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerStatement, {producerStatement}, 0).goInMain()
  }
  
  static function printPage (producerStatement :  ProducerStatement) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerStatement, {producerStatement}, 0).printPage()
  }
  
  static function push (producerStatement :  ProducerStatement) : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerStatement, {producerStatement}, 0).push()
  }
  
  
}