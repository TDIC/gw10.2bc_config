package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/LateDistributionExceptionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class LateDistributionExceptionsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($agencyCyclesWithException :  gw.api.database.IQueryBeanResult<AgencyCycleProcess>) : void {
    __widgetOf(this, pcf.LateDistributionExceptionsLV, SECTION_WIDGET_CLASS).setVariables(false, {$agencyCyclesWithException})
  }
  
  function refreshVariables ($agencyCyclesWithException :  gw.api.database.IQueryBeanResult<AgencyCycleProcess>) : void {
    __widgetOf(this, pcf.LateDistributionExceptionsLV, SECTION_WIDGET_CLASS).setVariables(true, {$agencyCyclesWithException})
  }
  
  
}