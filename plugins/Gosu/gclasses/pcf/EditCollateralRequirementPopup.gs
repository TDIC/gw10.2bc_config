package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/EditCollateralRequirementPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class EditCollateralRequirementPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (requirement :  CollateralRequirement) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.EditCollateralRequirementPopup, {requirement}, 0)
  }
  
  static function push (requirement :  CollateralRequirement) : pcf.api.Location {
    return __newDestinationForLocation(pcf.EditCollateralRequirementPopup, {requirement}, 0).push()
  }
  
  
}