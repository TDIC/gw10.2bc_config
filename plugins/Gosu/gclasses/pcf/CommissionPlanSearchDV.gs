package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/CommissionPlanSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CommissionPlanSearchDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($searchCriteria :  CommissionPlanSearchCriteria) : void {
    __widgetOf(this, pcf.CommissionPlanSearchDV, SECTION_WIDGET_CLASS).setVariables(false, {$searchCriteria})
  }
  
  function refreshVariables ($searchCriteria :  CommissionPlanSearchCriteria) : void {
    __widgetOf(this, pcf.CommissionPlanSearchDV, SECTION_WIDGET_CLASS).setVariables(true, {$searchCriteria})
  }
  
  
}