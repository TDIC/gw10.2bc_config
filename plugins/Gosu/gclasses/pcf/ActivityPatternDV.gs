package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/activitypatterns/ActivityPatternDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ActivityPatternDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($activityPattern :  ActivityPattern, $isNew :  boolean) : void {
    __widgetOf(this, pcf.ActivityPatternDV, SECTION_WIDGET_CLASS).setVariables(false, {$activityPattern, $isNew})
  }
  
  function refreshVariables ($activityPattern :  ActivityPattern, $isNew :  boolean) : void {
    __widgetOf(this, pcf.ActivityPatternDV, SECTION_WIDGET_CLASS).setVariables(true, {$activityPattern, $isNew})
  }
  
  
}