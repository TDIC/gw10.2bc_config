package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/payment/InstallmentAlignmentInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class InstallmentAlignmentInputSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($paymentPlan :  PaymentPlan) : void {
    __widgetOf(this, pcf.InstallmentAlignmentInputSet, SECTION_WIDGET_CLASS).setVariables(false, {$paymentPlan})
  }
  
  function refreshVariables ($paymentPlan :  PaymentPlan) : void {
    __widgetOf(this, pcf.InstallmentAlignmentInputSet, SECTION_WIDGET_CLASS).setVariables(true, {$paymentPlan})
  }
  
  
}