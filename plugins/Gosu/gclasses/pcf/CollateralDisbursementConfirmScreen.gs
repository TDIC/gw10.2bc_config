package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralDisbursementConfirmScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CollateralDisbursementConfirmScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($disbursement :  CollateralDisbursement) : void {
    __widgetOf(this, pcf.CollateralDisbursementConfirmScreen, SECTION_WIDGET_CLASS).setVariables(false, {$disbursement})
  }
  
  function refreshVariables ($disbursement :  CollateralDisbursement) : void {
    __widgetOf(this, pcf.CollateralDisbursementConfirmScreen, SECTION_WIDGET_CLASS).setVariables(true, {$disbursement})
  }
  
  
}