package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/DocumentTemplateSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DocumentTemplateSearchPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination () : pcf.api.Destination {
    return __newDestinationForLocation(pcf.DocumentTemplateSearchPopup, {}, 0)
  }
  
  static function createDestination (documentContainer :  DocumentContainer, documentCreationInfo :  gw.document.DocumentCreationInfo) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.DocumentTemplateSearchPopup, {documentContainer, documentCreationInfo}, 1)
  }
  
  static function createDestination (symbolProvider :  gw.document.SymbolProvider) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.DocumentTemplateSearchPopup, {symbolProvider}, 2)
  }
  
  function pickValueAndCommit (value :  gw.plugin.document.IDocumentTemplateDescriptor) : void {
    __widgetOf(this, pcf.DocumentTemplateSearchPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push () : pcf.api.Location {
    return __newDestinationForLocation(pcf.DocumentTemplateSearchPopup, {}, 0).push()
  }
  
  static function push (documentContainer :  DocumentContainer, documentCreationInfo :  gw.document.DocumentCreationInfo) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DocumentTemplateSearchPopup, {documentContainer, documentCreationInfo}, 1).push()
  }
  
  static function push (symbolProvider :  gw.document.SymbolProvider) : pcf.api.Location {
    return __newDestinationForLocation(pcf.DocumentTemplateSearchPopup, {symbolProvider}, 2).push()
  }
  
  
}