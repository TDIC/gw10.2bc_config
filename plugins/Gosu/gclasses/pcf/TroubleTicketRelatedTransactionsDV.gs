package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketRelatedTransactionsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TroubleTicketRelatedTransactionsDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($TroubleTicket :  TroubleTicket) : void {
    __widgetOf(this, pcf.TroubleTicketRelatedTransactionsDV, SECTION_WIDGET_CLASS).setVariables(false, {$TroubleTicket})
  }
  
  function refreshVariables ($TroubleTicket :  TroubleTicket) : void {
    __widgetOf(this, pcf.TroubleTicketRelatedTransactionsDV, SECTION_WIDGET_CLASS).setVariables(true, {$TroubleTicket})
  }
  
  
}