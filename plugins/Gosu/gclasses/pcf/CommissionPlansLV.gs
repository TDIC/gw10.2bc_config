package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlansLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CommissionPlansLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($commissionPlans :  gw.api.database.IQueryBeanResult<CommissionPlan>) : void {
    __widgetOf(this, pcf.CommissionPlansLV, SECTION_WIDGET_CLASS).setVariables(false, {$commissionPlans})
  }
  
  function refreshVariables ($commissionPlans :  gw.api.database.IQueryBeanResult<CommissionPlan>) : void {
    __widgetOf(this, pcf.CommissionPlansLV, SECTION_WIDGET_CLASS).setVariables(true, {$commissionPlans})
  }
  
  
}