package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailTroubleTickets.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyDetailTroubleTickets extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (plcyPeriod :  PolicyPeriod, relatedToPolicy :  Boolean, relatedToPolicyPeriod :  Boolean) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PolicyDetailTroubleTickets, {plcyPeriod, relatedToPolicy, relatedToPolicyPeriod}, 0)
  }
  
  static function drilldown (plcyPeriod :  PolicyPeriod, relatedToPolicy :  Boolean, relatedToPolicyPeriod :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailTroubleTickets, {plcyPeriod, relatedToPolicy, relatedToPolicyPeriod}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (plcyPeriod :  PolicyPeriod, relatedToPolicy :  Boolean, relatedToPolicyPeriod :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailTroubleTickets, {plcyPeriod, relatedToPolicy, relatedToPolicyPeriod}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (plcyPeriod :  PolicyPeriod, relatedToPolicy :  Boolean, relatedToPolicyPeriod :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailTroubleTickets, {plcyPeriod, relatedToPolicy, relatedToPolicyPeriod}, 0).goInMain()
  }
  
  static function printPage (plcyPeriod :  PolicyPeriod, relatedToPolicy :  Boolean, relatedToPolicyPeriod :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailTroubleTickets, {plcyPeriod, relatedToPolicy, relatedToPolicyPeriod}, 0).printPage()
  }
  
  static function push (plcyPeriod :  PolicyPeriod, relatedToPolicy :  Boolean, relatedToPolicyPeriod :  Boolean) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyDetailTroubleTickets, {plcyPeriod, relatedToPolicy, relatedToPolicyPeriod}, 0).push()
  }
  
  
}