package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyAddChargesListDetailPanel.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyAddChargesListDetailPanel extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($billingInstruction :  PlcyBillingInstruction, $policyPeriod :  PolicyPeriod, $chargeToInvoicingOverridesViewMap :  java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>) : void {
    __widgetOf(this, pcf.PolicyAddChargesListDetailPanel, SECTION_WIDGET_CLASS).setVariables(false, {$billingInstruction, $policyPeriod, $chargeToInvoicingOverridesViewMap})
  }
  
  function refreshVariables ($billingInstruction :  PlcyBillingInstruction, $policyPeriod :  PolicyPeriod, $chargeToInvoicingOverridesViewMap :  java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>) : void {
    __widgetOf(this, pcf.PolicyAddChargesListDetailPanel, SECTION_WIDGET_CLASS).setVariables(true, {$billingInstruction, $policyPeriod, $chargeToInvoicingOverridesViewMap})
  }
  
  
}