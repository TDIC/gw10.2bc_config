package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyInvoiceItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyInvoiceItemsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($invoiceItems :  InvoiceItem[]) : void {
    __widgetOf(this, pcf.AgencyInvoiceItemsLV, SECTION_WIDGET_CLASS).setVariables(false, {$invoiceItems})
  }
  
  function refreshVariables ($invoiceItems :  InvoiceItem[]) : void {
    __widgetOf(this, pcf.AgencyInvoiceItemsLV, SECTION_WIDGET_CLASS).setVariables(true, {$invoiceItems})
  }
  
  
}