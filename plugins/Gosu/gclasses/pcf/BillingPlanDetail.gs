package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class BillingPlanDetail extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (billingPlan :  BillingPlan) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.BillingPlanDetail, {billingPlan}, 0)
  }
  
  static function drilldown (billingPlan :  BillingPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.BillingPlanDetail, {billingPlan}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (billingPlan :  BillingPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.BillingPlanDetail, {billingPlan}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (billingPlan :  BillingPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.BillingPlanDetail, {billingPlan}, 0).goInMain()
  }
  
  static function printPage (billingPlan :  BillingPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.BillingPlanDetail, {billingPlan}, 0).printPage()
  }
  
  static function push (billingPlan :  BillingPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.BillingPlanDetail, {billingPlan}, 0).push()
  }
  
  
}