package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentStandardPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewCommissionPaymentStandardPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (producer :  Producer) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.NewCommissionPaymentStandardPopup, {producer}, 0)
  }
  
  static function push (producer :  Producer) : pcf.api.Location {
    return __newDestinationForLocation(pcf.NewCommissionPaymentStandardPopup, {producer}, 0).push()
  }
  
  
}