package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultiplePoliciesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class SelectMultiplePoliciesPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination () : pcf.api.Destination {
    return __newDestinationForLocation(pcf.SelectMultiplePoliciesPopup, {}, 0)
  }
  
  function pickValueAndCommit (value :  Policy[]) : void {
    __widgetOf(this, pcf.SelectMultiplePoliciesPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push () : pcf.api.Location {
    return __newDestinationForLocation(pcf.SelectMultiplePoliciesPopup, {}, 0).push()
  }
  
  
}