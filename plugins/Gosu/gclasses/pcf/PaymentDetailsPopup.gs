package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/PaymentDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PaymentDetailsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (paymentReceipt :  PaymentReceipt) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PaymentDetailsPopup, {paymentReceipt}, 0)
  }
  
  static function push (paymentReceipt :  PaymentReceipt) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentDetailsPopup, {paymentReceipt}, 0).push()
  }
  
  
}