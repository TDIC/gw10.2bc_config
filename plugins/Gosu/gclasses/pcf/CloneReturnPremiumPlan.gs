package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/invoice/CloneReturnPremiumPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CloneReturnPremiumPlan extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (returnPremiumPlan :  ReturnPremiumPlan) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.CloneReturnPremiumPlan, {returnPremiumPlan}, 0)
  }
  
  static function drilldown (returnPremiumPlan :  ReturnPremiumPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CloneReturnPremiumPlan, {returnPremiumPlan}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (returnPremiumPlan :  ReturnPremiumPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CloneReturnPremiumPlan, {returnPremiumPlan}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (returnPremiumPlan :  ReturnPremiumPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CloneReturnPremiumPlan, {returnPremiumPlan}, 0).goInMain()
  }
  
  static function printPage (returnPremiumPlan :  ReturnPremiumPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CloneReturnPremiumPlan, {returnPremiumPlan}, 0).printPage()
  }
  
  static function push (returnPremiumPlan :  ReturnPremiumPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.CloneReturnPremiumPlan, {returnPremiumPlan}, 0).push()
  }
  
  
}