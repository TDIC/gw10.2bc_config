package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountPaymentForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AccountPaymentForward extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (moneyReceivedID :  String) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AccountPaymentForward, {moneyReceivedID}, 0)
  }
  
  static function drilldown (moneyReceivedID :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountPaymentForward, {moneyReceivedID}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (moneyReceivedID :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountPaymentForward, {moneyReceivedID}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (moneyReceivedID :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountPaymentForward, {moneyReceivedID}, 0).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInWorkspace (moneyReceivedID :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountPaymentForward, {moneyReceivedID}, 0).goInWorkspace()
  }
  
  static function printPage (moneyReceivedID :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountPaymentForward, {moneyReceivedID}, 0).printPage()
  }
  
  static function push (moneyReceivedID :  String) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AccountPaymentForward, {moneyReceivedID}, 0).push()
  }
  
  
}