package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TransactionsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TransactionsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (transactions :  entity.Transaction[]) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.TransactionsPopup, {transactions}, 0)
  }
  
  function pickValueAndCommit (value :  Transaction[]) : void {
    __widgetOf(this, pcf.TransactionsPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (transactions :  entity.Transaction[]) : pcf.api.Location {
    return __newDestinationForLocation(pcf.TransactionsPopup, {transactions}, 0).push()
  }
  
  
}