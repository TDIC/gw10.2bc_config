package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralManualDrawdownConfirmScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CollateralManualDrawdownConfirmScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($collateralDrawdown :  gw.api.web.collateral.CollateralDrawdownUtil) : void {
    __widgetOf(this, pcf.CollateralManualDrawdownConfirmScreen, SECTION_WIDGET_CLASS).setVariables(false, {$collateralDrawdown})
  }
  
  function refreshVariables ($collateralDrawdown :  gw.api.web.collateral.CollateralDrawdownUtil) : void {
    __widgetOf(this, pcf.CollateralManualDrawdownConfirmScreen, SECTION_WIDGET_CLASS).setVariables(true, {$collateralDrawdown})
  }
  
  
}