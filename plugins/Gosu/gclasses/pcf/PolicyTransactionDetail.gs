package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyTransactionDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyTransactionDetail extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (policyPeriod :  PolicyPeriod, transaction :  Transaction) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PolicyTransactionDetail, {policyPeriod, transaction}, 0)
  }
  
  static function drilldown (policyPeriod :  PolicyPeriod, transaction :  Transaction) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyTransactionDetail, {policyPeriod, transaction}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (policyPeriod :  PolicyPeriod, transaction :  Transaction) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyTransactionDetail, {policyPeriod, transaction}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (policyPeriod :  PolicyPeriod, transaction :  Transaction) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyTransactionDetail, {policyPeriod, transaction}, 0).goInMain()
  }
  
  static function printPage (policyPeriod :  PolicyPeriod, transaction :  Transaction) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyTransactionDetail, {policyPeriod, transaction}, 0).printPage()
  }
  
  static function push (policyPeriod :  PolicyPeriod, transaction :  Transaction) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyTransactionDetail, {policyPeriod, transaction}, 0).push()
  }
  
  
}