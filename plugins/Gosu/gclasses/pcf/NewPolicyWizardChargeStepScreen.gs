package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/NewPolicyWizardChargeStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewPolicyWizardChargeStepScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($account :  Account, $policyPeriod :  PolicyPeriod, $billingInstruction :  NewPlcyPeriodBI, $chargeToInvoicingOverridesViewMap :  java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>, $view :  gw.web.policy.PolicyWizardChargeStepScreenView) : void {
    __widgetOf(this, pcf.NewPolicyWizardChargeStepScreen, SECTION_WIDGET_CLASS).setVariables(false, {$account, $policyPeriod, $billingInstruction, $chargeToInvoicingOverridesViewMap, $view})
  }
  
  function refreshVariables ($account :  Account, $policyPeriod :  PolicyPeriod, $billingInstruction :  NewPlcyPeriodBI, $chargeToInvoicingOverridesViewMap :  java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>, $view :  gw.web.policy.PolicyWizardChargeStepScreenView) : void {
    __widgetOf(this, pcf.NewPolicyWizardChargeStepScreen, SECTION_WIDGET_CLASS).setVariables(true, {$account, $policyPeriod, $billingInstruction, $chargeToInvoicingOverridesViewMap, $view})
  }
  
  
}