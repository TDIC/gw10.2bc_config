package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ContactSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ContactSearchScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($isProducerContact :  boolean) : void {
    __widgetOf(this, pcf.ContactSearchScreen, SECTION_WIDGET_CLASS).setVariables(false, {$isProducerContact})
  }
  
  function refreshVariables ($isProducerContact :  boolean) : void {
    __widgetOf(this, pcf.ContactSearchScreen, SECTION_WIDGET_CLASS).setVariables(true, {$isProducerContact})
  }
  
  
}