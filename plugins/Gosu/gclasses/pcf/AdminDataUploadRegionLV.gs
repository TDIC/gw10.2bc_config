package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadRegionLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AdminDataUploadRegionLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($processor :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) : void {
    __widgetOf(this, pcf.AdminDataUploadRegionLV, SECTION_WIDGET_CLASS).setVariables(false, {$processor})
  }
  
  function refreshVariables ($processor :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) : void {
    __widgetOf(this, pcf.AdminDataUploadRegionLV, SECTION_WIDGET_CLASS).setVariables(true, {$processor})
  }
  
  
}