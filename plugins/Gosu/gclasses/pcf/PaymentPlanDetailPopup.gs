package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PaymentPlanDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PaymentPlanDetailPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (paymentPlan :  PaymentPlan) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PaymentPlanDetailPopup, {paymentPlan}, 0)
  }
  
  static function push (paymentPlan :  PaymentPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PaymentPlanDetailPopup, {paymentPlan}, 0).push()
  }
  
  
}