package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopBatchPaymentsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DesktopBatchPaymentsScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($desktopBatchPaymentsView :  gw.web.payment.batch.DesktopBatchPaymentsView) : void {
    __widgetOf(this, pcf.DesktopBatchPaymentsScreen, SECTION_WIDGET_CLASS).setVariables(false, {$desktopBatchPaymentsView})
  }
  
  function refreshVariables ($desktopBatchPaymentsView :  gw.web.payment.batch.DesktopBatchPaymentsView) : void {
    __widgetOf(this, pcf.DesktopBatchPaymentsScreen, SECTION_WIDGET_CLASS).setVariables(true, {$desktopBatchPaymentsView})
  }
  
  
}