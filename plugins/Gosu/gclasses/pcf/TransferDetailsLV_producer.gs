package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/TransferDetailsLV.producer.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TransferDetailsLV_producer extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($fundsTransferUtil :  gw.api.web.transaction.FundsTransferUtil, $canEditPage :  boolean) : void {
    __widgetOf(this, pcf.TransferDetailsLV_producer, SECTION_WIDGET_CLASS).setVariables(false, {$fundsTransferUtil, $canEditPage})
  }
  
  function refreshVariables ($fundsTransferUtil :  gw.api.web.transaction.FundsTransferUtil, $canEditPage :  boolean) : void {
    __widgetOf(this, pcf.TransferDetailsLV_producer, SECTION_WIDGET_CLASS).setVariables(true, {$fundsTransferUtil, $canEditPage})
  }
  
  
}