package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/SelectCreditItemsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class SelectCreditItemsScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($producer :  Producer, $currentInvoiceItems :  java.util.List<InvoiceItem>, $disbursement :  AgencyDisbursement, $payCommissionReference :  boolean[]) : void {
    __widgetOf(this, pcf.SelectCreditItemsScreen, SECTION_WIDGET_CLASS).setVariables(false, {$producer, $currentInvoiceItems, $disbursement, $payCommissionReference})
  }
  
  function refreshVariables ($producer :  Producer, $currentInvoiceItems :  java.util.List<InvoiceItem>, $disbursement :  AgencyDisbursement, $payCommissionReference :  boolean[]) : void {
    __widgetOf(this, pcf.SelectCreditItemsScreen, SECTION_WIDGET_CLASS).setVariables(true, {$producer, $currentInvoiceItems, $disbursement, $payCommissionReference})
  }
  
  
}