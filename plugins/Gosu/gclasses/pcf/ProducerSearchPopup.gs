package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ProducerSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ProducerSearchPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination () : pcf.api.Destination {
    return __newDestinationForLocation(pcf.ProducerSearchPopup, {}, 0)
  }
  
  function pickValueAndCommit (value :  Producer) : void {
    __widgetOf(this, pcf.ProducerSearchPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push () : pcf.api.Location {
    return __newDestinationForLocation(pcf.ProducerSearchPopup, {}, 0).push()
  }
  
  
}