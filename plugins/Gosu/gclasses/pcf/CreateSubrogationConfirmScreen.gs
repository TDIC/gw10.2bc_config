package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/CreateSubrogationConfirmScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CreateSubrogationConfirmScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($subrogation :  Subrogation) : void {
    __widgetOf(this, pcf.CreateSubrogationConfirmScreen, SECTION_WIDGET_CLASS).setVariables(false, {$subrogation})
  }
  
  function refreshVariables ($subrogation :  Subrogation) : void {
    __widgetOf(this, pcf.CreateSubrogationConfirmScreen, SECTION_WIDGET_CLASS).setVariables(true, {$subrogation})
  }
  
  
}