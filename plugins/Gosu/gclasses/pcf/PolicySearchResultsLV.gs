package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/PolicySearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicySearchResultsLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policySearchViews :  gw.api.database.IQueryBeanResult<PolicySearchView>, $tAccountOwnerReference :  gw.api.web.accounting.TAccountOwnerReference, $isWizard :  boolean, $showHyperlinks :  boolean, $showCheckboxes :  Boolean) : void {
    __widgetOf(this, pcf.PolicySearchResultsLV, SECTION_WIDGET_CLASS).setVariables(false, {$policySearchViews, $tAccountOwnerReference, $isWizard, $showHyperlinks, $showCheckboxes})
  }
  
  function refreshVariables ($policySearchViews :  gw.api.database.IQueryBeanResult<PolicySearchView>, $tAccountOwnerReference :  gw.api.web.accounting.TAccountOwnerReference, $isWizard :  boolean, $showHyperlinks :  boolean, $showCheckboxes :  Boolean) : void {
    __widgetOf(this, pcf.PolicySearchResultsLV, SECTION_WIDGET_CLASS).setVariables(true, {$policySearchViews, $tAccountOwnerReference, $isWizard, $showHyperlinks, $showCheckboxes})
  }
  
  
}