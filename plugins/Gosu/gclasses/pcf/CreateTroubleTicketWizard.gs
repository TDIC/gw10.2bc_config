package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/CreateTroubleTicketWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CreateTroubleTicketWizard extends com.guidewire.pl.web.codegen.WizardBase {
  static function createDestination (relatedEntity :  KeyableBean) : pcf.api.Destination {
    return __newDestinationForWizard(pcf.CreateTroubleTicketWizard, {relatedEntity}, 0)
  }
  
  static function drilldown (relatedEntity :  KeyableBean) : pcf.api.Location {
    return __newDestinationForWizard(pcf.CreateTroubleTicketWizard, {relatedEntity}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (relatedEntity :  KeyableBean) : pcf.api.Location {
    return __newDestinationForWizard(pcf.CreateTroubleTicketWizard, {relatedEntity}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (relatedEntity :  KeyableBean) : pcf.api.Location {
    return __newDestinationForWizard(pcf.CreateTroubleTicketWizard, {relatedEntity}, 0).goInMain()
  }
  
  static function printPage (relatedEntity :  KeyableBean) : pcf.api.Location {
    return __newDestinationForWizard(pcf.CreateTroubleTicketWizard, {relatedEntity}, 0).printPage()
  }
  
  static function push (relatedEntity :  KeyableBean) : pcf.api.Location {
    return __newDestinationForWizard(pcf.CreateTroubleTicketWizard, {relatedEntity}, 0).push()
  }
  
  
}