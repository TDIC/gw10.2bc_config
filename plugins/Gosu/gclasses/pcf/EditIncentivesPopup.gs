package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/commission/EditIncentivesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class EditIncentivesPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (commissionSubPlan :  CommissionSubPlan, incentiveType :  typekey.Incentive) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.EditIncentivesPopup, {commissionSubPlan, incentiveType}, 0)
  }
  
  static function push (commissionSubPlan :  CommissionSubPlan, incentiveType :  typekey.Incentive) : pcf.api.Location {
    return __newDestinationForLocation(pcf.EditIncentivesPopup, {commissionSubPlan, incentiveType}, 0).push()
  }
  
  
}