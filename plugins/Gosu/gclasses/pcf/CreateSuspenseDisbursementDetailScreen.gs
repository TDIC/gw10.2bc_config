package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/CreateSuspenseDisbursementDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CreateSuspenseDisbursementDetailScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($disbursementVar :  SuspenseDisbursement) : void {
    __widgetOf(this, pcf.CreateSuspenseDisbursementDetailScreen, SECTION_WIDGET_CLASS).setVariables(false, {$disbursementVar})
  }
  
  function refreshVariables ($disbursementVar :  SuspenseDisbursement) : void {
    __widgetOf(this, pcf.CreateSuspenseDisbursementDetailScreen, SECTION_WIDGET_CLASS).setVariables(true, {$disbursementVar})
  }
  
  
}