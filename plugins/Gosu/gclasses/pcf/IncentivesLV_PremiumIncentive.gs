package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/commission/IncentivesLV.PremiumIncentive.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class IncentivesLV_PremiumIncentive extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($commissionSubPlan :  CommissionSubPlan) : void {
    __widgetOf(this, pcf.IncentivesLV_PremiumIncentive, SECTION_WIDGET_CLASS).setVariables(false, {$commissionSubPlan})
  }
  
  function refreshVariables ($commissionSubPlan :  CommissionSubPlan) : void {
    __widgetOf(this, pcf.IncentivesLV_PremiumIncentive, SECTION_WIDGET_CLASS).setVariables(true, {$commissionSubPlan})
  }
  
  
}