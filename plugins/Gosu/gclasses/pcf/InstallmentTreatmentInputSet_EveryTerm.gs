package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/payment/InstallmentTreatmentInputSet.EveryTerm.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class InstallmentTreatmentInputSet_EveryTerm extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($viewHelper :  gw.admin.paymentplan.InstallmentViewHelper) : void {
    __widgetOf(this, pcf.InstallmentTreatmentInputSet_EveryTerm, SECTION_WIDGET_CLASS).setVariables(false, {$viewHelper})
  }
  
  function refreshVariables ($viewHelper :  gw.admin.paymentplan.InstallmentViewHelper) : void {
    __widgetOf(this, pcf.InstallmentTreatmentInputSet_EveryTerm, SECTION_WIDGET_CLASS).setVariables(true, {$viewHelper})
  }
  
  
}