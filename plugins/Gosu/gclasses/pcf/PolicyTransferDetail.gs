package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyTransferDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyTransferDetail extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (policyHistory :  PolicyHistory) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.PolicyTransferDetail, {policyHistory}, 0)
  }
  
  static function drilldown (policyHistory :  PolicyHistory) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyTransferDetail, {policyHistory}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (policyHistory :  PolicyHistory) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyTransferDetail, {policyHistory}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (policyHistory :  PolicyHistory) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyTransferDetail, {policyHistory}, 0).goInMain()
  }
  
  static function printPage (policyHistory :  PolicyHistory) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyTransferDetail, {policyHistory}, 0).printPage()
  }
  
  static function push (policyHistory :  PolicyHistory) : pcf.api.Location {
    return __newDestinationForLocation(pcf.PolicyTransferDetail, {policyHistory}, 0).push()
  }
  
  
}