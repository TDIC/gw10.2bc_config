package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyChangeWizardChargesStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class PolicyChangeWizardChargesStepScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($policyPeriod :  PolicyPeriod, $policyChange :  PolicyChange, $chargeToInvoicingOverridesMap :  java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>, $view :  gw.web.policy.PolicyWizardChargeStepScreenView) : void {
    __widgetOf(this, pcf.PolicyChangeWizardChargesStepScreen, SECTION_WIDGET_CLASS).setVariables(false, {$policyPeriod, $policyChange, $chargeToInvoicingOverridesMap, $view})
  }
  
  function refreshVariables ($policyPeriod :  PolicyPeriod, $policyChange :  PolicyChange, $chargeToInvoicingOverridesMap :  java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>, $view :  gw.web.policy.PolicyWizardChargeStepScreenView) : void {
    __widgetOf(this, pcf.PolicyChangeWizardChargesStepScreen, SECTION_WIDGET_CLASS).setVariables(true, {$policyPeriod, $policyChange, $chargeToInvoicingOverridesMap, $view})
  }
  
  
}