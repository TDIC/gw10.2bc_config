package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/BatchPaymentsActionConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class BatchPaymentsActionConfirmationPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (selectedBatches :  BatchPayment[], action :  gw.web.payment.batch.BatchPaymentPopupActions) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.BatchPaymentsActionConfirmationPopup, {selectedBatches, action}, 0)
  }
  
  static function push (selectedBatches :  BatchPayment[], action :  gw.web.payment.batch.BatchPaymentPopupActions) : pcf.api.Location {
    return __newDestinationForLocation(pcf.BatchPaymentsActionConfirmationPopup, {selectedBatches, action}, 0).push()
  }
  
  
}