package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/allocationplan/AddInvoiceItemOrderingPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AddInvoiceItemOrderingPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (paymentAllocationPlan :  PaymentAllocationPlan) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AddInvoiceItemOrderingPopup, {paymentAllocationPlan}, 0)
  }
  
  static function push (paymentAllocationPlan :  PaymentAllocationPlan) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AddInvoiceItemOrderingPopup, {paymentAllocationPlan}, 0).push()
  }
  
  
}