package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentAdvanceConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewCommissionPaymentAdvanceConfirmationScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($advancePayment :  AdvanceCmsnPayment) : void {
    __widgetOf(this, pcf.NewCommissionPaymentAdvanceConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(false, {$advancePayment})
  }
  
  function refreshVariables ($advancePayment :  AdvanceCmsnPayment) : void {
    __widgetOf(this, pcf.NewCommissionPaymentAdvanceConfirmationScreen, SECTION_WIDGET_CLASS).setVariables(true, {$advancePayment})
  }
  
  
}