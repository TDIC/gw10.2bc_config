package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketRelatedEntitiesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class TroubleTicketRelatedEntitiesDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($troubleTicket :  TroubleTicket, $createTroubleTicketHelper :  CreateTroubleTicketHelper) : void {
    __widgetOf(this, pcf.TroubleTicketRelatedEntitiesDV, SECTION_WIDGET_CLASS).setVariables(false, {$troubleTicket, $createTroubleTicketHelper})
  }
  
  function refreshVariables ($troubleTicket :  TroubleTicket, $createTroubleTicketHelper :  CreateTroubleTicketHelper) : void {
    __widgetOf(this, pcf.TroubleTicketRelatedEntitiesDV, SECTION_WIDGET_CLASS).setVariables(true, {$troubleTicket, $createTroubleTicketHelper})
  }
  
  
}