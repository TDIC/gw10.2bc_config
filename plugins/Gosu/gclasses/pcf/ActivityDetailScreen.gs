package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/ActivityDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ActivityDetailScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($activity :  Activity, $assigneeHolder :  gw.api.assignment.Assignee[]) : void {
    __widgetOf(this, pcf.ActivityDetailScreen, SECTION_WIDGET_CLASS).setVariables(false, {$activity, $assigneeHolder})
  }
  
  function refreshVariables ($activity :  Activity, $assigneeHolder :  gw.api.assignment.Assignee[]) : void {
    __widgetOf(this, pcf.ActivityDetailScreen, SECTION_WIDGET_CLASS).setVariables(true, {$activity, $assigneeHolder})
  }
  
  
}