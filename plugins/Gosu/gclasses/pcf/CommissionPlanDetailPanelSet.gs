package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlanDetailPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class CommissionPlanDetailPanelSet extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($commissionPlan :  CommissionPlan) : void {
    __widgetOf(this, pcf.CommissionPlanDetailPanelSet, SECTION_WIDGET_CLASS).setVariables(false, {$commissionPlan})
  }
  
  function refreshVariables ($commissionPlan :  CommissionPlan) : void {
    __widgetOf(this, pcf.CommissionPlanDetailPanelSet, SECTION_WIDGET_CLASS).setVariables(true, {$commissionPlan})
  }
  
  
}