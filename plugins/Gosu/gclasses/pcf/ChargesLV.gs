package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/accounting/ChargesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ChargesLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($charges :  Charge[], $showCheckboxes :  Boolean, $field :  Number, $showPolicyColumn :  Boolean, $showAccount :  Boolean, $showHoldColumn :  Boolean) : void {
    __widgetOf(this, pcf.ChargesLV, SECTION_WIDGET_CLASS).setVariables(false, {$charges, $showCheckboxes, $field, $showPolicyColumn, $showAccount, $showHoldColumn})
  }
  
  function refreshVariables ($charges :  Charge[], $showCheckboxes :  Boolean, $field :  Number, $showPolicyColumn :  Boolean, $showAccount :  Boolean, $showHoldColumn :  Boolean) : void {
    __widgetOf(this, pcf.ChargesLV, SECTION_WIDGET_CLASS).setVariables(true, {$charges, $showCheckboxes, $field, $showPolicyColumn, $showAccount, $showHoldColumn})
  }
  
  
}