package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_AddStatementsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyDistributionWizard_AddStatementsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyDistributionWizard_AddStatementsPopup, {wizardState}, 0)
  }
  
  function pickValueAndCommit (value :  StatementInvoice[]) : void {
    __widgetOf(this, pcf.AgencyDistributionWizard_AddStatementsPopup, LOCATION_WIDGET_CLASS).setPickedValueAndCommitChanges(value)
  }
  
  static function push (wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyDistributionWizard_AddStatementsPopup, {wizardState}, 0).push()
  }
  
  
}