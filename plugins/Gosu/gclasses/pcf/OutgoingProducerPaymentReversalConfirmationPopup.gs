package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/OutgoingProducerPaymentReversalConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class OutgoingProducerPaymentReversalConfirmationPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (payment :  ProducerPaymentSent) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.OutgoingProducerPaymentReversalConfirmationPopup, {payment}, 0)
  }
  
  static function push (payment :  ProducerPaymentSent) : pcf.api.Location {
    return __newDestinationForLocation(pcf.OutgoingProducerPaymentReversalConfirmationPopup, {payment}, 0).push()
  }
  
  
}