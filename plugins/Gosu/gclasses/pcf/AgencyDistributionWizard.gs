package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyDistributionWizard extends com.guidewire.pl.web.codegen.WizardBase {
  static function createDestination (producer :  Producer, moneyToEdit :  BaseMoneyReceived) : pcf.api.Destination {
    return __newDestinationForWizard(pcf.AgencyDistributionWizard, {producer, moneyToEdit}, 1)
  }
  
  static function createDestination (producer :  Producer, distributionType :  gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum) : pcf.api.Destination {
    return __newDestinationForWizard(pcf.AgencyDistributionWizard, {producer, distributionType}, 0)
  }
  
  static function drilldown (producer :  Producer, moneyToEdit :  BaseMoneyReceived) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AgencyDistributionWizard, {producer, moneyToEdit}, 1).drilldown()
  }
  
  static function drilldown (producer :  Producer, distributionType :  gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AgencyDistributionWizard, {producer, distributionType}, 0).drilldown()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (producer :  Producer, moneyToEdit :  BaseMoneyReceived) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AgencyDistributionWizard, {producer, moneyToEdit}, 1).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function go (producer :  Producer, distributionType :  gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AgencyDistributionWizard, {producer, distributionType}, 0).go()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (producer :  Producer, moneyToEdit :  BaseMoneyReceived) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AgencyDistributionWizard, {producer, moneyToEdit}, 1).goInMain()
  }
  
  @com.guidewire.pl.system.expression.WebImmediate
  static function goInMain (producer :  Producer, distributionType :  gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AgencyDistributionWizard, {producer, distributionType}, 0).goInMain()
  }
  
  static function printPage (producer :  Producer, moneyToEdit :  BaseMoneyReceived) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AgencyDistributionWizard, {producer, moneyToEdit}, 1).printPage()
  }
  
  static function printPage (producer :  Producer, distributionType :  gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AgencyDistributionWizard, {producer, distributionType}, 0).printPage()
  }
  
  static function push (producer :  Producer, moneyToEdit :  BaseMoneyReceived) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AgencyDistributionWizard, {producer, moneyToEdit}, 1).push()
  }
  
  static function push (producer :  Producer, distributionType :  gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum) : pcf.api.Location {
    return __newDestinationForWizard(pcf.AgencyDistributionWizard, {producer, distributionType}, 0).push()
  }
  
  
}