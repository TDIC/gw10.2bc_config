package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadAuditLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class DataUploadAuditLV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($processor :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) : void {
    __widgetOf(this, pcf.DataUploadAuditLV, SECTION_WIDGET_CLASS).setVariables(false, {$processor})
  }
  
  function refreshVariables ($processor :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) : void {
    __widgetOf(this, pcf.DataUploadAuditLV, SECTION_WIDGET_CLASS).setVariables(true, {$processor})
  }
  
  
}