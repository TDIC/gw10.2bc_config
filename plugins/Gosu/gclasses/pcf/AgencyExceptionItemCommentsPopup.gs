package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyExceptionItemCommentsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyExceptionItemCommentsPopup extends com.guidewire.pl.web.codegen.LocationBase {
  static function createDestination (exceptionItemViews :  gw.api.web.invoice.ExceptionItemView[]) : pcf.api.Destination {
    return __newDestinationForLocation(pcf.AgencyExceptionItemCommentsPopup, {exceptionItemViews}, 0)
  }
  
  static function push (exceptionItemViews :  gw.api.web.invoice.ExceptionItemView[]) : pcf.api.Location {
    return __newDestinationForLocation(pcf.AgencyExceptionItemCommentsPopup, {exceptionItemViews}, 0).push()
  }
  
  
}