package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistItemsLV.item.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class AgencyDistItemsLV_item extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($agencyCycleDistView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistView, $wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : void {
    __widgetOf(this, pcf.AgencyDistItemsLV_item, SECTION_WIDGET_CLASS).setVariables(false, {$agencyCycleDistView, $wizardState})
  }
  
  function refreshVariables ($agencyCycleDistView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistView, $wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : void {
    __widgetOf(this, pcf.AgencyDistItemsLV_item, SECTION_WIDGET_CLASS).setVariables(true, {$agencyCycleDistView, $wizardState})
  }
  
  
}