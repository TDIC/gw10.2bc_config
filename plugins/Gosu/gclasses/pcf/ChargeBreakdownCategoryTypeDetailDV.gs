package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/chargepatterns/ChargeBreakdownCategoryTypeDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class ChargeBreakdownCategoryTypeDetailDV extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($categoryType :  ChargeBreakdownCategoryType) : void {
    __widgetOf(this, pcf.ChargeBreakdownCategoryTypeDetailDV, SECTION_WIDGET_CLASS).setVariables(false, {$categoryType})
  }
  
  function refreshVariables ($categoryType :  ChargeBreakdownCategoryType) : void {
    __widgetOf(this, pcf.ChargeBreakdownCategoryTypeDetailDV, SECTION_WIDGET_CLASS).setVariables(true, {$categoryType})
  }
  
  
}