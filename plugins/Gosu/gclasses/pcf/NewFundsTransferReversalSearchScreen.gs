package pcf

uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewFundsTransferReversalSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
public class NewFundsTransferReversalSearchScreen extends com.guidewire.pl.web.codegen.SectionBase {
  function onEnter ($transferFundReversalContext :  gw.transaction.TransferFundsReversalWizardContext) : void {
    __widgetOf(this, pcf.NewFundsTransferReversalSearchScreen, SECTION_WIDGET_CLASS).setVariables(false, {$transferFundReversalContext})
  }
  
  function refreshVariables ($transferFundReversalContext :  gw.transaction.TransferFundsReversalWizardContext) : void {
    __widgetOf(this, pcf.NewFundsTransferReversalSearchScreen, SECTION_WIDGET_CLASS).setVariables(true, {$transferFundReversalContext})
  }
  
  
}