package tdic.util.gunit.test

uses gw.api.system.server.Runlevel
uses gw.testharness.RunLevel
uses gw.testharness.ServerTest
uses gw.testharness.TestBase
uses tdic.util.gunit.GUnitAssert
uses tdic.util.gunit.test.stubs.AbstractBaseClassTest
uses tdic.util.gunit.test.stubs.SubclassTest
uses tdic.util.gunit.TextTestRunner
uses java.lang.Throwable
uses java.util.Arrays

@ServerTest
@RunLevel(Runlevel.NONE)
class AbstractBaseAndSubclassTest extends TestBase {

  override function beforeMethod() {
    AbstractBaseClassTest.clearStaticState()
  }

  override function afterMethod(possibleException : Throwable) {
    AbstractBaseClassTest.clearStaticState()
  }

  function testSequenceOfSuperAndSubclassMethodCalls() {
    var runner = new TextTestRunner()

    runner.runTestsInClass(SubclassTest)

    var expectedMethodCalls = Arrays.asList({
        "base: beforeClass",
        //
    //    "base: beforeMethod",
    //    "base: testInBaseClass",
    //    "base: afterMethod",
        //
        "base: beforeMethod",
        "testInSubclass",
        "base: afterMethod",
        //
        "base: afterClass"
      })
    GUnitAssert.assertCollectionEquals(expectedMethodCalls, SubclassTest._methodNamesCalled)
  }

}
