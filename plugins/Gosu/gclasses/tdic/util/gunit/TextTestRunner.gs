package tdic.util.gunit

uses gw.api.system.server.Runlevel
uses gw.lang.reflect.IMethodInfo
uses gw.lang.reflect.IType
uses gw.lang.reflect.ReflectUtil
uses gw.lang.reflect.TypeSystem
uses gw.pl.util.FileUtil
uses gw.testharness.RunLevel
uses gw.testharness.TestBase
uses org.slf4j.Logger

uses java.io.File
uses java.lang.Throwable
uses java.util.List

class TextTestRunner implements ITestActions {

  var _runlevelAccessor : IRunlevelAccessor
  var _testReporter     : ITestReporter     as TestReporter
  var _testActions      : ITestActions      as TestActions
  public static var LOGGER_CATEGORY : String = "ABC.GUnit"
  var _logger : Logger



  construct() {
    this(new RunlevelWrapper())
  }

  construct(runlevelAccessor : IRunlevelAccessor) {
    this(runlevelAccessor, new TextTestReporter(runlevelAccessor))
  }

  construct(runlevelAccessor : IRunlevelAccessor, testReporterIn : ITestReporter) {
    _testActions      = this
    _runlevelAccessor = runlevelAccessor
    _testReporter     = testReporterIn
  }


  function initalize() {
    _testReporter.begin()
    _testActions.begin()
  }

  override function begin() {
  }

  function finish() {
    _testActions.end()
    _testReporter.end()
  }

  override function end() {
  }

  function runAllTests() {
    runTestsInPackage("")
  }

  function runTestsInPackage(packageName : String) {
    var currentRunlevel = _runlevelAccessor.getCurrentServerRunlevel()
    var gtestRootDirectory = TestConstants.TestRoot
    var directoryToSearch = new File(gtestRootDirectory, packageName.replace('.', File.separatorChar))
    for (file in FileUtil.getFilesInDirectory(directoryToSearch, "", true)) {
      if (not file.Directory and file.Name.endsWith("Test.gs") and !file.Name.equalsIgnoreCase("MainTest.gs")) {
        var fullyQualifiedClassName = TestFileUtil.extractFullyQualifiedClassName(gtestRootDirectory, file)
        var iType = TypeSystem.getByFullName(fullyQualifiedClassName)
        var requiredRunlevel = getRequiredRunlevel(iType)

        if (not iType.Abstract and iType.AllTypesInHierarchy.contains(TestBase)) {
          if (currentRunlevel.ordinal() >= requiredRunlevel.ordinal()) {
            _testActions.runTestsInClass(iType)
          } else {
            _testReporter.skipClassDueToRunlevel(iType)
          }
        }
      }
    }
  }

  private static function getRequiredRunlevel(iType : IType) : Runlevel {
      var runLevelAnnotation = iType.TypeInfo.getAnnotation(RunLevel)
 
     var res = gw.api.system.server.Runlevel.MULTIUSER
     if (runLevelAnnotation != null && runLevelAnnotation.Instance != null ) {
       var inst  = runLevelAnnotation.Instance as RunLevel
       if (inst.value() != null)  res = inst.value()
     }
    return res
  }

  override function runTestsInClass(iType : IType) {
    var testMethods = iType.TypeInfo.Methods.where(\ method -> (
            method.DisplayName.startsWith("test")
        and method.Parameters.length == 0
        and method.ReturnType == void
      ))
    if (testMethods.size() == 0) {
      _testReporter.classHasNoTestMethods(iType)
    } else {
      _testActions.runTestsInClass(iType, testMethods)
    }
  }

  override function runTestsInClass(iType : IType, testMethods : List<IMethodInfo>) {
    testMethods.each(\ method -> {
        var isFirstMethodInClass = (method == testMethods.first())
        var isLastMethodInClass = (method == testMethods.last())
        _testActions.runTestMethod(iType, method, isFirstMethodInClass, isLastMethodInClass)
      })
  }

  public function runTestMethod(method : IMethodInfo) {
    var iType = method.OwnersType
    runTestMethod(iType, method, true, true)
  }

  override function runTestMethod(iType : IType, method : IMethodInfo, isFirstMethodInClass : boolean, isLastMethodInClass : boolean) {
    try {
      var defaultConstructor = method.OwnersType.TypeInfo.getCallableConstructor({})
      var testInstance = defaultConstructor.Constructor.newInstance(null) as TestBase

      if (isFirstMethodInClass) {
        try {
          ReflectUtil.invokeMethod(testInstance, "beforeClass", {})
        } catch (ex : Throwable) {
          print("Caught exception from 'beforeClass' call.")
          print("  Owner:  " + method.OwnersType)
          print("  Method: " + method)
          print("\n")
          print("Exception:\n" + ex + "\n\n\n")
          throw ex
        }
      }

      ReflectUtil.invokeMethod(testInstance, "beforeMethod", {})

      var testException : Throwable = null
      var afterMethodWasCalled = false
      try {
        try {
          _testActions.invokeTestMethod(method, testInstance)

          afterMethodWasCalled = true
          ReflectUtil.invokeMethod(testInstance, "afterMethod", {null})
        } catch (ex : Throwable) {
          testException = ex
          if (not afterMethodWasCalled) {
            ReflectUtil.invokeMethod(testInstance, "afterMethod", {ex})
          }
        }
      } finally {
        if (isLastMethodInClass) {
          ReflectUtil.invokeMethod(testInstance, "afterClass", {})
        }
      }

      if (testException != null) {
        throw testException
      }

      _testReporter.testMethodPassed(method)
    } catch (ex : Throwable) {
      _testReporter.testMethodFailed(method, ex)
    }
  }

  override function invokeTestMethod(method : IMethodInfo, testInstance : TestBase) {
    ReflectUtil.invokeMethod(testInstance, method.DisplayName, {})
  }

}
