package tdic.util.build

uses java.io.File
uses java.io.FileInputStream

uses gw.api.locale.DisplayKey
uses gw.api.system.server.ServerUtil
uses gw.api.system.PLConfigParameters
uses com.guidewire.modules.file.VirtualDirectory
uses com.guidewire.modules.path.ResourceLocatorFinder

/**
 * BrittoS - Display Environment, Build number and date
 */
class BuildInfo {
  private static var _buildNumber : String = ""
  private static var _buildDate : String = ""

  public static function getBuildInfo() : String {
    populateJenkinsProperties()
    return DisplayKey.get("TDIC.Web.TabBar.BuildInfo", ServerUtil.getEnv().toUpperCase(), _buildNumber, _buildDate)
  }

  private static function populateJenkinsProperties() {
    try {
      var root = ResourceLocatorFinder.get().getServletContextRoot()
      var resources = new File(root, PLConfigParameters.WebResourcesDir.Value.concat("/pages"))
      var virtualDirectory = new VirtualDirectory(new File[] { resources })
      var cp = virtualDirectory.getFile("jenkins.properties")
      var jenkinsProp = new Properties()
      jenkinsProp.load(new FileInputStream(cp.Path))
      _buildNumber = jenkinsProp.getProperty("build.number")
      _buildDate = jenkinsProp.getProperty("build.date")
    } catch(e : Exception) {
      //Suppress exception
    }
  }
}