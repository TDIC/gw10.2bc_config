package tdic.util.dataloader.parser

uses tdic.util.dataloader.processor.DataLoaderProcessor
uses org.apache.poi.ss.usermodel.*
uses java.util.ArrayList
uses java.io.File
uses java.io.BufferedInputStream
uses java.io.FileOutputStream
uses java.lang.Exception
uses gw.api.util.DisplayableException
uses org.slf4j.LoggerFactory

class ExcelFileParser implements ExcelFileParserInterface {
  public static final var LOG: org.slf4j.Logger = LoggerFactory.getLogger("DataLoader.Parser")

  construct() {
  }

  override function readFile(webFile: gw.api.web.WebFile): File {
    var tmpFile = File.createTempFile("sampledata", ".xls")
    try {
      var bos = new FileOutputStream(tmpFile)
      var bis = new BufferedInputStream(webFile.InputStream)
      var ba = new byte[2048]
      var count = bis.read(ba)
      while (count > 0)
      {
        bos.write(ba, 0, count)
        count = bis.read(ba)
      }
      bos.close()
      bis.close()
      return tmpFile
    } catch (e: Exception) {
      var msg = "ExcelFileParser: Error occurred while reading the file: "
      LOG.error(msg, e)
      throw new DisplayableException(msg + e.toString())
    }
  }

  override function importFile(filePath: String, dataLoaderHelper: DataLoaderProcessor): DataLoaderProcessor {
    return dataLoaderHelper
  }

  public static function getSheet(wb: Workbook, sheetName: String): Sheet {
    var result = wb.getSheet(sheetName)
    if (result == null) {
      var msg = sheetName + " sheet: Sheet not found"
      LOG.warn(msg)
    }
    return result
  }

  protected static reified function replaceDataWithParsedArray<T> (store: ArrayList <T>, parsed: ArrayList <T>) {
    if (store.HasElements) store.removeAll(store)
    store.addAll(parsed)
  }
}
