package tdic.util.dataloader.parser.planparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses java.util.ArrayList
uses tdic.util.dataloader.data.plandata.CommissionPlanData
uses tdic.util.dataloader.data.plandata.CommissionSubPlanData
uses java.lang.Exception
uses gw.api.util.DisplayableException

class CommissionDefaultSubPlanWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }
  var firstRowDone = false
  var currRowNum: int
  var commissionSubPlanArray = new ArrayList <CommissionSubPlanData>()
  override function parseSheet(sheet: Sheet): ArrayList <CommissionSubPlanData> {
    // Check that sheet exists         
    if (sheet == null)
    {
      LOG.warn("No " + sheet.SheetName + " sheet")
      return null
    }
    LOG.info(sheet.SheetName + " sheet: Parsing of default subplan started ***")
    try
    {
      // Iterate through each row in the Commission SubPlan sheet 
      for (var row in sheet.rowIterator())
      {
        currRowNum = row.RowNum
        // skip the first row (it has headers)
        if (!firstRowDone)
        {
          firstRowDone = true
          continue
        }
        // Define and instantiate variables
        var commissionSubPlanData = new CommissionSubPlanData()
        // Populate Commission SubPlan Data
        commissionSubPlanData.PlanName = convertCellToString(row.getCell(CommissionPlanData.COL_NAME))
        commissionSubPlanData.SubPlanName = "default"
        if (commissionSubPlanData.PlanName != null) {
          // Get the rest of the SubPlan Data from common function
          commissionSubPlanData = CommissionSubPlanParserUtility.populateSubPlanData(commissionSubPlanData, row, CommissionPlanData.COL_PRIMARY_RATE)
          // Save to array
          commissionSubPlanArray.add(commissionSubPlanData)
        }
      }
      LOG.info(sheet.SheetName + " sheet: Parsing of default subplan completed ***")
      return commissionSubPlanArray
    } catch (e: Exception) {
      var msg = sheet.SheetName + " sheet: Exception while parsing default subplan of row " + currRowNum + " : "
      LOG.warn(msg, e)
      throw new DisplayableException(msg + e)
    }
  }
}