package tdic.util.dataloader.parser.planparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses java.lang.Integer
uses java.math.BigDecimal
uses tdic.util.dataloader.data.plandata.DelinquencyPlanData
uses gw.api.upgrade.Coercions

class DelinquencyPlanWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parseDelinquencyPlanRow(row: Row): DelinquencyPlanData {
    var result = new DelinquencyPlanData()
    // Populate Delinquency Plan Data
    result.PublicID = convertCellToString(row.getCell(DelinquencyPlanData.COL_PUBLIC_ID))
    result.Name = convertCellToString(row.getCell(DelinquencyPlanData.COL_NAME))
    result.EffectiveDate = convertCellToDate(row.getCell(DelinquencyPlanData.COL_EFFECTIVE_DATE))
    result.ExpirationDate = convertCellToDate(row.getCell(DelinquencyPlanData.COL_EXPIRATION_DATE))
    result.CancellationTarget = typekey.CancellationTarget.get(convertCellToString(row.getCell(DelinquencyPlanData.COL_CANCELLATION_TARGET)))
    result.HoldInvoicingOnTargetedPolicies = Coercions.makeBooleanFrom(convertCellToString(row.getCell(DelinquencyPlanData.COL_HOLD_INVOICING_ON_TARGETED_POLICIES)))
    result.GracePeriodDays = Coercions.makeIntFrom(convertCellToString(row.getCell(DelinquencyPlanData.COL_GRACE_PERIOD_DAYS)))
    result.LateFee = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(DelinquencyPlanData.COL_LATE_FEE)))
    result.ReinstatementFee = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(DelinquencyPlanData.COL_REINSTATEMENT_FEE)))
    result.WriteoffThreshold = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(DelinquencyPlanData.COL_WRITE_OFF_THRESHOLD)))
    result.EnterDelinquencyThreshold_Account = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(DelinquencyPlanData.COL_ENTER_DELINQUENCY_THRESHOLD_ACCOUNT)))
    result.EnterDelinquencyThreshold_Policy = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(DelinquencyPlanData.COL_ENTER_DELINQUENCY_THRESHOLD_POLICY)))
    result.CancelPolicy = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(DelinquencyPlanData.COL_CANCEL_POLICY)))
    result.ExitDelinquency = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(DelinquencyPlanData.COL_EXIT_DELINQUENCY)))
    result.ApplicableSegments = typekey.ApplicableSegments.get(convertCellToString(row.getCell(DelinquencyPlanData.COL_APPLICABLE_SEGMENTS)))
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList <DelinquencyPlanData> {
    return parseSheet(sheet, \r -> parseDelinquencyPlanRow(r), true)
  }
}