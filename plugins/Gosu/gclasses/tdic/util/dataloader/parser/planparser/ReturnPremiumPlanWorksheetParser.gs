package tdic.util.dataloader.parser.planparser

uses tdic.util.dataloader.parser.WorksheetParserInterface
uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.data.plandata.ReturnPremiumPlanData
uses java.util.ArrayList
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row

class ReturnPremiumPlanWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parseReturnPremiumPlanRow(row: Row): ReturnPremiumPlanData {
    var result = new ReturnPremiumPlanData ()
    // Populate Return Premium Plan Data
    result.PublicID = convertCellToString(row.getCell(ReturnPremiumPlanData.COL_PUBLIC_ID))
    result.Name = convertCellToString(row.getCell(ReturnPremiumPlanData.COL_NAME))
    result.Description = convertCellToString(row.getCell(ReturnPremiumPlanData.COL_DESCRIPTION))
    result.EffectiveDate = convertCellToDate(row.getCell(ReturnPremiumPlanData.COL_EFFECTIVE_DATE))
    result.ExpirationDate = convertCellToDate(row.getCell(ReturnPremiumPlanData.COL_EXPIRATION_DATE))
    result.Qualifier = typekey.ReturnPremiumChargeQualification.get(convertCellToString(row.getCell(ReturnPremiumPlanData.COL_QUALIFIER)))
    result.ListBillExcess = typekey.ListBillAccountExcess.get(convertCellToString(row.getCell(ReturnPremiumPlanData.COL_LISTBILL_EXCESS)))
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList < ReturnPremiumPlanData > {
    return parseSheet(sheet, \r -> parseReturnPremiumPlanRow(r), true)
  }
}