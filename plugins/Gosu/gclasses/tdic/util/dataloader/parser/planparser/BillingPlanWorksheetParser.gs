package tdic.util.dataloader.parser.planparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses java.lang.Integer
uses java.math.BigDecimal
uses tdic.util.dataloader.data.plandata.BillingPlanData
uses gw.api.upgrade.Coercions

class BillingPlanWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parseBillingPlanRow(row: Row): BillingPlanData {
    var result = new BillingPlanData()
    // Populate Billing Plan Data
    result.PublicID = convertCellToString(row.getCell(BillingPlanData.COL_PUBLIC_ID))
    result.Name = convertCellToString(row.getCell(BillingPlanData.COL_NAME))
    result.Description = convertCellToString(row.getCell(BillingPlanData.COL_DESCRIPTION))
    result.LeadTime = Coercions.makeIntFrom(convertCellToString(row.getCell(BillingPlanData.COL_LEADTIME)))
    result.NonResponsiveLeadTime = Coercions.makeIntFrom(convertCellToString(row.getCell(BillingPlanData.COL_NONRESPONSIVE_LEADTIME)))
    result.FixPaymentDueDateOn = typekey.DayOfMonthLogic.get(convertCellToString(row.getCell(BillingPlanData.COL_FIX_PAYMENT_DUEDATE_ON)))
    result.EffectiveDate = convertCellToDate(row.getCell(BillingPlanData.COL_EFFECTIVE_DATE))
    result.ExpirationDate = convertCellToDate(row.getCell(BillingPlanData.COL_EXPIRATION_DATE))
    result.InvoiceFee = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(BillingPlanData.COL_INVOICE_FEE)))
    result.PaymentReversalFee = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(BillingPlanData.COL_PAYMENT_REVERSAL_FEE)))
    result.SkipInstallmentFees = Coercions.makeBooleanFrom(convertCellToString(row.getCell(BillingPlanData.COL_SKIP_INSTALLMENT_FEES)))
    result.LineItemsShow = typekey.AggregationType.get(convertCellToString(row.getCell(BillingPlanData.COL_LINE_ITEMS_SHOW)))
    result.SuppressLowBalInvoices = Coercions.makeBooleanFrom(convertCellToString(row.getCell(BillingPlanData.COL_SUPPRESS_LOWBAL_INVOICES)))
    result.LowBalanceThreshold = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(BillingPlanData.COL_LOW_BALANCE_THRESHOLD)))
    result.LowBalanceMethod = typekey.LowBalanceMethod.get(convertCellToString(row.getCell(BillingPlanData.COL_LOW_BALANCE_METHOD)))
    result.ReviewDisbursementsOver = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(BillingPlanData.COL_REVIEW_DISBURSEMENTS_OVER)))
    result.DelayDisbursement = Coercions.makeIntFrom(convertCellToString(row.getCell(BillingPlanData.COL_DELAY_DISBURSEMENT)))
    result.AutomaticDisbursementOver = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(BillingPlanData.COL_AUTOMATIC_DISBURSEMENT_OVER)))
    result.AvailableDisbAmtType = typekey.AvailableDisbAmtType.get(convertCellToString(row.getCell(BillingPlanData.COL_AVAILABLE_DISB_AMT_TYPE)))
    result.CreateApprActForAutoDisb = Coercions.makeBooleanFrom(convertCellToString(row.getCell(BillingPlanData.COL_CREATE_APPR_ACT_FOR_AUTO_DISB)))
    result.SendAutoDisbAwaitingApproval = Coercions.makeBooleanFrom(convertCellToString(row.getCell(BillingPlanData.COL_SEND_AUTO_DISB_AWAITING_APPROVAL)))
    result.RequestInterval = Coercions.makeIntFrom(convertCellToString(row.getCell(BillingPlanData.COL_REQUEST_INTERVAL)))
    result.ChangeDeadlineInterval = Coercions.makeIntFrom(convertCellToString(row.getCell(BillingPlanData.COL_CHANGE_DEADLINE_INTERVAL)))
    result.DraftInterval = Coercions.makeIntFrom(convertCellToString(row.getCell(BillingPlanData.COL_DRAFT_INTERVAL)))
    result.DraftDayLogic = typekey.DayOfMonthLogic.get(convertCellToString(row.getCell(BillingPlanData.COL_DRAFT_DAY_LOGIC)))

    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList <BillingPlanData> {
    return parseSheet(sheet, \r -> parseBillingPlanRow(r), true)
  }
}
