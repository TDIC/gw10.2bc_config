package tdic.util.dataloader.parser.planparser

uses java.util.ArrayList
uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Row
uses tdic.util.dataloader.data.plandata.GLTAccountNameMappingData
uses org.apache.poi.ss.usermodel.Cell
uses java.lang.IllegalArgumentException
uses org.apache.poi.ss.usermodel.Sheet
uses gw.api.upgrade.Coercions

/**
 * US570 - General Ledger Integration
 * 11/17/2014 Alvin Lee
 *
 * Excel Data Loader Additions for custom GL Integration entities.
 * Does the parsing of the GL T-Account Name Mapping data from the rows in the Excel spreadsheet.
 */
class GLTAccountNameMappingWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface  {

  /**
   * Standard constructor
   */
  construct() {
  }

  /**
   * Parses the Excel spreadsheet row into GL Mapping data
   * 
   * @param row The row in the Excel spreadsheet
   * @return A GLTAccountNameMappingData object containing the parsed data
   */
  function parseGLTAccountNameMappingRow(row:Row):GLTAccountNameMappingData {
    var result = new GLTAccountNameMappingData()

    // Populate GL Mapping Data
    var transactionType : Cell
    try {
      transactionType = row.getCell(GLTAccountNameMappingData.COL_TRANS_TYPE)
      result.TransactionType = typekey.Transaction.get(convertCellToString(transactionType))
    } catch (iae : IllegalArgumentException) {
      LOG.warn("Invalid Transaction Type value '" + transactionType + "' in the GLTAccountMapping sheet", iae)
      throw new IllegalArgumentException("Invalid Transaction Type value '" + transactionType + "' in the GLTAccountMapping sheet", iae)
    }
    var lineItemType : Cell
    try {
      lineItemType = row.getCell(GLTAccountNameMappingData.COL_LINE_ITEM_TYPE)
      result.LineItemType = typekey.LedgerSide.get(convertCellToString(lineItemType))
    } catch (iae : IllegalArgumentException) {
      LOG.warn("Invalid T-Account Type value '" + lineItemType + "' in the GLTAccountMapping sheet", iae)
      throw new IllegalArgumentException("Invalid T-Account Type value '" + lineItemType + "' in the GLTAccountMapping sheet", iae)
    }
    var futureTerm : Cell
    try {
      futureTerm = row.getCell(GLTAccountNameMappingData.COL_FUTURE_TERM)
      result.FutureTerm = Coercions.makePBooleanFrom(convertCellToString(futureTerm))
    } catch (iae : IllegalArgumentException) {
      LOG.warn("Invalid Future Term value '" + futureTerm + "' in the GLTAccountMapping sheet", iae)
      throw new IllegalArgumentException("Invalid Future Term value '" + futureTerm + "' in the GLTAccountMapping sheet", iae)
    }
    var invoiceStatus : Cell
    try {
      invoiceStatus = row.getCell(GLTAccountNameMappingData.COL_INVOICE_STATUS)
      result.InvoiceStatus = typekey.InvoiceStatus.get(convertCellToString(invoiceStatus))
    } catch (iae : IllegalArgumentException) {
      LOG.warn("Invalid Invoice Status value '" + futureTerm + "' in the GLTAccountMapping sheet", iae)
      throw new IllegalArgumentException("Invalid Invoice Status value '" + futureTerm + "' in the GLTAccountMapping sheet", iae)
    }
    var originalTAccountName : Cell
    try {
      originalTAccountName = row.getCell(GLTAccountNameMappingData.COL_ORIGINAL_TACCOUNT_NAME)
      result.OriginalTAccountName = typekey.GLOriginalTAccount_TDIC.get(convertCellToString(originalTAccountName))
    } catch (iae : IllegalArgumentException) {
      LOG.warn("Invalid GW T-Account Name value '" + originalTAccountName + "' in the GLTAccountMapping sheet", iae)
      throw new IllegalArgumentException("Invalid GW T-Account Name value '" + originalTAccountName + "' in the GLTAccountMapping sheet", iae)
    }
    var mappedTAccountName : Cell
    try {
      mappedTAccountName = row.getCell(GLTAccountNameMappingData.COL_MAPPED_TACCOUNT_NAME)
      result.MappedTAccountName = convertCellToString(mappedTAccountName)
    } catch (iae : IllegalArgumentException) {
      LOG.warn("Invalid T-Account Name value '" + mappedTAccountName + "' in the GLTAccountMapping sheet", iae)
      throw new IllegalArgumentException("Invalid T-Account Name value '" + mappedTAccountName + "' in the GLTAccountMapping sheet", iae)
    }
    return result
  }

  /**
   * Parses the Excel sheet for the GL T-Account Name Mapping Data.
   * 
   * @param sheet The sheet in the Excel spreadsheet containing the GL T-Account Name Mapping Data
   * @return An ArrayList<GLTAccountNameMappingData> containing all the parsed data
   */
  override function parseSheet(sheet : Sheet): ArrayList<GLTAccountNameMappingData> {
   return parseSheet(sheet, \ r -> parseGLTAccountNameMappingRow(r), false)
  }

}
