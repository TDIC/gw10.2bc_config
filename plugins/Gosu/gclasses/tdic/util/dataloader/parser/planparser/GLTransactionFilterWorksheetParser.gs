package tdic.util.dataloader.parser.planparser

uses java.util.ArrayList
uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Row
uses tdic.util.dataloader.data.plandata.GLTransactionFilterData
uses org.apache.poi.ss.usermodel.Cell
uses java.lang.IllegalArgumentException
uses org.apache.poi.ss.usermodel.Sheet
uses tdic.util.dataloader.data.plandata.GLTransactionFilterData

/**
 * US570 - General Ledger Integration
 * 11/17/2014 Alvin Lee
 *
 * Excel Data Loader Additions for custom GL Integration entities.
 * Does the parsing of the GL Filter data from the rows in the Excel spreadsheet.
 */
class GLTransactionFilterWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface  {

  /**
   * Standard constructor
   */
  construct() {
  }

  /**
   * Parses the Excel spreadsheet row into GL Filter data
   * 
   * @param row The row in the Excel spreadsheet
   * @return A GLTransactionFilterData object containing the parsed data
   */
  function parseGLFilterRow(row:Row): GLTransactionFilterData {
    var result = new GLTransactionFilterData ()

    // Populate GL Filter Data
    var transType : Cell
    try {
      transType = row.getCell(GLTransactionFilterData.COL_TRANS_TYPE)
      result.TransactionType=typekey.Transaction.get(convertCellToString(transType))
    } catch (iae : IllegalArgumentException) {
      LOG.warn("Invalid Transaction Type value '" + transType + "' in the GLIntegrationTransactionFilter sheet", iae)
      throw new IllegalArgumentException("Invalid Transaction Type value '" + transType + "' in the GLIntegrationTransactionFilter sheet", iae)
    }
    return result
  }

  /**
   * Parses the Excel sheet for the GL Filter Data.
   * 
   * @param sheet The sheet in the Excel spreadsheet containing the GL Filter Data
   * @return An ArrayList<GLIntegrationTransactionFilterData> containing all the parsed data
   */
  override function parseSheet(sheet : Sheet): ArrayList< GLTransactionFilterData > {
   return parseSheet(sheet, \ r -> parseGLFilterRow(r), false)
  }

}
