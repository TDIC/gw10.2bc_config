package tdic.util.dataloader.parser.planparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses tdic.util.dataloader.data.plandata.ChargePatternData
uses gw.api.upgrade.Coercions

class ChargePatternWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parseChargePatternRow(row: Row): ChargePatternData {
    var result = new ChargePatternData()
    // Populate Charge Pattern Data
    result.PublicID = convertCellToString(row.getCell(ChargePatternData.COL_PUBLIC_ID))
    result.ChargeCode = convertCellToString(row.getCell(ChargePatternData.COL_CHARGE_CODE))
    result.ChargeName = convertCellToString(row.getCell(ChargePatternData.COL_CHARGE_NAME))
    result.ChargeType = convertCellToString(row.getCell(ChargePatternData.COL_CHARGE_TYPE))
    result.Category = typekey.ChargeCategory.get(convertCellToString(row.getCell(ChargePatternData.COL_CATEGORY)))
    result.TAccount_Owner = convertCellToString(row.getCell(ChargePatternData.COL_TACCOUNT_OWNER))
    result.InvoicingApproach = typekey.InvoiceTreatment.get(convertCellToString(row.getCell(ChargePatternData.COL_INVOICING_APPROACH)))
    result.Priority = typekey.ChargePriority.get(convertCellToString(row.getCell(ChargePatternData.COL_PRIORITY)))
    result.Include_In_Equity_Dating = Coercions.makeBooleanFrom(convertCellToString(row.getCell(ChargePatternData.COL_INCLUDE_IN_EQUITY_DATING)))
    result.Periodicity = typekey.Periodicity.get(convertCellToString(row.getCell(ChargePatternData.COL_PERIODICITY)))
    result.Exclude = Coercions.makeBooleanFrom(convertCellToString(row.getCell(ChargePatternData.COL_EXCLUDE)))
    result.PrintPriority = typekey.ChargePrintPriority_TDIC.get(convertCellToString(row.getCell(ChargePatternData.COL_PRINTPRIORITY)))
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList <ChargePatternData> {
    return parseSheet(sheet, \r -> parseChargePatternRow(r), true)
  }
}
