package tdic.util.dataloader.parser.planparser

uses java.util.ArrayList
uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Row
uses tdic.util.dataloader.data.plandata.GLTransactionNameMappingData
uses org.apache.poi.ss.usermodel.Cell
uses java.lang.IllegalArgumentException
uses org.apache.poi.ss.usermodel.Sheet

/**
 * US570 - General Ledger Integration
 * 11/17/2014 Alvin Lee
 *
 * Excel Data Loader Additions for custom GL Integration entities.
 * Does the parsing of the GL Mapping data from the rows in the Excel spreadsheet.
 */
class GLTransactionNameMappingWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface  {

  /**
   * Standard constructor
   */
  construct() {
  }

  /**
   * Parses the Excel spreadhseet row into GL Mapping data
   * 
   * @param row The row in the Excel spreadsheet
   * @return A GLIntegrationTransactionMappingData object containing the parsed data
   */
  function parseGLMappingRow(row:Row): GLTransactionNameMappingData {
    var result = new GLTransactionNameMappingData ()

    // Populate GL Mapping Data
    var originalTransactionName : Cell
    try {
      originalTransactionName = row.getCell(GLTransactionNameMappingData.COL_ORIGINAL_TRANSACTION_NAME)
      result.OriginalTransactionName = typekey.Transaction.get(convertCellToString(originalTransactionName))
    } catch (iae : IllegalArgumentException) {
      LOG.warn("Invalid Original Transaction Name value '" + originalTransactionName + "' in the GLTransactionNameMapping sheet", iae)
      throw new IllegalArgumentException("Invalid Transaction Type value '" + originalTransactionName + "' in the GLTransactionNameMapping sheet", iae)
    }
    var mappedTransactionName : Cell
    try {
      mappedTransactionName = row.getCell(GLTransactionNameMappingData.COL_MAPPED_TRANSACTION_NAME)
      result.MappedTransactionName = convertCellToString(mappedTransactionName)
    } catch (iae : IllegalArgumentException) {
      LOG.warn("Invalid Mapped Transaction Name value '" + mappedTransactionName + "' in the GLTransactionNameMapping sheet", iae)
      throw new IllegalArgumentException("Invalid Mapped Name value '" + mappedTransactionName + "' in the GLTransactionNameMapping sheet", iae)
    }
    return result
  }

  /**
   * Parses the Excel sheet for the GL Mapping Data.
   * 
   * @param sheet The sheet in the Excel spreadsheet containing the GL Mapping Data
   * @return An ArrayList<GLIntegrationTransactionMappingData> containing all the parsed data
   */
  override function parseSheet(sheet : Sheet): ArrayList< GLTransactionNameMappingData > {
   return parseSheet(sheet, \ r -> parseGLMappingRow(r), false)
  }

}
