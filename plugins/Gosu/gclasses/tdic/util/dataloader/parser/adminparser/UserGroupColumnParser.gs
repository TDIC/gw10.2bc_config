package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses java.lang.Integer
uses java.util.ArrayList
uses tdic.util.dataloader.data.admindata.UserGroupColumnData
uses java.lang.Exception

class UserGroupColumnParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }
  var firstRowDone = false
  var currRowNum: int
  var userGroupColumnArray = new ArrayList <UserGroupColumnData>()
  override function parseSheet(sheet: Sheet): ArrayList <UserGroupColumnData> {
    // Check that sheet exists         
    if (sheet == null)
    {
      LOG.warn("No User Group Sheet")
      return null
    }
    LOG.info("User Group Sheet: Parsing of group columns started ***")
    try
    {
      // Iterate through each row in the User Group sheet 
      for (var row in sheet)
      {
        currRowNum = row.RowNum
        // Get the Groups from the first row (it has headers)
        if (!firstRowDone)
        {
          // each cell beginning with column 2 (B) contains a group name
          var x = 0 as Integer
          var cells = row.cellIterator()
          for (myCell in cells) {
            x = x + 1
            if (x >= 3) {
              var column = myCell.toString()
              if (column != null and column != "") {
                // add to group array
                var userGroupColumnData = new UserGroupColumnData()
                userGroupColumnData.Column = x
                userGroupColumnData.Group = column
                userGroupColumnArray.add(userGroupColumnData)
              }
            }
          }
          firstRowDone = true
          continue
        }
      }
      LOG.info("User Group Sheet: Parsing of group columns complete ***")
      return userGroupColumnArray
    }
        catch (e: Exception)
        {
          throw "Error parsing User Group column sheet at line: " + currRowNum + " Exception : " + e.toString()
        }
  }
}
