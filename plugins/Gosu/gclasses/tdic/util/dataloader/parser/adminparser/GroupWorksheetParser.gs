package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses java.lang.Integer
uses tdic.util.dataloader.data.admindata.GroupData
uses tdic.util.dataloader.data.admindata.RegionData
uses tdic.util.dataloader.data.admindata.SecurityZoneData
uses tdic.util.dataloader.data.admindata.UserData
uses gw.api.upgrade.Coercions

class GroupWorksheetParser extends WorksheetParserUtil {
  construct() {
  }
  var securityZoneArray = new ArrayList <SecurityZoneData>()
  var regionArray = new ArrayList <RegionData>()
  var userArray = new ArrayList <UserData>()
  function parseGroupRow(row: Row): GroupData {
    // Define and instantiate variables 
    var result = new GroupData()
    //get Regions from array
    var regionID1 = (convertCellToString(row.getCell(GroupData.COL_REGION1)))
    var regionID2 = (convertCellToString(row.getCell(GroupData.COL_REGION2)))
    var regionID3 = (convertCellToString(row.getCell(GroupData.COL_REGION3)))
    if (regionArray != null){
      result.Region1 = regionArray.firstWhere(\p -> p.Name == regionID1)
      result.Region2 = regionArray.firstWhere(\p -> p.Name == regionID2)
      result.Region3 = regionArray.firstWhere(\p -> p.Name == regionID3)
    }
    //get Secutity Zone from array
    var secZoneID = (convertCellToString(row.getCell(GroupData.COL_SECURTIY_ZONE)))
    if (securityZoneArray != null){
      result.SecurityZone = securityZoneArray.firstWhere(\p -> p.Name == secZoneID)
    }
    //get Super from User array
    var userID = (convertCellToString(row.getCell(GroupData.COL_SUPERVISOR)))
    if (userArray != null){
      result.Supervisor = userArray.firstWhere(\p -> p.ReferenceName == userID)
    }
    // Populate Group Data
    result.Name = convertCellToString(row.getCell(GroupData.COL_NAME))
    result.PublicID = convertCellToString(row.getCell(GroupData.COL_PUBLIC_ID))
    result.Parent = convertCellToString(row.getCell(GroupData.COL_PARENT))
    result.GroupType = typekey.GroupType.get(convertCellToString(row.getCell(GroupData.COL_GROUP_TYPE)))
    result.Exclude = Coercions.makeBooleanFrom(convertCellToString(row.getCell(GroupData.COL_EXCLUDE)))
    result.LoadFactor = Coercions.makeIntFrom(convertCellToString(row.getCell(GroupData.COL_LOAD_FACTOR)))
    return result
  }

  function parseGroupSheet(sheet: Sheet, regArray: ArrayList <RegionData>,
                           secZoneArray: ArrayList <SecurityZoneData>, usrArray: ArrayList <UserData>): ArrayList <GroupData> {
    regionArray = regArray
    userArray = usrArray
    securityZoneArray = secZoneArray
    return parseSheet(sheet, \r -> parseGroupRow(r), true);
  }
}
