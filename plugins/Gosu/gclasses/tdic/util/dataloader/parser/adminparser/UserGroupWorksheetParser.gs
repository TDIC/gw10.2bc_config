package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses tdic.util.dataloader.data.admindata.UserGroupData

class UserGroupWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parseUserGroupRow(row: Row): UserGroupData {
    var result = new UserGroupData()
    result.UserReferenceName = row.getCell(UserGroupData.COL_USER_REFERENCE_NAME).StringCellValue
    result.UserPublicID = row.getCell(UserGroupData.COL_USER_PUBLIC_ID).StringCellValue
    if (result.UserPublicID.Empty)
      return null
    if (result.UserPublicID != null and result.UserPublicID != "") {
      // step through columns, add custom limit amount to array if cell is non-blank
      for (myCell in row index col) {
        // skip first column
        if (col > 1) {
          // add load factor to array
          result.LoadFactor.add(myCell.toString())
        }
      }
    }
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList <UserGroupData> {
    return super.parseSheet(sheet, \r -> parseUserGroupRow(r), false)
  }
}
