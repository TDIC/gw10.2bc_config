package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses tdic.util.dataloader.data.admindata.AuthorityLimitProfileData
uses gw.api.upgrade.Coercions

class AuthorityLimitProfileWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  override function parseSheet(sheet: Sheet): ArrayList <AuthorityLimitProfileData> {
    return super.parseSheet(sheet, \r -> parseAuthorityLimitProfileRow(r), true);
  }

  function parseAuthorityLimitProfileRow(row: Row): AuthorityLimitProfileData {
    // Define and instantiate variables
    var result = new AuthorityLimitProfileData()
    // Populate AuthorityLimitProfile Data
    result.Name = convertCellToString(row.getCell(AuthorityLimitProfileData.COL_NAME))
    result.PublicID = convertCellToString(row.getCell(AuthorityLimitProfileData.COL_PUBLIC_ID))
    result.Description = convertCellToString(row.getCell(AuthorityLimitProfileData.COL_DESCRIPTION))
    result.Exclude = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AuthorityLimitProfileData.COL_EXCLUDE)))
    return result
  }
}
