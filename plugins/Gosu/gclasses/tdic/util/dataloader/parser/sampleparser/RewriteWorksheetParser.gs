package tdic.util.dataloader.parser.sampleparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses tdic.util.dataloader.data.sampledata.ChargeData
uses tdic.util.dataloader.data.sampledata.RewriteData
uses tdic.util.dataloader.data.sampledata.PolicyPeriodData
uses java.math.BigDecimal
uses java.lang.Integer
uses gw.util.GWBaseDateEnhancement
uses gw.api.upgrade.Coercions

class RewriteWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  // first column is offset days; not imported  
  var REWRITE_DATE: int = 1
  var PRIOR_POLICY_NUMBER: int = 2
  var DESCRIPTION: int = 3
  var LOOKUP_POLICY_NUMBER: int = 20
  var CHANGE_POLICY_NUMBER: int = 21
  //Payment Plan Modifiers
  var MATCH_PLANNED_INSTALLMENTS: int = 22
  var SUPPRESS_DOWN_PAYMENT: int = 23
  var MAX_NUM_OF_INSTALLMENTS: int = 24
  var DOWN_PAYMENT_OVERRIDE: int = 25
  var policyArray: ArrayList <PolicyPeriodData>
  function parseRewriteRow(row: Row): RewriteData {
    var origPolicyNumber = convertCellToString(row.getCell(PRIOR_POLICY_NUMBER))
    if (origPolicyNumber.Empty || origPolicyNumber == null)
      return null

    // Define and instantiate data variables
    var rewriteData = new RewriteData()
    var charges = new ArrayList <ChargeData>()
    var lookupPolicyNumber = convertCellToString(row.getCell(LOOKUP_POLICY_NUMBER))
    var changePolicyNumber = Coercions.makeBooleanFrom(convertCellToString(row.getCell(CHANGE_POLICY_NUMBER)))
    var ppData: PolicyPeriodData
    if (policyArray != null){
      ppData = policyArray.firstWhere(\p -> p.PolicyNumber == lookupPolicyNumber)
    }
    // Populate charges; each charge has 4 columns: charge, amount, payer, stream
    var i = 4
    while (i < 20) {
      var chgCell = i
      var amtCell = i + 1
      var payerCell = i + 2
      var invoiceStreamCell = i + 3
      var amt = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(amtCell as short)))
      var chg = convertCellToString(row.getCell(chgCell as short))
      var payer = convertCellToString(row.getCell(payerCell as short))
      var invoiceStream = convertCellToString(row.getCell(invoiceStreamCell as short))
      populateCharges(charges, amt, chg, payer, invoiceStream)
      i = i + 4
    }
    // Populate Payment Plan Modifiers
    var bolMatchPlannedInstallments = setBoolean(convertCellToString(row.getCell(MATCH_PLANNED_INSTALLMENTS)), false)
    var bolSuppressDownPayment = setBoolean(convertCellToString(row.getCell(SUPPRESS_DOWN_PAYMENT)), false)
    var strMaxNumOfInstallments = convertCellToString(row.getCell(MAX_NUM_OF_INSTALLMENTS))
    var strDownPaymentOverride = convertCellToString(row.getCell(DOWN_PAYMENT_OVERRIDE))
    rewriteData.MatchPlannedInstallments = bolMatchPlannedInstallments
    rewriteData.SuppressDownPayment = bolSuppressDownPayment
    if (strMaxNumOfInstallments != null && strMaxNumOfInstallments != "") {
      rewriteData.MaxNumberOfInstallmentsOverride = Coercions.makeIntFrom(strMaxNumOfInstallments)
    }
    if (strDownPaymentOverride != null && strDownPaymentOverride != "") {
      rewriteData.DownPaymentOverridePercentage = (Coercions.makeBigDecimalFrom(strDownPaymentOverride)).multiply(100)
    }
    // Populate Rewrite DTO 
    rewriteData.PriorPolicyPeriod = origPolicyNumber
    // Save the looked up policy data array to rewrite data
    rewriteData.LookupPolicyPeriod = ppData
    rewriteData.ChangePolicyNumber = changePolicyNumber
    rewriteData.Description = convertCellToString(row.getCell(DESCRIPTION))
    rewriteData.EntryDate = row.getCell(REWRITE_DATE).DateCellValue
    if (rewriteData.EntryDate == null  or rewriteData.EntryDate < gw.api.util.DateUtil.currentDate()) {
      rewriteData.EntryDate = gw.api.util.DateUtil.currentDate()
    }
    rewriteData.Charges = (charges?.toTypedArray())
    return rewriteData
  }

  override function parseSheet(sheet: Sheet): ArrayList <RewriteData> {
    return null
    //Use parseSheetandAddPolicyPeriod instead
  }

  public function parseSheetandAddPolicyPeriod(sheet: Sheet, pcyArray: ArrayList <PolicyPeriodData>): ArrayList <RewriteData> {
    policyArray = pcyArray
    return super.parseSheet(sheet, \r -> parseRewriteRow(r), false)
  }
}