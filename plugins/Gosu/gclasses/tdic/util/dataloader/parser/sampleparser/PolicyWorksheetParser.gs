package tdic.util.dataloader.parser.sampleparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses tdic.util.dataloader.data.sampledata.PolicyPeriodData
uses tdic.util.dataloader.data.AddressData
uses tdic.util.dataloader.data.sampledata.ContactData
uses java.util.ArrayList
uses gw.api.upgrade.Coercions

class PolicyWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }
  var ACCOUNT_NAME: int = 0
  var POLICY_NUMBER: int = 1
  // offset days = column 2; not imported
  var POLICY_PERIOD_EFF_DATE: int = 3
  var POLICY_PERIOD_EXP_DATE: int = 4
  var PAYMENT_PLAN: int = 5
  var BILLING_METHOD: int = 6
  var PRODUCER_CODE: int = 7
  var LOBCODE: int = 8
  var UNDER_AUDIT: int = 9
  var ASSIGNED_RISK: int = 10
  var UNDERWRITER: int = 11
  var UW_COMPANY: int = 12
  var PP_CONTACT_TYPE: int = 13
  var PP_CONTACT_NAME: int = 14
  var PP_CONTACT_FIRST_NAME: int = 15
  var PP_CONTACT_LAST_NAME: int = 16
  var PP_CONTACT_ADDRESS_LINE1: int = 17
  var PP_CONTACT_ADDRESS_LINE2: int = 18
  var PP_CONTACT_CITY: int = 19
  var PP_CONTACT_STATE: int = 20
  var PP_CONTACT_ZIP: int = 21
  var PP_CONTACT_COUNTRY: int = 22
  var PP_CONTACT_PHONE: int = 23
  var PP_CONTACT_EMAIL: int = 24
  var PP_CONTACT_ROLE: int = 25
  var LIST_BILL_PAYER_ACCOUNT: int = 26
  var LIST_BILL_PAYER_PAYMENT_PLAN: int = 27
  var LIST_BILL_PAYER_INVOICE_STREAM: int = 28
  var PP_DBA: int = 29
  function parsePolicyRow(row: Row): PolicyPeriodData {
    var ppData = new PolicyPeriodData()
    var addressData = new AddressData()
    var contactData = new ContactData()
    ppData.PolicyNumber = convertCellToString(row.getCell(POLICY_NUMBER))
    if (ppData.PolicyNumber.Empty)
      return null
    // Populate Policy Period Contact Role
    ppData.Role = typekey.PolicyPeriodRole.get(convertCellToString(row.getCell(PP_CONTACT_ROLE)))
    // Populate Policy Period Contact Address
    addressData.AddressLine1 = convertCellToString(row.getCell(PP_CONTACT_ADDRESS_LINE1))
    addressData.AddressLine2 = convertCellToString(row.getCell(PP_CONTACT_ADDRESS_LINE2))
    addressData.City = convertCellToString(row.getCell(PP_CONTACT_CITY))
    addressData.State = convertCellToString(row.getCell(PP_CONTACT_STATE))
    addressData.PostalCode = convertCellToString(row.getCell(PP_CONTACT_ZIP))
    addressData.Country = typekey.Country.get(convertCellToString(row.getCell(PP_CONTACT_COUNTRY)))
    // Populate Period Contact
    contactData.Name = convertCellToString(row.getCell(PP_CONTACT_NAME))
    contactData.FirstName = convertCellToString(row.getCell(PP_CONTACT_FIRST_NAME))
    contactData.LastName = convertCellToString(row.getCell(PP_CONTACT_LAST_NAME))
    contactData.ContactType = typekey.Contact.get(convertCellToString(row.getCell(PP_CONTACT_TYPE)))
    contactData.Email = convertCellToString(row.getCell(PP_CONTACT_EMAIL))
    contactData.WorkPhone = convertCellToString(row.getCell(PP_CONTACT_PHONE))
    contactData.PrimaryAddress = addressData
    // Populate Policy Period
    ppData.AccountName = convertCellToString(row.getCell(ACCOUNT_NAME))
    ppData.PolicyPerEffDate = row.getCell(POLICY_PERIOD_EFF_DATE).DateCellValue
    ppData.PolicyPerExpirDate = row.getCell(POLICY_PERIOD_EXP_DATE).DateCellValue

    var billingMethod = convertCellToString(row.getCell(BILLING_METHOD))
    var supportedBillingMethod = false
    if (billingMethod == null) {
      ppData.BillingMethod = null
    } else {
      if (billingMethod != "ListBill" and billingMethod != "DirectBill" and billingMethod != "AgencyBill") {
        ppData.BillingMethod = billingMethod + " is not a supported billing method"
      } else {
        ppData.BillingMethod = billingMethod
        supportedBillingMethod = true
      }
    }

    ppData.PaymentPlan = convertCellToString(row.getCell(PAYMENT_PLAN))
    ppData.PaymentMethod = PaymentMethod.TC_CHECK
    ppData.ProducerCode = convertCellToString(row.getCell(PRODUCER_CODE))
    ppData.LOBCode = typekey.LOBCode.get(convertCellToString(row.getCell(LOBCODE)))
    ppData.UnderAudit = Coercions.makeBooleanFrom(convertCellToString(row.getCell(UNDER_AUDIT)))
    if (ppData.UnderAudit == null) {
      ppData.UnderAudit = false
    }
    ppData.AssignedRisk = Coercions.makeBooleanFrom(convertCellToString(row.getCell(ASSIGNED_RISK)))
    if (ppData.AssignedRisk == null) {
      ppData.AssignedRisk = false
    }
    ppData.Underwriter = convertCellToString(row.getCell(UNDERWRITER))
    ppData.UWCompany = typekey.UWCompany.get(convertCellToString(row.getCell(UW_COMPANY)))
    ppData.Contact = contactData

    var billingMethodMessage: String
    if (supportedBillingMethod == true) {
      ppData.ListBillPayerAccount = convertCellToString(row.getCell(LIST_BILL_PAYER_ACCOUNT))
      ppData.ListBillPayerPaymentPlan = convertCellToString(row.getCell(LIST_BILL_PAYER_PAYMENT_PLAN))
      ppData.ListBillPayerInvoiceStream = convertCellToString(row.getCell(LIST_BILL_PAYER_INVOICE_STREAM))
    } else {
      if (billingMethod == null) {
        billingMethodMessage = "not loaded because the Billing Method is null"
      } else {
        billingMethodMessage = "not loaded because the loaded Billing Method is not acceptable."
      }
      ppData.ListBillPayerAccount = billingMethodMessage
      ppData.ListBillPayerPaymentPlan = billingMethodMessage
      ppData.ListBillPayerInvoiceStream = billingMethodMessage
    }

    // example extension column
    ppData.DBA = convertCellToString(row.getCell(PP_DBA))
    return ppData
  }

  override function parseSheet(sheet: Sheet): ArrayList <PolicyPeriodData> {
    return parseSheet(sheet, \r -> parsePolicyRow(r), false)
  }
}
