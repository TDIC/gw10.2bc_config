package tdic.util.dataloader.parser.sampleparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses java.util.ArrayList
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses tdic.util.dataloader.data.sampledata.NewRenewalData
uses tdic.util.dataloader.data.sampledata.PolicyPeriodData
uses tdic.util.dataloader.data.sampledata.ChargeData
uses java.math.BigDecimal
uses gw.util.GWBaseDateEnhancement
uses java.lang.Integer
uses gw.api.upgrade.Coercions

class NewRenewalWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  // first column is offset days; not imported
  var RENEW_DATE: int = 1
  var POLICY_NUMBER: int = 2
  var DESCRIPTION: int = 3
  //Payment Plan Modifiers
  var MATCH_PLANNED_INSTALLMENTS: int = 20
  var SUPPRESS_DOWN_PAYMENT: int = 21
  var MAX_NUM_OF_INSTALLMENTS: int = 22
  var DOWN_PAYMENT_OVERRIDE: int = 23
  var policyArray: ArrayList <PolicyPeriodData>
  function parseNewRenewalRow(row: Row): NewRenewalData {
    var newrenewalData = new NewRenewalData()
    var charges = new ArrayList <ChargeData>()
    var policyNumber = convertCellToString(row.getCell(POLICY_NUMBER))
    var ppData: PolicyPeriodData
    if (policyArray != null){
      ppData = policyArray.firstWhere(\p -> p.PolicyNumber == policyNumber)
    }
    if (ppData == null)
      return null
    // Populate charges; each charge has 4 columns: charge, amount, payer, stream
    var i = 4
    while (i < 20) {
      var chgCell = i
      var amtCell = i + 1
      var payerCell = i + 2
      var invoiceStreamCell = i + 3
      var amt = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(amtCell as short)))
      var chg = convertCellToString(row.getCell(chgCell as short))
      var payer = convertCellToString(row.getCell(payerCell as short))
      var invoiceStream = convertCellToString(row.getCell(invoiceStreamCell as short))
      populateCharges(charges, amt, chg, payer, invoiceStream)
      i = i + 4
    }
    // Populate Payment Plan Modifiers
    var bolMatchPlannedInstallments = setBoolean(convertCellToString(row.getCell(MATCH_PLANNED_INSTALLMENTS)), false)
    var bolSuppressDownPayment = setBoolean(convertCellToString(row.getCell(SUPPRESS_DOWN_PAYMENT)), false)
    var strMaxNumOfInstallments = convertCellToString(row.getCell(MAX_NUM_OF_INSTALLMENTS))
    var strDownPaymentOverride = convertCellToString(row.getCell(DOWN_PAYMENT_OVERRIDE))
    newrenewalData.MatchPlannedInstallments = bolMatchPlannedInstallments
    newrenewalData.SuppressDownPayment = bolSuppressDownPayment
    if (strMaxNumOfInstallments != null && strMaxNumOfInstallments != "") {
      newrenewalData.MaxNumberOfInstallmentsOverride = Coercions.makeIntFrom(strMaxNumOfInstallments)
    }
    if (strDownPaymentOverride != null && strDownPaymentOverride != "") {
      newrenewalData.DownPaymentOverridePercentage = (Coercions.makeBigDecimalFrom(strDownPaymentOverride)).multiply(100)
    }
    // Populate Issuance Data
    newrenewalData.EntryDate = row.getCell(RENEW_DATE).DateCellValue
    if (newrenewalData.EntryDate == null  or newrenewalData.EntryDate < gw.api.util.DateUtil.currentDate()) {
      newrenewalData.EntryDate = gw.api.util.DateUtil.currentDate()
    }
    newrenewalData.NewRenewalPolicyPeriod = ppData
    newrenewalData.Description = convertCellToString(row.getCell(DESCRIPTION))
    newrenewalData.Charges = (charges?.toTypedArray())
    return newrenewalData
  }

  override function parseSheet(sheet: Sheet): ArrayList <Object> {
    return null
    //Use parseSheetandAddPolicyPeriod instead
  }

  public function parseSheetandAddPolicyPeriod(sheet: Sheet, pcyArray: ArrayList <PolicyPeriodData>): ArrayList <NewRenewalData> {
    policyArray = pcyArray
    return super.parseSheet(sheet, \r -> parseNewRenewalRow(r), false)
  }
}