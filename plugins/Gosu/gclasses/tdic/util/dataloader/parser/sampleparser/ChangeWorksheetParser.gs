package tdic.util.dataloader.parser.sampleparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.math.BigDecimal
uses java.lang.Integer
uses tdic.util.dataloader.data.sampledata.PolicyChangeData
uses java.util.ArrayList
uses tdic.util.dataloader.data.sampledata.ChargeData
uses gw.util.GWBaseDateEnhancement
uses gw.api.upgrade.Coercions

class ChangeWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  // first column is offset days; not imported
  var MODIFICATION_DATE: int = 1
  var POLICY_NUMBER: int = 2
  var DESCRIPTION: int = 3
  var MATCH_PLANNED_INSTALLMENTS: int = 20
  var SUPPRESS_DOWN_PAYMENT: int = 21
  var MAX_NUM_OF_INSTALLMENTS: int = 22
  var DOWN_PAYMENT_OVERRIDE: int = 23
  function parsePolicyChangeRow(row: Row): PolicyChangeData {
    var changeData = new PolicyChangeData()
    var charges = new ArrayList <ChargeData>()
    changeData.PolicyNumber = convertCellToString(row.getCell(POLICY_NUMBER))
    if (changeData.PolicyNumber == null)
      return null
    // Populate charges; each charge has 4 columns: charge, amount, payer, stream
    var i = 4
    while (i < 20) {
      var chgCell = i
      var amtCell = i + 1
      var payerCell = i + 2
      var invoiceStreamCell = i + 3
      var amt = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(amtCell as short)))
      var chg = convertCellToString(row.getCell(chgCell as short))
      var payer = convertCellToString(row.getCell(payerCell as short))
      var invoiceStream = convertCellToString(row.getCell(invoiceStreamCell as short))
      populateCharges(charges, amt, chg, payer, invoiceStream)
      i = i + 4
    }
    // Populate Payment Plan Modifiers
    var bolMatchPlannedInstallments = setBoolean(convertCellToString(row.getCell(MATCH_PLANNED_INSTALLMENTS)), false)
    var bolSuppressDownPayment = setBoolean(convertCellToString(row.getCell(SUPPRESS_DOWN_PAYMENT)), false)
    var strMaxNumOfInstallments = convertCellToString(row.getCell(MAX_NUM_OF_INSTALLMENTS))
    var strDownPaymentOverride = convertCellToString(row.getCell(DOWN_PAYMENT_OVERRIDE))
    changeData.MatchPlannedInstallments = bolMatchPlannedInstallments
    changeData.SuppressDownPayment = bolSuppressDownPayment
    if (strMaxNumOfInstallments != null && strMaxNumOfInstallments != "") {
      changeData.MaxNumberOfInstallmentsOverride = Coercions.makeIntFrom(strMaxNumOfInstallments)
    }
    if (strDownPaymentOverride != null && strDownPaymentOverride != "") {
      changeData.DownPaymentOverridePercentage = (Coercions.makeBigDecimalFrom(strDownPaymentOverride)).multiply(100)
    }
    // Populate Policy Change data
    changeData.ModificationDate = row.getCell(MODIFICATION_DATE).DateCellValue
    changeData.EntryDate = row.getCell(MODIFICATION_DATE).DateCellValue
    if (changeData.ModificationDate == null  or changeData.ModificationDate < gw.api.util.DateUtil.currentDate()) {
      changeData.ModificationDate = gw.api.util.DateUtil.currentDate()
      changeData.EntryDate = gw.api.util.DateUtil.currentDate()
    }
    changeData.Description = convertCellToString(row.getCell(DESCRIPTION))
    changeData.Charges = (charges?.toTypedArray())
    return changeData
  }

  override function parseSheet(sheet: Sheet): ArrayList <PolicyChangeData> {
    return parseSheet(sheet, \r -> parsePolicyChangeRow(r), false)
  }
}