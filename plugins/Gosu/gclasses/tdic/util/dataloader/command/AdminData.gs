package tdic.util.dataloader.command


uses gw.command.Argument
uses gw.command.BCBaseCommand
uses tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
//uses com.guidewire.pl.quickjump.Argument
uses tdic.util.dataloader.util.DataLoaderUtil
uses tdic.util.dataloader.export.adminexport.BCAdminExport

@Export class AdminData extends BCBaseCommand {
  construct() {
    super();
  }

  @Argument("path", DataLoaderUtil.getImportAdminDatafilePath())
  function loadAll() {
    var adminData = createDataLoaderProcessor("ALL")
    adminData.Path = getArgumentAsString("path")
    adminData.readAndImportFile()
    adminData.loadData()
    pcf.AdminDataUploadConfirmation.go(adminData);
  }

  //Role

  @Argument("path", DataLoaderUtil.getImportAdminDatafilePath())
  function loadRole() {
    var adminData = createDataLoaderProcessor("Role")
    adminData.Path = getArgumentAsString("path")
    adminData.readAndImportFile()
    adminData.loadData()
    pcf.AdminDataUploadConfirmation.go(adminData);
  }

  //ActivityPattern

  @Argument("path", DataLoaderUtil.getImportAdminDatafilePath())
  function loadActivityPattern() {
    var adminData = createDataLoaderProcessor("ActivityPattern")
    adminData.Path = getArgumentAsString("path")
    adminData.readAndImportFile()
    adminData.loadData()
    pcf.AdminDataUploadConfirmation.go(adminData);
  }

  //SecurityZone

  @Argument("path", DataLoaderUtil.getImportAdminDatafilePath())
  function loadSecurityZone() {
    var adminData = createDataLoaderProcessor("SecurityZone")
    adminData.Path = getArgumentAsString("path")
    adminData.readAndImportFile()
    adminData.loadData()
    pcf.AdminDataUploadConfirmation.go(adminData);
  }

  //AuthorityLimit

  @Argument("path", DataLoaderUtil.getImportAdminDatafilePath())
  function loadAuthorityLimit() {
    var adminData = createDataLoaderProcessor("AuthorityLimit")
    adminData.Path = getArgumentAsString("path")
    adminData.readAndImportFile()
    adminData.loadData()
    pcf.AdminDataUploadConfirmation.go(adminData);
  }

  //UserAuthorityLimit

  @Argument("path", DataLoaderUtil.getImportAdminDatafilePath())
  function loadUserAuthorityLimit() {
    var adminData = createDataLoaderProcessor("UserAuthorityLimit")
    adminData.Path = getArgumentAsString("path")
    adminData.readAndImportFile()
    adminData.loadData()
    pcf.AdminDataUploadConfirmation.go(adminData);
  }

  //User

  @Argument("path", DataLoaderUtil.getImportAdminDatafilePath())
  function loadUsers() {
    var adminData = createDataLoaderProcessor("User")
    adminData.Path = getArgumentAsString("path")
    adminData.readAndImportFile()
    adminData.loadData()
    pcf.AdminDataUploadConfirmation.go(adminData);
  }

  //Group

  @Argument("path", DataLoaderUtil.getImportAdminDatafilePath())
  function loadGroup() {
    var adminData = createDataLoaderProcessor("Group")
    adminData.Path = getArgumentAsString("path")
    adminData.readAndImportFile()
    adminData.loadData()
    pcf.AdminDataUploadConfirmation.go(adminData);
  }

  //UserGroup

  @Argument("path", DataLoaderUtil.getImportAdminDatafilePath())
  function loadUserGroup() {
    var adminData = createDataLoaderProcessor("UserGroup")
    adminData.Path = getArgumentAsString("path")
    adminData.readAndImportFile()
    adminData.loadData()
    pcf.AdminDataUploadConfirmation.go(adminData);
  }

  //Queue - used by Activities accelerator

  @Argument("path", DataLoaderUtil.getImportAdminDatafilePath())
  function loadQueue() {
    var adminData = createDataLoaderProcessor("Queue")
    adminData.Path = getArgumentAsString("path")
    adminData.readAndImportFile()
    adminData.loadData()
    pcf.AdminDataUploadConfirmation.go(adminData);
  }

  //Region

  @Argument("path", DataLoaderUtil.getImportAdminDatafilePath())
  function loadRegion() {
    var adminData = createDataLoaderProcessor("Region")
    adminData.Path = getArgumentAsString("path")
    adminData.readAndImportFile()
    adminData.loadData()
    pcf.AdminDataUploadConfirmation.go(adminData);
  }

  //Attribute

  @Argument("path", DataLoaderUtil.getImportAdminDatafilePath())
  function loadAttribute() {
    var adminData = createDataLoaderProcessor("Attribute")
    adminData.Path = getArgumentAsString("path")
    adminData.readAndImportFile()
    adminData.loadData()
    pcf.AdminDataUploadConfirmation.go(adminData);
  }

  //Holiday

  @Argument("path", DataLoaderUtil.getImportAdminDatafilePath())
  function loadHoliday() {
    var adminData = createDataLoaderProcessor("Holiday")
    adminData.Path = getArgumentAsString("path")
    adminData.readAndImportFile()
    adminData.loadData()
    pcf.AdminDataUploadConfirmation.go(adminData);
  }

  //Export to Excel

  @Argument("path", DataLoaderUtil.getExportAdminDatafilePath())
  function export() {
    var path = getArgumentAsString("path")
    BCAdminExport.exportDataToFile(path, false)
  }

  /****Support functions****/
  function createDataLoaderProcessor(loadEntity: String): BCAdminDataLoaderProcessor {
    var adminData = new BCAdminDataLoaderProcessor()
    if (loadEntity == "ALL") {
      adminData.ChkRole = true
      adminData.ChkActivityPattern = true
      adminData.ChkSecurityZone = true
      adminData.ChkAuthorityLimit = true
      adminData.ChkUserAuthorityLimit = true
      adminData.ChkUser = true
      adminData.ChkGroup = true
      adminData.ChkUserGroup = true
      adminData.ChkQueue = true
      adminData.ChkRegion = true
      adminData.ChkAttribute = true
      adminData.ChkHoliday = true
    }
    else {
      adminData.ChkRole = false
      adminData.ChkActivityPattern = false
      adminData.ChkSecurityZone = false
      adminData.ChkAuthorityLimit = false
      adminData.ChkUserAuthorityLimit = false
      adminData.ChkUser = false
      adminData.ChkGroup = false
      adminData.ChkUserGroup = false
      adminData.ChkQueue = false
      adminData.ChkRegion = false
      adminData.ChkAttribute = false
      adminData.ChkHoliday = false
      if (loadEntity == "Role") {
        adminData.ChkRole = true
      }
      if (loadEntity == "ActivityPattern") {
        adminData.ChkActivityPattern = true
      }
      if (loadEntity == "SecurityZone") {
        adminData.ChkSecurityZone = true
      }
      if (loadEntity == "AuthorityLimit") {
        adminData.ChkAuthorityLimit = true
      }
      if (loadEntity == "UserAuthorityLimit") {
        adminData.ChkUserAuthorityLimit = true
      }
      if (loadEntity == "User") {
        adminData.ChkUser = true
      }
      if (loadEntity == "Group") {
        adminData.ChkGroup = true
      }
      if (loadEntity == "UserGroup") {
        adminData.ChkUserGroup = true
      }
      if (loadEntity == "Queue") {
        adminData.ChkQueue = true
      }
      if (loadEntity == "Region") {
        adminData.ChkRegion = true
      }
      if (loadEntity == "Attribute") {
        adminData.ChkAttribute = true
      }
      if (loadEntity == "Holiday") {
        adminData.ChkHoliday = true
      }
    }
    return adminData;
  }
}
