package tdic.util.dataloader.command

uses gw.command.Argument
uses gw.command.BCBaseCommand
uses tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
//uses com.guidewire.pl.quickjump.Argument
uses tdic.util.dataloader.util.DataLoaderUtil
uses tdic.util.dataloader.export.planexport.BCPlanExport

@Export class PlanData extends BCBaseCommand {
  construct() {
    super();
  }

  //ALL

  @Argument("path", DataLoaderUtil.getImportPlanDatafilePath())
  function loadAll() {
    var planData = createDataLoaderProcessor("ALL")
    planData.Path = getArgumentAsString("path")
    planData.readAndImportFile()
    planData.loadData()
    pcf.PlanDataUploadConfirmation.go(planData);
  }

  //BillingPlans

  @Argument("path", DataLoaderUtil.getImportPlanDatafilePath())
  function loadBillingPlan() {
    var planData = createDataLoaderProcessor("BillingPlan")
    planData.Path = getArgumentAsString("path")
    planData.readAndImportFile()
    planData.loadData()
    pcf.PlanDataUploadConfirmation.go(planData);
  }

  //AgencyBillPlans

  @Argument("path", DataLoaderUtil.getImportPlanDatafilePath())
  function loadAgencyBillPlan() {
    var planData = createDataLoaderProcessor("AgencyBillingPlan")
    planData.Path = getArgumentAsString("path")
    planData.readAndImportFile()
    planData.loadData()
    pcf.PlanDataUploadConfirmation.go(planData);
  }

  //PaymentPlans

  @Argument("path", DataLoaderUtil.getImportPlanDatafilePath())
  function loadPaymentPlan() {
    var planData = createDataLoaderProcessor("PaymentPlan")
    planData.Path = getArgumentAsString("path")
    planData.readAndImportFile()
    planData.loadData()
    pcf.PlanDataUploadConfirmation.go(planData);
  }

  //PaymentPlanOverrides

  @Argument("path", DataLoaderUtil.getImportPlanDatafilePath())
  function loadPaymentPlanOverride() {
    var planData = createDataLoaderProcessor("PaymentPlanOverride")
    planData.Path = getArgumentAsString("path")
    planData.readAndImportFile()
    planData.loadData()
    pcf.PlanDataUploadConfirmation.go(planData);
  }

  //DelinquencyPlan

  @Argument("path", DataLoaderUtil.getImportPlanDatafilePath())
  function loadDelinquencyPlan() {
    var planData = createDataLoaderProcessor("DelinquencyPlan")
    planData.Path = getArgumentAsString("path")
    planData.readAndImportFile()
    planData.loadData()
    pcf.PlanDataUploadConfirmation.go(planData);
  }

  //DelinquencyPlanWorkflow

  @Argument("path", DataLoaderUtil.getImportPlanDatafilePath())
  function loadDelinquencyPlanWorkflow() {
    var planData = createDataLoaderProcessor("DelinquencyPlanWorkflow")
    planData.Path = getArgumentAsString("path")
    planData.readAndImportFile()
    planData.loadData()
    pcf.PlanDataUploadConfirmation.go(planData);
  }

  //CommissionPlan

  @Argument("path", DataLoaderUtil.getImportPlanDatafilePath())
  function loadCommissionPlan() {
    var planData = createDataLoaderProcessor("CommissionPlan")
    planData.Path = getArgumentAsString("path")
    planData.readAndImportFile()
    planData.loadData()
    pcf.PlanDataUploadConfirmation.go(planData);
  }

  //CommissionSubPlan

  @Argument("path", DataLoaderUtil.getImportPlanDatafilePath())
  function loadCommissionSubPlan() {
    var planData = createDataLoaderProcessor("CommissionSubPlan")
    planData.Path = getArgumentAsString("path")
    planData.readAndImportFile()
    planData.loadData()
    pcf.PlanDataUploadConfirmation.go(planData);
  }

  //ChargePattern

  @Argument("path", DataLoaderUtil.getImportPlanDatafilePath())
  function loadChargePattern() {
    var planData = createDataLoaderProcessor("ChargePattern")
    planData.Path = getArgumentAsString("path")
    planData.readAndImportFile()
    planData.loadData()
    pcf.PlanDataUploadConfirmation.go(planData);
  }

  //Payment Allocation Plan

  @Argument("path", DataLoaderUtil.getImportPlanDatafilePath())
  function loadPaymentAllocationPlan() {
    var planData = createDataLoaderProcessor("PaymentAllocationPlan")
    planData.Path = getArgumentAsString("path")
    planData.readAndImportFile()
    planData.loadData()
    pcf.PlanDataUploadConfirmation.go(planData);
  }

  //Return Premium Plan

  @Argument("path", DataLoaderUtil.getImportPlanDatafilePath())
  function loadReturnPremiumPlan() {
    var planData = createDataLoaderProcessor("ReturnPremiumPlan")
    planData.Path = getArgumentAsString("path")
    planData.readAndImportFile()
    planData.loadData()
    pcf.PlanDataUploadConfirmation.go(planData);
  }

  //Return Premium Scheme

  @Argument("path", DataLoaderUtil.getImportPlanDatafilePath())
  function loadReturnPremiumScheme() {
    var planData = createDataLoaderProcessor("ReturnPremiumScheme")
    planData.Path = getArgumentAsString("path")
    planData.readAndImportFile()
    planData.loadData()
    pcf.PlanDataUploadConfirmation.go(planData);
  }

  //Export to Excel

  @Argument("path", DataLoaderUtil.getExportPlanDatafilePath())
  function export() {
    var path = getArgumentAsString("path")
    BCPlanExport.exportDataToFile(path, false)
  }

  /**
   * US570 - Excel Data Loader Additions for custom GL Integration entities
   * 11/14/2014 Alvin Lee
   */
  @Argument("path",  DataLoaderUtil.getImportPlanDatafilePath())
  function loadGLTransactionFilter(){
    var planData = createDataLoaderProcessor("GLTransactionFilter")
    planData.Path=getArgumentAsString("path")
    planData.readAndImportFile()
    planData.loadData()
    pcf.PlanDataUploadConfirmation.go(planData);
  }

  /**
   * US570 - Excel Data Loader Additions for custom GL Integration entities
   * 11/14/2014 Alvin Lee
   */
  @Argument("path",  DataLoaderUtil.getImportPlanDatafilePath())
  function loadGLTransactionNameMapping(){
    var planData = createDataLoaderProcessor("GLTransactionNameMapping")
    planData.Path=getArgumentAsString("path")
    planData.readAndImportFile()
    planData.loadData()
    pcf.PlanDataUploadConfirmation.go(planData);
  }

  /**
   * US570 - Excel Data Loader Additions for custom GL Integration entities
   * 11/14/2014 Alvin Lee
   */
  @Argument("path",  DataLoaderUtil.getImportPlanDatafilePath())
  function loadGLTAccountNameMapping(){
    var planData = createDataLoaderProcessor("GLTAccountNameMapping")
    planData.Path=getArgumentAsString("path")
    planData.readAndImportFile()
    planData.loadData()
    pcf.PlanDataUploadConfirmation.go(planData);
  }

  /****Support functions****/
  function createDataLoaderProcessor(loadEntity: String): BCPlanDataLoaderProcessor {
    var planData = new BCPlanDataLoaderProcessor()
    if (loadEntity == "ALL") {
      planData.ChkBillingPlan = true
      planData.ChkAgencyBillPlan = true
      planData.ChkPaymentPlan = true
      planData.ChkPaymentPlanOverride = true
      planData.ChkDelinquencyPlan = true
      planData.ChkDelinquencyPlanWorkflow = true
      planData.ChkCommissionPlan = true
      planData.ChkCommissionSubPlan = true
      planData.ChkChargePattern = true
      planData.ChkPaymentAllocationPlan = true
      planData.ChkReturnPremiumPlan = true
      planData.ChkReturnPremiumScheme = true
      /**
       * US570 - Excel Data Loader Additions for custom GL Integration entities
       * 11/14/2014 Alvin Lee
       */
      planData.ChkGLTransactionFilter=true
      planData.ChkGLTransactionNameMapping=true
      planData.ChkGLTAccountNameMapping=true
    }
    else {
      planData.ChkBillingPlan = false
      planData.ChkAgencyBillPlan = false
      planData.ChkPaymentPlan = false
      planData.ChkPaymentPlanOverride = false
      planData.ChkDelinquencyPlan = false
      planData.ChkDelinquencyPlanWorkflow = false
      planData.ChkCommissionPlan = false
      planData.ChkCommissionSubPlan = false
      planData.ChkChargePattern = false
      planData.ChkPaymentAllocationPlan = false
      planData.ChkReturnPremiumPlan = false
      planData.ChkReturnPremiumScheme = false
      /**
       * US570 - Excel Data Loader Additions for custom GL Integration entities
       * 11/14/2014 Alvin Lee
       */
      planData.ChkGLTransactionFilter=false
      planData.ChkGLTransactionNameMapping=false
      planData.ChkGLTAccountNameMapping=false
      if (loadEntity == "BillingPlan") {
        planData.ChkBillingPlan = true
      }
      if (loadEntity == "AgencyBillPlan") {
        planData.ChkAgencyBillPlan = true
      }
      if (loadEntity == "PaymentPlan") {
        planData.ChkPaymentPlan = true
      }
      if (loadEntity == "PaymentPlanOverride") {
        planData.ChkPaymentPlanOverride = true
      }
      if (loadEntity == "DelinquencyPlan") {
        planData.ChkDelinquencyPlan = true
      }
      if (loadEntity == "DelinquencyPlanWorkflow") {
        planData.ChkDelinquencyPlanWorkflow = true
      }
      if (loadEntity == "CommissionPlan") {
        planData.ChkCommissionPlan = true
      }
      if (loadEntity == "CommissionSubPlan") {
        planData.ChkCommissionSubPlan = true
      }
      if (loadEntity == "ChargePattern") {
        planData.ChkChargePattern = true
      }
      if (loadEntity == "PaymentAllocationPlan") {
        planData.ChkPaymentAllocationPlan = true
      }
      if (loadEntity == "ReturnPremiumPlan") {
        planData.ChkReturnPremiumPlan = true
      }
      if (loadEntity == "ReturnPremiumScheme") {
        planData.ChkReturnPremiumScheme = true
      }
      /**
       * US570 - Excel Data Loader Additions for custom GL Integration entities
       * 11/14/2014 Alvin Lee
       */
      if (loadEntity=="GLTransactionFilter") {
        planData.ChkGLTransactionFilter=true
      }
      if (loadEntity=="GLTransactionNameMapping") {
        planData.ChkGLTransactionNameMapping=true
      }
      if (loadEntity=="GLTAccountNameMapping") {
        planData.ChkGLTAccountNameMapping=true
      }
    }
    return planData;
  }

  public function loadPlanData() {
    var planData = createDataLoaderProcessor("ALL")
    planData.Path = DataLoaderUtil.getImportPlanDatafilePath()
    planData.readAndImportFile()
    planData.loadData()
  }
}
