package tdic.util.dataloader.export.planexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.plandata.DelinquencyPlanWorkflowData
uses java.util.Collection
uses java.util.ArrayList

class BCDelPlanWorkflowExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return DelinquencyPlanWorkflowData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getDelWorkflowEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(DelinquencyPlanWorkflowData.CD), queryDelWorkflows(), \e -> getRowFromDelWorkflow(e))
  }

  // query data

  public static function queryDelWorkflows(): Collection <DelinquencyPlanEvent> {
    var result = new ArrayList <DelinquencyPlanEvent>()
    Query.make(DelinquencyPlan).select()*.DelinquencyPlanReasons*.OrderedEvents.each(\l -> result.addAll(l))
    return result
  }

  // build column map from entity

  public static function getRowFromDelWorkflow(event: DelinquencyPlanEvent): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(DelinquencyPlanWorkflowData.COL_DELINQUENCY_PLAN_NAME), event.DelinquencyPlanReason.DelinquencyPlan)
    colMap.put(colIndexString(DelinquencyPlanWorkflowData.COL_DELINQUENCY_REASON), event.DelinquencyPlanReason.DelinquencyReason)
    colMap.put(colIndexString(DelinquencyPlanWorkflowData.COL_WORKFLOW_TYPE), event.DelinquencyPlanReason.WorkflowType)
    colMap.put(colIndexString(DelinquencyPlanWorkflowData.COL_EVENT_NAME), event.EventName)
    colMap.put(colIndexString(DelinquencyPlanWorkflowData.COL_EVENT_TRIGGER), event.TriggerBasis)
    colMap.put(colIndexString(DelinquencyPlanWorkflowData.COL_EVENT_OFFSET), event.OffsetDays)
    colMap.put(colIndexString(DelinquencyPlanWorkflowData.COL_EVENT_RELATIVE_ORDER), event.RelativeOrder)
    colMap.put(colIndexString(DelinquencyPlanWorkflowData.COL_EVENT_AUTOMATIC), event.Automatic)
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getDelWorkflowColumnTypes(): HashMap {
    return getColumnTypes(DelinquencyPlanWorkflowData.CD)
  }
}



