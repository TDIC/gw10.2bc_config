package tdic.util.dataloader.export.planexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.plandata.CommissionPlanData
uses java.util.Collection
uses gw.api.upgrade.Coercions

class BCCommissionPlanExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return CommissionPlanData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getCommPlanEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(CommissionPlanData.CD), queryCommPlans(), \e -> getRowFromCommPlan(e))
  }

  // query data

  public static function queryCommPlans(): Collection <CommissionPlan> {
    var result = Query.make(CommissionPlan).select()
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromCommPlan(plan: CommissionPlan): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(CommissionPlanData.COL_PUBLIC_ID), plan.PublicID)
    colMap.put(colIndexString(CommissionPlanData.COL_NAME), plan.Name)
    colMap.put(colIndexString(CommissionPlanData.COL_EFFECTIVE_DATE), plan.EffectiveDate.AsUIStyle)
    colMap.put(colIndexString(CommissionPlanData.COL_EXPIRATION_DATE), plan.ExpirationDate.AsUIStyle)
    colMap.put(colIndexString(CommissionPlanData.COL_ALLOWED_TIERS_GOLD), plan.ProducerTiers.hasMatch(\a -> a.toString() == "Gold"))
    colMap.put(colIndexString(CommissionPlanData.COL_ALLOWED_TIERS_SILVER), plan.ProducerTiers.hasMatch(\a -> a.toString() == "Silver"))
    colMap.put(colIndexString(CommissionPlanData.COL_ALLOWED_TIERS_BRONZE), plan.ProducerTiers.hasMatch(\a -> a.toString() == "Bronze"))
    // default subplan data
    var subplan = plan.DefaultSubPlan
    colMap.put(colIndexString(CommissionPlanData.COL_PRIMARY_RATE), subplan.primary["Rate"])
    colMap.put(colIndexString(CommissionPlanData.COL_SECONDARY_RATE), subplan.secondary["Rate"])
    colMap.put(colIndexString(CommissionPlanData.COL_REFERRER_RATE), subplan.referrer["Rate"])
    colMap.put(colIndexString(CommissionPlanData.COL_EARN_COMMISSIONS), subplan.PayableCriteria)
    colMap.put(colIndexString(CommissionPlanData.COL_SUSPEND_FOR_DELINQUENCY), subplan.SuspendForDelinquency)
    var incentive: PremiumIncentive = null
    // import of incentives not supported yet, so don't bother exporting the values
    /*
    if (subplan.Incentives.Count > 0 and subplan.Incentives[0] typeis PremiumIncentive) {
      incentive = subplan.Incentives[0] as PremiumIncentive
    }
    */
    colMap.put(colIndexString(CommissionPlanData.COL_PREMIUM_INCENTIVE_BONUS_PERCENTAGE), incentive != null ? incentive.BonusPercentage : Coercions.makeBigDecimalFrom(""))
    colMap.put(colIndexString(CommissionPlanData.COL_PREMIUM_INCENTIVE_THRESHOLD), incentive != null ? incentive.Threshold : "")
    // get commissionable charge items
    var codes = subplan.CommissionableChargeItems*.ChargePattern*.ChargeCode
    colMap.put(colIndexString(CommissionPlanData.COL_COMMISSIONABLE_ITEM1), codes.Count > 0 ? codes[0] : "")
    colMap.put(colIndexString(CommissionPlanData.COL_COMMISSIONABLE_ITEM2), codes.Count > 1 ? codes[1] : "")
    colMap.put(colIndexString(CommissionPlanData.COL_COMMISSIONABLE_ITEM3), codes.Count > 2 ? codes[2] : "")
    colMap.put(colIndexString(CommissionPlanData.COL_COMMISSIONABLE_ITEM4), codes.Count > 3 ? codes[3] : "")
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getCommPlanColumnTypes(): HashMap {
    return getColumnTypes(CommissionPlanData.CD)
  }
}
