package tdic.util.dataloader.export.planexport

uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.plandata.GLTAccountNameMappingData
uses java.util.HashMap
uses java.util.Collection
uses gw.api.database.Query

/**
 * US570 - General Ledger Integration
 * 11/17/2014 Alvin Lee
 *
 * Excel Data Loader Additions for custom GL Integration entities.  Export for T-Account name mappings.
 */
class BCGLTAccountNameMappingExport extends WorksheetExportUtil {

  /**
   * Standard constructor.
   */
  construct() {
  }
  
  /**
   * Gets the Excel sheet name for the GL T-Account Name Mappings.
   * 
   * @return A String containing the Excel sheet name for the GL T-Account Name Mappings
   */
  public static function getSheetName() : String {
    return GLTAccountNameMappingData.SHEET
  }
  
  /**
   * A method to fetch the entity values for the GL T-Account Name Mappings.
   * 
   * @return A HashMap containing the header name and values of the entity
   */
  public static function getGLTAccountNameMappingEntityValues () : HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(GLTAccountNameMappingData.CD), queryGLTAccountNameMappings(), \e -> getRowFromGLTAccountNameMapping(e)) 
  }
  
  /**
   * Queries the existing GL T-Account Name Mappings in the database.
   * 
   * @return A Collection<GLMappedTransaction_Ext> of the GL T-Account Name Mappings retrieved from the database
   */
  public static function queryGLTAccountNameMappings() : Collection<GLIntegrationTAccountMapping_TDIC> {
    var result = Query.make(GLIntegrationTAccountMapping_TDIC).select()
    return result.toCollection()
  }
  
  /**
   * Method to build column map from entity.
   * 
   * @param glTAccountMapping The GLIntegrationTAccountMapping_TDIC with the values to map
   * @return A HashMap containing the column name mapped to the entity's values
   */
  public static function getRowFromGLTAccountNameMapping(glTAccountMapping : GLIntegrationTAccountMapping_TDIC) : HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(GLTAccountNameMappingData.COL_TRANS_TYPE), glTAccountMapping.TranasctionType)
    colMap.put(colIndexString(GLTAccountNameMappingData.COL_LINE_ITEM_TYPE), glTAccountMapping.LineItemType)
    colMap.put(colIndexString(GLTAccountNameMappingData.COL_FUTURE_TERM), glTAccountMapping.FutureTerm)
    colMap.put(colIndexString(GLTAccountNameMappingData.COL_INVOICE_STATUS), glTAccountMapping.InvoiceStatus)
    colMap.put(colIndexString(GLTAccountNameMappingData.COL_ORIGINAL_TACCOUNT_NAME), glTAccountMapping.OriginalTAccountName)
    colMap.put(colIndexString(GLTAccountNameMappingData.COL_MAPPED_TACCOUNT_NAME), glTAccountMapping.MappedTAccountName)
    return colMap
  }
  
  /**
   * Method to fetch the type of each column in the entity.  Used to create cell type during excel export.
   * 
   * @return A HashMap containing the type of each column in the entity
   */
  static function getGLTAccountNameMappingColumnTypes() : HashMap {
    return getColumnTypes(GLTAccountNameMappingData.CD)
  }

}
