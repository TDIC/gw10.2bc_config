package tdic.util.dataloader.export.planexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.plandata.AgencyBillPlanData
uses java.util.Collection

class BCAgencyBillPlanExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return AgencyBillPlanData.SHEET
  }

  // Function to get agency bill plan data

  public static function getAgcyBillPlanEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(AgencyBillPlanData.CD), queryAgencyBillPlans(), \e -> getRowFromAgencyBillPlan(e))
  }

  // query data

  public static function queryAgencyBillPlans(): Collection <AgencyBillPlan> {
    var result = Query.make(AgencyBillPlan).select()
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromAgencyBillPlan(plan: AgencyBillPlan): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(AgencyBillPlanData.COL_PUBLIC_ID), plan.PublicID)
    colMap.put(colIndexString(AgencyBillPlanData.COL_NAME), plan.Name)
    colMap.put(colIndexString(AgencyBillPlanData.COL_WorkflowType), plan.WorkflowPlan)
    colMap.put(colIndexString(AgencyBillPlanData.COL_EFFECTIVE_DATE), plan.EffectiveDate.AsUIStyle)
    colMap.put(colIndexString(AgencyBillPlanData.COL_EXPIRATION_DATE), plan.ExpirationDate.AsUIStyle)
    colMap.put(colIndexString(AgencyBillPlanData.COL_CycleCloseDayOfMonthLogic), plan.CycleCloseDayOfMonthLogic)
    colMap.put(colIndexString(AgencyBillPlanData.COL_CycleCloseDayOfMonth), plan.CycleCloseDayOfMonth)
    colMap.put(colIndexString(AgencyBillPlanData.COL_PaymentTermsInDays), plan.PaymentTermsInDays)
    colMap.put(colIndexString(AgencyBillPlanData.COL_ExceptionForPastDueStatement), plan.ExceptionForPastDueStatement)
    colMap.put(colIndexString(AgencyBillPlanData.COL_StatementSentAfterCycleClose), plan.StatementSentAfterCycleClose)
    colMap.put(colIndexString(AgencyBillPlanData.COL_DaysAfterCycleCloseToSendStmnt), plan.DaysAfterCycleCloseToSendStmnt)
    colMap.put(colIndexString(AgencyBillPlanData.COL_SnapshotNonPastDueItems), plan.SnapshotNonPastDueItems)
    colMap.put(colIndexString(AgencyBillPlanData.COL_StatementsWithLowNetSuppressed), plan.StatementsWithLowNetSuppressed)
    colMap.put(colIndexString(AgencyBillPlanData.COL_NetThresholdForSuppressingStmt), plan.NetThresholdForSuppressingStmt)
    colMap.put(colIndexString(AgencyBillPlanData.COL_ReminderSentIfPromiseNotRcvd), plan.ReminderSentIfPromiseNotRcvd)
    colMap.put(colIndexString(AgencyBillPlanData.COL_DaysUntilPromiseReminderSent), plan.DaysUntilPromiseReminderSent)
    colMap.put(colIndexString(AgencyBillPlanData.COL_PromiseDueInDays), plan.PromiseDueInDays)
    colMap.put(colIndexString(AgencyBillPlanData.COL_ExceptionIfPromiseNotReceived), plan.ExceptionIfPromiseNotReceived)
    colMap.put(colIndexString(AgencyBillPlanData.COL_PromiseExceptionsSent), plan.PromiseExceptionsSent)
    colMap.put(colIndexString(AgencyBillPlanData.COL_AutoProcessWhenPaymentMatches), plan.AutoProcessWhenPaymentMatches)
    colMap.put(colIndexString(AgencyBillPlanData.COL_PaymentExceptionsSent), plan.PaymentExceptionsSent)
    colMap.put(colIndexString(AgencyBillPlanData.COL_FirstDunningSentIfNotPaid), plan.FirstDunningSentIfNotPaid)
    colMap.put(colIndexString(AgencyBillPlanData.COL_DaysUntilFirstDunningSent), plan.DaysUntilFirstDunningSent)
    colMap.put(colIndexString(AgencyBillPlanData.COL_SecondDunningSentIfNotPaid), plan.SecondDunningSentIfNotPaid)
    colMap.put(colIndexString(AgencyBillPlanData.COL_DaysUntilSecondDunningSent), plan.DaysUntilSecondDunningSent)
    colMap.put(colIndexString(AgencyBillPlanData.COL_ProducerWriteoffThreshold), plan.ProducerWriteoffThreshold)
    colMap.put(colIndexString(AgencyBillPlanData.COL_LowCommissionCleared), plan.LowCommissionCleared)
    colMap.put(colIndexString(AgencyBillPlanData.COL_ClearCommissionThreshold), plan.ClearCommissionThreshold)
    colMap.put(colIndexString(AgencyBillPlanData.COL_LowGrossCleared), plan.LowGrossCleared)
    colMap.put(colIndexString(AgencyBillPlanData.COL_ClearGrossThreshold), plan.ClearGrossThreshold)
    colMap.put(colIndexString(AgencyBillPlanData.COL_ClearingLogicTarget), plan.ClearingLogicTarget)
    colMap.put(colIndexString(AgencyBillPlanData.COL_CreateOffsetsOnBilledInvoices), plan.CreateOffsetsOnBilledInvoices)
    colMap.put(colIndexString(AgencyBillPlanData.COL_PmntSchdChngOffsetsOnBilled), plan.PmntSchdChngOffsetsOnBilled)
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getAgcyBillPlanColumnTypes(): HashMap {
    return getColumnTypes(AgencyBillPlanData.CD)
  }
}
