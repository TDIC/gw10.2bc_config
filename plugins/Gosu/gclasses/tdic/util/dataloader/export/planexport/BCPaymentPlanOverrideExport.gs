package tdic.util.dataloader.export.planexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.plandata.PaymentPlanOverrideData
uses java.util.Collection

class BCPaymentPlanOverrideExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return PaymentPlanOverrideData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getPayPlanOverrideEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(PaymentPlanOverrideData.CD), queryPayPlanOverrides(), \e -> getRowFromPayPlanOverride(e))
  }

  // query data

  public static function queryPayPlanOverrides(): Collection <ChargeSlicingOverrides> {
    var result = Query.make(ChargeSlicingOverrides).select()
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromPayPlanOverride(plan: ChargeSlicingOverrides): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(PaymentPlanOverrideData.COL_LookupPaymentPlanData), plan.ChargeSlicingModifier["PaymentPlan"])
    colMap.put(colIndexString(PaymentPlanOverrideData.COL_OverrideBillingInstruction), plan.ChargeSlicingModifier["BillingInstructionType"])
    colMap.put(colIndexString(PaymentPlanOverrideData.COL_DOWNPAYMENT_PERCENT), plan.DownPaymentPercent)
    colMap.put(colIndexString(PaymentPlanOverrideData.COL_MAXIMUM_NUMBER_OF_INSTALLMENTS), plan.MaximumNumberOfInstallments)
    colMap.put(colIndexString(PaymentPlanOverrideData.COL_DAYS_FROM_REFERENCE_DATE_TO_DOWNPAYMENT), plan.DaysFromReferenceDateToDownPayment)
    colMap.put(colIndexString(PaymentPlanOverrideData.COL_DOWNPAYMENT_AFTER), plan.DownPaymentAfter)
    colMap.put(colIndexString(PaymentPlanOverrideData.COL_DAYS_FROM_REFERENCE_DATE_TO_FIRST_INSTALLMENT), plan.DaysFromReferenceDateToFirstInstallment)
    colMap.put(colIndexString(PaymentPlanOverrideData.COL_FIRST_INSTALLMENT_AFTER), plan.FirstInstallmentAfter)
    colMap.put(colIndexString(PaymentPlanOverrideData.COL_DAYS_FROM_REFERENCE_DATE_TO_ONETIME_CHARGE), plan.DaysFromReferenceDateToOneTimeCharge)
    colMap.put(colIndexString(PaymentPlanOverrideData.COL_ONETIME_CHARGE_AFTER), plan.OneTimeChargeAfter)
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getPayPlanOverrideColumnTypes(): HashMap {
    return getColumnTypes(PaymentPlanOverrideData.CD)
  }
}
