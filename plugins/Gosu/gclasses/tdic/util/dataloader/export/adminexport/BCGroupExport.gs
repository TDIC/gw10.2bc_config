package tdic.util.dataloader.export.adminexport

uses java.util.HashMap
uses gw.api.database.Query
uses java.util.Collection
uses tdic.util.dataloader.data.admindata.GroupData
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.util.DataLoaderUtil

class BCGroupExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return GroupData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getGroupEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(GroupData.CD), queryGroups(), \e -> getRowFromGroup(e))
  }

  // query data

  public static function queryGroups(): Collection <Group> {
    var result = Query.make(Group).select().where(\g -> !DataLoaderUtil.preventExport(g)).orderBy(\g -> g.PublicID)
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromGroup(group: Group): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(GroupData.COL_NAME), group.Name)
    colMap.put(colIndexString(GroupData.COL_PUBLIC_ID), group.PublicID)
    colMap.put(colIndexString(GroupData.COL_PARENT), group.Parent)
    colMap.put(colIndexString(GroupData.COL_SUPERVISOR), group.Supervisor.DisplayName + " (" + group.Supervisor.Credential + ")")
    colMap.put(colIndexString(GroupData.COL_GROUP_TYPE), group.GroupType)
    colMap.put(colIndexString(GroupData.COL_EXCLUDE), "false")
    colMap.put(colIndexString(GroupData.COL_LOAD_FACTOR), group.LoadFactor)
    colMap.put(colIndexString(GroupData.COL_SECURTIY_ZONE), group.SecurityZone)
    colMap.put(colIndexString(GroupData.COL_REGION1), group.Regions.Count > 0 ? group.Regions[0] : "")
    colMap.put(colIndexString(GroupData.COL_REGION2), group.Regions.Count > 1 ? group.Regions[1] : "")
    colMap.put(colIndexString(GroupData.COL_REGION3), group.Regions.Count > 2 ? group.Regions[2] : "")
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getGroupColumnTypes(): HashMap {
    return getColumnTypes(GroupData.CD)
  }
}
