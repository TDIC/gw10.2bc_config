package tdic.util.dataloader.export.adminexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.data.admindata.ActivityPatternData
uses tdic.util.dataloader.export.WorksheetExportUtil
uses java.util.Collection
uses gw.api.database.QuerySelectColumns
uses gw.api.path.Paths

class BCActivityPatternExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return ActivityPatternData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getActPatternEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(ActivityPatternData.CD), queryActPatterns(), \e -> getRowFromActPattern(e))
  }

  // query data

  public static function queryActPatterns(): Collection <ActivityPattern> {
    var result = Query.make(ActivityPattern).select().orderBy(
      QuerySelectColumns.path(Paths.make(entity.ActivityPattern#PublicID))
    )
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromActPattern(actPattern: ActivityPattern): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(ActivityPatternData.COL_CODE), actPattern.Code)
    colMap.put(colIndexString(ActivityPatternData.COL_PUBLIC_ID), actPattern.PublicID)
    colMap.put(colIndexString(ActivityPatternData.COL_EXCLUDE), "false")
    colMap.put(colIndexString(ActivityPatternData.COL_SUBJECT), actPattern.Subject)
    colMap.put(colIndexString(ActivityPatternData.COL_DESCRIPTION), actPattern.Description)
    colMap.put(colIndexString(ActivityPatternData.COL_ACTIVITY_CLASS), actPattern.ActivityClass)
    colMap.put(colIndexString(ActivityPatternData.COL_TYPE), actPattern.Type)
    colMap.put(colIndexString(ActivityPatternData.COL_CATEGORY), actPattern.Category)
    colMap.put(colIndexString(ActivityPatternData.COL_MANDATORY), actPattern.Mandatory)
    colMap.put(colIndexString(ActivityPatternData.COL_PRIORITY), actPattern.Priority)
    colMap.put(colIndexString(ActivityPatternData.COL_RECURRING), actPattern.Recurring)
    colMap.put(colIndexString(ActivityPatternData.COL_TARGET_DAYS), actPattern.TargetDays)
    colMap.put(colIndexString(ActivityPatternData.COL_TARGET_HOURS), actPattern.TargetHours)
    colMap.put(colIndexString(ActivityPatternData.COL_TARGET_INCLUDE_DAYS), actPattern.TargetIncludeDays)
    colMap.put(colIndexString(ActivityPatternData.COL_TARGET_START_POINT), actPattern.TargetStartPoint)
    colMap.put(colIndexString(ActivityPatternData.COL_ESCALATION_DAYS), actPattern.EscalationDays)
    colMap.put(colIndexString(ActivityPatternData.COL_ESCALATION_HOURS), actPattern.EscalationHours)
    colMap.put(colIndexString(ActivityPatternData.COL_ESCALATION_INCL_DAYS), actPattern.EscalationInclDays)
    colMap.put(colIndexString(ActivityPatternData.COL_ESCALATION_START_POINT), actPattern.EscalationStartPt)
    colMap.put(colIndexString(ActivityPatternData.COL_AUTOMATED_ONLY), actPattern.AutomatedOnly)
    colMap.put(colIndexString(ActivityPatternData.COL_COMMAND), actPattern.Command)
    // colMap.put(colIndexString(ActivityPatternData.COL_QUEUE_DISPLAYNAME), actPattern.AssignableQueue_ext.DisplayName)
    // use reflection to avoid compile errors if activity pattern queue extension is not used
    var queueName = ActivityPatternData.getAssignableQueue(actPattern).DisplayName
    colMap.put(colIndexString(ActivityPatternData.COL_QUEUE_DISPLAYNAME), queueName != null ? queueName : "")
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getActPatternColumnTypes(): HashMap {
    return getColumnTypes(ActivityPatternData.CD)
  }
}
