package tdic.util.dataloader.export.adminexport

uses java.util.HashMap
uses org.apache.poi.ss.usermodel.Cell
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.admindata.UserAuthorityLimitData
uses java.util.Collection
uses java.util.List

class BCUserAuthorityLimitExport extends WorksheetExportUtil {
  // constants
  public static var authLimitTypes: List <AuthorityLimitType> = AuthorityLimitType.getTypeKeys(false).copy().sortBy(\a -> a.Code)
  construct(uCol: Collection <User>) {
    Users = uCol
  }
  var _users: Collection <User> as Users
  // get sheet name

  public static function getSheetName(): String {
    return UserAuthorityLimitData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public function getUserAuthLimitEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getUserAuthLimitHeaderNames(), Users, \e -> getUserAuthLimitRowFromUser(e))
  }

  // build column map from entity

  public static function getUserAuthLimitRowFromUser(user: User): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(UserAuthorityLimitData.COL_USER_REFERENCE_NAME), user.DisplayName + " (" + user.Credential + ")")
    colMap.put(colIndexString(UserAuthorityLimitData.COL_USER_PUBLIC_ID), user.PublicID)
    // export associated profile only for non-custom profiles
    if (!user.AuthorityProfile.Custom) {
      colMap.put(colIndexString(UserAuthorityLimitData.COL_PROFILE), user.AuthorityProfile.Name)
    } else {
      // null or custom profile
      colMap.put(colIndexString(UserAuthorityLimitData.COL_PROFILE), "")
    }
    // export an Authority Limit Public-ID that is unique per user, suggested is username
    colMap.put(colIndexString(UserAuthorityLimitData.COL_AUTHORITY_LIMIT_PUBLIC_ID), user.Credential)
    // export limit values only for custom profiles and empty values otherwise
    var i = 0
    for (limitType in authLimitTypes) {
      if (user.AuthorityProfile.Custom) {
        var limit = user.AuthorityProfile.Limits.firstWhere(\a -> a.LimitType == limitType)
        colMap.put(colIndexString(UserAuthorityLimitData.COL_AUTHORITY_LIMIT_VALUE1 + i), limit.LimitAmount)
      } else {
        colMap.put(colIndexString(UserAuthorityLimitData.COL_AUTHORITY_LIMIT_VALUE1 + i), "")
      }
      i++
    }
    return colMap
  }

  /* @return : HashMap containing the user authority limit names
     @desc   : Method to fetch the header name for the user authority export sheet
  */

  static function getUserAuthLimitHeaderNames(): HashMap {
    var headerMap = new HashMap()
    // headers of fixed columns
    var fixed_colHeaders = UserAuthorityLimitData.CD*.ColHeader
    for (colHeader in fixed_colHeaders index i) {
      headerMap.put(colIndexString(i), colHeader)
    }
    // headers of dynamic columns
    for (aLimitType in authLimitTypes index i) {
      headerMap.put(colIndexString(UserAuthorityLimitData.COL_AUTHORITY_LIMIT_VALUE1 + i), aLimitType.Code)
    }
    return headerMap
  }

  /* @return : HashMap containing the type of each column in user authority limit entity
     @desc   : Method to fetch the type of each column in user authority limit  entity. 
               Used to create cell type during excel export  
  */

  static function getUserAuthLimitColumnTypes(): HashMap {
    var columnTypeMap = new HashMap()
    // types of fixed columns
    var fixed_colTypes = UserAuthorityLimitData.CD*.ColType
    for (colType in fixed_colTypes index i) {
      columnTypeMap.put(i, colType)
    }
    // types of dynamic columns
    for (i in 0..|authLimitTypes.Count) {
      columnTypeMap.put(UserAuthorityLimitData.COL_AUTHORITY_LIMIT_VALUE1 + i, Cell.CELL_TYPE_STRING)
    }
    return columnTypeMap
  }
}
