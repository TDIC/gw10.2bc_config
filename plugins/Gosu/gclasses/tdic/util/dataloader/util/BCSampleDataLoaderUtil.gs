package tdic.util.dataloader.util

uses java.io.File
uses java.io.BufferedWriter
uses java.io.FileWriter
uses gw.api.databuilder.UniqueKeyGenerator
uses gw.api.database.Relop
uses java.util.List
uses gw.api.database.QuerySelectColumns
uses gw.api.path.Paths

class BCSampleDataLoaderUtil extends DataLoaderUtil {
  construct() {
  }

  public static function printSampleData() {
    printPaymentPlans()
    printBillingPlans()
    printCommissionPlans()
    printAgencyBillPlans()
    printDelinquencyPlans()
    printChargePatterns()
    printLOB()
    printUsers()
    printProducerTiers()
    printPeriodicity()
    printAccountRole()
    printProducerRole()
    printInvoiceDelivery()
    printUWCompany()
    printPolicyPeriodRole()
    printPaymentMethod()
    printCancelType()
    printDistributionLimitType()
    printAccountType()
    printBatchProcessType()
    printPaymentReversalReason()
    printPaymentMethod()
    printCreditCardIssuer()
  }

  public static function printPaymentPlans() {
    var query = gw.api.database.Query.make(entity.PaymentPlan).select()
    print("Payment Plans")
    for (y in query) {
      print(y.Name)
    }
    print("")
  }

  public static function printBillingPlans() {
    var query = gw.api.database.Query.make(entity.BillingPlan).select()
    print("Billing Plans")
    for (y in query) {
      print(y.Name)
    }
    print("")
  }

  public static function printCommissionPlans() {
    var query = gw.api.database.Query.make(entity.CommissionPlan).select()
    print("Commission Plans")
    for (y in query) {
      print(y.Name)
    }
    print("")
  }

  public static function printAgencyBillPlans() {
    var query = gw.api.database.Query.make(entity.AgencyBillPlan).select()
    print("Agency Bill Plans")
    for (y in query) {
      print(y.Name)
    }
    print("")
  }

  public static function printDelinquencyPlans() {
    var query = gw.api.database.Query.make(entity.DelinquencyPlan).select()
    print("Delinquency Plans")
    for (y in query) {
      print(y.Name)
    }
    print("")
  }

  public static function printChargePatterns() {
    var query = gw.api.database.Query.make(entity.ChargePattern).select()
    print("Charge Patterns")
    for (y in query) {
      print(y.ChargeCode)
    }
    print("")
  }

  public static function printLOB() {
    print("Lines of Business")
    for (y in LOBCode.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printUsers() {
    var query = gw.api.database.Query.make(entity.User).select()
    print("Users")
    for (y in query) {
      print(y.Credential)
    }
    print("")
  }

  public static function printProducerTiers() {
    print("Producer Tiers")
    for (y in ProducerTier.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printPeriodicity() {
    print("Periodicity")
    for (y in Periodicity.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printAccountRole() {
    print("Account Contact Role")
    for (y in AccountRole.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printProducerRole() {
    print("Producer Contact Role")
    for (y in ProducerRole.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printInvoiceDelivery() {
    print("Invoice Delivery")
    for (y in InvoiceDeliveryMethod.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printUWCompany() {
    print("UW Company Type")
    for (y in UWCompany.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printPolicyPeriodRole() {
    print("Policy Period Contact Role")
    for (y in PolicyPeriodRole.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printPaymentMethod() {
    print("Payment Method")
    for (y in PaymentMethod.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printCancelType() {
    print("Cancellation Type")
    for (y in CancellationType.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printDistributionLimitType() {
    print("Distribution Limit Type")
    for (y in DistributionLimitType.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printAccountType() {
    print("Account Type")
    for (y in AccountType.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printBatchProcessType() {
    print("Batch Process Type")
    for (y in BatchProcessType.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printPaymentReversalReason() {
    print("Payment Reversal Reason Type")
    for (y in PaymentReversalReason.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printCreditCardIssuer() {
    print("Credit Card Issuer")
    //SW - Commented out to be fixed later.
    /*for (y in CreditCardIssuer.getTypeKeys( false )){

      print(y.Code)
    } */
    print("")
  }

  public static function runBatch() {
    var object: List <Object>
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("Invoice", object?.toTypedArray())
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("AccountInactivity", object?.toTypedArray())
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("ActivityEsc", object?.toTypedArray())
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("InvoiceDue", object?.toTypedArray())
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("AdvanceExpiration", object?.toTypedArray())
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("AgencySuspensePayment", object?.toTypedArray())
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("AutomaticDisbursement", object?.toTypedArray())
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("ChargeProRataTx", object?.toTypedArray())
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("CmsnPayable", object?.toTypedArray())
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("CommissionPmt", object?.toTypedArray())
    // gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("ExcessFund", object as java.lang.Object[])
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("PolicyClosure", object?.toTypedArray())
    //gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("ProducerPayment", object as java.lang.Object[])
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("Disbursement", object?.toTypedArray())
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("SuspensePayment", object?.toTypedArray())
   // gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("TroubleTicketEsc", object as java.lang.Object[])
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("StatementBilled", object?.toTypedArray())
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("StatementDue", object?.toTypedArray())
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("PaymentRequest", object?.toTypedArray())
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("NewPayment", object?.toTypedArray())
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("ReleaseTtktHoldTypes", object?.toTypedArray())
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("ReleaseChargeHolds", object?.toTypedArray())
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("AgencySuspensePayment", object?.toTypedArray())
    gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop("Workflow", object?.toTypedArray())
  }

  public static function runABatch(batchName: String) {
    var object: List <Object>
    if (batchName != null && batchName != "Default") {
      gw.workqueue.WorkQueueTestUtil.startWriterThenWorkersAndWaitUntilWorkFinishedThenStop(batchName,object?.toTypedArray())
    }
  }

  public static function printRequiredPaymentsToFile() {
    printRequiredPaymentsToFile(null)
  }

  // used by the SampleData command

  public static function printRequiredPaymentsToFile(filePath: String) {
    if (filePath == null) filePath = DataLoaderUtil.getFullExportDatafilePath(DataLoaderUtil.EXPORT_DBPAYMENTFILE)
    var payments = gw.api.database.Query.make(entity.Invoice).compare("Subtype", Relop.Equals, typekey.Invoice.TC_ACCOUNTINVOICE).select().where(\i -> !i.isClosed()).orderBy(\i -> i.InvoiceNumber)
    var toFile = new File(filePath)
    var writer = new BufferedWriter(new FileWriter(toFile))
    for (payment in payments) {
      var paymentAmount: java.math.BigDecimal
      var sPaymentRefNumber = payment.InvoiceNumber + "-" + UniqueKeyGenerator.get().nextKey()
      if ((payment.InvoiceStream.InvoicePayer as Account).DistributionLimitTypeFromPlan == TC_OUTSTANDING && payment.Amount != null) {
        paymentAmount = payment.Amount
        var stringToFile = payment.DueDate.AsUIStyle + "\t\"" + payment.InvoiceItems.first().AccountPayer.AccountName
            + "\"\t\t" + "" + "\t\"" + sPaymentRefNumber + "\"\t" + "\"cash\"" + "\t" + paymentAmount
        writer.write(stringToFile)
        writer.newLine()
      } else {
        if (!payment.Paid && payment.AmountDue != null) {
          paymentAmount = payment.AmountDue
          var stringToFile = payment.DueDate.AsUIStyle + "\t\"" + payment.InvoiceItems.first().AccountPayer.AccountName
              + "\"\t\t" + "" + "\t\"" + sPaymentRefNumber + "\"\t" + "\"cash\"" + "\t" + paymentAmount
          writer.write(stringToFile)
          writer.newLine()
        }
      }
    }
    print("Payment File Location: " + toFile.AbsolutePath)
    writer.flush()
    writer.close()
  }

  //Generate the Agency Bill Payments file

  public static function printABRequiredPaymentsToFile() {
    printABRequiredPaymentsToFile(null)
  }

  public static function printABRequiredPaymentsToFile(filePath: String) {
    if (filePath == null) filePath = DataLoaderUtil.getFullExportDatafilePath(DataLoaderUtil.EXPORT_ABPAYMENTFILE)
    var payments = gw.api.database.Query.make(entity.StatementInvoice).select().orderBy(
      QuerySelectColumns.path(Paths.make(entity.StatementInvoice#PaymentDueDate))
    )
    var toFile = new File(filePath)
    var writer = new BufferedWriter(new FileWriter(toFile))
    for (payment in payments) {
      if (payment.NetAmountDue.IsNotZero) {
        var sPaymentRefNumber = payment.InvoiceNumber + "-" + UniqueKeyGenerator.get().nextKey()
        var stringToFile = payment.DueDate + "\t\"" + payment.AgencyBillCycle.Producer + "\"\t" + payment.InvoiceNumber + "\t" + "" + "\t\"" + sPaymentRefNumber + "\"\t" + "\"cash\"" + "\t" + payment.NetAmount
        /* foreach (invitem in payment.InvoiceItems index i) {
             stringToFile = stringToFile + "\t" + invitem.GrossAmountDueIncludingPromise + "\t" + invitem.PrimaryCommissionAmount
        } */
        writer.write(stringToFile)
        writer.newLine()
      }
    }
    print("Payment File Location: " + toFile.AbsolutePath)
    writer.flush()
    writer.close()
  }

  //Generate the reversals file

  public static function printReversalsToFile() {
    printReversalsToFile(null)
  }

  public static function printReversalsToFile(filePath: String) {
    if (filePath == null) filePath = DataLoaderUtil.getFullExportDatafilePath(DataLoaderUtil.EXPORT_PAYMENTREVERSALFILE)
    var payments = gw.api.database.Query.make(entity.PaymentMoneyReceived).and(\r -> {
      r.compare("ReversalReason", Relop.Equals, null)
      r.compare("ReversalDate", Relop.Equals, null)
      r.compare("MoneyBeingModified", Relop.Equals, null)
    }).select().orderBy(
      QuerySelectColumns.path(Paths.make(entity.PaymentMoneyReceived#ReceivedDate))
    )
    var toFile = new File(filePath)
    var writer = new BufferedWriter(new FileWriter(toFile))
    for (payment in payments) {
      if (payment.Amount.IsNotZero) {
        var sPaymentRefNumber: String
        if (payment.RefNumber != null) {
          sPaymentRefNumber = payment.RefNumber
        } else {
          sPaymentRefNumber = ""
        }
        var stringToFile = payment.ReceivedDate + "\t\"" + sPaymentRefNumber + "\"\t\"" + payment.MoneyReceivedTransaction.TransactionNumber + "\"\t\"modification\"\t" + payment.Amount
        writer.write(stringToFile)
        writer.newLine()
      }
    }
    print("Payment File Location: " + toFile.AbsolutePath)
    writer.flush()
    writer.close()
  }
}
