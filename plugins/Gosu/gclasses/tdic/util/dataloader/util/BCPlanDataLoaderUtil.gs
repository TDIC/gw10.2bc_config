package tdic.util.dataloader.util

uses gw.api.database.Relop

class BCPlanDataLoaderUtil extends DataLoaderUtil {
  construct() {
  }

  public static function findTAccountOwnerPatternByName(name: String): TAccountOwnerPattern {
    return (gw.api.database.Query.make(entity.TAccountOwnerPattern).compare("TAccountOwner", Relop.Equals, name).select().getFirstResult())
  }

  public static function findCommissionSubPlanByName(planName: String, subPlanName: String): CondCmsnSubPlan {
    var subPlanQuery = gw.api.database.Query.make(CondCmsnSubPlan)
    subPlanQuery.compare("Name", Equals, subPlanName)
    var commPlanQuery = subPlanQuery.join("CommissionPlan")
    commPlanQuery.compare("Name", Equals, planName)
    var resultSet = subPlanQuery.select().AtMostOneRow
    return resultSet
  }

  public static function printTypelistData() {
    printAggregationType()
    printPeriodicity()
    printPaymentScheduledAfter()
    printApplicableSegments()
    printDelinquencyReason()
    printDelinquencyTriggerBasis()
    printChargeCategory()
    printChargePriority()
  }

  public static function printAggregationType() {
    print("Aggregation Type")
    for (y in AggregationType.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printPeriodicity() {
    print("Periodicity")
    for (y in Periodicity.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printPaymentScheduledAfter() {
    print("Payment Scheduled After")
    for (y in PaymentScheduledAfter.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printApplicableSegments() {
    print("Applicable Segments")
    for (y in ApplicableSegments.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printDelinquencyReason() {
    print("Delinquency Reason")
    for (y in DelinquencyReason.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printDelinquencyTriggerBasis() {
    print("Delinquency Trigger Basis")
    for (y in DelinquencyTriggerBasis.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printChargeCategory() {
    print("Charge Category")
    for (y in ChargeCategory.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }

  public static function printChargePriority() {
    print("Charge Priority")
    for (y in ChargePriority.getTypeKeys(false)) {
      print(y.Code)
    }
    print("")
  }
}
