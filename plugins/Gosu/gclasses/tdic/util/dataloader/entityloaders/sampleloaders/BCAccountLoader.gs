package tdic.util.dataloader.entityloaders.sampleloaders

uses gw.api.util.CurrencyUtil
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCAccountInfo
uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.PCAccountInfo_InsuredContact
uses gw.webservice.policycenter.bc1000.entity.types.complex.PCNewAccountInfo
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.sampledata.AccountData
uses gw.command.demo.GeneralUtil
uses tdic.util.dataloader.util.BCSampleDataLoaderUtil
uses gw.transaction.Transaction
uses tdic.util.dataloader.data.sampledata.InvoiceStreamData
uses java.util.ArrayList
uses gw.api.domain.invoice.AnchorDate
uses gw.api.database.Query
uses java.lang.Exception
uses gw.api.database.InOperation
uses gw.api.database.Relop

class BCAccountLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }
  var _Account: Account as NewAccount
  var _bcContact: Contact as NewBCContact
  var _PossibleInvoiceStreams: ArrayList <InvoiceStreamData> as PossibleInvoiceStreams

  override function createEntity(applicationData: ApplicationData) {
    var accountData = applicationData as AccountData
    NewAccount = GeneralUtil.findAccountByName(accountData.AccountName)
    NewBCContact = convertContactToBC(accountData.Contact)

    // persists a BCContact if new; look OK in BC_Contact
    if (NewAccount == null) {
      var newAccountInfo = createAccountInfo(accountData)
      try {
        Transaction.runWithNewBundle(\bundle ->{
          var account = newAccountInfo.toNewAccount(CurrencyUtil.DefaultCurrency, bundle)
          initializeAccountDefaults(account, accountData)
          //set contact as primary payer
          account.Contacts.first().PrimaryPayer = true
          LOG.info("New Account: " + account.PublicID + " - " + " AccountNumber: " + accountData.AccountNumber)
          accountData.Skipped = false
        })
      } catch (e:Exception) {
        if (LOG.InfoEnabled){
          LOG.info("Account Load: " + accountData.AccountName + e.toString())
        }
        accountData.Error = true
      }
    } else {
      updateAccount(accountData)
    }
  }

  private function createAccountInfo(accountData : AccountData ) : PCNewAccountInfo {
    var accountInfo = new PCNewAccountInfo()
    accountInfo.AccountName = accountData.AccountName
    accountInfo.AccountNumber = accountData.AccountNumber
    accountInfo.InsuredContact = BCContactToPCAccountInfo_InsuredContact(NewBCContact)
    return accountInfo
  }

  private function initializeAccountDefaults(newAccount: Account, accountData : AccountData) {
    newAccount.DelinquencyPlan = BCSampleDataLoaderUtil.findDelinquencyPlanByName(accountData.DelinquencyPlan)
    newAccount.BillingLevel = typekey.BillingLevel.get(accountData.BillingLevel)
    newAccount.AccountType = accountData.AccountType
    newAccount.FEIN = accountData.FEIN
    newAccount.InvoiceDayOfMonth = accountData.InvoiceDOM
    newAccount.FirstTwicePerMonthInvoiceDayOfMonth = accountData.InvoiceTwiceFirst
    newAccount.SecondTwicePerMonthInvoiceDayOfMonth = accountData.InvoiceTwiceSecond
    newAccount.InvoiceDayOfWeek = typekey.DayOfWeek.get(accountData.InvoiceDayofWeek)
    newAccount.InvoiceDayOfMonth = accountData.InvoiceDOM
    newAccount.InvoiceDeliveryType = accountData.InvoiceDelivery
    if (accountData.PaymentMethod == TC_ACH or accountData.PaymentMethod == TC_CREDITCARD) {
      var paymentInstrument = new PaymentInstrument();
      paymentInstrument.Account = newAccount
      paymentInstrument.PaymentMethod = accountData.PaymentMethod
      paymentInstrument.Token = accountData.Token
      newAccount.DefaultPaymentInstrument = paymentInstrument
      //NewAccount.PaymentInstruments.add(paymentInst)
    }
    if (accountData.DueOrInvoiceInd == "Due") {
      newAccount.BillDateOrDueDateBilling = typekey.BillDateOrDueDateBilling.TC_DUEDATEBILLING
    }
    else {
      newAccount.BillDateOrDueDateBilling = typekey.BillDateOrDueDateBilling.TC_BILLDATEBILLING
    }
    // In the ListBill case, add PaymentPlan and InvoiceStream
    if (accountData.AccountType == typekey.AccountType.TC_LISTBILL) {
      if (accountData.PaymentPlan != null) {
        var newPaymentPlan = GeneralUtil.findPaymentPlanByName(accountData.PaymentPlan)
        if (newPaymentPlan != null) {
          var newPaymentPlans = com.google.common.collect.Lists.newArrayList < PaymentPlan >()
          newPaymentPlans.add(newPaymentPlan)
          newAccount.resyncPaymentPlans(newPaymentPlans)
        }
      }
      if (accountData.InvoiceStream != null) {
        // search among the records from the InvoiceStream spreadsheet tab for a match (invoiceStream.Name == accountData.InvoiceStream)
        if (PossibleInvoiceStreams != null) {
          var matchingInvoiceStream = PossibleInvoiceStreams.firstWhere(\i -> i.InvoiceStreamName == accountData.InvoiceStream)
          if (matchingInvoiceStream != null) {
            // add the invoice stream
            var newInvoiceStream = gw.api.domain.invoice.InvoiceStreamFactory.createInvoiceStreamFor(newAccount, matchingInvoiceStream.Periodicity)
            // Set the various attributes
            if (matchingInvoiceStream.DueOrInvoiceDay != null) {
              if (accountData.DueOrInvoiceInd.trim() != matchingInvoiceStream.DueOrInvoiceDayString.trim()) {
                newInvoiceStream.OverridingBillDateOrDueDateBilling = matchingInvoiceStream.DueOrInvoiceDay
              }
            }
            if (matchingInvoiceStream.LeadTime != null) {
              if (newAccount.BillingPlan.PaymentDueInterval != matchingInvoiceStream.LeadTime) {
                newInvoiceStream.OverridingLeadTimeDayCount = matchingInvoiceStream.LeadTime
              }
            }
            var savedPaymentPlan = newAccount.PaymentPlans.first()
            // there is only one since this the referenced account is newly saved.
            // if the periodicity of the account spreadsheet tab's differs from that of the matching invoice stream,
            // then we are setting all of the "override" fields on that stream (i.e. checkInequalities will be set to false) .
            var checkInequalities: Boolean = true
            if (savedPaymentPlan != null) {
              if (matchingInvoiceStream.Periodicity != savedPaymentPlan.Periodicity) {
                checkInequalities = false
              }
            }
            switch (matchingInvoiceStream.Periodicity) {
              case typekey.Periodicity.TC_TWICEPERMONTH:
                if (matchingInvoiceStream.TwiceAMonthFirstDay != null and matchingInvoiceStream.TwiceAMonthSecondDay != null) {
                  if (checkInequalities == false or (
                      (matchingInvoiceStream.TwiceAMonthFirstDay != accountData.InvoiceTwiceFirst or
                          matchingInvoiceStream.TwiceAMonthSecondDay != accountData.InvoiceTwiceSecond))) {
                    newInvoiceStream.OverridingAnchorDates = { AnchorDate.fromDayOfMonth(matchingInvoiceStream.TwiceAMonthFirstDay), AnchorDate.fromDayOfMonth(matchingInvoiceStream.TwiceAMonthSecondDay) }
                  }
                }
                break
              case typekey.Periodicity.TC_EVERYWEEK:
                if (matchingInvoiceStream.WeeklyDayOfWeek != null) {
                  if (checkInequalities == false or (
                      matchingInvoiceStream.WeeklyDayOfWeek != typekey.DayOfWeek.get(accountData.InvoiceDayofWeek))) {
                    newInvoiceStream.OverridingAnchorDates = { AnchorDate.fromDayOfWeek(matchingInvoiceStream.WeeklyDayOfWeek) }
                  }
                }
                break
              case typekey.Periodicity.TC_EVERYOTHERWEEK:
                if (matchingInvoiceStream.EveryOtherWeekAnchorDate != null) {
                  if (checkInequalities == false or (
                      matchingInvoiceStream.EveryOtherWeekAnchorDate != accountData.InvoiceWeekAnchorDate)) {
                    newInvoiceStream.OverridingAnchorDates = { AnchorDate.fromDate(matchingInvoiceStream.EveryOtherWeekAnchorDate) }
                  }
                }
                break
              default:
                if (matchingInvoiceStream.MonthlyDayOfMonth != null) {
                  if (checkInequalities == false or (
                      matchingInvoiceStream.MonthlyDayOfMonth != accountData.InvoiceDOM)) {
                    newInvoiceStream.OverridingAnchorDates = { AnchorDate.fromDayOfMonth(matchingInvoiceStream.MonthlyDayOfMonth) }
                  }
                }
                break
            }
            var createPaymentInstrument: Boolean = false
            if (matchingInvoiceStream.Token != null and matchingInvoiceStream.PaymentMethod != null) {
              if (newAccount.DefaultPaymentInstrument == null) {
                createPaymentInstrument = true
              } else {
                if (newAccount.DefaultPaymentInstrument.PaymentMethod != matchingInvoiceStream.PaymentMethod or
                    newAccount.DefaultPaymentInstrument.Token != matchingInvoiceStream.Token) {
                  createPaymentInstrument = true
                }
              }
            }
            if (createPaymentInstrument) {
              var paymentInstrument = new PaymentInstrument()
              paymentInstrument.Account = newAccount
              paymentInstrument.PaymentMethod = matchingInvoiceStream.PaymentMethod
              paymentInstrument.Token = matchingInvoiceStream.Token
              newInvoiceStream.OverridingPaymentInstrument = paymentInstrument
            }
          }
        }
      }
    }
  }

  private function updateAccount(accountData: AccountData) {
    try {
      Transaction.runWithNewBundle(\bundle ->
          {
            // Upon return from convertAccountContactToBC, if AccountContact found, won't be in bundle;  If new, will be in bundle
            var accountContact = convertAccountContactToBC(NewAccount.AccountName, accountData.Role, accountData.PrimaryPayer)
            if (!accountContact.New) {
              accountContact = bundle.add(accountContact)
              // add because not already there if found; reference swap
            }
            var anAccount: Account
            if (accountContact.New)
            {
              var accountID = NewAccount.PublicID
              anAccount = Query.make(Account).compare(Account#PublicID, Equals, accountID).select().AtMostOneRow
              //anAccount = Account(accountID)
              if (anAccount == null) {
                LOG.error("No Account found for PublicID; " + accountID)
              }
              anAccount = bundle.add(anAccount)
              // reference swap
              anAccount.addToContacts(accountContact)
            } else {
              accountData.Skipped = true
            }
            if (LOG.DebugEnabled) {
              LOG.debug("about to commit: " + bundle + "\n")
            }
            bundle.commit()
            accountData.Error = false
            if (LOG.DebugEnabled) {
              LOG.debug("committed: " + bundle + "\n")
            }
            LOG.info("Existing Account: " + anAccount + "\n" + " AccountContact: " + accountContact + " ID: " + accountContact.ID + "\n" + " InsuredAccount: " + accountContact.InsuredAccount + "\n")
          }
      )
    } catch (e:Exception) {
      accountData.Error = true
    }
  }

  private function convertAccountContactToBC(accountName: String, role: AccountRole, primaryPayer: Boolean): AccountContact
  {
    var accountContact = Query.make(entity.AccountContact).and(\r -> {
      r.compare(AccountContact#ID, NotEquals, null)
      r.subselect(AccountContact#Contact, CompareIn, Query.make(entity.Contact), Contact#ID).compare(Contact#ID, Equals, _bcContact.ID)
      r.subselect(AccountContact#Account, CompareIn, Query.make(entity.Account), Account#ID).compare(Account#AccountName, Equals, accountName)

    }).select().FirstResult
    if (accountContact == null) {
      //Build new AccountContact
      accountContact = new AccountContact()
      accountContact.Contact = _bcContact
      accountContact.PrimaryPayer = primaryPayer
      var AccountContactRole = new AccountContactRole()
      AccountContactRole.Role = role
      accountContact.addToRoles(AccountContactRole)
    }
    return accountContact
  }

  private function BCContactToPCAccountInfo_InsuredContact(c:Contact) : PCAccountInfo_InsuredContact {
    var pcic = new PCAccountInfo_InsuredContact()
    var pcContactInfo = BCContactToPCContactInfo(c)
    pcic.AddressBookUID = pcContactInfo.AddressBookUID
    pcic.ContactType = pcContactInfo.ContactType
    pcic.FirstName = pcContactInfo.FirstName
    pcic.LastName = pcContactInfo.LastName
    pcic.Name = pcContactInfo.Name
    pcic.EmailAddress1 = pcContactInfo.EmailAddress1
    pcic.WorkPhone = pcContactInfo.WorkPhone
    pcic.WorkPhoneCountry = pcContactInfo.WorkPhoneCountry
    pcic.WorkPhoneExtension = pcContactInfo.WorkPhoneExtension
    //pcic.Addresses = pcContactInfo.Addresses
    return pcic
  }

}
