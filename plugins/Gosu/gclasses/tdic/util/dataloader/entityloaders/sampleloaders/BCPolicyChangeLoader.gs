package tdic.util.dataloader.entityloaders.sampleloaders

uses gw.command.demo.GeneralUtil
uses gw.webservice.policycenter.bc1000.entity.types.complex.PolicyChangeInfo
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.sampledata.PolicyChangeData
uses gw.transaction.Transaction
uses java.util.ArrayList
uses java.lang.Exception
uses gw.api.database.Relop

class BCPolicyChangeLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aPolicyChangeData = applicationData as PolicyChangeData
    // check for existing PolicyPeriod
    var pp = GeneralUtil.findPolicyPeriod(aPolicyChangeData.AssociatedPolicyPeriod)
    if (pp != null) {
      aPolicyChangeData.TermNumber = pp.TermNumber
      var newPolicyChangeInfo = createPolicyChangeInfo(aPolicyChangeData, pp)
      if (aPolicyChangeData.Charges != null) {
        buildCharges(newPolicyChangeInfo, aPolicyChangeData)
      }
      else {
        if (LOG.DebugEnabled) {
          LOG.debug("null Charges")
        }
      }
      try {
        Transaction.runWithNewBundle(\bundle -> {
          if (LOG.DebugEnabled) {
            LOG.debug("about to call BillingAPI.sendBillingInstruction with Policy Change: " + newPolicyChangeInfo)
          }
          var bi = newPolicyChangeInfo.executePolicyChangeBI()
          LOG.info("New Policy Change: " + "\n" + " PolicyNumber: " + newPolicyChangeInfo.PolicyNumber + "\n")
          if (LOG.DebugEnabled) {
            LOG.debug("BillingInstruction: " + bi + " returned")
          }
          aPolicyChangeData.Skipped = false
        } )
      } catch (e: Exception) {
        if (LOG.InfoEnabled){
          LOG.info("Policy Change Load: " + aPolicyChangeData.AssociatedPolicyPeriod + e.toString())
        }
        aPolicyChangeData.Error = true
      }
    }
  }

  private function createPolicyChangeInfo(aPolicyChangeData: PolicyChangeData, pp: PolicyPeriod): PolicyChangeInfo {
    var aChange = new PolicyChangeInfo()
    aChange.PolicyNumber = aPolicyChangeData.AssociatedPolicyPeriod
    aChange.TermNumber = aPolicyChangeData.TermNumber
    aChange.Description = aPolicyChangeData.Description
    aChange.EffectiveDate = DateToXmlDate(aPolicyChangeData.ModificationDate)
    return aChange
  }

  private function buildCharges(info: PolicyChangeInfo, data:PolicyChangeData) {
    var d = DateToXmlDate(data.EntryDate)
    var chargesToProcess = data.Charges.where(\elt -> elt.ChargePattern != null and elt.ChargeAmount != null and elt.ChargeCurrency != null and elt.ChargeAmount != 0)
    chargesToProcess.each(\elt -> {
      info.addChargeInfo(ChargeDataToChargeInfo(elt, d))
    })
  }
}
