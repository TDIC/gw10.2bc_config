package tdic.util.dataloader.entityloaders.sampleloaders

uses gw.webservice.bc.bc1000.PaymentAPI
uses gw.webservice.bc.bc1000.PaymentInstrumentRecord
uses gw.webservice.bc.bc1000.PaymentReceiptRecord
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.sampledata.DirectPaymentData
uses gw.command.demo.GeneralUtil
uses gw.transaction.Transaction
uses java.lang.Exception

class BCDirectPaymentLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aDirectPayData = applicationData as DirectPaymentData
    try {
      Transaction.runWithNewBundle(\bundle ->
      {
        var paymentAPI = new PaymentAPI()
        var account = GeneralUtil.findAccountByName(aDirectPayData.AccountName)
        if (account != null) {
          if (LOG.DebugEnabled) {
            LOG.debug("Adding payment for account " + account.DisplayName)
          }
          var aPayment = new PaymentReceiptRecord()
          aPayment.AccountID = account.PublicID
          aPayment.PaymentReceiptType = PaymentReceiptRecord.paymentReceiptType.DIRECTBILLMONEYDETAILS
          aPayment.MonetaryAmount = aDirectPayData.Amount.ofDefaultCurrency()
          aPayment.Description = aDirectPayData.Description
          aPayment.ReceivedDate = aDirectPayData.ReceivedDate
          if (aDirectPayData.ReferenceNumber != null) {
            aPayment.RefNumber = aDirectPayData.ReferenceNumber
          }
          aPayment.PaymentInstrumentRecord = new PaymentInstrumentRecord();
          aPayment.PaymentInstrumentRecord.OneTime = true
          aPayment.PaymentInstrumentRecord.PaymentMethod = aDirectPayData.PaymentMethod
          if (aDirectPayData.PaymentToken != null) {
            aPayment.PaymentInstrumentRecord.Token = aDirectPayData.PaymentToken
          }
          for (pr in account.PaymentInstruments) {
            if (pr.PaymentMethod == aDirectPayData.PaymentMethod) {
              aPayment.PaymentInstrumentRecord.PublicID = pr.PublicID
              aPayment.PaymentInstrumentRecord.DisplayName = pr.DisplayName
              break;
            }
          }
          var pp: PolicyPeriod = null
          // Validate PolicyPeriod exists and is open
          if (aDirectPayData.PolicyNumber != null) {
            pp = GeneralUtil.findPolicyPeriod(aDirectPayData.PolicyNumber)
          }
          // If PolicyPeriod is valid
          if (pp != null) {
            if (LOG.DebugEnabled) {
              LOG.debug("Getting ready to make payment for PolicyPeriod " + pp.PublicID)
            }
            paymentAPI.makeDirectBillPaymentToPolicyPeriod(aPayment, pp.PublicID)
          } else {
            if (LOG.DebugEnabled) {
              LOG.debug("Getting ready to make payment onto Account " + account.PublicID)
            }
            paymentAPI.makeDirectBillPayment(aPayment)
          }
        } else {
          var suspensePayment = new PaymentReceiptRecord()
          suspensePayment.PaymentReceiptType = PaymentReceiptRecord.paymentReceiptType.SUSPENSEPAYMENT
          suspensePayment.MonetaryAmount = aDirectPayData.Amount.ofDefaultCurrency()
          suspensePayment.Description = aDirectPayData.Description
          suspensePayment.PaymentDate = aDirectPayData.ReceivedDate
          if (aDirectPayData.ReferenceNumber != null) {
            suspensePayment.RefNumber = aDirectPayData.ReferenceNumber
          }
          suspensePayment.PaymentInstrumentRecord = new PaymentInstrumentRecord();
          suspensePayment.PaymentInstrumentRecord.OneTime = true
          suspensePayment.PaymentInstrumentRecord.PaymentMethod = aDirectPayData.PaymentMethod
          if (LOG.DebugEnabled) {
            LOG.debug("Creating Suspense payment" + suspensePayment.MonetaryAmount + " on " + suspensePayment.PaymentDate)
          }
          paymentAPI.makeSuspensePayment(suspensePayment)
        }
        LOG.info("New Direct Payment: " + aDirectPayData.ReceivedDate + "\n" + " Account: " + aDirectPayData.AccountName + "\n")
        aDirectPayData.Skipped = false
      })
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("Payment Load: " + aDirectPayData.AccountName + e.toString())
      }
      aDirectPayData.Error = true
    }
  }
}
