package tdic.util.dataloader.entityloaders.sampleloaders

uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.PolicyChangeInfo_PrimaryNamedInsuredContact
uses gw.webservice.policycenter.bc1000.entity.types.complex.IssuePolicyInfo
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.sampledata.IssuanceData
uses gw.command.demo.GeneralUtil
uses tdic.util.dataloader.data.sampledata.PolicyPeriodData
uses tdic.util.dataloader.data.sampledata.InvoiceStreamData
uses gw.transaction.Transaction
uses java.util.ArrayList
uses gw.pl.persistence.core.Bundle
uses gw.api.web.policy.NewPolicyUtil
uses java.lang.Exception
uses gw.api.database.Query
uses gw.api.database.Relop

class BCIssuanceLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }
  var _BCContact: Contact as NewBCContact
  var aIssuanceData: IssuanceData
  var _PossibleInvoiceStreams: ArrayList <InvoiceStreamData> as PossibleInvoiceStreams
  override function createEntity(applicationData: ApplicationData) {
    aIssuanceData = applicationData as IssuanceData
    NewBCContact = convertContactToBC(aIssuanceData.IssuancePolicyPeriod.Contact)
    var thisAcct = GeneralUtil.findAccountByName(aIssuanceData.IssuancePolicyPeriod.AccountName)
    if(thisAcct == null) {
      if(LOG.InfoEnabled){
        LOG.info("Issuance Load - No Account exists for " + aIssuanceData.IssuancePolicyPeriod.AccountName)
      }
      aIssuanceData.Error = true
      return
    } else {
      if(LOG.DebugEnabled) {
        LOG.debug("Account: " + thisAcct.AccountNumber + " found with AccountName: " + thisAcct.AccountName)
      }
    }
    var pp = GeneralUtil.findPolicyPeriod(aIssuanceData.IssuancePolicyPeriod.PolicyNumber)
    // Skip existing Policy periods
    if(pp != null){
      return
    }
    var newIssuePolicyInfo = createIssuePolicyInfo(thisAcct, aIssuanceData)
    if (aIssuanceData.Charges != null) {
      buildCharges(newIssuePolicyInfo, aIssuanceData)
    } else {
      if(LOG.DebugEnabled) {
        LOG.debug("null Charges")
      }
    }
    try{
      Transaction.runWithNewBundle(\bundle -> {
        var bi = newIssuePolicyInfo.executeIssuanceBI()
        LOG.info("New Issuance: " + "\n" + " PolicyNumber: " + aIssuanceData.IssuancePolicyPeriod.PolicyNumber)
        if (LOG.DebugEnabled) {
          LOG.debug("BillingInstruction: " + bi + " returned")
        }
        aIssuanceData.Skipped = false
        aIssuanceData.IssuancePolicyPeriod.Skipped = false
      })
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("Issuance Load - " + aIssuanceData.IssuancePolicyPeriod.PolicyNumber, e)
      }
      aIssuanceData.Error = true
    }
  }

  private function BCContactToPrimaryNamedInsuredContact(c:Contact):PolicyChangeInfo_PrimaryNamedInsuredContact {
    var pcic = new PolicyChangeInfo_PrimaryNamedInsuredContact()
    pcic.$TypeInstance = BCContactToPCContactInfo(c)
    return pcic
  }

  private function createIssuePolicyInfo(acct:Account, issuanceData: IssuanceData): IssuePolicyInfo {
    var ppData = issuanceData.IssuancePolicyPeriod
    var ipi = new IssuePolicyInfo()
    ipi.AccountNumber = acct.AccountNumber
    ipi.Currency = (acct.Currency) as String
    ipi.PolicyNumber = ppData.PolicyNumber
    ipi.BillingMethodCode = (typekey.PolicyPeriodBillingMethod.get(ppData.BillingMethod)).Code
    ipi.ModelDate = DateToXmlDate(issuanceData.EntryDate)
    ipi.EffectiveDate = DateToXmlDate(ppData.PolicyPerEffDate)
    ipi.TermNumber = 1
    ipi.PeriodStart = DateToXmlDate(ppData.PolicyPerEffDate)
    ipi.PeriodEnd = DateToXmlDate(ppData.PolicyPerExpirDate)
    ipi.PaymentPlanPublicId = Query.make(entity.PaymentPlan).compare(PaymentPlan#Name, Equals, ppData.PaymentPlan).select().getAtMostOneRow()?.PublicID
    //ipi.JurisdictionCode = ppData.Jurisdiction?.Code
    ipi.ProductCode = ppData.LOBCode.Code
    ipi.Description = issuanceData.Description
    /*if (issuanceData.DepositRequirement != null)  {
      ipi.DepositRequirement = issuanceData.DepositRequirement + " " + ipi.Currency
    }*/
    if (ppData.ProducerCode != null) {
      var prodCode = Query.make(ProducerCode).compare(ProducerCode#Code, Equals, ppData.ProducerCode).select().FirstResult
      if(prodCode != null) {
        ipi.ProducerCodeOfRecordId = prodCode.PublicID
        if (LOG.DebugEnabled) {
          LOG.debug("Account: " + acct.AccountNumber + " added Primary Producer Code: " + prodCode)
        }
      } else {
        LOG.info("No Producer found for Producer Code " + ppData.ProducerCode)
      }
    }
    if (ppData.UnderAudit != null) {
      ipi.HasScheduledFinalAudit = ppData.UnderAudit
    }
    if (ppData.AssignedRisk != null) {
      ipi.AssignedRisk = ppData.AssignedRisk
    }
    if (ppData.UWCompany != null) {
      ipi.UWCompanyCode = ppData.UWCompany.Code
    }
    ipi.PrimaryNamedInsuredContact = BCContactToPrimaryNamedInsuredContact(NewBCContact)
    if(ppData.ListBillPayerAccount != null) {
      var lbpAccount = GeneralUtil.findAccountByName(ppData.ListBillPayerAccount)
      if (lbpAccount == null) {
        lbpAccount = GeneralUtil.findAccountByName(ppData.ListBillPayerAccount)
      }
      if (lbpAccount != null) {
        ipi.AltBillingAccountNumber = lbpAccount.AccountNumber
        if((ppData.BillingMethod == typekey.PolicyPeriodBillingMethod.TC_LISTBILL.Code)
            and ppData.ListBillPayerInvoiceStream != null) {
          ipi.InvoiceStreamId = lbpAccount.InvoiceStreams.first()?.PublicID
        }
      }
    }
    return ipi
  }

  private function buildCharges(ipi: IssuePolicyInfo, id:IssuanceData) {
    var d = DateToXmlDate(id.EntryDate)
    var chargesToProcess = id.Charges.where(\elt -> elt.ChargePattern != null and elt.ChargeAmount != null and elt.ChargeCurrency != null and elt.ChargeAmount != 0)
    chargesToProcess.each(\elt -> {
      ipi.addChargeInfo(ChargeDataToChargeInfo(elt, d))
    })
  }
}
