package tdic.util.dataloader.entityloaders.adminloaders

uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.admindata.AuthorityLimitData
uses tdic.util.dataloader.util.BCAdminDataLoaderUtil
uses gw.command.demo.GeneralUtil
uses java.lang.Exception

class BCAuthorityLimitLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aAuthorityLimitData = applicationData as AuthorityLimitData
    try {
      var currAuthorityLimit = BCAdminDataLoaderUtil.findAuthorityLimitByPublicID(aAuthorityLimitData.PublicID)
      // add or update if Exclude is false
      if (aAuthorityLimitData.Exclude != true) {
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          var aAuthorityLimit: AuthorityLimit
          // If the public id is not found then create a new authority limit
          if (currAuthorityLimit == null) {
            aAuthorityLimit = new AuthorityLimit()
          } else {
            // If the public id is found then update the existing authority limit
            aAuthorityLimit = bundle.add(currAuthorityLimit)
          }
          aAuthorityLimit.Profile = GeneralUtil.findAuthorityLimitProfileByPublicId(aAuthorityLimitData.Profile.PublicID)
          aAuthorityLimit.PublicID = aAuthorityLimitData.PublicID
          aAuthorityLimit.LimitType = aAuthorityLimitData.LimitType
          aAuthorityLimit.LimitAmount = aAuthorityLimitData.LimitAmount.ofDefaultCurrency()
          aAuthorityLimitData.Skipped = false
          if (!aAuthorityLimit.New) {
            if (aAuthorityLimit.getChangedFields().Count > 0) {
              aAuthorityLimitData.Updated = true
            } else {
              aAuthorityLimitData.Unchanged = true
            }
          }
          bundle.commit()
        }, User.util.UnrestrictedUser)
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("Authority Limit Load: " + aAuthorityLimitData.PublicID + " " + e.toString())
      }
      aAuthorityLimitData.Error = true
    }
  }
}