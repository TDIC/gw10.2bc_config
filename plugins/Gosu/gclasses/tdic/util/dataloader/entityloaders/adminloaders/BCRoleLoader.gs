package tdic.util.dataloader.entityloaders.adminloaders

uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.admindata.RoleData
uses gw.command.demo.GeneralUtil
uses java.util.ArrayList
uses tdic.util.dataloader.util.BCAdminDataLoaderUtil
uses java.lang.Exception

class BCRoleLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  private function generatePublicID(role: Role, permission: SystemPermissionType): String {
    var result = role.PublicID + ":" + permission.Ordinal
    return result
  }

  override function createEntity(applicationData: ApplicationData) {
    var aRoleData = applicationData as RoleData
    try {
      var currRole = GeneralUtil.findRoleByPublicId(aRoleData.PublicID)
      // add or update if Exclude is false
      if (aRoleData.Exclude != true) {
        if (currRole == null) {
          // add if not on file
          gw.transaction.Transaction.runWithNewBundle(\bundle -> {
            var aRole = new Role()
            aRole.PublicID = aRoleData.PublicID
            aRole.Name = aRoleData.Name
            aRole.Description = aRoleData.Description
            for (permission in aRoleData.Permissions) {
              var rolePrivilege = new RolePrivilege()
              // define PublicID that is unique for this Role and permission
              rolePrivilege.PublicID = generatePublicID(aRole, permission as SystemPermissionType)
              rolePrivilege.Permission = permission as SystemPermissionType
              aRole.addToPrivileges(rolePrivilege)
            }
            aRole.RoleType = TC_USER
            aRoleData.Skipped = false
            bundle.commit()
          }, User.util.UnrestrictedUser)
          aRoleData.Skipped = false
        } else {
          gw.transaction.Transaction.runWithNewBundle(\bundle -> {
            var aRole = bundle.add(currRole)
            aRole.Name = aRoleData.Name
            aRole.Description = aRoleData.Description
            var rolePermissions = new ArrayList <SystemPermissionType> ()
            var currRolePermissions = new ArrayList <SystemPermissionType> ()
            for (permission in aRoleData.Permissions) {
              rolePermissions.add(permission as SystemPermissionType)
            }
            currRolePermissions.addAll(aRole.Privileges*.Permission?.toList() as ArrayList <SystemPermissionType>)
            var addedPermissions = rolePermissions.subtract(currRolePermissions)
            var removedPermissions = currRolePermissions.subtract(rolePermissions)
            for (addPermission in addedPermissions) {
              // define PublicID that is unique for this Role and permission
              var publicID = generatePublicID(aRole, addPermission)
              // check if there is already an older entry in the DB with this publicID
              var rolePrivilege = BCAdminDataLoaderUtil.findRolePrivilegeByPublicID(publicID)
              if (rolePrivilege == null) {
                // create new
                rolePrivilege = new RolePrivilege()
                rolePrivilege.PublicID = publicID
              } else {
                // update entry
                rolePrivilege = bundle.add(rolePrivilege)
              }
              rolePrivilege.Permission = addPermission
              aRole.addToPrivileges(rolePrivilege)
            }
            for (remPermission in removedPermissions) {
              var rolePrivilage = aRole.Privileges.firstWhere(\r -> r.Permission == remPermission)
              aRole.removeFromPrivileges(rolePrivilage)
            }
            aRoleData.Skipped = false
            if (aRole.getChangedFields().Count > 0) {
              aRoleData.Updated = true
            } else {
              aRoleData.Unchanged = true
            }
            bundle.commit()
          }, User.util.UnrestrictedUser)
        }
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("Role Load: " + aRoleData.PublicID + e.toString())
      }
      aRoleData.Error = true
    }
  }
}
