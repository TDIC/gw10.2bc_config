package tdic.util.dataloader.entityloaders.adminloaders

uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.util.BCAdminDataLoaderUtil
uses tdic.util.dataloader.data.admindata.HolidayData
uses java.lang.Exception

class BCHolidayLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aHolidayData = applicationData as HolidayData
    try {
      var currHoliday = BCAdminDataLoaderUtil.findHolidayByPublicID(aHolidayData.PublicID)
      // add or update if Exclude is false
      if (aHolidayData.Exclude != true){
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          var aHoliday: Holiday
          // If the public id is not found then create a new holiday
          if (currHoliday == null) {
            aHoliday = new Holiday()
          } else {
            // If the public id is found then update the existing holiday
            aHoliday = bundle.add(currHoliday)
          }
          aHoliday.PublicID = aHolidayData.PublicID
          aHoliday.Name = aHolidayData.Name
          aHoliday.OccurrenceDate = aHolidayData.OccurrenceDate
          aHoliday.AppliesToAllZones = aHolidayData.AppliesToAllZones
          aHolidayData.Skipped = false
          if (!aHoliday.New) {
            if (aHoliday.getChangedFields().Count > 0) {
              aHolidayData.Updated = true
            } else {
              aHolidayData.Unchanged = true
            }
          }
          bundle.commit()
        }, User.util.UnrestrictedUser)
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("Holiday Load: " + aHolidayData.Name + " " + e.toString())
      }
      aHolidayData.Error = true
    }
  }
}