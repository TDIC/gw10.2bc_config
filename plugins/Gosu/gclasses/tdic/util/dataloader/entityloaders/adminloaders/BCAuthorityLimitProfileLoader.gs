package tdic.util.dataloader.entityloaders.adminloaders

uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.admindata.AuthorityLimitProfileData
uses gw.command.demo.GeneralUtil
uses java.lang.Exception

class BCAuthorityLimitProfileLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aAuthorityLimitProfileData = applicationData as AuthorityLimitProfileData
    try {
      var currAuthorityLimitProfile = GeneralUtil.findAuthorityLimitProfileByPublicId(aAuthorityLimitProfileData.PublicID)
      // add or update if Exclude is false
      if (aAuthorityLimitProfileData.Exclude != true) {
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          // If the public id is not found then create a new authority limit profile
          var aAuthorityLimitProfile: AuthorityLimitProfile
          if (currAuthorityLimitProfile == null) {
            aAuthorityLimitProfile = new AuthorityLimitProfile()
          } else {
            // If the public id is found then create update the existing profile
            aAuthorityLimitProfile = bundle.add(currAuthorityLimitProfile)
          }
          aAuthorityLimitProfile.PublicID = aAuthorityLimitProfileData.PublicID
          aAuthorityLimitProfile.Name = aAuthorityLimitProfileData.Name
          aAuthorityLimitProfile.Description = aAuthorityLimitProfileData.Description
          aAuthorityLimitProfileData.Skipped = false
          if (!aAuthorityLimitProfile.New) {
            if (aAuthorityLimitProfile.getChangedFields().Count > 0) {
              aAuthorityLimitProfileData.Updated = true
            } else {
              aAuthorityLimitProfileData.Unchanged = true
            }
          }
          bundle.commit()
        }, User.util.UnrestrictedUser)
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("Authority Limit Profile Load: " + aAuthorityLimitProfileData.Name + " " + e.toString())
      }
      aAuthorityLimitProfileData.Error = true
    }
  }
}