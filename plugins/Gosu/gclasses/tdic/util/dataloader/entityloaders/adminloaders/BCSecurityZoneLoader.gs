package tdic.util.dataloader.entityloaders.adminloaders

uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.admindata.SecurityZoneData
uses tdic.util.dataloader.util.BCAdminDataLoaderUtil
uses java.lang.Exception

class BCSecurityZoneLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aSecurityZoneData = applicationData as SecurityZoneData
    try {
      var currSecurityZone = BCAdminDataLoaderUtil.findSecurityZoneByPublicID(aSecurityZoneData.PublicID)
      // add or update if Exclude is false
      if (aSecurityZoneData.Exclude != true){
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          var aSecurityZone: SecurityZone
          if (currSecurityZone == null) {
            // add if not on file and
            aSecurityZone = new SecurityZone()
            aSecurityZone.PublicID = aSecurityZoneData.PublicID
          } else {
            // update if on file
            aSecurityZone = bundle.add(currSecurityZone)
          }
          aSecurityZone.Name = aSecurityZoneData.Name
          aSecurityZone.Description = aSecurityZoneData.Description
          aSecurityZoneData.Skipped = false
          if (!aSecurityZone.New) {
            if (aSecurityZone.getChangedFields().Count > 0) {
              aSecurityZoneData.Updated = true
            } else {
              aSecurityZoneData.Unchanged = true
            }
          }
          bundle.commit()
        }, User.util.UnrestrictedUser)
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("Security Zone Load: " + aSecurityZoneData.Name + " " + e.toString())
      }
      aSecurityZoneData.Error = true
    }
  }
}