package tdic.util.dataloader.entityloaders.planloaders

uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.plandata.DelinquencyPlanData
uses gw.command.demo.GeneralUtil
uses java.lang.Exception

class BCDelinquencyPlanLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aDelinquencyPlanData = applicationData as DelinquencyPlanData
    try {
      var currDelinquencyPlan = GeneralUtil.findDelinquencyPlanByPublicId(aDelinquencyPlanData.PublicID)
      // add or update if Exclude is false
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        var aDelinquencyPlan: DelinquencyPlan
        if (currDelinquencyPlan == null) {
          // create new
          aDelinquencyPlan = new DelinquencyPlan()
        } else {
          // update existing
          aDelinquencyPlan = bundle.add(currDelinquencyPlan)
        }
        aDelinquencyPlan.PublicID = aDelinquencyPlanData.PublicID
        aDelinquencyPlan.Name = aDelinquencyPlanData.Name
        aDelinquencyPlan.EffectiveDate = aDelinquencyPlanData.EffectiveDate
        aDelinquencyPlan.ExpirationDate = aDelinquencyPlanData.ExpirationDate
        //aDelinquencyPlan.Currencies
        aDelinquencyPlan.CancellationTarget = aDelinquencyPlanData.CancellationTarget
        aDelinquencyPlan.HoldInvoicingOnDlnqPolicies = aDelinquencyPlanData.HoldInvoicingOnTargetedPolicies
        aDelinquencyPlan.GracePeriodDays = aDelinquencyPlanData.GracePeriodDays
        //aDelinquencyPlan.LateFeeAmount = aDelinquencyPlanData.LateFee.ofDefaultCurrency()
        //aDelinquencyPlan.ReinstatementFeeAmount = aDelinquencyPlanData.ReinstatementFee.ofDefaultCurrency()
        aDelinquencyPlan.WriteoffThreshold = aDelinquencyPlanData.WriteoffThreshold.ofDefaultCurrency()
        aDelinquencyPlan.AcctEnterDelinquencyThreshold = aDelinquencyPlanData.EnterDelinquencyThreshold_Account.ofDefaultCurrency()
        aDelinquencyPlan.PolEnterDelinquencyThreshold = aDelinquencyPlanData.EnterDelinquencyThreshold_Policy.ofDefaultCurrency()
        aDelinquencyPlan.CancellationThreshold = aDelinquencyPlanData.CancelPolicy.ofDefaultCurrency()
        aDelinquencyPlan.ExitDelinquencyThreshold = aDelinquencyPlanData.ExitDelinquency.ofDefaultCurrency()
        aDelinquencyPlan.ApplicableSegments = aDelinquencyPlanData.ApplicableSegments
        aDelinquencyPlanData.Skipped = false
        if (!aDelinquencyPlan.New) {
          if (aDelinquencyPlan.getChangedFields().Count > 0) {
            aDelinquencyPlanData.Updated = true
          } else {
            aDelinquencyPlanData.Unchanged = true
          }
        }
        bundle.commit()
      })
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("DelinquencyPlan Load: " + aDelinquencyPlanData.PublicID + e.toString())
      }
      aDelinquencyPlanData.Error = true
    }
  }
}