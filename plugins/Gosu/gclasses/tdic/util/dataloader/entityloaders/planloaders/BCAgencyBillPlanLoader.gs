package tdic.util.dataloader.entityloaders.planloaders

uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.plandata.AgencyBillPlanData
uses gw.command.demo.GeneralUtil
uses java.lang.Exception

class BCAgencyBillPlanLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var anAgencyBillPlanData = applicationData as AgencyBillPlanData
    try {
      var currAgcyBillingPlan = GeneralUtil.findAgencyBillPlanByPublicId(anAgencyBillPlanData.PublicID)
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        var anAgencyBillPlan: AgencyBillPlan
        if (currAgcyBillingPlan == null) {
          // create new
          anAgencyBillPlan = new AgencyBillPlan()
        } else {
          // update existing
          anAgencyBillPlan = bundle.add(currAgcyBillingPlan)
        }
        anAgencyBillPlan.PublicID = anAgencyBillPlanData.PublicID
        anAgencyBillPlan.Name = anAgencyBillPlanData.Name
        //anAgencyBillPlan.Currencies
        anAgencyBillPlan.WorkflowPlan = typekey.Workflow.get(anAgencyBillPlanData.WorkflowType)
        anAgencyBillPlan.EffectiveDate = anAgencyBillPlanData.EffectiveDate
        anAgencyBillPlan.ExpirationDate = anAgencyBillPlanData.ExpirationDate
        anAgencyBillPlan.CycleCloseDayOfMonthLogic = typekey.DayOfMonthLogic.get(anAgencyBillPlanData.CycleCloseDayOfMonthLogic)
        anAgencyBillPlan.CycleCloseDayOfMonth = anAgencyBillPlanData.CycleCloseDayOfMonth
        anAgencyBillPlan.PaymentTermsInDays = anAgencyBillPlanData.PaymentTermsInDays
        anAgencyBillPlan.ExceptionForPastDueStatement = anAgencyBillPlanData.ExceptionForPastDueStatement
        anAgencyBillPlan.StatementSentAfterCycleClose = anAgencyBillPlanData.StatementSentAfterCycleClose
        anAgencyBillPlan.DaysAfterCycleCloseToSendStmnt = anAgencyBillPlanData.DaysAfterCycleCloseToSendStmnt
        anAgencyBillPlan.SnapshotNonPastDueItems = anAgencyBillPlanData.SnapshotNonPastDueItems
        anAgencyBillPlan.StatementsWithLowNetSuppressed = anAgencyBillPlanData.StatementsWithLowNetSuppressed
        anAgencyBillPlan.NetThresholdForSuppressingStmt = anAgencyBillPlanData.NetThresholdForSuppressingStmt.ofDefaultCurrency()
        anAgencyBillPlan.ReminderSentIfPromiseNotRcvd = anAgencyBillPlanData.ReminderSentIfPromiseNotRcvd
        anAgencyBillPlan.DaysUntilPromiseReminderSent = anAgencyBillPlanData.DaysUntilPromiseReminderSent
        anAgencyBillPlan.PromiseDueInDays = anAgencyBillPlanData.PromiseDueInDays
        anAgencyBillPlan.ExceptionIfPromiseNotReceived = anAgencyBillPlanData.ExceptionIfPromiseNotReceived
        anAgencyBillPlan.PromiseExceptionsSent = anAgencyBillPlanData.PromiseExceptionsSent
        anAgencyBillPlan.AutoProcessWhenPaymentMatches = anAgencyBillPlanData.AutoProcessWhenPaymentMatches
        anAgencyBillPlan.PaymentExceptionsSent = anAgencyBillPlanData.PaymentExceptionsSent
        anAgencyBillPlan.FirstDunningSentIfNotPaid = anAgencyBillPlanData.FirstDunningSentIfNotPaid
        anAgencyBillPlan.DaysUntilFirstDunningSent = anAgencyBillPlanData.DaysUntilFirstDunningSent
        anAgencyBillPlan.SecondDunningSentIfNotPaid = anAgencyBillPlanData.SecondDunningSentIfNotPaid
        anAgencyBillPlan.DaysUntilSecondDunningSent = anAgencyBillPlanData.DaysUntilSecondDunningSent
        anAgencyBillPlan.ProducerWriteoffThreshold = anAgencyBillPlanData.ProducerWriteoffThreshold.ofDefaultCurrency()
        anAgencyBillPlan.LowCommissionCleared = anAgencyBillPlanData.LowCommissionCleared
        anAgencyBillPlan.ClearCommissionThreshold = anAgencyBillPlanData.ClearCommissionThreshold.ofDefaultCurrency()
        anAgencyBillPlan.LowGrossCleared = anAgencyBillPlanData.LowGrossCleared
        anAgencyBillPlan.ClearGrossThreshold = anAgencyBillPlanData.ClearGrossThreshold.ofDefaultCurrency()
        anAgencyBillPlan.ClearingLogicTarget = typekey.ClearingLogicTarget.get(anAgencyBillPlanData.ClearingLogicTarget)
        anAgencyBillPlan.CreateOffsetsOnBilledInvoices = anAgencyBillPlanData.CreateOffsetsOnBilledInvoices
        anAgencyBillPlan.PmntSchdChngOffsetsOnBilled = anAgencyBillPlanData.PmntSchdChngOffsetsOnBilled
        anAgencyBillPlanData.Skipped = false
        if (!anAgencyBillPlan.New) {
          if (anAgencyBillPlan.getChangedFields().Count > 0) {
            anAgencyBillPlanData.Updated = true
          } else {
            anAgencyBillPlanData.Unchanged = true
          }
        }
        bundle.commit()
      })
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("AgencyBillPlan Load: " + anAgencyBillPlanData.PublicID + e.toString())
      }
      anAgencyBillPlanData.Error = true
    }
  }
}