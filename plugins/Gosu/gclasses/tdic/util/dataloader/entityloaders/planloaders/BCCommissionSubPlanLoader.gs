package tdic.util.dataloader.entityloaders.planloaders

uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.plandata.CommissionSubPlanData
uses tdic.util.dataloader.util.BCPlanDataLoaderUtil
uses gw.command.demo.GeneralUtil
uses java.util.ArrayList
uses java.lang.Exception

class BCCommissionSubPlanLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aSubPlanData = applicationData as CommissionSubPlanData
    try {
      var currCommissionPlan = GeneralUtil.findCommissionPlanByName(aSubPlanData.PlanName)
      var currCommissionSubPlan = BCPlanDataLoaderUtil.findCommissionSubPlanByName(aSubPlanData.PlanName,
          aSubPlanData.SubPlanName)
      // add or update if commission plan exists (these are loaded before sub plans)
      if (currCommissionPlan != null) {
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          bundle.add(currCommissionPlan)
          var aSubPlan: CondCmsnSubPlan
          if (currCommissionSubPlan == null) {
            // // create new CondCmsnSubPlan entity attached to Commission Plan
            aSubPlan = currCommissionPlan.addSubPlan()
          }
          else {
            // update existing
            aSubPlan = bundle.add(currCommissionSubPlan)
          }
          aSubPlan.Name = aSubPlanData.SubPlanName
          // aCommissionSubPlan.Priority = aCommissionSubPlanData.Priority  -- virtual, cannot be set
          aSubPlan.PayableCriteria = typekey.PayableCriteria.get(aSubPlanData.Earn_Commissions)
          aSubPlan.SuspendForDelinquency = aSubPlanData.Suspend_for_Delinquency
          aSubPlan.AssignedRisk = aSubPlanData.AssignedRisk
          aSubPlan.AllEvaluations = aSubPlanData.AllEvaluations
          aSubPlan.AllLOBCodes = aSubPlanData.AllLOBCodes
          aSubPlan.LOBCodes[0].Selected = aSubPlanData.LOBWorkersComp
          aSubPlan.LOBCodes[1].Selected = aSubPlanData.LOBCommercialProperty
          aSubPlan.LOBCodes[2].Selected = aSubPlanData.LOBInlandMarine
          aSubPlan.LOBCodes[3].Selected = aSubPlanData.LOBGeneralLiability
          aSubPlan.LOBCodes[4].Selected = aSubPlanData.LOBBusinessAuto
          aSubPlan.LOBCodes[5].Selected = aSubPlanData.LOBPersonalAuto
          aSubPlan.LOBCodes[6].Selected = aSubPlanData.LOBBusinessOwners
          aSubPlan.AllSegments = aSubPlanData.AllSegments
          aSubPlan.SelectedSegments[0].Selected = aSubPlanData.SegmentLargeBusiness
          aSubPlan.SelectedSegments[1].Selected = aSubPlanData.SegmentMediumBusiness
          aSubPlan.SelectedSegments[2].Selected = aSubPlanData.SegmentPersonal
          aSubPlan.SelectedSegments[3].Selected = aSubPlanData.SegmentSmallBusiness
          aSubPlan.SelectedSegments[4].Selected = aSubPlanData.SegmentSubprime
          aSubPlan.AllJurisdictions = aSubPlanData.AllJurisdictions
          aSubPlan.AllTerms = aSubPlanData.AllTerms
          aSubPlan.getRenewalRange(0, 1).Covered = aSubPlanData.TermsInitialBusiness
          aSubPlan.getRenewalRange(1, 2).Covered = aSubPlanData.TermsFirstRenewal
          aSubPlan.getRenewalRange(2, 3).Covered = aSubPlanData.TermsSecondRenewal
          aSubPlan.getRenewalRange(3, 4).Covered = aSubPlanData.TermsThirdRenewal
          aSubPlan.getRenewalRangeOnOrAfter(4).Covered = aSubPlanData.TermsThereafter
          aSubPlan.AllUWCompanies = aSubPlanData.AllUWCompanies
          // Default Rates
          // set/reset rate for all roles, not just the defined ones (needed for update case)
          for (role in {"primary", "secondary", "referrer"}) {
            var rateData = aSubPlanData.SubPlanRates.firstWhere(\c -> c.Role == role)
            var rate = (rateData != null) ? rateData.Rate : 0
            if (rate != null)
                aSubPlan.setBaseRate(typekey.PolicyRole.get(role), rate)
          }
          // Commmissionable Items
          var newItems = new ArrayList <ChargePattern> ()
          for (aItem in aSubPlanData.CommissionItems) {
            var chrgItem = GeneralUtil.findChargePatternByChargeCode(aItem.ChargePattern)
            newItems.add(chrgItem)
          }
          var existingItems = new ArrayList <ChargePattern> ()
          existingItems.addAll(aSubPlan.CommissionableChargeItems*.ChargePattern?.toList() as ArrayList <ChargePattern>)
          var addedItems = newItems.subtract(existingItems)
          var removedItems = existingItems.subtract(newItems)
          // Add any new charge items to the subplan
          for (addItem in addedItems.where(\c -> c != null)) {
            var commChrgItem = new CommissionableChargeItem(bundle)
            commChrgItem.ChargePattern = addItem
            aSubPlan.addToCommissionableChargeItems(commChrgItem)
          }
          // Remove deleted charge items from subplan
          for (remItem in removedItems) {
            var commChrgItem = aSubPlan.CommissionableChargeItems.firstWhere(\c -> c.ChargePattern == remItem)
            aSubPlan.removeFromCommissionableChargeItems(commChrgItem)
          }
          if (!aSubPlan.New) {
            if (aSubPlan.getChangedFields().Count > 0) {
              aSubPlanData.Updated = true
            } else {
              aSubPlanData.Unchanged = true
            }
          }
          aSubPlanData.Skipped = false
        })
        aSubPlanData.Skipped = false
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("CommissionSubPlan Load: " + aSubPlanData.PlanName + " " + aSubPlanData.SubPlanName + e.toString())
      }
      aSubPlanData.Error = true
    }
  }
}