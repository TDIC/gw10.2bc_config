package tdic.util.dataloader.entityloaders.planloaders

uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.plandata.PaymentAllocationPlanData
uses java.lang.Exception

class PaymentAllocationPlanLoader extends BCLoader implements EntityLoaderInterface {
  override function createEntity(applicationData: ApplicationData) {
  /* For a new Payment Allocation Plan, first need to add the plan -- default Filters and Pay Order Items are added automatically.
     Delete the defaults and update with the spreadsheet values.
   */

    var aPaymentAllocationPlanData = applicationData as PaymentAllocationPlanData
    try {
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        var payPlan = PaymentAllocationPlan.finder.findActivePlanByExactName(PaymentAllocationPlan, aPaymentAllocationPlanData.Name) as PaymentAllocationPlan
        if (payPlan != null) {
          payPlan = bundle.add(payPlan)
          aPaymentAllocationPlanData.Updated = true
        } else {
          payPlan = new PaymentAllocationPlan()
          payPlan.PublicID = aPaymentAllocationPlanData.PublicID
          payPlan.Name = aPaymentAllocationPlanData.Name
          payPlan.Description = aPaymentAllocationPlanData.Description
          payPlan.EffectiveDate = aPaymentAllocationPlanData.EffectiveDate
          payPlan.ExpirationDate = aPaymentAllocationPlanData.ExpirationDate
          aPaymentAllocationPlanData.Skipped = false
        }
        //remove all default or existing filters and pay order items
        for (filter in payPlan.DistributionFilters) {
          payPlan.removeDistributionCriteriaFilter(filter)
        }
        for (payOrder in payPlan.InvoiceItemOrderings)  {
          payPlan.removeInvoiceItemOrderingFor(payOrder)
        }

        //from non-blank spreadsheet columns, add filters and pay order items
        for (filter in aPaymentAllocationPlanData.Filters) {
          if (filter != null) {
            payPlan.addDistributionCriteriaFilter(filter)
          }
        }
        for (payOrder in aPaymentAllocationPlanData.PayOrderTypes) {
          if (payOrder != null) {
            payPlan.addInvoiceItemOrderingFor(payOrder)
          }
        }

        bundle.commit()
      })
    } catch (ex: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("PaymentAllocationLoad Load: " + aPaymentAllocationPlanData.PublicID + ex.toString())
      }
      aPaymentAllocationPlanData.Error = true
    }
  }
}