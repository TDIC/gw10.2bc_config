package tdic.util.dataloader.entityloaders.planloaders

uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.plandata.ReturnPremiumSchemeData
uses java.lang.Exception

class ReturnPremiumSchemeLoader extends BCLoader implements EntityLoaderInterface {
  override function createEntity(applicationData: ApplicationData) {
    var aReturnSchemeData = applicationData as ReturnPremiumSchemeData
    try {
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        var existingPlan = ReturnPremiumPlan.finder.findActivePlanByExactName(ReturnPremiumPlan, aReturnSchemeData.ReturnPremiumPlanName) as ReturnPremiumPlan
        if (existingPlan != null) {
          existingPlan = bundle.add(existingPlan)
          var existingScheme = existingPlan.ReturnPremiumHandlingSchemes?.where(\elt -> elt.HandlingCondition == aReturnSchemeData.HandlingCondition).first()
          if (existingScheme != null) {
            existingScheme.StartDateOption = aReturnSchemeData.StartDateOption
            existingScheme.AllocateMethod = aReturnSchemeData.AllocateMethod
            existingScheme.AllocateTiming = aReturnSchemeData.AllocateTiming
            existingScheme.ExcessTreatment = aReturnSchemeData.ExcessTreatment
            aReturnSchemeData.Updated = true
          } else {
            var newScheme = existingPlan.addHandlingSchemeFor(aReturnSchemeData.HandlingCondition)
            newScheme.StartDateOption = aReturnSchemeData.StartDateOption
            newScheme.AllocateMethod = aReturnSchemeData.AllocateMethod
            newScheme.AllocateTiming = aReturnSchemeData.AllocateTiming
            newScheme.ExcessTreatment = aReturnSchemeData.ExcessTreatment
            aReturnSchemeData.Skipped = false
          }
        } else {
          LOG.error("NO RETURN PREMIUM PLAN EXISTS WITH THIS NAME: " + aReturnSchemeData.ReturnPremiumPlanName)
          aReturnSchemeData.Error = true
        }
        bundle.commit()
      }, User.util.UnrestrictedUser)
    } catch (ex: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("ReturnPremiumLoad Load: " + aReturnSchemeData.PublicID + ex.toString())
      }
      aReturnSchemeData.Error = true
    }
  }
}