package tdic.util.dataloader.entityloaders.planloaders

uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.plandata.GLTransactionFilterData
uses java.lang.IllegalArgumentException
uses gw.command.demo.GeneralUtil
uses java.lang.Exception

/**
 * US570 - General Ledger Integration
 * 11/14/2014 Alvin Lee
 *
 * Excel Data Loader Additions for custom GL Integration entities.
 * Class to load the GL Filters. It will check the existing GL Filters prior to creating a new one.
 */
class BCGLTransactionFilterLoader extends BCLoader implements EntityLoaderInterface {

  /**
   * Standard constructor
   */
  construct() {
  }

  /**
   * This will create the new GLIntegrationTransactionFilter_TDIC entity and commit the bundle
   * if it does not already exist.
   * 
   * @param applicationData A GLTransactionFilterData object containing the data for the GL Filter
   */
  override function createEntity(applicationData : ApplicationData) {
    var aGLFilterData = applicationData as GLTransactionFilterData
    try {
      if (aGLFilterData.TransactionType == null) {
        throw new IllegalArgumentException("Transaction Type is null")
      }
      var currFilter = GeneralUtil.findGLTransactionFilterByTransactionType(aGLFilterData.TransactionType)
      gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
        var aGLFilter : GLIntegrationTransactionFilter_TDIC
        if (currFilter == null) { // create new
          aGLFilter = new GLIntegrationTransactionFilter_TDIC()
        } else { // update existing
          aGLFilter = bundle.add(currFilter)
        }
        aGLFilter.TransactionType = aGLFilterData.TransactionType
        bundle.commit()
        aGLFilterData.Skipped = false
      })
    } catch(e : Exception) {
      LOG.warn("GLTransactionFilter Load Failed: " + aGLFilterData.TransactionType + e.toString())
      aGLFilterData.Error = true
    }
  }

}