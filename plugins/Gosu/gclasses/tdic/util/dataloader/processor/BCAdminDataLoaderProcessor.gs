package tdic.util.dataloader.processor

uses java.util.ArrayList
uses tdic.util.dataloader.data.admindata.ActivityPatternData
uses tdic.util.dataloader.data.AddressData
uses tdic.util.dataloader.data.admindata.AdminLocalizationData
uses tdic.util.dataloader.data.admindata.AuthorityLimitData
uses tdic.util.dataloader.data.admindata.AuthorityLimitProfileData
uses tdic.util.dataloader.data.admindata.CollectionAgencyData
uses tdic.util.dataloader.data.admindata.UserAuthorityLimitData
uses tdic.util.dataloader.data.admindata.AuthorityLimitTypeData
uses tdic.util.dataloader.data.admindata.GroupData
uses tdic.util.dataloader.data.admindata.RegionData
uses tdic.util.dataloader.data.admindata.RoleData
uses tdic.util.dataloader.data.admindata.SecurityZoneData
uses tdic.util.dataloader.data.admindata.UserData
uses tdic.util.dataloader.data.admindata.UserGroupData
uses tdic.util.dataloader.data.admindata.UserGroupColumnData
uses tdic.util.dataloader.data.admindata.AttributeData
uses tdic.util.dataloader.data.admindata.QueueData
uses tdic.util.dataloader.data.admindata.HolidayData
uses tdic.util.dataloader.entityloaders.adminloaders.BCAdminLocalizationLoader
uses tdic.util.dataloader.entityloaders.adminloaders.BCCollectionAgencyLoader
uses tdic.util.dataloader.parser.adminparser.BCAdminExcelFileParser
uses tdic.util.dataloader.entityloaders.adminloaders.BCRoleLoader
uses tdic.util.dataloader.entityloaders.adminloaders.BCActivityPatternLoader
uses tdic.util.dataloader.entityloaders.adminloaders.BCSecurityZoneLoader
uses tdic.util.dataloader.entityloaders.adminloaders.BCAuthorityLimitProfileLoader
uses tdic.util.dataloader.entityloaders.adminloaders.BCAuthorityLimitLoader
uses tdic.util.dataloader.entityloaders.adminloaders.BCUserAuthorityLimitLoader
uses tdic.util.dataloader.entityloaders.adminloaders.BCUserLoader
uses tdic.util.dataloader.entityloaders.adminloaders.BCGroupLoader
uses tdic.util.dataloader.entityloaders.adminloaders.BCUserGroupLoader
uses tdic.util.dataloader.entityloaders.adminloaders.BCRegionLoader
uses tdic.util.dataloader.entityloaders.adminloaders.BCAttributeLoader
uses tdic.util.dataloader.entityloaders.adminloaders.BCQueueLoader
uses tdic.util.dataloader.entityloaders.adminloaders.BCHolidayLoader
uses tdic.util.dataloader.util.DataLoaderUtil
uses java.lang.Exception
uses org.slf4j.LoggerFactory

class BCAdminDataLoaderProcessor extends DataLoaderProcessor {
  construct() {
    this.RelativePath = DataLoaderUtil.getRelativeAdminDatafilePath()
  }
  static final var LOG = LoggerFactory.getLogger("AdminData.AdminDataProcessor")
  private var _roleArray: ArrayList <RoleData> as RoleArray = new ArrayList <RoleData>()
  private var _activityPatternArray: ArrayList <ActivityPatternData> as ActivityPatternArray = new ArrayList <ActivityPatternData>()
  private var _securityZoneArray: ArrayList <SecurityZoneData> as SecurityZoneArray = new ArrayList <SecurityZoneData>()
  private var _authorityLimitProfileArray: ArrayList <AuthorityLimitProfileData> as AuthorityLimitProfileArray = new ArrayList <AuthorityLimitProfileData>()
  private var _authorityLimitArray: ArrayList <AuthorityLimitData> as AuthorityLimitArray = new ArrayList <AuthorityLimitData>()
  private var _userAuthorityLimitArray: ArrayList <UserAuthorityLimitData> as UserAuthorityLimitArray = new ArrayList <UserAuthorityLimitData>()
  private var _authorityLimitTypeArray: ArrayList <AuthorityLimitTypeData> as AuthorityLimitTypeArray = new ArrayList <AuthorityLimitTypeData>()
  private var _addressArray: ArrayList <AddressData> as AddressArray = new ArrayList <AddressData>()
  private var _userArray: ArrayList <UserData> as UserArray = new ArrayList <UserData>()
  private var _groupArray: ArrayList <GroupData> as GroupArray = new ArrayList <GroupData>()
  private var _userGroupArray: ArrayList <UserGroupData> as UserGroupArray = new ArrayList <UserGroupData>()
  private var _userGroupColumnArray: ArrayList <UserGroupColumnData> as UserGroupColumnArray = new ArrayList <UserGroupColumnData>()
  private var _regionArray: ArrayList <RegionData> as RegionArray = new ArrayList <RegionData>()
  private var _attributeArray: ArrayList <AttributeData> as AttributeArray = new ArrayList <AttributeData>()
  private var _queueArray: ArrayList <QueueData> as QueueArray = new ArrayList <QueueData>()
  private var _holidayArray: ArrayList <HolidayData> as HolidayArray = new ArrayList <HolidayData>()
  private var _collectionAgencyArray : ArrayList <CollectionAgencyData> as CollectionAgencyArray = new ArrayList <CollectionAgencyData>()
  private var _localizationArray: ArrayList <AdminLocalizationData> as LocalizationArray = new ArrayList <AdminLocalizationData>()
  private var _chkRole: Boolean as ChkRole = true
  private var _chkActivityPattern: Boolean as ChkActivityPattern = true
  private var _chkSecurityZone: Boolean as ChkSecurityZone = true
  private var _chkAuthorityLimit: Boolean as ChkAuthorityLimit = true
  private var _chkUserAuthorityLimit: Boolean as ChkUserAuthorityLimit = true
  private var _chkUser: Boolean as ChkUser = true
  private var _chkGroup: Boolean as ChkGroup = true
  private var _chkUserGroup: Boolean as ChkUserGroup = true
  private var _chkRegion: Boolean as ChkRegion = false
  private var _chkAttribute: Boolean as ChkAttribute = false
  private var _chkQueue: Boolean as ChkQueue = true
  private var _chkHoliday: Boolean as ChkHoliday = false
  private var _chkCollectionAgencies: Boolean as ChkCollectionAgencies = false
  private var _chkLocalization: Boolean as ChkLocalization = true
  private var _LoadCompleted: Boolean as LoadCompleted = false
  public function readAndImportFile() {
    readFile()
    importFile()
  }

  public function loadData() {
    // Process selected sheets
    if (ChkRole) {
      processRole()
    }
    if (ChkSecurityZone) {
      processSecurityZone()
    }
    if (ChkAuthorityLimit) {
      processAuthorityLimitProfile()
      processAuthorityLimit()
    }
    if (ChkUser) {
      processUser()
    }
    if (ChkUserAuthorityLimit) {
      processUserAuthorityLimit()
    }
    if (ChkGroup) {
      processGroup()
    }
    if (ChkUserGroup) {
      processUserGroup()
    }
    if (ChkRegion) {
      processRegion()
    }
    if (ChkAttribute) {
      processAttribute()
    }
    if (ChkQueue) {
      processQueue()
    }
    if (ChkActivityPattern) {
      processActivityPattern()
    }
    if (ChkHoliday) {
      processHoliday()
    }
    if (ChkCollectionAgencies) {
      processCollectionAgencies()
    }
    if (ChkLocalization) {
      processLocalization()
    }
    LoadCompleted = true
    LOG.info("Loaded data")
  }

  private function processRole() {
    for (role in RoleArray) {
      var roleLoader = new BCRoleLoader()
      roleLoader.createEntity(role)
    }
  }

  function processActivityPattern() {
    for (activityPattern in ActivityPatternArray) {
      var activityPatternLoader = new BCActivityPatternLoader()
      activityPatternLoader.createEntity(activityPattern)
    }
  }

  function processSecurityZone() {
    for (securityZone in SecurityZoneArray) {
      var securityZoneLoader = new BCSecurityZoneLoader()
      securityZoneLoader.createEntity(securityZone)
    }
  }

  function processRegion() {
    for (region in RegionArray) {
      var regionLoader = new BCRegionLoader()
      regionLoader.createEntity(region)
    }
  }

  function processAttribute() {
    for (attribute in AttributeArray) {
      var attributeLoader = new BCAttributeLoader()
      attributeLoader.createEntity(attribute)
    }
  }

  function processAuthorityLimitProfile() {
    for (authorityLimitProfile in AuthorityLimitProfileArray) {
      var authorityLimitProfileLoader = new BCAuthorityLimitProfileLoader()
      authorityLimitProfileLoader.createEntity(authorityLimitProfile)
    }
  }

  function processAuthorityLimit() {
    for (authorityLimit in AuthorityLimitArray) {
      var authorityLimitLoader = new BCAuthorityLimitLoader()
      authorityLimitLoader.createEntity(authorityLimit)
    }
  }

  function processUserAuthorityLimit() {
    for (userAuthorityLimit in UserAuthorityLimitArray) {
      var userAuthorityLimitLoader = new BCUserAuthorityLimitLoader()
      userAuthorityLimitLoader.createEntityWithLimitType(userAuthorityLimit, AuthorityLimitTypeArray)
    }
  }

  function processUser() {
    for (user in UserArray) {
      var userLoader = new BCUserLoader()
      userLoader.createEntity(user)
    }
  }

  function processGroup() {
    // order groups by hierarchy to enforce parent groups to be loaded before their subgroups
    for (group in GroupData.sortByHierarchy(GroupArray.toTypedArray())) {
      var groupLoader = new BCGroupLoader()
      groupLoader.createEntity(group)
    }
  }

  function processUserGroup() {
    for (userGroup in UserGroupArray) {
      var userGroupLoader = new BCUserGroupLoader()
      userGroupLoader.createEntityWithGroup(userGroup, UserGroupColumnArray)
    }
  }

  function processQueue() {
    for (queue in QueueArray) {
      var queueLoader = new BCQueueLoader()
      queueLoader.createEntity(queue)
    }
  }

  function processHoliday() {
    for (holiday in HolidayArray) {
      var holidayLoader = new BCHolidayLoader()
      holidayLoader.createEntity(holiday)
    }
  }

  function processCollectionAgencies() {
    for (collectionAgency in CollectionAgencyArray) {
      var collectionAgencyLoader = new BCCollectionAgencyLoader()
      collectionAgencyLoader.createEntity(collectionAgency)
    }
  }

  function processLocalization() {
    for (localization in LocalizationArray) {
      var localizationLoader = new BCAdminLocalizationLoader()
      localizationLoader.createEntity(localization)
    }
  }

  private function importFile() {
    try {
      BCAdminExcelFileParser.importAdminFile(Path, this)
      if (!IsLoadingFromConfig) {
        TempFile.deleteOnExit()
      }
    } catch (e: Exception) {
      throw e.toString()
    }
  }
}

