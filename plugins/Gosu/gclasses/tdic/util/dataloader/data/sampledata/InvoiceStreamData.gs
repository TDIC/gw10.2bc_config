package tdic.util.dataloader.data.sampledata
uses tdic.util.dataloader.data.ApplicationData
uses java.lang.Integer
uses java.util.Date

class InvoiceStreamData extends ApplicationData {

  construct() {

  }

  private var _invoiceStreamName          : String                           as InvoiceStreamName
  private var _description                : String                           as Description
  private var _dueOrInvoiceDay            : BillDateOrDueDateBilling         as DueOrInvoiceDay
  private var _dueOrInvoiceDayString      : String                           as DueOrInvoiceDayString
  private var _monthlyDayOfMonth          : Integer                          as MonthlyDayOfMonth
  private var _twiceAMonthFirstDay        : Integer                          as TwiceAMonthFirstDay
  private var _twiceAMonthSecondDay       : Integer                          as TwiceAMonthSecondDay
  private var _weeklyDayOfWeek            : DayOfWeek                        as WeeklyDayOfWeek
  private var _everyOtherWeekAnchorDate   : Date                             as EveryOtherWeekAnchorDate
  private var _paymentMethod              : PaymentMethod                    as PaymentMethod
  private var _paymentMethodString        : String                           as PaymentMethodString
  private var _token                      : String                           as Token
  private var _leadTime                   : Integer                          as LeadTime
  private var _periodicity                : Periodicity                      as Periodicity
  private var _periodicityString          : String                      as PeriodicityString
  
}
