package tdic.util.dataloader.data.sampledata

uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.AddressData
uses java.lang.Integer
uses java.util.Date

class ProducerData extends ApplicationData{

  construct() {

  }
  private var _producercode                     : String              as ProducerCode
  private var _producername                     : String              as ProducerName
  private var _primaryaddress                   : AddressData         as PrimaryAddress
  private var _active                           : Boolean             as Active
  private var _contact                          : ContactData         as Contact
  private var _accountrep                       : String              as AccountRep
  private var _agencybillplan                   : String              as AgencyBillPlan
  private var _commissionplan                   : String              as CommissionPlan
  private var _tier                             : String              as Tier
  private var _periodicity                      : String              as Periodicity
  private var _commissionDOM                    : Integer             as CommissionDOM
  private var _role                             : ProducerRole        as Role
  private var _createdate                       : Date                as CreateDate
}
