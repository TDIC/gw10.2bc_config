package tdic.util.dataloader.data.sampledata

uses java.util.Date
uses java.math.BigDecimal
uses java.lang.Integer

class PolicyChangeData extends ExistingPolicyData {

  construct() {

  }
  private var _modificationdate           : Date                  as ModificationDate
  private var _policynumber               : String                as PolicyNumber
  //Payment Plan Modifiers
  private var _matchplannedinstallments   : boolean               as MatchPlannedInstallments
  private var _suppressdownpayment        : boolean               as SuppressDownPayment
  private var _maxnuminstallments         : Integer               as MaxNumberOfInstallmentsOverride
  private var _downpaymentoverride        : BigDecimal            as DownPaymentOverridePercentage
}
