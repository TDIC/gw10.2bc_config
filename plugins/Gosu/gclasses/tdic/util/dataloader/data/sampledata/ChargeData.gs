package tdic.util.dataloader.data.sampledata

uses tdic.util.dataloader.data.ApplicationData
uses java.math.BigDecimal

class ChargeData extends ApplicationData {

  construct() {

  }
  private var _ChargePattern               : String            as ChargePattern
  private var _ChargeAmount                : BigDecimal        as ChargeAmount
  private var _Payer                       : String            as Payer
  private var _InvoiceStream               : String            as InvoiceStream
  private var _ChargeCurrency              : Currency          as ChargeCurrency
  
}
