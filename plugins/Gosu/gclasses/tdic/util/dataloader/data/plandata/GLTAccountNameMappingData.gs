package tdic.util.dataloader.data.plandata

uses tdic.util.dataloader.data.ColumnDescriptor

/**
 * US570 - General Ledger Integration
 * 11/14/2014 Alvin Lee
 *
 * Excel Data Loader Additions for custom GL Integration entities - An object to store the data for the
 * GLIntegrationTAccountMapping_TDIC object.
 */
class GLTAccountNameMappingData extends PlanData {

  /**
   * Constant for String defining what Excel sheet to use.
   */
  public static final var SHEET : String = "GLTAccountMapping"
  
  /**
   * Constant for the column indicating the Transaction Type
   */
  public static final var COL_TRANS_TYPE : int = 0

  /**
   * Constant for the column indicating the Line Item Type
   */
  public static final var COL_LINE_ITEM_TYPE : int = 1
  
  /**
   * Constant for the column indicating the Future Term
   */
  public static final var COL_FUTURE_TERM : int = 2

  /**
   * Constant for the column indicating the Payment Invoice Status
   */
  public static final var COL_INVOICE_STATUS : int = 3

  /**
   * Constant for the column indicating the Original T-Account Name
   */
  public static final var COL_ORIGINAL_TACCOUNT_NAME : int = 4

  /**
   * Constant for the column indicating the Mapped T-Account Name
   */
  public static final var COL_MAPPED_TACCOUNT_NAME : int = 5

  /**
   * Mapping of the columns to the fields.
   */
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_TRANS_TYPE, "Transaction Type", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_LINE_ITEM_TYPE, "Line Item Type", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_FUTURE_TERM, "Future Term", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_INVOICE_STATUS, "Invoice Status", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ORIGINAL_TACCOUNT_NAME, "Original T-Account Name", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_MAPPED_TACCOUNT_NAME, "Mapped T-Account Name", ColumnDescriptor.TYPE_STRING)
  }

  /**
   * Standard constructor.
   */
  construct() {
  }
  
  /**
   * The Transaction Type for the T-Account Mapping
   */
  var _transactionType : typekey.Transaction as TransactionType

  /**
   * The Line Item Type for the T-Account Mapping
   */
  var _lineItemType : typekey.LedgerSide as LineItemType

  /**
   * An indicator if the payment is for a future policy term (i.e. a policy period with an effective date in the future)
   */
  var _futureTerm : boolean as FutureTerm

  /**
   * The Payment Invoice Status for the T-Account Mapping
   */
  var invoiceStatus : typekey.InvoiceStatus as InvoiceStatus

  /**
   * The Original T-Account Name for the T-Account Mapping
   */
  var _originalTAccountName : typekey.GLOriginalTAccount_TDIC as OriginalTAccountName

  /**
   * The Mapped T-Account Name for the T-Account Mapping
   */
  var _mappedTAccountName : String as MappedTAccountName

}
