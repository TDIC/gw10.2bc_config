package tdic.util.dataloader.data.plandata
uses tdic.util.dataloader.data.ApplicationData
uses java.lang.Integer
uses tdic.util.dataloader.data.ColumnDescriptor

class DelinquencyPlanWorkflowData extends ApplicationData {

  // constants
  public static final var SHEET : String = "DelinquencyPlan_Workflow"
  public static final var COL_DELINQUENCY_PLAN_NAME : int = 0
  public static final var COL_DELINQUENCY_REASON : int = 1
  public static final var COL_WORKFLOW_TYPE : int = 2
  public static final var COL_EVENT_NAME : int = 3
  public static final var COL_EVENT_TRIGGER : int = 4
  public static final var COL_EVENT_OFFSET : int = 5
  public static final var COL_EVENT_RELATIVE_ORDER : int = 6
  public static final var COL_EVENT_AUTOMATIC : int = 7
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_DELINQUENCY_PLAN_NAME, "Delinquency Plan Name", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_DELINQUENCY_REASON, "Delinquency Reason", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_WORKFLOW_TYPE, "Workflow Type", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_EVENT_NAME, "Event Name", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_EVENT_TRIGGER, "Event Trigger", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_EVENT_OFFSET, "Event Offset", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_EVENT_RELATIVE_ORDER, "Event Relative Order", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_EVENT_AUTOMATIC, "Event Automatic", ColumnDescriptor.TYPE_STRING)
  }

  construct() {

  }
  
  // properties
  var _DelinquencyPlanName        : String                   as DelinquencyPlanName
  var _DelinquencyReason          : DelinquencyReason        as DelinquencyReason
  var _WorkflowType               : String                   as WorkflowType
  var _EventName                  : DelinquencyEventName     as EventName
  var _EventTrigger               : DelinquencyTriggerBasis  as EventTrigger
  var _EventOffset                : Integer                  as EventOffset
  var _EventRelativeOrder         : Integer                  as EventRelativeOrder
  var _EventAutomatic             : Boolean                  as EventAutomatic

}
