package tdic.util.dataloader.data.plandata

uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.ColumnDescriptor

class ChargePatternData extends ApplicationData {

  // constants
  public static final var SHEET : String = "ChargePattern"
  public static final var COL_PUBLIC_ID : int = 0
  public static final var COL_CHARGE_CODE : int = 1
  public static final var COL_CHARGE_NAME : int = 2
  public static final var COL_CHARGE_TYPE : int = 3
  public static final var COL_PERIODICITY : int = 4
  public static final var COL_CATEGORY : int = 5
  public static final var COL_TACCOUNT_OWNER : int = 6
  public static final var COL_INVOICING_APPROACH : int = 7
  public static final var COL_PRIORITY : int = 8
  public static final var COL_INCLUDE_IN_EQUITY_DATING : int = 9
  public static final var COL_EXCLUDE : int = 10
  public static final var COL_PRINTPRIORITY : int = 11
  public static final var CD : ColumnDescriptor[] = {
      new ColumnDescriptor(COL_PUBLIC_ID, "Public ID", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_CHARGE_CODE, "ChargeCode", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_CHARGE_NAME, "Charge Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_CHARGE_TYPE, "Charge Type", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_PERIODICITY, "Frequency (For ProRata only)", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_CATEGORY, "Category", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_TACCOUNT_OWNER, "T-Account Owner", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_INVOICING_APPROACH, "Invoicing Approach", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_PRIORITY, "Priority", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_INCLUDE_IN_EQUITY_DATING, "Include Equity Dating", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_EXCLUDE, "Exclude", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_PRINTPRIORITY, "Print Priority", ColumnDescriptor.TYPE_STRING)
  }
  
  construct() {

  }
  
  // properties
  var _ChargeCode                    : String              as ChargeCode
  var _ChargeName                    : String              as ChargeName
  var _ChargeType                    : String              as ChargeType
  var _Periodicity                   : Periodicity         as Periodicity
  var _Category                      : ChargeCategory      as Category
  var _TAccount_Owner                : String              as TAccount_Owner
  var _InvoicingApproach             : InvoiceTreatment    as InvoicingApproach
  var _Priority                      : ChargePriority      as Priority
  var _Include_In_Equity_Dating      : Boolean             as Include_In_Equity_Dating
  var _printPriority                 : ChargePrintPriority_TDIC       as PrintPriority

}
