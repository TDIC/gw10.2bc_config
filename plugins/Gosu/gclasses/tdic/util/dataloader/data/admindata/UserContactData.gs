package tdic.util.dataloader.data.admindata

uses tdic.util.dataloader.data.AddressData
uses tdic.util.dataloader.data.ApplicationData
uses java.util.Date

class UserContactData extends ApplicationData {

  construct() {

  }

  private var _lastname                : String                       as LastName
  private var _firstname               : String                       as FirstName
  private var _middlename              : String                       as MiddleName
  private var _name                    : String                       as Name
  private var _homephone               : String                       as HomePhone
  private var _workphone               : String                       as WorkPhone
  private var _cellphone               : String                       as CellPhone
  private var _faxphone                : String                       as FaxPhone
  private var _primaryphone            : String                       as PrimaryPhone
  private var _primaryaddress          : AddressData                  as PrimaryAddress
  private var _nonprimaryaddress       : AddressData                  as NonPrimaryAddress
  private var _formername              : String                       as FormerName
  private var _emailaddress1           : String                       as EmailAddress1
  private var _emailaddress2           : String                       as EmailAddress2
  private var _gender                  : String                       as Gender
  private var _dateofbirth             : Date                         as DateOfBirth
  private var _employeenumber          : String                       as EmployeeNumber
  private var _notes                   : String                       as Notes
}
