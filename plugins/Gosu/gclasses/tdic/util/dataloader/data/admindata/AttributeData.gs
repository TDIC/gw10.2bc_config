package tdic.util.dataloader.data.admindata

uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.ColumnDescriptor

class AttributeData extends ApplicationData {

  // constants
  public static final var SHEET : String = "Attribute"
  public static final var COL_NAME : int = 0
  public static final var COL_PUBLIC_ID : int = 1
  public static final var COL_DESCRIPTION : int = 2
  public static final var COL_USER_ATTRIBUTE_TYPE : int = 3
  public static final var COL_ACTIVE : int = 4
  public static final var COL_EXCLUDE : int = 5
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_NAME, "Name", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_PUBLIC_ID, "Public ID", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_DESCRIPTION, "Description", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_USER_ATTRIBUTE_TYPE, "Type", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ACTIVE, "Active", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_EXCLUDE, "Exclude", ColumnDescriptor.TYPE_STRING)
  }

  construct() {

  }

  // properties
  private var _name                    : String                       as Name
  private var _description             : String                       as Description
  private var _userAttributeType       : UserAttributeType            as UserAttributeType
  private var _active                  : Boolean                      as Active
}
