package tdic.util.dataloader.data.admindata

uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.ColumnDescriptor

class AdminLocalizationData extends ApplicationData {
  // constants
  public static final var SHEET : String = "Localization"
  public static final var COL_LANGUAGE : int = 0
  public static final var COL_ROLE : int = 1
  public static final var COL_ROLE_NAME : int = 2
  public static final var COL_ROLE_DESC : int = 3
  public static final var COL_ACTIVITY_PATTERN : int = 4
  public static final var COL_ACTV_PATTERN_SUBJ : int = 5
  public static final var COL_ACTV_PATTERN_DESC : int = 6
  public static final var COL_SEC_ZONE : int = 7
  public static final var COL_SEC_ZONE_NAME :int = 8
  public static final var COL_SEC_ZONE_DESC : int = 9
  public static final var COL_AUTH_LIMIT_PROFILE : int = 10
  public static final var COL_AUTH_LIMIT_PROFILE_NAME : int = 11
  public static final var COL_AUTH_LIMIT_PROFILE_DESC : int = 12
  public static final var COL_GROUP : int = 13
  public static final var COL_GROUP_NAME : int = 14
  public static final var COL_REGION : int = 15
  public static final var COL_REGION_NAME : int = 16
  public static final var COL_ATTRIBUTE : int = 17
  public static final var COL_ATTRIBUTE_NAME : int = 18
  public static final var COL_ATTRIBUTE_DESC : int = 19
  public static final var COL_HOLIDAY : int = 20
  public static final var COL_HOLIDAY_NAME : int = 21
  public static final var CD : ColumnDescriptor[] = {
      new ColumnDescriptor(0,"Language",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(1,"Role",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(2,"Role Name",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(3,"Role Desc",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(4,"Activity Pattern",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(5,"Actv Pattern Subj",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(6,"Actv Pattern Desc",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(7,"Sec Zone",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(8,"Sec Zone Name",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(9,"Sec Zone Desc",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(10,"Auth Limit Profile",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(11,"Auth Limit Profile Name",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(12,"Auth Limit Profile Desc",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(13,"Group",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(14,"Group Name",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(15,"Region",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(16,"Region Name",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(17,"Attribute",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(18,"Attribute Name",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(19,"Attribute Desc",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(20,"Holiday",ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(21,"Holiday Name",ColumnDescriptor.TYPE_STRING)
  }

  construct() {
  }

  // properties
  private var _language             : LanguageType   as Language
  private var _role                 : String         as Role
  private var _rolename             : String         as RoleName
  private var _roledesc             : String         as RoleDesc
  private var _activitypattern      : String         as ActivityPattern
  private var _actvpatternsubj      : String         as ActvPatternSubj
  private var _actvpatterndesc      : String         as ActvPatternDesc
  private var _seczone              : String         as SecZone
  private var _seczonename          : String         as SecZoneName
  private var _seczonedesc          : String         as SecZoneDesc
  private var _authlimitprofile     : String         as AuthLimitProfile
  private var _authlimitprofilename : String         as AuthLimitProfileName
  private var _authlimitprofiledesc : String         as AuthLimitProfileDesc
  private var _group                : String         as Group
  private var _groupname            : String         as GroupName
  private var _region               : String         as Region
  private var _regionname           : String         as RegionName
  private var _attribute            : String         as Attribute
  private var _attributename        : String         as AttributeName
  private var _attributedesc        : String         as AttributeDesc
  private var _holiday              : String         as Holiday
  private var _holidayname          : String         as HolidayName
}