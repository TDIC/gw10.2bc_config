package tdic.util.dataloader.data.admindata

uses tdic.util.dataloader.data.ApplicationData
uses java.util.ArrayList
uses tdic.util.dataloader.data.ColumnDescriptor

class UserAuthorityLimitData extends ApplicationData {

  // constants
  public static final var SHEET : String = "UserAuthorityLimit"
  public static final var COL_USER_REFERENCE_NAME : int = 0
  public static final var COL_USER_PUBLIC_ID : int = 1
  public static final var COL_PROFILE : int = 2
  public static final var COL_AUTHORITY_LIMIT_PUBLIC_ID : int = 3
  public static final var COL_AUTHORITY_LIMIT_VALUE1 : int = 4
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_USER_REFERENCE_NAME, "Enter the Authority Limit", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_USER_PUBLIC_ID, "User Public-ID", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_PROFILE, "Authority Profile", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_AUTHORITY_LIMIT_PUBLIC_ID, "Authority Limit Public-ID", ColumnDescriptor.TYPE_STRING)
  }
    
  construct() {

  }
  
  // properties
  private var _userReferenceName       : String                       as UserReferenceName
  private var _userPublicID            : String                       as UserPublicID
  private var _profile                 : AuthorityLimitProfileData    as Profile
  private var _authorityLimitPublicID  : String                       as AuthorityLimitPublicID
  private var _limitAmounts            : ArrayList                    as LimitAmounts = new ArrayList()
}
