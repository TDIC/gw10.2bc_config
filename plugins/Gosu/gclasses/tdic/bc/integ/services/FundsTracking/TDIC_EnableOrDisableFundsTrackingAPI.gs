package tdic.bc.integ.services.FundsTracking

uses gw.api.webservice.systemTools.SystemRunlevel
uses gw.wsi.pl.SystemToolsAPI
uses gw.xml.ws.annotation.WsiWebService

/**
 * API to Enable or Disable Funds Traking
 */
@WsiWebService
@Exportable
class TDIC_EnableOrDisableFundsTrackingAPI {
  /*
  Below method is to Enable Funds Tracking
   */
  @Throws(com.guidewire.pl.system.exception.TransactionException, "If communication errors occur")
  @Returns("Status")
  function enableFundsTracking() : String {
    try {
      if( not gw.api.domain.fundstracking.FundsTrackingSwitch.isEnabled()){
        gw.api.domain.fundstracking.FundsTrackingSwitch.enable()
        return "Successfully Enabled Funds Tracking"
      } else {
        return "Funds Tracking is already Enabled"
      }
    } catch (e : com.guidewire.pl.system.exception.TransactionException) {
      return "Error while Enabling Funds Tracking" + e.Message
    } catch (e : Exception) {
      return "Error while Enabling Funds Tracking" + e.Message
    }
  }

  /*
  Below method is to Disable Funds Tracking
   */
  @Throws(com.guidewire.pl.system.exception.TransactionException, "If communication errors occur")
  @Returns("Status")
  function disableFundsTracking() : String {
    try {
      if(gw.api.domain.fundstracking.FundsTrackingSwitch.isEnabled()){
        gw.api.domain.fundstracking.FundsTrackingSwitch.disable()
        return "Successfully Disabled Funds Tracking"
      } else {
        return "Funds Tracking is already Disabled"
      }
    } catch (e : com.guidewire.pl.system.exception.TransactionException) {
      return "Error while Disabling Funds Tracking" + e.Message
    } catch (e : Exception) {
      return "Error while Disabling Funds Tracking" + e.Message
    }
  }

  /*
  Below method will set the Run Level to 'Maintenance'
   */
  @Returns("Status")
  function setRunLevelToMaintenance() : String {
    try{
      new gw.api.webservice.systemTools.SystemToolsImpl().setRunlevel(SystemRunlevel.MAINTENANCE)
      return "Successfully Set the Run Level to 'Maintenance'"
    } catch (e : gw.api.webservice.exception.ServerStateException ) {
      return "Error while setting the Run Level to 'Maintenance'" + e.Message
    }
  }
}