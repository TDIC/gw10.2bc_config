package tdic.bc.integ.services.policybilling.dto

uses java.util.Date
uses gw.xml.ws.annotation.WsiExportable

/**
 * US1137
 * 03/02/2015 Kesava Tavva
 * GW-445 11/09/2015 Praneeth
 * DTO object to hold PolicyNumber, PaidThroughDate, FirstInvoicePaidFull and UnpaidPolicyChangeInvoice
 */
@WsiExportable
final class TDIC_PolicyBillingDTO {
  var policyNumber : String as PolicyNumber
  var paidThroughDate : Date as PaidThroughDate
  var effectiveDate : Date as EffectiveDate
  var expirationDate : Date as ExpirationDate
  var firstInvoicePaidFull : Boolean as FirstInvoicePaidFull
  var unpaidPolicyChangeInvoice : Boolean as UnpaidPolicyChangeInvoice
  var cancellationDate: Date as CancellationDate
  var paidToBilledRatio:double as PaidToBilledRatio
  /**
   * US1137
   * 03/02/2015 Kesava Tavva
   * GW-445 11/09/2015 Praneeth
   * Override function to return field values in a String object.
   */
  override function toString() : String {
    return "PolicyNumber : ${PolicyNumber} | PaidThroughDate : ${PaidThroughDate} | EffectiveDate : ${EffectiveDate} | ExpirationDate : ${ExpirationDate} | FirstInvoicePaidFull : ${FirstInvoicePaidFull} | UnpaidPolicyChangeInvoice : ${UnpaidPolicyChangeInvoice} | CancellationDate : ${CancellationDate} | PaidToBilledRatio : ${PaidToBilledRatio} "
  }
}