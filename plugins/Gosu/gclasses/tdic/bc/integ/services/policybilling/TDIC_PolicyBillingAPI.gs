package tdic.bc.integ.services.policybilling

uses gw.api.database.Query
uses gw.api.system.BCLoggerCategory
uses gw.api.webservice.exception.SOAPServerException
uses gw.xml.ws.annotation.WsiWebService
uses java.util.ArrayList
uses java.util.HashMap
uses org.slf4j.LoggerFactory
uses tdic.bc.integ.services.policybilling.dto.TDIC_PolicyBillingDTO
uses tdic.bc.integ.services.policybilling.dto.TDIC_PolicyBillingRequest
uses tdic.bc.integ.services.policybilling.dto.TDIC_PolicyBillingResponse

/**
 * US1137
 * 03/02/2015 Kesava Tavva
 * GW-445 11/09/2015 Praneeth
 * Webservice API to retrieve Paid through dates/First invoice paid in full and Unpaid Policy Change Invoice for given policy numbers.
 */
@WsiWebService
class TDIC_PolicyBillingAPI {

  private static final var _logger = LoggerFactory.getLogger("POLICY_BILLING_API")
  private static var CLASS_NAME = "TDIC_PolicyBillingAPI"

  /**
   * US1137
   * 03/02/2015 Kesava Tavva
   * GW-445 11/09/2015 Praneeth
   * Service to retrieve Paid through dates for a given list of policy numbers As well First invoice paid in full and Unpaid Policy Change Invoice
   */
  @Param("policyNumber", "String value of PolicyNumber")
  @Returns("TDIC_PolicyBillingDTO, DTO Object contains policy numnber and paidthrough date")
  @Throws(SOAPServerException, "If communication error or any other SOAP problem occurs.")

  function getPaidThroughDateForPolicy(policyNumber : String) : TDIC_PolicyBillingDTO {
    var logPrefix = "${CLASS_NAME}#getPaidThroughDateForPolicy(String, Date)"
    _logger.trace("${logPrefix} - Entering")
    _logger.info("${logPrefix} - Retrieving paid through date for PolicyNumber: ${policyNumber}")
    var response : TDIC_PolicyBillingDTO
    var req = new TDIC_PolicyBillingRequest()
    var policyNumbersList = new ArrayList<String>()
    policyNumbersList.add(policyNumber)
    req.PolicyNumbersList = policyNumbersList
    var result = getPaidThroughDatesForPolicies(req)
    if(result.PolicyBillingDTOList.HasElements)
      response = result.PolicyBillingDTOList.get(0)
    _logger.info("${logPrefix} - Service response details for PolicyNumber: ${policyNumber}-->Response dto: ${response}")
    _logger.trace("${logPrefix} - Exiting")
    return response
  }

  /**
   * US1137
   * 03/02/2015 Kesava Tavva
   * GW-445 11/09/2015 Praneeth
   * Service to retrieve Paid through dates for a given list of policy numbers As well First invoice paid in full and Unpaid Policy Change Invoice
   * GW 1401 - Refactor Paid Through Date for better claims performance.
   * 03/22/2016 Praneethk
   */
  @Param("request", "Service request object with list of TDIC_PolicyBillingDTO which has policy number")
  @Returns("TDIC_PolicyBillingResponse, Response contains list of TDIC_PolicyBillingDTo which containts Policy number and its PaidThroughDate")
  @Throws(SOAPServerException, "If communication error or any other SOAP problem occurs.")
  function getPaidThroughDatesForPolicies(request : TDIC_PolicyBillingRequest) : TDIC_PolicyBillingResponse {

    var logPrefix = "${CLASS_NAME}#getPaidThroughDateForPolicy(TDIC_PolicyBillingRequest)"
    _logger.trace("${logPrefix} - Entering")
    _logger.info("${logPrefix} - Retrieving paid through dates for Request : ${request}")
    var response : TDIC_PolicyBillingResponse
    if(not request.PolicyNumbersList.HasElements){
      _logger.info("${logPrefix} - Request doesn't have any policy number details")
      _logger.trace("${logPrefix} - Exiting")
      return response
    }
    var policyPeriods = Query.make(PolicyPeriod).compareIn("PolicyNumber",request.PolicyNumbersList?.toTypedArray()).select()
    response = new TDIC_PolicyBillingResponse()
    var dtoList = new ArrayList<TDIC_PolicyBillingDTO>()
    var pbDTO : TDIC_PolicyBillingDTO
    var FirstInvoicePaidFullMap = new HashMap<String, Boolean>()
    var UnpaidEndorsementMap = new HashMap<String, Boolean>()
    policyPeriods?.each( \ pp -> {

      if(pp.PaidThroughDate > pp.EffectiveDate or pp.Cancellation.ModificationDate != null){
        pbDTO = new TDIC_PolicyBillingDTO()
        pbDTO.PolicyNumber = pp.PolicyNumber
        pbDTO.PaidThroughDate = pp.Invoices.where(\inv-> !inv.Paid and inv.Date<pp.ExpirationDate).Count==0? pp.ExpirationDate :pp.PaidThroughDate //GWPS-96 : Keshavg : Updated PaidThroughDate to exclude change due to FinalAudit Invoice.
        pbDTO.EffectiveDate = pp.EffectiveDate
        pbDTO.ExpirationDate = pp.ExpirationDate
        if(!FirstInvoicePaidFullMap.containsKey(pp.PolicyNumber)){
            FirstInvoicePaidFullMap.put(pp.PolicyNumber, pp.FirstInvoicePaidInFull_TDIC)
        }
        pbDTO.FirstInvoicePaidFull = FirstInvoicePaidFullMap.get(pp.PolicyNumber)

        if(!UnpaidEndorsementMap.containsKey(pp.PolicyNumber)){
            UnpaidEndorsementMap.put(pp.PolicyNumber,pp.UnpaidPolicyChangeInvoice_TDIC)
        }
        pbDTO.UnpaidPolicyChangeInvoice = UnpaidEndorsementMap.get(pp.PolicyNumber)
        //GW2543
        pbDTO.CancellationDate = pp.Cancellation.ModificationDate
        pbDTO.PaidToBilledRatio = pp.getPaidToBilledRatio()

        dtoList.add(pbDTO)
      }
    })
    response.PolicyBillingDTOList = dtoList
    _logger.info("${logPrefix} - Request: ${request} | Response: ${response}")
    _logger.trace("${logPrefix} - Exiting")
    return response
  }
}