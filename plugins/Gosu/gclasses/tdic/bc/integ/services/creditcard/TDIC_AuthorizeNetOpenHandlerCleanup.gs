package tdic.bc.integ.services.creditcard

uses com.tdic.util.properties.PropertyUtil
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory

uses java.sql.SQLException

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 3/13/17
 * Time: 1:06 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_AuthorizeNetOpenHandlerCleanup {

  public static final var LOGGER : Logger = LoggerFactory.getLogger("CREDIT_CARD_HANDLER")

  /**
   * The logger tag for instances of this Batch.
   */
  private static final var LOG_TAG = "TDIC_AuthorizenetOpenHandlerCleanup#"

  /**
   * Count for open handler keys in GWINT database.
   */
  //private var openHandlerKeys : List

  /**
   * The name of the Authorize.Net Login ID property in cached properties.
   */
  private static final var API_LOGIN_ID_PROPERTY = "auth.net.api.login"

  /**
   * The name of the Authorize.Net Transaction Key property in cached properties.
   */
  private static final var TRANSACTION_KEY_PROPERTY = "auth.net.trans.key"

  /**
   * The name of the Authorize.Net Environment property in cached properties.
   */
  private static final var ENVIRONMENT_PROPERTY = "auth.net.env"

  /**
   * The root name of the Authorize.Net Open Handler properties in cached properties.
   */
  private static final var OPEN_HANDLER_PROPERTY_ROOT = "auth.net.openhandler."

  /**
   * Function to find out if any stale open handlers exist
   */
  public function findAndRemoveOpenHandlers() : String {
    var returnValue : String = null

    var logTag = LOG_TAG + "findOpenHandlers() - "
    var openHandlerKeys =  PropertyUtil.getInstance().getIntProperties().keySet().where(\k -> (k as String).contains(OPEN_HANDLER_PROPERTY_ROOT))
    if(openHandlerKeys.size() > 0) {
      LOGGER.info(logTag + openHandlerKeys.size() + " Open Authorize.Net handler keys found")
      try {
        removeOpenHandlers(openHandlerKeys)
      }
      catch(ex: Exception) {
        returnValue = ex.StackTraceAsString
      }
    }

    return returnValue
  }



  /**
   * Function to remove stale open handlers
   */
  protected function removeOpenHandlers(openHandlerKeys: List) {
    var logTag = LOG_TAG + "removeOpenHandlers() - "
    try {
      var apiLoginID = PropertyUtil.getInstance().getProperty(API_LOGIN_ID_PROPERTY)
      var transactionKey = PropertyUtil.getInstance().getProperty(TRANSACTION_KEY_PROPERTY)
      var environment = PropertyUtil.getInstance().getProperty(ENVIRONMENT_PROPERTY)
      var merchant = TDIC_AuthorizeNetCreditCardHandler.createMerchant(environment, apiLoginID, transactionKey)
      var openHandlerCustomerProfileID : String
      for(anOpenHandlerKey in openHandlerKeys) {
        openHandlerCustomerProfileID = PropertyUtil.getInstance().getProperty(anOpenHandlerKey as String)
        if(!(TDIC_AuthorizeNetCreditCardHandler.openHandlerExists(openHandlerCustomerProfileID))) {
          var accountNumber = TDIC_AuthorizeNetCreditCardHandler.extractAccountNumberFromPropertyName(anOpenHandlerKey as String)
          LOGGER.info(logTag + "Finishing Credit Card Handler key for account " + accountNumber)
          LOGGER.info(logTag + "Deleting customer profile " + openHandlerCustomerProfileID + " for open credit card handler on account " + accountNumber)
          TDIC_AuthorizeNetCreditCardHandler.deleteCustomerProfile(merchant, openHandlerCustomerProfileID)
          PropertyUtil.getInstance().getIntProperties().remove(anOpenHandlerKey as String)
        }
      }
    }
    catch (sql:SQLException) {
      LOGGER.error(logTag + "SQLException= " + sql)
      throw sql
    }
    catch (ex:Exception) {
      LOGGER.error(logTag+ "Exception= " + ex)
      throw ex
    }
  }
}
