package tdic.bc.integ.services.check.dto

uses gw.xml.ws.annotation.WsiExportable

@WsiExportable
final class TDIC_CheckStatusRequest {

  var checkStatusDTOList: List<TDIC_CheckStatusDTO> as CheckStatusDTOList

  override function toString() : String{
    var request = new StringBuffer("Request: ")
    for(record in CheckStatusDTOList){
      request.append("Record: "+record.toString()+"; ")
    }
    return request.toString()
  }
}