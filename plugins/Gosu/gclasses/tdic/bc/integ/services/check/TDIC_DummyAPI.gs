package tdic.bc.integ.services.check

uses gw.xml.ws.annotation.WsiWebService

@WsiWebService
class TDIC_DummyAPI {
  function echo(message: String):String{
    return message
  }
}