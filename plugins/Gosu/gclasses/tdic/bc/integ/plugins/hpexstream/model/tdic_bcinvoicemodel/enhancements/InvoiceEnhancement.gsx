package tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoicemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement InvoiceEnhancement : tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoicemodel.Invoice {
  public static function create(object : entity.Invoice) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoicemodel.Invoice {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoicemodel.Invoice(object)
  }

  public static function create(object : entity.Invoice, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoicemodel.Invoice {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoicemodel.Invoice(object, options)
  }

}