package tdic.bc.integ.plugins.hpexstream.model.tdic_bcpolicyperiodcontactmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyPeriodContactEnhancement : tdic.bc.integ.plugins.hpexstream.model.tdic_bcpolicyperiodcontactmodel.PolicyPeriodContact {
  public static function create(object : entity.PolicyPeriodContact) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcpolicyperiodcontactmodel.PolicyPeriodContact {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcpolicyperiodcontactmodel.PolicyPeriodContact(object)
  }

  public static function create(object : entity.PolicyPeriodContact, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcpolicyperiodcontactmodel.PolicyPeriodContact {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcpolicyperiodcontactmodel.PolicyPeriodContact(object, options)
  }

}