package tdic.bc.integ.plugins.hpexstream.model.tdic_bcdelinquencyprocessmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement DelinquencyProcessEnhancement : tdic.bc.integ.plugins.hpexstream.model.tdic_bcdelinquencyprocessmodel.DelinquencyProcess {
  public static function create(object : entity.DelinquencyProcess) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcdelinquencyprocessmodel.DelinquencyProcess {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcdelinquencyprocessmodel.DelinquencyProcess(object)
  }

  public static function create(object : entity.DelinquencyProcess, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcdelinquencyprocessmodel.DelinquencyProcess {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcdelinquencyprocessmodel.DelinquencyProcess(object, options)
  }

}