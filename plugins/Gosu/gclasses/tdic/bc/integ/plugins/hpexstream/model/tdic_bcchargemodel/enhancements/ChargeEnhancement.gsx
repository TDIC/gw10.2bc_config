package tdic.bc.integ.plugins.hpexstream.model.tdic_bcchargemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ChargeEnhancement : tdic.bc.integ.plugins.hpexstream.model.tdic_bcchargemodel.Charge {
  public static function create(object : entity.Charge) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcchargemodel.Charge {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcchargemodel.Charge(object)
  }

  public static function create(object : entity.Charge, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcchargemodel.Charge {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcchargemodel.Charge(object, options)
  }

}