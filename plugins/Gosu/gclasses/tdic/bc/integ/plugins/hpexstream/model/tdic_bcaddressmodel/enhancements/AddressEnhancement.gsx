package tdic.bc.integ.plugins.hpexstream.model.tdic_bcaddressmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement AddressEnhancement : tdic.bc.integ.plugins.hpexstream.model.tdic_bcaddressmodel.Address {
  public static function create(object : entity.Address) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcaddressmodel.Address {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcaddressmodel.Address(object)
  }

  public static function create(object : entity.Address, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcaddressmodel.Address {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcaddressmodel.Address(object, options)
  }

}