package tdic.bc.integ.plugins.hpexstream.model.tdic_bccancellationmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement CancellationEnhancement : tdic.bc.integ.plugins.hpexstream.model.tdic_bccancellationmodel.Cancellation {
  public static function create(object : entity.Cancellation) : tdic.bc.integ.plugins.hpexstream.model.tdic_bccancellationmodel.Cancellation {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bccancellationmodel.Cancellation(object)
  }

  public static function create(object : entity.Cancellation, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.hpexstream.model.tdic_bccancellationmodel.Cancellation {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bccancellationmodel.Cancellation(object, options)
  }

}