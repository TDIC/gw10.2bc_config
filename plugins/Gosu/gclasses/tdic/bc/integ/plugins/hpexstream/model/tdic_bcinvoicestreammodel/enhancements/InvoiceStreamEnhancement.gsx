package tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoicestreammodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement InvoiceStreamEnhancement : tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoicestreammodel.InvoiceStream {
  public static function create(object : entity.InvoiceStream) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoicestreammodel.InvoiceStream {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoicestreammodel.InvoiceStream(object)
  }

  public static function create(object : entity.InvoiceStream, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoicestreammodel.InvoiceStream {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoicestreammodel.InvoiceStream(object, options)
  }

}