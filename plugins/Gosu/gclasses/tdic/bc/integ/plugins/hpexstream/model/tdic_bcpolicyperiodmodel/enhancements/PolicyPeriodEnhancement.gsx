package tdic.bc.integ.plugins.hpexstream.model.tdic_bcpolicyperiodmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PolicyPeriodEnhancement : tdic.bc.integ.plugins.hpexstream.model.tdic_bcpolicyperiodmodel.PolicyPeriod {
  public static function create(object : entity.PolicyPeriod) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcpolicyperiodmodel.PolicyPeriod {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcpolicyperiodmodel.PolicyPeriod(object)
  }

  public static function create(object : entity.PolicyPeriod, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcpolicyperiodmodel.PolicyPeriod {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcpolicyperiodmodel.PolicyPeriod(object, options)
  }

}