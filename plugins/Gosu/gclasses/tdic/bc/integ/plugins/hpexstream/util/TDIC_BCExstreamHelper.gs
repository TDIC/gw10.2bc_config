/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P.
 * © Copyright 2009-2014 Guidewire Software, Inc.
 */
package tdic.bc.integ.plugins.hpexstream.util

uses com.tdic.plugins.hpexstream.core.xml.TDIC_ExstreamXMLCreatorFactory
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.system.database.SequenceUtil
uses gw.api.util.DateUtil
uses gw.pl.persistence.core.Bundle
uses gw.plugin.Plugins
uses gw.plugin.document.IDocumentTemplateSource
uses gw.util.concurrent.LockingLazyVar
uses com.tdic.plugins.hpexstream.core.bo.TDIC_TemplateIds
uses com.tdic.plugins.hpexstream.core.bo.TDIC_TemplateidToDocPubIdMap
uses com.tdic.plugins.hpexstream.core.util.TDIC_ExstreamHelper
uses com.tdic.plugins.hpexstream.core.util.TDIC_Helper

uses java.lang.IllegalArgumentException
uses java.util.ArrayList
uses tdic.bc.integ.plugins.hpexstream.bo.TDIC_DocumentRequestSource
uses java.util.List
uses org.slf4j.LoggerFactory

class TDIC_BCExstreamHelper extends TDIC_ExstreamHelper implements TDIC_Helper {
  private static var _theInstance = LockingLazyVar.make(\-> new TDIC_BCExstreamHelper ())
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  private static var _portalStatusYes =  "Yes"
  private static var _portalStatusNo =  "No"
  private static var _authorSystemGenerated = "System Generated"
  static property get Instance(): TDIC_BCExstreamHelper {
    return _theInstance.get()
  }

  /**
   * This function
   * 1) Sets the appropriate document status and doc delivery status
   * 2) Instantiates Document Approval Workflow
   * when the user tries to Finalize a live document
   */
  @Param("pLiveDocument", "Live document being finalized.")
  @Returns("void")
  function finalizeLiveDocument(pLiveDocument: Document): void {
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {

      // Update document status and doc delivery status
      var writeableDoc = bundle.add(pLiveDocument)
      writeableDoc.Status = DocumentStatusType.TC_APPROVING
      writeableDoc.DeliveryStatus_TDIC = DocDlvryStatusType_TDIC.TC_SENDING

      // Save updated document metadata
      var documentMetadataSource = Plugins.get(gw.plugin.document.IDocumentMetadataSource)
      documentMetadataSource.saveDocument(writeableDoc)

      // Instantiate workflow
      var workflow = new entity.DocumentApprovalWF_TDIC()
      workflow.Document = writeableDoc
      workflow.startAsynchronously()
    })
  }

  /**
   * US555
   * 11/28/2014 shanem
   *
   * Returns a document name for a given PolicyPeriod
   */
  @Param("aPolicyNumber", "PolicyNumber of the document's associated policy")
  @Param("aTemplateId", "Template ID used to create document")
  @Returns("Template Desccriptor")
  function getDocumentName(anAccountNumber: String, aPolicyNumber: String, aTemplateId: String): String {
    if (anAccountNumber == null || aTemplateId == null){
      throw new IllegalArgumentException("Policy Number or Template Id Is NULL")
    }
    var fullName = aTemplateId + "_" + anAccountNumber + "_" + aPolicyNumber + "_" + DateUtil.currentDate()
    return aPolicyNumber == null ? aTemplateId + "_" + anAccountNumber + "_" + DateUtil.currentDate() : fullName
  }

  function createDocumentStub(aTemplateId: String, aPrintOrder: int, anAccount: Account, eventName: String, user: String): void {
    createDocumentStub(aTemplateId, aPrintOrder, anAccount, null, eventName, user)
  }

  function createDocumentStub(aTemplateId: String, aPrintOrder: int, anAccount: Account, aPolicyPeriod: PolicyPeriod, eventName: String): void {
    createDocumentStub(aTemplateId, aPrintOrder, anAccount, aPolicyPeriod, eventName, "System Generated")
  }

  /**
   * US644
   * 11/28/2014 shanem
   *
   * Creates a document for a given Account/Policy
   */
  @Param("aTemplateId", "Template to create stub for")
  @Param("aPolicyPeriod", "PolicyPeriod to associate document stub with")
  function createDocumentStub(aTemplateId: String, aPrintOrder: int, anAccount: Account, aPolicyPeriod: PolicyPeriod, eventName: String, user: String): void {
    _logger.trace("TDIC_BCExstreamHelper#createDocumentStub() - Entering.")

    if(ScriptParameters.TDIC_DataSource != typekey.DataSource_TDIC.TC_BILLINGCENTER.Code) {
      return
    }

    var doc: Document
    var dts = gw.plugin.Plugins.get(IDocumentTemplateSource)
    var templateDescriptor = dts.getDocumentTemplate(aTemplateId, null)
    if (templateDescriptor != null) {
      var bundle = anAccount.Bundle;
      if (bundle == null) {
        bundle = gw.transaction.Transaction.getCurrent();
      }
      doc = new Document(bundle);

      doc.PendingDocUID = (SequenceUtil.next(1, "ExstreamDocUID")) as String
      doc.Name = getDocumentName(anAccount.AccountNumber, aPolicyPeriod.PolicyNumber, aTemplateId)
      doc.Status = DocumentStatusType.TC_APPROVED
      doc.Type = typekey.DocumentType.get(templateDescriptor.TemplateType)
      doc.Subtype = typekey.OnBaseDocumentSubtype_Ext.get(templateDescriptor.getMetadataPropertyValue("subtype") as String)
      doc.Author = user
      doc.Description = templateDescriptor.Description
      doc.MimeType = templateDescriptor.MimeType
      doc.DeliveryStatus_TDIC = DocDlvryStatusType_TDIC.TC_SENDING
      doc.TemplateId_TDIC = aTemplateId
      doc.Event_TDIC = eventName
      doc.DateCreated = DateUtil.currentDate()
      doc.DateModified = DateUtil.currentDate()
      doc.Policy = aPolicyPeriod == null ? null : aPolicyPeriod.Policy
      //TODO: PolicyPeriod reference has been deleted from Document entity. Need Review.
      doc.PolicyPeriod = aPolicyPeriod
      doc.PrintOrder_TDIC = aPrintOrder
      doc.PortalStatus_TDIC = user.equalsIgnoreCase(_authorSystemGenerated) ? _portalStatusYes : _portalStatusNo
      anAccount.addDocument(doc)
      if (aPolicyPeriod != null){
        aPolicyPeriod.Policy.addDocument(doc)
      }
    } else {
      _logger.warn("TDIC_BCExstreamHelper#createDocumentStub() -No Template Descriptor found for Template Id " + aTemplateId)
    }
    _logger.trace("TDIC_BCExstreamHelper#createDocumentStub() - Created document" + doc.Name)
    _logger.trace("TDIC_BCExstreamHelper#createDocumentStub() - Exiting.")
  }

  /**
   * US644
   * 11/28/2014 shanem
   *
   * Creates a document stub for all template Id's
   */
  @Param("aTemplateIdList", "Templates to create stubs for")
  @Param("aPolicyPeriod", "PolicyPeriod for documents to be associated with")
  function createDocumentStubs(anEventName: String, anAccount: Account, aPolicyPeriod: PolicyPeriod): void {
    _logger.trace("TDIC_BCExstreamHelper#createDocumentStubs() - Entering.")

    if(ScriptParameters.TDIC_DataSource != typekey.DataSource_TDIC.TC_BILLINGCENTER.Code){
      return
    }

    var helper = new TDIC_ExstreamHelper()
    var effDate = aPolicyPeriod == null ? anAccount.EffectiveDate : aPolicyPeriod.EffectiveDate
    var templatesForEvent = helper.getEventTemplates(anEventName, effDate)
    templatesForEvent.eachKeyAndValue(\aTemplateId, aPrintOrder -> createDocumentStub(aTemplateId, aPrintOrder, anAccount, aPolicyPeriod, anEventName))
    _logger.debug("TDIC_BCExstreamHelper#createDocumentsForEvent() - Created " + templatesForEvent.Count + " document stubs.")
    _logger.trace("TDIC_BCExstreamHelper#createDocumentStubs() - Exiting.")
  }

  /**
   * US644
   * 11/28/2014 shanem
   *
   * Creates a message for documents to be sent asynchronously
   */
  @Param("aMessageContext", "The data type is: MessageContext")
  @Param("aBatchIdMap", "HashMap of templateIds and their corresponding document stub public Id")
  @Param("aDoc", "Document to create message for.")
  function createBatchMessage(aMessageContext: MessageContext, aBatchIdMap: TDIC_TemplateIds, batchDocuments: ArrayList<Document>): void {
    if (aBatchIdMap != null){
      var anAccount = aMessageContext.Root as Account
      var parameters = new java.util.HashMap(){"account" -> anAccount}
      var payload = TDIC_ExstreamXMLCreatorFactory
          .getExstreamXMLCreator(parameters)
          .generateTransactionXML(aBatchIdMap, aMessageContext.EventName, true, null, null)
      var message = aMessageContext.createMessage(payload)
      //US555, 11/13/14, shanem: the message root just needs to be any batch document for this policy
      message.MessageRoot = batchDocuments.first()
    }
  }

  /**
   * GW-526
   * 12/07/2015 Kesava Tavva
   *
   * Creates a message for documents to be sent asynchronously
   */
  @Param("aMessageContext", "The data type is: MessageContext")
  @Param("eventName", "The document event name, might be different than aMessageContext.EventName")
  @Param("aBatchIdMap", "HashMap of templateIds and their corresponding document stub public Id")
  @Param("aDoc", "Document to create message for.")
  function createBatchMessage(aMessageContext: MessageContext, eventName : String, aBatchIdMap: TDIC_TemplateIds, batchDocuments: List<Document>, aPolicyPeriod: PolicyPeriod): void {
    if (aBatchIdMap != null){
      var docRequestSource = new TDIC_DocumentRequestSource()
      docRequestSource.PolicyPeriodSource = aPolicyPeriod
      var anAccount = aPolicyPeriod.Policy.Account;

      if(aMessageContext.Root typeis DirectBillMoneyRcvd){
        docRequestSource.PaymentReversalInfo = aMessageContext.Root
      }
      var parameters : HashMap
      if(eventName.equalsIgnoreCase(TDIC_DocCreationEventType.TC_INVOICE.Code)){
        var anInvoice = aMessageContext.Root as Invoice
        parameters = new java.util.HashMap(){"invoices" -> {anInvoice}}
        parameters.put("policyPeriod",aPolicyPeriod)
      } else if(eventName.equalsIgnoreCase(TDIC_DocCreationEventType.TC_REMINDERNOTICE.Code)) {
        var policyDlnqProcess = aMessageContext.Root as PolicyDlnqProcess
        parameters = new java.util.HashMap(){"delinquencyProcess" -> policyDlnqProcess}
        parameters.put("policyPeriod",aPolicyPeriod)
      } else if(eventName.equalsIgnoreCase("PaymentReversalAPW") || eventName.equalsIgnoreCase("PaymentReversalAPWReturn") || eventName.equalsIgnoreCase("PaymentReversalCheck")) {
        var paymentMoneyReceived = aMessageContext.Root as PaymentMoneyReceived
        parameters = new java.util.HashMap(){"paymentMoneyReceived_TDIC" -> paymentMoneyReceived}
        parameters.put("policyPeriod",aPolicyPeriod)
      } else{
        parameters = new java.util.HashMap(){"account" -> anAccount}
      }

      var payload = TDIC_ExstreamXMLCreatorFactory
          .getExstreamXMLCreator(parameters)
          .generateTransactionXML(aBatchIdMap, eventName, true, null, null, docRequestSource)
      var message = aMessageContext.createMessage(payload)
      message.MessageRoot = batchDocuments.first()
    }
  }
  /**
   * US644
   * 05/14/2015 Kesava
   *
   * Creates a message for Policy documents to be sent asynchronously
   */
  @Param("aMessageContext", "The data type is: MessageContext")
  function createBatchMessageForPolicy(aMessageContext: MessageContext): void {
    var aPolicyPeriod = aMessageContext.Root as PolicyPeriod
    var documents = aPolicyPeriod.Bundle.InsertedBeans.whereTypeIs(entity.Document)
    createBatchMessageForDocuments(aMessageContext, aMessageContext.EventName, documents)
  }

  /**
   * US644
   * 05/14/2015 Kesava
   *
   * Creates a message for Policy documents to be sent asynchronously
   */
  @Param("aMessageContext", "The data type is: MessageContext")
  function createBatchMessageForInvoice(aMessageContext: MessageContext): void {
    var aInvoice = aMessageContext.Root as Invoice
    var documents = aInvoice.Bundle.InsertedBeans.whereTypeIs(entity.Document)
    createBatchMessageForDocuments(aMessageContext, aMessageContext.EventName, documents)
  }

  /**
   * US644
   * 05/14/2015 Kesava
   *
   * Creates a message for Policy documents to be sent asynchronously
   */
  @Param("aMessageContext", "The data type is: MessageContext")
  function createBatchMessageForPolicyDlnqProcess(aMessageContext: MessageContext): void {
    var policyDlnqProcess = aMessageContext.Root as PolicyDlnqProcess
    var documents = policyDlnqProcess.Bundle.InsertedBeans.whereTypeIs(entity.Document)
    createBatchMessageForDocuments(aMessageContext, aMessageContext.EventName, documents)
  }

  /**
   * US644
   * 05/14/2015 Kesava
   *
   * Creates a message for Account documents to be sent asynchronously
   */
  @Param("aMessageContext", "The data type is: MessageContext")
  function createBatchMessageForAccount(aMessageContext: MessageContext): void {
    var anAccount: Account
    if(aMessageContext.Root typeis DirectBillMoneyRcvd)
      anAccount = aMessageContext.Root.Account
    else
      anAccount = aMessageContext.Root as Account

    var documents = anAccount.Bundle.InsertedBeans.whereTypeIs(entity.Document)
    createBatchMessageForDocuments(aMessageContext, aMessageContext.EventName, documents)
  }

  /**
   * US644
   * 05/14/2015 Kesava
   *
   * Creates a message for Account documents to be sent asynchronously
   */
  @Param("aMessageContext", "The data type is: MessageContext")
  @Param("eventName", "The document event name, might be different from aMessageContext.eventName")
  @Param("documents", "Inserted documents from the bundle")
  public function createBatchMessageForDocuments(aMessageContext: MessageContext, eventName : String, documentList : List<Document>): void {
    var bcHelper = new TDIC_BCExstreamHelper()

    //Get documents created for this event name, with a status of 'Sending'.
    var batchDocs = documentList.where(\ elt -> (elt.Event_TDIC == eventName && elt.DeliveryStatus_TDIC == DocDlvryStatusType_TDIC.TC_SENDING))
        .orderBy( \ elt -> elt.PrintOrder_TDIC)
  // GWPS-2425 : Getting ArrayIndexOutofBounds Exception when there is no document in the sending status
    if(batchDocs.HasElements){
    //Create XML Model for TemplateIds
    var batchIds = new ArrayList<TDIC_TemplateidToDocPubIdMap>()
    var templates = new ArrayList<String>()
    batchDocs.each( \ doc -> {
      templates.add(doc.TemplateId_TDIC)
      batchIds.add(new TDIC_TemplateidToDocPubIdMap(doc.TemplateId_TDIC, doc))
    })

    var docSourcePolicyPeriod = batchDocs[0]?.PolicyPeriod
    var templateMap = new TDIC_TemplateIds(templates?.toTypedArray(), batchIds?.toTypedArray())
    //US555, 11/13/14, shanem: the message root just needs to be any batch document for this policy
    bcHelper.createBatchMessage(aMessageContext, eventName, templateMap, documentList, docSourcePolicyPeriod)
  }

  }

  override function createStub(aTemplateId: String, aPrintOrder: int, anEntity: Object, eventName: String, user: String) {
    createDocumentStub(aTemplateId, aPrintOrder, (anEntity as Account), eventName, user)
  }

  override function skipDocCreation(aTemplateId: String, anEntity: Object, aBundle: Bundle) {
    if (anEntity typeis Account) {
      var account = aBundle.add(anEntity)
      account.addEvent(aTemplateId)
    }
  }

  @Param("aMessageContext", "The data type is: MessageContext")
  function createBatchMessageForPaymentMoneyReceived(aMessageContext: MessageContext, customEvent : String): void {
    var paymentMoneyRcevd = aMessageContext.Root as PaymentMoneyReceived
    var period = (paymentMoneyRcevd as DirectBillMoneyRcvd).DirectBillPayment.ReversedDistItems[0]?.PolicyPeriod
    var documents = period.Account.Bundle.InsertedBeans?.whereTypeIs(entity.Document)
    createBatchMessageForDocuments(aMessageContext, customEvent, documents)
  }


}
