package tdic.bc.integ.plugins.hpexstream.model.tdic_bcaccountmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement AccountEnhancement : tdic.bc.integ.plugins.hpexstream.model.tdic_bcaccountmodel.Account {
  public static function create(object : entity.Account) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcaccountmodel.Account {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcaccountmodel.Account(object)
  }

  public static function create(object : entity.Account, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcaccountmodel.Account {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcaccountmodel.Account(object, options)
  }

}