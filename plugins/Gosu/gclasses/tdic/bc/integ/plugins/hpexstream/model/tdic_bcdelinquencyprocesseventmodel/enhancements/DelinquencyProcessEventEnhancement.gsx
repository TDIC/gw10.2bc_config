package tdic.bc.integ.plugins.hpexstream.model.tdic_bcdelinquencyprocesseventmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement DelinquencyProcessEventEnhancement : tdic.bc.integ.plugins.hpexstream.model.tdic_bcdelinquencyprocesseventmodel.DelinquencyProcessEvent {
  public static function create(object : entity.DelinquencyProcessEvent) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcdelinquencyprocesseventmodel.DelinquencyProcessEvent {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcdelinquencyprocesseventmodel.DelinquencyProcessEvent(object)
  }

  public static function create(object : entity.DelinquencyProcessEvent, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcdelinquencyprocesseventmodel.DelinquencyProcessEvent {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcdelinquencyprocesseventmodel.DelinquencyProcessEvent(object, options)
  }

}