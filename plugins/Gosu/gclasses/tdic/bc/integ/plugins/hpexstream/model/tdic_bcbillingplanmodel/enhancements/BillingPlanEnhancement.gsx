package tdic.bc.integ.plugins.hpexstream.model.tdic_bcbillingplanmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement BillingPlanEnhancement : tdic.bc.integ.plugins.hpexstream.model.tdic_bcbillingplanmodel.BillingPlan {
  public static function create(object : entity.BillingPlan) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcbillingplanmodel.BillingPlan {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcbillingplanmodel.BillingPlan(object)
  }

  public static function create(object : entity.BillingPlan, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcbillingplanmodel.BillingPlan {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcbillingplanmodel.BillingPlan(object, options)
  }

}