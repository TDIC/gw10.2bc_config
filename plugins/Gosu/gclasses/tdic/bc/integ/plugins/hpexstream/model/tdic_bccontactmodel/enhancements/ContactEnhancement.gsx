package tdic.bc.integ.plugins.hpexstream.model.tdic_bccontactmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement ContactEnhancement : tdic.bc.integ.plugins.hpexstream.model.tdic_bccontactmodel.Contact {
  public static function create(object : entity.Contact) : tdic.bc.integ.plugins.hpexstream.model.tdic_bccontactmodel.Contact {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bccontactmodel.Contact(object)
  }

  public static function create(object : entity.Contact, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.hpexstream.model.tdic_bccontactmodel.Contact {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bccontactmodel.Contact(object, options)
  }

}