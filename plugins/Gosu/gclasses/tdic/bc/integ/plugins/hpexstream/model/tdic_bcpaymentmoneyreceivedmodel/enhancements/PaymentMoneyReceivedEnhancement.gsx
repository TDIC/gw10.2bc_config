package tdic.bc.integ.plugins.hpexstream.model.tdic_bcpaymentmoneyreceivedmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PaymentMoneyReceivedEnhancement : tdic.bc.integ.plugins.hpexstream.model.tdic_bcpaymentmoneyreceivedmodel.PaymentMoneyReceived {
  public static function create(object : entity.PaymentMoneyReceived) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcpaymentmoneyreceivedmodel.PaymentMoneyReceived {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcpaymentmoneyreceivedmodel.PaymentMoneyReceived(object)
  }

  public static function create(object : entity.PaymentMoneyReceived, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcpaymentmoneyreceivedmodel.PaymentMoneyReceived {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcpaymentmoneyreceivedmodel.PaymentMoneyReceived(object, options)
  }

}