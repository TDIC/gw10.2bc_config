package tdic.bc.integ.plugins.hpexstream.model.accountmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement AccountEnhancement : tdic.bc.integ.plugins.hpexstream.model.accountmodel.Account {
  public static function create(object : entity.Account) : tdic.bc.integ.plugins.hpexstream.model.accountmodel.Account {
    return new tdic.bc.integ.plugins.hpexstream.model.accountmodel.Account(object)
  }

  public static function create(object : entity.Account, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.hpexstream.model.accountmodel.Account {
    return new tdic.bc.integ.plugins.hpexstream.model.accountmodel.Account(object, options)
  }

}