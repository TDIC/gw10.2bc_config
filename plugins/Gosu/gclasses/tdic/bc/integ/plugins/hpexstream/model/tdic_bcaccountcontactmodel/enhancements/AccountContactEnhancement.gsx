package tdic.bc.integ.plugins.hpexstream.model.tdic_bcaccountcontactmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement AccountContactEnhancement : tdic.bc.integ.plugins.hpexstream.model.tdic_bcaccountcontactmodel.AccountContact {
  public static function create(object : entity.AccountContact) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcaccountcontactmodel.AccountContact {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcaccountcontactmodel.AccountContact(object)
  }

  public static function create(object : entity.AccountContact, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcaccountcontactmodel.AccountContact {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcaccountcontactmodel.AccountContact(object, options)
  }

}