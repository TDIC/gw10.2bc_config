package tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoiceitemmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement InvoiceItemEnhancement : tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoiceitemmodel.InvoiceItem {
  public static function create(object : entity.InvoiceItem) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoiceitemmodel.InvoiceItem {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoiceitemmodel.InvoiceItem(object)
  }

  public static function create(object : entity.InvoiceItem, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoiceitemmodel.InvoiceItem {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcinvoiceitemmodel.InvoiceItem(object, options)
  }

}