package tdic.bc.integ.plugins.hpexstream.model.tdic_bcbusinessobjectmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_BCBusinessObjectEnhancement : tdic.bc.integ.plugins.hpexstream.model.tdic_bcbusinessobjectmodel.TDIC_BCBusinessObject {
  public static function create(object : tdic.bc.integ.plugins.hpexstream.bo.TDIC_BCBusinessObject) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcbusinessobjectmodel.TDIC_BCBusinessObject {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcbusinessobjectmodel.TDIC_BCBusinessObject(object)
  }

  public static function create(object : tdic.bc.integ.plugins.hpexstream.bo.TDIC_BCBusinessObject, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.hpexstream.model.tdic_bcbusinessobjectmodel.TDIC_BCBusinessObject {
    return new tdic.bc.integ.plugins.hpexstream.model.tdic_bcbusinessobjectmodel.TDIC_BCBusinessObject(object, options)
  }

}