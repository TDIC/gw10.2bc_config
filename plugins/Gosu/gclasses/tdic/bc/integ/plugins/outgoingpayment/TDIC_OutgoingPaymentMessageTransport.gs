package tdic.bc.integ.plugins.outgoingpayment

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.pl.exception.GWConfigurationException
uses org.slf4j.LoggerFactory
uses tdic.bc.integ.plugins.message.TDIC_MessagePlugin
uses tdic.bc.integ.plugins.outgoingpayment.dto.OutgoingPaymentWritableBean

uses java.sql.Connection
uses java.sql.SQLException

/**
 * US570 - Lawson Outgoing Payments Integration
 * 11/24/2014 Alvin Lee
 *
 * Implementation of a Messaging Transport plugin to process Outgoing Payments.  This will write a message to the
 * integration database with the outgoing payment details, to be later retrieved and written to the outgoing payment
 * flat file to be sent to Lawson.
 */
class TDIC_OutgoingPaymentMessageTransport extends TDIC_MessagePlugin {

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("OUTGOING_PAYMENT")

  /**
   * Notification email's subject for Outgoing Payment batch job failures.
   */
  public static final var EMAIL_SUBJECT_FAILURE: String = "Outgoing Payment Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * Key for looking up the notification email addresses from the integration database (value: BCInfraIssueNotificationEmail).
   */
  public static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "BCInfraIssueNotificationEmail"

  /**
   * Key for looking up the Integration DB URL from properties file
   */
  public static final var INTEGRATION_DB_URL_KEY : String = "IntDBURL"

  /**
   * Default constructor
   */
  construct() {
    _logger.debug("TDIC_OutgoingPaymentMessageTransport#construct - Creating Outgoing Payment Message Transport plugin")
  }

  override function send(msg: entity.Message, payload: String) {
    _logger.debug("TDIC_OutgoingPaymentMessageTransport#send - Payload: " + payload)

    // Check for duplicates
    var history = gw.api.database.Query.make(MessageHistory).compare(MessageHistory#SenderRefID, Equals, msg.SenderRefID).select().AtMostOneRow
    if (history != null) {
      _logger.warn("TDIC_OutgoingPaymentMessageTransport#send - Duplicate message found with SenderRefID: " + msg.SenderRefID)
      history = gw.transaction.Transaction.getCurrent().add(history)
      history.reportDuplicate()
      return
    }

    // Get Outgoing Payment XML model
    var outgoingPaymentModel = tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentwritablebeanmodel.OutgoingPaymentWritableBean.parse(payload)

    var dbConnection : Connection = null
    try {
      // Get a connection from DatabaseManager using the GWINT DB URL
      var dbUrl = PropertyUtil.getInstance().getProperty(INTEGRATION_DB_URL_KEY)
      dbConnection = DatabaseManager.getConnection(dbUrl)

      // Get the Outgoing Payment writable bean from the model
      var outgoingPaymentBean = new OutgoingPaymentWritableBean()
      outgoingPaymentBean.loadFromModel(outgoingPaymentModel)

      // Validate Vendor Number prior to writing to integration database; send email and report error if null
      if (outgoingPaymentBean.VendorNumber == null || outgoingPaymentBean.VendorNumber.Empty) {
        _logger.warn("TDIC_OutgoingPaymentMessageTransport#send - Vendor Number not found on outgoing payment with Public ID "
            + outgoingPaymentBean.PublicID)
        var failureNotificationEmail = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)
        if (failureNotificationEmail != null) {
          EmailUtil.sendEmail(failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
              "TDIC_OutgoingPaymentMessageTransport#send - Outgoing Payment messaging has found a disbursement whose "
                  + " contact has no vendor number.  Account number for the disbursement: "
                  + outgoingPaymentBean.AccountNumber + ". PNI/Policy Holders Name: " + outgoingPaymentBean.AccountName +
                  ". Amount of Failed Disbursement: $" + outgoingPaymentBean.Amount +
                  ". Outgoing Payment Public ID: " + outgoingPaymentBean.PublicID
                  + ". The Contact entity must be updated with the Vendor Number before retrying this message.")
          _logger.info("TDIC_OutgoingPaymentMessageTransport#send - Failure email notification sent. Reporting error.")
          msg.ErrorDescription = "No vendor number for contact"
          msg.reportError(ErrorCategory.TC_VENDERNUMBERERROR_TDIC)
          return
        }
        else {
          throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
              + FAILURE_NOTIFICATION_EMAIL_KEY + "' from integration database.")
        }
      }

      // Execute and commit DB Connection
      if (dbConnection != null) {
        outgoingPaymentBean.executeCreate(dbConnection)
        dbConnection.commit()
      }
      else {
        _logger.error("TDIC_OutgoingPaymentMessageTransport#send - Unable to get DB connection using JDBC URL: " + dbUrl)
        throw new SQLException("Unable to get DB connection using JDBC URL: " + dbUrl)
      }

      //Acknowledge message
      msg.reportAck()
    } catch(sqle : SQLException) {
      // Handle database error; no need to call reportError here since we are re-throwing the exception
      _logger.error("Database connectivity/execution error persisting entity", sqle)
      if (dbConnection != null) {
        // Attempt to rollback
        try {
          dbConnection.rollback()
        } catch (rbe: Exception) {
          _logger.error("Exception while rolling back DB", rbe)
        }
      }
      throw sqle
    } catch (e : Exception) {
      // Handle other errors; no need to call reportError here since we are re-throwing the exception
      _logger.error("Unexpected error persisting entity", e)
      if (dbConnection != null) {
        // Attempt to rollback
        try {
          dbConnection.rollback()
        } catch (rbe: Exception) {
          _logger.error("Exception while rolling back DB", rbe)
        }
      }
      throw e
    } finally {
      // Close database connection
      if (dbConnection != null) {
        _logger.debug("Closing DB connection")
        try {
          DatabaseManager.closeConnection(dbConnection)
        } catch (e : Exception) {
          _logger.error("Exception on closing DB (continuing)", e)
        }
      }
    }
  }

  override function shutdown() {
    _logger.info("TDIC_OutgoingPaymentMessageTransport#shutdown - Entering")
  }

  override function resume() {
    _logger.info("TDIC_OutgoingPaymentMessageTransport#resume - Entering")
  }

  override property  set DestinationID(p0: int) {
  }
}