package tdic.bc.integ.plugins.outgoingpayment

uses gw.api.gx.GXOptions
uses gw.plugin.messaging.MessageRequest
uses org.slf4j.LoggerFactory
uses tdic.bc.integ.plugins.outgoingpayment.dto.OutgoingPaymentWritableBean
uses entity.Contact

/**
 * US570 - Lawson Outgoing Payments Integration
 * 11/24/2014 Alvin Lee
 *
 * Implementation of a Messaging Request plugin to process Outgoing Payments.  This is mainly for setting the
 * SenderRefID field on the message.
 */
class TDIC_OutgoingPaymentMessageRequest implements MessageRequest {

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("OUTGOING_PAYMENT")

  /**
   * Sets the SenderRefID on the message.
   */
  @Param("msg","the original message")
  @Returns("A String for the payload, which will be unchanged in this case")
  override function beforeSend(msg: entity.Message): String {
    _logger.debug("TDIC_OutgoingPaymentMessageRequest#beforeSend() - Entering")
    if (msg.SenderRefID == null) {
      msg.SenderRefID = msg.PublicID
    }
    _logger.debug("TDIC_OutgoingPaymentMessageRequest#beforeSend() - SenderRefID: " + msg.SenderRefID)

    // Modify payload to late bind Vendor Number, in case a vendor number is not available at the time of messaging.
    _logger.debug("TDIC_OutgoingPaymentMessageRequest#beforeSend() - Late binding Vendor Number...")
    var outgoingPaymentModel = tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentwritablebeanmodel.OutgoingPaymentWritableBean.parse(msg.Payload)
    var outgoingPaymentBean = new OutgoingPaymentWritableBean()
    outgoingPaymentBean.loadFromModel(outgoingPaymentModel)
    if (msg.MessageRoot typeis OutgoingDisbPmnt && msg.MessageRoot.Disbursement typeis AccountDisbursement) {
      var acctDisb = msg.MessageRoot.Disbursement
      // Determine the Contact for the payment
      var unapplied = acctDisb.UnappliedFund
      var paymentContact : Contact = null
      if (acctDisb.UnappliedFund.Policy != null) {
        _logger.debug("TDIC_OutgoingPaymentMessageRequest#beforeSend() - Using Primary Named Insured from Policy: "
            + acctDisb.UnappliedFund.Policy)
        paymentContact = unapplied.Policy.LatestPolicyPeriod.PrimaryInsured.Contact
      }
      else {
        _logger.debug("TDIC_OutgoingPaymentMessageRequest#beforeSend() - Using Primary Payer from Account: "
            + acctDisb.Account.AccountNumber)
        paymentContact = acctDisb.Account.PrimaryPayer.Contact
      }
      _logger.debug("TDIC_OutgoingPaymentMessageRequest#beforeSend() - Mapping Vendor Number: "
          + paymentContact.VendorNumber)
      outgoingPaymentBean.VendorNumber = paymentContact.VendorNumber
    }
    // Build XML Model options object
    var options = new GXOptions()
    options.Incremental = false // Send all values, not just changed ones
    options.Verbose = true // Send all fields, even nulls
    return new tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentwritablebeanmodel.OutgoingPaymentWritableBean(outgoingPaymentBean, options).asUTFString()
  }

  override function afterSend(p0: entity.Message) {
  }

  override function shutdown() {
  }

  override function suspend() {
  }

  override function resume() {
  }

  override property  set DestinationID(p0: int) {
  }
}