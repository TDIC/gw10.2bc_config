package tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentwritablebeanmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement OutgoingPaymentWritableBeanEnhancement : tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentwritablebeanmodel.OutgoingPaymentWritableBean {
  public static function create(object : tdic.bc.integ.plugins.outgoingpayment.dto.OutgoingPaymentWritableBean) : tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentwritablebeanmodel.OutgoingPaymentWritableBean {
    return new tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentwritablebeanmodel.OutgoingPaymentWritableBean(object)
  }

  public static function create(object : tdic.bc.integ.plugins.outgoingpayment.dto.OutgoingPaymentWritableBean, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentwritablebeanmodel.OutgoingPaymentWritableBean {
    return new tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentwritablebeanmodel.OutgoingPaymentWritableBean(object, options)
  }

}