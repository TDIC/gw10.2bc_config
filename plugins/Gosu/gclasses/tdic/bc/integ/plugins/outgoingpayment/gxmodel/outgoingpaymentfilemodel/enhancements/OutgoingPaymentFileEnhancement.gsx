package tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentfilemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement OutgoingPaymentFileEnhancement : tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentfilemodel.OutgoingPaymentFile {
  public static function create(object : tdic.bc.integ.plugins.outgoingpayment.dto.OutgoingPaymentFile) : tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentfilemodel.OutgoingPaymentFile {
    return new tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentfilemodel.OutgoingPaymentFile(object)
  }

  public static function create(object : tdic.bc.integ.plugins.outgoingpayment.dto.OutgoingPaymentFile, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentfilemodel.OutgoingPaymentFile {
    return new tdic.bc.integ.plugins.outgoingpayment.gxmodel.outgoingpaymentfilemodel.OutgoingPaymentFile(object, options)
  }

}