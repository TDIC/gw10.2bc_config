package tdic.bc.integ.plugins.outgoingpayment.dto

uses gw.api.util.DateUtil
uses org.slf4j.LoggerFactory
uses com.tdic.util.database.IWritableBeanInfo
uses java.text.SimpleDateFormat
uses java.util.Map
uses java.sql.Connection
uses java.sql.PreparedStatement

/**
 * US570 - Lawson Outgoing Payments Integration
 * 11/24/2014 Alvin Lee
 *
 * Concrete implementation of class describing the Outgoing Payment fields to persist to the integration database.
 */
class OutgoingPaymentBeanInfo<T> extends IWritableBeanInfo<T> {

  /**
   * Class level logger
   */
  private static final var _logger = LoggerFactory.getLogger("TDIC_INTEGRATION")
  private static final var ProcessedDate_ColumnSQLName: String = "ProcessedDate"

  // Map to bind fields to export to the SQL database. Fields not listed here will not be exported
  public static var _fields:Map<String, Map<String, Object>> = {
      "AccountName" -> {FIELD_SQL_NAME -> "AccountName",
          FIELD_BINDER -> stringFieldBinder
      },
      "AccountNumber" -> {FIELD_SQL_NAME -> "AccountNumber",
          FIELD_BINDER -> stringFieldBinder
      },
      "Amount" -> {FIELD_SQL_NAME -> "Amount",
          FIELD_BINDER -> bigDecimalFieldBinder
      },
      "DueDate" -> {FIELD_SQL_NAME -> "DueDate",
          FIELD_BINDER -> stringFieldBinder
      },
      "LOB" -> {FIELD_SQL_NAME -> "LOB",
          FIELD_BINDER -> stringFieldBinder
      },
      "PolicyNumber" -> {FIELD_SQL_NAME -> "PolicyNumber",
          FIELD_BINDER -> stringFieldBinder
      },
      "StateCode" -> {FIELD_SQL_NAME -> "StateCode",
          FIELD_BINDER -> stringFieldBinder
      },
      "VendorNumber" -> {FIELD_SQL_NAME -> "VendorNumber",
          FIELD_BINDER -> stringFieldBinder
      }
  }

  /**
   * Default constructor with no parameters.
   */
  construct() {
  }

  /**
   * Abstract method implementation, provides the SQL name of the table for this object
   */
  override property get TableName():String {
    return "OutgoingPaymentIntegration"
  }

  /**
   * Abstract method implementation, provides the Gosu name of the object's Public ID property for use with reflection calls
   */
  override property get IDColumnName(): String {
    return "PublicID"
  }

  /**
   * Abstract method implementation, provides the SQL name of the Public ID property in the database table
   */
  override property get IDColumnSQLName(): String {
    return "OutgoingPaymentPublicID"
  }

  /**
   * Abstract method implementation, provides the SQL name of the Processed field in the database table
   */
  override property get ProcessedColumnSQLName(): String {
    return "Processed"
  }

  /**
   * Abstract method implementation to parameterize a call in order to create, bind, and return a JDBC
   * prepared statement that will insert a new object when it is executed
   *
   * @param conn - database connection
   * @param entity - Entity that is to be inserted into the database
   * @return PreparedStatement that will insert values based on the provided entity object
   */
  override function createAndBindCreateSQL(conn:Connection, entity : T) : PreparedStatement {
    return createAndBindCreateSQL(conn, entity, _fields)
  }

  /**
   * Abstract method implementation to parameterize a call in order to create, bind, and return a JDBC
   * prepared statement that will retrieve an object when it is executed
   *
   * @param conn - database connection
   * @param entity - Entity with populated primary key to be used to retrieve values from the database
   * @return PreparedStatement that will return values based on the provided entity object
   */
  override function createAndBindRetrieveSQL(conn:Connection, entity : T) : PreparedStatement {
    return createAndBindRetrieveSQL(conn, entity, _fields)
  }

  /**
   * Abstract method implementation to parameterize a call in order to create, bind, and return a JDBC
   * prepared statement that will update an object when it is executed
   *
   * @param conn - database connection
   * @param entity - Entity that is to be updated in the database
   * @return PreparedStatement that will update values based on the provided entity object
   */
  override function createAndBindUpdateSQL(conn:Connection, entity : T) : PreparedStatement {
    return createAndBindUpdateSQL(conn, entity, _fields)
  }

  /**
   * Abstract method implementation to populate the fields of an entity from a GX model representation.
   */
  @Param("entity", "The object to populate")
  @Param("model", "The GX model containing the data to populate into the entity")
  override function loadFromModel(entity : Object, model : Object) {
    loadFromModel(entity, model, _fields)
  }

  /**
   * Abstract method implementation to parameterize a call in order to create, bind, and return a JDBC
   * prepared statement that will update an object when it is executed
   *
   * @param conn - database connection
   * @param entity - Entity that is to be updated in the database
   * @return PreparedStatement that will insert values based on the provided entity object
   */
  override function markProcessed(conn : Connection, entity : T) : PreparedStatement {
    return markProcessedAndUpdateProcessedDate(conn, entity)
  }

  /**
   * Abstract method implementation to parameterize a call in order to create, bind, and return a JDBC
   * prepared statement that will retrieve all objects of our entity type that have not yet been written to an export file
   *
   * @param conn - database connection
   * @return PreparedStatement that will insert values based on the provided entity object
   */
  override function createRetrieveAllSQL(conn: Connection): PreparedStatement {
    return createRetrieveAllSQL(conn, _fields)
  }

  /**
   * markProcessedAndUpdateProcessedDate method is to set Processed Flag and ProcessedDate.
   * Implemented based on the requirements mentioned in the JIRA GWPS-482.
   *
   * @param conn - Database connection to use
   * @param entity - entity to persist
   * @return a PreparedStatement for updating the entity as processed
   */
  private function markProcessedAndUpdateProcessedDate(conn: Connection, entity: T):PreparedStatement {
    _logger.debug("UpdateSQL for ${entity.Class.SimpleName}")

    var builder = new StringBuilder()
    var sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
    var processedDate = sdf.format(DateUtil.currentDate())
    builder.append("update ").append(TableName).append(" set ").append(ProcessedColumnSQLName).append(" = ?")
        .append(", ").append(ProcessedDate_ColumnSQLName).append(" = ?")
    builder.append(" where ").append(IDColumnSQLName).append("=?")

    var stmt = conn.prepareStatement(builder.toString())
    _logger.debug("Generated SQL is: ${builder.toString()}")
    _logger.debug("key is ${entity[IDColumnName]}")

    stmt.setBoolean(1, true)
    stmt.setString(2, processedDate)
    stmt.setString(3, entity[IDColumnName] as String)

    return stmt
  }

}