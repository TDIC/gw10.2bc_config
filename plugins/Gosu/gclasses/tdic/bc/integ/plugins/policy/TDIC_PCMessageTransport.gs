package tdic.bc.integ.plugins.policy

uses gw.pl.logging.LoggerCategory
uses gw.xml.ws.WsdlConfig
uses tdic.bc.integ.plugins.message.TDIC_MessagePlugin
uses wsi.remote.gw.webservice.pc.pc1000.activitymodel.types.complex.Activity
uses wsi.remote.gw.webservice.pc.pc1000.policyapi.PolicyAPI
uses wsi.remote.gw.webservice.pc.pc1000.policyapi.enums.ActivityType
uses java.lang.Exception
uses java.util.Date
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: skiriaki
 * Date: 1/12/15
 * Time: 10:35 AM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_PCMessageTransport  extends TDIC_MessagePlugin {
  private static var _logger = LoggerFactory.getLogger("TDIC_INTEGRATION")
  public static final var EVENT_PCREINSTATE_REQUEUST : String = "ReinstatePolicy"
  public static final var EVENT_BC_CANCEL_PROCESS_DATE_PUSH : String = "PushCancelProcessDate"

  /**
   * Default constructor
   */
  construct() {
    _logger.debug("TDIC_PCMessageTransport#construct - Creating TDIC PC Message Transport plugin")
  }

  override function send(msg: Message, payload: String) {
    _logger.debug("TDIC_PCMessageTransport#send() - Entering")

    var policyPeriod = msg.MessageRoot as PolicyPeriod
    switch (msg.EventName){
      case (EVENT_PCREINSTATE_REQUEUST):
        // Call PC web service to add an activity to rescind a cancellation or reinstate cancelled policy
        var activityType: ActivityType = ActivityType.Approval
        var activityPatternCode = "approve_reinstatement"
        var activityFields: Activity = new wsi.remote.gw.webservice.pc.pc1000.activitymodel.types.complex.Activity()
        //activityFields.ApprovalIssue //only set this text if this is an Approval Issue
        activityFields.Subject = "Non-pay Reinstatement Request"
        activityFields.Description = "Reinstatement request triggered by customer payment after cancellation effective date"
        activityFields.TargetDate = gw.api.util.DateUtil.currentDate().addDays(6)
        activityFields.EscalationDate = gw.api.util.DateUtil.currentDate().addDays(8)
        activityFields.Mandatory = false
        var activity_PolicyPeriod = new wsi.remote.gw.webservice.pc.pc1000.activitymodel.anonymous.elements.Activity_PolicyPeriod()
        activity_PolicyPeriod.PolicyNumber = policyPeriod.PolicyNumber
        activityFields.PolicyPeriod = activity_PolicyPeriod
        try {
          callPCPolicyAPI(\ api -> {api.addActivityFromPatternAndAutoAssign(msg.Payload, activityType, activityPatternCode, activityFields)})
        } catch (e: Exception) {
          _logger.error("TDIC_PCMessageTransport#send() - ReinstatePolicy exception calling PC web service", e)
        }
        break;
      case (EVENT_BC_CANCEL_PROCESS_DATE_PUSH):
        _logger.debug("TDIC_PCMessageTransport#send - pushing BC Cancel Process Date to PC")
        var tmpCancelProcessDate : Date
        for (each in policyPeriod.getActiveDelinquencyProcesses(typekey.DelinquencyReason.TC_PASTDUE).union(policyPeriod.getActiveDelinquencyProcesses(typekey.DelinquencyReason.TC_NOTTAKEN))){
          if (each.NextEvent.EventName == typekey.DelinquencyEventName.TC_CANCELINPAS){
            tmpCancelProcessDate = each.NextEvent.TargetDate
          } else {
            tmpCancelProcessDate = null
          }
          try {
            callPCPolicyAPI(\ api -> {api.setPolicyBCCancelProcessDate(policyPeriod.PolicyNumber, tmpCancelProcessDate)})
          } catch (e: Exception) {
            _logger.error("TDIC_PCMessageTransport#send() - BCCancelProcessDate exception calling PC web service", e)
          }
        }
        break
    }

    //Acknowledge message
    msg.reportAck()

    _logger.debug("TDIC_PCMessageTransport#send() - Exiting")
  }

  protected function callPCPolicyAPI(call : block(p : PolicyAPI)){
    //var config = createWsiConfig()
    var api = new PolicyAPI()
    try{
      call(api)
    } catch(e : wsi.remote.gw.webservice.pc.pc1000.policyrenewalapi.faults.AlreadyExecutedException){
      // ignore this duplicated call
    }
  }

  private function createWsiConfig() : WsdlConfig{
    // TODO mvu: platform will add more support to wsi webservice in studio so that we don't need
    // to hard code the password like this. Instead, we probably will enter the password in some config file in studio
    var config = new WsdlConfig()
    config.Guidewire.Locale = User.util.CurrentLocale.Code
    config.Guidewire.Authentication.Username = "su"
    config.Guidewire.Authentication.Password = "gw"
    return config
  }

  override function shutdown() {
    _logger.debug("TDIC_PCMessageTransport#shutdown()")
  }

  override function resume() {
    _logger.debug("TDIC_PCMessageTransport#resume()")
  }

  override property  set DestinationID(p0: int) {
  }
}