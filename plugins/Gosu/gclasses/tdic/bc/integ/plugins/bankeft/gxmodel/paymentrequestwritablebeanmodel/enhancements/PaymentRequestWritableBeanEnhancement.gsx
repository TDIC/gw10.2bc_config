package tdic.bc.integ.plugins.bankeft.gxmodel.paymentrequestwritablebeanmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement PaymentRequestWritableBeanEnhancement : tdic.bc.integ.plugins.bankeft.gxmodel.paymentrequestwritablebeanmodel.PaymentRequestWritableBean {
  public static function create(object : tdic.bc.integ.plugins.bankeft.dto.BankEFT.PaymentRequestWritableBean) : tdic.bc.integ.plugins.bankeft.gxmodel.paymentrequestwritablebeanmodel.PaymentRequestWritableBean {
    return new tdic.bc.integ.plugins.bankeft.gxmodel.paymentrequestwritablebeanmodel.PaymentRequestWritableBean(object)
  }

  public static function create(object : tdic.bc.integ.plugins.bankeft.dto.BankEFT.PaymentRequestWritableBean, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.bankeft.gxmodel.paymentrequestwritablebeanmodel.PaymentRequestWritableBean {
    return new tdic.bc.integ.plugins.bankeft.gxmodel.paymentrequestwritablebeanmodel.PaymentRequestWritableBean(object, options)
  }

}