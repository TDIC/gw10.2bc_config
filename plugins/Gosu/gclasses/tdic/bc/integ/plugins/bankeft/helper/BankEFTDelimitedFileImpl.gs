package tdic.bc.integ.plugins.bankeft.helper

uses java.util.ArrayList
uses java.text.SimpleDateFormat
uses gw.api.util.DateUtil
uses com.tdic.util.delimitedfile.DelimitedFileImpl
uses java.util.List

/**
 * An implementation of the Delimited File Utility used specifically for the generation of the Bank/EFT flat file.
 */
class BankEFTDelimitedFileImpl extends DelimitedFileImpl {

  /**
   * Overriding the default record types of the header lines, specifically for the Bank/EFT flat file.
   */
  @Returns("A List<String> containing the record types for the header lines")
  override property get HeaderRecordTypes() : List<String> {
    var recordTypes = new ArrayList<String>()
    recordTypes.add("File Header")
    recordTypes.add("Batch Header")
    return recordTypes
  }

  /**
   * Overriding the default record types of the trailer lines, specifically for the Bank/EFT flat file.
   */
  @Returns("A List<String> containing the record types for the trailer lines")
  override property get TrailerRecordTypes() : List<String> {
    var recordTypes = new ArrayList<String>()
    recordTypes.add("Batch Trailer")
    recordTypes.add("File Trailer")
    return recordTypes
  }

  /**
   * Formats the current date for the Bank/EFT flat file.
   */
  @Returns("A String containing the formatted current date for the Bank/EFT flat file")
  public static function formatCurrentDate() : String {
    var currentDateTime = DateUtil.currentDate()
    var dateFormat = new SimpleDateFormat("yyMMdd")
    return dateFormat.format(currentDateTime)
  }

  /**
   * Formats the current date in file creation date format for the Bank/EFT flat file.
   */
  @Returns("A String containing the current date in file creation date format for the Bank/EFT flat file")
  public static function formatFileCreationDate() : String {
    var currentDateTime = DateUtil.currentDate()
    var dateFormat = new SimpleDateFormat("yyMMdd")
    return dateFormat.format(currentDateTime)
  }

  /**
   * Formats the current time for the Bank/EFT flat file.
   */
  @Returns("A String containing the formatted current time for the Bank/EFT flat file")
  public static function formatCurrentTime() : String {
    var currentDateTime = DateUtil.currentDate()
    var timeFormat = new SimpleDateFormat("HHmm")
    return timeFormat.format(currentDateTime)
  }

  /**
   * Formats the next business date to current date, for the Bank/EFT flat file.
   */
  @Returns("A String containing the formatted next business date to current date for the Bank/EFT flat file")
  public static function formatNextBusinessDate() : String {
    var nextBusinessDate = DateUtil.addBusinessDays(Date.Today, 1, HolidayTagCode.TC_CORPORATE_TDIC);
    var dateFormat = new SimpleDateFormat("yyMMdd")
    return dateFormat.format(nextBusinessDate)
  }

}