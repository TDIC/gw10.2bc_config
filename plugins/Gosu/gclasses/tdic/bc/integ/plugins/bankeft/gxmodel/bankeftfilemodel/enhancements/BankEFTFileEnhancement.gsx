package tdic.bc.integ.plugins.bankeft.gxmodel.bankeftfilemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement BankEFTFileEnhancement : tdic.bc.integ.plugins.bankeft.gxmodel.bankeftfilemodel.BankEFTFile {
  public static function create(object : tdic.bc.integ.plugins.bankeft.dto.BankEFT.BankEFTFile) : tdic.bc.integ.plugins.bankeft.gxmodel.bankeftfilemodel.BankEFTFile {
    return new tdic.bc.integ.plugins.bankeft.gxmodel.bankeftfilemodel.BankEFTFile(object)
  }

  public static function create(object : tdic.bc.integ.plugins.bankeft.dto.BankEFT.BankEFTFile, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.bankeft.gxmodel.bankeftfilemodel.BankEFTFile {
    return new tdic.bc.integ.plugins.bankeft.gxmodel.bankeftfilemodel.BankEFTFile(object, options)
  }

}