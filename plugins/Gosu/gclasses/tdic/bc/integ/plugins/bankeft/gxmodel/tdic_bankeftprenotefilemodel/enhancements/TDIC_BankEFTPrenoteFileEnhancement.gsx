package tdic.bc.integ.plugins.bankeft.gxmodel.tdic_bankeftprenotefilemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_BankEFTPrenoteFileEnhancement : tdic.bc.integ.plugins.bankeft.gxmodel.tdic_bankeftprenotefilemodel.TDIC_BankEFTPrenoteFile {
  public static function create(object : tdic.bc.integ.plugins.bankeft.dto.BankPrenote.TDIC_BankEFTPrenoteFile) : tdic.bc.integ.plugins.bankeft.gxmodel.tdic_bankeftprenotefilemodel.TDIC_BankEFTPrenoteFile {
    return new tdic.bc.integ.plugins.bankeft.gxmodel.tdic_bankeftprenotefilemodel.TDIC_BankEFTPrenoteFile(object)
  }

  public static function create(object : tdic.bc.integ.plugins.bankeft.dto.BankPrenote.TDIC_BankEFTPrenoteFile, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.bankeft.gxmodel.tdic_bankeftprenotefilemodel.TDIC_BankEFTPrenoteFile {
    return new tdic.bc.integ.plugins.bankeft.gxmodel.tdic_bankeftprenotefilemodel.TDIC_BankEFTPrenoteFile(object, options)
  }

}