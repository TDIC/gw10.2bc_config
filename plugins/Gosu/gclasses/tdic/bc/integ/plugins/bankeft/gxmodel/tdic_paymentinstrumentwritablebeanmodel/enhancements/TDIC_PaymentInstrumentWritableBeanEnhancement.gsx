package tdic.bc.integ.plugins.bankeft.gxmodel.tdic_paymentinstrumentwritablebeanmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement TDIC_PaymentInstrumentWritableBeanEnhancement : tdic.bc.integ.plugins.bankeft.gxmodel.tdic_paymentinstrumentwritablebeanmodel.TDIC_PaymentInstrumentWritableBean {
  public static function create(object : tdic.bc.integ.plugins.bankeft.dto.BankPrenote.TDIC_PaymentInstrumentWritableBean) : tdic.bc.integ.plugins.bankeft.gxmodel.tdic_paymentinstrumentwritablebeanmodel.TDIC_PaymentInstrumentWritableBean {
    return new tdic.bc.integ.plugins.bankeft.gxmodel.tdic_paymentinstrumentwritablebeanmodel.TDIC_PaymentInstrumentWritableBean(object)
  }

  public static function create(object : tdic.bc.integ.plugins.bankeft.dto.BankPrenote.TDIC_PaymentInstrumentWritableBean, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.bankeft.gxmodel.tdic_paymentinstrumentwritablebeanmodel.TDIC_PaymentInstrumentWritableBean {
    return new tdic.bc.integ.plugins.bankeft.gxmodel.tdic_paymentinstrumentwritablebeanmodel.TDIC_PaymentInstrumentWritableBean(object, options)
  }

}