package tdic.bc.integ.plugins.bankeft

uses gw.plugin.messaging.MessageRequest
uses gw.api.system.BCLoggerCategory
uses org.slf4j.LoggerFactory

/**
 * US568 - Bank/EFT Integration
 * 10/10/2014 Alvin Lee
 *
 * Implementation of a Messaging Request plugin to process Payment Requests.  This is mainly for setting the
 * SenderRefID field on the message.
 */
class TDIC_BankEFTMessageRequest implements MessageRequest {

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("BANK_EFT")

  /**
   * Sets the SenderRefID on the message.
   */
  @Param("msg","the original message")
  @Returns("A String for the payload, which will be unchanged in this case")
  override function beforeSend(msg: entity.Message): String {
    _logger.debug("TDIC_BankEFTMessageRequest#beforeSend() - Entering")
    if (msg.SenderRefID == null) {
      msg.SenderRefID = msg.PublicID
    }
    _logger.debug("SenderRefID: " + msg.SenderRefID)
    return msg.Payload
  }

  override function afterSend(p0: entity.Message) {
  }

  override function shutdown() {
  }

  override function suspend() {
  }

  override function resume() {
  }

  override property  set DestinationID(p0: int) {
  }
}