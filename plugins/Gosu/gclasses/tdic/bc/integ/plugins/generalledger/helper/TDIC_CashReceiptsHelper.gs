package tdic.bc.integ.plugins.generalledger.helper

uses gw.api.system.BCLoggerCategory
uses java.lang.Exception
uses gw.xml.XmlElement
uses tdic.bc.integ.plugins.generalledger.dto.CashReceiptsBucket
uses tdic.bc.integ.plugins.generalledger.dto.CashReceiptsBucketGroup
uses tdic.bc.integ.plugins.generalledger.dto.CashReceiptsFile
uses tdic.bc.integ.plugins.generalledger.dto.CashReceiptsTransactionLineWritableBean
uses tdic.bc.integ.plugins.generalledger.dto.GLRuntimeException
uses com.tdic.util.delimitedfile.DelimitedFileImpl
uses java.util.List
uses org.slf4j.LoggerFactory

/**
 * US570 - General Ledger Integration
 * 12/19/2014 Alvin Lee
 *
 * Helper class with methods to process cash receipts file.  These are mainly used to calculate amounts for each cash
 * receipts bucket.
 */
class TDIC_CashReceiptsHelper {

  /**
   * Standard logger for General Ledger
   */
  private static final var _logger = LoggerFactory.getLogger("GENERAL_LEDGER")

  /**
   * Process the Unapplied T-Account for the SuspPymtTransaction or DirectBillMoneyReceivedTxn.
   * This will take into account whether or not the payment is reversed, the payment method, and whether it is a debit
   * line or credit line when determining which bucket to add or subtract the amount.
   */
  @Param("transLine", "The CashReceiptsTransactionLineWritableBean containing the information about the transaction line")
  @Param("bucketGroup", "The group of cash receipts buckets, holding the bucket that will be updated")
  @Throws(GLRuntimeException, "If unable to map to the correct bucket based on the payment method")
  public static function processUnapplied(transLine : CashReceiptsTransactionLineWritableBean, bucketGroup : CashReceiptsBucketGroup) {
    _logger.debug("TDIC_CashReceiptsHelper#processUnapplied - Entering")
    var bucket : CashReceiptsBucket
    // Check for reversals.
    // For suspense payments, must distinguish between true reversal or just suspense being applied.
    // For money received, must distinguish between actual NSF payment reversals vs. reversals due to other reasons (e.g. payment being moved).
    // If NSF reversal, add to the NSF line.  Otherwise, decrease the amount from the DEP line.
    if (((transLine.TransactionSubtype == typekey.Transaction.TC_SUSPPYMTTRANSACTION.Code && transLine.DirectBillMoneyRcvdPublicID == null)
        || (transLine.TransactionSubtype == typekey.Transaction.TC_DIRECTBILLMONEYRECEIVEDTXN.Code && transLine.PaymentReversalReason != PaymentReversalReason.TC_MOVED.Code && transLine.PaymentReversalReason != PaymentReversalReason.TC_COMPANYERROR_TDIC.Code))
        && transLine.Description.endsWith("(Reversed)")) {
      _logger.debug("TDIC_CashReceiptsHelper#processUnapplied - Treating line as modification to NSF reversal bucket")
      // Treat as NSF
      switch (transLine.PaymentMethod) {
        case PaymentMethod.TC_CHECK.Code:
            bucket = bucketGroup.NameBucketMap.get("IHDUnappliedReversed")
            break
        case PaymentMethod.TC_CREDITCARD.Code:
            if (transLine.CreditCardType == CreditCardType_TDIC.TC_V.Code || transLine.CreditCardType == CreditCardType_TDIC.TC_M.Code) {
              bucket = bucketGroup.NameBucketMap.get("VisaMCUnappliedReversed")
            }
            else if (transLine.CreditCardType == CreditCardType_TDIC.TC_A.Code) {
              bucket = bucketGroup.NameBucketMap.get("AmexUnappliedReversed")
            }
            else {
              _logger.warn("TDIC_CashReceiptsHelper#processUnapplied - Cannot map bucket - Invalid credit card type: " + transLine.CreditCardType)
              throw new GLRuntimeException("Cannot map bucket - Invalid credit card type: " + transLine.CreditCardType)
            }
            break
        case PaymentMethod.TC_LOCKBOX.Code:
            bucket = bucketGroup.NameBucketMap.get("LockboxUnappliedReversed")
            break
        case PaymentMethod.TC_ACH.Code:
            bucket = bucketGroup.NameBucketMap.get("EFTUnappliedReversed")
            break
        default:
            _logger.warn("TDIC_CashReceiptsHelper#processUnapplied - Cannot map bucket - Unsupported payment method: " + transLine.PaymentMethod)
            throw new GLRuntimeException("Cannot map bucket - Unsupported payment method: " + transLine.PaymentMethod)
      }
      if (bucket.MappedTAccountCode == null) {
        bucket.MappedTAccountCode = transLine.MappedTAccountName
      }
      if (transLine.LineItemType == LedgerSide.TC_DEBIT.Code) {
        _logger.debug("TDIC_CashReceiptsHelper#processUnapplied - Adding " + transLine.Amount + " to NSF Unapplied for payment method: " + transLine.PaymentMethod)
        bucket.TotalBucketAmount = bucket.TotalBucketAmount.add(transLine.Amount)
      }
      else if (transLine.LineItemType == LedgerSide.TC_CREDIT.Code) {
        _logger.debug("TDIC_CashReceiptsHelper#processUnapplied - Subtracting " + transLine.Amount + " from NSF Unapplied for payment method: " + transLine.PaymentMethod)
        bucket.TotalBucketAmount = bucket.TotalBucketAmount.subtract(transLine.Amount)
      }
    }
    else {
      _logger.debug("TDIC_CashReceiptsHelper#processUnapplied - Treating line as modification to DEP bucket")
      // Treat as DEP
      switch (transLine.PaymentMethod) {
        case PaymentMethod.TC_CHECK.Code:
            bucket = bucketGroup.NameBucketMap.get("IHDUnapplied")
            break
        case PaymentMethod.TC_CREDITCARD.Code:
            if (transLine.CreditCardType == CreditCardType_TDIC.TC_V.Code || transLine.CreditCardType == CreditCardType_TDIC.TC_M.Code) {
              bucket = bucketGroup.NameBucketMap.get("VisaMCUnapplied")
            }
            else if (transLine.CreditCardType == CreditCardType_TDIC.TC_A.Code) {
              bucket = bucketGroup.NameBucketMap.get("AmexUnapplied")
            }
            else {
              _logger.warn("TDIC_CashReceiptsHelper#processUnapplied - Cannot map bucket - Invalid credit card type: " + transLine.CreditCardType)
              throw new GLRuntimeException("Cannot map bucket - Invalid credit card type: " + transLine.CreditCardType)
            }
            break
        case PaymentMethod.TC_LOCKBOX.Code:
            bucket = bucketGroup.NameBucketMap.get("LockboxUnapplied")
            break
        case PaymentMethod.TC_ACH.Code:
            bucket = bucketGroup.NameBucketMap.get("EFTUnapplied")
            break
        default:
            _logger.warn("TDIC_CashReceiptsHelper#processUnapplied - Cannot map bucket - Unsupported payment method: " + transLine.PaymentMethod)
            throw new GLRuntimeException("Cannot map bucket - Unsupported payment method: " + transLine.PaymentMethod)
      }
      if (bucket.MappedTAccountCode == null) {
        bucket.MappedTAccountCode = transLine.MappedTAccountName
      }
      if(transLine.MappedTAccountName == "6790,0000"){
        //GWPS-926
        bucket.DepOrNsf = "WTH"
      }
      if (transLine.LineItemType == LedgerSide.TC_CREDIT.Code) {
        _logger.debug("TDIC_CashReceiptsHelper#processUnapplied - Adding " + transLine.Amount + " to DEP Unapplied for payment method: " + transLine.PaymentMethod)
        bucket.TotalBucketAmount = bucket.TotalBucketAmount.add(transLine.Amount)
      }
      else if (transLine.LineItemType == LedgerSide.TC_DEBIT.Code) {
        _logger.debug("TDIC_CashReceiptsHelper#processUnapplied - Subtracting " + transLine.Amount + " from DEP Unapplied for payment method: " + transLine.PaymentMethod)
        _logger.debug("PaymentReversalReason : " + transLine.PaymentReversalReason)

        bucket.TotalBucketAmount = bucket.TotalBucketAmount.subtract(transLine.Amount)
      }
    }
  }

  /**
   * Converts data to the lines that can be written to the destination flat file.
   */
  @Param("flatFileData", "The object containing the data to write to the destination flat file")
  @Returns("A List<String> containing the liens to be written to the destination flat file")
  @Throws(Exception, "If there are any unexpected errors encoding the data")
  public static function convertDataToFlatFileLines(flatFileData : CashReceiptsFile) : List<String> {
    var gxModel = new tdic.bc.integ.plugins.generalledger.gxmodel.cashreceiptsfilemodel.CashReceiptsFile(flatFileData)
    _logger.debug("TDIC_CashReceiptsHelper#convertDataToFlatFileLines - Cash Receipts GX Model Generated: ")
    _logger.debug(gxModel.asUTFString())
    var xml = XmlElement.parse(gxModel.asUTFString())
    var df = new DelimitedFileImpl()
    return df.encode("LawsonCashReceiptsSpec.xlsx", xml)
  }

  static function updateDEPorWTH(cashReceiptsBuckets : List<CashReceiptsBucket>) : void {
    for(bucket in cashReceiptsBuckets){
      if(bucket.TotalBucketAmount < 0){
        bucket.DepOrNsf = "WTH"
        bucket.TotalBucketAmount = - bucket.TotalBucketAmount
      }
    }
  }
}