package tdic.bc.integ.plugins.generalledger.gxmodel.gltransactionmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GLTransactionEnhancement : tdic.bc.integ.plugins.generalledger.gxmodel.gltransactionmodel.GLTransaction {
  public static function create(object : tdic.bc.integ.plugins.generalledger.dto.GLTransaction) : tdic.bc.integ.plugins.generalledger.gxmodel.gltransactionmodel.GLTransaction {
    return new tdic.bc.integ.plugins.generalledger.gxmodel.gltransactionmodel.GLTransaction(object)
  }

  public static function create(object : tdic.bc.integ.plugins.generalledger.dto.GLTransaction, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.generalledger.gxmodel.gltransactionmodel.GLTransaction {
    return new tdic.bc.integ.plugins.generalledger.gxmodel.gltransactionmodel.GLTransaction(object, options)
  }

}