package tdic.bc.integ.plugins.generalledger.dto

uses java.lang.RuntimeException

/**
 * US570 - General Ledger Integration
 * 12/22/2014 Alvin Lee
 *
 * Custom exception to catch Cash Receipts file errors but not fail the cash receipts batch entirely.
 */
class GLRuntimeException extends RuntimeException {

  construct(message : String) {
    super(message)
  }

}