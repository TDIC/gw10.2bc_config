package tdic.bc.integ.plugins.generalledger.gxmodel.cashreceiptsfilemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement CashReceiptsFileEnhancement : tdic.bc.integ.plugins.generalledger.gxmodel.cashreceiptsfilemodel.CashReceiptsFile {
  public static function create(object : tdic.bc.integ.plugins.generalledger.dto.CashReceiptsFile) : tdic.bc.integ.plugins.generalledger.gxmodel.cashreceiptsfilemodel.CashReceiptsFile {
    return new tdic.bc.integ.plugins.generalledger.gxmodel.cashreceiptsfilemodel.CashReceiptsFile(object)
  }

  public static function create(object : tdic.bc.integ.plugins.generalledger.dto.CashReceiptsFile, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.generalledger.gxmodel.cashreceiptsfilemodel.CashReceiptsFile {
    return new tdic.bc.integ.plugins.generalledger.gxmodel.cashreceiptsfilemodel.CashReceiptsFile(object, options)
  }

}