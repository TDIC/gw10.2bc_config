package tdic.bc.integ.plugins.generalledger.gxmodel.glfilemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement GLFileEnhancement : tdic.bc.integ.plugins.generalledger.gxmodel.glfilemodel.GLFile {
  public static function create(object : tdic.bc.integ.plugins.generalledger.dto.GLFile) : tdic.bc.integ.plugins.generalledger.gxmodel.glfilemodel.GLFile {
    return new tdic.bc.integ.plugins.generalledger.gxmodel.glfilemodel.GLFile(object)
  }

  public static function create(object : tdic.bc.integ.plugins.generalledger.dto.GLFile, options : gw.api.gx.GXOptions) : tdic.bc.integ.plugins.generalledger.gxmodel.glfilemodel.GLFile {
    return new tdic.bc.integ.plugins.generalledger.gxmodel.glfilemodel.GLFile(object, options)
  }

}