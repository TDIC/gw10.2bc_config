package tdic.bc.config.enhancement

uses java.util.Date
uses gw.api.system.BCConfigParameters
uses gw.api.upgrade.Coercions
uses org.slf4j.LoggerFactory
uses tdic.bc.integ.plugins.hpexstream.util.TDIC_BCExstreamHelper

enhancement PolicyDlnqProcessEnhancement : PolicyDlnqProcess {

  // Estimates the calculation effective date.  Ideally, this should be a web service call to PolicyCenter.
  // For now, it will be implemented in BillingCenter.
  public function calcCancelEffectiveDate_TDIC() : Date {
    var effectiveDate : Date;
    var effectiveTime = Coercions.makeDateFrom(BCConfigParameters.PASEffectiveTime.Value)
    var effectiveTimeMidnight = effectiveTime.trimToMidnight()
    var timeAfterMidnight = effectiveTime.Time - effectiveTimeMidnight.Time

    if (this.Reason == DelinquencyReason.TC_NOTTAKEN) {
      effectiveDate = new Date(this.PolicyPeriod.PolicyPerEffDate.Time + timeAfterMidnight);
    } else if (this.Reason == DelinquencyReason.TC_PASTDUE) {
      effectiveDate = Date.Today.addDays(13);   // Cancel effective date is 13 days in the future
      if (effectiveDate.before(this.PolicyPeriod.PolicyPerEffDate)) {
        // Effective date is before the period started, delay to period start
        effectiveDate = this.PolicyPeriod.PolicyPerEffDate;
      }
      effectiveDate = new Date(effectiveDate.Time + timeAfterMidnight);
    } else {
      throw "EstimatedCancellationEffectiveDate_TDIC not implemented for delinquency reason " + this.Reason;
    }

    return effectiveDate;
  }

  // Cancellation effective date.  Added here because sending it as part of the CancelInfo_TDIC caused issues
  // for John's programming in HP Exstream.
  public property get CancelEffectiveDate() : Date {
    return this.CancelInfo_TDIC.EffectiveDate;
  }


  /**
   * US644
   * 11/28/2014 shanem
   *
   * Get template Ids for event name and create appropriate documents on the PolicyPeriod
   */
  @Param("eventName", "Event Name to create documents for")
  function createDocumentsForEvent(eventName: String, aPolicyPeriod:PolicyPeriod=null) {
    var _exstreamLogger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
    _exstreamLogger.trace("TDIC_PolicyPeriodEnhancement#createDocumentsForEvent(${eventName}) - Entering.")
    var bcHelper = new TDIC_BCExstreamHelper()

    var acct = this.Account
    acct = gw.transaction.Transaction.getCurrent().add(acct)

    bcHelper.createDocumentStubs(eventName, acct, this.PolicyPeriod)
    _exstreamLogger.trace("TDIC_PolicyPeriodEnhancement#createDocumentsForEvent(${eventName}) - Notifying Exstream Message Queue.")
    this.addEvent(eventName)
    _exstreamLogger.trace("TDIC_PolicyPeriodEnhancement#createDocumentsForEvent(${eventName}) - Exiting.")
  }

}
