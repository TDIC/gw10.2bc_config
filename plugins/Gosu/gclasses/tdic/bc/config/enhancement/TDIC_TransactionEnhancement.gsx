package tdic.bc.config.enhancement

enhancement TDIC_TransactionEnhancement : Transaction {

  property get OutgoingDisplayName_TDIC(): String {

    if (this typeis TransferTransaction){
      var output = "Funds Transfer to "
      var lineItem = this?.LineItems?.firstWhere(\elt -> elt.Type == LedgerSide.TC_CREDIT)?.TAccount?.TAccountOwnerTypeNameTAccountName
      var policyNumber = lineItem?.split(":")?.last()?.remove("Designated Unapplied")?.remove("(")?.remove(")")?.trim()
      if (policyNumber != null){
        return output + policyNumber
      }
    }
    return null
  }

  property get IncomingDisplayName_TDIC(): String {

    if (this typeis TransferTransaction){
      var output = "Funds Transfer from "
      var lineItem = this?.LineItems?.firstWhere(\elt -> elt.Type == LedgerSide.TC_DEBIT)?.TAccount?.TAccountOwnerTypeNameTAccountName
      var policyNumber = lineItem?.split(":")?.last()?.remove("Designated Unapplied")?.remove("(")?.remove(")")?.trim()
      if (policyNumber != null){
        return output + policyNumber
      }
    }
    return null
  }

}
