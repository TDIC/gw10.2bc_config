package tdic.bc.config.enhancement

enhancement TDIC_DateEnhancement : java.util.Date {

  property get YearOfDate_TDIC() : String {
    return String.format("%04d",{this.YearOfDate})
  }

  property get MonthOfYear_TDIC() : String {
    return String.format("%02d",{this.MonthOfYear})
  }

  property get DayOfMonth_TDIC() : String {
    return String.format("%02d",{this.DayOfMonth})
  }

  property get Hour_TDIC() : String {
    return String.format("%02d",{this.Hour})
  }

  property get HourOfDay_TDIC() : String {
    return String.format("%02d",{this.HourOfDay})
  }

  property get Minute_TDIC() : String {
    return String.format("%02d",{this.Minute})
  }

  property get Second_TDIC() : String {
    return String.format("%02d",{this.Second})
  }

  property get MillisecondInSecond_TDIC() : String {
    return String.format("%03d",{this.MillisecondInSecond})
  }
}
