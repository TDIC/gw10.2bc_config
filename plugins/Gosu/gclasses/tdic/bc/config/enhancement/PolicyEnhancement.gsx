package tdic.bc.config.enhancement

uses java.util.Date
uses java.util.ArrayList

uses gw.api.util.DateUtil
uses gw.api.web.invoice.InvoiceStreamView
uses java.util.List

enhancement PolicyEnhancement : entity.Policy {
  /**
   * Return the policy period as of the specified date.
  */
  public function findPolicyPeriodByAsOfDate_TDIC(asOfDate : Date) : PolicyPeriod {
    var periods = this.PolicyPeriods.where (\pp -> pp.PolicyPerEffDate.beforeOrEqual(asOfDate)
                                               and pp.PolicyPerExpirDate.after(asOfDate));
    return periods.orderByDescending(\pp -> pp.BoundDate).first();
  }

  /**
   * Get the active delinquency processes.
   */
  public property get ActiveDelinquencyProcesses_TDIC() : List<PolicyDlnqProcess> {
    var retval = new ArrayList<PolicyDlnqProcess>();
    for (delinquency in this.Account.ActiveDelinquencyProcesses) {
      if (delinquency typeis PolicyDlnqProcess) {
        if (delinquency.PolicyPeriod.Policy == this) {
          retval.add(delinquency);
        }
      }
    }
    return retval;
  }

  /**
   * Get the invoicing day of month.
   */
  public property get InvoiceDayOfMonth_TDIC() : int {
    var dayOfMonth : int;
    var invoiceStreamView : InvoiceStreamView;

    var policyPeriods = this.PolicyPeriods.where (\pp -> pp.CreateTime != null)
                                          .orderByDescending (\pp -> pp.CreateTime);

    for (policyPeriod in policyPeriods) {
      if (policyPeriod.OverridingInvoiceStream != null) {
        invoiceStreamView = new InvoiceStreamView (policyPeriod.OverridingInvoiceStream);
        if (invoiceStreamView.OverrideAnchorDates) {
          // Invoice day of month was overridden on a prior term, use that value.
          dayOfMonth = invoiceStreamView.OverridingAnchorDateViews[0].DayOfMonth;
          break;
        }
      }
    }

    // If day of month was not found on any existing policy period, use the account version.
    if (dayOfMonth == 0) {
      if (this.Account != null) {
        dayOfMonth = this.Account.InvoiceDayOfMonth;
      }
    }

    return dayOfMonth;
  }

  /**
   * Returns the future policy period if exists
   */
  property get FuturePolicyPeriod_Ext() : PolicyPeriod {
    return this.PolicyPeriods.orderByDescending(\ pp -> pp.EffectiveDate).firstWhere(\pp -> pp.EffectiveDate.compareIgnoreTime(DateUtil.currentDate()) >= 0)
  }
}
