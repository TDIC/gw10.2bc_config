package tdic.bc.config.enhancement

uses tdic.bc.integ.plugins.hpexstream.util.TDIC_BCExstreamHelper
uses java.util.Date
uses gw.api.database.IQueryBeanResult

uses java.math.BigDecimal
uses gw.api.database.Query
uses gw.pl.currency.MonetaryAmount
uses java.util.List
uses gw.api.database.QuerySelectColumns
uses gw.api.path.Paths
uses org.slf4j.LoggerFactory

/**
 * US64: Maintain Policy Payment Schedule
 * 09/25/2014 Vicente
 *
 * PolicyPeriod Enhancement for TDIC.
 */
enhancement PolicyPeriodEnhancement: entity.PolicyPeriod {
  /**
   * US64: Maintain Policy Payment Schedule
   * 09/25/2014 Vicente
   *
   * TDIC's definition of an expired (worker's comp specific) policy period is if the final audit has completed and if
   * all invoices have been billed.
   */
  @Returns("True if the current date is after the expiration date")
  property get IsExpired_TDIC() : boolean {
    if (LoggerFactory.getLogger("Rules").DebugEnabled) {
      LoggerFactory.getLogger("Rules").debug("PolicyPeriodEnhancement#IsExpired_TDIC - Invoices for Policy Period: " + this)
      for (invoice in this.Invoices) {
        LoggerFactory.getLogger("Rules").debug("PolicyPeriodEnhancement#IsExpired_TDIC - Date: " + invoice.Date + " | Amount: "
            + invoice.Amount + " | Amount Due: " + invoice.AmountDue + " | Status: " + invoice.Status)
      }
    }
    var policyPeriodInvoices = this.Invoices.where( \ invoice -> invoice.Amount_amt != 0 || invoice.AmountDue_amt != 0)
    if (LoggerFactory.getLogger("Rules").DebugEnabled) {
      LoggerFactory.getLogger("Rules").debug("PolicyPeriodEnhancement#IsExpired_TDIC - Filtered Invoices for Policy Period: " + this)
      for (invoice in policyPeriodInvoices) {
        LoggerFactory.getLogger("Rules").debug("PolicyPeriodEnhancement#IsExpired_TDIC - Date: " + invoice.Date + " | Amount: "
            + invoice.Amount + " | Amount Due: " + invoice.AmountDue + " | Status: " + invoice.Status)
      }
    }
    return this.FinalAuditComplete_TDIC && !policyPeriodInvoices.hasMatch( \ invoice -> invoice.Status == InvoiceStatus.TC_PLANNED)
  }

  property get FinalAuditBilledDisplayName() : String {
    if(this.IsExpired_TDIC){
      return YesNo.TC_YES.DisplayName
    }
    return YesNo.TC_NO.DisplayName
  }

  /**
   * US561: Policy Feed to Pivotal/AS400
   * 12/01/2014 Vicente
   *
   * Returns the date of the first payment performed to this policy period or null if there was no a first payment
   */
  @Returns("Returns the date of the first payment performed to this policy period or null if there was no a first payment")
  property get FirstPaymentDate(): Date {
    var firstPolicyPeriodPayment = this.Policy.Account.findReceivedPaymentMoneysSortedByReceivedDate().orderBy(
      QuerySelectColumns.path(Paths.make(entity.PaymentMoneyReceived#AppliedDate))
    )
        .firstWhere(\payment -> not payment.Reversed and payment typeis DirectBillMoneyRcvd and payment.DirectBillPayment?.DistItems?.hasMatch(\elt1 -> elt1?.InvoiceItem?.PolicyPeriod == this))
    return firstPolicyPeriodPayment?.ReceivedDate
  }

  /**
   * US644
   * 11/28/2014 shanem
   *
   * Get template Ids for event name and create appropriate documents on the PolicyPeriod
   */
  @Param("eventName", "Event Name to create documents for")
  function createDocumentsForEvent(eventName: String, aPolicyPeriod:PolicyPeriod=null) {
    var _exstreamLogger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
    _exstreamLogger.trace("TDIC_PolicyPeriodEnhancement#createDocumentsForEvent(${eventName}) - Entering.")
    var bcHelper = new TDIC_BCExstreamHelper()

    var acct = this.Account;
      acct = gw.transaction.Transaction.getCurrent().add(acct);

    bcHelper.createDocumentStubs(eventName, acct, aPolicyPeriod != null ? aPolicyPeriod : this)
    _exstreamLogger.trace("TDIC_PolicyPeriodEnhancement#createDocumentsForEvent(${eventName}) - Notifying Exstream Message Queue.")
    acct.addEvent(eventName)
    _exstreamLogger.trace("TDIC_PolicyPeriodEnhancement#createDocumentsForEvent(${eventName}) - Exiting.")
  }

  /**
   * US1049 - Multiple APW Withdrawal Dates
   * 3/4/2015 Alvin Lee
   *
   * Set the Invoice Day of Month for APW policy periods based on the effective date.  This reads the script parameter
   * values for both possible APW Invoice Day of Month values.
   */
  @Returns("An integer representing the Invoice Day of Month based on the policy period effective date")
  property get APWInvoiceDayOfMonth_TDIC() : int {
    var effectiveDayOfMonth = this.PolicyPerEffDate.DayOfMonth
    if (effectiveDayOfMonth >= 11 && effectiveDayOfMonth <= 25) {
      return ScriptParameters.APWInvoiceDayOfMonth1
    }
    else {
      return ScriptParameters.APWInvoiceDayOfMonth2
    }
  }

  /**
   * US1186 - Attach Policies to Activities
   * 3/18/2015 Alvin Lee
   *
   * Query activities for the PolicyPeriod.
   */
  @Returns("A collection of activities associated with this policy period")
  property get Activities_TDIC() : IQueryBeanResult<Activity> {
    var activityQuery = gw.api.database.Query.make(Activity)
    activityQuery.compare(Activity#PolicyPeriod, Equals, this)
    return activityQuery.select()
  }

  /**
   * US1363 - Cancel payment requests upon policy cancellation
   * 3/24/2015 Alvin Lee
   *
   * Gets any outstanding payment requests associated with this policy period's account, where all policy periods
   * associated with the invoice on the payment request have been cancelled.
   */
  @Returns("A collection of payment requests associated with this policy period, tied to policies that are all cancelled")
  property get OpenPaymentRequestsWithCancelledPolicyPeriods_TDIC() : List<PaymentRequest> {
    return this.Account.PaymentRequests.where( \ paymentRequest -> paymentRequest.Invoice != null
        && paymentRequest.Invoice.Policies.allMatch( \ policyPeriod -> policyPeriod == this || policyPeriod.Canceled)
        && (paymentRequest.Status == PaymentRequestStatus.TC_CREATED || paymentRequest.Status == PaymentRequestStatus.TC_REQUESTED))
  }

  /**
   * US570 - General Ledger Integration
   * 4/27/2015 Alvin Lee
   *
   * Gets the amount paid towards billed invoices only for this policy period
   */
  @Returns("A BigDecimal providing the amount paid towards billed invoices only for this policy period")
  property get PaidAmountTowardBilledInvoices() : BigDecimal {
   /*GWPS 1675 - Modified to pick the amount from the dist items on the payment rather than from invoice to balace the GL*/
    var billedorDueAmount = 0bd
    var dbmrs = this.Account.findReceivedPaymentMoneysSortedByReceivedDate().where(\elt -> elt.DistributedDenorm and elt.UnappliedFund.PolicyPeriod_TDIC != null and elt.UnappliedFund.PolicyPeriod_TDIC == this)//

    for(dbmr  in dbmrs) {
      var billedItems = dbmr.DirectBillPayment.DistItems.where(\distItem ->  (distItem.InvoiceItem.Invoice.Status == InvoiceStatus.TC_BILLED or distItem.InvoiceItem.Invoice.Status == InvoiceStatus.TC_DUE)
                                                                            and distItem.InvoiceItem.PolicyPeriod == this)
       billedorDueAmount += billedItems.sum( Currency.TC_USD, \elt -> elt.GrossAmountToApply).Amount
    }
    return billedorDueAmount
  }

  /**
   * US570 - General Ledger Integration
   * 4/27/2015 Alvin Lee
   *
   * Gets the amount paid towards unbilled invoices only for this policy period
   */
  @Returns("A BigDecimal providing the amount paid towards unbilled invoices only for this policy period")
  property get PaidAmountTowardUnbilledInvoices() : BigDecimal {
   /*GWPS 1675 - Modified to pick the amount from the dist items on the payment rather than from invoice to balace the GL*/
      var unBilledAmount = 0bd
    var dbmrs = this.Account.findReceivedPaymentMoneysSortedByReceivedDate().where(\elt -> elt.DistributedDenorm and elt.UnappliedFund.PolicyPeriod_TDIC != null and elt.UnappliedFund.PolicyPeriod_TDIC == this)//

    for(dbmr  in dbmrs) {
      var unBilledItems = dbmr.DirectBillPayment.DistItems.where(\distItem ->  (distItem.InvoiceItem.Invoice.Status == InvoiceStatus.TC_PLANNED)
          and distItem.InvoiceItem.PolicyPeriod == this)
      unBilledAmount += unBilledItems.sum( Currency.TC_USD, \elt -> elt.GrossAmountToApply).Amount
    }
    return unBilledAmount


  }

  /**
   * GWPS 2105 - Added to calculate the undistributed amount from both undistributed payment and
   * as well as from partially distributed payments
   * @return unditributed amount for this policy period
   */
  property get undistributedAmount_TDIC() : BigDecimal {
    var totalUndistributedAmount = 0bd
    var undistPaymentAmount = 0bd
    var undistributedDMRs = this.Account.findReceivedPaymentMoneysSortedByReceivedDate()
        .where(\elt -> !elt.DistributedDenorm and
            elt.UnappliedFund.PolicyPeriod_TDIC != null and
            elt.UnappliedFund.PolicyPeriod_TDIC == this and
            elt.ReversalDate == null)
    undistPaymentAmount = undistributedDMRs?.sum(\dmr -> (dmr as PaymentMoneyReceived)?.Amount)
    var distributedDMRs = this.Account.findReceivedPaymentMoneysSortedByReceivedDate()
        .where(\elt -> elt.DistributedDenorm and
            elt.UnappliedFund.PolicyPeriod_TDIC != null and
            elt.UnappliedFund.PolicyPeriod_TDIC == this and
            elt.ReversalDate == null)
    var totalUndistAmountOfEachPayment = 0bd
    distributedDMRs.each(\dmr -> {
      var payment = dmr.BaseDist as DirectBillPayment
      var distAmount = dmr.DistributedDenorm ? payment.NetDistributedToInvoiceItems : 0bd.ofCurrency(this.Currency)
      var undistAmountOfEachPayment = (dmr as PaymentMoneyReceived).Amount - distAmount
      totalUndistAmountOfEachPayment = +undistAmountOfEachPayment
    })
    totalUndistributedAmount = undistPaymentAmount + totalUndistAmountOfEachPayment
    return totalUndistributedAmount
  }

  /**
   * GW393 - BC to CC for Claims verification
   * 10/20/2015 Hermia Kho
   *
   * Return a boolean value true if the first invoice for this policy period is paid in full.
   */
  @Returns("A Boolean returning True of the first Invoice for this policy period is paid in full")
  property get FirstInvoicePaidInFull_TDIC (): Boolean {
    var issuancePaymentTotalAmt : BigDecimal = 0.0
    var renewalPaymentTotalAmt : BigDecimal = 0.0
    var reinstatementPaymentTotalAmt : BigDecimal = 0.0
    var firstInvoicePaidInFull : Boolean = false

    /**
     * GW 1401 - Refactor Paid Through Date for better claims performance.
     * 03/22/2016 Praneethk
     */
    var queryobjBaseDistItem = Query.make(BaseDistItem)
    var policyPeriodQuery = Query.make(PolicyPeriod).compare("PolicyNumber",Equals,this.PolicyNumber).select().toTypedArray()
    queryobjBaseDistItem.compareIn("PolicyPeriod",policyPeriodQuery)

    var resultsobjBaseDistItem = queryobjBaseDistItem.select().where( \ elt -> elt.InvoiceItem.Charge.BillingInstruction typeis Issuance
        or elt.InvoiceItem.Charge.BillingInstruction typeis Renewal
        or elt.InvoiceItem.Charge.BillingInstruction typeis Reinstatement
        or elt.InvoiceItem.Charge.BillingInstruction typeis Cancellation)

    for (eachResultsobjBaseDistItem in resultsobjBaseDistItem) {
      if (eachResultsobjBaseDistItem.BaseDist.BaseMoneyReceived typeis DirectBillMoneyRcvd) {
        if (eachResultsobjBaseDistItem.InvoiceItem.Charge.BillingInstruction typeis Issuance) {
          issuancePaymentTotalAmt += eachResultsobjBaseDistItem.GrossAmountToApply_amt
        } else {
        if (eachResultsobjBaseDistItem.InvoiceItem.Charge.BillingInstruction typeis Renewal) {
          renewalPaymentTotalAmt += eachResultsobjBaseDistItem.GrossAmountToApply_amt
        } else {
        if (eachResultsobjBaseDistItem.InvoiceItem.Charge.BillingInstruction typeis Reinstatement) {
          reinstatementPaymentTotalAmt += eachResultsobjBaseDistItem.GrossAmountToApply_amt
        } else {
        if (eachResultsobjBaseDistItem.InvoiceItem.Charge.BillingInstruction typeis Cancellation and
          issuancePaymentTotalAmt > 0.0) {
          issuancePaymentTotalAmt += eachResultsobjBaseDistItem.GrossAmountToApply_amt
        } else {
        if (eachResultsobjBaseDistItem.InvoiceItem.Charge.BillingInstruction typeis Cancellation and
          renewalPaymentTotalAmt > 0.0) {
          renewalPaymentTotalAmt += eachResultsobjBaseDistItem.GrossAmountToApply_amt
        } else {
        if (eachResultsobjBaseDistItem.InvoiceItem.Charge.BillingInstruction typeis Cancellation and
          reinstatementPaymentTotalAmt > 0.0){
          reinstatementPaymentTotalAmt += eachResultsobjBaseDistItem.GrossAmountToApply_amt
        }
        }
        }
        }
        }
        }
      }  //* end typeis DirectBillMoneyRcvd *//
    } //* end for loop *//
    if (issuancePaymentTotalAmt > 0.0) {
      var issuanceInvoiceItem = queryobjBaseDistItem.select().where( \ elt -> elt.PolicyPeriod.PolicyNumber == this.PolicyNumber
         and elt.InvoiceItem.Charge.BillingInstruction typeis Issuance )
      var sortIssuanceInvoice = issuanceInvoiceItem.orderBy( \ elt -> elt.InvoiceItem.Invoice.EventDate)
      if (issuancePaymentTotalAmt >= sortIssuanceInvoice.first().InvoiceItem.Invoice.Amount_amt){
        firstInvoicePaidInFull = true
      }
    } else {
    if (renewalPaymentTotalAmt > 0.0) {
      var issuanceInvoiceItem = queryobjBaseDistItem.select().where( \ elt -> elt.PolicyPeriod.PolicyNumber == this.PolicyNumber
         and elt.InvoiceItem.Charge.BillingInstruction typeis Renewal )
      var sortIssuanceInvoice = issuanceInvoiceItem.orderBy( \ elt -> elt.InvoiceItem.Invoice.EventDate)
      if (renewalPaymentTotalAmt >= sortIssuanceInvoice.first().InvoiceItem.Invoice.Amount_amt){
        firstInvoicePaidInFull = true
      }
    } else {
    if (reinstatementPaymentTotalAmt > 0.0) {
       var issuanceInvoiceItem = queryobjBaseDistItem.select().where( \ elt -> elt.PolicyPeriod.PolicyNumber == this.PolicyNumber
          and elt.InvoiceItem.Charge.BillingInstruction typeis Reinstatement )
       var sortIssuanceInvoice = issuanceInvoiceItem.orderBy( \ elt -> elt.InvoiceItem.Invoice.EventDate)
       if (reinstatementPaymentTotalAmt >= sortIssuanceInvoice.first().InvoiceItem.Invoice.Amount_amt){
          firstInvoicePaidInFull = true
       }
    }
    }
    }
    return firstInvoicePaidInFull
  }
  /**
   * GW393 - BC to CC for Claims verification
   * 10/20/2015 Hermia Kho
   *
   * Gets a boolean value true if there is 'policy change' context invoice not fully paid in this policy period.
   */
  @Returns("A Boolean returning True if the billing instruction 'Policy Change' inovice is not fully paid")
  property get UnpaidPolicyChangeInvoice_TDIC (): Boolean {
  var policyChangePaymentTotalAmt : BigDecimal = 0.0
  var unpaidPolicyChangeInvoice : Boolean = false
  /**
   * GW 1401 - Refactor Paid Through Date for better claims performance.
   * 03/22/2016 Praneethk
   */

  var queryobjBaseDistItem = Query.make(BaseDistItem)
  var policyPeriodQuery = Query.make(PolicyPeriod).compare("PolicyNumber",Equals,this.PolicyNumber).select().toTypedArray()
  queryobjBaseDistItem.compareIn("PolicyPeriod",policyPeriodQuery)

  var resultsobjBaseDistItem = queryobjBaseDistItem.select().where( \ elt -> elt.InvoiceItem.Charge.BillingInstruction typeis PolicyChange)

  if (resultsobjBaseDistItem.Count <= 0) {
   //  there's no payments found with billing instruction 'PolicyChange. Need to check for unpaid invoice with billing instruction = 'PolicyChange'
    var resultsobjInvoiceItem = this.InvoiceItems.where( \ elt -> elt.Charge.BillingInstruction typeis PolicyChange)
    if (resultsobjInvoiceItem.Count > 0) {
      unpaidPolicyChangeInvoice = true
    }
  } else {
  //  there is payment found with billinginstruction = PolicyChange. Only sum amount with BaseMoneyReceived typeis DirectBillMoneyRcvd
  if (resultsobjBaseDistItem.Count > 0) {
    for (eachResultsobjBaseDistItem in resultsobjBaseDistItem) {
      if (eachResultsobjBaseDistItem.BaseDist.BaseMoneyReceived typeis DirectBillMoneyRcvd) {
        if (eachResultsobjBaseDistItem.InvoiceItem.Charge.BillingInstruction typeis PolicyChange) {
          policyChangePaymentTotalAmt += eachResultsobjBaseDistItem.GrossAmountToApply_amt
        }
      }
    }
  }
  }
  // If the payment for PolicyChange > 0, compare with the Invoice associated with it. If the payment >= invoice amount, then the inovoice
  // is fully paid, else it is not fully paid
  if (policyChangePaymentTotalAmt > 0.0) {
    var policyChangeInvoiceItem = queryobjBaseDistItem.select().where( \ elt -> elt.PolicyPeriod.PolicyNumber == this.PolicyNumber
       and elt.InvoiceItem.Charge.BillingInstruction typeis PolicyChange)
    var sortPolicyChangeInvoice = policyChangeInvoiceItem.orderBy( \ elt -> elt.InvoiceItem.Invoice.EventDate)
    if (policyChangePaymentTotalAmt >= sortPolicyChangeInvoice.first().InvoiceItem.Invoice.Amount_amt) {

      unpaidPolicyChangeInvoice = false
    } else {
      unpaidPolicyChangeInvoice = true
    }
  }
  return unpaidPolicyChangeInvoice
  }

  /**
   * All migrated policies will have the Public IDs starting with "CH:"
   * this property simply checks than in the period.
  */
  property get IsMigratedPolicy() : boolean {
    return this.PublicID.startsWithIgnoreCase("ch:")
  }

  /**
   * Vicente, 08/21/2014, US65: Get the list of all the available Payment Plans but the "TDIC Monthly APW" if there
   * is no ACH Payment Instrument
   *
   * RK, Moved this function from the PCF code. Makes sense to have it here rather than PCF
   **/
  property get AvailablePaymentPlans() : List<PaymentPlan> {

    var paymentPlanQuery = gw.api.database.Query.make(entity.PaymentPlan)
    //paymentPlanQuery.compare(entity.PaymentPlan#Currency, Equals, Currency.TC_USD)
    paymentPlanQuery.compare(entity.PaymentPlan#UserVisible, Equals, true)
    paymentPlanQuery.compare(entity.PaymentPlan#EffectiveDate, LessThanOrEquals, this.ExpirationDate)
    var queryRestriction = paymentPlanQuery.or( \ criteria -> {
      criteria.compare(entity.PaymentPlan#ExpirationDate, Equals, null)
      criteria.compare(entity.PaymentPlan#ExpirationDate, GreaterThanOrEquals, this.EffectiveDate)
    })
    var allPaymentPlans = queryRestriction.select().where(\elt -> elt.Currencies.hasMatch(\elt1 -> elt1.Value == Currency.TC_USD))

    var planList = allPaymentPlans.toList()

    //Remove "Monthly APW" payment plan for accounts other than ACH/EFT Payment Method
    if(not this.Account.HasACHPaymentInstrument_TDIC){
      planList.removeWhere( \ payPlan -> payPlan.IsMonthly )
    }

    return planList.sortBy( \ plan -> plan.PlanOrder )
  }

  /**
   * Get the amount billed and/or due.
   */
  protected function getTotalAmount_TDIC (includedStatus : InvoiceStatus[]) : MonetaryAmount {
    var adjustment = new MonetaryAmount (0, this.Currency);
    var total = new MonetaryAmount (0, this.Currency);

    for (invoice in this.Policy.Account.Invoices) {
      if (includedStatus.contains(invoice.Status)) {
        for (invoiceItem in invoice.InvoiceItems) {
          if (invoiceItem.PolicyPeriod == this and invoiceItem.Reversed == false) {
          total += (invoiceItem.Amount - invoiceItem.PaidAmount);

          for (paymentItem in invoiceItem.PaymentItems) {
              if (paymentItem.Reversed == false and
                  paymentItem typeis DirectBillPaymentItem and
                paymentItem.DBPmntDistributionContext_TDIC == DBPmntDistributionContext.TC_CANCELLATION) {
                adjustment += paymentItem.GrossAmountToApply;
            }
          }
        }
      }
    }
    }

    total += adjustment;

    return total;
  }

  /**
   * Get the amount due.
  */
  public property get AmountDue_TDIC() : MonetaryAmount {
    return getTotalAmount_TDIC ({InvoiceStatus.TC_DUE});
  }

  /**
   * Get the amount billed or due.
   */
  public property get AmountBilledOrDue_TDIC() : MonetaryAmount {
    return getTotalAmount_TDIC ({InvoiceStatus.TC_BILLED, InvoiceStatus.TC_DUE});
  }

  /**
   * Was policy period created by issuance (submission) (as opposed to renewal or rewrite).
  */
  public property get IsPolicyPeriodCreatedByIssuance_TDIC() : boolean {
    return this.Charges.hasMatch (\c -> c.BillingInstruction typeis Issuance);
  }

  //20170531 TJT: GW-2689
  // Reopen a policy that is Cancelled and Closed, so that we can take a collections payment, or a reinstatement pre-payment
  // Called by PolicyDetailsSummary.pcf > Actions > Reopen Closed Policy

  function reopenClosedPolicyPeriod_TDIC() {
    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      var tmpPeriod = bundle.add(this)
      tmpPeriod.scheduleFinalAudit()
      tmpPeriod.waiveFinalAudit()
    })
  }
  
  /**
   * Determine amount corresponding to the delinquency cancel.
   */
  public property get DelinquencyCancelAmount_TDIC() : MonetaryAmount {
    // Find the Direct Bill Payment Items in the database.
    var query = Query.make(DirectBillPaymentItem)
        .compare (DirectBillPaymentItem#PolicyPeriod, Equals, this)
        .compare (DirectBillPaymentItem#ReversedDate, Equals, null)
        .compare (DirectBillPaymentItem#DBPmntDistributionContext_TDIC, Equals,
            DBPmntDistributionContext.TC_CANCELLATION);
    var results = query.select();
    var amount = results.sum(\pi -> pi.GrossAmountToApply);

    // Find the Direct Bill Payment Items that haven't been committed to the database.
    for (bean in this.Bundle.InsertedBeans) {
      if (bean typeis DirectBillPaymentItem
          and bean.PolicyPeriod == this
          and bean.Reversed == false
          and bean.DBPmntDistributionContext_TDIC == DBPmntDistributionContext.TC_CANCELLATION) {
        amount += bean.GrossAmountToApply;
      }
    }

    amount = amount.negate();

    return amount;
  }

  //20170808 TJT: GW-2848
  public property get WrittenOff_TDIC() : MonetaryAmount {
    // this is the same formula used on the BC UI.
    return this.WriteoffExpense - this.NegativeWriteoff
  }

  // SujitN : GBC-429 - Return Premium Application
  property get ReturnPremiumPlan_TDIC() : ReturnPremiumPlan {
    var wcRetPremPlan = Query.make(ReturnPremiumPlan).compare(ReturnPremiumPlan#Name, Equals, "TDIC Return Premium Plan (WC)").select().FirstResult
    var nonWCRetPremPlan = Query.make(ReturnPremiumPlan).compare(ReturnPremiumPlan#Name, Equals, "TDIC Return Premium Plan (Non-WC)").select().FirstResult
    if(this.Policy.LOBCode == LOBCode.TC_WC7WORKERSCOMP){
      return wcRetPremPlan
    }
    return nonWCRetPremPlan
  }

  property get InvoiceStream() : InvoiceStream {
    //var invoiceStream = this.InvoiceItemsSortedByEventDate.first().Invoice.InvoiceStream
    //GWPS-2751 - the above code is returning the invoice stream as(Transferred policy - PublicID (Monthly)) from transferred account so
    // added below condition to get InvoiceStream as PolicyNumber from the current account invoice if the policy is transferred from one account to other
    var invoiceStream = this.InvoiceItemsSortedByEventDate?.where(\invoiceItems ->
        invoiceItems.Invoice.InvoiceStream?.Policy?.LatestPolicyPeriod!=null)?.first()?.Invoice.InvoiceStream
    return invoiceStream
  }

  public property get PaymentsReceived_TDIC() : MonetaryAmount {
    var total  =  new MonetaryAmount (0, this.Currency)
  /*GWPS 1675 - Modified to sumup the payments on billed, planned & due invoices to balance the GL*/
    //total = this.PaidAmount + this.InvoiceStream.UnappliedFund.Balance
    //GWPS-2105 - Adding the undistributed amount from the payments received for this policy
    var totalAmount = this.PaidAmountTowardBilledInvoices + this.PaidAmountTowardUnbilledInvoices + this.undistributedAmount_TDIC
    if(totalAmount != null)
    total = new MonetaryAmount (totalAmount, this.Currency)
    return total
  }

  public property get TotalPremium_TDIC() : MonetaryAmount {
    return this.getChargeTotal(null)
  }

  protected function getChargeTotal (chargePatternCode : String) : MonetaryAmount {
    var total = new MonetaryAmount (0, this.Currency)
    var charges = this.Charges
    var billingInstructions = charges.partition(\c -> c.BillingInstruction).Keys

    // Remove delinquency cancels that haven't been reinstated for in-progress delinqencies.
    billingInstructions.removeWhere(\bi -> bi typeis Cancellation and ignoreCancellation(bi))

    // Remove reversed audits without a following final audit.
    var lastNonReversedAudit = billingInstructions.whereTypeIs(Audit)
        .where(\a -> a.RevisionType_TDIC != AuditRevisionType_TDIC.TC_REVERSAL)
        .maxBy(\a -> a.CreateTime)
    billingInstructions.removeWhere(\bi -> bi typeis Audit and
        bi.RevisionType_TDIC == AuditRevisionType_TDIC.TC_REVERSAL and
        (lastNonReversedAudit == null or bi.CreateTime > lastNonReversedAudit.CreateTime))

    for (billingInstruction in billingInstructions) {
      for (charge in billingInstruction.Charges) {
        if (chargePatternCode == null or charge.ChargePattern.ChargeCode == chargePatternCode) {
          total += charge.Amount
        }
      }
    }

    return total
  }

  private function ignoreCancellation (cancellation : Cancellation) : boolean {
    var retval = false
    // Ignore cancellation if there are Cancellation contexts.
    for (charge in cancellation.Charges) {
      for (invoiceItem in charge.InvoiceItems) {
        for (paymentItem in invoiceItem.PaymentItems) {
          if (paymentItem typeis DirectBillPaymentItem and
              paymentItem.DBPmntDistributionContext_TDIC == DBPmntDistributionContext.TC_CANCELLATION) {
            return true
          }
        }
      }
    }
    return retval
  }
}