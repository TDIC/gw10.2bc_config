package tdic.bc.config.enhancement

uses java.util.ArrayList

uses tdic.bc.config.account.RelatedAccount
uses tdic.bc.integ.plugins.hpexstream.util.TDIC_BCExstreamHelper
uses gw.api.database.IQueryBeanResult
uses java.util.List
uses org.slf4j.LoggerFactory

/**
 * US65
 * 08/21/2014 Vicente
 *
 * Account Enhancement for TDIC.
 */
enhancement AccountEnhancement : Account {
  /**
   * US65
   * 08/21/2014 Vicente
   *
   * Evaluates if the Account has a Payment Instrument with an ACH/EFT Payment Method
   */
  @Returns("True if the Account has a Payment Instrument with an ACH/EFT Payment Method")
  property get HasACHPaymentInstrument_TDIC() : boolean {
    return this.PaymentInstruments.hasMatch( \ pInstrument -> pInstrument.PaymentMethod == typekey.PaymentMethod.TC_ACH
        && !pInstrument.Invalidated_TDIC)
  }

  /**
   * US62 - Display Related Accounts
   * 09/15/2014 Alvin Lee
   *
   * Account association within BillingCenter is set by the following rules:  If any other account shares the same
   * contact, as defined by the contact's ADA Number (an OfficialID), it is an associated account.  This goes for ANY
   * contact on ALL the account's policy periods.  Through US368 (PC-BC Integration for Billing Instructions), multiple
   * contacts will be brought into each policy period.  If any policy contact under this account's policy periods
   * matches a policy contact of any other account's policy periods, that account is an associated account.
   * It is possible for two different Contact entities to have the same ADA number (due to a user entering it twice).
   */
  @Returns("A Set<Account> of accounts that are associated with this account")
  property get AssociatedAccounts_TDIC() : List<RelatedAccount> {
    var associatedAccountList = new ArrayList<RelatedAccount>()
    for (contact in this.AllPolicyPeriods*.Contacts*.Contact.toSet()) {
      var adaOfficialID = contact.OfficialIDs.where( \ officialID -> officialID.OfficialIDType == OfficialIDType.TC_ADANUMBER_TDIC).first()
      // Prevent matching of null (and bringing back a bunch of results) if ADA number is not populated
      if (adaOfficialID == null) {
        continue
      }

      // Official ID level
      var officialIDQuery = gw.api.database.Query.make(OfficialID)
      officialIDQuery.compare(OfficialID#OfficialIDType, Equals, adaOfficialID.OfficialIDType)
      officialIDQuery.compare(OfficialID#OfficialIDValue, Equals, adaOfficialID.OfficialIDValue)

      // Contact level
      var contactQuery = gw.api.database.Query.make(Contact)
      contactQuery.subselect(Contact#ID, CompareIn, officialIDQuery, OfficialID#Contact)

      // PolicyPeriodContact level
      var policyContactQuery = gw.api.database.Query.make(PolicyPeriodContact)
      policyContactQuery.compareIn(PolicyPeriodContact#Contact, contactQuery.select().toTypedArray())

      // PolicyPeriod level
      var policyPeriodQuery = gw.api.database.Query.make(PolicyPeriod)
      policyPeriodQuery.subselect(PolicyPeriod#ID, CompareIn, policyContactQuery, PolicyPeriodContact#PolicyPeriod)

      // Policy level
      var policyQuery = gw.api.database.Query.make(Policy)
      policyQuery.compare(Policy#Account, NotEquals, this)  // Make sure account does not equal this one
      policyQuery.subselect(Policy#ID, CompareIn, policyPeriodQuery, PolicyPeriod#Policy)

      // Account level
      var acctQuery = gw.api.database.Query.make(Account)
      var acctResults = acctQuery.subselect(Account#ID, CompareIn, policyQuery, Policy#Account).select()
      acctResults.each( \ acct -> {
        var relatedAccount = new RelatedAccount()
        relatedAccount.Contact = contact
        relatedAccount.Account = acct
        associatedAccountList.add(relatedAccount)
      })
    }
    return associatedAccountList
  }

  /**
   * US64
   * 10/14/2014 Vicente
   *
   * Returns all non-blank invoices related to this account, sorted from oldest to newest
   */
  @Returns("All non-blank invoices related to this account, sorted from oldest to newest ")
  property get NonBlankInvoicesSortedByDate() : AccountInvoice[]{
    return this.getInvoicesSortedByDate().where( \ elt -> elt.Amount_amt!=0 or elt.AmountDue_amt != 0)
  }

  /**
   * US644
   * 11/28/2014 shanem
   *
   * Get template Ids for event name and create appropriate documents on the Account
   */
  @Param("eventName", "Event Name to create documents for")
  function createDocumentsForEvent(eventName: String) {
    var _exstreamLogger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
    _exstreamLogger.trace("TDIC_AccountEnhancement#createDocumentsForEvent() - Entering.")
    var bcHelper = new TDIC_BCExstreamHelper()
    bcHelper.createDocumentStubs(eventName, this, null /*PolicyPeriod*/)
    _exstreamLogger.trace("TDIC_AccountEnhancement#createDocumentsForEvent() - Notifying Exstream Message Queue.")
    this.addEvent(eventName)
  }

  /**
   * GW1353
   * 03/17/2016
   *
   * Get template Ids for event name and create appropriate documents on the Account
   */
  @Param("eventName", "Event Name to create documents for")
  function createDocumentsForEvent(eventName: String, pp: PolicyPeriod) {
    var _exstreamLogger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
    _exstreamLogger.trace("TDIC_AccountEnhancement#createDocumentsForEvent() - Entering.")
    var bcHelper = new TDIC_BCExstreamHelper()
    bcHelper.createDocumentStubs(eventName, this, pp)
    _exstreamLogger.trace("TDIC_AccountEnhancement#createDocumentsForEvent() - Notifying Exstream Message Queue.")
  }

  /**
   * US1131
   * 01/28/2015 Vicente
   *
   * Invalidate account's APW payment instruments that are not the default payment instrument or responsive
   */
  function updateAccountPaymentInstruments() {
    this.PaymentInstruments.where( \ pInstrument -> pInstrument.PaymentMethod == typekey.PaymentMethod.TC_ACH
        and pInstrument != this.DefaultPaymentInstrument).each( \ pInstrument -> { pInstrument.Invalidated_TDIC = true })
  }

  /**
   * US1364 - Account-level Activities
   * 3/30/2015 Alvin Lee
   *
   * Query activities for the Account.
   */
  @Returns("A collection of activities associated with this account")
  property get Activities_TDIC() : IQueryBeanResult<Activity> {
    var activityQuery = gw.api.database.Query.make(Activity)
    activityQuery.compare(Activity#Account, Equals, this)
    return activityQuery.select()
  }

  property get PaymentMoneyReceived_TDIC(): List<PaymentMoneyReceived>{
    return this.findReceivedPaymentMoneysSortedByReceivedDate().toList()
  }

  /**
   * All migrated accounts will have the Public IDs starting with "CH:"
   * this property simply checks than in the account.
   */
  property get IsMigratedAccount() : boolean {
    return this.PublicID.startsWithIgnoreCase("ch:")
  }
}
