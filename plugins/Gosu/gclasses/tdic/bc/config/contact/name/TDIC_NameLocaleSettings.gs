package tdic.bc.config.contact.name

uses java.util.Set
uses gw.api.name.NameLocaleSettings
uses gw.api.name.NameOwnerFieldId

/**
 * US1129,
 * 01/21/2015 Vicente
 *
 * Extension to NameLocaleSettings for additional fields shown in display names at TDIC.
 */
class TDIC_NameLocaleSettings extends NameLocaleSettings {

  public static final var TDIC_DEFAULT_DISPLAY_NAME_FIELDS : Set<NameOwnerFieldId> = {
      TDIC_NameOwnerFieldId.FIRSTNAME, TDIC_NameOwnerFieldId.MIDDLENAME, TDIC_NameOwnerFieldId.LASTNAME, TDIC_NameOwnerFieldId.SUFFIX, TDIC_NameOwnerFieldId.CREDENTIAL
  }.toSet().freeze()
}