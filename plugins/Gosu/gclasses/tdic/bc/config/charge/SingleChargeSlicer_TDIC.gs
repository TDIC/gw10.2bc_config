package tdic.bc.config.charge

uses gw.api.domain.charge.ChargeInitializer
uses gw.api.domain.invoice.ChargeInstallmentChanger
uses gw.api.domain.invoice.ChargeSlicer
uses gw.pl.currency.MonetaryAmount

class SingleChargeSlicer_TDIC implements ChargeSlicer {

  override function createEntries(chargeInitializer: ChargeInitializer) {
    chargeInitializer.Entries.each(\elt -> {
      elt.remove()
    })
    chargeInitializer.addEntry(chargeInitializer.Amount, InvoiceItemType.TC_INSTALLMENT, chargeInitializer.ChargeDate)
  }

  override function recreateInvoiceItems(chargeInstallmentChanger: ChargeInstallmentChanger, monetaryAmount: MonetaryAmount) {
  }

}