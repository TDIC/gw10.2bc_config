package tdic.bc.common.batch.bankeft

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.properties.PropertyUtil
uses gw.api.locale.DisplayKey
uses gw.processes.BatchProcessBase
uses java.sql.Connection
uses java.sql.ResultSet
uses java.lang.Exception
uses gw.api.system.server.ServerUtil
uses gw.pl.exception.GWConfigurationException
uses tdic.bc.integ.plugins.bankeft.dto.BankPrenote.TDIC_BankEFTPrenoteFile
uses tdic.bc.integ.plugins.bankeft.dto.BankPrenote.TDIC_PaymentInstrumentBeanInfo
uses tdic.bc.integ.plugins.bankeft.dto.BankPrenote.TDIC_PaymentInstrumentWritableBean
uses tdic.bc.integ.plugins.bankeft.helper.prenote.TDIC_BankEFTPrenoteHelper

uses java.sql.SQLException
uses java.io.File
uses org.apache.commons.io.FileUtils
uses java.io.IOException
uses com.tdic.util.misc.FtpUtil
uses com.tdic.util.misc.EmailUtil
uses gw.plugin.Plugins
uses gw.plugin.util.IEncryption
uses org.slf4j.LoggerFactory

/**
 * US568 - Bank/EFT Integration
 * 09/25/2014 Alvin Lee
 *
 * Custom batch job for the Bank/EFT integration to generate payment instrument flat files to be sent to the bank.
 */
class TDIC_BankEFTPrenoteBatchJob extends BatchProcessBase {

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("BANK_EFT")

  /**
   * Name of the Bank EFT flat file, hardcoded as this is not likely to change, unless the entire system changes (value: INPUT.DAT).
   */
  public static final var FILENAME : String = "INPUT_PRENOTE.DAT"

  /**
   * Key for looking up the file output directory from the properties file (value: eft.outputdir).
   */
  public static final var OUTPUT_DIR_PATH : String = "eft.outputdir"

  /**
   * Key for looking up the notification email addresses from properties file (value: PaymentNotificationEmail).
   */
  public static final var NOTIFICATION_EMAIL_KEY : String = "PaymentNotificationEmail"

  /**
   * Key for looking up the failure notification email addresses from the properties file (value: BCInfraIssueNotificationEmail).
   */
  public static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "BCInfraIssueNotificationEmail"

  /**
   * Name of the vendor for purposes of connecting to the FTP (value: BofaOutgoingACH).
   */
  public static final var VENDOR_NAME : String = "BofaOutgoingACH"

  /**
   * Absolute directory path/string to use to write files.
   */
  private var _destinationDirectoryPath : String = null

  /**
   * Notification email addresses in case of any errors in batch process.
   */
  private var _notificationEmail : String = null

  /**
   * Notification email addresses in case of any errors in batch process.
   */
  private var _failureNotificationEmail : String = null

  /**
   * Notification email's subject for Bank/EFT batch job completion.
   */
  public static final var EMAIL_SUBJECT_COMPLETED: String = "Bank/EFT Prenote Batch Job Completed on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * Notification email's subject for Bank/EFT batch job failures.
   */
  public static final var EMAIL_SUBJECT_FAILURE: String = "Bank/EFT Prenote Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId


  /**
   * Notification email's subject for Bank/EFT batch job failures.
   */
  public static final var EMAIL_SUBJECT_DB_FAILURE: String = "Database connection error on Server:"+gw.api.system.server.ServerUtil.ServerId

  /**
   * Notification email's subject for Bank/EFT batch job failures.
   */
  public static final var EMAIL_SUBJECT_UNEXPECTED_FAILURE: String = "Unexpected error on server: " +gw.api.system.server.ServerUtil.ServerId

  /**
   * The connection to the external database.
   */
  private var _dbConnection:Connection = null

  /**
   * Payment instruments to process as a part of a single batch run.
   */
  private var _paymentInstrumentResults : ResultSet = null

  /**
   * The PaymentInstrumentBeanInfo to interact with the external database.
   */
  private var _beanInfo : TDIC_PaymentInstrumentBeanInfo = null

  /**
   * Flag to represent if payments instruments are available and ready to be processed when batch is run.
   */
  private var _paymentInstrumentAvailableToProcess = true

  /**
   * Default constructor with no parameters.
   */
  construct() {
    super(BatchProcessType.TC_BANKEFTPRENOTEBATCHPROCESS_TDIC)
  }

  /**
   * Runs the actual batch process.
   */
  override function doWork() {
    _logger.info("TDIC_BankEFTPrenoteBatchJob#doWork() - Entering")
    if(!_paymentInstrumentAvailableToProcess){
      // Send email notification of failure
      if (_failureNotificationEmail != null) {
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE, DisplayKey.get("TDIC.Web.Plugin.EFTPrenote.Failure.Email.Desc"))
      }
    }else {
      // Set up file to write data
      var destFile = new File(_destinationDirectoryPath + "/" + FILENAME)

      _logger.debug("TDIC_BankEFTPrenoteBatchJob#doWork() - Destination file constructed: ${destFile.AbsolutePath}")

      var flatFileData = new TDIC_BankEFTPrenoteFile()
      try {
        while (_paymentInstrumentResults.next()) {
          // Check TerminateRequested flag before proceeding with each entry
          if (TerminateRequested) {
            _logger.info("TDIC_BankEFTPrenoteBatchJob#doWork() - Terminate requested during doWork() method. Progress: " + Progress + " rows completed.")
            // Write lines and then commit transaction
            if (flatFileData.PaymentInstruments != null && !flatFileData.PaymentInstruments.Empty) {
              FileUtils.writeLines(destFile, TDIC_BankEFTPrenoteHelper.convertDataToFlatFileLines(flatFileData))
            }
            // Upload file to the Bank of America FTP server
            _logger.debug("TDIC_BankEFTPrenoteBatchJob#doWork() - Uploading file to Bank of America FTP server")
            if (!FtpUtil.uploadFile(VENDOR_NAME, destFile)) {
              throw new Exception("Error uploading file to FTP.  Check integration log for stack trace.")
            }
            _dbConnection.commit()
            return
          }

          // Map fields to object
          var paymentInstrumentBean = new TDIC_PaymentInstrumentWritableBean()
          paymentInstrumentBean.ContactName = _paymentInstrumentResults.getString("ContactName").toUpperCase()
          paymentInstrumentBean.BankRoutingNumber = _paymentInstrumentResults.getString("BankRoutingNumber")
          var encryptionPlugin = Plugins.get("EncryptionByAESPlugin") as IEncryption
          paymentInstrumentBean.BankAccountNumber = encryptionPlugin.decrypt(_paymentInstrumentResults.getString("BankAccountNumber"))


          //Increment operations completed for the purpose of keeping progress.  Errors are not incremented since a single failure terminates all.
          flatFileData.addToPaymentInstruments(paymentInstrumentBean)
          incrementOperationsCompleted()

          // Mark the line as processed
          paymentInstrumentBean.PublicID = _paymentInstrumentResults.getString(_beanInfo.IDColumnSQLName)
          paymentInstrumentBean.markAsProcessed(_dbConnection)
        }

        // Write lines and then commit transaction
        if (flatFileData.PaymentInstruments != null && !flatFileData.PaymentInstruments.Empty) {
          FileUtils.writeLines(destFile, TDIC_BankEFTPrenoteHelper.convertDataToFlatFileLines(flatFileData))
        }

        // Upload file to the Bank of America FTP server
        _logger.debug("TDIC_BankEFTPrenoteBatchJob#doWork() - Uploading file to Bank of America FTP server")
        if (ScriptParameters.EnableEFTFileFTPUpload && !FtpUtil.uploadFile(VENDOR_NAME, destFile)) {
          throw new Exception("Error uploading file to FTP.  Check integration log for stack trace.")
        }

        _dbConnection.commit()
        EmailUtil.sendEmail(_notificationEmail, EMAIL_SUBJECT_COMPLETED, DisplayKey.get("TDIC.Web.Plugin.EFTPrenote.Success.Email.Desc"))
      } catch (ioe : IOException) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_BankEFTPrenoteBatchJob#doWork() - Error while writing to output file " + destFile.AbsolutePath, ioe)
        // Send email notification of failure
        if (_failureNotificationEmail != null) {
          EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE, DisplayKey.get("TDIC.Web.Plugin.EFTPrenote.Failure.Email.FileWrite.Desc") + " " + ioe.LocalizedMessage)
        }
        _dbConnection.rollback()
      } catch (sqle : SQLException) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_BankEFTPrenoteBatchJob#doWork() - Database connection error while attempting to access integration database table", sqle)
        // Send email notification of failure
        if (_failureNotificationEmail != null) {
          EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE, DisplayKey.get("TDIC.Web.Plugin.EFTPrenote.Failure.Email.DBConnection.Desc") + sqle.LocalizedMessage)
        }
        _dbConnection.rollback()
      } catch (e : Exception) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_BankEFTPrenoteBatchJob#doWork() - Error processing output file: " + e.LocalizedMessage, e)
        // Send email notification of failure
        if (_failureNotificationEmail != null) {
          EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE, DisplayKey.get("TDIC.Web.Plugin.EFTPrenote.Failure.Email.FileProcess.Desc") + e.LocalizedMessage)
        }
        _dbConnection.rollback()
      } finally {
        try {
          if (_dbConnection != null) {
            DatabaseManager.closeConnection(_dbConnection)
          }
        } catch (e : Exception) {
          _logger.error("TDIC_BankEFTPrenoteBatchJob#doWork() - Exception on closing DB (continuing)", e)
        }
      }

      _logger.info("TDIC_BankEFTPrenoteBatchJob#doWork() - Finished processing")
    }
  }

  /**
   * Determines if the process should run the doWork() method, based on if there are payment requests to process into a flat file.
   */
  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  override function checkInitialConditions() : boolean {
    _logger.debug("TDIC_BankEFTPrenoteBatchJob#checkInitialConditions() - Entering")

    var dbUrl : String
    try {
      var env = ServerUtil.getEnv()?.toLowerCase()
      if (env == null) {
        throw new GWConfigurationException("No environment specified.  Cannot load integration database.")
      }
      dbUrl = PropertyUtil.getInstance().getProperty("IntDBURL")
      if (dbUrl == null) {
        throw new GWConfigurationException("Integration Database not defined.")
      }

      _notificationEmail = PropertyUtil.getInstance().getProperty(NOTIFICATION_EMAIL_KEY)
      if (_notificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + NOTIFICATION_EMAIL_KEY + "' from properties file.")
      }

      _failureNotificationEmail = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)
      if (_failureNotificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + FAILURE_NOTIFICATION_EMAIL_KEY + "' from integration database.")
      }

      _destinationDirectoryPath = PropertyUtil.getInstance().getProperty(OUTPUT_DIR_PATH)
      if (_destinationDirectoryPath == null) {
        throw new GWConfigurationException("Cannot retrieve file output path with the key '" + OUTPUT_DIR_PATH
            + "' from integration database.")
      }
    } catch (gwce : GWConfigurationException) {
      // Send email notification of failure
      EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE, DisplayKey.get("TDIC.Web.Plugin.EFTPrenote.Failure.Email.DBProps.Desc"))
      throw gwce
    }

    try {
      // Get a connection from DatabaseManager using the GWINT DB URL
      _dbConnection = DatabaseManager.getConnection(dbUrl)
      _beanInfo = new TDIC_PaymentInstrumentBeanInfo()
      var stmt = _beanInfo.createRetrieveAllSQL(_dbConnection)
      _paymentInstrumentResults = stmt.executeQuery()
      _paymentInstrumentResults.last()
      // If the last payment request row is row 0, there are no payment requests to process
      if (_paymentInstrumentResults.Row != 0) {
        _logger.info("TDIC_BankEFTPrenoteBatchJobb#checkInitialConditions() - Results returned. Number of payment requests to be processed: " + _paymentInstrumentResults.Row)
        OperationsExpected = _paymentInstrumentResults.Row
        _paymentInstrumentResults.beforeFirst()
      }
      else {
        _paymentInstrumentAvailableToProcess = false
        _logger.warn("TDIC_BankEFTPrenoteBatchJob#checkInitialConditions() - No payment instruments to be processed.")
        // Attempt to close connection
        try {
          if (_dbConnection != null) {
            DatabaseManager.closeConnection(_dbConnection)
          }
        } catch (e : Exception) {
          _logger.error("TDIC_BankEFTPrenoteBatchJob#checkInitialConditions() - Exception on closing DB (continuing)", e)
        }
      }
      return true
    } catch (sqle : SQLException) {
      _logger.error("TDIC_BankEFTPrenoteBatchJob#checkInitialConditions() - Database connection error while attempting to read integration database", sqle)
      // Send email notification of failure
      if(_failureNotificationEmail != null){
        EmailUtil.sendEmail(_failureNotificationEmail,EMAIL_SUBJECT_DB_FAILURE, DisplayKey.get("TDIC.Web.Plugin.EFTPrenote.Failure.EMail.DBError.Desc") + sqle.LocalizedMessage)
      }
      // Attempt to close connection
      try {
        if (_dbConnection != null) {
          DatabaseManager.closeConnection(_dbConnection)
        }
      } catch (e : Exception) {
        _logger.error("TDIC_BankEFTPrenoteBatchJob#checkInitialConditions() - Exception on closing DB (continuing)", e)
      }
      throw sqle
    } catch (e : Exception) {
      // Handle other errors
      _logger.error("TDIC_BankEFTPrenoteBatchJob#checkInitialConditions() - Unexpected error while attempting to read integration database", e)
      // Send email notification of failure
      if(_failureNotificationEmail != null){
        EmailUtil.sendEmail(_failureNotificationEmail,EMAIL_SUBJECT_UNEXPECTED_FAILURE ,DisplayKey.get("TDIC.Web.Plugin.EFTPrenote.Failure.EMail.DBRead.Desc") + e.LocalizedMessage)
      }
      // Attempt to close connection
      try {
        if (_dbConnection != null) {
          DatabaseManager.closeConnection(_dbConnection)
        }
      } catch (e2 : Exception) {
        _logger.error("TTDIC_BankEFTPrenoteBatchJob#checkInitialConditions() - Exception on closing DB (continuing)", e2)
      }
      throw e
    }
  }

  /**
   * Called when the batch process is to be terminated.  This sets the TerminateRequested flag and returns true to indicate that the process can be stopped.
   *
   * @return A boolean to indicate that the process can be stopped.
   */
  override function requestTermination() : boolean {
    _logger.info("TDIC_BankEFTPrenoteBatchJob#requestTermination() - Terminate requested for batch process.")
    // Set TerminateRequested to true by calling super method
    super.requestTermination()
    return true
  }

}