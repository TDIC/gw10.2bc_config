package tdic.bc.common.batch.eftreversal.helper

uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.locale.DisplayKey
uses gw.api.util.DateUtil
uses gw.pl.exception.GWConfigurationException
uses org.slf4j.LoggerFactory


uses java.math.BigDecimal

uses entity.Invoice

/**
 * US1126 - CR - EFT/ACH Reversal Automation
 * 02/05/2015 Vicente
 * <p>
 * Helper class with methods to process the Payment Reverse and Payment Instrument Change operations for EFT/ACH Reversal
 * and Change Notifications batch process.
 */
class TDIC_EFTReversalAndChangeNotificationsBatchJobHelper {

  /**
   * The custom batch process does not have a current user, so a user needs to be specified in order to execute
   * the bundle which adds or makes changes to an entity.  This is currently set to the Super User (value: su).
   * TODO May need to change this user before production
   */
  /**
   * 01/19/2016 Hermia Kho
   * Replace Super_User "su" with "iu"
   */
  public static final var SUPER_USER : String = "iu"

  /**
   * Key for looking up the notification email addresses from the integration database (value: PaymentNotificationEmail).
   */
  public static final var NOTIFICATION_EMAIL_KEY : String = "PaymentNotificationEmail"

  /**
   * Notification email's subject for EFT/ACH Reversal and Change Notifications batch job warning/notification.
   */
  public static final var EMAIL_SUBJECT_WARNING : String = "EFT/ACH Returns Batch Job Warning Notification on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("EFT_REVERSAL")
  /**
   * prenote prefix.
   */
  private static final var _prenotePrefix = "bc:"

  /**
   * Returns the DirectBillMoneyRcvd associated with the input Invoice number
   */
  @Param("amount", "The amount associated with DirectBillMoneyRcvd.")
  @Param("invoiceNumber", "The invoice number associated with DirectBillMoneyRcvd.")
  @Returns("Returns the DirectBillMoneyRcvd associated with the input Invoice number")
  @Throws(EFTReversalValidationException, "If there are no invoice with the input invoice number")
  protected static function getDirectBillMoneyRcvdWithInvoiceNumber(amount : BigDecimal, invoiceNumber : String) : DirectBillMoneyRcvd {
    //Invoice level
    var invoiceQuery = gw.api.database.Query.make(Invoice)
    invoiceQuery.compare(Invoice#InvoiceNumber, Equals, invoiceNumber)

   //DirectBillMoneyRcvd level
    var directBillMoneyRcvdQuery = gw.api.database.Query.make(DirectBillMoneyRcvd)
    directBillMoneyRcvdQuery.subselect(DirectBillMoneyRcvd#Invoice, CompareIn, invoiceQuery, Invoice#ID)

    var results = directBillMoneyRcvdQuery.select()
    if (results == null || results.Count == 0) {
      _logger.warn("TDIC_EFTReversalAndChangeNotificationsBatchJobHelper#getDirectBillMoneyRcvdWithInvoiceNumber() - "
          + DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.ThereAreNoInvoicesWithTheInvoiceNumber", invoiceNumber))
      throw new EFTReversalValidationException(DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.ThereAreNoInvoicesWithTheInvoiceNumber", invoiceNumber))
    }
    // Filter for ACH payments only
    var filteredResults = results.where(\moneyRcvd -> moneyRcvd.PaymentInstrument.PaymentMethod == PaymentMethod.TC_ACH)
    // In case there is more than one payment made towards that invoice, validate the amount.
    var moneyRcvdAmount = filteredResults?.first().Amount

    /*GWPS-1781- For C01 the bank is sending amount as 0.00 instead of the exact amount in the Invoice.
    * GWPS-508 - To suppress the warnings for receiving no amount from bank */

    if(amount!=0)
    filteredResults = filteredResults.where(\moneyRcvd -> moneyRcvd.Amount == amount.ofDefaultCurrency())

    if (filteredResults.Count == 0) {
      _logger.warn("TDIC_EFTReversalAndChangeNotificationsBatchJobHelper#getDirectBillMoneyRcvdWithInvoiceNumber() - "
          + DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.PaymentAmountMismatch", invoiceNumber, moneyRcvdAmount, amount))
      throw new EFTReversalValidationException(DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.PaymentAmountMismatch", invoiceNumber, moneyRcvdAmount, amount))
    }
    // In case there is more than one payment made towards that invoice in the same amount, one should be previously reversed.
    // If all have already been reversed, return the first and let the Payment Reverse Operation error it out.
    // This is because the Change Operation also calls this method but does not care about the reversal status.
    if (filteredResults.allMatch(\moneyRcvd -> moneyRcvd.Reversed)) {
      _logger.debug("TDIC_EFTReversalAndChangeNotificationsBatchJobHelper#getDirectBillMoneyRcvdWithInvoiceNumber() - All payments for Invoice "
          + invoiceNumber + " have already been reversed. Returning first payment. If this is a reverse operation, it will error out. If it is a change operation, it will proceed.")
      return filteredResults.first()
    }
    filteredResults = filteredResults.where(\moneyRcvd -> !moneyRcvd.Reversed)
    // If there is still more than one matching result, throw an error
    if (filteredResults.Count > 1) {
      _logger.warn("TDIC_EFTReversalAndChangeNotificationsBatchJobHelper#getDirectBillMoneyRcvdWithInvoiceNumber() - "
          + DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.DuplicatePaymentForInvoice", invoiceNumber, amount))
      throw new EFTReversalValidationException(DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.DuplicatePaymentForInvoice", invoiceNumber, amount))
    }

    return filteredResults.first()
  }

  /**
   * Create the Trouble Ticket to indicate an error in file sent to the bank:
   *   -	Type: processing error
   *   -	Subject and details: reason code description
   *   -	Priority : High
   *   -	Due date: Next Day
   *   -	Escalation date : Two days after Due date
   *   -	Entities added: Account, Policy and Transaction
   */
  /**
   * GW666 change query to compare PublicID instead of Name
   * Hermia Kho - 12/15/2015
   */
  /**
   * GW654 add new finance group = "Finance APW" and assign NOC to Finance APW.
   * Hermia Kho - 12/22/2015
   */
  @Param("account", "Account object where the notification is added.")
  @Param("directBillMoneyRcvd", "DirectBillMoneyRcvd object related with the payment included in the file sent to the bank.")
  @Param("noc", "Payment instrument notification of change received.")
  @Returns("Trouble Ticket to indicate an error in file sent to the bank.")
  @Throws(EFTReversalValidationException, "Financial APW group does not exists.")
  protected static function createNotificationOfChangeTroubleTicket(account : Account, directBillMoneyRcvd : DirectBillMoneyRcvd, noc : NACHAChangeCode_TDIC) {
    var policy = directBillMoneyRcvd.UnappliedFund.Policy
    var transaction = {directBillMoneyRcvd.MoneyReceivedTransaction}
    var currentDate = DateUtil.currentDate()
    var troubleTicketHelper = new CreateTroubleTicketHelper(account.Bundle)
    var troubleTicket = troubleTicketHelper.createTroubleTicket(account)
    var financeAPWGroup = Query.make(Group).compare(Group#PublicID, Equals, "group:12").select().FirstResult
    troubleTicket.TicketType = TroubleTicketType.TC_PROCESSINGERROR
    troubleTicket.Title = noc.Description
    troubleTicket.DetailedDescription = noc.Description
    troubleTicket.Priority = Priority.TC_HIGH
    troubleTicket.TargetDate = currentDate.addDays(1)
    troubleTicket.EscalationDate = currentDate.addDays(3)
    troubleTicketHelper.linkTroubleTicketWithPolicy(troubleTicket, policy)
    troubleTicket.associateWithTransactions(transaction?.toTypedArray())
    troubleTicket.assignUserByRoundRobin(false, financeAPWGroup)
  }

  /**
   * Creates a payment request for the specified Invoice number
   */
  @Param("invoiceNumber", "The invoice number to associate with the new payment request")
  @Param("account", "The account on which to create the payment request")
  protected static function createPaymentRequestForInvoice(invoiceNumber : String, account : Account) {
    _logger.info("TDIC_EFTReversalAndChangeNotificationsBatchJobHelper#createPaymentRequestForInvoice() - Entering")
    var invoiceQuery = gw.api.database.Query.make(Invoice)
    invoiceQuery.compare(Invoice#InvoiceNumber, Equals, invoiceNumber)
    var results = invoiceQuery.select()

    if (results == null || results.Count == 0) {
      _logger.warn("TDIC_EFTReversalAndChangeNotificationsBatchJobHelper#createPaymentRequestForInvoice() - "
          + DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.ThereAreNoInvoicesWithTheInvoiceNumber", invoiceNumber))
      throw new EFTReversalValidationException(DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.ThereAreNoInvoicesWithTheInvoiceNumber", invoiceNumber))
    }
    // Verify that account is still on APW.  Otherwise, cannot create payment request.
    else if (results.first().InvoiceStream.getOriginalPaymentInstrument().PaymentMethod != PaymentMethod.TC_ACH) { //GPWS-1681 : Keshavg : checking condition if Acocunt is on APW
      _logger.info("TDIC_EFTReversalAndChangeNotificationsBatchJobHelper#createPaymentRequestForInvoice() - "
          + DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.AccountNoLongerAPW", account.AccountNumber))
      // Send warning/notification email
      var notificationEmail = PropertyUtil.getInstance().getProperty(NOTIFICATION_EMAIL_KEY)
      if (notificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + NOTIFICATION_EMAIL_KEY + "' from integration database.")
      }
      EmailUtil.sendEmail(notificationEmail, EMAIL_SUBJECT_WARNING, "EFT/ACH Returns Batch Job warning/notification: "
          + DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.AccountNoLongerAPW", account.AccountNumber))
      return
    }

    var targetInvoice = results.AtMostOneRow
  // Create the new payment request
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        account = bundle.add(account)
        _logger.info("creating Payment Request for invoice: "+ targetInvoice.InvoiceNumber)
        var paymentRequest = new PaymentRequest(account.Currency)
        paymentRequest.Account = account
        paymentRequest.Amount = targetInvoice.AmountDue
        paymentRequest.PaymentInstrument = targetInvoice.InvoiceStream.getOriginalPaymentInstrument()
        paymentRequest.Status = PaymentRequestStatus.TC_REQUESTED
        paymentRequest.RequestDate = DateUtil.currentDate()
        paymentRequest.DraftDate = targetInvoice.DueDate.addMonths(1).addBusinessDays(-3)//tdic.bc.config.payment.PaymentRequestUtil.getNextAvailableDraftDate(paymentRequest.RequestDate, paymentRequest.Account)
        _logger.info("Payment request Draft date : "+paymentRequest.DraftDate)
      //_logger.info("method draft date >>>:" + tdic.bc.config.payment.PaymentRequestUtil.getNextAvailableDraftDate(paymentRequest.RequestDate, paymentRequest.Account))
        _logger.info("TDIC_EFTReversalAndChangeNotificationsBatchJobHelper#createPaymentRequestForInvoice() - Payment Request being created for amount " +
            paymentRequest.Amount + " with draft date " + paymentRequest.DraftDate)

        paymentRequest.StatusDate = paymentRequest.ChangeDeadlineDate
        _logger.info("Payment Request - Due date : " + paymentRequest.DueDate + " and Draft date : " + paymentRequest.DraftDate)
        _logger.info("Invoice Due date : "+paymentRequest.Invoice.DueDate+"n amt: "+ paymentRequest.Amount)
        paymentRequest.Invoice = targetInvoice as AccountInvoice
        _logger.info("After invoice assignment, Draft Date : "+ paymentRequest.DraftDate +" and Due date : "+ paymentRequest.DueDate)
      }, SUPER_USER)

      _logger.debug("TDIC_EFTReversalAndChangeNotificationsBatchJobHelper#createPaymentRequestForInvoice() - Exiting")
    }
}