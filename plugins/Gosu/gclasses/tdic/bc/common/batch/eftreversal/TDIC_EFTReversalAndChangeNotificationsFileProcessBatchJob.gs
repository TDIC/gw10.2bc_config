package tdic.bc.common.batch.eftreversal

uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.Query
uses gw.api.locale.DisplayKey
uses gw.api.system.server.ServerUtil
uses gw.pl.exception.GWConfigurationException
uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory
uses tdic.bc.common.batch.eftreversal.helper.EFTPaymentDTO
uses tdic.bc.common.batch.eftreversal.helper.EFTReversalValidationException
uses tdic.bc.common.batch.eftreversal.helper.NACHAOperation
uses tdic.bc.common.batch.eftreversal.helper.PaymentInstrumentChangeOperation
uses tdic.bc.common.batch.eftreversal.helper.PaymentReverseOperation
uses java.io.BufferedReader
uses java.io.File
uses java.io.FileReader
uses java.io.IOException
uses java.math.BigDecimal

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 10/28/15
 * US1126 - CR - EFT/ACH Reversal Automation
 * Implementation of a custom batch process for the EFT/ACH Reversal and Change Notification Integration to process EFT/ACH reversal file.
 */
class TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob extends BatchProcessBase {

  /**
   * Name of the vendor for purposes of connecting to the FTP (value: BofaReversalIncoming).
   */
  public static final var VENDOR_NAME : String = "BofaIncoming"
  /**
   * Key for looking up the incoming file directory from the integration database (value: eftreversal.incomingdir).
   */
  public static final var INCOMING_DIRECTORY_KEY : String = "eftreversal.incomingdir"
  /**
   * Key for looking up the errer directory from the integration database (value: eftreversal.errordir).
   */
  public static final var ERROR_DIRECTORY_KEY : String = "eftreversal.errordir"
  /**
   * Key for looking up the done directory from the integration database (value: eftreversal.donedir).
   */
  public static final var DONE_DIRECTORY_KEY : String = "eftreversal.donedir"
  /**
   * Key for looking up the notification email addresses from the integration database (value: PaymentNotificationEmail).
   */
  public static final var NOTIFICATION_EMAIL_KEY : String = "PaymentNotificationEmail"
  /**
   * Key for looking up the notification failure email addresses from the integration database (value: BCInfraIssueNotificationEmail).
   */
  public static final var NOTIFICATION_FAILURE_EMAIL_KEY : String = "BCInfraIssueNotificationEmail"
  /**
   * Notification email's subject for EFT/ACH Reversal and Change Notifications batch job failures.
   */
  public static final var EMAIL_SUBJECT_FAILURE : String = "EFT/ACH Returns Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId
  /**
   * Notification email's subject for EFT/ACH Reversal and Change Notifications batch job success.
   */
  public static final var EMAIL_SUBJECT_SUCCESS : String = "EFT/ACH Returns Batch Job Success on server " + gw.api.system.server.ServerUtil.ServerId
  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("EFT_REVERSAL")
  /**
   * Reference to the group where the trouble tickets are going to be assigned
   */
  var _financeARGroup : Group
  /**
   * A Set<File> indicating the files that need to be processed, as more than one NACHA return file can be processed at once.
   */
  private var _incomingFilesToProcess = new HashSet<File>()
  /**
   * Absolute directory path/string for incoming files.
   */
  private var _incomingPath : String = null
  /**
   * Absolute directory path/string for completed files.
   */
  private var _donePath : String = null
  /**
   * Absolute directory path/string for error files.
   */
  private var _errorPath : String = null
  /**
   * A list with the NACHA operation that includes the invoices to be processed, the NACHA notification of change
   * code or return code replied by Bank of America and the corrected account number and routing number if needed
   */
  private var _NACHAOperations : List<NACHAOperation>
  /**
   * The last amount read from the last Entry Detail Record
   */
  private var _amount : BigDecimal
  /**
   * The last invoice number read from the last Entry Detail Record
   */
  private var _invoiceNumber : String
  /**
   * Notification email addresses in case of any errors in batch process.
   */
  private var _notificationEmail : String = null
  /**
   * Notification email addresses in case of any errors in batch process.
   */
  private var _notificationFailureEmail : String = null
  /**
   * Notification email addresses in case of Prenote Payment reversals.
   */
  private var _prenoteNotificationEmail : String = null
  /**
   * A list to hold records of all successfully processed EFT/ACH payment reversals
   */
  private var _ReversedPaymentDetailsList : List<EFTPaymentDTO>
  /**
   * A list to hold records of all EFT/ACH payment reversals which failed to reverse
   */
  private var _FailedReversalPaymentDetailsList : List<EFTPaymentDTO>
  /**
   * A list to hold records for all EFT payment prenote reversals
   */
  private var _PrenoteRevarsalList : List<EFTPaymentDTO>
  /**
   * Notification email's subject for successfully reversed EFT/ACH payments.
   */
  public static final var EMAIL_SUBJECT_REVERSAL_SUCCESS : String = "Successfully Reversed EFT/ACH Payments with Details"
  /**
   * Notification email's subject for EFT/ACH payments which failed to reverse.
   */
  public static final var EMAIL_SUBJECT_REVERSAL_FAILURE : String = "EFT/ACH Payment Details which Failed to Reverse"
  /**
   * Notification email's subject for EFT/ACH prenote payment reversals.
   */
  public static final var EMAIL_SUBJECT_PRENOTE_PAYMENTS : String = "EFT/ACH Prenote Return with Errors"
  /**
   *  Key for looking up the prenote notification email addresses from the properties file
   */
  private static final var PRENOTE_EMAIL_KEY : String = "PrenoteReversalNotificationEmail"
  /**
   * Default constructor with no parameters.
   */
  construct() {
    super(BatchProcessType.TC_EFTREVERSALFILEPROCESSBATCHPROCESS)
  }

  /**
   * Runs the actual batch process.
   */
  override function doWork() {
    _logger.debug("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#doWork() - Entering")
    //GINTEG-1135 : Skip if we don't have any data to process
    _logger.info("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#doWork()- Skip if we don't have any data to process")
    if (checkInitialConditions_TDIC()) {
      var reader : BufferedReader = null
      var incomingDirectory = new File(_incomingPath)
      for (incomingFile in incomingDirectory.listFiles()) {
        // Check TerminateRequested flag before proceeding with each file
        if (TerminateRequested) {
          _logger.warn("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#doWork() - Terminate requested during doWork() method. Progress: " + Progress + " files completed.")
          EmailUtil.sendEmail(_notificationFailureEmail, EMAIL_SUBJECT_FAILURE, "EFT/ACH Reversal and Change Notifications batch process file was terminated by request on file '"
              + incomingFile.Name + "'. Progress: " + Progress + " files completed.")
          return
        }

        incrementOperationsCompleted()
        _logger.info("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#doWork() - Processing file: " + incomingFile.Name)

        try {
          _NACHAOperations = new ArrayList<NACHAOperation>()

          var line : String = null
          // Read the file first and collect all Return and Change Notifications codes from the Bank; fail file if validation fails
          reader = new BufferedReader(new FileReader(incomingFile))
          line = reader.readLine()
          while (line != null) {
            processLine(line)
            line = reader.readLine()
          }

          _ReversedPaymentDetailsList = new ArrayList<EFTPaymentDTO>()
          _FailedReversalPaymentDetailsList = new ArrayList<EFTPaymentDTO>()
          _PrenoteRevarsalList = new ArrayList<EFTPaymentDTO>()
          // Process NACHA operations
          for (operation in _NACHAOperations) {
            if (typeof operation == PaymentReverseOperation) {
              var eftPaymentDetail = (operation as PaymentReverseOperation).process()
              if (eftPaymentDetail?.PaymentReversed) {
                _ReversedPaymentDetailsList.add(eftPaymentDetail)
              } else if (eftPaymentDetail.IsPrenoteReturn) {
                _PrenoteRevarsalList.add(eftPaymentDetail)
              } else {
                _FailedReversalPaymentDetailsList.add(eftPaymentDetail)
              }
            } else {
              (operation as PaymentInstrumentChangeOperation).process()
            }
          }
          // Move file to done directory
          if (!moveToDone(incomingFile, reader)) {
            throw new RuntimeException("Cannot move file to done directory")
          }
          _logger.info("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#doWork() - Finished processing file: " + incomingFile.Name)
        } catch (ioe : IOException) {
          incrementOperationsFailed()
          _logger.warn("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#doWork() - Error reading file: " + incomingFile.Name + ". Exception: "
              + ioe.LocalizedMessage, ioe)
          var fileMovedString = ""
          // Move file to error directory
          if (!moveToError(incomingFile, reader)) {
            _logger.warn("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#doWork() - Unable to move file to error directory")
            fileMovedString = ".  In addition, was not able move file to the error directory."
          }
          EmailUtil.sendEmail(_notificationFailureEmail, EMAIL_SUBJECT_FAILURE, "The NACHA return file '" + incomingFile.Name
              + "' has failed to process due to an error reading the file.  Exception: " + ioe.LocalizedMessage + fileMovedString)
        } catch (efe : EFTReversalValidationException) {
          incrementOperationsFailed()
          _logger.warn("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#doWork() - Error validating EFT Reversal file: " + incomingFile.Name
              + ". Exception: " + efe.LocalizedMessage, efe)
          var fileMovedString = ""
          // Move file to error directory
          if (!moveToError(incomingFile, reader)) {
            _logger.warn("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#doWork() - Unable to move file to error directory")
            fileMovedString = ".  In addition, was not able move file to the error directory."
          }
          EmailUtil.sendEmail(_notificationFailureEmail, EMAIL_SUBJECT_FAILURE, "The return NACHA file '" + incomingFile.Name
              + "' has failed to process due to an error reading the file.  Exception: " + efe.LocalizedMessage + fileMovedString)
        } catch (e : Exception) {
          incrementOperationsFailed()
          _logger.warn("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#doWork() - Unexpected runtime exception: " + e.LocalizedMessage, e)
          var fileMovedString = ""
          // Move file to error directory
          if (!moveToError(incomingFile, reader)) {
            _logger.warn("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#doWork() - Unable to move file to error directory")
            fileMovedString = ".  In addition, was not able move file to the error directory."
          }
          EmailUtil.sendEmail(_notificationFailureEmail, EMAIL_SUBJECT_FAILURE, "NACHA return file '" + incomingFile.Name
              + "' has failed to process due an unexpected error: " + e.LocalizedMessage + fileMovedString)
        }
        _logger.debug("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#doWork() - Finished processing file: " + incomingFile.Name)
      }
      var mailDesc : String
      if (_ReversedPaymentDetailsList.size() >= 1) {
        mailDesc = DisplayKey.get("TDIC.Web.Plugin.EFTReversal.Success.EMail.Desc")
        var details = ""
        for (eftNote in _ReversedPaymentDetailsList) {
          details = details +  DisplayKey.get("TDIC.Web.Plugin.EFTReversal.EMail.Details",stringValue(eftNote.TxnNumber),
              eftNote.ReversalDate,eftNote.Amount,stringValue(eftNote.PayeeName),stringValue(eftNote.PolicyNumber),stringValue(eftNote.ReasonCode),
              stringValue(eftNote.Description), stringValue(eftNote.RoutingNumber),stringValue(eftNote.AccountNumber))
        }
        EmailUtil.sendEmail(_notificationEmail, EMAIL_SUBJECT_REVERSAL_SUCCESS, mailDesc + details)
      }

      if (_FailedReversalPaymentDetailsList.size() >= 1) {
        mailDesc  = DisplayKey.get("TDIC.Web.Plugin.EFTReversal.Failure.EMail.Desc")
        var details = ""
        for (eftNote in _FailedReversalPaymentDetailsList) {
          details = details +  DisplayKey.get("TDIC.Web.Plugin.EFTReversal.EMail.Details",stringValue(eftNote.TxnNumber),
              eftNote.ReversalDate,eftNote.Amount,stringValue(eftNote.PayeeName),stringValue(eftNote.PolicyNumber),stringValue(eftNote.ReasonCode),
              stringValue(eftNote.Description),stringValue(eftNote.RoutingNumber), stringValue(eftNote.AccountNumber))
        }
        EmailUtil.sendEmail(_notificationFailureEmail, EMAIL_SUBJECT_REVERSAL_FAILURE, mailDesc + details)
      }

      if (_PrenoteRevarsalList.size() >= 1) {
        mailDesc  = DisplayKey.get("TDIC.Web.Plugin.PaymentReversal.Failure.EMail.Desc")
        var details = ""
        for (prenoteRecord in _PrenoteRevarsalList) {
          details = details +  DisplayKey.get("TDIC.Web.Plugin.PrenoteReturn.EMail.Details",stringValue(prenoteRecord.InvoiceNumber),
              stringValue(prenoteRecord.ReasonCode), stringValue(prenoteRecord.AccountNumber),
              stringValue(prenoteRecord.PayeeName), prenoteRecord.Description == null ? "None" : prenoteRecord.Description)
        }
        EmailUtil.sendEmail(_prenoteNotificationEmail, EMAIL_SUBJECT_PRENOTE_PAYMENTS, mailDesc + details)
      }

      if (OperationsFailed == 0) {
        EmailUtil.sendEmail(_notificationEmail, EMAIL_SUBJECT_SUCCESS, "EFT/ACH Reversal and Change Notifications Batch Job processed all the return files successfully on server "
            + gw.api.system.server.ServerUtil.ServerId)
      }
      _logger.debug("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#doWork() - Finished running batch")
    }
  }

  /**
   * Process individual file. First checks if Header Record format and Batch Header Record format are correct.
   * Then for every Entry Detail Record (Record Type Code 6) its invoice number is read and for every Addenda Record
   * (Record Type Code 7) associated with this invoice:
   * - If the Addenda type is 99(Return) the invoice number and return code are stored in _invoicesToProcessReturnCode
   * - If the Addenda type is 98(Notification of Change) the invoice number and notification of change code are stored
   * in _invoicesToProcessNOC. If is necessary the correct Account Number and Routing Number are stored as well.
   * <p>
   * This information is process later after process the file.
   */
  @Param("line", "The String text of the line to process")
  @Throws(EFTReversalValidationException, "If there are any validation issues processing the line")
  private function processLine(line : String) {
    _logger.trace("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#processLine - Processing line: " + line)

    var lineType = line.charAt(0)
    var recordTypeCode : String
    var code : String
    var accountNumber : String
    var routingNumber : String
    var noc : NACHAChangeCode_TDIC
    var returnCode : PaymentReversalReason
    switch (lineType) {
      //File Header Record.
      case '1':
        //Check File Header Record
        break

      //Batch Header Record.
      case '5':
        break

      //Entry Detail Record.
      case '6':
        // TODO: Use SDF Utility
        //Read Amount field of the Entry Detail Record
        var amountString = line.substring(29, 37) + "." + line.substring(37, 39)
        _amount = new BigDecimal(amountString)
        //Invoice number read. Field "Identification Number" of the Entry Detail Record
        _invoiceNumber = line.substring(39, 54).trim()
        break
      //Addenda Record.
      case '7':
        recordTypeCode = line.substring(1, 3)
        code = line.substring(3, 6)
        accountNumber = null
        routingNumber = null
        //Notification of change code
        if (recordTypeCode == "98") {
          try {
            noc = typekey.NACHAChangeCode_TDIC.get(code)
          } catch (iae : java.lang.IllegalArgumentException) {
            _logger.warn(DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.NotificationOfChangeCodeNotValid", code))
            throw new EFTReversalValidationException(DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.NotificationOfChangeCodeNotValid", code))
          }

          //C01: Incorrect DFI Account Number
          //C06: Incorrect DFI Account Number and Incorrect Transaction Code
          if (noc == typekey.NACHAChangeCode_TDIC.TC_C01 or
              noc == typekey.NACHAChangeCode_TDIC.TC_C06) {
            accountNumber = line.substring(35, 52).trim()
          }

          //C02: Incorrect Routing Number
          if (noc == typekey.NACHAChangeCode_TDIC.TC_C02) {
            routingNumber = line.substring(35, 44)
          }

          //C03: Incorrect Routing Number & Incorrect DFI Account Number
          //C07: Incorrect Routing Number, Incorrect DFI Account Number and Incorrect Transaction Code
          if (noc == typekey.NACHAChangeCode_TDIC.TC_C03 or
              noc == typekey.NACHAChangeCode_TDIC.TC_C07) {
            routingNumber = line.substring(35, 44)
            accountNumber = line.substring(47, 64).trim()
          }

          _NACHAOperations.add(new PaymentInstrumentChangeOperation(_amount, _invoiceNumber, noc, accountNumber, routingNumber))

          //Return code
        } else if (recordTypeCode == "99") {
          try {
            returnCode = typekey.PaymentReversalReason.get(code)
          } catch (iae : java.lang.IllegalArgumentException) {
            _logger.warn("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#processLine - " + DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.ReturnCodeNotValid", code))
            throw new EFTReversalValidationException(DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.ReturnCodeNotValid", code))
          }

          _NACHAOperations.add(new PaymentReverseOperation(_amount, _invoiceNumber, returnCode))
        }
        break
      //Batch Control Record. Last line of the fine before the File Control Record
      case '8':
        break
      //File Control Record. Last line of the fine
      case '9':
        break
      default:
        _logger.warn("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#processLine - " + DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.InvalidRecordTypeCode", code))
        throw new EFTReversalValidationException(DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.InvalidRecordTypeCode", code))
    }

    _logger.trace("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#processLine - Finished processing line: " + line)
  }

  /**
   * Determines if the process should run the doWork() method, based on if there are payment reversal request files to process.
   */
  /**
   * GW666 change query to compare PublicID instead of Name
   * Hermia Kho - 12/15/2015
   */
  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  @Throws(GWConfigurationException, "If there are setup issues, such as issues with retrieving properties from the integration database")
  @Throws(RuntimeException, "If there are connection issues with the bank's FTP server")
  function checkInitialConditions_TDIC() : boolean {
    _logger.debug("TDIC_EFTReversalAndChangeNotificationsBatchJob#checkInitialConditions() - Entering")

    var dbUrl : String
    try {
      // Get required values from integration database
      var env = ServerUtil.getEnv()?.toLowerCase()
      if (env == null) {
        throw new GWConfigurationException("No environment specified.  Cannot load integration database.")
      }
      dbUrl = PropertyUtil.getInstance().getProperty("IntDBURL")
      if (dbUrl == null) {
        throw new GWConfigurationException("Integration Database not defined.")
      }

      _notificationFailureEmail = PropertyUtil.getInstance().getProperty(NOTIFICATION_FAILURE_EMAIL_KEY)
      if (_notificationFailureEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification failure email addresses with the key '"
            + NOTIFICATION_FAILURE_EMAIL_KEY + "' from properties file.")
      }

      _notificationEmail = PropertyUtil.getInstance().getProperty(NOTIFICATION_EMAIL_KEY)
      if (_notificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + NOTIFICATION_EMAIL_KEY + "' from properties file.")
      }

      _prenoteNotificationEmail = PropertyUtil.getInstance().getProperty(PRENOTE_EMAIL_KEY)
      if (_prenoteNotificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve prenote notification email addresses with the key '"
            + PRENOTE_EMAIL_KEY + "' from properties file.")
      }

      _financeARGroup = Query.make(Group).compare(Group#PublicID, Equals, "group:3").select().FirstResult
      if (_financeARGroup == null) {
        throw new GWConfigurationException("Finance AR group does not exists.")
      }

      _incomingPath = PropertyUtil.getInstance().getProperty(INCOMING_DIRECTORY_KEY)
      if (_incomingPath == null) {
        throw new GWConfigurationException("Cannot retrieve incoming file path with the key '" + INCOMING_DIRECTORY_KEY
            + "' from integration database.")
      }

      _donePath = PropertyUtil.getInstance().getProperty(DONE_DIRECTORY_KEY)
      if (_donePath == null) {
        throw new GWConfigurationException("Cannot retrieve completed file path with the key '" + DONE_DIRECTORY_KEY
            + "' from integration database.")
      }

      _errorPath = PropertyUtil.getInstance().getProperty(ERROR_DIRECTORY_KEY)
      if (_errorPath == null) {
        throw new GWConfigurationException("Cannot retrieve error file path with the key '" + ERROR_DIRECTORY_KEY
            + "' from integration database.")
      }
      var errorDirectory = new File(_errorPath)
      if (!errorDirectory.exists()) {
        if (!errorDirectory.mkdirs()) {
          _logger.error("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#checkInitialConditions() - Failed to create error directory: " + _errorPath)
          EmailUtil.sendEmail(_notificationFailureEmail, EMAIL_SUBJECT_FAILURE, "Failed to create error directory:" + _errorPath)
          throw new GWConfigurationException("Failed to create error directory: " + _errorPath)
        }
      }

      var incomingDirectory = new File(_incomingPath)
      if (incomingDirectory.listFiles().Count > 0) {
        _logger.info("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#checkInitialConditions() - Incoming files directory" + _incomingPath + " has " + incomingDirectory.listFiles().Count + " files to process.")
        return true
      } else {
        _logger.info("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#checkInitialConditions() - Incoming files directory" + _incomingPath + " has " + incomingDirectory.listFiles().Count + " files to process.")
        return false
      }
    } catch (gwce : GWConfigurationException) {
      // Log
      _logger.error("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#checkInitialConditions() - Integration database either not property set up or missing required properties or group does not exists. Error: " + gwce.LocalizedMessage)
      EmailUtil.sendEmail(_notificationFailureEmail, EMAIL_SUBJECT_FAILURE,
          "The EFT/ACH Reversal and Change Notifications batch job has failed due to the integration database being not property set up or missing required properties or group does not exists. Error: " + gwce.LocalizedMessage)
      throw gwce
    } catch (e : Exception) {
      // Log
      _logger.error("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#checkInitialConditions() - An unexpected error occurred while determining NACHA return files to process. Error: " + e.LocalizedMessage)
      EmailUtil.sendEmail(_notificationFailureEmail, EMAIL_SUBJECT_FAILURE,
          "The EFT/ACH Reversal and Change Notifications batch job has failed due to an unexpected error occurred while determining NACHA return files to process. Error: " + e.LocalizedMessage)
      throw e
    }
  }

  /**
   * Moves a file to the done directory.
   */
  @Param("sourceFile", "A File object representing the source file to be moved")
  @Returns("A boolean indicating if the move was successful")
  private function moveToDone(sourceFile : File, reader : BufferedReader) : boolean {
    _logger.info("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#moveToDone() - Moving file " + sourceFile.Path + " to " + _donePath)
    // Close reader if open since it will prevent moving the file
    try {
      if (reader != null) {
        reader.close()
      }
    } catch (ioe : IOException) {
      _logger.warn("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#moveToDone() - Error closing file: " + sourceFile.Name, ioe)
    }
    var target = new File(_donePath + "/" + sourceFile.Name)
    return sourceFile.renameTo(target)
  }

  /**
   * Moves a file to the error directory.
   */
  @Param("sourceFile", "A File object representing the source file to be moved")
  @Returns("A boolean indicating if the move was successful")
  private function moveToError(sourceFile : File, reader : BufferedReader) : boolean {
    _logger.info("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#moveToError() - Moving file " + sourceFile.Path + " to " + _errorPath)
    // Close reader if open since it will prevent moving the file
    try {
      if (reader != null) {
        reader.close()
      }
    } catch (ioe : IOException) {
      _logger.warn("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#moveToError() - Error closing file: " + sourceFile.Name, ioe)
    }
    var target = new File(_errorPath + "/" + sourceFile.Name)
    // It is possible that the target file already exists (i.e. if the file failed already before).
    // If exists, delete the old one before moving the new one to the same location.
    if (target.exists()) {
      _logger.info("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#moveToError() - File already exists in error directory.  Deleting existing file.")
      if (!target.delete()) {
        _logger.warn("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#moveToError() - Error deleting file: " + target.Name)
      }
    }
    return sourceFile.renameTo(target)
  }

  /**
   * Called when the batch process is to be terminated.  This sets the TerminateRequested flag and returns true to indicate that the process can be stopped.
   *
   * @return A boolean to indicate that the process can be stopped.
   */
  override function requestTermination() : boolean {
    _logger.info("TDIC_EFTReversalAndChangeNotificationsFileProcessBatchJob#requestTermination() - Terminate requested for batch process.")
    // Set TerminateRequested to true by calling super method
    super.requestTermination()
    return true
  }

  /**
   * @return text for for null string.
   */
  private function stringValue(str: String) : String{
    return str == null ? "N/A" : str
  }

}