package tdic.bc.common.batch.plan

uses gw.api.database.Query
uses gw.api.web.payment.PaymentInstrumentFactory
uses gw.processes.BatchProcessBase
uses gw.transaction.Transaction
uses org.slf4j.LoggerFactory

class ChangeTDICDelinquencyPlan extends BatchProcessBase {

  private static var _logger = LoggerFactory.getLogger("Server.BatchProcess");
  private static final var _LOG_TAG = "${ChangeTDICDelinquencyPlan.Type.RelativeName} - "

  construct() {
    super(BatchProcessType.TC_UPDATEDELINQUENCYPLAN_TDIC);
  }

  override function checkInitialConditions() : boolean {
    return true;
  }

  override function requestTermination() : boolean {
    return true;
  }

  protected override function doWork() {

    // find the plan to be updated
    var dlnqPlan = Query.make(DelinquencyPlan).compare(DelinquencyPlan#Name, Equals, "TDIC Delinquency").select().FirstResult

    var wcDlnqPlan = Query.make(DelinquencyPlan).compare(DelinquencyPlan#Name, Equals, "TDIC WC Delinquency").select().FirstResult
    // Get all the WC accounts
    var query = Query.make(Account).compare(Account#DelinquencyPlan, Equals, wcDlnqPlan)
    var results = query.select()
    _logger.info(_LOG_TAG + "Found " + results.Count + " accounts.")

    for (account in results) {
      if (this.TerminateRequested) {
        break;
      }
      // update the delinquency plan for the accounts.
      if (account.DelinquencyPlan != null){
        changeDelinquencyPlan(account, dlnqPlan)
        incrementOperationsCompleted()
      }
    }
  }

  protected function changeDelinquencyPlan(account : Account, dlnqPlan : DelinquencyPlan) : void {
    _logger.info(_LOG_TAG + account.AccountNumber
        + " - Changing Delinquency plan from WC to Default")
    Transaction.runWithNewBundle(\bundle -> {
      account = bundle.add(account)
      var delinquencyPlan = bundle.add(dlnqPlan)
      account.DelinquencyPlan = delinquencyPlan
    }, "iu");
  }
}