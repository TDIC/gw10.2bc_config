package tdic.bc.common.batch.banklockbox.dto

uses java.math.BigDecimal

/**
 * US567 - Bank/Lockbox Integration
 * 08/19/2014 Alvin Lee
 *
 * Object used to store fields for a transaction line in the bank/lockbox file.
 */
class TransactionLine {

  /**
   * The batch number of the bank/lockbox file.
   */
  var _batchNumber : String as BatchNumber

  /**
   * The transaction sequence number of the batch.
   */
  var _transactionSequenceNumber : String as TransactionSequenceNumber

  /**
   * The amount of the check.
   */
  var _checkAmount : BigDecimal as CheckAmount

  /**
   * The number of the check.
   */
  var _checkNumber : String as CheckNumber

}