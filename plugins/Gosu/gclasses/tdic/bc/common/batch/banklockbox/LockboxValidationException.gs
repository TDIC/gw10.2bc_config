package tdic.bc.common.batch.banklockbox

uses java.lang.Exception

class LockboxValidationException extends Exception {

  construct(message : String) {
    super(message)
  }
}