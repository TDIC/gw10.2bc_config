package tdic.bc.common.batch.banklockbox.helper

uses java.lang.Integer
uses gw.api.system.BCLoggerCategory
uses java.util.Date
uses java.text.SimpleDateFormat
uses java.math.BigDecimal
uses gw.webservice.bc.bc1000.PaymentReceiptRecord
uses gw.webservice.bc.bc1000.PaymentInstrumentRecord
uses tdic.bc.common.batch.banklockbox.LockboxValidationException
uses tdic.bc.common.batch.banklockbox.dto.InvoiceLine
uses tdic.bc.common.batch.banklockbox.dto.LockboxBatchTotal
uses tdic.bc.common.batch.banklockbox.dto.LockboxFileTotal
uses tdic.bc.common.batch.banklockbox.dto.TransactionLine
uses tdic.bc.config.payment.PaymentHelper
uses gw.pl.persistence.core.Bundle
uses java.lang.Exception
uses java.util.Collection
uses gw.api.locale.DisplayKey
uses org.slf4j.LoggerFactory

/**
 * US567 - Bank/Lockbox Integration
 * 01/09/2015 Alvin Lee
 *
 * Helper class to process the lines of the Bank/Lockbox flat file.
 * TODO Refactor line substrings with SDF Utility
 */
class TDIC_BankLockboxHelper {

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("BANK_LOCKBOX")

  /**
   * Gets the Batch Deposit Date from a batch header line from the flat file.
   */
  @Param("line", "A String containing the line of text from the flat file")
  @Returns("An Date object representing the batch deposit date")
  @Throws(LockboxValidationException, "If there are any issues parsing the batch deposit date from the line of text")
  public static function getBatchDepositDateFromHeader(line : String) : Date {
    _logger.debug("TDIC_BankLockboxHelper#getBatchDepositDateFromHeader - Processing batch header line...")
    var batchDepositDate = line.substring(14, 20)
    _logger.debug("  Batch Deposit Date: " + batchDepositDate)
    if (!batchDepositDate.matches("[\\d]{6}")) {
      _logger.warn("TDIC_BankLockboxBatch#processLine - " + DisplayKey.get("TDIC.Validation.BankLockbox.BatchDepositDateParseError", batchDepositDate))
      throw new LockboxValidationException(DisplayKey.get("TDIC.Validation.BankLockbox.BatchDepositDateParseError", batchDepositDate))
    }
    return new SimpleDateFormat("yyMMdd").parse(batchDepositDate)
  }

  /**
   * Gets an TransactionLine item from a line of text from the flat file.
   */
  @Param("line", "A String containing the line of text from the flat file")
  @Returns("A TransactionLine object containing information about the invoice line")
  public static function getTransactionLine(line : String) : TransactionLine {
    _logger.debug("TDIC_BankLockboxHelper#getTransactionLine - Processing transaction line...")
    var transLine = new TransactionLine()
    transLine.BatchNumber = line.substring(1, 4)
    transLine.TransactionSequenceNumber = line.substring(4, 7)
    var paymentID = transLine.BatchNumber + "-" + transLine.TransactionSequenceNumber
    var checkAmount = line.substring(7, 15) + "." + line.substring(15, 17)
    transLine.CheckAmount = new BigDecimal(checkAmount)
    transLine.CheckNumber = line.substring(36, 44)
    _logger.trace("  Batch Number: " + transLine.BatchNumber)
    _logger.trace("  Transaction Sequence Number: " + transLine.TransactionSequenceNumber)
    _logger.trace("  Check Amount: " + transLine.CheckAmount)
    _logger.trace("  Check Number: " + transLine.CheckNumber)
    return transLine
  }

  /**
   * Gets an InvoiceLine item from a line of text from the flat file.
   */
  @Param("line", "A String containing the line of text from the flat file")
  @Returns("An InvoiceLine object containing information about the invoice line")
  public static function getInvoiceLine(line : String) : InvoiceLine {
    _logger.debug("TDIC_BankLockboxHelper#getInvoiceLine - Processing invoice line...")
    var invLine = new InvoiceLine()
    invLine.PolicyNumber = getPolicyNumber(line)
    try{
      invLine.TermNumber = Integer.parseInt(line.substring(21, 23))
    }catch(e: Exception){
      _logger.warn("TDIC_BankLockboxBatch#getInvoiceLine(String) - ${e.LocalizedMessage} replaced with 0 in InvoiceLine: ${line}")
      invLine.TermNumber = 0
    }
    invLine.InvoiceNumber = line.substring(23, 33)
    var invAmount = line.substring(39, 47) + "." + line.substring(47, 49)
    if(null != invAmount)
    invLine.InvoiceAmount = new BigDecimal(invAmount)
    _logger.trace("  Policy Number: " + invLine.PolicyNumber)
    _logger.trace("  Term Number: " + invLine.TermNumber)
    _logger.trace("  Invoice Number: " + invLine.InvoiceNumber)
    _logger.trace("  Invoice Amount: " + invLine.InvoiceAmount)
    return invLine
  }

  /**
   * Returns truncated policy number for CHSI coupon code 2022, CAWC and migrated policies.
   */
  private static function getPolicyNumber(line : String) : String {

    var polNumber = line.substring(11,21)
    if((polNumber.substring(0,6) == '000000') or (polNumber.substring(0,4) == '2022') or (polNumber.substring(0,4) == 'CAWC')){
      polNumber = polNumber.substring(4,10)
    }
    return polNumber
  }

  /**
   * Gets the totals from the batch trailer line of the flat file.
   */
  @Param("line", "A String containing the line of text from the flat file")
  @Returns("An LockboxBatchTotal object containing the totals from the batch trailer line")
  @Throws(LockboxValidationException, "If there are problems parsing the total amount from the line")
  public static function getBatchTotals(line : String) : LockboxBatchTotal {
    _logger.debug("TDIC_BankLockboxHelper#getBatchTotals - Processing batch trailer line...")
    var batchTotals = new LockboxBatchTotal()
    batchTotals.BatchNumber = line.substring(1, 4)
    batchTotals.TotalNumberTransactions = Integer.parseInt(line.substring(20, 23))
    var batchTotalAmount = line.substring(23, 31) + "." + line.substring(31, 33)
    if (!batchTotalAmount.matches("[\\d]{8}\\.[\\d]{2}")) {
      _logger.warn("TDIC_BankLockboxBatch#processLine - " + DisplayKey.get("TDIC.Validation.BankLockbox.BatchTotalAmountParseError", batchTotalAmount))
      throw new LockboxValidationException(DisplayKey.get("TDIC.Validation.BankLockbox.BatchTotalAmountParseError", batchTotalAmount))
    }
    batchTotals.TotalAmount = new BigDecimal(batchTotalAmount)
    _logger.trace("  Batch Number: " + batchTotals.BatchNumber)
    _logger.trace("  Batch Total Number of Transactions: " + batchTotals.TotalNumberTransactions)
    _logger.trace("  Batch Total Amount: " + batchTotals.TotalAmount)
    return batchTotals
  }

  /**
   * Gets the totals from the file trailer line of the flat file.
   */
  @Param("line", "A String containing the line of text from the flat file")
  @Returns("An LockboxFileTotal object containing the totals from the lockbox file")
  @Throws(LockboxValidationException, "If there are problems parsing the total amount from the line")
  public static function getFileTotals(line : String) : LockboxFileTotal {
    _logger.debug("TDIC_BankLockboxHelper#getFileTotals - Processing lockbox trailer line...")
    var fileTotals = new LockboxFileTotal()
    fileTotals.TotalNumberTransactions = Integer.parseInt(line.substring(20, 24))
    var fileTotalAmount = line.substring(24, 32) + "." + line.substring(32, 34)
    if (!fileTotalAmount.matches("[\\d]{8}\\.[\\d]{2}")) {
      _logger.warn("TDIC_BankLockboxBatch#processLine - " + DisplayKey.get("TDIC.Validation.BankLockbox.FileTotalAmountParseError", fileTotalAmount))
      throw new LockboxValidationException(DisplayKey.get("TDIC.Validation.BankLockbox.FileTotalAmountParseError", fileTotalAmount))
    }
    fileTotals.TotalAmount = new BigDecimal(fileTotalAmount)
    _logger.trace("  File Total Number of Transactions: " + fileTotals.TotalNumberTransactions)
    _logger.trace("  File Total Amount: " + fileTotals.TotalAmount)
    return fileTotals
  }

  /**
   * Process the payment using the information from the bank/lockbox file.
   */
  @Param("batchDepositDate", "The deposit date of the payment, which is the same for all checks within a batch")
  @Param("invLine", "The InvoiceLine object containing the data to process the payment.")
  @Param("transLine", "The TransactionLine object containing the data to process the payment.")
  @Throws(LockboxValidationException, "If the batch deposit date has not been read prior to processing the payment")
  public static function processPayment(batchDepositDate : Date, invLine : InvoiceLine, transLine : TransactionLine, bundle:Bundle) {
    _logger.debug("TDIC_BankLockboxHelper#processPayment - Entering")
    if (batchDepositDate == null) {
      _logger.warn("TDIC_BankLockboxHelper#processPayment - " + DisplayKey.get("TDIC.Validation.BankLockbox.BatchDepositDateMissing"))
      throw new LockboxValidationException(DisplayKey.get("TDIC.Validation.BankLockbox.BatchDepositDateMissing"))
    }
    var directBillPaymentReceipt = new PaymentReceiptRecord()
    directBillPaymentReceipt.ReceivedDate = batchDepositDate
    directBillPaymentReceipt.PaymentDate = batchDepositDate
    directBillPaymentReceipt.RefNumber = transLine.CheckNumber
    directBillPaymentReceipt.MonetaryAmount = transLine.CheckAmount.ofCurrency(Currency.TC_USD)
    directBillPaymentReceipt.LockboxTransactionNumber_TDIC = transLine.BatchNumber + "-" + transLine.TransactionSequenceNumber
    var instrument = new PaymentInstrumentRecord()
    instrument.PaymentMethod = PaymentMethod.TC_LOCKBOX
    directBillPaymentReceipt.PaymentInstrumentRecord = instrument
    directBillPaymentReceipt.PolicyNumber = invLine.PolicyNumber + "-" + invLine.TermNumber
    directBillPaymentReceipt.InvoiceNumber_TDIC = invLine.InvoiceNumber

    var paymentHelper = new PaymentHelper()

    // Attempt to process payment using invoice
    _logger.debug("TDIC_BankLockboxHelper#processPayment - Attempting to find an invoice with invoice number '" + invLine.InvoiceNumber + "'")
    var invoiceQuery = gw.api.database.Query.make(Invoice)
    invoiceQuery.compare(Invoice#InvoiceNumber, Equals, invLine.InvoiceNumber)
        .compare(Invoice#Amount, Equals, invLine.InvoiceAmount.ofCurrency(Currency.TC_USD))
    var invoiceResult = invoiceQuery.select().FirstResult
    var period = PolicyPeriod.finder.findByPolicyNumberAndTerm(invLine.PolicyNumber, invLine.TermNumber)

    if (invoiceResult != null && invoiceResult.Payer typeis Account) { // Found Invoice
      // If invoice has an invoice stream targeted towards a single policy period, distribute to that policy period
      if (invoiceResult.Policies.Count == 1){
        directBillPaymentReceipt.PaymentReceiptType = DIRECTBILLMONEYDETAILS
        var paymentAccount = invoiceResult.Payer
        _logger.info("TDIC_BankLockboxHelper#processPayment - Found invoice ${invoiceResult.InvoiceNumber} on Account " + paymentAccount.DisplayName)
        directBillPaymentReceipt.AccountNumber = paymentAccount.AccountNumber
        directBillPaymentReceipt.AccountID = paymentAccount.PublicID
        _logger.debug("TDIC_BankLockboxHelper#processPayment - Invoice has an invoice stream targeted towards a single invoice policy period.  Making a payment to that invoice.")
        var paymentID = paymentHelper.makeDirectBillPaymentToInvoice(directBillPaymentReceipt, invoiceResult.PublicID)
        _logger.info("TDIC_BankLockboxHelper#processPayment - Payment made using the InvoiceNumber with DirectBillMoneyRcvd Public ID: " + paymentID)
      }
    }
    else if (invoiceResult == null and period != null){ // No Invoice Found
      // Attempt to process payment using policy period
      _logger.info("TDIC_BankLockboxHelper#processPayment - Invoice not found.  Attempting to find policy period with policy number '"
          + invLine.PolicyNumber + "' and term '" + invLine.TermNumber + "'")
      // Process payment using policy period

        _logger.debug("TDIC_BankLockboxHelper#processPayment - Found policy period: " + period.PolicyNumberLong)

          directBillPaymentReceipt.PaymentReceiptType = DIRECTBILLMONEYDETAILS
          var paymentAccount = period.Account
          directBillPaymentReceipt.AccountNumber = paymentAccount.AccountNumber
          directBillPaymentReceipt.AccountID = paymentAccount.PublicID
          var paymentID = paymentHelper.makeDirectBillPaymentToPolicyPeriod(directBillPaymentReceipt, period.PublicID)
          _logger.info("TDIC_BankLockboxHelper#processPayment - Payment made on using PolicyNumber ${period.PolicyNumber} with DirectBillMoneyRcvd Public ID: " + paymentID)
      }
      else { //Neither Invoice nor Policy Found
        // Create Suspense Payment
        _logger.debug("TDIC_BankLockboxHelper#processPayment - Creating suspense payment since no match was found for policy number '" + invLine.PolicyNumber
            + "', term '" + invLine.TermNumber + "', or invoice number '" + invLine.InvoiceNumber + "'")
        directBillPaymentReceipt.PaymentReceiptType = SUSPENSEPAYMENT
        var paymentID = paymentHelper.makeSuspensePayment(directBillPaymentReceipt)
        _logger.debug("TDIC_BankLockboxHelper#processPayment - SuspensePayment created with Public ID: " + paymentID)
    }
  }

  /**
   * Apply suspense payment to target policy term unapplied fund.
   */
  @Param("suspensePayment", "Suspense paymnet transaction which is source for funds transfer.")
  @Param("targetPolicyPeriod", "Policy term to which payments get transferred to.")
  @Param("bundle", "Current bundle to commit transaction.")
  @Throws(LockboxValidationException, "If the batch deposit date has not been read prior to processing the payment")
  public static function applySuspensePaymentToTargetPolicyPeriod(aSuspensePayment: SuspensePayment, targetPolicyPeriod: PolicyPeriod, bundle: Bundle){
    try{
    /*GWPS-2004 : Fix the issue while applying the suspense payment to the policy. Instead of calling the processPayment() menthod, creating the
    * DBMR here itself using the details in the suspense payment.*/
      var  batchDepositDate = gw.api.util.DateUtil.currentDate()

      var directBillPaymentReceipt = new PaymentReceiptRecord()
      directBillPaymentReceipt.ReceivedDate = batchDepositDate
      directBillPaymentReceipt.PaymentDate = batchDepositDate
      directBillPaymentReceipt.RefNumber = aSuspensePayment.RefNumber
      directBillPaymentReceipt.MonetaryAmount = aSuspensePayment.Amount_amt.ofCurrency(Currency.TC_USD)
      directBillPaymentReceipt.LockboxTransactionNumber_TDIC = aSuspensePayment.LockboxTransactionNumber_TDIC //transLine.BatchNumber + "-" + transLine.TransactionSequenceNumber
      var instrument = new PaymentInstrumentRecord()
      instrument.PaymentMethod = PaymentMethod.TC_LOCKBOX
      directBillPaymentReceipt.PaymentInstrumentRecord = instrument
      directBillPaymentReceipt.PolicyNumber = targetPolicyPeriod.PolicyNumber + "-" + targetPolicyPeriod.TermNumber

      var paymentHelper = new PaymentHelper()

      directBillPaymentReceipt.PaymentReceiptType = DIRECTBILLMONEYDETAILS
      var paymentAccount = targetPolicyPeriod.Account
      directBillPaymentReceipt.AccountNumber = paymentAccount.AccountNumber
      directBillPaymentReceipt.AccountID = paymentAccount.PublicID
      var paymentID = paymentHelper.makeDirectBillPaymentToPolicyPeriod(directBillPaymentReceipt, targetPolicyPeriod.PublicID)
      _logger.debug("TDIC_BankLockboxHelper#applySuspensePaymentToTargetPolicyPeriod - Payment made with DirectBillMoneyRcvd Public ID: " + paymentID)

     aSuspensePayment.PolicyNumber = targetPolicyPeriod.PolicyNumberLong
     aSuspensePayment.AppliedByUser = User.util.CurrentUser
     aSuspensePayment.Status =  SuspensePaymentStatus.TC_APPLIED
     aSuspensePayment.setFieldValue(SuspensePayment.Type.TypeInfo.getProperty("PolicyPeriodAppliedTo").Name,targetPolicyPeriod)
      //create a reverse suspense transaction to offset suspense payment
      var t = aSuspensePayment.DisplayTransaction
      t = bundle.add(t)
      t.reverse()
      //update suspense payment with direct bill money received transaction number

        var dbmrt = bundle.getBeansByRootType(entity.Transaction)?.firstWhere( \ elt -> elt typeis entity.DirectBillMoneyReceivedTxn) as DirectBillMoneyReceivedTxn
      aSuspensePayment.setFieldValue(SuspensePayment.Type.TypeInfo.getProperty("PaymentMoneyReceived").Name,dbmrt.DirectBillMoneyRcvd)

    }catch(e: Exception){
      _logger.error("TDIC_BankLockboxHelper#applySuspensePaymentToTargetPolicyPeriod - Error in transferring suspense payment: ${SuspensePayment} to policy term: ${targetPolicyPeriod}",e)
      throw e
    }
  }
}