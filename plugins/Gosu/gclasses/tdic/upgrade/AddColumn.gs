package tdic.upgrade

uses gw.api.database.upgrade.before.BeforeUpgradeVersionTrigger
uses org.slf4j.LoggerFactory

// SujitN - Class that can be used to add a column.
class AddColumn extends BeforeUpgradeVersionTrigger {

  private static var _logger = LoggerFactory.getLogger("Server.Database.Upgrade");
  private static final var _LOG_TAG = "${AddColumn.Type.RelativeName} - "

  private var _versionNumber : int as readonly VersionNumber;
  private var _tableName : String as readonly TableName;
  private var _newcolumnName : String as readonly ColumnName;

  construct(versionNumber : int, tableName : String, columnName : String) {
    super(versionNumber);
    _versionNumber = versionNumber;
    _tableName = tableName;
    _newcolumnName = columnName;
  }

  override public property get Description() : String {
    return "adding column to " + _tableName + "." + _newcolumnName;
  }

  // SujitN - No version checks are needed for this upgrade.
  override function hasVersionCheck() : boolean {
    return false;
  }

  override function execute() : void {
    var table = getTable(_tableName);
    var newColumn = table.getColumn(_newcolumnName);

    // Step 1:  Create new column.
    _logger.info (_LOG_TAG + "Creating column: " + _tableName + "." + _newcolumnName);
    newColumn.create();

    // Step 2:  Set column to null.
    var builder = table.update();
    builder.set(newColumn, null);
    builder.execute();

    _logger.info (_LOG_TAG + "Completed " + Description);
  }
}