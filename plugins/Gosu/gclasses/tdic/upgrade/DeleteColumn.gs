package tdic.upgrade

uses gw.api.database.upgrade.before.BeforeUpgradeVersionTrigger
uses org.slf4j.LoggerFactory

// SujitN - Class that can be used to delete rows from a table.
class DeleteColumn extends BeforeUpgradeVersionTrigger {

  private static var _logger = LoggerFactory.getLogger("Server.Database.Upgrade");
  private static final var _LOG_TAG = "${DeleteColumn.Type.RelativeName} - "

  private var _versionNumber : int as readonly VersionNumber;
  private var _tableName : String as readonly TableName;
  private var _columnName : String as readonly ColumnName;

  construct(versionNumber : int, tableName : String, columnName : String) {
    super(versionNumber);
    _versionNumber = versionNumber;
    _tableName = tableName;
    _columnName = columnName;
  }

  override public property get Description() : String {
    return "Removing from " + _tableName + "." + _columnName;
  }

  // SujitN - No version checks are needed for this upgrade.
  override function hasVersionCheck() : boolean {
    return false;
  }

  override function execute() : void {
    var table = getTable(_tableName);
    var column = table.getColumn(_columnName)

    // Step 1:  delete rows matching criteria.
    var builder = table.delete();
    builder.compareIn(column, {1, 18, 21, 25, 26, 27, 29, 38})
    builder.execute();

    _logger.info (_LOG_TAG + "Completed " + Description);
  }
}