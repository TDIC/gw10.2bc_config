package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/AgencyCommissionPaymentWizard_ConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyCommissionPaymentWizard_ConfirmationScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/AgencyCommissionPaymentWizard_ConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyCommissionPaymentWizard_ConfirmationScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=AmountReceived_Input) at AgencyCommissionPaymentWizard_ConfirmationScreen.pcf: line 42, column 51
    function currency_11 () : typekey.Currency {
      return incomingProducerPayment.Currency
    }
    
    // 'value' attribute on TextInput (id=ProducerName_Input) at AgencyCommissionPaymentWizard_ConfirmationScreen.pcf: line 23, column 40
    function valueRoot_1 () : java.lang.Object {
      return incomingProducerPayment
    }
    
    // 'value' attribute on TextInput (id=ProducerName_Input) at AgencyCommissionPaymentWizard_ConfirmationScreen.pcf: line 23, column 40
    function value_0 () : entity.Producer {
      return incomingProducerPayment.Producer
    }
    
    // 'value' attribute on TextInput (id=PaymentInstrument_Input) at AgencyCommissionPaymentWizard_ConfirmationScreen.pcf: line 33, column 49
    function value_3 () : entity.PaymentInstrument {
      return incomingProducerPayment.PaymentInstrument
    }
    
    // 'value' attribute on TextInput (id=RefNumber_Input) at AgencyCommissionPaymentWizard_ConfirmationScreen.pcf: line 37, column 54
    function value_6 () : java.lang.String {
      return incomingProducerPayment.RefNumber
    }
    
    // 'value' attribute on MonetaryAmountInput (id=AmountReceived_Input) at AgencyCommissionPaymentWizard_ConfirmationScreen.pcf: line 42, column 51
    function value_9 () : gw.pl.currency.MonetaryAmount {
      return incomingProducerPayment.Amount
    }
    
    property get incomingProducerPayment () : IncomingProducerPayment {
      return getRequireValue("incomingProducerPayment", 0) as IncomingProducerPayment
    }
    
    property set incomingProducerPayment ($arg :  IncomingProducerPayment) {
      setRequireValue("incomingProducerPayment", 0, $arg)
    }
    
    
  }
  
  
}