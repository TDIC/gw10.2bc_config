package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/AccountViewBalanceSummaryLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountViewBalanceSummaryLVExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/acct360/AccountViewBalanceSummaryLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountViewBalanceSummaryLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RowIterator (id=TransactionActivitiesDetailsIterator) at AccountViewBalanceSummaryLV.pcf: line 20, column 66
    function value_37 () : gw.acc.acct360.accountview.Acct360TranDetail[] {
      return transactions //.sortBy(\o -> o.TransactionDate)
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get transactions () : gw.acc.acct360.accountview.Acct360TranDetail[] {
      return getRequireValue("transactions", 0) as gw.acc.acct360.accountview.Acct360TranDetail[]
    }
    
    property set transactions ($arg :  gw.acc.acct360.accountview.Acct360TranDetail[]) {
      setRequireValue("transactions", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/acct360/AccountViewBalanceSummaryLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountViewBalanceSummaryLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on MonetaryAmountCell (id=amt_Cell) at AccountViewBalanceSummaryLV.pcf: line 68, column 36
    function actionAvailable_24 () : java.lang.Boolean {
      return trans.AcctBalanceTran.Transactions.Count > 0
    }
    
    // 'actionAvailable' attribute on TextCell (id=PolicyNumber_Cell) at AccountViewBalanceSummaryLV.pcf: line 40, column 31
    function actionAvailable_8 () : java.lang.Boolean {
      return trans.PolicyPeriod!=null
    }
    
    // 'action' attribute on TextCell (id=newdesc_Cell) at AccountViewBalanceSummaryLV.pcf: line 59, column 31
    function action_18 () : void {
      gw.acc.acct360.accountview.AccountBalanceTxnUtil.navigate360(account, trans)
    }
    
    // 'action' attribute on MonetaryAmountCell (id=amt_Cell) at AccountViewBalanceSummaryLV.pcf: line 68, column 36
    function action_22 () : void {
      Acct360ViewTransactionsPopup.push(trans.AcctBalanceTran.Transactions)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at AccountViewBalanceSummaryLV.pcf: line 40, column 31
    function action_6 () : void {
      PolicyDetailSummaryPopup.push(trans.PolicyPeriod)
    }
    
    // 'action' attribute on MonetaryAmountCell (id=amt_Cell) at AccountViewBalanceSummaryLV.pcf: line 68, column 36
    function action_dest_23 () : pcf.api.Destination {
      return pcf.Acct360ViewTransactionsPopup.createDestination(trans.AcctBalanceTran.Transactions)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at AccountViewBalanceSummaryLV.pcf: line 40, column 31
    function action_dest_7 () : pcf.api.Destination {
      return pcf.PolicyDetailSummaryPopup.createDestination(trans.PolicyPeriod)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=amt_Cell) at AccountViewBalanceSummaryLV.pcf: line 68, column 36
    function currency_27 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on DateCell (id=TransactionDate_Cell) at AccountViewBalanceSummaryLV.pcf: line 26, column 42
    function valueRoot_1 () : java.lang.Object {
      return trans
    }
    
    // 'value' attribute on TextCell (id=product_Cell) at AccountViewBalanceSummaryLV.pcf: line 46, column 32
    function valueRoot_13 () : java.lang.Object {
      return trans.PolicyPeriod.Policy
    }
    
    // 'value' attribute on TextCell (id=jurisdiction_Cell) at AccountViewBalanceSummaryLV.pcf: line 52, column 37
    function valueRoot_16 () : java.lang.Object {
      return trans.PolicyPeriod
    }
    
    // 'value' attribute on DateCell (id=TransactionDate_Cell) at AccountViewBalanceSummaryLV.pcf: line 26, column 42
    function value_0 () : java.util.Date {
      return trans.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=product_Cell) at AccountViewBalanceSummaryLV.pcf: line 46, column 32
    function value_12 () : LOBCode {
      return trans.PolicyPeriod.Policy.LOBCode
    }
    
    // 'value' attribute on TextCell (id=jurisdiction_Cell) at AccountViewBalanceSummaryLV.pcf: line 52, column 37
    function value_15 () : Jurisdiction {
      return trans.PolicyPeriod.RiskJurisdiction
    }
    
    // 'value' attribute on TextCell (id=newdesc_Cell) at AccountViewBalanceSummaryLV.pcf: line 59, column 31
    function value_19 () : String {
      return trans.Description
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amt_Cell) at AccountViewBalanceSummaryLV.pcf: line 68, column 36
    function value_25 () : gw.pl.currency.MonetaryAmount {
      return trans.NetAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OutstandingAmount_Cell) at AccountViewBalanceSummaryLV.pcf: line 75, column 45
    function value_29 () : gw.pl.currency.MonetaryAmount {
      return trans.OutstandingBalance
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at AccountViewBalanceSummaryLV.pcf: line 31, column 40
    function value_3 () : java.util.Date {
      return trans.EffectiveDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=balance_Cell) at AccountViewBalanceSummaryLV.pcf: line 81, column 40
    function value_33 () : gw.pl.currency.MonetaryAmount {
      return trans.BalanceAmount
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AccountViewBalanceSummaryLV.pcf: line 40, column 31
    function value_9 () : String {
      return trans.PolicyNumber
    }
    
    property get trans () : gw.acc.acct360.accountview.Acct360TranDetail {
      return getIteratedValue(1) as gw.acc.acct360.accountview.Acct360TranDetail
    }
    
    
  }
  
  
}