package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/TroubleTicketSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TroubleTicketSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/search/TroubleTicketSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Number_Cell) at TroubleTicketSearchScreen.pcf: line 38, column 31
    function action_9 () : void {
      TroubleTicketDetailsPopup.push(troubleTicket)
    }
    
    // 'action' attribute on TextCell (id=Number_Cell) at TroubleTicketSearchScreen.pcf: line 38, column 31
    function action_dest_10 () : pcf.api.Destination {
      return pcf.TroubleTicketDetailsPopup.createDestination(troubleTicket)
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at TroubleTicketSearchScreen.pcf: line 38, column 31
    function valueRoot_12 () : java.lang.Object {
      return troubleTicket
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at TroubleTicketSearchScreen.pcf: line 38, column 31
    function value_11 () : java.lang.String {
      return troubleTicket.TroubleTicketNumber
    }
    
    // 'value' attribute on DateCell (id=TargetOrCloseDate_Cell) at TroubleTicketSearchScreen.pcf: line 43, column 58
    function value_14 () : java.util.Date {
      return troubleTicket.TargetOrCloseDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at TroubleTicketSearchScreen.pcf: line 49, column 31
    function value_17 () : typekey.Priority {
      return troubleTicket.Priority
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at TroubleTicketSearchScreen.pcf: line 55, column 31
    function value_20 () : java.lang.String {
      return troubleTicket.TicketStatus
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at TroubleTicketSearchScreen.pcf: line 60, column 46
    function value_23 () : java.lang.String {
      return troubleTicket.Title
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at TroubleTicketSearchScreen.pcf: line 66, column 31
    function value_26 () : java.lang.String {
      return troubleTicket.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at TroubleTicketSearchScreen.pcf: line 72, column 31
    function value_29 () : java.lang.String {
      return troubleTicket.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=CreatedBy_Cell) at TroubleTicketSearchScreen.pcf: line 77, column 42
    function value_32 () : entity.User {
      return troubleTicket.CreateUser
    }
    
    // 'value' attribute on TextCell (id=AssignedTo_Cell) at TroubleTicketSearchScreen.pcf: line 82, column 42
    function value_35 () : entity.User {
      return troubleTicket.AssignedUser
    }
    
    // 'value' attribute on TextCell (id=AssignedToQueue_Cell) at TroubleTicketSearchScreen.pcf: line 87, column 53
    function value_38 () : entity.AssignableQueue {
      return troubleTicket.AssignedQueue
    }
    
    property get troubleTicket () : entity.TroubleTicket {
      return getIteratedValue(2) as entity.TroubleTicket
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/TroubleTicketSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends TroubleTicketSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at TroubleTicketSearchScreen.pcf: line 17, column 53
    function def_onEnter_0 (def :  pcf.TroubleTicketSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at TroubleTicketSearchScreen.pcf: line 17, column 53
    function def_refreshVariables_1 (def :  pcf.TroubleTicketSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'maxSearchResults' attribute on SearchPanel at TroubleTicketSearchScreen.pcf: line 15, column 81
    function maxSearchResults_42 () : java.lang.Object {
      return gw.api.system.BCConfigParameters.MaxSearchResults.Value
    }
    
    // 'searchCriteria' attribute on SearchPanel at TroubleTicketSearchScreen.pcf: line 15, column 81
    function searchCriteria_44 () : gw.search.TroubleTicketSearchCriteria {
      return new gw.search.TroubleTicketSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at TroubleTicketSearchScreen.pcf: line 15, column 81
    function search_43 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria)
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at TroubleTicketSearchScreen.pcf: line 38, column 31
    function sortValue_2 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.TroubleTicketNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at TroubleTicketSearchScreen.pcf: line 49, column 31
    function sortValue_3 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.Priority
    }
    
    // 'sortBy' attribute on TextCell (id=Status_Cell) at TroubleTicketSearchScreen.pcf: line 55, column 31
    function sortValue_4 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.CloseDate
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at TroubleTicketSearchScreen.pcf: line 60, column 46
    function sortValue_5 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.Title
    }
    
    // 'value' attribute on TextCell (id=CreatedBy_Cell) at TroubleTicketSearchScreen.pcf: line 77, column 42
    function sortValue_6 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.CreateUser
    }
    
    // 'value' attribute on TextCell (id=AssignedTo_Cell) at TroubleTicketSearchScreen.pcf: line 82, column 42
    function sortValue_7 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.AssignedUser
    }
    
    // 'value' attribute on TextCell (id=AssignedToQueue_Cell) at TroubleTicketSearchScreen.pcf: line 87, column 53
    function sortValue_8 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.AssignedQueue
    }
    
    // 'value' attribute on RowIterator at TroubleTicketSearchScreen.pcf: line 30, column 86
    function value_41 () : gw.api.database.IQueryBeanResult<entity.TroubleTicket> {
      return troubleTickets
    }
    
    property get searchCriteria () : gw.search.TroubleTicketSearchCriteria {
      return getCriteriaValue(1) as gw.search.TroubleTicketSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.TroubleTicketSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    property get troubleTickets () : gw.api.database.IQueryBeanResult<TroubleTicket> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<TroubleTicket>
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/TroubleTicketSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TroubleTicketSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    
  }
  
  
}