package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/CommissionReceivableReductionWizardTargetsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionReceivableReductionWizardTargetsStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/CommissionReceivableReductionWizardTargetsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionReceivableReductionWizardTargetsStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=PayableBalance_Input) at CommissionReceivableReductionWizardTargetsStepScreen.pcf: line 22, column 63
    function currency_2 () : typekey.Currency {
      return helper.Producer.Currency
    }
    
    // 'value' attribute on RangeInput (id=ProducerCode_Input) at CommissionReceivableReductionWizardTargetsStepScreen.pcf: line 37, column 43
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      helper.ProducerCode = (__VALUE_TO_SET as entity.ProducerCode)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at CommissionReceivableReductionWizardTargetsStepScreen.pcf: line 29, column 34
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      helper.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at CommissionReceivableReductionWizardTargetsStepScreen.pcf: line 37, column 43
    function valueRange_12 () : java.lang.Object {
      return helper.Producer.ProducerCodes
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PayableBalance_Input) at CommissionReceivableReductionWizardTargetsStepScreen.pcf: line 22, column 63
    function valueRoot_1 () : java.lang.Object {
      return helper.ProducerCode
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at CommissionReceivableReductionWizardTargetsStepScreen.pcf: line 29, column 34
    function valueRoot_6 () : java.lang.Object {
      return helper
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PayableBalance_Input) at CommissionReceivableReductionWizardTargetsStepScreen.pcf: line 22, column 63
    function value_0 () : gw.pl.currency.MonetaryAmount {
      return helper.ProducerCode.TotalCommissionPayable
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at CommissionReceivableReductionWizardTargetsStepScreen.pcf: line 29, column 34
    function value_4 () : gw.pl.currency.MonetaryAmount {
      return helper.Amount
    }
    
    // 'value' attribute on RangeInput (id=ProducerCode_Input) at CommissionReceivableReductionWizardTargetsStepScreen.pcf: line 37, column 43
    function value_9 () : entity.ProducerCode {
      return helper.ProducerCode
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at CommissionReceivableReductionWizardTargetsStepScreen.pcf: line 37, column 43
    function verifyValueRangeIsAllowedType_13 ($$arg :  entity.ProducerCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at CommissionReceivableReductionWizardTargetsStepScreen.pcf: line 37, column 43
    function verifyValueRangeIsAllowedType_13 ($$arg :  gw.api.database.IQueryBeanResult<entity.ProducerCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at CommissionReceivableReductionWizardTargetsStepScreen.pcf: line 37, column 43
    function verifyValueRangeIsAllowedType_13 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at CommissionReceivableReductionWizardTargetsStepScreen.pcf: line 37, column 43
    function verifyValueRange_14 () : void {
      var __valueRangeArg = helper.Producer.ProducerCodes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_13(__valueRangeArg)
    }
    
    property get helper () : gw.producer.CommissionReceivableReductionHelper {
      return getRequireValue("helper", 0) as gw.producer.CommissionReceivableReductionHelper
    }
    
    property set helper ($arg :  gw.producer.CommissionReceivableReductionHelper) {
      setRequireValue("helper", 0, $arg)
    }
    
    
  }
  
  
}