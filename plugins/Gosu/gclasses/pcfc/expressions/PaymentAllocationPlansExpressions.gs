package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/allocationplan/PaymentAllocationPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentAllocationPlansExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/allocationplan/PaymentAllocationPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PaymentAllocationPlansExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at PaymentAllocationPlans.pcf: line 44, column 51
    function action_10 () : void {
      PaymentAllocationPlanDetail.go(paymentAllocationPlan)
    }
    
    // 'action' attribute on Link (id=MoveUp) at PaymentAllocationPlans.pcf: line 68, column 67
    function action_26 () : void {
      PlanHelper.moveUp(paymentAllocationPlan)
    }
    
    // 'action' attribute on Link (id=MoveDown) at PaymentAllocationPlans.pcf: line 76, column 73
    function action_29 () : void {
      PlanHelper.moveDown(paymentAllocationPlan)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at PaymentAllocationPlans.pcf: line 44, column 51
    function action_dest_11 () : pcf.api.Destination {
      return pcf.PaymentAllocationPlanDetail.createDestination(paymentAllocationPlan)
    }
    
    // 'available' attribute on Link (id=MoveUp) at PaymentAllocationPlans.pcf: line 68, column 67
    function available_24 () : java.lang.Boolean {
      return perm.System.payallocplanedit
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at PaymentAllocationPlans.pcf: line 38, column 46
    function valueRoot_8 () : java.lang.Object {
      return paymentAllocationPlan
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at PaymentAllocationPlans.pcf: line 44, column 51
    function value_12 () : java.lang.String {
      return paymentAllocationPlan.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at PaymentAllocationPlans.pcf: line 49, column 58
    function value_15 () : java.lang.String {
      return paymentAllocationPlan.Description
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at PaymentAllocationPlans.pcf: line 53, column 60
    function value_18 () : java.util.Date {
      return paymentAllocationPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at PaymentAllocationPlans.pcf: line 57, column 61
    function value_21 () : java.util.Date {
      return paymentAllocationPlan.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at PaymentAllocationPlans.pcf: line 38, column 46
    function value_7 () : java.lang.Integer {
      return paymentAllocationPlan.PlanOrder
    }
    
    // 'visible' attribute on Link (id=MoveUp) at PaymentAllocationPlans.pcf: line 68, column 67
    function visible_25 () : java.lang.Boolean {
      return paymentAllocationPlan.PlanOrder > 1
    }
    
    // 'visible' attribute on Link (id=MoveDown) at PaymentAllocationPlans.pcf: line 76, column 73
    function visible_28 () : java.lang.Boolean {
      return !paymentAllocationPlan.hasHighestPlanOrder()
    }
    
    property get paymentAllocationPlan () : entity.PaymentAllocationPlan {
      return getIteratedValue(1) as entity.PaymentAllocationPlan
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/allocationplan/PaymentAllocationPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentAllocationPlansExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=PaymentAllocationPlans) at PaymentAllocationPlans.pcf: line 9, column 74
    static function canVisit_31 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.payallocplanview
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at PaymentAllocationPlans.pcf: line 29, column 80
    function filters_1 () : gw.api.filters.IFilter[] {
      return new gw.api.web.plan.PlanListFilterSet().FilterOptions
    }
    
    // 'initialValue' attribute on Variable at PaymentAllocationPlans.pcf: line 13, column 42
    function initialValue_0 () : gw.api.web.plan.PlanHelper {
      return new gw.api.web.plan.PlanHelper()
    }
    
    // Page (id=PaymentAllocationPlans) at PaymentAllocationPlans.pcf: line 9, column 74
    static function parent_32 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at PaymentAllocationPlans.pcf: line 38, column 46
    function sortValue_2 (paymentAllocationPlan :  entity.PaymentAllocationPlan) : java.lang.Object {
      return paymentAllocationPlan.PlanOrder
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at PaymentAllocationPlans.pcf: line 44, column 51
    function sortValue_3 (paymentAllocationPlan :  entity.PaymentAllocationPlan) : java.lang.Object {
      return paymentAllocationPlan.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at PaymentAllocationPlans.pcf: line 49, column 58
    function sortValue_4 (paymentAllocationPlan :  entity.PaymentAllocationPlan) : java.lang.Object {
      return paymentAllocationPlan.Description
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at PaymentAllocationPlans.pcf: line 53, column 60
    function sortValue_5 (paymentAllocationPlan :  entity.PaymentAllocationPlan) : java.lang.Object {
      return paymentAllocationPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at PaymentAllocationPlans.pcf: line 57, column 61
    function sortValue_6 (paymentAllocationPlan :  entity.PaymentAllocationPlan) : java.lang.Object {
      return paymentAllocationPlan.ExpirationDate
    }
    
    // 'value' attribute on RowIterator at PaymentAllocationPlans.pcf: line 24, column 92
    function value_30 () : gw.api.database.IQueryBeanResult<entity.PaymentAllocationPlan> {
      return gw.api.database.Query.make(PaymentAllocationPlan).select()
    }
    
    override property get CurrentLocation () : pcf.PaymentAllocationPlans {
      return super.CurrentLocation as pcf.PaymentAllocationPlans
    }
    
    property get PlanHelper () : gw.api.web.plan.PlanHelper {
      return getVariableValue("PlanHelper", 0) as gw.api.web.plan.PlanHelper
    }
    
    property set PlanHelper ($arg :  gw.api.web.plan.PlanHelper) {
      setVariableValue("PlanHelper", 0, $arg)
    }
    
    
  }
  
  
}