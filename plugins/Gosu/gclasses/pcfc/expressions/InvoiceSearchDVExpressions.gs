package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/InvoiceSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/InvoiceSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountNumberCriterion_Input) at InvoiceSearchDV.pcf: line 60, column 39
    function action_33 () : void {
      AccountSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountNumberCriterion_Input) at InvoiceSearchDV.pcf: line 60, column 39
    function action_dest_34 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'available' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at InvoiceSearchDV.pcf: line 45, column 43
    function available_19 () : java.lang.Boolean {
      return (searchCriteria.CurrencyCriterion != null or gw.api.util.CurrencyUtil.isSingleCurrencyMode())
    }
    
    // 'conversionExpression' attribute on PickerInput (id=AccountNumberCriterion_Input) at InvoiceSearchDV.pcf: line 60, column 39
    function conversionExpression_35 (PickedValue :  Account) : java.lang.String {
      return PickedValue.DisplayName
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at InvoiceSearchDV.pcf: line 45, column 43
    function currency_23 () : typekey.Currency {
      return gw.search.SearchHelper.getDefaultCurrencyIfNull(searchCriteria.CurrencyCriterion)
    }
    
    // 'def' attribute on InputSetRef at InvoiceSearchDV.pcf: line 64, column 77
    function def_onEnter_40 (def :  pcf.ContactCriteriaInputSet) : void {
      def.onEnter(searchCriteria.ContactCriteria,true)
    }
    
    // 'def' attribute on InputSetRef at InvoiceSearchDV.pcf: line 68, column 41
    function def_onEnter_42 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at InvoiceSearchDV.pcf: line 64, column 77
    function def_refreshVariables_41 (def :  pcf.ContactCriteriaInputSet) : void {
      def.refreshVariables(searchCriteria.ContactCriteria,true)
    }
    
    // 'def' attribute on InputSetRef at InvoiceSearchDV.pcf: line 68, column 41
    function def_refreshVariables_43 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TextInput (id=InvoiceNumberCriterion_Input) at InvoiceSearchDV.pcf: line 18, column 47
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.InvoiceNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at InvoiceSearchDV.pcf: line 35, column 66
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.CurrencyCriterion = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at InvoiceSearchDV.pcf: line 45, column 43
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MinAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at InvoiceSearchDV.pcf: line 52, column 43
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MaxAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at InvoiceSearchDV.pcf: line 60, column 39
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AccountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at InvoiceSearchDV.pcf: line 23, column 46
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.EarliestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at InvoiceSearchDV.pcf: line 28, column 44
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.LatestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'onChange' attribute on PostOnChange at InvoiceSearchDV.pcf: line 37, column 54
    function onChange_12 () : void {
      blankMinimumAndMaximumFields()
    }
    
    // 'value' attribute on TextInput (id=InvoiceNumberCriterion_Input) at InvoiceSearchDV.pcf: line 18, column 47
    function valueRoot_2 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TextInput (id=InvoiceNumberCriterion_Input) at InvoiceSearchDV.pcf: line 18, column 47
    function value_0 () : java.lang.String {
      return searchCriteria.InvoiceNumber
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at InvoiceSearchDV.pcf: line 35, column 66
    function value_14 () : typekey.Currency {
      return searchCriteria.CurrencyCriterion
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at InvoiceSearchDV.pcf: line 45, column 43
    function value_20 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MinAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at InvoiceSearchDV.pcf: line 52, column 43
    function value_27 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MaxAmount
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at InvoiceSearchDV.pcf: line 60, column 39
    function value_36 () : java.lang.String {
      return searchCriteria.AccountNumber
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at InvoiceSearchDV.pcf: line 23, column 46
    function value_4 () : java.util.Date {
      return searchCriteria.EarliestDate
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at InvoiceSearchDV.pcf: line 28, column 44
    function value_8 () : java.util.Date {
      return searchCriteria.LatestDate
    }
    
    // 'visible' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at InvoiceSearchDV.pcf: line 35, column 66
    function visible_13 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    property get searchCriteria () : gw.search.InvoiceSearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.search.InvoiceSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.InvoiceSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    function blankMinimumAndMaximumFields() {
      searchCriteria.MinAmount = null
      searchCriteria.MaxAmount = null
    }
    
    
  }
  
  
}