package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyPlanDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyPlanDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at DelinquencyPlanDetailScreen.pcf: line 34, column 42
    function def_onEnter_10 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(dlnqPlan, { "Name"}, { DisplayKey.get("Java.DelinquencyPlan.Field.Name") })
    }
    
    // 'def' attribute on PanelRef at DelinquencyPlanDetailScreen.pcf: line 40, column 53
    function def_onEnter_12 (def :  pcf.DelinquencyPlanReasonsDV) : void {
      def.onEnter(dlnqPlan)
    }
    
    // 'def' attribute on PanelRef at DelinquencyPlanDetailScreen.pcf: line 31, column 52
    function def_onEnter_7 (def :  pcf.DelinquencyPlanDetailDV) : void {
      def.onEnter(dlnqPlan)
    }
    
    // 'def' attribute on PanelRef at DelinquencyPlanDetailScreen.pcf: line 34, column 42
    function def_refreshVariables_11 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(dlnqPlan, { "Name"}, { DisplayKey.get("Java.DelinquencyPlan.Field.Name") })
    }
    
    // 'def' attribute on PanelRef at DelinquencyPlanDetailScreen.pcf: line 40, column 53
    function def_refreshVariables_13 (def :  pcf.DelinquencyPlanReasonsDV) : void {
      def.refreshVariables(dlnqPlan)
    }
    
    // 'def' attribute on PanelRef at DelinquencyPlanDetailScreen.pcf: line 31, column 52
    function def_refreshVariables_8 (def :  pcf.DelinquencyPlanDetailDV) : void {
      def.refreshVariables(dlnqPlan)
    }
    
    // 'editable' attribute on PanelRef at DelinquencyPlanDetailScreen.pcf: line 34, column 42
    function editable_9 () : java.lang.Boolean {
      return not dlnqPlan.InUse
    }
    
    // 'initialValue' attribute on Variable at DelinquencyPlanDetailScreen.pcf: line 14, column 23
    function initialValue_0 () : boolean {
      return not dlnqPlan.InUse
    }
    
    // EditButtons at DelinquencyPlanDetailScreen.pcf: line 16, column 21
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'label' attribute on AlertBar (id=DelinquencyPlanDetailScreen_DelinquencyPlanInUse) at DelinquencyPlanDetailScreen.pcf: line 25, column 33
    function label_6 () : java.lang.Object {
      return DisplayKey.get("Web.DelinquencyPlanDetailScreen.DelinquencyPlanInUse", dlnqPlan.Name)
    }
    
    // 'mode' attribute on ToolbarButtonSetRef at DelinquencyPlanDetailScreen.pcf: line 19, column 76
    function mode_2 () : java.lang.Object {
      return gw.plugin.Plugins.get(gw.module.IFeesThresholds).getPCFMode()
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at DelinquencyPlanDetailScreen.pcf: line 19, column 76
    function toolbarButtonSet_onEnter_3 (def :  pcf.DelinquencyPlanCloneToolbarButtonSet_default) : void {
      def.onEnter(dlnqPlan)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at DelinquencyPlanDetailScreen.pcf: line 19, column 76
    function toolbarButtonSet_refreshVariables_4 (def :  pcf.DelinquencyPlanCloneToolbarButtonSet_default) : void {
      def.refreshVariables(dlnqPlan)
    }
    
    // 'visible' attribute on AlertBar (id=DelinquencyPlanDetailScreen_DelinquencyPlanInUse) at DelinquencyPlanDetailScreen.pcf: line 25, column 33
    function visible_5 () : java.lang.Boolean {
      return ! planNotInUse
    }
    
    property get dlnqPlan () : DelinquencyPlan {
      return getRequireValue("dlnqPlan", 0) as DelinquencyPlan
    }
    
    property set dlnqPlan ($arg :  DelinquencyPlan) {
      setRequireValue("dlnqPlan", 0, $arg)
    }
    
    property get planNotInUse () : boolean {
      return getVariableValue("planNotInUse", 0) as java.lang.Boolean
    }
    
    property set planNotInUse ($arg :  boolean) {
      setVariableValue("planNotInUse", 0, $arg)
    }
    
    
  }
  
  
}