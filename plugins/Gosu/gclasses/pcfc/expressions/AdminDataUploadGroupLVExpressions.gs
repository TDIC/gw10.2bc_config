package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadGroupLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadGroupLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadGroupLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadGroupLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadGroupLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TypeKeyCell (id=groupType_Cell) at AdminDataUploadGroupLV.pcf: line 50, column 42
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.GroupType")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadGroupLV.pcf: line 55, column 42
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'label' attribute on TextCell (id=loadFactor_Cell) at AdminDataUploadGroupLV.pcf: line 60, column 42
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.LoadFactor")
    }
    
    // 'label' attribute on TextCell (id=securityZone_Cell) at AdminDataUploadGroupLV.pcf: line 65, column 41
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.SecurityZone")
    }
    
    // 'label' attribute on TextCell (id=region1_Cell) at AdminDataUploadGroupLV.pcf: line 70, column 41
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.Region1")
    }
    
    // 'label' attribute on TextCell (id=region2_Cell) at AdminDataUploadGroupLV.pcf: line 75, column 41
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.Region2")
    }
    
    // 'label' attribute on TextCell (id=region3_Cell) at AdminDataUploadGroupLV.pcf: line 80, column 41
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.Region3")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at AdminDataUploadGroupLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.Name")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadGroupLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.PublicID")
    }
    
    // 'label' attribute on TextCell (id=parent_Cell) at AdminDataUploadGroupLV.pcf: line 40, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.Parent")
    }
    
    // 'label' attribute on TextCell (id=supervisor_Cell) at AdminDataUploadGroupLV.pcf: line 45, column 41
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.Supervisor")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadGroupLV.pcf: line 23, column 46
    function sortValue_1 (group :  tdic.util.dataloader.data.admindata.GroupData) : java.lang.Object {
      return processor.getLoadStatus(group)
    }
    
    // 'value' attribute on TextCell (id=supervisor_Cell) at AdminDataUploadGroupLV.pcf: line 45, column 41
    function sortValue_10 (group :  tdic.util.dataloader.data.admindata.GroupData) : java.lang.Object {
      return group.Supervisor.ReferenceName
    }
    
    // 'value' attribute on TypeKeyCell (id=groupType_Cell) at AdminDataUploadGroupLV.pcf: line 50, column 42
    function sortValue_12 (group :  tdic.util.dataloader.data.admindata.GroupData) : java.lang.Object {
      return group.GroupType
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadGroupLV.pcf: line 55, column 42
    function sortValue_14 (group :  tdic.util.dataloader.data.admindata.GroupData) : java.lang.Object {
      return group.Exclude
    }
    
    // 'value' attribute on TextCell (id=loadFactor_Cell) at AdminDataUploadGroupLV.pcf: line 60, column 42
    function sortValue_16 (group :  tdic.util.dataloader.data.admindata.GroupData) : java.lang.Object {
      return group.LoadFactor
    }
    
    // 'value' attribute on TextCell (id=securityZone_Cell) at AdminDataUploadGroupLV.pcf: line 65, column 41
    function sortValue_18 (group :  tdic.util.dataloader.data.admindata.GroupData) : java.lang.Object {
      return group.SecurityZone.Name
    }
    
    // 'value' attribute on TextCell (id=region1_Cell) at AdminDataUploadGroupLV.pcf: line 70, column 41
    function sortValue_20 (group :  tdic.util.dataloader.data.admindata.GroupData) : java.lang.Object {
      return group.Region1.Name
    }
    
    // 'value' attribute on TextCell (id=region2_Cell) at AdminDataUploadGroupLV.pcf: line 75, column 41
    function sortValue_22 (group :  tdic.util.dataloader.data.admindata.GroupData) : java.lang.Object {
      return group.Region2.Name
    }
    
    // 'value' attribute on TextCell (id=region3_Cell) at AdminDataUploadGroupLV.pcf: line 80, column 41
    function sortValue_24 (group :  tdic.util.dataloader.data.admindata.GroupData) : java.lang.Object {
      return group.Region3.Name
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadGroupLV.pcf: line 29, column 41
    function sortValue_4 (group :  tdic.util.dataloader.data.admindata.GroupData) : java.lang.Object {
      return group.Name
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadGroupLV.pcf: line 35, column 41
    function sortValue_6 (group :  tdic.util.dataloader.data.admindata.GroupData) : java.lang.Object {
      return group.PublicID
    }
    
    // 'value' attribute on TextCell (id=parent_Cell) at AdminDataUploadGroupLV.pcf: line 40, column 41
    function sortValue_8 (group :  tdic.util.dataloader.data.admindata.GroupData) : java.lang.Object {
      return group.Parent
    }
    
    // 'value' attribute on RowIterator (id=Group) at AdminDataUploadGroupLV.pcf: line 15, column 92
    function value_86 () : java.util.ArrayList<tdic.util.dataloader.data.admindata.GroupData> {
      return processor.GroupArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadGroupLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadGroupLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminDataUploadGroupLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at AdminDataUploadGroupLV.pcf: line 17, column 90
    function highlighted_85 () : java.lang.Boolean {
      return (group.Error or group.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadGroupLV.pcf: line 23, column 46
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at AdminDataUploadGroupLV.pcf: line 29, column 41
    function label_30 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.Name")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadGroupLV.pcf: line 35, column 41
    function label_35 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.PublicID")
    }
    
    // 'label' attribute on TextCell (id=parent_Cell) at AdminDataUploadGroupLV.pcf: line 40, column 41
    function label_40 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.Parent")
    }
    
    // 'label' attribute on TextCell (id=supervisor_Cell) at AdminDataUploadGroupLV.pcf: line 45, column 41
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.Supervisor")
    }
    
    // 'label' attribute on TypeKeyCell (id=groupType_Cell) at AdminDataUploadGroupLV.pcf: line 50, column 42
    function label_50 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.GroupType")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadGroupLV.pcf: line 55, column 42
    function label_55 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'label' attribute on TextCell (id=loadFactor_Cell) at AdminDataUploadGroupLV.pcf: line 60, column 42
    function label_60 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.LoadFactor")
    }
    
    // 'label' attribute on TextCell (id=securityZone_Cell) at AdminDataUploadGroupLV.pcf: line 65, column 41
    function label_65 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.SecurityZone")
    }
    
    // 'label' attribute on TextCell (id=region1_Cell) at AdminDataUploadGroupLV.pcf: line 70, column 41
    function label_70 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.Region1")
    }
    
    // 'label' attribute on TextCell (id=region2_Cell) at AdminDataUploadGroupLV.pcf: line 75, column 41
    function label_75 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.Region2")
    }
    
    // 'label' attribute on TextCell (id=region3_Cell) at AdminDataUploadGroupLV.pcf: line 80, column 41
    function label_80 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Group.Region3")
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadGroupLV.pcf: line 29, column 41
    function valueRoot_32 () : java.lang.Object {
      return group
    }
    
    // 'value' attribute on TextCell (id=supervisor_Cell) at AdminDataUploadGroupLV.pcf: line 45, column 41
    function valueRoot_47 () : java.lang.Object {
      return group.Supervisor
    }
    
    // 'value' attribute on TextCell (id=securityZone_Cell) at AdminDataUploadGroupLV.pcf: line 65, column 41
    function valueRoot_67 () : java.lang.Object {
      return group.SecurityZone
    }
    
    // 'value' attribute on TextCell (id=region1_Cell) at AdminDataUploadGroupLV.pcf: line 70, column 41
    function valueRoot_72 () : java.lang.Object {
      return group.Region1
    }
    
    // 'value' attribute on TextCell (id=region2_Cell) at AdminDataUploadGroupLV.pcf: line 75, column 41
    function valueRoot_77 () : java.lang.Object {
      return group.Region2
    }
    
    // 'value' attribute on TextCell (id=region3_Cell) at AdminDataUploadGroupLV.pcf: line 80, column 41
    function valueRoot_82 () : java.lang.Object {
      return group.Region3
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadGroupLV.pcf: line 23, column 46
    function value_26 () : java.lang.String {
      return processor.getLoadStatus(group)
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadGroupLV.pcf: line 29, column 41
    function value_31 () : java.lang.String {
      return group.Name
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadGroupLV.pcf: line 35, column 41
    function value_36 () : java.lang.String {
      return group.PublicID
    }
    
    // 'value' attribute on TextCell (id=parent_Cell) at AdminDataUploadGroupLV.pcf: line 40, column 41
    function value_41 () : java.lang.String {
      return group.Parent
    }
    
    // 'value' attribute on TextCell (id=supervisor_Cell) at AdminDataUploadGroupLV.pcf: line 45, column 41
    function value_46 () : java.lang.String {
      return group.Supervisor.ReferenceName
    }
    
    // 'value' attribute on TypeKeyCell (id=groupType_Cell) at AdminDataUploadGroupLV.pcf: line 50, column 42
    function value_51 () : typekey.GroupType {
      return group.GroupType
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadGroupLV.pcf: line 55, column 42
    function value_56 () : java.lang.Boolean {
      return group.Exclude
    }
    
    // 'value' attribute on TextCell (id=loadFactor_Cell) at AdminDataUploadGroupLV.pcf: line 60, column 42
    function value_61 () : java.lang.Integer {
      return group.LoadFactor
    }
    
    // 'value' attribute on TextCell (id=securityZone_Cell) at AdminDataUploadGroupLV.pcf: line 65, column 41
    function value_66 () : java.lang.String {
      return group.SecurityZone.Name
    }
    
    // 'value' attribute on TextCell (id=region1_Cell) at AdminDataUploadGroupLV.pcf: line 70, column 41
    function value_71 () : java.lang.String {
      return group.Region1.Name
    }
    
    // 'value' attribute on TextCell (id=region2_Cell) at AdminDataUploadGroupLV.pcf: line 75, column 41
    function value_76 () : java.lang.String {
      return group.Region2.Name
    }
    
    // 'value' attribute on TextCell (id=region3_Cell) at AdminDataUploadGroupLV.pcf: line 80, column 41
    function value_81 () : java.lang.String {
      return group.Region3.Name
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadGroupLV.pcf: line 23, column 46
    function visible_27 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get group () : tdic.util.dataloader.data.admindata.GroupData {
      return getIteratedValue(1) as tdic.util.dataloader.data.admindata.GroupData
    }
    
    
  }
  
  
}