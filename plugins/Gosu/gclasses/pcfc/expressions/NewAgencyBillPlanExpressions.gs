package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.CurrencyUtil
@javax.annotation.Generated("config/web/pcf/admin/agencybill/NewAgencyBillPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewAgencyBillPlanExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/agencybill/NewAgencyBillPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewAgencyBillPlanExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (currency :  Currency) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=NewAgencyBillPlan) at NewAgencyBillPlan.pcf: line 13, column 69
    function afterCancel_6 () : void {
      AgencyBillPlans.go()
    }
    
    // 'afterCancel' attribute on Page (id=NewAgencyBillPlan) at NewAgencyBillPlan.pcf: line 13, column 69
    function afterCancel_dest_7 () : pcf.api.Destination {
      return pcf.AgencyBillPlans.createDestination()
    }
    
    // 'afterCommit' attribute on Page (id=NewAgencyBillPlan) at NewAgencyBillPlan.pcf: line 13, column 69
    function afterCommit_8 (TopLocation :  pcf.api.Location) : void {
      AgencyBillPlans.go()
    }
    
    // 'canVisit' attribute on Page (id=NewAgencyBillPlan) at NewAgencyBillPlan.pcf: line 13, column 69
    static function canVisit_9 (currency :  Currency) : java.lang.Boolean {
      return perm.System.admintabview and perm.System.agencybillplancreate
    }
    
    // 'def' attribute on PanelRef at NewAgencyBillPlan.pcf: line 29, column 55
    function def_onEnter_2 (def :  pcf.AgencyBillPlanDetailDV) : void {
      def.onEnter(agencyBillPlan)
    }
    
    // 'def' attribute on PanelRef at NewAgencyBillPlan.pcf: line 31, column 124
    function def_onEnter_4 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(agencyBillPlan, { "Name"}, { DisplayKey.get("Web.Admin.Plan.Name") })
    }
    
    // 'def' attribute on PanelRef at NewAgencyBillPlan.pcf: line 29, column 55
    function def_refreshVariables_3 (def :  pcf.AgencyBillPlanDetailDV) : void {
      def.refreshVariables(agencyBillPlan)
    }
    
    // 'def' attribute on PanelRef at NewAgencyBillPlan.pcf: line 31, column 124
    function def_refreshVariables_5 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(agencyBillPlan, { "Name"}, { DisplayKey.get("Web.Admin.Plan.Name") })
    }
    
    // 'initialValue' attribute on Variable at NewAgencyBillPlan.pcf: line 19, column 30
    function initialValue_0 () : AgencyBillPlan {
      return initAgencyBillPlan()
    }
    
    // EditButtons at NewAgencyBillPlan.pcf: line 26, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'parent' attribute on Page (id=NewAgencyBillPlan) at NewAgencyBillPlan.pcf: line 13, column 69
    static function parent_10 (currency :  Currency) : pcf.api.Destination {
      return pcf.AgencyBillPlans.createDestination()
    }
    
    override property get CurrentLocation () : pcf.NewAgencyBillPlan {
      return super.CurrentLocation as pcf.NewAgencyBillPlan
    }
    
    property get agencyBillPlan () : AgencyBillPlan {
      return getVariableValue("agencyBillPlan", 0) as AgencyBillPlan
    }
    
    property set agencyBillPlan ($arg :  AgencyBillPlan) {
      setVariableValue("agencyBillPlan", 0, $arg)
    }
    
    property get currency () : Currency {
      return getVariableValue("currency", 0) as Currency
    }
    
    property set currency ($arg :  Currency) {
      setVariableValue("currency", 0, $arg)
    }
    
    
    function initAgencyBillPlan() : AgencyBillPlan {
      var newAgencyBillPlan = new AgencyBillPlan(CurrentLocation)
      if (not isMultiCurrencyMode()) {
        newAgencyBillPlan.addToCurrencies(currency)
      }
      newAgencyBillPlan.EffectiveDate = gw.api.util.DateUtil.currentDate();
      return newAgencyBillPlan;
    }
    
    function isMultiCurrencyMode(): boolean {
      return CurrencyUtil.isMultiCurrencyMode() and Currency.getTypeKeys(true).Count > 1
    }
    
    
  }
  
  
}