package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/accounting/CommissionRatesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionRatesPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/accounting/CommissionRatesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionRatesPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (charges :  Charge[]) : int {
      return 0
    }
    
    // 'canVisit' attribute on Popup (id=CommissionRatesPopup) at CommissionRatesPopup.pcf: line 10, column 72
    static function canVisit_2 (charges :  Charge[]) : java.lang.Boolean {
      return perm.Account.chrgcmsnrateoverride
    }
    
    // 'def' attribute on PanelRef at CommissionRatesPopup.pcf: line 33, column 65
    function def_onEnter_0 (def :  pcf.ChargesLV) : void {
      def.onEnter(charges, false, 2, true, false, false)
    }
    
    // 'def' attribute on PanelRef at CommissionRatesPopup.pcf: line 33, column 65
    function def_refreshVariables_1 (def :  pcf.ChargesLV) : void {
      def.refreshVariables(charges, false, 2, true, false, false)
    }
    
    override property get CurrentLocation () : pcf.CommissionRatesPopup {
      return super.CurrentLocation as pcf.CommissionRatesPopup
    }
    
    property get charges () : Charge[] {
      return getVariableValue("charges", 0) as Charge[]
    }
    
    property set charges ($arg :  Charge[]) {
      setVariableValue("charges", 0, $arg)
    }
    
    property get isAgencyBillCharge () : Boolean {
      return getVariableValue("isAgencyBillCharge", 0) as Boolean
    }
    
    property set isAgencyBillCharge ($arg :  Boolean) {
      setVariableValue("isAgencyBillCharge", 0, $arg)
    }
    
    
  }
  
  
}