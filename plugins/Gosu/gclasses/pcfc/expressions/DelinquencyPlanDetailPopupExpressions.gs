package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyPlanDetailPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyPlanDetailPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (delinquencyPlan :  DelinquencyPlan) : int {
      return 0
    }
    
    // 'canEdit' attribute on Popup (id=DelinquencyPlanDetailPopup) at DelinquencyPlanDetailPopup.pcf: line 8, column 86
    function canEdit_2 () : java.lang.Boolean {
      return perm.System.delplanedit
    }
    
    // 'def' attribute on ScreenRef at DelinquencyPlanDetailPopup.pcf: line 15, column 59
    function def_onEnter_0 (def :  pcf.DelinquencyPlanDetailScreen) : void {
      def.onEnter(delinquencyPlan)
    }
    
    // 'def' attribute on ScreenRef at DelinquencyPlanDetailPopup.pcf: line 15, column 59
    function def_refreshVariables_1 (def :  pcf.DelinquencyPlanDetailScreen) : void {
      def.refreshVariables(delinquencyPlan)
    }
    
    // 'title' attribute on Popup (id=DelinquencyPlanDetailPopup) at DelinquencyPlanDetailPopup.pcf: line 8, column 86
    static function title_3 (delinquencyPlan :  DelinquencyPlan) : java.lang.Object {
      return DisplayKey.get("Web.DelinquencyPlan.Title",  delinquencyPlan )
    }
    
    override property get CurrentLocation () : pcf.DelinquencyPlanDetailPopup {
      return super.CurrentLocation as pcf.DelinquencyPlanDetailPopup
    }
    
    property get delinquencyPlan () : DelinquencyPlan {
      return getVariableValue("delinquencyPlan", 0) as DelinquencyPlan
    }
    
    property set delinquencyPlan ($arg :  DelinquencyPlan) {
      setVariableValue("delinquencyPlan", 0, $arg)
    }
    
    
  }
  
  
}