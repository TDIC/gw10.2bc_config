package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/DisbursementConfirmPanelSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DisbursementConfirmPanelSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/disbursement/DisbursementConfirmPanelSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DisbursementConfirmPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on Label at DisbursementConfirmPanelSet.default.pcf: line 14, column 68
    function label_0 () : java.lang.String {
      return disbursement.CreateDisbursementConfirmationText
    }
    
    property get disbursement () : Disbursement {
      return getRequireValue("disbursement", 0) as Disbursement
    }
    
    property set disbursement ($arg :  Disbursement) {
      setRequireValue("disbursement", 0, $arg)
    }
    
    
  }
  
  
}