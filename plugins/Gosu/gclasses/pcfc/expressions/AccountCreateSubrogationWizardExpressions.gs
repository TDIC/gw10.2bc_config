package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountCreateSubrogationWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountCreateSubrogationWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountCreateSubrogationWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountCreateSubrogationWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Wizard (id=AccountCreateSubrogationWizard) at AccountCreateSubrogationWizard.pcf: line 11, column 41
    function beforeCommit_8 (pickedValue :  java.lang.Object) : void {
      subrogation.doSubrogation()
    }
    
    // 'canVisit' attribute on Wizard (id=AccountCreateSubrogationWizard) at AccountCreateSubrogationWizard.pcf: line 11, column 41
    static function canVisit_9 (account :  Account) : java.lang.Boolean {
      return perm.Transaction.subrtxn
    }
    
    // 'initialValue' attribute on Variable at AccountCreateSubrogationWizard.pcf: line 20, column 27
    function initialValue_0 () : Subrogation {
      return new Subrogation(account.Currency)
    }
    
    // 'onFirstEnter' attribute on WizardStep (id=Step1) at AccountCreateSubrogationWizard.pcf: line 26, column 88
    function onFirstEnter_1 () : void {
      subrogation.SourceAccount = account
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountCreateSubrogationWizard.pcf: line 26, column 88
    function screen_onEnter_2 (def :  pcf.AccountCreateSubrogationTargetsScreen) : void {
      def.onEnter(subrogation)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountCreateSubrogationWizard.pcf: line 31, column 86
    function screen_onEnter_4 (def :  pcf.CreateSubrogationInfoScreen) : void {
      def.onEnter(subrogation)
    }
    
    // 'screen' attribute on WizardStep (id=Step3) at AccountCreateSubrogationWizard.pcf: line 36, column 89
    function screen_onEnter_6 (def :  pcf.CreateSubrogationConfirmScreen) : void {
      def.onEnter(subrogation)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountCreateSubrogationWizard.pcf: line 26, column 88
    function screen_refreshVariables_3 (def :  pcf.AccountCreateSubrogationTargetsScreen) : void {
      def.refreshVariables(subrogation)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountCreateSubrogationWizard.pcf: line 31, column 86
    function screen_refreshVariables_5 (def :  pcf.CreateSubrogationInfoScreen) : void {
      def.refreshVariables(subrogation)
    }
    
    // 'screen' attribute on WizardStep (id=Step3) at AccountCreateSubrogationWizard.pcf: line 36, column 89
    function screen_refreshVariables_7 (def :  pcf.CreateSubrogationConfirmScreen) : void {
      def.refreshVariables(subrogation)
    }
    
    // 'tabBar' attribute on Wizard (id=AccountCreateSubrogationWizard) at AccountCreateSubrogationWizard.pcf: line 11, column 41
    function tabBar_onEnter_10 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AccountCreateSubrogationWizard) at AccountCreateSubrogationWizard.pcf: line 11, column 41
    function tabBar_refreshVariables_11 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AccountCreateSubrogationWizard {
      return super.CurrentLocation as pcf.AccountCreateSubrogationWizard
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get subrogation () : Subrogation {
      return getVariableValue("subrogation", 0) as Subrogation
    }
    
    property set subrogation ($arg :  Subrogation) {
      setVariableValue("subrogation", 0, $arg)
    }
    
    
  }
  
  
}