package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailTroubleTickets.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailTroubleTicketsExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailTroubleTickets.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailTroubleTicketsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=NewTroubleTicketButton) at AccountDetailTroubleTickets.pcf: line 25, column 50
    function action_1 () : void {
      CreateTroubleTicketWizard.push(account)
    }
    
    // 'action' attribute on ToolbarButton (id=NewTroubleTicketButton) at AccountDetailTroubleTickets.pcf: line 25, column 50
    function action_dest_2 () : pcf.api.Destination {
      return pcf.CreateTroubleTicketWizard.createDestination(account)
    }
    
    // 'canVisit' attribute on Page (id=AccountDetailTroubleTickets) at AccountDetailTroubleTickets.pcf: line 9, column 79
    static function canVisit_5 (account :  Account) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctttktview
    }
    
    // 'def' attribute on PanelRef at AccountDetailTroubleTickets.pcf: line 18, column 56
    function def_onEnter_3 (def :  pcf.TroubleTicketsLV) : void {
      def.onEnter(account.TroubleTickets)
    }
    
    // 'def' attribute on PanelRef at AccountDetailTroubleTickets.pcf: line 18, column 56
    function def_refreshVariables_4 (def :  pcf.TroubleTicketsLV) : void {
      def.refreshVariables(account.TroubleTickets)
    }
    
    // Page (id=AccountDetailTroubleTickets) at AccountDetailTroubleTickets.pcf: line 9, column 79
    static function parent_6 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    // 'visible' attribute on ToolbarButton (id=NewTroubleTicketButton) at AccountDetailTroubleTickets.pcf: line 25, column 50
    function visible_0 () : java.lang.Boolean {
      return perm.TroubleTicket.create
    }
    
    override property get CurrentLocation () : pcf.AccountDetailTroubleTickets {
      return super.CurrentLocation as pcf.AccountDetailTroubleTickets
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  
}