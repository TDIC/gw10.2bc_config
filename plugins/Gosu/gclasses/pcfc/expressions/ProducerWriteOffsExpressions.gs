package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/ProducerWriteOffs.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerWriteOffsExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/ProducerWriteOffs.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ProducerWriteOffsScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ButtonCell (id=Reverse_Cell) at ProducerWriteOffs.pcf: line 70, column 58
    function action_22 () : void {
      gw.producer.ProducerWriteOffsHelper.reverse(writeOff, CurrentLocation as ProducerWriteOffs)
    }
    
    // 'available' attribute on ButtonCell (id=Reverse_Cell) at ProducerWriteOffs.pcf: line 70, column 58
    function available_21 () : java.lang.Boolean {
      return writeOff.canReverse()
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at ProducerWriteOffs.pcf: line 88, column 49
    function currency_33 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'value' attribute on DateCell (id=CreationDate_Cell) at ProducerWriteOffs.pcf: line 76, column 46
    function valueRoot_26 () : java.lang.Object {
      return writeOff
    }
    
    // 'value' attribute on TextCell (id=WriteOffTarget_Cell) at ProducerWriteOffs.pcf: line 98, column 62
    function valueRoot_39 () : java.lang.Object {
      return writeOff.WriteOffTarget
    }
    
    // 'value' attribute on DateCell (id=CreationDate_Cell) at ProducerWriteOffs.pcf: line 76, column 46
    function value_25 () : java.util.Date {
      return writeOff.CreateTime
    }
    
    // 'value' attribute on DateCell (id=ExecutionDate_Cell) at ProducerWriteOffs.pcf: line 82, column 49
    function value_28 () : java.util.Date {
      return writeOff.ExecutionDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at ProducerWriteOffs.pcf: line 88, column 49
    function value_31 () : gw.pl.currency.MonetaryAmount {
      return writeOff.DisplayAmount
    }
    
    // 'value' attribute on TextCell (id=WriteOffType_Cell) at ProducerWriteOffs.pcf: line 93, column 48
    function value_35 () : java.lang.String {
      return writeOff.WriteOffType
    }
    
    // 'value' attribute on TextCell (id=WriteOffTarget_Cell) at ProducerWriteOffs.pcf: line 98, column 62
    function value_38 () : java.lang.String {
      return writeOff.WriteOffTarget.DisplayName
    }
    
    // 'value' attribute on TextCell (id=ReversalStatus_Cell) at ProducerWriteOffs.pcf: line 102, column 82
    function value_41 () : java.lang.String {
      return gw.producer.ProducerWriteOffsHelper.statusFor(writeOff)
    }
    
    // 'visible' attribute on ButtonCell (id=Reverse_Cell) at ProducerWriteOffs.pcf: line 70, column 58
    function visible_24 () : java.lang.Boolean {
      return perm.System.prodwriteoffsedit
    }
    
    property get writeOff () : gw.api.domain.accounting.AbstractWriteOff {
      return getIteratedValue(2) as gw.api.domain.accounting.AbstractWriteOff
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/ProducerWriteOffs.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerWriteOffsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=ProducerWriteOffs) at ProducerWriteOffs.pcf: line 9, column 69
    function canEdit_45 () : java.lang.Boolean {
      return perm.System.prodwriteoffsedit
    }
    
    // 'canVisit' attribute on Page (id=ProducerWriteOffs) at ProducerWriteOffs.pcf: line 9, column 69
    static function canVisit_46 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodwriteoffsview
    }
    
    // Page (id=ProducerWriteOffs) at ProducerWriteOffs.pcf: line 9, column 69
    static function parent_47 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    override property get CurrentLocation () : pcf.ProducerWriteOffs {
      return super.CurrentLocation as pcf.ProducerWriteOffs
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/ProducerWriteOffs.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerWriteOffsScreenExpressionsImpl extends ProducerWriteOffsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=WriteOffAgeRestriction_Input) at ProducerWriteOffs.pcf: line 31, column 68
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      writeOffAgeRestriction = (__VALUE_TO_SET as gw.producer.ProducerWriteOffAgeRestriction)
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerWriteOffs.pcf: line 55, column 39
    function filter_10 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardBeanFilter(DisplayKey.get("Web.ProducerWriteOffs.ProducerWriteOffsLV.Status.Executed"), \ x -> gw.producer.ProducerWriteOffsHelper.isExecuted(x as gw.api.domain.accounting.AbstractWriteOff))
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerWriteOffs.pcf: line 57, column 243
    function filter_11 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardBeanFilter(DisplayKey.get("Web.ProducerWriteOffs.ProducerWriteOffsLV.Status.ReversalPending"), \ x -> (x as gw.api.domain.accounting.AbstractWriteOff).PendingReversal == true)
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerWriteOffs.pcf: line 59, column 229
    function filter_12 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardBeanFilter(DisplayKey.get("Web.ProducerWriteOffs.ProducerWriteOffsLV.Status.Reversed"), \ x -> (x as gw.api.domain.accounting.AbstractWriteOff).Reversed == true)
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerWriteOffs.pcf: line 61, column 229
    function filter_13 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardBeanFilter(DisplayKey.get("Web.ProducerWriteOffs.ProducerWriteOffsLV.Status.Rejected"), \ x -> (x as gw.api.domain.accounting.AbstractWriteOff).Rejected == true)
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerWriteOffs.pcf: line 50, column 67
    function filter_8 () : gw.api.filters.IFilter {
      return gw.api.filters.StandardQueryFilters.ALL
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerWriteOffs.pcf: line 52, column 227
    function filter_9 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardBeanFilter(DisplayKey.get("Web.ProducerWriteOffs.ProducerWriteOffsLV.Status.Pending"), \ x -> (x as gw.api.domain.accounting.AbstractWriteOff).Pending == true)
    }
    
    // 'initialValue' attribute on Variable at ProducerWriteOffs.pcf: line 20, column 60
    function initialValue_0 () : gw.producer.ProducerWriteOffAgeRestriction {
      return gw.producer.ProducerWriteOffAgeRestriction.ALL_OPTIONS.first()
    }
    
    // 'optionLabel' attribute on ToolbarRangeInput (id=WriteOffAgeRestriction_Input) at ProducerWriteOffs.pcf: line 31, column 68
    function optionLabel_3 (VALUE :  gw.producer.ProducerWriteOffAgeRestriction) : java.lang.String {
      return VALUE.getDisplayText()
    }
    
    // 'value' attribute on DateCell (id=CreationDate_Cell) at ProducerWriteOffs.pcf: line 76, column 46
    function sortValue_15 (writeOff :  gw.api.domain.accounting.AbstractWriteOff) : java.lang.Object {
      return writeOff.CreateTime
    }
    
    // 'value' attribute on DateCell (id=ExecutionDate_Cell) at ProducerWriteOffs.pcf: line 82, column 49
    function sortValue_16 (writeOff :  gw.api.domain.accounting.AbstractWriteOff) : java.lang.Object {
      return writeOff.ExecutionDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at ProducerWriteOffs.pcf: line 88, column 49
    function sortValue_17 (writeOff :  gw.api.domain.accounting.AbstractWriteOff) : java.lang.Object {
      return writeOff.DisplayAmount
    }
    
    // 'value' attribute on TextCell (id=WriteOffType_Cell) at ProducerWriteOffs.pcf: line 93, column 48
    function sortValue_18 (writeOff :  gw.api.domain.accounting.AbstractWriteOff) : java.lang.Object {
      return writeOff.WriteOffType
    }
    
    // 'value' attribute on TextCell (id=WriteOffTarget_Cell) at ProducerWriteOffs.pcf: line 98, column 62
    function sortValue_19 (writeOff :  gw.api.domain.accounting.AbstractWriteOff) : java.lang.Object {
      return writeOff.WriteOffTarget.DisplayName
    }
    
    // 'value' attribute on TextCell (id=ReversalStatus_Cell) at ProducerWriteOffs.pcf: line 102, column 82
    function sortValue_20 (writeOff :  gw.api.domain.accounting.AbstractWriteOff) : java.lang.Object {
      return gw.producer.ProducerWriteOffsHelper.statusFor(writeOff)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=WriteOffAgeRestriction_Input) at ProducerWriteOffs.pcf: line 31, column 68
    function valueRange_4 () : java.lang.Object {
      return gw.producer.ProducerWriteOffAgeRestriction.ALL_OPTIONS
    }
    
    // 'value' attribute on ToolbarRangeInput (id=WriteOffAgeRestriction_Input) at ProducerWriteOffs.pcf: line 31, column 68
    function value_1 () : gw.producer.ProducerWriteOffAgeRestriction {
      return writeOffAgeRestriction
    }
    
    // 'value' attribute on RowIterator (id=ProducerWriteOffsIterator) at ProducerWriteOffs.pcf: line 45, column 125
    function value_43 () : java.util.List<gw.api.domain.accounting.AbstractWriteOff<entity.ActivityCreatedByAppr>> {
      return gw.producer.ProducerWriteOffsHelper.findProducerWriteOffsConstrainedByAge(producer, writeOffAgeRestriction)
    }
    
    // 'type' attribute on RowIterator (id=ProducerWriteOffsIterator) at ProducerWriteOffs.pcf: line 45, column 125
    function verifyIteratorType_44 () : void {
      var entry : gw.api.domain.accounting.AbstractWriteOff<entity.ActivityCreatedByAppr> = null
      var typedEntry : gw.api.domain.accounting.AbstractWriteOff
      // If this cast fails to compile then the type specified by the 'type' attribute on
      // 'RowIterator' is not compatible with the member type of the 'valueType' attribute
      typedEntry = entry as gw.api.domain.accounting.AbstractWriteOff
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=WriteOffAgeRestriction_Input) at ProducerWriteOffs.pcf: line 31, column 68
    function verifyValueRangeIsAllowedType_5 ($$arg :  gw.producer.ProducerWriteOffAgeRestriction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=WriteOffAgeRestriction_Input) at ProducerWriteOffs.pcf: line 31, column 68
    function verifyValueRangeIsAllowedType_5 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=WriteOffAgeRestriction_Input) at ProducerWriteOffs.pcf: line 31, column 68
    function verifyValueRange_6 () : void {
      var __valueRangeArg = gw.producer.ProducerWriteOffAgeRestriction.ALL_OPTIONS
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_5(__valueRangeArg)
    }
    
    // 'visible' attribute on ButtonCell (id=Reverse_Cell) at ProducerWriteOffs.pcf: line 70, column 58
    function visible_14 () : java.lang.Boolean {
      return perm.System.prodwriteoffsedit
    }
    
    property get writeOffAgeRestriction () : gw.producer.ProducerWriteOffAgeRestriction {
      return getVariableValue("writeOffAgeRestriction", 1) as gw.producer.ProducerWriteOffAgeRestriction
    }
    
    property set writeOffAgeRestriction ($arg :  gw.producer.ProducerWriteOffAgeRestriction) {
      setVariableValue("writeOffAgeRestriction", 1, $arg)
    }
    
    
  }
  
  
}