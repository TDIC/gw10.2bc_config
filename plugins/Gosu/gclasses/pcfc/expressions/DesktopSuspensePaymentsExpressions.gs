package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopSuspensePayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopSuspensePaymentsExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopSuspensePayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopSuspensePaymentsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=DesktopSuspensePayments) at DesktopSuspensePayments.pcf: line 9, column 69
    static function canVisit_84 () : java.lang.Boolean {
      return perm.System.viewdesktop and perm.SuspensePayment.view and perm.System.susppmntview
    }
    
    // 'initialValue' attribute on Variable at DesktopSuspensePayments.pcf: line 15, column 54
    function initialValue_0 () : gw.api.web.payment.SuspensePaymentUtil {
      return new gw.api.web.payment.SuspensePaymentUtil()
    }
    
    // 'initialValue' attribute on Variable at DesktopSuspensePayments.pcf: line 19, column 71
    function initialValue_1 () : gw.api.database.IQueryBeanResult<SuspensePayment> {
      return suspensePaymentUtil.getSuspensePayments()
    }
    
    // 'initialValue' attribute on Variable at DesktopSuspensePayments.pcf: line 23, column 59
    function initialValue_2 () : gw.api.web.payment.SuspensePaymentFilterSet {
      return new gw.api.web.payment.SuspensePaymentFilterSet()
    }
    
    // Page (id=DesktopSuspensePayments) at DesktopSuspensePayments.pcf: line 9, column 69
    static function parent_85 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.DesktopSuspensePayments {
      return super.CurrentLocation as pcf.DesktopSuspensePayments
    }
    
    property get filterSet () : gw.api.web.payment.SuspensePaymentFilterSet {
      return getVariableValue("filterSet", 0) as gw.api.web.payment.SuspensePaymentFilterSet
    }
    
    property set filterSet ($arg :  gw.api.web.payment.SuspensePaymentFilterSet) {
      setVariableValue("filterSet", 0, $arg)
    }
    
    property get suspensePaymentUtil () : gw.api.web.payment.SuspensePaymentUtil {
      return getVariableValue("suspensePaymentUtil", 0) as gw.api.web.payment.SuspensePaymentUtil
    }
    
    property set suspensePaymentUtil ($arg :  gw.api.web.payment.SuspensePaymentUtil) {
      setVariableValue("suspensePaymentUtil", 0, $arg)
    }
    
    property get suspensePayments () : gw.api.database.IQueryBeanResult<SuspensePayment> {
      return getVariableValue("suspensePayments", 0) as gw.api.database.IQueryBeanResult<SuspensePayment>
    }
    
    property set suspensePayments ($arg :  gw.api.database.IQueryBeanResult<SuspensePayment>) {
      setVariableValue("suspensePayments", 0, $arg)
    }
    
    function appliedAction(suspensePayment : SuspensePayment) {
      if (suspensePayment.PolicyPeriodAppliedTo != null) {
        PolicySummary.push(suspensePayment.PolicyPeriodAppliedTo)
      } else if (suspensePayment.AccountAppliedTo != null) {
        AccountSummaryPopup.push(suspensePayment.AccountAppliedTo);
      } else if (suspensePayment.DisbursementAppliedTo != null) {
        DisbursementDetail.push(suspensePayment.DisbursementAppliedTo, false);
      } else if (suspensePayment.ProducerAppliedTo != null) {
        ProducerDetailPopup.push(suspensePayment.ProducerAppliedTo);
      }
    }
    
    function transactionNumberAction(suspensePayment : SuspensePayment) {
      if (suspensePaymentUtil.getDesktopTransaction(suspensePayment) != null) {
        TransactionDetailPopup.push(suspensePaymentUtil.getDesktopTransaction(suspensePayment))
      } else {
        SuspensePaymentMultipleTransactionsPopup.push(suspensePayment)
      }
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopSuspensePayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopSuspensePaymentsScreenExpressionsImpl extends DesktopSuspensePaymentsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=NewPayment) at DesktopSuspensePayments.pcf: line 61, column 51
    function action_15 () : void {
      NewSuspensePayment.push(currencyForNewPayment)
    }
    
    // 'action' attribute on ToolbarButton (id=NewPayment) at DesktopSuspensePayments.pcf: line 61, column 51
    function action_dest_16 () : pcf.api.Destination {
      return pcf.NewSuspensePayment.createDestination(currencyForNewPayment)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=ReverseSuspensePayments) at DesktopSuspensePayments.pcf: line 48, column 90
    function allCheckedRowsAction_5 (CheckedValues :  entity.SuspensePayment[], CheckedValuesErrors :  java.util.Map) : void {
      suspensePaymentUtil.validateSuspensePaymentsAreReversible(CheckedValues?.toList()); ReverseSuspensePaymentsPopup.push(CheckedValues?.toList())
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=ReverseSuspensePayments) at DesktopSuspensePayments.pcf: line 48, column 90
    function available_4 () : java.lang.Boolean {
      return perm.SuspensePayment.edit
    }
    
    // 'value' attribute on ToolbarRangeInput (id=CurrencyToUse_Input) at DesktopSuspensePayments.pcf: line 56, column 102
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      currencyForNewPayment = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopSuspensePayments.pcf: line 82, column 85
    function filter_17 () : gw.api.filters.IFilter {
      return filterSet.AllFilter
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopSuspensePayments.pcf: line 85, column 90
    function filter_18 () : gw.api.filters.IFilter {
      return filterSet.ReversedFilter
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopSuspensePayments.pcf: line 88, column 89
    function filter_19 () : gw.api.filters.IFilter {
      return filterSet.AppliedFilter
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopSuspensePayments.pcf: line 92, column 37
    function filter_20 () : gw.api.filters.IFilter {
      return filterSet.OpenFilter
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopSuspensePayments.pcf: line 97, column 32
    function filter_21 () : gw.api.filters.IFilter {
      return filterSet.DisbursedFilter
    }
    
    // 'initialValue' attribute on Variable at DesktopSuspensePayments.pcf: line 29, column 26
    function initialValue_3 () : Currency {
      return gw.api.util.CurrencyUtil.getDefaultCurrency()
    }
    
    // 'value' attribute on DateCell (id=PaymentDate_Cell) at DesktopSuspensePayments.pcf: line 107, column 52
    function sortValue_22 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.PaymentDate
    }
    
    // 'value' attribute on TextCell (id=RefNumber_Cell) at DesktopSuspensePayments.pcf: line 111, column 50
    function sortValue_23 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.RefNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DesktopSuspensePayments.pcf: line 116, column 58
    function sortValue_24 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.Status
    }
    
    // 'value' attribute on AltUserCell (id=AppliedBy_Cell) at DesktopSuspensePayments.pcf: line 136, column 54
    function sortValue_25 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.AppliedByUser
    }
    
    // 'value' attribute on TypeKeyCell (id=Currency_Cell) at DesktopSuspensePayments.pcf: line 142, column 45
    function sortValue_26 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at DesktopSuspensePayments.pcf: line 149, column 47
    function sortValue_27 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.Amount
    }
    
    // '$$sumValue' attribute on RowIterator at DesktopSuspensePayments.pcf: line 149, column 47
    function sumValueRoot_31 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment
    }
    
    // 'footerSumValue' attribute on RowIterator at DesktopSuspensePayments.pcf: line 149, column 47
    function sumValue_30 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.Amount
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=CurrencyToUse_Input) at DesktopSuspensePayments.pcf: line 56, column 102
    function valueRange_9 () : java.lang.Object {
      return Currency.getTypeKeys(false)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=CurrencyToUse_Input) at DesktopSuspensePayments.pcf: line 56, column 102
    function value_7 () : typekey.Currency {
      return currencyForNewPayment
    }
    
    // 'value' attribute on RowIterator at DesktopSuspensePayments.pcf: line 74, column 86
    function value_83 () : gw.api.database.IQueryBeanResult<entity.SuspensePayment> {
      return suspensePayments
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=CurrencyToUse_Input) at DesktopSuspensePayments.pcf: line 56, column 102
    function verifyValueRangeIsAllowedType_10 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=CurrencyToUse_Input) at DesktopSuspensePayments.pcf: line 56, column 102
    function verifyValueRangeIsAllowedType_10 ($$arg :  typekey.Currency[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=CurrencyToUse_Input) at DesktopSuspensePayments.pcf: line 56, column 102
    function verifyValueRange_11 () : void {
      var __valueRangeArg = Currency.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_10(__valueRangeArg)
    }
    
    // 'visible' attribute on ToolbarButton (id=NewPayment) at DesktopSuspensePayments.pcf: line 61, column 51
    function visible_14 () : java.lang.Boolean {
      return perm.SuspensePayment.process
    }
    
    // 'visible' attribute on TextCell (id=EditPayment_Cell) at DesktopSuspensePayments.pcf: line 170, column 51
    function visible_28 () : java.lang.Boolean {
      return perm.System.susppmntedit
    }
    
    // 'visible' attribute on TextCell (id=ApplyPayment_Cell) at DesktopSuspensePayments.pcf: line 180, column 51
    function visible_29 () : java.lang.Boolean {
      return perm.System.susppmntproc
    }
    
    // 'visible' attribute on ToolbarRangeInput (id=CurrencyToUse_Input) at DesktopSuspensePayments.pcf: line 56, column 102
    function visible_6 () : java.lang.Boolean {
      return perm.SuspensePayment.process and gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    property get currencyForNewPayment () : Currency {
      return getVariableValue("currencyForNewPayment", 1) as Currency
    }
    
    property set currencyForNewPayment ($arg :  Currency) {
      setVariableValue("currencyForNewPayment", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopSuspensePayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DesktopSuspensePaymentsScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=ApplyTo_Cell) at DesktopSuspensePayments.pcf: line 131, column 52
    function actionAvailable_49 () : java.lang.Boolean {
      return suspensePaymentUtil.isActionAvailable(suspensePayment)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at DesktopSuspensePayments.pcf: line 123, column 29
    function action_45 () : void {
      transactionNumberAction(suspensePayment)
    }
    
    // 'action' attribute on TextCell (id=ApplyTo_Cell) at DesktopSuspensePayments.pcf: line 131, column 52
    function action_48 () : void {
      appliedAction(suspensePayment);
    }
    
    // MenuItem (id=UserBrowseMenuItem) at DesktopSuspensePayments.pcf: line 136, column 54
    function action_53 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // 'action' attribute on TextCell (id=EditPayment_Cell) at DesktopSuspensePayments.pcf: line 170, column 51
    function action_73 () : void {
      EditSuspensePaymentPopup.push(suspensePayment)
    }
    
    // 'action' attribute on TextCell (id=ApplyPayment_Cell) at DesktopSuspensePayments.pcf: line 180, column 51
    function action_79 () : void {
      ApplySuspensePaymentPopup.push(suspensePayment)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at DesktopSuspensePayments.pcf: line 136, column 54
    function action_dest_54 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'action' attribute on TextCell (id=EditPayment_Cell) at DesktopSuspensePayments.pcf: line 170, column 51
    function action_dest_74 () : pcf.api.Destination {
      return pcf.EditSuspensePaymentPopup.createDestination(suspensePayment)
    }
    
    // 'action' attribute on TextCell (id=ApplyPayment_Cell) at DesktopSuspensePayments.pcf: line 180, column 51
    function action_dest_80 () : pcf.api.Destination {
      return pcf.ApplySuspensePaymentPopup.createDestination(suspensePayment)
    }
    
    // 'available' attribute on TextCell (id=EditPayment_Cell) at DesktopSuspensePayments.pcf: line 170, column 51
    function available_71 () : java.lang.Boolean {
      return suspensePayment.Status == SuspensePaymentStatus.TC_OPEN
    }
    
    // 'condition' attribute on ToolbarFlag at DesktopSuspensePayments.pcf: line 77, column 33
    function condition_34 () : java.lang.Boolean {
      return suspensePayment.Status == typekey.SuspensePaymentStatus.TC_OPEN
    }
    
    // 'condition' attribute on ToolbarFlag at DesktopSuspensePayments.pcf: line 101, column 44
    function condition_35 () : java.lang.Boolean {
      return suspensePayment.Status == typekey.SuspensePaymentStatus.TC_OPEN or suspensePayment.Status == typekey.SuspensePaymentStatus.TC_DISBURSED
    }
    
    // 'value' attribute on DateCell (id=PaymentDate_Cell) at DesktopSuspensePayments.pcf: line 107, column 52
    function valueRoot_37 () : java.lang.Object {
      return suspensePayment
    }
    
    // 'value' attribute on DateCell (id=PaymentDate_Cell) at DesktopSuspensePayments.pcf: line 107, column 52
    function value_36 () : java.util.Date {
      return suspensePayment.PaymentDate
    }
    
    // 'value' attribute on TextCell (id=RefNumber_Cell) at DesktopSuspensePayments.pcf: line 111, column 50
    function value_39 () : java.lang.String {
      return suspensePayment.RefNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DesktopSuspensePayments.pcf: line 116, column 58
    function value_42 () : typekey.SuspensePaymentStatus {
      return suspensePayment.Status
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at DesktopSuspensePayments.pcf: line 123, column 29
    function value_46 () : java.lang.String {
      return suspensePaymentUtil.getDesktopTransactionDisplayValue(suspensePayment)
    }
    
    // 'value' attribute on TextCell (id=ApplyTo_Cell) at DesktopSuspensePayments.pcf: line 131, column 52
    function value_50 () : java.lang.String {
      return suspensePayment.ApplyEntity
    }
    
    // 'value' attribute on AltUserCell (id=AppliedBy_Cell) at DesktopSuspensePayments.pcf: line 136, column 54
    function value_55 () : entity.User {
      return suspensePayment.AppliedByUser
    }
    
    // 'value' attribute on TypeKeyCell (id=Currency_Cell) at DesktopSuspensePayments.pcf: line 142, column 45
    function value_58 () : typekey.Currency {
      return suspensePayment.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at DesktopSuspensePayments.pcf: line 149, column 47
    function value_61 () : gw.pl.currency.MonetaryAmount {
      return suspensePayment.Amount
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DesktopSuspensePayments.pcf: line 155, column 52
    function value_65 () : java.lang.String {
      return suspensePayment.Description
    }
    
    // 'value' attribute on TextCell (id=InvoiceNumber_Cell) at DesktopSuspensePayments.pcf: line 160, column 54
    function value_68 () : java.lang.String {
      return suspensePayment.InvoiceNumber
    }
    
    // 'valueVisible' attribute on TextCell (id=EditPayment_Cell) at DesktopSuspensePayments.pcf: line 170, column 51
    function visible_72 () : java.lang.Boolean {
      return perm.SuspensePayment.edit
    }
    
    // 'visible' attribute on TextCell (id=EditPayment_Cell) at DesktopSuspensePayments.pcf: line 170, column 51
    function visible_76 () : java.lang.Boolean {
      return perm.System.susppmntedit
    }
    
    // 'visible' attribute on TextCell (id=ApplyPayment_Cell) at DesktopSuspensePayments.pcf: line 180, column 51
    function visible_82 () : java.lang.Boolean {
      return perm.System.susppmntproc
    }
    
    property get suspensePayment () : entity.SuspensePayment {
      return getIteratedValue(2) as entity.SuspensePayment
    }
    
    
  }
  
  
}