package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountContactCorrespondenceDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountContactCorrespondenceDVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountContactCorrespondenceDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountContactCorrespondenceDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on InputIterator (id=CorrespondenceTypes) at AccountContactCorrespondenceDV.pcf: line 18, column 56
    function value_6 () : gw.account.AccountCorrespondence[] {
      return gw.account.AccountCorrespondence.getRows(accountContact)
    }
    
    property get accountContact () : AccountContact {
      return getRequireValue("accountContact", 0) as AccountContact
    }
    
    property set accountContact ($arg :  AccountContact) {
      setRequireValue("accountContact", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountContactCorrespondenceDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountContactCorrespondenceDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on CheckBoxInput (id=CorrespondenceType_Input) at AccountContactCorrespondenceDV.pcf: line 24, column 51
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      correspondenceTypeRow.Recipient = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'label' attribute on CheckBoxInput (id=CorrespondenceType_Input) at AccountContactCorrespondenceDV.pcf: line 24, column 51
    function label_0 () : java.lang.Object {
      return correspondenceTypeRow.CorrespondenceType
    }
    
    // 'value' attribute on CheckBoxInput (id=CorrespondenceType_Input) at AccountContactCorrespondenceDV.pcf: line 24, column 51
    function valueRoot_3 () : java.lang.Object {
      return correspondenceTypeRow
    }
    
    // 'value' attribute on CheckBoxInput (id=CorrespondenceType_Input) at AccountContactCorrespondenceDV.pcf: line 24, column 51
    function value_1 () : java.lang.Boolean {
      return correspondenceTypeRow.Recipient
    }
    
    property get correspondenceTypeRow () : gw.account.AccountCorrespondence {
      return getIteratedValue(1) as gw.account.AccountCorrespondence
    }
    
    
  }
  
  
}