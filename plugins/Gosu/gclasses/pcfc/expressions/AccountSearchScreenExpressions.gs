package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/AccountSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/search/AccountSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get isClearBundle () : Boolean {
      return getRequireValue("isClearBundle", 0) as Boolean
    }
    
    property set isClearBundle ($arg :  Boolean) {
      setRequireValue("isClearBundle", 0, $arg)
    }
    
    property get showHyperlinks () : Boolean {
      return getRequireValue("showHyperlinks", 0) as Boolean
    }
    
    property set showHyperlinks ($arg :  Boolean) {
      setRequireValue("showHyperlinks", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/AccountSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends AccountSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at AccountSearchScreen.pcf: line 23, column 47
    function def_onEnter_0 (def :  pcf.AccountSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at AccountSearchScreen.pcf: line 27, column 94
    function def_onEnter_2 (def :  pcf.AccountSearchResultsLV) : void {
      def.onEnter(accountSearchViews, null, false, showHyperlinks, false)
    }
    
    // 'def' attribute on PanelRef at AccountSearchScreen.pcf: line 23, column 47
    function def_refreshVariables_1 (def :  pcf.AccountSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at AccountSearchScreen.pcf: line 27, column 94
    function def_refreshVariables_3 (def :  pcf.AccountSearchResultsLV) : void {
      def.refreshVariables(accountSearchViews, null, false, showHyperlinks, false)
    }
    
    // 'maxSearchResults' attribute on SearchPanel at AccountSearchScreen.pcf: line 21, column 85
    function maxSearchResults_4 () : java.lang.Object {
      return gw.api.system.BCConfigParameters.MaxSearchResults.Value
    }
    
    // 'searchCriteria' attribute on SearchPanel at AccountSearchScreen.pcf: line 21, column 85
    function searchCriteria_6 () : gw.search.AccountSearchCriteria {
      return new gw.search.AccountSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at AccountSearchScreen.pcf: line 21, column 85
    function search_5 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria, StringCriterionMode.TC_STARTSWITH)
    }
    
    property get accountSearchViews () : gw.api.database.IQueryBeanResult<AccountSearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<AccountSearchView>
    }
    
    property get searchCriteria () : gw.search.AccountSearchCriteria {
      return getCriteriaValue(1) as gw.search.AccountSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.AccountSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}