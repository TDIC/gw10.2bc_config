package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountSummaryForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountSummaryForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountSummaryForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountSummaryForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (accountNumber :  String) : int {
      return 0
    }
    
    // 'action' attribute on ForwardCondition at AccountSummaryForward.pcf: line 18, column 36
    function action_1 () : void {
      AccountOverview.go(account)
    }
    
    // 'action' attribute on ForwardCondition at AccountSummaryForward.pcf: line 21, column 36
    function action_4 () : void {
      AccountSearch.go()
    }
    
    // 'action' attribute on ForwardCondition at AccountSummaryForward.pcf: line 18, column 36
    function action_dest_2 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(account)
    }
    
    // 'action' attribute on ForwardCondition at AccountSummaryForward.pcf: line 21, column 36
    function action_dest_5 () : pcf.api.Destination {
      return pcf.AccountSearch.createDestination()
    }
    
    // 'condition' attribute on ForwardCondition at AccountSummaryForward.pcf: line 18, column 36
    function condition_3 () : java.lang.Boolean {
      return account != null
    }
    
    // 'condition' attribute on ForwardCondition at AccountSummaryForward.pcf: line 21, column 36
    function condition_6 () : java.lang.Boolean {
      return account == null
    }
    
    // 'initialValue' attribute on Variable at AccountSummaryForward.pcf: line 15, column 30
    function initialValue_0 () : entity.Account {
      return gw.api.database.Query.make(entity.Account).compare("AccountNumber", Equals, accountNumber).select().getFirstResult()
    }
    
    override property get CurrentLocation () : pcf.AccountSummaryForward {
      return super.CurrentLocation as pcf.AccountSummaryForward
    }
    
    property get account () : entity.Account {
      return getVariableValue("account", 0) as entity.Account
    }
    
    property set account ($arg :  entity.Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get accountNumber () : String {
      return getVariableValue("accountNumber", 0) as String
    }
    
    property set accountNumber ($arg :  String) {
      setVariableValue("accountNumber", 0, $arg)
    }
    
    
  }
  
  
}