package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/groups/GroupsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class GroupsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/groups/GroupsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class GroupsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on TextCell (id=Name_Cell) at GroupsLV.pcf: line 24, column 38
    function sortValue_0 (Group :  entity.Group) : java.lang.Object {
      return Group.Name
    }
    
    // 'value' attribute on RowIterator at GroupsLV.pcf: line 14, column 72
    function value_29 () : gw.api.database.IQueryBeanResult<entity.Group> {
      return Groups
    }
    
    property get Groups () : gw.api.database.IQueryBeanResult<Group> {
      return getRequireValue("Groups", 0) as gw.api.database.IQueryBeanResult<Group>
    }
    
    property set Groups ($arg :  gw.api.database.IQueryBeanResult<Group>) {
      setRequireValue("Groups", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/groups/GroupsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends GroupsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on GroupCell (id=Parent_Cell) at GroupWidget.xml: line 7, column 52
    function actionAvailable_17 () : java.lang.Boolean {
      return Group.Parent != null
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at GroupsLV.pcf: line 24, column 38
    function action_1 () : void {
      NewGroupDetailPage.push(Group)
    }
    
    // 'action' attribute on GroupCell (id=Parent_Cell) at GroupWidget.xml: line 10, column 49
    function action_11 () : void {
      pcf.GroupSearchPopup.push()
    }
    
    // 'action' attribute on GroupCell (id=Parent_Cell) at GroupWidget.xml: line 13, column 49
    function action_13 () : void {
      pcf.OrganizationGroupTreePopup.push()
    }
    
    // 'action' attribute on GroupCell (id=Parent_Cell) at GroupWidget.xml: line 7, column 52
    function action_15 () : void {
      GroupDetailPage.push(Group.Parent)
    }
    
    // 'action' attribute on GroupCell (id=Parent_Cell) at GroupsLV.pcf: line 38, column 33
    function action_23 () : void {
      GroupDetailPage.push(Group.Parent)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at GroupsLV.pcf: line 30, column 37
    function action_6 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // 'action' attribute on GroupCell (id=Parent_Cell) at GroupWidget.xml: line 10, column 49
    function action_dest_12 () : pcf.api.Destination {
      return pcf.GroupSearchPopup.createDestination()
    }
    
    // 'action' attribute on GroupCell (id=Parent_Cell) at GroupWidget.xml: line 13, column 49
    function action_dest_14 () : pcf.api.Destination {
      return pcf.OrganizationGroupTreePopup.createDestination()
    }
    
    // 'action' attribute on GroupCell (id=Parent_Cell) at GroupWidget.xml: line 7, column 52
    function action_dest_16 () : pcf.api.Destination {
      return pcf.GroupDetailPage.createDestination(Group.Parent)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at GroupsLV.pcf: line 24, column 38
    function action_dest_2 () : pcf.api.Destination {
      return pcf.NewGroupDetailPage.createDestination(Group)
    }
    
    // 'action' attribute on GroupCell (id=Parent_Cell) at GroupsLV.pcf: line 38, column 33
    function action_dest_24 () : pcf.api.Destination {
      return pcf.GroupDetailPage.createDestination(Group.Parent)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at GroupsLV.pcf: line 30, column 37
    function action_dest_7 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'valueRange' attribute on GroupCell (id=Parent_Cell) at GroupWidget.xml: line 7, column 52
    function valueRange_20 () : java.lang.Object {
      return gw.api.admin.BaseAdminUtil.getGroupsForCurrentUser()
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at GroupsLV.pcf: line 24, column 38
    function valueRoot_4 () : java.lang.Object {
      return Group
    }
    
    // 'value' attribute on GroupCell (id=Parent_Cell) at GroupWidget.xml: line 7, column 52
    function value_18 () : entity.Group {
      return Group.Parent
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at GroupsLV.pcf: line 24, column 38
    function value_3 () : java.lang.String {
      return Group.DisplayName
    }
    
    // 'value' attribute on AltUserCell (id=Supervisor_Cell) at GroupsLV.pcf: line 30, column 37
    function value_8 () : entity.User {
      return Group.Supervisor
    }
    
    // 'valueRange' attribute on GroupCell (id=Parent_Cell) at GroupWidget.xml: line 7, column 52
    function verifyValueRangeIsAllowedType_21 ($$arg :  entity.Group[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on GroupCell (id=Parent_Cell) at GroupWidget.xml: line 7, column 52
    function verifyValueRangeIsAllowedType_21 ($$arg :  gw.api.database.IQueryBeanResult<entity.Group>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on GroupCell (id=Parent_Cell) at GroupWidget.xml: line 7, column 52
    function verifyValueRangeIsAllowedType_21 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on GroupCell (id=Parent_Cell) at GroupWidget.xml: line 7, column 52
    function verifyValueRange_22 () : void {
      var __valueRangeArg = gw.api.admin.BaseAdminUtil.getGroupsForCurrentUser()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_21(__valueRangeArg)
    }
    
    property get Group () : entity.Group {
      return getIteratedValue(1) as entity.Group
    }
    
    
  }
  
  
}