package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.acc.acct360.accountview.Acct360TranDetail
@javax.annotation.Generated("config/web/pcf/acc/acct360/AccountView360Screen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountView360ScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/acct360/AccountView360Screen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountView360ScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_HeldChargesAlertBar) at AccountView360Screen.pcf: line 52, column 43
    function action_15 () : void {
      AccountDetailCharges.go(account)
    }
    
    // 'action' attribute on ToolbarButton (id=searchProduct) at AccountView360Screen.pcf: line 75, column 52
    function action_26 () : void {
      acctBalanceSummary = gw.acc.acct360.accountview.AccountBalanceTxnUtil.createTransactionRecords(account,null,null,selectedProduct)
    }
    
    // 'action' attribute on ToolbarButton (id=searchPolicy) at AccountView360Screen.pcf: line 88, column 52
    function action_36 () : void {
      acctBalanceSummary = gw.acc.acct360.accountview.AccountBalanceTxnUtil.createTransactionRecords(account,null,selectedPolicy, null)
    }
    
    // 'action' attribute on ToolbarButton (id=searchPP) at AccountView360Screen.pcf: line 100, column 109
    function action_43 () : void {
      acctBalanceSummary = gw.acc.acct360.accountview.AccountBalanceTxnUtil.createTransactionRecords(account,selectedPolicyPeriod,policyToDisplay,null)
    }
    
    // 'action' attribute on ToolbarButton (id=CollectionAgencyReportToolbarButton) at AccountView360Screen.pcf: line 104, column 114
    function action_44 () : void {
      gw.api.print.ListViewPrintOptionPopupAction.printListViewOnlyWithOptions("AccountViewBalanceSummaryLV", DisplayKey.get("Accelerator.Acc360.AccountView.Account360Screen.PrintExport"), null)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_TroubleTicketAlertAlertBar) at AccountView360Screen.pcf: line 37, column 50
    function action_5 () : void {
      TroubleTicketAlertForward.push(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_DelinquencyAlertAlertBar) at AccountView360Screen.pcf: line 42, column 67
    function action_9 () : void {
      AccountDetailDelinquencies.go(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_DelinquencyAlertAlertBar) at AccountView360Screen.pcf: line 42, column 67
    function action_dest_10 () : pcf.api.Destination {
      return pcf.AccountDetailDelinquencies.createDestination(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_HeldChargesAlertBar) at AccountView360Screen.pcf: line 52, column 43
    function action_dest_16 () : pcf.api.Destination {
      return pcf.AccountDetailCharges.createDestination(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_TroubleTicketAlertAlertBar) at AccountView360Screen.pcf: line 37, column 50
    function action_dest_6 () : pcf.api.Destination {
      return pcf.TroubleTicketAlertForward.createDestination(account)
    }
    
    // 'available' attribute on AlertBar (id=AccountDetailSummary_HeldChargesAlertBar) at AccountView360Screen.pcf: line 52, column 43
    function available_13 () : java.lang.Boolean {
      return perm.System.acctchargesview
    }
    
    // 'available' attribute on AlertBar (id=AccountDetailSummary_TroubleTicketAlertAlertBar) at AccountView360Screen.pcf: line 37, column 50
    function available_3 () : java.lang.Boolean {
      return perm.System.acctttktview
    }
    
    // 'def' attribute on PanelRef at AccountView360Screen.pcf: line 61, column 75
    function def_onEnter_45 (def :  pcf.AccountViewBalanceSummaryLV) : void {
      def.onEnter(account,acctBalanceSummary)
    }
    
    // 'def' attribute on PanelRef at AccountView360Screen.pcf: line 61, column 75
    function def_refreshVariables_46 (def :  pcf.AccountViewBalanceSummaryLV) : void {
      def.refreshVariables(account,acctBalanceSummary)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=searchByProduct_Input) at AccountView360Screen.pcf: line 70, column 52
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectedProduct = (__VALUE_TO_SET as LOBCode)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=policyrange_Input) at AccountView360Screen.pcf: line 83, column 52
    function defaultSetter_29 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectedPolicy = (__VALUE_TO_SET as Policy)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=PolicyPeriodFilter_Input) at AccountView360Screen.pcf: line 96, column 43
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectedPolicyPeriod = (__VALUE_TO_SET as PolicyPeriod)
    }
    
    // 'initialValue' attribute on Variable at AccountView360Screen.pcf: line 16, column 50
    function initialValue_0 () : java.util.List<PolicyPeriod> {
      return getPolicyPeriods()
    }
    
    // 'initialValue' attribute on Variable at AccountView360Screen.pcf: line 23, column 22
    function initialValue_1 () : Policy {
      return policyToDisplay
    }
    
    // 'initialValue' attribute on Variable at AccountView360Screen.pcf: line 31, column 62
    function initialValue_2 () : gw.acc.acct360.accountview.Acct360TranDetail[] {
      return showTransactionRecords()
    }
    
    // 'label' attribute on AlertBar (id=AccountDetailSummary_DelinquencyAlertAlertBar) at AccountView360Screen.pcf: line 42, column 67
    function label_11 () : java.lang.Object {
      return DisplayKey.get("Web.AccountDetailSummary.DelinquencyAlert", account.getDelinquencyPlan())
    }
    
    // 'label' attribute on AlertBar (id=AccountDetailSummary_TroubleTicketAlertAlertBar) at AccountView360Screen.pcf: line 37, column 50
    function label_7 () : java.lang.Object {
      return account.AlertBarDisplayText
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=searchByProduct_Input) at AccountView360Screen.pcf: line 70, column 52
    function valueRange_20 () : java.lang.Object {
      return LOBCode.getTypeKeys(false)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=policyrange_Input) at AccountView360Screen.pcf: line 83, column 52
    function valueRange_30 () : java.lang.Object {
      return account.Policies
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=PolicyPeriodFilter_Input) at AccountView360Screen.pcf: line 96, column 43
    function valueRange_39 () : java.lang.Object {
      return plcyPeriodList
    }
    
    // 'value' attribute on ToolbarRangeInput (id=searchByProduct_Input) at AccountView360Screen.pcf: line 70, column 52
    function value_18 () : LOBCode {
      return selectedProduct
    }
    
    // 'value' attribute on ToolbarRangeInput (id=policyrange_Input) at AccountView360Screen.pcf: line 83, column 52
    function value_28 () : Policy {
      return selectedPolicy
    }
    
    // 'value' attribute on ToolbarRangeInput (id=PolicyPeriodFilter_Input) at AccountView360Screen.pcf: line 96, column 43
    function value_37 () : PolicyPeriod {
      return selectedPolicyPeriod
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=searchByProduct_Input) at AccountView360Screen.pcf: line 70, column 52
    function verifyValueRangeIsAllowedType_21 ($$arg :  LOBCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=searchByProduct_Input) at AccountView360Screen.pcf: line 70, column 52
    function verifyValueRangeIsAllowedType_21 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=searchByProduct_Input) at AccountView360Screen.pcf: line 70, column 52
    function verifyValueRangeIsAllowedType_21 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=policyrange_Input) at AccountView360Screen.pcf: line 83, column 52
    function verifyValueRangeIsAllowedType_31 ($$arg :  Policy[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=policyrange_Input) at AccountView360Screen.pcf: line 83, column 52
    function verifyValueRangeIsAllowedType_31 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=policyrange_Input) at AccountView360Screen.pcf: line 83, column 52
    function verifyValueRangeIsAllowedType_31 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=PolicyPeriodFilter_Input) at AccountView360Screen.pcf: line 96, column 43
    function verifyValueRangeIsAllowedType_40 ($$arg :  PolicyPeriod[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=PolicyPeriodFilter_Input) at AccountView360Screen.pcf: line 96, column 43
    function verifyValueRangeIsAllowedType_40 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=PolicyPeriodFilter_Input) at AccountView360Screen.pcf: line 96, column 43
    function verifyValueRangeIsAllowedType_40 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=searchByProduct_Input) at AccountView360Screen.pcf: line 70, column 52
    function verifyValueRange_22 () : void {
      var __valueRangeArg = LOBCode.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_21(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=policyrange_Input) at AccountView360Screen.pcf: line 83, column 52
    function verifyValueRange_32 () : void {
      var __valueRangeArg = account.Policies
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_31(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=PolicyPeriodFilter_Input) at AccountView360Screen.pcf: line 96, column 43
    function verifyValueRange_41 () : void {
      var __valueRangeArg = plcyPeriodList
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_40(__valueRangeArg)
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailSummary_WriteoffAlertAlertBar) at AccountView360Screen.pcf: line 46, column 125
    function visible_12 () : java.lang.Boolean {
      return account.WriteoffExpense.Amount > java.lang.Math.max( (0.1 * account.TotalValue) as double, 0 as double)
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailSummary_HeldChargesAlertBar) at AccountView360Screen.pcf: line 52, column 43
    function visible_14 () : java.lang.Boolean {
      return account.hasHeldCharges()
    }
    
    // 'visible' attribute on ToolbarRangeInput (id=searchByProduct_Input) at AccountView360Screen.pcf: line 70, column 52
    function visible_17 () : java.lang.Boolean {
      return policyToDisplay == null
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailSummary_TroubleTicketAlertAlertBar) at AccountView360Screen.pcf: line 37, column 50
    function visible_4 () : java.lang.Boolean {
      return account.HasActiveTroubleTickets
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailSummary_DelinquencyAlertAlertBar) at AccountView360Screen.pcf: line 42, column 67
    function visible_8 () : java.lang.Boolean {
      return account.hasActiveDelinquenciesOutOfGracePeriod()
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get acctBalanceSummary () : gw.acc.acct360.accountview.Acct360TranDetail[] {
      return getVariableValue("acctBalanceSummary", 0) as gw.acc.acct360.accountview.Acct360TranDetail[]
    }
    
    property set acctBalanceSummary ($arg :  gw.acc.acct360.accountview.Acct360TranDetail[]) {
      setVariableValue("acctBalanceSummary", 0, $arg)
    }
    
    property get plcyPeriodList () : java.util.List<PolicyPeriod> {
      return getVariableValue("plcyPeriodList", 0) as java.util.List<PolicyPeriod>
    }
    
    property set plcyPeriodList ($arg :  java.util.List<PolicyPeriod>) {
      setVariableValue("plcyPeriodList", 0, $arg)
    }
    
    property get policyToDisplay () : Policy {
      return getRequireValue("policyToDisplay", 0) as Policy
    }
    
    property set policyToDisplay ($arg :  Policy) {
      setRequireValue("policyToDisplay", 0, $arg)
    }
    
    property get selectedPolicy () : Policy {
      return getVariableValue("selectedPolicy", 0) as Policy
    }
    
    property set selectedPolicy ($arg :  Policy) {
      setVariableValue("selectedPolicy", 0, $arg)
    }
    
    property get selectedPolicyPeriod () : PolicyPeriod {
      return getVariableValue("selectedPolicyPeriod", 0) as PolicyPeriod
    }
    
    property set selectedPolicyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("selectedPolicyPeriod", 0, $arg)
    }
    
    property get selectedProduct () : LOBCode {
      return getVariableValue("selectedProduct", 0) as LOBCode
    }
    
    property set selectedProduct ($arg :  LOBCode) {
      setVariableValue("selectedProduct", 0, $arg)
    }
    
    
    function getPolicyPeriods () : java.util.List<PolicyPeriod> {
      if (policyToDisplay == null) {
        return account.PolicyPeriods.where( \ elt -> elt.BillingMethod==PolicyPeriodBillingMethod.TC_DIRECTBILL).toList()
      } else {
        return policyToDisplay.OrderedPolicyPeriods.toList()
      }
    }
    
    function showTransactionRecords () : Acct360TranDetail[] {
      if(selectedPolicyPeriod != null){
        return gw.acc.acct360.accountview.AccountBalanceTxnUtil.createTransactionRecords(account,selectedPolicyPeriod,policyToDisplay,null)
      } else if(selectedPolicy != null){
        return gw.acc.acct360.accountview.AccountBalanceTxnUtil.createTransactionRecords(account,null,selectedPolicy,null)
      } else if(selectedProduct != null) {
        return gw.acc.acct360.accountview.AccountBalanceTxnUtil.createTransactionRecords(account,null,policyToDisplay,selectedProduct)
      } else {
        return gw.acc.acct360.accountview.AccountBalanceTxnUtil.createTransactionRecords(account,null,policyToDisplay,null)  
      }
    }
    
    
  }
  
  
}