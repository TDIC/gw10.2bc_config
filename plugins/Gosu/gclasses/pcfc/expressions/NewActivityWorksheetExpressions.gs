package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/NewActivityWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewActivityWorksheetExpressions {
  @javax.annotation.Generated("config/web/pcf/activity/NewActivityWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewActivityWorksheetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=NewActivityScreen_CreateActivityButton) at NewActivityWorksheet.pcf: line 31, column 85
    function action_2 () : void {
      assigneeHolder[0].assignToThis(activity); CurrentLocation.commit()
    }
    
    // 'action' attribute on ToolbarButton (id=NewActivityWorksheet_CancelButton) at NewActivityWorksheet.pcf: line 35, column 62
    function action_3 () : void {
      CurrentLocation.cancel()
    }
    
    // 'afterCancel' attribute on Worksheet (id=NewActivityWorksheet) at NewActivityWorksheet.pcf: line 12, column 63
    function afterCancel_8 () : void {
      DesktopActivities.go()
    }
    
    // 'afterCancel' attribute on Worksheet (id=NewActivityWorksheet) at NewActivityWorksheet.pcf: line 12, column 63
    function afterCancel_dest_9 () : pcf.api.Destination {
      return pcf.DesktopActivities.createDestination()
    }
    
    // 'afterCommit' attribute on Worksheet (id=NewActivityWorksheet) at NewActivityWorksheet.pcf: line 12, column 63
    function afterCommit_10 (TopLocation :  pcf.api.Location) : void {
      gw.api.web.activity.ActivityUtil.markActivityAsViewed(activity)
    }
    
    // 'def' attribute on PanelRef at NewActivityWorksheet.pcf: line 38, column 59
    function def_onEnter_4 (def :  pcf.ActivityDetailDV_default) : void {
      def.onEnter(activity, assigneeHolder)
    }
    
    // 'def' attribute on PanelRef at NewActivityWorksheet.pcf: line 38, column 59
    function def_onEnter_6 (def :  pcf.ActivityDetailDV_disbappractivity) : void {
      def.onEnter(activity, assigneeHolder)
    }
    
    // 'def' attribute on PanelRef at NewActivityWorksheet.pcf: line 38, column 59
    function def_refreshVariables_5 (def :  pcf.ActivityDetailDV_default) : void {
      def.refreshVariables(activity, assigneeHolder)
    }
    
    // 'def' attribute on PanelRef at NewActivityWorksheet.pcf: line 38, column 59
    function def_refreshVariables_7 (def :  pcf.ActivityDetailDV_disbappractivity) : void {
      def.refreshVariables(activity, assigneeHolder)
    }
    
    // 'initialValue' attribute on Variable at NewActivityWorksheet.pcf: line 20, column 24
    function initialValue_0 () : Activity {
      return new Activity()
    }
    
    // 'initialValue' attribute on Variable at NewActivityWorksheet.pcf: line 24, column 44
    function initialValue_1 () : gw.api.assignment.Assignee[] {
      return activity.InitialAssigneeForPicker
    }
    
    override property get CurrentLocation () : pcf.NewActivityWorksheet {
      return super.CurrentLocation as pcf.NewActivityWorksheet
    }
    
    property get activity () : Activity {
      return getVariableValue("activity", 0) as Activity
    }
    
    property set activity ($arg :  Activity) {
      setVariableValue("activity", 0, $arg)
    }
    
    property get assigneeHolder () : gw.api.assignment.Assignee[] {
      return getVariableValue("assigneeHolder", 0) as gw.api.assignment.Assignee[]
    }
    
    property set assigneeHolder ($arg :  gw.api.assignment.Assignee[]) {
      setVariableValue("assigneeHolder", 0, $arg)
    }
    
    
  }
  
  
}