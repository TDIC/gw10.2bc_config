package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.DateUtil
@javax.annotation.Generated("config/web/pcf/payment/PaymentRequestDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentRequestDVExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/PaymentRequestDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentRequestDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextInput (id=accountNumber_Input) at PaymentRequestDV.pcf: line 36, column 40
    function action_3 () : void {
      AccountSummary.push(account)
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at PaymentRequestDV.pcf: line 101, column 46
    function action_38 () : void {
      NewPaymentInstrumentPopup.push(tdic.bc.config.payment.PaymentInstrumentFilters.newPaymentInstrumentFilterACHOnly,paymentRequest)
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at PaymentRequestDV.pcf: line 101, column 46
    function action_dest_39 () : pcf.api.Destination {
      return pcf.NewPaymentInstrumentPopup.createDestination(tdic.bc.config.payment.PaymentInstrumentFilters.newPaymentInstrumentFilterACHOnly,paymentRequest)
    }
    
    // 'action' attribute on TextInput (id=accountNumber_Input) at PaymentRequestDV.pcf: line 36, column 40
    function action_dest_4 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination(account)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=amount_Input) at PaymentRequestDV.pcf: line 77, column 40
    function currency_21 () : typekey.Currency {
      return paymentRequest.Currency
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=accountUnbilled_Input) at PaymentRequestDV.pcf: line 117, column 50
    function currency_50 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on RangeInput (id=UnappliedFunds_Input) at PaymentRequestDV.pcf: line 69, column 42
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentRequest.UnappliedFund_TDIC = (__VALUE_TO_SET as entity.UnappliedFund)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amount_Input) at PaymentRequestDV.pcf: line 77, column 40
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentRequest.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on DateInput (id=paymentDate_Input) at PaymentRequestDV.pcf: line 83, column 43
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentRequest.DraftDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=paymentDueDate_Input) at PaymentRequestDV.pcf: line 90, column 50
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentRequest.DueDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeInput (id=paymentRequestPaymentInstrument_Input) at PaymentRequestDV.pcf: line 101, column 46
    function defaultSetter_42 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentRequest.PaymentInstrument = (__VALUE_TO_SET as entity.PaymentInstrument)
    }
    
    // 'initialValue' attribute on Variable at PaymentRequestDV.pcf: line 13, column 30
    function initialValue_0 () : entity.Account {
      return paymentRequest.Account
    }
    
    // 'initialValue' attribute on Variable at PaymentRequestDV.pcf: line 17, column 49
    function initialValue_1 () : gw.payment.PaymentInstrumentRange {
      return new gw.payment.PaymentInstrumentRange(account.PaymentInstruments)
    }
    
    // 'initialValue' attribute on Variable at PaymentRequestDV.pcf: line 21, column 61
    function initialValue_2 () : gw.web.account.AccountSummaryFinancialsHelper {
      return new gw.web.account.AccountSummaryFinancialsHelper(account)
    }
    
    // 'onChange' attribute on PostOnChange at PaymentRequestDV.pcf: line 92, column 133
    function onChange_29 () : void {
      paymentRequest.DraftDate = paymentRequest.DueDate.addBusinessDays(-account.BillingPlan.DraftIntervalDayCount)
    }
    
    // 'onPick' attribute on RangeInput (id=paymentRequestPaymentInstrument_Input) at PaymentRequestDV.pcf: line 101, column 46
    function onPick_40 (PickedValue :  PaymentInstrument) : void {
      paymentInstrumentRange.addPaymentInstrument(paymentRequest.PaymentInstrument)
    }
    
    // 'validationExpression' attribute on DateInput (id=paymentDate_Input) at PaymentRequestDV.pcf: line 83, column 43
    function validationExpression_23 () : java.lang.Object {
      return ((paymentRequest.DraftDate == null) or gw.api.util.DateUtil.verifyDateOnOrAfterToday(paymentRequest.DraftDate)) ? null : DisplayKey.get("Web.NewPaymentRequestScreen.DraftDateInPast")
    }
    
    // 'validationExpression' attribute on DateInput (id=paymentDueDate_Input) at PaymentRequestDV.pcf: line 90, column 50
    function validationExpression_30 () : java.lang.Object {
      return validateDueDate()
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at PaymentRequestDV.pcf: line 69, column 42
    function valueRange_14 () : java.lang.Object {
      return account.UnappliedFundsSortedByDisplayName
    }
    
    // 'valueRange' attribute on RangeInput (id=paymentRequestPaymentInstrument_Input) at PaymentRequestDV.pcf: line 101, column 46
    function valueRange_44 () : java.lang.Object {
      return gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, gw.payment.PaymentInstrumentFilters.paymentRequestPaymentInstrumentFilter)
    }
    
    // 'value' attribute on RangeInput (id=UnappliedFunds_Input) at PaymentRequestDV.pcf: line 69, column 42
    function valueRoot_13 () : java.lang.Object {
      return paymentRequest
    }
    
    // 'value' attribute on MonetaryAmountInput (id=accountUnbilled_Input) at PaymentRequestDV.pcf: line 117, column 50
    function valueRoot_49 () : java.lang.Object {
      return financialsHelper
    }
    
    // 'value' attribute on TextInput (id=accountNumber_Input) at PaymentRequestDV.pcf: line 36, column 40
    function valueRoot_6 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on RangeInput (id=UnappliedFunds_Input) at PaymentRequestDV.pcf: line 69, column 42
    function value_11 () : entity.UnappliedFund {
      return paymentRequest.UnappliedFund_TDIC
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amount_Input) at PaymentRequestDV.pcf: line 77, column 40
    function value_18 () : gw.pl.currency.MonetaryAmount {
      return paymentRequest.Amount
    }
    
    // 'value' attribute on DateInput (id=paymentDate_Input) at PaymentRequestDV.pcf: line 83, column 43
    function value_24 () : java.util.Date {
      return paymentRequest.DraftDate
    }
    
    // 'value' attribute on DateInput (id=paymentDueDate_Input) at PaymentRequestDV.pcf: line 90, column 50
    function value_32 () : java.util.Date {
      return paymentRequest.DueDate
    }
    
    // 'value' attribute on RangeInput (id=paymentRequestPaymentInstrument_Input) at PaymentRequestDV.pcf: line 101, column 46
    function value_41 () : entity.PaymentInstrument {
      return paymentRequest.PaymentInstrument
    }
    
    // 'value' attribute on MonetaryAmountInput (id=accountUnbilled_Input) at PaymentRequestDV.pcf: line 117, column 50
    function value_48 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.UnbilledAmount
    }
    
    // 'value' attribute on TextInput (id=accountNumber_Input) at PaymentRequestDV.pcf: line 36, column 40
    function value_5 () : java.lang.String {
      return account.AccountNumber
    }
    
    // 'value' attribute on MonetaryAmountInput (id=totalCurrentBilled_Input) at PaymentRequestDV.pcf: line 123, column 48
    function value_52 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.BilledAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=totalPastDue_Input) at PaymentRequestDV.pcf: line 129, column 52
    function value_56 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.DelinquentAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=totalOutstanding_Input) at PaymentRequestDV.pcf: line 136, column 53
    function value_60 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.OutstandingAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=totalUnderContract_Input) at PaymentRequestDV.pcf: line 143, column 46
    function value_64 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.TotalValue
    }
    
    // 'value' attribute on TextInput (id=accountName_Input) at PaymentRequestDV.pcf: line 40, column 47
    function value_8 () : java.lang.String {
      return account.AccountNameLocalized
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at PaymentRequestDV.pcf: line 69, column 42
    function verifyValueRangeIsAllowedType_15 ($$arg :  entity.UnappliedFund[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at PaymentRequestDV.pcf: line 69, column 42
    function verifyValueRangeIsAllowedType_15 ($$arg :  gw.api.database.IQueryBeanResult<entity.UnappliedFund>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at PaymentRequestDV.pcf: line 69, column 42
    function verifyValueRangeIsAllowedType_15 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=paymentRequestPaymentInstrument_Input) at PaymentRequestDV.pcf: line 101, column 46
    function verifyValueRangeIsAllowedType_45 ($$arg :  entity.PaymentInstrument[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=paymentRequestPaymentInstrument_Input) at PaymentRequestDV.pcf: line 101, column 46
    function verifyValueRangeIsAllowedType_45 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentInstrument>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=paymentRequestPaymentInstrument_Input) at PaymentRequestDV.pcf: line 101, column 46
    function verifyValueRangeIsAllowedType_45 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at PaymentRequestDV.pcf: line 69, column 42
    function verifyValueRange_16 () : void {
      var __valueRangeArg = account.UnappliedFundsSortedByDisplayName
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_15(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=paymentRequestPaymentInstrument_Input) at PaymentRequestDV.pcf: line 101, column 46
    function verifyValueRange_46 () : void {
      var __valueRangeArg = gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, gw.payment.PaymentInstrumentFilters.paymentRequestPaymentInstrumentFilter)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_45(__valueRangeArg)
    }
    
    // 'visible' attribute on DateInput (id=paymentDueDate_Input) at PaymentRequestDV.pcf: line 90, column 50
    function visible_31 () : java.lang.Boolean {
      return paymentRequest.Invoice == null
    }
    
    property get account () : entity.Account {
      return getVariableValue("account", 0) as entity.Account
    }
    
    property set account ($arg :  entity.Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get financialsHelper () : gw.web.account.AccountSummaryFinancialsHelper {
      return getVariableValue("financialsHelper", 0) as gw.web.account.AccountSummaryFinancialsHelper
    }
    
    property set financialsHelper ($arg :  gw.web.account.AccountSummaryFinancialsHelper) {
      setVariableValue("financialsHelper", 0, $arg)
    }
    
    property get isOneTimePaymentRequest () : Boolean {
      return getVariableValue("isOneTimePaymentRequest", 0) as Boolean
    }
    
    property set isOneTimePaymentRequest ($arg :  Boolean) {
      setVariableValue("isOneTimePaymentRequest", 0, $arg)
    }
    
    property get paymentInstrumentRange () : gw.payment.PaymentInstrumentRange {
      return getVariableValue("paymentInstrumentRange", 0) as gw.payment.PaymentInstrumentRange
    }
    
    property set paymentInstrumentRange ($arg :  gw.payment.PaymentInstrumentRange) {
      setVariableValue("paymentInstrumentRange", 0, $arg)
    }
    
    property get paymentRequest () : PaymentRequest {
      return getRequireValue("paymentRequest", 0) as PaymentRequest
    }
    
    property set paymentRequest ($arg :  PaymentRequest) {
      setRequireValue("paymentRequest", 0, $arg)
    }
    
    property get unAppliedFund () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("unAppliedFund", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set unAppliedFund ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("unAppliedFund", 0, $arg)
    }
    
    
    function validateDueDate() : String {
      if(paymentRequest.DueDate == null)
        return null
    
      if(!DateUtil.verifyDateOnOrAfterToday(paymentRequest.DueDate)){
        return DisplayKey.get("Web.NewPaymentRequestScreen.Validation.DueDateInThePast")
      }
    
      if(paymentRequest.DraftDate != null and DateUtil.compareIgnoreTime(paymentRequest.DraftDate, paymentRequest.DueDate) > 0) {
        return DisplayKey.get("Web.NewPaymentRequestScreen.Validation.DueDateBeforeDraftDate")
      }
    
      return null
    }
    
    
  }
  
  
}