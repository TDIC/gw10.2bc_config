package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/PaymentRequestSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentRequestSearchExpressions {
  @javax.annotation.Generated("config/web/pcf/search/PaymentRequestSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentRequestSearchExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=PaymentRequestSearch) at PaymentRequestSearch.pcf: line 8, column 74
    static function canVisit_2 () : java.lang.Boolean {
      return perm.System.viewsearch and perm.System.pmntreqsearch
    }
    
    // 'def' attribute on ScreenRef at PaymentRequestSearch.pcf: line 10, column 43
    function def_onEnter_0 (def :  pcf.PaymentRequestSearchScreen) : void {
      def.onEnter()
    }
    
    // 'def' attribute on ScreenRef at PaymentRequestSearch.pcf: line 10, column 43
    function def_refreshVariables_1 (def :  pcf.PaymentRequestSearchScreen) : void {
      def.refreshVariables()
    }
    
    // Page (id=PaymentRequestSearch) at PaymentRequestSearch.pcf: line 8, column 74
    static function parent_3 () : pcf.api.Destination {
      return pcf.SearchGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.PaymentRequestSearch {
      return super.CurrentLocation as pcf.PaymentRequestSearch
    }
    
    
  }
  
  
}