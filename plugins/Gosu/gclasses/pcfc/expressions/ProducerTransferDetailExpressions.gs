package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/ProducerTransferDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerTransferDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/ProducerTransferDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerTransferDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (history :  ProducerPolicyHistory) : int {
      return 0
    }
    
    // 'action' attribute on TextInput (id=AccountNumber_Input) at ProducerTransferDetail.pcf: line 45, column 64
    function action_12 () : void {
      AccountOverview.go(policyPeriod.Policy.Account)
    }
    
    // 'action' attribute on TextInput (id=PolicyNumber_Input) at ProducerTransferDetail.pcf: line 35, column 48
    function action_2 () : void {
      PolicyOverview.go(policyPeriod)
    }
    
    // 'action' attribute on TextInput (id=OriginalProducerName_Input) at ProducerTransferDetail.pcf: line 77, column 42
    function action_32 () : void {
      ProducerDetail.go(policyTransfer.SourceProducerCode.Producer)
    }
    
    // 'action' attribute on TextInput (id=NewProducerName_Input) at ProducerTransferDetail.pcf: line 89, column 42
    function action_40 () : void {
      ProducerDetail.go(policyTransfer.DestinationProducerCode.Producer)
    }
    
    // 'action' attribute on TextInput (id=Insured_Input) at ProducerTransferDetail.pcf: line 40, column 71
    function action_7 () : void {
      AccountOverview.go(policyPeriod.Policy.Account)
    }
    
    // 'action' attribute on TextInput (id=AccountNumber_Input) at ProducerTransferDetail.pcf: line 45, column 64
    function action_dest_13 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(policyPeriod.Policy.Account)
    }
    
    // 'action' attribute on TextInput (id=PolicyNumber_Input) at ProducerTransferDetail.pcf: line 35, column 48
    function action_dest_3 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(policyPeriod)
    }
    
    // 'action' attribute on TextInput (id=OriginalProducerName_Input) at ProducerTransferDetail.pcf: line 77, column 42
    function action_dest_33 () : pcf.api.Destination {
      return pcf.ProducerDetail.createDestination(policyTransfer.SourceProducerCode.Producer)
    }
    
    // 'action' attribute on TextInput (id=NewProducerName_Input) at ProducerTransferDetail.pcf: line 89, column 42
    function action_dest_41 () : pcf.api.Destination {
      return pcf.ProducerDetail.createDestination(policyTransfer.DestinationProducerCode.Producer)
    }
    
    // 'action' attribute on TextInput (id=Insured_Input) at ProducerTransferDetail.pcf: line 40, column 71
    function action_dest_8 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(policyPeriod.Policy.Account)
    }
    
    // 'initialValue' attribute on Variable at ProducerTransferDetail.pcf: line 18, column 28
    function initialValue_0 () : PolicyPeriod {
      return history.PolicyPeriod
    }
    
    // 'initialValue' attribute on Variable at ProducerTransferDetail.pcf: line 22, column 30
    function initialValue_1 () : PolicyTransfer {
      return history.PolicyTransfer
    }
    
    // 'parent' attribute on Page (id=ProducerTransferDetail) at ProducerTransferDetail.pcf: line 9, column 74
    static function parent_48 (history :  ProducerPolicyHistory) : pcf.api.Destination {
      return pcf.PolicyDetailHistory.createDestination(history.PolicyPeriod)
    }
    
    // 'value' attribute on TextInput (id=Insured_Input) at ProducerTransferDetail.pcf: line 40, column 71
    function valueRoot_10 () : java.lang.Object {
      return policyPeriod.Policy.Account
    }
    
    // 'value' attribute on DateInput (id=TransferDate_Input) at ProducerTransferDetail.pcf: line 59, column 40
    function valueRoot_24 () : java.lang.Object {
      return history
    }
    
    // 'value' attribute on TypeKeyInput (id=CommissionTreatment_Input) at ProducerTransferDetail.pcf: line 69, column 59
    function valueRoot_30 () : java.lang.Object {
      return policyTransfer
    }
    
    // 'value' attribute on TextInput (id=OriginalProducerName_Input) at ProducerTransferDetail.pcf: line 77, column 42
    function valueRoot_35 () : java.lang.Object {
      return policyTransfer.SourceProducerCode
    }
    
    // 'value' attribute on TextInput (id=NewProducerName_Input) at ProducerTransferDetail.pcf: line 89, column 42
    function valueRoot_43 () : java.lang.Object {
      return policyTransfer.DestinationProducerCode
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at ProducerTransferDetail.pcf: line 35, column 48
    function valueRoot_5 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at ProducerTransferDetail.pcf: line 45, column 64
    function value_14 () : java.lang.String {
      return policyPeriod.Policy.Account.AccountNumber
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at ProducerTransferDetail.pcf: line 49, column 52
    function value_17 () : java.util.Date {
      return policyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at ProducerTransferDetail.pcf: line 53, column 54
    function value_20 () : java.util.Date {
      return policyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on DateInput (id=TransferDate_Input) at ProducerTransferDetail.pcf: line 59, column 40
    function value_23 () : java.util.Date {
      return history.EventDate
    }
    
    // 'value' attribute on TypeKeyInput (id=RoleTransferred_Input) at ProducerTransferDetail.pcf: line 64, column 45
    function value_26 () : typekey.PolicyRole {
      return history.RoleTransferred
    }
    
    // 'value' attribute on TypeKeyInput (id=CommissionTreatment_Input) at ProducerTransferDetail.pcf: line 69, column 59
    function value_29 () : typekey.CommissionTransferOption {
      return policyTransfer.CommissionTransferOption
    }
    
    // 'value' attribute on TextInput (id=OriginalProducerName_Input) at ProducerTransferDetail.pcf: line 77, column 42
    function value_34 () : entity.Producer {
      return policyTransfer.SourceProducerCode.Producer
    }
    
    // 'value' attribute on TextInput (id=OriginalProducerCode_Input) at ProducerTransferDetail.pcf: line 81, column 61
    function value_37 () : java.lang.String {
      return policyTransfer.SourceProducerCode.Code
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at ProducerTransferDetail.pcf: line 35, column 48
    function value_4 () : java.lang.String {
      return policyPeriod.PolicyNumber
    }
    
    // 'value' attribute on TextInput (id=NewProducerName_Input) at ProducerTransferDetail.pcf: line 89, column 42
    function value_42 () : entity.Producer {
      return policyTransfer.DestinationProducerCode.Producer
    }
    
    // 'value' attribute on TextInput (id=NewProducerCode_Input) at ProducerTransferDetail.pcf: line 93, column 66
    function value_45 () : java.lang.String {
      return policyTransfer.DestinationProducerCode.Code
    }
    
    // 'value' attribute on TextInput (id=Insured_Input) at ProducerTransferDetail.pcf: line 40, column 71
    function value_9 () : java.lang.String {
      return policyPeriod.Policy.Account.AccountNameLocalized
    }
    
    override property get CurrentLocation () : pcf.ProducerTransferDetail {
      return super.CurrentLocation as pcf.ProducerTransferDetail
    }
    
    property get history () : ProducerPolicyHistory {
      return getVariableValue("history", 0) as ProducerPolicyHistory
    }
    
    property set history ($arg :  ProducerPolicyHistory) {
      setVariableValue("history", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get policyTransfer () : PolicyTransfer {
      return getVariableValue("policyTransfer", 0) as PolicyTransfer
    }
    
    property set policyTransfer ($arg :  PolicyTransfer) {
      setVariableValue("policyTransfer", 0, $arg)
    }
    
    
  }
  
  
}