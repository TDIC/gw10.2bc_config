package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewChargeReversalWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewChargeReversalWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=NewChargeReversalWizard) at NewChargeReversalWizard.pcf: line 14, column 34
    function afterCancel_7 () : void {
      DesktopGroup.go()
    }
    
    // 'afterCancel' attribute on Wizard (id=NewChargeReversalWizard) at NewChargeReversalWizard.pcf: line 14, column 34
    function afterCancel_dest_8 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewChargeReversalWizard) at NewChargeReversalWizard.pcf: line 14, column 34
    function afterFinish_13 () : void {
      DesktopGroup.go()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewChargeReversalWizard) at NewChargeReversalWizard.pcf: line 14, column 34
    function afterFinish_dest_14 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'allowNext' attribute on WizardStep (id=Step1) at NewChargeReversalWizard.pcf: line 28, column 91
    function allowNext_1 () : java.lang.Boolean {
      return reversal.Charge != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=NewChargeReversalWizard) at NewChargeReversalWizard.pcf: line 14, column 34
    function beforeCommit_9 (pickedValue :  java.lang.Object) : void {
      reversal.reverse()
    }
    
    // 'canVisit' attribute on Wizard (id=NewChargeReversalWizard) at NewChargeReversalWizard.pcf: line 14, column 34
    static function canVisit_10 () : java.lang.Boolean {
      return perm.Transaction.revtxn
    }
    
    // 'initialValue' attribute on Variable at NewChargeReversalWizard.pcf: line 20, column 30
    function initialValue_0 () : ChargeReversal {
      return new ChargeReversal()
    }
    
    // 'onExit' attribute on WizardStep (id=Step1) at NewChargeReversalWizard.pcf: line 28, column 91
    function onExit_2 () : void {
      reversal.initiateApprovalActivityIfUserLacksAuthority()
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at NewChargeReversalWizard.pcf: line 28, column 91
    function screen_onEnter_3 (def :  pcf.NewChargeReversalChargeSearchScreen) : void {
      def.onEnter(reversal)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at NewChargeReversalWizard.pcf: line 33, column 91
    function screen_onEnter_5 (def :  pcf.NewChargeReversalConfirmationScreen) : void {
      def.onEnter(reversal)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at NewChargeReversalWizard.pcf: line 28, column 91
    function screen_refreshVariables_4 (def :  pcf.NewChargeReversalChargeSearchScreen) : void {
      def.refreshVariables(reversal)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at NewChargeReversalWizard.pcf: line 33, column 91
    function screen_refreshVariables_6 (def :  pcf.NewChargeReversalConfirmationScreen) : void {
      def.refreshVariables(reversal)
    }
    
    // 'tabBar' attribute on Wizard (id=NewChargeReversalWizard) at NewChargeReversalWizard.pcf: line 14, column 34
    function tabBar_onEnter_11 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=NewChargeReversalWizard) at NewChargeReversalWizard.pcf: line 14, column 34
    function tabBar_refreshVariables_12 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.NewChargeReversalWizard {
      return super.CurrentLocation as pcf.NewChargeReversalWizard
    }
    
    property get reversal () : ChargeReversal {
      return getVariableValue("reversal", 0) as ChargeReversal
    }
    
    property set reversal ($arg :  ChargeReversal) {
      setVariableValue("reversal", 0, $arg)
    }
    
    
  }
  
  
}