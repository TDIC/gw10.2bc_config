package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.pl.currency.MonetaryAmount
@javax.annotation.Generated("config/web/pcf/accounting/TroubleTicketTransactionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TroubleTicketTransactionsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/accounting/TroubleTicketTransactionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Debit_Cell) at TroubleTicketTransactionsLV.pcf: line 97, column 38
    function currency_26 () : typekey.Currency {
      return LineItem.Currency
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at TroubleTicketTransactionsLV.pcf: line 92, column 74
    function valueRoot_22 () : java.lang.Object {
      return LineItem.TAccount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Debit_Cell) at TroubleTicketTransactionsLV.pcf: line 97, column 38
    function valueRoot_25 () : java.lang.Object {
      return LineItem
    }
    
    // 'value' attribute on TextCell (id=Credit_Cell) at TroubleTicketTransactionsLV.pcf: line 102, column 43
    function valueRoot_29 () : java.lang.Object {
      return 0
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at TroubleTicketTransactionsLV.pcf: line 92, column 74
    function value_21 () : java.lang.String {
      return LineItem.TAccount.TAccountOwnerTypeNameTAccountName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Debit_Cell) at TroubleTicketTransactionsLV.pcf: line 97, column 38
    function value_24 () : gw.pl.currency.MonetaryAmount {
      return LineItem.Amount
    }
    
    // 'value' attribute on TextCell (id=Credit_Cell) at TroubleTicketTransactionsLV.pcf: line 102, column 43
    function value_28 () : java.lang.Double {
      return 0.0
    }
    
    property get LineItem () : entity.LineItem {
      return getIteratedValue(2) as entity.LineItem
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/accounting/TroubleTicketTransactionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Credit_Cell) at TroubleTicketTransactionsLV.pcf: line 134, column 38
    function currency_40 () : typekey.Currency {
      return LineItem.Currency
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at TroubleTicketTransactionsLV.pcf: line 124, column 74
    function valueRoot_33 () : java.lang.Object {
      return LineItem.TAccount
    }
    
    // 'value' attribute on TextCell (id=Debit_Cell) at TroubleTicketTransactionsLV.pcf: line 129, column 43
    function valueRoot_36 () : java.lang.Object {
      return 0
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Credit_Cell) at TroubleTicketTransactionsLV.pcf: line 134, column 38
    function valueRoot_39 () : java.lang.Object {
      return LineItem
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at TroubleTicketTransactionsLV.pcf: line 124, column 74
    function value_32 () : java.lang.String {
      return LineItem.TAccount.TAccountOwnerTypeNameTAccountName
    }
    
    // 'value' attribute on TextCell (id=Debit_Cell) at TroubleTicketTransactionsLV.pcf: line 129, column 43
    function value_35 () : java.lang.Double {
      return 0.0
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Credit_Cell) at TroubleTicketTransactionsLV.pcf: line 134, column 38
    function value_38 () : gw.pl.currency.MonetaryAmount {
      return LineItem.Amount
    }
    
    property get LineItem () : entity.LineItem {
      return getIteratedValue(2) as entity.LineItem
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/accounting/TroubleTicketTransactionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TroubleTicketTransactionsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at TroubleTicketTransactionsLV.pcf: line 46, column 50
    function action_4 () : void {
      TransactionDetailPopup.push(transaction)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at TroubleTicketTransactionsLV.pcf: line 46, column 50
    function action_dest_5 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(transaction)
    }
    
    // 'checkBoxVisible' attribute on RowIterator (id=Transactions) at TroubleTicketTransactionsLV.pcf: line 29, column 40
    function checkBoxVisible_43 () : java.lang.Boolean {
      return showCheckboxes
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Debit_Cell) at TroubleTicketTransactionsLV.pcf: line 63, column 48
    function currency_16 () : typekey.Currency {
      return transaction.Currency
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at TroubleTicketTransactionsLV.pcf: line 56, column 89
    function valueRoot_13 () : java.lang.Object {
      return transaction.FirstLineItem.TAccount
    }
    
    // 'value' attribute on DateCell (id=TransactionDate_Cell) at TroubleTicketTransactionsLV.pcf: line 40, column 48
    function valueRoot_2 () : java.lang.Object {
      return transaction
    }
    
    // 'value' attribute on DateCell (id=TransactionDate_Cell) at TroubleTicketTransactionsLV.pcf: line 40, column 48
    function value_1 () : java.util.Date {
      return transaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at TroubleTicketTransactionsLV.pcf: line 56, column 89
    function value_12 () : java.lang.String {
      return transaction.FirstLineItem.TAccount.TAccountOwnerTypeNameTAccountName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Debit_Cell) at TroubleTicketTransactionsLV.pcf: line 63, column 48
    function value_15 () : gw.pl.currency.MonetaryAmount {
      return getDebitAmount(transaction)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Credit_Cell) at TroubleTicketTransactionsLV.pcf: line 70, column 49
    function value_18 () : gw.pl.currency.MonetaryAmount {
      return getCreditAmount(transaction)
    }
    
    // 'value' attribute on RowIterator (id=OtherDebits) at TroubleTicketTransactionsLV.pcf: line 79, column 39
    function value_31 () : entity.LineItem[] {
      return org.apache.commons.lang.ArrayUtils.remove(transaction.DebitLineItems, 0) as LineItem[]
    }
    
    // 'value' attribute on RowIterator (id=OtherCredits) at TroubleTicketTransactionsLV.pcf: line 111, column 39
    function value_42 () : entity.LineItem[] {
      return transaction.CreditLineItems
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at TroubleTicketTransactionsLV.pcf: line 46, column 50
    function value_6 () : java.lang.String {
      return transaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at TroubleTicketTransactionsLV.pcf: line 51, column 48
    function value_9 () : java.lang.String {
      return transaction.LongDisplayName
    }
    
    property get transaction () : entity.Transaction {
      return getIteratedValue(1) as entity.Transaction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/accounting/TroubleTicketTransactionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TroubleTicketTransactionsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=TransactionDate_Cell) at TroubleTicketTransactionsLV.pcf: line 40, column 48
    function sortValue_0 (transaction :  entity.Transaction) : java.lang.Object {
      return transaction.TransactionDate
    }
    
    // 'value' attribute on RowIterator (id=Transactions) at TroubleTicketTransactionsLV.pcf: line 29, column 40
    function value_44 () : entity.Transaction[] {
      return transactions
    }
    
    property get showCheckboxes () : Boolean {
      return getRequireValue("showCheckboxes", 0) as Boolean
    }
    
    property set showCheckboxes ($arg :  Boolean) {
      setRequireValue("showCheckboxes", 0, $arg)
    }
    
    property get transactions () : entity.Transaction[] {
      return getRequireValue("transactions", 0) as entity.Transaction[]
    }
    
    property set transactions ($arg :  entity.Transaction[]) {
      setRequireValue("transactions", 0, $arg)
    }
    
    
    function getDebitAmount(transaction : Transaction) : MonetaryAmount {
      var debitAmount = transaction.FirstLineItem.Type == TC_DEBIT ? transaction.FirstLineItem.Amount : 0
      return ((debitAmount) as java.math.BigDecimal).ofCurrency(transaction.Currency)
    }
    
    function getCreditAmount(transaction : Transaction) : MonetaryAmount {
      var debitAmount = transaction.FirstLineItem.Type == TC_CREDIT ? transaction.FirstLineItem.Amount : 0
      return ((debitAmount) as java.math.BigDecimal).ofCurrency(transaction.Currency)
    }
    
    
  }
  
  
}