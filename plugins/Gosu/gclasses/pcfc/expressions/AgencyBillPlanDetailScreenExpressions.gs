package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/agencybill/AgencyBillPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillPlanDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/agencybill/AgencyBillPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillPlanDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=Clone) at AgencyBillPlanDetailScreen.pcf: line 17, column 53
    function action_2 () : void {
      CloneAgencyBillPlan.go(agencyBillPlan)
    }
    
    // 'action' attribute on ToolbarButton (id=Clone) at AgencyBillPlanDetailScreen.pcf: line 17, column 53
    function action_dest_3 () : pcf.api.Destination {
      return pcf.CloneAgencyBillPlan.createDestination(agencyBillPlan)
    }
    
    // 'def' attribute on PanelRef at AgencyBillPlanDetailScreen.pcf: line 20, column 53
    function def_onEnter_4 (def :  pcf.AgencyBillPlanDetailDV) : void {
      def.onEnter(agencyBillPlan)
    }
    
    // 'def' attribute on PanelRef at AgencyBillPlanDetailScreen.pcf: line 23, column 44
    function def_onEnter_7 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(agencyBillPlan, { "Name"}, { DisplayKey.get("Web.AgencyBillPlanDetailDV.Name") })
    }
    
    // 'def' attribute on PanelRef at AgencyBillPlanDetailScreen.pcf: line 20, column 53
    function def_refreshVariables_5 (def :  pcf.AgencyBillPlanDetailDV) : void {
      def.refreshVariables(agencyBillPlan)
    }
    
    // 'def' attribute on PanelRef at AgencyBillPlanDetailScreen.pcf: line 23, column 44
    function def_refreshVariables_8 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(agencyBillPlan, { "Name"}, { DisplayKey.get("Web.AgencyBillPlanDetailDV.Name") })
    }
    
    // 'editable' attribute on PanelRef at AgencyBillPlanDetailScreen.pcf: line 23, column 44
    function editable_6 () : java.lang.Boolean {
      return not agencyBillPlan.InUse
    }
    
    // EditButtons at AgencyBillPlanDetailScreen.pcf: line 11, column 21
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'visible' attribute on ToolbarButton (id=Clone) at AgencyBillPlanDetailScreen.pcf: line 17, column 53
    function visible_1 () : java.lang.Boolean {
      return perm.System.agencybillplancreate
    }
    
    property get agencyBillPlan () : AgencyBillPlan {
      return getRequireValue("agencyBillPlan", 0) as AgencyBillPlan
    }
    
    property set agencyBillPlan ($arg :  AgencyBillPlan) {
      setRequireValue("agencyBillPlan", 0, $arg)
    }
    
    
  }
  
  
}