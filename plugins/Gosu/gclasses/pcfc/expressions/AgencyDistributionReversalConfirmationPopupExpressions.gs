package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyDistributionReversalConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyDistributionReversalConfirmationPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyDistributionReversalConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyDistributionReversalConfirmationPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (money :  AgencyBillMoneyRcvd) : int {
      return 1
    }
    
    static function __constructorIndex (agencyCycleDist :  AgencyCycleDist) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=AgencyDistributionReversalConfirmationPopup) at AgencyDistributionReversalConfirmationPopup.pcf: line 10, column 239
    function beforeCommit_8 (pickedValue :  java.lang.Object) : void {
      reverse()
    }
    
    // 'value' attribute on TypeKeyInput (id=ReversalReason_Input) at AgencyDistributionReversalConfirmationPopup.pcf: line 56, column 56
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      reversalReason = (__VALUE_TO_SET as typekey.PaymentReversalReason)
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionReversalConfirmationPopup.pcf: line 18, column 31
    function initialValue_0 () : AgencyCycleDist {
      return money.AgencyCyclePayment
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionReversalConfirmationPopup.pcf: line 28, column 54
    function initialValue_1 () : gw.agencybill.AgencyDistributionHelper {
      return new gw.agencybill.AgencyDistributionHelper()
    }
    
    // 'label' attribute on AlertBar (id=negativeWarning) at AgencyDistributionReversalConfirmationPopup.pcf: line 44, column 62
    function label_4 () : java.lang.Object {
      return helper.processWarningMessage()
    }
    
    // 'title' attribute on Popup (id=AgencyDistributionReversalConfirmationPopup) at AgencyDistributionReversalConfirmationPopup.pcf: line 10, column 239
    static function title_9 (agencyCycleDist :  AgencyCycleDist, money :  AgencyBillMoneyRcvd) : java.lang.Object {
      return agencyCycleDist typeis AgencyCyclePromise ? DisplayKey.get("Web.AgencyDistributionReversalConfirmationPopup.Promise.Title") : DisplayKey.get("Web.AgencyDistributionReversalConfirmationPopup.Payment.Title")
    }
    
    // 'value' attribute on TypeKeyInput (id=ReversalReason_Input) at AgencyDistributionReversalConfirmationPopup.pcf: line 56, column 56
    function value_5 () : typekey.PaymentReversalReason {
      return reversalReason
    }
    
    // 'visible' attribute on AlertBar (id=LargePaymentWarning) at AgencyDistributionReversalConfirmationPopup.pcf: line 40, column 45
    function visible_2 () : java.lang.Boolean {
      return DistItemCount > 10000
    }
    
    // 'visible' attribute on AlertBar (id=negativeWarning) at AgencyDistributionReversalConfirmationPopup.pcf: line 44, column 62
    function visible_3 () : java.lang.Boolean {
      return helper.isNegative(agencyCycleDist, money)
    }
    
    override property get CurrentLocation () : pcf.AgencyDistributionReversalConfirmationPopup {
      return super.CurrentLocation as pcf.AgencyDistributionReversalConfirmationPopup
    }
    
    property get agencyCycleDist () : AgencyCycleDist {
      return getVariableValue("agencyCycleDist", 0) as AgencyCycleDist
    }
    
    property set agencyCycleDist ($arg :  AgencyCycleDist) {
      setVariableValue("agencyCycleDist", 0, $arg)
    }
    
    property get helper () : gw.agencybill.AgencyDistributionHelper {
      return getVariableValue("helper", 0) as gw.agencybill.AgencyDistributionHelper
    }
    
    property set helper ($arg :  gw.agencybill.AgencyDistributionHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get money () : AgencyBillMoneyRcvd {
      return getVariableValue("money", 0) as AgencyBillMoneyRcvd
    }
    
    property set money ($arg :  AgencyBillMoneyRcvd) {
      setVariableValue("money", 0, $arg)
    }
    
    property get reversalReason () : PaymentReversalReason {
      return getVariableValue("reversalReason", 0) as PaymentReversalReason
    }
    
    property set reversalReason ($arg :  PaymentReversalReason) {
      setVariableValue("reversalReason", 0, $arg)
    }
    
    function reverse() {
       if (agencyCycleDist != null) {
         agencyCycleDist.reverse(reversalReason)
       } else {
         money.reverse(reversalReason)
       }
    }
    
    // Query is faster than loading all the dist items in agencyCycleDist.DistItems.Count
    property get DistItemCount() : int {
      return gw.api.database.Query.make(BaseDistItem)
          .compare(BaseDistItem#ActiveDist, Equals, agencyCycleDist)
          .compare(BaseDistItem#ReversedDate, Equals, null)
          .select().Count 
    }
    
    
  }
  
  
}