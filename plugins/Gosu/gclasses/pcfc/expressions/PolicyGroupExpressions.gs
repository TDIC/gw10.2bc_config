package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyGroupExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationGroupMenuItemExpressionsImpl extends PolicyGroupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyOverview.pcf: line 16, column 45
    function action_0 () : void {
      pcf.PolicySummary.go(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyOverview.pcf: line 19, column 51
    function action_2 () : void {
      pcf.PolicyDetailSummary.go(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyOverview.pcf: line 16, column 45
    function action_dest_1 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyOverview.pcf: line 19, column 51
    function action_dest_3 () : pcf.api.Destination {
      return pcf.PolicyDetailSummary.createDestination(plcyPeriod)
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 1) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PolicyGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyGroupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'acceleratedMenuActions' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 14, column 26
    function acceleratedMenuActions_onEnter_34 (def :  pcf.PolicyDetailAcceleratedMenuActions) : void {
      def.onEnter(plcyPeriod)
    }
    
    // 'acceleratedMenuActions' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 14, column 26
    function acceleratedMenuActions_refreshVariables_35 (def :  pcf.PolicyDetailAcceleratedMenuActions) : void {
      def.refreshVariables(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 27, column 51
    function action_10 () : void {
      pcf.PolicyDetailCharges.go(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 29, column 52
    function action_12 () : void {
      pcf.PolicyDetailPayments.go(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 31, column 55
    function action_14 () : void {
      pcf.PolicyDetailCommissions.go(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 33, column 51
    function action_16 () : void {
      pcf.PolicyDetailHistory.go(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 35, column 70
    function action_18 () : void {
      pcf.PolicyDetailTroubleTickets.go(plcyPeriod, true, true)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 38, column 72
    function action_20 () : void {
      pcf.PolicyDetailDocuments.go(plcyPeriod.Policy, plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 40, column 55
    function action_22 () : void {
      pcf.PolicyDetailNotes.go(plcyPeriod, true)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 42, column 50
    function action_24 () : void {
      pcf.PolicyDetailLedger.go(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 44, column 51
    function action_26 () : void {
      pcf.PolicyDetailJournal.go(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 46, column 57
    function action_28 () : void {
      pcf.PolicyDetailDelinquencies.go(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 48, column 45
    function action_30 () : void {
      pcf.Policy360View.go(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 50, column 58
    function action_32 () : void {
      pcf.PolicyPeriodActivitiesPage.go(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 21, column 46
    function action_4 () : void {
      pcf.PolicyOverview.go(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 23, column 52
    function action_6 () : void {
      pcf.PolicyDetailContacts.go(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 25, column 56
    function action_8 () : void {
      pcf.PolicyDetailTransactions.go(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 27, column 51
    function action_dest_11 () : pcf.api.Destination {
      return pcf.PolicyDetailCharges.createDestination(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 29, column 52
    function action_dest_13 () : pcf.api.Destination {
      return pcf.PolicyDetailPayments.createDestination(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 31, column 55
    function action_dest_15 () : pcf.api.Destination {
      return pcf.PolicyDetailCommissions.createDestination(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 33, column 51
    function action_dest_17 () : pcf.api.Destination {
      return pcf.PolicyDetailHistory.createDestination(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 35, column 70
    function action_dest_19 () : pcf.api.Destination {
      return pcf.PolicyDetailTroubleTickets.createDestination(plcyPeriod, true, true)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 38, column 72
    function action_dest_21 () : pcf.api.Destination {
      return pcf.PolicyDetailDocuments.createDestination(plcyPeriod.Policy, plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 40, column 55
    function action_dest_23 () : pcf.api.Destination {
      return pcf.PolicyDetailNotes.createDestination(plcyPeriod, true)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 42, column 50
    function action_dest_25 () : pcf.api.Destination {
      return pcf.PolicyDetailLedger.createDestination(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 44, column 51
    function action_dest_27 () : pcf.api.Destination {
      return pcf.PolicyDetailJournal.createDestination(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 46, column 57
    function action_dest_29 () : pcf.api.Destination {
      return pcf.PolicyDetailDelinquencies.createDestination(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 48, column 45
    function action_dest_31 () : pcf.api.Destination {
      return pcf.Policy360View.createDestination(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 50, column 58
    function action_dest_33 () : pcf.api.Destination {
      return pcf.PolicyPeriodActivitiesPage.createDestination(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 21, column 46
    function action_dest_5 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 23, column 52
    function action_dest_7 () : pcf.api.Destination {
      return pcf.PolicyDetailContacts.createDestination(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 25, column 56
    function action_dest_9 () : pcf.api.Destination {
      return pcf.PolicyDetailTransactions.createDestination(plcyPeriod)
    }
    
    // 'afterEnter' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 14, column 26
    function afterEnter_36 () : void {
      gw.api.web.policy.PolicyPeriodUtil.addToRecentlyViewedPolicyPeriods(plcyPeriod)
    }
    
    // 'canVisit' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 14, column 26
    static function canVisit_37 (plcyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return plcyPeriod.ViewableByCurrentUser
    }
    
    // LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 14, column 26
    static function firstVisitableChildDestinationMethod_45 (plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
      var dest : pcf.api.Destination
      dest = pcf.PolicyOverview.createDestination(plcyPeriod)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PolicyDetailContacts.createDestination(plcyPeriod)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PolicyDetailTransactions.createDestination(plcyPeriod)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PolicyDetailCharges.createDestination(plcyPeriod)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PolicyDetailPayments.createDestination(plcyPeriod)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PolicyDetailCommissions.createDestination(plcyPeriod)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PolicyDetailHistory.createDestination(plcyPeriod)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PolicyDetailTroubleTickets.createDestination(plcyPeriod, true, true)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PolicyDetailDocuments.createDestination(plcyPeriod.Policy, plcyPeriod)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PolicyDetailNotes.createDestination(plcyPeriod, true)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PolicyDetailLedger.createDestination(plcyPeriod)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PolicyDetailJournal.createDestination(plcyPeriod)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PolicyDetailDelinquencies.createDestination(plcyPeriod)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.Policy360View.createDestination(plcyPeriod)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PolicyPeriodActivitiesPage.createDestination(plcyPeriod)
      if (dest.canVisitSelf()) {
        return dest
      }
      return null
    }
    
    // 'infoBar' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 14, column 26
    function infoBar_onEnter_38 (def :  pcf.PolicyInfoBar) : void {
      def.onEnter(plcyPeriod)
    }
    
    // 'infoBar' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 14, column 26
    function infoBar_refreshVariables_39 (def :  pcf.PolicyInfoBar) : void {
      def.refreshVariables(plcyPeriod)
    }
    
    // 'menuActions' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 14, column 26
    function menuActions_onEnter_40 (def :  pcf.PolicyDetailMenuActions) : void {
      def.onEnter(plcyPeriod)
    }
    
    // 'menuActions' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 14, column 26
    function menuActions_refreshVariables_41 (def :  pcf.PolicyDetailMenuActions) : void {
      def.refreshVariables(plcyPeriod)
    }
    
    // 'parent' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 14, column 26
    static function parent_42 (plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyForward.createDestination()
    }
    
    // 'tabBar' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 14, column 26
    function tabBar_onEnter_43 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on LocationGroup (id=PolicyGroup) at PolicyGroup.pcf: line 14, column 26
    function tabBar_refreshVariables_44 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.PolicyGroup {
      return super.CurrentLocation as pcf.PolicyGroup
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 0, $arg)
    }
    
    
  }
  
  
}