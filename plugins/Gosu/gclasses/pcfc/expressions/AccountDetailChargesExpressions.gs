package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailCharges.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailChargesExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailCharges.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailChargesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'afterReturnFromPopup' attribute on Page (id=AccountDetailCharges) at AccountDetailCharges.pcf: line 12, column 72
    function afterReturnFromPopup_26 (popupCommitted :  boolean) : void {
      charges = account.getChargesRelatedToAccount( filterType )
    }
    
    // 'beforeCommit' attribute on Page (id=AccountDetailCharges) at AccountDetailCharges.pcf: line 12, column 72
    function beforeCommit_27 (pickedValue :  java.lang.Object) : void {
      validateAndExecute();
    }
    
    // 'canEdit' attribute on Page (id=AccountDetailCharges) at AccountDetailCharges.pcf: line 12, column 72
    function canEdit_28 () : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctchargesview
    }
    
    // 'canVisit' attribute on Page (id=AccountDetailCharges) at AccountDetailCharges.pcf: line 12, column 72
    static function canVisit_29 (account :  Account) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctchargesview
    }
    
    // 'initialValue' attribute on Variable at AccountDetailCharges.pcf: line 21, column 32
    function initialValue_0 () : ChargeFilterType {
      return ChargeFilterType.TC_ALL
    }
    
    // 'initialValue' attribute on Variable at AccountDetailCharges.pcf: line 25, column 31
    function initialValue_1 () : entity.Charge[] {
      return account.ChargesRelatedToAccount
    }
    
    // Page (id=AccountDetailCharges) at AccountDetailCharges.pcf: line 12, column 72
    static function parent_30 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    // 'visible' attribute on AlertBar (id=MissingArchivedCharges) at AccountDetailCharges.pcf: line 35, column 156
    function visible_2 () : java.lang.Boolean {
      return account.PolicyPeriods.hasMatch(\ policyPeriod -> policyPeriod.Archived) or account.Invoices.hasMatch(\ invoice -> invoice.Frozen)
    }
    
    override property get CurrentLocation () : pcf.AccountDetailCharges {
      return super.CurrentLocation as pcf.AccountDetailCharges
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get charges () : entity.Charge[] {
      return getVariableValue("charges", 0) as entity.Charge[]
    }
    
    property set charges ($arg :  entity.Charge[]) {
      setVariableValue("charges", 0, $arg)
    }
    
    property get filterType () : ChargeFilterType {
      return getVariableValue("filterType", 0) as ChargeFilterType
    }
    
    property set filterType ($arg :  ChargeFilterType) {
      setVariableValue("filterType", 0, $arg)
    }
    
    property get targetInvoice () : Invoice {
      return getVariableValue("targetInvoice", 0) as Invoice
    }
    
    property set targetInvoice ($arg :  Invoice) {
      setVariableValue("targetInvoice", 0, $arg)
    }
    
    function validateAndExecute() {
      gw.api.web.account.AccountUtil.validateInvoiceItems(account);
     }
    
    function allInvoiceItemsFromCharges( fromCharges : Charge[] ) : InvoiceItem[] {
      var allItems = fromCharges.flatMap( \ eachCharge -> eachCharge.AllInvoiceItems )
      return gw.api.web.invoice.InvoiceItems.withoutOffsetsOrCommissionRemainder(allItems)?.toTypedArray()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailCharges.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailChargesListDetailPanelExpressionsImpl extends AccountDetailChargesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=EditInvoiceItems) at AccountDetailCharges.pcf: line 101, column 63
    function action_17 () : void {
      EditInvoiceItemsPopup.push(charge)
    }
    
    // 'action' attribute on ToolbarButton (id=EditInvoiceItems) at AccountDetailCharges.pcf: line 101, column 63
    function action_dest_18 () : pcf.api.Destination {
      return pcf.EditInvoiceItemsPopup.createDestination(charge)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=EditHolds) at AccountDetailCharges.pcf: line 64, column 99
    function allCheckedRowsAction_12 (CheckedValues :  entity.Charge[], CheckedValuesErrors :  java.util.Map) : void {
      ChargeHoldsPopup.push(CheckedValues)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=MoveInvoiceItems) at AccountDetailCharges.pcf: line 108, column 63
    function allCheckedRowsAction_20 (CheckedValues :  entity.InvoiceItem[], CheckedValuesErrors :  java.util.Map) : void {
      gw.api.web.invoice.InvoiceUtil.validateItemsForMove(CheckedValues); MoveInvoiceItemsPopup.push(CheckedValues, CheckedValues[0].Payer)
    }
    
    // 'available' attribute on ToolbarButton (id=EditInvoiceItems) at AccountDetailCharges.pcf: line 101, column 63
    function available_15 () : java.lang.Boolean {
      return !charge.Reversed && !charge.Reversal
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=ReverseButton) at AccountDetailCharges.pcf: line 58, column 80
    function checkedRowAction_10 (element :  entity.Charge, CheckedValue :  entity.Charge) : void {
      NewChargeReversalConfirmationPopup.push(CheckedValue)
    }
    
    // 'def' attribute on PanelRef at AccountDetailCharges.pcf: line 41, column 64
    function def_onEnter_13 (def :  pcf.ChargesLV) : void {
      def.onEnter(charges, true, 0, true, false, true)
    }
    
    // 'def' attribute on PanelRef at AccountDetailCharges.pcf: line 94, column 74
    function def_onEnter_21 (def :  pcf.InvoiceItemsLV) : void {
      def.onEnter(charge.AllInvoiceItems, charge, true)
    }
    
    // 'def' attribute on PanelRef at AccountDetailCharges.pcf: line 133, column 60
    function def_onEnter_23 (def :  pcf.ChargeBreakdownItemsDisplayLV) : void {
      def.onEnter(charge)
    }
    
    // 'def' attribute on PanelRef at AccountDetailCharges.pcf: line 41, column 64
    function def_refreshVariables_14 (def :  pcf.ChargesLV) : void {
      def.refreshVariables(charges, true, 0, true, false, true)
    }
    
    // 'def' attribute on PanelRef at AccountDetailCharges.pcf: line 94, column 74
    function def_refreshVariables_22 (def :  pcf.InvoiceItemsLV) : void {
      def.refreshVariables(charge.AllInvoiceItems, charge, true)
    }
    
    // 'def' attribute on PanelRef at AccountDetailCharges.pcf: line 133, column 60
    function def_refreshVariables_24 (def :  pcf.ChargeBreakdownItemsDisplayLV) : void {
      def.refreshVariables(charge)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=FilterTypeSelector_Input) at AccountDetailCharges.pcf: line 48, column 52
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      filterType = (__VALUE_TO_SET as typekey.ChargeFilterType)
    }
    
    // 'onChange' attribute on PostOnChange at AccountDetailCharges.pcf: line 50, column 88
    function onChange_3 () : void {
      charges = account.getChargesRelatedToAccount( filterType )
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=FilterTypeSelector_Input) at AccountDetailCharges.pcf: line 48, column 52
    function valueRange_6 () : java.lang.Object {
      return typekey.ChargeFilterType.getTypeKeys(false)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=FilterTypeSelector_Input) at AccountDetailCharges.pcf: line 48, column 52
    function value_4 () : typekey.ChargeFilterType {
      return filterType
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=FilterTypeSelector_Input) at AccountDetailCharges.pcf: line 48, column 52
    function verifyValueRangeIsAllowedType_7 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=FilterTypeSelector_Input) at AccountDetailCharges.pcf: line 48, column 52
    function verifyValueRangeIsAllowedType_7 ($$arg :  typekey.ChargeFilterType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=FilterTypeSelector_Input) at AccountDetailCharges.pcf: line 48, column 52
    function verifyValueRange_8 () : void {
      var __valueRangeArg = typekey.ChargeFilterType.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_7(__valueRangeArg)
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=EditHolds) at AccountDetailCharges.pcf: line 64, column 99
    function visible_11 () : java.lang.Boolean {
      return perm.Transaction.chargeholdcreate and perm.Transaction.chargeholdrelease
    }
    
    // 'visible' attribute on ToolbarButton (id=EditInvoiceItems) at AccountDetailCharges.pcf: line 101, column 63
    function visible_16 () : java.lang.Boolean {
      return perm.System.acctchargesedit_TDIC
    }
    
    // 'visible' attribute on Card (id=ChargeBreakdownCard) at AccountDetailCharges.pcf: line 131, column 57
    function visible_25 () : java.lang.Boolean {
      return charge.BreakdownItems.HasElements
    }
    
    property get charge () : Charge {
      return getCurrentSelection(1) as Charge
    }
    
    property set charge ($arg :  Charge) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  
}