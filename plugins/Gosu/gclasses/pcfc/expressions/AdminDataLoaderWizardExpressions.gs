package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataLoaderWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataLoaderWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataLoaderWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataLoaderWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterFinish' attribute on Wizard (id=AdminDataLoaderWizard) at AdminDataLoaderWizard.pcf: line 8, column 36
    function afterFinish_10 () : void {
      AdminDataUploadConfirmation.go(processor)
    }
    
    // 'afterFinish' attribute on Wizard (id=AdminDataLoaderWizard) at AdminDataLoaderWizard.pcf: line 8, column 36
    function afterFinish_dest_11 () : pcf.api.Destination {
      return pcf.AdminDataUploadConfirmation.createDestination(processor)
    }
    
    // 'initialValue' attribute on Variable at AdminDataLoaderWizard.pcf: line 12, column 73
    function initialValue_0 () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return new tdic.util.dataloader.processor.BCAdminDataLoaderProcessor()
    }
    
    // 'onExit' attribute on WizardStep (id=upload) at AdminDataLoaderWizard.pcf: line 17, column 82
    function onExit_1 () : void {
      processor.readAndImportFile()
    }
    
    // 'onExit' attribute on WizardStep (id=review) at AdminDataLoaderWizard.pcf: line 23, column 82
    function onExit_4 () : void {
      processor.loadData()
    }
    
    // 'screen' attribute on WizardStep (id=upload) at AdminDataLoaderWizard.pcf: line 17, column 82
    function screen_onEnter_2 (def :  pcf.AdminDataUploadImportScreen) : void {
      def.onEnter(processor)
    }
    
    // 'screen' attribute on WizardStep (id=review) at AdminDataLoaderWizard.pcf: line 23, column 82
    function screen_onEnter_5 (def :  pcf.AdminDataUploadReviewScreen) : void {
      def.onEnter(processor)
    }
    
    // 'screen' attribute on WizardStep (id=upload) at AdminDataLoaderWizard.pcf: line 17, column 82
    function screen_refreshVariables_3 (def :  pcf.AdminDataUploadImportScreen) : void {
      def.refreshVariables(processor)
    }
    
    // 'screen' attribute on WizardStep (id=review) at AdminDataLoaderWizard.pcf: line 23, column 82
    function screen_refreshVariables_6 (def :  pcf.AdminDataUploadReviewScreen) : void {
      def.refreshVariables(processor)
    }
    
    // 'tabBar' attribute on Wizard (id=AdminDataLoaderWizard) at AdminDataLoaderWizard.pcf: line 8, column 36
    function tabBar_onEnter_8 (def :  pcf.InternalToolsTabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AdminDataLoaderWizard) at AdminDataLoaderWizard.pcf: line 8, column 36
    function tabBar_refreshVariables_9 (def :  pcf.InternalToolsTabBar) : void {
      def.refreshVariables()
    }
    
    // '$$wizardStepAvailable' attribute on WizardStep (id=review) at AdminDataLoaderWizard.pcf: line 23, column 82
    function wizardStepAvailable_7 () : java.lang.Boolean {
      return processor != null
    }
    
    override property get CurrentLocation () : pcf.AdminDataLoaderWizard {
      return super.CurrentLocation as pcf.AdminDataLoaderWizard
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getVariableValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setVariableValue("processor", 0, $arg)
    }
    
    
  }
  
  
}