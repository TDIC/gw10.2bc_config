package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextInput (id=PaymentAllocationPlan_Input) at AccountDetailDV.pcf: line 292, column 55
    function action_121 () : void {
      PaymentAllocationPlanDetailPopup.push(account.PaymentAllocationPlan)
    }
    
    // 'action' attribute on TextInput (id=BillingPlan_Input) at AccountDetailDV.pcf: line 303, column 43
    function action_126 () : void {
      BillingPlanDetailPopup.push(account.BillingPlan)
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at AccountDetailDV.pcf: line 394, column 46
    function action_148 () : void {
      NewPaymentInstrumentPopup.push(gw.payment.PaymentInstrumentFilters.accountDetailsPaymentInstrumentOptions,account,false)
    }
    
    // 'action' attribute on Link (id=ListBillPaymentPlansLink) at AccountDetailDV.pcf: line 416, column 89
    function action_167 () : void {
      ListBillPaymentPlansPopup.push(account)
    }
    
    // 'action' attribute on MenuItem (id=FixBillDateOrDueDateAccountLevelPopup) at AccountDetailDV.pcf: line 434, column 50
    function action_173 () : void {
      AccountLevelChargeDateSettingsPopup.push(account)
    }
    
    // 'action' attribute on MenuItem (id=BillingMethodChangerPopup) at AccountDetailDV.pcf: line 471, column 50
    function action_192 () : void {
      BillingLevelChangerPopup.push(account)
    }
    
    // 'action' attribute on RangeInput (id=DelinquencyPlan_Input) at AccountDetailDV.pcf: line 523, column 45
    function action_216 () : void {
      DelinquencyPlanDetailPopup.push(account.DelinquencyPlan)
    }
    
    // 'action' attribute on RangeInput (id=SecurityZone_Input) at AccountDetailDV.pcf: line 116, column 42
    function action_27 () : void {
      SecurityZoneDetail.go(account.SecurityZone)
    }
    
    // 'action' attribute on TextInput (id=PaymentAllocationPlan_Input) at AccountDetailDV.pcf: line 292, column 55
    function action_dest_122 () : pcf.api.Destination {
      return pcf.PaymentAllocationPlanDetailPopup.createDestination(account.PaymentAllocationPlan)
    }
    
    // 'action' attribute on TextInput (id=BillingPlan_Input) at AccountDetailDV.pcf: line 303, column 43
    function action_dest_127 () : pcf.api.Destination {
      return pcf.BillingPlanDetailPopup.createDestination(account.BillingPlan)
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at AccountDetailDV.pcf: line 394, column 46
    function action_dest_149 () : pcf.api.Destination {
      return pcf.NewPaymentInstrumentPopup.createDestination(gw.payment.PaymentInstrumentFilters.accountDetailsPaymentInstrumentOptions,account,false)
    }
    
    // 'action' attribute on Link (id=ListBillPaymentPlansLink) at AccountDetailDV.pcf: line 416, column 89
    function action_dest_168 () : pcf.api.Destination {
      return pcf.ListBillPaymentPlansPopup.createDestination(account)
    }
    
    // 'action' attribute on MenuItem (id=FixBillDateOrDueDateAccountLevelPopup) at AccountDetailDV.pcf: line 434, column 50
    function action_dest_174 () : pcf.api.Destination {
      return pcf.AccountLevelChargeDateSettingsPopup.createDestination(account)
    }
    
    // 'action' attribute on MenuItem (id=BillingMethodChangerPopup) at AccountDetailDV.pcf: line 471, column 50
    function action_dest_193 () : pcf.api.Destination {
      return pcf.BillingLevelChangerPopup.createDestination(account)
    }
    
    // 'action' attribute on RangeInput (id=DelinquencyPlan_Input) at AccountDetailDV.pcf: line 523, column 45
    function action_dest_217 () : pcf.api.Destination {
      return pcf.DelinquencyPlanDetailPopup.createDestination(account.DelinquencyPlan)
    }
    
    // 'action' attribute on RangeInput (id=SecurityZone_Input) at AccountDetailDV.pcf: line 116, column 42
    function action_dest_28 () : pcf.api.Destination {
      return pcf.SecurityZoneDetail.createDestination(account.SecurityZone)
    }
    
    // 'available' attribute on MenuItem (id=FixBillDateOrDueDateAccountLevelPopup) at AccountDetailDV.pcf: line 434, column 50
    function available_171 () : java.lang.Boolean {
      return !CurrentLocation.InEditMode
    }
    
    // 'available' attribute on MenuItem (id=BillingMethodChangerPopup) at AccountDetailDV.pcf: line 471, column 50
    function available_190 () : java.lang.Boolean {
      return !CurrentLocation.InEditMode &&  perm.System.acctdetedit
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=dueDateBilling_Input) at AccountDetailDV.pcf: line 311, column 67
    function defaultSetter_133 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.BillDateOrDueDateBilling = (__VALUE_TO_SET as typekey.BillDateOrDueDateBilling)
    }
    
    // 'value' attribute on TextInput (id=InvoiceDayOfMonth_Input) at AccountDetailDV.pcf: line 328, column 41
    function defaultSetter_139 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.InvoiceDayOfMonth = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TypeKeyInput (id=SendInvoicesBy_Input) at AccountDetailDV.pcf: line 385, column 52
    function defaultSetter_145 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.InvoiceDeliveryType = (__VALUE_TO_SET as typekey.InvoiceDeliveryMethod)
    }
    
    // 'value' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at AccountDetailDV.pcf: line 394, column 46
    function defaultSetter_152 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.DefaultPaymentInstrument = (__VALUE_TO_SET as entity.PaymentInstrument)
    }
    
    // 'value' attribute on RangeInput (id=PaymentPlan_Input) at AccountDetailDV.pcf: line 410, column 42
    function defaultSetter_160 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.ChangePolicyPaymentPlan_TDIC = (__VALUE_TO_SET as entity.PaymentPlan)
    }
    
    // 'value' attribute on RangeInput (id=DelinquencyPlan_Input) at AccountDetailDV.pcf: line 523, column 45
    function defaultSetter_219 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.DelinquencyPlan = (__VALUE_TO_SET as entity.DelinquencyPlan)
    }
    
    // 'value' attribute on RangeInput (id=CollectionAgency_Input) at AccountDetailDV.pcf: line 531, column 58
    function defaultSetter_228 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.CollectionAgency = (__VALUE_TO_SET as entity.CollectionAgency)
    }
    
    // 'value' attribute on RangeInput (id=SecurityZone_Input) at AccountDetailDV.pcf: line 116, column 42
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.SecurityZone = (__VALUE_TO_SET as entity.SecurityZone)
    }
    
    // 'editable' attribute on RangeInput (id=DelinquencyPlan_Input) at AccountDetailDV.pcf: line 523, column 45
    function editable_215 () : java.lang.Boolean {
      return perm.System.deliqedit_TDIC
    }
    
    // 'initialValue' attribute on Variable at AccountDetailDV.pcf: line 16, column 57
    function initialValue_0 () : gw.api.web.account.AccountSearchConverter {
      return new gw.api.web.account.AccountSearchConverter()
    }
    
    // 'initialValue' attribute on Variable at AccountDetailDV.pcf: line 20, column 68
    function initialValue_1 () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return gw.api.database.Query.make(SecurityZone).select()
    }
    
    // 'initialValue' attribute on Variable at AccountDetailDV.pcf: line 25, column 49
    function initialValue_2 () : gw.payment.PaymentInstrumentRange {
      return new gw.payment.PaymentInstrumentRange(account.PaymentInstruments)
    }
    
    // 'initialValue' attribute on Variable at AccountDetailDV.pcf: line 29, column 45
    function initialValue_3 () : gw.pl.currency.MonetaryAmount {
      return account.DelinquentAmount
    }
    
    // 'initialValue' attribute on Variable at AccountDetailDV.pcf: line 33, column 33
    function initialValue_4 () : java.lang.Boolean {
      return account.BillingLevel == BillingLevel.TC_ACCOUNT
    }
    
    // 'initialValue' attribute on Variable at AccountDetailDV.pcf: line 37, column 33
    function initialValue_5 () : java.lang.Boolean {
      return account.BillingLevel != BillingLevel.TC_POLICYDESIGNATEDUNAPPLIED
    }
    
    // 'initialValue' attribute on Variable at AccountDetailDV.pcf: line 41, column 64
    function initialValue_6 () : gw.util.IOrderedList<entity.UnappliedFund> {
      return account.UnappliedFunds.where(\uf -> uf.Balance.Amount != 0).orderBy(\uf -> uf.DisplayName)
    }
    
    // 'onPick' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at AccountDetailDV.pcf: line 394, column 46
    function onPick_150 (PickedValue :  PaymentInstrument) : void {
      paymentInstrumentRange.addPaymentInstrument(account.DefaultPaymentInstrument)
    }
    
    // 'validationExpression' attribute on TextInput (id=InvoiceDayOfMonth_Input) at AccountDetailDV.pcf: line 328, column 41
    function validationExpression_137 () : java.lang.Object {
      return account.InvoiceDayOfMonth != null and account.InvoiceDayOfMonth > 0 and account.InvoiceDayOfMonth <= 31 ? null : DisplayKey.get("Java.Account.InvoiceDayOfMonth.ValidationError")
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at AccountDetailDV.pcf: line 394, column 46
    function valueRange_154 () : java.lang.Object {
      return gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, gw.payment.PaymentInstrumentFilters.accountDetailsPaymentInstrumentFilter)
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at AccountDetailDV.pcf: line 410, column 42
    function valueRange_162 () : java.lang.Object {
      return AvailablePaymentPlans
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at AccountDetailDV.pcf: line 523, column 45
    function valueRange_221 () : java.lang.Object {
      return account.getApplicableDelinquencyPlans()
    }
    
    // 'valueRange' attribute on RangeInput (id=CollectionAgency_Input) at AccountDetailDV.pcf: line 531, column 58
    function valueRange_230 () : java.lang.Object {
      return gw.api.database.Query.make(CollectionAgency).select()
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at AccountDetailDV.pcf: line 116, column 42
    function valueRange_32 () : java.lang.Object {
      return allSecurityZones
    }
    
    // 'value' attribute on TextInput (id=PaymentAllocationPlan_Input) at AccountDetailDV.pcf: line 292, column 55
    function valueRoot_124 () : java.lang.Object {
      return account.PaymentAllocationPlan
    }
    
    // 'value' attribute on TextInput (id=BillingPlan_Input) at AccountDetailDV.pcf: line 303, column 43
    function valueRoot_129 () : java.lang.Object {
      return account.BillingPlan
    }
    
    // 'value' attribute on TextInput (id=PrimaryContact_Input) at AccountDetailDV.pcf: line 123, column 51
    function valueRoot_37 () : java.lang.Object {
      return account.PrimaryPayer
    }
    
    // 'value' attribute on TextInput (id=ADANumber_Input) at AccountDetailDV.pcf: line 132, column 82
    function valueRoot_44 () : java.lang.Object {
      return account.PrimaryPayer.Contact
    }
    
    // 'value' attribute on MonetaryAmountInput (id=AccountUnbilledAmount_Input) at AccountDetailDV.pcf: line 162, column 57
    function valueRoot_59 () : java.lang.Object {
      return summaryFinancialsHelper
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at AccountDetailDV.pcf: line 48, column 40
    function valueRoot_8 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at AccountDetailDV.pcf: line 52, column 47
    function value_10 () : java.lang.String {
      return account.AccountNameLocalized
    }
    
    // 'value' attribute on InputIterator (id=unappliedFunds_TDIC) at AccountDetailDV.pcf: line 236, column 70
    function value_104 () : gw.util.IOrderedList<entity.UnappliedFund> {
      return unappliedFunds_TDIC
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at AccountDetailDV.pcf: line 252, column 46
    function value_106 () : gw.pl.currency.MonetaryAmount {
      return account.TotalUnappliedAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=SuspenseAmount_Input) at AccountDetailDV.pcf: line 269, column 41
    function value_117 () : gw.pl.currency.MonetaryAmount {
      return account.SuspenseAmount
    }
    
    // 'value' attribute on TextInput (id=PaymentAllocationPlan_Input) at AccountDetailDV.pcf: line 292, column 55
    function value_123 () : java.lang.String {
      return account.PaymentAllocationPlan.Name
    }
    
    // 'value' attribute on TextInput (id=BillingPlan_Input) at AccountDetailDV.pcf: line 303, column 43
    function value_128 () : java.lang.String {
      return account.BillingPlan.Name
    }
    
    // 'value' attribute on TypeKeyInput (id=AccountType_Input) at AccountDetailDV.pcf: line 57, column 42
    function value_13 () : typekey.AccountType {
      return account.AccountType
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=dueDateBilling_Input) at AccountDetailDV.pcf: line 311, column 67
    function value_132 () : typekey.BillDateOrDueDateBilling {
      return account.BillDateOrDueDateBilling
    }
    
    // 'value' attribute on TextInput (id=InvoiceDayOfMonth_Input) at AccountDetailDV.pcf: line 328, column 41
    function value_138 () : java.lang.Integer {
      return account.InvoiceDayOfMonth
    }
    
    // 'value' attribute on TypeKeyInput (id=SendInvoicesBy_Input) at AccountDetailDV.pcf: line 385, column 52
    function value_144 () : typekey.InvoiceDeliveryMethod {
      return account.InvoiceDeliveryType
    }
    
    // 'value' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at AccountDetailDV.pcf: line 394, column 46
    function value_151 () : entity.PaymentInstrument {
      return account.DefaultPaymentInstrument
    }
    
    // 'value' attribute on RangeInput (id=PaymentPlan_Input) at AccountDetailDV.pcf: line 410, column 42
    function value_159 () : entity.PaymentPlan {
      return account.ChangePolicyPaymentPlan_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at AccountDetailDV.pcf: line 81, column 39
    function value_16 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on TextInput (id=FixBill_Input) at AccountDetailDV.pcf: line 427, column 67
    function value_176 () : java.lang.Object {
      return null
    }
    
    // 'value' attribute on DateInput (id=CreationDate_Input) at AccountDetailDV.pcf: line 96, column 37
    function value_19 () : java.util.Date {
      return account.CreateTime
    }
    
    // 'value' attribute on MonetaryAmountInput (id=WriteoffAmount_Input) at AccountDetailDV.pcf: line 504, column 59
    function value_208 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.WrittenOffAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=DelinquentAmount_Input) at AccountDetailDV.pcf: line 514, column 34
    function value_212 () : gw.pl.currency.MonetaryAmount {
      return policiesPastDue
    }
    
    // 'value' attribute on RangeInput (id=DelinquencyPlan_Input) at AccountDetailDV.pcf: line 523, column 45
    function value_218 () : entity.DelinquencyPlan {
      return account.DelinquencyPlan
    }
    
    // 'value' attribute on RangeInput (id=CollectionAgency_Input) at AccountDetailDV.pcf: line 531, column 58
    function value_227 () : entity.CollectionAgency {
      return account.CollectionAgency
    }
    
    // 'value' attribute on DateInput (id=CloseDate_Input) at AccountDetailDV.pcf: line 101, column 35
    function value_23 () : java.util.Date {
      return account.CloseDate
    }
    
    // 'value' attribute on RangeInput (id=SecurityZone_Input) at AccountDetailDV.pcf: line 116, column 42
    function value_29 () : entity.SecurityZone {
      return account.SecurityZone
    }
    
    // 'value' attribute on TextInput (id=PrimaryContact_Input) at AccountDetailDV.pcf: line 123, column 51
    function value_36 () : java.lang.String {
      return account.PrimaryPayer.DisplayName
    }
    
    // 'value' attribute on TextInput (id=FEIN_Input) at AccountDetailDV.pcf: line 127, column 31
    function value_39 () : java.lang.String {
      return account.FEIN
    }
    
    // 'value' attribute on TextInput (id=ADANumber_Input) at AccountDetailDV.pcf: line 132, column 82
    function value_43 () : java.lang.String {
      return account.PrimaryPayer.Contact.ADANumberOfficialID_TDIC
    }
    
    // 'value' attribute on TextInput (id=Address_Input) at AccountDetailDV.pcf: line 136, column 124
    function value_47 () : java.lang.String {
      return new gw.api.address.AddressFormatter().format(account.PrimaryPayer.Contact.PrimaryAddress, "\n")
    }
    
    // 'value' attribute on TextInput (id=Phone_Input) at AccountDetailDV.pcf: line 140, column 62
    function value_49 () : java.lang.String {
      return account.PrimaryPayer.Contact.WorkPhoneValue
    }
    
    // 'value' attribute on TextInput (id=Email_Input) at AccountDetailDV.pcf: line 144, column 61
    function value_52 () : java.lang.String {
      return account.PrimaryPayer.Contact.EmailAddress1
    }
    
    // 'value' attribute on TextInput (id=OrgType_Input) at AccountDetailDV.pcf: line 151, column 43
    function value_55 () : java.lang.String {
      return account.OrganizationType
    }
    
    // 'value' attribute on MonetaryAmountInput (id=AccountUnbilledAmount_Input) at AccountDetailDV.pcf: line 162, column 57
    function value_58 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.UnbilledAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=AccountCurrentAmountBilled_Input) at AccountDetailDV.pcf: line 169, column 55
    function value_62 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.BilledAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=AccountPastDueAmount_Input) at AccountDetailDV.pcf: line 176, column 59
    function value_66 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.DelinquentAmount
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at AccountDetailDV.pcf: line 48, column 40
    function value_7 () : java.lang.String {
      return account.AccountNumber
    }
    
    // 'value' attribute on MonetaryAmountInput (id=AccountTotalOutstanding_Input) at AccountDetailDV.pcf: line 183, column 60
    function value_70 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.OutstandingAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PoliciesUnbilledAmount_Input) at AccountDetailDV.pcf: line 193, column 65
    function value_74 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.PoliciesUnbilledAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PoliciesCurrentAmountDue_Input) at AccountDetailDV.pcf: line 200, column 63
    function value_78 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.PoliciesBilledAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PoliciesPastDueAmount_Input) at AccountDetailDV.pcf: line 207, column 67
    function value_82 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.PoliciesDelinquentAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PoliciesTotalOutstanding_Input) at AccountDetailDV.pcf: line 214, column 68
    function value_86 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.PoliciesOutstandingAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PoliciesPaidAmount_Input) at AccountDetailDV.pcf: line 221, column 61
    function value_90 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.PoliciesPaidAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PoliciesTotalValue_Input) at AccountDetailDV.pcf: line 228, column 61
    function value_94 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.PoliciesTotalValue
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at AccountDetailDV.pcf: line 394, column 46
    function verifyValueRangeIsAllowedType_155 ($$arg :  entity.PaymentInstrument[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at AccountDetailDV.pcf: line 394, column 46
    function verifyValueRangeIsAllowedType_155 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentInstrument>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at AccountDetailDV.pcf: line 394, column 46
    function verifyValueRangeIsAllowedType_155 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at AccountDetailDV.pcf: line 410, column 42
    function verifyValueRangeIsAllowedType_163 ($$arg :  entity.PaymentPlan[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at AccountDetailDV.pcf: line 410, column 42
    function verifyValueRangeIsAllowedType_163 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentPlan>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at AccountDetailDV.pcf: line 410, column 42
    function verifyValueRangeIsAllowedType_163 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at AccountDetailDV.pcf: line 523, column 45
    function verifyValueRangeIsAllowedType_222 ($$arg :  entity.DelinquencyPlan[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at AccountDetailDV.pcf: line 523, column 45
    function verifyValueRangeIsAllowedType_222 ($$arg :  gw.api.database.IQueryBeanResult<entity.DelinquencyPlan>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at AccountDetailDV.pcf: line 523, column 45
    function verifyValueRangeIsAllowedType_222 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CollectionAgency_Input) at AccountDetailDV.pcf: line 531, column 58
    function verifyValueRangeIsAllowedType_231 ($$arg :  entity.CollectionAgency[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CollectionAgency_Input) at AccountDetailDV.pcf: line 531, column 58
    function verifyValueRangeIsAllowedType_231 ($$arg :  gw.api.database.IQueryBeanResult<entity.CollectionAgency>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CollectionAgency_Input) at AccountDetailDV.pcf: line 531, column 58
    function verifyValueRangeIsAllowedType_231 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at AccountDetailDV.pcf: line 116, column 42
    function verifyValueRangeIsAllowedType_33 ($$arg :  entity.SecurityZone[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at AccountDetailDV.pcf: line 116, column 42
    function verifyValueRangeIsAllowedType_33 ($$arg :  gw.api.database.IQueryBeanResult<entity.SecurityZone>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at AccountDetailDV.pcf: line 116, column 42
    function verifyValueRangeIsAllowedType_33 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at AccountDetailDV.pcf: line 394, column 46
    function verifyValueRange_156 () : void {
      var __valueRangeArg = gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, gw.payment.PaymentInstrumentFilters.accountDetailsPaymentInstrumentFilter)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_155(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at AccountDetailDV.pcf: line 410, column 42
    function verifyValueRange_164 () : void {
      var __valueRangeArg = AvailablePaymentPlans
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_163(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at AccountDetailDV.pcf: line 523, column 45
    function verifyValueRange_223 () : void {
      var __valueRangeArg = account.getApplicableDelinquencyPlans()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_222(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=CollectionAgency_Input) at AccountDetailDV.pcf: line 531, column 58
    function verifyValueRange_232 () : void {
      var __valueRangeArg = gw.api.database.Query.make(CollectionAgency).select()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_231(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at AccountDetailDV.pcf: line 116, column 42
    function verifyValueRange_34 () : void {
      var __valueRangeArg = allSecurityZones
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_33(__valueRangeArg)
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at AccountDetailDV.pcf: line 252, column 46
    function visible_105 () : java.lang.Boolean {
      return unappliedFunds_TDIC.Empty
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=TotalUnappliedAmount_TDIC_Input) at AccountDetailDV.pcf: line 262, column 52
    function visible_111 () : java.lang.Boolean {
      return unappliedFunds_TDIC.HasElements
    }
    
    // 'visible' attribute on RangeInput (id=PaymentPlan_Input) at AccountDetailDV.pcf: line 410, column 42
    function visible_158 () : java.lang.Boolean {
      return showPaymentPlanMenu()
    }
    
    // 'visible' attribute on ContentInput at AccountDetailDV.pcf: line 412, column 64
    function visible_169 () : java.lang.Boolean {
      return account.AccountType==AccountType.TC_LISTBILL
    }
    
    // 'visible' attribute on InputDivider at AccountDetailDV.pcf: line 419, column 68
    function visible_170 () : java.lang.Boolean {
      return account.BillingLevel != BillingLevel.TC_ACCOUNT
    }
    
    // 'visible' attribute on InputDivider at AccountDetailDV.pcf: line 455, column 42
    function visible_189 () : java.lang.Boolean {
      return !account.isListBill()
    }
    
    // 'visible' attribute on DateInput (id=CloseDate_Input) at AccountDetailDV.pcf: line 101, column 35
    function visible_22 () : java.lang.Boolean {
      return account.Closed
    }
    
    // 'visible' attribute on RangeInput (id=CollectionAgency_Input) at AccountDetailDV.pcf: line 531, column 58
    function visible_226 () : java.lang.Boolean {
      return account.hasActiveDelinquencyProcess()
    }
    
    // 'visible' attribute on TextInput (id=ADANumber_Input) at AccountDetailDV.pcf: line 132, column 82
    function visible_42 () : java.lang.Boolean {
      return account.PrimaryPayer.Contact.ADANumberOfficialID_TDIC != null
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get accountSearchConverter () : gw.api.web.account.AccountSearchConverter {
      return getVariableValue("accountSearchConverter", 0) as gw.api.web.account.AccountSearchConverter
    }
    
    property set accountSearchConverter ($arg :  gw.api.web.account.AccountSearchConverter) {
      setVariableValue("accountSearchConverter", 0, $arg)
    }
    
    property get allSecurityZones () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return getVariableValue("allSecurityZones", 0) as gw.api.database.IQueryBeanResult<SecurityZone>
    }
    
    property set allSecurityZones ($arg :  gw.api.database.IQueryBeanResult<SecurityZone>) {
      setVariableValue("allSecurityZones", 0, $arg)
    }
    
    property get invoiceBy () : java.lang.Boolean {
      return getVariableValue("invoiceBy", 0) as java.lang.Boolean
    }
    
    property set invoiceBy ($arg :  java.lang.Boolean) {
      setVariableValue("invoiceBy", 0, $arg)
    }
    
    property get paymentInstrumentRange () : gw.payment.PaymentInstrumentRange {
      return getVariableValue("paymentInstrumentRange", 0) as gw.payment.PaymentInstrumentRange
    }
    
    property set paymentInstrumentRange ($arg :  gw.payment.PaymentInstrumentRange) {
      setVariableValue("paymentInstrumentRange", 0, $arg)
    }
    
    property get policiesPastDue () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("policiesPastDue", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set policiesPastDue ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("policiesPastDue", 0, $arg)
    }
    
    property get separateIncomingFundsBy () : java.lang.Boolean {
      return getVariableValue("separateIncomingFundsBy", 0) as java.lang.Boolean
    }
    
    property set separateIncomingFundsBy ($arg :  java.lang.Boolean) {
      setVariableValue("separateIncomingFundsBy", 0, $arg)
    }
    
    property get summaryFinancialsHelper () : gw.web.account.AccountSummaryFinancialsHelper {
      return getRequireValue("summaryFinancialsHelper", 0) as gw.web.account.AccountSummaryFinancialsHelper
    }
    
    property set summaryFinancialsHelper ($arg :  gw.web.account.AccountSummaryFinancialsHelper) {
      setRequireValue("summaryFinancialsHelper", 0, $arg)
    }
    
    property get unappliedFunds_TDIC () : gw.util.IOrderedList<entity.UnappliedFund> {
      return getVariableValue("unappliedFunds_TDIC", 0) as gw.util.IOrderedList<entity.UnappliedFund>
    }
    
    property set unappliedFunds_TDIC ($arg :  gw.util.IOrderedList<entity.UnappliedFund>) {
      setVariableValue("unappliedFunds_TDIC", 0, $arg)
    }
    
    function updateBillingLevel() : String {
      if (invoiceBy) {
        account.BillingLevel = BillingLevel.TC_ACCOUNT
        separateIncomingFundsBy = true
      } else if (separateIncomingFundsBy) {
        account.BillingLevel = BillingLevel.TC_POLICYDEFAULTUNAPPLIED
      } else {
        account.BillingLevel = BillingLevel.TC_POLICYDESIGNATEDUNAPPLIED
      }
      return null;
    }
    
    /**
     * Vicente, 08/21/2014, US65: Returns true if the Default Payment Instrument is changed to a non ACH/EFT Payment Instrument 
     * from an ACH/EFT Payment Instrument
     **/
    function showPaymentPlanMenu() : boolean {
      var previousPaymentInstrument = (account.getOriginalValue("DefaultPaymentInstrument") as Acctpmntinst).ForeignEntity
      if (account.DefaultPaymentInstrument.PaymentMethod != typekey.PaymentMethod.TC_ACH
          and (previousPaymentInstrument.PaymentMethod == typekey.PaymentMethod.TC_ACH)) {
        return true
      }
      else {
        return false
      }
    }
    
    /**
     * Vicente, 08/21/2014, US65: Get the list of all the available Payment Plans but the "TDIC Monthly APW" 
     **/
    property get AvailablePaymentPlans() : java.util.List<PaymentPlan> {
      //Check if all Payment Plans overlap with all the policy periods of the account
      var paymentPlanQuery = gw.api.database.Query.make(entity.PaymentPlan)
      //paymentPlanQuery.compare(entity.PaymentPlan#Currency, Equals, Currency.TC_USD)
      paymentPlanQuery.compare(entity.PaymentPlan#UserVisible, Equals, true)
      var allPaymentPlans = paymentPlanQuery.select()
    
      return allPaymentPlans.where( \ paymentP -> paymentP.Name != "TDIC Monthly APW")
          .where( \ paymentP -> account.AllPolicyPeriods.allMatch( \ policyPeriod -> policyPeriod.ExpirationDate >= paymentP.EffectiveDate
              && (paymentP.ExpirationDate == null || policyPeriod.EffectiveDate <= paymentP.ExpirationDate)))
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountDetailDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at AccountDetailDV.pcf: line 243, column 42
    function currency_101 () : typekey.Currency {
      return unappliedFund.Currency
    }
    
    // 'label' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at AccountDetailDV.pcf: line 243, column 42
    function label_98 () : java.lang.Object {
      return unappliedFund
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at AccountDetailDV.pcf: line 243, column 42
    function value_99 () : gw.pl.currency.MonetaryAmount {
      return unappliedFund.Balance
    }
    
    property get unappliedFund () : entity.UnappliedFund {
      return getIteratedValue(1) as entity.UnappliedFund
    }
    
    
  }
  
  
}