package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/BatchInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BatchInfoDVExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/BatchInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BatchInfoDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=BatchAmount_Input) at BatchInfoDV.pcf: line 31, column 24
    function currency_12 () : typekey.Currency {
      return batchPaymentDetailsView.BatchPayment.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=BatchAmount_Input) at BatchInfoDV.pcf: line 31, column 24
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      batchPaymentDetailsView.BatchPayment.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'valueRange' attribute on RangeInput (id=BatchStatus_Input) at BatchInfoDV.pcf: line 22, column 25
    function valueRange_5 () : java.lang.Object {
      return BatchPaymentsStatus.getTypeKeys(false)
    }
    
    // 'value' attribute on TextInput (id=BatchNumber_Input) at BatchInfoDV.pcf: line 15, column 25
    function valueRoot_1 () : java.lang.Object {
      return batchPaymentDetailsView.BatchPayment
    }
    
    // 'value' attribute on TextInput (id=BatchNumber_Input) at BatchInfoDV.pcf: line 15, column 25
    function value_0 () : java.lang.String {
      return batchPaymentDetailsView.BatchPayment.BatchNumber
    }
    
    // 'value' attribute on MonetaryAmountInput (id=RemainingAmount_Input) at BatchInfoDV.pcf: line 39, column 25
    function value_14 () : gw.pl.currency.MonetaryAmount {
      return batchPaymentDetailsView.BatchPayment.RemainingAmount
    }
    
    // 'value' attribute on DateInput (id=Created_Input) at BatchInfoDV.pcf: line 44, column 115
    function value_18 () : java.util.Date {
      return batchPaymentDetailsView.BatchPayment.CreateTime
    }
    
    // 'value' attribute on TextInput (id=CreatedBy_Input) at BatchInfoDV.pcf: line 50, column 115
    function value_23 () : User {
      return batchPaymentDetailsView.BatchPayment.CreateUser
    }
    
    // 'value' attribute on DateInput (id=LastEdited_Input) at BatchInfoDV.pcf: line 55, column 115
    function value_28 () : java.util.Date {
      return batchPaymentDetailsView.BatchPayment.UpdateTime
    }
    
    // 'value' attribute on RangeInput (id=BatchStatus_Input) at BatchInfoDV.pcf: line 22, column 25
    function value_3 () : BatchPaymentsStatus {
      return batchPaymentDetailsView.BatchPayment.BatchStatus
    }
    
    // 'value' attribute on TextInput (id=LastEditedBy_Input) at BatchInfoDV.pcf: line 61, column 115
    function value_33 () : User {
      return batchPaymentDetailsView.BatchPayment.UpdateUser
    }
    
    // 'value' attribute on DateInput (id=PostedDate_Input) at BatchInfoDV.pcf: line 66, column 51
    function value_38 () : java.util.Date {
      return batchPaymentDetailsView.BatchPayment.PostedDate
    }
    
    // 'value' attribute on TextInput (id=PostedBy_Input) at BatchInfoDV.pcf: line 72, column 51
    function value_43 () : User {
      return batchPaymentDetailsView.BatchPayment.PostedBy
    }
    
    // 'value' attribute on DateInput (id=ReversalDate_Input) at BatchInfoDV.pcf: line 77, column 53
    function value_48 () : java.util.Date {
      return batchPaymentDetailsView.BatchPayment.ReversalDate
    }
    
    // 'value' attribute on TextInput (id=ReversedBy_Input) at BatchInfoDV.pcf: line 83, column 53
    function value_53 () : User {
      return batchPaymentDetailsView.BatchPayment.ReversedByUser
    }
    
    // 'value' attribute on MonetaryAmountInput (id=BatchAmount_Input) at BatchInfoDV.pcf: line 31, column 24
    function value_9 () : gw.pl.currency.MonetaryAmount {
      return batchPaymentDetailsView.BatchPayment.Amount
    }
    
    // 'valueRange' attribute on RangeInput (id=BatchStatus_Input) at BatchInfoDV.pcf: line 22, column 25
    function verifyValueRangeIsAllowedType_6 ($$arg :  BatchPaymentsStatus[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BatchStatus_Input) at BatchInfoDV.pcf: line 22, column 25
    function verifyValueRangeIsAllowedType_6 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BatchStatus_Input) at BatchInfoDV.pcf: line 22, column 25
    function verifyValueRangeIsAllowedType_6 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BatchStatus_Input) at BatchInfoDV.pcf: line 22, column 25
    function verifyValueRange_7 () : void {
      var __valueRangeArg = BatchPaymentsStatus.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_6(__valueRangeArg)
    }
    
    // 'visible' attribute on DateInput (id=Created_Input) at BatchInfoDV.pcf: line 44, column 115
    function visible_17 () : java.lang.Boolean {
      return batchPaymentDetailsView.Mode == gw.web.payment.batch.BatchPaymentDetailsView.Mode.MODIFICATION
    }
    
    // 'visible' attribute on DateInput (id=PostedDate_Input) at BatchInfoDV.pcf: line 66, column 51
    function visible_37 () : java.lang.Boolean {
      return batchPaymentDetailsView.Posted
    }
    
    // 'visible' attribute on DateInput (id=ReversalDate_Input) at BatchInfoDV.pcf: line 77, column 53
    function visible_47 () : java.lang.Boolean {
      return batchPaymentDetailsView.Reversed
    }
    
    property get batchPaymentDetailsView () : gw.web.payment.batch.BatchPaymentDetailsView {
      return getRequireValue("batchPaymentDetailsView", 0) as gw.web.payment.batch.BatchPaymentDetailsView
    }
    
    property set batchPaymentDetailsView ($arg :  gw.web.payment.batch.BatchPaymentDetailsView) {
      setRequireValue("batchPaymentDetailsView", 0, $arg)
    }
    
    
  }
  
  
}