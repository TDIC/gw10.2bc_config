package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyItemCreditsInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyItemCreditsInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyItemCreditsInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyItemCreditsInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=PaidAmount_Input) at AgencyItemCreditsInputSet.pcf: line 16, column 39
    function currency_2 () : typekey.Currency {
      return invoiceItem.Currency
    }
    
    // 'outputConversion' attribute on TextInput (id=CommissionPercentage_Input) at AgencyItemCreditsInputSet.pcf: line 30, column 41
    function outputConversion_8 (VALUE :  java.math.BigDecimal) : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(VALUE, 3)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PaidAmount_Input) at AgencyItemCreditsInputSet.pcf: line 16, column 39
    function valueRoot_1 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PaidAmount_Input) at AgencyItemCreditsInputSet.pcf: line 16, column 39
    function value_0 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.PaidAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=NetPaidAmount_Input) at AgencyItemCreditsInputSet.pcf: line 37, column 60
    function value_11 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.NetAmountPaidForPrimaryProducer
    }
    
    // 'value' attribute on MonetaryAmountInput (id=AgencyBillRetained_Input) at AgencyItemCreditsInputSet.pcf: line 23, column 54
    function value_4 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.PrimaryAgencyBillRetained
    }
    
    // 'value' attribute on TextInput (id=CommissionPercentage_Input) at AgencyItemCreditsInputSet.pcf: line 30, column 41
    function value_9 () : java.math.BigDecimal {
      return invoiceItem.PrimaryAgencyBillRetained.percentageOfAsBigDecimal(invoiceItem.PaidAmount)
    }
    
    property get invoiceItem () : InvoiceItem {
      return getRequireValue("invoiceItem", 0) as InvoiceItem
    }
    
    property set invoiceItem ($arg :  InvoiceItem) {
      setRequireValue("invoiceItem", 0, $arg)
    }
    
    
  }
  
  
}