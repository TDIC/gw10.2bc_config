package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultiplePolicyPeriodsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SelectMultiplePolicyPeriodsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultiplePolicyPeriodsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SelectMultiplePolicyPeriodsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policySearchCriteria :  gw.search.PolicySearchCriteria) : int {
      return 0
    }
    
    // 'def' attribute on ScreenRef at SelectMultiplePolicyPeriodsPopup.pcf: line 15, column 70
    function def_onEnter_0 (def :  pcf.SelectMultiplePolicyPeriodsScreen) : void {
      def.onEnter(policySearchCriteria)
    }
    
    // 'def' attribute on ScreenRef at SelectMultiplePolicyPeriodsPopup.pcf: line 15, column 70
    function def_refreshVariables_1 (def :  pcf.SelectMultiplePolicyPeriodsScreen) : void {
      def.refreshVariables(policySearchCriteria)
    }
    
    override property get CurrentLocation () : pcf.SelectMultiplePolicyPeriodsPopup {
      return super.CurrentLocation as pcf.SelectMultiplePolicyPeriodsPopup
    }
    
    property get policySearchCriteria () : gw.search.PolicySearchCriteria {
      return getVariableValue("policySearchCriteria", 0) as gw.search.PolicySearchCriteria
    }
    
    property set policySearchCriteria ($arg :  gw.search.PolicySearchCriteria) {
      setVariableValue("policySearchCriteria", 0, $arg)
    }
    
    
  }
  
  
}