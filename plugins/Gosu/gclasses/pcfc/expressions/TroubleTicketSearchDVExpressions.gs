package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/TroubleTicketSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TroubleTicketSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/TroubleTicketSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TroubleTicketSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerInput (id=PolicyNumberCriterion_Input) at TroubleTicketSearchDV.pcf: line 34, column 39
    function action_11 () : void {
      PolicySearchPopup.push(true)
    }
    
    // 'pickLocation' attribute on PickerInput (id=ProducerNameCriterion_Input) at TroubleTicketSearchDV.pcf: line 42, column 39
    function action_18 () : void {
      ProducerSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=ProducerNameKanjiCriterion_Input) at TroubleTicketSearchDV.pcf: line 51, column 84
    function action_27 () : void {
      ProducerSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountNumberCriterion_Input) at TroubleTicketSearchDV.pcf: line 26, column 39
    function action_4 () : void {
      AccountSearchPopup.push()
    }
    
    // MenuItem (id=UserBrowseMenuItem) at TroubleTicketSearchDV.pcf: line 68, column 48
    function action_44 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=PolicyNumberCriterion_Input) at TroubleTicketSearchDV.pcf: line 34, column 39
    function action_dest_12 () : pcf.api.Destination {
      return pcf.PolicySearchPopup.createDestination(true)
    }
    
    // 'pickLocation' attribute on PickerInput (id=ProducerNameCriterion_Input) at TroubleTicketSearchDV.pcf: line 42, column 39
    function action_dest_19 () : pcf.api.Destination {
      return pcf.ProducerSearchPopup.createDestination()
    }
    
    // 'pickLocation' attribute on PickerInput (id=ProducerNameKanjiCriterion_Input) at TroubleTicketSearchDV.pcf: line 51, column 84
    function action_dest_28 () : pcf.api.Destination {
      return pcf.ProducerSearchPopup.createDestination()
    }
    
    // MenuItem (id=UserBrowseMenuItem) at TroubleTicketSearchDV.pcf: line 68, column 48
    function action_dest_45 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountNumberCriterion_Input) at TroubleTicketSearchDV.pcf: line 26, column 39
    function action_dest_5 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'conversionExpression' attribute on PickerInput (id=PolicyNumberCriterion_Input) at TroubleTicketSearchDV.pcf: line 34, column 39
    function conversionExpression_13 (PickedValue :  PolicyPeriod) : java.lang.String {
      return PickedValue.DisplayName
    }
    
    // 'conversionExpression' attribute on PickerInput (id=ProducerNameCriterion_Input) at TroubleTicketSearchDV.pcf: line 42, column 39
    function conversionExpression_21 (PickedValue :  Producer) : java.lang.String {
      return PickedValue.Name
    }
    
    // 'conversionExpression' attribute on PickerInput (id=ProducerNameKanjiCriterion_Input) at TroubleTicketSearchDV.pcf: line 51, column 84
    function conversionExpression_30 (PickedValue :  Producer) : java.lang.String {
      return PickedValue.NameKanji
    }
    
    // 'conversionExpression' attribute on PickerInput (id=AccountNumberCriterion_Input) at TroubleTicketSearchDV.pcf: line 26, column 39
    function conversionExpression_6 (PickedValue :  Account) : java.lang.String {
      return PickedValue.DisplayName
    }
    
    // 'def' attribute on InputSetRef at TroubleTicketSearchDV.pcf: line 79, column 78
    function def_onEnter_57 (def :  pcf.ContactCriteriaInputSet) : void {
      def.onEnter(searchCriteria.ContactCriteria, true)
    }
    
    // 'def' attribute on InputSetRef at TroubleTicketSearchDV.pcf: line 83, column 41
    function def_onEnter_59 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at TroubleTicketSearchDV.pcf: line 79, column 78
    function def_refreshVariables_58 (def :  pcf.ContactCriteriaInputSet) : void {
      def.refreshVariables(searchCriteria.ContactCriteria, true)
    }
    
    // 'def' attribute on InputSetRef at TroubleTicketSearchDV.pcf: line 83, column 41
    function def_refreshVariables_60 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TextInput (id=TroubleTicketNumberCriterion_Input) at TroubleTicketSearchDV.pcf: line 18, column 53
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.TroubleTicketNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PickerInput (id=PolicyNumberCriterion_Input) at TroubleTicketSearchDV.pcf: line 34, column 39
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PickerInput (id=ProducerNameCriterion_Input) at TroubleTicketSearchDV.pcf: line 42, column 39
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ProducerName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PickerInput (id=ProducerNameKanjiCriterion_Input) at TroubleTicketSearchDV.pcf: line 51, column 84
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ProducerNameKanji = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=StatusCriterion_Input) at TroubleTicketSearchDV.pcf: line 58, column 50
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.IsClosed = (__VALUE_TO_SET as typekey.TroubleTicketStatus)
    }
    
    // 'value' attribute on CheckBoxInput (id=HasHoldCriterion_Input) at TroubleTicketSearchDV.pcf: line 63, column 41
    function defaultSetter_41 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.HasHold = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on AltUserInput (id=AssignedToUserCriterion_Input) at TroubleTicketSearchDV.pcf: line 68, column 48
    function defaultSetter_47 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AssignedToUser = (__VALUE_TO_SET as entity.User)
    }
    
    // 'value' attribute on RangeInput (id=AssignedToQueueCriterion_Input) at TroubleTicketSearchDV.pcf: line 75, column 45
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AssignedToQueue_Ext = (__VALUE_TO_SET as entity.AssignableQueue)
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at TroubleTicketSearchDV.pcf: line 26, column 39
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AccountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'label' attribute on PickerInput (id=ProducerNameCriterion_Input) at TroubleTicketSearchDV.pcf: line 42, column 39
    function label_20 () : java.lang.Object {
      return gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP ? DisplayKey.get("Web.TroubleTicketSearchDV.ProducerNamePhonetic") : DisplayKey.get("Web.TroubleTicketSearchDV.ProducerName")
    }
    
    // 'valueRange' attribute on RangeInput (id=AssignedToQueueCriterion_Input) at TroubleTicketSearchDV.pcf: line 75, column 45
    function valueRange_53 () : java.lang.Object {
      return gw.api.database.Query.make(AssignableQueue).select()
    }
    
    // 'value' attribute on TextInput (id=TroubleTicketNumberCriterion_Input) at TroubleTicketSearchDV.pcf: line 18, column 53
    function valueRoot_2 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TextInput (id=TroubleTicketNumberCriterion_Input) at TroubleTicketSearchDV.pcf: line 18, column 53
    function value_0 () : java.lang.String {
      return searchCriteria.TroubleTicketNumber
    }
    
    // 'value' attribute on PickerInput (id=PolicyNumberCriterion_Input) at TroubleTicketSearchDV.pcf: line 34, column 39
    function value_14 () : java.lang.String {
      return searchCriteria.PolicyNumber
    }
    
    // 'value' attribute on PickerInput (id=ProducerNameCriterion_Input) at TroubleTicketSearchDV.pcf: line 42, column 39
    function value_22 () : java.lang.String {
      return searchCriteria.ProducerName
    }
    
    // 'value' attribute on PickerInput (id=ProducerNameKanjiCriterion_Input) at TroubleTicketSearchDV.pcf: line 51, column 84
    function value_31 () : java.lang.String {
      return searchCriteria.ProducerNameKanji
    }
    
    // 'value' attribute on TypeKeyInput (id=StatusCriterion_Input) at TroubleTicketSearchDV.pcf: line 58, column 50
    function value_36 () : typekey.TroubleTicketStatus {
      return searchCriteria.IsClosed
    }
    
    // 'value' attribute on CheckBoxInput (id=HasHoldCriterion_Input) at TroubleTicketSearchDV.pcf: line 63, column 41
    function value_40 () : java.lang.Boolean {
      return searchCriteria.HasHold
    }
    
    // 'value' attribute on AltUserInput (id=AssignedToUserCriterion_Input) at TroubleTicketSearchDV.pcf: line 68, column 48
    function value_46 () : entity.User {
      return searchCriteria.AssignedToUser
    }
    
    // 'value' attribute on RangeInput (id=AssignedToQueueCriterion_Input) at TroubleTicketSearchDV.pcf: line 75, column 45
    function value_50 () : entity.AssignableQueue {
      return searchCriteria.AssignedToQueue_Ext
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at TroubleTicketSearchDV.pcf: line 26, column 39
    function value_7 () : java.lang.String {
      return searchCriteria.AccountNumber
    }
    
    // 'valueRange' attribute on RangeInput (id=AssignedToQueueCriterion_Input) at TroubleTicketSearchDV.pcf: line 75, column 45
    function verifyValueRangeIsAllowedType_54 ($$arg :  entity.AssignableQueue[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AssignedToQueueCriterion_Input) at TroubleTicketSearchDV.pcf: line 75, column 45
    function verifyValueRangeIsAllowedType_54 ($$arg :  gw.api.database.IQueryBeanResult<entity.AssignableQueue>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AssignedToQueueCriterion_Input) at TroubleTicketSearchDV.pcf: line 75, column 45
    function verifyValueRangeIsAllowedType_54 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AssignedToQueueCriterion_Input) at TroubleTicketSearchDV.pcf: line 75, column 45
    function verifyValueRange_55 () : void {
      var __valueRangeArg = gw.api.database.Query.make(AssignableQueue).select()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_54(__valueRangeArg)
    }
    
    // 'visible' attribute on PickerInput (id=ProducerNameKanjiCriterion_Input) at TroubleTicketSearchDV.pcf: line 51, column 84
    function visible_29 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP
    }
    
    property get searchCriteria () : gw.search.TroubleTicketSearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.search.TroubleTicketSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.TroubleTicketSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    
  }
  
  
}