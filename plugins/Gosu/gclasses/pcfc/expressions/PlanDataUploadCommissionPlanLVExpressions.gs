package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadCommissionPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanDataUploadCommissionPlanLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadCommissionPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PlanDataUploadCommissionPlanLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at PlanDataUploadCommissionPlanLV.pcf: line 17, column 108
    function highlighted_57 () : java.lang.Boolean {
      return (commissionPlan.Error or commissionPlan.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 23, column 46
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 28, column 41
    function label_22 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionPlan.PublicID")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 34, column 41
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionPlan.Name")
    }
    
    // 'label' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 39, column 39
    function label_32 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionPlan.EffectiveDate")
    }
    
    // 'label' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 44, column 39
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionPlan.ExpirationDate")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allowedTiers_Gold_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 49, column 42
    function label_42 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionPlan.AllowedTiers_Gold")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allowedTiers_Silver_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 54, column 42
    function label_47 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionPlan.AllowedTiers_Silver")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allowedTiers_Bronze_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 59, column 42
    function label_52 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionPlan.AllowedTiers_Bronze")
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 28, column 41
    function valueRoot_24 () : java.lang.Object {
      return commissionPlan
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 23, column 46
    function value_18 () : java.lang.String {
      return processor.getLoadStatus(commissionPlan)
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 28, column 41
    function value_23 () : java.lang.String {
      return commissionPlan.PublicID
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 34, column 41
    function value_28 () : java.lang.String {
      return commissionPlan.Name
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 39, column 39
    function value_33 () : java.util.Date {
      return commissionPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 44, column 39
    function value_38 () : java.util.Date {
      return commissionPlan.ExpirationDate
    }
    
    // 'value' attribute on BooleanRadioCell (id=allowedTiers_Gold_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 49, column 42
    function value_43 () : java.lang.Boolean {
      return commissionPlan.AllowedTiers_Gold
    }
    
    // 'value' attribute on BooleanRadioCell (id=allowedTiers_Silver_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 54, column 42
    function value_48 () : java.lang.Boolean {
      return commissionPlan.AllowedTiers_Silver
    }
    
    // 'value' attribute on BooleanRadioCell (id=allowedTiers_Bronze_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 59, column 42
    function value_53 () : java.lang.Boolean {
      return commissionPlan.AllowedTiers_Bronze
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 23, column 46
    function visible_19 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get commissionPlan () : tdic.util.dataloader.data.plandata.CommissionPlanData {
      return getIteratedValue(1) as tdic.util.dataloader.data.plandata.CommissionPlanData
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadCommissionPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanDataUploadCommissionPlanLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allowedTiers_Gold_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 49, column 42
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionPlan.AllowedTiers_Gold")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allowedTiers_Silver_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 54, column 42
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionPlan.AllowedTiers_Silver")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allowedTiers_Bronze_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 59, column 42
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionPlan.AllowedTiers_Bronze")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 28, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionPlan.PublicID")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 34, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionPlan.Name")
    }
    
    // 'label' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 39, column 39
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionPlan.EffectiveDate")
    }
    
    // 'label' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 44, column 39
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionPlan.ExpirationDate")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 23, column 46
    function sortValue_1 (commissionPlan :  tdic.util.dataloader.data.plandata.CommissionPlanData) : java.lang.Object {
      return processor.getLoadStatus(commissionPlan)
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 44, column 39
    function sortValue_10 (commissionPlan :  tdic.util.dataloader.data.plandata.CommissionPlanData) : java.lang.Object {
      return commissionPlan.ExpirationDate
    }
    
    // 'value' attribute on BooleanRadioCell (id=allowedTiers_Gold_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 49, column 42
    function sortValue_12 (commissionPlan :  tdic.util.dataloader.data.plandata.CommissionPlanData) : java.lang.Object {
      return commissionPlan.AllowedTiers_Gold
    }
    
    // 'value' attribute on BooleanRadioCell (id=allowedTiers_Silver_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 54, column 42
    function sortValue_14 (commissionPlan :  tdic.util.dataloader.data.plandata.CommissionPlanData) : java.lang.Object {
      return commissionPlan.AllowedTiers_Silver
    }
    
    // 'value' attribute on BooleanRadioCell (id=allowedTiers_Bronze_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 59, column 42
    function sortValue_16 (commissionPlan :  tdic.util.dataloader.data.plandata.CommissionPlanData) : java.lang.Object {
      return commissionPlan.AllowedTiers_Bronze
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 28, column 41
    function sortValue_4 (commissionPlan :  tdic.util.dataloader.data.plandata.CommissionPlanData) : java.lang.Object {
      return commissionPlan.PublicID
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 34, column 41
    function sortValue_6 (commissionPlan :  tdic.util.dataloader.data.plandata.CommissionPlanData) : java.lang.Object {
      return commissionPlan.Name
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 39, column 39
    function sortValue_8 (commissionPlan :  tdic.util.dataloader.data.plandata.CommissionPlanData) : java.lang.Object {
      return commissionPlan.EffectiveDate
    }
    
    // 'value' attribute on RowIterator (id=CommissionPlan) at PlanDataUploadCommissionPlanLV.pcf: line 15, column 100
    function value_58 () : java.util.ArrayList<tdic.util.dataloader.data.plandata.CommissionPlanData> {
      return processor.CommissionPlanArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadCommissionPlanLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}