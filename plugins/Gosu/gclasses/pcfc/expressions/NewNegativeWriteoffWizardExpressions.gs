package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewNegativeWriteoffWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewNegativeWriteoffWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    static function __constructorIndex (passedInTarget :  entity.TAccountOwner) : int {
      return 1
    }
    
    // 'allowFinish' attribute on WizardStep (id=ConfirmationStep) at NewNegativeWriteoffWizard.pcf: line 44, column 93
    function allowFinish_10 () : java.lang.Boolean {
      return uiWriteOff != null && uiWriteOff.Amount != null
    }
    
    // 'allowNext' attribute on WizardStep (id=TargetStep) at NewNegativeWriteoffWizard.pcf: line 32, column 87
    function allowNext_3 () : java.lang.Boolean {
      return targetOfWriteOff.TAccountOwner != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=NewNegativeWriteoffWizard) at NewNegativeWriteoffWizard.pcf: line 8, column 36
    function beforeCommit_13 (pickedValue :  java.lang.Object) : void {
      uiWriteOff.doWriteOff()
    }
    
    // 'initialValue' attribute on Variable at NewNegativeWriteoffWizard.pcf: line 16, column 60
    function initialValue_0 () : gw.api.web.accounting.TAccountOwnerReference {
      return new gw.api.web.accounting.TAccountOwnerReference()
    }
    
    // 'initialValue' attribute on Variable at NewNegativeWriteoffWizard.pcf: line 20, column 56
    function initialValue_1 () : gw.api.web.accounting.UIWriteOffCreation {
      return createNewUIWriteOff( )
    }
    
    // 'initialValue' attribute on Variable at NewNegativeWriteoffWizard.pcf: line 25, column 36
    function initialValue_2 () : entity.TAccountOwner {
      return uiWriteOff != null ? uiWriteOff.WriteOff.TAccountOwner : null
    }
    
    // 'onExit' attribute on WizardStep (id=TargetStep) at NewNegativeWriteoffWizard.pcf: line 32, column 87
    function onExit_4 () : void {
      onTargetStepExit()
    }
    
    // 'onExit' attribute on WizardStep (id=DetailsStep) at NewNegativeWriteoffWizard.pcf: line 38, column 88
    function onExit_7 () : void {
      onDetailsStepExit()
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at NewNegativeWriteoffWizard.pcf: line 44, column 93
    function screen_onEnter_11 (def :  pcf.NewNegativeWriteoffWizardConfirmationStepScreen) : void {
      def.onEnter(uiWriteOff)
    }
    
    // 'screen' attribute on WizardStep (id=TargetStep) at NewNegativeWriteoffWizard.pcf: line 32, column 87
    function screen_onEnter_5 (def :  pcf.NewNegativeWriteoffWizardAccountSelectStepScreen) : void {
      def.onEnter(targetOfWriteOff)
    }
    
    // 'screen' attribute on WizardStep (id=DetailsStep) at NewNegativeWriteoffWizard.pcf: line 38, column 88
    function screen_onEnter_8 (def :  pcf.NewNegativeWriteoffWizardDetailsStepScreen) : void {
      def.onEnter(uiWriteOff)
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at NewNegativeWriteoffWizard.pcf: line 44, column 93
    function screen_refreshVariables_12 (def :  pcf.NewNegativeWriteoffWizardConfirmationStepScreen) : void {
      def.refreshVariables(uiWriteOff)
    }
    
    // 'screen' attribute on WizardStep (id=TargetStep) at NewNegativeWriteoffWizard.pcf: line 32, column 87
    function screen_refreshVariables_6 (def :  pcf.NewNegativeWriteoffWizardAccountSelectStepScreen) : void {
      def.refreshVariables(targetOfWriteOff)
    }
    
    // 'screen' attribute on WizardStep (id=DetailsStep) at NewNegativeWriteoffWizard.pcf: line 38, column 88
    function screen_refreshVariables_9 (def :  pcf.NewNegativeWriteoffWizardDetailsStepScreen) : void {
      def.refreshVariables(uiWriteOff)
    }
    
    // 'tabBar' attribute on Wizard (id=NewNegativeWriteoffWizard) at NewNegativeWriteoffWizard.pcf: line 8, column 36
    function tabBar_onEnter_14 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=NewNegativeWriteoffWizard) at NewNegativeWriteoffWizard.pcf: line 8, column 36
    function tabBar_refreshVariables_15 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.NewNegativeWriteoffWizard {
      return super.CurrentLocation as pcf.NewNegativeWriteoffWizard
    }
    
    property get passedInTarget () : entity.TAccountOwner {
      return getVariableValue("passedInTarget", 0) as entity.TAccountOwner
    }
    
    property set passedInTarget ($arg :  entity.TAccountOwner) {
      setVariableValue("passedInTarget", 0, $arg)
    }
    
    property get targetOfWriteOff () : gw.api.web.accounting.TAccountOwnerReference {
      return getVariableValue("targetOfWriteOff", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set targetOfWriteOff ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setVariableValue("targetOfWriteOff", 0, $arg)
    }
    
    property get uiWriteOff () : gw.api.web.accounting.UIWriteOffCreation {
      return getVariableValue("uiWriteOff", 0) as gw.api.web.accounting.UIWriteOffCreation
    }
    
    property set uiWriteOff ($arg :  gw.api.web.accounting.UIWriteOffCreation) {
      setVariableValue("uiWriteOff", 0, $arg)
    }
    
    
                  function createNewUIWriteOff(): gw.api.web.accounting.UIWriteOffCreation {
                    discardOldWriteOff();
                    if (targetOfWriteOff.TAccountOwner typeis Account) {
                      var factory = new gw.api.web.accounting.WriteOffFactory(CurrentLocation)
                      var writeOff = factory.createAccountNegativeWriteOff(targetOfWriteOff.TAccountOwner)
                      return new gw.api.web.accounting.UIWriteOffCreation(writeOff);
                    } else {
                      return null;
                    }
                  }
    
                  public function onTargetStepExit() {
                    if (targetOfWriteOff == null) {
                      discardOldWriteOff();
                      uiWriteOff = null;
                    }
                    else if (uiWriteOff == null || uiWriteOff.WriteOff.TAccountOwner != targetOfWriteOff.TAccountOwner) {
                      uiWriteOff = createNewUIWriteOff()
                    }
                  }
    
                  public function onDetailsStepExit() {
                    uiWriteOff.initiateApprovalActivityIfUserLacksAuthority()
                  }
    
                  private function discardOldWriteOff() {
                    if (uiWriteOff != null) {
                      uiWriteOff.cleanupBeforeDiscard();
                    }
                  }
          
    
    
  }
  
  
}