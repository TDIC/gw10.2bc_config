package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewTransferWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewTransferWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewTransferWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewTransferWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=NewTransferWizard) at NewTransferWizard.pcf: line 11, column 28
    function afterCancel_12 () : void {
      DesktopGroup.go()
    }
    
    // 'afterCancel' attribute on Wizard (id=NewTransferWizard) at NewTransferWizard.pcf: line 11, column 28
    function afterCancel_dest_13 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewTransferWizard) at NewTransferWizard.pcf: line 11, column 28
    function afterFinish_17 () : void {
      DesktopGroup.go()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewTransferWizard) at NewTransferWizard.pcf: line 11, column 28
    function afterFinish_dest_18 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'allowNext' attribute on WizardStep (id=Step1) at NewTransferWizard.pcf: line 28, column 92
    function allowNext_2 () : java.lang.Boolean {
      return fundsTransferUtil.SourceOwner != null
    }
    
    // 'canVisit' attribute on Wizard (id=NewTransferWizard) at NewTransferWizard.pcf: line 11, column 28
    static function canVisit_14 () : java.lang.Boolean {
      return perm.Transaction.cashtx
    }
    
    // 'initialValue' attribute on Variable at NewTransferWizard.pcf: line 17, column 56
    function initialValue_0 () : gw.api.web.transaction.FundsTransferUtil {
      return createNewFundsTransferUtil()
    }
    
    // 'initialValue' attribute on Variable at NewTransferWizard.pcf: line 21, column 60
    function initialValue_1 () : gw.api.web.accounting.TAccountOwnerReference {
      return new gw.api.web.accounting.TAccountOwnerReference()
    }
    
    // 'onExit' attribute on WizardStep (id=Step1) at NewTransferWizard.pcf: line 28, column 92
    function onExit_3 () : void {
      fundsTransferUtil.SourceOwner = tAccountOwnerReference.TAccountOwner
    }
    
    // 'onExit' attribute on WizardStep (id=Step2) at NewTransferWizard.pcf: line 35, column 88
    function onExit_6 () : void {
      fundsTransferUtil.setSources();validateTransferAmount(); fundsTransferUtil.createTransfers();
    }
    
    // 'onFirstEnter' attribute on WizardStep (id=Step2) at NewTransferWizard.pcf: line 35, column 88
    function onFirstEnter_7 () : void {
      createInitialTransferRow()
    }
    
    // 'screen' attribute on WizardStep (id=Step3) at NewTransferWizard.pcf: line 41, column 93
    function screen_onEnter_10 (def :  pcf.TransferConfirmationScreen) : void {
      def.onEnter(fundsTransferUtil)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at NewTransferWizard.pcf: line 28, column 92
    function screen_onEnter_4 (def :  pcf.WizardsStep1AccountPolicySearchScreen) : void {
      def.onEnter(tAccountOwnerReference, null, true, false)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at NewTransferWizard.pcf: line 35, column 88
    function screen_onEnter_8 (def :  pcf.TransferDetailsScreen) : void {
      def.onEnter(fundsTransferUtil)
    }
    
    // 'screen' attribute on WizardStep (id=Step3) at NewTransferWizard.pcf: line 41, column 93
    function screen_refreshVariables_11 (def :  pcf.TransferConfirmationScreen) : void {
      def.refreshVariables(fundsTransferUtil)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at NewTransferWizard.pcf: line 28, column 92
    function screen_refreshVariables_5 (def :  pcf.WizardsStep1AccountPolicySearchScreen) : void {
      def.refreshVariables(tAccountOwnerReference, null, true, false)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at NewTransferWizard.pcf: line 35, column 88
    function screen_refreshVariables_9 (def :  pcf.TransferDetailsScreen) : void {
      def.refreshVariables(fundsTransferUtil)
    }
    
    // 'tabBar' attribute on Wizard (id=NewTransferWizard) at NewTransferWizard.pcf: line 11, column 28
    function tabBar_onEnter_15 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=NewTransferWizard) at NewTransferWizard.pcf: line 11, column 28
    function tabBar_refreshVariables_16 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.NewTransferWizard {
      return super.CurrentLocation as pcf.NewTransferWizard
    }
    
    property get fundsTransferUtil () : gw.api.web.transaction.FundsTransferUtil {
      return getVariableValue("fundsTransferUtil", 0) as gw.api.web.transaction.FundsTransferUtil
    }
    
    property set fundsTransferUtil ($arg :  gw.api.web.transaction.FundsTransferUtil) {
      setVariableValue("fundsTransferUtil", 0, $arg)
    }
    
    property get tAccountOwnerReference () : gw.api.web.accounting.TAccountOwnerReference {
      return getVariableValue("tAccountOwnerReference", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set tAccountOwnerReference ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setVariableValue("tAccountOwnerReference", 0, $arg)
    }
    
    function createInitialTransferRow() { 
      fundsTransferUtil.addToTransfers(new FundsTransfer(CurrentLocation, fundsTransferUtil.SourceOwner.Currency))
    }
    
    function createNewFundsTransferUtil() : gw.api.web.transaction.FundsTransferUtil {
      fundsTransferUtil = new gw.api.web.transaction.FundsTransferUtil();
      fundsTransferUtil.TargetType = TAccountOwnerType.TC_ACCOUNT
      return fundsTransferUtil;
    }
    
    function validateTransferAmount() {
      if (!fundsTransferUtil.validateAmounts()) {
        throw new gw.api.util.DisplayableException(DisplayKey.get("Web.NewTransferWizard.InsufficientUnappliedFundsForTransfer"))
      }
    }
    
    
  }
  
  
}