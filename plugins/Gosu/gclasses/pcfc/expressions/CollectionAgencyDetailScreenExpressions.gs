package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/collection/CollectionAgencyDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CollectionAgencyDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/collection/CollectionAgencyDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CollectionAgencyDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at CollectionAgencyDetailScreen.pcf: line 15, column 57
    function def_onEnter_1 (def :  pcf.CollectionAgencyDetailDV) : void {
      def.onEnter(collectionAgency)
    }
    
    // 'def' attribute on PanelRef at CollectionAgencyDetailScreen.pcf: line 17, column 59
    function def_onEnter_3 (def :  pcf.CollectionAgencyAccountsLV) : void {
      def.onEnter(collectionAgency)
    }
    
    // 'def' attribute on PanelRef at CollectionAgencyDetailScreen.pcf: line 15, column 57
    function def_refreshVariables_2 (def :  pcf.CollectionAgencyDetailDV) : void {
      def.refreshVariables(collectionAgency)
    }
    
    // 'def' attribute on PanelRef at CollectionAgencyDetailScreen.pcf: line 17, column 59
    function def_refreshVariables_4 (def :  pcf.CollectionAgencyAccountsLV) : void {
      def.refreshVariables(collectionAgency)
    }
    
    // EditButtons at CollectionAgencyDetailScreen.pcf: line 12, column 21
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    property get collectionAgency () : CollectionAgency {
      return getRequireValue("collectionAgency", 0) as CollectionAgency
    }
    
    property set collectionAgency ($arg :  CollectionAgency) {
      setRequireValue("collectionAgency", 0, $arg)
    }
    
    
  }
  
  
}