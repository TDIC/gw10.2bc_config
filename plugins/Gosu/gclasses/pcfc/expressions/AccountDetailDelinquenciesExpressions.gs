package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailDelinquencies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailDelinquenciesExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailDelinquencies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailDelinquenciesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    static function __constructorIndex (account :  Account, selectedDelinquencyProcessOnEnter :  DelinquencyProcess) : int {
      return 1
    }
    
    // 'canVisit' attribute on Page (id=AccountDetailDelinquencies) at AccountDetailDelinquencies.pcf: line 10, column 78
    static function canVisit_3 (account :  Account, selectedDelinquencyProcessOnEnter :  DelinquencyProcess) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctdelview
    }
    
    // 'def' attribute on ScreenRef at AccountDetailDelinquencies.pcf: line 23, column 91
    function def_onEnter_1 (def :  pcf.DelinquencyProcessesDetailScreen) : void {
      def.onEnter(account, selectedDelinquencyProcessOnEnter)
    }
    
    // 'def' attribute on ScreenRef at AccountDetailDelinquencies.pcf: line 23, column 91
    function def_refreshVariables_2 (def :  pcf.DelinquencyProcessesDetailScreen) : void {
      def.refreshVariables(account, selectedDelinquencyProcessOnEnter)
    }
    
    // 'initialValue' attribute on Variable at AccountDetailDelinquencies.pcf: line 21, column 34
    function initialValue_0 () : DelinquencyProcess {
      return (account.DelinquencyProcesses.length == 0 ? null : account.DelinquencyProcesses[0])
    }
    
    // Page (id=AccountDetailDelinquencies) at AccountDetailDelinquencies.pcf: line 10, column 78
    static function parent_4 (account :  Account, selectedDelinquencyProcessOnEnter :  DelinquencyProcess) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.AccountDetailDelinquencies {
      return super.CurrentLocation as pcf.AccountDetailDelinquencies
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get selectedDelinquencyProcessOnEnter () : DelinquencyProcess {
      return getVariableValue("selectedDelinquencyProcessOnEnter", 0) as DelinquencyProcess
    }
    
    property set selectedDelinquencyProcessOnEnter ($arg :  DelinquencyProcess) {
      setVariableValue("selectedDelinquencyProcessOnEnter", 0, $arg)
    }
    
    
  }
  
  
}