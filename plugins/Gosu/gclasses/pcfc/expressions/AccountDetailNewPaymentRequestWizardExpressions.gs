package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.web.payment.PaymentInstrumentFactory
@javax.annotation.Generated("config/web/pcf/payment/AccountDetailNewPaymentRequestWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailNewPaymentRequestWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/AccountDetailNewPaymentRequestWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailNewPaymentRequestWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Wizard (id=AccountDetailNewPaymentRequestWizard) at AccountDetailNewPaymentRequestWizard.pcf: line 7, column 47
    function beforeCommit_5 (pickedValue :  java.lang.Object) : void {
      finalDataSet()
    }
    
    // 'initialValue' attribute on Variable at AccountDetailNewPaymentRequestWizard.pcf: line 16, column 30
    function initialValue_0 () : PaymentRequest {
      return initNewPaymentRequest()
    }
    
    // 'screen' attribute on WizardStep (id=newPaymentRequest) at AccountDetailNewPaymentRequestWizard.pcf: line 22, column 109
    function screen_onEnter_1 (def :  pcf.NewPaymentRequestScreen) : void {
      def.onEnter(paymentRequest)
    }
    
    // 'screen' attribute on WizardStep (id=newPaymentRequestConfirmation) at AccountDetailNewPaymentRequestWizard.pcf: line 28, column 104
    function screen_onEnter_3 (def :  pcf.NewPaymentRequestConfirmationScreen) : void {
      def.onEnter(paymentRequest)
    }
    
    // 'screen' attribute on WizardStep (id=newPaymentRequest) at AccountDetailNewPaymentRequestWizard.pcf: line 22, column 109
    function screen_refreshVariables_2 (def :  pcf.NewPaymentRequestScreen) : void {
      def.refreshVariables(paymentRequest)
    }
    
    // 'screen' attribute on WizardStep (id=newPaymentRequestConfirmation) at AccountDetailNewPaymentRequestWizard.pcf: line 28, column 104
    function screen_refreshVariables_4 (def :  pcf.NewPaymentRequestConfirmationScreen) : void {
      def.refreshVariables(paymentRequest)
    }
    
    // 'tabBar' attribute on Wizard (id=AccountDetailNewPaymentRequestWizard) at AccountDetailNewPaymentRequestWizard.pcf: line 7, column 47
    function tabBar_onEnter_6 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AccountDetailNewPaymentRequestWizard) at AccountDetailNewPaymentRequestWizard.pcf: line 7, column 47
    function tabBar_refreshVariables_7 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AccountDetailNewPaymentRequestWizard {
      return super.CurrentLocation as pcf.AccountDetailNewPaymentRequestWizard
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get paymentRequest () : PaymentRequest {
      return getVariableValue("paymentRequest", 0) as PaymentRequest
    }
    
    property set paymentRequest ($arg :  PaymentRequest) {
      setVariableValue("paymentRequest", 0, $arg)
    }
    
    
    function initNewPaymentRequest() : PaymentRequest {
            var npr = new PaymentRequest(account.Currency)
            npr.Account = account
            npr.RequestDate = gw.api.util.DateUtil.currentDate()
            npr.Status = TC_REQUESTED
    /**
             * US1158 - APW Payment Reversals
             * 3/11/2015 Alvin Lee
             *
             * Set the draft date based on the next available APW withdrawal date.
             * OOTB: npr.DraftDate = util.DateUtil.addDays(npr.RequestDate, account.BillingPlan.DraftInterval);
             */
            npr.DueDate = npr.RequestDate.addDays(account.BillingPlan.NonResponsivePmntDueInterval)
            npr.DraftDate = npr.DueDate.addBusinessDays(-account.BillingPlan.DraftIntervalDayCount)
            //npr.DraftDate = account.BillingPlan.DraftInterval.addTo(npr.RequestDate)
            /*if (gw.payment.PaymentInstrumentFilters.paymentRequestPaymentInstrumentFilter.contains(account.DefaultPaymentInstrument.PaymentMethod)) {
              npr.PaymentInstrument = account.DefaultPaymentInstrument
            }*/
            npr.PaymentInstrument = account.DefaultPaymentInstrument
            npr.UnappliedFund_TDIC = account.DefaultUnappliedFund
            npr.IsOneTimePayment_TDIC = true
            return npr
          }
    
          function finalDataSet() {
            paymentRequest.StatusDate = paymentRequest.ChangeDeadlineDate
          }
    
    
  }
  
  
}