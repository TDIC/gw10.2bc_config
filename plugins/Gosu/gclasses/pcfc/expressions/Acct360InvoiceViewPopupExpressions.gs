package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/Acct360InvoiceViewPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class Acct360InvoiceViewPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/acct360/Acct360InvoiceViewPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Acct360InvoiceViewPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (invoice :  AccountInvoice) : int {
      return 0
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=PriorAmountDue_Input) at Acct360InvoiceViewPopup.pcf: line 65, column 56
    function currency_24 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on DateInput (id=PaymentDueDate_Input) at Acct360InvoiceViewPopup.pcf: line 48, column 44
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoice.PaymentDueDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at Acct360InvoiceViewPopup.pcf: line 92, column 49
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      aggregation = (__VALUE_TO_SET as typekey.AggregationType)
    }
    
    // 'value' attribute on DateInput (id=InvoiceDate_Input) at Acct360InvoiceViewPopup.pcf: line 39, column 39
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoice.EventDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'editable' attribute on DateInput (id=InvoiceDate_Input) at Acct360InvoiceViewPopup.pcf: line 39, column 39
    function editable_3 () : java.lang.Boolean {
      return invoice.Status == TC_PLANNED and perm.Account.invcdateedit
    }
    
    // 'initialValue' attribute on Variable at Acct360InvoiceViewPopup.pcf: line 18, column 23
    function initialValue_0 () : Account {
      return invoice.Account
    }
    
    // 'initialValue' attribute on Variable at Acct360InvoiceViewPopup.pcf: line 22, column 39
    function initialValue_1 () : typekey.AggregationType {
      return AggregationType.TC_CHARGES
    }
    
    // 'initialValue' attribute on Variable at Acct360InvoiceViewPopup.pcf: line 26, column 30
    function initialValue_2 () : AccountInvoice {
      return invoice.PreviousInvoice
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_InstallmentNumber_Cell) at Acct360InvoiceViewPopup.pcf: line 111, column 48
    function sortValue_39 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.InstallmentNumber
    }
    
    // 'value' attribute on DateCell (id=InvoiceItemsLV_EventDate_Cell) at Acct360InvoiceViewPopup.pcf: line 118, column 68
    function sortValue_40 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.Date
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_Charge_Cell) at Acct360InvoiceViewPopup.pcf: line 131, column 114
    function sortValue_42 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.Charge
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_Context_Cell) at Acct360InvoiceViewPopup.pcf: line 137, column 158
    function sortValue_44 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.Charge.BillingInstruction
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_ChargeGroup_Cell) at Acct360InvoiceViewPopup.pcf: line 142, column 73
    function sortValue_46 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.Charge.ChargeGroup
    }
    
    // 'value' attribute on TypeKeyCell (id=InvoiceItemsLV_Description_Cell) at Acct360InvoiceViewPopup.pcf: line 148, column 68
    function sortValue_48 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=InvoiceItemsLV_Product_Cell) at Acct360InvoiceViewPopup.pcf: line 154, column 127
    function sortValue_50 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.PolicyPeriod.Policy.LOBCode
    }
    
    // 'value' attribute on MonetaryAmountCell (id=InvoiceItemsLV_Amount_Cell) at Acct360InvoiceViewPopup.pcf: line 161, column 45
    function sortValue_52 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=InvoiceItemsLV_PaidAmount_Cell) at Acct360InvoiceViewPopup.pcf: line 166, column 49
    function sortValue_53 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.PaidAmount
    }
    
    // '$$sumValue' attribute on RowIterator at Acct360InvoiceViewPopup.pcf: line 161, column 45
    function sumValueRoot_61 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem
    }
    
    // 'footerSumValue' attribute on RowIterator at Acct360InvoiceViewPopup.pcf: line 161, column 45
    function sumValue_60 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.Amount
    }
    
    // 'validationExpression' attribute on DateInput (id=PaymentDueDate_Input) at Acct360InvoiceViewPopup.pcf: line 48, column 44
    function validationExpression_12 () : java.lang.Object {
      return gw.api.web.invoice.InvoiceUtil.validatePaymentDueDate(invoice)
    }
    
    // 'validationExpression' attribute on DateInput (id=InvoiceDate_Input) at Acct360InvoiceViewPopup.pcf: line 39, column 39
    function validationExpression_4 () : java.lang.Object {
      return gw.api.web.invoice.InvoiceUtil.validateInvoiceDate(invoice)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at Acct360InvoiceViewPopup.pcf: line 92, column 49
    function valueRange_35 () : java.lang.Object {
      return typekey.AggregationType.getTypeKeys(false)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PriorAmountDue_Input) at Acct360InvoiceViewPopup.pcf: line 65, column 56
    function valueRoot_23 () : java.lang.Object {
      return previousInvoice
    }
    
    // 'value' attribute on DateInput (id=InvoiceDate_Input) at Acct360InvoiceViewPopup.pcf: line 39, column 39
    function valueRoot_7 () : java.lang.Object {
      return invoice
    }
    
    // 'value' attribute on RowIterator at Acct360InvoiceViewPopup.pcf: line 103, column 50
    function value_102 () : entity.InvoiceLineItem[] {
      return invoice.getAggregatedInvoiceItems(aggregation)
    }
    
    // 'value' attribute on DateInput (id=PaymentDueDate_Input) at Acct360InvoiceViewPopup.pcf: line 48, column 44
    function value_13 () : java.util.Date {
      return invoice.PaymentDueDate
    }
    
    // 'value' attribute on TextInput (id=Status_Input) at Acct360InvoiceViewPopup.pcf: line 54, column 42
    function value_19 () : java.lang.String {
      return invoice.StatusForUI
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PriorAmountDue_Input) at Acct360InvoiceViewPopup.pcf: line 65, column 56
    function value_22 () : gw.pl.currency.MonetaryAmount {
      return previousInvoice.OutstandingAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalCharges_Input) at Acct360InvoiceViewPopup.pcf: line 72, column 75
    function value_26 () : gw.pl.currency.MonetaryAmount {
      return invoice.Status != TC_PLANNED ? invoice.Amount : null
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalAmountDue_Input) at Acct360InvoiceViewPopup.pcf: line 79, column 48
    function value_29 () : gw.pl.currency.MonetaryAmount {
      return invoice.OutstandingAmount
    }
    
    // 'value' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at Acct360InvoiceViewPopup.pcf: line 92, column 49
    function value_33 () : typekey.AggregationType {
      return aggregation
    }
    
    // 'value' attribute on DateInput (id=InvoiceDate_Input) at Acct360InvoiceViewPopup.pcf: line 39, column 39
    function value_5 () : java.util.Date {
      return invoice.EventDate
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at Acct360InvoiceViewPopup.pcf: line 92, column 49
    function verifyValueRangeIsAllowedType_36 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at Acct360InvoiceViewPopup.pcf: line 92, column 49
    function verifyValueRangeIsAllowedType_36 ($$arg :  typekey.AggregationType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at Acct360InvoiceViewPopup.pcf: line 92, column 49
    function verifyValueRange_37 () : void {
      var __valueRangeArg = typekey.AggregationType.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_36(__valueRangeArg)
    }
    
    // 'visible' attribute on DateCell (id=InvoiceItemsLV_EventDate_Cell) at Acct360InvoiceViewPopup.pcf: line 118, column 68
    function visible_41 () : java.lang.Boolean {
      return aggregation==AggregationType.TC_CHARGES
    }
    
    // 'visible' attribute on TextCell (id=InvoiceItemsLV_Charge_Cell) at Acct360InvoiceViewPopup.pcf: line 131, column 114
    function visible_43 () : java.lang.Boolean {
      return aggregation==AggregationType.TC_CATEGORIES or aggregation==AggregationType.TC_CHARGES
    }
    
    // 'visible' attribute on TextCell (id=InvoiceItemsLV_Context_Cell) at Acct360InvoiceViewPopup.pcf: line 137, column 158
    function visible_45 () : java.lang.Boolean {
      return aggregation==AggregationType.TC_CONTEXTS or aggregation==AggregationType.TC_CATEGORIES or aggregation==AggregationType.TC_CHARGES
    }
    
    // 'visible' attribute on TextCell (id=InvoiceItemsLV_ChargeGroup_Cell) at Acct360InvoiceViewPopup.pcf: line 142, column 73
    function visible_47 () : java.lang.Boolean {
      return aggregation==AggregationType.TC_CHARGEGROUPS
    }
    
    // 'visible' attribute on TypeKeyCell (id=InvoiceItemsLV_Product_Cell) at Acct360InvoiceViewPopup.pcf: line 154, column 127
    function visible_51 () : java.lang.Boolean {
      return aggregation != AggregationType.TC_CATEGORIES && aggregation != AggregationType.TC_CONTEXTS
    }
    
    override property get CurrentLocation () : pcf.Acct360InvoiceViewPopup {
      return super.CurrentLocation as pcf.Acct360InvoiceViewPopup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get aggregation () : typekey.AggregationType {
      return getVariableValue("aggregation", 0) as typekey.AggregationType
    }
    
    property set aggregation ($arg :  typekey.AggregationType) {
      setVariableValue("aggregation", 0, $arg)
    }
    
    property get invoice () : AccountInvoice {
      return getVariableValue("invoice", 0) as AccountInvoice
    }
    
    property set invoice ($arg :  AccountInvoice) {
      setVariableValue("invoice", 0, $arg)
    }
    
    property get previousInvoice () : AccountInvoice {
      return getVariableValue("previousInvoice", 0) as AccountInvoice
    }
    
    property set previousInvoice ($arg :  AccountInvoice) {
      setVariableValue("previousInvoice", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/acct360/Acct360InvoiceViewPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends Acct360InvoiceViewPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=InvoiceItemsLV_Policy_Cell) at Acct360InvoiceViewPopup.pcf: line 125, column 161
    function actionAvailable_71 () : java.lang.Boolean {
      return invoiceItem.PolicyPeriod != null
    }
    
    // 'action' attribute on TextCell (id=InvoiceItemsLV_Policy_Cell) at Acct360InvoiceViewPopup.pcf: line 125, column 161
    function action_69 () : void {
      PolicySummary.push(invoiceItem.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=InvoiceItemsLV_Policy_Cell) at Acct360InvoiceViewPopup.pcf: line 125, column 161
    function action_dest_70 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(invoiceItem.PolicyPeriod)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=InvoiceItemsLV_Amount_Cell) at Acct360InvoiceViewPopup.pcf: line 161, column 45
    function currency_96 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_InstallmentNumber_Cell) at Acct360InvoiceViewPopup.pcf: line 111, column 48
    function valueRoot_63 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_Context_Cell) at Acct360InvoiceViewPopup.pcf: line 137, column 158
    function valueRoot_79 () : java.lang.Object {
      return invoiceItem.Charge
    }
    
    // 'value' attribute on TypeKeyCell (id=InvoiceItemsLV_Product_Cell) at Acct360InvoiceViewPopup.pcf: line 154, column 127
    function valueRoot_91 () : java.lang.Object {
      return invoiceItem.PolicyPeriod.Policy
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_InstallmentNumber_Cell) at Acct360InvoiceViewPopup.pcf: line 111, column 48
    function value_62 () : java.lang.Integer {
      return invoiceItem.InstallmentNumber
    }
    
    // 'value' attribute on DateCell (id=InvoiceItemsLV_EventDate_Cell) at Acct360InvoiceViewPopup.pcf: line 118, column 68
    function value_65 () : java.util.Date {
      return invoiceItem.Date
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_Policy_Cell) at Acct360InvoiceViewPopup.pcf: line 125, column 161
    function value_72 () : java.lang.String {
      return invoiceItem.PolicyPeriod == null ? DisplayKey.get("Web.InvoiceItemsLV.AccountCharge") : invoiceItem.PolicyPeriod.DisplayName
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_Charge_Cell) at Acct360InvoiceViewPopup.pcf: line 131, column 114
    function value_74 () : entity.Charge {
      return invoiceItem.Charge
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_Context_Cell) at Acct360InvoiceViewPopup.pcf: line 137, column 158
    function value_78 () : entity.BillingInstruction {
      return invoiceItem.Charge.BillingInstruction
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_ChargeGroup_Cell) at Acct360InvoiceViewPopup.pcf: line 142, column 73
    function value_82 () : java.lang.String {
      return invoiceItem.Charge.ChargeGroup
    }
    
    // 'value' attribute on TypeKeyCell (id=InvoiceItemsLV_Description_Cell) at Acct360InvoiceViewPopup.pcf: line 148, column 68
    function value_86 () : typekey.InvoiceItemType {
      return invoiceItem.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=InvoiceItemsLV_Product_Cell) at Acct360InvoiceViewPopup.pcf: line 154, column 127
    function value_90 () : typekey.LOBCode {
      return invoiceItem.PolicyPeriod.Policy.LOBCode
    }
    
    // 'value' attribute on MonetaryAmountCell (id=InvoiceItemsLV_Amount_Cell) at Acct360InvoiceViewPopup.pcf: line 161, column 45
    function value_94 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=InvoiceItemsLV_PaidAmount_Cell) at Acct360InvoiceViewPopup.pcf: line 166, column 49
    function value_98 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.PaidAmount
    }
    
    // 'visible' attribute on DateCell (id=InvoiceItemsLV_EventDate_Cell) at Acct360InvoiceViewPopup.pcf: line 118, column 68
    function visible_67 () : java.lang.Boolean {
      return aggregation==AggregationType.TC_CHARGES
    }
    
    // 'visible' attribute on TextCell (id=InvoiceItemsLV_Charge_Cell) at Acct360InvoiceViewPopup.pcf: line 131, column 114
    function visible_76 () : java.lang.Boolean {
      return aggregation==AggregationType.TC_CATEGORIES or aggregation==AggregationType.TC_CHARGES
    }
    
    // 'visible' attribute on TextCell (id=InvoiceItemsLV_Context_Cell) at Acct360InvoiceViewPopup.pcf: line 137, column 158
    function visible_80 () : java.lang.Boolean {
      return aggregation==AggregationType.TC_CONTEXTS or aggregation==AggregationType.TC_CATEGORIES or aggregation==AggregationType.TC_CHARGES
    }
    
    // 'visible' attribute on TextCell (id=InvoiceItemsLV_ChargeGroup_Cell) at Acct360InvoiceViewPopup.pcf: line 142, column 73
    function visible_84 () : java.lang.Boolean {
      return aggregation==AggregationType.TC_CHARGEGROUPS
    }
    
    // 'visible' attribute on TypeKeyCell (id=InvoiceItemsLV_Product_Cell) at Acct360InvoiceViewPopup.pcf: line 154, column 127
    function visible_92 () : java.lang.Boolean {
      return aggregation != AggregationType.TC_CATEGORIES && aggregation != AggregationType.TC_CONTEXTS
    }
    
    property get invoiceItem () : entity.InvoiceLineItem {
      return getIteratedValue(1) as entity.InvoiceLineItem
    }
    
    
  }
  
  
}