package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopBatchPaymentsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopBatchPaymentsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopBatchPaymentsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopBatchPaymentsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get desktopBatchPaymentsView () : gw.web.payment.batch.DesktopBatchPaymentsView {
      return getRequireValue("desktopBatchPaymentsView", 0) as gw.web.payment.batch.DesktopBatchPaymentsView
    }
    
    property set desktopBatchPaymentsView ($arg :  gw.web.payment.batch.DesktopBatchPaymentsView) {
      setRequireValue("desktopBatchPaymentsView", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopBatchPaymentsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends DesktopBatchPaymentsScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=CreateBatch) at DesktopBatchPaymentsScreen.pcf: line 46, column 13
    function action_14 () : void {
      pcf.NewBatchPaymentPage.go(desktopBatchPaymentsView.CurrencyToCreateNewBatchWith)
    }
    
    // 'action' attribute on ToolbarButton (id=CreateBatch) at DesktopBatchPaymentsScreen.pcf: line 46, column 13
    function action_dest_15 () : pcf.api.Destination {
      return pcf.NewBatchPaymentPage.createDestination(desktopBatchPaymentsView.CurrencyToCreateNewBatchWith)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=Post) at DesktopBatchPaymentsScreen.pcf: line 53, column 86
    function allCheckedRowsAction_17 (CheckedValues :  BatchPayment[], CheckedValuesErrors :  java.util.Map) : void {
      BatchPaymentsActionConfirmationPopup.push(CheckedValues, gw.web.payment.batch.BatchPaymentPopupActions.POST)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=Reverse) at DesktopBatchPaymentsScreen.pcf: line 60, column 89
    function allCheckedRowsAction_19 (CheckedValues :  BatchPayment[], CheckedValuesErrors :  java.util.Map) : void {
      BatchPaymentsActionConfirmationPopup.push(CheckedValues, gw.web.payment.batch.BatchPaymentPopupActions.REVERSE)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=Delete) at DesktopBatchPaymentsScreen.pcf: line 67, column 88
    function allCheckedRowsAction_21 (CheckedValues :  BatchPayment[], CheckedValuesErrors :  java.util.Map) : void {
      BatchPaymentsActionConfirmationPopup.push(CheckedValues, gw.web.payment.batch.BatchPaymentPopupActions.DELETE)
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=Post) at DesktopBatchPaymentsScreen.pcf: line 53, column 86
    function available_16 () : java.lang.Boolean {
      return perm.BatchPayment.modify or perm.BatchPayment.process
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=Reverse) at DesktopBatchPaymentsScreen.pcf: line 60, column 89
    function available_18 () : java.lang.Boolean {
      return perm.BatchPayment.modify
    }
    
    // 'available' attribute on ToolbarRangeInput (id=NewBatchPaymentCurrency_Input) at DesktopBatchPaymentsScreen.pcf: line 37, column 70
    function available_2 () : java.lang.Boolean {
      return perm.BatchPayment.process
    }
    
    // 'def' attribute on PanelRef at DesktopBatchPaymentsScreen.pcf: line 22, column 66
    function def_onEnter_0 (def :  pcf.DesktopBatchPaymentsSearchDV) : void {
      def.onEnter(batchSearchCriteria)
    }
    
    // 'def' attribute on PanelRef (id=DesktopBatchPaymentsLVRef) at DesktopBatchPaymentsScreen.pcf: line 26, column 40
    function def_onEnter_22 (def :  pcf.DesktopBatchPaymentsLV) : void {
      def.onEnter(batchPayments)
    }
    
    // 'def' attribute on PanelRef at DesktopBatchPaymentsScreen.pcf: line 22, column 66
    function def_refreshVariables_1 (def :  pcf.DesktopBatchPaymentsSearchDV) : void {
      def.refreshVariables(batchSearchCriteria)
    }
    
    // 'def' attribute on PanelRef (id=DesktopBatchPaymentsLVRef) at DesktopBatchPaymentsScreen.pcf: line 26, column 40
    function def_refreshVariables_23 (def :  pcf.DesktopBatchPaymentsLV) : void {
      def.refreshVariables(batchPayments)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=NewBatchPaymentCurrency_Input) at DesktopBatchPaymentsScreen.pcf: line 37, column 70
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      desktopBatchPaymentsView.CurrencyToCreateNewBatchWith = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'maxSearchResults' attribute on SearchPanel at DesktopBatchPaymentsScreen.pcf: line 20, column 82
    function maxSearchResults_24 () : java.lang.Object {
      return gw.api.system.BCConfigParameters.MaxSearchResults.Value
    }
    
    // 'searchCriteria' attribute on SearchPanel at DesktopBatchPaymentsScreen.pcf: line 20, column 82
    function searchCriteria_26 () : gw.search.BatchPaymentsSearchCriteria {
      return new gw.search.BatchPaymentsSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at DesktopBatchPaymentsScreen.pcf: line 20, column 82
    function search_25 () : java.lang.Object {
      return batchSearchCriteria.performSearch()
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=NewBatchPaymentCurrency_Input) at DesktopBatchPaymentsScreen.pcf: line 37, column 70
    function valueRange_7 () : java.lang.Object {
      return desktopBatchPaymentsView.AllCurrDerivedFromCurrencyMode
    }
    
    // 'value' attribute on ToolbarRangeInput (id=NewBatchPaymentCurrency_Input) at DesktopBatchPaymentsScreen.pcf: line 37, column 70
    function valueRoot_6 () : java.lang.Object {
      return desktopBatchPaymentsView
    }
    
    // 'value' attribute on ToolbarRangeInput (id=NewBatchPaymentCurrency_Input) at DesktopBatchPaymentsScreen.pcf: line 37, column 70
    function value_4 () : typekey.Currency {
      return desktopBatchPaymentsView.CurrencyToCreateNewBatchWith
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=NewBatchPaymentCurrency_Input) at DesktopBatchPaymentsScreen.pcf: line 37, column 70
    function verifyValueRangeIsAllowedType_8 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=NewBatchPaymentCurrency_Input) at DesktopBatchPaymentsScreen.pcf: line 37, column 70
    function verifyValueRangeIsAllowedType_8 ($$arg :  typekey.Currency[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=NewBatchPaymentCurrency_Input) at DesktopBatchPaymentsScreen.pcf: line 37, column 70
    function verifyValueRange_9 () : void {
      var __valueRangeArg = desktopBatchPaymentsView.AllCurrDerivedFromCurrencyMode
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_8(__valueRangeArg)
    }
    
    // 'visible' attribute on ToolbarRangeInput (id=NewBatchPaymentCurrency_Input) at DesktopBatchPaymentsScreen.pcf: line 37, column 70
    function visible_3 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    property get batchPayments () : gw.api.database.IQueryBeanResult<BatchPayment> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<BatchPayment>
    }
    
    property get batchSearchCriteria () : gw.search.BatchPaymentsSearchCriteria {
      return getCriteriaValue(1) as gw.search.BatchPaymentsSearchCriteria
    }
    
    property set batchSearchCriteria ($arg :  gw.search.BatchPaymentsSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}