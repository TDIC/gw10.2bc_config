package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/ProducerPaymentReversalConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerPaymentReversalConfirmationPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/ProducerPaymentReversalConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerPaymentReversalConfirmationPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (payment :  ProducerPaymentRecd) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=ProducerPaymentReversalConfirmationPopup) at ProducerPaymentReversalConfirmationPopup.pcf: line 10, column 79
    function beforeCommit_4 (pickedValue :  java.lang.Object) : void {
      payment.IncomingPayment.handlePaymentReversal(payment, reason)
    }
    
    // 'value' attribute on TypeKeyInput (id=Reason_Input) at ProducerPaymentReversalConfirmationPopup.pcf: line 36, column 56
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      reason = (__VALUE_TO_SET as typekey.PaymentReversalReason)
    }
    
    // 'label' attribute on Label (id=Confirmation) at ProducerPaymentReversalConfirmationPopup.pcf: line 29, column 120
    function label_0 () : java.lang.String {
      return DisplayKey.get("Web.PaymentReversalConfirmation.Confirmation", payment.Amount.render())
    }
    
    // 'value' attribute on TypeKeyInput (id=Reason_Input) at ProducerPaymentReversalConfirmationPopup.pcf: line 36, column 56
    function value_1 () : typekey.PaymentReversalReason {
      return reason
    }
    
    override property get CurrentLocation () : pcf.ProducerPaymentReversalConfirmationPopup {
      return super.CurrentLocation as pcf.ProducerPaymentReversalConfirmationPopup
    }
    
    property get payment () : ProducerPaymentRecd {
      return getVariableValue("payment", 0) as ProducerPaymentRecd
    }
    
    property set payment ($arg :  ProducerPaymentRecd) {
      setVariableValue("payment", 0, $arg)
    }
    
    property get reason () : PaymentReversalReason {
      return getVariableValue("reason", 0) as PaymentReversalReason
    }
    
    property set reason ($arg :  PaymentReversalReason) {
      setVariableValue("reason", 0, $arg)
    }
    
    
  }
  
  
}