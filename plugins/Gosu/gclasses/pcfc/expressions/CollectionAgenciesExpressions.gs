package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/businesssettings/CollectionAgencies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CollectionAgenciesExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/CollectionAgencies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CollectionAgenciesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=removeCollectionAgenciesButton) at CollectionAgencies.pcf: line 22, column 88
    function allCheckedRowsAction_1 (CheckedValues :  entity.CollectionAgency[], CheckedValuesErrors :  java.util.Map) : void {
      gw.api.web.collection.CollectionAgencyUtil.deleteAgencies(CheckedValues)
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=removeCollectionAgenciesButton) at CollectionAgencies.pcf: line 22, column 88
    function available_0 () : java.lang.Boolean {
      return perm.System.colagencydelete
    }
    
    // 'canVisit' attribute on Page (id=CollectionAgencies) at CollectionAgencies.pcf: line 8, column 70
    static function canVisit_10 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.colagencyview
    }
    
    // Page (id=CollectionAgencies) at CollectionAgencies.pcf: line 8, column 70
    static function parent_11 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CollectionAgencies.pcf: line 40, column 48
    function sortValue_2 (collectionAgency :  entity.CollectionAgency) : java.lang.Object {
      return collectionAgency.Name
    }
    
    // 'value' attribute on RowIterator at CollectionAgencies.pcf: line 31, column 89
    function value_9 () : gw.api.database.IQueryBeanResult<entity.CollectionAgency> {
      return gw.api.database.Query.make(entity.CollectionAgency).compare("Name", NotEquals, null).select()
    }
    
    override property get CurrentLocation () : pcf.CollectionAgencies {
      return super.CurrentLocation as pcf.CollectionAgencies
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/CollectionAgencies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CollectionAgenciesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at CollectionAgencies.pcf: line 40, column 48
    function action_4 () : void {
      CollectionAgencyDetail.go(collectionAgency)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at CollectionAgencies.pcf: line 40, column 48
    function action_dest_5 () : pcf.api.Destination {
      return pcf.CollectionAgencyDetail.createDestination(collectionAgency)
    }
    
    // 'condition' attribute on ToolbarFlag at CollectionAgencies.pcf: line 34, column 40
    function condition_3 () : java.lang.Boolean {
      return !collectionAgency.isAssigned()
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CollectionAgencies.pcf: line 40, column 48
    function valueRoot_7 () : java.lang.Object {
      return collectionAgency
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CollectionAgencies.pcf: line 40, column 48
    function value_6 () : java.lang.String {
      return collectionAgency.Name
    }
    
    property get collectionAgency () : entity.CollectionAgency {
      return getIteratedValue(1) as entity.CollectionAgency
    }
    
    
  }
  
  
}