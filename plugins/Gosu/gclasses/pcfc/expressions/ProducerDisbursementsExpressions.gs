package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerDisbursements.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerDisbursementsExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerDisbursements.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerDisbursementsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'afterReturnFromPopup' attribute on Page (id=ProducerDisbursements) at ProducerDisbursements.pcf: line 11, column 73
    function afterReturnFromPopup_4 (popupCommitted :  boolean) : void {
      if (popupCommitted) CurrentLocation.commit()
    }
    
    // 'canVisit' attribute on Page (id=ProducerDisbursements) at ProducerDisbursements.pcf: line 11, column 73
    static function canVisit_5 (producer :  Producer) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctdisbview
    }
    
    // 'def' attribute on PanelRef at ProducerDisbursements.pcf: line 29, column 52
    function def_onEnter_2 (def :  pcf.DisbursementsLV) : void {
      def.onEnter(disbursements, null)
    }
    
    // 'def' attribute on PanelRef at ProducerDisbursements.pcf: line 29, column 52
    function def_refreshVariables_3 (def :  pcf.DisbursementsLV) : void {
      def.refreshVariables(disbursements, null)
    }
    
    // 'initialValue' attribute on Variable at ProducerDisbursements.pcf: line 20, column 56
    function initialValue_0 () : gw.api.web.disbursement.DisbursementUtil {
      return new gw.api.web.disbursement.DisbursementUtil()
    }
    
    // 'initialValue' attribute on Variable at ProducerDisbursements.pcf: line 25, column 57
    function initialValue_1 () : java.util.List<entity.Disbursement> {
      return disbursementUtil.getProducerDisbursements(producer)
    }
    
    // Page (id=ProducerDisbursements) at ProducerDisbursements.pcf: line 11, column 73
    static function parent_6 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    override property get CurrentLocation () : pcf.ProducerDisbursements {
      return super.CurrentLocation as pcf.ProducerDisbursements
    }
    
    property get disbursementUtil () : gw.api.web.disbursement.DisbursementUtil {
      return getVariableValue("disbursementUtil", 0) as gw.api.web.disbursement.DisbursementUtil
    }
    
    property set disbursementUtil ($arg :  gw.api.web.disbursement.DisbursementUtil) {
      setVariableValue("disbursementUtil", 0, $arg)
    }
    
    property get disbursements () : java.util.List<entity.Disbursement> {
      return getVariableValue("disbursements", 0) as java.util.List<entity.Disbursement>
    }
    
    property set disbursements ($arg :  java.util.List<entity.Disbursement>) {
      setVariableValue("disbursements", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}