package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/NoPermissionsPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NoPermissionsPageExpressions {
  @javax.annotation.Generated("config/web/pcf/NoPermissionsPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NoPermissionsPageExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'parent' attribute on Page (id=NoPermissionsPage) at NoPermissionsPage.pcf: line 9, column 69
    static function parent_0 () : pcf.api.Destination {
      return pcf.FirstPage.createDestination()
    }
    
    override property get CurrentLocation () : pcf.NoPermissionsPage {
      return super.CurrentLocation as pcf.NoPermissionsPage
    }
    
    
  }
  
  
}