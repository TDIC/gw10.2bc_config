package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.document.SymbolProvider
uses gw.document.SimpleSymbol
uses gw.document.GosuCaseInsensitiveBackwardsCompatibleSymbolProvider
@javax.annotation.Generated("config/web/pcf/activity/ActivityDetailDV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ActivityDetailDV_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/activity/ActivityDetailDV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ActivityDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=ActivityDetailDV_AssignActivity_PickerButton) at AssigneeWidget.xml: line 7, column 25
    function action_27 () : void {
      AssigneePickerPopup.push(new gw.api.assignment.AssigneePicker(entity.Activity.Type.isAssignableFrom(typeof (activity))))
    }
    
    // 'action' attribute on MenuItem (id=ActivityDetailDV_AssignActivity_PickerButton) at AssigneeWidget.xml: line 7, column 25
    function action_dest_28 () : pcf.api.Destination {
      return pcf.AssigneePickerPopup.createDestination(new gw.api.assignment.AssigneePicker(entity.Activity.Type.isAssignableFrom(typeof (activity))))
    }
    
    // 'def' attribute on InputSetRef at ActivityDetailDV.default.pcf: line 52, column 47
    function def_onEnter_25 (def :  pcf.EdgeEntitiesInputSet) : void {
      def.onEnter(activity)
    }
    
    // 'def' attribute on InputSetRef at ActivityDetailDV.default.pcf: line 52, column 47
    function def_refreshVariables_26 (def :  pcf.EdgeEntitiesInputSet) : void {
      def.refreshVariables(activity)
    }
    
    // 'value' attribute on TypeKeyInput (id=ActivityDetailDV_Priority_Input) at ActivityDetailDV.default.pcf: line 38, column 39
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      activity.Priority = (__VALUE_TO_SET as typekey.Priority)
    }
    
    // 'value' attribute on DateInput (id=ActivityDetailDV_TargetDate_Input) at ActivityDetailDV.default.pcf: line 44, column 38
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      activity.TargetDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=ActivityDetailDV_Subject_Input) at ActivityDetailDV.default.pcf: line 26, column 35
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      activity.Subject = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=ActivityDetailDV_EscalationDate_Input) at ActivityDetailDV.default.pcf: line 50, column 42
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      activity.EscalationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on AssigneeInput (id=ActivityDetailDV_AssignActivity_Input) at AssigneeWidget.xml: line 7, column 25
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      AssigneeHolder[0] = (__VALUE_TO_SET as gw.api.assignment.Assignee)
    }
    
    // 'value' attribute on TextAreaInput (id=ActivityDetailDV_Description_Input) at ActivityDetailDV.default.pcf: line 32, column 39
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      activity.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on AssigneeInput (id=ActivityDetailDV_AssignActivity_Input) at AssigneeWidget.xml: line 7, column 25
    function editable_29 () : java.lang.Boolean {
      return activity.canAssign()
    }
    
    // 'initialValue' attribute on Variable at ActivityDetailDV.default.pcf: line 17, column 42
    function initialValue_0 () : gw.document.SymbolProvider {
      return createSymbolProvider()
    }
    
    // 'validationExpression' attribute on DateInput (id=ActivityDetailDV_TargetDate_Input) at ActivityDetailDV.default.pcf: line 44, column 38
    function validationExpression_13 () : java.lang.Object {
      return gw.activity.ActivityMethods.validateTargetDate(activity)
    }
    
    // 'validationExpression' attribute on DateInput (id=ActivityDetailDV_EscalationDate_Input) at ActivityDetailDV.default.pcf: line 50, column 42
    function validationExpression_19 () : java.lang.Object {
      return gw.activity.ActivityMethods.validateEscalationDate(activity)
    }
    
    // 'valueRange' attribute on AssigneeInput (id=ActivityDetailDV_AssignActivity_Input) at AssigneeWidget.xml: line 7, column 25
    function valueRange_33 () : java.lang.Object {
      return activity.SuggestedAssigneesIncludingQueues_Ext
    }
    
    // 'valueRange' attribute on RangeInput (id=ActivityDetailDV_Status_Input) at ActivityDetailDV.default.pcf: line 79, column 45
    function valueRange_44 () : java.lang.Object {
      return activity.BCStatusRange
    }
    
    // 'value' attribute on TextInput (id=ActivityDetailDV_Subject_Input) at ActivityDetailDV.default.pcf: line 26, column 35
    function valueRoot_3 () : java.lang.Object {
      return activity
    }
    
    // 'value' attribute on TextInput (id=ActivityDetailDV_Subject_Input) at ActivityDetailDV.default.pcf: line 26, column 35
    function value_1 () : java.lang.String {
      return activity.Subject
    }
    
    // 'value' attribute on DateInput (id=ActivityDetailDV_TargetDate_Input) at ActivityDetailDV.default.pcf: line 44, column 38
    function value_14 () : java.util.Date {
      return activity.TargetDate
    }
    
    // 'value' attribute on DateInput (id=ActivityDetailDV_EscalationDate_Input) at ActivityDetailDV.default.pcf: line 50, column 42
    function value_20 () : java.util.Date {
      return activity.EscalationDate
    }
    
    // 'value' attribute on AssigneeInput (id=ActivityDetailDV_AssignActivity_Input) at AssigneeWidget.xml: line 7, column 25
    function value_31 () : gw.api.assignment.Assignee {
      return AssigneeHolder[0]
    }
    
    // 'value' attribute on RangeInput (id=ActivityDetailDV_Status_Input) at ActivityDetailDV.default.pcf: line 79, column 45
    function value_42 () : typekey.ActivityStatus {
      return activity.Status
    }
    
    // 'value' attribute on TextInput (id=ActivityDetailDV_CompletedBy_Input) at ActivityDetailDV.default.pcf: line 85, column 41
    function value_49 () : entity.User {
      return activity.CloseUser
    }
    
    // 'value' attribute on TextAreaInput (id=ActivityDetailDV_Description_Input) at ActivityDetailDV.default.pcf: line 32, column 39
    function value_5 () : java.lang.String {
      return activity.Description
    }
    
    // 'value' attribute on DateInput (id=ActivityDetailDV_CompletionDate_Input) at ActivityDetailDV.default.pcf: line 90, column 41
    function value_54 () : java.util.Date {
      return activity.CloseDate
    }
    
    // 'value' attribute on TypeKeyInput (id=ActivityDetailDV_Priority_Input) at ActivityDetailDV.default.pcf: line 38, column 39
    function value_9 () : typekey.Priority {
      return activity.Priority
    }
    
    // 'valueRange' attribute on AssigneeInput (id=ActivityDetailDV_AssignActivity_Input) at AssigneeWidget.xml: line 7, column 25
    function verifyValueRangeIsAllowedType_34 ($$arg :  gw.api.assignment.Assignee[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on AssigneeInput (id=ActivityDetailDV_AssignActivity_Input) at AssigneeWidget.xml: line 7, column 25
    function verifyValueRangeIsAllowedType_34 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ActivityDetailDV_Status_Input) at ActivityDetailDV.default.pcf: line 79, column 45
    function verifyValueRangeIsAllowedType_45 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ActivityDetailDV_Status_Input) at ActivityDetailDV.default.pcf: line 79, column 45
    function verifyValueRangeIsAllowedType_45 ($$arg :  typekey.ActivityStatus[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on AssigneeInput (id=ActivityDetailDV_AssignActivity_Input) at AssigneeWidget.xml: line 7, column 25
    function verifyValueRange_35 () : void {
      var __valueRangeArg = activity.SuggestedAssigneesIncludingQueues_Ext
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_34(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=ActivityDetailDV_Status_Input) at ActivityDetailDV.default.pcf: line 79, column 45
    function verifyValueRange_46 () : void {
      var __valueRangeArg = activity.BCStatusRange
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_45(__valueRangeArg)
    }
    
    // 'visible' attribute on AssigneeInput (id=ActivityDetailDV_AssignActivity_Input) at AssigneeWidget.xml: line 7, column 25
    function visible_30 () : java.lang.Boolean {
      return !gw.api.web.activity.ActivityUtil.isShared(activity)
    }
    
    // 'visible' attribute on TextInput (id=ActivityDetailDV_CompletedBy_Input) at ActivityDetailDV.default.pcf: line 85, column 41
    function visible_48 () : java.lang.Boolean {
      return activity.IsCompleted
    }
    
    property get AssigneeHolder () : gw.api.assignment.Assignee[] {
      return getRequireValue("AssigneeHolder", 0) as gw.api.assignment.Assignee[]
    }
    
    property set AssigneeHolder ($arg :  gw.api.assignment.Assignee[]) {
      setRequireValue("AssigneeHolder", 0, $arg)
    }
    
    property get activity () : Activity {
      return getRequireValue("activity", 0) as Activity
    }
    
    property set activity ($arg :  Activity) {
      setRequireValue("activity", 0, $arg)
    }
    
    property get symbolProvider () : gw.document.SymbolProvider {
      return getVariableValue("symbolProvider", 0) as gw.document.SymbolProvider
    }
    
    property set symbolProvider ($arg :  gw.document.SymbolProvider) {
      setVariableValue("symbolProvider", 0, $arg)
    }
    
    
    
          function createSymbolProvider(): SymbolProvider {
            return new GosuCaseInsensitiveBackwardsCompatibleSymbolProvider({
              "Activity"->activity,
              "AssigneeHolder"->AssigneeHolder
            })
          }
    
          function getDisplayName(templateFilename: String): String {
            if (templateFilename == null) {
              return null
            }
            var ets = gw.plugin.Plugins.get(gw.plugin.email.IEmailTemplateSource)
            return ets.getEmailTemplate(templateFilename).getName();
          }
    
    
  }
  
  
}