package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.web.invoice.InvoiceStreamView
uses com.google.common.collect.Maps
uses java.util.Map
@javax.annotation.Generated("config/web/pcf/account/AccountDetailInvoiceStreams.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailInvoiceStreamsExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailInvoiceStreams.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailInvoiceStreamsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'afterCommit' attribute on Page (id=AccountDetailInvoiceStreams) at AccountDetailInvoiceStreams.pcf: line 11, column 79
    function afterCommit_14 (TopLocation :  pcf.api.Location) : void {
      remapViews()
    }
    
    // 'canEdit' attribute on Page (id=AccountDetailInvoiceStreams) at AccountDetailInvoiceStreams.pcf: line 11, column 79
    function canEdit_15 () : java.lang.Boolean {
      return perm.System.invcstrmedit
    }
    
    // 'canVisit' attribute on Page (id=AccountDetailInvoiceStreams) at AccountDetailInvoiceStreams.pcf: line 11, column 79
    static function canVisit_16 (account :  Account) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctinvcview
    }
    
    // 'updateConfirmMessage' attribute on EditButtons at AccountDetailInvoiceStreams.pcf: line 33, column 112
    function confirmMessage_3 () : java.lang.String {
      return gw.api.util.EquityValidationHelper.isEquityViolatedCheckAllPolicies(account)
    }
    
    // 'initialValue' attribute on Variable at AccountDetailInvoiceStreams.pcf: line 17, column 30
    function initialValue_0 () : java.util.Date {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'initialValue' attribute on Variable at AccountDetailInvoiceStreams.pcf: line 24, column 39
    function initialValue_1 () : entity.AccountInvoice[] {
      return account.InvoicesSortedByDate
    }
    
    // 'initialValue' attribute on Variable at AccountDetailInvoiceStreams.pcf: line 28, column 54
    function initialValue_2 () : gw.api.web.invoice.InvoiceStreamView[] {
      return remapViews()
    }
    
    // EditButtons at AccountDetailInvoiceStreams.pcf: line 33, column 112
    function label_4 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // Page (id=AccountDetailInvoiceStreams) at AccountDetailInvoiceStreams.pcf: line 11, column 79
    static function parent_17 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.AccountDetailInvoiceStreams {
      return super.CurrentLocation as pcf.AccountDetailInvoiceStreams
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get invoiceStreamViews () : gw.api.web.invoice.InvoiceStreamView[] {
      return getVariableValue("invoiceStreamViews", 0) as gw.api.web.invoice.InvoiceStreamView[]
    }
    
    property set invoiceStreamViews ($arg :  gw.api.web.invoice.InvoiceStreamView[]) {
      setVariableValue("invoiceStreamViews", 0, $arg)
    }
    
    property get invoices () : entity.AccountInvoice[] {
      return getVariableValue("invoices", 0) as entity.AccountInvoice[]
    }
    
    property set invoices ($arg :  entity.AccountInvoice[]) {
      setVariableValue("invoices", 0, $arg)
    }
    
    property get today () : java.util.Date {
      return getVariableValue("today", 0) as java.util.Date
    }
    
    property set today ($arg :  java.util.Date) {
      setVariableValue("today", 0, $arg)
    }
    
    
    
    function createNewStream(): InvoiceStreamView {
      var unappliedFund = account.DefaultUnappliedFund
      var addedInvoiceStream = gw.api.domain.invoice.InvoiceStreamFactory.createInvoiceStreamFor(unappliedFund, Periodicity.TC_MONTHLY)
      invoiceStreamViews = remapViews()
      for (var newView in invoiceStreamViews) {
        if (newView.InvoiceStream.equals(addedInvoiceStream)) {
          return newView
        }
      }
      return null
    }
    
    function remapViews(): InvoiceStreamView[] {
      var viewMap = Maps.newHashMap() as Map <InvoiceStream, InvoiceStreamView>
      for (var view in invoiceStreamViews) {
        viewMap.put(view.InvoiceStream, view)
      }
      return account.InvoiceStreams.map(\elt -> {
        var view = viewMap.get(elt)
        if (view != null) {
          return view
        } else {
          return new InvoiceStreamView(elt)
        }
      })
    }
        
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailInvoiceStreams.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceStreamsDetailPanelExpressionsImpl extends AccountDetailInvoiceStreamsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at AccountDetailInvoiceStreams.pcf: line 71, column 79
    function def_onEnter_12 (def :  pcf.AccountInvoiceStreamDetailDV) : void {
      def.onEnter(account, invoiceStreamView)
    }
    
    // 'def' attribute on PanelRef at AccountDetailInvoiceStreams.pcf: line 71, column 79
    function def_refreshVariables_13 (def :  pcf.AccountInvoiceStreamDetailDV) : void {
      def.refreshVariables(account, invoiceStreamView)
    }
    
    // 'value' attribute on TextCell (id=InvoiceStreamDisplayName_Cell) at AccountDetailInvoiceStreams.pcf: line 60, column 62
    function sortValue_7 (eachInvoiceStreamView :  gw.api.web.invoice.InvoiceStreamView) : java.lang.Object {
      return eachInvoiceStreamView.DisplayName
    }
    
    // 'toCreateAndAdd' attribute on AddButton (id=AddInvoiceStreamButton) at AccountDetailInvoiceStreams.pcf: line 46, column 37
    function toCreateAndAdd_6 (CheckedValues :  Object[]) : java.lang.Object {
      return createNewStream()
    }
    
    // 'value' attribute on RowIterator at AccountDetailInvoiceStreams.pcf: line 54, column 66
    function value_11 () : gw.api.web.invoice.InvoiceStreamView[] {
      return invoiceStreamViews
    }
    
    // 'visible' attribute on AddButton (id=AddInvoiceStreamButton) at AccountDetailInvoiceStreams.pcf: line 46, column 37
    function visible_5 () : java.lang.Boolean {
      return InEditMode
    }
    
    property get invoiceStreamView () : gw.api.web.invoice.InvoiceStreamView {
      return getCurrentSelection(1) as gw.api.web.invoice.InvoiceStreamView
    }
    
    property set invoiceStreamView ($arg :  gw.api.web.invoice.InvoiceStreamView) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailInvoiceStreams.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends InvoiceStreamsDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=InvoiceStreamDisplayName_Cell) at AccountDetailInvoiceStreams.pcf: line 60, column 62
    function valueRoot_9 () : java.lang.Object {
      return eachInvoiceStreamView
    }
    
    // 'value' attribute on TextCell (id=InvoiceStreamDisplayName_Cell) at AccountDetailInvoiceStreams.pcf: line 60, column 62
    function value_8 () : java.lang.String {
      return eachInvoiceStreamView.DisplayName
    }
    
    property get eachInvoiceStreamView () : gw.api.web.invoice.InvoiceStreamView {
      return getIteratedValue(2) as gw.api.web.invoice.InvoiceStreamView
    }
    
    
  }
  
  
}