package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ProducerSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerSearchPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/search/ProducerSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerSearchPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'def' attribute on ScreenRef at ProducerSearchPopup.pcf: line 12, column 49
    function def_onEnter_0 (def :  pcf.ProducerSearchScreen) : void {
      def.onEnter(false, false)
    }
    
    // 'def' attribute on ScreenRef at ProducerSearchPopup.pcf: line 12, column 49
    function def_refreshVariables_1 (def :  pcf.ProducerSearchScreen) : void {
      def.refreshVariables(false, false)
    }
    
    override property get CurrentLocation () : pcf.ProducerSearchPopup {
      return super.CurrentLocation as pcf.ProducerSearchPopup
    }
    
    
  }
  
  
}