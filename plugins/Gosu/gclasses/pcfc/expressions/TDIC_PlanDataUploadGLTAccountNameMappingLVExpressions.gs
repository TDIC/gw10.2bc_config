package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_PlanDataUploadGLTAccountNameMappingLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_PlanDataUploadGLTAccountNameMappingLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 17, column 114
    function highlighted_29 () : java.lang.Boolean {
      return (glTAccountMapping.Error or glTAccountMapping.Skipped)  && processor.LoadCompleted
    }
    
    // 'value' attribute on TypeKeyCell (id=transactionType_Cell) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 28, column 44
    function valueRoot_12 () : java.lang.Object {
      return glTAccountMapping
    }
    
    // 'value' attribute on TypeKeyCell (id=transactionType_Cell) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 28, column 44
    function value_11 () : typekey.Transaction {
      return glTAccountMapping.TransactionType
    }
    
    // 'value' attribute on TextCell (id=tAccountType_Cell) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 33, column 43
    function value_14 () : typekey.LedgerSide {
      return glTAccountMapping.LineItemType
    }
    
    // 'value' attribute on TextCell (id=futureTerm_Cell) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 38, column 42
    function value_17 () : java.lang.Boolean {
      return glTAccountMapping.FutureTerm
    }
    
    // 'value' attribute on TypeKeyCell (id=invoiceStatus_Cell) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 43, column 46
    function value_20 () : typekey.InvoiceStatus {
      return glTAccountMapping.InvoiceStatus
    }
    
    // 'value' attribute on TypeKeyCell (id=GWTAccountName_Cell) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 48, column 56
    function value_23 () : typekey.GLOriginalTAccount_TDIC {
      return glTAccountMapping.OriginalTAccountName
    }
    
    // 'value' attribute on TextCell (id=tAccountName_Cell) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 53, column 41
    function value_26 () : java.lang.String {
      return glTAccountMapping.MappedTAccountName
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 23, column 46
    function value_8 () : java.lang.String {
      return processor.getLoadStatus(glTAccountMapping)
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 23, column 46
    function visible_9 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get glTAccountMapping () : tdic.util.dataloader.data.plandata.GLTAccountNameMappingData {
      return getIteratedValue(1) as tdic.util.dataloader.data.plandata.GLTAccountNameMappingData
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_PlanDataUploadGLTAccountNameMappingLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 23, column 46
    function sortValue_0 (glTAccountMapping :  tdic.util.dataloader.data.plandata.GLTAccountNameMappingData) : java.lang.Object {
      return processor.getLoadStatus(glTAccountMapping)
    }
    
    // 'value' attribute on TypeKeyCell (id=transactionType_Cell) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 28, column 44
    function sortValue_2 (glTAccountMapping :  tdic.util.dataloader.data.plandata.GLTAccountNameMappingData) : java.lang.Object {
      return glTAccountMapping.TransactionType
    }
    
    // 'value' attribute on TextCell (id=tAccountType_Cell) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 33, column 43
    function sortValue_3 (glTAccountMapping :  tdic.util.dataloader.data.plandata.GLTAccountNameMappingData) : java.lang.Object {
      return glTAccountMapping.LineItemType
    }
    
    // 'value' attribute on TextCell (id=futureTerm_Cell) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 38, column 42
    function sortValue_4 (glTAccountMapping :  tdic.util.dataloader.data.plandata.GLTAccountNameMappingData) : java.lang.Object {
      return glTAccountMapping.FutureTerm
    }
    
    // 'value' attribute on TypeKeyCell (id=invoiceStatus_Cell) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 43, column 46
    function sortValue_5 (glTAccountMapping :  tdic.util.dataloader.data.plandata.GLTAccountNameMappingData) : java.lang.Object {
      return glTAccountMapping.InvoiceStatus
    }
    
    // 'value' attribute on TypeKeyCell (id=GWTAccountName_Cell) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 48, column 56
    function sortValue_6 (glTAccountMapping :  tdic.util.dataloader.data.plandata.GLTAccountNameMappingData) : java.lang.Object {
      return glTAccountMapping.OriginalTAccountName
    }
    
    // 'value' attribute on TextCell (id=tAccountName_Cell) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 53, column 41
    function sortValue_7 (glTAccountMapping :  tdic.util.dataloader.data.plandata.GLTAccountNameMappingData) : java.lang.Object {
      return glTAccountMapping.MappedTAccountName
    }
    
    // 'value' attribute on RowIterator (id=GLTAccountMapping) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 15, column 107
    function value_30 () : java.util.ArrayList<tdic.util.dataloader.data.plandata.GLTAccountNameMappingData> {
      return processor.GLTAccountNameMappingArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at TDIC_PlanDataUploadGLTAccountNameMappingLV.pcf: line 23, column 46
    function visible_1 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}