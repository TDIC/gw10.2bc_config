package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentAdvanceDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewCommissionPaymentAdvanceDetailsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentAdvanceDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewCommissionPaymentAdvanceDetailsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentAdvanceDetailsScreen.pcf: line 14, column 66
    function def_onEnter_0 (def :  pcf.NewCommissionPaymentAdvanceDV) : void {
      def.onEnter(advancePayment, true)
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentAdvanceDetailsScreen.pcf: line 16, column 63
    function def_onEnter_2 (def :  pcf.NewCommissionPaymentTimeDV) : void {
      def.onEnter(advancePayment, true)
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentAdvanceDetailsScreen.pcf: line 14, column 66
    function def_refreshVariables_1 (def :  pcf.NewCommissionPaymentAdvanceDV) : void {
      def.refreshVariables(advancePayment, true)
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentAdvanceDetailsScreen.pcf: line 16, column 63
    function def_refreshVariables_3 (def :  pcf.NewCommissionPaymentTimeDV) : void {
      def.refreshVariables(advancePayment, true)
    }
    
    property get advancePayment () : AdvanceCmsnPayment {
      return getRequireValue("advancePayment", 0) as AdvanceCmsnPayment
    }
    
    property set advancePayment ($arg :  AdvanceCmsnPayment) {
      setRequireValue("advancePayment", 0, $arg)
    }
    
    
  }
  
  
}