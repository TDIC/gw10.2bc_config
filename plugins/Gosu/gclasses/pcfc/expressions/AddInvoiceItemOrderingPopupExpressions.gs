package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/allocationplan/AddInvoiceItemOrderingPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AddInvoiceItemOrderingPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/allocationplan/AddInvoiceItemOrderingPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AddInvoiceItemOrderingPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (paymentAllocationPlan :  PaymentAllocationPlan) : int {
      return 0
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=AddSelectedInvoiceItemOrderings) at AddInvoiceItemOrderingPopup.pcf: line 21, column 84
    function allCheckedRowsAction_0 (CheckedValues :  typekey.InvoiceItemOrderingType[], CheckedValuesErrors :  java.util.Map) : void {
      CheckedValues.each(\ ordering -> paymentAllocationPlan.addInvoiceItemOrderingFor(ordering)); CurrentLocation.commit()
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemOrderingName_Cell) at AddInvoiceItemOrderingPopup.pcf: line 37, column 56
    function sortValue_1 (invoiceItemOrdering :  typekey.InvoiceItemOrderingType) : java.lang.Object {
      return invoiceItemOrdering.DisplayName
    }
    
    // 'value' attribute on TextCell (id=DistributionFilterDescription_Cell) at AddInvoiceItemOrderingPopup.pcf: line 41, column 56
    function sortValue_2 (invoiceItemOrdering :  typekey.InvoiceItemOrderingType) : java.lang.Object {
      return invoiceItemOrdering.Description
    }
    
    // 'value' attribute on RowIterator (id=InvoiceItemOrderings) at AddInvoiceItemOrderingPopup.pcf: line 32, column 77
    function value_10 () : java.util.List<typekey.InvoiceItemOrderingType> {
      return OrderingsNotOnPlan
    }
    
    override property get CurrentLocation () : pcf.AddInvoiceItemOrderingPopup {
      return super.CurrentLocation as pcf.AddInvoiceItemOrderingPopup
    }
    
    property get paymentAllocationPlan () : PaymentAllocationPlan {
      return getVariableValue("paymentAllocationPlan", 0) as PaymentAllocationPlan
    }
    
    property set paymentAllocationPlan ($arg :  PaymentAllocationPlan) {
      setVariableValue("paymentAllocationPlan", 0, $arg)
    }
    
    property get OrderingsNotOnPlan(): java.util.List<InvoiceItemOrderingType> {
        return InvoiceItemOrderingType.getTypeKeys(false).where(\ ordering -> paymentAllocationPlan.getInvoiceItemOrderingFor(ordering) == null)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/allocationplan/AddInvoiceItemOrderingPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AddInvoiceItemOrderingPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'checkBoxVisible' attribute on RowIterator (id=InvoiceItemOrderings) at AddInvoiceItemOrderingPopup.pcf: line 32, column 77
    function checkBoxVisible_9 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemOrderingName_Cell) at AddInvoiceItemOrderingPopup.pcf: line 37, column 56
    function valueRoot_4 () : java.lang.Object {
      return invoiceItemOrdering
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemOrderingName_Cell) at AddInvoiceItemOrderingPopup.pcf: line 37, column 56
    function value_3 () : java.lang.String {
      return invoiceItemOrdering.DisplayName
    }
    
    // 'value' attribute on TextCell (id=DistributionFilterDescription_Cell) at AddInvoiceItemOrderingPopup.pcf: line 41, column 56
    function value_6 () : java.lang.String {
      return invoiceItemOrdering.Description
    }
    
    property get invoiceItemOrdering () : typekey.InvoiceItemOrderingType {
      return getIteratedValue(1) as typekey.InvoiceItemOrderingType
    }
    
    
  }
  
  
}