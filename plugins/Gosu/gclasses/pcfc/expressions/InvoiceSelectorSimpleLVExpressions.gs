package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/InvoiceSelectorSimpleLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceSelectorSimpleLVExpressions {
  @javax.annotation.Generated("config/web/pcf/invoice/InvoiceSelectorSimpleLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceSelectorSimpleLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on TextCell (id=InvoiceStatus_Cell) at InvoiceSelectorSimpleLV.pcf: line 23, column 40
    function sortValue_0 (invoice :  entity.Invoice) : java.lang.Object {
      return invoice.Status
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at InvoiceSelectorSimpleLV.pcf: line 27, column 38
    function sortValue_1 (invoice :  entity.Invoice) : java.lang.Object {
      return invoice.EventDate
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at InvoiceSelectorSimpleLV.pcf: line 31, column 36
    function sortValue_2 (invoice :  entity.Invoice) : java.lang.Object {
      return invoice.DueDate
    }
    
    // 'value' attribute on TextCell (id=InvoiceNumber_Cell) at InvoiceSelectorSimpleLV.pcf: line 35, column 42
    function sortValue_3 (invoice :  entity.Invoice) : java.lang.Object {
      return invoice.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=InvoiceStream_Cell) at InvoiceSelectorSimpleLV.pcf: line 40, column 45
    function sortValue_4 (invoice :  entity.Invoice) : java.lang.Object {
      return invoice.InvoiceStream
    }
    
    // 'value' attribute on BooleanRadioCell (id=InvoiceAdHoc_Cell) at InvoiceSelectorSimpleLV.pcf: line 44, column 38
    function sortValue_5 (invoice :  entity.Invoice) : java.lang.Object {
      return invoice.isAdHoc()
    }
    
    // 'value' attribute on RowIterator at InvoiceSelectorSimpleLV.pcf: line 17, column 56
    function value_25 () : java.util.List<entity.Invoice> {
      return payer.InvoicesSortedByEventDate
    }
    
    property get payer () : gw.api.domain.invoice.InvoicePayer {
      return getRequireValue("payer", 0) as gw.api.domain.invoice.InvoicePayer
    }
    
    property set payer ($arg :  gw.api.domain.invoice.InvoicePayer) {
      setRequireValue("payer", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/invoice/InvoiceSelectorSimpleLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends InvoiceSelectorSimpleLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'canPick' attribute on RowIterator at InvoiceSelectorSimpleLV.pcf: line 17, column 56
    function canPick_23 () : java.lang.Boolean {
      return !invoice.Frozen
    }
    
    // 'value' attribute on TextCell (id=InvoiceStatus_Cell) at InvoiceSelectorSimpleLV.pcf: line 23, column 40
    function valueRoot_7 () : java.lang.Object {
      return invoice
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at InvoiceSelectorSimpleLV.pcf: line 31, column 36
    function value_12 () : java.util.Date {
      return invoice.DueDate
    }
    
    // 'value' attribute on TextCell (id=InvoiceNumber_Cell) at InvoiceSelectorSimpleLV.pcf: line 35, column 42
    function value_15 () : java.lang.String {
      return invoice.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=InvoiceStream_Cell) at InvoiceSelectorSimpleLV.pcf: line 40, column 45
    function value_18 () : entity.InvoiceStream {
      return invoice.InvoiceStream
    }
    
    // 'value' attribute on BooleanRadioCell (id=InvoiceAdHoc_Cell) at InvoiceSelectorSimpleLV.pcf: line 44, column 38
    function value_21 () : java.lang.Boolean {
      return invoice.isAdHoc()
    }
    
    // 'value' attribute on TextCell (id=InvoiceStatus_Cell) at InvoiceSelectorSimpleLV.pcf: line 23, column 40
    function value_6 () : java.lang.String {
      return invoice.StatusForUI
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at InvoiceSelectorSimpleLV.pcf: line 27, column 38
    function value_9 () : java.util.Date {
      return invoice.EventDate
    }
    
    property get invoice () : entity.Invoice {
      return getIteratedValue(1) as entity.Invoice
    }
    
    
  }
  
  
}