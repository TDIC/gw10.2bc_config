package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/CurrentDateInfoBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CurrentDateInfoBarExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/CurrentDateInfoBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CurrentDateInfoBarExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on InfoBarElement (id=BuildInfo) at CurrentDateInfoBar.pcf: line 14, column 93
    function label_2 () : java.lang.Object {
      return DisplayKey.get("TDIC.Web.TabBar.TestEnvInfo",gw.api.system.server.ServerUtil.getEnv().toUpperCase())
    }
    
    // 'value' attribute on InfoBarElement (id=CurrentDate) at CurrentDateInfoBar.pcf: line 10, column 51
    function value_0 () : java.lang.Object {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'visible' attribute on InfoBarElement (id=BuildInfo) at CurrentDateInfoBar.pcf: line 14, column 93
    function visible_1 () : java.lang.Boolean {
      return gw.api.system.server.ServerUtil.getEnv().toUpperCase() != "PROD"
    }
    
    
  }
  
  
}