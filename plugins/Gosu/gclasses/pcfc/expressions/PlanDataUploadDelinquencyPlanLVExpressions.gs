package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadDelinquencyPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanDataUploadDelinquencyPlanLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadDelinquencyPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PlanDataUploadDelinquencyPlanLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at PlanDataUploadDelinquencyPlanLV.pcf: line 17, column 110
    function highlighted_113 () : java.lang.Boolean {
      return (delinquencyPlan.Error or delinquencyPlan.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=exitDelinquency_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 94, column 45
    function label_103 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.ExitDelinquency")
    }
    
    // 'label' attribute on TypeKeyCell (id=applicableSegments_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 99, column 51
    function label_108 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.ApplicableSegments")
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 23, column 46
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 28, column 41
    function label_38 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.PublicID")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 34, column 41
    function label_43 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.PlanName")
    }
    
    // 'label' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 39, column 39
    function label_48 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.EffectiveDate")
    }
    
    // 'label' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 44, column 39
    function label_53 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.ExpirationDate")
    }
    
    // 'label' attribute on TypeKeyCell (id=cancellationTarget_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 49, column 51
    function label_58 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.CancellationTarget")
    }
    
    // 'label' attribute on BooleanRadioCell (id=holdInvoicingOnTargetedPolicies_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 54, column 42
    function label_63 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.HoldInvoicing")
    }
    
    // 'label' attribute on TextCell (id=GracePeriodDays_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 59, column 42
    function label_68 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.GracePeriodDays")
    }
    
    // 'label' attribute on TextCell (id=lateFee_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 64, column 45
    function label_73 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.LateFee")
    }
    
    // 'label' attribute on TextCell (id=reinstatementFee_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 69, column 45
    function label_78 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.ReinstatementFee")
    }
    
    // 'label' attribute on TextCell (id=writeoffThreshold_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 74, column 45
    function label_83 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.WriteoffThreshold")
    }
    
    // 'label' attribute on TextCell (id=enterDelinquencyThreshold_Account_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 79, column 45
    function label_88 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.EnterDelinquencyThreshold_Account")
    }
    
    // 'label' attribute on TextCell (id=EnterDelinquencyThreshold_Policy_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 84, column 45
    function label_93 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.EnterDelinquencyThreshold_Policy")
    }
    
    // 'label' attribute on TextCell (id=cancelPolicy_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 89, column 45
    function label_98 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.CancelPolicy")
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 28, column 41
    function valueRoot_40 () : java.lang.Object {
      return delinquencyPlan
    }
    
    // 'value' attribute on TextCell (id=exitDelinquency_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 94, column 45
    function value_104 () : java.math.BigDecimal {
      return delinquencyPlan.ExitDelinquency
    }
    
    // 'value' attribute on TypeKeyCell (id=applicableSegments_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 99, column 51
    function value_109 () : typekey.ApplicableSegments {
      return delinquencyPlan.ApplicableSegments
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 23, column 46
    function value_34 () : java.lang.String {
      return processor.getLoadStatus(delinquencyPlan)
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 28, column 41
    function value_39 () : java.lang.String {
      return delinquencyPlan.PublicID
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 34, column 41
    function value_44 () : java.lang.String {
      return delinquencyPlan.Name
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 39, column 39
    function value_49 () : java.util.Date {
      return delinquencyPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 44, column 39
    function value_54 () : java.util.Date {
      return delinquencyPlan.ExpirationDate
    }
    
    // 'value' attribute on TypeKeyCell (id=cancellationTarget_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 49, column 51
    function value_59 () : typekey.CancellationTarget {
      return delinquencyPlan.CancellationTarget
    }
    
    // 'value' attribute on BooleanRadioCell (id=holdInvoicingOnTargetedPolicies_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 54, column 42
    function value_64 () : java.lang.Boolean {
      return delinquencyPlan.HoldInvoicingOnTargetedPolicies
    }
    
    // 'value' attribute on TextCell (id=GracePeriodDays_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 59, column 42
    function value_69 () : java.lang.Integer {
      return delinquencyPlan.GracePeriodDays
    }
    
    // 'value' attribute on TextCell (id=lateFee_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 64, column 45
    function value_74 () : java.math.BigDecimal {
      return delinquencyPlan.LateFee
    }
    
    // 'value' attribute on TextCell (id=reinstatementFee_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 69, column 45
    function value_79 () : java.math.BigDecimal {
      return delinquencyPlan.ReinstatementFee
    }
    
    // 'value' attribute on TextCell (id=writeoffThreshold_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 74, column 45
    function value_84 () : java.math.BigDecimal {
      return delinquencyPlan.WriteoffThreshold
    }
    
    // 'value' attribute on TextCell (id=enterDelinquencyThreshold_Account_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 79, column 45
    function value_89 () : java.math.BigDecimal {
      return delinquencyPlan.EnterDelinquencyThreshold_Account
    }
    
    // 'value' attribute on TextCell (id=EnterDelinquencyThreshold_Policy_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 84, column 45
    function value_94 () : java.math.BigDecimal {
      return delinquencyPlan.EnterDelinquencyThreshold_Policy
    }
    
    // 'value' attribute on TextCell (id=cancelPolicy_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 89, column 45
    function value_99 () : java.math.BigDecimal {
      return delinquencyPlan.CancelPolicy
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 23, column 46
    function visible_35 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get delinquencyPlan () : tdic.util.dataloader.data.plandata.DelinquencyPlanData {
      return getIteratedValue(1) as tdic.util.dataloader.data.plandata.DelinquencyPlanData
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadDelinquencyPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanDataUploadDelinquencyPlanLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TypeKeyCell (id=cancellationTarget_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 49, column 51
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.CancellationTarget")
    }
    
    // 'label' attribute on BooleanRadioCell (id=holdInvoicingOnTargetedPolicies_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 54, column 42
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.HoldInvoicing")
    }
    
    // 'label' attribute on TextCell (id=GracePeriodDays_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 59, column 42
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.GracePeriodDays")
    }
    
    // 'label' attribute on TextCell (id=lateFee_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 64, column 45
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.LateFee")
    }
    
    // 'label' attribute on TextCell (id=reinstatementFee_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 69, column 45
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.ReinstatementFee")
    }
    
    // 'label' attribute on TextCell (id=writeoffThreshold_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 74, column 45
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.WriteoffThreshold")
    }
    
    // 'label' attribute on TextCell (id=enterDelinquencyThreshold_Account_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 79, column 45
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.EnterDelinquencyThreshold_Account")
    }
    
    // 'label' attribute on TextCell (id=EnterDelinquencyThreshold_Policy_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 84, column 45
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.EnterDelinquencyThreshold_Policy")
    }
    
    // 'label' attribute on TextCell (id=cancelPolicy_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 89, column 45
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.CancelPolicy")
    }
    
    // 'label' attribute on TextCell (id=exitDelinquency_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 94, column 45
    function label_29 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.ExitDelinquency")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 28, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.PublicID")
    }
    
    // 'label' attribute on TypeKeyCell (id=applicableSegments_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 99, column 51
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.ApplicableSegments")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 34, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.PlanName")
    }
    
    // 'label' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 39, column 39
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.EffectiveDate")
    }
    
    // 'label' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 44, column 39
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlan.ExpirationDate")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 23, column 46
    function sortValue_1 (delinquencyPlan :  tdic.util.dataloader.data.plandata.DelinquencyPlanData) : java.lang.Object {
      return processor.getLoadStatus(delinquencyPlan)
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 44, column 39
    function sortValue_10 (delinquencyPlan :  tdic.util.dataloader.data.plandata.DelinquencyPlanData) : java.lang.Object {
      return delinquencyPlan.ExpirationDate
    }
    
    // 'value' attribute on TypeKeyCell (id=cancellationTarget_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 49, column 51
    function sortValue_12 (delinquencyPlan :  tdic.util.dataloader.data.plandata.DelinquencyPlanData) : java.lang.Object {
      return delinquencyPlan.CancellationTarget
    }
    
    // 'value' attribute on BooleanRadioCell (id=holdInvoicingOnTargetedPolicies_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 54, column 42
    function sortValue_14 (delinquencyPlan :  tdic.util.dataloader.data.plandata.DelinquencyPlanData) : java.lang.Object {
      return delinquencyPlan.HoldInvoicingOnTargetedPolicies
    }
    
    // 'value' attribute on TextCell (id=GracePeriodDays_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 59, column 42
    function sortValue_16 (delinquencyPlan :  tdic.util.dataloader.data.plandata.DelinquencyPlanData) : java.lang.Object {
      return delinquencyPlan.GracePeriodDays
    }
    
    // 'value' attribute on TextCell (id=lateFee_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 64, column 45
    function sortValue_18 (delinquencyPlan :  tdic.util.dataloader.data.plandata.DelinquencyPlanData) : java.lang.Object {
      return delinquencyPlan.LateFee
    }
    
    // 'value' attribute on TextCell (id=reinstatementFee_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 69, column 45
    function sortValue_20 (delinquencyPlan :  tdic.util.dataloader.data.plandata.DelinquencyPlanData) : java.lang.Object {
      return delinquencyPlan.ReinstatementFee
    }
    
    // 'value' attribute on TextCell (id=writeoffThreshold_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 74, column 45
    function sortValue_22 (delinquencyPlan :  tdic.util.dataloader.data.plandata.DelinquencyPlanData) : java.lang.Object {
      return delinquencyPlan.WriteoffThreshold
    }
    
    // 'value' attribute on TextCell (id=enterDelinquencyThreshold_Account_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 79, column 45
    function sortValue_24 (delinquencyPlan :  tdic.util.dataloader.data.plandata.DelinquencyPlanData) : java.lang.Object {
      return delinquencyPlan.EnterDelinquencyThreshold_Account
    }
    
    // 'value' attribute on TextCell (id=EnterDelinquencyThreshold_Policy_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 84, column 45
    function sortValue_26 (delinquencyPlan :  tdic.util.dataloader.data.plandata.DelinquencyPlanData) : java.lang.Object {
      return delinquencyPlan.EnterDelinquencyThreshold_Policy
    }
    
    // 'value' attribute on TextCell (id=cancelPolicy_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 89, column 45
    function sortValue_28 (delinquencyPlan :  tdic.util.dataloader.data.plandata.DelinquencyPlanData) : java.lang.Object {
      return delinquencyPlan.CancelPolicy
    }
    
    // 'value' attribute on TextCell (id=exitDelinquency_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 94, column 45
    function sortValue_30 (delinquencyPlan :  tdic.util.dataloader.data.plandata.DelinquencyPlanData) : java.lang.Object {
      return delinquencyPlan.ExitDelinquency
    }
    
    // 'value' attribute on TypeKeyCell (id=applicableSegments_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 99, column 51
    function sortValue_32 (delinquencyPlan :  tdic.util.dataloader.data.plandata.DelinquencyPlanData) : java.lang.Object {
      return delinquencyPlan.ApplicableSegments
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 28, column 41
    function sortValue_4 (delinquencyPlan :  tdic.util.dataloader.data.plandata.DelinquencyPlanData) : java.lang.Object {
      return delinquencyPlan.PublicID
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 34, column 41
    function sortValue_6 (delinquencyPlan :  tdic.util.dataloader.data.plandata.DelinquencyPlanData) : java.lang.Object {
      return delinquencyPlan.Name
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 39, column 39
    function sortValue_8 (delinquencyPlan :  tdic.util.dataloader.data.plandata.DelinquencyPlanData) : java.lang.Object {
      return delinquencyPlan.EffectiveDate
    }
    
    // 'value' attribute on RowIterator (id=DelinquencyPlan) at PlanDataUploadDelinquencyPlanLV.pcf: line 15, column 101
    function value_114 () : java.util.ArrayList<tdic.util.dataloader.data.plandata.DelinquencyPlanData> {
      return processor.DelinquencyPlanArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadDelinquencyPlanLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}