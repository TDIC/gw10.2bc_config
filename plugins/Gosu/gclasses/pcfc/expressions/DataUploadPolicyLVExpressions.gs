package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadPolicyLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DataUploadPolicyLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadPolicyLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DataUploadPolicyLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadPolicyLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=paymentPlan_Cell) at DataUploadPolicyLV.pcf: line 50, column 41
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.PaymentPlan")
    }
    
    // 'label' attribute on TextCell (id=billingMethod_Cell) at DataUploadPolicyLV.pcf: line 55, column 41
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.BillingMethod")
    }
    
    // 'label' attribute on TextCell (id=producerCode_Cell) at DataUploadPolicyLV.pcf: line 60, column 41
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ProducerCode")
    }
    
    // 'label' attribute on TypeKeyCell (id=lobCode_Cell) at DataUploadPolicyLV.pcf: line 65, column 40
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.LOB")
    }
    
    // 'label' attribute on BooleanRadioCell (id=underAudit_Cell) at DataUploadPolicyLV.pcf: line 70, column 42
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.UnderAudit")
    }
    
    // 'label' attribute on TextCell (id=underwriter_Cell) at DataUploadPolicyLV.pcf: line 75, column 41
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Underwriter")
    }
    
    // 'label' attribute on TypeKeyCell (id=uwCompany_Cell) at DataUploadPolicyLV.pcf: line 80, column 42
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.UWCompany")
    }
    
    // 'label' attribute on TypeKeyCell (id=contactType_Cell) at DataUploadPolicyLV.pcf: line 85, column 40
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ContactType")
    }
    
    // 'label' attribute on TextCell (id=contactName_Cell) at DataUploadPolicyLV.pcf: line 90, column 41
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ContactName")
    }
    
    // 'label' attribute on TextCell (id=firstName_Cell) at DataUploadPolicyLV.pcf: line 95, column 41
    function label_29 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.FirstName")
    }
    
    // 'label' attribute on TextCell (id=accountName_Cell) at DataUploadPolicyLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.AccountName")
    }
    
    // 'label' attribute on TextCell (id=lastName_Cell) at DataUploadPolicyLV.pcf: line 100, column 41
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.LastName")
    }
    
    // 'label' attribute on TextCell (id=addressLine1_Cell) at DataUploadPolicyLV.pcf: line 105, column 41
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Line1")
    }
    
    // 'label' attribute on TextCell (id=addressLine2_Cell) at DataUploadPolicyLV.pcf: line 110, column 41
    function label_35 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Line2")
    }
    
    // 'label' attribute on TextCell (id=city_Cell) at DataUploadPolicyLV.pcf: line 115, column 41
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.City")
    }
    
    // 'label' attribute on TextCell (id=state_Cell) at DataUploadPolicyLV.pcf: line 120, column 41
    function label_39 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.State")
    }
    
    // 'label' attribute on TextCell (id=postalCode_Cell) at DataUploadPolicyLV.pcf: line 125, column 41
    function label_41 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.PostalCode")
    }
    
    // 'label' attribute on TypeKeyCell (id=country_Cell) at DataUploadPolicyLV.pcf: line 130, column 40
    function label_43 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Country")
    }
    
    // 'label' attribute on TextCell (id=phone_Cell) at DataUploadPolicyLV.pcf: line 135, column 41
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Phone")
    }
    
    // 'label' attribute on TextCell (id=email_Cell) at DataUploadPolicyLV.pcf: line 140, column 41
    function label_47 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Email")
    }
    
    // 'label' attribute on TextCell (id=contactRole_Cell) at DataUploadPolicyLV.pcf: line 145, column 41
    function label_49 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ContactRole")
    }
    
    // 'label' attribute on TextCell (id=policyNumber_Cell) at DataUploadPolicyLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.PolicyNumber")
    }
    
    // 'label' attribute on TextCell (id=policyListBillPayerAccount_Cell) at DataUploadPolicyLV.pcf: line 151, column 41
    function label_51 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ListBillPayerAccount")
    }
    
    // 'label' attribute on TextCell (id=policyListBillPayerPaymentPlan_Cell) at DataUploadPolicyLV.pcf: line 157, column 41
    function label_53 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ListBillPayerPaymentPlan")
    }
    
    // 'label' attribute on TextCell (id=policyListBillPayerInvoiceStream_Cell) at DataUploadPolicyLV.pcf: line 163, column 41
    function label_55 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ListBillPayerInvoiceStream")
    }
    
    // 'label' attribute on DateCell (id=effectiveDate_Cell) at DataUploadPolicyLV.pcf: line 40, column 39
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.EffectiveDate")
    }
    
    // 'label' attribute on DateCell (id=expirationDate_Cell) at DataUploadPolicyLV.pcf: line 45, column 39
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ExpirationDate")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadPolicyLV.pcf: line 23, column 46
    function sortValue_1 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return processor.getLoadStatus(policy)
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at DataUploadPolicyLV.pcf: line 45, column 39
    function sortValue_10 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.PolicyPerExpirDate
    }
    
    // 'value' attribute on TextCell (id=paymentPlan_Cell) at DataUploadPolicyLV.pcf: line 50, column 41
    function sortValue_12 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.PaymentPlan
    }
    
    // 'value' attribute on TextCell (id=billingMethod_Cell) at DataUploadPolicyLV.pcf: line 55, column 41
    function sortValue_14 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.BillingMethod
    }
    
    // 'value' attribute on TextCell (id=producerCode_Cell) at DataUploadPolicyLV.pcf: line 60, column 41
    function sortValue_16 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.ProducerCode
    }
    
    // 'value' attribute on TypeKeyCell (id=lobCode_Cell) at DataUploadPolicyLV.pcf: line 65, column 40
    function sortValue_18 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.LOBCode
    }
    
    // 'value' attribute on BooleanRadioCell (id=underAudit_Cell) at DataUploadPolicyLV.pcf: line 70, column 42
    function sortValue_20 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.UnderAudit
    }
    
    // 'value' attribute on TextCell (id=underwriter_Cell) at DataUploadPolicyLV.pcf: line 75, column 41
    function sortValue_22 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.Underwriter
    }
    
    // 'value' attribute on TypeKeyCell (id=uwCompany_Cell) at DataUploadPolicyLV.pcf: line 80, column 42
    function sortValue_24 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.UWCompany
    }
    
    // 'value' attribute on TypeKeyCell (id=contactType_Cell) at DataUploadPolicyLV.pcf: line 85, column 40
    function sortValue_26 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.Contact.ContactType
    }
    
    // 'value' attribute on TextCell (id=contactName_Cell) at DataUploadPolicyLV.pcf: line 90, column 41
    function sortValue_28 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.Contact.Name
    }
    
    // 'value' attribute on TextCell (id=firstName_Cell) at DataUploadPolicyLV.pcf: line 95, column 41
    function sortValue_30 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.Contact.FirstName
    }
    
    // 'value' attribute on TextCell (id=lastName_Cell) at DataUploadPolicyLV.pcf: line 100, column 41
    function sortValue_32 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.Contact.LastName
    }
    
    // 'value' attribute on TextCell (id=addressLine1_Cell) at DataUploadPolicyLV.pcf: line 105, column 41
    function sortValue_34 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.Contact.PrimaryAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=addressLine2_Cell) at DataUploadPolicyLV.pcf: line 110, column 41
    function sortValue_36 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.Contact.PrimaryAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at DataUploadPolicyLV.pcf: line 115, column 41
    function sortValue_38 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.Contact.PrimaryAddress.City
    }
    
    // 'value' attribute on TextCell (id=accountName_Cell) at DataUploadPolicyLV.pcf: line 29, column 41
    function sortValue_4 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.AccountName
    }
    
    // 'value' attribute on TextCell (id=state_Cell) at DataUploadPolicyLV.pcf: line 120, column 41
    function sortValue_40 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.Contact.PrimaryAddress.State
    }
    
    // 'value' attribute on TextCell (id=postalCode_Cell) at DataUploadPolicyLV.pcf: line 125, column 41
    function sortValue_42 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.Contact.PrimaryAddress.PostalCode
    }
    
    // 'value' attribute on TypeKeyCell (id=country_Cell) at DataUploadPolicyLV.pcf: line 130, column 40
    function sortValue_44 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.Contact.PrimaryAddress.Country
    }
    
    // 'value' attribute on TextCell (id=phone_Cell) at DataUploadPolicyLV.pcf: line 135, column 41
    function sortValue_46 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.Contact.WorkPhone
    }
    
    // 'value' attribute on TextCell (id=email_Cell) at DataUploadPolicyLV.pcf: line 140, column 41
    function sortValue_48 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.Contact.Email
    }
    
    // 'value' attribute on TextCell (id=contactRole_Cell) at DataUploadPolicyLV.pcf: line 145, column 41
    function sortValue_50 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.Role.Code
    }
    
    // 'value' attribute on TextCell (id=policyListBillPayerAccount_Cell) at DataUploadPolicyLV.pcf: line 151, column 41
    function sortValue_52 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.ListBillPayerAccount
    }
    
    // 'value' attribute on TextCell (id=policyListBillPayerPaymentPlan_Cell) at DataUploadPolicyLV.pcf: line 157, column 41
    function sortValue_54 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.ListBillPayerPaymentPlan
    }
    
    // 'value' attribute on TextCell (id=policyListBillPayerInvoiceStream_Cell) at DataUploadPolicyLV.pcf: line 163, column 41
    function sortValue_56 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.ListBillPayerInvoiceStream
    }
    
    // 'value' attribute on TextCell (id=policyNumber_Cell) at DataUploadPolicyLV.pcf: line 35, column 41
    function sortValue_6 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.PolicyNumber
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at DataUploadPolicyLV.pcf: line 40, column 39
    function sortValue_8 (policy :  tdic.util.dataloader.data.sampledata.PolicyPeriodData) : java.lang.Object {
      return policy.PolicyPerEffDate
    }
    
    // 'value' attribute on RowIterator (id=policyID) at DataUploadPolicyLV.pcf: line 15, column 100
    function value_198 () : java.util.ArrayList<tdic.util.dataloader.data.sampledata.PolicyPeriodData> {
      return processor.PolicyArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadPolicyLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCSampleDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCSampleDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadPolicyLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DataUploadPolicyLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at DataUploadPolicyLV.pcf: line 17, column 91
    function highlighted_197 () : java.lang.Boolean {
      return (policy.Skipped or policy.Error) && processor.LoadCompleted
    }
    
    // 'label' attribute on BooleanRadioCell (id=underAudit_Cell) at DataUploadPolicyLV.pcf: line 70, column 42
    function label_102 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.UnderAudit")
    }
    
    // 'label' attribute on TextCell (id=underwriter_Cell) at DataUploadPolicyLV.pcf: line 75, column 41
    function label_107 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Underwriter")
    }
    
    // 'label' attribute on TypeKeyCell (id=uwCompany_Cell) at DataUploadPolicyLV.pcf: line 80, column 42
    function label_112 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.UWCompany")
    }
    
    // 'label' attribute on TypeKeyCell (id=contactType_Cell) at DataUploadPolicyLV.pcf: line 85, column 40
    function label_117 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ContactType")
    }
    
    // 'label' attribute on TextCell (id=contactName_Cell) at DataUploadPolicyLV.pcf: line 90, column 41
    function label_122 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ContactName")
    }
    
    // 'label' attribute on TextCell (id=firstName_Cell) at DataUploadPolicyLV.pcf: line 95, column 41
    function label_127 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.FirstName")
    }
    
    // 'label' attribute on TextCell (id=lastName_Cell) at DataUploadPolicyLV.pcf: line 100, column 41
    function label_132 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.LastName")
    }
    
    // 'label' attribute on TextCell (id=addressLine1_Cell) at DataUploadPolicyLV.pcf: line 105, column 41
    function label_137 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Line1")
    }
    
    // 'label' attribute on TextCell (id=addressLine2_Cell) at DataUploadPolicyLV.pcf: line 110, column 41
    function label_142 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Line2")
    }
    
    // 'label' attribute on TextCell (id=city_Cell) at DataUploadPolicyLV.pcf: line 115, column 41
    function label_147 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.City")
    }
    
    // 'label' attribute on TextCell (id=state_Cell) at DataUploadPolicyLV.pcf: line 120, column 41
    function label_152 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.State")
    }
    
    // 'label' attribute on TextCell (id=postalCode_Cell) at DataUploadPolicyLV.pcf: line 125, column 41
    function label_157 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.PostalCode")
    }
    
    // 'label' attribute on TypeKeyCell (id=country_Cell) at DataUploadPolicyLV.pcf: line 130, column 40
    function label_162 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Country")
    }
    
    // 'label' attribute on TextCell (id=phone_Cell) at DataUploadPolicyLV.pcf: line 135, column 41
    function label_167 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Phone")
    }
    
    // 'label' attribute on TextCell (id=email_Cell) at DataUploadPolicyLV.pcf: line 140, column 41
    function label_172 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Email")
    }
    
    // 'label' attribute on TextCell (id=contactRole_Cell) at DataUploadPolicyLV.pcf: line 145, column 41
    function label_177 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ContactRole")
    }
    
    // 'label' attribute on TextCell (id=policyListBillPayerAccount_Cell) at DataUploadPolicyLV.pcf: line 151, column 41
    function label_182 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ListBillPayerAccount")
    }
    
    // 'label' attribute on TextCell (id=policyListBillPayerPaymentPlan_Cell) at DataUploadPolicyLV.pcf: line 157, column 41
    function label_187 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ListBillPayerPaymentPlan")
    }
    
    // 'label' attribute on TextCell (id=policyListBillPayerInvoiceStream_Cell) at DataUploadPolicyLV.pcf: line 163, column 41
    function label_192 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ListBillPayerInvoiceStream")
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadPolicyLV.pcf: line 23, column 46
    function label_57 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=accountName_Cell) at DataUploadPolicyLV.pcf: line 29, column 41
    function label_62 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.AccountName")
    }
    
    // 'label' attribute on TextCell (id=policyNumber_Cell) at DataUploadPolicyLV.pcf: line 35, column 41
    function label_67 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.PolicyNumber")
    }
    
    // 'label' attribute on DateCell (id=effectiveDate_Cell) at DataUploadPolicyLV.pcf: line 40, column 39
    function label_72 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.EffectiveDate")
    }
    
    // 'label' attribute on DateCell (id=expirationDate_Cell) at DataUploadPolicyLV.pcf: line 45, column 39
    function label_77 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ExpirationDate")
    }
    
    // 'label' attribute on TextCell (id=paymentPlan_Cell) at DataUploadPolicyLV.pcf: line 50, column 41
    function label_82 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.PaymentPlan")
    }
    
    // 'label' attribute on TextCell (id=billingMethod_Cell) at DataUploadPolicyLV.pcf: line 55, column 41
    function label_87 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.BillingMethod")
    }
    
    // 'label' attribute on TextCell (id=producerCode_Cell) at DataUploadPolicyLV.pcf: line 60, column 41
    function label_92 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ProducerCode")
    }
    
    // 'label' attribute on TypeKeyCell (id=lobCode_Cell) at DataUploadPolicyLV.pcf: line 65, column 40
    function label_97 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.LOB")
    }
    
    // 'value' attribute on TypeKeyCell (id=contactType_Cell) at DataUploadPolicyLV.pcf: line 85, column 40
    function valueRoot_119 () : java.lang.Object {
      return policy.Contact
    }
    
    // 'value' attribute on TextCell (id=addressLine1_Cell) at DataUploadPolicyLV.pcf: line 105, column 41
    function valueRoot_139 () : java.lang.Object {
      return policy.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=contactRole_Cell) at DataUploadPolicyLV.pcf: line 145, column 41
    function valueRoot_179 () : java.lang.Object {
      return policy.Role
    }
    
    // 'value' attribute on TextCell (id=accountName_Cell) at DataUploadPolicyLV.pcf: line 29, column 41
    function valueRoot_64 () : java.lang.Object {
      return policy
    }
    
    // 'value' attribute on BooleanRadioCell (id=underAudit_Cell) at DataUploadPolicyLV.pcf: line 70, column 42
    function value_103 () : java.lang.Boolean {
      return policy.UnderAudit
    }
    
    // 'value' attribute on TextCell (id=underwriter_Cell) at DataUploadPolicyLV.pcf: line 75, column 41
    function value_108 () : java.lang.String {
      return policy.Underwriter
    }
    
    // 'value' attribute on TypeKeyCell (id=uwCompany_Cell) at DataUploadPolicyLV.pcf: line 80, column 42
    function value_113 () : typekey.UWCompany {
      return policy.UWCompany
    }
    
    // 'value' attribute on TypeKeyCell (id=contactType_Cell) at DataUploadPolicyLV.pcf: line 85, column 40
    function value_118 () : typekey.Contact {
      return policy.Contact.ContactType
    }
    
    // 'value' attribute on TextCell (id=contactName_Cell) at DataUploadPolicyLV.pcf: line 90, column 41
    function value_123 () : java.lang.String {
      return policy.Contact.Name
    }
    
    // 'value' attribute on TextCell (id=firstName_Cell) at DataUploadPolicyLV.pcf: line 95, column 41
    function value_128 () : java.lang.String {
      return policy.Contact.FirstName
    }
    
    // 'value' attribute on TextCell (id=lastName_Cell) at DataUploadPolicyLV.pcf: line 100, column 41
    function value_133 () : java.lang.String {
      return policy.Contact.LastName
    }
    
    // 'value' attribute on TextCell (id=addressLine1_Cell) at DataUploadPolicyLV.pcf: line 105, column 41
    function value_138 () : java.lang.String {
      return policy.Contact.PrimaryAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=addressLine2_Cell) at DataUploadPolicyLV.pcf: line 110, column 41
    function value_143 () : java.lang.String {
      return policy.Contact.PrimaryAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at DataUploadPolicyLV.pcf: line 115, column 41
    function value_148 () : java.lang.String {
      return policy.Contact.PrimaryAddress.City
    }
    
    // 'value' attribute on TextCell (id=state_Cell) at DataUploadPolicyLV.pcf: line 120, column 41
    function value_153 () : java.lang.String {
      return policy.Contact.PrimaryAddress.State
    }
    
    // 'value' attribute on TextCell (id=postalCode_Cell) at DataUploadPolicyLV.pcf: line 125, column 41
    function value_158 () : java.lang.String {
      return policy.Contact.PrimaryAddress.PostalCode
    }
    
    // 'value' attribute on TypeKeyCell (id=country_Cell) at DataUploadPolicyLV.pcf: line 130, column 40
    function value_163 () : typekey.Country {
      return policy.Contact.PrimaryAddress.Country
    }
    
    // 'value' attribute on TextCell (id=phone_Cell) at DataUploadPolicyLV.pcf: line 135, column 41
    function value_168 () : java.lang.String {
      return policy.Contact.WorkPhone
    }
    
    // 'value' attribute on TextCell (id=email_Cell) at DataUploadPolicyLV.pcf: line 140, column 41
    function value_173 () : java.lang.String {
      return policy.Contact.Email
    }
    
    // 'value' attribute on TextCell (id=contactRole_Cell) at DataUploadPolicyLV.pcf: line 145, column 41
    function value_178 () : java.lang.String {
      return policy.Role.Code
    }
    
    // 'value' attribute on TextCell (id=policyListBillPayerAccount_Cell) at DataUploadPolicyLV.pcf: line 151, column 41
    function value_183 () : java.lang.String {
      return policy.ListBillPayerAccount
    }
    
    // 'value' attribute on TextCell (id=policyListBillPayerPaymentPlan_Cell) at DataUploadPolicyLV.pcf: line 157, column 41
    function value_188 () : java.lang.String {
      return policy.ListBillPayerPaymentPlan
    }
    
    // 'value' attribute on TextCell (id=policyListBillPayerInvoiceStream_Cell) at DataUploadPolicyLV.pcf: line 163, column 41
    function value_193 () : java.lang.String {
      return policy.ListBillPayerInvoiceStream
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadPolicyLV.pcf: line 23, column 46
    function value_58 () : java.lang.String {
      return processor.getLoadStatus(policy)
    }
    
    // 'value' attribute on TextCell (id=accountName_Cell) at DataUploadPolicyLV.pcf: line 29, column 41
    function value_63 () : java.lang.String {
      return policy.AccountName
    }
    
    // 'value' attribute on TextCell (id=policyNumber_Cell) at DataUploadPolicyLV.pcf: line 35, column 41
    function value_68 () : java.lang.String {
      return policy.PolicyNumber
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at DataUploadPolicyLV.pcf: line 40, column 39
    function value_73 () : java.util.Date {
      return policy.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at DataUploadPolicyLV.pcf: line 45, column 39
    function value_78 () : java.util.Date {
      return policy.PolicyPerExpirDate
    }
    
    // 'value' attribute on TextCell (id=paymentPlan_Cell) at DataUploadPolicyLV.pcf: line 50, column 41
    function value_83 () : java.lang.String {
      return policy.PaymentPlan
    }
    
    // 'value' attribute on TextCell (id=billingMethod_Cell) at DataUploadPolicyLV.pcf: line 55, column 41
    function value_88 () : java.lang.String {
      return policy.BillingMethod
    }
    
    // 'value' attribute on TextCell (id=producerCode_Cell) at DataUploadPolicyLV.pcf: line 60, column 41
    function value_93 () : java.lang.String {
      return policy.ProducerCode
    }
    
    // 'value' attribute on TypeKeyCell (id=lobCode_Cell) at DataUploadPolicyLV.pcf: line 65, column 40
    function value_98 () : typekey.LOBCode {
      return policy.LOBCode
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadPolicyLV.pcf: line 23, column 46
    function visible_59 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get policy () : tdic.util.dataloader.data.sampledata.PolicyPeriodData {
      return getIteratedValue(1) as tdic.util.dataloader.data.sampledata.PolicyPeriodData
    }
    
    
  }
  
  
}