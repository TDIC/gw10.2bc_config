package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailCommissionsProducerInfoInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailCommissionsProducerInfoInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailCommissionsProducerInfoInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyDetailCommissionsProducerInfoInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=PriorPolicyCommissionsLV_ProducerCode_Cell) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 87, column 195
    function action_42 () : void {
      ProducerDetailPopup.push(historyEntry.PolicyCommission.ProducerCode.Producer)
    }
    
    // 'action' attribute on TextCell (id=PriorPolicyCommissionsLV_CommissionPlan_Cell) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 98, column 50
    function action_49 () : void {
      CommissionPlanDetailPopup.push(historyEntry.PolicyCommission.CommissionSubPlan.CommissionPlan)
    }
    
    // 'action' attribute on TextCell (id=PriorPolicyCommissionsLV_ProducerCode_Cell) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 87, column 195
    function action_dest_43 () : pcf.api.Destination {
      return pcf.ProducerDetailPopup.createDestination(historyEntry.PolicyCommission.ProducerCode.Producer)
    }
    
    // 'action' attribute on TextCell (id=PriorPolicyCommissionsLV_CommissionPlan_Cell) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 98, column 50
    function action_dest_50 () : pcf.api.Destination {
      return pcf.CommissionPlanDetailPopup.createDestination(historyEntry.PolicyCommission.CommissionSubPlan.CommissionPlan)
    }
    
    // 'available' attribute on TextCell (id=PriorPolicyCommissionsLV_ProducerCode_Cell) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 87, column 195
    function available_41 () : java.lang.Boolean {
      return historyEntry.PolicyCommission != null
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=PriorPolicyCommissionsLV_CommissionReserved_Cell) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 111, column 126
    function currency_58 () : typekey.Currency {
      return policyPeriod.Currency
    }
    
    // 'outputConversion' attribute on TextCell (id=PriorPolicyCommissionsLV_CommissionRate_Cell) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 105, column 49
    function outputConversion_54 (VALUE :  java.math.BigDecimal) : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(VALUE, 2)
    }
    
    // 'value' attribute on TextCell (id=PriorPolicyCommissionsLV_CommissionPlan_Cell) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 98, column 50
    function valueRoot_52 () : java.lang.Object {
      return historyEntry.PolicyCommission.CommissionSubPlan
    }
    
    // 'value' attribute on TextCell (id=PriorPolicyCommissionsLV_ProducerCode_Cell) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 87, column 195
    function value_44 () : java.lang.String {
      return historyEntry.PolicyCommission == null ? DisplayKey.get("Web.PolicyDetailCommissions.ProducerName.None") : historyEntry.PolicyCommission.ProducerCode.DisplayName
    }
    
    // 'value' attribute on TextCell (id=PriorPolicyCommissionsLV_Dates_Cell) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 92, column 54
    function value_47 () : java.lang.String {
      return formatDateRange(historyEntry)
    }
    
    // 'value' attribute on TextCell (id=PriorPolicyCommissionsLV_CommissionPlan_Cell) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 98, column 50
    function value_51 () : entity.CommissionPlan {
      return historyEntry.PolicyCommission.CommissionSubPlan.CommissionPlan
    }
    
    // 'value' attribute on TextCell (id=PriorPolicyCommissionsLV_CommissionRate_Cell) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 105, column 49
    function value_55 () : java.math.BigDecimal {
      return historyEntry.PolicyCommission != null ? historyEntry.PolicyCommission.getCommissionRateAsBigDecimal(null) : null
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PriorPolicyCommissionsLV_CommissionReserved_Cell) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 111, column 126
    function value_57 () : gw.pl.currency.MonetaryAmount {
      return historyEntry.PolicyCommission == null ? null : historyEntry.PolicyCommission.CommissionReserveBalance
    }
    
    property get historyEntry () : gw.api.domain.account.PolicyCommissionHistoryEntry {
      return getIteratedValue(1) as gw.api.domain.account.PolicyCommissionHistoryEntry
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailCommissionsProducerInfoInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailCommissionsProducerInfoInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextInput (id=CommissionPlan_Input) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 53, column 51
    function action_20 () : void {
      CommissionPlanDetailPopup.push(current.PolicyCommission.CommissionSubPlan.CommissionPlan)
    }
    
    // 'action' attribute on TextInput (id=ProducerName_Input) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 36, column 65
    function action_5 () : void {
      ProducerDetailPopup.push(current.PolicyCommission.ProducerCode.Producer)
    }
    
    // 'action' attribute on TextInput (id=CommissionPlan_Input) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 53, column 51
    function action_dest_21 () : pcf.api.Destination {
      return pcf.CommissionPlanDetailPopup.createDestination(current.PolicyCommission.CommissionSubPlan.CommissionPlan)
    }
    
    // 'action' attribute on TextInput (id=ProducerName_Input) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 36, column 65
    function action_dest_6 () : pcf.api.Destination {
      return pcf.ProducerDetailPopup.createDestination(current.PolicyCommission.ProducerCode.Producer)
    }
    
    // 'available' attribute on TextInput (id=ProducerName_Input) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 36, column 65
    function available_4 () : java.lang.Boolean {
      return current.PolicyCommission != null
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=CommissionReserved_Input) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 68, column 51
    function currency_34 () : typekey.Currency {
      return policyPeriod.Currency
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 17, column 68
    function initialValue_0 () : gw.api.domain.account.PolicyCommissionHistoryEntry[] {
      return policyPeriod.getPolicyCommissionHistoryForRole(role)
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 22, column 66
    function initialValue_1 () : gw.api.domain.account.PolicyCommissionHistoryEntry {
      return history.length == 0 ? null : history[0]
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 27, column 68
    function initialValue_2 () : gw.api.domain.account.PolicyCommissionHistoryEntry[] {
      return gw.api.util.ArrayUtil.subArray(history, 1, history.length) as gw.api.domain.account.PolicyCommissionHistoryEntry[]
    }
    
    // 'label' attribute on Label at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 29, column 33
    function label_3 () : java.lang.String {
      return role.DisplayName
    }
    
    // 'outputConversion' attribute on TextInput (id=CommissionRate_Input) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 60, column 51
    function outputConversion_27 (VALUE :  java.math.BigDecimal) : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(VALUE, 2)
    }
    
    // 'value' attribute on TextCell (id=PriorPolicyCommissionsLV_ProducerCode_Cell) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 87, column 195
    function sortValue_37 (historyEntry :  gw.api.domain.account.PolicyCommissionHistoryEntry) : java.lang.Object {
      return historyEntry.PolicyCommission == null ? DisplayKey.get("Web.PolicyDetailCommissions.ProducerName.None") : historyEntry.PolicyCommission.ProducerCode.DisplayName
    }
    
    // 'sortBy' attribute on TextCell (id=PriorPolicyCommissionsLV_Dates_Cell) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 92, column 54
    function sortValue_38 (historyEntry :  gw.api.domain.account.PolicyCommissionHistoryEntry) : java.lang.Object {
      return historyEntry.StartDate
    }
    
    // 'value' attribute on TextCell (id=PriorPolicyCommissionsLV_CommissionPlan_Cell) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 98, column 50
    function sortValue_39 (historyEntry :  gw.api.domain.account.PolicyCommissionHistoryEntry) : java.lang.Object {
      return historyEntry.PolicyCommission.CommissionSubPlan.CommissionPlan
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PriorPolicyCommissionsLV_CommissionReserved_Cell) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 111, column 126
    function sortValue_40 (historyEntry :  gw.api.domain.account.PolicyCommissionHistoryEntry) : java.lang.Object {
      return historyEntry.PolicyCommission == null ? null : historyEntry.PolicyCommission.CommissionReserveBalance
    }
    
    // 'value' attribute on TextInput (id=ProducerCode_Input) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 41, column 51
    function valueRoot_12 () : java.lang.Object {
      return current.PolicyCommission.ProducerCode
    }
    
    // 'value' attribute on TextInput (id=CommissionPlan_Input) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 53, column 51
    function valueRoot_23 () : java.lang.Object {
      return current.PolicyCommission.CommissionSubPlan
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CommissionReserved_Input) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 68, column 51
    function valueRoot_33 () : java.lang.Object {
      return current.PolicyCommission
    }
    
    // 'value' attribute on TextInput (id=ProducerCode_Input) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 41, column 51
    function value_11 () : java.lang.String {
      return current.PolicyCommission.ProducerCode.Code
    }
    
    // 'value' attribute on TextInput (id=DateRange_Input) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 46, column 48
    function value_16 () : java.lang.String {
      return formatDateRange(current)
    }
    
    // 'value' attribute on TextInput (id=CommissionPlan_Input) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 53, column 51
    function value_22 () : entity.CommissionPlan {
      return current.PolicyCommission.CommissionSubPlan.CommissionPlan
    }
    
    // 'value' attribute on TextInput (id=CommissionRate_Input) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 60, column 51
    function value_28 () : java.math.BigDecimal {
      return current.PolicyCommission.getCommissionRateAsBigDecimal( null )
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CommissionReserved_Input) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 68, column 51
    function value_32 () : gw.pl.currency.MonetaryAmount {
      return current.PolicyCommission.CommissionReserveBalance
    }
    
    // 'value' attribute on RowIterator at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 80, column 76
    function value_60 () : gw.api.domain.account.PolicyCommissionHistoryEntry[] {
      return previousEntries
    }
    
    // 'value' attribute on TextInput (id=ProducerName_Input) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 36, column 65
    function value_7 () : java.lang.Comparable<java.lang.Object> {
      return current.PolicyCommission == null ? DisplayKey.get("Web.PolicyDetailCommissions.ProducerName.None") : current.PolicyCommission.ProducerCode.Producer
    }
    
    // 'visible' attribute on TextInput (id=DateRange_Input) at PolicyDetailCommissionsProducerInfoInputSet.pcf: line 46, column 48
    function visible_15 () : java.lang.Boolean {
      return previousEntries.length > 0
    }
    
    property get current () : gw.api.domain.account.PolicyCommissionHistoryEntry {
      return getVariableValue("current", 0) as gw.api.domain.account.PolicyCommissionHistoryEntry
    }
    
    property set current ($arg :  gw.api.domain.account.PolicyCommissionHistoryEntry) {
      setVariableValue("current", 0, $arg)
    }
    
    property get history () : gw.api.domain.account.PolicyCommissionHistoryEntry[] {
      return getVariableValue("history", 0) as gw.api.domain.account.PolicyCommissionHistoryEntry[]
    }
    
    property set history ($arg :  gw.api.domain.account.PolicyCommissionHistoryEntry[]) {
      setVariableValue("history", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get previousEntries () : gw.api.domain.account.PolicyCommissionHistoryEntry[] {
      return getVariableValue("previousEntries", 0) as gw.api.domain.account.PolicyCommissionHistoryEntry[]
    }
    
    property set previousEntries ($arg :  gw.api.domain.account.PolicyCommissionHistoryEntry[]) {
      setVariableValue("previousEntries", 0, $arg)
    }
    
    property get role () : PolicyRole {
      return getRequireValue("role", 0) as PolicyRole
    }
    
    property set role ($arg :  PolicyRole) {
      setRequireValue("role", 0, $arg)
    }
    
    function formatDateRange(historyEntry : gw.api.domain.account.PolicyCommissionHistoryEntry) : String {
            var startDate = historyEntry.StartDate == null ?
                    DisplayKey.get("Web.PolicyDetailCommissions.CurrentProducerDateRange.Original") :
                    gw.api.util.StringUtil.formatDate(historyEntry.StartDate, "short");
            var endDate = historyEntry.EndDate == null ?
                    DisplayKey.get("Web.PolicyDetailCommissions.CurrentProducerDateRange.Current") :
                    gw.api.util.StringUtil.formatDate(historyEntry.EndDate, "short");
            return DisplayKey.get("Web.PolicyDetailCommissions.CurrentProducerDateRange", startDate, endDate);
          }
    
    
  }
  
  
}