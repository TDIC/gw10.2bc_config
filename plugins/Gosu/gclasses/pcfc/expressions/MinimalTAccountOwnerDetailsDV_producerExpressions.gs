package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/MinimalTAccountOwnerDetailsDV.producer.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class MinimalTAccountOwnerDetailsDV_producerExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/MinimalTAccountOwnerDetailsDV.producer.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class MinimalTAccountOwnerDetailsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextInput (id=Producer_Input) at MinimalTAccountOwnerDetailsDV.producer.pcf: line 27, column 38
    function action_1 () : void {
      pcf.ProducerDetailPopup.push(producer)
    }
    
    // 'action' attribute on TextInput (id=Producer_Input) at MinimalTAccountOwnerDetailsDV.producer.pcf: line 27, column 38
    function action_dest_2 () : pcf.api.Destination {
      return pcf.ProducerDetailPopup.createDestination(producer)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at MinimalTAccountOwnerDetailsDV.producer.pcf: line 38, column 43
    function currency_7 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'initialValue' attribute on Variable at MinimalTAccountOwnerDetailsDV.producer.pcf: line 18, column 24
    function initialValue_0 () : Producer {
      return tAccountOwner as Producer
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at MinimalTAccountOwnerDetailsDV.producer.pcf: line 38, column 43
    function valueRoot_6 () : java.lang.Object {
      return producer
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at MinimalTAccountOwnerDetailsDV.producer.pcf: line 27, column 38
    function value_3 () : entity.Producer {
      return producer
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at MinimalTAccountOwnerDetailsDV.producer.pcf: line 38, column 43
    function value_5 () : gw.pl.currency.MonetaryAmount {
      return producer.UnappliedAmount
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    property get tAccountOwner () : TAccountOwner {
      return getRequireValue("tAccountOwner", 0) as TAccountOwner
    }
    
    property set tAccountOwner ($arg :  TAccountOwner) {
      setRequireValue("tAccountOwner", 0, $arg)
    }
    
    property get unapplied () : UnappliedFund {
      return getRequireValue("unapplied", 0) as UnappliedFund
    }
    
    property set unapplied ($arg :  UnappliedFund) {
      setRequireValue("unapplied", 0, $arg)
    }
    
    
  }
  
  
}