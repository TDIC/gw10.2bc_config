package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/TransactionSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransactionSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/TransactionSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransactionSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at TransactionSearchDV.pcf: line 52, column 43
    function available_26 () : java.lang.Boolean {
      return (searchCriteria.Currency != null or gw.api.util.CurrencyUtil.isSingleCurrencyMode())
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at TransactionSearchDV.pcf: line 52, column 43
    function currency_30 () : typekey.Currency {
      return gw.search.SearchHelper.getDefaultCurrencyIfNull(searchCriteria.Currency)
    }
    
    // 'def' attribute on InputSetRef at TransactionSearchDV.pcf: line 63, column 41
    function def_onEnter_40 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at TransactionSearchDV.pcf: line 63, column 41
    function def_refreshVariables_41 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TextInput (id=TransactionNumberCriterion_Input) at TransactionSearchDV.pcf: line 18, column 51
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.TransactionNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at TransactionSearchDV.pcf: line 30, column 46
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.EarliestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at TransactionSearchDV.pcf: line 35, column 44
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.LatestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at TransactionSearchDV.pcf: line 42, column 66
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Currency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at TransactionSearchDV.pcf: line 52, column 43
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MinAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at TransactionSearchDV.pcf: line 59, column 43
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MaxAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on RangeInput (id=TransactionTypeCriterion_Input) at TransactionSearchDV.pcf: line 25, column 42
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.TransactionType = (__VALUE_TO_SET as typekey.Transaction)
    }
    
    // 'onChange' attribute on PostOnChange at TransactionSearchDV.pcf: line 44, column 54
    function onChange_19 () : void {
      blankMinimumAndMaximumFields()
    }
    
    // 'valueRange' attribute on RangeInput (id=TransactionTypeCriterion_Input) at TransactionSearchDV.pcf: line 25, column 42
    function valueRange_7 () : java.lang.Object {
      return gw.api.web.transaction.TransactionUtil.getNonAbstractTypes()
    }
    
    // 'value' attribute on TextInput (id=TransactionNumberCriterion_Input) at TransactionSearchDV.pcf: line 18, column 51
    function valueRoot_2 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TextInput (id=TransactionNumberCriterion_Input) at TransactionSearchDV.pcf: line 18, column 51
    function value_0 () : java.lang.String {
      return searchCriteria.TransactionNumber
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at TransactionSearchDV.pcf: line 30, column 46
    function value_11 () : java.util.Date {
      return searchCriteria.EarliestDate
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at TransactionSearchDV.pcf: line 35, column 44
    function value_15 () : java.util.Date {
      return searchCriteria.LatestDate
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at TransactionSearchDV.pcf: line 42, column 66
    function value_21 () : typekey.Currency {
      return searchCriteria.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at TransactionSearchDV.pcf: line 52, column 43
    function value_27 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MinAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at TransactionSearchDV.pcf: line 59, column 43
    function value_34 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MaxAmount
    }
    
    // 'value' attribute on RangeInput (id=TransactionTypeCriterion_Input) at TransactionSearchDV.pcf: line 25, column 42
    function value_4 () : typekey.Transaction {
      return searchCriteria.TransactionType
    }
    
    // 'valueRange' attribute on RangeInput (id=TransactionTypeCriterion_Input) at TransactionSearchDV.pcf: line 25, column 42
    function verifyValueRangeIsAllowedType_8 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=TransactionTypeCriterion_Input) at TransactionSearchDV.pcf: line 25, column 42
    function verifyValueRangeIsAllowedType_8 ($$arg :  typekey.Transaction[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=TransactionTypeCriterion_Input) at TransactionSearchDV.pcf: line 25, column 42
    function verifyValueRange_9 () : void {
      var __valueRangeArg = gw.api.web.transaction.TransactionUtil.getNonAbstractTypes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_8(__valueRangeArg)
    }
    
    // 'visible' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at TransactionSearchDV.pcf: line 42, column 66
    function visible_20 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    property get searchCriteria () : gw.search.TransactionSearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.search.TransactionSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.TransactionSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    function blankMinimumAndMaximumFields() {
      searchCriteria.MinAmount = null
      searchCriteria.MaxAmount = null
    }
    
    
  }
  
  
}