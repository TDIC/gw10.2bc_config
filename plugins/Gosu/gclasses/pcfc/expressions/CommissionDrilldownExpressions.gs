package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/tools/CommissionDrilldown.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionDrilldownExpressions {
  @javax.annotation.Generated("config/web/pcf/tools/CommissionDrilldown.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionDrilldownExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'pickLocation' attribute on PickerInput (id=PolicyPeriod_Input) at CommissionDrilldown.pcf: line 43, column 45
    function action_4 () : void {
      PolicySearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=PolicyPeriod_Input) at CommissionDrilldown.pcf: line 43, column 45
    function action_dest_5 () : pcf.api.Destination {
      return pcf.PolicySearchPopup.createDestination()
    }
    
    // RowTree at CommissionDrilldown.pcf: line 68, column 54
    function containerLabel_51 (row :  java.lang.Object) : java.lang.String {
      return ""
    }
    
    // 'value' attribute on TypeKeyInput (id=Role_Input) at CommissionDrilldown.pcf: line 53, column 44
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      role = (__VALUE_TO_SET as typekey.PolicyRole)
    }
    
    // 'value' attribute on PickerInput (id=PolicyPeriod_Input) at CommissionDrilldown.pcf: line 43, column 45
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod = (__VALUE_TO_SET as entity.PolicyPeriod)
    }
    
    // 'initialValue' attribute on Variable at CommissionDrilldown.pcf: line 18, column 26
    function initialValue_0 () : PolicyRole {
      return PolicyRole.TC_PRIMARY
    }
    
    // 'initialValue' attribute on Variable at CommissionDrilldown.pcf: line 23, column 65
    function initialValue_1 () : gw.api.domain.commission.PolicyCommissionTreePage {
      return new gw.api.domain.commission.PolicyCommissionTreePage(policyPeriod, role)
    }
    
    // 'initialValue' attribute on Variable at CommissionDrilldown.pcf: line 27, column 55
    function initialValue_2 () : gw.api.web.policy.PolicySearchConverter {
      return new gw.api.web.policy.PolicySearchConverter()
    }
    
    // 'inputConversion' attribute on PickerInput (id=PolicyPeriod_Input) at CommissionDrilldown.pcf: line 43, column 45
    function inputConversion_7 (VALUE :  java.lang.String) : java.lang.Object {
      return policySearchConverter.getPolicyPeriod(VALUE)
    }
    
    // 'onChange' attribute on PostOnChange at CommissionDrilldown.pcf: line 56, column 69
    function onChange_11 () : void {
      drilldownPage.refresh(policyPeriod, role)
    }
    
    // 'onChange' attribute on PostOnChange at CommissionDrilldown.pcf: line 46, column 69
    function onChange_3 () : void {
      drilldownPage.refresh(policyPeriod, role)
    }
    
    // 'onPick' attribute on PickerInput (id=PolicyPeriod_Input) at CommissionDrilldown.pcf: line 43, column 45
    function onPick_6 (PickedValue :  PolicyPeriod) : void {
      drilldownPage.refresh(policyPeriod, role)
    }
    
    // 'parent' attribute on Page (id=CommissionDrilldown) at CommissionDrilldown.pcf: line 11, column 79
    static function parent_52 () : pcf.api.Destination {
      return pcf.ServerTools.createDestination()
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CommissionDrilldown.pcf: line 76, column 29
    function sortValue_15 (row :  gw.api.domain.commission.CommissionDrilldownRow) : java.lang.Object {
      return row.Name
    }
    
    // 'value' attribute on TextCell (id=CommissionableGross_Cell) at CommissionDrilldown.pcf: line 82, column 48
    function sortValue_16 (row :  gw.api.domain.commission.CommissionDrilldownRow) : java.lang.Object {
      return row.CommissionableGross
    }
    
    // 'value' attribute on BooleanRadioCell (id=Active_Cell) at CommissionDrilldown.pcf: line 91, column 36
    function sortValue_17 (row :  gw.api.domain.commission.CommissionDrilldownRow) : java.lang.Object {
      return true
    }
    
    // 'value' attribute on TextCell (id=Reserve_Cell) at CommissionDrilldown.pcf: line 97, column 36
    function sortValue_18 (row :  gw.api.domain.commission.CommissionDrilldownRow) : java.lang.Object {
      return row.Reserve
    }
    
    // 'value' attribute on TextCell (id=Earned_Cell) at CommissionDrilldown.pcf: line 103, column 35
    function sortValue_19 (row :  gw.api.domain.commission.CommissionDrilldownRow) : java.lang.Object {
      return row.Earned
    }
    
    // 'value' attribute on TextCell (id=Paid_Cell) at CommissionDrilldown.pcf: line 109, column 33
    function sortValue_20 (row :  gw.api.domain.commission.CommissionDrilldownRow) : java.lang.Object {
      return row.Paid
    }
    
    // 'value' attribute on TextCell (id=WrittenOff_Cell) at CommissionDrilldown.pcf: line 115, column 39
    function sortValue_21 (row :  gw.api.domain.commission.CommissionDrilldownRow) : java.lang.Object {
      return row.WrittenOff
    }
    
    // 'value' attribute on TextCell (id=Total_Cell) at CommissionDrilldown.pcf: line 121, column 34
    function sortValue_22 (row :  gw.api.domain.commission.CommissionDrilldownRow) : java.lang.Object {
      return row.Total
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at CommissionDrilldown.pcf: line 126, column 33
    function sortValue_23 (row :  gw.api.domain.commission.CommissionDrilldownRow) : java.lang.Object {
      return row.Rate
    }
    
    // 'value' attribute on TypeKeyInput (id=Role_Input) at CommissionDrilldown.pcf: line 53, column 44
    function value_12 () : typekey.PolicyRole {
      return role
    }
    
    // 'value' attribute on RowTree at CommissionDrilldown.pcf: line 68, column 54
    function value_50 () : java.lang.Object {
      return drilldownPage.PolicyCommissionTree
    }
    
    // 'value' attribute on PickerInput (id=PolicyPeriod_Input) at CommissionDrilldown.pcf: line 43, column 45
    function value_8 () : entity.PolicyPeriod {
      return policyPeriod
    }
    
    override property get CurrentLocation () : pcf.CommissionDrilldown {
      return super.CurrentLocation as pcf.CommissionDrilldown
    }
    
    property get drilldownPage () : gw.api.domain.commission.PolicyCommissionTreePage {
      return getVariableValue("drilldownPage", 0) as gw.api.domain.commission.PolicyCommissionTreePage
    }
    
    property set drilldownPage ($arg :  gw.api.domain.commission.PolicyCommissionTreePage) {
      setVariableValue("drilldownPage", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get policySearchConverter () : gw.api.web.policy.PolicySearchConverter {
      return getVariableValue("policySearchConverter", 0) as gw.api.web.policy.PolicySearchConverter
    }
    
    property set policySearchConverter ($arg :  gw.api.web.policy.PolicySearchConverter) {
      setVariableValue("policySearchConverter", 0, $arg)
    }
    
    property get role () : PolicyRole {
      return getVariableValue("role", 0) as PolicyRole
    }
    
    property set role ($arg :  PolicyRole) {
      setVariableValue("role", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/tools/CommissionDrilldown.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RowTreeEntryExpressionsImpl extends CommissionDrilldownExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'iconColor' attribute on BooleanRadioCell (id=Active_Cell) at CommissionDrilldown.pcf: line 91, column 36
    function iconColor_31 () : gw.api.web.color.GWColor {
      return row.Active ? gw.api.web.color.GWColor.THEME_ALERT_SUCCESS : gw.api.web.color.GWColor.THEME_ALERT_NEUTRAL
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Active_Cell) at CommissionDrilldown.pcf: line 91, column 36
    function icon_30 () : java.lang.String {
      return row.Active ? "circle_checkmark" : "circle"
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CommissionDrilldown.pcf: line 76, column 29
    function valueRoot_25 () : java.lang.Object {
      return row
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CommissionDrilldown.pcf: line 76, column 29
    function value_24 () : java.lang.String {
      return row.Name
    }
    
    // 'value' attribute on TextCell (id=CommissionableGross_Cell) at CommissionDrilldown.pcf: line 82, column 48
    function value_27 () : java.lang.String {
      return row.CommissionableGross
    }
    
    // 'value' attribute on TextCell (id=Reserve_Cell) at CommissionDrilldown.pcf: line 97, column 36
    function value_32 () : java.lang.String {
      return row.Reserve
    }
    
    // 'value' attribute on TextCell (id=Earned_Cell) at CommissionDrilldown.pcf: line 103, column 35
    function value_35 () : java.lang.String {
      return row.Earned
    }
    
    // 'value' attribute on TextCell (id=Paid_Cell) at CommissionDrilldown.pcf: line 109, column 33
    function value_38 () : java.lang.String {
      return row.Paid
    }
    
    // 'value' attribute on TextCell (id=WrittenOff_Cell) at CommissionDrilldown.pcf: line 115, column 39
    function value_41 () : java.lang.String {
      return row.WrittenOff
    }
    
    // 'value' attribute on TextCell (id=Total_Cell) at CommissionDrilldown.pcf: line 121, column 34
    function value_44 () : java.lang.String {
      return row.Total
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at CommissionDrilldown.pcf: line 126, column 33
    function value_47 () : java.lang.String {
      return row.Rate
    }
    
    property get row () : gw.api.domain.commission.CommissionDrilldownRow {
      return getIteratedValue(1) as gw.api.domain.commission.CommissionDrilldownRow
    }
    
    
  }
  
  
}