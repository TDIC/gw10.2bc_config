package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopHeldCharges.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopHeldChargesExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopHeldCharges.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopHeldChargesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=DesktopHeldCharges) at DesktopHeldCharges.pcf: line 8, column 64
    static function canVisit_3 () : java.lang.Boolean {
      return perm.System.viewdesktop
    }
    
    // 'def' attribute on PanelRef at DesktopHeldCharges.pcf: line 22, column 116
    function def_onEnter_1 (def :  pcf.ChargesLV) : void {
      def.onEnter(charges?.toTypedArray(), false, gw.api.web.accounting.ChargeView.HOLD, true, true, false)
    }
    
    // 'def' attribute on PanelRef at DesktopHeldCharges.pcf: line 22, column 116
    function def_refreshVariables_2 (def :  pcf.ChargesLV) : void {
      def.refreshVariables(charges?.toTypedArray(), false, gw.api.web.accounting.ChargeView.HOLD, true, true, false)
    }
    
    // 'initialValue' attribute on Variable at DesktopHeldCharges.pcf: line 14, column 51
    function initialValue_0 () : java.util.List<entity.Charge> {
      return gw.api.web.accounting.HeldChargesUtil.findAllHeldCharges()
    }
    
    // Page (id=DesktopHeldCharges) at DesktopHeldCharges.pcf: line 8, column 64
    static function parent_4 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.DesktopHeldCharges {
      return super.CurrentLocation as pcf.DesktopHeldCharges
    }
    
    property get charges () : java.util.List<entity.Charge> {
      return getVariableValue("charges", 0) as java.util.List<entity.Charge>
    }
    
    property set charges ($arg :  java.util.List<entity.Charge>) {
      setVariableValue("charges", 0, $arg)
    }
    
    property get showCheckboxes () : boolean {
      return getVariableValue("showCheckboxes", 0) as java.lang.Boolean
    }
    
    property set showCheckboxes ($arg :  boolean) {
      setVariableValue("showCheckboxes", 0, $arg)
    }
    
    
  }
  
  
}