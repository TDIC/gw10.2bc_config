package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopMenuActionsExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopMenuActionsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_MultiPaymentEntryWizard) at DesktopMenuActions.pcf: line 30, column 60
    function action_14 () : void {
      pcf.MultiPaymentEntryWizard.go(gw.api.util.CurrencyUtil.getDefaultCurrency())
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewPaymentRequest) at DesktopMenuActions.pcf: line 45, column 87
    function action_16 () : void {
      NewPaymentRequestWizard.go()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewBatchPayment) at DesktopMenuActions.pcf: line 50, column 108
    function action_24 () : void {
      pcf.NewBatchPaymentPage.go(gw.api.util.CurrencyUtil.getDefaultCurrency())
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewDisbursementTransaction) at DesktopMenuActions.pcf: line 70, column 48
    function action_27 () : void {
      CreateDisbursementWizard.go()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewTransactionTransfer) at DesktopMenuActions.pcf: line 75, column 44
    function action_30 () : void {
      NewTransferWizard.go()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewTransactionWriteoff) at DesktopMenuActions.pcf: line 80, column 71
    function action_33 () : void {
      NewWriteoffWizard.go()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewTransactionNegativeWriteoff) at DesktopMenuActions.pcf: line 85, column 71
    function action_36 () : void {
      NewNegativeWriteoffWizard.push()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewTransactionSubrogation) at DesktopMenuActions.pcf: line 90, column 45
    function action_39 () : void {
      CreateSubrogationWizard.go()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewChargeReversal) at DesktopMenuActions.pcf: line 95, column 44
    function action_42 () : void {
      NewChargeReversalWizard.go()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewWriteoffReversal) at DesktopMenuActions.pcf: line 100, column 44
    function action_45 () : void {
      NewWriteoffReversalWizard.go()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewNegativeWriteoffReversal) at DesktopMenuActions.pcf: line 105, column 44
    function action_48 () : void {
      NewNegativeWriteoffReversalWizard.go()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewFundsTransferReversal) at DesktopMenuActions.pcf: line 110, column 49
    function action_51 () : void {
      NewFundsTransferReversalWizard.go()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewTransactionGeneral) at DesktopMenuActions.pcf: line 115, column 44
    function action_54 () : void {
      NewTransactionWizard.go()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_UserPreferences) at DesktopMenuActions.pcf: line 138, column 21
    function action_59 () : void {
      UserPreferencesWorksheet.goInWorkspace()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewSuspensePayment) at DesktopMenuActions.pcf: line 14, column 78
    function action_6 () : void {
      NewSuspensePayment.go(gw.api.util.CurrencyUtil.getDefaultCurrency())
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_MultiPaymentEntryWizard) at DesktopMenuActions.pcf: line 30, column 60
    function action_dest_15 () : pcf.api.Destination {
      return pcf.MultiPaymentEntryWizard.createDestination(gw.api.util.CurrencyUtil.getDefaultCurrency())
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewPaymentRequest) at DesktopMenuActions.pcf: line 45, column 87
    function action_dest_17 () : pcf.api.Destination {
      return pcf.NewPaymentRequestWizard.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewBatchPayment) at DesktopMenuActions.pcf: line 50, column 108
    function action_dest_25 () : pcf.api.Destination {
      return pcf.NewBatchPaymentPage.createDestination(gw.api.util.CurrencyUtil.getDefaultCurrency())
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewDisbursementTransaction) at DesktopMenuActions.pcf: line 70, column 48
    function action_dest_28 () : pcf.api.Destination {
      return pcf.CreateDisbursementWizard.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewTransactionTransfer) at DesktopMenuActions.pcf: line 75, column 44
    function action_dest_31 () : pcf.api.Destination {
      return pcf.NewTransferWizard.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewTransactionWriteoff) at DesktopMenuActions.pcf: line 80, column 71
    function action_dest_34 () : pcf.api.Destination {
      return pcf.NewWriteoffWizard.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewTransactionNegativeWriteoff) at DesktopMenuActions.pcf: line 85, column 71
    function action_dest_37 () : pcf.api.Destination {
      return pcf.NewNegativeWriteoffWizard.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewTransactionSubrogation) at DesktopMenuActions.pcf: line 90, column 45
    function action_dest_40 () : pcf.api.Destination {
      return pcf.CreateSubrogationWizard.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewChargeReversal) at DesktopMenuActions.pcf: line 95, column 44
    function action_dest_43 () : pcf.api.Destination {
      return pcf.NewChargeReversalWizard.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewWriteoffReversal) at DesktopMenuActions.pcf: line 100, column 44
    function action_dest_46 () : pcf.api.Destination {
      return pcf.NewWriteoffReversalWizard.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewNegativeWriteoffReversal) at DesktopMenuActions.pcf: line 105, column 44
    function action_dest_49 () : pcf.api.Destination {
      return pcf.NewNegativeWriteoffReversalWizard.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewFundsTransferReversal) at DesktopMenuActions.pcf: line 110, column 49
    function action_dest_52 () : pcf.api.Destination {
      return pcf.NewFundsTransferReversalWizard.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewTransactionGeneral) at DesktopMenuActions.pcf: line 115, column 44
    function action_dest_55 () : pcf.api.Destination {
      return pcf.NewTransactionWizard.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_UserPreferences) at DesktopMenuActions.pcf: line 138, column 21
    function action_dest_60 () : pcf.api.Destination {
      return pcf.UserPreferencesWorksheet.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=DesktopMenuActions_NewSuspensePayment) at DesktopMenuActions.pcf: line 14, column 78
    function action_dest_7 () : pcf.api.Destination {
      return pcf.NewSuspensePayment.createDestination(gw.api.util.CurrencyUtil.getDefaultCurrency())
    }
    
    // 'def' attribute on MenuItemSetRef at DesktopMenuActions.pcf: line 123, column 64
    function def_onEnter_56 (def :  pcf.NewActivityMenuItemSet) : void {
      def.onEnter(false, null, null, null)
    }
    
    // 'def' attribute on MenuItemSetRef at DesktopMenuActions.pcf: line 123, column 64
    function def_refreshVariables_57 (def :  pcf.NewActivityMenuItemSet) : void {
      def.refreshVariables(false, null, null, null)
    }
    
    // 'value' attribute on MenuItemIterator at DesktopMenuActions.pcf: line 19, column 68
    function value_4 () : java.util.List<typekey.Currency> {
      return Currency.getTypeKeys(false)
    }
    
    // 'visible' attribute on MenuItemIterator at DesktopMenuActions.pcf: line 19, column 68
    function visible_0 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on MenuItem (id=DesktopMenuActions_MultiPaymentEntryWizard) at DesktopMenuActions.pcf: line 30, column 60
    function visible_13 () : java.lang.Boolean {
      return perm.DirectBillMoneyRcvd.pmntmanmultproc
    }
    
    // 'visible' attribute on MenuItem (id=DesktopMenuActions_NewBatchPayment) at DesktopMenuActions.pcf: line 50, column 108
    function visible_23 () : java.lang.Boolean {
      return gw.api.system.BCConfigParameters.EnableBatchPayments.Value and perm.BatchPayment.process
    }
    
    // 'visible' attribute on MenuItem (id=DesktopMenuActions_NewDisbursementTransaction) at DesktopMenuActions.pcf: line 70, column 48
    function visible_26 () : java.lang.Boolean {
      return perm.Transaction.disbcreate
    }
    
    // 'visible' attribute on MenuItem (id=DesktopMenuActions_NewTransactionTransfer) at DesktopMenuActions.pcf: line 75, column 44
    function visible_29 () : java.lang.Boolean {
      return perm.Transaction.cashtx
    }
    
    // 'visible' attribute on MenuItem (id=DesktopMenuActions_NewTransactionWriteoff) at DesktopMenuActions.pcf: line 80, column 71
    function visible_32 () : java.lang.Boolean {
      return perm.Transaction.acctwo or perm.Transaction.plcywo
    }
    
    // 'visible' attribute on MenuItem (id=DesktopMenuActions_NewTransactionSubrogation) at DesktopMenuActions.pcf: line 90, column 45
    function visible_38 () : java.lang.Boolean {
      return perm.Transaction.subrtxn
    }
    
    // 'visible' attribute on MenuItem (id=DesktopMenuActions_NewChargeReversal) at DesktopMenuActions.pcf: line 95, column 44
    function visible_41 () : java.lang.Boolean {
      return perm.Transaction.revtxn
    }
    
    // 'visible' attribute on MenuItem (id=DesktopMenuActions_NewSuspensePayment) at DesktopMenuActions.pcf: line 14, column 78
    function visible_5 () : java.lang.Boolean {
      return perm.SuspensePayment.process and  perm.System.susppmntproc
    }
    
    // 'visible' attribute on MenuItem (id=DesktopMenuActions_NewFundsTransferReversal) at DesktopMenuActions.pcf: line 110, column 49
    function visible_50 () : java.lang.Boolean {
      return perm.System.transferreversal
    }
    
    // 'visible' attribute on MenuItem (id=DesktopMenuActions_NewTransactionGeneral) at DesktopMenuActions.pcf: line 115, column 44
    function visible_53 () : java.lang.Boolean {
      return perm.Transaction.gentxn
    }
    
    // 'visible' attribute on MenuItem (id=NewAssignedActivity) at DesktopMenuActions.pcf: line 121, column 38
    function visible_58 () : java.lang.Boolean {
      return perm.Activity.create
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends DesktopMenuActionsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=MultiplePaymentCurrencyItem) at DesktopMenuActions.pcf: line 39, column 31
    function action_9 () : void {
      pcf.MultiPaymentEntryWizard.go(currency)
    }
    
    // 'action' attribute on MenuItem (id=MultiplePaymentCurrencyItem) at DesktopMenuActions.pcf: line 39, column 31
    function action_dest_10 () : pcf.api.Destination {
      return pcf.MultiPaymentEntryWizard.createDestination(currency)
    }
    
    // 'label' attribute on MenuItem (id=MultiplePaymentCurrencyItem) at DesktopMenuActions.pcf: line 39, column 31
    function label_11 () : java.lang.Object {
      return currency
    }
    
    property get currency () : typekey.Currency {
      return getIteratedValue(1) as typekey.Currency
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends DesktopMenuActionsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=BatchPaymentsCurrencyItem) at DesktopMenuActions.pcf: line 59, column 31
    function action_19 () : void {
      pcf.NewBatchPaymentPage.go(currency)
    }
    
    // 'action' attribute on MenuItem (id=BatchPaymentsCurrencyItem) at DesktopMenuActions.pcf: line 59, column 31
    function action_dest_20 () : pcf.api.Destination {
      return pcf.NewBatchPaymentPage.createDestination(currency)
    }
    
    // 'label' attribute on MenuItem (id=BatchPaymentsCurrencyItem) at DesktopMenuActions.pcf: line 59, column 31
    function label_21 () : java.lang.Object {
      return currency
    }
    
    property get currency () : typekey.Currency {
      return getIteratedValue(1) as typekey.Currency
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DesktopMenuActionsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=SuspensePaymentCurrencyItem) at DesktopMenuActions.pcf: line 23, column 31
    function action_1 () : void {
      NewSuspensePayment.go(currency)
    }
    
    // 'action' attribute on MenuItem (id=SuspensePaymentCurrencyItem) at DesktopMenuActions.pcf: line 23, column 31
    function action_dest_2 () : pcf.api.Destination {
      return pcf.NewSuspensePayment.createDestination(currency)
    }
    
    // 'label' attribute on MenuItem (id=SuspensePaymentCurrencyItem) at DesktopMenuActions.pcf: line 23, column 31
    function label_3 () : java.lang.Object {
      return currency
    }
    
    property get currency () : typekey.Currency {
      return getIteratedValue(1) as typekey.Currency
    }
    
    
  }
  
  
}