package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/multicurrencyplans/PlanMultiCurrencyFeeThresholdInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanMultiCurrencyFeeThresholdInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/multicurrencyplans/PlanMultiCurrencyFeeThresholdInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PlanMultiCurrencyFeeThresholdInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=FeeThreshold_Input) at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 62, column 112
    function currency_23 () : typekey.Currency {
      return currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=FeeThreshold_Input) at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 62, column 112
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      (plan[feeThresholdDefaultPropertyInfo.Name + currency.Code] as FeeThresholdDefault).Value = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'editable' attribute on MonetaryAmountInput (id=FeeThreshold_Input) at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 62, column 112
    function editable_16 () : java.lang.Boolean {
      return alwaysEditable || !plan.InUse || (plan[feeThresholdDefaultPropertyInfo.Name + currency.Code] as KeyableBean).New
    }
    
    // 'label' attribute on MonetaryAmountInput (id=FeeThreshold_Input) at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 62, column 112
    function label_18 () : java.lang.Object {
      return currency.DisplayName
    }
    
    // 'required' attribute on MonetaryAmountInput (id=FeeThreshold_Input) at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 62, column 112
    function required_19 () : java.lang.Boolean {
      return required
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=FeeThreshold_Input) at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 62, column 112
    function validationExpression_17 () : java.lang.Object {
      return validationFunc?.invokeWithArgs({plan, currency})
    }
    
    // 'value' attribute on MonetaryAmountInput (id=FeeThreshold_Input) at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 62, column 112
    function valueRoot_22 () : java.lang.Object {
      return (plan[feeThresholdDefaultPropertyInfo.Name + currency.Code] as FeeThresholdDefault)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=FeeThreshold_Input) at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 62, column 112
    function value_20 () : gw.pl.currency.MonetaryAmount {
      return (plan[feeThresholdDefaultPropertyInfo.Name + currency.Code] as FeeThresholdDefault).Value
    }
    
    property get currency () : Currency {
      return getIteratedValue(1) as Currency
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/multicurrencyplans/PlanMultiCurrencyFeeThresholdInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanMultiCurrencyFeeThresholdInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=FeeThresholdSingleCurrency_Input) at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 42, column 41
    function currency_10 () : typekey.Currency {
      return plan.Currencies[0]
    }
    
    // 'value' attribute on MonetaryAmountInput (id=FeeThresholdSingleCurrency_Input) at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 42, column 41
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      (plan[feeThresholdDefaultPropertyInfo.Name + plan.Currencies[0].Code] as FeeThresholdDefault).Value = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'editable' attribute on MonetaryAmountInput (id=FeeThresholdSingleCurrency_Input) at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 42, column 41
    function editable_2 () : java.lang.Boolean {
      return !plan.InUse || alwaysEditable
    }
    
    // 'initialValue' attribute on Variable at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 29, column 26
    function initialValue_0 () : Currency[] {
      return plan.Currencies
    }
    
    // 'initialValue' attribute on Variable at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 33, column 23
    function initialValue_1 () : Boolean {
      return isMultiCurrencyMode()
    }
    
    // 'label' attribute on MonetaryAmountInput (id=FeeThresholdSingleCurrency_Input) at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 42, column 41
    function label_5 () : java.lang.Object {
      return feeThresholdLabel
    }
    
    // 'required' attribute on MonetaryAmountInput (id=FeeThresholdSingleCurrency_Input) at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 42, column 41
    function required_6 () : java.lang.Boolean {
      return required
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=FeeThresholdSingleCurrency_Input) at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 42, column 41
    function validationExpression_3 () : java.lang.Object {
      return validationFunc?.invokeWithArgs({plan, plan.Currencies[0]})
    }
    
    // 'value' attribute on MonetaryAmountInput (id=FeeThresholdSingleCurrency_Input) at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 42, column 41
    function valueRoot_9 () : java.lang.Object {
      return (plan[feeThresholdDefaultPropertyInfo.Name + plan.Currencies[0].Code] as FeeThresholdDefault)
    }
    
    // 'value' attribute on InputIterator at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 54, column 34
    function value_28 () : Currency[] {
      return currencies //Changing this to be backed by a query will almost certainly cause this page to break because of the use of forceRefresh property on the iterator
    }
    
    // 'value' attribute on MonetaryAmountInput (id=FeeThresholdSingleCurrency_Input) at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 42, column 41
    function value_7 () : gw.pl.currency.MonetaryAmount {
      return (plan[feeThresholdDefaultPropertyInfo.Name + plan.Currencies[0].Code] as FeeThresholdDefault).Value
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=FeeThresholdSingleCurrency_Input) at PlanMultiCurrencyFeeThresholdInputSet.pcf: line 42, column 41
    function visible_4 () : java.lang.Boolean {
      return !isMultiCurrencyMode()
    }
    
    property get alwaysEditable () : Boolean {
      return getRequireValue("alwaysEditable", 0) as Boolean
    }
    
    property set alwaysEditable ($arg :  Boolean) {
      setRequireValue("alwaysEditable", 0, $arg)
    }
    
    property get currencies () : Currency[] {
      return getVariableValue("currencies", 0) as Currency[]
    }
    
    property set currencies ($arg :  Currency[]) {
      setVariableValue("currencies", 0, $arg)
    }
    
    property get feeThresholdDefaultPropertyInfo () : gw.lang.reflect.IPropertyInfo {
      return getRequireValue("feeThresholdDefaultPropertyInfo", 0) as gw.lang.reflect.IPropertyInfo
    }
    
    property set feeThresholdDefaultPropertyInfo ($arg :  gw.lang.reflect.IPropertyInfo) {
      setRequireValue("feeThresholdDefaultPropertyInfo", 0, $arg)
    }
    
    property get feeThresholdLabel () : String {
      return getRequireValue("feeThresholdLabel", 0) as String
    }
    
    property set feeThresholdLabel ($arg :  String) {
      setRequireValue("feeThresholdLabel", 0, $arg)
    }
    
    property get multiCurrencyMode () : Boolean {
      return getVariableValue("multiCurrencyMode", 0) as Boolean
    }
    
    property set multiCurrencyMode ($arg :  Boolean) {
      setVariableValue("multiCurrencyMode", 0, $arg)
    }
    
    property get plan () : MultiCurrencyPlan {
      return getRequireValue("plan", 0) as MultiCurrencyPlan
    }
    
    property set plan ($arg :  MultiCurrencyPlan) {
      setRequireValue("plan", 0, $arg)
    }
    
    property get required () : boolean {
      return getRequireValue("required", 0) as java.lang.Boolean
    }
    
    property set required ($arg :  boolean) {
      setRequireValue("required", 0, $arg)
    }
    
    property get validationFunc () : gw.lang.function.IBlock {
      return getRequireValue("validationFunc", 0) as gw.lang.function.IBlock
    }
    
    property set validationFunc ($arg :  gw.lang.function.IBlock) {
      setRequireValue("validationFunc", 0, $arg)
    }
    
    function isMultiCurrencyMode() : boolean {
          return gw.api.util.CurrencyUtil.isMultiCurrencyMode() and Currency.getTypeKeys(true).Count > 1
        }
    
    
  }
  
  
}