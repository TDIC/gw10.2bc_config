package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyContactDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyContactDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyContactDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyContactDetailDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at PolicyContactDetailDV.pcf: line 101, column 55
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyContactRole.Role = (__VALUE_TO_SET as typekey.PolicyPeriodRole)
    }
    
    // 'requestValidationExpression' attribute on TypeKeyCell (id=Role_Cell) at PolicyContactDetailDV.pcf: line 101, column 55
    function requestValidationExpression_50 (VALUE :  typekey.PolicyPeriodRole) : java.lang.Object {
      return VALUE != PolicyPeriodRole.TC_PRIMARYINSURED || hasPrimaryInsured()== null ? null : DisplayKey.get("Web.PolicyPeriodContacts.ErrorMessage.PolicyPeriodCannotHaveMoreThanOnePrimaryInsured", hasPrimaryInsured())
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at PolicyContactDetailDV.pcf: line 101, column 55
    function valueRoot_53 () : java.lang.Object {
      return policyContactRole
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at PolicyContactDetailDV.pcf: line 101, column 55
    function value_51 () : typekey.PolicyPeriodRole {
      return policyContactRole.Role
    }
    
    property get policyContactRole () : entity.PolPeriodContactRole {
      return getIteratedValue(1) as entity.PolPeriodContactRole
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PolicyContactDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyContactDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at PolicyContactDetailDV.pcf: line 47, column 43
    function def_onEnter_16 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at PolicyContactDetailDV.pcf: line 47, column 43
    function def_onEnter_18 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at PolicyContactDetailDV.pcf: line 47, column 43
    function def_onEnter_20 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef at PolicyContactDetailDV.pcf: line 71, column 222
    function def_onEnter_43 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.BusinessPhoneOwner(new gw.api.phone.ContactPhoneDelegate(policyPeriodContact.Contact, Contact#WorkPhone), DisplayKey.get("Web.ContactDetailDV.Phone"), false))
    }
    
    // 'def' attribute on InputSetRef at PolicyContactDetailDV.pcf: line 34, column 102
    function def_onEnter_5 (def :  pcf.NameInputSet_Contact) : void {
      def.onEnter(new gw.api.name.BCNameOwner(policyPeriodContact.Contact))
    }
    
    // 'def' attribute on InputSetRef at PolicyContactDetailDV.pcf: line 34, column 102
    function def_onEnter_7 (def :  pcf.NameInputSet_Person) : void {
      def.onEnter(new gw.api.name.BCNameOwner(policyPeriodContact.Contact))
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at PolicyContactDetailDV.pcf: line 47, column 43
    function def_refreshVariables_17 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at PolicyContactDetailDV.pcf: line 47, column 43
    function def_refreshVariables_19 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at PolicyContactDetailDV.pcf: line 47, column 43
    function def_refreshVariables_21 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef at PolicyContactDetailDV.pcf: line 71, column 222
    function def_refreshVariables_44 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.BusinessPhoneOwner(new gw.api.phone.ContactPhoneDelegate(policyPeriodContact.Contact, Contact#WorkPhone), DisplayKey.get("Web.ContactDetailDV.Phone"), false))
    }
    
    // 'def' attribute on InputSetRef at PolicyContactDetailDV.pcf: line 34, column 102
    function def_refreshVariables_6 (def :  pcf.NameInputSet_Contact) : void {
      def.refreshVariables(new gw.api.name.BCNameOwner(policyPeriodContact.Contact))
    }
    
    // 'def' attribute on InputSetRef at PolicyContactDetailDV.pcf: line 34, column 102
    function def_refreshVariables_8 (def :  pcf.NameInputSet_Person) : void {
      def.refreshVariables(new gw.api.name.BCNameOwner(policyPeriodContact.Contact))
    }
    
    // 'value' attribute on TextInput (id=Email_Input) at PolicyContactDetailDV.pcf: line 76, column 60
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriodContact.Contact.EmailAddress1 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'encryptionExpression' attribute on PrivacyInput (id=SSN_Input) at PolicyContactDetailDV.pcf: line 55, column 70
    function encryptionExpression_27 (VALUE :  java.lang.String) : java.lang.String {
      return gw.api.privacy.EncryptionMaskExpressions.maskTaxId(VALUE)
    }
    
    // 'initialValue' attribute on Variable at PolicyContactDetailDV.pcf: line 17, column 43
    function initialValue_0 () : gw.api.address.AddressOwner {
      return new gw.api.address.ContactAddressOwner(policyPeriodContact.Contact)
    }
    
    // 'initialValue' attribute on Variable at PolicyContactDetailDV.pcf: line 22, column 35
    function initialValue_1 () : entity.PolicyPeriod {
      return policyPeriodContact.PolicyPeriod
    }
    
    // 'mode' attribute on InputSetRef (id=globalAddressContainer) at PolicyContactDetailDV.pcf: line 47, column 43
    function mode_22 () : java.lang.Object {
      return gw.api.address.AddressCountrySettings.getSettings(addressOwner.SelectedCountry).PCFMode
    }
    
    // 'mode' attribute on InputSetRef at PolicyContactDetailDV.pcf: line 34, column 102
    function mode_9 () : java.lang.Object {
      return policyPeriodContact.Contact typeis Person ? "Person" : "Contact"
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at PolicyContactDetailDV.pcf: line 101, column 55
    function sortValue_49 (policyContactRole :  entity.PolPeriodContactRole) : java.lang.Object {
      return policyContactRole.Role
    }
    
    // 'toAdd' attribute on RowIterator at PolicyContactDetailDV.pcf: line 92, column 55
    function toAdd_55 (policyContactRole :  entity.PolPeriodContactRole) : void {
      policyPeriodContact.addToRoles(policyContactRole)
    }
    
    // 'toRemove' attribute on RowIterator at PolicyContactDetailDV.pcf: line 92, column 55
    function toRemove_56 (policyContactRole :  entity.PolPeriodContactRole) : void {
      policyPeriodContact.removeFromRoles(policyContactRole)
    }
    
    // 'value' attribute on TypeKeyInput (id=Credential_Input) at PolicyContactDetailDV.pcf: line 40, column 62
    function valueRoot_12 () : java.lang.Object {
      return (policyPeriodContact.Contact as Person)
    }
    
    // 'value' attribute on PrivacyInput (id=SSN_Input) at PolicyContactDetailDV.pcf: line 55, column 70
    function valueRoot_26 () : java.lang.Object {
      return policyPeriodContact.Contact
    }
    
    // 'value' attribute on TextInput (id=Type_Input) at PolicyContactDetailDV.pcf: line 29, column 65
    function valueRoot_3 () : java.lang.Object {
      return policyPeriodContact.Contact.Subtype
    }
    
    // 'value' attribute on TypeKeyInput (id=Credential_Input) at PolicyContactDetailDV.pcf: line 40, column 62
    function value_11 () : typekey.Credential_TDIC {
      return (policyPeriodContact.Contact as Person).Credential_TDIC
    }
    
    // 'value' attribute on TextInput (id=Type_Input) at PolicyContactDetailDV.pcf: line 29, column 65
    function value_2 () : java.lang.String {
      return policyPeriodContact.Contact.Subtype.DisplayName
    }
    
    // 'value' attribute on PrivacyInput (id=SSN_Input) at PolicyContactDetailDV.pcf: line 55, column 70
    function value_25 () : java.lang.String {
      return policyPeriodContact.Contact.SSNOfficialID
    }
    
    // 'value' attribute on TextInput (id=FEIN_Input) at PolicyContactDetailDV.pcf: line 60, column 71
    function value_31 () : java.lang.String {
      return policyPeriodContact.Contact.FEINOfficialID
    }
    
    // 'value' attribute on TextInput (id=ADANumber_Input) at PolicyContactDetailDV.pcf: line 65, column 62
    function value_36 () : java.lang.String {
      return policyPeriodContact.Contact.ADANumberOfficialID_TDIC
    }
    
    // 'value' attribute on TextInput (id=VendorNumber_Input) at PolicyContactDetailDV.pcf: line 69, column 59
    function value_40 () : java.lang.String {
      return policyPeriodContact.Contact.VendorNumber
    }
    
    // 'value' attribute on TextInput (id=Email_Input) at PolicyContactDetailDV.pcf: line 76, column 60
    function value_45 () : java.lang.String {
      return policyPeriodContact.Contact.EmailAddress1
    }
    
    // 'value' attribute on RowIterator at PolicyContactDetailDV.pcf: line 92, column 55
    function value_57 () : entity.PolPeriodContactRole[] {
      return policyPeriodContact.Roles
    }
    
    // 'visible' attribute on TypeKeyInput (id=Credential_Input) at PolicyContactDetailDV.pcf: line 40, column 62
    function visible_10 () : java.lang.Boolean {
      return policyPeriodContact.Contact typeis Person
    }
    
    // 'visible' attribute on InputDivider at PolicyContactDetailDV.pcf: line 42, column 47
    function visible_15 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'visible' attribute on PrivacyInput (id=SSN_Input) at PolicyContactDetailDV.pcf: line 55, column 70
    function visible_24 () : java.lang.Boolean {
      return policyPeriodContact.Contact.SSNOfficialID != null
    }
    
    // 'visible' attribute on TextInput (id=FEIN_Input) at PolicyContactDetailDV.pcf: line 60, column 71
    function visible_30 () : java.lang.Boolean {
      return policyPeriodContact.Contact.FEINOfficialID != null
    }
    
    property get addressOwner () : gw.api.address.AddressOwner {
      return getVariableValue("addressOwner", 0) as gw.api.address.AddressOwner
    }
    
    property set addressOwner ($arg :  gw.api.address.AddressOwner) {
      setVariableValue("addressOwner", 0, $arg)
    }
    
    property get isNew () : Boolean {
      return getRequireValue("isNew", 0) as Boolean
    }
    
    property set isNew ($arg :  Boolean) {
      setRequireValue("isNew", 0, $arg)
    }
    
    property get policyPeriod () : entity.PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as entity.PolicyPeriod
    }
    
    property set policyPeriod ($arg :  entity.PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get policyPeriodContact () : PolicyPeriodContact {
      return getRequireValue("policyPeriodContact", 0) as PolicyPeriodContact
    }
    
    property set policyPeriodContact ($arg :  PolicyPeriodContact) {
      setRequireValue("policyPeriodContact", 0, $arg)
    }
    
    function hasPrimaryInsured() : String {
      var pp = policyPeriodContact.getPolicyPeriod()
      for(c in pp.getContacts()){
        if(c != policyPeriodContact && c.PrimaryPolicyPeriod == pp){
          return c.getContact().DisplayName
        }
      }
      return null
    }
    
    
  }
  
  
}