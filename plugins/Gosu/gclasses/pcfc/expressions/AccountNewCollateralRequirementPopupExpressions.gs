package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/AccountNewCollateralRequirementPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountNewCollateralRequirementPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/collateral/AccountNewCollateralRequirementPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountNewCollateralRequirementPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (collateral :  Collateral) : int {
      return 0
    }
    
    // 'afterCommit' attribute on Popup (id=AccountNewCollateralRequirementPopup) at AccountNewCollateralRequirementPopup.pcf: line 10, column 76
    function afterCommit_7 (TopLocation :  pcf.api.Location) : void {
      AccountCollateral.go(collateral.Account)
    }
    
    // 'def' attribute on PanelRef at AccountNewCollateralRequirementPopup.pcf: line 34, column 59
    function def_onEnter_5 (def :  pcf.CollateralRequirementDV) : void {
      def.onEnter(requirement, true)
    }
    
    // 'def' attribute on PanelRef at AccountNewCollateralRequirementPopup.pcf: line 34, column 59
    function def_refreshVariables_6 (def :  pcf.CollateralRequirementDV) : void {
      def.refreshVariables(requirement, true)
    }
    
    // 'initialValue' attribute on Variable at AccountNewCollateralRequirementPopup.pcf: line 19, column 37
    function initialValue_0 () : CollateralRequirement {
      return createNewRequirement()
    }
    
    // 'initialValue' attribute on Variable at AccountNewCollateralRequirementPopup.pcf: line 23, column 49
    function initialValue_1 () : gw.api.web.account.CollateralUtil {
      return new gw.api.web.account.CollateralUtil()
    }
    
    // EditButtons at AccountNewCollateralRequirementPopup.pcf: line 27, column 23
    function label_2 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'label' attribute on AlertBar (id=effectiveDateAlert) at AccountNewCollateralRequirementPopup.pcf: line 32, column 35
    function label_4 () : java.lang.Object {
      return DisplayKey.get("Web.NewCollateralRequirement.Alert", collateralUtil.getNextInvoiceDate(requirement))
    }
    
    // 'visible' attribute on AlertBar (id=effectiveDateAlert) at AccountNewCollateralRequirementPopup.pcf: line 32, column 35
    function visible_3 () : java.lang.Boolean {
      return showAlertBar()
    }
    
    override property get CurrentLocation () : pcf.AccountNewCollateralRequirementPopup {
      return super.CurrentLocation as pcf.AccountNewCollateralRequirementPopup
    }
    
    property get collateral () : Collateral {
      return getVariableValue("collateral", 0) as Collateral
    }
    
    property set collateral ($arg :  Collateral) {
      setVariableValue("collateral", 0, $arg)
    }
    
    property get collateralUtil () : gw.api.web.account.CollateralUtil {
      return getVariableValue("collateralUtil", 0) as gw.api.web.account.CollateralUtil
    }
    
    property set collateralUtil ($arg :  gw.api.web.account.CollateralUtil) {
      setVariableValue("collateralUtil", 0, $arg)
    }
    
    property get requirement () : CollateralRequirement {
      return getVariableValue("requirement", 0) as CollateralRequirement
    }
    
    property set requirement ($arg :  CollateralRequirement) {
      setVariableValue("requirement", 0, $arg)
    }
    
    function createNewRequirement() : CollateralRequirement{
              var collReq = new CollateralRequirement(collateral.Currency)
              collateral.addToRequirements(collReq);
              collReq.CreateCharge = collateral.isChargeAllocated();
              return collReq;
            }
            
            function showAlertBar() : boolean{
              if(requirement.RequirementType == TC_CASH
                    && gw.api.util.DateUtil.compareIgnoreTime(collateralUtil.getNextInvoiceDate(requirement), requirement.EffectiveDate) > 0  
                    && requirement.CreateCharge
                    && requirement.isNewChargeCreatedIfRequired())
                  return true;
              return false;
            }
    
    
  }
  
  
}