package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanFeeHandlingInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BillingPlanFeeHandlingInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanFeeHandlingInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BillingPlanFeeHandlingInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=InvoiceFeeDefaults) at BillingPlanFeeHandlingInputSet.default.pcf: line 16, column 32
    function def_onEnter_0 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(billingPlan, BillingPlan#InvoiceFeeDefaults.PropertyInfo, DisplayKey.get("Web.BillingPlanDetailDV.InvoiceFeeAmount"), false, false, null)
    }
    
    // 'def' attribute on InputSetRef (id=PaymentReversalFeeDefaults) at BillingPlanFeeHandlingInputSet.default.pcf: line 19, column 40
    function def_onEnter_2 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(billingPlan, BillingPlan#PaymentReversalFeeDefaults.PropertyInfo, DisplayKey.get("Web.BillingPlanDetailDV.PaymentReversalFeeAmount"), false, false, null)
    }
    
    // 'def' attribute on InputSetRef (id=InvoiceFeeDefaults) at BillingPlanFeeHandlingInputSet.default.pcf: line 16, column 32
    function def_refreshVariables_1 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(billingPlan, BillingPlan#InvoiceFeeDefaults.PropertyInfo, DisplayKey.get("Web.BillingPlanDetailDV.InvoiceFeeAmount"), false, false, null)
    }
    
    // 'def' attribute on InputSetRef (id=PaymentReversalFeeDefaults) at BillingPlanFeeHandlingInputSet.default.pcf: line 19, column 40
    function def_refreshVariables_3 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(billingPlan, BillingPlan#PaymentReversalFeeDefaults.PropertyInfo, DisplayKey.get("Web.BillingPlanDetailDV.PaymentReversalFeeAmount"), false, false, null)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SkipInstallmentFees_Input) at BillingPlanFeeHandlingInputSet.default.pcf: line 25, column 48
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.SkipInstallmentFees = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'editable' attribute on BooleanRadioInput (id=SkipInstallmentFees_Input) at BillingPlanFeeHandlingInputSet.default.pcf: line 25, column 48
    function editable_4 () : java.lang.Boolean {
      return planNotInUse
    }
    
    // 'value' attribute on BooleanRadioInput (id=SkipInstallmentFees_Input) at BillingPlanFeeHandlingInputSet.default.pcf: line 25, column 48
    function valueRoot_7 () : java.lang.Object {
      return billingPlan
    }
    
    // 'value' attribute on BooleanRadioInput (id=SkipInstallmentFees_Input) at BillingPlanFeeHandlingInputSet.default.pcf: line 25, column 48
    function value_5 () : java.lang.Boolean {
      return billingPlan.SkipInstallmentFees
    }
    
    property get billingPlan () : BillingPlan {
      return getRequireValue("billingPlan", 0) as BillingPlan
    }
    
    property set billingPlan ($arg :  BillingPlan) {
      setRequireValue("billingPlan", 0, $arg)
    }
    
    property get planNotInUse () : boolean {
      return getRequireValue("planNotInUse", 0) as java.lang.Boolean
    }
    
    property set planNotInUse ($arg :  boolean) {
      setRequireValue("planNotInUse", 0, $arg)
    }
    
    
  }
  
  
}