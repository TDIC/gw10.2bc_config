package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/InvoiceStreamAnchorDateInputSet.twicepermonth.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceStreamAnchorDateInputSet_twicepermonthExpressions {
  @javax.annotation.Generated("config/web/pcf/account/InvoiceStreamAnchorDateInputSet.twicepermonth.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceStreamAnchorDateInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=OverridingFirstInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.twicepermonth.pcf: line 39, column 98
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceDayChangeHelper.OverridingAnchorDateViews[0].DayOfMonth = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=OverridingSecondInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.twicepermonth.pcf: line 49, column 98
    function defaultSetter_24 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceDayChangeHelper.OverridingAnchorDateViews[1].DayOfMonth = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverrideInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.twicepermonth.pcf: line 29, column 44
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceDayChangeHelper.OverrideAnchorDates = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'editable' attribute on TextInput (id=OverridingFirstInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.twicepermonth.pcf: line 39, column 98
    function editable_14 () : java.lang.Boolean {
      return isEditMode && invoiceDayChangeHelper.OverrideAnchorDates
    }
    
    // 'editable' attribute on BooleanRadioInput (id=OverrideInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.twicepermonth.pcf: line 29, column 44
    function editable_6 () : java.lang.Boolean {
      return isEditMode
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverrideInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.twicepermonth.pcf: line 29, column 44
    function valueRoot_10 () : java.lang.Object {
      return invoiceDayChangeHelper
    }
    
    // 'value' attribute on TextInput (id=AccountInvoiceDays_Input) at InvoiceStreamAnchorDateInputSet.twicepermonth.pcf: line 17, column 253
    function value_0 () : java.lang.String {
      return DisplayKey.get("Web.AccountDetailInvoices.InvoiceStream.AccountTwicePerMonthInvoiceDayValues", invoiceDayChangeHelper.PayerDefaultAnchorDateViews[0].DayOfMonth, invoiceDayChangeHelper.PayerDefaultAnchorDateViews[1].DayOfMonth)
    }
    
    // 'value' attribute on TextInput (id=OverridingFirstInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.twicepermonth.pcf: line 39, column 98
    function value_16 () : java.lang.Integer {
      return invoiceDayChangeHelper.OverridingAnchorDateViews[0].DayOfMonth
    }
    
    // 'value' attribute on TextInput (id=OverridingSecondInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.twicepermonth.pcf: line 49, column 98
    function value_23 () : java.lang.Integer {
      return invoiceDayChangeHelper.OverridingAnchorDateViews[1].DayOfMonth
    }
    
    // 'value' attribute on TextInput (id=OverridingInvoiceDays_Input) at InvoiceStreamAnchorDateInputSet.twicepermonth.pcf: line 22, column 100
    function value_3 () : java.lang.String {
      return DisplayKey.get("Web.AccountDetailInvoices.InvoiceStream.AccountTwicePerMonthInvoiceDayValues", invoiceDayChangeHelper.OverridingAnchorDateViews[0].DayOfMonth, invoiceDayChangeHelper.OverridingAnchorDateViews[1].DayOfMonth)
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverrideInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.twicepermonth.pcf: line 29, column 44
    function value_8 () : java.lang.Boolean {
      return invoiceDayChangeHelper.OverrideAnchorDates
    }
    
    // 'visible' attribute on TextInput (id=OverridingFirstInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.twicepermonth.pcf: line 39, column 98
    function visible_15 () : java.lang.Boolean {
      return invoiceDayChangeHelper.OverrideAnchorDates && CurrentLocation.InEditMode
    }
    
    // 'visible' attribute on TextInput (id=OverridingInvoiceDays_Input) at InvoiceStreamAnchorDateInputSet.twicepermonth.pcf: line 22, column 100
    function visible_2 () : java.lang.Boolean {
      return invoiceDayChangeHelper.OverrideAnchorDates && !CurrentLocation.InEditMode
    }
    
    // 'visible' attribute on BooleanRadioInput (id=OverrideInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.twicepermonth.pcf: line 29, column 44
    function visible_7 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    property get invoiceDayChangeHelper () : gw.api.web.invoice.InvoiceDayChangeHelper {
      return getRequireValue("invoiceDayChangeHelper", 0) as gw.api.web.invoice.InvoiceDayChangeHelper
    }
    
    property set invoiceDayChangeHelper ($arg :  gw.api.web.invoice.InvoiceDayChangeHelper) {
      setRequireValue("invoiceDayChangeHelper", 0, $arg)
    }
    
    property get isEditMode () : boolean {
      return getRequireValue("isEditMode", 0) as java.lang.Boolean
    }
    
    property set isEditMode ($arg :  boolean) {
      setRequireValue("isEditMode", 0, $arg)
    }
    
    
  }
  
  
}