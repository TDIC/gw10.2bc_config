package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyDisbursementDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyDisbursementDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyDisbursementDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyDisbursementDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=unapplied_Input) at AgencyDisbursementDetailScreen.pcf: line 46, column 61
    function currency_7 () : typekey.Currency {
      return disbursementVar.Currency
    }
    
    // 'def' attribute on PanelRef at AgencyDisbursementDetailScreen.pcf: line 78, column 76
    function def_onEnter_22 (def :  pcf.CreateDisbursementDetailDV) : void {
      def.onEnter(disbursementVar, !fromCreditItems)
    }
    
    // 'def' attribute on PanelRef at AgencyDisbursementDetailScreen.pcf: line 78, column 76
    function def_refreshVariables_23 (def :  pcf.CreateDisbursementDetailDV) : void {
      def.refreshVariables(disbursementVar, !fromCreditItems)
    }
    
    // 'value' attribute on RangeRadioInput (id=payCommission_Input) at AgencyDisbursementDetailScreen.pcf: line 71, column 41
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      payCommissionReference[0] = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'initialValue' attribute on Variable at AgencyDisbursementDetailScreen.pcf: line 13, column 30
    function initialValue_0 () : java.util.Date {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'initialValue' attribute on Variable at AgencyDisbursementDetailScreen.pcf: line 17, column 23
    function initialValue_1 () : Boolean {
      return perm.Transaction.disbpayeeedit
    }
    
    // 'onChange' attribute on PostOnChange at AgencyDisbursementDetailScreen.pcf: line 73, column 135
    function onChange_13 () : void {
      disbursementVar.Amount = disbursementVar.computeDisbursementAmount( invoiceItems,  payCommissionReference[0])
    }
    
    // 'optionLabel' attribute on RangeRadioInput (id=payCommission_Input) at AgencyDisbursementDetailScreen.pcf: line 71, column 41
    function optionLabel_16 (VALUE :  java.lang.Boolean) : java.lang.String {
      return VALUE ? DisplayKey.get("Web.AgencyDisbursementDetailScreen.DisburseNetValue") : DisplayKey.get("Web.AgencyDisbursementDetailScreen.DisburseGrossValue")
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=payCommission_Input) at AgencyDisbursementDetailScreen.pcf: line 71, column 41
    function valueRange_17 () : java.lang.Object {
      return new Boolean[] {true, false}
    }
    
    // 'value' attribute on TextInput (id=producer_Input) at AgencyDisbursementDetailScreen.pcf: line 39, column 40
    function valueRoot_3 () : java.lang.Object {
      return disbursementVar
    }
    
    // 'value' attribute on MonetaryAmountInput (id=unapplied_Input) at AgencyDisbursementDetailScreen.pcf: line 46, column 61
    function valueRoot_6 () : java.lang.Object {
      return disbursementVar.Producer
    }
    
    // 'value' attribute on RangeRadioInput (id=payCommission_Input) at AgencyDisbursementDetailScreen.pcf: line 71, column 41
    function value_14 () : java.lang.Boolean {
      return payCommissionReference[0]
    }
    
    // 'value' attribute on TextInput (id=producer_Input) at AgencyDisbursementDetailScreen.pcf: line 39, column 40
    function value_2 () : entity.Producer {
      return disbursementVar.Producer
    }
    
    // 'value' attribute on MonetaryAmountInput (id=unapplied_Input) at AgencyDisbursementDetailScreen.pcf: line 46, column 61
    function value_5 () : gw.pl.currency.MonetaryAmount {
      return disbursementVar.Producer.UnappliedAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=pending_Input) at AgencyDisbursementDetailScreen.pcf: line 53, column 65
    function value_9 () : gw.pl.currency.MonetaryAmount {
      return disbursementVar.Producer.PendingDisbursement
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=payCommission_Input) at AgencyDisbursementDetailScreen.pcf: line 71, column 41
    function verifyValueRangeIsAllowedType_18 ($$arg :  java.lang.Boolean[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=payCommission_Input) at AgencyDisbursementDetailScreen.pcf: line 71, column 41
    function verifyValueRangeIsAllowedType_18 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=payCommission_Input) at AgencyDisbursementDetailScreen.pcf: line 71, column 41
    function verifyValueRange_19 () : void {
      var __valueRangeArg = new Boolean[] {true, false}
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_18(__valueRangeArg)
    }
    
    // 'visible' attribute on DetailViewPanel (id=DisbursementMethodDV) at AgencyDisbursementDetailScreen.pcf: line 58, column 33
    function visible_21 () : java.lang.Boolean {
      return fromCreditItems
    }
    
    property get CanEditDisbursementPayee () : Boolean {
      return getVariableValue("CanEditDisbursementPayee", 0) as Boolean
    }
    
    property set CanEditDisbursementPayee ($arg :  Boolean) {
      setVariableValue("CanEditDisbursementPayee", 0, $arg)
    }
    
    property get disbursementVar () : AgencyDisbursement {
      return getRequireValue("disbursementVar", 0) as AgencyDisbursement
    }
    
    property set disbursementVar ($arg :  AgencyDisbursement) {
      setRequireValue("disbursementVar", 0, $arg)
    }
    
    property get fromCreditItems () : boolean {
      return getRequireValue("fromCreditItems", 0) as java.lang.Boolean
    }
    
    property set fromCreditItems ($arg :  boolean) {
      setRequireValue("fromCreditItems", 0, $arg)
    }
    
    property get invoiceItems () : List<InvoiceItem> {
      return getRequireValue("invoiceItems", 0) as List<InvoiceItem>
    }
    
    property set invoiceItems ($arg :  List<InvoiceItem>) {
      setRequireValue("invoiceItems", 0, $arg)
    }
    
    property get payCommissionReference () : boolean[] {
      return getRequireValue("payCommissionReference", 0) as boolean[]
    }
    
    property set payCommissionReference ($arg :  boolean[]) {
      setRequireValue("payCommissionReference", 0, $arg)
    }
    
    property get today () : java.util.Date {
      return getVariableValue("today", 0) as java.util.Date
    }
    
    property set today ($arg :  java.util.Date) {
      setVariableValue("today", 0, $arg)
    }
    
    
  }
  
  
}