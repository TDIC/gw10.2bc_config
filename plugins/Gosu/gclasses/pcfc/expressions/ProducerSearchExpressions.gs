package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ProducerSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerSearchExpressions {
  @javax.annotation.Generated("config/web/pcf/search/ProducerSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerSearchExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=ProducerSearch) at ProducerSearch.pcf: line 8, column 68
    static function canVisit_2 () : java.lang.Boolean {
      return perm.System.viewsearch and perm.System.prodsearch
    }
    
    // 'def' attribute on ScreenRef at ProducerSearch.pcf: line 10, column 47
    function def_onEnter_0 (def :  pcf.ProducerSearchScreen) : void {
      def.onEnter(true, true)
    }
    
    // 'def' attribute on ScreenRef at ProducerSearch.pcf: line 10, column 47
    function def_refreshVariables_1 (def :  pcf.ProducerSearchScreen) : void {
      def.refreshVariables(true, true)
    }
    
    // Page (id=ProducerSearch) at ProducerSearch.pcf: line 8, column 68
    static function parent_3 () : pcf.api.Destination {
      return pcf.SearchGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.ProducerSearch {
      return super.CurrentLocation as pcf.ProducerSearch
    }
    
    
  }
  
  
}