package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExecutedPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillExecutedPaymentsExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExecutedPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillExecutedPaymentsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    static function __constructorIndex (producer :  Producer, initialSelectedMoney :  AgencyBillMoneyRcvd) : int {
      return 1
    }
    
    // 'canVisit' attribute on Page (id=AgencyBillExecutedPayments) at AgencyBillExecutedPayments.pcf: line 8, column 79
    static function canVisit_121 (initialSelectedMoney :  AgencyBillMoneyRcvd, producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodpmntview
    }
    
    // Page (id=AgencyBillExecutedPayments) at AgencyBillExecutedPayments.pcf: line 8, column 79
    static function parent_122 (initialSelectedMoney :  AgencyBillMoneyRcvd, producer :  Producer) : pcf.api.Destination {
      return pcf.AgencyBillPayments.createDestination(producer)
    }
    
    override property get CurrentLocation () : pcf.AgencyBillExecutedPayments {
      return super.CurrentLocation as pcf.AgencyBillExecutedPayments
    }
    
    property get initialSelectedMoney () : AgencyBillMoneyRcvd {
      return getVariableValue("initialSelectedMoney", 0) as AgencyBillMoneyRcvd
    }
    
    property set initialSelectedMoney ($arg :  AgencyBillMoneyRcvd) {
      setVariableValue("initialSelectedMoney", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExecutedPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at AgencyBillExecutedPayments.pcf: line 130, column 85
    function action_20 () : void {
      TransactionDetailPopup.push(moneyReceived.MoneyReceivedTransaction)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at AgencyBillExecutedPayments.pcf: line 130, column 85
    function action_dest_21 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(moneyReceived.MoneyReceivedTransaction)
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillExecutedPayments.pcf: line 104, column 34
    function condition_12 () : java.lang.Boolean {
      return moneyReceived.Executed
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillExecutedPayments.pcf: line 107, column 47
    function condition_13 () : java.lang.Boolean {
      return not (moneyReceived.Reversed or moneyReceived.Modified) 
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillExecutedPayments.pcf: line 110, column 43
    function condition_14 () : java.lang.Boolean {
      return moneyReceived.BaseDist != null and moneyReceived.BaseDist.hasFrozenDistItem()
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillExecutedPayments.pcf: line 113, column 32
    function condition_15 () : java.lang.Boolean {
      return moneyReceived.BaseDist != null and moneyReceived.BaseDist.Frozen
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillExecutedPayments.pcf: line 116, column 39
    function condition_16 () : java.lang.Boolean {
      return (moneyReceived.BaseDist as AgencyCycleDist).SuspDistItemsThatHaveNotBeenReleased.HasElements
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=ReceivedAmount_Cell) at AgencyBillExecutedPayments.pcf: line 137, column 54
    function currency_27 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AgencyBillExecutedPayments.pcf: line 123, column 55
    function valueRoot_18 () : java.lang.Object {
      return moneyReceived
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at AgencyBillExecutedPayments.pcf: line 130, column 85
    function valueRoot_23 () : java.lang.Object {
      return moneyReceived.MoneyReceivedTransaction
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AgencyBillExecutedPayments.pcf: line 123, column 55
    function value_17 () : java.util.Date {
      return moneyReceived.ReceivedDate
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at AgencyBillExecutedPayments.pcf: line 130, column 85
    function value_22 () : java.lang.String {
      return moneyReceived.MoneyReceivedTransaction.TransactionNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ReceivedAmount_Cell) at AgencyBillExecutedPayments.pcf: line 137, column 54
    function value_25 () : gw.pl.currency.MonetaryAmount {
      return moneyReceived.TotalAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Distributed_Cell) at AgencyBillExecutedPayments.pcf: line 144, column 63
    function value_29 () : gw.pl.currency.MonetaryAmount {
      return moneyReceived.NetAmountDistributed
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Suspense_Cell) at AgencyBillExecutedPayments.pcf: line 151, column 60
    function value_33 () : gw.pl.currency.MonetaryAmount {
      return moneyReceived.NetSuspenseAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Remaining_Cell) at AgencyBillExecutedPayments.pcf: line 158, column 125
    function value_37 () : gw.pl.currency.MonetaryAmount {
      return moneyReceived.TotalAmount - moneyReceived.NetAmountDistributed - moneyReceived.NetSuspenseAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=WriteOff_Cell) at AgencyBillExecutedPayments.pcf: line 165, column 232
    function value_40 () : gw.pl.currency.MonetaryAmount {
      return moneyReceived.AgencyCyclePayment.ProducerWriteoffAmount == null ? new gw.pl.currency.MonetaryAmount(java.math.BigDecimal.ZERO, producer.Currency) : moneyReceived.AgencyCyclePayment.ProducerWriteoffAmount
    }
    
    // 'value' attribute on BooleanRadioCell (id=Reversed_Cell) at AgencyBillExecutedPayments.pcf: line 170, column 51
    function value_43 () : java.lang.Boolean {
      return moneyReceived.Reversed
    }
    
    // 'value' attribute on TextCell (id=PaymentInstrument_Cell) at AgencyBillExecutedPayments.pcf: line 175, column 57
    function value_46 () : entity.PaymentInstrument {
      return moneyReceived.PaymentInstrument
    }
    
    // 'value' attribute on TextCell (id=RefNumber_Cell) at AgencyBillExecutedPayments.pcf: line 181, column 52
    function value_49 () : java.lang.String {
      return moneyReceived.RefNumber
    }
    
    property get moneyReceived () : entity.AgencyBillMoneyRcvd {
      return getIteratedValue(2) as entity.AgencyBillMoneyRcvd
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExecutedPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends AgencyBillExecutedPaymentsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextInput (id=TransactionNumber_Input) at AgencyBillExecutedPayments.pcf: line 218, column 85
    function action_69 () : void {
      TransactionDetailPopup.push(selectedMoneyReceived.MoneyReceivedTransaction)
    }
    
    // 'action' attribute on TextInput (id=OriginalPayment_Input) at AgencyBillExecutedPayments.pcf: line 233, column 73
    function action_83 () : void {
      AgencyBillSelectPaymentForward.go((selectedMoneyReceived.MovedFromPMR as AgencyBillMoneyRcvd).Producer, selectedMoneyReceived.MovedFromPMR as AgencyBillMoneyRcvd)
    }
    
    // 'action' attribute on TextInput (id=TransactionNumber_Input) at AgencyBillExecutedPayments.pcf: line 218, column 85
    function action_dest_70 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(selectedMoneyReceived.MoneyReceivedTransaction)
    }
    
    // 'action' attribute on TextInput (id=OriginalPayment_Input) at AgencyBillExecutedPayments.pcf: line 233, column 73
    function action_dest_84 () : pcf.api.Destination {
      return pcf.AgencyBillSelectPaymentForward.createDestination((selectedMoneyReceived.MovedFromPMR as AgencyBillMoneyRcvd).Producer, selectedMoneyReceived.MovedFromPMR as AgencyBillMoneyRcvd)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=Edit) at AgencyBillExecutedPayments.pcf: line 55, column 29
    function checkedRowAction_5 (moneyReceived :  entity.AgencyBillMoneyRcvd, CheckedValue :  entity.AgencyBillMoneyRcvd) : void {
      AgencyDistributionWizard.go(producer, CheckedValue)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=Reverse) at AgencyBillExecutedPayments.pcf: line 63, column 29
    function checkedRowAction_6 (moneyReceived :  entity.AgencyBillMoneyRcvd, CheckedValue :  entity.AgencyBillMoneyRcvd) : void {
      AgencyDistributionReversalConfirmationPopup.push(CheckedValue)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=ViewSuspenseItems) at AgencyBillExecutedPayments.pcf: line 71, column 29
    function checkedRowAction_7 (moneyReceived :  entity.AgencyBillMoneyRcvd, CheckedValue :  entity.AgencyBillMoneyRcvd) : void {
      AgencySuspenseItemsPopup.push(CheckedValue.BaseDist as AgencyCycleDist, false)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=MoveToDifferentProducer) at AgencyBillExecutedPayments.pcf: line 79, column 29
    function checkedRowAction_8 (moneyReceived :  entity.AgencyBillMoneyRcvd, CheckedValue :  entity.AgencyBillMoneyRcvd) : void {
      MoveAgencyPaymentConfirmationPopup.push(CheckedValue)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=Split) at AgencyBillExecutedPayments.pcf: line 87, column 29
    function checkedRowAction_9 (moneyReceived :  entity.AgencyBillMoneyRcvd, CheckedValue :  entity.AgencyBillMoneyRcvd) : void {
      AgencyPaymentSplitConfirmationPopup.push(CheckedValue)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=ReceivedAmount_Input) at AgencyBillExecutedPayments.pcf: line 244, column 62
    function currency_91 () : typekey.Currency {
      return selectedMoneyReceived.Currency
    }
    
    // 'def' attribute on PanelRef at AgencyBillExecutedPayments.pcf: line 300, column 98
    function def_onEnter_117 (def :  pcf.AgencyDistItemsReadOnlyCV) : void {
      def.onEnter(selectedMoneyReceived.BaseDist as AgencyCycleDist)
    }
    
    // 'def' attribute on PanelRef at AgencyBillExecutedPayments.pcf: line 191, column 91
    function def_onEnter_53 (def :  pcf.AgencyBillDistArchiveWarningPanelSet) : void {
      def.onEnter(selectedMoneyReceived.BaseDist)
    }
    
    // 'def' attribute on PanelRef at AgencyBillExecutedPayments.pcf: line 300, column 98
    function def_refreshVariables_118 (def :  pcf.AgencyDistItemsReadOnlyCV) : void {
      def.refreshVariables(selectedMoneyReceived.BaseDist as AgencyCycleDist)
    }
    
    // 'def' attribute on PanelRef at AgencyBillExecutedPayments.pcf: line 191, column 91
    function def_refreshVariables_54 (def :  pcf.AgencyBillDistArchiveWarningPanelSet) : void {
      def.refreshVariables(selectedMoneyReceived.BaseDist)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillExecutedPayments.pcf: line 37, column 131
    function filter_1 () : gw.api.filters.IFilter {
      return new gw.payment.BaseMoneyReceivedDateFilter(30)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillExecutedPayments.pcf: line 40, column 131
    function filter_2 () : gw.api.filters.IFilter {
      return new gw.payment.BaseMoneyReceivedDateFilter(60)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillExecutedPayments.pcf: line 43, column 131
    function filter_3 () : gw.api.filters.IFilter {
      return new gw.payment.BaseMoneyReceivedDateFilter(90)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillExecutedPayments.pcf: line 46, column 124
    function filter_4 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("All", \ qf -> {})
    }
    
    // 'initialValue' attribute on Variable at AgencyBillExecutedPayments.pcf: line 28, column 79
    function initialValue_0 () : gw.api.database.IQueryBeanResult<AgencyBillMoneyRcvd> {
      return producer.findAgencyMoneyReceiveds()
    }
    
    // 'selectionOnEnter' attribute on ListDetailPanel at AgencyBillExecutedPayments.pcf: line 23, column 45
    function selectionOnEnter_120 () : java.lang.Object {
      return initialSelectedMoney
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AgencyBillExecutedPayments.pcf: line 123, column 55
    function sortValue_10 (moneyReceived :  entity.AgencyBillMoneyRcvd) : java.lang.Object {
      return moneyReceived.ReceivedDate
    }
    
    // 'value' attribute on TextCell (id=PaymentInstrument_Cell) at AgencyBillExecutedPayments.pcf: line 175, column 57
    function sortValue_11 (moneyReceived :  entity.AgencyBillMoneyRcvd) : java.lang.Object {
      return moneyReceived.PaymentInstrument
    }
    
    // 'title' attribute on Card (id=PaymentDetailCard) at AgencyBillExecutedPayments.pcf: line 189, column 141
    function title_119 () : java.lang.String {
      return DisplayKey.get("Web.AgencyBillPayments.Executed.PaymentDetail", selectedMoneyReceived.ReceivedDate.AsUIStyle)
    }
    
    // 'value' attribute on TextInput (id=PaymentInstrumentToken_Input) at AgencyBillExecutedPayments.pcf: line 296, column 74
    function valueRoot_115 () : java.lang.Object {
      return selectedMoneyReceived.PaymentInstrument
    }
    
    // 'value' attribute on TypeKeyInput (id=Reversed_Input) at AgencyBillExecutedPayments.pcf: line 202, column 61
    function valueRoot_57 () : java.lang.Object {
      return selectedMoneyReceived
    }
    
    // 'value' attribute on DateInput (id=DistributedDate_Input) at AgencyBillExecutedPayments.pcf: line 211, column 70
    function valueRoot_65 () : java.lang.Object {
      return selectedMoneyReceived.BaseDist
    }
    
    // 'value' attribute on TextInput (id=TransactionNumber_Input) at AgencyBillExecutedPayments.pcf: line 218, column 85
    function valueRoot_73 () : java.lang.Object {
      return selectedMoneyReceived.MoneyReceivedTransaction
    }
    
    // 'value' attribute on TextInput (id=ProducerWriteOff_Input) at AgencyBillExecutedPayments.pcf: line 265, column 49
    function value_101 () : java.lang.Object {
      return selectedMoneyReceived.DistributedDenorm ? selectedMoneyReceived.AgencyCyclePayment.ProducerWriteoffAmount : 0
    }
    
    // 'value' attribute on TextInput (id=ItemWriteOff_Input) at AgencyBillExecutedPayments.pcf: line 272, column 49
    function value_103 () : java.lang.Object {
      return selectedMoneyReceived.DistributedDenorm ? selectedMoneyReceived.AgencyCyclePayment.ItemWriteOffAmount : 0
    }
    
    // 'value' attribute on MonetaryAmountInput (id=RemainingAmount_Input) at AgencyBillExecutedPayments.pcf: line 279, column 149
    function value_105 () : gw.pl.currency.MonetaryAmount {
      return selectedMoneyReceived.TotalAmount - selectedMoneyReceived.NetAmountDistributed - selectedMoneyReceived.NetSuspenseAmount
    }
    
    // 'value' attribute on TextInput (id=PaymentInstrument_Input) at AgencyBillExecutedPayments.pcf: line 288, column 57
    function value_108 () : entity.PaymentInstrument {
      return selectedMoneyReceived.PaymentInstrument
    }
    
    // 'value' attribute on TextInput (id=RefNumber_Input) at AgencyBillExecutedPayments.pcf: line 292, column 60
    function value_111 () : java.lang.String {
      return selectedMoneyReceived.RefNumber
    }
    
    // 'value' attribute on TextInput (id=PaymentInstrumentToken_Input) at AgencyBillExecutedPayments.pcf: line 296, column 74
    function value_114 () : java.lang.String {
      return selectedMoneyReceived.PaymentInstrument.Token
    }
    
    // 'value' attribute on RowIterator (id=MoneyReceivedIterator) at AgencyBillExecutedPayments.pcf: line 99, column 94
    function value_52 () : gw.api.database.IQueryBeanResult<entity.AgencyBillMoneyRcvd> {
      return moneyReceiveds
    }
    
    // 'value' attribute on TypeKeyInput (id=Reversed_Input) at AgencyBillExecutedPayments.pcf: line 202, column 61
    function value_56 () : typekey.PaymentReversalReason {
      return selectedMoneyReceived.ReversalReason
    }
    
    // 'value' attribute on DateInput (id=ReceivedDate_Input) at AgencyBillExecutedPayments.pcf: line 206, column 63
    function value_60 () : java.util.Date {
      return selectedMoneyReceived.ReceivedDate
    }
    
    // 'value' attribute on DateInput (id=DistributedDate_Input) at AgencyBillExecutedPayments.pcf: line 211, column 70
    function value_64 () : java.util.Date {
      return selectedMoneyReceived.BaseDist.DistributedDate
    }
    
    // 'value' attribute on TextInput (id=TransactionNumber_Input) at AgencyBillExecutedPayments.pcf: line 218, column 85
    function value_72 () : java.lang.String {
      return selectedMoneyReceived.MoneyReceivedTransaction.TransactionNumber
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AgencyBillExecutedPayments.pcf: line 222, column 55
    function value_76 () : java.lang.String {
      return selectedMoneyReceived.Name
    }
    
    // 'value' attribute on TextInput (id=PaymentDescription_Input) at AgencyBillExecutedPayments.pcf: line 226, column 62
    function value_79 () : java.lang.String {
      return selectedMoneyReceived.Description
    }
    
    // 'value' attribute on TextInput (id=OriginalPayment_Input) at AgencyBillExecutedPayments.pcf: line 233, column 73
    function value_85 () : entity.PaymentMoneyReceived {
      return selectedMoneyReceived.MovedFromPMR
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ReceivedAmount_Input) at AgencyBillExecutedPayments.pcf: line 244, column 62
    function value_89 () : gw.pl.currency.MonetaryAmount {
      return selectedMoneyReceived.TotalAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Distributed_Input) at AgencyBillExecutedPayments.pcf: line 251, column 71
    function value_93 () : gw.pl.currency.MonetaryAmount {
      return selectedMoneyReceived.NetAmountDistributed
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Suspense_Input) at AgencyBillExecutedPayments.pcf: line 258, column 68
    function value_97 () : gw.pl.currency.MonetaryAmount {
      return selectedMoneyReceived.NetSuspenseAmount
    }
    
    // 'visible' attribute on TypeKeyInput (id=Reversed_Input) at AgencyBillExecutedPayments.pcf: line 202, column 61
    function visible_55 () : java.lang.Boolean {
      return selectedMoneyReceived.Reversed
    }
    
    // 'visible' attribute on DateInput (id=DistributedDate_Input) at AgencyBillExecutedPayments.pcf: line 211, column 70
    function visible_63 () : java.lang.Boolean {
      return selectedMoneyReceived.DistributedDenorm
    }
    
    // 'visible' attribute on TextInput (id=TransactionNumber_Input) at AgencyBillExecutedPayments.pcf: line 218, column 85
    function visible_68 () : java.lang.Boolean {
      return selectedMoneyReceived.MoneyReceivedTransaction != null
    }
    
    // 'visible' attribute on TextInput (id=OriginalPayment_Input) at AgencyBillExecutedPayments.pcf: line 233, column 73
    function visible_82 () : java.lang.Boolean {
      return selectedMoneyReceived.MovedFromPMR != null
    }
    
    property get moneyReceiveds () : gw.api.database.IQueryBeanResult<AgencyBillMoneyRcvd> {
      return getVariableValue("moneyReceiveds", 1) as gw.api.database.IQueryBeanResult<AgencyBillMoneyRcvd>
    }
    
    property set moneyReceiveds ($arg :  gw.api.database.IQueryBeanResult<AgencyBillMoneyRcvd>) {
      setVariableValue("moneyReceiveds", 1, $arg)
    }
    
    property get selectedMoneyReceived () : AgencyBillMoneyRcvd {
      return getCurrentSelection(1) as AgencyBillMoneyRcvd
    }
    
    property set selectedMoneyReceived ($arg :  AgencyBillMoneyRcvd) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  
}