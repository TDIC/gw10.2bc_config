package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DisbursementSearchCriteriaInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DisbursementSearchCriteriaInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/search/DisbursementSearchCriteriaInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DisbursementSearchCriteriaInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 80, column 41
    function available_42 () : java.lang.Boolean {
      return (searchCriteria.Currency != null or gw.api.util.CurrencyUtil.isSingleCurrencyMode())
    }
    
    // 'available' attribute on DateInput (id=EarliestIssueDateCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 40, column 49
    function available_8 () : java.lang.Boolean {
      return searchCriteria.Status == TC_SENT
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 80, column 41
    function currency_46 () : typekey.Currency {
      return gw.search.SearchHelper.getDefaultCurrencyIfNull(searchCriteria.Currency)
    }
    
    // 'value' attribute on TypeKeyInput (id=disbursementSubtype_Input) at DisbursementSearchCriteriaInputSet.pcf: line 23, column 40
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      disbursementSubtypeHolder[0] = (__VALUE_TO_SET as typekey.Disbursement)
    }
    
    // 'value' attribute on DateInput (id=EarliestIssueDateCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 40, column 49
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.EarliestIssueDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=LatestIssueDateCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 46, column 47
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.LatestIssueDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeInput (id=MethodCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 53, column 42
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Method = (__VALUE_TO_SET as typekey.PaymentMethod)
    }
    
    // 'value' attribute on TextInput (id=TokenCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 58, column 37
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Token = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=CheckNumberCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 63, column 43
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.CheckNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 70, column 64
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Currency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 80, column 41
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MinAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TypeKeyInput (id=StatusCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 31, column 46
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Status = (__VALUE_TO_SET as typekey.DisbursementStatus)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 87, column 41
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MaxAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextInput (id=PayeeCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 92, column 37
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Payee = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=ReasonCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 98, column 35
    function defaultSetter_61 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Reason = (__VALUE_TO_SET as typekey.Reason)
    }
    
    // 'filter' attribute on TypeKeyInput (id=disbursementSubtype_Input) at DisbursementSearchCriteriaInputSet.pcf: line 23, column 40
    function filter_2 (VALUE :  typekey.Disbursement, VALUES :  typekey.Disbursement[]) : java.lang.Boolean {
      return gw.api.web.disbursement.DisbursementUtil.isDisbursementSubtypeAbstract(VALUE);
    }
    
    // 'onChange' attribute on PostOnChange at DisbursementSearchCriteriaInputSet.pcf: line 72, column 52
    function onChange_35 () : void {
      blankMinimumAndMaximumFields()
    }
    
    // 'valueRange' attribute on RangeInput (id=MethodCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 53, column 42
    function valueRange_23 () : java.lang.Object {
      return PaymentMethodRange
    }
    
    // 'value' attribute on TypeKeyInput (id=StatusCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 31, column 46
    function valueRoot_6 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TypeKeyInput (id=disbursementSubtype_Input) at DisbursementSearchCriteriaInputSet.pcf: line 23, column 40
    function value_0 () : typekey.Disbursement {
      return disbursementSubtypeHolder[0]
    }
    
    // 'value' attribute on DateInput (id=LatestIssueDateCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 46, column 47
    function value_15 () : java.util.Date {
      return searchCriteria.LatestIssueDate
    }
    
    // 'value' attribute on RangeInput (id=MethodCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 53, column 42
    function value_20 () : typekey.PaymentMethod {
      return searchCriteria.Method
    }
    
    // 'value' attribute on TextInput (id=TokenCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 58, column 37
    function value_27 () : java.lang.String {
      return searchCriteria.Token
    }
    
    // 'value' attribute on TextInput (id=CheckNumberCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 63, column 43
    function value_31 () : java.lang.String {
      return searchCriteria.CheckNumber
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 70, column 64
    function value_37 () : typekey.Currency {
      return searchCriteria.Currency
    }
    
    // 'value' attribute on TypeKeyInput (id=StatusCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 31, column 46
    function value_4 () : typekey.DisbursementStatus {
      return searchCriteria.Status
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 80, column 41
    function value_43 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MinAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 87, column 41
    function value_50 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MaxAmount
    }
    
    // 'value' attribute on TextInput (id=PayeeCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 92, column 37
    function value_56 () : java.lang.String {
      return searchCriteria.Payee
    }
    
    // 'value' attribute on TypeKeyInput (id=ReasonCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 98, column 35
    function value_60 () : typekey.Reason {
      return searchCriteria.Reason
    }
    
    // 'value' attribute on DateInput (id=EarliestIssueDateCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 40, column 49
    function value_9 () : java.util.Date {
      return searchCriteria.EarliestIssueDate
    }
    
    // 'valueRange' attribute on RangeInput (id=MethodCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 53, column 42
    function verifyValueRangeIsAllowedType_24 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=MethodCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 53, column 42
    function verifyValueRangeIsAllowedType_24 ($$arg :  typekey.PaymentMethod[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=MethodCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 53, column 42
    function verifyValueRange_25 () : void {
      var __valueRangeArg = PaymentMethodRange
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_24(__valueRangeArg)
    }
    
    // 'visible' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at DisbursementSearchCriteriaInputSet.pcf: line 70, column 64
    function visible_36 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    property get disbursementSubtypeHolder () : typekey.Disbursement[] {
      return getRequireValue("disbursementSubtypeHolder", 0) as typekey.Disbursement[]
    }
    
    property set disbursementSubtypeHolder ($arg :  typekey.Disbursement[]) {
      setRequireValue("disbursementSubtypeHolder", 0, $arg)
    }
    
    property get searchCriteria () : gw.search.DisbSearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.search.DisbSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.DisbSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    property get PaymentMethodRange() : java.util.List<PaymentMethod> {
      var paymentMethodRange = new java.util.ArrayList<PaymentMethod>();
     //  HermiaK, 12/18/2014, US126 Exclude ACH/EFT, Credit Card, Wire from "Payment Method" 
     //     paymentMethodRange.add( PaymentMethod.TC_ACH );          
            paymentMethodRange.add( PaymentMethod.TC_CHECK );
     //     paymentMethodRange.add( PaymentMethod.TC_CREDITCARD );   
     //     paymentMethodRange.add( PaymentMethod.TC_WIRE );         
            return paymentMethodRange;
    }
    
    function blankMinimumAndMaximumFields() {
      searchCriteria.MinAmount = null
      searchCriteria.MaxAmount = null
    }
    
    
  }
  
  
}