package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_DistributionScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyDistributionWizard_DistributionScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_DistributionScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyDistributionWizard_DistributionScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=SavePayment) at AgencyDistributionWizard_DistributionScreen.pcf: line 18, column 23
    function action_1 () : void {
      validateAmountToApply(); wizardState.save(CurrentLocation as pcf.api.Wizard);
    }
    
    // 'action' attribute on ToolbarButton (id=AddPolicies) at AgencyDistributionWizard_DistributionScreen.pcf: line 48, column 125
    function action_11 () : void {
      AgencyDistributionWizard_AddPoliciesPopup.push(wizardState)
    }
    
    // 'action' attribute on ToolbarButton (id=SuspenseItems) at AgencyDistributionWizard_DistributionScreen.pcf: line 24, column 23
    function action_2 () : void {
      AgencySuspenseItemsPopup.push( wizardState.AgencyCycleDistView.AgencyCycleDist, true )
    }
    
    // 'action' attribute on ToolbarButton (id=AddItems) at AgencyDistributionWizard_DistributionScreen.pcf: line 42, column 122
    function action_8 () : void {
      AgencyBillAddInvoiceItemsPopup.push(wizardState)
    }
    
    // 'action' attribute on ToolbarButton (id=AddPolicies) at AgencyDistributionWizard_DistributionScreen.pcf: line 48, column 125
    function action_dest_12 () : pcf.api.Destination {
      return pcf.AgencyDistributionWizard_AddPoliciesPopup.createDestination(wizardState)
    }
    
    // 'action' attribute on ToolbarButton (id=SuspenseItems) at AgencyDistributionWizard_DistributionScreen.pcf: line 24, column 23
    function action_dest_3 () : pcf.api.Destination {
      return pcf.AgencySuspenseItemsPopup.createDestination( wizardState.AgencyCycleDistView.AgencyCycleDist, true )
    }
    
    // 'action' attribute on ToolbarButton (id=AddItems) at AgencyDistributionWizard_DistributionScreen.pcf: line 42, column 122
    function action_dest_9 () : pcf.api.Destination {
      return pcf.AgencyBillAddInvoiceItemsPopup.createDestination(wizardState)
    }
    
    // 'available' attribute on ToolbarButton (id=SavePayment) at AgencyDistributionWizard_DistributionScreen.pcf: line 18, column 23
    function available_0 () : java.lang.Boolean {
      return !wizardState.AgencyCycleDistView.AgencyCycleDist.Modifying
    }
    
    // 'def' attribute on PanelRef (id=AgencyDistItemsPanel) at AgencyDistributionWizard_DistributionScreen.pcf: line 35, column 149
    function def_onEnter_20 (def :  pcf.AgencyDistItemsLV_item) : void {
      def.onEnter(wizardState.AgencyCycleDistView, wizardState)
    }
    
    // 'def' attribute on PanelRef (id=AgencyDistItemsPanel) at AgencyDistributionWizard_DistributionScreen.pcf: line 35, column 149
    function def_onEnter_22 (def :  pcf.AgencyDistItemsLV_policy) : void {
      def.onEnter(wizardState.AgencyCycleDistView, wizardState)
    }
    
    // 'def' attribute on PanelRef at AgencyDistributionWizard_DistributionScreen.pcf: line 31, column 77
    function def_onEnter_5 (def :  pcf.AgencyDistributionWizard_DistributionInfoPanelSet) : void {
      def.onEnter(wizardState)
    }
    
    // 'def' attribute on PanelRef (id=AgencyDistItemsPanel) at AgencyDistributionWizard_DistributionScreen.pcf: line 35, column 149
    function def_refreshVariables_21 (def :  pcf.AgencyDistItemsLV_item) : void {
      def.refreshVariables(wizardState.AgencyCycleDistView, wizardState)
    }
    
    // 'def' attribute on PanelRef (id=AgencyDistItemsPanel) at AgencyDistributionWizard_DistributionScreen.pcf: line 35, column 149
    function def_refreshVariables_23 (def :  pcf.AgencyDistItemsLV_policy) : void {
      def.refreshVariables(wizardState.AgencyCycleDistView, wizardState)
    }
    
    // 'def' attribute on PanelRef at AgencyDistributionWizard_DistributionScreen.pcf: line 31, column 77
    function def_refreshVariables_6 (def :  pcf.AgencyDistributionWizard_DistributionInfoPanelSet) : void {
      def.refreshVariables(wizardState)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=DistributeBy_Input) at AgencyDistributionWizard_DistributionScreen.pcf: line 55, column 85
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      wizardState.DistributeBy = (__VALUE_TO_SET as gw.agencybill.AgencyDistributionWizardHelper.DistributeByEnum)
    }
    
    // 'mode' attribute on PanelRef (id=AgencyDistItemsPanel) at AgencyDistributionWizard_DistributionScreen.pcf: line 35, column 149
    function mode_24 () : java.lang.Object {
      return wizardState.DistributeBy == gw.agencybill.AgencyDistributionWizardHelper.DistributeByEnum.ITEM ? "item" : "policy"
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=DistributeBy_Input) at AgencyDistributionWizard_DistributionScreen.pcf: line 55, column 85
    function valueRange_16 () : java.lang.Object {
      return wizardState.DistributeByValues
    }
    
    // 'value' attribute on ToolbarRangeInput (id=DistributeBy_Input) at AgencyDistributionWizard_DistributionScreen.pcf: line 55, column 85
    function valueRoot_15 () : java.lang.Object {
      return wizardState
    }
    
    // 'value' attribute on ToolbarRangeInput (id=DistributeBy_Input) at AgencyDistributionWizard_DistributionScreen.pcf: line 55, column 85
    function value_13 () : gw.agencybill.AgencyDistributionWizardHelper.DistributeByEnum {
      return wizardState.DistributeBy
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=DistributeBy_Input) at AgencyDistributionWizard_DistributionScreen.pcf: line 55, column 85
    function verifyValueRangeIsAllowedType_17 ($$arg :  gw.agencybill.AgencyDistributionWizardHelper.DistributeByEnum[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=DistributeBy_Input) at AgencyDistributionWizard_DistributionScreen.pcf: line 55, column 85
    function verifyValueRangeIsAllowedType_17 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=DistributeBy_Input) at AgencyDistributionWizard_DistributionScreen.pcf: line 55, column 85
    function verifyValueRange_18 () : void {
      var __valueRangeArg = wizardState.DistributeByValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_17(__valueRangeArg)
    }
    
    // 'visible' attribute on ToolbarButton (id=AddPolicies) at AgencyDistributionWizard_DistributionScreen.pcf: line 48, column 125
    function visible_10 () : java.lang.Boolean {
      return gw.agencybill.AgencyDistributionWizardHelper.DistributeByEnum.POLICY.equals(wizardState.DistributeBy) 
    }
    
    // 'visible' attribute on AlertBar (id=FrozenDistItemAlert) at AgencyDistributionWizard_DistributionScreen.pcf: line 29, column 75
    function visible_4 () : java.lang.Boolean {
      return wizardState.IsDitributingDistWithFrozenDistItemsByPolicy
    }
    
    // 'visible' attribute on ToolbarButton (id=AddItems) at AgencyDistributionWizard_DistributionScreen.pcf: line 42, column 122
    function visible_7 () : java.lang.Boolean {
      return gw.agencybill.AgencyDistributionWizardHelper.DistributeByEnum.ITEM.equals(wizardState.DistributeBy)
    }
    
    property get wizardState () : gw.agencybill.AgencyDistributionWizardHelper {
      return getRequireValue("wizardState", 0) as gw.agencybill.AgencyDistributionWizardHelper
    }
    
    property set wizardState ($arg :  gw.agencybill.AgencyDistributionWizardHelper) {
      setRequireValue("wizardState", 0, $arg)
    }
    
    function validateAmountToApply() {
      var agencyCyleDistChargeOwnerViews = wizardState.AgencyCycleDistView.ChargeOwners
      for (var agencyCycleDistChargeOwnerView in agencyCyleDistChargeOwnerViews) {
        var agencyDistItems = agencyCycleDistChargeOwnerView.AgencyDistItems
        for (var agencyDistItem in agencyDistItems) {
          if (wizardState.isNetAmountToApplyInvalid(agencyDistItem)) {
            throw new gw.api.util.DisplayableException(DisplayKey.get("Java.Error.AgencyCycleDistChargeOwnerView.InvalidGrossAndCommissionDistributionAmount"));
          } 
        }
      }
    }
    
    
  }
  
  
}