package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailPaymentsExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailPaymentsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'location' attribute on LocationGroup (id=AccountDetailPayments) at AccountDetailPayments.pcf: line 14, column 44
    function action_0 () : void {
      pcf.AccountPayments.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountDetailPayments) at AccountDetailPayments.pcf: line 16, column 55
    function action_2 () : void {
      pcf.AccountCreditDistributions.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountDetailPayments) at AccountDetailPayments.pcf: line 18, column 51
    function action_4 () : void {
      pcf.AccountPaymentRequests.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountDetailPayments) at AccountDetailPayments.pcf: line 14, column 44
    function action_dest_1 () : pcf.api.Destination {
      return pcf.AccountPayments.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountDetailPayments) at AccountDetailPayments.pcf: line 16, column 55
    function action_dest_3 () : pcf.api.Destination {
      return pcf.AccountCreditDistributions.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountDetailPayments) at AccountDetailPayments.pcf: line 18, column 51
    function action_dest_5 () : pcf.api.Destination {
      return pcf.AccountPaymentRequests.createDestination(account)
    }
    
    // LocationGroup (id=AccountDetailPayments) at AccountDetailPayments.pcf: line 7, column 73
    static function firstVisitableChildDestinationMethod_9 (account :  Account) : pcf.api.Destination {
      var dest : pcf.api.Destination
      dest = pcf.AccountPayments.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountCreditDistributions.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountPaymentRequests.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      return null
    }
    
    // LocationGroup (id=AccountDetailPayments) at AccountDetailPayments.pcf: line 7, column 73
    static function parent_6 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    // 'tabBar' attribute on LocationGroup (id=AccountDetailPayments) at AccountDetailPayments.pcf: line 7, column 73
    function tabBar_onEnter_7 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on LocationGroup (id=AccountDetailPayments) at AccountDetailPayments.pcf: line 7, column 73
    function tabBar_refreshVariables_8 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AccountDetailPayments {
      return super.CurrentLocation as pcf.AccountDetailPayments
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  
}