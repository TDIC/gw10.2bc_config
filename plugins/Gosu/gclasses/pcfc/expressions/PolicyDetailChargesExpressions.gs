package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailCharges.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailChargesExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailCharges.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CardViewPanelExpressionsImpl extends PolicyDetailChargesListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=MoveInvoiceItems) at PolicyDetailCharges.pcf: line 77, column 63
    function allCheckedRowsAction_8 (CheckedValues :  entity.InvoiceItem[], CheckedValuesErrors :  java.util.Map) : void {
      gw.api.web.invoice.InvoiceUtil.validateItemsForMove(CheckedValues); MoveInvoiceItemsPopup.push(CheckedValues, CheckedValues[0].Payer)
    }
    
    // 'def' attribute on PanelRef at PolicyDetailCharges.pcf: line 86, column 60
    function def_onEnter_11 (def :  pcf.ChargeBreakdownItemsDisplayLV) : void {
      def.onEnter(charge)
    }
    
    // 'def' attribute on PanelRef at PolicyDetailCharges.pcf: line 63, column 74
    function def_onEnter_9 (def :  pcf.InvoiceItemsLV) : void {
      def.onEnter(charge.AllInvoiceItems, charge, true)
    }
    
    // 'def' attribute on PanelRef at PolicyDetailCharges.pcf: line 63, column 74
    function def_refreshVariables_10 (def :  pcf.InvoiceItemsLV) : void {
      def.refreshVariables(charge.AllInvoiceItems, charge, true)
    }
    
    // 'def' attribute on PanelRef at PolicyDetailCharges.pcf: line 86, column 60
    function def_refreshVariables_12 (def :  pcf.ChargeBreakdownItemsDisplayLV) : void {
      def.refreshVariables(charge)
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailCharges.pcf: line 58, column 31
    function initialValue_6 () : Invoice[] {
      return getFutureInvoices()
    }
    
    // 'visible' attribute on Card (id=ChargeBreakdownCard) at PolicyDetailCharges.pcf: line 84, column 57
    function visible_13 () : java.lang.Boolean {
      return charge.BreakdownItems.HasElements
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=MoveInvoiceItems) at PolicyDetailCharges.pcf: line 77, column 63
    function visible_7 () : java.lang.Boolean {
      return perm.System.plcychargesedit_TDIC
    }
    
    property get futureInvoices () : Invoice[] {
      return getVariableValue("futureInvoices", 2) as Invoice[]
    }
    
    property set futureInvoices ($arg :  Invoice[]) {
      setVariableValue("futureInvoices", 2, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailCharges.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailChargesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Page (id=PolicyDetailCharges) at PolicyDetailCharges.pcf: line 10, column 71
    function beforeCommit_14 (pickedValue :  java.lang.Object) : void {
      validateAndExecute()
    }
    
    // 'canEdit' attribute on Page (id=PolicyDetailCharges) at PolicyDetailCharges.pcf: line 10, column 71
    function canEdit_15 () : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcychargesview
    }
    
    // 'canVisit' attribute on Page (id=PolicyDetailCharges) at PolicyDetailCharges.pcf: line 10, column 71
    static function canVisit_16 (plcyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcychargesview and not plcyPeriod.Archived
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailCharges.pcf: line 19, column 51
    function initialValue_0 () : java.util.List<entity.Charge> {
      return plcyPeriod.Archived ? new java.util.ArrayList<entity.Charge>(0) : plcyPeriod.Charges
    }
    
    // Page (id=PolicyDetailCharges) at PolicyDetailCharges.pcf: line 10, column 71
    static function parent_17 (plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyGroup.createDestination(plcyPeriod)
    }
    
    override property get CurrentLocation () : pcf.PolicyDetailCharges {
      return super.CurrentLocation as pcf.PolicyDetailCharges
    }
    
    property get charges () : java.util.List<entity.Charge> {
      return getVariableValue("charges", 0) as java.util.List<entity.Charge>
    }
    
    property set charges ($arg :  java.util.List<entity.Charge>) {
      setVariableValue("charges", 0, $arg)
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 0, $arg)
    }
    
    property get targetInvoice () : Invoice {
      return getVariableValue("targetInvoice", 0) as Invoice
    }
    
    property set targetInvoice ($arg :  Invoice) {
      setVariableValue("targetInvoice", 0, $arg)
    }
    
    function validateAndExecute() {
                        gw.api.web.policy.PolicyPeriodUtil.validateInvoiceItems(plcyPeriod);
                    }
    
                    function getFutureInvoices() : Invoice[] {
                      if (plcyPeriod.Archived) {
                        return new Invoice[0]  
                      } else {
                        return plcyPeriod.AgencyBill ?
                                plcyPeriod.PrimaryPolicyCommission.ProducerCode.Producer.FutureStatementsSortedByDate
                          : plcyPeriod.Account.FutureInvoicesSortedByDate
                        }
                    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailCharges.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailChargesListDetailPanelExpressionsImpl extends PolicyDetailChargesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=EditHolds) at PolicyDetailCharges.pcf: line 43, column 99
    function allCheckedRowsAction_3 (CheckedValues :  entity.Charge[], CheckedValuesErrors :  java.util.Map) : void {
      ChargeHoldsPopup.push(CheckedValues)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=ReverseButton) at PolicyDetailCharges.pcf: line 37, column 80
    function checkedRowAction_1 (element :  entity.Charge, CheckedValue :  entity.Charge) : void {
      NewChargeReversalConfirmationPopup.push(CheckedValue)
    }
    
    // 'def' attribute on PanelRef at PolicyDetailCharges.pcf: line 30, column 128
    function def_onEnter_4 (def :  pcf.ChargesLV) : void {
      def.onEnter(plcyPeriod.Archived ? new Charge[0] : plcyPeriod.Charges.toTypedArray(), true, 0, false, true, true)
    }
    
    // 'def' attribute on PanelRef at PolicyDetailCharges.pcf: line 30, column 128
    function def_refreshVariables_5 (def :  pcf.ChargesLV) : void {
      def.refreshVariables(plcyPeriod.Archived ? new Charge[0] : plcyPeriod.Charges.toTypedArray(), true, 0, false, true, true)
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=EditHolds) at PolicyDetailCharges.pcf: line 43, column 99
    function visible_2 () : java.lang.Boolean {
      return perm.Transaction.chargeholdcreate and perm.Transaction.chargeholdrelease
    }
    
    property get charge () : Charge {
      return getCurrentSelection(1) as Charge
    }
    
    property set charge ($arg :  Charge) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  
}