package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/LateDistributionExceptionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LateDistributionExceptionsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/LateDistributionExceptionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends LateDistributionExceptionsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Statement_Cell) at LateDistributionExceptionsLV.pcf: line 40, column 82
    function action_12 () : void {
      AgencyBillStatementDetail.go(cycleException.AgencyBillCycle)
    }
    
    // 'action' attribute on TextCell (id=Agency_Cell) at LateDistributionExceptionsLV.pcf: line 54, column 40
    function action_21 () : void {
      ProducerDetailPopup.push(cycleException.AgencyBillCycle.Producer)
    }
    
    // 'action' attribute on TextCell (id=Statement_Cell) at LateDistributionExceptionsLV.pcf: line 40, column 82
    function action_dest_13 () : pcf.api.Destination {
      return pcf.AgencyBillStatementDetail.createDestination(cycleException.AgencyBillCycle)
    }
    
    // 'action' attribute on TextCell (id=Agency_Cell) at LateDistributionExceptionsLV.pcf: line 54, column 40
    function action_dest_22 () : pcf.api.Destination {
      return pcf.ProducerDetailPopup.createDestination(cycleException.AgencyBillCycle.Producer)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at LateDistributionExceptionsLV.pcf: line 47, column 78
    function currency_19 () : typekey.Currency {
      return cycleException.Currency
    }
    
    // 'value' attribute on TextCell (id=Agency_Cell) at LateDistributionExceptionsLV.pcf: line 54, column 40
    function valueRoot_24 () : java.lang.Object {
      return cycleException.AgencyBillCycle
    }
    
    // 'value' attribute on DateCell (id=cycleCloseDate_Cell) at LateDistributionExceptionsLV.pcf: line 29, column 78
    function valueRoot_7 () : java.lang.Object {
      return cycleException.AgencyBillCycle.StatementInvoice
    }
    
    // 'value' attribute on TextCell (id=Statement_Cell) at LateDistributionExceptionsLV.pcf: line 40, column 82
    function value_14 () : java.lang.String {
      return cycleException.AgencyBillCycle.StatementInvoice.InvoiceNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at LateDistributionExceptionsLV.pcf: line 47, column 78
    function value_17 () : gw.pl.currency.MonetaryAmount {
      return cycleException.AgencyBillCycle.StatementInvoice.NetAmount
    }
    
    // 'value' attribute on TextCell (id=Agency_Cell) at LateDistributionExceptionsLV.pcf: line 54, column 40
    function value_23 () : entity.Producer {
      return cycleException.AgencyBillCycle.Producer
    }
    
    // 'value' attribute on DateCell (id=cycleCloseDate_Cell) at LateDistributionExceptionsLV.pcf: line 29, column 78
    function value_6 () : java.util.Date {
      return cycleException.AgencyBillCycle.StatementInvoice.EventDate
    }
    
    // 'value' attribute on DateCell (id=paymentDueDate_Cell) at LateDistributionExceptionsLV.pcf: line 34, column 76
    function value_9 () : java.util.Date {
      return cycleException.AgencyBillCycle.StatementInvoice.DueDate
    }
    
    property get cycleException () : entity.AgencyCycleProcess {
      return getIteratedValue(1) as entity.AgencyCycleProcess
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/LateDistributionExceptionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LateDistributionExceptionsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=cycleCloseDate_Cell) at LateDistributionExceptionsLV.pcf: line 29, column 78
    function sortValue_0 (cycleException :  entity.AgencyCycleProcess) : java.lang.Object {
      return cycleException.AgencyBillCycle.StatementInvoice.EventDate
    }
    
    // 'value' attribute on TextCell (id=Statement_Cell) at LateDistributionExceptionsLV.pcf: line 40, column 82
    function sortValue_1 (cycleException :  entity.AgencyCycleProcess) : java.lang.Object {
      return cycleException.AgencyBillCycle.StatementInvoice.InvoiceNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at LateDistributionExceptionsLV.pcf: line 47, column 78
    function sortValue_2 (cycleException :  entity.AgencyCycleProcess) : java.lang.Object {
      return cycleException.AgencyBillCycle.StatementInvoice.NetAmount
    }
    
    // 'value' attribute on TextCell (id=Agency_Cell) at LateDistributionExceptionsLV.pcf: line 54, column 40
    function sortValue_3 (cycleException :  entity.AgencyCycleProcess) : java.lang.Object {
      return cycleException.AgencyBillCycle.Producer
    }
    
    // '$$sumValue' attribute on RowIterator at LateDistributionExceptionsLV.pcf: line 47, column 78
    function sumValueRoot_5 (cycleException :  entity.AgencyCycleProcess) : java.lang.Object {
      return getStatememtInvoice(cycleException)
    }
    
    // 'footerSumValue' attribute on RowIterator at LateDistributionExceptionsLV.pcf: line 47, column 78
    function sumValue_4 (cycleException :  entity.AgencyCycleProcess) : java.lang.Object {
      return getStatememtInvoice(cycleException).NetAmount
    }
    
    // 'value' attribute on RowIterator at LateDistributionExceptionsLV.pcf: line 22, column 85
    function value_26 () : gw.api.database.IQueryBeanResult<entity.AgencyCycleProcess> {
      return agencyCyclesWithException
    }
    
    property get agencyCyclesWithException () : gw.api.database.IQueryBeanResult<AgencyCycleProcess> {
      return getRequireValue("agencyCyclesWithException", 0) as gw.api.database.IQueryBeanResult<AgencyCycleProcess>
    }
    
    property set agencyCyclesWithException ($arg :  gw.api.database.IQueryBeanResult<AgencyCycleProcess>) {
      setRequireValue("agencyCyclesWithException", 0, $arg)
    }
    
    function getStatememtInvoice(agencyCycleProcess : AgencyCycleProcess) : entity.StatementInvoice {
      return agencyCycleProcess.StatementInvoice
    }
    
    
  }
  
  
}