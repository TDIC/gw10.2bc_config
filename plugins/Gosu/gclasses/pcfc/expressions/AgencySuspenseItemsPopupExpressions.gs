package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AgencySuspenseItemsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencySuspenseItemsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AgencySuspenseItemsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencySuspenseItemsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (distribution :  AgencyCycleDist, editing :  boolean) : int {
      return 0
    }
    
    // 'autoAdd' attribute on RowIterator (id=SuspenseItems) at AgencySuspenseItemsPopup.pcf: line 37, column 49
    function autoAdd_3 () : java.lang.Boolean {
      return distribution.SuspDistItemsThatHaveNotBeenReleased.Count == 0
    }
    
    // 'canEdit' attribute on Popup (id=AgencySuspenseItemsPopup) at AgencySuspenseItemsPopup.pcf: line 9, column 76
    function canEdit_37 () : java.lang.Boolean {
      return editing
    }
    
    // EditButtons at AgencySuspenseItemsPopup.pcf: line 21, column 31
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencySuspenseItemsPopup.pcf: line 47, column 29
    function sortValue_1 (item :  entity.BaseSuspDistItem) : java.lang.Object {
      return item.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AgencySuspenseItemsPopup.pcf: line 83, column 45
    function sortValue_2 (item :  entity.BaseSuspDistItem) : java.lang.Object {
      return item.PaymentComments
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=SuspenseItems) at AgencySuspenseItemsPopup.pcf: line 37, column 49
    function toCreateAndAdd_34 () : entity.BaseSuspDistItem {
      return distribution.createAndAddSuspDistItem()
    }
    
    // 'toRemove' attribute on RowIterator (id=SuspenseItems) at AgencySuspenseItemsPopup.pcf: line 37, column 49
    function toRemove_35 (item :  entity.BaseSuspDistItem) : void {
      item.release()
    }
    
    // 'value' attribute on RowIterator (id=SuspenseItems) at AgencySuspenseItemsPopup.pcf: line 37, column 49
    function value_36 () : entity.BaseSuspDistItem[] {
      return distribution.SuspDistItemsThatHaveNotBeenReleased
    }
    
    override property get CurrentLocation () : pcf.AgencySuspenseItemsPopup {
      return super.CurrentLocation as pcf.AgencySuspenseItemsPopup
    }
    
    property get distribution () : AgencyCycleDist {
      return getVariableValue("distribution", 0) as AgencyCycleDist
    }
    
    property set distribution ($arg :  AgencyCycleDist) {
      setVariableValue("distribution", 0, $arg)
    }
    
    property get editing () : boolean {
      return getVariableValue("editing", 0) as java.lang.Boolean
    }
    
    property set editing ($arg :  boolean) {
      setVariableValue("editing", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AgencySuspenseItemsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AgencySuspenseItemsPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AgencySuspenseItemsPopup.pcf: line 57, column 47
    function currency_15 () : typekey.Currency {
      return distribution.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AgencySuspenseItemsPopup.pcf: line 57, column 47
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      item.GrossAmountToApply = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionAmountToApply_Cell) at AgencySuspenseItemsPopup.pcf: line 68, column 52
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      item.CommissionAmountToApply = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AgencySuspenseItemsPopup.pcf: line 83, column 45
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      item.PaymentComments = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencySuspenseItemsPopup.pcf: line 47, column 29
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      item.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextCell (id=PolicyNumber_Cell) at AgencySuspenseItemsPopup.pcf: line 47, column 29
    function editable_4 () : java.lang.Boolean {
      return !item.Executed
    }
    
    // 'validationExpression' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AgencySuspenseItemsPopup.pcf: line 57, column 47
    function validationExpression_11 () : java.lang.Object {
      return item.GrossAmountToApply.IsZero and item.CommissionAmountToApply.IsZero ? DisplayKey.get("Web.AgencySuspenseItemsPopup.ZeroDollarGrossItem") : null
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencySuspenseItemsPopup.pcf: line 47, column 29
    function valueRoot_7 () : java.lang.Object {
      return item
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AgencySuspenseItemsPopup.pcf: line 57, column 47
    function value_12 () : gw.pl.currency.MonetaryAmount {
      return item.GrossAmountToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionAmountToApply_Cell) at AgencySuspenseItemsPopup.pcf: line 68, column 52
    function value_20 () : gw.pl.currency.MonetaryAmount {
      return item.CommissionAmountToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetAmountToApply_Cell) at AgencySuspenseItemsPopup.pcf: line 78, column 46
    function value_26 () : gw.pl.currency.MonetaryAmount {
      return item.NetAmountToApply
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AgencySuspenseItemsPopup.pcf: line 83, column 45
    function value_30 () : java.lang.String {
      return item.PaymentComments
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencySuspenseItemsPopup.pcf: line 47, column 29
    function value_5 () : java.lang.String {
      return item.PolicyNumber
    }
    
    property get item () : entity.BaseSuspDistItem {
      return getIteratedValue(1) as entity.BaseSuspDistItem
    }
    
    
  }
  
  
}