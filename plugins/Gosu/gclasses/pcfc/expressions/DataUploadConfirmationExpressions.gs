package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadConfirmation.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DataUploadConfirmationExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadConfirmation.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DataUploadConfirmationExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (processor :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) : int {
      return 0
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 213, column 48
    function def_onEnter_100 (def :  pcf.DataUploadBatchLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 221, column 58
    function def_onEnter_103 (def :  pcf.DataUploadPaymentReversalLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 109, column 50
    function def_onEnter_61 (def :  pcf.DataUploadAccountLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 117, column 51
    function def_onEnter_64 (def :  pcf.DataUploadProducerLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 125, column 49
    function def_onEnter_67 (def :  pcf.DataUploadPolicyLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 133, column 51
    function def_onEnter_70 (def :  pcf.DataUploadIssuanceLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 141, column 53
    function def_onEnter_73 (def :  pcf.DataUploadNewRenewalLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 149, column 49
    function def_onEnter_76 (def :  pcf.DataUploadChangeLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 157, column 48
    function def_onEnter_79 (def :  pcf.DataUploadRenewLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 165, column 49
    function def_onEnter_82 (def :  pcf.DataUploadCancelLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 173, column 52
    function def_onEnter_85 (def :  pcf.DataUploadDirectPayLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 181, column 50
    function def_onEnter_88 (def :  pcf.DataUploadRewriteLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 189, column 52
    function def_onEnter_91 (def :  pcf.DataUploadReinstateLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 197, column 48
    function def_onEnter_94 (def :  pcf.DataUploadAuditLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 205, column 52
    function def_onEnter_97 (def :  pcf.DataUploadAgencyPayLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 213, column 48
    function def_refreshVariables_101 (def :  pcf.DataUploadBatchLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 221, column 58
    function def_refreshVariables_104 (def :  pcf.DataUploadPaymentReversalLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 109, column 50
    function def_refreshVariables_62 (def :  pcf.DataUploadAccountLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 117, column 51
    function def_refreshVariables_65 (def :  pcf.DataUploadProducerLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 125, column 49
    function def_refreshVariables_68 (def :  pcf.DataUploadPolicyLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 133, column 51
    function def_refreshVariables_71 (def :  pcf.DataUploadIssuanceLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 141, column 53
    function def_refreshVariables_74 (def :  pcf.DataUploadNewRenewalLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 149, column 49
    function def_refreshVariables_77 (def :  pcf.DataUploadChangeLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 157, column 48
    function def_refreshVariables_80 (def :  pcf.DataUploadRenewLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 165, column 49
    function def_refreshVariables_83 (def :  pcf.DataUploadCancelLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 173, column 52
    function def_refreshVariables_86 (def :  pcf.DataUploadDirectPayLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 181, column 50
    function def_refreshVariables_89 (def :  pcf.DataUploadRewriteLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 189, column 52
    function def_refreshVariables_92 (def :  pcf.DataUploadReinstateLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 197, column 48
    function def_refreshVariables_95 (def :  pcf.DataUploadAuditLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadConfirmation.pcf: line 205, column 52
    function def_refreshVariables_98 (def :  pcf.DataUploadAgencyPayLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'label' attribute on Verbatim at DataUploadConfirmation.pcf: line 18, column 113
    function label_0 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationResultsLabel")
    }
    
    // 'label' attribute on TextInput (id=noAcct_Input) at DataUploadConfirmation.pcf: line 25, column 44
    function label_1 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Accounts"))
    }
    
    // 'label' attribute on TextInput (id=noIssuance_Input) at DataUploadConfirmation.pcf: line 40, column 44
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Issuances"))
    }
    
    // 'label' attribute on TextInput (id=noNewRenewals_Input) at DataUploadConfirmation.pcf: line 47, column 44
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.NewRenewals"))
    }
    
    // 'label' attribute on TextInput (id=noChange_Input) at DataUploadConfirmation.pcf: line 52, column 44
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Changes"))
    }
    
    // 'label' attribute on TextInput (id=noRenew_Input) at DataUploadConfirmation.pcf: line 57, column 44
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Renewals"))
    }
    
    // 'label' attribute on TextInput (id=noCancel_Input) at DataUploadConfirmation.pcf: line 64, column 44
    function label_29 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Cancellations"))
    }
    
    // 'label' attribute on TextInput (id=noRewrite_Input) at DataUploadConfirmation.pcf: line 69, column 44
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Rewrites"))
    }
    
    // 'label' attribute on TextInput (id=noReinstatement_Input) at DataUploadConfirmation.pcf: line 74, column 44
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Reinstatements"))
    }
    
    // 'label' attribute on TextInput (id=noAudits_Input) at DataUploadConfirmation.pcf: line 79, column 44
    function label_41 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Audits"))
    }
    
    // 'label' attribute on TextInput (id=noPaymentReversals_Input) at DataUploadConfirmation.pcf: line 86, column 44
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.PaymentReversals"))
    }
    
    // 'label' attribute on TextInput (id=noDirectPay_Input) at DataUploadConfirmation.pcf: line 91, column 44
    function label_49 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.DirectBillPayments"))
    }
    
    // 'label' attribute on TextInput (id=noProducer_Input) at DataUploadConfirmation.pcf: line 30, column 44
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producers"))
    }
    
    // 'label' attribute on TextInput (id=noAgencyBillPayments_Input) at DataUploadConfirmation.pcf: line 96, column 44
    function label_53 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.AgencyBillPayments"))
    }
    
    // 'label' attribute on TextInput (id=noBatches_Input) at DataUploadConfirmation.pcf: line 101, column 44
    function label_57 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Batches"))
    }
    
    // 'label' attribute on TextInput (id=noPol_Input) at DataUploadConfirmation.pcf: line 35, column 44
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policies"))
    }
    
    // 'parent' attribute on Page (id=DataUploadConfirmation) at DataUploadConfirmation.pcf: line 8, column 100
    static function parent_106 (processor :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) : pcf.api.Destination {
      return pcf.ServerTools.createDestination()
    }
    
    // 'title' attribute on Card (id=batch) at DataUploadConfirmation.pcf: line 211, column 101
    function title_102 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Batches")
    }
    
    // 'title' attribute on Card (id=paymentReversal) at DataUploadConfirmation.pcf: line 219, column 110
    function title_105 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.PaymentReversals")
    }
    
    // 'title' attribute on Page (id=DataUploadConfirmation) at DataUploadConfirmation.pcf: line 8, column 100
    static function title_107 (processor :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Confirmation")
    }
    
    // 'title' attribute on Card (id=account) at DataUploadConfirmation.pcf: line 107, column 102
    function title_63 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Accounts")
    }
    
    // 'title' attribute on Card (id=producer) at DataUploadConfirmation.pcf: line 115, column 103
    function title_66 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producers")
    }
    
    // 'title' attribute on Card (id=policy) at DataUploadConfirmation.pcf: line 123, column 102
    function title_69 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policies")
    }
    
    // 'title' attribute on Card (id=issuance) at DataUploadConfirmation.pcf: line 131, column 103
    function title_72 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Issuances")
    }
    
    // 'title' attribute on Card (id=newrenewal) at DataUploadConfirmation.pcf: line 139, column 105
    function title_75 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.NewRenewals")
    }
    
    // 'title' attribute on Card (id=change) at DataUploadConfirmation.pcf: line 147, column 101
    function title_78 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Changes")
    }
    
    // 'title' attribute on Card (id=renew) at DataUploadConfirmation.pcf: line 155, column 102
    function title_81 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Renewals")
    }
    
    // 'title' attribute on Card (id=cancel) at DataUploadConfirmation.pcf: line 163, column 107
    function title_84 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Cancellations")
    }
    
    // 'title' attribute on Card (id=directPayment) at DataUploadConfirmation.pcf: line 171, column 112
    function title_87 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.DirectBillPayments")
    }
    
    // 'title' attribute on Card (id=rewrite) at DataUploadConfirmation.pcf: line 179, column 102
    function title_90 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Rewrites")
    }
    
    // 'title' attribute on Card (id=reinstatements) at DataUploadConfirmation.pcf: line 187, column 108
    function title_93 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Reinstatements")
    }
    
    // 'title' attribute on Card (id=audits) at DataUploadConfirmation.pcf: line 195, column 100
    function title_96 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Audits")
    }
    
    // 'title' attribute on Card (id=agencyPayment) at DataUploadConfirmation.pcf: line 203, column 112
    function title_99 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.AgencyBillPayments")
    }
    
    // 'value' attribute on TextInput (id=noPol_Input) at DataUploadConfirmation.pcf: line 35, column 44
    function value_10 () : java.lang.Integer {
      return processor.PolicyArray == null ? 0 : processor.PolicyArray.countWhere(\ i -> i.Skipped == false and i.Error == false )
    }
    
    // 'value' attribute on TextInput (id=noIssuance_Input) at DataUploadConfirmation.pcf: line 40, column 44
    function value_14 () : java.lang.Integer {
      return processor.IssuanceArray == null ? 0 : processor.IssuanceArray.countWhere(\ i -> i.Skipped == false and i.Error == false )
    }
    
    // 'value' attribute on TextInput (id=noNewRenewals_Input) at DataUploadConfirmation.pcf: line 47, column 44
    function value_18 () : java.lang.Integer {
      return processor.NewRenewalArray == null ? 0 : processor.NewRenewalArray.countWhere(\ p -> p.Skipped == false and p.Error ==false)
    }
    
    // 'value' attribute on TextInput (id=noAcct_Input) at DataUploadConfirmation.pcf: line 25, column 44
    function value_2 () : java.lang.Integer {
      return processor.AccountArray == null ? 0 : processor.AccountArray.countWhere(\ a -> a.Skipped == false and a.Error== false )
    }
    
    // 'value' attribute on TextInput (id=noChange_Input) at DataUploadConfirmation.pcf: line 52, column 44
    function value_22 () : java.lang.Integer {
      return processor.PolicyChangeArray == null ? 0 : processor.PolicyChangeArray.countWhere(\ a -> a.Skipped == false and a.Error== false )
    }
    
    // 'value' attribute on TextInput (id=noRenew_Input) at DataUploadConfirmation.pcf: line 57, column 44
    function value_26 () : java.lang.Integer {
      return processor.RenewalArray == null ? 0 : processor.RenewalArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    // 'value' attribute on TextInput (id=noCancel_Input) at DataUploadConfirmation.pcf: line 64, column 44
    function value_30 () : java.lang.Integer {
      return processor.CancellationArray == null ? 0 : processor.CancellationArray.countWhere(\ i -> i.Skipped == false and i.Error == false )
    }
    
    // 'value' attribute on TextInput (id=noRewrite_Input) at DataUploadConfirmation.pcf: line 69, column 44
    function value_34 () : java.lang.Integer {
      return processor.RewriteArray == null ? 0 : processor.RewriteArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    // 'value' attribute on TextInput (id=noReinstatement_Input) at DataUploadConfirmation.pcf: line 74, column 44
    function value_38 () : java.lang.Integer {
      return processor.ReinstatementArray == null ? 0 : processor.ReinstatementArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    // 'value' attribute on TextInput (id=noAudits_Input) at DataUploadConfirmation.pcf: line 79, column 44
    function value_42 () : java.lang.Integer {
      return processor.AuditArray == null ? 0 : processor.AuditArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    // 'value' attribute on TextInput (id=noPaymentReversals_Input) at DataUploadConfirmation.pcf: line 86, column 44
    function value_46 () : java.lang.Integer {
      return processor.PaymentReversalArray == null ? 0 : processor.PaymentReversalArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    // 'value' attribute on TextInput (id=noDirectPay_Input) at DataUploadConfirmation.pcf: line 91, column 44
    function value_50 () : java.lang.Integer {
      return processor.DirectPaymentArray == null ? 0 : processor.DirectPaymentArray.countWhere(\ i -> i.Skipped == false and i.Error == false )
    }
    
    // 'value' attribute on TextInput (id=noAgencyBillPayments_Input) at DataUploadConfirmation.pcf: line 96, column 44
    function value_54 () : java.lang.Integer {
      return processor.AgencyBillPaymentArray == null ? 0 : processor.AgencyBillPaymentArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    // 'value' attribute on TextInput (id=noBatches_Input) at DataUploadConfirmation.pcf: line 101, column 44
    function value_58 () : java.lang.Integer {
      return processor.BatchArray == null ? 0 : processor.BatchArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    // 'value' attribute on TextInput (id=noProducer_Input) at DataUploadConfirmation.pcf: line 30, column 44
    function value_6 () : java.lang.Integer {
      return processor.ProducerArray == null ? 0 : processor.ProducerArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    override property get CurrentLocation () : pcf.DataUploadConfirmation {
      return super.CurrentLocation as pcf.DataUploadConfirmation
    }
    
    property get processor () : tdic.util.dataloader.processor.BCSampleDataLoaderProcessor {
      return getVariableValue("processor", 0) as tdic.util.dataloader.processor.BCSampleDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) {
      setVariableValue("processor", 0, $arg)
    }
    
    
  }
  
  
}