package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketRelatedTransactionsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TroubleTicketRelatedTransactionsDVExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketRelatedTransactionsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TroubleTicketRelatedTransactionsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=transactions) at TroubleTicketRelatedTransactionsDV.pcf: line 33, column 93
    function action_4 () : void {
      TroubleTicketTransactionsPopup.push(TroubleTicket)
    }
    
    // 'action' attribute on PickerToolbarButton (id=TroubleTicketRelatedTransactionsDV_AddButton) at TroubleTicketRelatedTransactionsDV.pcf: line 40, column 76
    function action_7 () : void {
      TransactionsPopup.push(TroubleTicket.PotentialAssociatedTransactions.iterator().toList().toTypedArray())
    }
    
    // 'action' attribute on ToolbarButton (id=transactions) at TroubleTicketRelatedTransactionsDV.pcf: line 33, column 93
    function action_dest_5 () : pcf.api.Destination {
      return pcf.TroubleTicketTransactionsPopup.createDestination(TroubleTicket)
    }
    
    // 'action' attribute on PickerToolbarButton (id=TroubleTicketRelatedTransactionsDV_AddButton) at TroubleTicketRelatedTransactionsDV.pcf: line 40, column 76
    function action_dest_8 () : pcf.api.Destination {
      return pcf.TransactionsPopup.createDestination(TroubleTicket.PotentialAssociatedTransactions.iterator().toList().toTypedArray())
    }
    
    // 'available' attribute on ToolbarButton (id=transactions) at TroubleTicketRelatedTransactionsDV.pcf: line 33, column 93
    function available_3 () : java.lang.Boolean {
      return !TroubleTicket.IsClosed
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=TroubleTicketRelatedTransactionsDV_RemoveButton) at TroubleTicketRelatedTransactionsDV.pcf: line 47, column 64
    function checkedRowAction_11 (element :  entity.Transaction, CheckedValue :  entity.Transaction) : void {
      TroubleTicket.removeAssociatedTransaction(CheckedValue)
    }
    
    // 'def' attribute on ListViewInput at TroubleTicketRelatedTransactionsDV.pcf: line 26, column 27
    function def_onEnter_12 (def :  pcf.TroubleTicketTransactionsLV) : void {
      def.onEnter(TroubleTicket.AssociatedTransactions, true)
    }
    
    // 'def' attribute on ListViewInput at TroubleTicketRelatedTransactionsDV.pcf: line 26, column 27
    function def_refreshVariables_13 (def :  pcf.TroubleTicketTransactionsLV) : void {
      def.refreshVariables(TroubleTicket.AssociatedTransactions, true)
    }
    
    // 'initialValue' attribute on Variable at TroubleTicketRelatedTransactionsDV.pcf: line 14, column 19
    function initialValue_0 () : int {
      return TroubleTicket.ArchivedAssociatedTransactionCount
    }
    
    // 'label' attribute on Label (id=ArchivedTransactionsLabel) at TroubleTicketRelatedTransactionsDV.pcf: line 20, column 48
    function label_2 () : java.lang.String {
      return DisplayKey.get("Web.TroubleTicketDetailsPopup.ArchivedTransactions", archivedTransactions)
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=TroubleTicketRelatedTransactionsDV_AddButton) at TroubleTicketRelatedTransactionsDV.pcf: line 40, column 76
    function onPick_9 (PickedValue :  Transaction[]) : void {
      TroubleTicket.associateWithTransactions(PickedValue)
    }
    
    // 'visible' attribute on Label (id=ArchivedTransactionsLabel) at TroubleTicketRelatedTransactionsDV.pcf: line 20, column 48
    function visible_1 () : java.lang.Boolean {
      return archivedTransactions > 0
    }
    
    property get TroubleTicket () : TroubleTicket {
      return getRequireValue("TroubleTicket", 0) as TroubleTicket
    }
    
    property set TroubleTicket ($arg :  TroubleTicket) {
      setRequireValue("TroubleTicket", 0, $arg)
    }
    
    property get archivedTransactions () : int {
      return getVariableValue("archivedTransactions", 0) as java.lang.Integer
    }
    
    property set archivedTransactions ($arg :  int) {
      setVariableValue("archivedTransactions", 0, $arg)
    }
    
    
  }
  
  
}