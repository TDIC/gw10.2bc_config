package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyExceptionItemCommentsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyExceptionItemCommentsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyExceptionItemCommentsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyExceptionItemCommentsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (exceptionItemViews :  gw.api.web.invoice.ExceptionItemView[]) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=AgencyExceptionItemCommentsPopup) at AgencyExceptionItemCommentsPopup.pcf: line 10, column 84
    function beforeCommit_7 (pickedValue :  java.lang.Object) : void {
      gw.api.web.producer.agencybill.ExceptionItemUtil.applyCommentToInvoiceItems(exceptionItemViews*.ExceptionItem, exceptionCommentsText, CurrentLocation)
    }
    
    // 'def' attribute on PanelRef at AgencyExceptionItemCommentsPopup.pcf: line 37, column 73
    function def_onEnter_5 (def :  pcf.AgencyBillExceptionsLV) : void {
      def.onEnter(exceptionItemViews, false, false)
    }
    
    // 'def' attribute on PanelRef at AgencyExceptionItemCommentsPopup.pcf: line 37, column 73
    function def_refreshVariables_6 (def :  pcf.AgencyBillExceptionsLV) : void {
      def.refreshVariables(exceptionItemViews, false, false)
    }
    
    // 'value' attribute on TextAreaInput (id=exceptionComments_Input) at AgencyExceptionItemCommentsPopup.pcf: line 33, column 44
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      exceptionCommentsText = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at AgencyExceptionItemCommentsPopup.pcf: line 19, column 32
    function initialValue_0 () : java.lang.String {
      return gw.api.web.producer.agencybill.ExceptionItemUtil.getInitialItemExceptionComments(exceptionItemViews*.ExceptionItem)
    }
    
    // EditButtons at AgencyExceptionItemCommentsPopup.pcf: line 22, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'value' attribute on TextAreaInput (id=exceptionComments_Input) at AgencyExceptionItemCommentsPopup.pcf: line 33, column 44
    function value_2 () : java.lang.String {
      return exceptionCommentsText
    }
    
    override property get CurrentLocation () : pcf.AgencyExceptionItemCommentsPopup {
      return super.CurrentLocation as pcf.AgencyExceptionItemCommentsPopup
    }
    
    property get exceptionCommentsText () : java.lang.String {
      return getVariableValue("exceptionCommentsText", 0) as java.lang.String
    }
    
    property set exceptionCommentsText ($arg :  java.lang.String) {
      setVariableValue("exceptionCommentsText", 0, $arg)
    }
    
    property get exceptionItemViews () : gw.api.web.invoice.ExceptionItemView[] {
      return getVariableValue("exceptionItemViews", 0) as gw.api.web.invoice.ExceptionItemView[]
    }
    
    property set exceptionItemViews ($arg :  gw.api.web.invoice.ExceptionItemView[]) {
      setVariableValue("exceptionItemViews", 0, $arg)
    }
    
    
  }
  
  
}