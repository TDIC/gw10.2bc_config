package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/allocationplan/PaymentAllocationPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentAllocationPlanDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/allocationplan/PaymentAllocationPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends PaymentAllocationPlanDetailScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=MoveUp) at PaymentAllocationPlanDetailScreen.pcf: line 159, column 113
    function action_54 () : void {
      moveUp(invoiceItemOrdering)
    }
    
    // 'action' attribute on Link (id=MoveDown) at PaymentAllocationPlanDetailScreen.pcf: line 167, column 61
    function action_57 () : void {
      moveDown(invoiceItemOrdering)
    }
    
    // 'available' attribute on Link (id=MoveUp) at PaymentAllocationPlanDetailScreen.pcf: line 159, column 113
    function available_52 () : java.lang.Boolean {
      return perm.System.payallocplanedit && CurrentLocation.InEditMode
    }
    
    // 'checkBoxVisible' attribute on RowIterator (id=InvoiceItemOrderingIterator) at PaymentAllocationPlanDetailScreen.pcf: line 131, column 77
    function checkBoxVisible_58 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'value' attribute on TextCell (id=Priority_Cell) at PaymentAllocationPlanDetailScreen.pcf: line 138, column 46
    function valueRoot_44 () : java.lang.Object {
      return paymentAllocationPlan.getInvoiceItemOrderingFor(invoiceItemOrdering)
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemOrderingName_Cell) at PaymentAllocationPlanDetailScreen.pcf: line 143, column 29
    function valueRoot_47 () : java.lang.Object {
      return invoiceItemOrdering
    }
    
    // 'value' attribute on TextCell (id=Priority_Cell) at PaymentAllocationPlanDetailScreen.pcf: line 138, column 46
    function value_43 () : java.lang.Integer {
      return paymentAllocationPlan.getInvoiceItemOrderingFor(invoiceItemOrdering).Priority
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemOrderingName_Cell) at PaymentAllocationPlanDetailScreen.pcf: line 143, column 29
    function value_46 () : java.lang.String {
      return invoiceItemOrdering.DisplayName
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemOrderingDescription_Cell) at PaymentAllocationPlanDetailScreen.pcf: line 148, column 56
    function value_49 () : java.lang.String {
      return invoiceItemOrdering.Description
    }
    
    // 'visible' attribute on Link (id=MoveUp) at PaymentAllocationPlanDetailScreen.pcf: line 159, column 113
    function visible_53 () : java.lang.Boolean {
      return paymentAllocationPlan.getInvoiceItemOrderingFor(invoiceItemOrdering).Priority > 1
    }
    
    // 'visible' attribute on Link (id=MoveDown) at PaymentAllocationPlanDetailScreen.pcf: line 167, column 61
    function visible_56 () : java.lang.Boolean {
      return canMoveDown(invoiceItemOrdering)
    }
    
    property get invoiceItemOrdering () : typekey.InvoiceItemOrderingType {
      return getIteratedValue(1) as typekey.InvoiceItemOrderingType
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/allocationplan/PaymentAllocationPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PaymentAllocationPlanDetailScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'checkBoxVisible' attribute on RowIterator (id=DistributionFilterIterator) at PaymentAllocationPlanDetailScreen.pcf: line 87, column 76
    function checkBoxVisible_34 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'value' attribute on TextCell (id=DistributionFilterName_Cell) at PaymentAllocationPlanDetailScreen.pcf: line 93, column 29
    function valueRoot_29 () : java.lang.Object {
      return distributionFilter
    }
    
    // 'value' attribute on TextCell (id=DistributionFilterName_Cell) at PaymentAllocationPlanDetailScreen.pcf: line 93, column 29
    function value_28 () : java.lang.String {
      return distributionFilter.DisplayName
    }
    
    // 'value' attribute on TextCell (id=DistributionFilterDescription_Cell) at PaymentAllocationPlanDetailScreen.pcf: line 98, column 55
    function value_31 () : java.lang.String {
      return distributionFilter.Description
    }
    
    property get distributionFilter () : typekey.DistributionFilterType {
      return getIteratedValue(1) as typekey.DistributionFilterType
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/allocationplan/PaymentAllocationPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentAllocationPlanDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=AddDistributionFilter) at PaymentAllocationPlanDetailScreen.pcf: line 69, column 96
    function action_24 () : void {
      AddDistributionFilterPopup.push(paymentAllocationPlan)
    }
    
    // 'action' attribute on ToolbarButton (id=Clone) at PaymentAllocationPlanDetailScreen.pcf: line 21, column 51
    function action_3 () : void {
      ClonePaymentAllocationPlan.go(paymentAllocationPlan)
    }
    
    // 'action' attribute on ToolbarButton (id=AddInvoiceItemOrdering) at PaymentAllocationPlanDetailScreen.pcf: line 113, column 98
    function action_38 () : void {
      AddInvoiceItemOrderingPopup.push(paymentAllocationPlan)
    }
    
    // 'action' attribute on ToolbarButton (id=AddDistributionFilter) at PaymentAllocationPlanDetailScreen.pcf: line 69, column 96
    function action_dest_25 () : pcf.api.Destination {
      return pcf.AddDistributionFilterPopup.createDestination(paymentAllocationPlan)
    }
    
    // 'action' attribute on ToolbarButton (id=AddInvoiceItemOrdering) at PaymentAllocationPlanDetailScreen.pcf: line 113, column 98
    function action_dest_39 () : pcf.api.Destination {
      return pcf.AddInvoiceItemOrderingPopup.createDestination(paymentAllocationPlan)
    }
    
    // 'action' attribute on ToolbarButton (id=Clone) at PaymentAllocationPlanDetailScreen.pcf: line 21, column 51
    function action_dest_4 () : pcf.api.Destination {
      return pcf.ClonePaymentAllocationPlan.createDestination(paymentAllocationPlan)
    }
    
    // 'available' attribute on ToolbarButton (id=AddDistributionFilter) at PaymentAllocationPlanDetailScreen.pcf: line 69, column 96
    function available_23 () : java.lang.Boolean {
      return !areAllAvailableFiltersAdded()
    }
    
    // 'available' attribute on ToolbarButton (id=AddInvoiceItemOrdering) at PaymentAllocationPlanDetailScreen.pcf: line 113, column 98
    function available_37 () : java.lang.Boolean {
      return !areAllAvailableOrderingsAdded()
    }
    
    // 'def' attribute on PanelRef at PaymentAllocationPlanDetailScreen.pcf: line 174, column 249
    function def_onEnter_61 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(paymentAllocationPlan, { "Name", "Description" }, { DisplayKey.get("Web.PaymentAllocationPlanDetailDV.Name"), DisplayKey.get("Web.PaymentAllocationPlanDetailDV.Description") })
    }
    
    // 'def' attribute on PanelRef at PaymentAllocationPlanDetailScreen.pcf: line 174, column 249
    function def_refreshVariables_62 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(paymentAllocationPlan, { "Name", "Description" }, { DisplayKey.get("Web.PaymentAllocationPlanDetailDV.Name"), DisplayKey.get("Web.PaymentAllocationPlanDetailDV.Description") })
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at PaymentAllocationPlanDetailScreen.pcf: line 42, column 53
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentAllocationPlan.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at PaymentAllocationPlanDetailScreen.pcf: line 50, column 56
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentAllocationPlan.EffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at PaymentAllocationPlanDetailScreen.pcf: line 56, column 57
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentAllocationPlan.ExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at PaymentAllocationPlanDetailScreen.pcf: line 35, column 46
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentAllocationPlan.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at PaymentAllocationPlanDetailScreen.pcf: line 13, column 23
    function initialValue_0 () : boolean {
      return gw.api.util.LocaleUtil.getAllLanguages().Count > 1
    }
    
    // EditButtons at PaymentAllocationPlanDetailScreen.pcf: line 15, column 21
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'value' attribute on TextCell (id=DistributionFilterName_Cell) at PaymentAllocationPlanDetailScreen.pcf: line 93, column 29
    function sortValue_26 (distributionFilter :  typekey.DistributionFilterType) : java.lang.Object {
      return distributionFilter.DisplayName
    }
    
    // 'value' attribute on TextCell (id=DistributionFilterDescription_Cell) at PaymentAllocationPlanDetailScreen.pcf: line 98, column 55
    function sortValue_27 (distributionFilter :  typekey.DistributionFilterType) : java.lang.Object {
      return distributionFilter.Description
    }
    
    // 'value' attribute on TextCell (id=Priority_Cell) at PaymentAllocationPlanDetailScreen.pcf: line 138, column 46
    function sortValue_40 (invoiceItemOrdering :  typekey.InvoiceItemOrderingType) : java.lang.Object {
      return paymentAllocationPlan.getInvoiceItemOrderingFor(invoiceItemOrdering).Priority
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemOrderingName_Cell) at PaymentAllocationPlanDetailScreen.pcf: line 143, column 29
    function sortValue_41 (invoiceItemOrdering :  typekey.InvoiceItemOrderingType) : java.lang.Object {
      return invoiceItemOrdering.DisplayName
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemOrderingDescription_Cell) at PaymentAllocationPlanDetailScreen.pcf: line 148, column 56
    function sortValue_42 (invoiceItemOrdering :  typekey.InvoiceItemOrderingType) : java.lang.Object {
      return invoiceItemOrdering.Description
    }
    
    // 'toRemove' attribute on RowIterator (id=DistributionFilterIterator) at PaymentAllocationPlanDetailScreen.pcf: line 87, column 76
    function toRemove_35 (distributionFilter :  typekey.DistributionFilterType) : void {
      paymentAllocationPlan.removeDistributionCriteriaFilter(distributionFilter)
    }
    
    // 'toRemove' attribute on RowIterator (id=InvoiceItemOrderingIterator) at PaymentAllocationPlanDetailScreen.pcf: line 131, column 77
    function toRemove_59 (invoiceItemOrdering :  typekey.InvoiceItemOrderingType) : void {
      paymentAllocationPlan.removeInvoiceItemOrderingFor(invoiceItemOrdering)
    }
    
    // 'validationExpression' attribute on DateInput (id=ExpirationDate_Input) at PaymentAllocationPlanDetailScreen.pcf: line 56, column 57
    function validationExpression_17 () : java.lang.Object {
      return paymentAllocationPlan.hasValidExpirationDate() ? null : DisplayKey.get("Web.Plan.ExpirationDate.ValidationError")
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at PaymentAllocationPlanDetailScreen.pcf: line 35, column 46
    function valueRoot_7 () : java.lang.Object {
      return paymentAllocationPlan
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at PaymentAllocationPlanDetailScreen.pcf: line 50, column 56
    function value_13 () : java.util.Date {
      return paymentAllocationPlan.EffectiveDate
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at PaymentAllocationPlanDetailScreen.pcf: line 56, column 57
    function value_18 () : java.util.Date {
      return paymentAllocationPlan.ExpirationDate
    }
    
    // 'value' attribute on RowIterator (id=DistributionFilterIterator) at PaymentAllocationPlanDetailScreen.pcf: line 87, column 76
    function value_36 () : java.util.List<typekey.DistributionFilterType> {
      return paymentAllocationPlan.DistributionFilters.toList()
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at PaymentAllocationPlanDetailScreen.pcf: line 35, column 46
    function value_5 () : java.lang.String {
      return paymentAllocationPlan.Name
    }
    
    // 'value' attribute on RowIterator (id=InvoiceItemOrderingIterator) at PaymentAllocationPlanDetailScreen.pcf: line 131, column 77
    function value_60 () : java.util.List<typekey.InvoiceItemOrderingType> {
      return paymentAllocationPlan.InvoiceItemOrderings.toList()
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at PaymentAllocationPlanDetailScreen.pcf: line 42, column 53
    function value_9 () : java.lang.String {
      return paymentAllocationPlan.Description
    }
    
    // 'visible' attribute on ToolbarButton (id=Clone) at PaymentAllocationPlanDetailScreen.pcf: line 21, column 51
    function visible_2 () : java.lang.Boolean {
      return perm.System.payallocplancreate
    }
    
    property get hasMultipleLanguages () : boolean {
      return getVariableValue("hasMultipleLanguages", 0) as java.lang.Boolean
    }
    
    property set hasMultipleLanguages ($arg :  boolean) {
      setVariableValue("hasMultipleLanguages", 0, $arg)
    }
    
    property get paymentAllocationPlan () : PaymentAllocationPlan {
      return getRequireValue("paymentAllocationPlan", 0) as PaymentAllocationPlan
    }
    
    property set paymentAllocationPlan ($arg :  PaymentAllocationPlan) {
      setRequireValue("paymentAllocationPlan", 0, $arg)
    }
    
    function moveUp(invoiceItemOrdering : InvoiceItemOrderingType) {
      paymentAllocationPlan.increaseInvoiceItemOrderingPriority(invoiceItemOrdering)
    }
    
    function moveDown(invoiceItemOrdering : InvoiceItemOrderingType) {
      paymentAllocationPlan.decreaseInvoiceItemOrderingPriority(invoiceItemOrdering)
    }
    
    function canMoveDown(invoiceItemOrdering : InvoiceItemOrderingType) : boolean {
      final var orderings = paymentAllocationPlan.InvoiceItemOrderings
      
      var sortedOrderings = orderings.toList().sortBy(\ ordering -> paymentAllocationPlan.getInvoiceItemOrderingFor(ordering).Priority)
      
      return !(sortedOrderings.last() == invoiceItemOrdering)
    }
    
    function areAllAvailableOrderingsAdded() : boolean {
      var orderingsOnPlan = paymentAllocationPlan.InvoiceItemOrderings
      var availableOrderings = InvoiceItemOrderingType.getTypeKeys(false)
      
      foreach (orderingType in availableOrderings) {
        if (!orderingsOnPlan.contains(orderingType)) return false
      }
      
      return true
    }
    
    function areAllAvailableFiltersAdded() : boolean {
      var filtersOnPlan = paymentAllocationPlan.DistributionFilters
      var availableFilters = DistributionFilterType.getTypeKeys(false)
    
      foreach (filterType in availableFilters) {
        if (!filtersOnPlan.contains(filterType)) return false
      }
    
      return true
    }
    
    
  }
  
  
}