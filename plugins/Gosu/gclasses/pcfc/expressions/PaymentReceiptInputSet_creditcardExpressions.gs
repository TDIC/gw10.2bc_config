package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/PaymentReceiptInputSet.creditcard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentReceiptInputSet_creditcardExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/PaymentReceiptInputSet.creditcard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentReceiptInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.creditcard.pcf: line 13, column 43
    function def_onEnter_0 (def :  pcf.PaymentInstrumentInputSet_ach) : void {
      def.onEnter(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.creditcard.pcf: line 13, column 43
    function def_onEnter_2 (def :  pcf.PaymentInstrumentInputSet_creditcard) : void {
      def.onEnter(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.creditcard.pcf: line 13, column 43
    function def_onEnter_4 (def :  pcf.PaymentInstrumentInputSet_default) : void {
      def.onEnter(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.creditcard.pcf: line 13, column 43
    function def_onEnter_6 (def :  pcf.PaymentInstrumentInputSet_misc) : void {
      def.onEnter(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.creditcard.pcf: line 13, column 43
    function def_onEnter_8 (def :  pcf.PaymentInstrumentInputSet_wire) : void {
      def.onEnter(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.creditcard.pcf: line 13, column 43
    function def_refreshVariables_1 (def :  pcf.PaymentInstrumentInputSet_ach) : void {
      def.refreshVariables(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.creditcard.pcf: line 13, column 43
    function def_refreshVariables_3 (def :  pcf.PaymentInstrumentInputSet_creditcard) : void {
      def.refreshVariables(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.creditcard.pcf: line 13, column 43
    function def_refreshVariables_5 (def :  pcf.PaymentInstrumentInputSet_default) : void {
      def.refreshVariables(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.creditcard.pcf: line 13, column 43
    function def_refreshVariables_7 (def :  pcf.PaymentInstrumentInputSet_misc) : void {
      def.refreshVariables(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.creditcard.pcf: line 13, column 43
    function def_refreshVariables_9 (def :  pcf.PaymentInstrumentInputSet_wire) : void {
      def.refreshVariables(paymentReceipt.PaymentInstrument)
    }
    
    // 'mode' attribute on InputSetRef at PaymentReceiptInputSet.creditcard.pcf: line 13, column 43
    function mode_10 () : java.lang.Object {
      return PaymentMethod.TC_CREDITCARD
    }
    
    property get paymentReceipt () : PaymentReceipt {
      return getRequireValue("paymentReceipt", 0) as PaymentReceipt
    }
    
    property set paymentReceipt ($arg :  PaymentReceipt) {
      setRequireValue("paymentReceipt", 0, $arg)
    }
    
    
  }
  
  
}