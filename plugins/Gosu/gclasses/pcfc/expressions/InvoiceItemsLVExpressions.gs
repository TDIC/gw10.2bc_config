package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/InvoiceItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceItemsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/invoice/InvoiceItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceItemsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at InvoiceItemsLV.pcf: line 159, column 87
    function currency_82 () : typekey.Currency {
      return charge != null ? charge.Currency : null
    }
    
    // 'editable' attribute on RowIterator at InvoiceItemsLV.pcf: line 28, column 40
    function editable_15 () : java.lang.Boolean {
      return charge != null
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at InvoiceItemsLV.pcf: line 40, column 44
    function sortValue_0 (invoiceItem :  entity.InvoiceItem) : java.lang.Object {
      return invoiceItem.InstallmentNumber
    }
    
    // 'value' attribute on DateCell (id=PaymentDate_Cell) at InvoiceItemsLV.pcf: line 47, column 51
    function sortValue_2 (invoiceItem :  entity.InvoiceItem) : java.lang.Object {
      return invoiceItem.EventDateAndInvoice
    }
    
    // 'value' attribute on TextCell (id=Invoice_Cell) at InvoiceItemsLV.pcf: line 55, column 25
    function sortValue_3 (invoiceItem :  entity.InvoiceItem) : java.lang.Object {
      return invoiceItem.Invoice
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at InvoiceItemsLV.pcf: line 59, column 47
    function sortValue_4 (invoiceItem :  entity.InvoiceItem) : java.lang.Object {
      return invoiceItem.InvoiceDueDate
    }
    
    // 'value' attribute on TextCell (id=Context_Cell) at InvoiceItemsLV.pcf: line 79, column 50
    function sortValue_5 (invoiceItem :  entity.InvoiceItem) : java.lang.Object {
      return invoiceItem.Charge.BillingInstruction
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at InvoiceItemsLV.pcf: line 88, column 25
    function sortValue_6 (invoiceItem :  entity.InvoiceItem) : java.lang.Object {
      return invoiceItem.Type
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at InvoiceItemsLV.pcf: line 99, column 38
    function sortValue_7 (invoiceItem :  entity.InvoiceItem) : java.lang.Object {
      return invoiceItem.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PaidAmount_Cell) at InvoiceItemsLV.pcf: line 110, column 43
    function sortValue_8 (invoiceItem :  entity.InvoiceItem) : java.lang.Object {
      return invoiceItem.PaidAmount
    }
    
    // 'value' attribute on TextCell (id=InvoiceStream_Cell) at InvoiceItemsLV.pcf: line 115, column 45
    function sortValue_9 (invoiceItem :  entity.InvoiceItem) : java.lang.Object {
      return invoiceItem.Invoice.InvoiceStream
    }
    
    // '$$sumValue' attribute on RowIterator at InvoiceItemsLV.pcf: line 99, column 38
    function sumValueRoot_12 (invoiceItem :  entity.InvoiceItem) : java.lang.Object {
      return invoiceItem
    }
    
    // 'footerSumValue' attribute on RowIterator at InvoiceItemsLV.pcf: line 99, column 38
    function sumValue_11 (invoiceItem :  entity.InvoiceItem) : java.lang.Object {
      return invoiceItem.Amount
    }
    
    // 'footerSumValue' attribute on RowIterator at InvoiceItemsLV.pcf: line 110, column 43
    function sumValue_13 (invoiceItem :  entity.InvoiceItem) : java.lang.Object {
      return invoiceItem.PaidAmount
    }
    
    // 'toRemove' attribute on RowIterator at InvoiceItemsLV.pcf: line 28, column 40
    function toRemove_69 (invoiceItem :  entity.InvoiceItem) : void {
      invoiceItem.canRemove()
    }
    
    // 'value' attribute on RowIterator at InvoiceItemsLV.pcf: line 28, column 40
    function value_70 () : entity.InvoiceItem[] {
      return invoiceItems
    }
    
    // 'value' attribute on TextCell (id=Spacer1_Cell) at InvoiceItemsLV.pcf: line 132, column 39
    function value_71 () : java.lang.Object {
      return null
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at InvoiceItemsLV.pcf: line 159, column 87
    function value_81 () : gw.pl.currency.MonetaryAmount {
      return charge != null ? charge.Amount - charge.getInvoiceItemTotal() : null
    }
    
    // 'visible' attribute on TextCell (id=InstallmentNumber_Cell) at InvoiceItemsLV.pcf: line 40, column 44
    function visible_1 () : java.lang.Boolean {
      return showInstallmentNumber
    }
    
    property get charge () : Charge {
      return getRequireValue("charge", 0) as Charge
    }
    
    property set charge ($arg :  Charge) {
      setRequireValue("charge", 0, $arg)
    }
    
    property get invoiceItems () : InvoiceItem[] {
      return getRequireValue("invoiceItems", 0) as InvoiceItem[]
    }
    
    property set invoiceItems ($arg :  InvoiceItem[]) {
      setRequireValue("invoiceItems", 0, $arg)
    }
    
    property get showInstallmentNumber () : Boolean {
      return getRequireValue("showInstallmentNumber", 0) as Boolean
    }
    
    property set showInstallmentNumber ($arg :  Boolean) {
      setRequireValue("showInstallmentNumber", 0, $arg)
    }
    
    
                  function goToPayer(invoiceItem: InvoiceItem) {
                    var payer = invoiceItem.getPayer()
                    if (payer typeis Producer) {
                      ProducerDetail.push(payer)
                    } else {
                      AccountSummary.push(payer as Account)
                    }
                  }
    
                  function getPayerDisplay(invoiceItem: InvoiceItem): String {
                    var invoiceHolder = invoiceItem.getPayer()
                    if (invoiceHolder == null) {
                      return charge.OwnerAccount as String
                    }
                    if (typeof invoiceHolder == Account or not invoiceItem.PolicyPeriodItem) {
                      return invoiceHolder as String
                    }
                    var itemCommission = invoiceItem.ActivePrimaryItemCommission
                    var producerCode = itemCommission.PolicyCommission.ProducerCode
                    var producer = producerCode.Producer
                    return producer.DisplayName + " " + producerCode.Code
                  }
          
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/invoice/InvoiceItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends InvoiceItemsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on DateCell (id=PaymentDate_Cell) at InvoiceItemsLV.pcf: line 47, column 51
    function action_22 () : void {
      InvoiceItemDetailPopup.push(invoiceItem)
    }
    
    // 'action' attribute on TextCell (id=itemOwner_Cell) at InvoiceItemsLV.pcf: line 67, column 39
    function action_34 () : void {
      AccountSummary.push(invoiceItem.Owner)
    }
    
    // 'action' attribute on TextCell (id=itemPayer_Cell) at InvoiceItemsLV.pcf: line 74, column 49
    function action_39 () : void {
      goToPayer(invoiceItem)
    }
    
    // 'action' attribute on MonetaryAmountCell (id=Amount_Cell) at InvoiceItemsLV.pcf: line 99, column 38
    function action_54 () : void {
      InvoiceItemDetailPopup.push(invoiceItem)
    }
    
    // 'action' attribute on DateCell (id=PaymentDate_Cell) at InvoiceItemsLV.pcf: line 47, column 51
    function action_dest_23 () : pcf.api.Destination {
      return pcf.InvoiceItemDetailPopup.createDestination(invoiceItem)
    }
    
    // 'action' attribute on TextCell (id=itemOwner_Cell) at InvoiceItemsLV.pcf: line 67, column 39
    function action_dest_35 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination(invoiceItem.Owner)
    }
    
    // 'action' attribute on MonetaryAmountCell (id=Amount_Cell) at InvoiceItemsLV.pcf: line 99, column 38
    function action_dest_55 () : pcf.api.Destination {
      return pcf.InvoiceItemDetailPopup.createDestination(invoiceItem)
    }
    
    // 'checkBoxVisible' attribute on RowIterator at InvoiceItemsLV.pcf: line 28, column 40
    function checkBoxVisible_68 () : java.lang.Boolean {
      return charge != null && invoiceItem.canReverse() && invoiceItem.Type != InvoiceItemType.TC_COMMISSIONREMAINDER
    }
    
    // 'condition' attribute on ToolbarFlag at InvoiceItemsLV.pcf: line 31, column 27
    function condition_16 () : java.lang.Boolean {
      return !invoiceItem.Frozen
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at InvoiceItemsLV.pcf: line 99, column 38
    function currency_58 () : typekey.Currency {
      return invoiceItem.Currency
    }
    
    // 'editable' attribute on DateCell (id=PaymentDate_Cell) at InvoiceItemsLV.pcf: line 47, column 51
    function editable_21 () : java.lang.Boolean {
      return invoiceItem.New
    }
    
    // 'editable' attribute on MonetaryAmountCell (id=Amount_Cell) at InvoiceItemsLV.pcf: line 99, column 38
    function editable_53 () : java.lang.Boolean {
      return invoiceItem.New or ( invoiceItem.Invoice.Status == InvoiceStatus.TC_PLANNED and invoiceItem.Type != InvoiceItemType.TC_COMMISSIONADJUSTMENT and invoiceItem.Type != InvoiceItemType.TC_COMMISSIONREMAINDER )
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at InvoiceItemsLV.pcf: line 88, column 25
    function valueRange_48 () : java.lang.Object {
      return gw.api.web.invoice.InvoiceUtil.getUserVisibleInvoiceItemTypes()
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at InvoiceItemsLV.pcf: line 40, column 44
    function valueRoot_18 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on TextCell (id=Context_Cell) at InvoiceItemsLV.pcf: line 79, column 50
    function valueRoot_43 () : java.lang.Object {
      return invoiceItem.Charge
    }
    
    // 'value' attribute on TextCell (id=InvoiceStream_Cell) at InvoiceItemsLV.pcf: line 115, column 45
    function valueRoot_66 () : java.lang.Object {
      return invoiceItem.Invoice
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at InvoiceItemsLV.pcf: line 40, column 44
    function value_17 () : java.lang.Integer {
      return invoiceItem.InstallmentNumber
    }
    
    // 'value' attribute on DateCell (id=PaymentDate_Cell) at InvoiceItemsLV.pcf: line 47, column 51
    function value_24 () : java.util.Date {
      return invoiceItem.EventDateAndInvoice
    }
    
    // 'value' attribute on TextCell (id=Invoice_Cell) at InvoiceItemsLV.pcf: line 55, column 25
    function value_28 () : entity.Invoice {
      return invoiceItem.Invoice
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at InvoiceItemsLV.pcf: line 59, column 47
    function value_31 () : java.util.Date {
      return invoiceItem.InvoiceDueDate
    }
    
    // 'value' attribute on TextCell (id=itemOwner_Cell) at InvoiceItemsLV.pcf: line 67, column 39
    function value_36 () : entity.Account {
      return invoiceItem.Owner
    }
    
    // 'value' attribute on TextCell (id=itemPayer_Cell) at InvoiceItemsLV.pcf: line 74, column 49
    function value_40 () : java.lang.String {
      return getPayerDisplay(invoiceItem)
    }
    
    // 'value' attribute on TextCell (id=Context_Cell) at InvoiceItemsLV.pcf: line 79, column 50
    function value_42 () : entity.BillingInstruction {
      return invoiceItem.Charge.BillingInstruction
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at InvoiceItemsLV.pcf: line 88, column 25
    function value_46 () : typekey.InvoiceItemType {
      return invoiceItem.Type
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at InvoiceItemsLV.pcf: line 99, column 38
    function value_56 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PaidAmount_Cell) at InvoiceItemsLV.pcf: line 110, column 43
    function value_61 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.PaidAmount
    }
    
    // 'value' attribute on TextCell (id=InvoiceStream_Cell) at InvoiceItemsLV.pcf: line 115, column 45
    function value_65 () : entity.InvoiceStream {
      return invoiceItem.Invoice.InvoiceStream
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at InvoiceItemsLV.pcf: line 88, column 25
    function verifyValueRangeIsAllowedType_49 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at InvoiceItemsLV.pcf: line 88, column 25
    function verifyValueRangeIsAllowedType_49 ($$arg :  typekey.InvoiceItemType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at InvoiceItemsLV.pcf: line 88, column 25
    function verifyValueRange_50 () : void {
      var __valueRangeArg = gw.api.web.invoice.InvoiceUtil.getUserVisibleInvoiceItemTypes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_49(__valueRangeArg)
    }
    
    // 'visible' attribute on TextCell (id=InstallmentNumber_Cell) at InvoiceItemsLV.pcf: line 40, column 44
    function visible_19 () : java.lang.Boolean {
      return showInstallmentNumber
    }
    
    property get invoiceItem () : entity.InvoiceItem {
      return getIteratedValue(1) as entity.InvoiceItem
    }
    
    
  }
  
  
}