package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/NewTroubleTicketInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewTroubleTicketInfoDVExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/NewTroubleTicketInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewTroubleTicketInfoDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeInput (id=TicketType_Input) at NewTroubleTicketInfoDV.pcf: line 25, column 48
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      ticket.TicketType = (__VALUE_TO_SET as typekey.TroubleTicketType)
    }
    
    // 'value' attribute on TextAreaInput (id=DetailedDescription_Input) at NewTroubleTicketInfoDV.pcf: line 40, column 45
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      ticket.DetailedDescription = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Priority_Input) at NewTroubleTicketInfoDV.pcf: line 47, column 39
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      ticket.Priority = (__VALUE_TO_SET as typekey.Priority)
    }
    
    // 'value' attribute on DateInput (id=DueDate_Input) at NewTroubleTicketInfoDV.pcf: line 53, column 36
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      ticket.TargetDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=EscalationDate_Input) at NewTroubleTicketInfoDV.pcf: line 59, column 40
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      ticket.EscalationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at NewTroubleTicketInfoDV.pcf: line 33, column 31
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      ticket.Title = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'validationExpression' attribute on DateInput (id=DueDate_Input) at NewTroubleTicketInfoDV.pcf: line 53, column 36
    function validationExpression_19 () : java.lang.Object {
      return gw.troubleticket.TroubleTicketMethods.validateTargetDate(ticket)
    }
    
    // 'validationExpression' attribute on DateInput (id=EscalationDate_Input) at NewTroubleTicketInfoDV.pcf: line 59, column 40
    function validationExpression_25 () : java.lang.Object {
      return gw.troubleticket.TroubleTicketMethods.validateEscalationDate(ticket)
    }
    
    // 'valueRange' attribute on RangeInput (id=TicketType_Input) at NewTroubleTicketInfoDV.pcf: line 25, column 48
    function valueRange_3 () : java.lang.Object {
      return ticket.AvailableTicketTypes
    }
    
    // 'value' attribute on RangeInput (id=TicketType_Input) at NewTroubleTicketInfoDV.pcf: line 25, column 48
    function valueRoot_2 () : java.lang.Object {
      return ticket
    }
    
    // 'value' attribute on RangeInput (id=TicketType_Input) at NewTroubleTicketInfoDV.pcf: line 25, column 48
    function value_0 () : typekey.TroubleTicketType {
      return ticket.TicketType
    }
    
    // 'value' attribute on TextAreaInput (id=DetailedDescription_Input) at NewTroubleTicketInfoDV.pcf: line 40, column 45
    function value_11 () : java.lang.String {
      return ticket.DetailedDescription
    }
    
    // 'value' attribute on TypeKeyInput (id=Priority_Input) at NewTroubleTicketInfoDV.pcf: line 47, column 39
    function value_15 () : typekey.Priority {
      return ticket.Priority
    }
    
    // 'value' attribute on DateInput (id=DueDate_Input) at NewTroubleTicketInfoDV.pcf: line 53, column 36
    function value_20 () : java.util.Date {
      return ticket.TargetDate
    }
    
    // 'value' attribute on DateInput (id=EscalationDate_Input) at NewTroubleTicketInfoDV.pcf: line 59, column 40
    function value_26 () : java.util.Date {
      return ticket.EscalationDate
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at NewTroubleTicketInfoDV.pcf: line 33, column 31
    function value_7 () : java.lang.String {
      return ticket.Title
    }
    
    // 'valueRange' attribute on RangeInput (id=TicketType_Input) at NewTroubleTicketInfoDV.pcf: line 25, column 48
    function verifyValueRangeIsAllowedType_4 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=TicketType_Input) at NewTroubleTicketInfoDV.pcf: line 25, column 48
    function verifyValueRangeIsAllowedType_4 ($$arg :  typekey.TroubleTicketType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=TicketType_Input) at NewTroubleTicketInfoDV.pcf: line 25, column 48
    function verifyValueRange_5 () : void {
      var __valueRangeArg = ticket.AvailableTicketTypes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_4(__valueRangeArg)
    }
    
    property get AssigneeHolder () : gw.api.assignment.Assignee[] {
      return getRequireValue("AssigneeHolder", 0) as gw.api.assignment.Assignee[]
    }
    
    property set AssigneeHolder ($arg :  gw.api.assignment.Assignee[]) {
      setRequireValue("AssigneeHolder", 0, $arg)
    }
    
    property get ticket () : TroubleTicket {
      return getRequireValue("ticket", 0) as TroubleTicket
    }
    
    property set ticket ($arg :  TroubleTicket) {
      setRequireValue("ticket", 0, $arg)
    }
    
    
  }
  
  
}