package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/AccountSearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountSearchResultsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/AccountSearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountSearchResultsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at AccountSearchResultsLV.pcf: line 54, column 25
    function sortValue_1 (accountSearchView :  entity.AccountSearchView) : java.lang.Object {
      return accountSearchView.AccountNumber
    }
    
    // 'sortBy' attribute on TextCell (id=AccountName_Cell) at AccountSearchResultsLV.pcf: line 60, column 135
    function sortValue_2 (accountSearchView :  entity.AccountSearchView) : java.lang.Object {
      return accountSearchView.AccountName
    }
    
    // 'value' attribute on TypeKeyCell (id=Currency_Cell) at AccountSearchResultsLV.pcf: line 66, column 69
    function sortValue_3 (accountSearchView :  entity.AccountSearchView) : java.lang.Object {
      return accountSearchView.Currency
    }
    
    // 'value' attribute on TypeKeyCell (id=Segment_Cell) at AccountSearchResultsLV.pcf: line 73, column 25
    function sortValue_5 (accountSearchView :  entity.AccountSearchView) : java.lang.Object {
      return accountSearchView.Segment
    }
    
    // 'value' attribute on TypeKeyCell (id=AccountType_Cell) at AccountSearchResultsLV.pcf: line 79, column 25
    function sortValue_6 (accountSearchView :  entity.AccountSearchView) : java.lang.Object {
      return accountSearchView.AccountType
    }
    
    // 'sortBy' attribute on DateCell (id=CreationDate_Cell) at AccountSearchResultsLV.pcf: line 85, column 49
    function sortValue_7 (accountSearchView :  entity.AccountSearchView) : java.lang.Object {
      return accountSearchView.CreateTime
    }
    
    // 'value' attribute on TypeKeyCell (id=DelinquencyStatus_Cell) at AccountSearchResultsLV.pcf: line 91, column 25
    function sortValue_8 (accountSearchView :  entity.AccountSearchView) : java.lang.Object {
      return accountSearchView.DelinquencyStatus
    }
    
    // 'value' attribute on RowIterator at AccountSearchResultsLV.pcf: line 34, column 84
    function value_39 () : gw.api.database.IQueryBeanResult<entity.AccountSearchView> {
      return accountSearchViews
    }
    
    // 'visible' attribute on LinkCell (id=SelectCell) at AccountSearchResultsLV.pcf: line 39, column 50
    function visible_0 () : java.lang.Boolean {
      return isWizard and !showCheckboxes
    }
    
    // 'visible' attribute on TypeKeyCell (id=Currency_Cell) at AccountSearchResultsLV.pcf: line 66, column 69
    function visible_4 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    property get accountSearchViews () : gw.api.database.IQueryBeanResult<AccountSearchView> {
      return getRequireValue("accountSearchViews", 0) as gw.api.database.IQueryBeanResult<AccountSearchView>
    }
    
    property set accountSearchViews ($arg :  gw.api.database.IQueryBeanResult<AccountSearchView>) {
      setRequireValue("accountSearchViews", 0, $arg)
    }
    
    property get isWizard () : boolean {
      return getRequireValue("isWizard", 0) as java.lang.Boolean
    }
    
    property set isWizard ($arg :  boolean) {
      setRequireValue("isWizard", 0, $arg)
    }
    
    property get showCheckboxes () : Boolean {
      return getRequireValue("showCheckboxes", 0) as Boolean
    }
    
    property set showCheckboxes ($arg :  Boolean) {
      setRequireValue("showCheckboxes", 0, $arg)
    }
    
    property get showHyperlinks () : boolean {
      return getRequireValue("showHyperlinks", 0) as java.lang.Boolean
    }
    
    property set showHyperlinks ($arg :  boolean) {
      setRequireValue("showHyperlinks", 0, $arg)
    }
    
    property get tAccountOwnerReference () : gw.api.web.accounting.TAccountOwnerReference {
      return getRequireValue("tAccountOwnerReference", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set tAccountOwnerReference ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setRequireValue("tAccountOwnerReference", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/AccountSearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountSearchResultsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at AccountSearchResultsLV.pcf: line 54, column 25
    function action_12 () : void {
      AccountOverview.go(accountSearchView.Account)
    }
    
    // 'action' attribute on Link (id=Select) at AccountSearchResultsLV.pcf: line 44, column 38
    function action_9 () : void {
      tAccountOwnerReference.TAccountOwner = accountSearchView.Account; (CurrentLocation as pcf.api.Wizard).next();
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at AccountSearchResultsLV.pcf: line 54, column 25
    function action_dest_13 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(accountSearchView.Account)
    }
    
    // 'available' attribute on TextCell (id=AccountNumber_Cell) at AccountSearchResultsLV.pcf: line 54, column 25
    function available_11 () : java.lang.Boolean {
      return showHyperlinks
    }
    
    // 'canPick' attribute on RowIterator at AccountSearchResultsLV.pcf: line 34, column 84
    function canPick_36 () : java.lang.Boolean {
      return !isWizard
    }
    
    // 'checkBoxVisible' attribute on RowIterator at AccountSearchResultsLV.pcf: line 34, column 84
    function checkBoxVisible_37 () : java.lang.Boolean {
      return showCheckboxes
    }
    
    // 'pickValue' attribute on RowIterator at AccountSearchResultsLV.pcf: line 34, column 84
    function pickValue_38 () : java.lang.Object {
      return gw.api.web.search.SearchPopupUtil.getAccount(accountSearchView)
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at AccountSearchResultsLV.pcf: line 54, column 25
    function valueRoot_15 () : java.lang.Object {
      return accountSearchView
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at AccountSearchResultsLV.pcf: line 54, column 25
    function value_14 () : java.lang.String {
      return accountSearchView.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=AccountName_Cell) at AccountSearchResultsLV.pcf: line 60, column 135
    function value_18 () : java.lang.String {
      return accountSearchView.AccountNameKanji.HasContent ? accountSearchView.AccountNameKanji : accountSearchView.AccountName
    }
    
    // 'value' attribute on TypeKeyCell (id=Currency_Cell) at AccountSearchResultsLV.pcf: line 66, column 69
    function value_20 () : typekey.Currency {
      return accountSearchView.Currency
    }
    
    // 'value' attribute on TypeKeyCell (id=Segment_Cell) at AccountSearchResultsLV.pcf: line 73, column 25
    function value_24 () : typekey.AccountSegment {
      return accountSearchView.Segment
    }
    
    // 'value' attribute on TypeKeyCell (id=AccountType_Cell) at AccountSearchResultsLV.pcf: line 79, column 25
    function value_27 () : typekey.AccountType {
      return accountSearchView.AccountType
    }
    
    // 'value' attribute on DateCell (id=CreationDate_Cell) at AccountSearchResultsLV.pcf: line 85, column 49
    function value_30 () : java.util.Date {
      return accountSearchView.CreateTime
    }
    
    // 'value' attribute on TypeKeyCell (id=DelinquencyStatus_Cell) at AccountSearchResultsLV.pcf: line 91, column 25
    function value_33 () : typekey.DelinquencyStatus {
      return accountSearchView.DelinquencyStatus
    }
    
    // 'visible' attribute on LinkCell (id=SelectCell) at AccountSearchResultsLV.pcf: line 39, column 50
    function visible_10 () : java.lang.Boolean {
      return isWizard and !showCheckboxes
    }
    
    // 'visible' attribute on TypeKeyCell (id=Currency_Cell) at AccountSearchResultsLV.pcf: line 66, column 69
    function visible_22 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    property get accountSearchView () : entity.AccountSearchView {
      return getIteratedValue(1) as entity.AccountSearchView
    }
    
    
  }
  
  
}