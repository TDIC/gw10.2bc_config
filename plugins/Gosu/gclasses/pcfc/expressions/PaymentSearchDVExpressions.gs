package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/PaymentSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/PaymentSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerInput (id=PolicyNumberCriterion_Input) at PaymentSearchDV.pcf: line 41, column 74
    function action_16 () : void {
      PolicySearchPopup.push(true)
    }
    
    // 'pickLocation' attribute on PickerInput (id=ProducerNameCriterion_Input) at PaymentSearchDV.pcf: line 50, column 89
    function action_25 () : void {
      ProducerSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=ProducerNameKanjiCriterion_Input) at PaymentSearchDV.pcf: line 59, column 168
    function action_36 () : void {
      ProducerSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountNumberCriterion_Input) at PaymentSearchDV.pcf: line 32, column 74
    function action_7 () : void {
      AccountSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=PolicyNumberCriterion_Input) at PaymentSearchDV.pcf: line 41, column 74
    function action_dest_17 () : pcf.api.Destination {
      return pcf.PolicySearchPopup.createDestination(true)
    }
    
    // 'pickLocation' attribute on PickerInput (id=ProducerNameCriterion_Input) at PaymentSearchDV.pcf: line 50, column 89
    function action_dest_26 () : pcf.api.Destination {
      return pcf.ProducerSearchPopup.createDestination()
    }
    
    // 'pickLocation' attribute on PickerInput (id=ProducerNameKanjiCriterion_Input) at PaymentSearchDV.pcf: line 59, column 168
    function action_dest_37 () : pcf.api.Destination {
      return pcf.ProducerSearchPopup.createDestination()
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountNumberCriterion_Input) at PaymentSearchDV.pcf: line 32, column 74
    function action_dest_8 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'available' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at PaymentSearchDV.pcf: line 120, column 43
    function available_81 () : java.lang.Boolean {
      return (searchCriteria.CurrencyType != null or gw.api.util.CurrencyUtil.isSingleCurrencyMode())
    }
    
    // 'conversionExpression' attribute on PickerInput (id=AccountNumberCriterion_Input) at PaymentSearchDV.pcf: line 32, column 74
    function conversionExpression_10 (PickedValue :  Account) : java.lang.String {
      return PickedValue.DisplayName
    }
    
    // 'conversionExpression' attribute on PickerInput (id=PolicyNumberCriterion_Input) at PaymentSearchDV.pcf: line 41, column 74
    function conversionExpression_19 (PickedValue :  PolicyPeriod) : java.lang.String {
      return PickedValue.DisplayName
    }
    
    // 'conversionExpression' attribute on PickerInput (id=ProducerNameCriterion_Input) at PaymentSearchDV.pcf: line 50, column 89
    function conversionExpression_29 (PickedValue :  Producer) : java.lang.String {
      return PickedValue.Name
    }
    
    // 'conversionExpression' attribute on PickerInput (id=ProducerNameKanjiCriterion_Input) at PaymentSearchDV.pcf: line 59, column 168
    function conversionExpression_39 (PickedValue :  Producer) : java.lang.String {
      return PickedValue.NameKanji
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at PaymentSearchDV.pcf: line 120, column 43
    function currency_85 () : typekey.Currency {
      return gw.search.SearchHelper.getDefaultCurrencyIfNull(searchCriteria.CurrencyType)
    }
    
    // 'def' attribute on InputSetRef at PaymentSearchDV.pcf: line 140, column 41
    function def_onEnter_101 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at PaymentSearchDV.pcf: line 136, column 77
    function def_onEnter_99 (def :  pcf.ContactCriteriaInputSet) : void {
      def.onEnter(searchCriteria.ContactCriteria,true)
    }
    
    // 'def' attribute on InputSetRef at PaymentSearchDV.pcf: line 136, column 77
    function def_refreshVariables_100 (def :  pcf.ContactCriteriaInputSet) : void {
      def.refreshVariables(searchCriteria.ContactCriteria,true)
    }
    
    // 'def' attribute on InputSetRef at PaymentSearchDV.pcf: line 140, column 41
    function def_refreshVariables_102 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on RangeInput (id=PaymentTypeCriterion_Input) at PaymentSearchDV.pcf: line 21, column 49
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.PaymentType = (__VALUE_TO_SET as gw.search.PaymentSearchType)
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at PaymentSearchDV.pcf: line 32, column 74
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AccountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PickerInput (id=PolicyNumberCriterion_Input) at PaymentSearchDV.pcf: line 41, column 74
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PickerInput (id=ProducerNameCriterion_Input) at PaymentSearchDV.pcf: line 50, column 89
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ProducerName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PickerInput (id=ProducerNameKanjiCriterion_Input) at PaymentSearchDV.pcf: line 59, column 168
    function defaultSetter_41 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ProducerNameKanji = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=ProducerCodeCriterion_Input) at PaymentSearchDV.pcf: line 65, column 89
    function defaultSetter_47 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ProducerCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at PaymentSearchDV.pcf: line 72, column 66
    function defaultSetter_54 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.CurrencyType = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on RangeInput (id=MethodCriterion_Input) at PaymentSearchDV.pcf: line 83, column 44
    function defaultSetter_59 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Method = (__VALUE_TO_SET as typekey.PaymentMethod)
    }
    
    // 'value' attribute on TextInput (id=CreditCardLastFourDigits_Input) at PaymentSearchDV.pcf: line 96, column 63
    function defaultSetter_66 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.CreditCardLastFourDigits_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=CheckNumberCriterion_Input) at PaymentSearchDV.pcf: line 101, column 45
    function defaultSetter_70 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.CheckNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at PaymentSearchDV.pcf: line 107, column 46
    function defaultSetter_74 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.EarliestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at PaymentSearchDV.pcf: line 113, column 44
    function defaultSetter_78 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.LatestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at PaymentSearchDV.pcf: line 120, column 43
    function defaultSetter_83 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MinAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at PaymentSearchDV.pcf: line 127, column 43
    function defaultSetter_90 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MaxAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on CheckBoxInput (id=HasSuspenseItemsCriterion_Input) at PaymentSearchDV.pcf: line 132, column 60
    function defaultSetter_96 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.OwnerOfActiveSuspenseItems = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'label' attribute on PickerInput (id=ProducerNameCriterion_Input) at PaymentSearchDV.pcf: line 50, column 89
    function label_28 () : java.lang.Object {
      return searchCriteria.getDisplayKeyForProducerNameKanji()
    }
    
    // 'onChange' attribute on PostOnChange at PaymentSearchDV.pcf: line 74, column 54
    function onChange_51 () : void {
      blankMinimumAndMaximumFields()
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentTypeCriterion_Input) at PaymentSearchDV.pcf: line 21, column 49
    function valueRange_3 () : java.lang.Object {
      return gw.search.PaymentSearchType.AllValues
    }
    
    // 'valueRange' attribute on RangeInput (id=MethodCriterion_Input) at PaymentSearchDV.pcf: line 83, column 44
    function valueRange_61 () : java.lang.Object {
      return getPaymentMethodRange()
    }
    
    // 'value' attribute on RangeInput (id=PaymentTypeCriterion_Input) at PaymentSearchDV.pcf: line 21, column 49
    function valueRoot_2 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on RangeInput (id=PaymentTypeCriterion_Input) at PaymentSearchDV.pcf: line 21, column 49
    function value_0 () : gw.search.PaymentSearchType {
      return searchCriteria.PaymentType
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at PaymentSearchDV.pcf: line 32, column 74
    function value_11 () : java.lang.String {
      return searchCriteria.AccountNumber
    }
    
    // 'value' attribute on PickerInput (id=PolicyNumberCriterion_Input) at PaymentSearchDV.pcf: line 41, column 74
    function value_20 () : java.lang.String {
      return searchCriteria.PolicyNumber
    }
    
    // 'value' attribute on PickerInput (id=ProducerNameCriterion_Input) at PaymentSearchDV.pcf: line 50, column 89
    function value_30 () : java.lang.String {
      return searchCriteria.ProducerName
    }
    
    // 'value' attribute on PickerInput (id=ProducerNameKanjiCriterion_Input) at PaymentSearchDV.pcf: line 59, column 168
    function value_40 () : java.lang.String {
      return searchCriteria.ProducerNameKanji
    }
    
    // 'value' attribute on TextInput (id=ProducerCodeCriterion_Input) at PaymentSearchDV.pcf: line 65, column 89
    function value_46 () : java.lang.String {
      return searchCriteria.ProducerCode
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at PaymentSearchDV.pcf: line 72, column 66
    function value_53 () : typekey.Currency {
      return searchCriteria.CurrencyType
    }
    
    // 'value' attribute on RangeInput (id=MethodCriterion_Input) at PaymentSearchDV.pcf: line 83, column 44
    function value_58 () : typekey.PaymentMethod {
      return searchCriteria.Method
    }
    
    // 'value' attribute on TextInput (id=CreditCardLastFourDigits_Input) at PaymentSearchDV.pcf: line 96, column 63
    function value_65 () : java.lang.String {
      return searchCriteria.CreditCardLastFourDigits_TDIC
    }
    
    // 'value' attribute on TextInput (id=CheckNumberCriterion_Input) at PaymentSearchDV.pcf: line 101, column 45
    function value_69 () : java.lang.String {
      return searchCriteria.CheckNumber
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at PaymentSearchDV.pcf: line 107, column 46
    function value_73 () : java.util.Date {
      return searchCriteria.EarliestDate
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at PaymentSearchDV.pcf: line 113, column 44
    function value_77 () : java.util.Date {
      return searchCriteria.LatestDate
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at PaymentSearchDV.pcf: line 120, column 43
    function value_82 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MinAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at PaymentSearchDV.pcf: line 127, column 43
    function value_89 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MaxAmount
    }
    
    // 'value' attribute on CheckBoxInput (id=HasSuspenseItemsCriterion_Input) at PaymentSearchDV.pcf: line 132, column 60
    function value_95 () : java.lang.Boolean {
      return searchCriteria.OwnerOfActiveSuspenseItems
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentTypeCriterion_Input) at PaymentSearchDV.pcf: line 21, column 49
    function verifyValueRangeIsAllowedType_4 ($$arg :  gw.search.PaymentSearchType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentTypeCriterion_Input) at PaymentSearchDV.pcf: line 21, column 49
    function verifyValueRangeIsAllowedType_4 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=MethodCriterion_Input) at PaymentSearchDV.pcf: line 83, column 44
    function verifyValueRangeIsAllowedType_62 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=MethodCriterion_Input) at PaymentSearchDV.pcf: line 83, column 44
    function verifyValueRangeIsAllowedType_62 ($$arg :  typekey.PaymentMethod[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentTypeCriterion_Input) at PaymentSearchDV.pcf: line 21, column 49
    function verifyValueRange_5 () : void {
      var __valueRangeArg = gw.search.PaymentSearchType.AllValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_4(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=MethodCriterion_Input) at PaymentSearchDV.pcf: line 83, column 44
    function verifyValueRange_63 () : void {
      var __valueRangeArg = getPaymentMethodRange()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_62(__valueRangeArg)
    }
    
    // 'visible' attribute on PickerInput (id=ProducerNameCriterion_Input) at PaymentSearchDV.pcf: line 50, column 89
    function visible_27 () : java.lang.Boolean {
      return searchCriteria.PaymentType == gw.search.PaymentSearchType.AGENCYBILL
    }
    
    // 'visible' attribute on PickerInput (id=ProducerNameKanjiCriterion_Input) at PaymentSearchDV.pcf: line 59, column 168
    function visible_38 () : java.lang.Boolean {
      return (searchCriteria.PaymentType == gw.search.PaymentSearchType.AGENCYBILL) && (gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP)
    }
    
    // 'visible' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at PaymentSearchDV.pcf: line 72, column 66
    function visible_52 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on PickerInput (id=AccountNumberCriterion_Input) at PaymentSearchDV.pcf: line 32, column 74
    function visible_9 () : java.lang.Boolean {
      return searchCriteria.PaymentType == DIRECTBILL_AND_SUSPENSE
    }
    
    property get searchCriteria () : gw.search.PaymentSearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.search.PaymentSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.PaymentSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    // The AgencyMoneyReceivedDV page uses the PaymentSearchCriteria class
    // as well, since the functionality is identical. Keep that in mind when
    // modifying this page and the PaymentSearchCriteria class.
    
    function getPaymentMethodRange() : java.util.List<PaymentMethod> {
      var paymentMethodRange = new java.util.ArrayList<PaymentMethod>();
      //US123, 01/05/2015 Vicente, Wire and Credit Card removed
      paymentMethodRange.add( PaymentMethod.TC_ACH );
      paymentMethodRange.add( PaymentMethod.TC_CHECK );
      paymentMethodRange.add( PaymentMethod.TC_CREDITCARD );
      return paymentMethodRange;
    }
    
    function blankMinimumAndMaximumFields() {
      searchCriteria.MinAmount = null
      searchCriteria.MaxAmount = null
    }
    
    
  }
  
  
}