package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillPromises.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillPromisesExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillPromises.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillPromisesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'location' attribute on LocationGroup (id=AgencyBillPromises) at AgencyBillPromises.pcf: line 14, column 56
    function action_0 () : void {
      pcf.AgencyBillExecutedPromises.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=AgencyBillPromises) at AgencyBillPromises.pcf: line 16, column 53
    function action_2 () : void {
      pcf.AgencyBillSavedPromises.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=AgencyBillPromises) at AgencyBillPromises.pcf: line 14, column 56
    function action_dest_1 () : pcf.api.Destination {
      return pcf.AgencyBillExecutedPromises.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=AgencyBillPromises) at AgencyBillPromises.pcf: line 16, column 53
    function action_dest_3 () : pcf.api.Destination {
      return pcf.AgencyBillSavedPromises.createDestination(producer)
    }
    
    // LocationGroup (id=AgencyBillPromises) at AgencyBillPromises.pcf: line 7, column 34
    static function firstVisitableChildDestinationMethod_7 (producer :  Producer) : pcf.api.Destination {
      var dest : pcf.api.Destination
      dest = pcf.AgencyBillExecutedPromises.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AgencyBillSavedPromises.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      return null
    }
    
    // LocationGroup (id=AgencyBillPromises) at AgencyBillPromises.pcf: line 7, column 34
    static function parent_4 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    // 'tabBar' attribute on LocationGroup (id=AgencyBillPromises) at AgencyBillPromises.pcf: line 7, column 34
    function tabBar_onEnter_5 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on LocationGroup (id=AgencyBillPromises) at AgencyBillPromises.pcf: line 7, column 34
    function tabBar_refreshVariables_6 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AgencyBillPromises {
      return super.CurrentLocation as pcf.AgencyBillPromises
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}