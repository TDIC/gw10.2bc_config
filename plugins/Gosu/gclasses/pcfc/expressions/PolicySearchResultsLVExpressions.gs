package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/PolicySearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicySearchResultsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/PolicySearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicySearchResultsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at PolicySearchResultsLV.pcf: line 55, column 25
    function action_12 () : void {
      PolicySummary.go(policySearchView.PolicyPeriod)
    }
    
    // 'action' attribute on Link (id=Select) at PolicySearchResultsLV.pcf: line 44, column 38
    function action_9 () : void {
      tAccountOwnerReference.TAccountOwner = policySearchView.PolicyPeriod; (CurrentLocation as pcf.api.Wizard).next()
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at PolicySearchResultsLV.pcf: line 55, column 25
    function action_dest_13 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(policySearchView.PolicyPeriod)
    }
    
    // 'available' attribute on TextCell (id=PolicyNumber_Cell) at PolicySearchResultsLV.pcf: line 55, column 25
    function available_11 () : java.lang.Boolean {
      return showHyperlinks
    }
    
    // 'canPick' attribute on RowIterator at PolicySearchResultsLV.pcf: line 34, column 83
    function canPick_48 () : java.lang.Boolean {
      return !isWizard
    }
    
    // 'checkBoxVisible' attribute on RowIterator at PolicySearchResultsLV.pcf: line 34, column 83
    function checkBoxVisible_49 () : java.lang.Boolean {
      return showCheckboxes
    }
    
    // 'pickValue' attribute on RowIterator at PolicySearchResultsLV.pcf: line 34, column 83
    function pickValue_50 () : java.lang.Object {
      return gw.api.web.search.SearchPopupUtil.getPolicyPeriod(policySearchView)
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at PolicySearchResultsLV.pcf: line 55, column 25
    function valueRoot_15 () : java.lang.Object {
      return policySearchView
    }
    
    // 'value' attribute on TextCell (id=AccountName_Cell) at PolicySearchResultsLV.pcf: line 70, column 57
    function valueRoot_25 () : java.lang.Object {
      return policySearchView.Account
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at PolicySearchResultsLV.pcf: line 76, column 45
    function valueRoot_28 () : java.lang.Object {
      return policySearchView.Policy.LatestPolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at PolicySearchResultsLV.pcf: line 55, column 25
    function value_14 () : java.lang.String {
      return policySearchView.PolicyNumberLong
    }
    
    // 'value' attribute on TypeKeyCell (id=Currency_Cell) at PolicySearchResultsLV.pcf: line 60, column 41
    function value_18 () : typekey.Currency {
      return policySearchView.Currency
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at PolicySearchResultsLV.pcf: line 66, column 25
    function value_21 () : java.lang.String {
      return policySearchView.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=AccountName_Cell) at PolicySearchResultsLV.pcf: line 70, column 57
    function value_24 () : java.lang.String {
      return policySearchView.Account.AccountName
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at PolicySearchResultsLV.pcf: line 76, column 45
    function value_27 () : typekey.Jurisdiction {
      return policySearchView.Policy.LatestPolicyPeriod.RiskJurisdiction
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at PolicySearchResultsLV.pcf: line 81, column 52
    function value_30 () : typekey.PolicyClosureStatus {
      return policySearchView.ClosureStatus
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at PolicySearchResultsLV.pcf: line 87, column 25
    function value_33 () : typekey.LOBCode {
      return policySearchView.LOBCode
    }
    
    // 'value' attribute on DateCell (id=PolicyPerEffDate_Cell) at PolicySearchResultsLV.pcf: line 91, column 54
    function value_36 () : java.util.Date {
      return policySearchView.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=PolicyPerExpirDate_Cell) at PolicySearchResultsLV.pcf: line 95, column 56
    function value_39 () : java.util.Date {
      return policySearchView.PolicyPerExpirDate
    }
    
    // 'value' attribute on TextCell (id=Producer_Cell) at PolicySearchResultsLV.pcf: line 101, column 25
    function value_42 () : java.lang.String {
      return policySearchView.PrimaryCommissionProducer
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at PolicySearchResultsLV.pcf: line 107, column 25
    function value_45 () : java.lang.String {
      return policySearchView.PrimaryCommissionProducerCode
    }
    
    // 'visible' attribute on LinkCell (id=SelectCell) at PolicySearchResultsLV.pcf: line 39, column 50
    function visible_10 () : java.lang.Boolean {
      return isWizard and !showCheckboxes
    }
    
    property get policySearchView () : entity.PolicySearchView {
      return getIteratedValue(1) as entity.PolicySearchView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/PolicySearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicySearchResultsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on TextCell (id=PolicyNumber_Cell) at PolicySearchResultsLV.pcf: line 55, column 25
    function sortValue_1 (policySearchView :  entity.PolicySearchView) : java.lang.Object {
      return policySearchView.PolicyNumberLong
    }
    
    // 'value' attribute on TypeKeyCell (id=Currency_Cell) at PolicySearchResultsLV.pcf: line 60, column 41
    function sortValue_2 (policySearchView :  entity.PolicySearchView) : java.lang.Object {
      return policySearchView.Currency
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at PolicySearchResultsLV.pcf: line 66, column 25
    function sortValue_3 (policySearchView :  entity.PolicySearchView) : java.lang.Object {
      return policySearchView.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=AccountName_Cell) at PolicySearchResultsLV.pcf: line 70, column 57
    function sortValue_4 (policySearchView :  entity.PolicySearchView) : java.lang.Object {
      return policySearchView.Account.AccountName
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at PolicySearchResultsLV.pcf: line 81, column 52
    function sortValue_5 (policySearchView :  entity.PolicySearchView) : java.lang.Object {
      return policySearchView.ClosureStatus
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at PolicySearchResultsLV.pcf: line 87, column 25
    function sortValue_6 (policySearchView :  entity.PolicySearchView) : java.lang.Object {
      return policySearchView.LOBCode
    }
    
    // 'value' attribute on DateCell (id=PolicyPerEffDate_Cell) at PolicySearchResultsLV.pcf: line 91, column 54
    function sortValue_7 (policySearchView :  entity.PolicySearchView) : java.lang.Object {
      return policySearchView.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=PolicyPerExpirDate_Cell) at PolicySearchResultsLV.pcf: line 95, column 56
    function sortValue_8 (policySearchView :  entity.PolicySearchView) : java.lang.Object {
      return policySearchView.PolicyPerExpirDate
    }
    
    // 'value' attribute on RowIterator at PolicySearchResultsLV.pcf: line 34, column 83
    function value_51 () : gw.api.database.IQueryBeanResult<entity.PolicySearchView> {
      return policySearchViews
    }
    
    // 'visible' attribute on LinkCell (id=SelectCell) at PolicySearchResultsLV.pcf: line 39, column 50
    function visible_0 () : java.lang.Boolean {
      return isWizard and !showCheckboxes
    }
    
    property get isWizard () : boolean {
      return getRequireValue("isWizard", 0) as java.lang.Boolean
    }
    
    property set isWizard ($arg :  boolean) {
      setRequireValue("isWizard", 0, $arg)
    }
    
    property get policySearchViews () : gw.api.database.IQueryBeanResult<PolicySearchView> {
      return getRequireValue("policySearchViews", 0) as gw.api.database.IQueryBeanResult<PolicySearchView>
    }
    
    property set policySearchViews ($arg :  gw.api.database.IQueryBeanResult<PolicySearchView>) {
      setRequireValue("policySearchViews", 0, $arg)
    }
    
    property get showCheckboxes () : Boolean {
      return getRequireValue("showCheckboxes", 0) as Boolean
    }
    
    property set showCheckboxes ($arg :  Boolean) {
      setRequireValue("showCheckboxes", 0, $arg)
    }
    
    property get showHyperlinks () : boolean {
      return getRequireValue("showHyperlinks", 0) as java.lang.Boolean
    }
    
    property set showHyperlinks ($arg :  boolean) {
      setRequireValue("showHyperlinks", 0, $arg)
    }
    
    property get tAccountOwnerReference () : gw.api.web.accounting.TAccountOwnerReference {
      return getRequireValue("tAccountOwnerReference", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set tAccountOwnerReference ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setRequireValue("tAccountOwnerReference", 0, $arg)
    }
    
    
  }
  
  
}