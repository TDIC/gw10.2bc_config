package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/SuspenseCreateDisbursementWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SuspenseCreateDisbursementWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/SuspenseCreateDisbursementWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SuspenseCreateDisbursementWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (suspensePayment :  SuspensePayment) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Wizard (id=SuspenseCreateDisbursementWizard) at SuspenseCreateDisbursementWizard.pcf: line 10, column 43
    function beforeCommit_6 (pickedValue :  java.lang.Object) : void {
      disbursement.executeDisbursementOrCreateApprovalActivityIfNeeded()
    }
    
    // 'canVisit' attribute on Wizard (id=SuspenseCreateDisbursementWizard) at SuspenseCreateDisbursementWizard.pcf: line 10, column 43
    static function canVisit_7 (suspensePayment :  SuspensePayment) : java.lang.Boolean {
      return perm.Transaction.disbcreate
    }
    
    // 'initialValue' attribute on Variable at SuspenseCreateDisbursementWizard.pcf: line 16, column 36
    function initialValue_0 () : SuspenseDisbursement {
      return newDisbursement()
    }
    
    // 'onExit' attribute on WizardStep (id=Step1) at SuspenseCreateDisbursementWizard.pcf: line 25, column 97
    function onExit_1 () : void {
      disbursement.initiateApprovalActivityIfUserLacksAuthority()
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at SuspenseCreateDisbursementWizard.pcf: line 25, column 97
    function screen_onEnter_2 (def :  pcf.CreateSuspenseDisbursementDetailScreen) : void {
      def.onEnter(disbursement)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at SuspenseCreateDisbursementWizard.pcf: line 30, column 97
    function screen_onEnter_4 (def :  pcf.CreateDisbursementConfirmScreen) : void {
      def.onEnter(disbursement)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at SuspenseCreateDisbursementWizard.pcf: line 25, column 97
    function screen_refreshVariables_3 (def :  pcf.CreateSuspenseDisbursementDetailScreen) : void {
      def.refreshVariables(disbursement)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at SuspenseCreateDisbursementWizard.pcf: line 30, column 97
    function screen_refreshVariables_5 (def :  pcf.CreateDisbursementConfirmScreen) : void {
      def.refreshVariables(disbursement)
    }
    
    // 'tabBar' attribute on Wizard (id=SuspenseCreateDisbursementWizard) at SuspenseCreateDisbursementWizard.pcf: line 10, column 43
    function tabBar_onEnter_8 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=SuspenseCreateDisbursementWizard) at SuspenseCreateDisbursementWizard.pcf: line 10, column 43
    function tabBar_refreshVariables_9 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.SuspenseCreateDisbursementWizard {
      return super.CurrentLocation as pcf.SuspenseCreateDisbursementWizard
    }
    
    property get disbursement () : SuspenseDisbursement {
      return getVariableValue("disbursement", 0) as SuspenseDisbursement
    }
    
    property set disbursement ($arg :  SuspenseDisbursement) {
      setVariableValue("disbursement", 0, $arg)
    }
    
    property get suspensePayment () : SuspensePayment {
      return getVariableValue("suspensePayment", 0) as SuspensePayment
    }
    
    property set suspensePayment ($arg :  SuspensePayment) {
      setVariableValue("suspensePayment", 0, $arg)
    }
    
    function newDisbursement() : SuspenseDisbursement{
      var disb = new SuspenseDisbursement(suspensePayment.Currency)
      disb.setSuspensePaymentAndFields(suspensePayment)
      return disb;
    }
    
    
  }
  
  
}