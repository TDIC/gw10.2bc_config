package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffWizardAccountSelectStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewNegativeWriteoffWizardAccountSelectStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffWizardAccountSelectStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewNegativeWriteoffWizardAccountSelectStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get tAccountOwnerReference () : gw.api.web.accounting.TAccountOwnerReference {
      return getRequireValue("tAccountOwnerReference", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set tAccountOwnerReference ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setRequireValue("tAccountOwnerReference", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffWizardAccountSelectStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends NewNegativeWriteoffWizardAccountSelectStepScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffWizardAccountSelectStepScreen.pcf: line 18, column 47
    function def_onEnter_0 (def :  pcf.AccountSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffWizardAccountSelectStepScreen.pcf: line 24, column 102
    function def_onEnter_2 (def :  pcf.AccountSearchResultsLV) : void {
      def.onEnter(accountSearchViews, tAccountOwnerReference, true, false, false)
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffWizardAccountSelectStepScreen.pcf: line 18, column 47
    function def_refreshVariables_1 (def :  pcf.AccountSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffWizardAccountSelectStepScreen.pcf: line 24, column 102
    function def_refreshVariables_3 (def :  pcf.AccountSearchResultsLV) : void {
      def.refreshVariables(accountSearchViews, tAccountOwnerReference, true, false, false)
    }
    
    // 'searchCriteria' attribute on SearchPanel at NewNegativeWriteoffWizardAccountSelectStepScreen.pcf: line 16, column 85
    function searchCriteria_5 () : gw.search.AccountSearchCriteria {
      return new gw.search.AccountSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at NewNegativeWriteoffWizardAccountSelectStepScreen.pcf: line 16, column 85
    function search_4 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria, StringCriterionMode.TC_STARTSWITH)
    }
    
    property get accountSearchViews () : gw.api.database.IQueryBeanResult<AccountSearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<AccountSearchView>
    }
    
    property get searchCriteria () : gw.search.AccountSearchCriteria {
      return getCriteriaValue(1) as gw.search.AccountSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.AccountSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}