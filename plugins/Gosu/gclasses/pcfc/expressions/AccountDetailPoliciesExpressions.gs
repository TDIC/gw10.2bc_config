package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailPolicies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailPoliciesExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailPolicies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailPoliciesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=AccountDetailPolicies) at AccountDetailPolicies.pcf: line 9, column 73
    static function canVisit_20 (account :  Account) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctplcyview
    }
    
    // Page (id=AccountDetailPolicies) at AccountDetailPolicies.pcf: line 9, column 73
    static function parent_21 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.AccountDetailPolicies {
      return super.CurrentLocation as pcf.AccountDetailPolicies
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailPolicies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailPoliciesScreenExpressionsImpl extends AccountDetailPoliciesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=TotalValue_Cell) at AccountDetailPolicies.pcf: line 55, column 69
    function currency_6 () : typekey.Currency {
      return account.Currency
    }
    
    // 'initialValue' attribute on Variable at AccountDetailPolicies.pcf: line 20, column 24
    function initialValue_0 () : String {
      return null
    }
    
    // 'value' attribute on PanelIterator (id=OwnerPolicies) at AccountDetailPolicies.pcf: line 31, column 41
    function value_4 () : entity.Policy[] {
      return account.Policies.where(\ policy -> policy.PolicyPeriods.length > 0)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalValue_Cell) at AccountDetailPolicies.pcf: line 55, column 69
    function value_5 () : gw.pl.currency.MonetaryAmount {
      return account.TotalValue - account.RawTotalValue
    }
    
    // 'visible' attribute on Card (id=PayerTab) at AccountDetailPolicies.pcf: line 62, column 131
    function visible_19 () : java.lang.Boolean {
      return !gw.api.web.account.Policies.findPoliciesWhereAccountIsOverridingPayerOnAtLeastOnePeriod(account, null).Empty
    }
    
    // 'visible' attribute on Card (id=OwnedTab) at AccountDetailPolicies.pcf: line 26, column 104
    function visible_8 () : java.lang.Boolean {
      return !account.Policies.where(\ policy -> policy.PolicyPeriods.length > 0).IsEmpty
    }
    
    property get policyStartsWith () : String {
      return getVariableValue("policyStartsWith", 1) as String
    }
    
    property set policyStartsWith ($arg :  String) {
      setVariableValue("policyStartsWith", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailPolicies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelIteratorEntryExpressionsImpl extends AccountDetailPoliciesScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef (id=OwnedPolicyPeriods) at AccountDetailPolicies.pcf: line 34, column 39
    function def_onEnter_2 (def :  pcf.PolicyPeriodPanelLV) : void {
      def.onEnter(policy.PolicyPeriods.toList())
    }
    
    // 'def' attribute on PanelRef (id=OwnedPolicyPeriods) at AccountDetailPolicies.pcf: line 34, column 39
    function def_refreshVariables_3 (def :  pcf.PolicyPeriodPanelLV) : void {
      def.refreshVariables(policy.PolicyPeriods.toList())
    }
    
    // 'title' attribute on TitleBar at AccountDetailPolicies.pcf: line 36, column 46
    function title_1 () : java.lang.String {
      return policy.NumberAndLOB
    }
    
    property get policy () : entity.Policy {
      return getIteratedValue(2) as entity.Policy
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailPolicies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PayerPoliciesExpressionsImpl extends AccountDetailPoliciesScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=PolicyFilterGoButton) at AccountDetailPolicies.pcf: line 84, column 118
    function action_13 () : void {
      policies = gw.api.web.account.Policies.findPoliciesWhereAccountIsOverridingPayerOnAtLeastOnePeriod(account, policyStartsWith)
    }
    
    // 'action' attribute on ToolbarButton (id=PolicyFilterClearButton) at AccountDetailPolicies.pcf: line 88, column 121
    function action_14 () : void {
      policyStartsWith = null; policies= gw.api.web.account.Policies.findPoliciesWhereAccountIsOverridingPayerOnAtLeastOnePeriod(account, policyStartsWith)
    }
    
    // 'def' attribute on PanelRef at AccountDetailPolicies.pcf: line 72, column 42
    function def_onEnter_15 (def :  pcf.PoliciesLV) : void {
      def.onEnter(policies)
    }
    
    // 'def' attribute on PanelRef (id=PayerPolicyPeriods) at AccountDetailPolicies.pcf: line 97, column 43
    function def_onEnter_17 (def :  pcf.PolicyPeriodPanelLV) : void {
      def.onEnter(gw.api.web.account.Policies.findPolicyPeriodsFromPolicyWhereAccountIsOverridingPayer(account, policy, policyStartsWith).toList())
    }
    
    // 'def' attribute on PanelRef at AccountDetailPolicies.pcf: line 72, column 42
    function def_refreshVariables_16 (def :  pcf.PoliciesLV) : void {
      def.refreshVariables(policies)
    }
    
    // 'def' attribute on PanelRef (id=PayerPolicyPeriods) at AccountDetailPolicies.pcf: line 97, column 43
    function def_refreshVariables_18 (def :  pcf.PolicyPeriodPanelLV) : void {
      def.refreshVariables(gw.api.web.account.Policies.findPolicyPeriodsFromPolicyWhereAccountIsOverridingPayer(account, policy, policyStartsWith).toList())
    }
    
    // 'value' attribute on TextValue (id=PolicyPeriodFilter) at AccountDetailPolicies.pcf: line 79, column 49
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyStartsWith = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at AccountDetailPolicies.pcf: line 70, column 70
    function initialValue_9 () : gw.api.database.IQueryBeanResult<Policy> {
      return gw.api.web.account.Policies.findPoliciesWhereAccountIsOverridingPayerOnAtLeastOnePeriod(account, policyStartsWith)
    }
    
    // 'value' attribute on TextValue (id=PolicyPeriodFilter) at AccountDetailPolicies.pcf: line 79, column 49
    function value_10 () : java.lang.String {
      return policyStartsWith
    }
    
    property get policies () : gw.api.database.IQueryBeanResult<Policy> {
      return getVariableValue("policies", 2) as gw.api.database.IQueryBeanResult<Policy>
    }
    
    property set policies ($arg :  gw.api.database.IQueryBeanResult<Policy>) {
      setVariableValue("policies", 2, $arg)
    }
    
    property get policy () : Policy {
      return getCurrentSelection(2) as Policy
    }
    
    property set policy ($arg :  Policy) {
      setCurrentSelection(2, $arg)
    }
    
    
  }
  
  
}