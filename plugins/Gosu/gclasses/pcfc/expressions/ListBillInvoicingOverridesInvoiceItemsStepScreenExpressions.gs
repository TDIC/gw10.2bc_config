package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/ListBillInvoicingOverridesInvoiceItemsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ListBillInvoicingOverridesInvoiceItemsStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/ListBillInvoicingOverridesInvoiceItemsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListBillInvoicingOverridesInvoiceItemsStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on BooleanRadioInput (id=PaymentPlanChangeOption_Input) at ListBillInvoicingOverridesInvoiceItemsStepScreen.pcf: line 25, column 59
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      listBillHelper.RefactorAllInvoiceItems = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=reversePayments_Input) at ListBillInvoicingOverridesInvoiceItemsStepScreen.pcf: line 42, column 120
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      listBillHelper.ReversePaymentsWhenMovingInvoiceItems = (__VALUE_TO_SET as gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Redistribute_Input) at ListBillInvoicingOverridesInvoiceItemsStepScreen.pcf: line 33, column 120
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      listBillHelper.RedistributePaymentsWhenChangingPaymentPlan = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'valueRange' attribute on RangeInput (id=reversePayments_Input) at ListBillInvoicingOverridesInvoiceItemsStepScreen.pcf: line 42, column 120
    function valueRange_16 () : java.lang.Object {
      return gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem.values()
    }
    
    // 'value' attribute on BooleanRadioInput (id=PaymentPlanChangeOption_Input) at ListBillInvoicingOverridesInvoiceItemsStepScreen.pcf: line 25, column 59
    function valueRoot_2 () : java.lang.Object {
      return listBillHelper
    }
    
    // 'value' attribute on BooleanRadioInput (id=PaymentPlanChangeOption_Input) at ListBillInvoicingOverridesInvoiceItemsStepScreen.pcf: line 25, column 59
    function value_0 () : java.lang.Boolean {
      return listBillHelper.RefactorAllInvoiceItems
    }
    
    // 'value' attribute on RangeInput (id=reversePayments_Input) at ListBillInvoicingOverridesInvoiceItemsStepScreen.pcf: line 42, column 120
    function value_13 () : gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem {
      return listBillHelper.ReversePaymentsWhenMovingInvoiceItems
    }
    
    // 'value' attribute on BooleanRadioInput (id=Redistribute_Input) at ListBillInvoicingOverridesInvoiceItemsStepScreen.pcf: line 33, column 120
    function value_6 () : java.lang.Boolean {
      return listBillHelper.RedistributePaymentsWhenChangingPaymentPlan
    }
    
    // 'valueRange' attribute on RangeInput (id=reversePayments_Input) at ListBillInvoicingOverridesInvoiceItemsStepScreen.pcf: line 42, column 120
    function verifyValueRangeIsAllowedType_17 ($$arg :  gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=reversePayments_Input) at ListBillInvoicingOverridesInvoiceItemsStepScreen.pcf: line 42, column 120
    function verifyValueRangeIsAllowedType_17 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=reversePayments_Input) at ListBillInvoicingOverridesInvoiceItemsStepScreen.pcf: line 42, column 120
    function verifyValueRange_18 () : void {
      var __valueRangeArg = gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem.values()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_17(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=reversePayments_Input) at ListBillInvoicingOverridesInvoiceItemsStepScreen.pcf: line 42, column 120
    function visible_11 () : java.lang.Boolean {
      return policyPeriod.isPaymentHasBeenApplied() and policyPeriod.PaymentPlan == listBillHelper.PaymentPlan
    }
    
    // 'visible' attribute on BooleanRadioInput (id=Redistribute_Input) at ListBillInvoicingOverridesInvoiceItemsStepScreen.pcf: line 33, column 120
    function visible_4 () : java.lang.Boolean {
      return policyPeriod.isPaymentHasBeenApplied() and policyPeriod.PaymentPlan != listBillHelper.PaymentPlan
    }
    
    property get listBillHelper () : gw.api.web.policy.ListBillInvoicingOverridesHelper {
      return getRequireValue("listBillHelper", 0) as gw.api.web.policy.ListBillInvoicingOverridesHelper
    }
    
    property set listBillHelper ($arg :  gw.api.web.policy.ListBillInvoicingOverridesHelper) {
      setRequireValue("listBillHelper", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    function clearOverridesOnListBillPayerChange() {
      if(!listBillHelper.ListBillAccount.PaymentPlans.contains(listBillHelper.PaymentPlan)){
        listBillHelper.PaymentPlan = null
      }
       listBillHelper.InvoiceStream = null
    }
    
    
  }
  
  
}