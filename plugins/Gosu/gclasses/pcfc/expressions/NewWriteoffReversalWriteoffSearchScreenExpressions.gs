package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalWriteoffSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewWriteoffReversalWriteoffSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalWriteoffSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewWriteoffReversalWriteoffSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at NewWriteoffReversalWriteoffSearchScreen.pcf: line 16, column 23
    function initialValue_0 () : Boolean {
      return account == null
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get accountEditable () : Boolean {
      return getVariableValue("accountEditable", 0) as Boolean
    }
    
    property set accountEditable ($arg :  Boolean) {
      setVariableValue("accountEditable", 0, $arg)
    }
    
    property get reversal () : WriteoffReversal {
      return getRequireValue("reversal", 0) as WriteoffReversal
    }
    
    property set reversal ($arg :  WriteoffReversal) {
      setRequireValue("reversal", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalWriteoffSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends NewWriteoffReversalWriteoffSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffReversalWriteoffSearchScreen.pcf: line 32, column 69
    function def_onEnter_1 (def :  pcf.WriteoffSearchDV) : void {
      def.onEnter(searchCriteria, accountEditable)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffReversalWriteoffSearchScreen.pcf: line 36, column 72
    function def_onEnter_3 (def :  pcf.NewWriteoffReversalWriteoffsLV) : void {
      def.onEnter(reversal, writeOffs)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffReversalWriteoffSearchScreen.pcf: line 32, column 69
    function def_refreshVariables_2 (def :  pcf.WriteoffSearchDV) : void {
      def.refreshVariables(searchCriteria, accountEditable)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffReversalWriteoffSearchScreen.pcf: line 36, column 72
    function def_refreshVariables_4 (def :  pcf.NewWriteoffReversalWriteoffsLV) : void {
      def.refreshVariables(reversal, writeOffs)
    }
    
    // 'searchCriteria' attribute on SearchPanel at NewWriteoffReversalWriteoffSearchScreen.pcf: line 30, column 80
    function searchCriteria_6 () : gw.search.WriteoffSearchCriteria {
      return new gw.search.WriteoffSearchCriteria(account)
    }
    
    // 'search' attribute on SearchPanel at NewWriteoffReversalWriteoffSearchScreen.pcf: line 30, column 80
    function search_5 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria)
    }
    
    property get searchCriteria () : gw.search.WriteoffSearchCriteria {
      return getCriteriaValue(1) as gw.search.WriteoffSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.WriteoffSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    property get writeOffs () : gw.api.database.IQueryBeanResult<Writeoff> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<Writeoff>
    }
    
    
  }
  
  
}