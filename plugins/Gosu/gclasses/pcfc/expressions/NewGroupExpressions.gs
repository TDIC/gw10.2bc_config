package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/groups/NewGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewGroupExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/groups/NewGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewGroupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=NewGroup) at NewGroup.pcf: line 13, column 60
    function afterCancel_6 () : void {
      Groups.go()
    }
    
    // 'afterCancel' attribute on Page (id=NewGroup) at NewGroup.pcf: line 13, column 60
    function afterCancel_dest_7 () : pcf.api.Destination {
      return pcf.Groups.createDestination()
    }
    
    // 'afterCommit' attribute on Page (id=NewGroup) at NewGroup.pcf: line 13, column 60
    function afterCommit_8 (TopLocation :  pcf.api.Location) : void {
      GroupDetailPage.go(Group)
    }
    
    // 'canVisit' attribute on Page (id=NewGroup) at NewGroup.pcf: line 13, column 60
    static function canVisit_9 () : java.lang.Boolean {
      return perm.Group.create and perm.System.useradmin
    }
    
    // 'def' attribute on PanelRef at NewGroup.pcf: line 26, column 37
    function def_onEnter_2 (def :  pcf.GroupDetailDV) : void {
      def.onEnter(Group)
    }
    
    // 'def' attribute on PanelRef at NewGroup.pcf: line 28, column 272
    function def_onEnter_4 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(Group, { "Name" }, { (gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP) ? DisplayKey.get("Web.Admin.GroupDetail.BasicDV.NamePhonetic") : DisplayKey.get("Web.Admin.GroupDetail.BasicDV.Name") })
    }
    
    // 'def' attribute on PanelRef at NewGroup.pcf: line 26, column 37
    function def_refreshVariables_3 (def :  pcf.GroupDetailDV) : void {
      def.refreshVariables(Group)
    }
    
    // 'def' attribute on PanelRef at NewGroup.pcf: line 28, column 272
    function def_refreshVariables_5 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(Group, { "Name" }, { (gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP) ? DisplayKey.get("Web.Admin.GroupDetail.BasicDV.NamePhonetic") : DisplayKey.get("Web.Admin.GroupDetail.BasicDV.Name") })
    }
    
    // 'initialValue' attribute on Variable at NewGroup.pcf: line 19, column 21
    function initialValue_0 () : Group {
      return gw.api.web.admin.BCGroupUtil.createGroup()
    }
    
    // EditButtons at NewGroup.pcf: line 23, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'parent' attribute on Page (id=NewGroup) at NewGroup.pcf: line 13, column 60
    static function parent_10 () : pcf.api.Destination {
      return pcf.Admin.createDestination()
    }
    
    override property get CurrentLocation () : pcf.NewGroup {
      return super.CurrentLocation as pcf.NewGroup
    }
    
    property get Group () : Group {
      return getVariableValue("Group", 0) as Group
    }
    
    property set Group ($arg :  Group) {
      setVariableValue("Group", 0, $arg)
    }
    
    
  }
  
  
}