package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/PaymentInstrumentInputSet.creditcard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentInstrumentInputSet_creditcardExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/PaymentInstrumentInputSet.creditcard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentInstrumentInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get paymentInstrument () : PaymentInstrument {
      return getRequireValue("paymentInstrument", 0) as PaymentInstrument
    }
    
    property set paymentInstrument ($arg :  PaymentInstrument) {
      setRequireValue("paymentInstrument", 0, $arg)
    }
    
    
  }
  
  
}