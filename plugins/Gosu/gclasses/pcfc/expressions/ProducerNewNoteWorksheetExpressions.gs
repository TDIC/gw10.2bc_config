package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerNewNoteWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerNewNoteWorksheetExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerNewNoteWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerNewNoteWorksheetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'action' attribute on PickerToolbarButton (id=NewNoteWorksheet_UseTemplateButton) at ProducerNewNoteWorksheet.pcf: line 37, column 69
    function action_3 () : void {
      PickNoteTemplatePopup.push(createSearchCriteria())
    }
    
    // 'action' attribute on PickerToolbarButton (id=linkDocsPickerButton) at ProducerNewNoteWorksheet.pcf: line 42, column 88
    function action_6 () : void {
      PickExistingDocumentPopup.push(producer)
    }
    
    // 'action' attribute on PickerToolbarButton (id=NewNoteWorksheet_UseTemplateButton) at ProducerNewNoteWorksheet.pcf: line 37, column 69
    function action_dest_4 () : pcf.api.Destination {
      return pcf.PickNoteTemplatePopup.createDestination(createSearchCriteria())
    }
    
    // 'action' attribute on PickerToolbarButton (id=linkDocsPickerButton) at ProducerNewNoteWorksheet.pcf: line 42, column 88
    function action_dest_7 () : pcf.api.Destination {
      return pcf.PickExistingDocumentPopup.createDestination(producer)
    }
    
    // 'beforeCommit' attribute on Worksheet (id=ProducerNewNoteWorksheet) at ProducerNewNoteWorksheet.pcf: line 13, column 67
    function beforeCommit_11 (pickedValue :  java.lang.Object) : void {
      producer.addNote(newNote)
    }
    
    // 'canVisit' attribute on Worksheet (id=ProducerNewNoteWorksheet) at ProducerNewNoteWorksheet.pcf: line 13, column 67
    static function canVisit_12 (producer :  Producer) : java.lang.Boolean {
      return perm.Note.create
    }
    
    // 'def' attribute on PanelRef at ProducerNewNoteWorksheet.pcf: line 45, column 35
    function def_onEnter_9 (def :  pcf.NewNoteDV) : void {
      def.onEnter(newNote)
    }
    
    // 'def' attribute on PanelRef at ProducerNewNoteWorksheet.pcf: line 45, column 35
    function def_refreshVariables_10 (def :  pcf.NewNoteDV) : void {
      def.refreshVariables(newNote)
    }
    
    // 'initialValue' attribute on Variable at ProducerNewNoteWorksheet.pcf: line 24, column 20
    function initialValue_0 () : Note {
      return gw.pcf.note.NoteHelper.createNoteWithCurrentUsersLanguage()
    }
    
    // 'initialValue' attribute on Variable at ProducerNewNoteWorksheet.pcf: line 28, column 71
    function initialValue_1 () : java.util.Map<java.lang.String, java.lang.Object> {
      return gw.api.util.SymbolTableUtil.populateBeans( producer )
    }
    
    // EditButtons at ProducerNewNoteWorksheet.pcf: line 32, column 23
    function label_2 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=NewNoteWorksheet_UseTemplateButton) at ProducerNewNoteWorksheet.pcf: line 37, column 69
    function onPick_5 (PickedValue :  NoteTemplateSearchResults) : void {
      newNote.useTemplate( PickedValue, symbolTable )
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=linkDocsPickerButton) at ProducerNewNoteWorksheet.pcf: line 42, column 88
    function onPick_8 (PickedValue :  Document) : void {
      acc.onbase.util.NotesUtil.linkDocumentToNote(PickedValue, newNote)
    }
    
    // 'parent' attribute on Worksheet (id=ProducerNewNoteWorksheet) at ProducerNewNoteWorksheet.pcf: line 13, column 67
    static function parent_13 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    override property get CurrentLocation () : pcf.ProducerNewNoteWorksheet {
      return super.CurrentLocation as pcf.ProducerNewNoteWorksheet
    }
    
    property get newNote () : Note {
      return getVariableValue("newNote", 0) as Note
    }
    
    property set newNote ($arg :  Note) {
      setVariableValue("newNote", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    property get symbolTable () : java.util.Map<java.lang.String, java.lang.Object> {
      return getVariableValue("symbolTable", 0) as java.util.Map<java.lang.String, java.lang.Object>
    }
    
    property set symbolTable ($arg :  java.util.Map<java.lang.String, java.lang.Object>) {
      setVariableValue("symbolTable", 0, $arg)
    }
    
    function createSearchCriteria() : NoteTemplateSearchCriteria {
      var rtn = new NoteTemplateSearchCriteria()
      // rtn.Language = Account.AccountHolder.Language 
      rtn.AvailableSymbols = symbolTable.Keys.join( "," )
      return rtn  
    }
    
    
  }
  
  
}