package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadQueueLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadQueueLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadQueueLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadQueueLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadQueueLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at AdminDataUploadQueueLV.pcf: line 51, column 41
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Queue.Description")
    }
    
    // 'label' attribute on BooleanRadioCell (id=subGroupVisble_Cell) at AdminDataUploadQueueLV.pcf: line 56, column 42
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Queue.SubGroupVisible")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadQueueLV.pcf: line 61, column 42
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'label' attribute on TextCell (id=displayName_Cell) at AdminDataUploadQueueLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Queue.DisplayName")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadQueueLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Queue.PublicID")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at AdminDataUploadQueueLV.pcf: line 41, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Queue.Name")
    }
    
    // 'label' attribute on TextCell (id=group_Cell) at AdminDataUploadQueueLV.pcf: line 46, column 41
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Queue.Group")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadQueueLV.pcf: line 23, column 46
    function sortValue_1 (queue :  tdic.util.dataloader.data.admindata.QueueData) : java.lang.Object {
      return processor.getLoadStatus(queue)
    }
    
    // 'value' attribute on TextCell (id=group_Cell) at AdminDataUploadQueueLV.pcf: line 46, column 41
    function sortValue_10 (queue :  tdic.util.dataloader.data.admindata.QueueData) : java.lang.Object {
      return queue.Group.Name
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at AdminDataUploadQueueLV.pcf: line 51, column 41
    function sortValue_12 (queue :  tdic.util.dataloader.data.admindata.QueueData) : java.lang.Object {
      return queue.Description
    }
    
    // 'value' attribute on BooleanRadioCell (id=subGroupVisble_Cell) at AdminDataUploadQueueLV.pcf: line 56, column 42
    function sortValue_14 (queue :  tdic.util.dataloader.data.admindata.QueueData) : java.lang.Object {
      return queue.SubGroupVisible
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadQueueLV.pcf: line 61, column 42
    function sortValue_16 (queue :  tdic.util.dataloader.data.admindata.QueueData) : java.lang.Object {
      return queue.Exclude
    }
    
    // 'value' attribute on TextCell (id=displayName_Cell) at AdminDataUploadQueueLV.pcf: line 29, column 41
    function sortValue_4 (queue :  tdic.util.dataloader.data.admindata.QueueData) : java.lang.Object {
      return queue.DisplayName
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadQueueLV.pcf: line 35, column 41
    function sortValue_6 (queue :  tdic.util.dataloader.data.admindata.QueueData) : java.lang.Object {
      return queue.PublicID
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadQueueLV.pcf: line 41, column 41
    function sortValue_8 (queue :  tdic.util.dataloader.data.admindata.QueueData) : java.lang.Object {
      return queue.Name
    }
    
    // 'value' attribute on RowIterator (id=Queue) at AdminDataUploadQueueLV.pcf: line 15, column 92
    function value_58 () : java.util.ArrayList<tdic.util.dataloader.data.admindata.QueueData> {
      return processor.QueueArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadQueueLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadQueueLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminDataUploadQueueLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at AdminDataUploadQueueLV.pcf: line 17, column 90
    function highlighted_57 () : java.lang.Boolean {
      return (queue.Error or queue.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadQueueLV.pcf: line 23, column 46
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=displayName_Cell) at AdminDataUploadQueueLV.pcf: line 29, column 41
    function label_22 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Queue.DisplayName")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadQueueLV.pcf: line 35, column 41
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Queue.PublicID")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at AdminDataUploadQueueLV.pcf: line 41, column 41
    function label_32 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Queue.Name")
    }
    
    // 'label' attribute on TextCell (id=group_Cell) at AdminDataUploadQueueLV.pcf: line 46, column 41
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Queue.Group")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at AdminDataUploadQueueLV.pcf: line 51, column 41
    function label_42 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Queue.Description")
    }
    
    // 'label' attribute on BooleanRadioCell (id=subGroupVisble_Cell) at AdminDataUploadQueueLV.pcf: line 56, column 42
    function label_47 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Queue.SubGroupVisible")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadQueueLV.pcf: line 61, column 42
    function label_52 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'value' attribute on TextCell (id=displayName_Cell) at AdminDataUploadQueueLV.pcf: line 29, column 41
    function valueRoot_24 () : java.lang.Object {
      return queue
    }
    
    // 'value' attribute on TextCell (id=group_Cell) at AdminDataUploadQueueLV.pcf: line 46, column 41
    function valueRoot_39 () : java.lang.Object {
      return queue.Group
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadQueueLV.pcf: line 23, column 46
    function value_18 () : java.lang.String {
      return processor.getLoadStatus(queue)
    }
    
    // 'value' attribute on TextCell (id=displayName_Cell) at AdminDataUploadQueueLV.pcf: line 29, column 41
    function value_23 () : java.lang.String {
      return queue.DisplayName
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadQueueLV.pcf: line 35, column 41
    function value_28 () : java.lang.String {
      return queue.PublicID
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadQueueLV.pcf: line 41, column 41
    function value_33 () : java.lang.String {
      return queue.Name
    }
    
    // 'value' attribute on TextCell (id=group_Cell) at AdminDataUploadQueueLV.pcf: line 46, column 41
    function value_38 () : java.lang.String {
      return queue.Group.Name
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at AdminDataUploadQueueLV.pcf: line 51, column 41
    function value_43 () : java.lang.String {
      return queue.Description
    }
    
    // 'value' attribute on BooleanRadioCell (id=subGroupVisble_Cell) at AdminDataUploadQueueLV.pcf: line 56, column 42
    function value_48 () : java.lang.Boolean {
      return queue.SubGroupVisible
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadQueueLV.pcf: line 61, column 42
    function value_53 () : java.lang.Boolean {
      return queue.Exclude
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadQueueLV.pcf: line 23, column 46
    function visible_19 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get queue () : tdic.util.dataloader.data.admindata.QueueData {
      return getIteratedValue(1) as tdic.util.dataloader.data.admindata.QueueData
    }
    
    
  }
  
  
}