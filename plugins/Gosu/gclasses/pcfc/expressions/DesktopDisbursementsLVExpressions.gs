package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopDisbursementsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopDisbursementsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopDisbursementsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopDisbursementsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at DesktopDisbursementsLV.pcf: line 14, column 54
    function initialValue_0 () : gw.api.web.payment.SuspensePaymentUtil {
      return new gw.api.web.payment.SuspensePaymentUtil()
    }
    
    // 'value' attribute on TextCell (id=ReviewedTDIC_Cell) at DesktopDisbursementsLV.pcf: line 36, column 42
    function sortValue_1 (disbursementVar :  entity.Disbursement) : java.lang.Object {
      return disbursementVar.Reviewed_TDIC
    }
    
    // 'value' attribute on DateCell (id=trigger_Cell) at DesktopDisbursementsLV.pcf: line 41, column 47
    function sortValue_2 (disbursementVar :  entity.Disbursement) : java.lang.Object {
      return disbursementVar.CreateTime
    }
    
    // 'sortBy' attribute on DateCell (id=due_Cell) at DesktopDisbursementsLV.pcf: line 46, column 140
    function sortValue_3 (disbursementVar :  entity.Disbursement) : java.lang.Object {
      return disbursementVar.DueDate
    }
    
    // 'sortBy' attribute on TypeKeyCell (id=status_Cell) at DesktopDisbursementsLV.pcf: line 52, column 51
    function sortValue_4 (disbursementVar :  entity.Disbursement) : java.lang.Object {
      return disbursementVar.Status
    }
    
    // 'value' attribute on TypeKeyCell (id=reason_Cell) at DesktopDisbursementsLV.pcf: line 58, column 39
    function sortValue_5 (disbursementVar :  entity.Disbursement) : java.lang.Object {
      return disbursementVar.Reason
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amount_Cell) at DesktopDisbursementsLV.pcf: line 65, column 43
    function sortValue_6 (disbursementVar :  entity.Disbursement) : java.lang.Object {
      return disbursementVar.Amount
    }
    
    // 'sortBy' attribute on TextCell (id=refNumber_Cell) at DesktopDisbursementsLV.pcf: line 70, column 62
    function sortValue_7 (disbursementVar :  entity.Disbursement) : java.lang.Object {
      return disbursementVar.OutgoingPayment.RefNumber
    }
    
    // 'value' attribute on TextCell (id=memo_TDIC_Cell) at DesktopDisbursementsLV.pcf: line 94, column 41
    function sortValue_8 (disbursementVar :  entity.Disbursement) : java.lang.Object {
      return disbursementVar.Memo
    }
    
    // '$$sumValue' attribute on RowIterator (id=DisbursementsRowIterator) at DesktopDisbursementsLV.pcf: line 65, column 43
    function sumValueRoot_10 (disbursementVar :  entity.Disbursement) : java.lang.Object {
      return disbursementVar
    }
    
    // 'footerSumValue' attribute on RowIterator (id=DisbursementsRowIterator) at DesktopDisbursementsLV.pcf: line 65, column 43
    function sumValue_9 (disbursementVar :  entity.Disbursement) : java.lang.Object {
      return disbursementVar.Amount
    }
    
    // 'value' attribute on RowIterator (id=DisbursementsRowIterator) at DesktopDisbursementsLV.pcf: line 27, column 79
    function value_66 () : gw.api.database.IQueryBeanResult<entity.Disbursement> {
      return disbursements
    }
    
    property get disbursements () : gw.api.database.IQueryBeanResult<Disbursement> {
      return getRequireValue("disbursements", 0) as gw.api.database.IQueryBeanResult<Disbursement>
    }
    
    property set disbursements ($arg :  gw.api.database.IQueryBeanResult<Disbursement>) {
      setRequireValue("disbursements", 0, $arg)
    }
    
    property get suspensePaymentUtil () : gw.api.web.payment.SuspensePaymentUtil {
      return getVariableValue("suspensePaymentUtil", 0) as gw.api.web.payment.SuspensePaymentUtil
    }
    
    property set suspensePaymentUtil ($arg :  gw.api.web.payment.SuspensePaymentUtil) {
      setVariableValue("suspensePaymentUtil", 0, $arg)
    }
    
    function getAssignedUser(disbursementVar : Disbursement) : String{
      if(disbursementVar.OpenApprovalActivity != null){
        return disbursementVar.OpenApprovalActivity.AssignedGroup.DisplayName
      }
      else if (disbursementVar.Status != DisbursementStatus.TC_AWAITINGAPPROVAL and disbursementVar.Approver_TDIC != null) {
        return disbursementVar.Approver_TDIC.DisplayName
      }
      return null
    }
    function suspenseTransactionNumberAction(disbursement : SuspenseDisbursement) {
      if (suspensePaymentUtil.getDesktopTransaction(disbursement.SuspensePayment) != null) {
        TransactionDetailPopup.push(suspensePaymentUtil.getDesktopTransaction(disbursement.SuspensePayment))
      } else {
        SuspensePaymentMultipleTransactionsPopup.push(disbursement.SuspensePayment)
      }
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopDisbursementsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DesktopDisbursementsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TypeKeyCell (id=reason_Cell) at DesktopDisbursementsLV.pcf: line 58, column 39
    function action_23 () : void {
      DisbursementDetail.go(disbursementVar, true)
    }
    
    // 'action' attribute on TextCell (id=account_Cell) at DesktopDisbursementsLV.pcf: line 84, column 25
    function action_39 () : void {
      AccountSummary.push((disbursementVar as AccountDisbursement).Account)
    }
    
    // 'action' attribute on TextCell (id=suspenseTransactionNumber_Cell) at DesktopDisbursementsLV.pcf: line 114, column 71
    function action_57 () : void {
      suspenseTransactionNumberAction(disbursementVar as SuspenseDisbursement)
    }
    
    // 'action' attribute on TypeKeyCell (id=reason_Cell) at DesktopDisbursementsLV.pcf: line 58, column 39
    function action_dest_24 () : pcf.api.Destination {
      return pcf.DisbursementDetail.createDestination(disbursementVar, true)
    }
    
    // 'action' attribute on TextCell (id=account_Cell) at DesktopDisbursementsLV.pcf: line 84, column 25
    function action_dest_40 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination((disbursementVar as AccountDisbursement).Account)
    }
    
    // 'available' attribute on TextCell (id=account_Cell) at DesktopDisbursementsLV.pcf: line 84, column 25
    function available_38 () : java.lang.Boolean {
      return disbursementVar typeis AccountDisbursement
    }
    
    // 'available' attribute on TextCell (id=suspenseTransactionNumber_Cell) at DesktopDisbursementsLV.pcf: line 114, column 71
    function available_55 () : java.lang.Boolean {
      return disbursementVar typeis SuspenseDisbursement
    }
    
    // 'condition' attribute on ToolbarFlag at DesktopDisbursementsLV.pcf: line 30, column 33
    function condition_11 () : java.lang.Boolean {
      return disbursementVar.canApprove() and disbursementVar.Reviewed_TDIC == true
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=amount_Cell) at DesktopDisbursementsLV.pcf: line 65, column 43
    function currency_30 () : typekey.Currency {
      return disbursementVar.Currency
    }
    
    // 'value' attribute on TextCell (id=ReviewedTDIC_Cell) at DesktopDisbursementsLV.pcf: line 36, column 42
    function valueRoot_13 () : java.lang.Object {
      return disbursementVar
    }
    
    // 'value' attribute on TextCell (id=refNumber_Cell) at DesktopDisbursementsLV.pcf: line 70, column 62
    function valueRoot_33 () : java.lang.Object {
      return disbursementVar.OutgoingPayment
    }
    
    // 'value' attribute on TextCell (id=ReviewedTDIC_Cell) at DesktopDisbursementsLV.pcf: line 36, column 42
    function value_12 () : java.lang.Boolean {
      return disbursementVar.Reviewed_TDIC
    }
    
    // 'value' attribute on DateCell (id=trigger_Cell) at DesktopDisbursementsLV.pcf: line 41, column 47
    function value_15 () : java.util.Date {
      return disbursementVar.CreateTime
    }
    
    // 'value' attribute on DateCell (id=due_Cell) at DesktopDisbursementsLV.pcf: line 46, column 140
    function value_18 () : java.util.Date {
      return ((disbursementVar.Status != TC_REJECTED) and (disbursementVar.Status != TC_REAPPLIED)) ? disbursementVar.DueDate : null
    }
    
    // 'value' attribute on TypeKeyCell (id=status_Cell) at DesktopDisbursementsLV.pcf: line 52, column 51
    function value_20 () : typekey.DisbursementStatus {
      return disbursementVar.Status
    }
    
    // 'value' attribute on TypeKeyCell (id=reason_Cell) at DesktopDisbursementsLV.pcf: line 58, column 39
    function value_25 () : typekey.Reason {
      return disbursementVar.Reason
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amount_Cell) at DesktopDisbursementsLV.pcf: line 65, column 43
    function value_28 () : gw.pl.currency.MonetaryAmount {
      return disbursementVar.Amount
    }
    
    // 'value' attribute on TextCell (id=refNumber_Cell) at DesktopDisbursementsLV.pcf: line 70, column 62
    function value_32 () : java.lang.String {
      return disbursementVar.OutgoingPayment.RefNumber
    }
    
    // 'value' attribute on TextCell (id=age_Cell) at DesktopDisbursementsLV.pcf: line 76, column 45
    function value_35 () : java.math.BigDecimal {
      return disbursementVar.Age
    }
    
    // 'value' attribute on TextCell (id=account_Cell) at DesktopDisbursementsLV.pcf: line 84, column 25
    function value_41 () : java.lang.String {
      return disbursementVar typeis AccountDisbursement ? disbursementVar.Account.AccountNumber : null
    }
    
    // 'value' attribute on TextCell (id=accountName_Cell) at DesktopDisbursementsLV.pcf: line 90, column 108
    function value_45 () : java.lang.String {
      return disbursementVar typeis AccountDisbursement ? disbursementVar.Account.AccountName : null
    }
    
    // 'value' attribute on TextCell (id=memo_TDIC_Cell) at DesktopDisbursementsLV.pcf: line 94, column 41
    function value_48 () : java.lang.String {
      return disbursementVar.Memo
    }
    
    // 'value' attribute on TextCell (id=unappliedFunds_Cell) at DesktopDisbursementsLV.pcf: line 100, column 45
    function value_51 () : entity.UnappliedFund {
      return disbursementVar typeis AccountDisbursement ? disbursementVar.UnappliedFund : null
    }
    
    // 'value' attribute on TextCell (id=state_Cell) at DesktopDisbursementsLV.pcf: line 106, column 45
    function value_53 () : typekey.Jurisdiction {
      return disbursementVar typeis AccountDisbursement && disbursementVar.UnappliedFund.Policy?.LatestPolicyPeriod != null ? disbursementVar.UnappliedFund.Policy.LatestPolicyPeriod.RiskJurisdiction : null
    }
    
    // 'value' attribute on TextCell (id=suspenseTransactionNumber_Cell) at DesktopDisbursementsLV.pcf: line 114, column 71
    function value_58 () : java.lang.String {
      return suspensePaymentUtil.getDesktopTransactionDisplayValue( (disbursementVar as SuspenseDisbursement).SuspensePayment)
    }
    
    // 'value' attribute on TextCell (id=assignedUser_Cell) at DesktopDisbursementsLV.pcf: line 138, column 55
    function value_61 () : java.lang.String {
      return getAssignedUser( disbursementVar )
    }
    
    // 'value' attribute on TextCell (id=ReviewedBy_Cell) at DesktopDisbursementsLV.pcf: line 145, column 36
    function value_63 () : entity.User {
      return disbursementVar.ReviewedBy_TDIC
    }
    
    property get disbursementVar () : entity.Disbursement {
      return getIteratedValue(1) as entity.Disbursement
    }
    
    
  }
  
  
}