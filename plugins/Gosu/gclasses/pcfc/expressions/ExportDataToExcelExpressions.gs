package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/DataExport/ExportDataToExcel.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ExportDataToExcelExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/DataExport/ExportDataToExcel.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ExportDataToExcelExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ButtonInput (id=planExportButton_Input) at ExportDataToExcel.pcf: line 57, column 52
    function action_12 () : void {
      tdic.util.dataloader.export.planexport.BCPlanExport.exportData()
    }
    
    // 'action' attribute on ButtonInput (id=adminExportButton_Input) at ExportDataToExcel.pcf: line 50, column 53
    function action_9 () : void {
      tdic.util.dataloader.export.adminexport.BCAdminExport.exportData()
    }
    
    // 'value' attribute on RangeInput (id=dataType_Input) at ExportDataToExcel.pcf: line 41, column 42
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      excelExportData = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at ExportDataToExcel.pcf: line 18, column 43
    function initialValue_0 () : gw.api.admin.ExportDataInfo {
      return new gw.api.admin.ExportDataInfo()
    }
    
    // 'initialValue' attribute on Variable at ExportDataToExcel.pcf: line 30, column 22
    function initialValue_1 () : String {
      return adminData
    }
    
    // 'parent' attribute on Page (id=ExportDataToExcel) at ExportDataToExcel.pcf: line 12, column 80
    static function parent_14 () : pcf.api.Destination {
      return pcf.ServerTools.createDestination()
    }
    
    // 'valueRange' attribute on RangeInput (id=dataType_Input) at ExportDataToExcel.pcf: line 41, column 42
    function valueRange_4 () : java.lang.Object {
      return {adminData, planData}
    }
    
    // 'value' attribute on RangeInput (id=dataType_Input) at ExportDataToExcel.pcf: line 41, column 42
    function value_2 () : java.lang.String {
      return excelExportData
    }
    
    // 'valueRange' attribute on RangeInput (id=dataType_Input) at ExportDataToExcel.pcf: line 41, column 42
    function verifyValueRangeIsAllowedType_5 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=dataType_Input) at ExportDataToExcel.pcf: line 41, column 42
    function verifyValueRangeIsAllowedType_5 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=dataType_Input) at ExportDataToExcel.pcf: line 41, column 42
    function verifyValueRange_6 () : void {
      var __valueRangeArg = {adminData, planData}
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_5(__valueRangeArg)
    }
    
    // 'visible' attribute on ButtonInput (id=planExportButton_Input) at ExportDataToExcel.pcf: line 57, column 52
    function visible_11 () : java.lang.Boolean {
      return excelExportData == planData
    }
    
    // 'visible' attribute on ButtonInput (id=adminExportButton_Input) at ExportDataToExcel.pcf: line 50, column 53
    function visible_8 () : java.lang.Boolean {
      return excelExportData == adminData
    }
    
    override property get CurrentLocation () : pcf.ExportDataToExcel {
      return super.CurrentLocation as pcf.ExportDataToExcel
    }
    
    property get ExportDataInfo () : gw.api.admin.ExportDataInfo {
      return getVariableValue("ExportDataInfo", 0) as gw.api.admin.ExportDataInfo
    }
    
    property set ExportDataInfo ($arg :  gw.api.admin.ExportDataInfo) {
      setVariableValue("ExportDataInfo", 0, $arg)
    }
    
    property get adminData () : String {
      return getVariableValue("adminData", 0) as String
    }
    
    property set adminData ($arg :  String) {
      setVariableValue("adminData", 0, $arg)
    }
    
    property get excelExportData () : String {
      return getVariableValue("excelExportData", 0) as String
    }
    
    property set excelExportData ($arg :  String) {
      setVariableValue("excelExportData", 0, $arg)
    }
    
    property get planData () : String {
      return getVariableValue("planData", 0) as String
    }
    
    property set planData ($arg :  String) {
      setVariableValue("planData", 0, $arg)
    }
    
    
  }
  
  
}