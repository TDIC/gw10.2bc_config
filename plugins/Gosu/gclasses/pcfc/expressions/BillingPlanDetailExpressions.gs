package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BillingPlanDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BillingPlanDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (billingPlan :  BillingPlan) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=BillingPlanDetail) at BillingPlanDetail.pcf: line 10, column 87
    function canEdit_2 () : java.lang.Boolean {
      return perm.System.billplanedit
    }
    
    // 'def' attribute on ScreenRef at BillingPlanDetail.pcf: line 17, column 51
    function def_onEnter_0 (def :  pcf.BillingPlanDetailScreen) : void {
      def.onEnter(billingPlan)
    }
    
    // 'def' attribute on ScreenRef at BillingPlanDetail.pcf: line 17, column 51
    function def_refreshVariables_1 (def :  pcf.BillingPlanDetailScreen) : void {
      def.refreshVariables(billingPlan)
    }
    
    // 'parent' attribute on Page (id=BillingPlanDetail) at BillingPlanDetail.pcf: line 10, column 87
    static function parent_3 (billingPlan :  BillingPlan) : pcf.api.Destination {
      return pcf.BillingPlans.createDestination()
    }
    
    // 'title' attribute on Page (id=BillingPlanDetail) at BillingPlanDetail.pcf: line 10, column 87
    static function title_4 (billingPlan :  BillingPlan) : java.lang.Object {
      return DisplayKey.get("Web.BillingPlanDetail.Title", billingPlan.Name)
    }
    
    override property get CurrentLocation () : pcf.BillingPlanDetail {
      return super.CurrentLocation as pcf.BillingPlanDetail
    }
    
    property get billingPlan () : BillingPlan {
      return getVariableValue("billingPlan", 0) as BillingPlan
    }
    
    property set billingPlan ($arg :  BillingPlan) {
      setVariableValue("billingPlan", 0, $arg)
    }
    
    
  }
  
  
}