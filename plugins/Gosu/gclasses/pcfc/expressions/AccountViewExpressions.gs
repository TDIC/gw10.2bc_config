package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/AccountView.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountViewExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/acct360/AccountView.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountViewExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'def' attribute on ScreenRef at AccountView.pcf: line 14, column 50
    function def_onEnter_0 (def :  pcf.AccountView360Screen) : void {
      def.onEnter(account, null)
    }
    
    // 'def' attribute on ScreenRef at AccountView.pcf: line 14, column 50
    function def_refreshVariables_1 (def :  pcf.AccountView360Screen) : void {
      def.refreshVariables(account, null)
    }
    
    // Page (id=AccountView) at AccountView.pcf: line 7, column 72
    static function parent_2 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.AccountView {
      return super.CurrentLocation as pcf.AccountView
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  
}