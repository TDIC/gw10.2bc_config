package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewNegativeWriteoffReversalWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewNegativeWriteoffReversalWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=NewNegativeWriteoffReversalWizard) at NewNegativeWriteoffReversalWizard.pcf: line 12, column 44
    function afterCancel_8 () : void {
      DesktopGroup.go()
    }
    
    // 'afterCancel' attribute on Wizard (id=NewNegativeWriteoffReversalWizard) at NewNegativeWriteoffReversalWizard.pcf: line 12, column 44
    function afterCancel_dest_9 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewNegativeWriteoffReversalWizard) at NewNegativeWriteoffReversalWizard.pcf: line 12, column 44
    function afterFinish_13 () : void {
      DesktopGroup.go()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewNegativeWriteoffReversalWizard) at NewNegativeWriteoffReversalWizard.pcf: line 12, column 44
    function afterFinish_dest_14 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'allowNext' attribute on WizardStep (id=Step1) at NewNegativeWriteoffReversalWizard.pcf: line 25, column 103
    function allowNext_1 () : java.lang.Boolean {
      return negativeWriteoffToReverse.getNegativeWriteoff() != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=NewNegativeWriteoffReversalWizard) at NewNegativeWriteoffReversalWizard.pcf: line 12, column 44
    function beforeCommit_10 (pickedValue :  java.lang.Object) : void {
      negativeWriteoffToReverse.reverse()
    }
    
    // 'initialValue' attribute on Variable at NewNegativeWriteoffReversalWizard.pcf: line 18, column 35
    function initialValue_0 () : NegativeWriteoffRev {
      return new NegativeWriteoffRev()
    }
    
    // 'onExit' attribute on WizardStep (id=Step1) at NewNegativeWriteoffReversalWizard.pcf: line 25, column 103
    function onExit_2 () : void {
      negativeWriteoffToReverse.initiateApprovalActivityIfUserLacksAuthority()
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at NewNegativeWriteoffReversalWizard.pcf: line 25, column 103
    function screen_onEnter_3 (def :  pcf.NewNegativeWriteoffReversalSearchScreen) : void {
      def.onEnter(negativeWriteoffToReverse, null)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at NewNegativeWriteoffReversalWizard.pcf: line 31, column 101
    function screen_onEnter_6 (def :  pcf.NewNegativeWriteoffReversalConfirmationScreen) : void {
      def.onEnter(negativeWriteoffToReverse)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at NewNegativeWriteoffReversalWizard.pcf: line 25, column 103
    function screen_refreshVariables_4 (def :  pcf.NewNegativeWriteoffReversalSearchScreen) : void {
      def.refreshVariables(negativeWriteoffToReverse, null)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at NewNegativeWriteoffReversalWizard.pcf: line 31, column 101
    function screen_refreshVariables_7 (def :  pcf.NewNegativeWriteoffReversalConfirmationScreen) : void {
      def.refreshVariables(negativeWriteoffToReverse)
    }
    
    // 'tabBar' attribute on Wizard (id=NewNegativeWriteoffReversalWizard) at NewNegativeWriteoffReversalWizard.pcf: line 12, column 44
    function tabBar_onEnter_11 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=NewNegativeWriteoffReversalWizard) at NewNegativeWriteoffReversalWizard.pcf: line 12, column 44
    function tabBar_refreshVariables_12 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.NewNegativeWriteoffReversalWizard {
      return super.CurrentLocation as pcf.NewNegativeWriteoffReversalWizard
    }
    
    property get negativeWriteoffToReverse () : NegativeWriteoffRev {
      return getVariableValue("negativeWriteoffToReverse", 0) as NegativeWriteoffRev
    }
    
    property set negativeWriteoffToReverse ($arg :  NegativeWriteoffRev) {
      setVariableValue("negativeWriteoffToReverse", 0, $arg)
    }
    
    
  }
  
  
}