package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/ProducerAgencyBillCycles.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerAgencyBillCyclesExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/ProducerAgencyBillCycles.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ProducerAgencyBillCyclesLDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=number_Cell) at ProducerAgencyBillCycles.pcf: line 88, column 65
    function action_31 () : void {
      AgencyBillStatementDetail.drilldown(cycle)
    }
    
    // 'action' attribute on TextCell (id=number_Cell) at ProducerAgencyBillCycles.pcf: line 88, column 65
    function action_dest_32 () : pcf.api.Destination {
      return pcf.AgencyBillStatementDetail.createDestination(cycle)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=amount_Cell) at ProducerAgencyBillCycles.pcf: line 95, column 61
    function currency_38 () : typekey.Currency {
      return cycle.Currency
    }
    
    // 'value' attribute on DateCell (id=dateSent_Cell) at ProducerAgencyBillCycles.pcf: line 77, column 61
    function valueRoot_26 () : java.lang.Object {
      return cycle.StatementInvoice
    }
    
    // 'value' attribute on MonetaryAmountCell (id=paid_Cell) at ProducerAgencyBillCycles.pcf: line 103, column 51
    function valueRoot_41 () : java.lang.Object {
      return cycle
    }
    
    // 'value' attribute on DateCell (id=dateSent_Cell) at ProducerAgencyBillCycles.pcf: line 77, column 61
    function value_25 () : java.util.Date {
      return cycle.StatementInvoice.EventDate
    }
    
    // 'value' attribute on DateCell (id=dateDue_Cell) at ProducerAgencyBillCycles.pcf: line 82, column 59
    function value_28 () : java.util.Date {
      return cycle.StatementInvoice.DueDate
    }
    
    // 'value' attribute on TextCell (id=number_Cell) at ProducerAgencyBillCycles.pcf: line 88, column 65
    function value_33 () : java.lang.String {
      return cycle.StatementInvoice.InvoiceNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amount_Cell) at ProducerAgencyBillCycles.pcf: line 95, column 61
    function value_36 () : gw.pl.currency.MonetaryAmount {
      return cycle.StatementInvoice.NetAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=paid_Cell) at ProducerAgencyBillCycles.pcf: line 103, column 51
    function value_40 () : gw.pl.currency.MonetaryAmount {
      return cycle.ActualPaidAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=writtenOff_Cell) at ProducerAgencyBillCycles.pcf: line 111, column 51
    function value_44 () : gw.pl.currency.MonetaryAmount {
      return cycle.WrittenOffAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=due_Cell) at ProducerAgencyBillCycles.pcf: line 119, column 47
    function value_48 () : gw.pl.currency.MonetaryAmount {
      return cycle.NetAmountDue
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at ProducerAgencyBillCycles.pcf: line 125, column 33
    function value_52 () : java.lang.String {
      return cycle.StatementInvoice.DisplayStatus
    }
    
    // 'value' attribute on DateCell (id=promiseDue_Cell) at ProducerAgencyBillCycles.pcf: line 130, column 66
    function value_55 () : java.util.Date {
      return cycle.StatementInvoice.PromiseDueDate
    }
    
    // 'value' attribute on TextCell (id=invoiceStream_Cell) at ProducerAgencyBillCycles.pcf: line 136, column 53
    function value_58 () : entity.InvoiceStream {
      return cycle.StatementInvoice.InvoiceStream
    }
    
    property get cycle () : entity.AgencyBillCycle {
      return getIteratedValue(2) as entity.AgencyBillCycle
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/ProducerAgencyBillCycles.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerAgencyBillCyclesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=ProducerAgencyBillCycles) at ProducerAgencyBillCycles.pcf: line 10, column 76
    static function canVisit_102 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodabcyclesview
    }
    
    // 'initialValue' attribute on Variable at ProducerAgencyBillCycles.pcf: line 19, column 43
    function initialValue_0 () : List<AgencyBillCycle> {
      return gw.api.web.producer.ProducerUtil.getNonEmptyCyclesAsList(producer)
    }
    
    // 'initialValue' attribute on Variable at ProducerAgencyBillCycles.pcf: line 23, column 72
    function initialValue_1 () : java.util.Map<AgencyBillCycle, AgencyCyclePromise> {
      return new java.util.HashMap<AgencyBillCycle, AgencyCyclePromise>()
    }
    
    // Page (id=ProducerAgencyBillCycles) at ProducerAgencyBillCycles.pcf: line 10, column 76
    static function parent_103 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    override property get CurrentLocation () : pcf.ProducerAgencyBillCycles {
      return super.CurrentLocation as pcf.ProducerAgencyBillCycles
    }
    
    property get cycleToRecentPromiseMap () : java.util.Map<AgencyBillCycle, AgencyCyclePromise> {
      return getVariableValue("cycleToRecentPromiseMap", 0) as java.util.Map<AgencyBillCycle, AgencyCyclePromise>
    }
    
    property set cycleToRecentPromiseMap ($arg :  java.util.Map<AgencyBillCycle, AgencyCyclePromise>) {
      setVariableValue("cycleToRecentPromiseMap", 0, $arg)
    }
    
    property get cycles () : List<AgencyBillCycle> {
      return getVariableValue("cycles", 0) as List<AgencyBillCycle>
    }
    
    property set cycles ($arg :  List<AgencyBillCycle>) {
      setVariableValue("cycles", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/ProducerAgencyBillCycles.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerAgencyBillCyclesLDVExpressionsImpl extends ProducerAgencyBillCyclesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextInput (id=PaymentType_Input) at ProducerAgencyBillCycles.pcf: line 165, column 81
    function action_72 () : void {
      AgencyBillPlanDetailPopup.push(producer.AgencyBillPlan)
    }
    
    // 'action' attribute on TextInput (id=PaymentType_Input) at ProducerAgencyBillCycles.pcf: line 165, column 81
    function action_dest_73 () : pcf.api.Destination {
      return pcf.AgencyBillPlanDetailPopup.createDestination(producer.AgencyBillPlan)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=ClearCommissionDifferencesLogic_Input) at ProducerAgencyBillCycles.pcf: line 153, column 132
    function currency_64 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'def' attribute on PanelRef at ProducerAgencyBillCycles.pcf: line 199, column 87
    function def_onEnter_100 (def :  pcf.AgencyBillCycleEventsLV) : void {
      def.onEnter(selectedAgencyBillCycle.StatementInvoice)
    }
    
    // 'def' attribute on PanelRef at ProducerAgencyBillCycles.pcf: line 199, column 87
    function def_refreshVariables_101 (def :  pcf.AgencyBillCycleEventsLV) : void {
      def.refreshVariables(selectedAgencyBillCycle.StatementInvoice)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=InvoiceStreamFilter_Input) at ProducerAgencyBillCycles.pcf: line 41, column 48
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceStreamSelection = (__VALUE_TO_SET as entity.InvoiceStream)
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerAgencyBillCycles.pcf: line 63, column 100
    function filter_10 () : gw.api.filters.IFilter {
      return gw.agencybill.ProducerAgencyBillCycleFilters.PLANNED
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerAgencyBillCycles.pcf: line 66, column 99
    function filter_11 () : gw.api.filters.IFilter {
      return gw.agencybill.ProducerAgencyBillCycleFilters.CLOSED
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerAgencyBillCycles.pcf: line 69, column 96
    function filter_12 () : gw.api.filters.IFilter {
      return gw.api.filters.StandardQueryFilters.ALL
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerAgencyBillCycles.pcf: line 60, column 97
    function filter_9 () : gw.api.filters.IFilter {
      return gw.agencybill.ProducerAgencyBillCycleFilters.OPEN
    }
    
    // 'onChange' attribute on PostOnChange at ProducerAgencyBillCycles.pcf: line 43, column 243
    function onChange_2 () : void {
      cycles = invoiceStreamSelection != null ? producer.AgencyBillCyclesSortedByStatementDate.where(\ i -> i.StatementInvoice.InvoiceStream == invoiceStreamSelection) : producer.AgencyBillCyclesSortedByStatementDate
    }
    
    // 'value' attribute on DateCell (id=dateSent_Cell) at ProducerAgencyBillCycles.pcf: line 77, column 61
    function sortValue_13 (cycle :  entity.AgencyBillCycle) : java.lang.Object {
      return cycle.StatementInvoice.EventDate
    }
    
    // 'value' attribute on TextCell (id=number_Cell) at ProducerAgencyBillCycles.pcf: line 88, column 65
    function sortValue_14 (cycle :  entity.AgencyBillCycle) : java.lang.Object {
      return cycle.StatementInvoice.InvoiceNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amount_Cell) at ProducerAgencyBillCycles.pcf: line 95, column 61
    function sortValue_15 (cycle :  entity.AgencyBillCycle) : java.lang.Object {
      return cycle.StatementInvoice.NetAmount
    }
    
    // 'value' attribute on TextCell (id=invoiceStream_Cell) at ProducerAgencyBillCycles.pcf: line 136, column 53
    function sortValue_16 (cycle :  entity.AgencyBillCycle) : java.lang.Object {
      return cycle.StatementInvoice.InvoiceStream
    }
    
    // '$$sumValue' attribute on RowIterator (id=agencyBillCycles) at ProducerAgencyBillCycles.pcf: line 95, column 61
    function sumValueRoot_18 (cycle :  entity.AgencyBillCycle) : java.lang.Object {
      return cycle
    }
    
    // 'footerSumValue' attribute on RowIterator (id=agencyBillCycles) at ProducerAgencyBillCycles.pcf: line 95, column 61
    function sumValue_17 (cycle :  entity.AgencyBillCycle) : java.lang.Object {
      return cycle.NetAmount
    }
    
    // 'footerSumValue' attribute on RowIterator (id=agencyBillCycles) at ProducerAgencyBillCycles.pcf: line 103, column 51
    function sumValue_19 (cycle :  entity.AgencyBillCycle) : java.lang.Object {
      return cycle.ActualPaidAmount
    }
    
    // 'footerSumValue' attribute on RowIterator (id=agencyBillCycles) at ProducerAgencyBillCycles.pcf: line 111, column 51
    function sumValue_21 (cycle :  entity.AgencyBillCycle) : java.lang.Object {
      return cycle.WrittenOffAmount
    }
    
    // 'footerSumValue' attribute on RowIterator (id=agencyBillCycles) at ProducerAgencyBillCycles.pcf: line 119, column 47
    function sumValue_23 (cycle :  entity.AgencyBillCycle) : java.lang.Object {
      return cycle.NetAmountDue
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=InvoiceStreamFilter_Input) at ProducerAgencyBillCycles.pcf: line 41, column 48
    function valueRange_5 () : java.lang.Object {
      return producer.InvoiceStreams.sortDescending()
    }
    
    // 'value' attribute on TextInput (id=PaymentType_Input) at ProducerAgencyBillCycles.pcf: line 165, column 81
    function valueRoot_75 () : java.lang.Object {
      return selectedAgencyBillCycle.Producer.AgencyBillPlan
    }
    
    // 'value' attribute on DateInput (id=MostRecentPromiseDate_Input) at ProducerAgencyBillCycles.pcf: line 172, column 88
    function valueRoot_79 () : java.lang.Object {
      return selectedAgencyBillCycle.MostRecentExecutedPromise
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PromisedAmount_Input) at ProducerAgencyBillCycles.pcf: line 188, column 88
    function valueRoot_90 () : java.lang.Object {
      return selectedAgencyBillCycle
    }
    
    // 'value' attribute on ToolbarRangeInput (id=InvoiceStreamFilter_Input) at ProducerAgencyBillCycles.pcf: line 41, column 48
    function value_3 () : entity.InvoiceStream {
      return invoiceStreamSelection
    }
    
    // 'value' attribute on RowIterator (id=agencyBillCycles) at ProducerAgencyBillCycles.pcf: line 54, column 72
    function value_61 () : java.util.List<entity.AgencyBillCycle> {
      return cycles
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ClearCommissionDifferencesLogic_Input) at ProducerAgencyBillCycles.pcf: line 153, column 132
    function value_63 () : gw.pl.currency.MonetaryAmount {
      return selectedAgencyBillCycle.StatementInvoice.AgencyBillCycle.Producer.AgencyBillPlan.getClearCommissionThreshold(producer.Currency)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ClearGrossDifferencesLogic_Input) at ProducerAgencyBillCycles.pcf: line 160, column 127
    function value_68 () : gw.pl.currency.MonetaryAmount {
      return selectedAgencyBillCycle.StatementInvoice.AgencyBillCycle.Producer.AgencyBillPlan.getClearGrossThreshold(producer.Currency)
    }
    
    // 'value' attribute on TextInput (id=PaymentType_Input) at ProducerAgencyBillCycles.pcf: line 165, column 81
    function value_74 () : java.lang.String {
      return selectedAgencyBillCycle.Producer.AgencyBillPlan.Name
    }
    
    // 'value' attribute on DateInput (id=MostRecentPromiseDate_Input) at ProducerAgencyBillCycles.pcf: line 172, column 88
    function value_78 () : java.util.Date {
      return selectedAgencyBillCycle.MostRecentExecutedPromise.DistributedDate
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MostRecentPromiseAmount_Input) at ProducerAgencyBillCycles.pcf: line 179, column 88
    function value_83 () : gw.pl.currency.MonetaryAmount {
      return selectedAgencyBillCycle.MostRecentExecutedPromise.Amount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PromisedAmount_Input) at ProducerAgencyBillCycles.pcf: line 188, column 88
    function value_89 () : gw.pl.currency.MonetaryAmount {
      return selectedAgencyBillCycle.PromisedAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnpromisedAmount_Input) at ProducerAgencyBillCycles.pcf: line 195, column 88
    function value_95 () : gw.pl.currency.MonetaryAmount {
      return selectedAgencyBillCycle.UnpromisedAmount
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=InvoiceStreamFilter_Input) at ProducerAgencyBillCycles.pcf: line 41, column 48
    function verifyValueRangeIsAllowedType_6 ($$arg :  entity.InvoiceStream[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=InvoiceStreamFilter_Input) at ProducerAgencyBillCycles.pcf: line 41, column 48
    function verifyValueRangeIsAllowedType_6 ($$arg :  gw.api.database.IQueryBeanResult<entity.InvoiceStream>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=InvoiceStreamFilter_Input) at ProducerAgencyBillCycles.pcf: line 41, column 48
    function verifyValueRangeIsAllowedType_6 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=InvoiceStreamFilter_Input) at ProducerAgencyBillCycles.pcf: line 41, column 48
    function verifyValueRange_7 () : void {
      var __valueRangeArg = producer.InvoiceStreams.sortDescending()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_6(__valueRangeArg)
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=ClearCommissionDifferencesLogic_Input) at ProducerAgencyBillCycles.pcf: line 153, column 132
    function visible_62 () : java.lang.Boolean {
      return selectedAgencyBillCycle.StatementInvoice.AgencyBillCycle.Producer.AgencyBillPlan.LowCommissionCleared
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=ClearGrossDifferencesLogic_Input) at ProducerAgencyBillCycles.pcf: line 160, column 127
    function visible_67 () : java.lang.Boolean {
      return selectedAgencyBillCycle.StatementInvoice.AgencyBillCycle.Producer.AgencyBillPlan.LowGrossCleared
    }
    
    // 'visible' attribute on DateInput (id=MostRecentPromiseDate_Input) at ProducerAgencyBillCycles.pcf: line 172, column 88
    function visible_77 () : java.lang.Boolean {
      return selectedAgencyBillCycle.MostRecentExecutedPromise != null
    }
    
    property get invoiceStreamSelection () : InvoiceStream {
      return getVariableValue("invoiceStreamSelection", 1) as InvoiceStream
    }
    
    property set invoiceStreamSelection ($arg :  InvoiceStream) {
      setVariableValue("invoiceStreamSelection", 1, $arg)
    }
    
    property get selectedAgencyBillCycle () : AgencyBillCycle {
      return getCurrentSelection(1) as AgencyBillCycle
    }
    
    property set selectedAgencyBillCycle ($arg :  AgencyBillCycle) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  
}