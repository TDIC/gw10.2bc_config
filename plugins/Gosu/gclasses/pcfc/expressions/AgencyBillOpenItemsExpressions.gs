package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillOpenItems.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillOpenItemsExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillOpenItems.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillOpenItemsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=PolicyFilterGoButton) at AgencyBillOpenItems.pcf: line 63, column 122
    function action_19 () : void {
      
    }
    
    // 'action' attribute on ToolbarButton (id=PolicyFilterClearButton) at AgencyBillOpenItems.pcf: line 67, column 125
    function action_20 () : void {
      agencyBillStatementView.PolicyPeriodFilter = null
    }
    
    // 'canVisit' attribute on Page (id=AgencyBillOpenItems) at AgencyBillOpenItems.pcf: line 8, column 81
    static function canVisit_27 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodabopenitemsview
    }
    
    // 'def' attribute on PanelRef at AgencyBillOpenItems.pcf: line 30, column 112
    function def_onEnter_22 (def :  pcf.PolicyActivityLV_itemlevel) : void {
      def.onEnter(agencyBillStatementView, viewOption)
    }
    
    // 'def' attribute on PanelRef at AgencyBillOpenItems.pcf: line 30, column 112
    function def_onEnter_24 (def :  pcf.PolicyActivityLV_policylevel) : void {
      def.onEnter(agencyBillStatementView, viewOption)
    }
    
    // 'def' attribute on PanelRef at AgencyBillOpenItems.pcf: line 30, column 112
    function def_refreshVariables_23 (def :  pcf.PolicyActivityLV_itemlevel) : void {
      def.refreshVariables(agencyBillStatementView, viewOption)
    }
    
    // 'def' attribute on PanelRef at AgencyBillOpenItems.pcf: line 30, column 112
    function def_refreshVariables_25 (def :  pcf.PolicyActivityLV_policylevel) : void {
      def.refreshVariables(agencyBillStatementView, viewOption)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=ByItemOrPolicy_Input) at AgencyBillOpenItems.pcf: line 49, column 53
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      showByPolicy = (__VALUE_TO_SET as typekey.StatementViewOption)
    }
    
    // 'value' attribute on TextValue (id=PolicyPeriodFilter) at AgencyBillOpenItems.pcf: line 58, column 43
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillStatementView.PolicyPeriodFilter = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=DisplayMode_Input) at AgencyBillOpenItems.pcf: line 40, column 55
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewOption = (__VALUE_TO_SET as typekey.InvoiceItemViewOption)
    }
    
    // 'initialValue' attribute on Variable at AgencyBillOpenItems.pcf: line 17, column 37
    function initialValue_0 () : InvoiceItemViewOption {
      return InvoiceItemViewOption.TC_BILLED
    }
    
    // 'initialValue' attribute on Variable at AgencyBillOpenItems.pcf: line 21, column 43
    function initialValue_1 () : typekey.StatementViewOption {
      return StatementViewOption.TC_BYITEM
    }
    
    // 'initialValue' attribute on Variable at AgencyBillOpenItems.pcf: line 25, column 58
    function initialValue_2 () : gw.api.web.invoice.AgencyBillStatementView {
      return gw.api.web.invoice.AgencyBillStatementView.createStatementViewForOpenItems(producer)
    }
    
    // 'mode' attribute on PanelRef at AgencyBillOpenItems.pcf: line 30, column 112
    function mode_26 () : java.lang.Object {
      return showByPolicy == StatementViewOption.TC_BYITEM ? "itemlevel" : "policylevel"
    }
    
    // Page (id=AgencyBillOpenItems) at AgencyBillOpenItems.pcf: line 8, column 81
    static function parent_28 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    // 'title' attribute on Page (id=AgencyBillOpenItems) at AgencyBillOpenItems.pcf: line 8, column 81
    static function title_29 (producer :  Producer) : java.lang.Object {
      return DisplayKey.get("Web.AgencyBillOpenItems.Title", producer)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=ByItemOrPolicy_Input) at AgencyBillOpenItems.pcf: line 49, column 53
    function valueRange_11 () : java.lang.Object {
      return StatementViewOption.getTypeKeys( false )
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=DisplayMode_Input) at AgencyBillOpenItems.pcf: line 40, column 55
    function valueRange_5 () : java.lang.Object {
      return InvoiceItemViewOption.getTypeKeys( false )
    }
    
    // 'value' attribute on TextValue (id=PolicyPeriodFilter) at AgencyBillOpenItems.pcf: line 58, column 43
    function valueRoot_17 () : java.lang.Object {
      return agencyBillStatementView
    }
    
    // 'value' attribute on TextValue (id=PolicyPeriodFilter) at AgencyBillOpenItems.pcf: line 58, column 43
    function value_15 () : java.lang.String {
      return agencyBillStatementView.PolicyPeriodFilter
    }
    
    // 'value' attribute on ToolbarRangeInput (id=DisplayMode_Input) at AgencyBillOpenItems.pcf: line 40, column 55
    function value_3 () : typekey.InvoiceItemViewOption {
      return viewOption
    }
    
    // 'value' attribute on ToolbarRangeInput (id=ByItemOrPolicy_Input) at AgencyBillOpenItems.pcf: line 49, column 53
    function value_9 () : typekey.StatementViewOption {
      return showByPolicy
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=ByItemOrPolicy_Input) at AgencyBillOpenItems.pcf: line 49, column 53
    function verifyValueRangeIsAllowedType_12 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=ByItemOrPolicy_Input) at AgencyBillOpenItems.pcf: line 49, column 53
    function verifyValueRangeIsAllowedType_12 ($$arg :  typekey.StatementViewOption[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=DisplayMode_Input) at AgencyBillOpenItems.pcf: line 40, column 55
    function verifyValueRangeIsAllowedType_6 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=DisplayMode_Input) at AgencyBillOpenItems.pcf: line 40, column 55
    function verifyValueRangeIsAllowedType_6 ($$arg :  typekey.InvoiceItemViewOption[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=ByItemOrPolicy_Input) at AgencyBillOpenItems.pcf: line 49, column 53
    function verifyValueRange_13 () : void {
      var __valueRangeArg = StatementViewOption.getTypeKeys( false )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_12(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=DisplayMode_Input) at AgencyBillOpenItems.pcf: line 40, column 55
    function verifyValueRange_7 () : void {
      var __valueRangeArg = InvoiceItemViewOption.getTypeKeys( false )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_6(__valueRangeArg)
    }
    
    // 'visible' attribute on Toolbar (id=PolicyActivityLV_tb) at AgencyBillOpenItems.pcf: line 32, column 53
    function visible_21 () : java.lang.Boolean {
      return agencyBillStatementView != null
    }
    
    override property get CurrentLocation () : pcf.AgencyBillOpenItems {
      return super.CurrentLocation as pcf.AgencyBillOpenItems
    }
    
    property get agencyBillStatementView () : gw.api.web.invoice.AgencyBillStatementView {
      return getVariableValue("agencyBillStatementView", 0) as gw.api.web.invoice.AgencyBillStatementView
    }
    
    property set agencyBillStatementView ($arg :  gw.api.web.invoice.AgencyBillStatementView) {
      setVariableValue("agencyBillStatementView", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    property get showByPolicy () : typekey.StatementViewOption {
      return getVariableValue("showByPolicy", 0) as typekey.StatementViewOption
    }
    
    property set showByPolicy ($arg :  typekey.StatementViewOption) {
      setVariableValue("showByPolicy", 0, $arg)
    }
    
    property get viewOption () : InvoiceItemViewOption {
      return getVariableValue("viewOption", 0) as InvoiceItemViewOption
    }
    
    property set viewOption ($arg :  InvoiceItemViewOption) {
      setVariableValue("viewOption", 0, $arg)
    }
    
    
  }
  
  
}