package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopGroupExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopGroupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 16, column 39
    function action_0 () : void {
      pcf.DesktopActivities.go()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 26, column 42
    function action_10 () : void {
      pcf.DesktopDelinquencies.go()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 28, column 42
    function action_12 () : void {
      pcf.DesktopDisbursements.go()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 30, column 45
    function action_14 () : void {
      pcf.DesktopSuspensePayments.go()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 32, column 40
    function action_16 () : void {
      pcf.DesktopAgencyItems.go()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 34, column 40
    function action_18 () : void {
      pcf.DesktopHeldCharges.go()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 18, column 46
    function action_2 () : void {
      pcf.DesktopCreatedActivities.go()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 36, column 48
    function action_20 () : void {
      pcf.DesktopBatchPaymentsSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 20, column 35
    function action_4 () : void {
      pcf.DesktopQueues.go()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 22, column 38
    function action_6 () : void {
      pcf.DesktopApprovals.go()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 24, column 43
    function action_8 () : void {
      pcf.DesktopTroubleTickets.go()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 16, column 39
    function action_dest_1 () : pcf.api.Destination {
      return pcf.DesktopActivities.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 26, column 42
    function action_dest_11 () : pcf.api.Destination {
      return pcf.DesktopDelinquencies.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 28, column 42
    function action_dest_13 () : pcf.api.Destination {
      return pcf.DesktopDisbursements.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 30, column 45
    function action_dest_15 () : pcf.api.Destination {
      return pcf.DesktopSuspensePayments.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 32, column 40
    function action_dest_17 () : pcf.api.Destination {
      return pcf.DesktopAgencyItems.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 34, column 40
    function action_dest_19 () : pcf.api.Destination {
      return pcf.DesktopHeldCharges.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 36, column 48
    function action_dest_21 () : pcf.api.Destination {
      return pcf.DesktopBatchPaymentsSearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 18, column 46
    function action_dest_3 () : pcf.api.Destination {
      return pcf.DesktopCreatedActivities.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 20, column 35
    function action_dest_5 () : pcf.api.Destination {
      return pcf.DesktopQueues.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 22, column 38
    function action_dest_7 () : pcf.api.Destination {
      return pcf.DesktopApprovals.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 24, column 43
    function action_dest_9 () : pcf.api.Destination {
      return pcf.DesktopTroubleTickets.createDestination()
    }
    
    // 'browserTitle' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 12, column 60
    function browserTitle_22 () : java.lang.String {
      return gw.api.util.DateUtil.currentDate() as String
    }
    
    // 'canVisit' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 12, column 60
    static function canVisit_23 () : java.lang.Boolean {
      return perm.System.viewdesktop
    }
    
    // LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 12, column 60
    static function firstVisitableChildDestinationMethod_30 () : pcf.api.Destination {
      var dest : pcf.api.Destination
      dest = pcf.DesktopActivities.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.DesktopCreatedActivities.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.DesktopQueues.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.DesktopApprovals.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.DesktopTroubleTickets.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.DesktopDelinquencies.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.DesktopDisbursements.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.DesktopSuspensePayments.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.DesktopAgencyItems.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.DesktopHeldCharges.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.DesktopBatchPaymentsSearch.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      return null
    }
    
    // 'infoBar' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 12, column 60
    function infoBar_onEnter_24 (def :  pcf.CurrentDateInfoBar) : void {
      def.onEnter()
    }
    
    // 'infoBar' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 12, column 60
    function infoBar_refreshVariables_25 (def :  pcf.CurrentDateInfoBar) : void {
      def.refreshVariables()
    }
    
    // 'menuActions' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 12, column 60
    function menuActions_onEnter_26 (def :  pcf.DesktopMenuActions) : void {
      def.onEnter()
    }
    
    // 'menuActions' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 12, column 60
    function menuActions_refreshVariables_27 (def :  pcf.DesktopMenuActions) : void {
      def.refreshVariables()
    }
    
    // 'tabBar' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 12, column 60
    function tabBar_onEnter_28 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on LocationGroup (id=DesktopGroup) at DesktopGroup.pcf: line 12, column 60
    function tabBar_refreshVariables_29 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.DesktopGroup {
      return super.CurrentLocation as pcf.DesktopGroup
    }
    
    
  }
  
  
}