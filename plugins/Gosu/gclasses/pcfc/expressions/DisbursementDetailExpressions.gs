package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/DisbursementDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DisbursementDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/disbursement/DisbursementDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DisbursementDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (disbursement :  Disbursement, startInEdit :  Boolean) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=Update) at DisbursementDetail.pcf: line 28, column 78
    function action_0 () : void {
      CurrentLocation.commit(); DesktopDisbursements.go()
    }
    
    // 'action' attribute on ToolbarButton (id=Cancel) at DisbursementDetail.pcf: line 33, column 78
    function action_1 () : void {
      CurrentLocation.cancel(); DesktopDisbursements.go()
    }
    
    // 'action' attribute on ToolbarButton (id=Void) at DisbursementDetail.pcf: line 66, column 76
    function action_11 () : void {
      VoidDisbursementPopup.push(disbursement)
    }
    
    // 'action' attribute on ToolbarButton (id=ToolbarButton) at DisbursementDetail.pcf: line 39, column 68
    function action_3 () : void {
      assignReviewedBy() ; CurrentLocation.commit();
    }
    
    // 'action' attribute on ToolbarButton (id=Approve) at DisbursementDetail.pcf: line 45, column 79
    function action_5 () : void {
      disbursement.getOpenApprovalActivity().approve(); setapprover() ; CurrentLocation.commit(); DesktopDisbursements.go();
    }
    
    // 'action' attribute on ToolbarButton (id=Reject) at DisbursementDetail.pcf: line 52, column 78
    function action_7 () : void {
      disbursement.rejectDisbursement(); CurrentLocation.commit(); DesktopDisbursements.go();
    }
    
    // 'action' attribute on ToolbarButton (id=RejectWithHold) at DisbursementDetail.pcf: line 59, column 86
    function action_9 () : void {
      disbursement.rejectDisbursementAndHold(); CurrentLocation.commit(); DesktopDisbursements.go();
    }
    
    // 'action' attribute on ToolbarButton (id=Void) at DisbursementDetail.pcf: line 66, column 76
    function action_dest_12 () : pcf.api.Destination {
      return pcf.VoidDisbursementPopup.createDestination(disbursement)
    }
    
    // 'afterReturnFromPopup' attribute on Page (id=DisbursementDetail) at DisbursementDetail.pcf: line 12, column 70
    function afterReturnFromPopup_15 (popupCommitted :  boolean) : void {
      if (popupCommitted) CurrentLocation.commit()
    }
    
    // 'available' attribute on ToolbarButton (id=Void) at DisbursementDetail.pcf: line 66, column 76
    function available_10 () : java.lang.Boolean {
      return disbursement.canVoid()
    }
    
    // 'available' attribute on ToolbarButton (id=ToolbarButton) at DisbursementDetail.pcf: line 39, column 68
    function available_2 () : java.lang.Boolean {
      return !disbursement.Reviewed_TDIC and perm.System.disbreview_TDIC
    }
    
    // 'available' attribute on ToolbarButton (id=Approve) at DisbursementDetail.pcf: line 45, column 79
    function available_4 () : java.lang.Boolean {
      return disbursement.canApprove()
    }
    
    // 'available' attribute on ToolbarButton (id=Reject) at DisbursementDetail.pcf: line 52, column 78
    function available_6 () : java.lang.Boolean {
      return disbursement.canReject()
    }
    
    // 'available' attribute on ToolbarButton (id=RejectWithHold) at DisbursementDetail.pcf: line 59, column 86
    function available_8 () : java.lang.Boolean {
      return disbursement.canRejectAndHold()
    }
    
    // 'canEdit' attribute on Page (id=DisbursementDetail) at DisbursementDetail.pcf: line 12, column 70
    function canEdit_16 () : java.lang.Boolean {
      return perm.System.disbcreate && disbursement.Status != DisbursementStatus.TC_REAPPLIED && disbursement.Status != DisbursementStatus.TC_REJECTED && disbursement.Status != DisbursementStatus.TC_VOIDED
    }
    
    // 'def' attribute on PanelRef at DisbursementDetail.pcf: line 69, column 51
    function def_onEnter_13 (def :  pcf.DisbursementDetailDV) : void {
      def.onEnter(disbursement)
    }
    
    // 'def' attribute on PanelRef at DisbursementDetail.pcf: line 69, column 51
    function def_refreshVariables_14 (def :  pcf.DisbursementDetailDV) : void {
      def.refreshVariables(disbursement)
    }
    
    // 'parent' attribute on Page (id=DisbursementDetail) at DisbursementDetail.pcf: line 12, column 70
    static function parent_17 (disbursement :  Disbursement, startInEdit :  Boolean) : pcf.api.Destination {
      return pcf.DesktopDisbursements.createDestination()
    }
    
    // 'startInEditMode' attribute on Page (id=DisbursementDetail) at DisbursementDetail.pcf: line 12, column 70
    function startInEditMode_18 () : java.lang.Boolean {
      return startInEdit
    }
    
    override property get CurrentLocation () : pcf.DisbursementDetail {
      return super.CurrentLocation as pcf.DisbursementDetail
    }
    
    property get disbursement () : Disbursement {
      return getVariableValue("disbursement", 0) as Disbursement
    }
    
    property set disbursement ($arg :  Disbursement) {
      setVariableValue("disbursement", 0, $arg)
    }
    
    property get startInEdit () : Boolean {
      return getVariableValue("startInEdit", 0) as Boolean
    }
    
    property set startInEdit ($arg :  Boolean) {
      setVariableValue("startInEdit", 0, $arg)
    }
    
    function assignReviewedBy() {
      disbursement.Reviewed_TDIC = true
      disbursement.ReviewedBy_TDIC = User.util.CurrentUser
    }
    function setapprover() {
      disbursement.Approver_TDIC = User.util.CurrentUser
    }
    
    
  }
  
  
}