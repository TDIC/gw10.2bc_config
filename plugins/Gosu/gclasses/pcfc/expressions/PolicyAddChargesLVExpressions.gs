package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyAddChargesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyAddChargesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyAddChargesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyAddChargesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at PolicyAddChargesLV.pcf: line 66, column 63
    function action_27 () : void {
      AccountSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at PolicyAddChargesLV.pcf: line 66, column 63
    function action_dest_28 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at PolicyAddChargesLV.pcf: line 96, column 38
    function currency_54 () : typekey.Currency {
      return billingInstruction.Currency
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at PolicyAddChargesLV.pcf: line 50, column 44
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      initializer.ChargePattern = (__VALUE_TO_SET as entity.ChargePattern)
    }
    
    // 'value' attribute on TextCell (id=ChargeGroup_Cell) at PolicyAddChargesLV.pcf: line 57, column 44
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      initializer.ChargeGroup = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=Payer_Cell) at PolicyAddChargesLV.pcf: line 66, column 63
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeToInvoicingOverridesViewMap.get(initializer).OverridingPayerAccount = (__VALUE_TO_SET as entity.Account)
    }
    
    // 'value' attribute on RangeCell (id=InvoiceStream_Cell) at PolicyAddChargesLV.pcf: line 84, column 101
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeToInvoicingOverridesViewMap.get(initializer).OverridingInvoiceStream = (__VALUE_TO_SET as entity.InvoiceStream)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at PolicyAddChargesLV.pcf: line 96, column 38
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      initializer.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'editable' attribute on RangeCell (id=InvoiceStream_Cell) at PolicyAddChargesLV.pcf: line 84, column 101
    function editable_37 () : java.lang.Boolean {
      return !view.isPayerAccountNull(initializer)
    }
    
    // 'inputConversion' attribute on TextCell (id=Payer_Cell) at PolicyAddChargesLV.pcf: line 66, column 63
    function inputConversion_30 (VALUE :  java.lang.String) : java.lang.Object {
      return accountSearchConverter.getAccount(VALUE)
    }
    
    // 'noneSelectedLabel' attribute on RangeCell (id=InvoiceStream_Cell) at PolicyAddChargesLV.pcf: line 84, column 101
    function noneSelectedLabel_42 () : java.lang.String {
      return null
    }
    
    // 'onChange' attribute on PostOnChange at PolicyAddChargesLV.pcf: line 68, column 85
    function onChange_26 () : void {
      chargeToInvoicingOverridesViewMap.get(initializer).update()
    }
    
    // 'onChange' attribute on PostOnChange at PolicyAddChargesLV.pcf: line 86, column 85
    function onChange_36 () : void {
      chargeToInvoicingOverridesViewMap.get(initializer).update()
    }
    
    // 'onPick' attribute on TextCell (id=Payer_Cell) at PolicyAddChargesLV.pcf: line 66, column 63
    function onPick_29 (PickedValue :  Account) : void {
      chargeToInvoicingOverridesViewMap.get(initializer).update()
    }
    
    // 'optionLabel' attribute on RangeCell (id=Type_Cell) at PolicyAddChargesLV.pcf: line 50, column 44
    function optionLabel_17 (VALUE :  entity.ChargePattern) : java.lang.String {
      return VALUE.ChargeName
    }
    
    // 'validationExpression' attribute on RangeCell (id=InvoiceStream_Cell) at PolicyAddChargesLV.pcf: line 84, column 101
    function validationExpression_38 () : java.lang.Object {
      return view.overridingInvoiceStreamValidation(initializer)
    }
    
    // 'validationExpression' attribute on MonetaryAmountCell (id=Amount_Cell) at PolicyAddChargesLV.pcf: line 96, column 38
    function validationExpression_50 () : java.lang.Object {
      return view.chargeAmountValidation(initializer)
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at PolicyAddChargesLV.pcf: line 50, column 44
    function valueRange_18 () : java.lang.Object {
      return chargePatterns
    }
    
    // 'valueRange' attribute on RangeCell (id=InvoiceStream_Cell) at PolicyAddChargesLV.pcf: line 84, column 101
    function valueRange_43 () : java.lang.Object {
      return view.getEligibleInvoiceStreams(initializer)
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at PolicyAddChargesLV.pcf: line 50, column 44
    function valueRoot_16 () : java.lang.Object {
      return initializer
    }
    
    // 'value' attribute on TextCell (id=Payer_Cell) at PolicyAddChargesLV.pcf: line 66, column 63
    function valueRoot_33 () : java.lang.Object {
      return chargeToInvoicingOverridesViewMap.get(initializer)
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at PolicyAddChargesLV.pcf: line 50, column 44
    function value_14 () : entity.ChargePattern {
      return initializer.ChargePattern
    }
    
    // 'value' attribute on TextCell (id=ChargeGroup_Cell) at PolicyAddChargesLV.pcf: line 57, column 44
    function value_22 () : java.lang.String {
      return initializer.ChargeGroup
    }
    
    // 'value' attribute on TextCell (id=Payer_Cell) at PolicyAddChargesLV.pcf: line 66, column 63
    function value_31 () : entity.Account {
      return chargeToInvoicingOverridesViewMap.get(initializer).OverridingPayerAccount
    }
    
    // 'value' attribute on RangeCell (id=InvoiceStream_Cell) at PolicyAddChargesLV.pcf: line 84, column 101
    function value_39 () : entity.InvoiceStream {
      return chargeToInvoicingOverridesViewMap.get(initializer).OverridingInvoiceStream
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at PolicyAddChargesLV.pcf: line 96, column 38
    function value_51 () : gw.pl.currency.MonetaryAmount {
      return initializer.Amount
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at PolicyAddChargesLV.pcf: line 50, column 44
    function verifyValueRangeIsAllowedType_19 ($$arg :  entity.ChargePattern[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at PolicyAddChargesLV.pcf: line 50, column 44
    function verifyValueRangeIsAllowedType_19 ($$arg :  gw.api.database.IQueryBeanResult<entity.ChargePattern>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at PolicyAddChargesLV.pcf: line 50, column 44
    function verifyValueRangeIsAllowedType_19 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=InvoiceStream_Cell) at PolicyAddChargesLV.pcf: line 84, column 101
    function verifyValueRangeIsAllowedType_44 ($$arg :  entity.InvoiceStream[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=InvoiceStream_Cell) at PolicyAddChargesLV.pcf: line 84, column 101
    function verifyValueRangeIsAllowedType_44 ($$arg :  gw.api.database.IQueryBeanResult<entity.InvoiceStream>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=InvoiceStream_Cell) at PolicyAddChargesLV.pcf: line 84, column 101
    function verifyValueRangeIsAllowedType_44 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at PolicyAddChargesLV.pcf: line 50, column 44
    function verifyValueRange_20 () : void {
      var __valueRangeArg = chargePatterns
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_19(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=InvoiceStream_Cell) at PolicyAddChargesLV.pcf: line 84, column 101
    function verifyValueRange_45 () : void {
      var __valueRangeArg = view.getEligibleInvoiceStreams(initializer)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_44(__valueRangeArg)
    }
    
    // 'visible' attribute on TextCell (id=Payer_Cell) at PolicyAddChargesLV.pcf: line 66, column 63
    function visible_34 () : java.lang.Boolean {
      return chargeToInvoicingOverridesViewMap != null
    }
    
    // 'visible' attribute on RangeCell (id=InvoiceStream_Cell) at PolicyAddChargesLV.pcf: line 84, column 101
    function visible_48 () : java.lang.Boolean {
      return chargeToInvoicingOverridesViewMap != null and view.areAnyChargePayersAccounts()
    }
    
    property get initializer () : gw.api.domain.charge.ChargeInitializer {
      return getIteratedValue(1) as gw.api.domain.charge.ChargeInitializer
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PolicyAddChargesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyAddChargesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at PolicyAddChargesLV.pcf: line 19, column 57
    function initialValue_0 () : gw.api.web.account.AccountSearchConverter {
      return new gw.api.web.account.AccountSearchConverter()
    }
    
    // 'initialValue' attribute on Variable at PolicyAddChargesLV.pcf: line 26, column 41
    function initialValue_1 () : List<ChargePattern> {
      return gw.api.web.accounting.ChargePatternHelper.getChargePatterns(entity.PolicyPeriod)
    }
    
    // 'initialValue' attribute on Variable at PolicyAddChargesLV.pcf: line 30, column 52
    function initialValue_2 () : gw.web.policy.PolicyAddChargesLvView {
      return new gw.web.policy.PolicyAddChargesLvView(billingInstruction, policyPeriod, chargeToInvoicingOverridesViewMap)
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at PolicyAddChargesLV.pcf: line 50, column 44
    function sortValue_3 (initializer :  gw.api.domain.charge.ChargeInitializer) : java.lang.Object {
      return initializer.ChargePattern
    }
    
    // 'value' attribute on TextCell (id=ChargeGroup_Cell) at PolicyAddChargesLV.pcf: line 57, column 44
    function sortValue_4 (initializer :  gw.api.domain.charge.ChargeInitializer) : java.lang.Object {
      return initializer.ChargeGroup
    }
    
    // 'value' attribute on TextCell (id=Payer_Cell) at PolicyAddChargesLV.pcf: line 66, column 63
    function sortValue_5 (initializer :  gw.api.domain.charge.ChargeInitializer) : java.lang.Object {
      return chargeToInvoicingOverridesViewMap.get(initializer).OverridingPayerAccount
    }
    
    // 'value' attribute on RangeCell (id=InvoiceStream_Cell) at PolicyAddChargesLV.pcf: line 84, column 101
    function sortValue_7 (initializer :  gw.api.domain.charge.ChargeInitializer) : java.lang.Object {
      return chargeToInvoicingOverridesViewMap.get(initializer).OverridingInvoiceStream
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at PolicyAddChargesLV.pcf: line 96, column 38
    function sortValue_9 (initializer :  gw.api.domain.charge.ChargeInitializer) : java.lang.Object {
      return initializer.Amount
    }
    
    // '$$sumValue' attribute on RowIterator (id=ChargeIterator) at PolicyAddChargesLV.pcf: line 96, column 38
    function sumValueRoot_13 (initializer :  gw.api.domain.charge.ChargeInitializer) : java.lang.Object {
      return initializer
    }
    
    // 'footerSumValue' attribute on RowIterator (id=ChargeIterator) at PolicyAddChargesLV.pcf: line 96, column 38
    function sumValue_12 (initializer :  gw.api.domain.charge.ChargeInitializer) : java.lang.Object {
      return initializer.Amount
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=ChargeIterator) at PolicyAddChargesLV.pcf: line 39, column 80
    function toCreateAndAdd_57 () : gw.api.domain.charge.ChargeInitializer {
      return view.addInitializer()
    }
    
    // 'toRemove' attribute on RowIterator (id=ChargeIterator) at PolicyAddChargesLV.pcf: line 39, column 80
    function toRemove_58 (initializer :  gw.api.domain.charge.ChargeInitializer) : void {
      view.removeInitializer(initializer)
    }
    
    // 'value' attribute on RowIterator (id=ChargeIterator) at PolicyAddChargesLV.pcf: line 39, column 80
    function value_59 () : java.util.List<gw.api.domain.charge.ChargeInitializer> {
      return billingInstruction.ChargeInitializers
    }
    
    // 'visible' attribute on TextCell (id=Payer_Cell) at PolicyAddChargesLV.pcf: line 66, column 63
    function visible_6 () : java.lang.Boolean {
      return chargeToInvoicingOverridesViewMap != null
    }
    
    // 'visible' attribute on RangeCell (id=InvoiceStream_Cell) at PolicyAddChargesLV.pcf: line 84, column 101
    function visible_8 () : java.lang.Boolean {
      return chargeToInvoicingOverridesViewMap != null and view.areAnyChargePayersAccounts()
    }
    
    property get accountSearchConverter () : gw.api.web.account.AccountSearchConverter {
      return getVariableValue("accountSearchConverter", 0) as gw.api.web.account.AccountSearchConverter
    }
    
    property set accountSearchConverter ($arg :  gw.api.web.account.AccountSearchConverter) {
      setVariableValue("accountSearchConverter", 0, $arg)
    }
    
    property get billingInstruction () : BillingInstruction {
      return getRequireValue("billingInstruction", 0) as BillingInstruction
    }
    
    property set billingInstruction ($arg :  BillingInstruction) {
      setRequireValue("billingInstruction", 0, $arg)
    }
    
    property get chargePatterns () : List<ChargePattern> {
      return getVariableValue("chargePatterns", 0) as List<ChargePattern>
    }
    
    property set chargePatterns ($arg :  List<ChargePattern>) {
      setVariableValue("chargePatterns", 0, $arg)
    }
    
    property get chargeToInvoicingOverridesViewMap () : java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView> {
      return getRequireValue("chargeToInvoicingOverridesViewMap", 0) as java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>
    }
    
    property set chargeToInvoicingOverridesViewMap ($arg :  java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>) {
      setRequireValue("chargeToInvoicingOverridesViewMap", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get view () : gw.web.policy.PolicyAddChargesLvView {
      return getVariableValue("view", 0) as gw.web.policy.PolicyAddChargesLvView
    }
    
    property set view ($arg :  gw.web.policy.PolicyAddChargesLvView) {
      setVariableValue("view", 0, $arg)
    }
    
    
  }
  
  
}