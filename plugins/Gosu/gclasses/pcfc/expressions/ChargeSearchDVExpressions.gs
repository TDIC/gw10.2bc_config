package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ChargeSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChargeSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/ChargeSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChargeSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at ChargeSearchDV.pcf: line 81, column 36
    function action_40 () : void {
      AccountSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=PolicyPicker) at ChargeSearchDV.pcf: line 94, column 41
    function action_49 () : void {
      PolicySearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at ChargeSearchDV.pcf: line 81, column 36
    function action_dest_41 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=PolicyPicker) at ChargeSearchDV.pcf: line 94, column 41
    function action_dest_50 () : pcf.api.Destination {
      return pcf.PolicySearchPopup.createDestination()
    }
    
    // 'available' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at ChargeSearchDV.pcf: line 49, column 41
    function available_18 () : java.lang.Boolean {
      return chargeSearch.Currency != null || accountEditable == false
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at ChargeSearchDV.pcf: line 49, column 41
    function currency_22 () : typekey.Currency {
      return getAmountFieldCurrency()
    }
    
    // 'def' attribute on InputSetRef at ChargeSearchDV.pcf: line 104, column 41
    function def_onEnter_56 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at ChargeSearchDV.pcf: line 104, column 41
    function def_refreshVariables_57 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at ChargeSearchDV.pcf: line 39, column 66
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeSearch.Currency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at ChargeSearchDV.pcf: line 49, column 41
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeSearch.MinAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at ChargeSearchDV.pcf: line 56, column 41
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeSearch.MaxAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on RangeInput (id=ChargePatternCriterion_Input) at ChargeSearchDV.pcf: line 32, column 43
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeSearch.ChargePattern = (__VALUE_TO_SET as entity.ChargePattern)
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at ChargeSearchDV.pcf: line 64, column 44
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeSearch.EarliestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at ChargeSearchDV.pcf: line 69, column 42
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeSearch.LatestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=AccountCriterion_Input) at ChargeSearchDV.pcf: line 81, column 36
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeSearch.Account = (__VALUE_TO_SET as entity.Account)
    }
    
    // 'value' attribute on TextInput (id=PolicyPeriodCriterion_Input) at ChargeSearchDV.pcf: line 94, column 41
    function defaultSetter_53 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeSearch.PolicyPeriod = (__VALUE_TO_SET as entity.PolicyPeriod)
    }
    
    // 'editable' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at ChargeSearchDV.pcf: line 39, column 66
    function editable_10 () : java.lang.Boolean {
      return accountEditable && gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'editable' attribute on TextInput (id=AccountCriterion_Input) at ChargeSearchDV.pcf: line 81, column 36
    function editable_42 () : java.lang.Boolean {
      return accountEditable
    }
    
    // 'initialValue' attribute on Variable at ChargeSearchDV.pcf: line 16, column 57
    function initialValue_0 () : gw.api.web.account.AccountSearchConverter {
      return new gw.api.web.account.AccountSearchConverter(StringCriterionMode.TC_STARTSWITH)
    }
    
    // 'initialValue' attribute on Variable at ChargeSearchDV.pcf: line 20, column 55
    function initialValue_1 () : gw.api.web.policy.PolicySearchConverter {
      return new gw.api.web.policy.PolicySearchConverter(StringCriterionMode.TC_STARTSWITH)
    }
    
    // 'inputConversion' attribute on TextInput (id=AccountCriterion_Input) at ChargeSearchDV.pcf: line 81, column 36
    function inputConversion_43 (VALUE :  java.lang.String) : java.lang.Object {
      return accountSearchConverter.getAccount(VALUE)
    }
    
    // 'inputConversion' attribute on TextInput (id=PolicyPeriodCriterion_Input) at ChargeSearchDV.pcf: line 94, column 41
    function inputConversion_51 (VALUE :  java.lang.String) : java.lang.Object {
      return policySearchConverter.getPolicyPeriod(VALUE)
    }
    
    // 'onChange' attribute on PostOnChange at ChargeSearchDV.pcf: line 41, column 54
    function onChange_9 () : void {
      blankMinimumAndMaximumFields()
    }
    
    // 'valueRange' attribute on RangeInput (id=ChargePatternCriterion_Input) at ChargeSearchDV.pcf: line 32, column 43
    function valueRange_5 () : java.lang.Object {
      return gw.api.web.accounting.ChargePatternHelper.getReversableChargePatterns()
    }
    
    // 'value' attribute on RangeInput (id=ChargePatternCriterion_Input) at ChargeSearchDV.pcf: line 32, column 43
    function valueRoot_4 () : java.lang.Object {
      return chargeSearch
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at ChargeSearchDV.pcf: line 39, column 66
    function value_12 () : typekey.Currency {
      return chargeSearch.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at ChargeSearchDV.pcf: line 49, column 41
    function value_19 () : gw.pl.currency.MonetaryAmount {
      return chargeSearch.MinAmount
    }
    
    // 'value' attribute on RangeInput (id=ChargePatternCriterion_Input) at ChargeSearchDV.pcf: line 32, column 43
    function value_2 () : entity.ChargePattern {
      return chargeSearch.ChargePattern
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at ChargeSearchDV.pcf: line 56, column 41
    function value_26 () : gw.pl.currency.MonetaryAmount {
      return chargeSearch.MaxAmount
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at ChargeSearchDV.pcf: line 64, column 44
    function value_32 () : java.util.Date {
      return chargeSearch.EarliestDate
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at ChargeSearchDV.pcf: line 69, column 42
    function value_36 () : java.util.Date {
      return chargeSearch.LatestDate
    }
    
    // 'value' attribute on TextInput (id=AccountCriterion_Input) at ChargeSearchDV.pcf: line 81, column 36
    function value_44 () : entity.Account {
      return chargeSearch.Account
    }
    
    // 'value' attribute on TextInput (id=PolicyPeriodCriterion_Input) at ChargeSearchDV.pcf: line 94, column 41
    function value_52 () : entity.PolicyPeriod {
      return chargeSearch.PolicyPeriod
    }
    
    // 'valueRange' attribute on RangeInput (id=ChargePatternCriterion_Input) at ChargeSearchDV.pcf: line 32, column 43
    function verifyValueRangeIsAllowedType_6 ($$arg :  entity.ChargePattern[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ChargePatternCriterion_Input) at ChargeSearchDV.pcf: line 32, column 43
    function verifyValueRangeIsAllowedType_6 ($$arg :  gw.api.database.IQueryBeanResult<entity.ChargePattern>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ChargePatternCriterion_Input) at ChargeSearchDV.pcf: line 32, column 43
    function verifyValueRangeIsAllowedType_6 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ChargePatternCriterion_Input) at ChargeSearchDV.pcf: line 32, column 43
    function verifyValueRange_7 () : void {
      var __valueRangeArg = gw.api.web.accounting.ChargePatternHelper.getReversableChargePatterns()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_6(__valueRangeArg)
    }
    
    // 'visible' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at ChargeSearchDV.pcf: line 39, column 66
    function visible_11 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    property get accountEditable () : Boolean {
      return getRequireValue("accountEditable", 0) as Boolean
    }
    
    property set accountEditable ($arg :  Boolean) {
      setRequireValue("accountEditable", 0, $arg)
    }
    
    property get accountSearchConverter () : gw.api.web.account.AccountSearchConverter {
      return getVariableValue("accountSearchConverter", 0) as gw.api.web.account.AccountSearchConverter
    }
    
    property set accountSearchConverter ($arg :  gw.api.web.account.AccountSearchConverter) {
      setVariableValue("accountSearchConverter", 0, $arg)
    }
    
    property get chargeSearch () : gw.search.ReversibleChargeSearchCriteria {
      return getRequireValue("chargeSearch", 0) as gw.search.ReversibleChargeSearchCriteria
    }
    
    property set chargeSearch ($arg :  gw.search.ReversibleChargeSearchCriteria) {
      setRequireValue("chargeSearch", 0, $arg)
    }
    
    property get policySearchConverter () : gw.api.web.policy.PolicySearchConverter {
      return getVariableValue("policySearchConverter", 0) as gw.api.web.policy.PolicySearchConverter
    }
    
    property set policySearchConverter ($arg :  gw.api.web.policy.PolicySearchConverter) {
      setVariableValue("policySearchConverter", 0, $arg)
    }
    
    function getAmountFieldCurrency() : Currency
    {
      if (accountEditable) {
        return gw.search.SearchHelper.getDefaultCurrencyIfNull(chargeSearch.Currency)
      }
      else {
        return chargeSearch.Account.Currency
      }
    }
    
    function blankMinimumAndMaximumFields() {
      chargeSearch.MinAmount = null
      chargeSearch.MaxAmount = null
    }
    
    
  }
  
  
}