package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.EquityValidationHelper
uses gw.acc.acct360.accountview.AccountBalanceTxnUtil
@javax.annotation.Generated("config/web/pcf/invoice/EditInvoiceItemsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class EditInvoiceItemsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/invoice/EditInvoiceItemsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class EditInvoiceItemsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (charge :  Charge) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=EditInvoiceItemsPopup) at EditInvoiceItemsPopup.pcf: line 10, column 66
    function beforeCommit_86 (pickedValue :  java.lang.Object) : void {
      performAcct360Tracking();changer.execute()
    }
    
    // 'updateConfirmMessage' attribute on EditButtons at EditInvoiceItemsPopup.pcf: line 33, column 117
    function confirmMessage_3 () : java.lang.String {
      return gw.api.util.EquityValidationHelper.isEquityViolated(charge.PolicyPeriod, changer)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at EditInvoiceItemsPopup.pcf: line 57, column 36
    function currency_15 () : typekey.Currency {
      return currency
    }
    
    // 'initialValue' attribute on Variable at EditInvoiceItemsPopup.pcf: line 19, column 62
    function initialValue_0 () : gw.api.domain.invoice.ChargeInstallmentChanger {
      return new gw.api.domain.invoice.ChargeInstallmentChanger(charge)
    }
    
    // 'initialValue' attribute on Variable at EditInvoiceItemsPopup.pcf: line 23, column 32
    function initialValue_1 () : typekey.Currency {
      return charge.Currency
    }
    
    // 'initialValue' attribute on Variable at EditInvoiceItemsPopup.pcf: line 27, column 41
    function initialValue_2 () : typekey.InvoiceItemType[] {
      return gw.api.web.invoice.InvoiceUtil.getUserVisibleInvoiceItemTypes()
    }
    
    // EditButtons at EditInvoiceItemsPopup.pcf: line 33, column 117
    function label_4 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'showUpdateConfirmMessage' attribute on EditButtons at EditInvoiceItemsPopup.pcf: line 33, column 117
    function showConfirmMessage_5 () : java.lang.Boolean {
      return changer.isValid()
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at EditInvoiceItemsPopup.pcf: line 90, column 33
    function sortValue_18 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.InvoiceItem.InstallmentNumber
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at EditInvoiceItemsPopup.pcf: line 98, column 34
    function sortValue_19 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.EventDate
    }
    
    // 'value' attribute on PickerCell (id=Invoice_Cell) at EditInvoiceItemsPopup.pcf: line 111, column 31
    function sortValue_20 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.Invoice
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at EditInvoiceItemsPopup.pcf: line 116, column 35
    function sortValue_21 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.InvoiceItem.Invoice == null ? entry.Invoice.PaymentDueDate : entry.InvoiceItem.InvoiceDueDate
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at EditInvoiceItemsPopup.pcf: line 132, column 31
    function sortValue_22 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.InvoiceItemType
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at EditInvoiceItemsPopup.pcf: line 141, column 38
    function sortValue_23 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PaidAmount_Cell) at EditInvoiceItemsPopup.pcf: line 152, column 55
    function sortValue_24 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.InvoiceItem.PaidAmount
    }
    
    // 'value' attribute on TextCell (id=EditType_Cell) at EditInvoiceItemsPopup.pcf: line 157, column 44
    function sortValue_25 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.DisplayName
    }
    
    // '$$sumValue' attribute on RowIterator at EditInvoiceItemsPopup.pcf: line 141, column 38
    function sumValueRoot_27 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry
    }
    
    // '$$sumValue' attribute on RowIterator at EditInvoiceItemsPopup.pcf: line 152, column 55
    function sumValueRoot_29 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.InvoiceItem
    }
    
    // 'footerSumValue' attribute on RowIterator at EditInvoiceItemsPopup.pcf: line 141, column 38
    function sumValue_26 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.Amount
    }
    
    // 'footerSumValue' attribute on RowIterator at EditInvoiceItemsPopup.pcf: line 152, column 55
    function sumValue_28 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.InvoiceItem.PaidAmount
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at EditInvoiceItemsPopup.pcf: line 79, column 100
    function toCreateAndAdd_84 () : gw.api.domain.invoice.ChargeInstallmentChanger.Entry {
      return changer.newEntry()
    }
    
    // 'value' attribute on TextInput (id=itemOwner_Input) at EditInvoiceItemsPopup.pcf: line 47, column 41
    function valueRoot_8 () : java.lang.Object {
      return charge
    }
    
    // 'value' attribute on TextInput (id=Context_Input) at EditInvoiceItemsPopup.pcf: line 52, column 52
    function value_10 () : entity.BillingInstruction {
      return charge.BillingInstruction
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at EditInvoiceItemsPopup.pcf: line 57, column 36
    function value_13 () : gw.pl.currency.MonetaryAmount {
      return charge.Amount
    }
    
    // 'value' attribute on TextInput (id=itemOwner_Input) at EditInvoiceItemsPopup.pcf: line 47, column 41
    function value_7 () : entity.Account {
      return charge.OwnerAccount
    }
    
    // 'value' attribute on RowIterator at EditInvoiceItemsPopup.pcf: line 79, column 100
    function value_85 () : java.util.List<gw.api.domain.invoice.ChargeInstallmentChanger.Entry> {
      return changer.Entries
    }
    
    // 'addVisible' attribute on IteratorButtons at EditInvoiceItemsPopup.pcf: line 39, column 45
    function visible_6 () : java.lang.Boolean {
      return !charge.Reversed && !charge.Reversal
    }
    
    override property get CurrentLocation () : pcf.EditInvoiceItemsPopup {
      return super.CurrentLocation as pcf.EditInvoiceItemsPopup
    }
    
    property get availableTypes () : typekey.InvoiceItemType[] {
      return getVariableValue("availableTypes", 0) as typekey.InvoiceItemType[]
    }
    
    property set availableTypes ($arg :  typekey.InvoiceItemType[]) {
      setVariableValue("availableTypes", 0, $arg)
    }
    
    property get changer () : gw.api.domain.invoice.ChargeInstallmentChanger {
      return getVariableValue("changer", 0) as gw.api.domain.invoice.ChargeInstallmentChanger
    }
    
    property set changer ($arg :  gw.api.domain.invoice.ChargeInstallmentChanger) {
      setVariableValue("changer", 0, $arg)
    }
    
    property get charge () : Charge {
      return getVariableValue("charge", 0) as Charge
    }
    
    property set charge ($arg :  Charge) {
      setVariableValue("charge", 0, $arg)
    }
    
    property get currency () : typekey.Currency {
      return getVariableValue("currency", 0) as typekey.Currency
    }
    
    property set currency ($arg :  typekey.Currency) {
      setVariableValue("currency", 0, $arg)
    }
    
    
        function getPayerDisplay(itemEntry: gw.api.domain.invoice.ChargeInstallmentChanger.Entry): String {
          var invoiceHolder = itemEntry.Invoice == null ? charge.DefaultPayer : itemEntry.Invoice.Payer
    
          if (typeof invoiceHolder == Account) {
            return invoiceHolder as String
          } else {
            var itemProducerCode = itemEntry.InvoiceItem.ActivePrimaryItemCommission.PolicyCommission.ProducerCode
            var chargeProducerCode = charge.DefaultPrimaryEarningProducerCode
    
            return itemProducerCode == null ? formatProducerCodeDisplay(chargeProducerCode) : formatProducerCodeDisplay(itemProducerCode)
          }
        }
    
        function formatProducerCodeDisplay(producerCode: ProducerCode): String {
          return producerCode.Producer.DisplayName + " " + producerCode.Code
        }
    
        function removeOrReverseAndInvalidateIterator(itemEntry: gw.api.domain.invoice.ChargeInstallmentChanger.Entry) {
          if (itemEntry.canReverseButNotRemove()) itemEntry.reverse()
          else itemEntry.remove()
            // There is some ND behavior with adding/editing and then reversing, so go ahead and reset
            gw.api.web.PebblesUtil.invalidateIterators(CurrentLocation, gw.api.domain.invoice.ChargeInstallmentChanger.Entry)
          }
    
          function performAcct360Tracking() {
            var invoiceItems: InvoiceItem[]
            var reversedEntries = changer.Entries.where(\entry -> entry.ReversedItem && entry.InvoiceItem.Invoice.BilledOrDue && !entry.InvoiceItem.AgencyBill && entry.InvoiceItem.Payer == charge.OwnerAccount)
            invoiceItems = reversedEntries*.InvoiceItem
            AccountBalanceTxnUtil.createMoveItemRecord(charge.OwnerAccount, invoiceItems, AcctBalItemMovedType_Ext.TC_INVOICEITEMEDIT)
        }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/invoice/EditInvoiceItemsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends EditInvoiceItemsPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=Invoice_Cell) at EditInvoiceItemsPopup.pcf: line 111, column 31
    function action_39 () : void {
      ChooseInvoicePlacementPopup.push(entry, charge.DefaultPayer)
    }
    
    // 'action' attribute on ButtonCell (id=Revert_Cell) at EditInvoiceItemsPopup.pcf: line 162, column 80
    function action_75 () : void {
      entry.revert()
    }
    
    // 'action' attribute on ButtonCell (id=ReverseOrRemove_Cell) at EditInvoiceItemsPopup.pcf: line 167, column 170
    function action_78 () : void {
      removeOrReverseAndInvalidateIterator(entry)
    }
    
    // 'pickLocation' attribute on PickerCell (id=Invoice_Cell) at EditInvoiceItemsPopup.pcf: line 111, column 31
    function action_dest_40 () : pcf.api.Destination {
      return pcf.ChooseInvoicePlacementPopup.createDestination(entry, charge.DefaultPayer)
    }
    
    // 'available' attribute on ButtonCell (id=Revert_Cell) at EditInvoiceItemsPopup.pcf: line 162, column 80
    function available_74 () : java.lang.Boolean {
      return entry.removesItem() || entry.modifiesItem() || entry.isReversedItem()
    }
    
    // 'available' attribute on ButtonCell (id=ReverseOrRemove_Cell) at EditInvoiceItemsPopup.pcf: line 167, column 170
    function available_77 () : java.lang.Boolean {
      return entry.canReverse() || entry.canRemove()
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at EditInvoiceItemsPopup.pcf: line 141, column 38
    function currency_64 () : typekey.Currency {
      return currency
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at EditInvoiceItemsPopup.pcf: line 98, column 34
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      entry.EventDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on PickerCell (id=Invoice_Cell) at EditInvoiceItemsPopup.pcf: line 111, column 31
    function defaultSetter_43 (__VALUE_TO_SET :  java.lang.Object) : void {
      entry.Invoice = (__VALUE_TO_SET as entity.Invoice)
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at EditInvoiceItemsPopup.pcf: line 132, column 31
    function defaultSetter_53 (__VALUE_TO_SET :  java.lang.Object) : void {
      entry.InvoiceItemType = (__VALUE_TO_SET as typekey.InvoiceItemType)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at EditInvoiceItemsPopup.pcf: line 141, column 38
    function defaultSetter_62 (__VALUE_TO_SET :  java.lang.Object) : void {
      entry.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'editable' attribute on DateCell (id=EventDate_Cell) at EditInvoiceItemsPopup.pcf: line 98, column 34
    function editable_33 () : java.lang.Boolean {
      return entry.addsItem()
    }
    
    // 'editable' attribute on MonetaryAmountCell (id=Amount_Cell) at EditInvoiceItemsPopup.pcf: line 141, column 38
    function editable_60 () : java.lang.Boolean {
      return entry.canSetAmount()
    }
    
    // 'useArchivedStyle' attribute on Row at EditInvoiceItemsPopup.pcf: line 82, column 83
    function useArchivedStyle_83 () : java.lang.Boolean {
      return entry.removesItem() || entry.isInOrphanedState()
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at EditInvoiceItemsPopup.pcf: line 132, column 31
    function valueRange_55 () : java.lang.Object {
      return availableTypes
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at EditInvoiceItemsPopup.pcf: line 90, column 33
    function valueRoot_31 () : java.lang.Object {
      return entry.InvoiceItem
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at EditInvoiceItemsPopup.pcf: line 98, column 34
    function valueRoot_36 () : java.lang.Object {
      return entry
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at EditInvoiceItemsPopup.pcf: line 90, column 33
    function value_30 () : java.lang.Integer {
      return entry.InvoiceItem.InstallmentNumber
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at EditInvoiceItemsPopup.pcf: line 98, column 34
    function value_34 () : java.util.Date {
      return entry.EventDate
    }
    
    // 'value' attribute on PickerCell (id=Invoice_Cell) at EditInvoiceItemsPopup.pcf: line 111, column 31
    function value_42 () : entity.Invoice {
      return entry.Invoice
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at EditInvoiceItemsPopup.pcf: line 116, column 35
    function value_47 () : java.util.Date {
      return entry.InvoiceItem.Invoice == null ? entry.Invoice.PaymentDueDate : entry.InvoiceItem.InvoiceDueDate
    }
    
    // 'value' attribute on TextCell (id=itemPayer_Cell) at EditInvoiceItemsPopup.pcf: line 123, column 31
    function value_49 () : java.lang.String {
      return getPayerDisplay(entry)
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at EditInvoiceItemsPopup.pcf: line 132, column 31
    function value_52 () : typekey.InvoiceItemType {
      return entry.InvoiceItemType
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at EditInvoiceItemsPopup.pcf: line 141, column 38
    function value_61 () : gw.pl.currency.MonetaryAmount {
      return entry.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PaidAmount_Cell) at EditInvoiceItemsPopup.pcf: line 152, column 55
    function value_67 () : gw.pl.currency.MonetaryAmount {
      return entry.InvoiceItem.PaidAmount
    }
    
    // 'value' attribute on TextCell (id=EditType_Cell) at EditInvoiceItemsPopup.pcf: line 157, column 44
    function value_71 () : java.lang.String {
      return entry.DisplayName
    }
    
    // 'value' attribute on ButtonCell (id=ReverseOrRemove_Cell) at EditInvoiceItemsPopup.pcf: line 167, column 170
    function value_79 () : java.lang.String {
      return entry.canReverseButNotRemove() ? DisplayKey.get("Web.InvoiceItemsLV.Reverse") : DisplayKey.get("Web.InvoiceItemsLV.Remove")
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at EditInvoiceItemsPopup.pcf: line 132, column 31
    function verifyValueRangeIsAllowedType_56 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at EditInvoiceItemsPopup.pcf: line 132, column 31
    function verifyValueRangeIsAllowedType_56 ($$arg :  typekey.InvoiceItemType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at EditInvoiceItemsPopup.pcf: line 132, column 31
    function verifyValueRange_57 () : void {
      var __valueRangeArg = availableTypes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_56(__valueRangeArg)
    }
    
    property get entry () : gw.api.domain.invoice.ChargeInstallmentChanger.Entry {
      return getIteratedValue(1) as gw.api.domain.invoice.ChargeInstallmentChanger.Entry
    }
    
    
  }
  
  
}