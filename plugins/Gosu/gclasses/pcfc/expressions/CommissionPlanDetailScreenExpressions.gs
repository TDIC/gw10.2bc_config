package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionPlanDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionPlanDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=Clone) at CommissionPlanDetailScreen.pcf: line 17, column 47
    function action_2 () : void {
      CloneCommissionPlan.go(commissionPlan)
    }
    
    // 'action' attribute on ToolbarButton (id=Clone) at CommissionPlanDetailScreen.pcf: line 17, column 47
    function action_dest_3 () : pcf.api.Destination {
      return pcf.CloneCommissionPlan.createDestination(commissionPlan)
    }
    
    // 'def' attribute on PanelRef at CommissionPlanDetailScreen.pcf: line 20, column 59
    function def_onEnter_4 (def :  pcf.CommissionPlanDetailPanelSet) : void {
      def.onEnter(commissionPlan)
    }
    
    // 'def' attribute on PanelRef at CommissionPlanDetailScreen.pcf: line 20, column 59
    function def_refreshVariables_5 (def :  pcf.CommissionPlanDetailPanelSet) : void {
      def.refreshVariables(commissionPlan)
    }
    
    // EditButtons at CommissionPlanDetailScreen.pcf: line 11, column 21
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'visible' attribute on ToolbarButton (id=Clone) at CommissionPlanDetailScreen.pcf: line 17, column 47
    function visible_1 () : java.lang.Boolean {
      return perm.System.commplancreate
    }
    
    property get commissionPlan () : CommissionPlan {
      return getRequireValue("commissionPlan", 0) as CommissionPlan
    }
    
    property set commissionPlan ($arg :  CommissionPlan) {
      setRequireValue("commissionPlan", 0, $arg)
    }
    
    
  }
  
  
}