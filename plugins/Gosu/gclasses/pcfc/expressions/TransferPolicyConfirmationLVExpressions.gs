package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyConfirmationLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransferPolicyConfirmationLVExpressions {
  @javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyConfirmationLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TransferPolicyConfirmationLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=policynumber_Cell) at TransferPolicyConfirmationLV.pcf: line 22, column 45
    function action_7 () : void {
      PolicySummary.push(policyPeriod)
    }
    
    // 'action' attribute on TextCell (id=policynumber_Cell) at TransferPolicyConfirmationLV.pcf: line 22, column 45
    function action_dest_8 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(policyPeriod)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Premium_Cell) at TransferPolicyConfirmationLV.pcf: line 39, column 44
    function currency_20 () : typekey.Currency {
      return policyPeriod.Currency
    }
    
    // 'value' attribute on TextCell (id=policynumber_Cell) at TransferPolicyConfirmationLV.pcf: line 22, column 45
    function valueRoot_10 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on TextCell (id=insured_Cell) at TransferPolicyConfirmationLV.pcf: line 27, column 69
    function valueRoot_13 () : java.lang.Object {
      return policyPeriod.Policy.Account
    }
    
    // 'value' attribute on TextCell (id=insured_Cell) at TransferPolicyConfirmationLV.pcf: line 27, column 69
    function value_12 () : java.lang.String {
      return policyPeriod.Policy.Account.AccountNameLocalized
    }
    
    // 'value' attribute on TypeKeyCell (id=product_Cell) at TransferPolicyConfirmationLV.pcf: line 32, column 47
    function value_15 () : typekey.AccountSegment {
      return policyPeriod.Policy.Account.Segment
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Premium_Cell) at TransferPolicyConfirmationLV.pcf: line 39, column 44
    function value_18 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.TotalValue
    }
    
    // 'value' attribute on TextCell (id=policynumber_Cell) at TransferPolicyConfirmationLV.pcf: line 22, column 45
    function value_9 () : java.lang.String {
      return policyPeriod.DisplayName
    }
    
    property get policyPeriod () : entity.PolicyPeriod {
      return getIteratedValue(1) as entity.PolicyPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyConfirmationLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransferPolicyConfirmationLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on TextCell (id=policynumber_Cell) at TransferPolicyConfirmationLV.pcf: line 22, column 45
    function sortValue_0 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PolicyNumber
    }
    
    // 'sortBy' attribute on TextCell (id=policynumber_Cell) at TransferPolicyConfirmationLV.pcf: line 22, column 45
    function sortValue_1 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return  policyPeriod.TermNumber
    }
    
    // 'sortBy' attribute on TextCell (id=insured_Cell) at TransferPolicyConfirmationLV.pcf: line 27, column 69
    function sortValue_2 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.Policy.Account.AccountName
    }
    
    // 'value' attribute on TypeKeyCell (id=product_Cell) at TransferPolicyConfirmationLV.pcf: line 32, column 47
    function sortValue_3 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.Policy.Account.Segment
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Premium_Cell) at TransferPolicyConfirmationLV.pcf: line 39, column 44
    function sortValue_4 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.TotalValue
    }
    
    // '$$sumValue' attribute on RowIterator at TransferPolicyConfirmationLV.pcf: line 39, column 44
    function sumValueRoot_6 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod
    }
    
    // 'footerSumValue' attribute on RowIterator at TransferPolicyConfirmationLV.pcf: line 39, column 44
    function sumValue_5 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.TotalValue
    }
    
    // 'value' attribute on RowIterator at TransferPolicyConfirmationLV.pcf: line 14, column 41
    function value_22 () : entity.PolicyPeriod[] {
      return policyTransfer.PolicyPeriodsToTransfer
    }
    
    property get policyTransfer () : PolicyTransfer {
      return getRequireValue("policyTransfer", 0) as PolicyTransfer
    }
    
    property set policyTransfer ($arg :  PolicyTransfer) {
      setRequireValue("policyTransfer", 0, $arg)
    }
    
    
  }
  
  
}