package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/NewAccountContactPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewAccountContactPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/NewAccountContactPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewAccountContactPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account, contactSubtype :  Type<Contact>) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=NewAccountContactPopup) at NewAccountContactPopup.pcf: line 12, column 69
    function beforeCommit_4 (pickedValue :  AccountContact) : void {
      duplicateContactsPopupNavigator.beforeCommitOfContactCreationPage(\ c -> {accountContact.Contact = c})
    }
    
    // 'canVisit' attribute on Popup (id=NewAccountContactPopup) at NewAccountContactPopup.pcf: line 12, column 69
    static function canVisit_5 (account :  Account, contactSubtype :  Type<Contact>) : java.lang.Boolean {
      return perm.AccountContact.create
    }
    
    // 'def' attribute on ScreenRef at NewAccountContactPopup.pcf: line 30, column 87
    function def_onEnter_2 (def :  pcf.NewAccountContactScreen) : void {
      def.onEnter(accountContact, duplicateContactsPopupNavigator)
    }
    
    // 'def' attribute on ScreenRef at NewAccountContactPopup.pcf: line 30, column 87
    function def_refreshVariables_3 (def :  pcf.NewAccountContactScreen) : void {
      def.refreshVariables(accountContact, duplicateContactsPopupNavigator)
    }
    
    // 'initialValue' attribute on Variable at NewAccountContactPopup.pcf: line 21, column 30
    function initialValue_0 () : AccountContact {
      return initNewAccountContact()
    }
    
    // 'initialValue' attribute on Variable at NewAccountContactPopup.pcf: line 28, column 72
    function initialValue_1 () : gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator {
      return createDuplicateContactsPopupNavigator()
    }
    
    override property get CurrentLocation () : pcf.NewAccountContactPopup {
      return super.CurrentLocation as pcf.NewAccountContactPopup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get accountContact () : AccountContact {
      return getVariableValue("accountContact", 0) as AccountContact
    }
    
    property set accountContact ($arg :  AccountContact) {
      setVariableValue("accountContact", 0, $arg)
    }
    
    property get contactSubtype () : Type<Contact> {
      return getVariableValue("contactSubtype", 0) as Type<Contact>
    }
    
    property set contactSubtype ($arg :  Type<Contact>) {
      setVariableValue("contactSubtype", 0, $arg)
    }
    
    property get duplicateContactsPopupNavigator () : gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator {
      return getVariableValue("duplicateContactsPopupNavigator", 0) as gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator
    }
    
    property set duplicateContactsPopupNavigator ($arg :  gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator) {
      setVariableValue("duplicateContactsPopupNavigator", 0, $arg)
    }
    
    function initNewAccountContact() : AccountContact {
      var newAccountContact = new AccountContact();
      var newContact = instantiateContact()
      newAccountContact.Contact = newContact
      account.addToContacts(newAccountContact);
      return newAccountContact;
    }
    
    private function instantiateContact() : Contact {
      // Instantiate the appropriate contact subtype (e.g. a Person or a Company)
      return contactSubtype.TypeInfo.getConstructor(null).Constructor.newInstance(null) as Contact  
    }
    
    function createDuplicateContactsPopupNavigator() : gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator {
      var newContactBeingCreatedOnThisPage = accountContact.Contact
      var existingContactsOnThisAccount = account.Contacts.map(\ ac -> ac.Contact).where(\ c -> c != newContactBeingCreatedOnThisPage)
      return new gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigatorImpl(accountContact.Contact, existingContactsOnThisAccount, entity.Account)
    }
    
    
  }
  
  
}