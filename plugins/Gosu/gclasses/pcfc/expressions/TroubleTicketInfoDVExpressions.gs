package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TroubleTicketInfoDVExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketInfoDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TroubleTicketInfoDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=AssignTicketOwner_PickerButton) at AssigneeWidget.xml: line 7, column 25
    function action_16 () : void {
      AssigneePickerPopup.push(new gw.api.assignment.AssigneePicker(entity.Activity.Type.isAssignableFrom(typeof (ticket))))
    }
    
    // 'action' attribute on MenuItem (id=AssignTicketOwner_PickerButton) at AssigneeWidget.xml: line 7, column 25
    function action_dest_17 () : pcf.api.Destination {
      return pcf.AssigneePickerPopup.createDestination(new gw.api.assignment.AssigneePicker(entity.Activity.Type.isAssignableFrom(typeof (ticket))))
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at TroubleTicketInfoDV.pcf: line 38, column 31
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      ticket.Title = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on AssigneeInput (id=AssignTicketOwner_Input) at AssigneeWidget.xml: line 7, column 25
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      AssigneeHolder[0] = (__VALUE_TO_SET as gw.api.assignment.Assignee)
    }
    
    // 'value' attribute on DateInput (id=DueDate_Input) at TroubleTicketInfoDV.pcf: line 65, column 36
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      ticket.TargetDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=EscalationDate_Input) at TroubleTicketInfoDV.pcf: line 71, column 40
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      ticket.EscalationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=Priority_Input) at TroubleTicketInfoDV.pcf: line 78, column 39
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      ticket.Priority = (__VALUE_TO_SET as typekey.Priority)
    }
    
    // 'value' attribute on TextAreaInput (id=DetailedDescription_Input) at TroubleTicketInfoDV.pcf: line 95, column 45
    function defaultSetter_58 (__VALUE_TO_SET :  java.lang.Object) : void {
      ticket.DetailedDescription = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=TicketType_Input) at TroubleTicketInfoDV.pcf: line 32, column 48
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      ticket.TicketType = (__VALUE_TO_SET as typekey.TroubleTicketType)
    }
    
    // 'editable' attribute on AssigneeInput (id=AssignTicketOwner_Input) at AssigneeWidget.xml: line 7, column 25
    function editable_18 () : java.lang.Boolean {
      return perm.TroubleTicket.assign(ticket)
    }
    
    // 'editable' attribute on DetailViewPanel (id=TroubleTicketInfoDV) at TroubleTicketInfoDV.pcf: line 9, column 30
    function editable_61 () : java.lang.Boolean {
      return !ticket.IsClosed
    }
    
    // 'validationExpression' attribute on DateInput (id=DueDate_Input) at TroubleTicketInfoDV.pcf: line 65, column 36
    function validationExpression_38 () : java.lang.Object {
      return gw.troubleticket.TroubleTicketMethods.validateTargetDate(ticket)
    }
    
    // 'validationExpression' attribute on DateInput (id=EscalationDate_Input) at TroubleTicketInfoDV.pcf: line 71, column 40
    function validationExpression_44 () : java.lang.Object {
      return gw.troubleticket.TroubleTicketMethods.validateEscalationDate(ticket)
    }
    
    // 'valueRange' attribute on AssigneeInput (id=AssignTicketOwner_Input) at AssigneeWidget.xml: line 7, column 25
    function valueRange_21 () : java.lang.Object {
      return ticket.SuggestedAssigneesIncludingQueues_Ext
    }
    
    // 'valueRange' attribute on RangeInput (id=TicketType_Input) at TroubleTicketInfoDV.pcf: line 32, column 48
    function valueRange_8 () : java.lang.Object {
      return ticket.AvailableTicketTypes
    }
    
    // 'value' attribute on TextInput (id=TicketNumber_Input) at TroubleTicketInfoDV.pcf: line 24, column 55
    function valueRoot_2 () : java.lang.Object {
      return ticket
    }
    
    // 'value' attribute on TextInput (id=TicketNumber_Input) at TroubleTicketInfoDV.pcf: line 24, column 55
    function value_1 () : java.lang.String {
      return ticket.TroubleTicketNumber
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at TroubleTicketInfoDV.pcf: line 38, column 31
    function value_12 () : java.lang.String {
      return ticket.Title
    }
    
    // 'value' attribute on AssigneeInput (id=AssignTicketOwner_Input) at AssigneeWidget.xml: line 7, column 25
    function value_19 () : gw.api.assignment.Assignee {
      return AssigneeHolder[0]
    }
    
    // 'value' attribute on TextInput (id=CreatedBy_Input) at TroubleTicketInfoDV.pcf: line 54, column 46
    function value_29 () : entity.User {
      return ticket.CreateUser
    }
    
    // 'value' attribute on DateInput (id=CreateDate_Input) at TroubleTicketInfoDV.pcf: line 59, column 46
    function value_34 () : java.util.Date {
      return ticket.CreateTime
    }
    
    // 'value' attribute on DateInput (id=DueDate_Input) at TroubleTicketInfoDV.pcf: line 65, column 36
    function value_39 () : java.util.Date {
      return ticket.TargetDate
    }
    
    // 'value' attribute on DateInput (id=EscalationDate_Input) at TroubleTicketInfoDV.pcf: line 71, column 40
    function value_45 () : java.util.Date {
      return ticket.EscalationDate
    }
    
    // 'value' attribute on RangeInput (id=TicketType_Input) at TroubleTicketInfoDV.pcf: line 32, column 48
    function value_5 () : typekey.TroubleTicketType {
      return ticket.TicketType
    }
    
    // 'value' attribute on TypeKeyInput (id=Priority_Input) at TroubleTicketInfoDV.pcf: line 78, column 39
    function value_50 () : typekey.Priority {
      return ticket.Priority
    }
    
    // 'value' attribute on TextInput (id=Status_Input) at TroubleTicketInfoDV.pcf: line 82, column 38
    function value_54 () : java.lang.String {
      return ticket.TicketStatus
    }
    
    // 'value' attribute on TextAreaInput (id=DetailedDescription_Input) at TroubleTicketInfoDV.pcf: line 95, column 45
    function value_57 () : java.lang.String {
      return ticket.DetailedDescription
    }
    
    // 'valueRange' attribute on AssigneeInput (id=AssignTicketOwner_Input) at AssigneeWidget.xml: line 7, column 25
    function verifyValueRangeIsAllowedType_22 ($$arg :  gw.api.assignment.Assignee[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on AssigneeInput (id=AssignTicketOwner_Input) at AssigneeWidget.xml: line 7, column 25
    function verifyValueRangeIsAllowedType_22 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=TicketType_Input) at TroubleTicketInfoDV.pcf: line 32, column 48
    function verifyValueRangeIsAllowedType_9 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=TicketType_Input) at TroubleTicketInfoDV.pcf: line 32, column 48
    function verifyValueRangeIsAllowedType_9 ($$arg :  typekey.TroubleTicketType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=TicketType_Input) at TroubleTicketInfoDV.pcf: line 32, column 48
    function verifyValueRange_10 () : void {
      var __valueRangeArg = ticket.AvailableTicketTypes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_9(__valueRangeArg)
    }
    
    // 'valueRange' attribute on AssigneeInput (id=AssignTicketOwner_Input) at AssigneeWidget.xml: line 7, column 25
    function verifyValueRange_23 () : void {
      var __valueRangeArg = ticket.SuggestedAssigneesIncludingQueues_Ext
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_22(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=TicketNumber_Input) at TroubleTicketInfoDV.pcf: line 24, column 55
    function visible_0 () : java.lang.Boolean {
      return ticket.TroubleTicketNumber != null
    }
    
    // 'visible' attribute on TextInput (id=CreatedBy_Input) at TroubleTicketInfoDV.pcf: line 54, column 46
    function visible_28 () : java.lang.Boolean {
      return ticket.CreateUser != null
    }
    
    // 'visible' attribute on DateInput (id=CreateDate_Input) at TroubleTicketInfoDV.pcf: line 59, column 46
    function visible_33 () : java.lang.Boolean {
      return ticket.CreateTime != null
    }
    
    property get AssigneeHolder () : gw.api.assignment.Assignee[] {
      return getRequireValue("AssigneeHolder", 0) as gw.api.assignment.Assignee[]
    }
    
    property set AssigneeHolder ($arg :  gw.api.assignment.Assignee[]) {
      setRequireValue("AssigneeHolder", 0, $arg)
    }
    
    property get ticket () : TroubleTicket {
      return getRequireValue("ticket", 0) as TroubleTicket
    }
    
    property set ticket ($arg :  TroubleTicket) {
      setRequireValue("ticket", 0, $arg)
    }
    
    
  }
  
  
}