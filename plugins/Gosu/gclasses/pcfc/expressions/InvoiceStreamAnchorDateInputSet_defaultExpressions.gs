package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/InvoiceStreamAnchorDateInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceStreamAnchorDateInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/account/InvoiceStreamAnchorDateInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceStreamAnchorDateInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverrideInvoiceAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.default.pcf: line 30, column 44
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceDayChangeHelper.OverrideAnchorDates = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateInput (id=OverridingAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.default.pcf: line 39, column 60
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceDayChangeHelper.OverridingAnchorDateViews[0].Date = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=OverridingSecondAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.default.pcf: line 48, column 133
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceDayChangeHelper.OverridingAnchorDateViews[1].Date = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'editable' attribute on DateInput (id=OverridingAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.default.pcf: line 39, column 60
    function editable_16 () : java.lang.Boolean {
      return isEditMode && invoiceDayChangeHelper.OverrideAnchorDates
    }
    
    // 'editable' attribute on BooleanRadioInput (id=OverrideInvoiceAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.default.pcf: line 30, column 44
    function editable_8 () : java.lang.Boolean {
      return isEditMode && invoiceDayChangeHelper.PayerDefaultAnchorDateViews.Count > 0
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverrideInvoiceAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.default.pcf: line 30, column 44
    function valueRoot_12 () : java.lang.Object {
      return invoiceDayChangeHelper
    }
    
    // 'value' attribute on DateInput (id=AccountInvoiceAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.default.pcf: line 18, column 82
    function value_1 () : java.util.Date {
      return invoiceDayChangeHelper.PayerDefaultAnchorDateViews[0].Date
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverrideInvoiceAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.default.pcf: line 30, column 44
    function value_10 () : java.lang.Boolean {
      return invoiceDayChangeHelper.OverrideAnchorDates
    }
    
    // 'value' attribute on DateInput (id=OverridingAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.default.pcf: line 39, column 60
    function value_18 () : java.util.Date {
      return invoiceDayChangeHelper.OverridingAnchorDateViews[0].Date
    }
    
    // 'value' attribute on DateInput (id=OverridingSecondAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.default.pcf: line 48, column 133
    function value_25 () : java.util.Date {
      return invoiceDayChangeHelper.OverridingAnchorDateViews[1].Date
    }
    
    // 'value' attribute on DateInput (id=AccountSecondInvoiceAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.default.pcf: line 23, column 82
    function value_5 () : java.util.Date {
      return invoiceDayChangeHelper.PayerDefaultAnchorDateViews[1].Date
    }
    
    // 'visible' attribute on DateInput (id=AccountInvoiceAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.default.pcf: line 18, column 82
    function visible_0 () : java.lang.Boolean {
      return invoiceDayChangeHelper.PayerDefaultAnchorDateViews.Count > 0
    }
    
    // 'visible' attribute on DateInput (id=OverridingSecondAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.default.pcf: line 48, column 133
    function visible_24 () : java.lang.Boolean {
      return invoiceDayChangeHelper.OverrideAnchorDates && invoiceDayChangeHelper.OverridingAnchorDateViews.Count > 1
    }
    
    // 'visible' attribute on DateInput (id=AccountSecondInvoiceAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.default.pcf: line 23, column 82
    function visible_4 () : java.lang.Boolean {
      return invoiceDayChangeHelper.PayerDefaultAnchorDateViews.Count > 1
    }
    
    // 'visible' attribute on BooleanRadioInput (id=OverrideInvoiceAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.default.pcf: line 30, column 44
    function visible_9 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    property get invoiceDayChangeHelper () : gw.api.web.invoice.InvoiceDayChangeHelper {
      return getRequireValue("invoiceDayChangeHelper", 0) as gw.api.web.invoice.InvoiceDayChangeHelper
    }
    
    property set invoiceDayChangeHelper ($arg :  gw.api.web.invoice.InvoiceDayChangeHelper) {
      setRequireValue("invoiceDayChangeHelper", 0, $arg)
    }
    
    property get isEditMode () : boolean {
      return getRequireValue("isEditMode", 0) as java.lang.Boolean
    }
    
    property set isEditMode ($arg :  boolean) {
      setRequireValue("isEditMode", 0, $arg)
    }
    
    
  }
  
  
}