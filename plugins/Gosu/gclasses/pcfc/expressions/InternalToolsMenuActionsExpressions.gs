package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/tools/InternalToolsMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InternalToolsMenuActionsExpressions {
  @javax.annotation.Generated("config/web/pcf/tools/InternalToolsMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InternalToolsMenuActionsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=ReturnToApp) at InternalToolsMenuActions.pcf: line 10, column 67
    function action_0 () : void {
      gw.api.util.NavUtil.goToDefaultEntryPoint()
    }
    
    // 'action' attribute on MenuItem (id=adminDataLoader) at InternalToolsMenuActions.pcf: line 17, column 84
    function action_1 () : void {
      AdminDataLoaderWizard.push()
    }
    
    // 'action' attribute on MenuItem (id=planDataLoader) at InternalToolsMenuActions.pcf: line 21, column 83
    function action_3 () : void {
      PlanDataLoaderWizard.push()
    }
    
    // 'action' attribute on MenuItem (id=sampleDataLoader) at InternalToolsMenuActions.pcf: line 25, column 85
    function action_5 () : void {
      BillingDataLoaderWizard.push()
    }
    
    // 'action' attribute on MenuItem (id=exportData) at InternalToolsMenuActions.pcf: line 30, column 76
    function action_7 () : void {
      ExportDataToExcel.go()
    }
    
    // 'action' attribute on MenuItem (id=adminDataLoader) at InternalToolsMenuActions.pcf: line 17, column 84
    function action_dest_2 () : pcf.api.Destination {
      return pcf.AdminDataLoaderWizard.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=planDataLoader) at InternalToolsMenuActions.pcf: line 21, column 83
    function action_dest_4 () : pcf.api.Destination {
      return pcf.PlanDataLoaderWizard.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=sampleDataLoader) at InternalToolsMenuActions.pcf: line 25, column 85
    function action_dest_6 () : pcf.api.Destination {
      return pcf.BillingDataLoaderWizard.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=exportData) at InternalToolsMenuActions.pcf: line 30, column 76
    function action_dest_8 () : pcf.api.Destination {
      return pcf.ExportDataToExcel.createDestination()
    }
    
    // 'label' attribute on MenuItem (id=InternalToolsMenuActions_Logout) at InternalToolsMenuActions.pcf: line 34, column 92
    function label_9 () : java.lang.Object {
      return DisplayKey.get("Web.TabBar.Logout", entity.User.util.CurrentUser)
    }
    
    
  }
  
  
}