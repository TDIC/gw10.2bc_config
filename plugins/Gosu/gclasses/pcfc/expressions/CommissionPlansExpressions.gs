package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/businesssettings/CommissionPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionPlansExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/CommissionPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionPlansExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=CommissionPlans) at CommissionPlans.pcf: line 9, column 67
    static function canVisit_3 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.commplanview
    }
    
    // 'def' attribute on PanelRef at CommissionPlans.pcf: line 18, column 51
    function def_onEnter_1 (def :  pcf.CommissionPlansLV) : void {
      def.onEnter(commissionPlans)
    }
    
    // 'def' attribute on PanelRef at CommissionPlans.pcf: line 18, column 51
    function def_refreshVariables_2 (def :  pcf.CommissionPlansLV) : void {
      def.refreshVariables(commissionPlans)
    }
    
    // 'initialValue' attribute on Variable at CommissionPlans.pcf: line 13, column 77
    function initialValue_0 () : gw.api.database.IQueryBeanResult<entity.CommissionPlan> {
      return gw.api.database.Query.make(CommissionPlan).select()
    }
    
    // Page (id=CommissionPlans) at CommissionPlans.pcf: line 9, column 67
    static function parent_4 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    override property get CurrentLocation () : pcf.CommissionPlans {
      return super.CurrentLocation as pcf.CommissionPlans
    }
    
    property get commissionPlans () : gw.api.database.IQueryBeanResult<entity.CommissionPlan> {
      return getVariableValue("commissionPlans", 0) as gw.api.database.IQueryBeanResult<entity.CommissionPlan>
    }
    
    property set commissionPlans ($arg :  gw.api.database.IQueryBeanResult<entity.CommissionPlan>) {
      setVariableValue("commissionPlans", 0, $arg)
    }
    
    
  }
  
  
}