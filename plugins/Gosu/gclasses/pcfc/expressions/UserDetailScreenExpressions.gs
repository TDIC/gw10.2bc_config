package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/users/UserDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class UserDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/users/UserDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class UserDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at UserDetailScreen.pcf: line 24, column 43
    function def_onEnter_3 (def :  pcf.UserDetailDV) : void {
      def.onEnter(User, false)
    }
    
    // 'def' attribute on PanelRef at UserDetailScreen.pcf: line 32, column 45
    function def_onEnter_5 (def :  pcf.UserAuthorityLimitsDV) : void {
      def.onEnter(User)
    }
    
    // 'def' attribute on PanelRef at UserDetailScreen.pcf: line 40, column 37
    function def_onEnter_7 (def :  pcf.UserProfileDV) : void {
      def.onEnter(User)
    }
    
    // 'def' attribute on PanelRef at UserDetailScreen.pcf: line 24, column 43
    function def_refreshVariables_4 (def :  pcf.UserDetailDV) : void {
      def.refreshVariables(User, false)
    }
    
    // 'def' attribute on PanelRef at UserDetailScreen.pcf: line 32, column 45
    function def_refreshVariables_6 (def :  pcf.UserAuthorityLimitsDV) : void {
      def.refreshVariables(User)
    }
    
    // 'def' attribute on PanelRef at UserDetailScreen.pcf: line 40, column 37
    function def_refreshVariables_8 (def :  pcf.UserProfileDV) : void {
      def.refreshVariables(User)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at UserDetailScreen.pcf: line 12, column 62
    function toolbarButtonSet_onEnter_0 (def :  pcf.UserDetailToolbarButtonSet) : void {
      def.onEnter(User)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at UserDetailScreen.pcf: line 12, column 62
    function toolbarButtonSet_refreshVariables_1 (def :  pcf.UserDetailToolbarButtonSet) : void {
      def.refreshVariables(User)
    }
    
    // 'visible' attribute on AlertBar (id=UserDetailScreen_AlertBar) at UserDetailScreen.pcf: line 18, column 40
    function visible_2 () : java.lang.Boolean {
      return not User.isEditable()
    }
    
    property get User () : User {
      return getRequireValue("User", 0) as User
    }
    
    property set User ($arg :  User) {
      setRequireValue("User", 0, $arg)
    }
    
    
  }
  
  
}