package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/collection/CollectionAgencyDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CollectionAgencyDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/collection/CollectionAgencyDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CollectionAgencyDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (collectionAgency :  CollectionAgency) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=CollectionAgencyDetail) at CollectionAgencyDetail.pcf: line 10, column 92
    function canEdit_2 () : java.lang.Boolean {
      return perm.System.colagencyedit
    }
    
    // 'def' attribute on ScreenRef at CollectionAgencyDetail.pcf: line 17, column 61
    function def_onEnter_0 (def :  pcf.CollectionAgencyDetailScreen) : void {
      def.onEnter(collectionAgency)
    }
    
    // 'def' attribute on ScreenRef at CollectionAgencyDetail.pcf: line 17, column 61
    function def_refreshVariables_1 (def :  pcf.CollectionAgencyDetailScreen) : void {
      def.refreshVariables(collectionAgency)
    }
    
    // 'parent' attribute on Page (id=CollectionAgencyDetail) at CollectionAgencyDetail.pcf: line 10, column 92
    static function parent_3 (collectionAgency :  CollectionAgency) : pcf.api.Destination {
      return pcf.CollectionAgencies.createDestination()
    }
    
    // 'title' attribute on Page (id=CollectionAgencyDetail) at CollectionAgencyDetail.pcf: line 10, column 92
    static function title_4 (collectionAgency :  CollectionAgency) : java.lang.Object {
      return DisplayKey.get("Web.CollectionAgencyDetail.Title", collectionAgency)
    }
    
    override property get CurrentLocation () : pcf.CollectionAgencyDetail {
      return super.CurrentLocation as pcf.CollectionAgencyDetail
    }
    
    property get collectionAgency () : CollectionAgency {
      return getVariableValue("collectionAgency", 0) as CollectionAgency
    }
    
    property set collectionAgency ($arg :  CollectionAgency) {
      setVariableValue("collectionAgency", 0, $arg)
    }
    
    
  }
  
  
}