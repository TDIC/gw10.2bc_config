package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/AgencyCommissionPaymentWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyCommissionPaymentWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/AgencyCommissionPaymentWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyCommissionPaymentWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Wizard (id=AgencyCommissionPaymentWizard) at AgencyCommissionPaymentWizard.pcf: line 7, column 40
    function beforeCommit_5 (pickedValue :  java.lang.Object) : void {
      incomingProducerPayment.recordPayment()
    }
    
    // 'initialValue' attribute on Variable at AgencyCommissionPaymentWizard.pcf: line 16, column 46
    function initialValue_0 () : entity.IncomingProducerPayment {
      return newIncomingProducerPayment()
    }
    
    // 'screen' attribute on WizardStep (id=SelectProducer) at AgencyCommissionPaymentWizard.pcf: line 21, column 99
    function screen_onEnter_1 (def :  pcf.AgencyCommissionPaymentWizard_SelectProducerScreen) : void {
      def.onEnter(incomingProducerPayment)
    }
    
    // 'screen' attribute on WizardStep (id=Confirmation) at AgencyCommissionPaymentWizard.pcf: line 26, column 97
    function screen_onEnter_3 (def :  pcf.AgencyCommissionPaymentWizard_ConfirmationScreen) : void {
      def.onEnter(incomingProducerPayment)
    }
    
    // 'screen' attribute on WizardStep (id=SelectProducer) at AgencyCommissionPaymentWizard.pcf: line 21, column 99
    function screen_refreshVariables_2 (def :  pcf.AgencyCommissionPaymentWizard_SelectProducerScreen) : void {
      def.refreshVariables(incomingProducerPayment)
    }
    
    // 'screen' attribute on WizardStep (id=Confirmation) at AgencyCommissionPaymentWizard.pcf: line 26, column 97
    function screen_refreshVariables_4 (def :  pcf.AgencyCommissionPaymentWizard_ConfirmationScreen) : void {
      def.refreshVariables(incomingProducerPayment)
    }
    
    // 'tabBar' attribute on Wizard (id=AgencyCommissionPaymentWizard) at AgencyCommissionPaymentWizard.pcf: line 7, column 40
    function tabBar_onEnter_6 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AgencyCommissionPaymentWizard) at AgencyCommissionPaymentWizard.pcf: line 7, column 40
    function tabBar_refreshVariables_7 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AgencyCommissionPaymentWizard {
      return super.CurrentLocation as pcf.AgencyCommissionPaymentWizard
    }
    
    property get incomingProducerPayment () : entity.IncomingProducerPayment {
      return getVariableValue("incomingProducerPayment", 0) as entity.IncomingProducerPayment
    }
    
    property set incomingProducerPayment ($arg :  entity.IncomingProducerPayment) {
      setVariableValue("incomingProducerPayment", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    function newIncomingProducerPayment() : IncomingProducerPayment {
      var ipp = new entity.IncomingProducerPayment(producer.Currency)
      ipp.Producer = producer
      return ipp
    }
    
    
  }
  
  
}