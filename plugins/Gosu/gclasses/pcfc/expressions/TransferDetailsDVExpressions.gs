package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/TransferDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransferDetailsDVExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/TransferDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountInfoExpressionsImpl extends TransferDetailsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at TransferDetailsDV.pcf: line 44, column 27
    function currency_15 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on RangeInput (id=UnappliedFunds_Input) at TransferDetailsDV.pcf: line 34, column 56
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      fundsTransferUtil.SourceUnappliedFunds = (__VALUE_TO_SET as entity.UnappliedFund)
    }
    
    // 'editable' attribute on InputSet (id=AccountInfo) at TransferDetailsDV.pcf: line 17, column 80
    function editable_17 () : java.lang.Boolean {
      return canEditPage
    }
    
    // 'initialValue' attribute on Variable at TransferDetailsDV.pcf: line 21, column 27
    function initialValue_0 () : Account {
      return fundsTransferUtil.SourceType == TAccountOwnerType.TC_ACCOUNT ? fundsTransferUtil.SourceOwner as Account : null
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at TransferDetailsDV.pcf: line 34, column 56
    function valueRange_8 () : java.lang.Object {
      return account.UnappliedFundsSortedByDisplayName
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at TransferDetailsDV.pcf: line 44, column 27
    function valueRoot_14 () : java.lang.Object {
      return fundsTransferUtil.SourceUnappliedFunds
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at TransferDetailsDV.pcf: line 25, column 42
    function valueRoot_2 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on RangeInput (id=UnappliedFunds_Input) at TransferDetailsDV.pcf: line 34, column 56
    function valueRoot_7 () : java.lang.Object {
      return fundsTransferUtil
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at TransferDetailsDV.pcf: line 25, column 42
    function value_1 () : java.lang.String {
      return account.AccountNumber
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at TransferDetailsDV.pcf: line 44, column 27
    function value_13 () : gw.pl.currency.MonetaryAmount {
      return fundsTransferUtil.SourceUnappliedFunds.Balance
    }
    
    // 'value' attribute on RangeInput (id=UnappliedFunds_Input) at TransferDetailsDV.pcf: line 34, column 56
    function value_5 () : entity.UnappliedFund {
      return fundsTransferUtil.SourceUnappliedFunds
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at TransferDetailsDV.pcf: line 34, column 56
    function verifyValueRangeIsAllowedType_9 ($$arg :  entity.UnappliedFund[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at TransferDetailsDV.pcf: line 34, column 56
    function verifyValueRangeIsAllowedType_9 ($$arg :  gw.api.database.IQueryBeanResult<entity.UnappliedFund>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at TransferDetailsDV.pcf: line 34, column 56
    function verifyValueRangeIsAllowedType_9 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at TransferDetailsDV.pcf: line 34, column 56
    function verifyValueRange_10 () : void {
      var __valueRangeArg = account.UnappliedFundsSortedByDisplayName
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_9(__valueRangeArg)
    }
    
    // 'visible' attribute on InputSet (id=AccountInfo) at TransferDetailsDV.pcf: line 17, column 80
    function visible_18 () : java.lang.Boolean {
      return fundsTransferUtil.SourceType == TAccountOwnerType.TC_ACCOUNT
    }
    
    // 'visible' attribute on RangeInput (id=UnappliedFunds_Input) at TransferDetailsDV.pcf: line 34, column 56
    function visible_4 () : java.lang.Boolean {
      return account.HasDesignatedUnappliedFund
    }
    
    property get account () : Account {
      return getVariableValue("account", 1) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/TransferDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransferDetailsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on ListViewInput at TransferDetailsDV.pcf: line 84, column 50
    function def_onEnter_43 (def :  pcf.TransferDetailsLV_account) : void {
      def.onEnter(fundsTransferUtil, canEditPage)
    }
    
    // 'def' attribute on ListViewInput at TransferDetailsDV.pcf: line 84, column 50
    function def_onEnter_45 (def :  pcf.TransferDetailsLV_producer) : void {
      def.onEnter(fundsTransferUtil, canEditPage)
    }
    
    // 'def' attribute on ListViewInput at TransferDetailsDV.pcf: line 84, column 50
    function def_refreshVariables_44 (def :  pcf.TransferDetailsLV_account) : void {
      def.refreshVariables(fundsTransferUtil, canEditPage)
    }
    
    // 'def' attribute on ListViewInput at TransferDetailsDV.pcf: line 84, column 50
    function def_refreshVariables_46 (def :  pcf.TransferDetailsLV_producer) : void {
      def.refreshVariables(fundsTransferUtil, canEditPage)
    }
    
    // 'value' attribute on RangeRadioInput (id=Target_Input) at TransferDetailsDV.pcf: line 77, column 31
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      fundsTransferUtil.TargetType = (__VALUE_TO_SET as typekey.TAccountOwnerType)
    }
    
    // 'editable' attribute on RangeRadioInput (id=Target_Input) at TransferDetailsDV.pcf: line 77, column 31
    function editable_30 () : java.lang.Boolean {
      return canEditPage
    }
    
    // 'mode' attribute on ListViewInput at TransferDetailsDV.pcf: line 84, column 50
    function mode_47 () : java.lang.Object {
      return fundsTransferUtil.TargetType.Code
    }
    
    // 'onChange' attribute on PostOnChange at TransferDetailsDV.pcf: line 79, column 35
    function onChange_29 () : void {
      cleanRows()
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=Target_Input) at TransferDetailsDV.pcf: line 77, column 31
    function valueRange_35 () : java.lang.Object {
      return getAvailableSourceTypes()
    }
    
    // 'value' attribute on RangeRadioInput (id=Target_Input) at TransferDetailsDV.pcf: line 77, column 31
    function valueRoot_34 () : java.lang.Object {
      return fundsTransferUtil
    }
    
    // 'value' attribute on RangeRadioInput (id=Target_Input) at TransferDetailsDV.pcf: line 77, column 31
    function value_32 () : typekey.TAccountOwnerType {
      return fundsTransferUtil.TargetType
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=Target_Input) at TransferDetailsDV.pcf: line 77, column 31
    function verifyValueRangeIsAllowedType_36 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=Target_Input) at TransferDetailsDV.pcf: line 77, column 31
    function verifyValueRangeIsAllowedType_36 ($$arg :  typekey.TAccountOwnerType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=Target_Input) at TransferDetailsDV.pcf: line 77, column 31
    function verifyValueRange_37 () : void {
      var __valueRangeArg = getAvailableSourceTypes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_36(__valueRangeArg)
    }
    
    property get canEditPage () : Boolean {
      return getRequireValue("canEditPage", 0) as Boolean
    }
    
    property set canEditPage ($arg :  Boolean) {
      setRequireValue("canEditPage", 0, $arg)
    }
    
    property get fundsTransferUtil () : gw.api.web.transaction.FundsTransferUtil {
      return getRequireValue("fundsTransferUtil", 0) as gw.api.web.transaction.FundsTransferUtil
    }
    
    property set fundsTransferUtil ($arg :  gw.api.web.transaction.FundsTransferUtil) {
      setRequireValue("fundsTransferUtil", 0, $arg)
    }
    
    function removeRow(transfer : FundsTransfer) {
      transfer.remove();
      fundsTransferUtil.removeFromTransfers(transfer);
    }
    
    function cleanRows() {
      fundsTransferUtil.TransferTargets.each(\ transfer : FundsTransfer -> removeRow(transfer));
      fundsTransferUtil.addToTransfers(new FundsTransfer(fundsTransferUtil.SourceOwner.Currency))
    }
    
    public static function validateAmount(value : gw.pl.currency.MonetaryAmount) : String{
      return value.IsPositive ? null : "Amount has to be a positive number."
    }
    
    
    public static function validateAmount(pFundsTransferUtil : gw.api.web.transaction.FundsTransferUtil) : String {
      if (!pFundsTransferUtil.validateAmounts()) {
        return DisplayKey.get("Web.TransferDetailsDV.CannotCreateTransfer");
      } else {
        return null;
      }
    }
    function getAvailableSourceTypes() : TAccountOwnerType[] {
      return new TAccountOwnerType[]{TAccountOwnerType.TC_ACCOUNT} 
      //  HermiaK 10/28/2014 US70 hide the Producer radio button 
      //  TAccountOwnerType.TC_PRODUCER}
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/TransferDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class producerInfoExpressionsImpl extends TransferDetailsDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=AvailableTransferAmount_Input) at TransferDetailsDV.pcf: line 64, column 45
    function currency_25 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'editable' attribute on InputSet (id=producerInfo) at TransferDetailsDV.pcf: line 49, column 81
    function editable_27 () : java.lang.Boolean {
      return canEditPage
    }
    
    // 'initialValue' attribute on Variable at TransferDetailsDV.pcf: line 53, column 28
    function initialValue_19 () : Producer {
      return fundsTransferUtil.SourceType == TAccountOwnerType.TC_PRODUCER ? fundsTransferUtil.SourceOwner as Producer : null
    }
    
    // 'value' attribute on TextInput (id=ProducerName_Input) at TransferDetailsDV.pcf: line 57, column 41
    function valueRoot_21 () : java.lang.Object {
      return producer
    }
    
    // 'value' attribute on TextInput (id=ProducerName_Input) at TransferDetailsDV.pcf: line 57, column 41
    function value_20 () : java.lang.String {
      return producer.DisplayName
    }
    
    // 'value' attribute on MonetaryAmountInput (id=AvailableTransferAmount_Input) at TransferDetailsDV.pcf: line 64, column 45
    function value_23 () : gw.pl.currency.MonetaryAmount {
      return producer.UnappliedAmount
    }
    
    // 'visible' attribute on InputSet (id=producerInfo) at TransferDetailsDV.pcf: line 49, column 81
    function visible_28 () : java.lang.Boolean {
      return fundsTransferUtil.SourceType == TAccountOwnerType.TC_PRODUCER
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 1) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 1, $arg)
    }
    
    
  }
  
  
}