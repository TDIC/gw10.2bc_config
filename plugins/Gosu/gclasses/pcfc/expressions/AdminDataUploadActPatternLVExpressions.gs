package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadActPatternLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadActPatternLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadActPatternLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadActPatternLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadActPatternLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at AdminDataUploadActPatternLV.pcf: line 50, column 41
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Description")
    }
    
    // 'label' attribute on TypeKeyCell (id=activityClass_Cell) at AdminDataUploadActPatternLV.pcf: line 55, column 46
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.ActivityClass")
    }
    
    // 'label' attribute on TypeKeyCell (id=activityType_Cell) at AdminDataUploadActPatternLV.pcf: line 60, column 45
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.ActivityType")
    }
    
    // 'label' attribute on TextCell (id=category_Cell) at AdminDataUploadActPatternLV.pcf: line 65, column 41
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Category")
    }
    
    // 'label' attribute on BooleanRadioCell (id=mandatory_Cell) at AdminDataUploadActPatternLV.pcf: line 70, column 42
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Mandatory")
    }
    
    // 'label' attribute on TextCell (id=priority_Cell) at AdminDataUploadActPatternLV.pcf: line 75, column 41
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Priority")
    }
    
    // 'label' attribute on BooleanRadioCell (id=recurring_Cell) at AdminDataUploadActPatternLV.pcf: line 80, column 42
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Recurring")
    }
    
    // 'label' attribute on TextCell (id=targetDays_Cell) at AdminDataUploadActPatternLV.pcf: line 85, column 42
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.TargetDays")
    }
    
    // 'label' attribute on TextCell (id=targetHours_Cell) at AdminDataUploadActPatternLV.pcf: line 90, column 42
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.TargetHours")
    }
    
    // 'label' attribute on TypeKeyCell (id=targetIncludeDays_Cell) at AdminDataUploadActPatternLV.pcf: line 95, column 48
    function label_29 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.TargetIncludeDays")
    }
    
    // 'label' attribute on TextCell (id=code_Cell) at AdminDataUploadActPatternLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Code")
    }
    
    // 'label' attribute on TypeKeyCell (id=targetStartPoint_Cell) at AdminDataUploadActPatternLV.pcf: line 100, column 47
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.TargetStartPoint")
    }
    
    // 'label' attribute on TextCell (id=escalationDays_Cell) at AdminDataUploadActPatternLV.pcf: line 105, column 42
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.EscalationDays")
    }
    
    // 'label' attribute on TextCell (id=escalationHours_Cell) at AdminDataUploadActPatternLV.pcf: line 110, column 42
    function label_35 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.EscalationHours")
    }
    
    // 'label' attribute on TypeKeyCell (id=escalationStartPt_Cell) at AdminDataUploadActPatternLV.pcf: line 115, column 47
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.EscalationStartPoint")
    }
    
    // 'label' attribute on BooleanRadioCell (id=automatedOnly_Cell) at AdminDataUploadActPatternLV.pcf: line 120, column 42
    function label_39 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.AutomatedOnly")
    }
    
    // 'label' attribute on TextCell (id=command_Cell) at AdminDataUploadActPatternLV.pcf: line 125, column 41
    function label_41 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Command")
    }
    
    // 'label' attribute on TextCell (id=queue_Cell) at AdminDataUploadActPatternLV.pcf: line 130, column 41
    function label_43 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Queue")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadActPatternLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.PublicID")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadActPatternLV.pcf: line 40, column 42
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'label' attribute on TextCell (id=subject_Cell) at AdminDataUploadActPatternLV.pcf: line 45, column 41
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Subject")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadActPatternLV.pcf: line 23, column 46
    function sortValue_1 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return processor.getLoadStatus(activityPattern)
    }
    
    // 'value' attribute on TextCell (id=subject_Cell) at AdminDataUploadActPatternLV.pcf: line 45, column 41
    function sortValue_10 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.Subject
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at AdminDataUploadActPatternLV.pcf: line 50, column 41
    function sortValue_12 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.Description
    }
    
    // 'value' attribute on TypeKeyCell (id=activityClass_Cell) at AdminDataUploadActPatternLV.pcf: line 55, column 46
    function sortValue_14 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.ActivityClass
    }
    
    // 'value' attribute on TypeKeyCell (id=activityType_Cell) at AdminDataUploadActPatternLV.pcf: line 60, column 45
    function sortValue_16 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.ActivityType
    }
    
    // 'value' attribute on TextCell (id=category_Cell) at AdminDataUploadActPatternLV.pcf: line 65, column 41
    function sortValue_18 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.Category
    }
    
    // 'value' attribute on BooleanRadioCell (id=mandatory_Cell) at AdminDataUploadActPatternLV.pcf: line 70, column 42
    function sortValue_20 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.Mandatory
    }
    
    // 'value' attribute on TextCell (id=priority_Cell) at AdminDataUploadActPatternLV.pcf: line 75, column 41
    function sortValue_22 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.Priority
    }
    
    // 'value' attribute on BooleanRadioCell (id=recurring_Cell) at AdminDataUploadActPatternLV.pcf: line 80, column 42
    function sortValue_24 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.Recurring
    }
    
    // 'value' attribute on TextCell (id=targetDays_Cell) at AdminDataUploadActPatternLV.pcf: line 85, column 42
    function sortValue_26 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.TargetDays
    }
    
    // 'value' attribute on TextCell (id=targetHours_Cell) at AdminDataUploadActPatternLV.pcf: line 90, column 42
    function sortValue_28 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.TargetHours
    }
    
    // 'value' attribute on TypeKeyCell (id=targetIncludeDays_Cell) at AdminDataUploadActPatternLV.pcf: line 95, column 48
    function sortValue_30 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.TargetIncludeDays
    }
    
    // 'value' attribute on TypeKeyCell (id=targetStartPoint_Cell) at AdminDataUploadActPatternLV.pcf: line 100, column 47
    function sortValue_32 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.TargetStartPoint
    }
    
    // 'value' attribute on TextCell (id=escalationDays_Cell) at AdminDataUploadActPatternLV.pcf: line 105, column 42
    function sortValue_34 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.EscalationDays
    }
    
    // 'value' attribute on TextCell (id=escalationHours_Cell) at AdminDataUploadActPatternLV.pcf: line 110, column 42
    function sortValue_36 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.EscalationHours
    }
    
    // 'value' attribute on TypeKeyCell (id=escalationStartPt_Cell) at AdminDataUploadActPatternLV.pcf: line 115, column 47
    function sortValue_38 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.EscalationStartPt
    }
    
    // 'value' attribute on TextCell (id=code_Cell) at AdminDataUploadActPatternLV.pcf: line 29, column 41
    function sortValue_4 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.Code
    }
    
    // 'value' attribute on BooleanRadioCell (id=automatedOnly_Cell) at AdminDataUploadActPatternLV.pcf: line 120, column 42
    function sortValue_40 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.AutomatedOnly
    }
    
    // 'value' attribute on TextCell (id=command_Cell) at AdminDataUploadActPatternLV.pcf: line 125, column 41
    function sortValue_42 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.Command
    }
    
    // 'value' attribute on TextCell (id=queue_Cell) at AdminDataUploadActPatternLV.pcf: line 130, column 41
    function sortValue_44 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.Queue.DisplayName
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadActPatternLV.pcf: line 35, column 41
    function sortValue_6 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.PublicID
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadActPatternLV.pcf: line 40, column 42
    function sortValue_8 (activityPattern :  tdic.util.dataloader.data.admindata.ActivityPatternData) : java.lang.Object {
      return activityPattern.Exclude
    }
    
    // 'value' attribute on RowIterator (id=actiivtyPatternID) at AdminDataUploadActPatternLV.pcf: line 15, column 102
    function value_156 () : java.util.ArrayList<tdic.util.dataloader.data.admindata.ActivityPatternData> {
      return processor.ActivityPatternArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadActPatternLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadActPatternLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminDataUploadActPatternLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at AdminDataUploadActPatternLV.pcf: line 17, column 110
    function highlighted_155 () : java.lang.Boolean {
      return (activityPattern.Error or activityPattern.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on BooleanRadioCell (id=recurring_Cell) at AdminDataUploadActPatternLV.pcf: line 80, column 42
    function label_100 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Recurring")
    }
    
    // 'label' attribute on TextCell (id=targetDays_Cell) at AdminDataUploadActPatternLV.pcf: line 85, column 42
    function label_105 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.TargetDays")
    }
    
    // 'label' attribute on TextCell (id=targetHours_Cell) at AdminDataUploadActPatternLV.pcf: line 90, column 42
    function label_110 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.TargetHours")
    }
    
    // 'label' attribute on TypeKeyCell (id=targetIncludeDays_Cell) at AdminDataUploadActPatternLV.pcf: line 95, column 48
    function label_115 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.TargetIncludeDays")
    }
    
    // 'label' attribute on TypeKeyCell (id=targetStartPoint_Cell) at AdminDataUploadActPatternLV.pcf: line 100, column 47
    function label_120 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.TargetStartPoint")
    }
    
    // 'label' attribute on TextCell (id=escalationDays_Cell) at AdminDataUploadActPatternLV.pcf: line 105, column 42
    function label_125 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.EscalationDays")
    }
    
    // 'label' attribute on TextCell (id=escalationHours_Cell) at AdminDataUploadActPatternLV.pcf: line 110, column 42
    function label_130 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.EscalationHours")
    }
    
    // 'label' attribute on TypeKeyCell (id=escalationStartPt_Cell) at AdminDataUploadActPatternLV.pcf: line 115, column 47
    function label_135 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.EscalationStartPoint")
    }
    
    // 'label' attribute on BooleanRadioCell (id=automatedOnly_Cell) at AdminDataUploadActPatternLV.pcf: line 120, column 42
    function label_140 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.AutomatedOnly")
    }
    
    // 'label' attribute on TextCell (id=command_Cell) at AdminDataUploadActPatternLV.pcf: line 125, column 41
    function label_145 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Command")
    }
    
    // 'label' attribute on TextCell (id=queue_Cell) at AdminDataUploadActPatternLV.pcf: line 130, column 41
    function label_150 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Queue")
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadActPatternLV.pcf: line 23, column 46
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=code_Cell) at AdminDataUploadActPatternLV.pcf: line 29, column 41
    function label_50 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Code")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadActPatternLV.pcf: line 35, column 41
    function label_55 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.PublicID")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadActPatternLV.pcf: line 40, column 42
    function label_60 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'label' attribute on TextCell (id=subject_Cell) at AdminDataUploadActPatternLV.pcf: line 45, column 41
    function label_65 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Subject")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at AdminDataUploadActPatternLV.pcf: line 50, column 41
    function label_70 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Description")
    }
    
    // 'label' attribute on TypeKeyCell (id=activityClass_Cell) at AdminDataUploadActPatternLV.pcf: line 55, column 46
    function label_75 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.ActivityClass")
    }
    
    // 'label' attribute on TypeKeyCell (id=activityType_Cell) at AdminDataUploadActPatternLV.pcf: line 60, column 45
    function label_80 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.ActivityType")
    }
    
    // 'label' attribute on TextCell (id=category_Cell) at AdminDataUploadActPatternLV.pcf: line 65, column 41
    function label_85 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Category")
    }
    
    // 'label' attribute on BooleanRadioCell (id=mandatory_Cell) at AdminDataUploadActPatternLV.pcf: line 70, column 42
    function label_90 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Mandatory")
    }
    
    // 'label' attribute on TextCell (id=priority_Cell) at AdminDataUploadActPatternLV.pcf: line 75, column 41
    function label_95 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPattern.Priority")
    }
    
    // 'value' attribute on TextCell (id=queue_Cell) at AdminDataUploadActPatternLV.pcf: line 130, column 41
    function valueRoot_152 () : java.lang.Object {
      return activityPattern.Queue
    }
    
    // 'value' attribute on TextCell (id=code_Cell) at AdminDataUploadActPatternLV.pcf: line 29, column 41
    function valueRoot_52 () : java.lang.Object {
      return activityPattern
    }
    
    // 'value' attribute on BooleanRadioCell (id=recurring_Cell) at AdminDataUploadActPatternLV.pcf: line 80, column 42
    function value_101 () : java.lang.Boolean {
      return activityPattern.Recurring
    }
    
    // 'value' attribute on TextCell (id=targetDays_Cell) at AdminDataUploadActPatternLV.pcf: line 85, column 42
    function value_106 () : java.lang.Integer {
      return activityPattern.TargetDays
    }
    
    // 'value' attribute on TextCell (id=targetHours_Cell) at AdminDataUploadActPatternLV.pcf: line 90, column 42
    function value_111 () : java.lang.Integer {
      return activityPattern.TargetHours
    }
    
    // 'value' attribute on TypeKeyCell (id=targetIncludeDays_Cell) at AdminDataUploadActPatternLV.pcf: line 95, column 48
    function value_116 () : typekey.IncludeDaysType {
      return activityPattern.TargetIncludeDays
    }
    
    // 'value' attribute on TypeKeyCell (id=targetStartPoint_Cell) at AdminDataUploadActPatternLV.pcf: line 100, column 47
    function value_121 () : typekey.StartPointType {
      return activityPattern.TargetStartPoint
    }
    
    // 'value' attribute on TextCell (id=escalationDays_Cell) at AdminDataUploadActPatternLV.pcf: line 105, column 42
    function value_126 () : java.lang.Integer {
      return activityPattern.EscalationDays
    }
    
    // 'value' attribute on TextCell (id=escalationHours_Cell) at AdminDataUploadActPatternLV.pcf: line 110, column 42
    function value_131 () : java.lang.Integer {
      return activityPattern.EscalationHours
    }
    
    // 'value' attribute on TypeKeyCell (id=escalationStartPt_Cell) at AdminDataUploadActPatternLV.pcf: line 115, column 47
    function value_136 () : typekey.StartPointType {
      return activityPattern.EscalationStartPt
    }
    
    // 'value' attribute on BooleanRadioCell (id=automatedOnly_Cell) at AdminDataUploadActPatternLV.pcf: line 120, column 42
    function value_141 () : java.lang.Boolean {
      return activityPattern.AutomatedOnly
    }
    
    // 'value' attribute on TextCell (id=command_Cell) at AdminDataUploadActPatternLV.pcf: line 125, column 41
    function value_146 () : java.lang.String {
      return activityPattern.Command
    }
    
    // 'value' attribute on TextCell (id=queue_Cell) at AdminDataUploadActPatternLV.pcf: line 130, column 41
    function value_151 () : java.lang.String {
      return activityPattern.Queue.DisplayName
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadActPatternLV.pcf: line 23, column 46
    function value_46 () : java.lang.String {
      return processor.getLoadStatus(activityPattern)
    }
    
    // 'value' attribute on TextCell (id=code_Cell) at AdminDataUploadActPatternLV.pcf: line 29, column 41
    function value_51 () : java.lang.String {
      return activityPattern.Code
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadActPatternLV.pcf: line 35, column 41
    function value_56 () : java.lang.String {
      return activityPattern.PublicID
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadActPatternLV.pcf: line 40, column 42
    function value_61 () : java.lang.Boolean {
      return activityPattern.Exclude
    }
    
    // 'value' attribute on TextCell (id=subject_Cell) at AdminDataUploadActPatternLV.pcf: line 45, column 41
    function value_66 () : java.lang.String {
      return activityPattern.Subject
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at AdminDataUploadActPatternLV.pcf: line 50, column 41
    function value_71 () : java.lang.String {
      return activityPattern.Description
    }
    
    // 'value' attribute on TypeKeyCell (id=activityClass_Cell) at AdminDataUploadActPatternLV.pcf: line 55, column 46
    function value_76 () : typekey.ActivityClass {
      return activityPattern.ActivityClass
    }
    
    // 'value' attribute on TypeKeyCell (id=activityType_Cell) at AdminDataUploadActPatternLV.pcf: line 60, column 45
    function value_81 () : typekey.ActivityType {
      return activityPattern.ActivityType
    }
    
    // 'value' attribute on TextCell (id=category_Cell) at AdminDataUploadActPatternLV.pcf: line 65, column 41
    function value_86 () : java.lang.String {
      return activityPattern.Category
    }
    
    // 'value' attribute on BooleanRadioCell (id=mandatory_Cell) at AdminDataUploadActPatternLV.pcf: line 70, column 42
    function value_91 () : java.lang.Boolean {
      return activityPattern.Mandatory
    }
    
    // 'value' attribute on TextCell (id=priority_Cell) at AdminDataUploadActPatternLV.pcf: line 75, column 41
    function value_96 () : java.lang.String {
      return activityPattern.Priority
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadActPatternLV.pcf: line 23, column 46
    function visible_47 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get activityPattern () : tdic.util.dataloader.data.admindata.ActivityPatternData {
      return getIteratedValue(1) as tdic.util.dataloader.data.admindata.ActivityPatternData
    }
    
    
  }
  
  
}