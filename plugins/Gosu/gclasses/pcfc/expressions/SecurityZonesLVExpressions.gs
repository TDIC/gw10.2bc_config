package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/securityzones/SecurityZonesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SecurityZonesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/securityzones/SecurityZonesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SecurityZonesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at SecurityZonesLV.pcf: line 25, column 38
    function action_1 () : void {
      SecurityZoneDetail.go(securityZone)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at SecurityZonesLV.pcf: line 25, column 38
    function action_dest_2 () : pcf.api.Destination {
      return pcf.SecurityZoneDetail.createDestination(securityZone)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at SecurityZonesLV.pcf: line 25, column 38
    function valueRoot_4 () : java.lang.Object {
      return securityZone
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at SecurityZonesLV.pcf: line 25, column 38
    function value_3 () : java.lang.String {
      return securityZone.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at SecurityZonesLV.pcf: line 30, column 45
    function value_6 () : java.lang.String {
      return securityZone.Description
    }
    
    property get securityZone () : entity.SecurityZone {
      return getIteratedValue(1) as entity.SecurityZone
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/securityzones/SecurityZonesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SecurityZonesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at SecurityZonesLV.pcf: line 25, column 38
    function sortValue_0 (securityZone :  entity.SecurityZone) : java.lang.Object {
      return securityZone.Name
    }
    
    // 'value' attribute on RowIterator at SecurityZonesLV.pcf: line 16, column 79
    function value_9 () : gw.api.database.IQueryBeanResult<entity.SecurityZone> {
      return securityZoneList
    }
    
    property get securityZoneList () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return getRequireValue("securityZoneList", 0) as gw.api.database.IQueryBeanResult<SecurityZone>
    }
    
    property set securityZoneList ($arg :  gw.api.database.IQueryBeanResult<SecurityZone>) {
      setRequireValue("securityZoneList", 0, $arg)
    }
    
    
  }
  
  
}