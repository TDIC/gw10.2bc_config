package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/chargepatterns/ChargeBreakdownCategoryTypeDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChargeBreakdownCategoryTypeDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/chargepatterns/ChargeBreakdownCategoryTypeDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChargeBreakdownCategoryTypeDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at ChargeBreakdownCategoryTypeDetailDV.pcf: line 17, column 36
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      categoryType.Code = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at ChargeBreakdownCategoryTypeDetailDV.pcf: line 23, column 36
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      categoryType.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at ChargeBreakdownCategoryTypeDetailDV.pcf: line 17, column 36
    function valueRoot_2 () : java.lang.Object {
      return categoryType
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at ChargeBreakdownCategoryTypeDetailDV.pcf: line 17, column 36
    function value_0 () : java.lang.String {
      return categoryType.Code
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at ChargeBreakdownCategoryTypeDetailDV.pcf: line 23, column 36
    function value_4 () : java.lang.String {
      return categoryType.Name
    }
    
    property get categoryType () : ChargeBreakdownCategoryType {
      return getRequireValue("categoryType", 0) as ChargeBreakdownCategoryType
    }
    
    property set categoryType ($arg :  ChargeBreakdownCategoryType) {
      setRequireValue("categoryType", 0, $arg)
    }
    
    
  }
  
  
}