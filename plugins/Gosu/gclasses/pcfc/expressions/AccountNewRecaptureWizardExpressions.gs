package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/AccountNewRecaptureWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountNewRecaptureWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/AccountNewRecaptureWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountNewRecaptureWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Wizard (id=AccountNewRecaptureWizard) at AccountNewRecaptureWizard.pcf: line 11, column 36
    function beforeCommit_3 (pickedValue :  java.lang.Object) : void {
      helper.BillingInstruction.execute()
    }
    
    // 'canVisit' attribute on Wizard (id=AccountNewRecaptureWizard) at AccountNewRecaptureWizard.pcf: line 11, column 36
    static function canVisit_4 (account :  Account) : java.lang.Boolean {
      return perm.Transaction.gentxn
    }
    
    // 'initialValue' attribute on Variable at AccountNewRecaptureWizard.pcf: line 20, column 54
    function initialValue_0 () : gw.accounting.NewRecaptureChargeHelper {
      return new gw.accounting.NewRecaptureChargeHelper(account)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountNewRecaptureWizard.pcf: line 26, column 81
    function screen_onEnter_1 (def :  pcf.RecaptureDetailsScreen) : void {
      def.onEnter(helper, account)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountNewRecaptureWizard.pcf: line 26, column 81
    function screen_refreshVariables_2 (def :  pcf.RecaptureDetailsScreen) : void {
      def.refreshVariables(helper, account)
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewRecaptureWizard) at AccountNewRecaptureWizard.pcf: line 11, column 36
    function tabBar_onEnter_5 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewRecaptureWizard) at AccountNewRecaptureWizard.pcf: line 11, column 36
    function tabBar_refreshVariables_6 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AccountNewRecaptureWizard {
      return super.CurrentLocation as pcf.AccountNewRecaptureWizard
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get helper () : gw.accounting.NewRecaptureChargeHelper {
      return getVariableValue("helper", 0) as gw.accounting.NewRecaptureChargeHelper
    }
    
    property set helper ($arg :  gw.accounting.NewRecaptureChargeHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    
  }
  
  
}