package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException
@javax.annotation.Generated("config/web/pcf/payment/NewMultiPaymentScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewMultiPaymentScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/NewMultiPaymentScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends NewMultiPaymentScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=Account_Cell) at NewMultiPaymentScreen.pcf: line 85, column 42
    function action_23 () : void {
      AccountSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerCell (id=Invoice_Cell) at NewMultiPaymentScreen.pcf: line 97, column 40
    function action_33 () : void {
      InvoiceSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerCell (id=PolicyPeriod_Cell) at NewMultiPaymentScreen.pcf: line 107, column 42
    function action_45 () : void {
      PolicySearchPopup.push(true)
    }
    
    // 'pickLocation' attribute on PickerCell (id=Account_Cell) at NewMultiPaymentScreen.pcf: line 85, column 42
    function action_dest_24 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'pickLocation' attribute on PickerCell (id=Invoice_Cell) at NewMultiPaymentScreen.pcf: line 97, column 40
    function action_dest_34 () : pcf.api.Destination {
      return pcf.InvoiceSearchPopup.createDestination()
    }
    
    // 'pickLocation' attribute on PickerCell (id=PolicyPeriod_Cell) at NewMultiPaymentScreen.pcf: line 107, column 42
    function action_dest_46 () : pcf.api.Destination {
      return pcf.PolicySearchPopup.createDestination(true)
    }
    
    // 'available' attribute on TextCell (id=RefNumber_Cell) at NewMultiPaymentScreen.pcf: line 155, column 43
    function available_71 () : java.lang.Boolean {
      return newPayment.PaymentInstrument.PaymentMethod == typekey.PaymentMethod.TC_CHECK
    }
    
    // 'checkBoxVisible' attribute on RowIterator at NewMultiPaymentScreen.pcf: line 73, column 43
    function checkBoxVisible_88 () : java.lang.Boolean {
      return editing
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at NewMultiPaymentScreen.pcf: line 164, column 40
    function currency_81 () : typekey.Currency {
      return currency
    }
    
    // 'value' attribute on PickerCell (id=Account_Cell) at NewMultiPaymentScreen.pcf: line 85, column 42
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPayment.AccountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PickerCell (id=Invoice_Cell) at NewMultiPaymentScreen.pcf: line 97, column 40
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPayment.Invoice = (__VALUE_TO_SET as entity.Invoice)
    }
    
    // 'value' attribute on PickerCell (id=PolicyPeriod_Cell) at NewMultiPaymentScreen.pcf: line 107, column 42
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPayment.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentType_Cell) at NewMultiPaymentScreen.pcf: line 127, column 50
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPayment.SuspensePayment = (__VALUE_TO_SET as typekey.MultiPaymentType)
    }
    
    // 'value' attribute on DateCell (id=PaymentDate_Cell) at NewMultiPaymentScreen.pcf: line 137, column 45
    function defaultSetter_61 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPayment.PaymentDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeCell (id=PaymentInstrument_Cell) at NewMultiPaymentScreen.pcf: line 146, column 50
    function defaultSetter_65 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPayment.PaymentInstrument = (__VALUE_TO_SET as entity.PaymentInstrument)
    }
    
    // 'value' attribute on TextCell (id=RefNumber_Cell) at NewMultiPaymentScreen.pcf: line 155, column 43
    function defaultSetter_73 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPayment.RefNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at NewMultiPaymentScreen.pcf: line 164, column 40
    function defaultSetter_79 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPayment.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at NewMultiPaymentScreen.pcf: line 169, column 45
    function defaultSetter_85 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPayment.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on PickerCell (id=Account_Cell) at NewMultiPaymentScreen.pcf: line 85, column 42
    function editable_25 () : java.lang.Boolean {
      return newPayment.PolicyNumber == null and newPayment.Invoice == null and newPayment.Producer == null
    }
    
    // 'editable' attribute on PickerCell (id=Invoice_Cell) at NewMultiPaymentScreen.pcf: line 97, column 40
    function editable_35 () : java.lang.Boolean {
      return newPayment.AccountNumber == null and newPayment.PolicyNumber == null and newPayment.Producer == null and newPayment.SuspensePayment == MultiPaymentType.TC_PAYMENT
    }
    
    // 'editable' attribute on PickerCell (id=PolicyPeriod_Cell) at NewMultiPaymentScreen.pcf: line 107, column 42
    function editable_47 () : java.lang.Boolean {
      return newPayment.AccountNumber == null and newPayment.Invoice == null and newPayment.Producer == null
    }
    
    // 'inputConversion' attribute on PickerCell (id=Invoice_Cell) at NewMultiPaymentScreen.pcf: line 97, column 40
    function inputConversion_37 (VALUE :  java.lang.String) : java.lang.Object {
      return PaymentEntry.findInvoiceFromInvoiceNumber(VALUE)
    }
    
    // 'onChange' attribute on PostOnChange at NewMultiPaymentScreen.pcf: line 130, column 149
    function onChange_55 () : void {
      if (newPayment.SuspensePayment == MultiPaymentType.TC_SUSPENSE) { newPayment.Invoice = null; newPayment.Producer = null }
    }
    
    // 'outputConversion' attribute on PickerCell (id=Invoice_Cell) at NewMultiPaymentScreen.pcf: line 97, column 40
    function outputConversion_38 (VALUE :  entity.Invoice) : java.lang.String {
      return newPayment.Invoice.InvoiceNumber
    }
    
    // 'validationExpression' attribute on PickerCell (id=Account_Cell) at NewMultiPaymentScreen.pcf: line 85, column 42
    function validationExpression_26 () : java.lang.Object {
      return newPayment.UnusedEntry ? null : newPayment.verifyAccount()
    }
    
    // 'validationExpression' attribute on PickerCell (id=Invoice_Cell) at NewMultiPaymentScreen.pcf: line 97, column 40
    function validationExpression_36 () : java.lang.Object {
      return newPayment.UnusedEntry ? null : newPayment.verifyInvoice()
    }
    
    // 'validationExpression' attribute on PickerCell (id=PolicyPeriod_Cell) at NewMultiPaymentScreen.pcf: line 107, column 42
    function validationExpression_48 () : java.lang.Object {
      return newPayment.UnusedEntry ? null : newPayment.verifyPolicy()
    }
    
    // 'validationExpression' attribute on MonetaryAmountCell (id=Amount_Cell) at NewMultiPaymentScreen.pcf: line 164, column 40
    function validationExpression_77 () : java.lang.Object {
      return newPayment.isUnusedEntry() ? null : newPayment.verifyAmount()
    }
    
    // 'valueRange' attribute on RangeCell (id=PaymentInstrument_Cell) at NewMultiPaymentScreen.pcf: line 146, column 50
    function valueRange_67 () : java.lang.Object {
      return paymentInstrumentRange.AvailableInstruments
    }
    
    // 'value' attribute on PickerCell (id=Account_Cell) at NewMultiPaymentScreen.pcf: line 85, column 42
    function valueRoot_29 () : java.lang.Object {
      return newPayment
    }
    
    // 'value' attribute on PickerCell (id=Account_Cell) at NewMultiPaymentScreen.pcf: line 85, column 42
    function value_27 () : java.lang.String {
      return newPayment.AccountNumber
    }
    
    // 'value' attribute on PickerCell (id=Invoice_Cell) at NewMultiPaymentScreen.pcf: line 97, column 40
    function value_39 () : entity.Invoice {
      return newPayment.Invoice
    }
    
    // 'value' attribute on PickerCell (id=PolicyPeriod_Cell) at NewMultiPaymentScreen.pcf: line 107, column 42
    function value_49 () : java.lang.String {
      return newPayment.PolicyNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentType_Cell) at NewMultiPaymentScreen.pcf: line 127, column 50
    function value_56 () : typekey.MultiPaymentType {
      return newPayment.SuspensePayment
    }
    
    // 'value' attribute on DateCell (id=PaymentDate_Cell) at NewMultiPaymentScreen.pcf: line 137, column 45
    function value_60 () : java.util.Date {
      return newPayment.PaymentDate
    }
    
    // 'value' attribute on RangeCell (id=PaymentInstrument_Cell) at NewMultiPaymentScreen.pcf: line 146, column 50
    function value_64 () : entity.PaymentInstrument {
      return newPayment.PaymentInstrument
    }
    
    // 'value' attribute on TextCell (id=RefNumber_Cell) at NewMultiPaymentScreen.pcf: line 155, column 43
    function value_72 () : java.lang.String {
      return newPayment.RefNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at NewMultiPaymentScreen.pcf: line 164, column 40
    function value_78 () : gw.pl.currency.MonetaryAmount {
      return newPayment.Amount
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at NewMultiPaymentScreen.pcf: line 169, column 45
    function value_84 () : java.lang.String {
      return newPayment.Description
    }
    
    // 'valueRange' attribute on RangeCell (id=PaymentInstrument_Cell) at NewMultiPaymentScreen.pcf: line 146, column 50
    function verifyValueRangeIsAllowedType_68 ($$arg :  entity.PaymentInstrument[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=PaymentInstrument_Cell) at NewMultiPaymentScreen.pcf: line 146, column 50
    function verifyValueRangeIsAllowedType_68 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentInstrument>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=PaymentInstrument_Cell) at NewMultiPaymentScreen.pcf: line 146, column 50
    function verifyValueRangeIsAllowedType_68 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=PaymentInstrument_Cell) at NewMultiPaymentScreen.pcf: line 146, column 50
    function verifyValueRange_69 () : void {
      var __valueRangeArg = paymentInstrumentRange.AvailableInstruments
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_68(__valueRangeArg)
    }
    
    property get newPayment () : entity.PaymentEntry {
      return getIteratedValue(1) as entity.PaymentEntry
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/payment/NewMultiPaymentScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewMultiPaymentScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=SplitMultiPayment) at NewMultiPaymentScreen.pcf: line 48, column 28
    function allCheckedRowsAction_6 (CheckedValues :  entity.PaymentEntry[], CheckedValuesErrors :  java.util.Map) : void {
      validateRowOkToSplit(CheckedValues[0]);AgencyMultiPaymentSplitConfirmationPopup.push(CheckedValues[0])
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=ControlAmountValidation_Input) at NewMultiPaymentScreen.pcf: line 60, column 60
    function currency_10 () : typekey.Currency {
      return currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ControlAmountValidation_Input) at NewMultiPaymentScreen.pcf: line 60, column 60
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      newMultiPayment.ControlAmountValidation = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'initialValue' attribute on Variable at NewMultiPaymentScreen.pcf: line 17, column 54
    function initialValue_0 () : gw.api.web.payment.SuspensePaymentUtil {
      return new gw.api.web.payment.SuspensePaymentUtil()
    }
    
    // 'initialValue' attribute on Variable at NewMultiPaymentScreen.pcf: line 21, column 57
    function initialValue_1 () : gw.api.web.account.AccountSearchConverter {
      return new gw.api.web.account.AccountSearchConverter()
    }
    
    // 'initialValue' attribute on Variable at NewMultiPaymentScreen.pcf: line 25, column 55
    function initialValue_2 () : gw.api.web.policy.PolicySearchConverter {
      return new gw.api.web.policy.PolicySearchConverter()
    }
    
    // 'initialValue' attribute on Variable at NewMultiPaymentScreen.pcf: line 32, column 49
    function initialValue_3 () : gw.payment.PaymentInstrumentRange {
      return new gw.payment.PaymentInstrumentRange( getMultiPaymentInstruments() )
    }
    
    // 'value' attribute on PickerCell (id=Account_Cell) at NewMultiPaymentScreen.pcf: line 85, column 42
    function sortValue_12 (newPayment :  entity.PaymentEntry) : java.lang.Object {
      return newPayment.AccountNumber
    }
    
    // 'value' attribute on PickerCell (id=Invoice_Cell) at NewMultiPaymentScreen.pcf: line 97, column 40
    function sortValue_13 (newPayment :  entity.PaymentEntry) : java.lang.Object {
      return newPayment.Invoice
    }
    
    // 'value' attribute on PickerCell (id=PolicyPeriod_Cell) at NewMultiPaymentScreen.pcf: line 107, column 42
    function sortValue_14 (newPayment :  entity.PaymentEntry) : java.lang.Object {
      return newPayment.PolicyNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentType_Cell) at NewMultiPaymentScreen.pcf: line 127, column 50
    function sortValue_15 (newPayment :  entity.PaymentEntry) : java.lang.Object {
      return newPayment.SuspensePayment
    }
    
    // 'value' attribute on DateCell (id=PaymentDate_Cell) at NewMultiPaymentScreen.pcf: line 137, column 45
    function sortValue_16 (newPayment :  entity.PaymentEntry) : java.lang.Object {
      return newPayment.PaymentDate
    }
    
    // 'value' attribute on RangeCell (id=PaymentInstrument_Cell) at NewMultiPaymentScreen.pcf: line 146, column 50
    function sortValue_17 (newPayment :  entity.PaymentEntry) : java.lang.Object {
      return newPayment.PaymentInstrument
    }
    
    // 'value' attribute on TextCell (id=RefNumber_Cell) at NewMultiPaymentScreen.pcf: line 155, column 43
    function sortValue_18 (newPayment :  entity.PaymentEntry) : java.lang.Object {
      return newPayment.RefNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at NewMultiPaymentScreen.pcf: line 164, column 40
    function sortValue_19 (newPayment :  entity.PaymentEntry) : java.lang.Object {
      return newPayment.Amount
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at NewMultiPaymentScreen.pcf: line 169, column 45
    function sortValue_20 (newPayment :  entity.PaymentEntry) : java.lang.Object {
      return newPayment.Description
    }
    
    // '$$sumValue' attribute on RowIterator at NewMultiPaymentScreen.pcf: line 164, column 40
    function sumValueRoot_22 (newPayment :  entity.PaymentEntry) : java.lang.Object {
      return newPayment
    }
    
    // 'footerSumValue' attribute on RowIterator at NewMultiPaymentScreen.pcf: line 164, column 40
    function sumValue_21 (newPayment :  entity.PaymentEntry) : java.lang.Object {
      return newPayment.Amount
    }
    
    // 'toCreateAndAdd' attribute on AddButton (id=AddEmptyPayments) at NewMultiPaymentScreen.pcf: line 38, column 65
    function toCreateAndAdd_4 (CheckedValues :  Object[]) : java.lang.Object {
      return newMultiPayment.addEmptyPaymentRows()
    }
    
    // 'toRemove' attribute on RowIterator at NewMultiPaymentScreen.pcf: line 73, column 43
    function toRemove_89 (newPayment :  entity.PaymentEntry) : void {
      newMultiPayment.removeFromPayments(newPayment)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ControlAmountValidation_Input) at NewMultiPaymentScreen.pcf: line 60, column 60
    function valueRoot_9 () : java.lang.Object {
      return newMultiPayment
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ControlAmountValidation_Input) at NewMultiPaymentScreen.pcf: line 60, column 60
    function value_7 () : gw.pl.currency.MonetaryAmount {
      return newMultiPayment.ControlAmountValidation
    }
    
    // 'value' attribute on RowIterator at NewMultiPaymentScreen.pcf: line 73, column 43
    function value_90 () : entity.PaymentEntry[] {
      return editing ? newMultiPayment.Payments : newMultiPayment.NonBlankPaymentEntries
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=SplitMultiPayment) at NewMultiPaymentScreen.pcf: line 48, column 28
    function visible_5 () : java.lang.Boolean {
      return editing
    }
    
    property get accountSearchConverter () : gw.api.web.account.AccountSearchConverter {
      return getVariableValue("accountSearchConverter", 0) as gw.api.web.account.AccountSearchConverter
    }
    
    property set accountSearchConverter ($arg :  gw.api.web.account.AccountSearchConverter) {
      setVariableValue("accountSearchConverter", 0, $arg)
    }
    
    property get currency () : Currency {
      return getRequireValue("currency", 0) as Currency
    }
    
    property set currency ($arg :  Currency) {
      setRequireValue("currency", 0, $arg)
    }
    
    property get editing () : boolean {
      return getRequireValue("editing", 0) as java.lang.Boolean
    }
    
    property set editing ($arg :  boolean) {
      setRequireValue("editing", 0, $arg)
    }
    
    property get newMultiPayment () : NewMultiPayment {
      return getRequireValue("newMultiPayment", 0) as NewMultiPayment
    }
    
    property set newMultiPayment ($arg :  NewMultiPayment) {
      setRequireValue("newMultiPayment", 0, $arg)
    }
    
    property get paymentInstrumentRange () : gw.payment.PaymentInstrumentRange {
      return getVariableValue("paymentInstrumentRange", 0) as gw.payment.PaymentInstrumentRange
    }
    
    property set paymentInstrumentRange ($arg :  gw.payment.PaymentInstrumentRange) {
      setVariableValue("paymentInstrumentRange", 0, $arg)
    }
    
    property get policySearchConverter () : gw.api.web.policy.PolicySearchConverter {
      return getVariableValue("policySearchConverter", 0) as gw.api.web.policy.PolicySearchConverter
    }
    
    property set policySearchConverter ($arg :  gw.api.web.policy.PolicySearchConverter) {
      setVariableValue("policySearchConverter", 0, $arg)
    }
    
    property get suspensePaymentUtil () : gw.api.web.payment.SuspensePaymentUtil {
      return getVariableValue("suspensePaymentUtil", 0) as gw.api.web.payment.SuspensePaymentUtil
    }
    
    property set suspensePaymentUtil ($arg :  gw.api.web.payment.SuspensePaymentUtil) {
      setVariableValue("suspensePaymentUtil", 0, $arg)
    }
    
    
    function validateRowOkToSplit(rowToBeValidated : PaymentEntry) {
      if (!String.isEmpty(rowToBeValidated.AccountNumber)) {
        throw new DisplayableException(DisplayKey.get("Web.NewMultiPaymentScreen.CanNotSplitRowWithAccountNumber"))
      }
      else if (rowToBeValidated.IsSuspensePayment) {
        throw new DisplayableException(DisplayKey.get("Web.NewMultiPaymentScreen.CanNotSplitRowWithSuspensePayment"))
      }
      else if (rowToBeValidated.Producer == null) {
          throw new DisplayableException(DisplayKey.get("Web.NewMultiPaymentScreen.CanNotSplitRowNotForProducer"))
      }
      else if (rowToBeValidated.Amount == null || rowToBeValidated.Amount.signum() <=0) {
        throw new DisplayableException(DisplayKey.get("Web.NewMultiPaymentScreen.CanNotSplitRowWithNonPositiveAmount"))    
      }
      else if (!newMultiPayment.Payments.hasMatch( \ paymentEntry -> paymentEntry.isUnusedEntry())) {
        throw new DisplayableException(DisplayKey.get("Web.NewMultiPaymentScreen.CanNotSplitRowIfNoEmptyRowsAvailable"))
      }
    }
    
    function getMultiPaymentInstruments() : java.util.List<PaymentInstrument> {
      var instruments = new java.util.ArrayList<PaymentInstrument>() { 
    /**
         * US75
         * 10/17/2014 Alvin Lee
         * 
         * Disable Cash as an available payment instrument; only Check payments are allowed on this screen.
         */
       // gw.api.web.payment.PaymentInstrumentFactory.getCashPaymentInstrument(),
        gw.api.web.payment.PaymentInstrumentFactory.getCheckPaymentInstrument()
      }
      return instruments
    }
    
    
  }
  
  
}