package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountPaymentForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountPaymentForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountPaymentForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountPaymentForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (moneyReceivedID :  String) : int {
      return 0
    }
    
    // 'action' attribute on ForwardCondition at AccountPaymentForward.pcf: line 18, column 42
    function action_1 () : void {
      AccountPayments.go(moneyReceived.Account, moneyReceived)
    }
    
    // 'action' attribute on ForwardCondition at AccountPaymentForward.pcf: line 21, column 42
    function action_4 () : void {
      PaymentSearch.go()
    }
    
    // 'action' attribute on ForwardCondition at AccountPaymentForward.pcf: line 18, column 42
    function action_dest_2 () : pcf.api.Destination {
      return pcf.AccountPayments.createDestination(moneyReceived.Account, moneyReceived)
    }
    
    // 'action' attribute on ForwardCondition at AccountPaymentForward.pcf: line 21, column 42
    function action_dest_5 () : pcf.api.Destination {
      return pcf.PaymentSearch.createDestination()
    }
    
    // 'condition' attribute on ForwardCondition at AccountPaymentForward.pcf: line 18, column 42
    function condition_3 () : java.lang.Boolean {
      return moneyReceived != null
    }
    
    // 'condition' attribute on ForwardCondition at AccountPaymentForward.pcf: line 21, column 42
    function condition_6 () : java.lang.Boolean {
      return moneyReceived == null
    }
    
    // 'initialValue' attribute on Variable at AccountPaymentForward.pcf: line 15, column 35
    function initialValue_0 () : DirectBillMoneyRcvd {
      return gw.api.database.Query.make(DirectBillMoneyRcvd).compare(DirectBillMoneyRcvd#PublicID, Equals, moneyReceivedID).select().AtMostOneRow
    }
    
    override property get CurrentLocation () : pcf.AccountPaymentForward {
      return super.CurrentLocation as pcf.AccountPaymentForward
    }
    
    property get moneyReceived () : DirectBillMoneyRcvd {
      return getVariableValue("moneyReceived", 0) as DirectBillMoneyRcvd
    }
    
    property set moneyReceived ($arg :  DirectBillMoneyRcvd) {
      setVariableValue("moneyReceived", 0, $arg)
    }
    
    property get moneyReceivedID () : String {
      return getVariableValue("moneyReceivedID", 0) as String
    }
    
    property set moneyReceivedID ($arg :  String) {
      setVariableValue("moneyReceivedID", 0, $arg)
    }
    
    
  }
  
  
}