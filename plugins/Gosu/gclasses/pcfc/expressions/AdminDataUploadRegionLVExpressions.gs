package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadRegionLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadRegionLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadRegionLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadRegionLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadRegionLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at AdminDataUploadRegionLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Region.Name")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadRegionLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Region.PublicID")
    }
    
    // 'label' attribute on TextCell (id=zones_Cell) at AdminDataUploadRegionLV.pcf: line 40, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Region.Zones")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadRegionLV.pcf: line 45, column 42
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadRegionLV.pcf: line 23, column 46
    function sortValue_1 (region :  tdic.util.dataloader.data.admindata.RegionData) : java.lang.Object {
      return processor.getLoadStatus(region)
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadRegionLV.pcf: line 45, column 42
    function sortValue_10 (region :  tdic.util.dataloader.data.admindata.RegionData) : java.lang.Object {
      return region.Exclude
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadRegionLV.pcf: line 29, column 41
    function sortValue_4 (region :  tdic.util.dataloader.data.admindata.RegionData) : java.lang.Object {
      return region.Name
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadRegionLV.pcf: line 35, column 41
    function sortValue_6 (region :  tdic.util.dataloader.data.admindata.RegionData) : java.lang.Object {
      return region.PublicID
    }
    
    // 'value' attribute on TextCell (id=zones_Cell) at AdminDataUploadRegionLV.pcf: line 40, column 41
    function sortValue_8 (region :  tdic.util.dataloader.data.admindata.RegionData) : java.lang.Object {
      return region.Zones
    }
    
    // 'value' attribute on RowIterator (id=Region) at AdminDataUploadRegionLV.pcf: line 15, column 93
    function value_37 () : java.util.ArrayList<tdic.util.dataloader.data.admindata.RegionData> {
      return processor.RegionArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadRegionLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadRegionLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminDataUploadRegionLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at AdminDataUploadRegionLV.pcf: line 17, column 92
    function highlighted_36 () : java.lang.Boolean {
      return (region.Error or region.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadRegionLV.pcf: line 23, column 46
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at AdminDataUploadRegionLV.pcf: line 29, column 41
    function label_16 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Region.Name")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadRegionLV.pcf: line 35, column 41
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Region.PublicID")
    }
    
    // 'label' attribute on TextCell (id=zones_Cell) at AdminDataUploadRegionLV.pcf: line 40, column 41
    function label_26 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Region.Zones")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadRegionLV.pcf: line 45, column 42
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadRegionLV.pcf: line 29, column 41
    function valueRoot_18 () : java.lang.Object {
      return region
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadRegionLV.pcf: line 23, column 46
    function value_12 () : java.lang.String {
      return processor.getLoadStatus(region)
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadRegionLV.pcf: line 29, column 41
    function value_17 () : java.lang.String {
      return region.Name
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadRegionLV.pcf: line 35, column 41
    function value_22 () : java.lang.String {
      return region.PublicID
    }
    
    // 'value' attribute on TextCell (id=zones_Cell) at AdminDataUploadRegionLV.pcf: line 40, column 41
    function value_27 () : java.lang.String {
      return region.Zones
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadRegionLV.pcf: line 45, column 42
    function value_32 () : java.lang.Boolean {
      return region.Exclude
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadRegionLV.pcf: line 23, column 46
    function visible_13 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get region () : tdic.util.dataloader.data.admindata.RegionData {
      return getIteratedValue(1) as tdic.util.dataloader.data.admindata.RegionData
    }
    
    
  }
  
  
}