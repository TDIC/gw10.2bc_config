package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/BatchPaymentEntriesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BatchPaymentEntriesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/BatchPaymentEntriesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BatchPaymentEntriesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at BatchPaymentEntriesLV.pcf: line 15, column 49
    function initialValue_0 () : gw.payment.PaymentInstrumentRange {
      return new gw.payment.PaymentInstrumentRange( batchPaymentDetailsView.BatchPaymentInstruments )
    }
    
    // 'sortBy' attribute on IteratorSort at BatchPaymentEntriesLV.pcf: line 34, column 24
    function sortBy_1 (currentBatchPaymentEntry :  BatchPaymentEntry) : java.lang.Object {
      return currentBatchPaymentEntry.PaymentDate
    }
    
    // 'value' attribute on PickerCell (id=Producer_Cell) at BatchPaymentEntriesLV.pcf: line 131, column 39
    function sortValue_10 (currentBatchPaymentEntry :  BatchPaymentEntry) : java.lang.Object {
      return currentBatchPaymentEntry.Producer
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at BatchPaymentEntriesLV.pcf: line 140, column 24
    function sortValue_11 (currentBatchPaymentEntry :  BatchPaymentEntry) : java.lang.Object {
      return currentBatchPaymentEntry.Description
    }
    
    // 'value' attribute on RangeCell (id=PaymentType_Cell) at BatchPaymentEntriesLV.pcf: line 42, column 40
    function sortValue_2 (currentBatchPaymentEntry :  BatchPaymentEntry) : java.lang.Object {
      return currentBatchPaymentEntry.PaymentType
    }
    
    // 'value' attribute on DateCell (id=PaymentDate_Cell) at BatchPaymentEntriesLV.pcf: line 49, column 57
    function sortValue_3 (currentBatchPaymentEntry :  BatchPaymentEntry) : java.lang.Object {
      return currentBatchPaymentEntry.PaymentDate
    }
    
    // 'value' attribute on RangeCell (id=PaymentInstrument_Cell) at BatchPaymentEntriesLV.pcf: line 57, column 41
    function sortValue_4 (currentBatchPaymentEntry :  BatchPaymentEntry) : java.lang.Object {
      return currentBatchPaymentEntry.PaymentInstrument
    }
    
    // 'value' attribute on TextCell (id=RefNumber_Cell) at BatchPaymentEntriesLV.pcf: line 67, column 54
    function sortValue_5 (currentBatchPaymentEntry :  BatchPaymentEntry) : java.lang.Object {
      return currentBatchPaymentEntry.RefNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at BatchPaymentEntriesLV.pcf: line 80, column 51
    function sortValue_6 (currentBatchPaymentEntry :  BatchPaymentEntry) : java.lang.Object {
      return currentBatchPaymentEntry.Amount
    }
    
    // 'value' attribute on PickerCell (id=Account_Cell) at BatchPaymentEntriesLV.pcf: line 93, column 40
    function sortValue_7 (currentBatchPaymentEntry :  BatchPaymentEntry) : java.lang.Object {
      return currentBatchPaymentEntry.AccountNumber
    }
    
    // 'value' attribute on PickerCell (id=Invoice_Cell) at BatchPaymentEntriesLV.pcf: line 106, column 38
    function sortValue_8 (currentBatchPaymentEntry :  BatchPaymentEntry) : java.lang.Object {
      return currentBatchPaymentEntry.Invoice
    }
    
    // 'value' attribute on PickerCell (id=PolicyPeriod_Cell) at BatchPaymentEntriesLV.pcf: line 118, column 40
    function sortValue_9 (currentBatchPaymentEntry :  BatchPaymentEntry) : java.lang.Object {
      return currentBatchPaymentEntry.PolicyNumber
    }
    
    // '$$sumValue' attribute on RowIterator at BatchPaymentEntriesLV.pcf: line 80, column 51
    function sumValueRoot_13 (currentBatchPaymentEntry :  BatchPaymentEntry) : java.lang.Object {
      return currentBatchPaymentEntry
    }
    
    // 'footerSumValue' attribute on RowIterator at BatchPaymentEntriesLV.pcf: line 80, column 51
    function sumValue_12 (currentBatchPaymentEntry :  BatchPaymentEntry) : java.lang.Object {
      return currentBatchPaymentEntry.Amount
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at BatchPaymentEntriesLV.pcf: line 31, column 39
    function toCreateAndAdd_97 () : BatchPaymentEntry {
      return batchPaymentDetailsView.addBatchPaymentEntry()
    }
    
    // 'toRemove' attribute on RowIterator at BatchPaymentEntriesLV.pcf: line 31, column 39
    function toRemove_98 (currentBatchPaymentEntry :  BatchPaymentEntry) : void {
      batchPaymentDetailsView.removeBatchPaymentEntry(currentBatchPaymentEntry)
    }
    
    // 'value' attribute on RowIterator at BatchPaymentEntriesLV.pcf: line 31, column 39
    function value_99 () : BatchPaymentEntry[] {
      return batchPaymentDetailsView.BatchPaymentEntries
    }
    
    property get batchPaymentDetailsView () : gw.web.payment.batch.BatchPaymentDetailsView {
      return getRequireValue("batchPaymentDetailsView", 0) as gw.web.payment.batch.BatchPaymentDetailsView
    }
    
    property set batchPaymentDetailsView ($arg :  gw.web.payment.batch.BatchPaymentDetailsView) {
      setRequireValue("batchPaymentDetailsView", 0, $arg)
    }
    
    property get paymentInstrumentRange () : gw.payment.PaymentInstrumentRange {
      return getVariableValue("paymentInstrumentRange", 0) as gw.payment.PaymentInstrumentRange
    }
    
    property set paymentInstrumentRange ($arg :  gw.payment.PaymentInstrumentRange) {
      setVariableValue("paymentInstrumentRange", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/payment/BatchPaymentEntriesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends BatchPaymentEntriesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=Account_Cell) at BatchPaymentEntriesLV.pcf: line 93, column 40
    function action_47 () : void {
      AccountSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerCell (id=Invoice_Cell) at BatchPaymentEntriesLV.pcf: line 106, column 38
    function action_57 () : void {
      InvoiceSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerCell (id=PolicyPeriod_Cell) at BatchPaymentEntriesLV.pcf: line 118, column 40
    function action_69 () : void {
      PolicySearchPopup.push(false)
    }
    
    // 'pickLocation' attribute on PickerCell (id=Producer_Cell) at BatchPaymentEntriesLV.pcf: line 131, column 39
    function action_80 () : void {
      ProducerSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerCell (id=Account_Cell) at BatchPaymentEntriesLV.pcf: line 93, column 40
    function action_dest_48 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'pickLocation' attribute on PickerCell (id=Invoice_Cell) at BatchPaymentEntriesLV.pcf: line 106, column 38
    function action_dest_58 () : pcf.api.Destination {
      return pcf.InvoiceSearchPopup.createDestination()
    }
    
    // 'pickLocation' attribute on PickerCell (id=PolicyPeriod_Cell) at BatchPaymentEntriesLV.pcf: line 118, column 40
    function action_dest_70 () : pcf.api.Destination {
      return pcf.PolicySearchPopup.createDestination(false)
    }
    
    // 'pickLocation' attribute on PickerCell (id=Producer_Cell) at BatchPaymentEntriesLV.pcf: line 131, column 39
    function action_dest_81 () : pcf.api.Destination {
      return pcf.ProducerSearchPopup.createDestination()
    }
    
    // 'checkBoxVisible' attribute on RowIterator at BatchPaymentEntriesLV.pcf: line 31, column 39
    function checkBoxVisible_96 () : java.lang.Boolean {
      return batchPaymentDetailsView.BatchPaymentEditable
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at BatchPaymentEntriesLV.pcf: line 80, column 51
    function currency_44 () : typekey.Currency {
      return batchPaymentDetailsView.BatchPayment.Currency
    }
    
    // 'value' attribute on DateCell (id=PaymentDate_Cell) at BatchPaymentEntriesLV.pcf: line 49, column 57
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentBatchPaymentEntry.PaymentDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeCell (id=PaymentInstrument_Cell) at BatchPaymentEntriesLV.pcf: line 57, column 41
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentBatchPaymentEntry.PaymentInstrument = (__VALUE_TO_SET as PaymentInstrument)
    }
    
    // 'value' attribute on TextCell (id=RefNumber_Cell) at BatchPaymentEntriesLV.pcf: line 67, column 54
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentBatchPaymentEntry.RefNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at BatchPaymentEntriesLV.pcf: line 80, column 51
    function defaultSetter_42 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentBatchPaymentEntry.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on PickerCell (id=Account_Cell) at BatchPaymentEntriesLV.pcf: line 93, column 40
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentBatchPaymentEntry.AccountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PickerCell (id=Invoice_Cell) at BatchPaymentEntriesLV.pcf: line 106, column 38
    function defaultSetter_64 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentBatchPaymentEntry.Invoice = (__VALUE_TO_SET as entity.Invoice)
    }
    
    // 'value' attribute on PickerCell (id=PolicyPeriod_Cell) at BatchPaymentEntriesLV.pcf: line 118, column 40
    function defaultSetter_75 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentBatchPaymentEntry.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PickerCell (id=Producer_Cell) at BatchPaymentEntriesLV.pcf: line 131, column 39
    function defaultSetter_87 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentBatchPaymentEntry.Producer = (__VALUE_TO_SET as entity.Producer)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at BatchPaymentEntriesLV.pcf: line 140, column 24
    function defaultSetter_93 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentBatchPaymentEntry.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextCell (id=RefNumber_Cell) at BatchPaymentEntriesLV.pcf: line 67, column 54
    function editable_32 () : java.lang.Boolean {
      return currentBatchPaymentEntry.RefNumberSupported
    }
    
    // 'editable' attribute on PickerCell (id=Account_Cell) at BatchPaymentEntriesLV.pcf: line 93, column 40
    function editable_49 () : java.lang.Boolean {
      return currentBatchPaymentEntry.canSetTarget(BatchPaymentEntry#AccountNumber)
    }
    
    // 'editable' attribute on PickerCell (id=Invoice_Cell) at BatchPaymentEntriesLV.pcf: line 106, column 38
    function editable_59 () : java.lang.Boolean {
      return currentBatchPaymentEntry.canSetTarget(BatchPaymentEntry#Invoice)
    }
    
    // 'editable' attribute on PickerCell (id=PolicyPeriod_Cell) at BatchPaymentEntriesLV.pcf: line 118, column 40
    function editable_71 () : java.lang.Boolean {
      return currentBatchPaymentEntry.canSetTarget(BatchPaymentEntry#PolicyNumber)
    }
    
    // 'editable' attribute on PickerCell (id=Producer_Cell) at BatchPaymentEntriesLV.pcf: line 131, column 39
    function editable_82 () : java.lang.Boolean {
      return currentBatchPaymentEntry.canSetTarget(BatchPaymentEntry#Producer)
    }
    
    // 'inputConversion' attribute on PickerCell (id=Invoice_Cell) at BatchPaymentEntriesLV.pcf: line 106, column 38
    function inputConversion_61 (VALUE :  java.lang.String) : java.lang.Object {
      return batchPaymentDetailsView.InvoiceConverter.getInvoice(VALUE)
    }
    
    // 'inputConversion' attribute on PickerCell (id=Producer_Cell) at BatchPaymentEntriesLV.pcf: line 131, column 39
    function inputConversion_84 (VALUE :  java.lang.String) : java.lang.Object {
      return batchPaymentDetailsView.ProducerConverter.getProducer(VALUE)
    }
    
    // 'onChange' attribute on PostOnChange at BatchPaymentEntriesLV.pcf: line 60, column 120
    function onChange_24 () : void {
      if(!currentBatchPaymentEntry.isRefNumberSupported()) currentBatchPaymentEntry.RefNumber = null
    }
    
    // 'onChange' attribute on PostOnChange at BatchPaymentEntriesLV.pcf: line 83, column 119
    function onChange_39 () : void {
      gw.api.util.BatchPaymentEntryValidationService.verifyAmountOnChange(currentBatchPaymentEntry)
    }
    
    // 'outputConversion' attribute on PickerCell (id=Invoice_Cell) at BatchPaymentEntriesLV.pcf: line 106, column 38
    function outputConversion_62 (VALUE :  entity.Invoice) : java.lang.String {
      return currentBatchPaymentEntry.Invoice.InvoiceNumber
    }
    
    // 'outputConversion' attribute on PickerCell (id=Producer_Cell) at BatchPaymentEntriesLV.pcf: line 131, column 39
    function outputConversion_85 (VALUE :  entity.Producer) : java.lang.String {
      return currentBatchPaymentEntry.Producer.Name
    }
    
    // 'requestValidationExpression' attribute on PickerCell (id=PolicyPeriod_Cell) at BatchPaymentEntriesLV.pcf: line 118, column 40
    function requestValidationExpression_73 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.api.util.BatchPaymentEntryValidationService.verifyPolicyIsNotArchived(VALUE)
    }
    
    // 'validationExpression' attribute on MonetaryAmountCell (id=Amount_Cell) at BatchPaymentEntriesLV.pcf: line 80, column 51
    function validationExpression_40 () : java.lang.Object {
      return gw.api.util.BatchPaymentEntryValidationService.verifyAmountFor(currentBatchPaymentEntry)
    }
    
    // 'validationExpression' attribute on PickerCell (id=Account_Cell) at BatchPaymentEntriesLV.pcf: line 93, column 40
    function validationExpression_50 () : java.lang.Object {
      return gw.api.util.BatchPaymentEntryValidationService.verifyAccountFor(currentBatchPaymentEntry)
    }
    
    // 'validationExpression' attribute on PickerCell (id=Invoice_Cell) at BatchPaymentEntriesLV.pcf: line 106, column 38
    function validationExpression_60 () : java.lang.Object {
      return gw.api.util.BatchPaymentEntryValidationService.verifyInvoiceFor(currentBatchPaymentEntry)
    }
    
    // 'validationExpression' attribute on PickerCell (id=PolicyPeriod_Cell) at BatchPaymentEntriesLV.pcf: line 118, column 40
    function validationExpression_72 () : java.lang.Object {
      return gw.api.util.BatchPaymentEntryValidationService.verifyPolicyFor(currentBatchPaymentEntry)
    }
    
    // 'validationExpression' attribute on PickerCell (id=Producer_Cell) at BatchPaymentEntriesLV.pcf: line 131, column 39
    function validationExpression_83 () : java.lang.Object {
      return gw.api.util.BatchPaymentEntryValidationService.verifyProducerFor(currentBatchPaymentEntry)
    }
    
    // 'valueRange' attribute on RangeCell (id=PaymentType_Cell) at BatchPaymentEntriesLV.pcf: line 42, column 40
    function valueRange_16 () : java.lang.Object {
      return MultiPaymentType.getTypeKeys(false)
    }
    
    // 'valueRange' attribute on RangeCell (id=PaymentInstrument_Cell) at BatchPaymentEntriesLV.pcf: line 57, column 41
    function valueRange_28 () : java.lang.Object {
      return paymentInstrumentRange.AvailableInstruments
    }
    
    // 'value' attribute on RangeCell (id=PaymentType_Cell) at BatchPaymentEntriesLV.pcf: line 42, column 40
    function valueRoot_15 () : java.lang.Object {
      return currentBatchPaymentEntry
    }
    
    // 'value' attribute on RangeCell (id=PaymentType_Cell) at BatchPaymentEntriesLV.pcf: line 42, column 40
    function value_14 () : MultiPaymentType {
      return currentBatchPaymentEntry.PaymentType
    }
    
    // 'value' attribute on DateCell (id=PaymentDate_Cell) at BatchPaymentEntriesLV.pcf: line 49, column 57
    function value_20 () : java.util.Date {
      return currentBatchPaymentEntry.PaymentDate
    }
    
    // 'value' attribute on RangeCell (id=PaymentInstrument_Cell) at BatchPaymentEntriesLV.pcf: line 57, column 41
    function value_25 () : PaymentInstrument {
      return currentBatchPaymentEntry.PaymentInstrument
    }
    
    // 'value' attribute on TextCell (id=RefNumber_Cell) at BatchPaymentEntriesLV.pcf: line 67, column 54
    function value_34 () : java.lang.String {
      return currentBatchPaymentEntry.RefNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at BatchPaymentEntriesLV.pcf: line 80, column 51
    function value_41 () : gw.pl.currency.MonetaryAmount {
      return currentBatchPaymentEntry.Amount
    }
    
    // 'value' attribute on PickerCell (id=Account_Cell) at BatchPaymentEntriesLV.pcf: line 93, column 40
    function value_51 () : java.lang.String {
      return currentBatchPaymentEntry.AccountNumber
    }
    
    // 'value' attribute on PickerCell (id=Invoice_Cell) at BatchPaymentEntriesLV.pcf: line 106, column 38
    function value_63 () : entity.Invoice {
      return currentBatchPaymentEntry.Invoice
    }
    
    // 'value' attribute on PickerCell (id=PolicyPeriod_Cell) at BatchPaymentEntriesLV.pcf: line 118, column 40
    function value_74 () : java.lang.String {
      return currentBatchPaymentEntry.PolicyNumber
    }
    
    // 'value' attribute on PickerCell (id=Producer_Cell) at BatchPaymentEntriesLV.pcf: line 131, column 39
    function value_86 () : entity.Producer {
      return currentBatchPaymentEntry.Producer
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at BatchPaymentEntriesLV.pcf: line 140, column 24
    function value_92 () : java.lang.String {
      return currentBatchPaymentEntry.Description
    }
    
    // 'valueRange' attribute on RangeCell (id=PaymentType_Cell) at BatchPaymentEntriesLV.pcf: line 42, column 40
    function verifyValueRangeIsAllowedType_17 ($$arg :  MultiPaymentType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=PaymentType_Cell) at BatchPaymentEntriesLV.pcf: line 42, column 40
    function verifyValueRangeIsAllowedType_17 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=PaymentType_Cell) at BatchPaymentEntriesLV.pcf: line 42, column 40
    function verifyValueRangeIsAllowedType_17 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=PaymentInstrument_Cell) at BatchPaymentEntriesLV.pcf: line 57, column 41
    function verifyValueRangeIsAllowedType_29 ($$arg :  PaymentInstrument[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=PaymentInstrument_Cell) at BatchPaymentEntriesLV.pcf: line 57, column 41
    function verifyValueRangeIsAllowedType_29 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=PaymentInstrument_Cell) at BatchPaymentEntriesLV.pcf: line 57, column 41
    function verifyValueRangeIsAllowedType_29 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=PaymentType_Cell) at BatchPaymentEntriesLV.pcf: line 42, column 40
    function verifyValueRange_18 () : void {
      var __valueRangeArg = MultiPaymentType.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_17(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=PaymentInstrument_Cell) at BatchPaymentEntriesLV.pcf: line 57, column 41
    function verifyValueRange_30 () : void {
      var __valueRangeArg = paymentInstrumentRange.AvailableInstruments
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_29(__valueRangeArg)
    }
    
    property get currentBatchPaymentEntry () : BatchPaymentEntry {
      return getIteratedValue(1) as BatchPaymentEntry
    }
    
    
  }
  
  
}