package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/MinimalTAccountOwnerDetailsDV.accountdefault.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class MinimalTAccountOwnerDetailsDV_accountdefaultExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/MinimalTAccountOwnerDetailsDV.accountdefault.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class MinimalTAccountOwnerDetailsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextInput (id=Account_Input) at MinimalTAccountOwnerDetailsDV.accountdefault.pcf: line 31, column 37
    function action_2 () : void {
      pcf.AccountSummaryPopup.push(account)
    }
    
    // 'action' attribute on TextInput (id=Account_Input) at MinimalTAccountOwnerDetailsDV.accountdefault.pcf: line 31, column 37
    function action_dest_3 () : pcf.api.Destination {
      return pcf.AccountSummaryPopup.createDestination(account)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=BilledAmount_Input) at MinimalTAccountOwnerDetailsDV.accountdefault.pcf: line 44, column 55
    function currency_11 () : typekey.Currency {
      return account.Currency
    }
    
    // 'initialValue' attribute on Variable at MinimalTAccountOwnerDetailsDV.accountdefault.pcf: line 18, column 23
    function initialValue_0 () : Account {
      return tAccountOwner as Account
    }
    
    // 'initialValue' attribute on Variable at MinimalTAccountOwnerDetailsDV.accountdefault.pcf: line 22, column 61
    function initialValue_1 () : gw.web.account.AccountSummaryFinancialsHelper {
      return new gw.web.account.AccountSummaryFinancialsHelper(account)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=BilledAmount_Input) at MinimalTAccountOwnerDetailsDV.accountdefault.pcf: line 44, column 55
    function valueRoot_10 () : java.lang.Object {
      return financialsHelper
    }
    
    // 'value' attribute on TextInput (id=Insured_Input) at MinimalTAccountOwnerDetailsDV.accountdefault.pcf: line 35, column 47
    function valueRoot_7 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PastDueAmount_Input) at MinimalTAccountOwnerDetailsDV.accountdefault.pcf: line 51, column 59
    function value_13 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.AccountDelinquentAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=OutstandingAmount_Input) at MinimalTAccountOwnerDetailsDV.accountdefault.pcf: line 58, column 60
    function value_17 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.AccountOutstandingAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnbilledAmount_Input) at MinimalTAccountOwnerDetailsDV.accountdefault.pcf: line 65, column 57
    function value_21 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.AccountUnbilledAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnpaidAmount_Input) at MinimalTAccountOwnerDetailsDV.accountdefault.pcf: line 72, column 55
    function value_25 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.AccountUnpaidAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at MinimalTAccountOwnerDetailsDV.accountdefault.pcf: line 80, column 42
    function value_29 () : gw.pl.currency.MonetaryAmount {
      return account.UnappliedAmount
    }
    
    // 'value' attribute on TextInput (id=Account_Input) at MinimalTAccountOwnerDetailsDV.accountdefault.pcf: line 31, column 37
    function value_4 () : entity.Account {
      return account
    }
    
    // 'value' attribute on TextInput (id=Insured_Input) at MinimalTAccountOwnerDetailsDV.accountdefault.pcf: line 35, column 47
    function value_6 () : java.lang.String {
      return account.AccountNameLocalized
    }
    
    // 'value' attribute on MonetaryAmountInput (id=BilledAmount_Input) at MinimalTAccountOwnerDetailsDV.accountdefault.pcf: line 44, column 55
    function value_9 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.AccountBilledAmount
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get financialsHelper () : gw.web.account.AccountSummaryFinancialsHelper {
      return getVariableValue("financialsHelper", 0) as gw.web.account.AccountSummaryFinancialsHelper
    }
    
    property set financialsHelper ($arg :  gw.web.account.AccountSummaryFinancialsHelper) {
      setVariableValue("financialsHelper", 0, $arg)
    }
    
    property get tAccountOwner () : TAccountOwner {
      return getRequireValue("tAccountOwner", 0) as TAccountOwner
    }
    
    property set tAccountOwner ($arg :  TAccountOwner) {
      setRequireValue("tAccountOwner", 0, $arg)
    }
    
    property get unapplied () : UnappliedFund {
      return getRequireValue("unapplied", 0) as UnappliedFund
    }
    
    property set unapplied ($arg :  UnappliedFund) {
      setRequireValue("unapplied", 0, $arg)
    }
    
    
  }
  
  
}