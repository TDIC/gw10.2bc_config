package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadUserAuthLimitLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadUserAuthLimitLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadUserAuthLimitLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadUserAuthLimitLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=userReferenceName_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 28, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserAuthorityLimit.UserName")
    }
    
    // 'label' attribute on TextCell (id=userPublicID_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 33, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserAuthorityLimit.UserPublicID")
    }
    
    // 'label' attribute on TextCell (id=profile_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 38, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserAuthorityLimit.Profile")
    }
    
    // 'label' attribute on TextCell (id=authLimitPublicID_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 43, column 41
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserAuthorityLimit.AuthorityLimitPublicID")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 23, column 46
    function sortValue_1 (userAuthLimit :  tdic.util.dataloader.data.admindata.UserAuthorityLimitData) : java.lang.Object {
      return processor.getLoadStatus(userAuthLimit)
    }
    
    // 'value' attribute on TextCell (id=authLimitPublicID_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 43, column 41
    function sortValue_10 (userAuthLimit :  tdic.util.dataloader.data.admindata.UserAuthorityLimitData) : java.lang.Object {
      return userAuthLimit.AuthorityLimitPublicID
    }
    
    // 'value' attribute on TextCell (id=userReferenceName_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 28, column 41
    function sortValue_4 (userAuthLimit :  tdic.util.dataloader.data.admindata.UserAuthorityLimitData) : java.lang.Object {
      return userAuthLimit.UserReferenceName
    }
    
    // 'value' attribute on TextCell (id=userPublicID_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 33, column 41
    function sortValue_6 (userAuthLimit :  tdic.util.dataloader.data.admindata.UserAuthorityLimitData) : java.lang.Object {
      return userAuthLimit.UserPublicID
    }
    
    // 'value' attribute on TextCell (id=profile_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 38, column 41
    function sortValue_8 (userAuthLimit :  tdic.util.dataloader.data.admindata.UserAuthorityLimitData) : java.lang.Object {
      return userAuthLimit.Profile.Name
    }
    
    // 'value' attribute on RowIterator (id=BCAuthLimit) at AdminDataUploadUserAuthLimitLV.pcf: line 15, column 105
    function value_37 () : java.util.ArrayList<tdic.util.dataloader.data.admindata.UserAuthorityLimitData> {
      return processor.UserAuthorityLimitArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadUserAuthLimitLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminDataUploadUserAuthLimitLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at AdminDataUploadUserAuthLimitLV.pcf: line 17, column 106
    function highlighted_36 () : java.lang.Boolean {
      return (userAuthLimit.Error or userAuthLimit.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 23, column 46
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=userReferenceName_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 28, column 41
    function label_16 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserAuthorityLimit.UserName")
    }
    
    // 'label' attribute on TextCell (id=userPublicID_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 33, column 41
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserAuthorityLimit.UserPublicID")
    }
    
    // 'label' attribute on TextCell (id=profile_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 38, column 41
    function label_26 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserAuthorityLimit.Profile")
    }
    
    // 'label' attribute on TextCell (id=authLimitPublicID_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 43, column 41
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserAuthorityLimit.AuthorityLimitPublicID")
    }
    
    // 'value' attribute on TextCell (id=userReferenceName_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 28, column 41
    function valueRoot_18 () : java.lang.Object {
      return userAuthLimit
    }
    
    // 'value' attribute on TextCell (id=profile_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 38, column 41
    function valueRoot_28 () : java.lang.Object {
      return userAuthLimit.Profile
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 23, column 46
    function value_12 () : java.lang.String {
      return processor.getLoadStatus(userAuthLimit)
    }
    
    // 'value' attribute on TextCell (id=userReferenceName_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 28, column 41
    function value_17 () : java.lang.String {
      return userAuthLimit.UserReferenceName
    }
    
    // 'value' attribute on TextCell (id=userPublicID_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 33, column 41
    function value_22 () : java.lang.String {
      return userAuthLimit.UserPublicID
    }
    
    // 'value' attribute on TextCell (id=profile_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 38, column 41
    function value_27 () : java.lang.String {
      return userAuthLimit.Profile.Name
    }
    
    // 'value' attribute on TextCell (id=authLimitPublicID_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 43, column 41
    function value_32 () : java.lang.String {
      return userAuthLimit.AuthorityLimitPublicID
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadUserAuthLimitLV.pcf: line 23, column 46
    function visible_13 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get userAuthLimit () : tdic.util.dataloader.data.admindata.UserAuthorityLimitData {
      return getIteratedValue(1) as tdic.util.dataloader.data.admindata.UserAuthorityLimitData
    }
    
    
  }
  
  
}