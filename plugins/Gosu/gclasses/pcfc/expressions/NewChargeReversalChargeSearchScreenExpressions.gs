package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalChargeSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewChargeReversalChargeSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalChargeSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewChargeReversalChargeSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get reversal () : ChargeReversal {
      return getRequireValue("reversal", 0) as ChargeReversal
    }
    
    property set reversal ($arg :  ChargeReversal) {
      setRequireValue("reversal", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalChargeSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends NewChargeReversalChargeSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalChargeSearchScreen.pcf: line 28, column 56
    function def_onEnter_0 (def :  pcf.ChargeSearchDV) : void {
      def.onEnter(searchCriteria, true)
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalChargeSearchScreen.pcf: line 32, column 66
    function def_onEnter_2 (def :  pcf.NewChargeReversalChargesLV) : void {
      def.onEnter(reversal, charges)
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalChargeSearchScreen.pcf: line 28, column 56
    function def_refreshVariables_1 (def :  pcf.ChargeSearchDV) : void {
      def.refreshVariables(searchCriteria, true)
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalChargeSearchScreen.pcf: line 32, column 66
    function def_refreshVariables_3 (def :  pcf.NewChargeReversalChargesLV) : void {
      def.refreshVariables(reversal, charges)
    }
    
    // 'searchCriteria' attribute on SearchPanel at NewChargeReversalChargeSearchScreen.pcf: line 26, column 78
    function searchCriteria_5 () : gw.search.ReversibleChargeSearchCriteria {
      return new gw.search.ReversibleChargeSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at NewChargeReversalChargeSearchScreen.pcf: line 26, column 78
    function search_4 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria)
    }
    
    property get charges () : gw.api.database.IQueryBeanResult<Charge> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<Charge>
    }
    
    property get searchCriteria () : gw.search.ReversibleChargeSearchCriteria {
      return getCriteriaValue(1) as gw.search.ReversibleChargeSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.ReversibleChargeSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}