package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/WizardsStep1AccountPolicySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WizardsStep1AccountPolicySearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/WizardsStep1AccountPolicySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanel2ExpressionsImpl extends WizardsStep1AccountPolicySearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at WizardsStep1AccountPolicySearchScreen.pcf: line 81, column 57
    function def_onEnter_16 (def :  pcf.PolicySearchDV) : void {
      def.onEnter(searchCriteria, false)
    }
    
    // 'def' attribute on PanelRef at WizardsStep1AccountPolicySearchScreen.pcf: line 87, column 98
    function def_onEnter_18 (def :  pcf.PolicySearchResultsLV) : void {
      def.onEnter(policySearchViews, targetOfWriteoff, true, false, false)
    }
    
    // 'def' attribute on PanelRef at WizardsStep1AccountPolicySearchScreen.pcf: line 81, column 57
    function def_refreshVariables_17 (def :  pcf.PolicySearchDV) : void {
      def.refreshVariables(searchCriteria, false)
    }
    
    // 'def' attribute on PanelRef at WizardsStep1AccountPolicySearchScreen.pcf: line 87, column 98
    function def_refreshVariables_19 (def :  pcf.PolicySearchResultsLV) : void {
      def.refreshVariables(policySearchViews, targetOfWriteoff, true, false, false)
    }
    
    // 'searchCriteria' attribute on SearchPanel at WizardsStep1AccountPolicySearchScreen.pcf: line 79, column 88
    function searchCriteria_21 () : gw.search.PolicySearchCriteria {
      return new gw.search.PolicySearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at WizardsStep1AccountPolicySearchScreen.pcf: line 79, column 88
    function search_20 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria, StringCriterionMode.TC_STARTSWITH)
    }
    
    property get policySearchViews () : gw.api.database.IQueryBeanResult<PolicySearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<PolicySearchView>
    }
    
    property get searchCriteria () : gw.search.PolicySearchCriteria {
      return getCriteriaValue(1) as gw.search.PolicySearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.PolicySearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/WizardsStep1AccountPolicySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanel3ExpressionsImpl extends WizardsStep1AccountPolicySearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at WizardsStep1AccountPolicySearchScreen.pcf: line 106, column 52
    function def_onEnter_23 (def :  pcf.ProducerSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at WizardsStep1AccountPolicySearchScreen.pcf: line 112, column 102
    function def_onEnter_25 (def :  pcf.ProducerSearchResultsLV) : void {
      def.onEnter(producerSearchViews, targetOfWriteoff, true, false, false)
    }
    
    // 'def' attribute on PanelRef at WizardsStep1AccountPolicySearchScreen.pcf: line 106, column 52
    function def_refreshVariables_24 (def :  pcf.ProducerSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at WizardsStep1AccountPolicySearchScreen.pcf: line 112, column 102
    function def_refreshVariables_26 (def :  pcf.ProducerSearchResultsLV) : void {
      def.refreshVariables(producerSearchViews, targetOfWriteoff, true, false, false)
    }
    
    // 'searchCriteria' attribute on SearchPanel at WizardsStep1AccountPolicySearchScreen.pcf: line 104, column 90
    function searchCriteria_28 () : gw.search.ProducerSearchCriteria {
      return new gw.search.ProducerSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at WizardsStep1AccountPolicySearchScreen.pcf: line 104, column 90
    function search_27 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria, StringCriterionMode.TC_STARTSWITH)
    }
    
    property get producerSearchViews () : gw.api.database.IQueryBeanResult<ProducerSearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<ProducerSearchView>
    }
    
    property get searchCriteria () : gw.search.ProducerSearchCriteria {
      return getCriteriaValue(1) as gw.search.ProducerSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.ProducerSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/WizardsStep1AccountPolicySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends WizardsStep1AccountPolicySearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at WizardsStep1AccountPolicySearchScreen.pcf: line 64, column 100
    function def_onEnter_11 (def :  pcf.AccountSearchResultsLV) : void {
      def.onEnter(accountSearchViews, targetOfWriteoff, true, false, false)
    }
    
    // 'def' attribute on PanelRef at WizardsStep1AccountPolicySearchScreen.pcf: line 58, column 51
    function def_onEnter_9 (def :  pcf.AccountSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at WizardsStep1AccountPolicySearchScreen.pcf: line 58, column 51
    function def_refreshVariables_10 (def :  pcf.AccountSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at WizardsStep1AccountPolicySearchScreen.pcf: line 64, column 100
    function def_refreshVariables_12 (def :  pcf.AccountSearchResultsLV) : void {
      def.refreshVariables(accountSearchViews, targetOfWriteoff, true, false, false)
    }
    
    // 'searchCriteria' attribute on SearchPanel at WizardsStep1AccountPolicySearchScreen.pcf: line 56, column 89
    function searchCriteria_14 () : gw.search.AccountSearchCriteria {
      return new gw.search.AccountSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at WizardsStep1AccountPolicySearchScreen.pcf: line 56, column 89
    function search_13 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria, StringCriterionMode.TC_STARTSWITH)
    }
    
    property get accountSearchViews () : gw.api.database.IQueryBeanResult<AccountSearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<AccountSearchView>
    }
    
    property get searchCriteria () : gw.search.AccountSearchCriteria {
      return getCriteriaValue(1) as gw.search.AccountSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.AccountSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/WizardsStep1AccountPolicySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WizardsStep1AccountPolicySearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeRadioInput (id=SourceType_Input) at WizardsStep1AccountPolicySearchScreen.pcf: line 40, column 46
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      sourceType = (__VALUE_TO_SET as typekey.TAccountOwnerType)
    }
    
    // 'initialValue' attribute on Variable at WizardsStep1AccountPolicySearchScreen.pcf: line 23, column 33
    function initialValue_0 () : TAccountOwnerType {
      return possibleSourceType
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=SourceType_Input) at WizardsStep1AccountPolicySearchScreen.pcf: line 40, column 46
    function valueRange_4 () : java.lang.Object {
      return getAvailableSourceTypes()
    }
    
    // 'value' attribute on RangeRadioInput (id=SourceType_Input) at WizardsStep1AccountPolicySearchScreen.pcf: line 40, column 46
    function value_2 () : typekey.TAccountOwnerType {
      return sourceType
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=SourceType_Input) at WizardsStep1AccountPolicySearchScreen.pcf: line 40, column 46
    function verifyValueRangeIsAllowedType_5 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=SourceType_Input) at WizardsStep1AccountPolicySearchScreen.pcf: line 40, column 46
    function verifyValueRangeIsAllowedType_5 ($$arg :  typekey.TAccountOwnerType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=SourceType_Input) at WizardsStep1AccountPolicySearchScreen.pcf: line 40, column 46
    function verifyValueRange_6 () : void {
      var __valueRangeArg = getAvailableSourceTypes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_5(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeRadioInput (id=SourceType_Input) at WizardsStep1AccountPolicySearchScreen.pcf: line 40, column 46
    function visible_1 () : java.lang.Boolean {
      return possibleSourceType==null
    }
    
    // 'visible' attribute on Card (id=Account) at WizardsStep1AccountPolicySearchScreen.pcf: line 49, column 62
    function visible_15 () : java.lang.Boolean {
      return sourceType == TAccountOwnerType.TC_ACCOUNT
    }
    
    // 'visible' attribute on Card (id=PolicyPeriod) at WizardsStep1AccountPolicySearchScreen.pcf: line 72, column 67
    function visible_22 () : java.lang.Boolean {
      return sourceType == TAccountOwnerType.TC_POLICYPERIOD
    }
    
    // 'visible' attribute on Card (id=Producer) at WizardsStep1AccountPolicySearchScreen.pcf: line 95, column 63
    function visible_29 () : java.lang.Boolean {
      return sourceType == TAccountOwnerType.TC_PRODUCER
    }
    
    property get possibleSourceType () : TAccountOwnerType {
      return getRequireValue("possibleSourceType", 0) as TAccountOwnerType
    }
    
    property set possibleSourceType ($arg :  TAccountOwnerType) {
      setRequireValue("possibleSourceType", 0, $arg)
    }
    
    property get showPolicyAsSource () : boolean {
      return getRequireValue("showPolicyAsSource", 0) as java.lang.Boolean
    }
    
    property set showPolicyAsSource ($arg :  boolean) {
      setRequireValue("showPolicyAsSource", 0, $arg)
    }
    
    property get showProducerAsSource () : boolean {
      return getRequireValue("showProducerAsSource", 0) as java.lang.Boolean
    }
    
    property set showProducerAsSource ($arg :  boolean) {
      setRequireValue("showProducerAsSource", 0, $arg)
    }
    
    property get sourceType () : TAccountOwnerType {
      return getVariableValue("sourceType", 0) as TAccountOwnerType
    }
    
    property set sourceType ($arg :  TAccountOwnerType) {
      setVariableValue("sourceType", 0, $arg)
    }
    
    property get targetOfWriteoff () : gw.api.web.accounting.TAccountOwnerReference {
      return getRequireValue("targetOfWriteoff", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set targetOfWriteoff ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setRequireValue("targetOfWriteoff", 0, $arg)
    }
    
    function getAvailableSourceTypes() : java.util.List<TAccountOwnerType> {
      var sourceValues : java.util.List<TAccountOwnerType> = com.google.common.collect.Lists.newArrayList<TAccountOwnerType>()
      sourceValues.add(TAccountOwnerType.TC_ACCOUNT)
      if(showPolicyAsSource){
        sourceValues.add(TAccountOwnerType.TC_POLICYPERIOD)
      }
      // HermiaK, 10/28/2014, US70: Remove "Producer" from radio button selection
      // if(showProducerAsSource){
      //  sourceValues.add(TAccountOwnerType.TC_PRODUCER)
      //}
      return sourceValues
    }
    
    
  }
  
  
}