package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/TransactionDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransactionDetailPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/search/TransactionDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransactionDetailPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (Transaction :  Transaction) : int {
      return 0
    }
    
    // 'def' attribute on PanelRef at TransactionDetailPopup.pcf: line 17, column 49
    function def_onEnter_0 (def :  pcf.TransactionDetailDV) : void {
      def.onEnter(Transaction)
    }
    
    // 'def' attribute on PanelRef at TransactionDetailPopup.pcf: line 17, column 49
    function def_refreshVariables_1 (def :  pcf.TransactionDetailDV) : void {
      def.refreshVariables(Transaction)
    }
    
    // 'title' attribute on Popup (id=TransactionDetailPopup) at TransactionDetailPopup.pcf: line 7, column 98
    static function title_2 (Transaction :  Transaction) : java.lang.Object {
      return DisplayKey.get("Web.TransactionDetail.Title", Transaction.LongDisplayName)
    }
    
    override property get CurrentLocation () : pcf.TransactionDetailPopup {
      return super.CurrentLocation as pcf.TransactionDetailPopup
    }
    
    property get Transaction () : Transaction {
      return getVariableValue("Transaction", 0) as Transaction
    }
    
    property set Transaction ($arg :  Transaction) {
      setVariableValue("Transaction", 0, $arg)
    }
    
    
  }
  
  
}