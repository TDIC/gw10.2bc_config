package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/InvoiceStreamAnchorDateInputSet.everyweek.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceStreamAnchorDateInputSet_everyweekExpressions {
  @javax.annotation.Generated("config/web/pcf/account/InvoiceStreamAnchorDateInputSet.everyweek.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceStreamAnchorDateInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyInput (id=OverridingInvoiceDayOfWeek_Input) at InvoiceStreamAnchorDateInputSet.everyweek.pcf: line 35, column 60
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceDayChangeHelper.OverridingAnchorDateViews[0].DayOfWeek = (__VALUE_TO_SET as typekey.DayOfWeek)
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverrideInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.everyweek.pcf: line 25, column 44
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceDayChangeHelper.OverrideAnchorDates = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'editable' attribute on TypeKeyInput (id=OverridingInvoiceDayOfWeek_Input) at InvoiceStreamAnchorDateInputSet.everyweek.pcf: line 35, column 60
    function editable_10 () : java.lang.Boolean {
      return isEditMode && invoiceDayChangeHelper.OverrideAnchorDates
    }
    
    // 'editable' attribute on BooleanRadioInput (id=OverrideInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.everyweek.pcf: line 25, column 44
    function editable_2 () : java.lang.Boolean {
      return isEditMode
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverrideInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.everyweek.pcf: line 25, column 44
    function valueRoot_6 () : java.lang.Object {
      return invoiceDayChangeHelper
    }
    
    // 'value' attribute on TypeKeyInput (id=AccountInvoiceDayOfWeek_Input) at InvoiceStreamAnchorDateInputSet.everyweek.pcf: line 18, column 38
    function value_0 () : typekey.DayOfWeek {
      return invoiceDayChangeHelper.PayerDefaultAnchorDateViews[0].DayOfWeek
    }
    
    // 'value' attribute on TypeKeyInput (id=OverridingInvoiceDayOfWeek_Input) at InvoiceStreamAnchorDateInputSet.everyweek.pcf: line 35, column 60
    function value_12 () : typekey.DayOfWeek {
      return invoiceDayChangeHelper.OverridingAnchorDateViews[0].DayOfWeek
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverrideInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.everyweek.pcf: line 25, column 44
    function value_4 () : java.lang.Boolean {
      return invoiceDayChangeHelper.OverrideAnchorDates
    }
    
    // 'visible' attribute on BooleanRadioInput (id=OverrideInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.everyweek.pcf: line 25, column 44
    function visible_3 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    property get invoiceDayChangeHelper () : gw.api.web.invoice.InvoiceDayChangeHelper {
      return getRequireValue("invoiceDayChangeHelper", 0) as gw.api.web.invoice.InvoiceDayChangeHelper
    }
    
    property set invoiceDayChangeHelper ($arg :  gw.api.web.invoice.InvoiceDayChangeHelper) {
      setRequireValue("invoiceDayChangeHelper", 0, $arg)
    }
    
    property get isEditMode () : boolean {
      return getRequireValue("isEditMode", 0) as java.lang.Boolean
    }
    
    property set isEditMode ($arg :  boolean) {
      setRequireValue("isEditMode", 0, $arg)
    }
    
    
  }
  
  
}