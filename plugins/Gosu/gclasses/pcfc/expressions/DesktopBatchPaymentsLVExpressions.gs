package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopBatchPaymentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopBatchPaymentsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopBatchPaymentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopBatchPaymentsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=BatchNumber_Cell) at DesktopBatchPaymentsLV.pcf: line 44, column 25
    function sortValue_0 (batchPayment :  BatchPayment) : java.lang.Object {
      return batchPayment.BatchNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=BatchStatus_Cell) at DesktopBatchPaymentsLV.pcf: line 51, column 25
    function sortValue_1 (batchPayment :  BatchPayment) : java.lang.Object {
      return batchPayment.BatchStatus
    }
    
    // 'value' attribute on MonetaryAmountCell (id=BatchAmount_Cell) at DesktopBatchPaymentsLV.pcf: line 56, column 40
    function sortValue_2 (batchPayment :  BatchPayment) : java.lang.Object {
      return batchPayment.Amount
    }
    
    // 'value' attribute on TextCell (id=BatchPaymentCount_Cell) at DesktopBatchPaymentsLV.pcf: line 62, column 32
    function sortValue_3 (batchPayment :  BatchPayment) : java.lang.Object {
      return batchPayment.Payments.Count
    }
    
    // 'value' attribute on TextCell (id=CreatedBy_Cell) at DesktopBatchPaymentsLV.pcf: line 69, column 29
    function sortValue_4 (batchPayment :  BatchPayment) : java.lang.Object {
      return batchPayment.CreateUser
    }
    
    // 'value' attribute on TextCell (id=LastEditedBy_Cell) at DesktopBatchPaymentsLV.pcf: line 76, column 29
    function sortValue_5 (batchPayment :  BatchPayment) : java.lang.Object {
      return batchPayment.UpdateUser
    }
    
    // 'toRemove' attribute on RowIterator at DesktopBatchPaymentsLV.pcf: line 25, column 72
    function toRemove_29 (batchPayment :  BatchPayment) : void {
      batchPayment.remove()
    }
    
    // 'value' attribute on RowIterator at DesktopBatchPaymentsLV.pcf: line 25, column 72
    function value_30 () : gw.api.database.IQueryBeanResult<BatchPayment> {
      return batchPayments
    }
    
    property get batchPayments () : gw.api.database.IQueryBeanResult<BatchPayment> {
      return getRequireValue("batchPayments", 0) as gw.api.database.IQueryBeanResult<BatchPayment>
    }
    
    property set batchPayments ($arg :  gw.api.database.IQueryBeanResult<BatchPayment>) {
      setRequireValue("batchPayments", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopBatchPaymentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DesktopBatchPaymentsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=BatchNumber_Cell) at DesktopBatchPaymentsLV.pcf: line 44, column 25
    function action_9 () : void {
      gw.web.payment.batch.DesktopBatchPaymentsView.goToBatchPaymentDetailsOrReloadPageIfBatchPaymentNull(batchPayment)
    }
    
    // 'condition' attribute on ToolbarFlag at DesktopBatchPaymentsLV.pcf: line 28, column 30
    function condition_6 () : java.lang.Boolean {
      return batchPayment.isEditable()
    }
    
    // 'condition' attribute on ToolbarFlag at DesktopBatchPaymentsLV.pcf: line 31, column 29
    function condition_7 () : java.lang.Boolean {
      return batchPayment.canBePosted()
    }
    
    // 'condition' attribute on ToolbarFlag at DesktopBatchPaymentsLV.pcf: line 34, column 31
    function condition_8 () : java.lang.Boolean {
      return batchPayment.canBeReversed()
    }
    
    // 'value' attribute on TextCell (id=BatchNumber_Cell) at DesktopBatchPaymentsLV.pcf: line 44, column 25
    function valueRoot_11 () : java.lang.Object {
      return batchPayment
    }
    
    // 'value' attribute on TextCell (id=BatchPaymentCount_Cell) at DesktopBatchPaymentsLV.pcf: line 62, column 32
    function valueRoot_21 () : java.lang.Object {
      return batchPayment.Payments
    }
    
    // 'value' attribute on TextCell (id=BatchNumber_Cell) at DesktopBatchPaymentsLV.pcf: line 44, column 25
    function value_10 () : String {
      return batchPayment.BatchNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=BatchStatus_Cell) at DesktopBatchPaymentsLV.pcf: line 51, column 25
    function value_13 () : BatchPaymentsStatus {
      return batchPayment.BatchStatus
    }
    
    // 'value' attribute on MonetaryAmountCell (id=BatchAmount_Cell) at DesktopBatchPaymentsLV.pcf: line 56, column 40
    function value_17 () : gw.pl.currency.MonetaryAmount {
      return batchPayment.Amount
    }
    
    // 'value' attribute on TextCell (id=BatchPaymentCount_Cell) at DesktopBatchPaymentsLV.pcf: line 62, column 32
    function value_20 () : Integer {
      return batchPayment.Payments.Count
    }
    
    // 'value' attribute on TextCell (id=CreatedBy_Cell) at DesktopBatchPaymentsLV.pcf: line 69, column 29
    function value_23 () : User {
      return batchPayment.CreateUser
    }
    
    // 'value' attribute on TextCell (id=LastEditedBy_Cell) at DesktopBatchPaymentsLV.pcf: line 76, column 29
    function value_26 () : User {
      return batchPayment.UpdateUser
    }
    
    // 'valueType' attribute on TypeKeyCell (id=BatchStatus_Cell) at DesktopBatchPaymentsLV.pcf: line 51, column 25
    function verifyValueType_16 () : void {
      var __valueTypeArg : BatchPaymentsStatus
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : gw.entity.TypeKey = __valueTypeArg
    }
    
    property get batchPayment () : BatchPayment {
      return getIteratedValue(1) as BatchPayment
    }
    
    
  }
  
  
}