package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadUserLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadUserLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadUserLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadUserLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadUserLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=lastName_Cell) at AdminDataUploadUserLV.pcf: line 50, column 41
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.LastName")
    }
    
    // 'label' attribute on TextCell (id=firstName_Cell) at AdminDataUploadUserLV.pcf: line 55, column 41
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.FirstName")
    }
    
    // 'label' attribute on TextCell (id=middleName_Cell) at AdminDataUploadUserLV.pcf: line 60, column 41
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.MiddleName")
    }
    
    // 'label' attribute on TextCell (id=employeeNumber_Cell) at AdminDataUploadUserLV.pcf: line 65, column 41
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.EmployeeNumber")
    }
    
    // 'label' attribute on TextCell (id=username_Cell) at AdminDataUploadUserLV.pcf: line 70, column 41
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.UserName")
    }
    
    // 'label' attribute on TextCell (id=password_Cell) at AdminDataUploadUserLV.pcf: line 75, column 41
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Password")
    }
    
    // 'label' attribute on TextCell (id=jobTitle_Cell) at AdminDataUploadUserLV.pcf: line 80, column 41
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.JobTitle")
    }
    
    // 'label' attribute on TextCell (id=department_Cell) at AdminDataUploadUserLV.pcf: line 85, column 41
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Department")
    }
    
    // 'label' attribute on TextCell (id=gender_Cell) at AdminDataUploadUserLV.pcf: line 90, column 41
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Gender")
    }
    
    // 'label' attribute on TextCell (id=homephone_Cell) at AdminDataUploadUserLV.pcf: line 95, column 41
    function label_29 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.HomePhone")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at AdminDataUploadUserLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Name")
    }
    
    // 'label' attribute on TextCell (id=workPhone_Cell) at AdminDataUploadUserLV.pcf: line 100, column 41
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.WorkPhone")
    }
    
    // 'label' attribute on TextCell (id=cellPhone_Cell) at AdminDataUploadUserLV.pcf: line 105, column 41
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.CellPhone")
    }
    
    // 'label' attribute on TextCell (id=faxPhone_Cell) at AdminDataUploadUserLV.pcf: line 110, column 41
    function label_35 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.FaxPhone")
    }
    
    // 'label' attribute on TextCell (id=primaryAddress_Cell) at AdminDataUploadUserLV.pcf: line 115, column 41
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.PrimaryAddress")
    }
    
    // 'label' attribute on TextCell (id=nonPrimaryAddress_Cell) at AdminDataUploadUserLV.pcf: line 120, column 41
    function label_39 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.SecondaryAddress")
    }
    
    // 'label' attribute on TextCell (id=formerName_Cell) at AdminDataUploadUserLV.pcf: line 125, column 41
    function label_41 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.FormerName")
    }
    
    // 'label' attribute on TextCell (id=emailAddress1_Cell) at AdminDataUploadUserLV.pcf: line 130, column 41
    function label_43 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.EmailAddress1")
    }
    
    // 'label' attribute on TextCell (id=emailAddress2_Cell) at AdminDataUploadUserLV.pcf: line 135, column 41
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.EmailAddress2")
    }
    
    // 'label' attribute on DateCell (id=dateOfBirth_Cell) at AdminDataUploadUserLV.pcf: line 140, column 39
    function label_47 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.DateOfBBirth")
    }
    
    // 'label' attribute on BooleanRadioCell (id=externalUser_Cell) at AdminDataUploadUserLV.pcf: line 145, column 42
    function label_49 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.ExternalUser")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadUserLV.pcf: line 34, column 42
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'label' attribute on TextCell (id=role1_Cell) at AdminDataUploadUserLV.pcf: line 150, column 41
    function label_51 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Role1")
    }
    
    // 'label' attribute on TextCell (id=role2_Cell) at AdminDataUploadUserLV.pcf: line 155, column 41
    function label_53 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Role2")
    }
    
    // 'label' attribute on TextCell (id=role3_Cell) at AdminDataUploadUserLV.pcf: line 160, column 41
    function label_55 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Role3")
    }
    
    // 'label' attribute on TextCell (id=role4_Cell) at AdminDataUploadUserLV.pcf: line 165, column 41
    function label_57 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Role4")
    }
    
    // 'label' attribute on TextCell (id=region1_Cell) at AdminDataUploadUserLV.pcf: line 185, column 41
    function label_65 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Region1")
    }
    
    // 'label' attribute on TextCell (id=region2_Cell) at AdminDataUploadUserLV.pcf: line 190, column 41
    function label_67 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Region2")
    }
    
    // 'label' attribute on TextCell (id=region3_Cell) at AdminDataUploadUserLV.pcf: line 195, column 41
    function label_69 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Region3")
    }
    
    // 'label' attribute on BooleanRadioCell (id=active_Cell) at AdminDataUploadUserLV.pcf: line 39, column 42
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Active")
    }
    
    // 'label' attribute on TextCell (id=language_Cell) at AdminDataUploadUserLV.pcf: line 200, column 41
    function label_71 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Language")
    }
    
    // 'label' attribute on TextCell (id=notes_Cell) at AdminDataUploadUserLV.pcf: line 205, column 41
    function label_73 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Notes")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadUserLV.pcf: line 45, column 41
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.PublicID")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadUserLV.pcf: line 23, column 46
    function sortValue_1 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return processor.getLoadStatus(user)
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadUserLV.pcf: line 45, column 41
    function sortValue_10 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.PublicID
    }
    
    // 'value' attribute on TextCell (id=lastName_Cell) at AdminDataUploadUserLV.pcf: line 50, column 41
    function sortValue_12 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Contact.LastName
    }
    
    // 'value' attribute on TextCell (id=firstName_Cell) at AdminDataUploadUserLV.pcf: line 55, column 41
    function sortValue_14 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Contact.FirstName
    }
    
    // 'value' attribute on TextCell (id=middleName_Cell) at AdminDataUploadUserLV.pcf: line 60, column 41
    function sortValue_16 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Contact.MiddleName
    }
    
    // 'value' attribute on TextCell (id=employeeNumber_Cell) at AdminDataUploadUserLV.pcf: line 65, column 41
    function sortValue_18 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Contact.EmployeeNumber
    }
    
    // 'value' attribute on TextCell (id=username_Cell) at AdminDataUploadUserLV.pcf: line 70, column 41
    function sortValue_20 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.UserName
    }
    
    // 'value' attribute on TextCell (id=password_Cell) at AdminDataUploadUserLV.pcf: line 75, column 41
    function sortValue_22 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Password
    }
    
    // 'value' attribute on TextCell (id=jobTitle_Cell) at AdminDataUploadUserLV.pcf: line 80, column 41
    function sortValue_24 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.JobTitle
    }
    
    // 'value' attribute on TextCell (id=department_Cell) at AdminDataUploadUserLV.pcf: line 85, column 41
    function sortValue_26 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Department
    }
    
    // 'value' attribute on TextCell (id=gender_Cell) at AdminDataUploadUserLV.pcf: line 90, column 41
    function sortValue_28 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Contact.Gender
    }
    
    // 'value' attribute on TextCell (id=homephone_Cell) at AdminDataUploadUserLV.pcf: line 95, column 41
    function sortValue_30 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Contact.HomePhone
    }
    
    // 'value' attribute on TextCell (id=workPhone_Cell) at AdminDataUploadUserLV.pcf: line 100, column 41
    function sortValue_32 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Contact.WorkPhone
    }
    
    // 'value' attribute on TextCell (id=cellPhone_Cell) at AdminDataUploadUserLV.pcf: line 105, column 41
    function sortValue_34 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Contact.CellPhone
    }
    
    // 'value' attribute on TextCell (id=faxPhone_Cell) at AdminDataUploadUserLV.pcf: line 110, column 41
    function sortValue_36 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Contact.FaxPhone
    }
    
    // 'value' attribute on TextCell (id=primaryAddress_Cell) at AdminDataUploadUserLV.pcf: line 115, column 41
    function sortValue_38 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Contact.PrimaryAddress.DisplayName
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadUserLV.pcf: line 29, column 41
    function sortValue_4 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.ReferenceName
    }
    
    // 'value' attribute on TextCell (id=nonPrimaryAddress_Cell) at AdminDataUploadUserLV.pcf: line 120, column 41
    function sortValue_40 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Contact.NonPrimaryAddress.DisplayName
    }
    
    // 'value' attribute on TextCell (id=formerName_Cell) at AdminDataUploadUserLV.pcf: line 125, column 41
    function sortValue_42 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Contact.FormerName
    }
    
    // 'value' attribute on TextCell (id=emailAddress1_Cell) at AdminDataUploadUserLV.pcf: line 130, column 41
    function sortValue_44 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Contact.EmailAddress1
    }
    
    // 'value' attribute on TextCell (id=emailAddress2_Cell) at AdminDataUploadUserLV.pcf: line 135, column 41
    function sortValue_46 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Contact.EmailAddress2
    }
    
    // 'value' attribute on DateCell (id=dateOfBirth_Cell) at AdminDataUploadUserLV.pcf: line 140, column 39
    function sortValue_48 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Contact.DateOfBirth
    }
    
    // 'value' attribute on BooleanRadioCell (id=externalUser_Cell) at AdminDataUploadUserLV.pcf: line 145, column 42
    function sortValue_50 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.ExternalUser
    }
    
    // 'value' attribute on TextCell (id=role1_Cell) at AdminDataUploadUserLV.pcf: line 150, column 41
    function sortValue_52 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Role1.Name
    }
    
    // 'value' attribute on TextCell (id=role2_Cell) at AdminDataUploadUserLV.pcf: line 155, column 41
    function sortValue_54 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Role2.Name
    }
    
    // 'value' attribute on TextCell (id=role3_Cell) at AdminDataUploadUserLV.pcf: line 160, column 41
    function sortValue_56 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Role3.Name
    }
    
    // 'value' attribute on TextCell (id=role4_Cell) at AdminDataUploadUserLV.pcf: line 165, column 41
    function sortValue_58 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Role4.Name
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadUserLV.pcf: line 34, column 42
    function sortValue_6 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Exclude
    }
    
    // 'value' attribute on TextCell (id=role5_Cell) at AdminDataUploadUserLV.pcf: line 170, column 41
    function sortValue_60 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Role5.Name
    }
    
    // 'value' attribute on TextCell (id=role6_Cell) at AdminDataUploadUserLV.pcf: line 175, column 41
    function sortValue_62 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Role6.Name
    }
    
    // 'value' attribute on TextCell (id=role7_Cell) at AdminDataUploadUserLV.pcf: line 180, column 41
    function sortValue_64 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Role7.Name
    }
    
    // 'value' attribute on TextCell (id=region1_Cell) at AdminDataUploadUserLV.pcf: line 185, column 41
    function sortValue_66 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Region1.Name
    }
    
    // 'value' attribute on TextCell (id=region2_Cell) at AdminDataUploadUserLV.pcf: line 190, column 41
    function sortValue_68 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Region2.Name
    }
    
    // 'value' attribute on TextCell (id=region3_Cell) at AdminDataUploadUserLV.pcf: line 195, column 41
    function sortValue_70 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Region3.Name
    }
    
    // 'value' attribute on TextCell (id=language_Cell) at AdminDataUploadUserLV.pcf: line 200, column 41
    function sortValue_72 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Language
    }
    
    // 'value' attribute on TextCell (id=notes_Cell) at AdminDataUploadUserLV.pcf: line 205, column 41
    function sortValue_74 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Contact.Notes
    }
    
    // 'value' attribute on BooleanRadioCell (id=active_Cell) at AdminDataUploadUserLV.pcf: line 39, column 42
    function sortValue_8 (user :  tdic.util.dataloader.data.admindata.UserData) : java.lang.Object {
      return user.Active
    }
    
    // 'value' attribute on RowIterator (id=User) at AdminDataUploadUserLV.pcf: line 15, column 91
    function value_261 () : java.util.ArrayList<tdic.util.dataloader.data.admindata.UserData> {
      return processor.UserArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadUserLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadUserLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminDataUploadUserLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at AdminDataUploadUserLV.pcf: line 17, column 88
    function highlighted_260 () : java.lang.Boolean {
      return (user.Error or user.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=lastName_Cell) at AdminDataUploadUserLV.pcf: line 50, column 41
    function label_100 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.LastName")
    }
    
    // 'label' attribute on TextCell (id=firstName_Cell) at AdminDataUploadUserLV.pcf: line 55, column 41
    function label_105 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.FirstName")
    }
    
    // 'label' attribute on TextCell (id=middleName_Cell) at AdminDataUploadUserLV.pcf: line 60, column 41
    function label_110 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.MiddleName")
    }
    
    // 'label' attribute on TextCell (id=employeeNumber_Cell) at AdminDataUploadUserLV.pcf: line 65, column 41
    function label_115 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.EmployeeNumber")
    }
    
    // 'label' attribute on TextCell (id=username_Cell) at AdminDataUploadUserLV.pcf: line 70, column 41
    function label_120 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.UserName")
    }
    
    // 'label' attribute on TextCell (id=password_Cell) at AdminDataUploadUserLV.pcf: line 75, column 41
    function label_125 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Password")
    }
    
    // 'label' attribute on TextCell (id=jobTitle_Cell) at AdminDataUploadUserLV.pcf: line 80, column 41
    function label_130 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.JobTitle")
    }
    
    // 'label' attribute on TextCell (id=department_Cell) at AdminDataUploadUserLV.pcf: line 85, column 41
    function label_135 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Department")
    }
    
    // 'label' attribute on TextCell (id=gender_Cell) at AdminDataUploadUserLV.pcf: line 90, column 41
    function label_140 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Gender")
    }
    
    // 'label' attribute on TextCell (id=homephone_Cell) at AdminDataUploadUserLV.pcf: line 95, column 41
    function label_145 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.HomePhone")
    }
    
    // 'label' attribute on TextCell (id=workPhone_Cell) at AdminDataUploadUserLV.pcf: line 100, column 41
    function label_150 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.WorkPhone")
    }
    
    // 'label' attribute on TextCell (id=cellPhone_Cell) at AdminDataUploadUserLV.pcf: line 105, column 41
    function label_155 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.CellPhone")
    }
    
    // 'label' attribute on TextCell (id=faxPhone_Cell) at AdminDataUploadUserLV.pcf: line 110, column 41
    function label_160 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.FaxPhone")
    }
    
    // 'label' attribute on TextCell (id=primaryAddress_Cell) at AdminDataUploadUserLV.pcf: line 115, column 41
    function label_165 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.PrimaryAddress")
    }
    
    // 'label' attribute on TextCell (id=nonPrimaryAddress_Cell) at AdminDataUploadUserLV.pcf: line 120, column 41
    function label_170 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.SecondaryAddress")
    }
    
    // 'label' attribute on TextCell (id=formerName_Cell) at AdminDataUploadUserLV.pcf: line 125, column 41
    function label_175 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.FormerName")
    }
    
    // 'label' attribute on TextCell (id=emailAddress1_Cell) at AdminDataUploadUserLV.pcf: line 130, column 41
    function label_180 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.EmailAddress1")
    }
    
    // 'label' attribute on TextCell (id=emailAddress2_Cell) at AdminDataUploadUserLV.pcf: line 135, column 41
    function label_185 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.EmailAddress2")
    }
    
    // 'label' attribute on DateCell (id=dateOfBirth_Cell) at AdminDataUploadUserLV.pcf: line 140, column 39
    function label_190 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.DateOfBBirth")
    }
    
    // 'label' attribute on BooleanRadioCell (id=externalUser_Cell) at AdminDataUploadUserLV.pcf: line 145, column 42
    function label_195 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.ExternalUser")
    }
    
    // 'label' attribute on TextCell (id=role1_Cell) at AdminDataUploadUserLV.pcf: line 150, column 41
    function label_200 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Role1")
    }
    
    // 'label' attribute on TextCell (id=role2_Cell) at AdminDataUploadUserLV.pcf: line 155, column 41
    function label_205 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Role2")
    }
    
    // 'label' attribute on TextCell (id=role3_Cell) at AdminDataUploadUserLV.pcf: line 160, column 41
    function label_210 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Role3")
    }
    
    // 'label' attribute on TextCell (id=role4_Cell) at AdminDataUploadUserLV.pcf: line 165, column 41
    function label_215 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Role4")
    }
    
    // 'label' attribute on TextCell (id=region1_Cell) at AdminDataUploadUserLV.pcf: line 185, column 41
    function label_235 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Region1")
    }
    
    // 'label' attribute on TextCell (id=region2_Cell) at AdminDataUploadUserLV.pcf: line 190, column 41
    function label_240 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Region2")
    }
    
    // 'label' attribute on TextCell (id=region3_Cell) at AdminDataUploadUserLV.pcf: line 195, column 41
    function label_245 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Region3")
    }
    
    // 'label' attribute on TextCell (id=language_Cell) at AdminDataUploadUserLV.pcf: line 200, column 41
    function label_250 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Language")
    }
    
    // 'label' attribute on TextCell (id=notes_Cell) at AdminDataUploadUserLV.pcf: line 205, column 41
    function label_255 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Notes")
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadUserLV.pcf: line 23, column 46
    function label_75 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at AdminDataUploadUserLV.pcf: line 29, column 41
    function label_80 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Name")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadUserLV.pcf: line 34, column 42
    function label_85 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'label' attribute on BooleanRadioCell (id=active_Cell) at AdminDataUploadUserLV.pcf: line 39, column 42
    function label_90 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.Active")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadUserLV.pcf: line 45, column 41
    function label_95 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.User.PublicID")
    }
    
    // 'value' attribute on TextCell (id=lastName_Cell) at AdminDataUploadUserLV.pcf: line 50, column 41
    function valueRoot_102 () : java.lang.Object {
      return user.Contact
    }
    
    // 'value' attribute on TextCell (id=primaryAddress_Cell) at AdminDataUploadUserLV.pcf: line 115, column 41
    function valueRoot_167 () : java.lang.Object {
      return user.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=nonPrimaryAddress_Cell) at AdminDataUploadUserLV.pcf: line 120, column 41
    function valueRoot_172 () : java.lang.Object {
      return user.Contact.NonPrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=role1_Cell) at AdminDataUploadUserLV.pcf: line 150, column 41
    function valueRoot_202 () : java.lang.Object {
      return user.Role1
    }
    
    // 'value' attribute on TextCell (id=role2_Cell) at AdminDataUploadUserLV.pcf: line 155, column 41
    function valueRoot_207 () : java.lang.Object {
      return user.Role2
    }
    
    // 'value' attribute on TextCell (id=role3_Cell) at AdminDataUploadUserLV.pcf: line 160, column 41
    function valueRoot_212 () : java.lang.Object {
      return user.Role3
    }
    
    // 'value' attribute on TextCell (id=role4_Cell) at AdminDataUploadUserLV.pcf: line 165, column 41
    function valueRoot_217 () : java.lang.Object {
      return user.Role4
    }
    
    // 'value' attribute on TextCell (id=role5_Cell) at AdminDataUploadUserLV.pcf: line 170, column 41
    function valueRoot_222 () : java.lang.Object {
      return user.Role5
    }
    
    // 'value' attribute on TextCell (id=role6_Cell) at AdminDataUploadUserLV.pcf: line 175, column 41
    function valueRoot_227 () : java.lang.Object {
      return user.Role6
    }
    
    // 'value' attribute on TextCell (id=role7_Cell) at AdminDataUploadUserLV.pcf: line 180, column 41
    function valueRoot_232 () : java.lang.Object {
      return user.Role7
    }
    
    // 'value' attribute on TextCell (id=region1_Cell) at AdminDataUploadUserLV.pcf: line 185, column 41
    function valueRoot_237 () : java.lang.Object {
      return user.Region1
    }
    
    // 'value' attribute on TextCell (id=region2_Cell) at AdminDataUploadUserLV.pcf: line 190, column 41
    function valueRoot_242 () : java.lang.Object {
      return user.Region2
    }
    
    // 'value' attribute on TextCell (id=region3_Cell) at AdminDataUploadUserLV.pcf: line 195, column 41
    function valueRoot_247 () : java.lang.Object {
      return user.Region3
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadUserLV.pcf: line 29, column 41
    function valueRoot_82 () : java.lang.Object {
      return user
    }
    
    // 'value' attribute on TextCell (id=lastName_Cell) at AdminDataUploadUserLV.pcf: line 50, column 41
    function value_101 () : java.lang.String {
      return user.Contact.LastName
    }
    
    // 'value' attribute on TextCell (id=firstName_Cell) at AdminDataUploadUserLV.pcf: line 55, column 41
    function value_106 () : java.lang.String {
      return user.Contact.FirstName
    }
    
    // 'value' attribute on TextCell (id=middleName_Cell) at AdminDataUploadUserLV.pcf: line 60, column 41
    function value_111 () : java.lang.String {
      return user.Contact.MiddleName
    }
    
    // 'value' attribute on TextCell (id=employeeNumber_Cell) at AdminDataUploadUserLV.pcf: line 65, column 41
    function value_116 () : java.lang.String {
      return user.Contact.EmployeeNumber
    }
    
    // 'value' attribute on TextCell (id=username_Cell) at AdminDataUploadUserLV.pcf: line 70, column 41
    function value_121 () : java.lang.String {
      return user.UserName
    }
    
    // 'value' attribute on TextCell (id=password_Cell) at AdminDataUploadUserLV.pcf: line 75, column 41
    function value_126 () : java.lang.String {
      return user.Password
    }
    
    // 'value' attribute on TextCell (id=jobTitle_Cell) at AdminDataUploadUserLV.pcf: line 80, column 41
    function value_131 () : java.lang.String {
      return user.JobTitle
    }
    
    // 'value' attribute on TextCell (id=department_Cell) at AdminDataUploadUserLV.pcf: line 85, column 41
    function value_136 () : java.lang.String {
      return user.Department
    }
    
    // 'value' attribute on TextCell (id=gender_Cell) at AdminDataUploadUserLV.pcf: line 90, column 41
    function value_141 () : java.lang.String {
      return user.Contact.Gender
    }
    
    // 'value' attribute on TextCell (id=homephone_Cell) at AdminDataUploadUserLV.pcf: line 95, column 41
    function value_146 () : java.lang.String {
      return user.Contact.HomePhone
    }
    
    // 'value' attribute on TextCell (id=workPhone_Cell) at AdminDataUploadUserLV.pcf: line 100, column 41
    function value_151 () : java.lang.String {
      return user.Contact.WorkPhone
    }
    
    // 'value' attribute on TextCell (id=cellPhone_Cell) at AdminDataUploadUserLV.pcf: line 105, column 41
    function value_156 () : java.lang.String {
      return user.Contact.CellPhone
    }
    
    // 'value' attribute on TextCell (id=faxPhone_Cell) at AdminDataUploadUserLV.pcf: line 110, column 41
    function value_161 () : java.lang.String {
      return user.Contact.FaxPhone
    }
    
    // 'value' attribute on TextCell (id=primaryAddress_Cell) at AdminDataUploadUserLV.pcf: line 115, column 41
    function value_166 () : java.lang.String {
      return user.Contact.PrimaryAddress.DisplayName
    }
    
    // 'value' attribute on TextCell (id=nonPrimaryAddress_Cell) at AdminDataUploadUserLV.pcf: line 120, column 41
    function value_171 () : java.lang.String {
      return user.Contact.NonPrimaryAddress.DisplayName
    }
    
    // 'value' attribute on TextCell (id=formerName_Cell) at AdminDataUploadUserLV.pcf: line 125, column 41
    function value_176 () : java.lang.String {
      return user.Contact.FormerName
    }
    
    // 'value' attribute on TextCell (id=emailAddress1_Cell) at AdminDataUploadUserLV.pcf: line 130, column 41
    function value_181 () : java.lang.String {
      return user.Contact.EmailAddress1
    }
    
    // 'value' attribute on TextCell (id=emailAddress2_Cell) at AdminDataUploadUserLV.pcf: line 135, column 41
    function value_186 () : java.lang.String {
      return user.Contact.EmailAddress2
    }
    
    // 'value' attribute on DateCell (id=dateOfBirth_Cell) at AdminDataUploadUserLV.pcf: line 140, column 39
    function value_191 () : java.util.Date {
      return user.Contact.DateOfBirth
    }
    
    // 'value' attribute on BooleanRadioCell (id=externalUser_Cell) at AdminDataUploadUserLV.pcf: line 145, column 42
    function value_196 () : java.lang.Boolean {
      return user.ExternalUser
    }
    
    // 'value' attribute on TextCell (id=role1_Cell) at AdminDataUploadUserLV.pcf: line 150, column 41
    function value_201 () : java.lang.String {
      return user.Role1.Name
    }
    
    // 'value' attribute on TextCell (id=role2_Cell) at AdminDataUploadUserLV.pcf: line 155, column 41
    function value_206 () : java.lang.String {
      return user.Role2.Name
    }
    
    // 'value' attribute on TextCell (id=role3_Cell) at AdminDataUploadUserLV.pcf: line 160, column 41
    function value_211 () : java.lang.String {
      return user.Role3.Name
    }
    
    // 'value' attribute on TextCell (id=role4_Cell) at AdminDataUploadUserLV.pcf: line 165, column 41
    function value_216 () : java.lang.String {
      return user.Role4.Name
    }
    
    // 'value' attribute on TextCell (id=role5_Cell) at AdminDataUploadUserLV.pcf: line 170, column 41
    function value_221 () : java.lang.String {
      return user.Role5.Name
    }
    
    // 'value' attribute on TextCell (id=role6_Cell) at AdminDataUploadUserLV.pcf: line 175, column 41
    function value_226 () : java.lang.String {
      return user.Role6.Name
    }
    
    // 'value' attribute on TextCell (id=role7_Cell) at AdminDataUploadUserLV.pcf: line 180, column 41
    function value_231 () : java.lang.String {
      return user.Role7.Name
    }
    
    // 'value' attribute on TextCell (id=region1_Cell) at AdminDataUploadUserLV.pcf: line 185, column 41
    function value_236 () : java.lang.String {
      return user.Region1.Name
    }
    
    // 'value' attribute on TextCell (id=region2_Cell) at AdminDataUploadUserLV.pcf: line 190, column 41
    function value_241 () : java.lang.String {
      return user.Region2.Name
    }
    
    // 'value' attribute on TextCell (id=region3_Cell) at AdminDataUploadUserLV.pcf: line 195, column 41
    function value_246 () : java.lang.String {
      return user.Region3.Name
    }
    
    // 'value' attribute on TextCell (id=language_Cell) at AdminDataUploadUserLV.pcf: line 200, column 41
    function value_251 () : java.lang.String {
      return user.Language
    }
    
    // 'value' attribute on TextCell (id=notes_Cell) at AdminDataUploadUserLV.pcf: line 205, column 41
    function value_256 () : java.lang.String {
      return user.Contact.Notes
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadUserLV.pcf: line 23, column 46
    function value_76 () : java.lang.String {
      return processor.getLoadStatus(user)
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadUserLV.pcf: line 29, column 41
    function value_81 () : java.lang.String {
      return user.ReferenceName
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadUserLV.pcf: line 34, column 42
    function value_86 () : java.lang.Boolean {
      return user.Exclude
    }
    
    // 'value' attribute on BooleanRadioCell (id=active_Cell) at AdminDataUploadUserLV.pcf: line 39, column 42
    function value_91 () : java.lang.Boolean {
      return user.Active
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadUserLV.pcf: line 45, column 41
    function value_96 () : java.lang.String {
      return user.PublicID
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadUserLV.pcf: line 23, column 46
    function visible_77 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get user () : tdic.util.dataloader.data.admindata.UserData {
      return getIteratedValue(1) as tdic.util.dataloader.data.admindata.UserData
    }
    
    
  }
  
  
}