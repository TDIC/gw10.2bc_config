package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadReturnPremiumSchemeLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanDataUploadReturnPremiumSchemeLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadReturnPremiumSchemeLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PlanDataUploadReturnPremiumSchemeLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at PlanDataUploadReturnPremiumSchemeLV.pcf: line 17, column 118
    function highlighted_50 () : java.lang.Boolean {
      return (returnPremiumScheme.Error or returnPremiumScheme.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 23, column 46
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 29, column 41
    function label_20 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumPlan.Name")
    }
    
    // 'label' attribute on TypeKeyCell (id=handlingCondition_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 35, column 63
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumScheme.HandlingCondition")
    }
    
    // 'label' attribute on TypeKeyCell (id=startDateOptoin_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 40, column 61
    function label_30 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumScheme.StartDateOption")
    }
    
    // 'label' attribute on TypeKeyCell (id=allocateTiming_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 45, column 60
    function label_35 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumScheme.AllocateTiming")
    }
    
    // 'label' attribute on TypeKeyCell (id=allocateMethod_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 50, column 60
    function label_40 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumScheme.AllocateMethod")
    }
    
    // 'label' attribute on TypeKeyCell (id=excessTreatment_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 55, column 61
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumScheme.ExcessTreatment")
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 29, column 41
    function valueRoot_22 () : java.lang.Object {
      return returnPremiumScheme
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 23, column 46
    function value_16 () : java.lang.String {
      return processor.getLoadStatus(returnPremiumScheme)
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 29, column 41
    function value_21 () : java.lang.String {
      return returnPremiumScheme.ReturnPremiumPlanName
    }
    
    // 'value' attribute on TypeKeyCell (id=handlingCondition_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 35, column 63
    function value_26 () : typekey.ReturnPremiumHandlingCondition {
      return returnPremiumScheme.HandlingCondition
    }
    
    // 'value' attribute on TypeKeyCell (id=startDateOptoin_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 40, column 61
    function value_31 () : typekey.ReturnPremiumStartDateOption {
      return returnPremiumScheme.StartDateOption
    }
    
    // 'value' attribute on TypeKeyCell (id=allocateTiming_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 45, column 60
    function value_36 () : typekey.ReturnPremiumAllocateTiming {
      return returnPremiumScheme.AllocateTiming
    }
    
    // 'value' attribute on TypeKeyCell (id=allocateMethod_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 50, column 60
    function value_41 () : typekey.ReturnPremiumAllocateMethod {
      return returnPremiumScheme.AllocateMethod
    }
    
    // 'value' attribute on TypeKeyCell (id=excessTreatment_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 55, column 61
    function value_46 () : typekey.ReturnPremiumExcessTreatment {
      return returnPremiumScheme.ExcessTreatment
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 23, column 46
    function visible_17 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get returnPremiumScheme () : tdic.util.dataloader.data.plandata.ReturnPremiumSchemeData {
      return getIteratedValue(1) as tdic.util.dataloader.data.plandata.ReturnPremiumSchemeData
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadReturnPremiumSchemeLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanDataUploadReturnPremiumSchemeLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TypeKeyCell (id=allocateMethod_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 50, column 60
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumScheme.AllocateMethod")
    }
    
    // 'label' attribute on TypeKeyCell (id=excessTreatment_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 55, column 61
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumScheme.ExcessTreatment")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumPlan.Name")
    }
    
    // 'label' attribute on TypeKeyCell (id=handlingCondition_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 35, column 63
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumScheme.HandlingCondition")
    }
    
    // 'label' attribute on TypeKeyCell (id=startDateOptoin_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 40, column 61
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumScheme.StartDateOption")
    }
    
    // 'label' attribute on TypeKeyCell (id=allocateTiming_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 45, column 60
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumScheme.AllocateTiming")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 23, column 46
    function sortValue_1 (returnPremiumScheme :  tdic.util.dataloader.data.plandata.ReturnPremiumSchemeData) : java.lang.Object {
      return processor.getLoadStatus(returnPremiumScheme)
    }
    
    // 'value' attribute on TypeKeyCell (id=allocateTiming_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 45, column 60
    function sortValue_10 (returnPremiumScheme :  tdic.util.dataloader.data.plandata.ReturnPremiumSchemeData) : java.lang.Object {
      return returnPremiumScheme.AllocateTiming
    }
    
    // 'value' attribute on TypeKeyCell (id=allocateMethod_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 50, column 60
    function sortValue_12 (returnPremiumScheme :  tdic.util.dataloader.data.plandata.ReturnPremiumSchemeData) : java.lang.Object {
      return returnPremiumScheme.AllocateMethod
    }
    
    // 'value' attribute on TypeKeyCell (id=excessTreatment_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 55, column 61
    function sortValue_14 (returnPremiumScheme :  tdic.util.dataloader.data.plandata.ReturnPremiumSchemeData) : java.lang.Object {
      return returnPremiumScheme.ExcessTreatment
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 29, column 41
    function sortValue_4 (returnPremiumScheme :  tdic.util.dataloader.data.plandata.ReturnPremiumSchemeData) : java.lang.Object {
      return returnPremiumScheme.ReturnPremiumPlanName
    }
    
    // 'value' attribute on TypeKeyCell (id=handlingCondition_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 35, column 63
    function sortValue_6 (returnPremiumScheme :  tdic.util.dataloader.data.plandata.ReturnPremiumSchemeData) : java.lang.Object {
      return returnPremiumScheme.HandlingCondition
    }
    
    // 'value' attribute on TypeKeyCell (id=startDateOptoin_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 40, column 61
    function sortValue_8 (returnPremiumScheme :  tdic.util.dataloader.data.plandata.ReturnPremiumSchemeData) : java.lang.Object {
      return returnPremiumScheme.StartDateOption
    }
    
    // 'value' attribute on RowIterator (id=ReturnPremiumScheme) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 15, column 105
    function value_51 () : java.util.ArrayList<tdic.util.dataloader.data.plandata.ReturnPremiumSchemeData> {
      return processor.ReturnPremiumSchemeArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadReturnPremiumSchemeLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}