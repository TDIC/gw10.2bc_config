package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillStatementTotalsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillStatementTotalsDVExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillStatementTotalsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillStatementTotalsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=GrossAmountOnThisStatement_Input) at AgencyBillStatementTotalsDV.pcf: line 35, column 82
    function currency_11 () : typekey.Currency {
      return agencyBillStatementView.Producer.Currency
    }
    
    // 'value' attribute on DateInput (id=BillingDate_Input) at AgencyBillStatementTotalsDV.pcf: line 16, column 52
    function valueRoot_1 () : java.lang.Object {
      return agencyBillStatementView
    }
    
    // 'value' attribute on TextInput (id=Status_Input) at AgencyBillStatementTotalsDV.pcf: line 24, column 73
    function valueRoot_7 () : java.lang.Object {
      return agencyBillStatementView.StatementInvoice
    }
    
    // 'value' attribute on DateInput (id=BillingDate_Input) at AgencyBillStatementTotalsDV.pcf: line 16, column 52
    function value_0 () : java.util.Date {
      return agencyBillStatementView.EventDate
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CommissionAmountOnThisStatement_Input) at AgencyBillStatementTotalsDV.pcf: line 42, column 87
    function value_13 () : gw.pl.currency.MonetaryAmount {
      return agencyBillStatementView.TotalCommissionAmountForItemsOnThisStatement
    }
    
    // 'value' attribute on MonetaryAmountInput (id=NetAmountOnThisStatement_Input) at AgencyBillStatementTotalsDV.pcf: line 49, column 80
    function value_17 () : gw.pl.currency.MonetaryAmount {
      return agencyBillStatementView.TotalNetAmountForItemsOnThisStatement
    }
    
    // 'value' attribute on MonetaryAmountInput (id=NetPaymentAppliedOnThisStatement_Input) at AgencyBillStatementTotalsDV.pcf: line 56, column 87
    function value_21 () : gw.pl.currency.MonetaryAmount {
      return agencyBillStatementView.TotalNetAmountSettledForItemsOnThisStatement
    }
    
    // 'value' attribute on MonetaryAmountInput (id=NetOfPaymentOnThisStatement_Input) at AgencyBillStatementTotalsDV.pcf: line 63, column 161
    function value_25 () : gw.pl.currency.MonetaryAmount {
      return agencyBillStatementView.TotalNetAmountForItemsOnThisStatement.subtract( agencyBillStatementView.TotalNetAmountSettledForItemsOnThisStatement )
    }
    
    // 'value' attribute on MonetaryAmountInput (id=GrossAmountFromPreviousUnpaidStatements_Input) at AgencyBillStatementTotalsDV.pcf: line 74, column 87
    function value_28 () : gw.pl.currency.MonetaryAmount {
      return agencyBillStatementView.TotalGrossAmountForPreviousSnapshotItemsOnly
    }
    
    // 'value' attribute on DateInput (id=DueDate_Input) at AgencyBillStatementTotalsDV.pcf: line 20, column 57
    function value_3 () : java.util.Date {
      return agencyBillStatementView.PaymentDueDate
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CommissionAmountFromPreviousUnpaidStatements_Input) at AgencyBillStatementTotalsDV.pcf: line 81, column 92
    function value_32 () : gw.pl.currency.MonetaryAmount {
      return agencyBillStatementView.TotalCommissionAmountForPreviousSnapshotItemsOnly
    }
    
    // 'value' attribute on MonetaryAmountInput (id=NetAmountFromPreviousUnpaidStatements_Input) at AgencyBillStatementTotalsDV.pcf: line 88, column 85
    function value_36 () : gw.pl.currency.MonetaryAmount {
      return agencyBillStatementView.TotalNetAmountForPreviousSnapshotItemsOnly
    }
    
    // 'value' attribute on MonetaryAmountInput (id=NetPaymentAppliedFromPreviousUnpaidStatements_Input) at AgencyBillStatementTotalsDV.pcf: line 95, column 92
    function value_40 () : gw.pl.currency.MonetaryAmount {
      return agencyBillStatementView.TotalNetAmountSettledForPreviousSnapshotItemsOnly
    }
    
    // 'value' attribute on MonetaryAmountInput (id=NetOfPaymentFromPreviousUnpaidStatements_Input) at AgencyBillStatementTotalsDV.pcf: line 102, column 171
    function value_44 () : gw.pl.currency.MonetaryAmount {
      return agencyBillStatementView.TotalNetAmountForPreviousSnapshotItemsOnly.subtract( agencyBillStatementView.TotalNetAmountSettledForPreviousSnapshotItemsOnly )
    }
    
    // 'value' attribute on MonetaryAmountInput (id=AmountDue_Input) at AgencyBillStatementTotalsDV.pcf: line 113, column 89
    function value_47 () : gw.pl.currency.MonetaryAmount {
      return agencyBillStatementView.TotalGrossAmountIncludingPreviousSnapshotItems
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CommissionAmount_Input) at AgencyBillStatementTotalsDV.pcf: line 120, column 94
    function value_51 () : gw.pl.currency.MonetaryAmount {
      return agencyBillStatementView.TotalCommissionAmountIncludingPreviousSnapshotItems
    }
    
    // 'value' attribute on MonetaryAmountInput (id=NetAmount_Input) at AgencyBillStatementTotalsDV.pcf: line 127, column 87
    function value_55 () : gw.pl.currency.MonetaryAmount {
      return agencyBillStatementView.TotalNetAmountIncludingPreviousSnapshotItems
    }
    
    // 'value' attribute on MonetaryAmountInput (id=NetPaymentApplied_Input) at AgencyBillStatementTotalsDV.pcf: line 134, column 85
    function value_59 () : gw.pl.currency.MonetaryAmount {
      return agencyBillStatementView.TotalNetAmountSettledInclPrevSnapshotItems
    }
    
    // 'value' attribute on TextInput (id=Status_Input) at AgencyBillStatementTotalsDV.pcf: line 24, column 73
    function value_6 () : java.lang.String {
      return agencyBillStatementView.StatementInvoice.DisplayStatus
    }
    
    // 'value' attribute on MonetaryAmountInput (id=NetOfPayment_Input) at AgencyBillStatementTotalsDV.pcf: line 141, column 166
    function value_63 () : gw.pl.currency.MonetaryAmount {
      return agencyBillStatementView.TotalNetAmountIncludingPreviousSnapshotItems.subtract( agencyBillStatementView.TotalNetAmountSettledInclPrevSnapshotItems )
    }
    
    // 'value' attribute on MonetaryAmountInput (id=GrossAmountOnThisStatement_Input) at AgencyBillStatementTotalsDV.pcf: line 35, column 82
    function value_9 () : gw.pl.currency.MonetaryAmount {
      return agencyBillStatementView.TotalGrossAmountForItemsOnThisStatement
    }
    
    property get agencyBillStatementView () : gw.api.web.invoice.AgencyBillStatementView {
      return getRequireValue("agencyBillStatementView", 0) as gw.api.web.invoice.AgencyBillStatementView
    }
    
    property set agencyBillStatementView ($arg :  gw.api.web.invoice.AgencyBillStatementView) {
      setRequireValue("agencyBillStatementView", 0, $arg)
    }
    
    
  }
  
  
}