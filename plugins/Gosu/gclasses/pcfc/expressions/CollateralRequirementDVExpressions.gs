package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralRequirementDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CollateralRequirementDVExpressions {
  @javax.annotation.Generated("config/web/pcf/collateral/CollateralRequirementDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CollateralRequirementDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=required_Input) at CollateralRequirementDV.pcf: line 38, column 38
    function currency_11 () : typekey.Currency {
      return requirement.Currency
    }
    
    // 'value' attribute on TypeKeyInput (id=type_Input) at CollateralRequirementDV.pcf: line 48, column 55
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      requirement.RequirementType = (__VALUE_TO_SET as typekey.CollateralRequirementType)
    }
    
    // 'value' attribute on RangeInput (id=levelRange_Input) at CollateralRequirementDV.pcf: line 59, column 45
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      level = (__VALUE_TO_SET as typekey.CollateralLevel)
    }
    
    // 'value' attribute on TextInput (id=name_Input) at CollateralRequirementDV.pcf: line 29, column 46
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      requirement.RequirementName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=policyPeriod_Input) at CollateralRequirementDV.pcf: line 71, column 60
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      requirement.PolicyPeriod = (__VALUE_TO_SET as entity.PolicyPeriod)
    }
    
    // 'value' attribute on RangeInput (id=Policy_Input) at CollateralRequirementDV.pcf: line 83, column 55
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      requirement.Policy = (__VALUE_TO_SET as entity.Policy)
    }
    
    // 'value' attribute on DateInput (id=effectiveDate_Input) at CollateralRequirementDV.pcf: line 90, column 43
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      requirement.EffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=expirationDate_Input) at CollateralRequirementDV.pcf: line 99, column 44
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      requirement.ExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on CheckBoxInput (id=segregate_Input) at CollateralRequirementDV.pcf: line 109, column 59
    function defaultSetter_63 (__VALUE_TO_SET :  java.lang.Object) : void {
      requirement.Segregated = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=createChargeCheckbox_Input) at CollateralRequirementDV.pcf: line 115, column 105
    function defaultSetter_69 (__VALUE_TO_SET :  java.lang.Object) : void {
      requirement.CreateCharge = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=required_Input) at CollateralRequirementDV.pcf: line 38, column 38
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      requirement.Required = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'editable' attribute on DateInput (id=effectiveDate_Input) at CollateralRequirementDV.pcf: line 90, column 43
    function editable_47 () : java.lang.Boolean {
      return effectiveDateEditable
    }
    
    // 'initialValue' attribute on Variable at CollateralRequirementDV.pcf: line 16, column 31
    function initialValue_0 () : CollateralLevel {
      return initialLevel()
    }
    
    // 'initialValue' attribute on Variable at CollateralRequirementDV.pcf: line 20, column 49
    function initialValue_1 () : gw.api.web.account.CollateralUtil {
      return new gw.api.web.account.CollateralUtil()
    }
    
    // 'onChange' attribute on PostOnChange at CollateralRequirementDV.pcf: line 50, column 41
    function onChange_14 () : void {
      recommendedDate()
    }
    
    // 'onChange' attribute on PostOnChange at CollateralRequirementDV.pcf: line 61, column 43
    function onChange_19 () : void {
      resetPolicyPeriod()
    }
    
    // 'onChange' attribute on PostOnChange at CollateralRequirementDV.pcf: line 73, column 41
    function onChange_26 () : void {
      recommendedDate()
    }
    
    // 'onChange' attribute on PostOnChange at CollateralRequirementDV.pcf: line 40, column 41
    function onChange_6 () : void {
      recommendedDate()
    }
    
    // 'required' attribute on RangeInput (id=policyPeriod_Input) at CollateralRequirementDV.pcf: line 71, column 60
    function required_28 () : java.lang.Boolean {
      return level ==CollateralLevel.TC_POLICYPERIOD
    }
    
    // 'required' attribute on RangeInput (id=Policy_Input) at CollateralRequirementDV.pcf: line 83, column 55
    function required_38 () : java.lang.Boolean {
      return level == CollateralLevel.TC_POLICY 
    }
    
    // 'validationExpression' attribute on DateInput (id=effectiveDate_Input) at CollateralRequirementDV.pcf: line 90, column 43
    function validationExpression_48 () : java.lang.Object {
      return gw.api.util.DateUtil.verifyDateOnOrAfterToday(requirement.EffectiveDate) ? null : DisplayKey.get("Web.NewCollateralDV.EffectiveDateError")
    }
    
    // 'validationExpression' attribute on DateInput (id=expirationDate_Input) at CollateralRequirementDV.pcf: line 99, column 44
    function validationExpression_55 () : java.lang.Object {
      return verifyExpirationDate()
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=required_Input) at CollateralRequirementDV.pcf: line 38, column 38
    function validationExpression_7 () : java.lang.Object {
      return requirement.Required.IsZero ? DisplayKey.get("Web.NewCollateralDV.AmountNotZero") : null
    }
    
    // 'valueRange' attribute on RangeInput (id=levelRange_Input) at CollateralRequirementDV.pcf: line 59, column 45
    function valueRange_22 () : java.lang.Object {
      return collateralUtil.getLevels()
    }
    
    // 'valueRange' attribute on RangeInput (id=policyPeriod_Input) at CollateralRequirementDV.pcf: line 71, column 60
    function valueRange_32 () : java.lang.Object {
      return requirement.Collateral.Account.OpenPolicyPeriods
    }
    
    // 'valueRange' attribute on RangeInput (id=Policy_Input) at CollateralRequirementDV.pcf: line 83, column 55
    function valueRange_42 () : java.lang.Object {
      return requirement.Collateral.Account.Policies
    }
    
    // 'value' attribute on TextInput (id=name_Input) at CollateralRequirementDV.pcf: line 29, column 46
    function valueRoot_4 () : java.lang.Object {
      return requirement
    }
    
    // 'value' attribute on TypeKeyInput (id=type_Input) at CollateralRequirementDV.pcf: line 48, column 55
    function value_15 () : typekey.CollateralRequirementType {
      return requirement.RequirementType
    }
    
    // 'value' attribute on TextInput (id=name_Input) at CollateralRequirementDV.pcf: line 29, column 46
    function value_2 () : java.lang.String {
      return requirement.RequirementName
    }
    
    // 'value' attribute on RangeInput (id=levelRange_Input) at CollateralRequirementDV.pcf: line 59, column 45
    function value_20 () : typekey.CollateralLevel {
      return level
    }
    
    // 'value' attribute on RangeInput (id=policyPeriod_Input) at CollateralRequirementDV.pcf: line 71, column 60
    function value_29 () : entity.PolicyPeriod {
      return requirement.PolicyPeriod
    }
    
    // 'value' attribute on RangeInput (id=Policy_Input) at CollateralRequirementDV.pcf: line 83, column 55
    function value_39 () : entity.Policy {
      return requirement.Policy
    }
    
    // 'value' attribute on DateInput (id=effectiveDate_Input) at CollateralRequirementDV.pcf: line 90, column 43
    function value_49 () : java.util.Date {
      return requirement.EffectiveDate
    }
    
    // 'value' attribute on DateInput (id=expirationDate_Input) at CollateralRequirementDV.pcf: line 99, column 44
    function value_56 () : java.util.Date {
      return requirement.ExpirationDate
    }
    
    // 'value' attribute on CheckBoxInput (id=segregate_Input) at CollateralRequirementDV.pcf: line 109, column 59
    function value_62 () : java.lang.Boolean {
      return requirement.Segregated
    }
    
    // 'value' attribute on CheckBoxInput (id=createChargeCheckbox_Input) at CollateralRequirementDV.pcf: line 115, column 105
    function value_68 () : java.lang.Boolean {
      return requirement.CreateCharge
    }
    
    // 'value' attribute on MonetaryAmountInput (id=required_Input) at CollateralRequirementDV.pcf: line 38, column 38
    function value_8 () : gw.pl.currency.MonetaryAmount {
      return requirement.Required
    }
    
    // 'valueRange' attribute on RangeInput (id=levelRange_Input) at CollateralRequirementDV.pcf: line 59, column 45
    function verifyValueRangeIsAllowedType_23 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=levelRange_Input) at CollateralRequirementDV.pcf: line 59, column 45
    function verifyValueRangeIsAllowedType_23 ($$arg :  typekey.CollateralLevel[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=policyPeriod_Input) at CollateralRequirementDV.pcf: line 71, column 60
    function verifyValueRangeIsAllowedType_33 ($$arg :  entity.PolicyPeriod[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=policyPeriod_Input) at CollateralRequirementDV.pcf: line 71, column 60
    function verifyValueRangeIsAllowedType_33 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyPeriod>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=policyPeriod_Input) at CollateralRequirementDV.pcf: line 71, column 60
    function verifyValueRangeIsAllowedType_33 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Policy_Input) at CollateralRequirementDV.pcf: line 83, column 55
    function verifyValueRangeIsAllowedType_43 ($$arg :  entity.Policy[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Policy_Input) at CollateralRequirementDV.pcf: line 83, column 55
    function verifyValueRangeIsAllowedType_43 ($$arg :  gw.api.database.IQueryBeanResult<entity.Policy>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Policy_Input) at CollateralRequirementDV.pcf: line 83, column 55
    function verifyValueRangeIsAllowedType_43 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=levelRange_Input) at CollateralRequirementDV.pcf: line 59, column 45
    function verifyValueRange_24 () : void {
      var __valueRangeArg = collateralUtil.getLevels()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_23(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=policyPeriod_Input) at CollateralRequirementDV.pcf: line 71, column 60
    function verifyValueRange_34 () : void {
      var __valueRangeArg = requirement.Collateral.Account.OpenPolicyPeriods
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_33(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=Policy_Input) at CollateralRequirementDV.pcf: line 83, column 55
    function verifyValueRange_44 () : void {
      var __valueRangeArg = requirement.Collateral.Account.Policies
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_43(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=policyPeriod_Input) at CollateralRequirementDV.pcf: line 71, column 60
    function visible_27 () : java.lang.Boolean {
      return level == CollateralLevel.TC_POLICYPERIOD
    }
    
    // 'visible' attribute on RangeInput (id=Policy_Input) at CollateralRequirementDV.pcf: line 83, column 55
    function visible_37 () : java.lang.Boolean {
      return level == CollateralLevel.TC_POLICY
    }
    
    // 'visible' attribute on CheckBoxInput (id=segregate_Input) at CollateralRequirementDV.pcf: line 109, column 59
    function visible_61 () : java.lang.Boolean {
      return requirement.RequirementType == TC_CASH
    }
    
    // 'visible' attribute on CheckBoxInput (id=createChargeCheckbox_Input) at CollateralRequirementDV.pcf: line 115, column 105
    function visible_67 () : java.lang.Boolean {
      return requirement.RequirementType == TC_CASH and requirement.isNewChargeCreatedIfRequired()
    }
    
    property get collateralUtil () : gw.api.web.account.CollateralUtil {
      return getVariableValue("collateralUtil", 0) as gw.api.web.account.CollateralUtil
    }
    
    property set collateralUtil ($arg :  gw.api.web.account.CollateralUtil) {
      setVariableValue("collateralUtil", 0, $arg)
    }
    
    property get effectiveDateEditable () : Boolean {
      return getRequireValue("effectiveDateEditable", 0) as Boolean
    }
    
    property set effectiveDateEditable ($arg :  Boolean) {
      setRequireValue("effectiveDateEditable", 0, $arg)
    }
    
    property get level () : CollateralLevel {
      return getVariableValue("level", 0) as CollateralLevel
    }
    
    property set level ($arg :  CollateralLevel) {
      setVariableValue("level", 0, $arg)
    }
    
    property get requirement () : CollateralRequirement {
      return getRequireValue("requirement", 0) as CollateralRequirement
    }
    
    property set requirement ($arg :  CollateralRequirement) {
      setRequireValue("requirement", 0, $arg)
    }
    
    function resetPolicyPeriod(){
            if(level != CollateralLevel.TC_POLICYPERIOD){
              requirement.PolicyPeriod = null;
            }
            if(level != CollateralLevel.TC_POLICY){
              requirement.Policy = null;
            }
          }
    
          function initialLevel() : CollateralLevel{
            if(requirement.PolicyPeriod != null){
              return CollateralLevel.TC_POLICYPERIOD
            }
            if (requirement.Policy != null){
               return CollateralLevel.TC_POLICY;
            }
            if(!requirement.isNew()){
              return CollateralLevel.TC_ACCOUNT;
            }
            return null;
          }
          
          function recommendedDate(){
            if(requirement.RequirementType == TC_CASH  && requirement.isNewChargeCreatedIfRequired() && effectiveDateEditable){
               if(requirement.PolicyPeriod != null){
                 requirement.EffectiveDate = collateralUtil.getInvoiceDueDateAfterDate(requirement, gw.api.upgrade.Coercions.makeDateFrom(getEffectiveDateForPolicyPeriodLevelReq()));      
               }else{
                 requirement.EffectiveDate = collateralUtil.getNextInvoiceDate(requirement);
               }
             }
            }
            
            function getEffectiveDateForPolicyPeriodLevelReq() : String {
              if(gw.api.util.DateUtil.verifyDateOnOrAfterToday( requirement.PolicyPeriod.PolicyPerEffDate )){
                return requirement.PolicyPeriod.PolicyPerEffDate as java.lang.String; 
              }
              return gw.api.util.DateUtil.currentDate() as java.lang.String;
            }
            
            function verifyExpirationDate () : String {
              if(requirement.ExpirationDate == null)
                return null;
              if(!gw.api.util.DateUtil.verifyDateOnOrAfterToday(requirement.ExpirationDate)) 
                 return DisplayKey.get("Web.NewCollateralDV.ExpirationDateError")        
              if(requirement.EffectiveDate > requirement.ExpirationDate)
                return DisplayKey.get("Web.NewCollateralDV.ExpirationDateBeforeEffectiveDateError")
              return null;
            }
        
    
    
  }
  
  
}