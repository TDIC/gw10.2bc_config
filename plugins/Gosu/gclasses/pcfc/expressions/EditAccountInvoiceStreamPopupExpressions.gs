package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/EditAccountInvoiceStreamPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class EditAccountInvoiceStreamPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/EditAccountInvoiceStreamPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class EditAccountInvoiceStreamPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account, invoiceStream :  InvoiceStream) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=EditButton) at EditAccountInvoiceStreamPopup.pcf: line 29, column 86
    function action_2 () : void {
      CurrentLocation.startEditing()
    }
    
    // 'action' attribute on ToolbarButton (id=UpdateButton) at EditAccountInvoiceStreamPopup.pcf: line 34, column 85
    function action_4 () : void {
      CurrentLocation.commit()
    }
    
    // 'action' attribute on ToolbarButton (id=CancelButton) at EditAccountInvoiceStreamPopup.pcf: line 39, column 85
    function action_6 () : void {
      CurrentLocation.cancel()
    }
    
    // 'canEdit' attribute on Popup (id=EditAccountInvoiceStreamPopup) at EditAccountInvoiceStreamPopup.pcf: line 10, column 76
    function canEdit_9 () : java.lang.Boolean {
      return perm.System.invcstrmedit
    }
    
    // 'canVisit' attribute on Popup (id=EditAccountInvoiceStreamPopup) at EditAccountInvoiceStreamPopup.pcf: line 10, column 76
    static function canVisit_10 (account :  Account, invoiceStream :  InvoiceStream) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctinvcview
    }
    
    // 'def' attribute on PanelRef at EditAccountInvoiceStreamPopup.pcf: line 42, column 73
    function def_onEnter_7 (def :  pcf.AccountInvoiceStreamDetailDV) : void {
      def.onEnter(account, invoiceStreamView)
    }
    
    // 'def' attribute on PanelRef at EditAccountInvoiceStreamPopup.pcf: line 42, column 73
    function def_refreshVariables_8 (def :  pcf.AccountInvoiceStreamDetailDV) : void {
      def.refreshVariables(account, invoiceStreamView)
    }
    
    // 'initialValue' attribute on Variable at EditAccountInvoiceStreamPopup.pcf: line 22, column 52
    function initialValue_0 () : gw.api.web.invoice.InvoiceStreamView {
      return new gw.api.web.invoice.InvoiceStreamView(invoiceStream)
    }
    
    // 'visible' attribute on ToolbarButton (id=EditButton) at EditAccountInvoiceStreamPopup.pcf: line 29, column 86
    function visible_1 () : java.lang.Boolean {
      return !CurrentLocation.InEditMode && perm.System.invcstrmedit
    }
    
    // 'visible' attribute on ToolbarButton (id=UpdateButton) at EditAccountInvoiceStreamPopup.pcf: line 34, column 85
    function visible_3 () : java.lang.Boolean {
      return CurrentLocation.InEditMode && perm.System.invcstrmedit
    }
    
    override property get CurrentLocation () : pcf.EditAccountInvoiceStreamPopup {
      return super.CurrentLocation as pcf.EditAccountInvoiceStreamPopup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get invoiceStream () : InvoiceStream {
      return getVariableValue("invoiceStream", 0) as InvoiceStream
    }
    
    property set invoiceStream ($arg :  InvoiceStream) {
      setVariableValue("invoiceStream", 0, $arg)
    }
    
    property get invoiceStreamView () : gw.api.web.invoice.InvoiceStreamView {
      return getVariableValue("invoiceStreamView", 0) as gw.api.web.invoice.InvoiceStreamView
    }
    
    property set invoiceStreamView ($arg :  gw.api.web.invoice.InvoiceStreamView) {
      setVariableValue("invoiceStreamView", 0, $arg)
    }
    
    
  }
  
  
}