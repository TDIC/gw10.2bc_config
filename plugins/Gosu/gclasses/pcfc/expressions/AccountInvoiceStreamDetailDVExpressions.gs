package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountInvoiceStreamDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountInvoiceStreamDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountInvoiceStreamDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountInvoiceStreamDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=InvoiceDayChangerPopup) at AccountInvoiceStreamDetailDV.pcf: line 59, column 50
    function action_22 () : void {
      InvoiceDayChangerPopup.push(invoiceStreamView.InvoiceStream)
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at AccountInvoiceStreamDetailDV.pcf: line 97, column 63
    function action_46 () : void {
      NewPaymentInstrumentPopup.push(tdic.bc.config.payment.PaymentInstrumentFilters.newPaymentInstrumentFilterACHOnly,account,false)
    }
    
    // 'action' attribute on MenuItem (id=InvoiceDayChangerPopup) at AccountInvoiceStreamDetailDV.pcf: line 59, column 50
    function action_dest_23 () : pcf.api.Destination {
      return pcf.InvoiceDayChangerPopup.createDestination(invoiceStreamView.InvoiceStream)
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at AccountInvoiceStreamDetailDV.pcf: line 97, column 63
    function action_dest_47 () : pcf.api.Destination {
      return pcf.NewPaymentInstrumentPopup.createDestination(tdic.bc.config.payment.PaymentInstrumentFilters.newPaymentInstrumentFilterACHOnly,account,false)
    }
    
    // 'available' attribute on MenuItem (id=InvoiceDayChangerPopup) at AccountInvoiceStreamDetailDV.pcf: line 59, column 50
    function available_20 () : java.lang.Boolean {
      return !CurrentLocation.InEditMode && perm.System.invcstrmedit
    }
    
    // 'def' attribute on InputSetRef at AccountInvoiceStreamDetailDV.pcf: line 69, column 47
    function def_onEnter_26 (def :  pcf.InvoiceStreamAnchorDateInputSet_default) : void {
      def.onEnter(invoiceDayChangeHelper, CurrentLocation.InEditMode && perm.System.acctdetedit && invoiceStreamView.InvoiceStream.New)
    }
    
    // 'def' attribute on InputSetRef at AccountInvoiceStreamDetailDV.pcf: line 69, column 47
    function def_onEnter_28 (def :  pcf.InvoiceStreamAnchorDateInputSet_everyotherweek) : void {
      def.onEnter(invoiceDayChangeHelper, CurrentLocation.InEditMode && perm.System.acctdetedit && invoiceStreamView.InvoiceStream.New)
    }
    
    // 'def' attribute on InputSetRef at AccountInvoiceStreamDetailDV.pcf: line 69, column 47
    function def_onEnter_30 (def :  pcf.InvoiceStreamAnchorDateInputSet_everyweek) : void {
      def.onEnter(invoiceDayChangeHelper, CurrentLocation.InEditMode && perm.System.acctdetedit && invoiceStreamView.InvoiceStream.New)
    }
    
    // 'def' attribute on InputSetRef at AccountInvoiceStreamDetailDV.pcf: line 69, column 47
    function def_onEnter_32 (def :  pcf.InvoiceStreamAnchorDateInputSet_monthly) : void {
      def.onEnter(invoiceDayChangeHelper, CurrentLocation.InEditMode && perm.System.acctdetedit && invoiceStreamView.InvoiceStream.New)
    }
    
    // 'def' attribute on InputSetRef at AccountInvoiceStreamDetailDV.pcf: line 69, column 47
    function def_onEnter_34 (def :  pcf.InvoiceStreamAnchorDateInputSet_twicepermonth) : void {
      def.onEnter(invoiceDayChangeHelper, CurrentLocation.InEditMode && perm.System.acctdetedit && invoiceStreamView.InvoiceStream.New)
    }
    
    // 'def' attribute on InputSetRef at AccountInvoiceStreamDetailDV.pcf: line 69, column 47
    function def_refreshVariables_27 (def :  pcf.InvoiceStreamAnchorDateInputSet_default) : void {
      def.refreshVariables(invoiceDayChangeHelper, CurrentLocation.InEditMode && perm.System.acctdetedit && invoiceStreamView.InvoiceStream.New)
    }
    
    // 'def' attribute on InputSetRef at AccountInvoiceStreamDetailDV.pcf: line 69, column 47
    function def_refreshVariables_29 (def :  pcf.InvoiceStreamAnchorDateInputSet_everyotherweek) : void {
      def.refreshVariables(invoiceDayChangeHelper, CurrentLocation.InEditMode && perm.System.acctdetedit && invoiceStreamView.InvoiceStream.New)
    }
    
    // 'def' attribute on InputSetRef at AccountInvoiceStreamDetailDV.pcf: line 69, column 47
    function def_refreshVariables_31 (def :  pcf.InvoiceStreamAnchorDateInputSet_everyweek) : void {
      def.refreshVariables(invoiceDayChangeHelper, CurrentLocation.InEditMode && perm.System.acctdetedit && invoiceStreamView.InvoiceStream.New)
    }
    
    // 'def' attribute on InputSetRef at AccountInvoiceStreamDetailDV.pcf: line 69, column 47
    function def_refreshVariables_33 (def :  pcf.InvoiceStreamAnchorDateInputSet_monthly) : void {
      def.refreshVariables(invoiceDayChangeHelper, CurrentLocation.InEditMode && perm.System.acctdetedit && invoiceStreamView.InvoiceStream.New)
    }
    
    // 'def' attribute on InputSetRef at AccountInvoiceStreamDetailDV.pcf: line 69, column 47
    function def_refreshVariables_35 (def :  pcf.InvoiceStreamAnchorDateInputSet_twicepermonth) : void {
      def.refreshVariables(invoiceDayChangeHelper, CurrentLocation.InEditMode && perm.System.acctdetedit && invoiceStreamView.InvoiceStream.New)
    }
    
    // 'value' attribute on TextInput (id=NewUnappliedFundsDescription_Input) at AccountInvoiceStreamDetailDV.pcf: line 187, column 57
    function defaultSetter_108 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceStreamView.DescriptionOnNewUnappliedFund = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=ExistingUnappliedFunds_Input) at AccountInvoiceStreamDetailDV.pcf: line 199, column 58
    function defaultSetter_117 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceStreamView.UnappliedFund = (__VALUE_TO_SET as entity.UnappliedFund)
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at AccountInvoiceStreamDetailDV.pcf: line 44, column 90
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceStreamView.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverridePaymentInstrument_Input) at AccountInvoiceStreamDetailDV.pcf: line 85, column 46
    function defaultSetter_42 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceStreamView.OverridePaymentInstrument = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=OverridingPaymentInstrument_Input) at AccountInvoiceStreamDetailDV.pcf: line 97, column 63
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceStreamView.OverridingPaymentInstrument = (__VALUE_TO_SET as entity.PaymentInstrument)
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverrideInvoicingLeadTime_Input) at AccountInvoiceStreamDetailDV.pcf: line 119, column 46
    function defaultSetter_65 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceStreamView.OverrideInvoicingLeadTime = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=Periodicity_Input) at AccountInvoiceStreamDetailDV.pcf: line 35, column 41
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceStreamView.Periodicity = (__VALUE_TO_SET as typekey.Periodicity)
    }
    
    // 'value' attribute on TextInput (id=OverridingLeadTimeDayCount_Input) at AccountInvoiceStreamDetailDV.pcf: line 129, column 63
    function defaultSetter_72 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceStreamView.OverridingLeadTimeDayCount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverrideBillDateOrDueDateBilling_Input) at AccountInvoiceStreamDetailDV.pcf: line 147, column 46
    function defaultSetter_82 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceStreamView.OverrideBillDateOrDueDateBilling = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=OverridingBillDateOrDueDateBilling_Input) at AccountInvoiceStreamDetailDV.pcf: line 157, column 70
    function defaultSetter_89 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceStreamView.OverridingBillDateOrDueDateBilling = (__VALUE_TO_SET as typekey.BillDateOrDueDateBilling)
    }
    
    // 'value' attribute on BooleanRadioInput (id=UnappliedFundsNewOrExisting_Input) at AccountInvoiceStreamDetailDV.pcf: line 171, column 55
    function defaultSetter_96 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceStreamView.IsUnappliedFundsNew = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'editable' attribute on RangeInput (id=ExistingUnappliedFunds_Input) at AccountInvoiceStreamDetailDV.pcf: line 199, column 58
    function editable_113 () : java.lang.Boolean {
      return invoiceStreamView.InvoiceStream.New  && !(invoiceStreamView.InvoiceStream.InvoicePayer as Account).ListBill
    }
    
    // 'editable' attribute on RangeInput (id=Periodicity_Input) at AccountInvoiceStreamDetailDV.pcf: line 35, column 41
    function editable_5 () : java.lang.Boolean {
      return invoiceStreamView.InvoiceStream.New
    }
    
    // 'editable' attribute on BooleanRadioInput (id=UnappliedFundsNewOrExisting_Input) at AccountInvoiceStreamDetailDV.pcf: line 171, column 55
    function editable_94 () : java.lang.Boolean {
      return invoiceStreamView.InvoiceStream.New && !(invoiceStreamView.InvoiceStream.InvoicePayer as Account).ListBill
    }
    
    // 'initialValue' attribute on Variable at AccountInvoiceStreamDetailDV.pcf: line 16, column 49
    function initialValue_0 () : gw.payment.PaymentInstrumentRange {
      return new gw.payment.PaymentInstrumentRange(account.PaymentInstruments)
    }
    
    // 'initialValue' attribute on Variable at AccountInvoiceStreamDetailDV.pcf: line 21, column 57
    function initialValue_1 () : gw.api.web.invoice.InvoiceDayChangeHelper {
      return invoiceStreamView!=null ? new gw.api.web.invoice.InvoiceDayChangeHelper(invoiceStreamView) : null
    }
    
    // 'mode' attribute on InputSetRef at AccountInvoiceStreamDetailDV.pcf: line 69, column 47
    function mode_36 () : java.lang.Object {
      return invoiceStreamView.Periodicity
    }
    
    // 'onPick' attribute on RangeInput (id=OverridingPaymentInstrument_Input) at AccountInvoiceStreamDetailDV.pcf: line 97, column 63
    function onPick_50 (PickedValue :  PaymentInstrument) : void {
      paymentInstrumentRange.addPaymentInstrument(invoiceStreamView.OverridingPaymentInstrument)
    }
    
    // 'valueRange' attribute on RangeInput (id=ExistingUnappliedFunds_Input) at AccountInvoiceStreamDetailDV.pcf: line 199, column 58
    function valueRange_119 () : java.lang.Object {
      return account.UnappliedFundsSortedByDisplayName
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingPaymentInstrument_Input) at AccountInvoiceStreamDetailDV.pcf: line 97, column 63
    function valueRange_54 () : java.lang.Object {
      return gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, gw.payment.PaymentInstrumentFilters.accountDetailsPaymentInstrumentFilter)
    }
    
    // 'valueRange' attribute on RangeInput (id=Periodicity_Input) at AccountInvoiceStreamDetailDV.pcf: line 35, column 41
    function valueRange_9 () : java.lang.Object {
      return gw.api.domain.invoice.InvoiceStreams.getInvoiceStreamPeriodicities()
    }
    
    // 'value' attribute on TextInput (id=SelectedUnappliedFunds_Input) at AccountInvoiceStreamDetailDV.pcf: line 179, column 168
    function valueRoot_102 () : java.lang.Object {
      return invoiceStreamView.InvoiceStream
    }
    
    // 'value' attribute on TextInput (id=InvoiceStreamDisplayName_Input) at AccountInvoiceStreamDetailDV.pcf: line 27, column 48
    function valueRoot_3 () : java.lang.Object {
      return invoiceStreamView
    }
    
    // 'value' attribute on TextInput (id=AccountPaymentInstrument_Input) at AccountInvoiceStreamDetailDV.pcf: line 78, column 47
    function valueRoot_38 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on TextInput (id=AccountInvoicingLeadTime_Input) at AccountInvoiceStreamDetailDV.pcf: line 112, column 40
    function valueRoot_61 () : java.lang.Object {
      return account.getInvoicingLeadTime(invoiceStreamView.PaymentInstrument)
    }
    
    // 'value' attribute on TextInput (id=SelectedUnappliedFunds_Input) at AccountInvoiceStreamDetailDV.pcf: line 179, column 168
    function value_101 () : entity.UnappliedFund {
      return invoiceStreamView.InvoiceStream.UnappliedFund
    }
    
    // 'value' attribute on TextInput (id=NewUnappliedFundsDescription_Input) at AccountInvoiceStreamDetailDV.pcf: line 187, column 57
    function value_107 () : java.lang.String {
      return invoiceStreamView.DescriptionOnNewUnappliedFund
    }
    
    // 'value' attribute on RangeInput (id=ExistingUnappliedFunds_Input) at AccountInvoiceStreamDetailDV.pcf: line 199, column 58
    function value_116 () : entity.UnappliedFund {
      return invoiceStreamView.UnappliedFund
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at AccountInvoiceStreamDetailDV.pcf: line 44, column 90
    function value_15 () : java.lang.String {
      return invoiceStreamView.Description
    }
    
    // 'value' attribute on TextInput (id=InvoiceStreamDisplayName_Input) at AccountInvoiceStreamDetailDV.pcf: line 27, column 48
    function value_2 () : java.lang.String {
      return invoiceStreamView.DisplayName
    }
    
    // 'value' attribute on TextInput (id=InvoiceDayLabel_Input) at AccountInvoiceStreamDetailDV.pcf: line 52, column 38
    function value_24 () : java.lang.Object {
      return null
    }
    
    // 'value' attribute on TextInput (id=AccountPaymentInstrument_Input) at AccountInvoiceStreamDetailDV.pcf: line 78, column 47
    function value_37 () : entity.PaymentInstrument {
      return account.DefaultPaymentInstrument
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverridePaymentInstrument_Input) at AccountInvoiceStreamDetailDV.pcf: line 85, column 46
    function value_41 () : java.lang.Boolean {
      return invoiceStreamView.OverridePaymentInstrument
    }
    
    // 'value' attribute on RangeInput (id=OverridingPaymentInstrument_Input) at AccountInvoiceStreamDetailDV.pcf: line 97, column 63
    function value_51 () : entity.PaymentInstrument {
      return invoiceStreamView.OverridingPaymentInstrument
    }
    
    // 'value' attribute on RangeInput (id=Periodicity_Input) at AccountInvoiceStreamDetailDV.pcf: line 35, column 41
    function value_6 () : typekey.Periodicity {
      return invoiceStreamView.Periodicity
    }
    
    // 'value' attribute on TextInput (id=AccountInvoicingLeadTime_Input) at AccountInvoiceStreamDetailDV.pcf: line 112, column 40
    function value_60 () : java.lang.Integer {
      return account.getInvoicingLeadTime(invoiceStreamView.PaymentInstrument).DayCount
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverrideInvoicingLeadTime_Input) at AccountInvoiceStreamDetailDV.pcf: line 119, column 46
    function value_64 () : java.lang.Boolean {
      return invoiceStreamView.OverrideInvoicingLeadTime
    }
    
    // 'value' attribute on TextInput (id=OverridingLeadTimeDayCount_Input) at AccountInvoiceStreamDetailDV.pcf: line 129, column 63
    function value_71 () : java.lang.Integer {
      return invoiceStreamView.OverridingLeadTimeDayCount
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=AccountBillDateOrDueDateBilling_Input) at AccountInvoiceStreamDetailDV.pcf: line 140, column 55
    function value_77 () : typekey.BillDateOrDueDateBilling {
      return account.BillDateOrDueDateBilling
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverrideBillDateOrDueDateBilling_Input) at AccountInvoiceStreamDetailDV.pcf: line 147, column 46
    function value_81 () : java.lang.Boolean {
      return invoiceStreamView.OverrideBillDateOrDueDateBilling
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=OverridingBillDateOrDueDateBilling_Input) at AccountInvoiceStreamDetailDV.pcf: line 157, column 70
    function value_88 () : typekey.BillDateOrDueDateBilling {
      return invoiceStreamView.OverridingBillDateOrDueDateBilling
    }
    
    // 'value' attribute on BooleanRadioInput (id=UnappliedFundsNewOrExisting_Input) at AccountInvoiceStreamDetailDV.pcf: line 171, column 55
    function value_95 () : java.lang.Boolean {
      return invoiceStreamView.IsUnappliedFundsNew
    }
    
    // 'valueRange' attribute on RangeInput (id=Periodicity_Input) at AccountInvoiceStreamDetailDV.pcf: line 35, column 41
    function verifyValueRangeIsAllowedType_10 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Periodicity_Input) at AccountInvoiceStreamDetailDV.pcf: line 35, column 41
    function verifyValueRangeIsAllowedType_10 ($$arg :  typekey.Periodicity[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ExistingUnappliedFunds_Input) at AccountInvoiceStreamDetailDV.pcf: line 199, column 58
    function verifyValueRangeIsAllowedType_120 ($$arg :  entity.UnappliedFund[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ExistingUnappliedFunds_Input) at AccountInvoiceStreamDetailDV.pcf: line 199, column 58
    function verifyValueRangeIsAllowedType_120 ($$arg :  gw.api.database.IQueryBeanResult<entity.UnappliedFund>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ExistingUnappliedFunds_Input) at AccountInvoiceStreamDetailDV.pcf: line 199, column 58
    function verifyValueRangeIsAllowedType_120 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingPaymentInstrument_Input) at AccountInvoiceStreamDetailDV.pcf: line 97, column 63
    function verifyValueRangeIsAllowedType_55 ($$arg :  entity.PaymentInstrument[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingPaymentInstrument_Input) at AccountInvoiceStreamDetailDV.pcf: line 97, column 63
    function verifyValueRangeIsAllowedType_55 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentInstrument>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingPaymentInstrument_Input) at AccountInvoiceStreamDetailDV.pcf: line 97, column 63
    function verifyValueRangeIsAllowedType_55 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Periodicity_Input) at AccountInvoiceStreamDetailDV.pcf: line 35, column 41
    function verifyValueRange_11 () : void {
      var __valueRangeArg = gw.api.domain.invoice.InvoiceStreams.getInvoiceStreamPeriodicities()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_10(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=ExistingUnappliedFunds_Input) at AccountInvoiceStreamDetailDV.pcf: line 199, column 58
    function verifyValueRange_121 () : void {
      var __valueRangeArg = account.UnappliedFundsSortedByDisplayName
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_120(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingPaymentInstrument_Input) at AccountInvoiceStreamDetailDV.pcf: line 97, column 63
    function verifyValueRange_56 () : void {
      var __valueRangeArg = gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, gw.payment.PaymentInstrumentFilters.accountDetailsPaymentInstrumentFilter)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_55(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=SelectedUnappliedFunds_Input) at AccountInvoiceStreamDetailDV.pcf: line 179, column 168
    function visible_100 () : java.lang.Boolean {
      return !invoiceStreamView.InvoiceStream.New || invoiceStreamView.IsUnappliedFundsNew || (invoiceStreamView.InvoiceStream.InvoicePayer as Account).ListBill
    }
    
    // 'visible' attribute on RangeInput (id=ExistingUnappliedFunds_Input) at AccountInvoiceStreamDetailDV.pcf: line 199, column 58
    function visible_114 () : java.lang.Boolean {
      return !invoiceStreamView.IsUnappliedFundsNew
    }
    
    // 'visible' attribute on TextInput (id=Description_Input) at AccountInvoiceStreamDetailDV.pcf: line 44, column 90
    function visible_14 () : java.lang.Boolean {
      return CurrentLocation.InEditMode || (invoiceStreamView.Description != null)
    }
    
    // 'visible' attribute on MenuItem (id=InvoiceDayChangerPopup) at AccountInvoiceStreamDetailDV.pcf: line 59, column 50
    function visible_21 () : java.lang.Boolean {
      return !CurrentLocation.InEditMode
    }
    
    // 'visible' attribute on BooleanRadioInput (id=OverridePaymentInstrument_Input) at AccountInvoiceStreamDetailDV.pcf: line 85, column 46
    function visible_40 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get invoiceDayChangeHelper () : gw.api.web.invoice.InvoiceDayChangeHelper {
      return getVariableValue("invoiceDayChangeHelper", 0) as gw.api.web.invoice.InvoiceDayChangeHelper
    }
    
    property set invoiceDayChangeHelper ($arg :  gw.api.web.invoice.InvoiceDayChangeHelper) {
      setVariableValue("invoiceDayChangeHelper", 0, $arg)
    }
    
    property get invoiceStreamView () : gw.api.web.invoice.InvoiceStreamView {
      return getRequireValue("invoiceStreamView", 0) as gw.api.web.invoice.InvoiceStreamView
    }
    
    property set invoiceStreamView ($arg :  gw.api.web.invoice.InvoiceStreamView) {
      setRequireValue("invoiceStreamView", 0, $arg)
    }
    
    property get paymentInstrumentRange () : gw.payment.PaymentInstrumentRange {
      return getVariableValue("paymentInstrumentRange", 0) as gw.payment.PaymentInstrumentRange
    }
    
    property set paymentInstrumentRange ($arg :  gw.payment.PaymentInstrumentRange) {
      setVariableValue("paymentInstrumentRange", 0, $arg)
    }
    
    
  }
  
  
}