package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanLowBalanceInvoicesInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BillingPlanLowBalanceInvoicesInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanLowBalanceInvoicesInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BillingPlanLowBalanceInvoicesInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=LowBalanceThresholdDefaults) at BillingPlanLowBalanceInvoicesInputSet.default.pcf: line 24, column 41
    function def_onEnter_7 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(billingPlan, BillingPlan#LowBalanceThresholdDefaults.PropertyInfo, DisplayKey.get("Web.BillingPlanDetailDV.LowBalanceThreshold"), billingPlan.SuppressLowBalInvoices, false, null)
    }
    
    // 'def' attribute on InputSetRef (id=LowBalanceThresholdDefaults) at BillingPlanLowBalanceInvoicesInputSet.default.pcf: line 24, column 41
    function def_refreshVariables_8 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(billingPlan, BillingPlan#LowBalanceThresholdDefaults.PropertyInfo, DisplayKey.get("Web.BillingPlanDetailDV.LowBalanceThreshold"), billingPlan.SuppressLowBalInvoices, false, null)
    }
    
    // 'value' attribute on TypeKeyInput (id=LowBalanceMethod_Input) at BillingPlanLowBalanceInvoicesInputSet.default.pcf: line 31, column 45
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.LowBalanceMethod = (__VALUE_TO_SET as typekey.LowBalanceMethod)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SuppressLowBalInvoices_Input) at BillingPlanLowBalanceInvoicesInputSet.default.pcf: line 18, column 50
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.SuppressLowBalInvoices = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'editable' attribute on BooleanRadioInput (id=SuppressLowBalInvoices_Input) at BillingPlanLowBalanceInvoicesInputSet.default.pcf: line 18, column 50
    function editable_0 () : java.lang.Boolean {
      return planNotInUse
    }
    
    // 'editable' attribute on TypeKeyInput (id=LowBalanceMethod_Input) at BillingPlanLowBalanceInvoicesInputSet.default.pcf: line 31, column 45
    function editable_9 () : java.lang.Boolean {
      return planNotInUse and billingPlan.SuppressLowBalInvoices
    }
    
    // 'value' attribute on BooleanRadioInput (id=SuppressLowBalInvoices_Input) at BillingPlanLowBalanceInvoicesInputSet.default.pcf: line 18, column 50
    function valueRoot_3 () : java.lang.Object {
      return billingPlan
    }
    
    // 'value' attribute on BooleanRadioInput (id=SuppressLowBalInvoices_Input) at BillingPlanLowBalanceInvoicesInputSet.default.pcf: line 18, column 50
    function value_1 () : java.lang.Boolean {
      return billingPlan.SuppressLowBalInvoices
    }
    
    // 'value' attribute on TypeKeyInput (id=LowBalanceMethod_Input) at BillingPlanLowBalanceInvoicesInputSet.default.pcf: line 31, column 45
    function value_11 () : typekey.LowBalanceMethod {
      return billingPlan.LowBalanceMethod
    }
    
    property get billingPlan () : BillingPlan {
      return getRequireValue("billingPlan", 0) as BillingPlan
    }
    
    property set billingPlan ($arg :  BillingPlan) {
      setRequireValue("billingPlan", 0, $arg)
    }
    
    property get planNotInUse () : boolean {
      return getRequireValue("planNotInUse", 0) as java.lang.Boolean
    }
    
    property set planNotInUse ($arg :  boolean) {
      setRequireValue("planNotInUse", 0, $arg)
    }
    
    
  }
  
  
}