package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlanDetailPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionPlanDetailPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlanDetailPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionPlanDetailPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at CommissionPlanDetailPanelSet.pcf: line 42, column 60
    function def_onEnter_20 (def :  pcf.PlanMultiCurrencyInputSet) : void {
      def.onEnter(commissionPlan)
    }
    
    // 'def' attribute on PanelRef at CommissionPlanDetailPanelSet.pcf: line 68, column 44
    function def_onEnter_31 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(commissionPlan, { "Name"}, { DisplayKey.get("Web.CommissionPlan.Name") })
    }
    
    // 'def' attribute on InputSetRef at CommissionPlanDetailPanelSet.pcf: line 42, column 60
    function def_refreshVariables_21 (def :  pcf.PlanMultiCurrencyInputSet) : void {
      def.refreshVariables(commissionPlan)
    }
    
    // 'def' attribute on PanelRef at CommissionPlanDetailPanelSet.pcf: line 68, column 44
    function def_refreshVariables_32 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(commissionPlan, { "Name"}, { DisplayKey.get("Web.CommissionPlan.Name") })
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at CommissionPlanDetailPanelSet.pcf: line 34, column 49
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      commissionPlan.EffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at CommissionPlanDetailPanelSet.pcf: line 40, column 50
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      commissionPlan.ExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at CommissionPlanDetailPanelSet.pcf: line 28, column 40
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      commissionPlan.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextInput (id=Name_Input) at CommissionPlanDetailPanelSet.pcf: line 28, column 40
    function editable_2 () : java.lang.Boolean {
      return planNotInUse
    }
    
    // 'initialValue' attribute on Variable at CommissionPlanDetailPanelSet.pcf: line 13, column 23
    function initialValue_0 () : Boolean {
      return not commissionPlan.InUse
    }
    
    // 'initialValue' attribute on Variable at CommissionPlanDetailPanelSet.pcf: line 17, column 23
    function initialValue_1 () : boolean {
      return gw.api.util.LocaleUtil.getAllLanguages().Count > 1
    }
    
    // 'validationExpression' attribute on DateInput (id=ExpirationDate_Input) at CommissionPlanDetailPanelSet.pcf: line 40, column 50
    function validationExpression_14 () : java.lang.Object {
      return commissionPlan.hasValidExpirationDate() ? null : DisplayKey.get("Web.Plan.ExpirationDate.ValidationError")
    }
    
    // 'validationExpression' attribute on InputIterator (id=Tiers) at CommissionPlanDetailPanelSet.pcf: line 54, column 70
    function validationExpression_22 () : java.lang.Object {
      return commissionPlan.hasAllowedTier() ? null : DisplayKey.get("Web.CommissionPlan.NoAllowedTiersFound")
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at CommissionPlanDetailPanelSet.pcf: line 28, column 40
    function valueRoot_5 () : java.lang.Object {
      return commissionPlan
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at CommissionPlanDetailPanelSet.pcf: line 40, column 50
    function value_15 () : java.util.Date {
      return commissionPlan.ExpirationDate
    }
    
    // 'value' attribute on InputIterator (id=Tiers) at CommissionPlanDetailPanelSet.pcf: line 54, column 70
    function value_29 () : gw.api.domain.commission.AllowedProducerTier[] {
      return commissionPlan.getProducerTiers()
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at CommissionPlanDetailPanelSet.pcf: line 28, column 40
    function value_3 () : java.lang.String {
      return commissionPlan.Name
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at CommissionPlanDetailPanelSet.pcf: line 34, column 49
    function value_9 () : java.util.Date {
      return commissionPlan.EffectiveDate
    }
    
    property get commissionPlan () : CommissionPlan {
      return getRequireValue("commissionPlan", 0) as CommissionPlan
    }
    
    property set commissionPlan ($arg :  CommissionPlan) {
      setRequireValue("commissionPlan", 0, $arg)
    }
    
    property get hasMultipleLanguages () : boolean {
      return getVariableValue("hasMultipleLanguages", 0) as java.lang.Boolean
    }
    
    property set hasMultipleLanguages ($arg :  boolean) {
      setVariableValue("hasMultipleLanguages", 0, $arg)
    }
    
    property get planNotInUse () : Boolean {
      return getVariableValue("planNotInUse", 0) as Boolean
    }
    
    property set planNotInUse ($arg :  Boolean) {
      setVariableValue("planNotInUse", 0, $arg)
    }
    
    function addSubPlan() : CondCmsnSubPlan {
            var subPlan = commissionPlan.addSubPlan()
            // Init defaults
            subPlan.AllLOBCodes = true
            subPlan.AllTerms = true
            subPlan.getRenewalRange(0, -1).Covered = true
            subPlan.AssignedRisk = AssignedRiskRestriction.TC_ALL
            subPlan.AllSegments = true
            subPlan.AllEvaluations = true
            subPlan.AllJurisdictions = true
            subPlan.AllUWCompanies = true
            return subPlan
          }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlanDetailPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=SubPlansLV_MoveUp) at CommissionPlanDetailPanelSet.pcf: line 118, column 109
    function action_43 () : void {
      (commissionSubPlan as CondCmsnSubPlan).changePriority(-1)
    }
    
    // 'action' attribute on Link (id=SubPlansLV_MoveDown) at CommissionPlanDetailPanelSet.pcf: line 127, column 142
    function action_45 () : void {
      (commissionSubPlan as CondCmsnSubPlan).changePriority(1)
    }
    
    // 'condition' attribute on ToolbarFlag at CommissionPlanDetailPanelSet.pcf: line 93, column 33
    function condition_35 () : java.lang.Boolean {
      return commissionSubPlan typeis CondCmsnSubPlan and (planNotInUse or commissionSubPlan.New)
    }
    
    // 'value' attribute on TextCell (id=SubPlansLV_Priority_Cell) at CommissionPlanDetailPanelSet.pcf: line 101, column 31
    function valueRoot_37 () : java.lang.Object {
      return commissionSubPlan
    }
    
    // 'value' attribute on TextCell (id=SubPlansLV_Priority_Cell) at CommissionPlanDetailPanelSet.pcf: line 101, column 31
    function value_36 () : java.lang.Integer {
      return commissionSubPlan.Priority
    }
    
    // 'value' attribute on TextCell (id=SubPlansLV_SubplanName_Cell) at CommissionPlanDetailPanelSet.pcf: line 106, column 49
    function value_39 () : java.lang.String {
      return commissionSubPlan.Name
    }
    
    // 'visible' attribute on Link (id=SubPlansLV_MoveUp) at CommissionPlanDetailPanelSet.pcf: line 118, column 109
    function visible_42 () : java.lang.Boolean {
      return commissionSubPlan typeis CondCmsnSubPlan and commissionSubPlan.Priority > 1
    }
    
    // 'visible' attribute on Link (id=SubPlansLV_MoveDown) at CommissionPlanDetailPanelSet.pcf: line 127, column 142
    function visible_44 () : java.lang.Boolean {
      return commissionSubPlan typeis CondCmsnSubPlan and commissionSubPlan.Priority < commissionPlan.SubPlans.length - 1
    }
    
    property get commissionSubPlan () : entity.CommissionSubPlan {
      return getIteratedValue(2) as entity.CommissionSubPlan
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlanDetailPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CommissionPlanDetailPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on CheckBoxInput (id=Tier_Input) at CommissionPlanDetailPanelSet.pcf: line 59, column 43
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      producerTier.Allowed = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'label' attribute on CheckBoxInput (id=Tier_Input) at CommissionPlanDetailPanelSet.pcf: line 59, column 43
    function label_23 () : java.lang.Object {
      return producerTier
    }
    
    // 'value' attribute on CheckBoxInput (id=Tier_Input) at CommissionPlanDetailPanelSet.pcf: line 59, column 43
    function value_24 () : java.lang.Boolean {
      return producerTier.Allowed
    }
    
    property get producerTier () : gw.api.domain.commission.AllowedProducerTier {
      return getIteratedValue(1) as gw.api.domain.commission.AllowedProducerTier
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlanDetailPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends CommissionPlanDetailPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at CommissionPlanDetailPanelSet.pcf: line 133, column 61
    function def_onEnter_49 (def :  pcf.CommissionSubPlanDetailCV) : void {
      def.onEnter(selectedSubPlan)
    }
    
    // 'def' attribute on PanelRef at CommissionPlanDetailPanelSet.pcf: line 133, column 61
    function def_refreshVariables_50 (def :  pcf.CommissionSubPlanDetailCV) : void {
      def.refreshVariables(selectedSubPlan)
    }
    
    // 'value' attribute on TextCell (id=SubPlansLV_Priority_Cell) at CommissionPlanDetailPanelSet.pcf: line 101, column 31
    function sortValue_33 (commissionSubPlan :  entity.CommissionSubPlan) : java.lang.Object {
      return commissionSubPlan.Priority
    }
    
    // 'value' attribute on TextCell (id=SubPlansLV_SubplanName_Cell) at CommissionPlanDetailPanelSet.pcf: line 106, column 49
    function sortValue_34 (commissionSubPlan :  entity.CommissionSubPlan) : java.lang.Object {
      return commissionSubPlan.Name
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at CommissionPlanDetailPanelSet.pcf: line 90, column 52
    function toCreateAndAdd_46 () : entity.CommissionSubPlan {
      return addSubPlan()
    }
    
    // 'toRemove' attribute on RowIterator at CommissionPlanDetailPanelSet.pcf: line 90, column 52
    function toRemove_47 (commissionSubPlan :  entity.CommissionSubPlan) : void {
      commissionPlan.removeSubPlan(commissionSubPlan as CondCmsnSubPlan)
    }
    
    // 'value' attribute on RowIterator at CommissionPlanDetailPanelSet.pcf: line 90, column 52
    function value_48 () : entity.CommissionSubPlan[] {
      return commissionPlan.SubPlans
    }
    
    property get selectedSubPlan () : CommissionSubPlan {
      return getCurrentSelection(1) as CommissionSubPlan
    }
    
    property set selectedSubPlan ($arg :  CommissionSubPlan) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  
}