package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'action' attribute on ForwardCondition at PolicyDetailForward.pcf: line 15, column 49
    function action_0 () : void {
      PolicyOverview.go(policyPeriod)
    }
    
    // 'action' attribute on ForwardCondition at PolicyDetailForward.pcf: line 15, column 49
    function action_dest_1 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(policyPeriod)
    }
    
    // 'canVisit' attribute on Forward (id=PolicyDetailForward) at PolicyDetailForward.pcf: line 8, column 30
    static function canVisit_2 (policyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcysummview and policyPeriod.ViewableByCurrentUser
    }
    
    override property get CurrentLocation () : pcf.PolicyDetailForward {
      return super.CurrentLocation as pcf.PolicyDetailForward
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}