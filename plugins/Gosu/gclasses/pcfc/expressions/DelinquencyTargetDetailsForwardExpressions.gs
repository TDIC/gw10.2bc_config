package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyTargetDetailsForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyTargetDetailsForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyTargetDetailsForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyTargetDetailsForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (target :  gw.api.domain.delinquency.DelinquencyTarget) : int {
      return 0
    }
    
    // 'action' attribute on ForwardCondition at DelinquencyTargetDetailsForward.pcf: line 14, column 47
    function action_0 () : void {
      PolicyOverview.go( target as PolicyPeriod )
    }
    
    // 'action' attribute on ForwardCondition at DelinquencyTargetDetailsForward.pcf: line 17, column 42
    function action_3 () : void {
      AccountOverview.go( target as Account )
    }
    
    // 'action' attribute on ForwardCondition at DelinquencyTargetDetailsForward.pcf: line 14, column 47
    function action_dest_1 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination( target as PolicyPeriod )
    }
    
    // 'action' attribute on ForwardCondition at DelinquencyTargetDetailsForward.pcf: line 17, column 42
    function action_dest_4 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination( target as Account )
    }
    
    // 'condition' attribute on ForwardCondition at DelinquencyTargetDetailsForward.pcf: line 14, column 47
    function condition_2 () : java.lang.Boolean {
      return target typeis PolicyPeriod
    }
    
    // 'condition' attribute on ForwardCondition at DelinquencyTargetDetailsForward.pcf: line 17, column 42
    function condition_5 () : java.lang.Boolean {
      return target typeis Account
    }
    
    override property get CurrentLocation () : pcf.DelinquencyTargetDetailsForward {
      return super.CurrentLocation as pcf.DelinquencyTargetDetailsForward
    }
    
    property get target () : gw.api.domain.delinquency.DelinquencyTarget {
      return getVariableValue("target", 0) as gw.api.domain.delinquency.DelinquencyTarget
    }
    
    property set target ($arg :  gw.api.domain.delinquency.DelinquencyTarget) {
      setVariableValue("target", 0, $arg)
    }
    
    
  }
  
  
}