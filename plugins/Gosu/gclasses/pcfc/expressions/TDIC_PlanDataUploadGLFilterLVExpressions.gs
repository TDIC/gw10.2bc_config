package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/TDIC_PlanDataUploadGLFilterLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_PlanDataUploadGLFilterLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/TDIC_PlanDataUploadGLFilterLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_PlanDataUploadGLFilterLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at TDIC_PlanDataUploadGLFilterLV.pcf: line 17, column 96
    function highlighted_9 () : java.lang.Boolean {
      return (glFilter.Error or glFilter.Skipped)  && processor.LoadCompleted
    }
    
    // 'value' attribute on TypeKeyCell (id=transactionType_Cell) at TDIC_PlanDataUploadGLFilterLV.pcf: line 28, column 44
    function valueRoot_7 () : java.lang.Object {
      return glFilter
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at TDIC_PlanDataUploadGLFilterLV.pcf: line 23, column 46
    function value_3 () : java.lang.String {
      return processor.getLoadStatus(glFilter)
    }
    
    // 'value' attribute on TypeKeyCell (id=transactionType_Cell) at TDIC_PlanDataUploadGLFilterLV.pcf: line 28, column 44
    function value_6 () : typekey.Transaction {
      return glFilter.TransactionType
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at TDIC_PlanDataUploadGLFilterLV.pcf: line 23, column 46
    function visible_4 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get glFilter () : tdic.util.dataloader.data.plandata.GLTransactionFilterData {
      return getIteratedValue(1) as tdic.util.dataloader.data.plandata.GLTransactionFilterData
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/TDIC_PlanDataUploadGLFilterLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_PlanDataUploadGLFilterLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at TDIC_PlanDataUploadGLFilterLV.pcf: line 23, column 46
    function sortValue_0 (glFilter :  tdic.util.dataloader.data.plandata.GLTransactionFilterData) : java.lang.Object {
      return processor.getLoadStatus(glFilter)
    }
    
    // 'value' attribute on TypeKeyCell (id=transactionType_Cell) at TDIC_PlanDataUploadGLFilterLV.pcf: line 28, column 44
    function sortValue_2 (glFilter :  tdic.util.dataloader.data.plandata.GLTransactionFilterData) : java.lang.Object {
      return glFilter.TransactionType
    }
    
    // 'value' attribute on RowIterator (id=GLFilter) at TDIC_PlanDataUploadGLFilterLV.pcf: line 15, column 105
    function value_10 () : java.util.ArrayList<tdic.util.dataloader.data.plandata.GLTransactionFilterData> {
      return processor.GLTransactionFilterArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at TDIC_PlanDataUploadGLFilterLV.pcf: line 23, column 46
    function visible_1 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}