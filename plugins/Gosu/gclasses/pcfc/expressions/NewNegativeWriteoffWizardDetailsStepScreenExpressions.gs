package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.web.accounting.UIWriteOffCreation
uses org.apache.velocity.util.introspection.UberspectImpl
@javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffWizardDetailsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewNegativeWriteoffWizardDetailsStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffWizardDetailsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewNegativeWriteoffWizardDetailsStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 47, column 45
    function def_onEnter_13 (def :  pcf.MinimalTAccountOwnerDetailsDV_accountdefault) : void {
      def.onEnter(taccountOwner, unappliedFund)
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 47, column 45
    function def_onEnter_15 (def :  pcf.MinimalTAccountOwnerDetailsDV_accountdesignated) : void {
      def.onEnter(taccountOwner, unappliedFund)
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 47, column 45
    function def_onEnter_17 (def :  pcf.MinimalTAccountOwnerDetailsDV_policy) : void {
      def.onEnter(taccountOwner, unappliedFund)
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 47, column 45
    function def_onEnter_19 (def :  pcf.MinimalTAccountOwnerDetailsDV_producer) : void {
      def.onEnter(taccountOwner, unappliedFund)
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 49, column 55
    function def_onEnter_22 (def :  pcf.NewNegativeWriteoffDetailsDV) : void {
      def.onEnter(uiWriteoff)
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 47, column 45
    function def_refreshVariables_14 (def :  pcf.MinimalTAccountOwnerDetailsDV_accountdefault) : void {
      def.refreshVariables(taccountOwner, unappliedFund)
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 47, column 45
    function def_refreshVariables_16 (def :  pcf.MinimalTAccountOwnerDetailsDV_accountdesignated) : void {
      def.refreshVariables(taccountOwner, unappliedFund)
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 47, column 45
    function def_refreshVariables_18 (def :  pcf.MinimalTAccountOwnerDetailsDV_policy) : void {
      def.refreshVariables(taccountOwner, unappliedFund)
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 47, column 45
    function def_refreshVariables_20 (def :  pcf.MinimalTAccountOwnerDetailsDV_producer) : void {
      def.refreshVariables(taccountOwner, unappliedFund)
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 49, column 55
    function def_refreshVariables_23 (def :  pcf.NewNegativeWriteoffDetailsDV) : void {
      def.refreshVariables(uiWriteoff)
    }
    
    // 'value' attribute on RangeInput (id=UnappliedFund_Input) at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 38, column 97
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      unappliedFund = (__VALUE_TO_SET as entity.UnappliedFund)
    }
    
    // 'initialValue' attribute on Variable at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 13, column 30
    function initialValue_0 () : java.util.Date {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'initialValue' attribute on Variable at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 17, column 29
    function initialValue_1 () : UnappliedFund {
      return (uiWriteoff.WriteOff typeis AcctNegativeWriteoff) ? uiWriteoff.WriteOff.UnappliedFund : null 
    }
    
    // 'initialValue' attribute on Variable at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 21, column 29
    function initialValue_2 () : TAccountOwner {
      return uiWriteoff.WriteOff.TAccountOwner
    }
    
    // 'mode' attribute on PanelRef at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 47, column 45
    function mode_21 () : java.lang.Object {
      return getTAccountOwnerDetailsMode()
    }
    
    // 'onChange' attribute on PostOnChange at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 40, column 51
    function onChange_3 () : void {
      updateTargetUnapplied()
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFund_Input) at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 38, column 97
    function valueRange_7 () : java.lang.Object {
      return (taccountOwner as Account).UnappliedFundsSortedByDisplayName
    }
    
    // 'value' attribute on RangeInput (id=UnappliedFund_Input) at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 38, column 97
    function value_5 () : entity.UnappliedFund {
      return unappliedFund
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFund_Input) at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 38, column 97
    function verifyValueRangeIsAllowedType_8 ($$arg :  entity.UnappliedFund[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFund_Input) at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 38, column 97
    function verifyValueRangeIsAllowedType_8 ($$arg :  gw.api.database.IQueryBeanResult<entity.UnappliedFund>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFund_Input) at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 38, column 97
    function verifyValueRangeIsAllowedType_8 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFund_Input) at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 38, column 97
    function verifyValueRange_9 () : void {
      var __valueRangeArg = (taccountOwner as Account).UnappliedFundsSortedByDisplayName
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_8(__valueRangeArg)
    }
    
    // 'visible' attribute on DetailViewPanel at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 26, column 131
    function visible_12 () : java.lang.Boolean {
      return uiWriteoff.WriteOff.TAccountOwner typeis Account and uiWriteoff.WriteOff.TAccountOwner.HasDesignatedUnappliedFund
    }
    
    // 'visible' attribute on RangeInput (id=UnappliedFund_Input) at NewNegativeWriteoffWizardDetailsStepScreen.pcf: line 38, column 97
    function visible_4 () : java.lang.Boolean {
      return taccountOwner typeis Account and taccountOwner.HasDesignatedUnappliedFund
    }
    
    property get taccountOwner () : TAccountOwner {
      return getVariableValue("taccountOwner", 0) as TAccountOwner
    }
    
    property set taccountOwner ($arg :  TAccountOwner) {
      setVariableValue("taccountOwner", 0, $arg)
    }
    
    property get today () : java.util.Date {
      return getVariableValue("today", 0) as java.util.Date
    }
    
    property set today ($arg :  java.util.Date) {
      setVariableValue("today", 0, $arg)
    }
    
    property get uiWriteoff () : gw.api.web.accounting.UIWriteOffCreation {
      return getRequireValue("uiWriteoff", 0) as gw.api.web.accounting.UIWriteOffCreation
    }
    
    property set uiWriteoff ($arg :  gw.api.web.accounting.UIWriteOffCreation) {
      setRequireValue("uiWriteoff", 0, $arg)
    }
    
    property get unappliedFund () : UnappliedFund {
      return getVariableValue("unappliedFund", 0) as UnappliedFund
    }
    
    property set unappliedFund ($arg :  UnappliedFund) {
      setVariableValue("unappliedFund", 0, $arg)
    }
    
    
    function getTAccountOwnerDetailsMode() : String {
      if (taccountOwner typeis Producer) {
        return "producer"
      } else if (taccountOwner typeis Account) {
        if (unappliedFund.DefaultForAccount) {
          return "accountdefault"
        } else {
          return "accountdesignated"
        }
      } else {
        throw new java.lang.UnsupportedOperationException("This t-account owner is unsupported for negative write-offs.")
      }  
    }
    
    function updateTargetUnapplied() {
        if (taccountOwner typeis Account and unappliedFund == null) {
          unappliedFund = taccountOwner.DefaultUnappliedFund
        } 
        uiWriteoff.setUnappliedFund(unappliedFund)
    /**
         * US102 
         * 09/19/2014 Hermia Kho
         * 
         * Comment out the value in u1Writeoff.UsefulAmount so that the field defaults to Null instead
         */
      
     // uiWriteoff.UseFullAmount = uiWriteoff.FullAmount.IsPositive
      if (not uiWriteoff.UseFullAmount) {
        uiWriteoff.Amount = null
      }
    }
    
    
  }
  
  
}