package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/ActivityDetailForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ActivityDetailForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/activity/ActivityDetailForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ActivityDetailForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (activity :  Activity) : int {
      return 0
    }
    
    // 'action' attribute on Forward (id=ActivityDetailForward) at ActivityDetailForward.pcf: line 8, column 87
    function action_0 () : void {
      ActivityDetailWorksheet.goInWorkspace(activity)
    }
    
    // 'action' attribute on Forward (id=ActivityDetailForward) at ActivityDetailForward.pcf: line 8, column 87
    function action_dest_1 () : pcf.api.Destination {
      return pcf.ActivityDetailWorksheet.createDestination(activity)
    }
    
    // 'onBeforeForward' attribute on Forward (id=ActivityDetailForward) at ActivityDetailForward.pcf: line 8, column 87
    function onBeforeForward_2 () : void {
      gw.api.web.activity.ActivityUtil.markActivityAsViewed(activity)
    }
    
    override property get CurrentLocation () : pcf.ActivityDetailForward {
      return super.CurrentLocation as pcf.ActivityDetailForward
    }
    
    property get activity () : Activity {
      return getVariableValue("activity", 0) as Activity
    }
    
    property set activity ($arg :  Activity) {
      setVariableValue("activity", 0, $arg)
    }
    
    
  }
  
  
}