package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/payment/PaymentPlanDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentPlanDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/payment/PaymentPlanDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentPlanDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (paymentPlan :  PaymentPlan) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=PaymentPlanDetail) at PaymentPlanDetail.pcf: line 10, column 87
    function canEdit_2 () : java.lang.Boolean {
      return perm.System.pmntplanedit
    }
    
    // 'def' attribute on ScreenRef at PaymentPlanDetail.pcf: line 21, column 60
    function def_onEnter_0 (def :  pcf.PaymentPlanDetailScreen) : void {
      def.onEnter(paymentPlan, isClone)
    }
    
    // 'def' attribute on ScreenRef at PaymentPlanDetail.pcf: line 21, column 60
    function def_refreshVariables_1 (def :  pcf.PaymentPlanDetailScreen) : void {
      def.refreshVariables(paymentPlan, isClone)
    }
    
    // 'parent' attribute on Page (id=PaymentPlanDetail) at PaymentPlanDetail.pcf: line 10, column 87
    static function parent_3 (paymentPlan :  PaymentPlan) : pcf.api.Destination {
      return pcf.PaymentPlans.createDestination()
    }
    
    // 'title' attribute on Page (id=PaymentPlanDetail) at PaymentPlanDetail.pcf: line 10, column 87
    static function title_4 (paymentPlan :  PaymentPlan) : java.lang.Object {
      return DisplayKey.get("Web.PaymentPlanDetail.Title", paymentPlan.Name)
    }
    
    override property get CurrentLocation () : pcf.PaymentPlanDetail {
      return super.CurrentLocation as pcf.PaymentPlanDetail
    }
    
    property get isClone () : boolean {
      return getVariableValue("isClone", 0) as java.lang.Boolean
    }
    
    property set isClone ($arg :  boolean) {
      setVariableValue("isClone", 0, $arg)
    }
    
    property get paymentPlan () : PaymentPlan {
      return getVariableValue("paymentPlan", 0) as PaymentPlan
    }
    
    property set paymentPlan ($arg :  PaymentPlan) {
      setVariableValue("paymentPlan", 0, $arg)
    }
    
    
  }
  
  
}