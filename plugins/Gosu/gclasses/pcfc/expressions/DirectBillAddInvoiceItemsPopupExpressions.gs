package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/DirectBillAddInvoiceItemsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DirectBillAddInvoiceItemsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/DirectBillAddInvoiceItemsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DirectBillAddInvoiceItemsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (directBillPaymentView :  gw.api.web.payment.DirectBillPaymentView, amountAvailableToDistribute :  gw.pl.currency.MonetaryAmount) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=cancel) at DirectBillAddInvoiceItemsPopup.pcf: line 27, column 62
    function action_1 () : void {
      CurrentLocation.cancel()
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=SelectInvoiceItems) at DirectBillAddInvoiceItemsPopup.pcf: line 23, column 97
    function allCheckedRowsAction_0 (CheckedValues :  entity.InvoiceItem[], CheckedValuesErrors :  java.util.Map) : void {
      directBillPaymentView.manuallyAddItems( CheckedValues ); recalculateDistribution(); CurrentLocation.commit()
    }
    
    // 'def' attribute on PanelRef at DirectBillAddInvoiceItemsPopup.pcf: line 30, column 116
    function def_onEnter_2 (def :  pcf.InvoiceItemSearchPanelSet) : void {
      def.onEnter(directBillPaymentView.Payment, directBillPaymentView.TargetAccount, null)
    }
    
    // 'def' attribute on PanelRef at DirectBillAddInvoiceItemsPopup.pcf: line 30, column 116
    function def_refreshVariables_3 (def :  pcf.InvoiceItemSearchPanelSet) : void {
      def.refreshVariables(directBillPaymentView.Payment, directBillPaymentView.TargetAccount, null)
    }
    
    override property get CurrentLocation () : pcf.DirectBillAddInvoiceItemsPopup {
      return super.CurrentLocation as pcf.DirectBillAddInvoiceItemsPopup
    }
    
    property get amountAvailableToDistribute () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("amountAvailableToDistribute", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set amountAvailableToDistribute ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("amountAvailableToDistribute", 0, $arg)
    }
    
    property get directBillPaymentView () : gw.api.web.payment.DirectBillPaymentView {
      return getVariableValue("directBillPaymentView", 0) as gw.api.web.payment.DirectBillPaymentView
    }
    
    property set directBillPaymentView ($arg :  gw.api.web.payment.DirectBillPaymentView) {
      setVariableValue("directBillPaymentView", 0, $arg)
    }
    
    public function recalculateDistribution() {
      if (directBillPaymentView.MoneyReceived.Amount == null) return;
      directBillPaymentView.recalculateDistribution(amountAvailableToDistribute)
    }
    
    
  }
  
  
}