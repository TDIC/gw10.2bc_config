package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/systemAdmin/Acct360EditRowPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class Acct360EditRowPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/acct360/systemAdmin/Acct360EditRowPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Acct360EditRowPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (acctBalanceRow :  AccountBalanceTxn_ext) : int {
      return 0
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=balanceamount_Input) at Acct360EditRowPopup.pcf: line 50, column 51
    function currency_20 () : typekey.Currency {
      return acctBalanceRow.Account.Currency
    }
    
    // 'value' attribute on TextInput (id=refNumber_Input) at Acct360EditRowPopup.pcf: line 44, column 53
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      acctBalanceRow.ReferenceNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=balanceamount_Input) at Acct360EditRowPopup.pcf: line 50, column 51
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      acctBalanceRow.BalanceAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=dueamount_Input) at Acct360EditRowPopup.pcf: line 56, column 47
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      acctBalanceRow.DueAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=paidamount_Input) at Acct360EditRowPopup.pcf: line 62, column 48
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      acctBalanceRow.PaidAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // EditButtons at Acct360EditRowPopup.pcf: line 17, column 23
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'value' attribute on TextInput (id=subtype_Input) at Acct360EditRowPopup.pcf: line 24, column 57
    function valueRoot_2 () : java.lang.Object {
      return acctBalanceRow.Subtype
    }
    
    // 'value' attribute on DateInput (id=transactiondate_Input) at Acct360EditRowPopup.pcf: line 29, column 53
    function valueRoot_5 () : java.lang.Object {
      return acctBalanceRow
    }
    
    // 'value' attribute on TextInput (id=subtype_Input) at Acct360EditRowPopup.pcf: line 24, column 57
    function value_1 () : java.lang.String {
      return acctBalanceRow.Subtype.DisplayName
    }
    
    // 'value' attribute on TextInput (id=policyperiod_Input) at Acct360EditRowPopup.pcf: line 39, column 39
    function value_10 () : PolicyPeriod {
      return acctBalanceRow.PolicyPeriod
    }
    
    // 'value' attribute on TextInput (id=refNumber_Input) at Acct360EditRowPopup.pcf: line 44, column 53
    function value_13 () : java.lang.String {
      return acctBalanceRow.ReferenceNumber
    }
    
    // 'value' attribute on MonetaryAmountInput (id=balanceamount_Input) at Acct360EditRowPopup.pcf: line 50, column 51
    function value_17 () : gw.pl.currency.MonetaryAmount {
      return acctBalanceRow.BalanceAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=dueamount_Input) at Acct360EditRowPopup.pcf: line 56, column 47
    function value_22 () : gw.pl.currency.MonetaryAmount {
      return acctBalanceRow.DueAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=paidamount_Input) at Acct360EditRowPopup.pcf: line 62, column 48
    function value_27 () : gw.pl.currency.MonetaryAmount {
      return acctBalanceRow.PaidAmount
    }
    
    // 'value' attribute on DateInput (id=transactiondate_Input) at Acct360EditRowPopup.pcf: line 29, column 53
    function value_4 () : java.util.Date {
      return acctBalanceRow.TransactionDate
    }
    
    // 'value' attribute on TextInput (id=account_Input) at Acct360EditRowPopup.pcf: line 34, column 34
    function value_7 () : Account {
      return acctBalanceRow.Account
    }
    
    override property get CurrentLocation () : pcf.Acct360EditRowPopup {
      return super.CurrentLocation as pcf.Acct360EditRowPopup
    }
    
    property get acctBalanceRow () : AccountBalanceTxn_ext {
      return getVariableValue("acctBalanceRow", 0) as AccountBalanceTxn_ext
    }
    
    property set acctBalanceRow ($arg :  AccountBalanceTxn_ext) {
      setVariableValue("acctBalanceRow", 0, $arg)
    }
    
    
  }
  
  
}