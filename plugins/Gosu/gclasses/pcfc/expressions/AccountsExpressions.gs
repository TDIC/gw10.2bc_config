package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/Accounts.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountsExpressions {
  @javax.annotation.Generated("config/web/pcf/account/Accounts.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=Accounts) at Accounts.pcf: line 8, column 60
    static function canVisit_2 () : java.lang.Boolean {
      return perm.System.accttabview
    }
    
    // 'def' attribute on ScreenRef at Accounts.pcf: line 10, column 46
    function def_onEnter_0 (def :  pcf.AccountSearchScreen) : void {
      def.onEnter(true, true)
    }
    
    // 'def' attribute on ScreenRef at Accounts.pcf: line 10, column 46
    function def_refreshVariables_1 (def :  pcf.AccountSearchScreen) : void {
      def.refreshVariables(true, true)
    }
    
    // Page (id=Accounts) at Accounts.pcf: line 8, column 60
    static function parent_3 () : pcf.api.Destination {
      return pcf.AccountsGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.Accounts {
      return super.CurrentLocation as pcf.Accounts
    }
    
    
  }
  
  
}