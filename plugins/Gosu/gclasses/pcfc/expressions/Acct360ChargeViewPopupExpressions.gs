package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/Acct360ChargeViewPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class Acct360ChargeViewPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/acct360/Acct360ChargeViewPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Acct360ChargeViewPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (billInstruction :  BillingInstruction) : int {
      return 0
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod) : int {
      return 1
    }
    
    // 'initialValue' attribute on Variable at Acct360ChargeViewPopup.pcf: line 23, column 24
    function initialValue_0 () : Charge[] {
      return initCharges()
    }
    
    override property get CurrentLocation () : pcf.Acct360ChargeViewPopup {
      return super.CurrentLocation as pcf.Acct360ChargeViewPopup
    }
    
    property get billInstruction () : BillingInstruction {
      return getVariableValue("billInstruction", 0) as BillingInstruction
    }
    
    property set billInstruction ($arg :  BillingInstruction) {
      setVariableValue("billInstruction", 0, $arg)
    }
    
    property get charges () : Charge[] {
      return getVariableValue("charges", 0) as Charge[]
    }
    
    property set charges ($arg :  Charge[]) {
      setVariableValue("charges", 0, $arg)
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 0, $arg)
    }
    
    function initCharges () : Charge[] {
      if (billInstruction != null) {
        return billInstruction.Charges
      } else {
        return plcyPeriod.Charges.toTypedArray()
      }
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/acct360/Acct360ChargeViewPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailChargesListDetailPanelExpressionsImpl extends Acct360ChargeViewPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at Acct360ChargeViewPopup.pcf: line 30, column 65
    function def_onEnter_1 (def :  pcf.ChargesLV) : void {
      def.onEnter(charges, false, 0, true, false, true)
    }
    
    // 'def' attribute on PanelRef at Acct360ChargeViewPopup.pcf: line 38, column 74
    function def_onEnter_3 (def :  pcf.InvoiceItemsLV) : void {
      def.onEnter(charge.AllInvoiceItems, charge, true)
    }
    
    // 'def' attribute on PanelRef at Acct360ChargeViewPopup.pcf: line 30, column 65
    function def_refreshVariables_2 (def :  pcf.ChargesLV) : void {
      def.refreshVariables(charges, false, 0, true, false, true)
    }
    
    // 'def' attribute on PanelRef at Acct360ChargeViewPopup.pcf: line 38, column 74
    function def_refreshVariables_4 (def :  pcf.InvoiceItemsLV) : void {
      def.refreshVariables(charge.AllInvoiceItems, charge, true)
    }
    
    property get charge () : Charge {
      return getCurrentSelection(1) as Charge
    }
    
    property set charge ($arg :  Charge) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  
}