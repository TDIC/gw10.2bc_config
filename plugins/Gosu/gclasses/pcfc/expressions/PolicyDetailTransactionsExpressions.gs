package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailTransactions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailTransactionsExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailTransactions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailTransactionsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=PolicyDetailTransactions) at PolicyDetailTransactions.pcf: line 9, column 76
    static function canVisit_4 (plcyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcytxnview
    }
    
    // 'def' attribute on PanelRef at PolicyDetailTransactions.pcf: line 24, column 48
    function def_onEnter_2 (def :  pcf.TransactionDetailsLV) : void {
      def.onEnter(plcyPeriod)
    }
    
    // 'def' attribute on PanelRef at PolicyDetailTransactions.pcf: line 24, column 48
    function def_refreshVariables_3 (def :  pcf.TransactionDetailsLV) : void {
      def.refreshVariables(plcyPeriod)
    }
    
    // 'label' attribute on AlertBar (id=ArchivedAlert) at PolicyDetailTransactions.pcf: line 22, column 40
    function label_1 () : java.lang.Object {
      return DisplayKey.get("Web.Archive.PolicyPeriod.ArchivedAlertBar", plcyPeriod.ArchiveDate.AsUIStyle)
    }
    
    // Page (id=PolicyDetailTransactions) at PolicyDetailTransactions.pcf: line 9, column 76
    static function parent_5 (plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyGroup.createDestination(plcyPeriod)
    }
    
    // 'visible' attribute on AlertBar (id=ArchivedAlert) at PolicyDetailTransactions.pcf: line 22, column 40
    function visible_0 () : java.lang.Boolean {
      return plcyPeriod.Archived
    }
    
    override property get CurrentLocation () : pcf.PolicyDetailTransactions {
      return super.CurrentLocation as pcf.PolicyDetailTransactions
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 0, $arg)
    }
    
    
  }
  
  
}