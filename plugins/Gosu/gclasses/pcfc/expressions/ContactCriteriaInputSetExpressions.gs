package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ContactCriteriaInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ContactCriteriaInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/search/ContactCriteriaInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ContactCriteriaInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ContactCriteriaInputSet.pcf: line 31, column 41
    function def_onEnter_12 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ContactCriteriaInputSet.pcf: line 31, column 41
    function def_onEnter_14 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ContactCriteriaInputSet.pcf: line 31, column 41
    function def_onEnter_16 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef at ContactCriteriaInputSet.pcf: line 23, column 54
    function def_onEnter_2 (def :  pcf.GlobalContactNameInputSet_Japan) : void {
      def.onEnter(new gw.api.name.SearchNameOwner (contactCriteria))
    }
    
    // 'def' attribute on InputSetRef at ContactCriteriaInputSet.pcf: line 23, column 54
    function def_onEnter_4 (def :  pcf.GlobalContactNameInputSet_default) : void {
      def.onEnter(new gw.api.name.SearchNameOwner (contactCriteria))
    }
    
    // 'def' attribute on InputSetRef at ContactCriteriaInputSet.pcf: line 26, column 54
    function def_onEnter_7 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.onEnter(new gw.api.name.SearchNameOwner (contactCriteria))
    }
    
    // 'def' attribute on InputSetRef at ContactCriteriaInputSet.pcf: line 26, column 54
    function def_onEnter_9 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.onEnter(new gw.api.name.SearchNameOwner (contactCriteria))
    }
    
    // 'def' attribute on InputSetRef at ContactCriteriaInputSet.pcf: line 26, column 54
    function def_refreshVariables_10 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner (contactCriteria))
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ContactCriteriaInputSet.pcf: line 31, column 41
    function def_refreshVariables_13 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ContactCriteriaInputSet.pcf: line 31, column 41
    function def_refreshVariables_15 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ContactCriteriaInputSet.pcf: line 31, column 41
    function def_refreshVariables_17 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef at ContactCriteriaInputSet.pcf: line 23, column 54
    function def_refreshVariables_3 (def :  pcf.GlobalContactNameInputSet_Japan) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner (contactCriteria))
    }
    
    // 'def' attribute on InputSetRef at ContactCriteriaInputSet.pcf: line 23, column 54
    function def_refreshVariables_5 (def :  pcf.GlobalContactNameInputSet_default) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner (contactCriteria))
    }
    
    // 'def' attribute on InputSetRef at ContactCriteriaInputSet.pcf: line 26, column 54
    function def_refreshVariables_8 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner (contactCriteria))
    }
    
    // 'value' attribute on TextInput (id=ADANumber_Input) at ContactCriteriaInputSet.pcf: line 39, column 37
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      contactCriteria.ADANumber_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextInput (id=ADANumber_Input) at ContactCriteriaInputSet.pcf: line 39, column 37
    function editable_19 () : java.lang.Boolean {
      return displayADA == true
    }
    
    // 'initialValue' attribute on Variable at ContactCriteriaInputSet.pcf: line 17, column 58
    function initialValue_0 () : gw.api.address.ContactCriteriaAddressOwner {
      return new gw.api.address.ContactCriteriaAddressOwner(contactCriteria)
    }
    
    // 'label' attribute on Label (id=ContactCriteria) at ContactCriteriaInputSet.pcf: line 20, column 179
    function label_1 () : java.lang.String {
      return displayADA == true? DisplayKey.get("Web.AddressCriteriaSearchDV.Contact") : DisplayKey.get("TDIC.Web.AddressCriteriaSearchDV.ProducerContact")
    }
    
    // 'mode' attribute on InputSetRef (id=globalAddressContainer) at ContactCriteriaInputSet.pcf: line 31, column 41
    function mode_18 () : java.lang.Object {
      return gw.api.address.AddressCountrySettings.getSettings(addressOwner.InputSetMode).PCFMode
    }
    
    // 'mode' attribute on InputSetRef at ContactCriteriaInputSet.pcf: line 23, column 54
    function mode_6 () : java.lang.Object {
      return gw.api.name.NameLocaleSettings.PCFMode
    }
    
    // 'value' attribute on TextInput (id=ADANumber_Input) at ContactCriteriaInputSet.pcf: line 39, column 37
    function valueRoot_23 () : java.lang.Object {
      return contactCriteria
    }
    
    // 'value' attribute on TextInput (id=ADANumber_Input) at ContactCriteriaInputSet.pcf: line 39, column 37
    function value_21 () : java.lang.String {
      return contactCriteria.ADANumber_TDIC
    }
    
    property get addressOwner () : gw.api.address.ContactCriteriaAddressOwner {
      return getVariableValue("addressOwner", 0) as gw.api.address.ContactCriteriaAddressOwner
    }
    
    property set addressOwner ($arg :  gw.api.address.ContactCriteriaAddressOwner) {
      setVariableValue("addressOwner", 0, $arg)
    }
    
    property get contactCriteria () : gw.search.ContactCriteria {
      return getRequireValue("contactCriteria", 0) as gw.search.ContactCriteria
    }
    
    property set contactCriteria ($arg :  gw.search.ContactCriteria) {
      setRequireValue("contactCriteria", 0, $arg)
    }
    
    property get displayADA () : boolean {
      return getRequireValue("displayADA", 0) as java.lang.Boolean
    }
    
    property set displayADA ($arg :  boolean) {
      setRequireValue("displayADA", 0, $arg)
    }
    
    
  }
  
  
}