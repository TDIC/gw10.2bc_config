package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.plugin.commission.ICommission
uses gw.plugin.Plugins
@javax.annotation.Generated("config/web/pcf/policy/OverrideCommissionPlanPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OverrideCommissionPlanPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/OverrideCommissionPlanPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends OverrideCommissionPlanPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=AlternatePicker) at OverrideCommissionPlanPopup.pcf: line 53, column 72
    function action_8 () : void {
      CommissionPlanSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=AlternatePicker) at OverrideCommissionPlanPopup.pcf: line 53, column 72
    function action_dest_9 () : pcf.api.Destination {
      return pcf.CommissionPlanSearchPopup.createDestination()
    }
    
    // 'value' attribute on TextInput (id=Alternate_Input) at OverrideCommissionPlanPopup.pcf: line 53, column 72
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      overrideSetting.AlternateCommissionPlan = (__VALUE_TO_SET as entity.CommissionPlan)
    }
    
    // 'value' attribute on TextInput (id=Percentage_Input) at OverrideCommissionPlanPopup.pcf: line 67, column 74
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      overrideSetting.Percentage = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TypeKeyInput (id=OverrideMethod_Input) at OverrideCommissionPlanPopup.pcf: line 42, column 58
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      overrideSetting.OverrideMethod = (__VALUE_TO_SET as typekey.CmsnPlanOverrideMethod)
    }
    
    // 'editable' attribute on TextInput (id=Alternate_Input) at OverrideCommissionPlanPopup.pcf: line 53, column 72
    function editable_10 () : java.lang.Boolean {
      return perm.Producer.commplanoride
    }
    
    // 'editable' attribute on TextInput (id=Percentage_Input) at OverrideCommissionPlanPopup.pcf: line 67, column 74
    function editable_19 () : java.lang.Boolean {
      return perm.Producer.commpercoride
    }
    
    // 'inputConversion' attribute on TextInput (id=Alternate_Input) at OverrideCommissionPlanPopup.pcf: line 53, column 72
    function inputConversion_12 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.api.web.plan.commission.CommissionPlanUtil.getActiveCommissionPlan(VALUE)
    }
    
    // 'value' attribute on TypeKeyInput (id=OverrideMethod_Input) at OverrideCommissionPlanPopup.pcf: line 42, column 58
    function valueRoot_6 () : java.lang.Object {
      return overrideSetting
    }
    
    // 'value' attribute on TextInput (id=Alternate_Input) at OverrideCommissionPlanPopup.pcf: line 53, column 72
    function value_13 () : entity.CommissionPlan {
      return overrideSetting.AlternateCommissionPlan
    }
    
    // 'value' attribute on TextInput (id=OverrideSetting_Input) at OverrideCommissionPlanPopup.pcf: line 35, column 65
    function value_2 () : entity.OverrideCommissionPlanSetting {
      return overrideSetting
    }
    
    // 'value' attribute on TextInput (id=Percentage_Input) at OverrideCommissionPlanPopup.pcf: line 67, column 74
    function value_21 () : java.math.BigDecimal {
      return overrideSetting.Percentage
    }
    
    // 'value' attribute on TypeKeyInput (id=OverrideMethod_Input) at OverrideCommissionPlanPopup.pcf: line 42, column 58
    function value_4 () : typekey.CmsnPlanOverrideMethod {
      return overrideSetting.OverrideMethod
    }
    
    // 'visible' attribute on TextInput (id=Alternate_Input) at OverrideCommissionPlanPopup.pcf: line 53, column 72
    function visible_11 () : java.lang.Boolean {
      return overrideSetting.OverrideMethod == TC_ALTERNATE
    }
    
    // 'visible' attribute on TextInput (id=Percentage_Input) at OverrideCommissionPlanPopup.pcf: line 67, column 74
    function visible_20 () : java.lang.Boolean {
      return overrideSetting.OverrideMethod == TC_PERCENTAGE
    }
    
    property get overrideSetting () : entity.OverrideCommissionPlanSetting {
      return getIteratedValue(1) as entity.OverrideCommissionPlanSetting
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/OverrideCommissionPlanPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OverrideCommissionPlanPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (PolicyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=OverrideCommissionPlanPopup) at OverrideCommissionPlanPopup.pcf: line 10, column 79
    function beforeCommit_28 (pickedValue :  java.lang.Object) : void {
      applyOverrideSettings();
    }
    
    // 'initialValue' attribute on Variable at OverrideCommissionPlanPopup.pcf: line 19, column 55
    function initialValue_0 () : entity.OverrideCommissionPlanSettingSet {
      return initOverrideSettings()
    }
    
    // EditButtons at OverrideCommissionPlanPopup.pcf: line 23, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'value' attribute on InputIterator at OverrideCommissionPlanPopup.pcf: line 30, column 64
    function value_27 () : entity.OverrideCommissionPlanSetting[] {
      return overrideSettings.OrderedSettings
    }
    
    override property get CurrentLocation () : pcf.OverrideCommissionPlanPopup {
      return super.CurrentLocation as pcf.OverrideCommissionPlanPopup
    }
    
    property get PolicyPeriod () : PolicyPeriod {
      return getVariableValue("PolicyPeriod", 0) as PolicyPeriod
    }
    
    property set PolicyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("PolicyPeriod", 0, $arg)
    }
    
    property get overrideSettings () : entity.OverrideCommissionPlanSettingSet {
      return getVariableValue("overrideSettings", 0) as entity.OverrideCommissionPlanSettingSet
    }
    
    property set overrideSettings ($arg :  entity.OverrideCommissionPlanSettingSet) {
      setVariableValue("overrideSettings", 0, $arg)
    }
    
    
    function initOverrideSettings() : OverrideCommissionPlanSettingSet {
      var result = PolicyPeriod.OverrideCommissionPlanSettings;
      for (setting in result.Settings) {
        // Initialize defaults
        setting.OverrideMethod = TC_NOCHANGE;
        var policyCommission = setting.PolicyCommission;
        setting.AlternateCommissionPlan = policyCommission.CommissionSubPlan.CommissionPlan;
        setting.Percentage = policyCommission.getCommissionRate(null).Points.resolve( 0,
        java.math.RoundingMode.HALF_EVEN );
      }
      return result;
    }
    
    function applyOverrideSettings() {
      for (setting in overrideSettings.OrderedSettings) {
        setting.apply()
        applyToCharges(setting)
      }
    }
    
    function applyToCharges(setting : OverrideCommissionPlanSetting) {
      for (chargeCommission in setting.PolicyCommission.CommissionableCharges) { 
        var role = chargeCommission.PolicyCommission.Role
        if (isCommissionable(chargeCommission)) {
          if (setting.OverrideMethod == typekey.CmsnPlanOverrideMethod.TC_ALTERNATE) {
            var subplan = setting.AlternateCommissionPlan.getApplicableSubPlan(chargeCommission.PolicyCommission.PolicyPeriod)
            var newRate = subplan.getBaseRate(chargeCommission.PolicyCommission.Role)
            chargeCommission.Charge.overrideCommissionRate(role, newRate)
          } else if (setting.OverrideMethod == typekey.CmsnPlanOverrideMethod.TC_PERCENTAGE) {
            chargeCommission.Charge.overrideCommissionRate(role, setting.Percentage)
          }
        }
      }
    }
    
    function isCommissionable(chargeCommission : ChargeCommission) : boolean {
      var policyCommission = chargeCommission.PolicyCommission
      return Plugins.get(ICommission)
          .isCommissionable(policyCommission.CommissionSubPlan, chargeCommission.Charge, policyCommission.Role)
    
    }
    
    
  }
  
  
}