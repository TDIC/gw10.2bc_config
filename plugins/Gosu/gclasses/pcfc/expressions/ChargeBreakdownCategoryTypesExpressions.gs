package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/businesssettings/ChargeBreakdownCategoryTypes.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChargeBreakdownCategoryTypesExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/ChargeBreakdownCategoryTypes.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChargeBreakdownCategoryTypesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=NewChargeBreakdownCategoryTypeButton) at ChargeBreakdownCategoryTypes.pcf: line 17, column 70
    function action_1 () : void {
      NewChargeBreakdownCategoryType.go()
    }
    
    // 'action' attribute on ToolbarButton (id=NewChargeBreakdownCategoryTypeButton) at ChargeBreakdownCategoryTypes.pcf: line 17, column 70
    function action_dest_2 () : pcf.api.Destination {
      return pcf.NewChargeBreakdownCategoryType.createDestination()
    }
    
    // 'canVisit' attribute on Page (id=ChargeBreakdownCategoryTypes) at ChargeBreakdownCategoryTypes.pcf: line 8, column 80
    static function canVisit_16 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.chargebreakdowncategorytypeview
    }
    
    // Page (id=ChargeBreakdownCategoryTypes) at ChargeBreakdownCategoryTypes.pcf: line 8, column 80
    static function parent_17 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at ChargeBreakdownCategoryTypes.pcf: line 32, column 44
    function sortValue_3 (categoryType :  entity.ChargeBreakdownCategoryType) : java.lang.Object {
      return categoryType.Code
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at ChargeBreakdownCategoryTypes.pcf: line 38, column 44
    function sortValue_4 (categoryType :  entity.ChargeBreakdownCategoryType) : java.lang.Object {
      return categoryType.Name
    }
    
    // 'value' attribute on RowIterator at ChargeBreakdownCategoryTypes.pcf: line 26, column 101
    function value_15 () : gw.api.database.IQueryBeanResult<entity.ChargeBreakdownCategoryType> {
      return gw.api.database.Query.make(ChargeBreakdownCategoryType).select()
    }
    
    // 'visible' attribute on ToolbarButton (id=NewChargeBreakdownCategoryTypeButton) at ChargeBreakdownCategoryTypes.pcf: line 17, column 70
    function visible_0 () : java.lang.Boolean {
      return perm.System.chargebreakdowncategorytypecreate
    }
    
    override property get CurrentLocation () : pcf.ChargeBreakdownCategoryTypes {
      return super.CurrentLocation as pcf.ChargeBreakdownCategoryTypes
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/ChargeBreakdownCategoryTypes.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ChargeBreakdownCategoryTypesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at ChargeBreakdownCategoryTypes.pcf: line 38, column 44
    function action_10 () : void {
      ChargeBreakdownCategoryTypeDetail.go(categoryType)
    }
    
    // 'action' attribute on TextCell (id=Code_Cell) at ChargeBreakdownCategoryTypes.pcf: line 32, column 44
    function action_5 () : void {
      ChargeBreakdownCategoryTypeDetail.go(categoryType)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at ChargeBreakdownCategoryTypes.pcf: line 38, column 44
    function action_dest_11 () : pcf.api.Destination {
      return pcf.ChargeBreakdownCategoryTypeDetail.createDestination(categoryType)
    }
    
    // 'action' attribute on TextCell (id=Code_Cell) at ChargeBreakdownCategoryTypes.pcf: line 32, column 44
    function action_dest_6 () : pcf.api.Destination {
      return pcf.ChargeBreakdownCategoryTypeDetail.createDestination(categoryType)
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at ChargeBreakdownCategoryTypes.pcf: line 32, column 44
    function valueRoot_8 () : java.lang.Object {
      return categoryType
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at ChargeBreakdownCategoryTypes.pcf: line 38, column 44
    function value_12 () : java.lang.String {
      return categoryType.Name
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at ChargeBreakdownCategoryTypes.pcf: line 32, column 44
    function value_7 () : java.lang.String {
      return categoryType.Code
    }
    
    property get categoryType () : entity.ChargeBreakdownCategoryType {
      return getIteratedValue(1) as entity.ChargeBreakdownCategoryType
    }
    
    
  }
  
  
}