package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DelinquencyProcessSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyProcessSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/DelinquencyProcessSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyProcessSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountNumberCriterion_Input) at DelinquencyProcessSearchDV.pcf: line 21, column 39
    function action_0 () : void {
      AccountSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=PolicyNumberCriterion_Input) at DelinquencyProcessSearchDV.pcf: line 35, column 39
    function action_11 () : void {
      PolicySearchPopup.push(true)
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountNumberCriterion_Input) at DelinquencyProcessSearchDV.pcf: line 21, column 39
    function action_dest_1 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'pickLocation' attribute on PickerInput (id=PolicyNumberCriterion_Input) at DelinquencyProcessSearchDV.pcf: line 35, column 39
    function action_dest_12 () : pcf.api.Destination {
      return pcf.PolicySearchPopup.createDestination(true)
    }
    
    // 'conversionExpression' attribute on PickerInput (id=PolicyNumberCriterion_Input) at DelinquencyProcessSearchDV.pcf: line 35, column 39
    function conversionExpression_13 (PickedValue :  PolicyPeriod) : java.lang.String {
      return PickedValue.DisplayName
    }
    
    // 'conversionExpression' attribute on PickerInput (id=AccountNumberCriterion_Input) at DelinquencyProcessSearchDV.pcf: line 21, column 39
    function conversionExpression_2 (PickedValue :  Account) : java.lang.String {
      return PickedValue.DisplayName
    }
    
    // 'def' attribute on InputSetRef at DelinquencyProcessSearchDV.pcf: line 58, column 77
    function def_onEnter_33 (def :  pcf.ContactCriteriaInputSet) : void {
      def.onEnter(searchCriteria.ContactCriteria,true)
    }
    
    // 'def' attribute on InputSetRef at DelinquencyProcessSearchDV.pcf: line 62, column 41
    function def_onEnter_35 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at DelinquencyProcessSearchDV.pcf: line 58, column 77
    function def_refreshVariables_34 (def :  pcf.ContactCriteriaInputSet) : void {
      def.refreshVariables(searchCriteria.ContactCriteria,true)
    }
    
    // 'def' attribute on InputSetRef at DelinquencyProcessSearchDV.pcf: line 62, column 41
    function def_refreshVariables_36 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on PickerInput (id=PolicyNumberCriterion_Input) at DelinquencyProcessSearchDV.pcf: line 35, column 39
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=StatusCriterion_Input) at DelinquencyProcessSearchDV.pcf: line 41, column 55
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Status = (__VALUE_TO_SET as typekey.DelinquencyProcessStatus)
    }
    
    // 'value' attribute on TypeKeyInput (id=ActiveCriterion_Input) at DelinquencyProcessSearchDV.pcf: line 47, column 43
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.DelinquencyActiveStatus = (__VALUE_TO_SET as typekey.ActiveStatus)
    }
    
    // 'value' attribute on RangeInput (id=CurrentEventName_Input) at DelinquencyProcessSearchDV.pcf: line 54, column 51
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.CurrentEventName = (__VALUE_TO_SET as typekey.DelinquencyEventName)
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at DelinquencyProcessSearchDV.pcf: line 21, column 39
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AccountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=AccountSegmentCriterion_Input) at DelinquencyProcessSearchDV.pcf: line 27, column 45
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Segment = (__VALUE_TO_SET as typekey.AccountSegment)
    }
    
    // 'valueRange' attribute on RangeInput (id=CurrentEventName_Input) at DelinquencyProcessSearchDV.pcf: line 54, column 51
    function valueRange_29 () : java.lang.Object {
      return DelinquencyEventName.getTypeKeys(false).where( \ delinquencyEventName -> delinquencyEventName!= typekey.DelinquencyEventName.TC_DUNNINGLETTER2)
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at DelinquencyProcessSearchDV.pcf: line 21, column 39
    function valueRoot_5 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on PickerInput (id=PolicyNumberCriterion_Input) at DelinquencyProcessSearchDV.pcf: line 35, column 39
    function value_14 () : java.lang.String {
      return searchCriteria.PolicyNumber
    }
    
    // 'value' attribute on TypeKeyInput (id=StatusCriterion_Input) at DelinquencyProcessSearchDV.pcf: line 41, column 55
    function value_18 () : typekey.DelinquencyProcessStatus {
      return searchCriteria.Status
    }
    
    // 'value' attribute on TypeKeyInput (id=ActiveCriterion_Input) at DelinquencyProcessSearchDV.pcf: line 47, column 43
    function value_22 () : typekey.ActiveStatus {
      return searchCriteria.DelinquencyActiveStatus
    }
    
    // 'value' attribute on RangeInput (id=CurrentEventName_Input) at DelinquencyProcessSearchDV.pcf: line 54, column 51
    function value_26 () : typekey.DelinquencyEventName {
      return searchCriteria.CurrentEventName
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at DelinquencyProcessSearchDV.pcf: line 21, column 39
    function value_3 () : java.lang.String {
      return searchCriteria.AccountNumber
    }
    
    // 'value' attribute on TypeKeyInput (id=AccountSegmentCriterion_Input) at DelinquencyProcessSearchDV.pcf: line 27, column 45
    function value_7 () : typekey.AccountSegment {
      return searchCriteria.Segment
    }
    
    // 'valueRange' attribute on RangeInput (id=CurrentEventName_Input) at DelinquencyProcessSearchDV.pcf: line 54, column 51
    function verifyValueRangeIsAllowedType_30 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CurrentEventName_Input) at DelinquencyProcessSearchDV.pcf: line 54, column 51
    function verifyValueRangeIsAllowedType_30 ($$arg :  typekey.DelinquencyEventName[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=CurrentEventName_Input) at DelinquencyProcessSearchDV.pcf: line 54, column 51
    function verifyValueRange_31 () : void {
      var __valueRangeArg = DelinquencyEventName.getTypeKeys(false).where( \ delinquencyEventName -> delinquencyEventName!= typekey.DelinquencyEventName.TC_DUNNINGLETTER2)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_30(__valueRangeArg)
    }
    
    property get searchCriteria () : gw.search.DelinquencySearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.search.DelinquencySearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.DelinquencySearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    
  }
  
  
}