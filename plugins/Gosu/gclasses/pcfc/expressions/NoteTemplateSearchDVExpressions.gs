package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/note/NoteTemplateSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NoteTemplateSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/note/NoteTemplateSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NoteTemplateSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at NoteTemplateSearchDV.pcf: line 49, column 41
    function def_onEnter_31 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at NoteTemplateSearchDV.pcf: line 49, column 41
    function def_refreshVariables_32 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at NoteTemplateSearchDV.pcf: line 32, column 67
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteTemplateSearchCriteria.Language = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'value' attribute on TypeKeyInput (id=Topic_Input) at NoteTemplateSearchDV.pcf: line 17, column 68
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteTemplateSearchCriteria.Topic = (__VALUE_TO_SET as typekey.NoteTopicType)
    }
    
    // 'value' attribute on RangeInput (id=Product_Input) at NoteTemplateSearchDV.pcf: line 40, column 62
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteTemplateSearchCriteria.LOB = (__VALUE_TO_SET as typekey.LOBCode)
    }
    
    // 'value' attribute on TextInput (id=Keywords_Input) at NoteTemplateSearchDV.pcf: line 45, column 54
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteTemplateSearchCriteria.Keywords = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Type_Input) at NoteTemplateSearchDV.pcf: line 24, column 63
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteTemplateSearchCriteria.Type = (__VALUE_TO_SET as typekey.NoteType)
    }
    
    // 'valueRange' attribute on RangeInput (id=Product_Input) at NoteTemplateSearchDV.pcf: line 40, column 62
    function valueRange_22 () : java.lang.Object {
      return LOBCode.getTypeKeys( false )
    }
    
    // 'value' attribute on TypeKeyInput (id=Topic_Input) at NoteTemplateSearchDV.pcf: line 17, column 68
    function valueRoot_3 () : java.lang.Object {
      return NoteTemplateSearchCriteria
    }
    
    // 'value' attribute on TypeKeyInput (id=Topic_Input) at NoteTemplateSearchDV.pcf: line 17, column 68
    function value_1 () : typekey.NoteTopicType {
      return NoteTemplateSearchCriteria.Topic
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at NoteTemplateSearchDV.pcf: line 32, column 67
    function value_13 () : typekey.LanguageType {
      return NoteTemplateSearchCriteria.Language
    }
    
    // 'value' attribute on RangeInput (id=Product_Input) at NoteTemplateSearchDV.pcf: line 40, column 62
    function value_19 () : typekey.LOBCode {
      return NoteTemplateSearchCriteria.LOB
    }
    
    // 'value' attribute on TextInput (id=Keywords_Input) at NoteTemplateSearchDV.pcf: line 45, column 54
    function value_27 () : java.lang.String {
      return NoteTemplateSearchCriteria.Keywords
    }
    
    // 'value' attribute on TypeKeyInput (id=Type_Input) at NoteTemplateSearchDV.pcf: line 24, column 63
    function value_7 () : typekey.NoteType {
      return NoteTemplateSearchCriteria.Type
    }
    
    // 'valueRange' attribute on RangeInput (id=Product_Input) at NoteTemplateSearchDV.pcf: line 40, column 62
    function verifyValueRangeIsAllowedType_23 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Product_Input) at NoteTemplateSearchDV.pcf: line 40, column 62
    function verifyValueRangeIsAllowedType_23 ($$arg :  typekey.LOBCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Product_Input) at NoteTemplateSearchDV.pcf: line 40, column 62
    function verifyValueRange_24 () : void {
      var __valueRangeArg = LOBCode.getTypeKeys( false )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_23(__valueRangeArg)
    }
    
    // 'visible' attribute on TypeKeyInput (id=Topic_Input) at NoteTemplateSearchDV.pcf: line 17, column 68
    function visible_0 () : java.lang.Boolean {
      return NoteTopicType.getTypeKeys( false ).Count > 1
    }
    
    // 'visible' attribute on TypeKeyInput (id=Language_Input) at NoteTemplateSearchDV.pcf: line 32, column 67
    function visible_12 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count > 1
    }
    
    // 'visible' attribute on RangeInput (id=Product_Input) at NoteTemplateSearchDV.pcf: line 40, column 62
    function visible_18 () : java.lang.Boolean {
      return LOBCode.getTypeKeys( false ).Count > 1
    }
    
    // 'visible' attribute on TypeKeyInput (id=Type_Input) at NoteTemplateSearchDV.pcf: line 24, column 63
    function visible_6 () : java.lang.Boolean {
      return NoteType.getTypeKeys( false ).Count > 1
    }
    
    property get NoteTemplateSearchCriteria () : NoteTemplateSearchCriteria {
      return getRequireValue("NoteTemplateSearchCriteria", 0) as NoteTemplateSearchCriteria
    }
    
    property set NoteTemplateSearchCriteria ($arg :  NoteTemplateSearchCriteria) {
      setRequireValue("NoteTemplateSearchCriteria", 0, $arg)
    }
    
    
  }
  
  
}