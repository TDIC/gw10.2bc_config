package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.CurrencyUtil
@javax.annotation.Generated("config/web/pcf/commission/EditIncentivesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class EditIncentivesPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/commission/EditIncentivesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class EditIncentivesPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (commissionSubPlan :  CommissionSubPlan, incentiveType :  typekey.Incentive) : int {
      return 0
    }
    
    // 'def' attribute on PanelRef at EditIncentivesPopup.pcf: line 26, column 30
    function def_onEnter_7 (def :  pcf.IncentivesLV_PremiumIncentive) : void {
      def.onEnter(commissionSubPlan)
    }
    
    // 'def' attribute on PanelRef at EditIncentivesPopup.pcf: line 26, column 30
    function def_refreshVariables_8 (def :  pcf.IncentivesLV_PremiumIncentive) : void {
      def.refreshVariables(commissionSubPlan)
    }
    
    // EditButtons (id=EditButtons) at EditIncentivesPopup.pcf: line 22, column 29
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'mode' attribute on PanelRef at EditIncentivesPopup.pcf: line 26, column 30
    function mode_9 () : java.lang.Object {
      return incentiveType
    }
    
    // 'title' attribute on Popup (id=EditIncentivesPopup) at EditIncentivesPopup.pcf: line 10, column 161
    static function title_10 (commissionSubPlan :  CommissionSubPlan, incentiveType :  typekey.Incentive) : java.lang.Object {
      return DisplayKey.get("Web.EditIncentivesPopup.Title", commissionSubPlan.CommissionPlan.Name, commissionSubPlan.Name, incentiveType.DisplayName)
    }
    
    // 'toCreateAndAdd' attribute on AddButton (id=AddButtonUnderSingleCurrencyMode) at EditIncentivesPopup.pcf: line 33, column 47
    function toCreateAndAdd_2 (CheckedValues :  Object[]) : java.lang.Object {
      return addToIncentives(getDefaultCurrency())
    }
    
    // 'value' attribute on AddMenuItemIterator at EditIncentivesPopup.pcf: line 42, column 38
    function value_5 () : Currency[] {
      return commissionSubPlan.CommissionPlan.Currencies
    }
    
    // 'visible' attribute on AddButton (id=AddButtonUnderSingleCurrencyMode) at EditIncentivesPopup.pcf: line 33, column 47
    function visible_1 () : java.lang.Boolean {
      return !isMultiCurrencyMode()
    }
    
    // 'visible' attribute on AddButton (id=AddButtonUnderMultiCurrencyMode) at EditIncentivesPopup.pcf: line 38, column 45
    function visible_6 () : java.lang.Boolean {
      return isMultiCurrencyMode()
    }
    
    override property get CurrentLocation () : pcf.EditIncentivesPopup {
      return super.CurrentLocation as pcf.EditIncentivesPopup
    }
    
    property get commissionSubPlan () : CommissionSubPlan {
      return getVariableValue("commissionSubPlan", 0) as CommissionSubPlan
    }
    
    property set commissionSubPlan ($arg :  CommissionSubPlan) {
      setVariableValue("commissionSubPlan", 0, $arg)
    }
    
    property get incentiveType () : typekey.Incentive {
      return getVariableValue("incentiveType", 0) as typekey.Incentive
    }
    
    property set incentiveType ($arg :  typekey.Incentive) {
      setVariableValue("incentiveType", 0, $arg)
    }
    
    
    function addToIncentives(currency : Currency) : PremiumIncentive {
      var newIncentive = new PremiumIncentive(currency)
      commissionSubPlan.addToIncentives(newIncentive)
      return newIncentive
    }
    
    function getDefaultCurrency() : Currency{
      if(!CurrencyUtil.isMultiCurrencyMode()){
        return CurrencyUtil.getDefaultCurrency()
      } else{
        return Currency.getTypeKeys(true).get(0)
      }
    }
    
    function isMultiCurrencyMode(): boolean {
      return CurrencyUtil.isMultiCurrencyMode() and Currency.getTypeKeys(true).Count > 1
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/commission/EditIncentivesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends EditIncentivesPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=currencySelection) at EditIncentivesPopup.pcf: line 47, column 64
    function label_3 () : java.lang.Object {
      return newCurrency.DisplayName
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=currencySelection) at EditIncentivesPopup.pcf: line 47, column 64
    function toCreateAndAdd_4 (CheckedValues :  Object[]) : java.lang.Object {
      return addToIncentives(newCurrency)
    }
    
    property get newCurrency () : Currency {
      return getIteratedValue(1) as Currency
    }
    
    
  }
  
  
}