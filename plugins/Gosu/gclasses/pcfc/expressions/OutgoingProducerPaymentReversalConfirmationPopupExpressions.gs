package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/OutgoingProducerPaymentReversalConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OutgoingProducerPaymentReversalConfirmationPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/OutgoingProducerPaymentReversalConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OutgoingProducerPaymentReversalConfirmationPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (payment :  ProducerPaymentSent) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=OutgoingProducerPaymentReversalConfirmationPopup) at OutgoingProducerPaymentReversalConfirmationPopup.pcf: line 10, column 79
    function beforeCommit_11 (pickedValue :  java.lang.Object) : void {
      reversePayment()
    }
    
    // 'value' attribute on TypeKeyInput (id=Reason_Input) at OutgoingProducerPaymentReversalConfirmationPopup.pcf: line 40, column 56
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      reason = (__VALUE_TO_SET as typekey.PaymentReversalReason)
    }
    
    // 'value' attribute on RangeInput (id=PaidCommissionReversalType_Input) at OutgoingProducerPaymentReversalConfirmationPopup.pcf: line 49, column 44
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      reverseOnlyPaymentSent = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'label' attribute on Label (id=Confirmation) at OutgoingProducerPaymentReversalConfirmationPopup.pcf: line 33, column 120
    function label_0 () : java.lang.String {
      return DisplayKey.get("Web.PaymentReversalConfirmation.Confirmation", payment.Amount.render())
    }
    
    // 'optionLabel' attribute on RangeInput (id=PaidCommissionReversalType_Input) at OutgoingProducerPaymentReversalConfirmationPopup.pcf: line 49, column 44
    function optionLabel_6 (VALUE :  java.lang.Boolean) : java.lang.String {
      return VALUE ? DisplayKey.get("Web.PaymentReversalConfirmation.PaymentSentOnly") : DisplayKey.get("Web.PaymentReversalConfirmation.PaymentSentAndAssociatedTxns")
    }
    
    // 'valueRange' attribute on RangeInput (id=PaidCommissionReversalType_Input) at OutgoingProducerPaymentReversalConfirmationPopup.pcf: line 49, column 44
    function valueRange_7 () : java.lang.Object {
      return {false, true}
    }
    
    // 'value' attribute on TypeKeyInput (id=Reason_Input) at OutgoingProducerPaymentReversalConfirmationPopup.pcf: line 40, column 56
    function value_1 () : typekey.PaymentReversalReason {
      return reason
    }
    
    // 'value' attribute on RangeInput (id=PaidCommissionReversalType_Input) at OutgoingProducerPaymentReversalConfirmationPopup.pcf: line 49, column 44
    function value_4 () : java.lang.Boolean {
      return reverseOnlyPaymentSent
    }
    
    // 'valueRange' attribute on RangeInput (id=PaidCommissionReversalType_Input) at OutgoingProducerPaymentReversalConfirmationPopup.pcf: line 49, column 44
    function verifyValueRangeIsAllowedType_8 ($$arg :  java.lang.Boolean[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaidCommissionReversalType_Input) at OutgoingProducerPaymentReversalConfirmationPopup.pcf: line 49, column 44
    function verifyValueRangeIsAllowedType_8 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaidCommissionReversalType_Input) at OutgoingProducerPaymentReversalConfirmationPopup.pcf: line 49, column 44
    function verifyValueRange_9 () : void {
      var __valueRangeArg = {false, true}
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_8(__valueRangeArg)
    }
    
    override property get CurrentLocation () : pcf.OutgoingProducerPaymentReversalConfirmationPopup {
      return super.CurrentLocation as pcf.OutgoingProducerPaymentReversalConfirmationPopup
    }
    
    property get payment () : ProducerPaymentSent {
      return getVariableValue("payment", 0) as ProducerPaymentSent
    }
    
    property set payment ($arg :  ProducerPaymentSent) {
      setVariableValue("payment", 0, $arg)
    }
    
    property get reason () : PaymentReversalReason {
      return getVariableValue("reason", 0) as PaymentReversalReason
    }
    
    property set reason ($arg :  PaymentReversalReason) {
      setVariableValue("reason", 0, $arg)
    }
    
    property get reverseOnlyPaymentSent () : boolean {
      return getVariableValue("reverseOnlyPaymentSent", 0) as java.lang.Boolean
    }
    
    property set reverseOnlyPaymentSent ($arg :  boolean) {
      setVariableValue("reverseOnlyPaymentSent", 0, $arg)
    }
    
    function reversePayment() {
      if (reverseOnlyPaymentSent) {
        payment.ProducerPayment.handlePaymentReversalOnlyReversingPaymentSent(reason)
      } else {
        payment.ProducerPayment.handlePaymentReversal(reason)
      }
    }
    
    
  }
  
  
}