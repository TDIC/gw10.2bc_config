package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ContactSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ContactSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/ContactSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ContactSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 31, column 121
    function def_onEnter_10 (def :  pcf.NameInputSet_Person) : void {
      def.onEnter(new gw.api.name.SearchNameOwner(searchCriteria))
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ContactSearchDV.pcf: line 41, column 43
    function def_onEnter_19 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ContactSearchDV.pcf: line 41, column 43
    function def_onEnter_21 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ContactSearchDV.pcf: line 41, column 43
    function def_onEnter_23 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 53, column 41
    function def_onEnter_32 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 31, column 121
    function def_onEnter_8 (def :  pcf.NameInputSet_Contact) : void {
      def.onEnter(new gw.api.name.SearchNameOwner(searchCriteria))
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 31, column 121
    function def_refreshVariables_11 (def :  pcf.NameInputSet_Person) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner(searchCriteria))
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ContactSearchDV.pcf: line 41, column 43
    function def_refreshVariables_20 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ContactSearchDV.pcf: line 41, column 43
    function def_refreshVariables_22 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ContactSearchDV.pcf: line 41, column 43
    function def_refreshVariables_24 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 53, column 41
    function def_refreshVariables_33 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'def' attribute on InputSetRef at ContactSearchDV.pcf: line 31, column 121
    function def_refreshVariables_9 (def :  pcf.NameInputSet_Contact) : void {
      def.refreshVariables(new gw.api.name.SearchNameOwner(searchCriteria))
    }
    
    // 'value' attribute on TextInput (id=TaxID_Input) at ContactSearchDV.pcf: line 36, column 39
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.TaxID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=ContactType_Input) at ContactSearchDV.pcf: line 26, column 37
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ContactSubtype = (__VALUE_TO_SET as typekey.Contact)
    }
    
    // 'value' attribute on TextInput (id=ADANumber_Input) at ContactSearchDV.pcf: line 49, column 79
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ADANumber_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at ContactSearchDV.pcf: line 14, column 43
    function initialValue_0 () : gw.api.address.AddressOwner {
      return new gw.api.address.ContactSearchAddressOwner(searchCriteria)
    }
    
    // 'label' attribute on TextInput (id=TaxID_Input) at ContactSearchDV.pcf: line 36, column 39
    function label_13 () : java.lang.Object {
      return searchCriteria.ContactSubtype == typekey.Contact.TC_PERSON ? DisplayKey.get("Web.ContactDetail.Name.TaxID.SSN") : DisplayKey.get("Web.ContactDetail.Name.TaxID.EIN")
    }
    
    // 'mode' attribute on InputSetRef at ContactSearchDV.pcf: line 31, column 121
    function mode_12 () : java.lang.Object {
      return (searchCriteria.ContactSubtype == typekey.Contact.TC_PERSON) ? "Person" : "Contact"
    }
    
    // 'mode' attribute on InputSetRef (id=globalAddressContainer) at ContactSearchDV.pcf: line 41, column 43
    function mode_25 () : java.lang.Object {
      return gw.api.address.AddressCountrySettings.getSettings(addressOwner.SelectedCountry).PCFMode
    }
    
    // 'valueRange' attribute on RangeInput (id=ContactType_Input) at ContactSearchDV.pcf: line 26, column 37
    function valueRange_4 () : java.lang.Object {
      return {typekey.Contact.TC_COMPANY, typekey.Contact.TC_PERSON}
    }
    
    // 'value' attribute on RangeInput (id=ContactType_Input) at ContactSearchDV.pcf: line 26, column 37
    function valueRoot_3 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on RangeInput (id=ContactType_Input) at ContactSearchDV.pcf: line 26, column 37
    function value_1 () : typekey.Contact {
      return searchCriteria.ContactSubtype
    }
    
    // 'value' attribute on TextInput (id=TaxID_Input) at ContactSearchDV.pcf: line 36, column 39
    function value_14 () : java.lang.String {
      return searchCriteria.TaxID
    }
    
    // 'value' attribute on TextInput (id=ADANumber_Input) at ContactSearchDV.pcf: line 49, column 79
    function value_27 () : java.lang.String {
      return searchCriteria.ADANumber_TDIC
    }
    
    // 'valueRange' attribute on RangeInput (id=ContactType_Input) at ContactSearchDV.pcf: line 26, column 37
    function verifyValueRangeIsAllowedType_5 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ContactType_Input) at ContactSearchDV.pcf: line 26, column 37
    function verifyValueRangeIsAllowedType_5 ($$arg :  typekey.Contact[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ContactType_Input) at ContactSearchDV.pcf: line 26, column 37
    function verifyValueRange_6 () : void {
      var __valueRangeArg = {typekey.Contact.TC_COMPANY, typekey.Contact.TC_PERSON}
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_5(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=ADANumber_Input) at ContactSearchDV.pcf: line 49, column 79
    function visible_26 () : java.lang.Boolean {
      return searchCriteria.ContactSubtype == typekey.Contact.TC_PERSON
    }
    
    property get addressOwner () : gw.api.address.AddressOwner {
      return getVariableValue("addressOwner", 0) as gw.api.address.AddressOwner
    }
    
    property set addressOwner ($arg :  gw.api.address.AddressOwner) {
      setVariableValue("addressOwner", 0, $arg)
    }
    
    property get searchCriteria () : ContactSearchCriteria {
      return getRequireValue("searchCriteria", 0) as ContactSearchCriteria
    }
    
    property set searchCriteria ($arg :  ContactSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    
  }
  
  
}