package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketRelatedEntitiesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TroubleTicketRelatedEntitiesPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketRelatedEntitiesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TroubleTicketRelatedEntitiesPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (troubleTicket :  TroubleTicket, createTroubleTicketHelper :  CreateTroubleTicketHelper) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=TroubleTicketRelatedEntitiesPopup) at TroubleTicketRelatedEntitiesPopup.pcf: line 10, column 89
    function beforeCommit_4 (pickedValue :  java.lang.Object) : void {
      troubleTicket.Hold.checkForHoldReleases(); troubleTicket.Hold.checkForHoldAdditions(); state.onExitUpdateTroubleTicketEntitiesScreen()
    }
    
    // 'def' attribute on PanelRef at TroubleTicketRelatedEntitiesPopup.pcf: line 29, column 89
    function def_onEnter_2 (def :  pcf.TroubleTicketRelatedEntitiesDV) : void {
      def.onEnter(troubleTicket, createTroubleTicketHelper)
    }
    
    // 'def' attribute on PanelRef at TroubleTicketRelatedEntitiesPopup.pcf: line 29, column 89
    function def_refreshVariables_3 (def :  pcf.TroubleTicketRelatedEntitiesDV) : void {
      def.refreshVariables(troubleTicket, createTroubleTicketHelper)
    }
    
    // 'initialValue' attribute on Variable at TroubleTicketRelatedEntitiesPopup.pcf: line 22, column 52
    function initialValue_0 () : gw.troubleticket.TroubleTicketHelper {
      return new gw.troubleticket.TroubleTicketHelper(troubleTicket)
    }
    
    // EditButtons at TroubleTicketRelatedEntitiesPopup.pcf: line 26, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    override property get CurrentLocation () : pcf.TroubleTicketRelatedEntitiesPopup {
      return super.CurrentLocation as pcf.TroubleTicketRelatedEntitiesPopup
    }
    
    property get createTroubleTicketHelper () : CreateTroubleTicketHelper {
      return getVariableValue("createTroubleTicketHelper", 0) as CreateTroubleTicketHelper
    }
    
    property set createTroubleTicketHelper ($arg :  CreateTroubleTicketHelper) {
      setVariableValue("createTroubleTicketHelper", 0, $arg)
    }
    
    property get state () : gw.troubleticket.TroubleTicketHelper {
      return getVariableValue("state", 0) as gw.troubleticket.TroubleTicketHelper
    }
    
    property set state ($arg :  gw.troubleticket.TroubleTicketHelper) {
      setVariableValue("state", 0, $arg)
    }
    
    property get troubleTicket () : TroubleTicket {
      return getVariableValue("troubleTicket", 0) as TroubleTicket
    }
    
    property set troubleTicket ($arg :  TroubleTicket) {
      setVariableValue("troubleTicket", 0, $arg)
    }
    
    
  }
  
  
}