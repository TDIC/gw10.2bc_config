package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/payment/InstallmentTreatmentInputSet.EveryTerm.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InstallmentTreatmentInputSet_EveryTermExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/payment/InstallmentTreatmentInputSet.EveryTerm.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InstallmentTreatmentInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToDownPayment_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 37, column 39
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.DaysFromReferenceDateToDownPayment = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeInput (id=DownPaymentBeforeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 48, column 47
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.BeforeAfterDownPayment = (__VALUE_TO_SET as gw.admin.paymentplan.When)
    }
    
    // 'value' attribute on TextInput (id=MaximumNumberOfInstallments_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 17, column 38
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.MaximumNumberOfInstallments = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleDownPaymentAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 58, column 52
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.DownPaymentAfter = (__VALUE_TO_SET as typekey.PaymentScheduledAfter)
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToFirstInstallment_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 70, column 39
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.DaysFromReferenceDateToFirstInstallment = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeInput (id=FirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 81, column 47
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.BeforeAfterFirstInstallment = (__VALUE_TO_SET as gw.admin.paymentplan.When)
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleFirstInstallmentAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 91, column 52
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.FirstInstallmentAfter = (__VALUE_TO_SET as typekey.PaymentScheduledAfter)
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToOneTimeCharge_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 104, column 39
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.DaysFromReferenceDateToOneTimeCharge = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeInput (id=OneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 115, column 47
    function defaultSetter_55 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.BeforeAfterOneTimeCharge = (__VALUE_TO_SET as gw.admin.paymentplan.When)
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleOneTimeChargeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 125, column 52
    function defaultSetter_63 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.OneTimeChargeAfter = (__VALUE_TO_SET as typekey.PaymentScheduledAfter)
    }
    
    // 'value' attribute on TextInput (id=DownPaymentPercent_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 30, column 43
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.DownPaymentPercent = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'noneSelectedLabel' attribute on TypeKeyInput (id=ScheduleDownPaymentAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 58, column 52
    function noneSelectedLabel_28 () : java.lang.String {
      return viewHelper.NoneSelectedLabel
    }
    
    // 'required' attribute on TextInput (id=MaximumNumberOfInstallments_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 17, column 38
    function required_0 () : java.lang.Boolean {
      return viewHelper.RequiredFieldCalculator.MaximumNumberOfInstallmentsRequired
    }
    
    // 'required' attribute on TextInput (id=DaysFromEventToFirstInstallment_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 70, column 39
    function required_31 () : java.lang.Boolean {
      return viewHelper.RequiredFieldCalculator.FirstInstallmentFieldsRequired
    }
    
    // 'required' attribute on TextInput (id=DaysFromEventToOneTimeCharge_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 104, column 39
    function required_49 () : java.lang.Boolean {
      return viewHelper.RequiredFieldCalculator.OneTimeChargesFieldsRequired
    }
    
    // 'required' attribute on TextInput (id=DownPaymentPercent_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 30, column 43
    function required_6 () : java.lang.Boolean {
      return viewHelper.RequiredFieldCalculator.DownPaymentFieldsRequired
    }
    
    // 'validationExpression' attribute on TextInput (id=DownPaymentPercent_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 30, column 43
    function validationExpression_5 () : java.lang.Object {
      return viewHelper.validateDownPayment()
    }
    
    // 'valueRange' attribute on RangeInput (id=DownPaymentBeforeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 48, column 47
    function valueRange_20 () : java.lang.Object {
      return gw.admin.paymentplan.When.AllValues
    }
    
    // 'valueRange' attribute on RangeInput (id=FirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 81, column 47
    function valueRange_39 () : java.lang.Object {
      return gw.admin.paymentplan.When .AllValues
    }
    
    // 'value' attribute on TextInput (id=MaximumNumberOfInstallments_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 17, column 38
    function valueRoot_3 () : java.lang.Object {
      return viewHelper
    }
    
    // 'value' attribute on TextInput (id=MaximumNumberOfInstallments_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 17, column 38
    function value_1 () : java.lang.Integer {
      return viewHelper.MaximumNumberOfInstallments
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToDownPayment_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 37, column 39
    function value_13 () : java.lang.Integer {
      return viewHelper.DaysFromReferenceDateToDownPayment
    }
    
    // 'value' attribute on RangeInput (id=DownPaymentBeforeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 48, column 47
    function value_17 () : gw.admin.paymentplan.When {
      return viewHelper.BeforeAfterDownPayment
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleDownPaymentAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 58, column 52
    function value_25 () : typekey.PaymentScheduledAfter {
      return viewHelper.DownPaymentAfter
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToFirstInstallment_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 70, column 39
    function value_32 () : java.lang.Integer {
      return viewHelper.DaysFromReferenceDateToFirstInstallment
    }
    
    // 'value' attribute on RangeInput (id=FirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 81, column 47
    function value_36 () : gw.admin.paymentplan.When {
      return viewHelper.BeforeAfterFirstInstallment
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleFirstInstallmentAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 91, column 52
    function value_44 () : typekey.PaymentScheduledAfter {
      return viewHelper.FirstInstallmentAfter
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToOneTimeCharge_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 104, column 39
    function value_50 () : java.lang.Integer {
      return viewHelper.DaysFromReferenceDateToOneTimeCharge
    }
    
    // 'value' attribute on RangeInput (id=OneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 115, column 47
    function value_54 () : gw.admin.paymentplan.When {
      return viewHelper.BeforeAfterOneTimeCharge
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleOneTimeChargeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 125, column 52
    function value_62 () : typekey.PaymentScheduledAfter {
      return viewHelper.OneTimeChargeAfter
    }
    
    // 'value' attribute on TextInput (id=DownPaymentPercent_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 30, column 43
    function value_7 () : java.math.BigDecimal {
      return viewHelper.DownPaymentPercent
    }
    
    // 'valueRange' attribute on RangeInput (id=DownPaymentBeforeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 48, column 47
    function verifyValueRangeIsAllowedType_21 ($$arg :  gw.admin.paymentplan.When[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DownPaymentBeforeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 48, column 47
    function verifyValueRangeIsAllowedType_21 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=FirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 81, column 47
    function verifyValueRangeIsAllowedType_40 ($$arg :  gw.admin.paymentplan.When[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=FirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 81, column 47
    function verifyValueRangeIsAllowedType_40 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 115, column 47
    function verifyValueRangeIsAllowedType_58 ($$arg :  gw.admin.paymentplan.When[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 115, column 47
    function verifyValueRangeIsAllowedType_58 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DownPaymentBeforeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 48, column 47
    function verifyValueRange_22 () : void {
      var __valueRangeArg = gw.admin.paymentplan.When.AllValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_21(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=FirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 81, column 47
    function verifyValueRange_41 () : void {
      var __valueRangeArg = gw.admin.paymentplan.When .AllValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_40(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=OneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.EveryTerm.pcf: line 115, column 47
    function verifyValueRange_59 () : void {
      var __valueRangeArg = gw.admin.paymentplan.When .AllValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_58(__valueRangeArg)
    }
    
    // 'visible' attribute on InputSet at InstallmentTreatmentInputSet.EveryTerm.pcf: line 19, column 199
    function visible_30 () : java.lang.Boolean {
      return viewHelper.FrequencyOfDownPayment != gw.admin.paymentplan.DownPaymentFrequency.NONE and viewHelper.FrequencyOfDownPayment != gw.admin.paymentplan.DownPaymentFrequency.NOT_OVERRIDDEN
    }
    
    property get viewHelper () : gw.admin.paymentplan.InstallmentViewHelper {
      return getRequireValue("viewHelper", 0) as gw.admin.paymentplan.InstallmentViewHelper
    }
    
    property set viewHelper ($arg :  gw.admin.paymentplan.InstallmentViewHelper) {
      setRequireValue("viewHelper", 0, $arg)
    }
    
    
  }
  
  
}