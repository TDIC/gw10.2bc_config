package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadAgencyBillPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanDataUploadAgencyBillPlanLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadAgencyBillPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PlanDataUploadAgencyBillPlanLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at PlanDataUploadAgencyBillPlanLV.pcf: line 17, column 108
    function highlighted_36 () : java.lang.Boolean {
      return (agencyBillPlan.Error or agencyBillPlan.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 23, column 46
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 28, column 41
    function label_16 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.AgencyBillPlan.PublicID")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 34, column 41
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.AgencyBillPlan.PlanName")
    }
    
    // 'label' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 39, column 39
    function label_26 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.AgencyBillPlan.EffectiveDate")
    }
    
    // 'label' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 44, column 39
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.AgencyBillPlan.ExpirationDate")
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 28, column 41
    function valueRoot_18 () : java.lang.Object {
      return agencyBillPlan
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 23, column 46
    function value_12 () : java.lang.String {
      return processor.getLoadStatus(agencyBillPlan)
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 28, column 41
    function value_17 () : java.lang.String {
      return agencyBillPlan.PublicID
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 34, column 41
    function value_22 () : java.lang.String {
      return agencyBillPlan.Name
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 39, column 39
    function value_27 () : java.util.Date {
      return agencyBillPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 44, column 39
    function value_32 () : java.util.Date {
      return agencyBillPlan.ExpirationDate
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 23, column 46
    function visible_13 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get agencyBillPlan () : tdic.util.dataloader.data.plandata.AgencyBillPlanData {
      return getIteratedValue(1) as tdic.util.dataloader.data.plandata.AgencyBillPlanData
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadAgencyBillPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanDataUploadAgencyBillPlanLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 28, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.AgencyBillPlan.PublicID")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 34, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.AgencyBillPlan.PlanName")
    }
    
    // 'label' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 39, column 39
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.AgencyBillPlan.EffectiveDate")
    }
    
    // 'label' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 44, column 39
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.AgencyBillPlan.ExpirationDate")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 23, column 46
    function sortValue_1 (agencyBillPlan :  tdic.util.dataloader.data.plandata.AgencyBillPlanData) : java.lang.Object {
      return processor.getLoadStatus(agencyBillPlan)
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 44, column 39
    function sortValue_10 (agencyBillPlan :  tdic.util.dataloader.data.plandata.AgencyBillPlanData) : java.lang.Object {
      return agencyBillPlan.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 28, column 41
    function sortValue_4 (agencyBillPlan :  tdic.util.dataloader.data.plandata.AgencyBillPlanData) : java.lang.Object {
      return agencyBillPlan.PublicID
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 34, column 41
    function sortValue_6 (agencyBillPlan :  tdic.util.dataloader.data.plandata.AgencyBillPlanData) : java.lang.Object {
      return agencyBillPlan.Name
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 39, column 39
    function sortValue_8 (agencyBillPlan :  tdic.util.dataloader.data.plandata.AgencyBillPlanData) : java.lang.Object {
      return agencyBillPlan.EffectiveDate
    }
    
    // 'value' attribute on RowIterator (id=AgencyBillPlan) at PlanDataUploadAgencyBillPlanLV.pcf: line 15, column 100
    function value_37 () : java.util.ArrayList<tdic.util.dataloader.data.plandata.AgencyBillPlanData> {
      return processor.AgencyBillPlanArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadAgencyBillPlanLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}