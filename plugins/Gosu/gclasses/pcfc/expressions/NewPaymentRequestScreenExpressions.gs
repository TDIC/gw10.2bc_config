package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/NewPaymentRequestScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewPaymentRequestScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/NewPaymentRequestScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewPaymentRequestScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewPaymentRequestScreen.pcf: line 22, column 47
    function def_onEnter_1 (def :  pcf.PaymentRequestDV) : void {
      def.onEnter(paymentRequest)
    }
    
    // 'def' attribute on PanelRef at NewPaymentRequestScreen.pcf: line 22, column 47
    function def_refreshVariables_2 (def :  pcf.PaymentRequestDV) : void {
      def.refreshVariables(paymentRequest)
    }
    
    // 'initialValue' attribute on Variable at NewPaymentRequestScreen.pcf: line 14, column 23
    function initialValue_0 () : Account {
      return paymentRequest.Account
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get payMethod () : PaymentMethod {
      return getVariableValue("payMethod", 0) as PaymentMethod
    }
    
    property set payMethod ($arg :  PaymentMethod) {
      setVariableValue("payMethod", 0, $arg)
    }
    
    property get paymentRequest () : PaymentRequest {
      return getRequireValue("paymentRequest", 0) as PaymentRequest
    }
    
    property set paymentRequest ($arg :  PaymentRequest) {
      setRequireValue("paymentRequest", 0, $arg)
    }
    
    
  }
  
  
}