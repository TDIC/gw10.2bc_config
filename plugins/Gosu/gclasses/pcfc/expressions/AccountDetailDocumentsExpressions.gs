package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailDocuments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailDocumentsExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailDocuments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailDocumentsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=AccountDetailDocuments) at AccountDetailDocuments.pcf: line 9, column 74
    static function canVisit_21 (account :  Account) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctdocview
    }
    
    // 'initialValue' attribute on Variable at AccountDetailDocuments.pcf: line 18, column 52
    function initialValue_0 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // Page (id=AccountDetailDocuments) at AccountDetailDocuments.pcf: line 9, column 74
    static function parent_22 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    // 'visible' attribute on AlertBar (id=DocumentStoreSuspendedWarning) at AccountDetailDocuments.pcf: line 24, column 76
    function visible_1 () : java.lang.Boolean {
      return documentsActionHelper.ShowDocumentStoreSuspendedWarning
    }
    
    // 'visible' attribute on AlertBar (id=IDCSDisabledAlert) at AccountDetailDocuments.pcf: line 28, column 67
    function visible_2 () : java.lang.Boolean {
      return not documentsActionHelper.ContentSourceEnabled
    }
    
    // 'visible' attribute on AlertBar (id=IDCSUnavailableAlert) at AccountDetailDocuments.pcf: line 32, column 71
    function visible_3 () : java.lang.Boolean {
      return documentsActionHelper.ShowContentServerDownWarning
    }
    
    // 'visible' attribute on AlertBar (id=IDMSUnavailableAlert) at AccountDetailDocuments.pcf: line 36, column 72
    function visible_4 () : java.lang.Boolean {
      return documentsActionHelper.ShowMetadataServerDownWarning
    }
    
    override property get CurrentLocation () : pcf.AccountDetailDocuments {
      return super.CurrentLocation as pcf.AccountDetailDocuments
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get documentsActionHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionHelper", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailDocuments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends AccountDetailDocumentsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=RefreshAsyncContent) at AccountDetailDocuments.pcf: line 92, column 109
    function action_16 () : void {
      
    }
    
    // 'available' attribute on ToolbarButton (id=AddDocuments) at AccountDetailDocuments.pcf: line 73, column 46
    function available_12 () : java.lang.Boolean {
      return documentsActionHelper.ContentSourceEnabled
    }
    
    // 'available' attribute on ToolbarButton (id=RefreshAsyncContent) at AccountDetailDocuments.pcf: line 92, column 109
    function available_14 () : java.lang.Boolean {
      return documentsActionHelper.DocumentContentServerAvailable
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=AccountDocuments_DeobsolesceButton) at AccountDetailDocuments.pcf: line 68, column 66
    function available_7 () : java.lang.Boolean {
      return documentsActionHelper.DocumentMetadataActionsAvailable
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=AccountDocuments_DeobsolesceButton) at AccountDetailDocuments.pcf: line 68, column 66
    function checkedRowAction_9 (element :  entity.Document, CheckedValue :  entity.Document) : void {
       CheckedValue.Obsolete = false
    }
    
    // 'def' attribute on MenuItemSetRef at AccountDetailDocuments.pcf: line 75, column 56
    function def_onEnter_10 (def :  pcf.NewDocumentMenuItemSet) : void {
      def.onEnter(account)
    }
    
    // 'def' attribute on PanelRef at AccountDetailDocuments.pcf: line 48, column 73
    function def_onEnter_17 (def :  pcf.DocumentsLV) : void {
      def.onEnter(documentList, documentSearchCriteria, true)
    }
    
    // 'def' attribute on PanelRef at AccountDetailDocuments.pcf: line 46, column 59
    function def_onEnter_5 (def :  pcf.DocumentSearchDV) : void {
      def.onEnter(documentSearchCriteria)
    }
    
    // 'def' attribute on MenuItemSetRef at AccountDetailDocuments.pcf: line 75, column 56
    function def_refreshVariables_11 (def :  pcf.NewDocumentMenuItemSet) : void {
      def.refreshVariables(account)
    }
    
    // 'def' attribute on PanelRef at AccountDetailDocuments.pcf: line 48, column 73
    function def_refreshVariables_18 (def :  pcf.DocumentsLV) : void {
      def.refreshVariables(documentList, documentSearchCriteria, true)
    }
    
    // 'def' attribute on PanelRef at AccountDetailDocuments.pcf: line 46, column 59
    function def_refreshVariables_6 (def :  pcf.DocumentSearchDV) : void {
      def.refreshVariables(documentSearchCriteria)
    }
    
    // 'searchCriteria' attribute on SearchPanel at AccountDetailDocuments.pcf: line 44, column 78
    function searchCriteria_20 () : entity.DocumentSearchCriteria {
      var c = new DocumentSearchCriteria(); c.IncludeObsoletes = false; c.Account = account; c.DateCriterionChoice.DateRangeChoice = null; return c;
    }
    
    // 'search' attribute on SearchPanel at AccountDetailDocuments.pcf: line 44, column 78
    function search_19 () : java.lang.Object {
      return documentSearchCriteria.performSearch(true) as gw.api.database.IQueryBeanResult<Document>
    }
    
    // 'visible' attribute on ToolbarButton (id=AddDocuments) at AccountDetailDocuments.pcf: line 73, column 46
    function visible_13 () : java.lang.Boolean {
      return perm.Document.create
    }
    
    // 'visible' attribute on ToolbarButton (id=RefreshAsyncContent) at AccountDetailDocuments.pcf: line 92, column 109
    function visible_15 () : java.lang.Boolean {
      return documentsActionHelper.isShowAsynchronousRefreshAction(documentList.toTypedArray())
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=AccountDocuments_DeobsolesceButton) at AccountDetailDocuments.pcf: line 68, column 66
    function visible_8 () : java.lang.Boolean {
      return documentSearchCriteria.IncludeObsoletes
    }
    
    property get documentList () : gw.api.database.IQueryBeanResult<Document> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<Document>
    }
    
    property get documentSearchCriteria () : entity.DocumentSearchCriteria {
      return getCriteriaValue(1) as entity.DocumentSearchCriteria
    }
    
    property set documentSearchCriteria ($arg :  entity.DocumentSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}