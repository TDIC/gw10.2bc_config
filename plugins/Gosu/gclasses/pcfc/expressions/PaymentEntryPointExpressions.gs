package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/entrypoints/Payment.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentEntryPointExpressions {
  @javax.annotation.Generated("config/web/pcf/entrypoints/Payment.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on EntryPoint (id=Payment) at Payment.pcf: line 7, column 55
    function location_0 () : pcf.api.Destination {
      return pcf.AccountPaymentForward.createDestination(moneyReceivedID)
    }
    
    property get moneyReceivedID () : String {
      return getVariableValue("moneyReceivedID", 0) as String
    }
    
    property set moneyReceivedID ($arg :  String) {
      setVariableValue("moneyReceivedID", 0, $arg)
    }
    
    
  }
  
  
}