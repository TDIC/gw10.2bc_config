package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/AccountNewWriteoffReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountNewWriteoffReversalWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/AccountNewWriteoffReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountNewWriteoffReversalWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (accountID :  gw.pl.persistence.core.Key) : int {
      return 0
    }
    
    // 'allowNext' attribute on WizardStep (id=Step1) at AccountNewWriteoffReversalWizard.pcf: line 27, column 95
    function allowNext_1 () : java.lang.Boolean {
      return reversal.Writeoff != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=AccountNewWriteoffReversalWizard) at AccountNewWriteoffReversalWizard.pcf: line 11, column 43
    function beforeCommit_6 (pickedValue :  java.lang.Object) : void {
      reversal.reverse()
    }
    
    // 'canVisit' attribute on Wizard (id=AccountNewWriteoffReversalWizard) at AccountNewWriteoffReversalWizard.pcf: line 11, column 43
    static function canVisit_7 (accountID :  gw.pl.persistence.core.Key) : java.lang.Boolean {
      return perm.Transaction.revtxn
    }
    
    // 'initialValue' attribute on Variable at AccountNewWriteoffReversalWizard.pcf: line 20, column 32
    function initialValue_0 () : WriteoffReversal {
      return new WriteoffReversal()
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountNewWriteoffReversalWizard.pcf: line 27, column 95
    function screen_onEnter_2 (def :  pcf.NewWriteoffReversalAccountWriteoffsScreen) : void {
      def.onEnter(reversal, accountID)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountNewWriteoffReversalWizard.pcf: line 32, column 93
    function screen_onEnter_4 (def :  pcf.NewWriteoffReversalConfirmationScreen) : void {
      def.onEnter(reversal)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountNewWriteoffReversalWizard.pcf: line 27, column 95
    function screen_refreshVariables_3 (def :  pcf.NewWriteoffReversalAccountWriteoffsScreen) : void {
      def.refreshVariables(reversal, accountID)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountNewWriteoffReversalWizard.pcf: line 32, column 93
    function screen_refreshVariables_5 (def :  pcf.NewWriteoffReversalConfirmationScreen) : void {
      def.refreshVariables(reversal)
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewWriteoffReversalWizard) at AccountNewWriteoffReversalWizard.pcf: line 11, column 43
    function tabBar_onEnter_8 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewWriteoffReversalWizard) at AccountNewWriteoffReversalWizard.pcf: line 11, column 43
    function tabBar_refreshVariables_9 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AccountNewWriteoffReversalWizard {
      return super.CurrentLocation as pcf.AccountNewWriteoffReversalWizard
    }
    
    property get accountID () : gw.pl.persistence.core.Key {
      return getVariableValue("accountID", 0) as gw.pl.persistence.core.Key
    }
    
    property set accountID ($arg :  gw.pl.persistence.core.Key) {
      setVariableValue("accountID", 0, $arg)
    }
    
    property get reversal () : WriteoffReversal {
      return getVariableValue("reversal", 0) as WriteoffReversal
    }
    
    property set reversal ($arg :  WriteoffReversal) {
      setVariableValue("reversal", 0, $arg)
    }
    
    
  }
  
  
}