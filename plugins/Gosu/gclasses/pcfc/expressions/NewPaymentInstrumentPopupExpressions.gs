package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.system.server.ServerUtil
@javax.annotation.Generated("config/web/pcf/payment/NewPaymentInstrumentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewPaymentInstrumentPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/NewPaymentInstrumentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewPaymentInstrumentPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (paymentMethodOptions :  java.util.List<PaymentMethod>, account :  Account, oneTimeIsEditable :  boolean) : int {
      return 1
    }
    
    static function __constructorIndex (paymentMethodOptions :  java.util.List<PaymentMethod>, account :  Account, oneTimeIsEditable :  boolean, creditCardHandler :  tdic.bc.integ.services.creditcard.TDIC_CreditCardHandler) : int {
      return 3
    }
    
    static function __constructorIndex (paymentMethodOptions :  java.util.List<PaymentMethod>, paymentRequest :  PaymentRequest) : int {
      return 4
    }
    
    static function __constructorIndex (paymentMethodOptions :  java.util.List<PaymentMethod>, producer :  Producer, oneTimeIsEditable :  boolean) : int {
      return 2
    }
    
    static function __constructorIndex (paymentMethodOptions :  java.util.List<PaymentMethod>, oneTimeIsEditable :  boolean) : int {
      return 0
    }
    
    // 'action' attribute on ButtonInput (id=CreditCard_Start_Input) at NewPaymentInstrumentPopup.pcf: line 148, column 123
    function action_58 () : void {
      creditCardHandler.startCreditCardProcess(account, paymentInstrument)
    }
    
    // 'afterEnter' attribute on Popup (id=NewPaymentInstrumentPopup) at NewPaymentInstrumentPopup.pcf: line 15, column 72
    function afterEnter_62 () : void {
      if (creditCardHandler != null) paymentInstrument.CreditCardPayeeEmail_TDIC = account.PrimaryPayer.Contact.EmailAddress1
    }
    
    // 'available' attribute on CheckBoxInput (id=OneTime_Input) at NewPaymentInstrumentPopup.pcf: line 143, column 189
    function available_48 () : java.lang.Boolean {
      return oneTimeIsEditable
    }
    
    // 'beforeCancel' attribute on Popup (id=NewPaymentInstrumentPopup) at NewPaymentInstrumentPopup.pcf: line 15, column 72
    function beforeCancel_63 () : void {
      if (creditCardHandler != null) creditCardHandler.finishCreditCardProcess()
    }
    
    // 'beforeCommit' attribute on Popup (id=NewPaymentInstrumentPopup) at NewPaymentInstrumentPopup.pcf: line 15, column 72
    function beforeCommit_64 (pickedValue :  PaymentInstrument) : void {
      if (creditCardHandler != null) creditCardHandler.populateCreditCardFields(paymentInstrument); handleOneTime()
    }
    
    // 'value' attribute on TypeKeyInput (id=CreditCardType_Input) at NewPaymentInstrumentPopup.pcf: line 78, column 87
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentInstrument.CreditCardType_TDIC = (__VALUE_TO_SET as typekey.CreditCardType_TDIC)
    }
    
    // 'value' attribute on TextInput (id=RecipientEmail_Input) at NewPaymentInstrumentPopup.pcf: line 86, column 87
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentInstrument.CreditCardPayeeEmail_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=BankName_Input) at NewPaymentInstrumentPopup.pcf: line 119, column 54
    function defaultSetter_36 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentInstrument.BankName_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=BankABARoutingNumber_Input) at NewPaymentInstrumentPopup.pcf: line 125, column 66
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentInstrument.BankABARoutingNumber_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=BankAccountNumber_Input) at NewPaymentInstrumentPopup.pcf: line 131, column 63
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentInstrument.BankAccountNumber_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CheckBoxInput (id=OneTime_Input) at NewPaymentInstrumentPopup.pcf: line 143, column 189
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      oneTime = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=PaymentMethod_Input) at NewPaymentInstrumentPopup.pcf: line 68, column 47
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentInstrument.PaymentMethod = (__VALUE_TO_SET as typekey.PaymentMethod)
    }
    
    // 'editable' attribute on TextInput (id=RecipientEmail_Input) at NewPaymentInstrumentPopup.pcf: line 86, column 87
    function editable_18 () : java.lang.Boolean {
      return paymentInstrument.Token == null
    }
    
    // 'editable' attribute on CheckBoxInput (id=OneTime_Input) at NewPaymentInstrumentPopup.pcf: line 143, column 189
    function editable_49 () : java.lang.Boolean {
      return paymentInstrument.PaymentMethod != PaymentMethod.TC_CREDITCARD
    }
    
    // 'initialValue' attribute on Variable at NewPaymentInstrumentPopup.pcf: line 29, column 33
    function initialValue_0 () : PaymentInstrument {
      return creditCardHandler != null ? tdic.bc.integ.services.creditcard.TDIC_PCFHelper.createNewCreditCardPaymentInstrument(account) : new PaymentInstrument()
    }
    
    // 'initialValue' attribute on Variable at NewPaymentInstrumentPopup.pcf: line 45, column 23
    function initialValue_1 () : boolean {
      return initializeOneTime()
    }
    
    // EditButtons at NewPaymentInstrumentPopup.pcf: line 57, column 126
    function label_4 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'pickValue' attribute on EditButtons at NewPaymentInstrumentPopup.pcf: line 57, column 126
    function pickValue_2 () : PaymentInstrument {
      return paymentInstrument
    }
    
    // TemplatePanel at NewPaymentInstrumentPopup.pcf: line 102, column 24
    function renderCall_33 (__writer :  java.io.Writer, __escaper :  gw.lang.parser.template.StringEscaper, __helper :  gw.api.web.template.TemplatePanelHelper) : void {
      pcfc.expressions.NewPaymentInstrumentPopup_TemplatePanel1.render(__writer, __escaper)
    }
    
    // TemplatePanel at NewPaymentInstrumentPopup.pcf: line 157, column 24
    function renderCall_60 (__writer :  java.io.Writer, __escaper :  gw.lang.parser.template.StringEscaper, __helper :  gw.api.web.template.TemplatePanelHelper) : void {
      pcfc.expressions.NewPaymentInstrumentPopup_TemplatePanel2.render(__writer, __escaper)
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentMethod_Input) at NewPaymentInstrumentPopup.pcf: line 68, column 47
    function valueRange_8 () : java.lang.Object {
      return paymentMethodOptions
    }
    
    // 'value' attribute on RangeInput (id=PaymentMethod_Input) at NewPaymentInstrumentPopup.pcf: line 68, column 47
    function valueRoot_7 () : java.lang.Object {
      return paymentInstrument
    }
    
    // 'value' attribute on TypeKeyInput (id=CreditCardType_Input) at NewPaymentInstrumentPopup.pcf: line 78, column 87
    function value_13 () : typekey.CreditCardType_TDIC {
      return paymentInstrument.CreditCardType_TDIC
    }
    
    // 'value' attribute on TextInput (id=RecipientEmail_Input) at NewPaymentInstrumentPopup.pcf: line 86, column 87
    function value_20 () : java.lang.String {
      return paymentInstrument.CreditCardPayeeEmail_TDIC
    }
    
    // 'value' attribute on TextInput (id=Token_Input) at NewPaymentInstrumentPopup.pcf: line 92, column 87
    function value_27 () : java.lang.String {
      return paymentInstrument.Token
    }
    
    // 'value' attribute on HiddenInput (id=AuthorizeUrl_Input) at NewPaymentInstrumentPopup.pcf: line 97, column 43
    function value_31 () : java.lang.String {
      return setAuthUrl()
    }
    
    // 'value' attribute on TextInput (id=BankName_Input) at NewPaymentInstrumentPopup.pcf: line 119, column 54
    function value_35 () : java.lang.String {
      return paymentInstrument.BankName_TDIC
    }
    
    // 'value' attribute on TextInput (id=BankABARoutingNumber_Input) at NewPaymentInstrumentPopup.pcf: line 125, column 66
    function value_39 () : java.lang.String {
      return paymentInstrument.BankABARoutingNumber_TDIC
    }
    
    // 'value' attribute on TextInput (id=BankAccountNumber_Input) at NewPaymentInstrumentPopup.pcf: line 131, column 63
    function value_43 () : java.lang.String {
      return paymentInstrument.BankAccountNumber_TDIC
    }
    
    // 'value' attribute on RangeInput (id=PaymentMethod_Input) at NewPaymentInstrumentPopup.pcf: line 68, column 47
    function value_5 () : typekey.PaymentMethod {
      return paymentInstrument.PaymentMethod
    }
    
    // 'value' attribute on CheckBoxInput (id=OneTime_Input) at NewPaymentInstrumentPopup.pcf: line 143, column 189
    function value_51 () : java.lang.Boolean {
      return oneTime
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentMethod_Input) at NewPaymentInstrumentPopup.pcf: line 68, column 47
    function verifyValueRangeIsAllowedType_9 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentMethod_Input) at NewPaymentInstrumentPopup.pcf: line 68, column 47
    function verifyValueRangeIsAllowedType_9 ($$arg :  typekey.PaymentMethod[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentMethod_Input) at NewPaymentInstrumentPopup.pcf: line 68, column 47
    function verifyValueRange_10 () : void {
      var __valueRangeArg = paymentMethodOptions
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_9(__valueRangeArg)
    }
    
    // 'visible' attribute on TypeKeyInput (id=CreditCardType_Input) at NewPaymentInstrumentPopup.pcf: line 78, column 87
    function visible_12 () : java.lang.Boolean {
      return paymentInstrument.PaymentMethod == PaymentMethod.TC_CREDITCARD
    }
    
    // 'updateVisible' attribute on EditButtons at NewPaymentInstrumentPopup.pcf: line 57, column 126
    function visible_3 () : java.lang.Boolean {
      return paymentInstrument.PaymentMethod != PaymentMethod.TC_CREDITCARD or paymentInstrument.Token != null
    }
    
    // 'visible' attribute on DetailViewPanel at NewPaymentInstrumentPopup.pcf: line 112, column 75
    function visible_47 () : java.lang.Boolean {
      return paymentInstrument.PaymentMethod == PaymentMethod.TC_ACH
    }
    
    // 'visible' attribute on CheckBoxInput (id=OneTime_Input) at NewPaymentInstrumentPopup.pcf: line 143, column 189
    function visible_50 () : java.lang.Boolean {
      return paymentInstrument.PaymentMethod == PaymentMethod.TC_CREDITCARD or (paymentInstrument.PaymentMethod == PaymentMethod.TC_ACH and paymentRequest.IsOneTimePayment_TDIC)
    }
    
    // 'visible' attribute on ButtonInput (id=CreditCard_Start_Input) at NewPaymentInstrumentPopup.pcf: line 148, column 123
    function visible_57 () : java.lang.Boolean {
      return paymentInstrument.PaymentMethod == PaymentMethod.TC_CREDITCARD and paymentInstrument.Token == null
    }
    
    // 'visible' attribute on PanelSet (id=CreditCardDetailsPanelSet) at NewPaymentInstrumentPopup.pcf: line 153, column 118
    function visible_61 () : java.lang.Boolean {
      return paymentInstrument.PaymentMethod == PaymentMethod.TC_CREDITCARD and paymentInstrument.Token != null
    }
    
    override property get CurrentLocation () : pcf.NewPaymentInstrumentPopup {
      return super.CurrentLocation as pcf.NewPaymentInstrumentPopup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get creditCardHandler () : tdic.bc.integ.services.creditcard.TDIC_CreditCardHandler {
      return getVariableValue("creditCardHandler", 0) as tdic.bc.integ.services.creditcard.TDIC_CreditCardHandler
    }
    
    property set creditCardHandler ($arg :  tdic.bc.integ.services.creditcard.TDIC_CreditCardHandler) {
      setVariableValue("creditCardHandler", 0, $arg)
    }
    
    property get oneTime () : boolean {
      return getVariableValue("oneTime", 0) as java.lang.Boolean
    }
    
    property set oneTime ($arg :  boolean) {
      setVariableValue("oneTime", 0, $arg)
    }
    
    property get oneTimeIsEditable () : boolean {
      return getVariableValue("oneTimeIsEditable", 0) as java.lang.Boolean
    }
    
    property set oneTimeIsEditable ($arg :  boolean) {
      setVariableValue("oneTimeIsEditable", 0, $arg)
    }
    
    property get paymentInstrument () : PaymentInstrument {
      return getVariableValue("paymentInstrument", 0) as PaymentInstrument
    }
    
    property set paymentInstrument ($arg :  PaymentInstrument) {
      setVariableValue("paymentInstrument", 0, $arg)
    }
    
    property get paymentMethodOptions () : java.util.List<PaymentMethod> {
      return getVariableValue("paymentMethodOptions", 0) as java.util.List<PaymentMethod>
    }
    
    property set paymentMethodOptions ($arg :  java.util.List<PaymentMethod>) {
      setVariableValue("paymentMethodOptions", 0, $arg)
    }
    
    property get paymentRequest () : PaymentRequest {
      return getVariableValue("paymentRequest", 0) as PaymentRequest
    }
    
    property set paymentRequest ($arg :  PaymentRequest) {
      setVariableValue("paymentRequest", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
        
    function setAuthUrl() : String {
      if(ServerUtil.getEnv().toUpperCase() == "PROD") {
        return "https://accept.authorize.net/customer/addPayment"
      }
      return "https://test.authorize.net/customer/addPayment"
    }
    
    function initializeOneTime() : boolean {
          var initialOneTimeValue = false
          if(paymentRequest != null){
            return paymentRequest.IsOneTimePayment_TDIC
          }
          if (!oneTimeIsEditable && (account == null && producer == null)) {
            initialOneTimeValue = true
          }
          if (creditCardHandler != null) {
            initialOneTimeValue = true
          }
          return initialOneTimeValue
        }
    
        function handleOneTime() {
          if (!oneTime) {
            if (account != null) {
              paymentInstrument.Account = account
            } else if (producer != null) {
              paymentInstrument.Producer = producer
            } else {
              throw DisplayKey.get("Web.NewPaymentInstrument.OneTime.Error")
            }
          }
        }
    
    
  }
  
  
}