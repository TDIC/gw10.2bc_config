package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/invoice/NewReturnPremiumPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewReturnPremiumPlanExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/invoice/NewReturnPremiumPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewReturnPremiumPlanExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=NewReturnPremiumPlan) at NewReturnPremiumPlan.pcf: line 14, column 72
    function afterCancel_3 () : void {
      ReturnPremiumPlans.go()
    }
    
    // 'afterCancel' attribute on Page (id=NewReturnPremiumPlan) at NewReturnPremiumPlan.pcf: line 14, column 72
    function afterCancel_dest_4 () : pcf.api.Destination {
      return pcf.ReturnPremiumPlans.createDestination()
    }
    
    // 'afterCommit' attribute on Page (id=NewReturnPremiumPlan) at NewReturnPremiumPlan.pcf: line 14, column 72
    function afterCommit_5 (TopLocation :  pcf.api.Location) : void {
      ReturnPremiumPlans.go()
    }
    
    // 'canVisit' attribute on Page (id=NewReturnPremiumPlan) at NewReturnPremiumPlan.pcf: line 14, column 72
    static function canVisit_6 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.retpremplancreate
    }
    
    // 'def' attribute on ScreenRef at NewReturnPremiumPlan.pcf: line 22, column 63
    function def_onEnter_1 (def :  pcf.ReturnPremiumPlanDetailScreen) : void {
      def.onEnter(returnPremiumPlan)
    }
    
    // 'def' attribute on ScreenRef at NewReturnPremiumPlan.pcf: line 22, column 63
    function def_refreshVariables_2 (def :  pcf.ReturnPremiumPlanDetailScreen) : void {
      def.refreshVariables(returnPremiumPlan)
    }
    
    // 'initialValue' attribute on Variable at NewReturnPremiumPlan.pcf: line 20, column 33
    function initialValue_0 () : ReturnPremiumPlan {
      return initReturnPremiumPlan()
    }
    
    // 'parent' attribute on Page (id=NewReturnPremiumPlan) at NewReturnPremiumPlan.pcf: line 14, column 72
    static function parent_7 () : pcf.api.Destination {
      return pcf.ReturnPremiumPlans.createDestination()
    }
    
    override property get CurrentLocation () : pcf.NewReturnPremiumPlan {
      return super.CurrentLocation as pcf.NewReturnPremiumPlan
    }
    
    property get returnPremiumPlan () : ReturnPremiumPlan {
      return getVariableValue("returnPremiumPlan", 0) as ReturnPremiumPlan
    }
    
    property set returnPremiumPlan ($arg :  ReturnPremiumPlan) {
      setVariableValue("returnPremiumPlan", 0, $arg)
    }
    
    function initReturnPremiumPlan(): ReturnPremiumPlan {
        var newReturnPremiumPlan = new ReturnPremiumPlan(CurrentLocation)
        newReturnPremiumPlan.EffectiveDate = gw.api.util.DateUtil.currentDate()
        return newReturnPremiumPlan
            }
    
    
  }
  
  
}