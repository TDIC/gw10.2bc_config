package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadChangeLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DataUploadChangeLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadChangeLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DataUploadChangeLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadChangeLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=amount1_Cell) at DataUploadChangeLV.pcf: line 50, column 45
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount1")
    }
    
    // 'label' attribute on TextCell (id=payer1_Cell) at DataUploadChangeLV.pcf: line 55, column 41
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Payer1")
    }
    
    // 'label' attribute on TextCell (id=invoiceStream1_Cell) at DataUploadChangeLV.pcf: line 60, column 41
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.InvoiceStream1")
    }
    
    // 'label' attribute on TextCell (id=charge2_Cell) at DataUploadChangeLV.pcf: line 65, column 41
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge2")
    }
    
    // 'label' attribute on TextCell (id=amount2_Cell) at DataUploadChangeLV.pcf: line 70, column 45
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount2")
    }
    
    // 'label' attribute on TextCell (id=payer2_Cell) at DataUploadChangeLV.pcf: line 75, column 41
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Payer2")
    }
    
    // 'label' attribute on TextCell (id=invoiceStream2_Cell) at DataUploadChangeLV.pcf: line 80, column 41
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.InvoiceStream2")
    }
    
    // 'label' attribute on TextCell (id=charge3_Cell) at DataUploadChangeLV.pcf: line 85, column 41
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge3")
    }
    
    // 'label' attribute on TextCell (id=amount3_Cell) at DataUploadChangeLV.pcf: line 90, column 45
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount3")
    }
    
    // 'label' attribute on TextCell (id=payer3_Cell) at DataUploadChangeLV.pcf: line 95, column 41
    function label_29 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Payer3")
    }
    
    // 'label' attribute on DateCell (id=createDate_Cell) at DataUploadChangeLV.pcf: line 29, column 39
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ModificationDate")
    }
    
    // 'label' attribute on TextCell (id=invoiceStream3_Cell) at DataUploadChangeLV.pcf: line 100, column 41
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.InvoiceStream3")
    }
    
    // 'label' attribute on TextCell (id=charge4_Cell) at DataUploadChangeLV.pcf: line 105, column 41
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge4")
    }
    
    // 'label' attribute on TextCell (id=amount4_Cell) at DataUploadChangeLV.pcf: line 110, column 45
    function label_35 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount4")
    }
    
    // 'label' attribute on TextCell (id=payer4_Cell) at DataUploadChangeLV.pcf: line 115, column 41
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Payer4")
    }
    
    // 'label' attribute on TextCell (id=invoiceStream4_Cell) at DataUploadChangeLV.pcf: line 120, column 41
    function label_39 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.InvoiceStream4")
    }
    
    // 'label' attribute on TextCell (id=policyNumber_Cell) at DataUploadChangeLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.PolicyNumber")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at DataUploadChangeLV.pcf: line 40, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Description")
    }
    
    // 'label' attribute on TextCell (id=charge1_Cell) at DataUploadChangeLV.pcf: line 45, column 41
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge1")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadChangeLV.pcf: line 23, column 46
    function sortValue_1 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return processor.getLoadStatus(change)
    }
    
    // 'value' attribute on TextCell (id=charge1_Cell) at DataUploadChangeLV.pcf: line 45, column 41
    function sortValue_10 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.Charges[0].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount1_Cell) at DataUploadChangeLV.pcf: line 50, column 45
    function sortValue_12 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.Charges[0].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=payer1_Cell) at DataUploadChangeLV.pcf: line 55, column 41
    function sortValue_14 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.Charges[0].Payer
    }
    
    // 'value' attribute on TextCell (id=invoiceStream1_Cell) at DataUploadChangeLV.pcf: line 60, column 41
    function sortValue_16 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.Charges[0].InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=charge2_Cell) at DataUploadChangeLV.pcf: line 65, column 41
    function sortValue_18 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.Charges[1].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount2_Cell) at DataUploadChangeLV.pcf: line 70, column 45
    function sortValue_20 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.Charges[1].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=payer2_Cell) at DataUploadChangeLV.pcf: line 75, column 41
    function sortValue_22 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.Charges[1].Payer
    }
    
    // 'value' attribute on TextCell (id=invoiceStream2_Cell) at DataUploadChangeLV.pcf: line 80, column 41
    function sortValue_24 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.Charges[1].InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=charge3_Cell) at DataUploadChangeLV.pcf: line 85, column 41
    function sortValue_26 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.Charges[2].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount3_Cell) at DataUploadChangeLV.pcf: line 90, column 45
    function sortValue_28 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.Charges[2].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=payer3_Cell) at DataUploadChangeLV.pcf: line 95, column 41
    function sortValue_30 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.Charges[2].Payer
    }
    
    // 'value' attribute on TextCell (id=invoiceStream3_Cell) at DataUploadChangeLV.pcf: line 100, column 41
    function sortValue_32 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.Charges[2].InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=charge4_Cell) at DataUploadChangeLV.pcf: line 105, column 41
    function sortValue_34 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.Charges[3].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount4_Cell) at DataUploadChangeLV.pcf: line 110, column 45
    function sortValue_36 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.Charges[3].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=payer4_Cell) at DataUploadChangeLV.pcf: line 115, column 41
    function sortValue_38 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.Charges[3].Payer
    }
    
    // 'value' attribute on DateCell (id=createDate_Cell) at DataUploadChangeLV.pcf: line 29, column 39
    function sortValue_4 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.EntryDate
    }
    
    // 'value' attribute on TextCell (id=invoiceStream4_Cell) at DataUploadChangeLV.pcf: line 120, column 41
    function sortValue_40 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.Charges[3].InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=policyNumber_Cell) at DataUploadChangeLV.pcf: line 35, column 41
    function sortValue_6 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.AssociatedPolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at DataUploadChangeLV.pcf: line 40, column 41
    function sortValue_8 (change :  tdic.util.dataloader.data.sampledata.PolicyChangeData) : java.lang.Object {
      return change.Description
    }
    
    // 'value' attribute on RowIterator (id=changeID) at DataUploadChangeLV.pcf: line 15, column 100
    function value_126 () : java.util.ArrayList<tdic.util.dataloader.data.sampledata.PolicyChangeData> {
      return processor.PolicyChangeArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadChangeLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCSampleDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCSampleDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadChangeLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DataUploadChangeLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at DataUploadChangeLV.pcf: line 17, column 92
    function highlighted_125 () : java.lang.Boolean {
      return (change.Error or change.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=payer3_Cell) at DataUploadChangeLV.pcf: line 95, column 41
    function label_101 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Payer3")
    }
    
    // 'label' attribute on TextCell (id=invoiceStream3_Cell) at DataUploadChangeLV.pcf: line 100, column 41
    function label_105 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.InvoiceStream3")
    }
    
    // 'label' attribute on TextCell (id=charge4_Cell) at DataUploadChangeLV.pcf: line 105, column 41
    function label_109 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge4")
    }
    
    // 'label' attribute on TextCell (id=amount4_Cell) at DataUploadChangeLV.pcf: line 110, column 45
    function label_113 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount4")
    }
    
    // 'label' attribute on TextCell (id=payer4_Cell) at DataUploadChangeLV.pcf: line 115, column 41
    function label_117 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Payer4")
    }
    
    // 'label' attribute on TextCell (id=invoiceStream4_Cell) at DataUploadChangeLV.pcf: line 120, column 41
    function label_121 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.InvoiceStream4")
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadChangeLV.pcf: line 23, column 46
    function label_41 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on DateCell (id=createDate_Cell) at DataUploadChangeLV.pcf: line 29, column 39
    function label_46 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ModificationDate")
    }
    
    // 'label' attribute on TextCell (id=policyNumber_Cell) at DataUploadChangeLV.pcf: line 35, column 41
    function label_51 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.PolicyNumber")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at DataUploadChangeLV.pcf: line 40, column 41
    function label_56 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Description")
    }
    
    // 'label' attribute on TextCell (id=charge1_Cell) at DataUploadChangeLV.pcf: line 45, column 41
    function label_61 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge1")
    }
    
    // 'label' attribute on TextCell (id=amount1_Cell) at DataUploadChangeLV.pcf: line 50, column 45
    function label_65 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount1")
    }
    
    // 'label' attribute on TextCell (id=payer1_Cell) at DataUploadChangeLV.pcf: line 55, column 41
    function label_69 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Payer1")
    }
    
    // 'label' attribute on TextCell (id=invoiceStream1_Cell) at DataUploadChangeLV.pcf: line 60, column 41
    function label_73 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.InvoiceStream1")
    }
    
    // 'label' attribute on TextCell (id=charge2_Cell) at DataUploadChangeLV.pcf: line 65, column 41
    function label_77 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge2")
    }
    
    // 'label' attribute on TextCell (id=amount2_Cell) at DataUploadChangeLV.pcf: line 70, column 45
    function label_81 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount2")
    }
    
    // 'label' attribute on TextCell (id=payer2_Cell) at DataUploadChangeLV.pcf: line 75, column 41
    function label_85 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Payer2")
    }
    
    // 'label' attribute on TextCell (id=invoiceStream2_Cell) at DataUploadChangeLV.pcf: line 80, column 41
    function label_89 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.InvoiceStream2")
    }
    
    // 'label' attribute on TextCell (id=charge3_Cell) at DataUploadChangeLV.pcf: line 85, column 41
    function label_93 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge3")
    }
    
    // 'label' attribute on TextCell (id=amount3_Cell) at DataUploadChangeLV.pcf: line 90, column 45
    function label_97 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount3")
    }
    
    // 'value' attribute on DateCell (id=createDate_Cell) at DataUploadChangeLV.pcf: line 29, column 39
    function valueRoot_48 () : java.lang.Object {
      return change
    }
    
    // 'value' attribute on TextCell (id=payer3_Cell) at DataUploadChangeLV.pcf: line 95, column 41
    function value_102 () : java.lang.String {
      return change.Charges[2].Payer
    }
    
    // 'value' attribute on TextCell (id=invoiceStream3_Cell) at DataUploadChangeLV.pcf: line 100, column 41
    function value_106 () : java.lang.String {
      return change.Charges[2].InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=charge4_Cell) at DataUploadChangeLV.pcf: line 105, column 41
    function value_110 () : java.lang.String {
      return change.Charges[3].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount4_Cell) at DataUploadChangeLV.pcf: line 110, column 45
    function value_114 () : java.math.BigDecimal {
      return change.Charges[3].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=payer4_Cell) at DataUploadChangeLV.pcf: line 115, column 41
    function value_118 () : java.lang.String {
      return change.Charges[3].Payer
    }
    
    // 'value' attribute on TextCell (id=invoiceStream4_Cell) at DataUploadChangeLV.pcf: line 120, column 41
    function value_122 () : java.lang.String {
      return change.Charges[3].InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadChangeLV.pcf: line 23, column 46
    function value_42 () : java.lang.String {
      return processor.getLoadStatus(change)
    }
    
    // 'value' attribute on DateCell (id=createDate_Cell) at DataUploadChangeLV.pcf: line 29, column 39
    function value_47 () : java.util.Date {
      return change.EntryDate
    }
    
    // 'value' attribute on TextCell (id=policyNumber_Cell) at DataUploadChangeLV.pcf: line 35, column 41
    function value_52 () : java.lang.String {
      return change.AssociatedPolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at DataUploadChangeLV.pcf: line 40, column 41
    function value_57 () : java.lang.String {
      return change.Description
    }
    
    // 'value' attribute on TextCell (id=charge1_Cell) at DataUploadChangeLV.pcf: line 45, column 41
    function value_62 () : java.lang.String {
      return change.Charges[0].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount1_Cell) at DataUploadChangeLV.pcf: line 50, column 45
    function value_66 () : java.math.BigDecimal {
      return change.Charges[0].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=payer1_Cell) at DataUploadChangeLV.pcf: line 55, column 41
    function value_70 () : java.lang.String {
      return change.Charges[0].Payer
    }
    
    // 'value' attribute on TextCell (id=invoiceStream1_Cell) at DataUploadChangeLV.pcf: line 60, column 41
    function value_74 () : java.lang.String {
      return change.Charges[0].InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=charge2_Cell) at DataUploadChangeLV.pcf: line 65, column 41
    function value_78 () : java.lang.String {
      return change.Charges[1].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount2_Cell) at DataUploadChangeLV.pcf: line 70, column 45
    function value_82 () : java.math.BigDecimal {
      return change.Charges[1].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=payer2_Cell) at DataUploadChangeLV.pcf: line 75, column 41
    function value_86 () : java.lang.String {
      return change.Charges[1].Payer
    }
    
    // 'value' attribute on TextCell (id=invoiceStream2_Cell) at DataUploadChangeLV.pcf: line 80, column 41
    function value_90 () : java.lang.String {
      return change.Charges[1].InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=charge3_Cell) at DataUploadChangeLV.pcf: line 85, column 41
    function value_94 () : java.lang.String {
      return change.Charges[2].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount3_Cell) at DataUploadChangeLV.pcf: line 90, column 45
    function value_98 () : java.math.BigDecimal {
      return change.Charges[2].ChargeAmount
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadChangeLV.pcf: line 23, column 46
    function visible_43 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get change () : tdic.util.dataloader.data.sampledata.PolicyChangeData {
      return getIteratedValue(1) as tdic.util.dataloader.data.sampledata.PolicyChangeData
    }
    
    
  }
  
  
}