package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadDirectPayLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DataUploadDirectPayLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadDirectPayLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DataUploadDirectPayLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadDirectPayLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TypeKeyCell (id=payMethod_Cell) at DataUploadDirectPayLV.pcf: line 50, column 46
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.PaymentMethod")
    }
    
    // 'label' attribute on TextCell (id=amount_Cell) at DataUploadDirectPayLV.pcf: line 56, column 45
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.Amount")
    }
    
    // 'label' attribute on DateCell (id=receiveDate_Cell) at DataUploadDirectPayLV.pcf: line 29, column 39
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.ReceivedDate")
    }
    
    // 'label' attribute on TextCell (id=accountName_Cell) at DataUploadDirectPayLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.AccountName")
    }
    
    // 'label' attribute on TextCell (id=policynumber_Cell) at DataUploadDirectPayLV.pcf: line 40, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.PolicyNumber")
    }
    
    // 'label' attribute on TextCell (id=reference_Cell) at DataUploadDirectPayLV.pcf: line 45, column 41
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.Description")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadDirectPayLV.pcf: line 23, column 46
    function sortValue_1 (directPayment :  tdic.util.dataloader.data.sampledata.DirectPaymentData) : java.lang.Object {
      return processor.getLoadStatus(directPayment)
    }
    
    // 'value' attribute on TextCell (id=reference_Cell) at DataUploadDirectPayLV.pcf: line 45, column 41
    function sortValue_10 (directPayment :  tdic.util.dataloader.data.sampledata.DirectPaymentData) : java.lang.Object {
      return directPayment.Description
    }
    
    // 'value' attribute on TypeKeyCell (id=payMethod_Cell) at DataUploadDirectPayLV.pcf: line 50, column 46
    function sortValue_12 (directPayment :  tdic.util.dataloader.data.sampledata.DirectPaymentData) : java.lang.Object {
      return directPayment.PaymentMethod
    }
    
    // 'value' attribute on TextCell (id=amount_Cell) at DataUploadDirectPayLV.pcf: line 56, column 45
    function sortValue_14 (directPayment :  tdic.util.dataloader.data.sampledata.DirectPaymentData) : java.lang.Object {
      return directPayment.Amount
    }
    
    // 'value' attribute on DateCell (id=receiveDate_Cell) at DataUploadDirectPayLV.pcf: line 29, column 39
    function sortValue_4 (directPayment :  tdic.util.dataloader.data.sampledata.DirectPaymentData) : java.lang.Object {
      return directPayment.EntryDate
    }
    
    // 'value' attribute on TextCell (id=accountName_Cell) at DataUploadDirectPayLV.pcf: line 35, column 41
    function sortValue_6 (directPayment :  tdic.util.dataloader.data.sampledata.DirectPaymentData) : java.lang.Object {
      return directPayment.AccountName
    }
    
    // 'value' attribute on TextCell (id=policynumber_Cell) at DataUploadDirectPayLV.pcf: line 40, column 41
    function sortValue_8 (directPayment :  tdic.util.dataloader.data.sampledata.DirectPaymentData) : java.lang.Object {
      return directPayment.PolicyNumber
    }
    
    // 'value' attribute on RowIterator (id=directPaymentID) at DataUploadDirectPayLV.pcf: line 15, column 101
    function value_51 () : java.util.ArrayList<tdic.util.dataloader.data.sampledata.DirectPaymentData> {
      return processor.DirectPaymentArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadDirectPayLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCSampleDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCSampleDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadDirectPayLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DataUploadDirectPayLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at DataUploadDirectPayLV.pcf: line 17, column 106
    function highlighted_50 () : java.lang.Boolean {
      return (directPayment.Error or directPayment.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadDirectPayLV.pcf: line 23, column 46
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on DateCell (id=receiveDate_Cell) at DataUploadDirectPayLV.pcf: line 29, column 39
    function label_20 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.ReceivedDate")
    }
    
    // 'label' attribute on TextCell (id=accountName_Cell) at DataUploadDirectPayLV.pcf: line 35, column 41
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.AccountName")
    }
    
    // 'label' attribute on TextCell (id=policynumber_Cell) at DataUploadDirectPayLV.pcf: line 40, column 41
    function label_30 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.PolicyNumber")
    }
    
    // 'label' attribute on TextCell (id=reference_Cell) at DataUploadDirectPayLV.pcf: line 45, column 41
    function label_35 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.Description")
    }
    
    // 'label' attribute on TypeKeyCell (id=payMethod_Cell) at DataUploadDirectPayLV.pcf: line 50, column 46
    function label_40 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.PaymentMethod")
    }
    
    // 'label' attribute on TextCell (id=amount_Cell) at DataUploadDirectPayLV.pcf: line 56, column 45
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.Amount")
    }
    
    // 'value' attribute on DateCell (id=receiveDate_Cell) at DataUploadDirectPayLV.pcf: line 29, column 39
    function valueRoot_22 () : java.lang.Object {
      return directPayment
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadDirectPayLV.pcf: line 23, column 46
    function value_16 () : java.lang.String {
      return processor.getLoadStatus(directPayment)
    }
    
    // 'value' attribute on DateCell (id=receiveDate_Cell) at DataUploadDirectPayLV.pcf: line 29, column 39
    function value_21 () : java.util.Date {
      return directPayment.EntryDate
    }
    
    // 'value' attribute on TextCell (id=accountName_Cell) at DataUploadDirectPayLV.pcf: line 35, column 41
    function value_26 () : java.lang.String {
      return directPayment.AccountName
    }
    
    // 'value' attribute on TextCell (id=policynumber_Cell) at DataUploadDirectPayLV.pcf: line 40, column 41
    function value_31 () : java.lang.String {
      return directPayment.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=reference_Cell) at DataUploadDirectPayLV.pcf: line 45, column 41
    function value_36 () : java.lang.String {
      return directPayment.Description
    }
    
    // 'value' attribute on TypeKeyCell (id=payMethod_Cell) at DataUploadDirectPayLV.pcf: line 50, column 46
    function value_41 () : typekey.PaymentMethod {
      return directPayment.PaymentMethod
    }
    
    // 'value' attribute on TextCell (id=amount_Cell) at DataUploadDirectPayLV.pcf: line 56, column 45
    function value_46 () : java.math.BigDecimal {
      return directPayment.Amount
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadDirectPayLV.pcf: line 23, column 46
    function visible_17 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get directPayment () : tdic.util.dataloader.data.sampledata.DirectPaymentData {
      return getIteratedValue(1) as tdic.util.dataloader.data.sampledata.DirectPaymentData
    }
    
    
  }
  
  
}