package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/ViewNotesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ViewNotesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/ViewNotesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ViewNotesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=ViewDetails_Cell) at ViewNotesLV.pcf: line 47, column 72
    function action_11 () : void {
      NoteDetailsPopup.push(noteItem)
    }
    
    // 'action' attribute on TextCell (id=ViewDetails_Cell) at ViewNotesLV.pcf: line 47, column 72
    function action_dest_12 () : pcf.api.Destination {
      return pcf.NoteDetailsPopup.createDestination(noteItem)
    }
    
    // 'value' attribute on DateCell (id=AuthoringDate_Cell) at ViewNotesLV.pcf: line 25, column 43
    function valueRoot_1 () : java.lang.Object {
      return noteItem
    }
    
    // 'value' attribute on DateCell (id=AuthoringDate_Cell) at ViewNotesLV.pcf: line 25, column 43
    function value_0 () : java.util.Date {
      return noteItem.AuthoringDate
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at ViewNotesLV.pcf: line 31, column 36
    function value_3 () : entity.User {
      return noteItem.Author
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at ViewNotesLV.pcf: line 36, column 37
    function value_6 () : java.lang.String {
      return noteItem.Subject
    }
    
    // 'value' attribute on TextCell (id=Body_Cell) at ViewNotesLV.pcf: line 41, column 52
    function value_9 () : java.lang.String {
      return noteItem.getTruncatedBodyText()
    }
    
    property get noteItem () : entity.Note {
      return getIteratedValue(1) as entity.Note
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/troubleticket/ViewNotesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ViewNotesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RowIterator at ViewNotesLV.pcf: line 19, column 71
    function value_13 () : gw.api.database.IQueryBeanResult<entity.Note> {
      return NoteList
    }
    
    property get NoteList () : gw.api.database.IQueryBeanResult<Note> {
      return getRequireValue("NoteList", 0) as gw.api.database.IQueryBeanResult<Note>
    }
    
    property set NoteList ($arg :  gw.api.database.IQueryBeanResult<Note>) {
      setRequireValue("NoteList", 0, $arg)
    }
    
    
  }
  
  
}