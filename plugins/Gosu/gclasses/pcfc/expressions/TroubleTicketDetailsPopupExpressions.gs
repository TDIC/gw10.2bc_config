package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TroubleTicketDetailsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TroubleTicketDetailsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (troubleTicket :  TroubleTicket) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=TroubleTicketDetailsPopup_EditDetailsButton) at TroubleTicketDetailsPopup.pcf: line 30, column 90
    function action_3 () : void {
      TroubleTicketInfoPopup.push(troubleTicket, assigneeHolder)
    }
    
    // 'action' attribute on ToolbarButton (id=TroubleTicketDetailsPopup_CloseButton) at TroubleTicketDetailsPopup.pcf: line 38, column 46
    function action_7 () : void {
      closeTicket()
    }
    
    // 'action' attribute on ToolbarButton (id=TroubleTicketDetailsPopup_EditDetailsButton) at TroubleTicketDetailsPopup.pcf: line 30, column 90
    function action_dest_4 () : pcf.api.Destination {
      return pcf.TroubleTicketInfoPopup.createDestination(troubleTicket, assigneeHolder)
    }
    
    // 'available' attribute on ToolbarButton (id=TroubleTicketDetailsPopup_EditDetailsButton) at TroubleTicketDetailsPopup.pcf: line 30, column 90
    function available_2 () : java.lang.Boolean {
      return !troubleTicket.IsClosed
    }
    
    // 'confirmMessage' attribute on ToolbarButton (id=TroubleTicketDetailsPopup_CloseButton) at TroubleTicketDetailsPopup.pcf: line 38, column 46
    function confirmMessage_8 () : java.lang.String {
      return gw.api.web.troubleticket.TroubleTicketUtil.getCloseTicketConfirmationMessage(troubleTicket)
    }
    
    // 'def' attribute on PanelRef at TroubleTicketDetailsPopup.pcf: line 43, column 45
    function def_onEnter_11 (def :  pcf.TroubleTicketInfoDV) : void {
      def.onEnter(troubleTicket, assigneeHolder)
    }
    
    // 'def' attribute on PanelRef at TroubleTicketDetailsPopup.pcf: line 45, column 86
    function def_onEnter_13 (def :  pcf.TroubleTicketTabbedDetailDV) : void {
      def.onEnter(troubleTicket, createTroubleTicketHelper)
    }
    
    // 'def' attribute on PanelRef at TroubleTicketDetailsPopup.pcf: line 43, column 45
    function def_refreshVariables_12 (def :  pcf.TroubleTicketInfoDV) : void {
      def.refreshVariables(troubleTicket, assigneeHolder)
    }
    
    // 'def' attribute on PanelRef at TroubleTicketDetailsPopup.pcf: line 45, column 86
    function def_refreshVariables_14 (def :  pcf.TroubleTicketTabbedDetailDV) : void {
      def.refreshVariables(troubleTicket, createTroubleTicketHelper)
    }
    
    // 'initialValue' attribute on Variable at TroubleTicketDetailsPopup.pcf: line 17, column 41
    function initialValue_0 () : CreateTroubleTicketHelper {
      return new CreateTroubleTicketHelper()
    }
    
    // 'initialValue' attribute on Variable at TroubleTicketDetailsPopup.pcf: line 21, column 44
    function initialValue_1 () : gw.api.assignment.Assignee[] {
      return troubleTicket.InitialAssigneeForPicker
    }
    
    override property get CurrentLocation () : pcf.TroubleTicketDetailsPopup {
      return super.CurrentLocation as pcf.TroubleTicketDetailsPopup
    }
    
    property get assigneeHolder () : gw.api.assignment.Assignee[] {
      return getVariableValue("assigneeHolder", 0) as gw.api.assignment.Assignee[]
    }
    
    property set assigneeHolder ($arg :  gw.api.assignment.Assignee[]) {
      setVariableValue("assigneeHolder", 0, $arg)
    }
    
    property get createTroubleTicketHelper () : CreateTroubleTicketHelper {
      return getVariableValue("createTroubleTicketHelper", 0) as CreateTroubleTicketHelper
    }
    
    property set createTroubleTicketHelper ($arg :  CreateTroubleTicketHelper) {
      setVariableValue("createTroubleTicketHelper", 0, $arg)
    }
    
    property get troubleTicket () : TroubleTicket {
      return getVariableValue("troubleTicket", 0) as TroubleTicket
    }
    
    property set troubleTicket ($arg :  TroubleTicket) {
      setVariableValue("troubleTicket", 0, $arg)
    }
    
    
    function closeTicket(){
            CurrentLocation.startEditing();
            // TODO : Put some exception handling here to display a user-readable message in the UI if the next line throws an exception
            troubleTicket.close();
            CurrentLocation.commit();
          }
        
    
    
  }
  
  
}