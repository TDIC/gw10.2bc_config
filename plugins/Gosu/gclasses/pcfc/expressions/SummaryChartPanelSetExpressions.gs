package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/summary/SummaryChartPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SummaryChartPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/summary/SummaryChartPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SummaryChartPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'categoryLabel' attribute on DataSeries at SummaryChartPanelSet.pcf: line 31, column 35
    function categoryLabel_1 (value :  gw.web.summary.SummaryChartEntry) : java.lang.String {
      return value.Label
    }
    
    // 'dataValues' attribute on DataSeries at SummaryChartPanelSet.pcf: line 31, column 35
    function dataValues_2 () : java.lang.Object {
      return summaryChartHelper.ChartEntries
    }
    
    // 'initialValue' attribute on Variable at SummaryChartPanelSet.pcf: line 15, column 49
    function initialValue_0 () : gw.web.summary.SummaryChartHelper {
      return new gw.web.summary.SummaryChartHelper(summaryChartEntries)
    }
    
    // 'strokeColor' attribute on DataSeries at SummaryChartPanelSet.pcf: line 31, column 35
    function strokeColor_4 () : List<gw.api.web.color.GWColor> {
      return summaryChartEntries.map(\elt -> elt.Color)
    }
    
    // 'value' attribute on DataSeries at SummaryChartPanelSet.pcf: line 31, column 35
    function value_3 (value :  gw.web.summary.SummaryChartEntry) : java.lang.Object {
      return value.ChartValue
    }
    
    // 'visible' attribute on ChartPanel (id=ValueChart) at SummaryChartPanelSet.pcf: line 24, column 19
    function visible_6 () : java.lang.Boolean {
      return summaryChartEntries.countWhere( \ elt -> gw.api.util.BCBigDecimalUtil.isNegative(elt.ChartValue)) == 0
    }
    
    property get summaryChartEntries () : List<gw.web.summary.SummaryChartEntry> {
      return getRequireValue("summaryChartEntries", 0) as List<gw.web.summary.SummaryChartEntry>
    }
    
    property set summaryChartEntries ($arg :  List<gw.web.summary.SummaryChartEntry>) {
      setRequireValue("summaryChartEntries", 0, $arg)
    }
    
    property get summaryChartHelper () : gw.web.summary.SummaryChartHelper {
      return getVariableValue("summaryChartHelper", 0) as gw.web.summary.SummaryChartHelper
    }
    
    property set summaryChartHelper ($arg :  gw.web.summary.SummaryChartHelper) {
      setVariableValue("summaryChartHelper", 0, $arg)
    }
    
    
  }
  
  
}