package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/invoice/ReturnPremiumPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ReturnPremiumPlansExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/invoice/ReturnPremiumPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ReturnPremiumPlansExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at ReturnPremiumPlans.pcf: line 44, column 47
    function action_10 () : void {
      ReturnPremiumPlanDetail.go(returnPremiumPlan)
    }
    
    // 'action' attribute on Link (id=MoveUp) at ReturnPremiumPlans.pcf: line 68, column 63
    function action_26 () : void {
      PlanHelper.moveUp(returnPremiumPlan)
    }
    
    // 'action' attribute on Link (id=MoveDown) at ReturnPremiumPlans.pcf: line 76, column 69
    function action_29 () : void {
      PlanHelper.moveDown(returnPremiumPlan)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at ReturnPremiumPlans.pcf: line 44, column 47
    function action_dest_11 () : pcf.api.Destination {
      return pcf.ReturnPremiumPlanDetail.createDestination(returnPremiumPlan)
    }
    
    // 'available' attribute on Link (id=MoveUp) at ReturnPremiumPlans.pcf: line 68, column 63
    function available_24 () : java.lang.Boolean {
      return perm.System.retpremplanedit
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at ReturnPremiumPlans.pcf: line 38, column 46
    function valueRoot_8 () : java.lang.Object {
      return returnPremiumPlan
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at ReturnPremiumPlans.pcf: line 44, column 47
    function value_12 () : java.lang.String {
      return returnPremiumPlan.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at ReturnPremiumPlans.pcf: line 49, column 54
    function value_15 () : java.lang.String {
      return returnPremiumPlan.Description
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at ReturnPremiumPlans.pcf: line 53, column 56
    function value_18 () : java.util.Date {
      return returnPremiumPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at ReturnPremiumPlans.pcf: line 57, column 57
    function value_21 () : java.util.Date {
      return returnPremiumPlan.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at ReturnPremiumPlans.pcf: line 38, column 46
    function value_7 () : java.lang.Integer {
      return returnPremiumPlan.PlanOrder
    }
    
    // 'visible' attribute on Link (id=MoveUp) at ReturnPremiumPlans.pcf: line 68, column 63
    function visible_25 () : java.lang.Boolean {
      return returnPremiumPlan.PlanOrder > 1
    }
    
    // 'visible' attribute on Link (id=MoveDown) at ReturnPremiumPlans.pcf: line 76, column 69
    function visible_28 () : java.lang.Boolean {
      return !returnPremiumPlan.hasHighestPlanOrder()
    }
    
    property get returnPremiumPlan () : entity.ReturnPremiumPlan {
      return getIteratedValue(1) as entity.ReturnPremiumPlan
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/invoice/ReturnPremiumPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ReturnPremiumPlansExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=ReturnPremiumPlans) at ReturnPremiumPlans.pcf: line 9, column 70
    static function canVisit_31 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.retpremplanview
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at ReturnPremiumPlans.pcf: line 29, column 80
    function filters_1 () : gw.api.filters.IFilter[] {
      return new gw.api.web.plan.PlanListFilterSet().FilterOptions
    }
    
    // 'initialValue' attribute on Variable at ReturnPremiumPlans.pcf: line 13, column 42
    function initialValue_0 () : gw.api.web.plan.PlanHelper {
      return new gw.api.web.plan.PlanHelper()
    }
    
    // Page (id=ReturnPremiumPlans) at ReturnPremiumPlans.pcf: line 9, column 70
    static function parent_32 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at ReturnPremiumPlans.pcf: line 38, column 46
    function sortValue_2 (returnPremiumPlan :  entity.ReturnPremiumPlan) : java.lang.Object {
      return returnPremiumPlan.PlanOrder
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at ReturnPremiumPlans.pcf: line 44, column 47
    function sortValue_3 (returnPremiumPlan :  entity.ReturnPremiumPlan) : java.lang.Object {
      return returnPremiumPlan.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at ReturnPremiumPlans.pcf: line 49, column 54
    function sortValue_4 (returnPremiumPlan :  entity.ReturnPremiumPlan) : java.lang.Object {
      return returnPremiumPlan.Description
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at ReturnPremiumPlans.pcf: line 53, column 56
    function sortValue_5 (returnPremiumPlan :  entity.ReturnPremiumPlan) : java.lang.Object {
      return returnPremiumPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at ReturnPremiumPlans.pcf: line 57, column 57
    function sortValue_6 (returnPremiumPlan :  entity.ReturnPremiumPlan) : java.lang.Object {
      return returnPremiumPlan.ExpirationDate
    }
    
    // 'value' attribute on RowIterator at ReturnPremiumPlans.pcf: line 24, column 88
    function value_30 () : gw.api.database.IQueryBeanResult<entity.ReturnPremiumPlan> {
      return gw.api.database.Query.make(ReturnPremiumPlan).select()
    }
    
    override property get CurrentLocation () : pcf.ReturnPremiumPlans {
      return super.CurrentLocation as pcf.ReturnPremiumPlans
    }
    
    property get PlanHelper () : gw.api.web.plan.PlanHelper {
      return getVariableValue("PlanHelper", 0) as gw.api.web.plan.PlanHelper
    }
    
    property set PlanHelper ($arg :  gw.api.web.plan.PlanHelper) {
      setVariableValue("PlanHelper", 0, $arg)
    }
    
    
  }
  
  
}