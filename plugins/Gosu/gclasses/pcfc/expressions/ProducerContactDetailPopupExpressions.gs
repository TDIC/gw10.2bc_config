package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerContactDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerContactDetailPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerContactDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerContactDetailPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (contact :  ProducerContact) : int {
      return 0
    }
    
    // 'canVisit' attribute on Popup (id=ProducerContactDetailPopup) at ProducerContactDetailPopup.pcf: line 11, column 73
    static function canVisit_4 (contact :  ProducerContact) : java.lang.Boolean {
      return perm.ProducerContact.edit
    }
    
    // 'def' attribute on PanelRef at ProducerContactDetailPopup.pcf: line 25, column 27
    function def_onEnter_2 (def :  pcf.ProducerContactDetailDV) : void {
      def.onEnter(contact, true)
    }
    
    // 'def' attribute on PanelRef at ProducerContactDetailPopup.pcf: line 25, column 27
    function def_refreshVariables_3 (def :  pcf.ProducerContactDetailDV) : void {
      def.refreshVariables(contact, true)
    }
    
    // EditButtons at ProducerContactDetailPopup.pcf: line 21, column 32
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'pickValue' attribute on EditButtons at ProducerContactDetailPopup.pcf: line 21, column 32
    function pickValue_0 () : ProducerContact {
      return contact
    }
    
    override property get CurrentLocation () : pcf.ProducerContactDetailPopup {
      return super.CurrentLocation as pcf.ProducerContactDetailPopup
    }
    
    property get contact () : ProducerContact {
      return getVariableValue("contact", 0) as ProducerContact
    }
    
    property set contact ($arg :  ProducerContact) {
      setVariableValue("contact", 0, $arg)
    }
    
    
  }
  
  
}