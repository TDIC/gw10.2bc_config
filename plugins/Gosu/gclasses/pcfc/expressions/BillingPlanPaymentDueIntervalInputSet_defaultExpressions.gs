package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanPaymentDueIntervalInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BillingPlanPaymentDueIntervalInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanPaymentDueIntervalInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BillingPlanPaymentDueIntervalInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeRadioInput (id=LeadTimeBusiness_Input) at BillingPlanPaymentDueIntervalInputSet.default.pcf: line 36, column 35
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.LeadTimeDayUnit = (__VALUE_TO_SET as typekey.DayUnit)
    }
    
    // 'value' attribute on TextInput (id=PaymentDueInterval_Input) at BillingPlanPaymentDueIntervalInputSet.default.pcf: line 20, column 38
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.PaymentDueInterval = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=NonResponsivePaymentDueInterval_Input) at BillingPlanPaymentDueIntervalInputSet.default.pcf: line 28, column 38
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.NonResponsivePmntDueInterval = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'editable' attribute on TextInput (id=PaymentDueInterval_Input) at BillingPlanPaymentDueIntervalInputSet.default.pcf: line 20, column 38
    function editable_0 () : java.lang.Boolean {
      return planNotInUse
    }
    
    // 'validationExpression' attribute on TextInput (id=NonResponsivePaymentDueInterval_Input) at BillingPlanPaymentDueIntervalInputSet.default.pcf: line 28, column 38
    function validationExpression_7 () : java.lang.Object {
      return isPaymentDueIntervalBigEnough(billingPlan.NonResponsivePmntDueInterval) ? null : DisplayKey.get("Java.Error.PaymentRequestInterval")
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=LeadTimeBusiness_Input) at BillingPlanPaymentDueIntervalInputSet.default.pcf: line 36, column 35
    function valueRange_18 () : java.lang.Object {
      return DayUnit.getTypeKeys(false)
    }
    
    // 'value' attribute on TextInput (id=PaymentDueInterval_Input) at BillingPlanPaymentDueIntervalInputSet.default.pcf: line 20, column 38
    function valueRoot_3 () : java.lang.Object {
      return billingPlan
    }
    
    // 'value' attribute on TextInput (id=PaymentDueInterval_Input) at BillingPlanPaymentDueIntervalInputSet.default.pcf: line 20, column 38
    function value_1 () : java.lang.Integer {
      return billingPlan.PaymentDueInterval
    }
    
    // 'value' attribute on RangeRadioInput (id=LeadTimeBusiness_Input) at BillingPlanPaymentDueIntervalInputSet.default.pcf: line 36, column 35
    function value_15 () : typekey.DayUnit {
      return billingPlan.LeadTimeDayUnit
    }
    
    // 'value' attribute on TextInput (id=NonResponsivePaymentDueInterval_Input) at BillingPlanPaymentDueIntervalInputSet.default.pcf: line 28, column 38
    function value_8 () : java.lang.Integer {
      return billingPlan.NonResponsivePmntDueInterval
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=LeadTimeBusiness_Input) at BillingPlanPaymentDueIntervalInputSet.default.pcf: line 36, column 35
    function verifyValueRangeIsAllowedType_19 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=LeadTimeBusiness_Input) at BillingPlanPaymentDueIntervalInputSet.default.pcf: line 36, column 35
    function verifyValueRangeIsAllowedType_19 ($$arg :  typekey.DayUnit[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=LeadTimeBusiness_Input) at BillingPlanPaymentDueIntervalInputSet.default.pcf: line 36, column 35
    function verifyValueRange_20 () : void {
      var __valueRangeArg = DayUnit.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_19(__valueRangeArg)
    }
    
    property get billingPlan () : BillingPlan {
      return getRequireValue("billingPlan", 0) as BillingPlan
    }
    
    property set billingPlan ($arg :  BillingPlan) {
      setRequireValue("billingPlan", 0, $arg)
    }
    
    property get planNotInUse () : boolean {
      return getRequireValue("planNotInUse", 0) as java.lang.Boolean
    }
    
    property set planNotInUse ($arg :  boolean) {
      setRequireValue("planNotInUse", 0, $arg)
    }
    
    function isPaymentDueIntervalBigEnough(interval : int) : boolean {
          return interval >= billingPlan.MinimumAcceptablePaymentDueIntervalDayCount
        }
    
    
  }
  
  
}