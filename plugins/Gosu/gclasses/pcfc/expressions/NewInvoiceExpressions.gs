package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/NewInvoice.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewInvoiceExpressions {
  @javax.annotation.Generated("config/web/pcf/invoice/NewInvoice.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewInvoiceExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account, selectedInvoiceStream :  InvoiceStream) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=NewInvoice) at NewInvoice.pcf: line 15, column 62
    function afterCancel_22 () : void {
      AccountDetailInvoices.go(account)
    }
    
    // 'afterCancel' attribute on Page (id=NewInvoice) at NewInvoice.pcf: line 15, column 62
    function afterCancel_dest_23 () : pcf.api.Destination {
      return pcf.AccountDetailInvoices.createDestination(account)
    }
    
    // 'afterCommit' attribute on Page (id=NewInvoice) at NewInvoice.pcf: line 15, column 62
    function afterCommit_24 (TopLocation :  pcf.api.Location) : void {
      AccountDetailInvoices.go(account)
    }
    
    // 'canVisit' attribute on Page (id=NewInvoice) at NewInvoice.pcf: line 15, column 62
    static function canVisit_25 (account :  Account, selectedInvoiceStream :  InvoiceStream) : java.lang.Boolean {
      return perm.Account.invccreate
    }
    
    // 'value' attribute on DateInput (id=DueDate_Input) at NewInvoice.pcf: line 53, column 48
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      newInvoice.PaymentDueDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on BooleanRadioInput (id=IsAdHoc_Input) at NewInvoice.pcf: line 59, column 40
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      newInvoice.AdHoc  = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateInput (id=InvoiceDate_Input) at NewInvoice.pcf: line 46, column 43
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      newInvoice.EventDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'initialValue' attribute on Variable at NewInvoice.pcf: line 21, column 30
    function initialValue_0 () : AccountInvoice {
      return createInvoice()
    }
    
    // 'initialValue' attribute on Variable at NewInvoice.pcf: line 28, column 29
    function initialValue_1 () : InvoiceStream {
      return null
    }
    
    // EditButtons at NewInvoice.pcf: line 31, column 23
    function label_2 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'parent' attribute on Page (id=NewInvoice) at NewInvoice.pcf: line 15, column 62
    static function parent_26 (account :  Account, selectedInvoiceStream :  InvoiceStream) : pcf.api.Destination {
      return pcf.AccountDetailInvoices.createDestination(account)
    }
    
    // 'validationExpression' attribute on DateInput (id=DueDate_Input) at NewInvoice.pcf: line 53, column 48
    function validationExpression_12 () : java.lang.Object {
      return gw.api.web.invoice.InvoiceUtil.validatePaymentDueDate(newInvoice)
    }
    
    // 'validationExpression' attribute on DateInput (id=InvoiceDate_Input) at NewInvoice.pcf: line 46, column 43
    function validationExpression_6 () : java.lang.Object {
      return gw.api.web.invoice.InvoiceUtil.validateInvoiceDate(newInvoice)
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at NewInvoice.pcf: line 39, column 41
    function valueRoot_4 () : java.lang.Object {
      return newInvoice
    }
    
    // 'value' attribute on DateInput (id=DueDate_Input) at NewInvoice.pcf: line 53, column 48
    function value_13 () : java.util.Date {
      return newInvoice.PaymentDueDate
    }
    
    // 'value' attribute on BooleanRadioInput (id=IsAdHoc_Input) at NewInvoice.pcf: line 59, column 40
    function value_18 () : java.lang.Boolean {
      return newInvoice.AdHoc 
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at NewInvoice.pcf: line 39, column 41
    function value_3 () : entity.Account {
      return newInvoice.Account
    }
    
    // 'value' attribute on DateInput (id=InvoiceDate_Input) at NewInvoice.pcf: line 46, column 43
    function value_7 () : java.util.Date {
      return newInvoice.EventDate
    }
    
    override property get CurrentLocation () : pcf.NewInvoice {
      return super.CurrentLocation as pcf.NewInvoice
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get newInvoice () : AccountInvoice {
      return getVariableValue("newInvoice", 0) as AccountInvoice
    }
    
    property set newInvoice ($arg :  AccountInvoice) {
      setVariableValue("newInvoice", 0, $arg)
    }
    
    property get selectedInvoiceStream () : InvoiceStream {
      return getVariableValue("selectedInvoiceStream", 0) as InvoiceStream
    }
    
    property set selectedInvoiceStream ($arg :  InvoiceStream) {
      setVariableValue("selectedInvoiceStream", 0, $arg)
    }
    
    function createInvoice() : AccountInvoice {
      // todo update this page to be able to create an account in a particular invoice stream
      return selectedInvoiceStream.createAndAddInvoice(gw.api.util.DateUtil.currentDate()) as AccountInvoice
      // return account.createAndAddPlannedInvoice(gw.api.util.DateUtil.addDays(gw.api.util.DateUtil.currentDate(), 1))
    }
    
    
  }
  
  
}