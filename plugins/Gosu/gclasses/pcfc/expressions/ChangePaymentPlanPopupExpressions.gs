package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.acc.acct360.accountview.AccountBalanceTxnUtil
uses gw.api.domain.invoice.PaymentPlanChanger
uses gw.invoice.InvoiceItemFilter
@javax.annotation.Generated("config/web/pcf/policy/ChangePaymentPlanPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChangePaymentPlanPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/ChangePaymentPlanPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChangePaymentPlanPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'available' attribute on CheckBoxInput (id=IncludeDownPaymentItems_Input) at ChangePaymentPlanPopup.pcf: line 80, column 75
    function available_16 () : java.lang.Boolean {
      return itemFilterType != InvoiceItemFilterType.TC_ALLITEMS
    }
    
    // 'beforeCommit' attribute on Popup (id=ChangePaymentPlanPopup) at ChangePaymentPlanPopup.pcf: line 11, column 69
    function beforeCommit_88 (pickedValue :  java.lang.Object) : void {
      execute()
    }
    
    // 'canEdit' attribute on Popup (id=ChangePaymentPlanPopup) at ChangePaymentPlanPopup.pcf: line 11, column 69
    function canEdit_89 () : java.lang.Boolean {
      return perm.PolicyPeriod.pmntscheplcyedit
    }
    
    // 'canVisit' attribute on Popup (id=ChangePaymentPlanPopup) at ChangePaymentPlanPopup.pcf: line 11, column 69
    static function canVisit_90 (policyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcypmntview
    }
    
    // 'updateConfirmMessage' attribute on EditButtons at ChangePaymentPlanPopup.pcf: line 59, column 33
    function confirmMessage_7 () : java.lang.String {
      return gw.api.util.ChangePaymentPlanHelper.getPaymentPlanWarning(changer, redistributePayments)
    }
    
    // 'value' attribute on RangeRadioInput (id=InvoiceItemFilterType_Input) at ChangePaymentPlanPopup.pcf: line 70, column 55
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      itemFilterType = (__VALUE_TO_SET as typekey.InvoiceItemFilterType)
    }
    
    // 'value' attribute on CheckBoxInput (id=IncludeDownPaymentItems_Input) at ChangePaymentPlanPopup.pcf: line 80, column 75
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      includeDownPaymentItems = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeRadioInput (id=RedistributePayments_Input) at ChangePaymentPlanPopup.pcf: line 91, column 86
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      redistributePayments = (__VALUE_TO_SET as gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange)
    }
    
    // 'value' attribute on RangeInput (id=PaymentPlan_Input) at ChangePaymentPlanPopup.pcf: line 107, column 44
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan = (__VALUE_TO_SET as entity.PaymentPlan)
    }
    
    // 'value' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at ChangePaymentPlanPopup.pcf: line 122, column 48
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.Account.DefaultPaymentInstrument = (__VALUE_TO_SET as entity.PaymentInstrument)
    }
    
    // 'initialValue' attribute on Variable at ChangePaymentPlanPopup.pcf: line 20, column 27
    function initialValue_0 () : PaymentPlan {
      return policyPeriod.PaymentPlan.MaximumNumberOfInstallments==4? gw.api.web.plan.PaymentPlans.findEligibleVisiblePaymentPlansForPolicyPeriod(policyPeriod).first(): policyPeriod.PaymentPlan
    }
    
    // 'initialValue' attribute on Variable at ChangePaymentPlanPopup.pcf: line 24, column 76
    function initialValue_1 () : gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange {
      return policyPeriod.AgencyBill ? gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange.ReverseOnly : gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange.ReverseAndRedistribute
    }
    
    // 'initialValue' attribute on Variable at ChangePaymentPlanPopup.pcf: line 28, column 45
    function initialValue_2 () : typekey.InvoiceItemFilterType {
      return InvoiceItemFilterType.TC_NOTFULLYPAIDITEMS
    }
    
    // 'initialValue' attribute on Variable at ChangePaymentPlanPopup.pcf: line 36, column 56
    function initialValue_3 () : gw.api.domain.invoice.PaymentPlanChanger {
      return createChanger()
    }
    
    // 'initialValue' attribute on Variable at ChangePaymentPlanPopup.pcf: line 40, column 90
    function initialValue_4 () : java.util.List<gw.api.domain.invoice.ChargeInstallmentChanger.Entry> {
      return changer.InstallmentPreview
    }
    
    // 'initialValue' attribute on Variable at ChangePaymentPlanPopup.pcf: line 44, column 49
    function initialValue_5 () : gw.payment.PaymentInstrumentRange {
      return new gw.payment.PaymentInstrumentRange(policyPeriod.Account.PaymentInstruments)
    }
    
    // 'initialValue' attribute on Variable at ChangePaymentPlanPopup.pcf: line 52, column 33
    function initialValue_6 () : PaymentInstrument {
      return policyPeriod.Account.DefaultPaymentInstrument
    }
    
    // 'label' attribute on CheckBoxInput (id=IncludeDownPaymentItems_Input) at ChangePaymentPlanPopup.pcf: line 80, column 75
    function label_18 () : java.lang.Object {
      return itemFilterType == InvoiceItemFilterType.TC_PLANNEDITEMS ? DisplayKey.get("Web.ChangePaymentPlan.IncludeBilledDueDownPaymentItems") : DisplayKey.get("Web.ChangePaymentPlan.IncludeFullyPaidDownPaymentItems")
    }
    
    // 'onChange' attribute on PostOnChange at ChangePaymentPlanPopup.pcf: line 82, column 43
    function onChange_15 () : void {
      updateEntries()
    }
    
    // 'onChange' attribute on PostOnChange at ChangePaymentPlanPopup.pcf: line 93, column 43
    function onChange_25 () : void {
      updateEntries()
    }
    
    // 'onChange' attribute on PostOnChange at ChangePaymentPlanPopup.pcf: line 109, column 43
    function onChange_35 () : void {
      updateEntries()
    }
    
    // 'onChange' attribute on PostOnChange at ChangePaymentPlanPopup.pcf: line 72, column 43
    function onChange_8 () : void {
      updateEntries()
    }
    
    // 'onPick' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at ChangePaymentPlanPopup.pcf: line 122, column 48
    function onPick_43 (PickedValue :  java.lang.Object) : void {
      paymentInstrumentRange.addPaymentInstrument(policyPeriod.Account.DefaultPaymentInstrument)
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at ChangePaymentPlanPopup.pcf: line 146, column 31
    function sortValue_52 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.InvoiceItem.InstallmentNumber
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at ChangePaymentPlanPopup.pcf: line 151, column 40
    function sortValue_53 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.EventDate
    }
    
    // 'value' attribute on TextCell (id=Invoice_Cell) at ChangePaymentPlanPopup.pcf: line 157, column 43
    function sortValue_54 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.Invoice
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at ChangePaymentPlanPopup.pcf: line 161, column 46
    function sortValue_55 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.Invoice.DueDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ChangePaymentPlanPopup.pcf: line 171, column 52
    function sortValue_56 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.InvoiceItemType
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at ChangePaymentPlanPopup.pcf: line 179, column 36
    function sortValue_57 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.Amount
    }
    
    // 'value' attribute on TextCell (id=EditType_Cell) at ChangePaymentPlanPopup.pcf: line 187, column 42
    function sortValue_58 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.DisplayName
    }
    
    // '$$sumValue' attribute on RowIterator at ChangePaymentPlanPopup.pcf: line 179, column 36
    function sumValueRoot_60 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry
    }
    
    // 'footerSumValue' attribute on RowIterator at ChangePaymentPlanPopup.pcf: line 179, column 36
    function sumValue_59 (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) : java.lang.Object {
      return entry.Amount
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=InvoiceItemFilterType_Input) at ChangePaymentPlanPopup.pcf: line 70, column 55
    function valueRange_11 () : java.lang.Object {
      return InvoiceItemFilterType.getTypeKeys(false)
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=RedistributePayments_Input) at ChangePaymentPlanPopup.pcf: line 91, column 86
    function valueRange_28 () : java.lang.Object {
      return gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange.values()
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at ChangePaymentPlanPopup.pcf: line 107, column 44
    function valueRange_38 () : java.lang.Object {
      return gw.api.web.plan.PaymentPlans.findEligibleVisiblePaymentPlansForPolicyPeriod(policyPeriod)
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at ChangePaymentPlanPopup.pcf: line 122, column 48
    function valueRange_47 () : java.lang.Object {
      return gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, gw.payment.PaymentInstrumentFilters.accountDetailsPaymentInstrumentFilter).where( \ paymentInstrument -> paymentInstrument.PaymentMethod == typekey.PaymentMethod.TC_ACH)
    }
    
    // 'value' attribute on TextInput (id=OriginalPlan_Input) at ChangePaymentPlanPopup.pcf: line 99, column 45
    function valueRoot_33 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at ChangePaymentPlanPopup.pcf: line 122, column 48
    function valueRoot_46 () : java.lang.Object {
      return policyPeriod.Account
    }
    
    // 'value' attribute on CheckBoxInput (id=IncludeDownPaymentItems_Input) at ChangePaymentPlanPopup.pcf: line 80, column 75
    function value_19 () : java.lang.Boolean {
      return includeDownPaymentItems
    }
    
    // 'value' attribute on RangeRadioInput (id=RedistributePayments_Input) at ChangePaymentPlanPopup.pcf: line 91, column 86
    function value_26 () : gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange {
      return redistributePayments
    }
    
    // 'value' attribute on TextInput (id=OriginalPlan_Input) at ChangePaymentPlanPopup.pcf: line 99, column 45
    function value_32 () : entity.PaymentPlan {
      return policyPeriod.PaymentPlan
    }
    
    // 'value' attribute on RangeInput (id=PaymentPlan_Input) at ChangePaymentPlanPopup.pcf: line 107, column 44
    function value_36 () : entity.PaymentPlan {
      return paymentPlan
    }
    
    // 'value' attribute on RowIterator at ChangePaymentPlanPopup.pcf: line 135, column 98
    function value_87 () : java.util.List<gw.api.domain.invoice.ChargeInstallmentChanger.Entry> {
      return entries
    }
    
    // 'value' attribute on RangeRadioInput (id=InvoiceItemFilterType_Input) at ChangePaymentPlanPopup.pcf: line 70, column 55
    function value_9 () : typekey.InvoiceItemFilterType {
      return itemFilterType
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=InvoiceItemFilterType_Input) at ChangePaymentPlanPopup.pcf: line 70, column 55
    function verifyValueRangeIsAllowedType_12 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=InvoiceItemFilterType_Input) at ChangePaymentPlanPopup.pcf: line 70, column 55
    function verifyValueRangeIsAllowedType_12 ($$arg :  typekey.InvoiceItemFilterType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=RedistributePayments_Input) at ChangePaymentPlanPopup.pcf: line 91, column 86
    function verifyValueRangeIsAllowedType_29 ($$arg :  gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=RedistributePayments_Input) at ChangePaymentPlanPopup.pcf: line 91, column 86
    function verifyValueRangeIsAllowedType_29 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at ChangePaymentPlanPopup.pcf: line 107, column 44
    function verifyValueRangeIsAllowedType_39 ($$arg :  entity.PaymentPlan[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at ChangePaymentPlanPopup.pcf: line 107, column 44
    function verifyValueRangeIsAllowedType_39 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentPlan>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at ChangePaymentPlanPopup.pcf: line 107, column 44
    function verifyValueRangeIsAllowedType_39 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at ChangePaymentPlanPopup.pcf: line 122, column 48
    function verifyValueRangeIsAllowedType_48 ($$arg :  entity.PaymentInstrument[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at ChangePaymentPlanPopup.pcf: line 122, column 48
    function verifyValueRangeIsAllowedType_48 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentInstrument>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at ChangePaymentPlanPopup.pcf: line 122, column 48
    function verifyValueRangeIsAllowedType_48 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=InvoiceItemFilterType_Input) at ChangePaymentPlanPopup.pcf: line 70, column 55
    function verifyValueRange_13 () : void {
      var __valueRangeArg = InvoiceItemFilterType.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_12(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=RedistributePayments_Input) at ChangePaymentPlanPopup.pcf: line 91, column 86
    function verifyValueRange_30 () : void {
      var __valueRangeArg = gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange.values()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_29(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at ChangePaymentPlanPopup.pcf: line 107, column 44
    function verifyValueRange_40 () : void {
      var __valueRangeArg = gw.api.web.plan.PaymentPlans.findEligibleVisiblePaymentPlansForPolicyPeriod(policyPeriod)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_39(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at ChangePaymentPlanPopup.pcf: line 122, column 48
    function verifyValueRange_49 () : void {
      var __valueRangeArg = gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, gw.payment.PaymentInstrumentFilters.accountDetailsPaymentInstrumentFilter).where( \ paymentInstrument -> paymentInstrument.PaymentMethod == typekey.PaymentMethod.TC_ACH)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_48(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at ChangePaymentPlanPopup.pcf: line 122, column 48
    function visible_42 () : java.lang.Boolean {
      return paymentInstrumentVisible
    }
    
    override property get CurrentLocation () : pcf.ChangePaymentPlanPopup {
      return super.CurrentLocation as pcf.ChangePaymentPlanPopup
    }
    
    property get changer () : gw.api.domain.invoice.PaymentPlanChanger {
      return getVariableValue("changer", 0) as gw.api.domain.invoice.PaymentPlanChanger
    }
    
    property set changer ($arg :  gw.api.domain.invoice.PaymentPlanChanger) {
      setVariableValue("changer", 0, $arg)
    }
    
    property get currentPaymentInstrument () : PaymentInstrument {
      return getVariableValue("currentPaymentInstrument", 0) as PaymentInstrument
    }
    
    property set currentPaymentInstrument ($arg :  PaymentInstrument) {
      setVariableValue("currentPaymentInstrument", 0, $arg)
    }
    
    property get entries () : java.util.List<gw.api.domain.invoice.ChargeInstallmentChanger.Entry> {
      return getVariableValue("entries", 0) as java.util.List<gw.api.domain.invoice.ChargeInstallmentChanger.Entry>
    }
    
    property set entries ($arg :  java.util.List<gw.api.domain.invoice.ChargeInstallmentChanger.Entry>) {
      setVariableValue("entries", 0, $arg)
    }
    
    property get includeDownPaymentItems () : boolean {
      return getVariableValue("includeDownPaymentItems", 0) as java.lang.Boolean
    }
    
    property set includeDownPaymentItems ($arg :  boolean) {
      setVariableValue("includeDownPaymentItems", 0, $arg)
    }
    
    property get itemFilterType () : typekey.InvoiceItemFilterType {
      return getVariableValue("itemFilterType", 0) as typekey.InvoiceItemFilterType
    }
    
    property set itemFilterType ($arg :  typekey.InvoiceItemFilterType) {
      setVariableValue("itemFilterType", 0, $arg)
    }
    
    property get paymentInstrumentRange () : gw.payment.PaymentInstrumentRange {
      return getVariableValue("paymentInstrumentRange", 0) as gw.payment.PaymentInstrumentRange
    }
    
    property set paymentInstrumentRange ($arg :  gw.payment.PaymentInstrumentRange) {
      setVariableValue("paymentInstrumentRange", 0, $arg)
    }
    
    property get paymentInstrumentVisible () : boolean {
      return getVariableValue("paymentInstrumentVisible", 0) as java.lang.Boolean
    }
    
    property set paymentInstrumentVisible ($arg :  boolean) {
      setVariableValue("paymentInstrumentVisible", 0, $arg)
    }
    
    property get paymentPlan () : PaymentPlan {
      return getVariableValue("paymentPlan", 0) as PaymentPlan
    }
    
    property set paymentPlan ($arg :  PaymentPlan) {
      setVariableValue("paymentPlan", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get redistributePayments () : gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange {
      return getVariableValue("redistributePayments", 0) as gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange
    }
    
    property set redistributePayments ($arg :  gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange) {
      setVariableValue("redistributePayments", 0, $arg)
    }
    
    
    function updateEntries() {
      changer.cleanup()
    /**
     * Vicente, 09/14/2014, US72, US73, US74, US91, US138, US139, US140: Installment Scheduling
     * Set the NewPaymentPlan_TDIC field for policy periods on the Account when there is a change in the payment plan.
     **/
      var changePlanType = policyPeriod.PaymentPlan.Name != "TDIC Monthly APW" and paymentPlan.Name == "TDIC Monthly APW"
          or policyPeriod.PaymentPlan.Name == "TDIC Monthly APW" and paymentPlan.Name != "TDIC Monthly APW"
      //If the new Payment Plan selected is non responsive the NewPaymentPlan_TDIC of all policies is set to the new Payment
      //Plan, if not just in the current Policy Period
      for(pPeriod in policyPeriod.Account.PolicyPeriods) {
        var bundle = gw.transaction.Transaction.getCurrent()
        pPeriod = bundle.add(pPeriod)
        if(changePlanType){
          pPeriod.NewPaymentPlan_TDIC = paymentPlan
        } else {
          pPeriod.NewPaymentPlan_TDIC = pPeriod.PaymentPlan
        }
      }
      policyPeriod.NewPaymentPlan_TDIC = paymentPlan
      changer = createChanger()
      entries = changer.InstallmentPreview
      if(policyPeriod.PaymentPlan.MaximumNumberOfInstallments != 12 and paymentPlan.MaximumNumberOfInstallments == 12) {
       if(entries*.Invoice.hasMatch(\elt -> elt.EventDate.DayOfMonth != 1))
        gw.api.util.LocationUtil.addRequestScopedWarningMessage("Please change the invoice date to the 1st of the month")
      }
    }
    
    function createChanger() : PaymentPlanChanger {
      return new PaymentPlanChanger(policyPeriod, paymentPlan, redistributePayments,
          InvoiceItemFilter.getFilter(itemFilterType, includeDownPaymentItems))
    }
    function execute() {
      var invItems = (itemFilterType == InvoiceItemFilterType.TC_ALLITEMS) ? policyPeriod.InvoiceItems.where(\ii -> ii.Invoice.BilledOrDue && !ii.AgencyBill
          && ii.Payer == policyPeriod.Account
          && !ii.Reversal
          && !ii.Reversed) // any billed item
          : policyPeriod.InvoiceItems.where(\ii -> ii.Invoice.BilledOrDue
          && !ii.AgencyBill
          && ii.Payer == policyPeriod.Account
          && !ii.Reversal
          && !ii.Reversed
          && !ii.FullyConsumed) // billed and not fully paid
      if (invItems.Count > 0 && itemFilterType != InvoiceItemFilterType.TC_PLANNEDITEMS) {
        AccountBalanceTxnUtil.createMoveItemRecord(policyPeriod.Account,
            invItems,
            AcctBalItemMovedType_Ext.TC_PAYMENTPLANCHANGE)
      }
      if(!(policyPeriod.PaymentPlan.Name != "TDIC Monthly APW" and paymentPlan.Name == "TDIC Monthly APW" and paymentInstrumentVisible)) {
        changer.execute()
      }
    }
    
    function getPayerDisplay(invoiceItem : InvoiceItem) : String {
      var invoiceHolder = invoiceItem.Payer
      if (invoiceHolder == null) {
        return invoiceItem.Charge.OwnerAccount as String
      }
      if (typeof invoiceHolder == Account or not invoiceItem.PolicyPeriodItem) {
        return invoiceHolder as String
      }
      var itemCommission = invoiceItem.ActivePrimaryItemCommission
      var producerCode = itemCommission.PolicyCommission.ProducerCode
      var producer = producerCode.Producer
      return producer.DisplayName + " " + producerCode.Code
    }
        
    /*function invalidateIterator() {
      gw.api.web.PebblesUtil.invalidateIterators(CurrentLocation, gw.api.domain.invoice.ChargeInstallmentChanger.Entry)
    }*/
    
    // TODO: revisit this method and its usage.
    /**
     * Vicente, 08/21/2014, US65: If "TDIC Monthly APW" selected and Account's Default Payment Method is not ACH/EFT
     * a field to select the Account's Default Payment Method is shown and a ACH/EFT Payment Instrument set as default
     **/
    /*function setDefaultPaymentInstrumentDropDownVisible() {
      if(policyPeriod.PaymentPlan.Name != "TDIC Monthly APW" and paymentPlan.Name == "TDIC Monthly APW" 
          and policyPeriod.Account.DefaultPaymentInstrument.PaymentMethod != typekey.PaymentMethod.TC_ACH ){
        var achPaymentInstrument = policyPeriod.Account.PaymentInstruments.firstWhere( \ pInstrument -> pInstrument.PaymentMethod == typekey.PaymentMethod.TC_ACH)
        //If there is no almost one Payment Instrument with ACH/EFT Payment Method the field is not shown
        if(achPaymentInstrument != null) {
          paymentInstrumentVisible = true
          //A Payment Instrument with ACH/EFT Payment Method is set by default
          policyPeriod.Account.DefaultPaymentInstrument = achPaymentInstrument
        }
      } else {
        paymentInstrumentVisible = false
        policyPeriod.Account.DefaultPaymentInstrument = currentPaymentInstrument
      }
    }*/
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/ChangePaymentPlanPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ChangePaymentPlanPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at ChangePaymentPlanPopup.pcf: line 179, column 36
    function currency_80 () : typekey.Currency {
      return entry.Amount.Currency
    }
    
    // 'editable' attribute on Row at ChangePaymentPlanPopup.pcf: line 138, column 81
    function editable_85 () : java.lang.Boolean {
      return entry.canSetAmount()
    }
    
    // 'useArchivedStyle' attribute on Row at ChangePaymentPlanPopup.pcf: line 138, column 81
    function useArchivedStyle_86 () : java.lang.Boolean {
      return entry.removesItem() || entry.isInOrphanedState()
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at ChangePaymentPlanPopup.pcf: line 146, column 31
    function valueRoot_62 () : java.lang.Object {
      return entry.InvoiceItem
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at ChangePaymentPlanPopup.pcf: line 151, column 40
    function valueRoot_65 () : java.lang.Object {
      return entry
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at ChangePaymentPlanPopup.pcf: line 161, column 46
    function valueRoot_71 () : java.lang.Object {
      return entry.Invoice
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at ChangePaymentPlanPopup.pcf: line 146, column 31
    function value_61 () : java.lang.Integer {
      return entry.InvoiceItem.InstallmentNumber
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at ChangePaymentPlanPopup.pcf: line 151, column 40
    function value_64 () : java.util.Date {
      return entry.EventDate
    }
    
    // 'value' attribute on TextCell (id=Invoice_Cell) at ChangePaymentPlanPopup.pcf: line 157, column 43
    function value_67 () : entity.Invoice {
      return entry.Invoice
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at ChangePaymentPlanPopup.pcf: line 161, column 46
    function value_70 () : java.util.Date {
      return entry.Invoice.DueDate
    }
    
    // 'value' attribute on TextCell (id=itemPayer_Cell) at ChangePaymentPlanPopup.pcf: line 166, column 59
    function value_73 () : java.lang.String {
      return getPayerDisplay(entry.InvoiceItem)
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ChangePaymentPlanPopup.pcf: line 171, column 52
    function value_75 () : typekey.InvoiceItemType {
      return entry.InvoiceItemType
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at ChangePaymentPlanPopup.pcf: line 179, column 36
    function value_78 () : gw.pl.currency.MonetaryAmount {
      return entry.Amount
    }
    
    // 'value' attribute on TextCell (id=EditType_Cell) at ChangePaymentPlanPopup.pcf: line 187, column 42
    function value_82 () : java.lang.String {
      return entry.DisplayName
    }
    
    property get entry () : gw.api.domain.invoice.ChargeInstallmentChanger.Entry {
      return getIteratedValue(1) as gw.api.domain.invoice.ChargeInstallmentChanger.Entry
    }
    
    
  }
  
  
}