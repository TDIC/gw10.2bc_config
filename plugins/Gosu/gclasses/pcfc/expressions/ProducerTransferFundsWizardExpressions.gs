package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/ProducerTransferFundsWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerTransferFundsWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/ProducerTransferFundsWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerTransferFundsWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'initialValue' attribute on Variable at ProducerTransferFundsWizard.pcf: line 17, column 56
    function initialValue_0 () : gw.api.web.transaction.FundsTransferUtil {
      return createFundTransferUtil()
    }
    
    // 'onExit' attribute on WizardStep (id=Step2) at ProducerTransferFundsWizard.pcf: line 28, column 93
    function onExit_3 () : void {
      fundsTransferUtil.setSources(); validateTransferAmount(); fundsTransferUtil.createTransfers();
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at ProducerTransferFundsWizard.pcf: line 22, column 88
    function screen_onEnter_1 (def :  pcf.TransferDetailsScreen) : void {
      def.onEnter(fundsTransferUtil)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at ProducerTransferFundsWizard.pcf: line 28, column 93
    function screen_onEnter_4 (def :  pcf.TransferConfirmationScreen) : void {
      def.onEnter(fundsTransferUtil)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at ProducerTransferFundsWizard.pcf: line 22, column 88
    function screen_refreshVariables_2 (def :  pcf.TransferDetailsScreen) : void {
      def.refreshVariables(fundsTransferUtil)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at ProducerTransferFundsWizard.pcf: line 28, column 93
    function screen_refreshVariables_5 (def :  pcf.TransferConfirmationScreen) : void {
      def.refreshVariables(fundsTransferUtil)
    }
    
    // 'tabBar' attribute on Wizard (id=ProducerTransferFundsWizard) at ProducerTransferFundsWizard.pcf: line 8, column 38
    function tabBar_onEnter_6 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=ProducerTransferFundsWizard) at ProducerTransferFundsWizard.pcf: line 8, column 38
    function tabBar_refreshVariables_7 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.ProducerTransferFundsWizard {
      return super.CurrentLocation as pcf.ProducerTransferFundsWizard
    }
    
    property get fundsTransferUtil () : gw.api.web.transaction.FundsTransferUtil {
      return getVariableValue("fundsTransferUtil", 0) as gw.api.web.transaction.FundsTransferUtil
    }
    
    property set fundsTransferUtil ($arg :  gw.api.web.transaction.FundsTransferUtil) {
      setVariableValue("fundsTransferUtil", 0, $arg)
    }
    
    property get producer () : entity.Producer {
      return getVariableValue("producer", 0) as entity.Producer
    }
    
    property set producer ($arg :  entity.Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    function createFundTransferUtil() : gw.api.web.transaction.FundsTransferUtil{
      var transferUtil = new gw.api.web.transaction.FundsTransferUtil()
      transferUtil.SourceOwner = producer
      transferUtil.TargetType = TAccountOwnerType.TC_PRODUCER
      transferUtil.addToTransfers(new FundsTransfer(producer.Currency))
      return transferUtil;
    }
    
    
    function validateTransferAmount() {
      if (!fundsTransferUtil.validateAmounts()) {
        throw new gw.api.util.DisplayableException(DisplayKey.get("Web.NewTransferWizard.InsufficientUnappliedFundsForTransfer"))
      }
    }
    
    
  }
  
  
}