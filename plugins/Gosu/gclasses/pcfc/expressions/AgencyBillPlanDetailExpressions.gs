package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/agencybill/AgencyBillPlanDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillPlanDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/agencybill/AgencyBillPlanDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillPlanDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (agencyBillPlan :  AgencyBillPlan) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=AgencyBillPlanDetail) at AgencyBillPlanDetail.pcf: line 10, column 93
    function canEdit_2 () : java.lang.Boolean {
      return perm.System.agencybillplanedit
    }
    
    // 'def' attribute on ScreenRef at AgencyBillPlanDetail.pcf: line 17, column 57
    function def_onEnter_0 (def :  pcf.AgencyBillPlanDetailScreen) : void {
      def.onEnter(agencyBillPlan)
    }
    
    // 'def' attribute on ScreenRef at AgencyBillPlanDetail.pcf: line 17, column 57
    function def_refreshVariables_1 (def :  pcf.AgencyBillPlanDetailScreen) : void {
      def.refreshVariables(agencyBillPlan)
    }
    
    // 'parent' attribute on Page (id=AgencyBillPlanDetail) at AgencyBillPlanDetail.pcf: line 10, column 93
    static function parent_3 (agencyBillPlan :  AgencyBillPlan) : pcf.api.Destination {
      return pcf.AgencyBillPlans.createDestination()
    }
    
    // 'title' attribute on Page (id=AgencyBillPlanDetail) at AgencyBillPlanDetail.pcf: line 10, column 93
    static function title_4 (agencyBillPlan :  AgencyBillPlan) : java.lang.Object {
      return DisplayKey.get("Web.AgencyBillPlanDetail.Title", agencyBillPlan.Name)
    }
    
    override property get CurrentLocation () : pcf.AgencyBillPlanDetail {
      return super.CurrentLocation as pcf.AgencyBillPlanDetail
    }
    
    property get agencyBillPlan () : AgencyBillPlan {
      return getVariableValue("agencyBillPlan", 0) as AgencyBillPlan
    }
    
    property set agencyBillPlan ($arg :  AgencyBillPlan) {
      setVariableValue("agencyBillPlan", 0, $arg)
    }
    
    
  }
  
  
}