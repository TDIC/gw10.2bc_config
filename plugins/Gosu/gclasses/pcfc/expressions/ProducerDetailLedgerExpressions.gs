package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerDetailLedger.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerDetailLedgerExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerDetailLedger.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerDetailLedgerExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=ProducerDetailLedger) at ProducerDetailLedger.pcf: line 9, column 72
    static function canVisit_2 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodledgerview
    }
    
    // 'def' attribute on ScreenRef at ProducerDetailLedger.pcf: line 16, column 61
    function def_onEnter_0 (def :  pcf.LedgerScreen) : void {
      def.onEnter(producer, producer.ProducerCodes)
    }
    
    // 'def' attribute on ScreenRef at ProducerDetailLedger.pcf: line 16, column 61
    function def_refreshVariables_1 (def :  pcf.LedgerScreen) : void {
      def.refreshVariables(producer, producer.ProducerCodes)
    }
    
    // Page (id=ProducerDetailLedger) at ProducerDetailLedger.pcf: line 9, column 72
    static function parent_3 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    override property get CurrentLocation () : pcf.ProducerDetailLedger {
      return super.CurrentLocation as pcf.ProducerDetailLedger
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}