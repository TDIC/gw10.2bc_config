package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/TDIC_CloseDelinquencyProcessPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_CloseDelinquencyProcessPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/delinquency/TDIC_CloseDelinquencyProcessPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_CloseDelinquencyProcessPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=DelinquencyTarget_Cell) at TDIC_CloseDelinquencyProcessPopup.pcf: line 52, column 55
    function action_8 () : void {
      DelinquencyTargetDetailsForward.go(target)
    }
    
    // 'action' attribute on TextCell (id=DelinquencyTarget_Cell) at TDIC_CloseDelinquencyProcessPopup.pcf: line 52, column 55
    function action_dest_9 () : pcf.api.Destination {
      return pcf.DelinquencyTargetDetailsForward.createDestination(target)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=PastDueAmt_Cell) at TDIC_CloseDelinquencyProcessPopup.pcf: line 72, column 54
    function currency_24 () : typekey.Currency {
      return target.getCurrency()
    }
    
    // 'value' attribute on TextCell (id=DelinquencyTarget_Cell) at TDIC_CloseDelinquencyProcessPopup.pcf: line 52, column 55
    function valueRoot_11 () : java.lang.Object {
      return target
    }
    
    // 'value' attribute on TypeKeyCell (id=DelinquencyOffering_Cell) at TDIC_CloseDelinquencyProcessPopup.pcf: line 57, column 56
    function valueRoot_14 () : java.lang.Object {
      return (target as PolicyPeriod)
    }
    
    // 'value' attribute on TextCell (id=DelinquencyTarget_Cell) at TDIC_CloseDelinquencyProcessPopup.pcf: line 52, column 55
    function value_10 () : java.lang.String {
      return target.TargetDisplayName
    }
    
    // 'value' attribute on TypeKeyCell (id=DelinquencyOffering_Cell) at TDIC_CloseDelinquencyProcessPopup.pcf: line 57, column 56
    function value_13 () : typekey.Offering_TDIC {
      return (target as PolicyPeriod).Offering_TDIC
    }
    
    // 'value' attribute on DateCell (id=EffDate_Cell) at TDIC_CloseDelinquencyProcessPopup.pcf: line 61, column 51
    function value_16 () : java.util.Date {
      return target.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpDate_Cell) at TDIC_CloseDelinquencyProcessPopup.pcf: line 65, column 52
    function value_19 () : java.util.Date {
      return target.ExpirationDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PastDueAmt_Cell) at TDIC_CloseDelinquencyProcessPopup.pcf: line 72, column 54
    function value_22 () : gw.pl.currency.MonetaryAmount {
      return target.DelinquentAmount
    }
    
    property get target () : gw.api.domain.delinquency.DelinquencyTarget {
      return getIteratedValue(1) as gw.api.domain.delinquency.DelinquencyTarget
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/delinquency/TDIC_CloseDelinquencyProcessPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_CloseDelinquencyProcessPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (dlnqTargets :  gw.api.domain.delinquency.DelinquencyTarget[]) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=Cancel) at TDIC_CloseDelinquencyProcessPopup.pcf: line 27, column 83
    function action_1 () : void {
      CurrentLocation.cancel()
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=Execute) at TDIC_CloseDelinquencyProcessPopup.pcf: line 23, column 84
    function allCheckedRowsAction_0 (CheckedValues :  gw.api.domain.delinquency.DelinquencyTarget[], CheckedValuesErrors :  java.util.Map) : void {
      performAction(CheckedValues)
    }
    
    // 'value' attribute on TextCell (id=DelinquencyTarget_Cell) at TDIC_CloseDelinquencyProcessPopup.pcf: line 52, column 55
    function sortValue_2 (target :  gw.api.domain.delinquency.DelinquencyTarget) : java.lang.Object {
      return target.TargetDisplayName
    }
    
    // 'value' attribute on TypeKeyCell (id=DelinquencyOffering_Cell) at TDIC_CloseDelinquencyProcessPopup.pcf: line 57, column 56
    function sortValue_3 (target :  gw.api.domain.delinquency.DelinquencyTarget) : java.lang.Object {
      return (target as PolicyPeriod).Offering_TDIC
    }
    
    // 'value' attribute on DateCell (id=EffDate_Cell) at TDIC_CloseDelinquencyProcessPopup.pcf: line 61, column 51
    function sortValue_4 (target :  gw.api.domain.delinquency.DelinquencyTarget) : java.lang.Object {
      return target.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpDate_Cell) at TDIC_CloseDelinquencyProcessPopup.pcf: line 65, column 52
    function sortValue_5 (target :  gw.api.domain.delinquency.DelinquencyTarget) : java.lang.Object {
      return target.ExpirationDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PastDueAmt_Cell) at TDIC_CloseDelinquencyProcessPopup.pcf: line 72, column 54
    function sortValue_6 (target :  gw.api.domain.delinquency.DelinquencyTarget) : java.lang.Object {
      return target.DelinquentAmount
    }
    
    // 'footerSumValue' attribute on RowIterator at TDIC_CloseDelinquencyProcessPopup.pcf: line 72, column 54
    function sumValue_7 (target :  gw.api.domain.delinquency.DelinquencyTarget) : java.lang.Object {
      return ( target typeis PolicyPeriod ? target.DelinquentAmount : 0bd.ofCurrency(target.DelinquentAmount.getCurrency()) )
    }
    
    // 'value' attribute on RowIterator at TDIC_CloseDelinquencyProcessPopup.pcf: line 43, column 75
    function value_26 () : gw.api.domain.delinquency.DelinquencyTarget[] {
      return dlnqTargets.where(\ target -> !(target typeis Account))
    }
    
    override property get CurrentLocation () : pcf.TDIC_CloseDelinquencyProcessPopup {
      return super.CurrentLocation as pcf.TDIC_CloseDelinquencyProcessPopup
    }
    
    property get dlnqTargets () : gw.api.domain.delinquency.DelinquencyTarget[] {
      return getVariableValue("dlnqTargets", 0) as gw.api.domain.delinquency.DelinquencyTarget[]
    }
    
    property set dlnqTargets ($arg :  gw.api.domain.delinquency.DelinquencyTarget[]) {
      setVariableValue("dlnqTargets", 0, $arg)
    }
    
    /**
     * Perform the popup action.
     */
    function performAction( checkedValues : Object[] ) {
      (checkedValues as gw.api.domain.delinquency.DelinquencyTarget[])*.DelinquencyProcesses?.where(\delinqProcess -> delinqProcess.Status == DelinquencyProcessStatus.TC_OPEN)
          .each(\openDelinqProcess -> exitDelinquency(openDelinqProcess))
      CurrentLocation.commit();
    }
    
    function exitDelinquency(delinquencyProcess : DelinquencyProcess) {
      delinquencyProcess.ExitDate = Date.CurrentDate
      delinquencyProcess.Phase = TC_EXITDELINQUENCY
      delinquencyProcess.invokeTrigger( TC_EXITDELINQUENCY )
    }
    
    
  }
  
  
}