package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.database.Relop
uses gw.api.database.Query
@javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalAccountWriteoffsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewWriteoffReversalAccountWriteoffsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalAccountWriteoffsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewWriteoffReversalAccountWriteoffsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=Writeoff_Input) at NewWriteoffReversalAccountWriteoffsScreen.pcf: line 24, column 40
    function valueRoot_1 () : java.lang.Object {
      return reversal
    }
    
    // 'value' attribute on TextInput (id=Writeoff_Input) at NewWriteoffReversalAccountWriteoffsScreen.pcf: line 24, column 40
    function value_0 () : entity.Writeoff {
      return reversal.Writeoff
    }
    
    property get accountID () : gw.pl.persistence.core.Key {
      return getRequireValue("accountID", 0) as gw.pl.persistence.core.Key
    }
    
    property set accountID ($arg :  gw.pl.persistence.core.Key) {
      setRequireValue("accountID", 0, $arg)
    }
    
    property get reversal () : WriteoffReversal {
      return getRequireValue("reversal", 0) as WriteoffReversal
    }
    
    property set reversal ($arg :  WriteoffReversal) {
      setRequireValue("reversal", 0, $arg)
    }
    
    
        function initializeSearchCriteria() : gw.search.WriteoffSearchCriteria {
          var account =  Query.make(Account).compare("ID", Relop.Equals, accountID).select().AtMostOneRow;
          return new gw.search.WriteoffSearchCriteria(account)
        }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalAccountWriteoffsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends NewWriteoffReversalAccountWriteoffsScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffReversalAccountWriteoffsScreen.pcf: line 39, column 59
    function def_onEnter_3 (def :  pcf.WriteoffSearchDV) : void {
      def.onEnter(searchCriteria, false)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffReversalAccountWriteoffsScreen.pcf: line 43, column 72
    function def_onEnter_5 (def :  pcf.NewWriteoffReversalWriteoffsLV) : void {
      def.onEnter(reversal, writeOffs)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffReversalAccountWriteoffsScreen.pcf: line 39, column 59
    function def_refreshVariables_4 (def :  pcf.WriteoffSearchDV) : void {
      def.refreshVariables(searchCriteria, false)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffReversalAccountWriteoffsScreen.pcf: line 43, column 72
    function def_refreshVariables_6 (def :  pcf.NewWriteoffReversalWriteoffsLV) : void {
      def.refreshVariables(reversal, writeOffs)
    }
    
    // 'searchCriteria' attribute on SearchPanel at NewWriteoffReversalAccountWriteoffsScreen.pcf: line 37, column 80
    function searchCriteria_8 () : gw.search.WriteoffSearchCriteria {
      return initializeSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at NewWriteoffReversalAccountWriteoffsScreen.pcf: line 37, column 80
    function search_7 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria)
    }
    
    property get searchCriteria () : gw.search.WriteoffSearchCriteria {
      return getCriteriaValue(1) as gw.search.WriteoffSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.WriteoffSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    property get writeOffs () : gw.api.database.IQueryBeanResult<Writeoff> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<Writeoff>
    }
    
    
  }
  
  
}