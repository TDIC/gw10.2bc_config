package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/NewAccountContactScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewAccountContactScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/account/NewAccountContactScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewAccountContactScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=UpdateButtonThatForcesCheckForDuplicates) at NewAccountContactScreen.pcf: line 18, column 81
    function action_1 () : void {
      duplicateContactsPopupNavigator.checkForDuplicatesOrUpdate(\ -> CurrentLocation.commit())
    }
    
    // 'action' attribute on ToolbarButton (id=CheckForDuplicates) at NewAccountContactScreen.pcf: line 26, column 81
    function action_6 () : void {
      duplicateContactsPopupNavigator.pushFromCurrentLocationToDuplicateContactsPopup()
    }
    
    // 'def' attribute on PanelRef at NewAccountContactScreen.pcf: line 29, column 46
    function def_onEnter_7 (def :  pcf.AccountContactCV) : void {
      def.onEnter(contact, true)
    }
    
    // 'def' attribute on PanelRef at NewAccountContactScreen.pcf: line 29, column 46
    function def_refreshVariables_8 (def :  pcf.AccountContactCV) : void {
      def.refreshVariables(contact, true)
    }
    
    // EditButtons at NewAccountContactScreen.pcf: line 21, column 91
    function label_4 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'pickValue' attribute on EditButtons at NewAccountContactScreen.pcf: line 21, column 91
    function pickValue_2 () : AccountContact {
      return contact
    }
    
    // 'visible' attribute on ToolbarButton (id=UpdateButtonThatForcesCheckForDuplicates) at NewAccountContactScreen.pcf: line 18, column 81
    function visible_0 () : java.lang.Boolean {
      return duplicateContactsPopupNavigator.ShowCheckForDuplicatesButton
    }
    
    // 'updateVisible' attribute on EditButtons at NewAccountContactScreen.pcf: line 21, column 91
    function visible_3 () : java.lang.Boolean {
      return not duplicateContactsPopupNavigator.ShowCheckForDuplicatesButton
    }
    
    property get contact () : AccountContact {
      return getRequireValue("contact", 0) as AccountContact
    }
    
    property set contact ($arg :  AccountContact) {
      setRequireValue("contact", 0, $arg)
    }
    
    property get duplicateContactsPopupNavigator () : gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator {
      return getRequireValue("duplicateContactsPopupNavigator", 0) as gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator
    }
    
    property set duplicateContactsPopupNavigator ($arg :  gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator) {
      setRequireValue("duplicateContactsPopupNavigator", 0, $arg)
    }
    
    
  }
  
  
}