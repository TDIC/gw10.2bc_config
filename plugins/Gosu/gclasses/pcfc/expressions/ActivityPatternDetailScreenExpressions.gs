package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/activitypatterns/ActivityPatternDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ActivityPatternDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/activitypatterns/ActivityPatternDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ActivityPatternDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=ActivityPatternDetailScreen_DeleteButton) at ActivityPatternDetailScreen.pcf: line 20, column 60
    function action_2 () : void {
      gw.api.admin.BaseAdminUtil.deleteActivityPattern(activityPattern); ActivityPatterns.go()
    }
    
    // 'available' attribute on ToolbarButton (id=ActivityPatternDetailScreen_DeleteButton) at ActivityPatternDetailScreen.pcf: line 20, column 60
    function available_1 () : java.lang.Boolean {
      return !activityPattern.SystemPattern and perm.ActivityPattern.delete
    }
    
    // 'def' attribute on PanelRef at ActivityPatternDetailScreen.pcf: line 23, column 56
    function def_onEnter_3 (def :  pcf.ActivityPatternDV) : void {
      def.onEnter(activityPattern, isNew)
    }
    
    // 'def' attribute on PanelRef at ActivityPatternDetailScreen.pcf: line 25, column 331
    function def_onEnter_5 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(activityPattern, { "Subject", "ShortSubject", "Description"}, { DisplayKey.get("Web.Admin.ActivityPatternDV.Subject"), DisplayKey.get("Web.Admin.ActivityPatternDV.ShortSubject"), DisplayKey.get("Web.Admin.ActivityPatternDV.Description")})
    }
    
    // 'def' attribute on PanelRef at ActivityPatternDetailScreen.pcf: line 23, column 56
    function def_refreshVariables_4 (def :  pcf.ActivityPatternDV) : void {
      def.refreshVariables(activityPattern, isNew)
    }
    
    // 'def' attribute on PanelRef at ActivityPatternDetailScreen.pcf: line 25, column 331
    function def_refreshVariables_6 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(activityPattern, { "Subject", "ShortSubject", "Description"}, { DisplayKey.get("Web.Admin.ActivityPatternDV.Subject"), DisplayKey.get("Web.Admin.ActivityPatternDV.ShortSubject"), DisplayKey.get("Web.Admin.ActivityPatternDV.Description")})
    }
    
    // EditButtons at ActivityPatternDetailScreen.pcf: line 14, column 21
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    property get activityPattern () : ActivityPattern {
      return getRequireValue("activityPattern", 0) as ActivityPattern
    }
    
    property set activityPattern ($arg :  ActivityPattern) {
      setRequireValue("activityPattern", 0, $arg)
    }
    
    property get isNew () : boolean {
      return getRequireValue("isNew", 0) as java.lang.Boolean
    }
    
    property set isNew ($arg :  boolean) {
      setRequireValue("isNew", 0, $arg)
    }
    
    
  }
  
  
}