package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/CommissionPayableReductionWizardTargetsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionPayableReductionWizardTargetsStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/CommissionPayableReductionWizardTargetsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionPayableReductionWizardTargetsStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=Cancel) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 18, column 60
    function action_0 () : void {
      (CurrentLocation as pcf.api.Wizard).cancel()
    }
    
    // 'action' attribute on ToolbarButton (id=ReduceCommissionPayable) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 24, column 37
    function action_2 () : void {
      reduceCommissionPayable()
    }
    
    // 'available' attribute on ToolbarButton (id=ReduceCommissionPayable) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 24, column 37
    function available_1 () : java.lang.Boolean {
      return perm.Producer.edit && (chargeCommissions != null && chargeCommissions.length > 0)
    }
    
    // 'sortBy' attribute on TextCell (id=ProducerCode_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 54, column 80
    function sortValue_3 (chargeCommission :  entity.ChargeCommission) : java.lang.Object {
      return chargeCommission.PolicyCommission.ProducerCode.Code
    }
    
    // 'sortBy' attribute on TextCell (id=PolicyNumber_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 60, column 88
    function sortValue_4 (chargeCommission :  entity.ChargeCommission) : java.lang.Object {
      return chargeCommission.PolicyCommission.PolicyPeriod.PolicyNumber
    }
    
    // 'sortBy' attribute on TextCell (id=Charge_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 67, column 46
    function sortValue_5 (chargeCommission :  entity.ChargeCommission) : java.lang.Object {
      return chargeCommission.Charge
    }
    
    // 'sortBy' attribute on TextCell (id=Context_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 74, column 58
    function sortValue_6 (chargeCommission :  entity.ChargeCommission) : java.lang.Object {
      return chargeCommission.Charge.BillingInstruction
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionReserve_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 80, column 70
    function sortValue_7 (chargeCommission :  entity.ChargeCommission) : java.lang.Object {
      return chargeCommission.CommissionReserveBalance
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionExpense_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 86, column 83
    function sortValue_8 (chargeCommission :  entity.ChargeCommission) : java.lang.Object {
      return chargeCommission.getCommissionExpenseNetOfAdjustment()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AmountToWriteOff_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 94, column 62
    function sortValue_9 (chargeCommission :  entity.ChargeCommission) : java.lang.Object {
      return chargeCommission.AmountToWriteOff
    }
    
    // 'value' attribute on RowIterator at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 40, column 53
    function value_43 () : entity.ChargeCommission[] {
      return chargeCommissions
    }
    
    property get approvalRequiredForWriteoff () : List<Boolean> {
      return getRequireValue("approvalRequiredForWriteoff", 0) as List<Boolean>
    }
    
    property set approvalRequiredForWriteoff ($arg :  List<Boolean>) {
      setRequireValue("approvalRequiredForWriteoff", 0, $arg)
    }
    
    property get chargeCommissions () : ChargeCommission[] {
      return getRequireValue("chargeCommissions", 0) as ChargeCommission[]
    }
    
    property set chargeCommissions ($arg :  ChargeCommission[]) {
      setRequireValue("chargeCommissions", 0, $arg)
    }
    
    function validateReductionAmount() {
      for (var chargeCommission in chargeCommissions) {
        if (chargeCommission.SelectedForWriteoff && !chargeCommission.AmountToWriteOff.IsPositive) {
            throw new gw.api.util.DisplayableException(DisplayKey.get("Web.PolicyCommissionsLV.WriteoffZeroOrNegative"))
          }
        }
    }
    
    function reduceCommissionPayable() {
      validateReductionAmount();
      var chargeCommissionWriteoffs = gw.api.web.producer.ProducerUtil.writeOffCommission(chargeCommissions);
      if (chargeCommissionWriteoffs.hasMatch(\ writeOff -> writeOff.ApprovalStatus == typekey.ApprovalStatus.TC_UNAPPROVED)) {
        approvalRequiredForWriteoff[0] = true
      }
      (CurrentLocation as pcf.api.Wizard).next()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/CommissionPayableReductionWizardTargetsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CommissionPayableReductionWizardTargetsStepScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=CommissionReserve_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 80, column 70
    function currency_28 () : typekey.Currency {
      return chargeCommission.Charge.Currency
    }
    
    // 'value' attribute on CheckBoxCell (id=ChargeCommissionSelected_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 46, column 64
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeCommission.SelectedForWriteoff = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AmountToWriteOff_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 94, column 62
    function defaultSetter_36 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeCommission.AmountToWriteOff = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'editable' attribute on MonetaryAmountCell (id=AmountToWriteOff_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 94, column 62
    function editable_34 () : java.lang.Boolean {
      return chargeCommission.AmountWrittenOff.IsZero
    }
    
    // 'value' attribute on CheckBoxCell (id=ChargeCommissionSelected_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 46, column 64
    function valueRoot_12 () : java.lang.Object {
      return chargeCommission
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 54, column 80
    function valueRoot_15 () : java.lang.Object {
      return chargeCommission.PolicyCommission.ProducerCode
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 60, column 88
    function valueRoot_18 () : java.lang.Object {
      return chargeCommission.PolicyCommission.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=Context_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 74, column 58
    function valueRoot_24 () : java.lang.Object {
      return chargeCommission.Charge
    }
    
    // 'value' attribute on CheckBoxCell (id=ChargeCommissionSelected_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 46, column 64
    function value_10 () : java.lang.Boolean {
      return chargeCommission.SelectedForWriteoff
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 54, column 80
    function value_14 () : java.lang.String {
      return chargeCommission.PolicyCommission.ProducerCode.Code
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 60, column 88
    function value_17 () : java.lang.String {
      return chargeCommission.PolicyCommission.PolicyPeriod.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=Charge_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 67, column 46
    function value_20 () : entity.Charge {
      return chargeCommission.Charge
    }
    
    // 'value' attribute on TextCell (id=Context_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 74, column 58
    function value_23 () : entity.BillingInstruction {
      return chargeCommission.Charge.BillingInstruction
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionReserve_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 80, column 70
    function value_26 () : gw.pl.currency.MonetaryAmount {
      return chargeCommission.CommissionReserveBalance
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionExpense_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 86, column 83
    function value_30 () : gw.pl.currency.MonetaryAmount {
      return chargeCommission.getCommissionExpenseNetOfAdjustment()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AmountToWriteOff_Cell) at CommissionPayableReductionWizardTargetsStepScreen.pcf: line 94, column 62
    function value_35 () : gw.pl.currency.MonetaryAmount {
      return chargeCommission.AmountToWriteOff
    }
    
    property get chargeCommission () : entity.ChargeCommission {
      return getIteratedValue(1) as entity.ChargeCommission
    }
    
    
  }
  
  
}