package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessActivitiesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyProcessActivitiesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessActivitiesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyProcessActivitiesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DelinquencyProcessActivitiesLV.pcf: line 31, column 25
    function sortValue_0 (Activity :  entity.Activity) : java.lang.Object {
      return Activity.Status
    }
    
    // 'value' attribute on DateCell (id=CreateDate_Cell) at DelinquencyProcessActivitiesLV.pcf: line 37, column 40
    function sortValue_1 (Activity :  entity.Activity) : java.lang.Object {
      return Activity.CreateTime
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at DelinquencyProcessActivitiesLV.pcf: line 43, column 25
    function sortValue_2 (Activity :  entity.Activity) : java.lang.Object {
      return Activity.Type
    }
    
    // 'value' attribute on CheckBoxCell (id=Approved_Cell) at DelinquencyProcessActivitiesLV.pcf: line 47, column 38
    function sortValue_3 (Activity :  entity.Activity) : java.lang.Object {
      return Activity.Approved
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at DelinquencyProcessActivitiesLV.pcf: line 52, column 37
    function sortValue_4 (Activity :  entity.Activity) : java.lang.Object {
      return Activity.Subject
    }
    
    // 'value' attribute on BooleanRadioCell (id=Mandatory_Cell) at DelinquencyProcessActivitiesLV.pcf: line 56, column 39
    function sortValue_5 (Activity :  entity.Activity) : java.lang.Object {
      return Activity.Mandatory
    }
    
    // 'value' attribute on RowIterator at DelinquencyProcessActivitiesLV.pcf: line 16, column 37
    function value_27 () : entity.Activity[] {
      return DelinquencyProcess.Activities
    }
    
    property get DelinquencyProcess () : DelinquencyProcess {
      return getRequireValue("DelinquencyProcess", 0) as DelinquencyProcess
    }
    
    property set DelinquencyProcess ($arg :  DelinquencyProcess) {
      setRequireValue("DelinquencyProcess", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessActivitiesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DelinquencyProcessActivitiesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=ViewActivityDetails_Cell) at DelinquencyProcessActivitiesLV.pcf: line 23, column 97
    function action_6 () : void {
      ActivityDetailForward.goInWorkspace(Activity);
    }
    
    // 'available' attribute on TypeKeyCell (id=Status_Cell) at DelinquencyProcessActivitiesLV.pcf: line 31, column 25
    function available_7 () : java.lang.Boolean {
      return gw.api.web.admin.ActivityPatternsUtil.getActivityPattern("approval") == (Activity.ActivityPattern)
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DelinquencyProcessActivitiesLV.pcf: line 31, column 25
    function valueRoot_9 () : java.lang.Object {
      return Activity
    }
    
    // 'value' attribute on DateCell (id=CreateDate_Cell) at DelinquencyProcessActivitiesLV.pcf: line 37, column 40
    function value_12 () : java.util.Date {
      return Activity.CreateTime
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at DelinquencyProcessActivitiesLV.pcf: line 43, column 25
    function value_15 () : typekey.ActivityType {
      return Activity.Type
    }
    
    // 'value' attribute on CheckBoxCell (id=Approved_Cell) at DelinquencyProcessActivitiesLV.pcf: line 47, column 38
    function value_18 () : java.lang.Boolean {
      return Activity.Approved
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at DelinquencyProcessActivitiesLV.pcf: line 52, column 37
    function value_21 () : java.lang.String {
      return Activity.Subject
    }
    
    // 'value' attribute on BooleanRadioCell (id=Mandatory_Cell) at DelinquencyProcessActivitiesLV.pcf: line 56, column 39
    function value_24 () : java.lang.Boolean {
      return Activity.Mandatory
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DelinquencyProcessActivitiesLV.pcf: line 31, column 25
    function value_8 () : typekey.ActivityStatus {
      return Activity.Status
    }
    
    property get Activity () : entity.Activity {
      return getIteratedValue(1) as entity.Activity
    }
    
    
  }
  
  
}