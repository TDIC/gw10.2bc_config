package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralManualDrawdownConfirmScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CollateralManualDrawdownConfirmScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/collateral/CollateralManualDrawdownConfirmScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CollateralManualDrawdownConfirmScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=drawdownAmount_Input) at CollateralManualDrawdownConfirmScreen.pcf: line 23, column 46
    function currency_2 () : typekey.Currency {
      return collateralDrawdown.Collateral.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=drawdownAmount_Input) at CollateralManualDrawdownConfirmScreen.pcf: line 23, column 46
    function valueRoot_1 () : java.lang.Object {
      return collateralDrawdown
    }
    
    // 'value' attribute on MonetaryAmountInput (id=drawdownAmount_Input) at CollateralManualDrawdownConfirmScreen.pcf: line 23, column 46
    function value_0 () : gw.pl.currency.MonetaryAmount {
      return collateralDrawdown.Amount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=chargeAmount_Input) at CollateralManualDrawdownConfirmScreen.pcf: line 30, column 54
    function value_5 () : gw.pl.currency.MonetaryAmount {
      return collateralDrawdown.ChargeAmount
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=chargeAmount_Input) at CollateralManualDrawdownConfirmScreen.pcf: line 30, column 54
    function visible_4 () : java.lang.Boolean {
      return collateralDrawdown.CreateCharge
    }
    
    property get collateralDrawdown () : gw.api.web.collateral.CollateralDrawdownUtil {
      return getRequireValue("collateralDrawdown", 0) as gw.api.web.collateral.CollateralDrawdownUtil
    }
    
    property set collateralDrawdown ($arg :  gw.api.web.collateral.CollateralDrawdownUtil) {
      setRequireValue("collateralDrawdown", 0, $arg)
    }
    
    
  }
  
  
}