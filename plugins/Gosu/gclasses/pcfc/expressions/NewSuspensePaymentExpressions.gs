package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.web.payment.PaymentInstrumentFactory
@javax.annotation.Generated("config/web/pcf/suspensepayment/NewSuspensePayment.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewSuspensePaymentExpressions {
  @javax.annotation.Generated("config/web/pcf/suspensepayment/NewSuspensePayment.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewSuspensePaymentExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (currency :  Currency) : int {
      return 0
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at NewSuspensePayment.pcf: line 114, column 37
    function action_43 () : void {
      AccountSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=PolicyPicker) at NewSuspensePayment.pcf: line 132, column 36
    function action_53 () : void {
      PolicySearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at NewSuspensePayment.pcf: line 114, column 37
    function action_dest_44 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=PolicyPicker) at NewSuspensePayment.pcf: line 132, column 36
    function action_dest_54 () : pcf.api.Destination {
      return pcf.PolicySearchPopup.createDestination()
    }
    
    // 'afterCancel' attribute on Page (id=NewSuspensePayment) at NewSuspensePayment.pcf: line 15, column 64
    function afterCancel_66 () : void {
      DesktopSuspensePayments.go()
    }
    
    // 'afterCancel' attribute on Page (id=NewSuspensePayment) at NewSuspensePayment.pcf: line 15, column 64
    function afterCancel_dest_67 () : pcf.api.Destination {
      return pcf.DesktopSuspensePayments.createDestination()
    }
    
    // 'afterCommit' attribute on Page (id=NewSuspensePayment) at NewSuspensePayment.pcf: line 15, column 64
    function afterCommit_68 (TopLocation :  pcf.api.Location) : void {
      DesktopSuspensePayments.go()
    }
    
    // 'beforeCommit' attribute on Page (id=NewSuspensePayment) at NewSuspensePayment.pcf: line 15, column 64
    function beforeCommit_69 (pickedValue :  java.lang.Object) : void {
      finalizeSuspensePayment() 
    }
    
    // 'canVisit' attribute on Page (id=NewSuspensePayment) at NewSuspensePayment.pcf: line 15, column 64
    static function canVisit_70 (currency :  Currency) : java.lang.Boolean {
      return perm.System.susppmntview
    }
    
    // 'conversionExpression' attribute on TextInput (id=AccountNumber_Input) at NewSuspensePayment.pcf: line 114, column 37
    function conversionExpression_45 (PickedValue :  Account) : java.lang.String {
      return (PickedValue as Account).AccountNumber
    }
    
    // 'conversionExpression' attribute on TextInput (id=PolicyNumber_Input) at NewSuspensePayment.pcf: line 132, column 36
    function conversionExpression_55 (PickedValue :  PolicyPeriod) : java.lang.String {
      return (PickedValue as PolicyPeriod).PolicyNumber
    }
    
    // 'def' attribute on InputSetRef at NewSuspensePayment.pcf: line 96, column 69
    function def_onEnter_28 (def :  pcf.PaymentReceiptInputSet_ach) : void {
      def.onEnter(suspensePayment)
    }
    
    // 'def' attribute on InputSetRef at NewSuspensePayment.pcf: line 96, column 69
    function def_onEnter_30 (def :  pcf.PaymentReceiptInputSet_check) : void {
      def.onEnter(suspensePayment)
    }
    
    // 'def' attribute on InputSetRef at NewSuspensePayment.pcf: line 96, column 69
    function def_onEnter_32 (def :  pcf.PaymentReceiptInputSet_creditcard) : void {
      def.onEnter(suspensePayment)
    }
    
    // 'def' attribute on InputSetRef at NewSuspensePayment.pcf: line 96, column 69
    function def_onEnter_34 (def :  pcf.PaymentReceiptInputSet_default) : void {
      def.onEnter(suspensePayment)
    }
    
    // 'def' attribute on InputSetRef at NewSuspensePayment.pcf: line 96, column 69
    function def_onEnter_36 (def :  pcf.PaymentReceiptInputSet_wire) : void {
      def.onEnter(suspensePayment)
    }
    
    // 'def' attribute on InputSetRef at NewSuspensePayment.pcf: line 96, column 69
    function def_refreshVariables_29 (def :  pcf.PaymentReceiptInputSet_ach) : void {
      def.refreshVariables(suspensePayment)
    }
    
    // 'def' attribute on InputSetRef at NewSuspensePayment.pcf: line 96, column 69
    function def_refreshVariables_31 (def :  pcf.PaymentReceiptInputSet_check) : void {
      def.refreshVariables(suspensePayment)
    }
    
    // 'def' attribute on InputSetRef at NewSuspensePayment.pcf: line 96, column 69
    function def_refreshVariables_33 (def :  pcf.PaymentReceiptInputSet_creditcard) : void {
      def.refreshVariables(suspensePayment)
    }
    
    // 'def' attribute on InputSetRef at NewSuspensePayment.pcf: line 96, column 69
    function def_refreshVariables_35 (def :  pcf.PaymentReceiptInputSet_default) : void {
      def.refreshVariables(suspensePayment)
    }
    
    // 'def' attribute on InputSetRef at NewSuspensePayment.pcf: line 96, column 69
    function def_refreshVariables_37 (def :  pcf.PaymentReceiptInputSet_wire) : void {
      def.refreshVariables(suspensePayment)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amount_Input) at NewSuspensePayment.pcf: line 77, column 45
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      suspensePayment.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on RangeInput (id=PaymentInstrument_Input) at NewSuspensePayment.pcf: line 86, column 50
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      suspensePayment.PaymentInstrument = (__VALUE_TO_SET as entity.PaymentInstrument)
    }
    
    // 'value' attribute on TextInput (id=InvoiceNumber_Input) at NewSuspensePayment.pcf: line 101, column 52
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      suspensePayment.InvoiceNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at NewSuspensePayment.pcf: line 114, column 37
    function defaultSetter_47 (__VALUE_TO_SET :  java.lang.Object) : void {
      accountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on Choice (id=AccountChoice) at NewSuspensePayment.pcf: line 108, column 56
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      applyTo = (__VALUE_TO_SET as typekey.SuspensePaymentApplyTo)
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at NewSuspensePayment.pcf: line 132, column 36
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at NewSuspensePayment.pcf: line 164, column 34
    function defaultSetter_64 (__VALUE_TO_SET :  java.lang.Object) : void {
      description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=paymentDate_Input) at NewSuspensePayment.pcf: line 65, column 50
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      suspensePayment.PaymentDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'initialValue' attribute on Variable at NewSuspensePayment.pcf: line 24, column 38
    function initialValue_0 () : entity.SuspensePayment {
      return initializeSuspensePayment()
    }
    
    // 'initialValue' attribute on Variable at NewSuspensePayment.pcf: line 28, column 46
    function initialValue_1 () : typekey.SuspensePaymentApplyTo {
      return suspensePayment.CurrentApplyTo
    }
    
    // 'initialValue' attribute on Variable at NewSuspensePayment.pcf: line 32, column 32
    function initialValue_2 () : java.lang.String {
      return suspensePayment.AccountNumber
    }
    
    // 'initialValue' attribute on Variable at NewSuspensePayment.pcf: line 36, column 32
    function initialValue_3 () : java.lang.String {
      return suspensePayment.PolicyNumber
    }
    
    // 'initialValue' attribute on Variable at NewSuspensePayment.pcf: line 40, column 32
    function initialValue_4 () : java.lang.String {
      return suspensePayment.ProducerName
    }
    
    // 'initialValue' attribute on Variable at NewSuspensePayment.pcf: line 44, column 32
    function initialValue_5 () : java.lang.String {
      return suspensePayment.Description
    }
    
    // 'initialValue' attribute on Variable at NewSuspensePayment.pcf: line 48, column 49
    function initialValue_6 () : gw.payment.PaymentInstrumentRange {
      return new gw.payment.PaymentInstrumentRange( getInitialSuspensePaymentInstruments() )
    }
    
    // EditButtons at NewSuspensePayment.pcf: line 55, column 32
    function label_7 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'mode' attribute on InputSetRef at NewSuspensePayment.pcf: line 96, column 69
    function mode_38 () : java.lang.Object {
      return suspensePayment.PaymentInstrument.PaymentMethod
    }
    
    // 'onPick' attribute on RangeInput (id=PaymentInstrument_Input) at NewSuspensePayment.pcf: line 86, column 50
    function onPick_20 (PickedValue :  java.lang.Object) : void {
      paymentInstrumentRange.addPaymentInstrument(suspensePayment.PaymentInstrument)
    }
    
    // 'option' attribute on Choice (id=AccountChoice) at NewSuspensePayment.pcf: line 108, column 56
    function option_49 () : java.lang.Object {
      return SuspensePaymentApplyTo.TC_ACCOUNT.Code
    }
    
    // 'option' attribute on Choice (id=PolicyChoice) at NewSuspensePayment.pcf: line 126, column 56
    function option_59 () : java.lang.Object {
      return SuspensePaymentApplyTo.TC_POLICY.Code
    }
    
    // 'parent' attribute on Page (id=NewSuspensePayment) at NewSuspensePayment.pcf: line 15, column 64
    static function parent_71 (currency :  Currency) : pcf.api.Destination {
      return pcf.DesktopSuspensePayments.createDestination()
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at NewSuspensePayment.pcf: line 86, column 50
    function valueRange_24 () : java.lang.Object {
      return paymentInstrumentRange.AvailableInstruments
    }
    
    // 'value' attribute on DateInput (id=paymentDate_Input) at NewSuspensePayment.pcf: line 65, column 50
    function valueRoot_10 () : java.lang.Object {
      return suspensePayment
    }
    
    // 'value' attribute on TextInput (id=Currency_Input) at NewSuspensePayment.pcf: line 70, column 43
    function value_12 () : typekey.Currency {
      return suspensePayment.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amount_Input) at NewSuspensePayment.pcf: line 77, column 45
    function value_15 () : gw.pl.currency.MonetaryAmount {
      return suspensePayment.Amount
    }
    
    // 'value' attribute on RangeInput (id=PaymentInstrument_Input) at NewSuspensePayment.pcf: line 86, column 50
    function value_21 () : entity.PaymentInstrument {
      return suspensePayment.PaymentInstrument
    }
    
    // 'value' attribute on TextInput (id=InvoiceNumber_Input) at NewSuspensePayment.pcf: line 101, column 52
    function value_39 () : java.lang.String {
      return suspensePayment.InvoiceNumber
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at NewSuspensePayment.pcf: line 114, column 37
    function value_46 () : java.lang.String {
      return accountNumber
    }
    
    // 'value' attribute on Choice (id=AccountChoice) at NewSuspensePayment.pcf: line 108, column 56
    function value_50 () : typekey.SuspensePaymentApplyTo {
      return applyTo
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at NewSuspensePayment.pcf: line 132, column 36
    function value_56 () : java.lang.String {
      return policyNumber
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at NewSuspensePayment.pcf: line 164, column 34
    function value_63 () : java.lang.String {
      return description
    }
    
    // 'value' attribute on DateInput (id=paymentDate_Input) at NewSuspensePayment.pcf: line 65, column 50
    function value_8 () : java.util.Date {
      return suspensePayment.PaymentDate
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at NewSuspensePayment.pcf: line 86, column 50
    function verifyValueRangeIsAllowedType_25 ($$arg :  entity.PaymentInstrument[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at NewSuspensePayment.pcf: line 86, column 50
    function verifyValueRangeIsAllowedType_25 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentInstrument>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at NewSuspensePayment.pcf: line 86, column 50
    function verifyValueRangeIsAllowedType_25 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at NewSuspensePayment.pcf: line 86, column 50
    function verifyValueRange_26 () : void {
      var __valueRangeArg = paymentInstrumentRange.AvailableInstruments
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_25(__valueRangeArg)
    }
    
    override property get CurrentLocation () : pcf.NewSuspensePayment {
      return super.CurrentLocation as pcf.NewSuspensePayment
    }
    
    property get accountNumber () : java.lang.String {
      return getVariableValue("accountNumber", 0) as java.lang.String
    }
    
    property set accountNumber ($arg :  java.lang.String) {
      setVariableValue("accountNumber", 0, $arg)
    }
    
    property get applyTo () : typekey.SuspensePaymentApplyTo {
      return getVariableValue("applyTo", 0) as typekey.SuspensePaymentApplyTo
    }
    
    property set applyTo ($arg :  typekey.SuspensePaymentApplyTo) {
      setVariableValue("applyTo", 0, $arg)
    }
    
    property get currency () : Currency {
      return getVariableValue("currency", 0) as Currency
    }
    
    property set currency ($arg :  Currency) {
      setVariableValue("currency", 0, $arg)
    }
    
    property get description () : java.lang.String {
      return getVariableValue("description", 0) as java.lang.String
    }
    
    property set description ($arg :  java.lang.String) {
      setVariableValue("description", 0, $arg)
    }
    
    property get paymentInstrumentRange () : gw.payment.PaymentInstrumentRange {
      return getVariableValue("paymentInstrumentRange", 0) as gw.payment.PaymentInstrumentRange
    }
    
    property set paymentInstrumentRange ($arg :  gw.payment.PaymentInstrumentRange) {
      setVariableValue("paymentInstrumentRange", 0, $arg)
    }
    
    property get policyNumber () : java.lang.String {
      return getVariableValue("policyNumber", 0) as java.lang.String
    }
    
    property set policyNumber ($arg :  java.lang.String) {
      setVariableValue("policyNumber", 0, $arg)
    }
    
    property get producerName () : java.lang.String {
      return getVariableValue("producerName", 0) as java.lang.String
    }
    
    property set producerName ($arg :  java.lang.String) {
      setVariableValue("producerName", 0, $arg)
    }
    
    property get suspensePayment () : entity.SuspensePayment {
      return getVariableValue("suspensePayment", 0) as entity.SuspensePayment
    }
    
    property set suspensePayment ($arg :  entity.SuspensePayment) {
      setVariableValue("suspensePayment", 0, $arg)
    }
    
    
    function getInitialSuspensePaymentInstruments() : java.util.List<PaymentInstrument> {
      var instruments = new java.util.ArrayList<PaymentInstrument>() { 
      // HermiaK, 10/17/2014, US947: Removing "Cash" from Payment Instrument drop down
      // gw.api.web.payment.PaymentInstrumentFactory.getCashPaymentInstrument(),
        gw.api.web.payment.PaymentInstrumentFactory.getCheckPaymentInstrument()
      }
        
      return instruments
    }
    
    function finalizeSuspensePayment() {
      suspensePayment.setApplyToTarget(applyTo, accountNumber, policyNumber, producerName)
      suspensePayment.Description = description
      suspensePayment.createSuspensePayment()
    }
    
    function initializeSuspensePayment(): SuspensePayment{
      var suspensePay = new SuspensePayment(CurrentLocation, currency)
      suspensePay.PaymentInstrument = PaymentInstrumentFactory.CheckPaymentInstrument
      return suspensePay
    }
    
    
  }
  
  
}