package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyRoleScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransferPolicyRoleScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyRoleScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransferPolicyRoleScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyInput (id=Role_Input) at TransferPolicyRoleScreen.pcf: line 24, column 42
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyTransfer.RoleToTransfer = (__VALUE_TO_SET as typekey.PolicyRole)
    }
    
    // 'filter' attribute on TypeKeyInput (id=Role_Input) at TransferPolicyRoleScreen.pcf: line 24, column 42
    function filter_3 (VALUE :  typekey.PolicyRole, VALUES :  typekey.PolicyRole[]) : java.util.List<typekey.PolicyRole> {
      return VALUES.toList().where(\key -> not policyTransfer.PolicyPeriod.AgencyBill or PolicyRole.TC_PRIMARY != null)
    }
    
    // 'value' attribute on TypeKeyInput (id=Role_Input) at TransferPolicyRoleScreen.pcf: line 24, column 42
    function valueRoot_2 () : java.lang.Object {
      return policyTransfer
    }
    
    // 'value' attribute on TypeKeyInput (id=Role_Input) at TransferPolicyRoleScreen.pcf: line 24, column 42
    function value_0 () : typekey.PolicyRole {
      return policyTransfer.RoleToTransfer
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at TransferPolicyRoleScreen.pcf: line 31, column 70
    function value_5 () : java.lang.String {
      return getCurrentProducer(policyTransfer.RoleToTransfer)
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at TransferPolicyRoleScreen.pcf: line 35, column 66
    function value_7 () : java.lang.String {
      return getCurrentCode(policyTransfer.RoleToTransfer)
    }
    
    property get policyTransfer () : PolTransferByRole {
      return getRequireValue("policyTransfer", 0) as PolTransferByRole
    }
    
    property set policyTransfer ($arg :  PolTransferByRole) {
      setRequireValue("policyTransfer", 0, $arg)
    }
    
    function getCurrentProducer(role : PolicyRole) : String {
      var currentProducer = policyTransfer.PolicyPeriod.getDefaultProducerCodeForRole(role)
      return (currentProducer == null) ? DisplayKey.get("Web.TransferPolicyProducer.None") : currentProducer.Producer.DisplayName
    }
    
    function getCurrentCode(role : PolicyRole) : String {
      var currentProducer = policyTransfer.PolicyPeriod.getDefaultProducerCodeForRole(role)
      return (currentProducer == null) ? DisplayKey.get("Web.TransferPolicyProducer.None") : currentProducer.Code
    }
    
    
  }
  
  
}