package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicySummaryFinancialsDV.Policy.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicySummaryFinancialsDV_PolicyExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicySummaryFinancialsDV.Policy.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicySummaryFinancialsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MonetaryAmountInput (id=ExpectedPayment_Input) at PolicySummaryFinancialsDV.Policy.pcf: line 21, column 59
    function action_1 () : void {
      PolicyDetailPayments.go(policyPeriod, true)
    }
    
    // 'action' attribute on MenuItem (id=ReverseLateFeeAction) at PolicySummaryFinancialsDV.Policy.pcf: line 45, column 91
    function action_11 () : void {
      PolicyLateFeeReversalWizard.go(policyPeriod)
    }
    
    // 'action' attribute on MonetaryAmountInput (id=NextPaymentDue_Input) at PolicySummaryFinancialsDV.Policy.pcf: line 55, column 155
    function action_16 () : void {
      PolicyDetailPayments.go(policyPeriod, true)
    }
    
    // 'action' attribute on MonetaryAmountInput (id=LastPayment_Input) at PolicySummaryFinancialsDV.Policy.pcf: line 61, column 171
    function action_23 () : void {
      PolicyDetailPayments.go(policyPeriod, false)
    }
    
    // 'action' attribute on MenuItem (id=ReverseLateFeeAction) at PolicySummaryFinancialsDV.Policy.pcf: line 45, column 91
    function action_dest_12 () : pcf.api.Destination {
      return pcf.PolicyLateFeeReversalWizard.createDestination(policyPeriod)
    }
    
    // 'action' attribute on MonetaryAmountInput (id=NextPaymentDue_Input) at PolicySummaryFinancialsDV.Policy.pcf: line 55, column 155
    function action_dest_17 () : pcf.api.Destination {
      return pcf.PolicyDetailPayments.createDestination(policyPeriod, true)
    }
    
    // 'action' attribute on MonetaryAmountInput (id=ExpectedPayment_Input) at PolicySummaryFinancialsDV.Policy.pcf: line 21, column 59
    function action_dest_2 () : pcf.api.Destination {
      return pcf.PolicyDetailPayments.createDestination(policyPeriod, true)
    }
    
    // 'action' attribute on MonetaryAmountInput (id=LastPayment_Input) at PolicySummaryFinancialsDV.Policy.pcf: line 61, column 171
    function action_dest_24 () : pcf.api.Destination {
      return pcf.PolicyDetailPayments.createDestination(policyPeriod, false)
    }
    
    // 'available' attribute on MonetaryAmountInput (id=ExpectedPayment_Input) at PolicySummaryFinancialsDV.Policy.pcf: line 21, column 59
    function available_0 () : java.lang.Boolean {
      return summaryHelper.Financials.ExpectedPayment != null and summaryHelper.Financials.ExpectedPayment.IsNotZero
    }
    
    // 'available' attribute on MenuItem (id=ReverseLateFeeAction) at PolicySummaryFinancialsDV.Policy.pcf: line 45, column 91
    function available_10 () : java.lang.Boolean {
      return summaryHelper.Financials.UnsettledLateFeesAmount.IsNotZero
    }
    
    // 'available' attribute on MonetaryAmountInput (id=LastPayment_Input) at PolicySummaryFinancialsDV.Policy.pcf: line 61, column 171
    function available_22 () : java.lang.Boolean {
      return summaryHelper.Financials.LastPayment != null
    }
    
    // 'iconColor' attribute on Link (id=PastDueFlag) at PolicySummaryFinancialsDV.Policy.pcf: line 30, column 63
    function iconColor_8 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_PROGRESS_OVERDUE
    }
    
    // 'label' attribute on MonetaryAmountInput (id=NextPaymentDue_Input) at PolicySummaryFinancialsDV.Policy.pcf: line 55, column 155
    function label_18 () : java.lang.Object {
      return summaryHelper.Financials.NextInvoiceDueLabel
    }
    
    // 'label' attribute on MonetaryAmountInput (id=LastPayment_Input) at PolicySummaryFinancialsDV.Policy.pcf: line 61, column 171
    function label_25 () : java.lang.Object {
      return summaryHelper.Financials.LastPaymentReceivedLabel
    }
    
    // 'label' attribute on Link (id=PastDue) at PolicySummaryFinancialsDV.Policy.pcf: line 33, column 71
    function label_9 () : java.lang.Object {
      return policyPeriod.DelinquentAmount.renderWithZeroDash()
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ExpectedPayment_Input) at PolicySummaryFinancialsDV.Policy.pcf: line 21, column 59
    function valueRoot_4 () : java.lang.Object {
      return summaryHelper.Financials
    }
    
    // 'value' attribute on MonetaryAmountInput (id=LateFee_Input) at PolicySummaryFinancialsDV.Policy.pcf: line 39, column 66
    function value_13 () : gw.pl.currency.MonetaryAmount {
      return summaryHelper.Financials.UnsettledLateFeesAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=NextPaymentDue_Input) at PolicySummaryFinancialsDV.Policy.pcf: line 55, column 155
    function value_19 () : gw.pl.currency.MonetaryAmount {
      return summaryHelper.Financials.NextDueInvoices.IsEmpty ? null : summaryHelper.Financials.NextDueInvoices*.AmountDue.sum(policyPeriod.Currency)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=LastPayment_Input) at PolicySummaryFinancialsDV.Policy.pcf: line 61, column 171
    function value_26 () : gw.pl.currency.MonetaryAmount {
      return summaryHelper.Financials.LastPayment != null ? summaryHelper.Financials.LastPayment.Amount : new gw.pl.currency.MonetaryAmount(0, policyPeriod.Currency)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ExpectedPayment_Input) at PolicySummaryFinancialsDV.Policy.pcf: line 21, column 59
    function value_3 () : gw.pl.currency.MonetaryAmount {
      return summaryHelper.Financials.ExpectedPayment
    }
    
    // 'visible' attribute on Link (id=PastDueFlag) at PolicySummaryFinancialsDV.Policy.pcf: line 30, column 63
    function visible_7 () : java.lang.Boolean {
      return policyPeriod.DelinquentAmount.IsPositive
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get summaryHelper () : gw.web.policy.PolicySummaryHelper {
      return getRequireValue("summaryHelper", 0) as gw.web.policy.PolicySummaryHelper
    }
    
    property set summaryHelper ($arg :  gw.web.policy.PolicySummaryHelper) {
      setRequireValue("summaryHelper", 0, $arg)
    }
    
    
  }
  
  
}