package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/ActivityDetailToolbarButtonSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ActivityDetailToolbarButtonSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/activity/ActivityDetailToolbarButtonSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ActivityDetailToolbarButtonSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=ActivityDetailToolbarButtons_UpdateButton) at ActivityDetailToolbarButtonSet.default.pcf: line 19, column 29
    function action_1 () : void {
      gw.acc.activityenhancement.DesktopQueuedActivitiesUtil.assignActivity(assigneeHolder[0] , activity); CurrentLocation.commit()
    }
    
    // 'action' attribute on ToolbarButton (id=ActivityDetailToolbarButtons_EditButton) at ActivityDetailToolbarButtonSet.default.pcf: line 24, column 56
    function action_3 () : void {
      CurrentLocation.startEditing()
    }
    
    // 'action' attribute on ToolbarButton (id=ActivityDetailToolbarButtons_CompleteButton) at ActivityDetailToolbarButtonSet.default.pcf: line 29, column 97
    function action_5 () : void {
      if (not CurrentLocation.InEditMode) {CurrentLocation.startEditing()}; gw.api.web.activity.ActivityUtil.completeActivity(activity); CurrentLocation.commit();
    }
    
    // 'action' attribute on ToolbarButton (id=ActivityDetailToolbarButtons_CancelButton) at ActivityDetailToolbarButtonSet.default.pcf: line 34, column 29
    function action_7 () : void {
      CurrentLocation.cancel()
    }
    
    // 'action' attribute on ToolbarButton (id=ActivityDetailToolbarButtons_CloseButton) at ActivityDetailToolbarButtonSet.default.pcf: line 39, column 33
    function action_9 () : void {
      CurrentLocation.cancel()
    }
    
    // 'visible' attribute on ToolbarButton (id=ActivityDetailToolbarButtons_UpdateButton) at ActivityDetailToolbarButtonSet.default.pcf: line 19, column 29
    function visible_0 () : java.lang.Boolean {
      return InEditMode
    }
    
    // 'visible' attribute on ToolbarButton (id=ActivityDetailToolbarButtons_EditButton) at ActivityDetailToolbarButtonSet.default.pcf: line 24, column 56
    function visible_2 () : java.lang.Boolean {
      return activity.canEdit() and not InEditMode
    }
    
    // 'visible' attribute on ToolbarButton (id=ActivityDetailToolbarButtons_CompleteButton) at ActivityDetailToolbarButtonSet.default.pcf: line 29, column 97
    function visible_4 () : java.lang.Boolean {
      return (activity.canComplete()) and (activity.canRecur() == false) and not InEditMode
    }
    
    // 'visible' attribute on ToolbarButton (id=ActivityDetailToolbarButtons_CloseButton) at ActivityDetailToolbarButtonSet.default.pcf: line 39, column 33
    function visible_8 () : java.lang.Boolean {
      return not InEditMode
    }
    
    property get activity () : Activity {
      return getRequireValue("activity", 0) as Activity
    }
    
    property set activity ($arg :  Activity) {
      setRequireValue("activity", 0, $arg)
    }
    
    property get assigneeHolder () : gw.api.assignment.Assignee[] {
      return getRequireValue("assigneeHolder", 0) as gw.api.assignment.Assignee[]
    }
    
    property set assigneeHolder ($arg :  gw.api.assignment.Assignee[]) {
      setRequireValue("assigneeHolder", 0, $arg)
    }
    
    function assignIfNotShared() {
      if(!gw.api.web.activity.ActivityUtil.isShared(activity)) {
        assigneeHolder[0].assignToThis(activity);
      }
    }
    
    
  }
  
  
}