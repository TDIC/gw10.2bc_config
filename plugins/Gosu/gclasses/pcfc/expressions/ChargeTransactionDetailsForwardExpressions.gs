package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/ChargeTransactionDetailsForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChargeTransactionDetailsForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/account/ChargeTransactionDetailsForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChargeTransactionDetailsForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (charge :  Charge) : int {
      return 0
    }
    
    // 'action' attribute on ForwardCondition at ChargeTransactionDetailsForward.pcf: line 14, column 70
    function action_0 () : void {
      TransactionDetailPopup.push(charge.ChargeInitialTxn)
    }
    
    // 'action' attribute on ForwardCondition at ChargeTransactionDetailsForward.pcf: line 14, column 70
    function action_dest_1 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(charge.ChargeInitialTxn)
    }
    
    override property get CurrentLocation () : pcf.ChargeTransactionDetailsForward {
      return super.CurrentLocation as pcf.ChargeTransactionDetailsForward
    }
    
    property get charge () : Charge {
      return getVariableValue("charge", 0) as Charge
    }
    
    property set charge ($arg :  Charge) {
      setVariableValue("charge", 0, $arg)
    }
    
    
  }
  
  
}