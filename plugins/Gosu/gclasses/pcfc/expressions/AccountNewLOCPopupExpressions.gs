package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/AccountNewLOCPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountNewLOCPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/collateral/AccountNewLOCPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountNewLOCPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (collateral :  Collateral) : int {
      return 0
    }
    
    // 'afterCommit' attribute on Popup (id=AccountNewLOCPopup) at AccountNewLOCPopup.pcf: line 11, column 70
    function afterCommit_4 (TopLocation :  pcf.api.Location) : void {
      AccountCollateral.go(collateral.Account)
    }
    
    // 'afterEnter' attribute on Popup (id=AccountNewLOCPopup) at AccountNewLOCPopup.pcf: line 11, column 70
    function afterEnter_5 () : void {
      collateral.addToLetterOfCredits(newLOC)
    }
    
    // 'def' attribute on PanelRef at AccountNewLOCPopup.pcf: line 27, column 30
    function def_onEnter_2 (def :  pcf.LOCDV) : void {
      def.onEnter(newLOC)
    }
    
    // 'def' attribute on PanelRef at AccountNewLOCPopup.pcf: line 27, column 30
    function def_refreshVariables_3 (def :  pcf.LOCDV) : void {
      def.refreshVariables(newLOC)
    }
    
    // 'initialValue' attribute on Variable at AccountNewLOCPopup.pcf: line 20, column 30
    function initialValue_0 () : LetterOfCredit {
      return new LetterOfCredit(collateral.Currency)
    }
    
    // EditButtons at AccountNewLOCPopup.pcf: line 24, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    override property get CurrentLocation () : pcf.AccountNewLOCPopup {
      return super.CurrentLocation as pcf.AccountNewLOCPopup
    }
    
    property get collateral () : Collateral {
      return getVariableValue("collateral", 0) as Collateral
    }
    
    property set collateral ($arg :  Collateral) {
      setVariableValue("collateral", 0, $arg)
    }
    
    property get newLOC () : LetterOfCredit {
      return getVariableValue("newLOC", 0) as LetterOfCredit
    }
    
    property set newLOC ($arg :  LetterOfCredit) {
      setVariableValue("newLOC", 0, $arg)
    }
    
    
  }
  
  
}