package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.CurrencyUtil
@javax.annotation.Generated("config/web/pcf/admin/commission/NewCommissionPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewCommissionPlanExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/commission/NewCommissionPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewCommissionPlanExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (currency :  Currency) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=NewCommissionPlan) at NewCommissionPlan.pcf: line 14, column 69
    function afterCancel_3 () : void {
      CommissionPlans.go()
    }
    
    // 'afterCancel' attribute on Page (id=NewCommissionPlan) at NewCommissionPlan.pcf: line 14, column 69
    function afterCancel_dest_4 () : pcf.api.Destination {
      return pcf.CommissionPlans.createDestination()
    }
    
    // 'afterCommit' attribute on Page (id=NewCommissionPlan) at NewCommissionPlan.pcf: line 14, column 69
    function afterCommit_5 (TopLocation :  pcf.api.Location) : void {
      CommissionPlans.go()
    }
    
    // 'canVisit' attribute on Page (id=NewCommissionPlan) at NewCommissionPlan.pcf: line 14, column 69
    static function canVisit_6 (currency :  Currency) : java.lang.Boolean {
      return perm.System.admintabview and perm.System.commplancreate
    }
    
    // 'def' attribute on ScreenRef at NewCommissionPlan.pcf: line 25, column 57
    function def_onEnter_1 (def :  pcf.CommissionPlanDetailScreen) : void {
      def.onEnter(commissionPlan)
    }
    
    // 'def' attribute on ScreenRef at NewCommissionPlan.pcf: line 25, column 57
    function def_refreshVariables_2 (def :  pcf.CommissionPlanDetailScreen) : void {
      def.refreshVariables(commissionPlan)
    }
    
    // 'initialValue' attribute on Variable at NewCommissionPlan.pcf: line 20, column 30
    function initialValue_0 () : CommissionPlan {
      return initCommissionPlan()
    }
    
    // 'parent' attribute on Page (id=NewCommissionPlan) at NewCommissionPlan.pcf: line 14, column 69
    static function parent_7 (currency :  Currency) : pcf.api.Destination {
      return pcf.CommissionPlans.createDestination()
    }
    
    override property get CurrentLocation () : pcf.NewCommissionPlan {
      return super.CurrentLocation as pcf.NewCommissionPlan
    }
    
    property get commissionPlan () : CommissionPlan {
      return getVariableValue("commissionPlan", 0) as CommissionPlan
    }
    
    property set commissionPlan ($arg :  CommissionPlan) {
      setVariableValue("commissionPlan", 0, $arg)
    }
    
    property get currency () : Currency {
      return getVariableValue("currency", 0) as Currency
    }
    
    property set currency ($arg :  Currency) {
      setVariableValue("currency", 0, $arg)
    }
    
    
    function initCommissionPlan() : CommissionPlan {
      var newCommissionPlan =  new CommissionPlan(CurrentLocation)
      if (not isMultiCurrencyMode()) {
        newCommissionPlan.addToCurrencies(currency)
      }
      newCommissionPlan.EffectiveDate = gw.api.util.DateUtil.currentDate();
      return newCommissionPlan;
    }
    
    function isMultiCurrencyMode(): boolean {
      return CurrencyUtil.isMultiCurrencyMode() and Currency.getTypeKeys(true).Count > 1
    }
    
    
  }
  
  
}