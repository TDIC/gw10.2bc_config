package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadReviewScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadReviewScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadReviewScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadReviewScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 166, column 51
    function def_onEnter_100 (def :  pcf.AdminDataUploadGroupLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 174, column 52
    function def_onEnter_102 (def :  pcf.AdminDataUploadRegionLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 182, column 55
    function def_onEnter_104 (def :  pcf.AdminDataUploadAttributeLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 190, column 55
    function def_onEnter_106 (def :  pcf.AdminDataUploadUserGroupLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 198, column 51
    function def_onEnter_108 (def :  pcf.AdminDataUploadQueueLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 206, column 53
    function def_onEnter_110 (def :  pcf.AdminDataUploadHolidayLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 214, column 64
    function def_onEnter_112 (def :  pcf.AdminDataUploadCollectionAgenciesLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 222, column 58
    function def_onEnter_115 (def :  pcf.AdminDataUploadLocalizationLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 102, column 50
    function def_onEnter_84 (def :  pcf.AdminDataUploadRoleLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 110, column 56
    function def_onEnter_86 (def :  pcf.AdminDataUploadActPatternLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 118, column 53
    function def_onEnter_88 (def :  pcf.AdminDataUploadSecZoneLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 126, column 62
    function def_onEnter_90 (def :  pcf.AdminDataUploadAuthLimitProfileLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 134, column 55
    function def_onEnter_92 (def :  pcf.AdminDataUploadAuthLimitLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 142, column 59
    function def_onEnter_94 (def :  pcf.AdminDataUploadUserAuthLimitLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 150, column 53
    function def_onEnter_96 (def :  pcf.AdminDataUploadAddressLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 158, column 50
    function def_onEnter_98 (def :  pcf.AdminDataUploadUserLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 166, column 51
    function def_refreshVariables_101 (def :  pcf.AdminDataUploadGroupLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 174, column 52
    function def_refreshVariables_103 (def :  pcf.AdminDataUploadRegionLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 182, column 55
    function def_refreshVariables_105 (def :  pcf.AdminDataUploadAttributeLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 190, column 55
    function def_refreshVariables_107 (def :  pcf.AdminDataUploadUserGroupLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 198, column 51
    function def_refreshVariables_109 (def :  pcf.AdminDataUploadQueueLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 206, column 53
    function def_refreshVariables_111 (def :  pcf.AdminDataUploadHolidayLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 214, column 64
    function def_refreshVariables_113 (def :  pcf.AdminDataUploadCollectionAgenciesLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 222, column 58
    function def_refreshVariables_116 (def :  pcf.AdminDataUploadLocalizationLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 102, column 50
    function def_refreshVariables_85 (def :  pcf.AdminDataUploadRoleLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 110, column 56
    function def_refreshVariables_87 (def :  pcf.AdminDataUploadActPatternLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 118, column 53
    function def_refreshVariables_89 (def :  pcf.AdminDataUploadSecZoneLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 126, column 62
    function def_refreshVariables_91 (def :  pcf.AdminDataUploadAuthLimitProfileLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 134, column 55
    function def_refreshVariables_93 (def :  pcf.AdminDataUploadAuthLimitLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 142, column 59
    function def_refreshVariables_95 (def :  pcf.AdminDataUploadUserAuthLimitLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 150, column 53
    function def_refreshVariables_97 (def :  pcf.AdminDataUploadAddressLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadReviewScreen.pcf: line 158, column 50
    function def_refreshVariables_99 (def :  pcf.AdminDataUploadUserLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkSecurityZone_Input) at AdminDataUploadReviewScreen.pcf: line 31, column 46
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkSecurityZone = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkRole_Input) at AdminDataUploadReviewScreen.pcf: line 21, column 38
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkRole = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkAuthorityLimit_Input) at AdminDataUploadReviewScreen.pcf: line 36, column 48
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkAuthorityLimit = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkUserAuthorityLimit_Input) at AdminDataUploadReviewScreen.pcf: line 43, column 52
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkUserAuthorityLimit = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkUser_Input) at AdminDataUploadReviewScreen.pcf: line 48, column 38
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkUser = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkGroup_Input) at AdminDataUploadReviewScreen.pcf: line 53, column 39
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkGroup = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkUserGroup_Input) at AdminDataUploadReviewScreen.pcf: line 58, column 43
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkUserGroup = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkRegion_Input) at AdminDataUploadReviewScreen.pcf: line 65, column 40
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkRegion = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkAttribute_Input) at AdminDataUploadReviewScreen.pcf: line 70, column 43
    function defaultSetter_56 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkAttribute = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkQueue_Input) at AdminDataUploadReviewScreen.pcf: line 75, column 39
    function defaultSetter_62 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkQueue = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkHoliday_Input) at AdminDataUploadReviewScreen.pcf: line 80, column 41
    function defaultSetter_68 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkHoliday = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkCollectionAgency_Input) at AdminDataUploadReviewScreen.pcf: line 88, column 42
    function defaultSetter_74 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkCollectionAgencies = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkActivityPattern_Input) at AdminDataUploadReviewScreen.pcf: line 26, column 49
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkActivityPattern = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkLocalization_Input) at AdminDataUploadReviewScreen.pcf: line 94, column 42
    function defaultSetter_80 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkLocalization = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkRole_Input) at AdminDataUploadReviewScreen.pcf: line 21, column 38
    function label_0 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Roles"), processor.RoleArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkSecurityZone_Input) at AdminDataUploadReviewScreen.pcf: line 31, column 46
    function label_12 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.AdminData.SecurityZones"), processor.SecurityZoneArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkAuthorityLimit_Input) at AdminDataUploadReviewScreen.pcf: line 36, column 48
    function label_18 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.AdminData.AuthorityLimits"), processor.AuthorityLimitArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkUserAuthorityLimit_Input) at AdminDataUploadReviewScreen.pcf: line 43, column 52
    function label_24 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserAuthorityLimits"), processor.UserAuthorityLimitArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkUser_Input) at AdminDataUploadReviewScreen.pcf: line 48, column 38
    function label_30 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Users"), processor.UserArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkGroup_Input) at AdminDataUploadReviewScreen.pcf: line 53, column 39
    function label_36 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Groups"), processor.GroupArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkUserGroup_Input) at AdminDataUploadReviewScreen.pcf: line 58, column 43
    function label_42 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserGroups"), processor.UserGroupArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkRegion_Input) at AdminDataUploadReviewScreen.pcf: line 65, column 40
    function label_48 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Regions"), processor.RegionArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkAttribute_Input) at AdminDataUploadReviewScreen.pcf: line 70, column 43
    function label_54 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Attributes"), processor.AttributeArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkActivityPattern_Input) at AdminDataUploadReviewScreen.pcf: line 26, column 49
    function label_6 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPatterns"), processor.ActivityPatternArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkQueue_Input) at AdminDataUploadReviewScreen.pcf: line 75, column 39
    function label_60 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Queues"), processor.QueueArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkHoliday_Input) at AdminDataUploadReviewScreen.pcf: line 80, column 41
    function label_66 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Holidays"), processor.HolidayArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkCollectionAgency_Input) at AdminDataUploadReviewScreen.pcf: line 88, column 42
    function label_72 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.CollectionAgencies"), processor.CollectionAgencyArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkLocalization_Input) at AdminDataUploadReviewScreen.pcf: line 94, column 42
    function label_78 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations"), processor.LocalizationArray.Count)
    }
    
    // 'title' attribute on Card (id=collectionAgencies) at AdminDataUploadReviewScreen.pcf: line 212, column 109
    function title_114 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.CollectionAgencies")
    }
    
    // 'title' attribute on Card (id=localization) at AdminDataUploadReviewScreen.pcf: line 220, column 104
    function title_117 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations")
    }
    
    // 'value' attribute on CheckBoxInput (id=chkRole_Input) at AdminDataUploadReviewScreen.pcf: line 21, column 38
    function valueRoot_3 () : java.lang.Object {
      return processor
    }
    
    // 'value' attribute on CheckBoxInput (id=chkRole_Input) at AdminDataUploadReviewScreen.pcf: line 21, column 38
    function value_1 () : java.lang.Boolean {
      return processor.ChkRole
    }
    
    // 'value' attribute on CheckBoxInput (id=chkSecurityZone_Input) at AdminDataUploadReviewScreen.pcf: line 31, column 46
    function value_13 () : java.lang.Boolean {
      return processor.ChkSecurityZone
    }
    
    // 'value' attribute on CheckBoxInput (id=chkAuthorityLimit_Input) at AdminDataUploadReviewScreen.pcf: line 36, column 48
    function value_19 () : java.lang.Boolean {
      return processor.ChkAuthorityLimit
    }
    
    // 'value' attribute on CheckBoxInput (id=chkUserAuthorityLimit_Input) at AdminDataUploadReviewScreen.pcf: line 43, column 52
    function value_25 () : java.lang.Boolean {
      return processor.ChkUserAuthorityLimit
    }
    
    // 'value' attribute on CheckBoxInput (id=chkUser_Input) at AdminDataUploadReviewScreen.pcf: line 48, column 38
    function value_31 () : java.lang.Boolean {
      return processor.ChkUser
    }
    
    // 'value' attribute on CheckBoxInput (id=chkGroup_Input) at AdminDataUploadReviewScreen.pcf: line 53, column 39
    function value_37 () : java.lang.Boolean {
      return processor.ChkGroup
    }
    
    // 'value' attribute on CheckBoxInput (id=chkUserGroup_Input) at AdminDataUploadReviewScreen.pcf: line 58, column 43
    function value_43 () : java.lang.Boolean {
      return processor.ChkUserGroup
    }
    
    // 'value' attribute on CheckBoxInput (id=chkRegion_Input) at AdminDataUploadReviewScreen.pcf: line 65, column 40
    function value_49 () : java.lang.Boolean {
      return processor.ChkRegion
    }
    
    // 'value' attribute on CheckBoxInput (id=chkAttribute_Input) at AdminDataUploadReviewScreen.pcf: line 70, column 43
    function value_55 () : java.lang.Boolean {
      return processor.ChkAttribute
    }
    
    // 'value' attribute on CheckBoxInput (id=chkQueue_Input) at AdminDataUploadReviewScreen.pcf: line 75, column 39
    function value_61 () : java.lang.Boolean {
      return processor.ChkQueue
    }
    
    // 'value' attribute on CheckBoxInput (id=chkHoliday_Input) at AdminDataUploadReviewScreen.pcf: line 80, column 41
    function value_67 () : java.lang.Boolean {
      return processor.ChkHoliday
    }
    
    // 'value' attribute on CheckBoxInput (id=chkActivityPattern_Input) at AdminDataUploadReviewScreen.pcf: line 26, column 49
    function value_7 () : java.lang.Boolean {
      return processor.ChkActivityPattern
    }
    
    // 'value' attribute on CheckBoxInput (id=chkCollectionAgency_Input) at AdminDataUploadReviewScreen.pcf: line 88, column 42
    function value_73 () : java.lang.Boolean {
      return processor.ChkCollectionAgencies
    }
    
    // 'value' attribute on CheckBoxInput (id=chkLocalization_Input) at AdminDataUploadReviewScreen.pcf: line 94, column 42
    function value_79 () : java.lang.Boolean {
      return processor.ChkLocalization
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}