package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyItemBalanceInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyItemBalanceInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyItemBalanceInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyItemBalanceInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=UnpaidAmount_Input) at AgencyItemBalanceInputSet.pcf: line 16, column 49
    function currency_2 () : typekey.Currency {
      return invoiceItem.Currency
    }
    
    // 'outputConversion' attribute on TextInput (id=CommissionPercentage_Input) at AgencyItemBalanceInputSet.pcf: line 31, column 41
    function outputConversion_8 (VALUE :  java.math.BigDecimal) : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(VALUE, 3)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnpaidAmount_Input) at AgencyItemBalanceInputSet.pcf: line 16, column 49
    function valueRoot_1 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnpaidAmount_Input) at AgencyItemBalanceInputSet.pcf: line 16, column 49
    function value_0 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.GrossUnsettledAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Net_Input) at AgencyItemBalanceInputSet.pcf: line 38, column 47
    function value_11 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.NetAmountUnsettled
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnsettledCommission_Input) at AgencyItemBalanceInputSet.pcf: line 23, column 48
    function value_4 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.UnsettledCommission
    }
    
    // 'value' attribute on TextInput (id=CommissionPercentage_Input) at AgencyItemBalanceInputSet.pcf: line 31, column 41
    function value_9 () : java.math.BigDecimal {
      return invoiceItem.UnsettledCommission.percentageOfAsBigDecimal(invoiceItem.GrossUnsettledAmount)
    }
    
    property get invoiceItem () : InvoiceItem {
      return getRequireValue("invoiceItem", 0) as InvoiceItem
    }
    
    property set invoiceItem ($arg :  InvoiceItem) {
      setRequireValue("invoiceItem", 0, $arg)
    }
    
    
  }
  
  
}