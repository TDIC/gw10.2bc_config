package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/groups/GroupUsersLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class GroupUsersLVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/groups/GroupUsersLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class GroupUsersLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on PickerCell (id=User_Cell) at GroupUsersLV.pcf: line 32, column 36
    function sortValue_0 (GroupUser :  entity.GroupUser) : java.lang.Object {
      return GroupUser.User
    }
    
    // 'value' attribute on TypeKeyCell (id=VacationStatus_Cell) at GroupUsersLV.pcf: line 38, column 51
    function sortValue_1 (GroupUser :  entity.GroupUser) : java.lang.Object {
      return GroupUser.User.VacationStatus
    }
    
    // 'value' attribute on PickerCell (id=BackupUser_Cell) at GroupUsersLV.pcf: line 45, column 36
    function sortValue_2 (GroupUser :  entity.GroupUser) : java.lang.Object {
      return GroupUser.User.BackupUser
    }
    
    // 'toAdd' attribute on RowIterator at GroupUsersLV.pcf: line 20, column 38
    function toAdd_20 (GroupUser :  entity.GroupUser) : void {
      Group.addToUsers(GroupUser)
    }
    
    // 'toRemove' attribute on RowIterator at GroupUsersLV.pcf: line 20, column 38
    function toRemove_21 (GroupUser :  entity.GroupUser) : void {
      Group.removeFromUsers(GroupUser)
    }
    
    // 'value' attribute on RowIterator at GroupUsersLV.pcf: line 20, column 38
    function value_22 () : entity.GroupUser[] {
      return Group.Users
    }
    
    property get Group () : Group {
      return getRequireValue("Group", 0) as Group
    }
    
    property set Group ($arg :  Group) {
      setRequireValue("Group", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/groups/GroupUsersLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends GroupUsersLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=BackupUser_Cell) at GroupUsersLV.pcf: line 45, column 36
    function action_13 () : void {
      UserSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerCell (id=User_Cell) at GroupUsersLV.pcf: line 32, column 36
    function action_3 () : void {
      UserSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerCell (id=BackupUser_Cell) at GroupUsersLV.pcf: line 45, column 36
    function action_dest_14 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'pickLocation' attribute on PickerCell (id=User_Cell) at GroupUsersLV.pcf: line 32, column 36
    function action_dest_4 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'value' attribute on TypeKeyCell (id=VacationStatus_Cell) at GroupUsersLV.pcf: line 38, column 51
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      GroupUser.User.VacationStatus = (__VALUE_TO_SET as typekey.VacationStatusType)
    }
    
    // 'value' attribute on PickerCell (id=BackupUser_Cell) at GroupUsersLV.pcf: line 45, column 36
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      GroupUser.User.BackupUser = (__VALUE_TO_SET as entity.User)
    }
    
    // 'value' attribute on PickerCell (id=User_Cell) at GroupUsersLV.pcf: line 32, column 36
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      GroupUser.User = (__VALUE_TO_SET as entity.User)
    }
    
    // 'editable' attribute on Row at GroupUsersLV.pcf: line 22, column 74
    function editable_19 () : java.lang.Boolean {
      return GroupUser.User == null or GroupUser.User.isEditable()
    }
    
    // 'value' attribute on TypeKeyCell (id=VacationStatus_Cell) at GroupUsersLV.pcf: line 38, column 51
    function valueRoot_11 () : java.lang.Object {
      return GroupUser.User
    }
    
    // 'value' attribute on PickerCell (id=User_Cell) at GroupUsersLV.pcf: line 32, column 36
    function valueRoot_7 () : java.lang.Object {
      return GroupUser
    }
    
    // 'value' attribute on PickerCell (id=BackupUser_Cell) at GroupUsersLV.pcf: line 45, column 36
    function value_15 () : entity.User {
      return GroupUser.User.BackupUser
    }
    
    // 'value' attribute on PickerCell (id=User_Cell) at GroupUsersLV.pcf: line 32, column 36
    function value_5 () : entity.User {
      return GroupUser.User
    }
    
    // 'value' attribute on TypeKeyCell (id=VacationStatus_Cell) at GroupUsersLV.pcf: line 38, column 51
    function value_9 () : typekey.VacationStatusType {
      return GroupUser.User.VacationStatus
    }
    
    property get GroupUser () : entity.GroupUser {
      return getIteratedValue(1) as entity.GroupUser
    }
    
    
  }
  
  
}