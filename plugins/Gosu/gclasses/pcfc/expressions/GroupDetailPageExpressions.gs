package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/groups/GroupDetailPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class GroupDetailPageExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/groups/GroupDetailPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class GroupDetailPageExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (Group :  Group) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=GroupDetailPage_BasicCard_DeleteButton) at GroupDetailPage.pcf: line 27, column 40
    function action_3 () : void {
      gw.api.admin.BaseAdminUtil.deleteGroup(Group); Admin.go()
    }
    
    // 'available' attribute on ToolbarButton (id=GroupDetailPage_BasicCard_DeleteButton) at GroupDetailPage.pcf: line 27, column 40
    function available_1 () : java.lang.Boolean {
      return Group.SafeToDelete
    }
    
    // 'canEdit' attribute on Page (id=GroupDetailPage) at GroupDetailPage.pcf: line 10, column 82
    function canEdit_8 () : java.lang.Boolean {
      return perm.Group.edit
    }
    
    // 'canVisit' attribute on Page (id=GroupDetailPage) at GroupDetailPage.pcf: line 10, column 82
    static function canVisit_9 (Group :  Group) : java.lang.Boolean {
      return perm.Group.edit
    }
    
    // 'def' attribute on PanelRef at GroupDetailPage.pcf: line 30, column 37
    function def_onEnter_4 (def :  pcf.GroupDetailDV) : void {
      def.onEnter(Group)
    }
    
    // 'def' attribute on PanelRef at GroupDetailPage.pcf: line 32, column 272
    function def_onEnter_6 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(Group, { "Name" }, { (gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP) ? DisplayKey.get("Web.Admin.GroupDetail.BasicDV.NamePhonetic") : DisplayKey.get("Web.Admin.GroupDetail.BasicDV.Name") })
    }
    
    // 'def' attribute on PanelRef at GroupDetailPage.pcf: line 30, column 37
    function def_refreshVariables_5 (def :  pcf.GroupDetailDV) : void {
      def.refreshVariables(Group)
    }
    
    // 'def' attribute on PanelRef at GroupDetailPage.pcf: line 32, column 272
    function def_refreshVariables_7 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(Group, { "Name" }, { (gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP) ? DisplayKey.get("Web.Admin.GroupDetail.BasicDV.NamePhonetic") : DisplayKey.get("Web.Admin.GroupDetail.BasicDV.Name") })
    }
    
    // EditButtons at GroupDetailPage.pcf: line 19, column 23
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'parent' attribute on Page (id=GroupDetailPage) at GroupDetailPage.pcf: line 10, column 82
    static function parent_10 (Group :  Group) : pcf.api.Destination {
      return pcf.Admin.createDestination()
    }
    
    // 'title' attribute on Page (id=GroupDetailPage) at GroupDetailPage.pcf: line 10, column 82
    static function title_11 (Group :  Group) : java.lang.Object {
      return DisplayKey.get("Web.Admin.GroupDetail", Group.DisplayName)
    }
    
    // 'visible' attribute on ToolbarButton (id=GroupDetailPage_BasicCard_DeleteButton) at GroupDetailPage.pcf: line 27, column 40
    function visible_2 () : java.lang.Boolean {
      return perm.Group.delete
    }
    
    override property get CurrentLocation () : pcf.GroupDetailPage {
      return super.CurrentLocation as pcf.GroupDetailPage
    }
    
    property get Group () : Group {
      return getVariableValue("Group", 0) as Group
    }
    
    property set Group ($arg :  Group) {
      setVariableValue("Group", 0, $arg)
    }
    
    
  }
  
  
}