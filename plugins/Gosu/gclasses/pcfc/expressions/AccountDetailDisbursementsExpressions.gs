package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailDisbursements.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailDisbursementsExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailDisbursements.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailDisbursementsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    static function __constructorIndex (account :  Account, initialSelectedDisbursement :  Disbursement) : int {
      return 1
    }
    
    // 'afterReturnFromPopup' attribute on Page (id=AccountDetailDisbursements) at AccountDetailDisbursements.pcf: line 11, column 78
    function afterReturnFromPopup_4 (popupCommitted :  boolean) : void {
      if (popupCommitted) CurrentLocation.commit()
    }
    
    // 'canEdit' attribute on Page (id=AccountDetailDisbursements) at AccountDetailDisbursements.pcf: line 11, column 78
    function canEdit_5 () : java.lang.Boolean {
      return perm.System.disbcreate
    }
    
    // 'canVisit' attribute on Page (id=AccountDetailDisbursements) at AccountDetailDisbursements.pcf: line 11, column 78
    static function canVisit_6 (account :  Account, initialSelectedDisbursement :  Disbursement) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctdisbview
    }
    
    // 'def' attribute on PanelRef at AccountDetailDisbursements.pcf: line 35, column 76
    function def_onEnter_2 (def :  pcf.DisbursementsLV) : void {
      def.onEnter(disbursements, initialSelectedDisbursement)
    }
    
    // 'def' attribute on PanelRef at AccountDetailDisbursements.pcf: line 35, column 76
    function def_refreshVariables_3 (def :  pcf.DisbursementsLV) : void {
      def.refreshVariables(disbursements, initialSelectedDisbursement)
    }
    
    // 'initialValue' attribute on Variable at AccountDetailDisbursements.pcf: line 22, column 56
    function initialValue_0 () : gw.api.web.disbursement.DisbursementUtil {
      return new gw.api.web.disbursement.DisbursementUtil()
    }
    
    // 'initialValue' attribute on Variable at AccountDetailDisbursements.pcf: line 27, column 57
    function initialValue_1 () : java.util.List<entity.Disbursement> {
      return disbursementUtil.getAccountAndCollateralDisbursements(account)
    }
    
    // Page (id=AccountDetailDisbursements) at AccountDetailDisbursements.pcf: line 11, column 78
    static function parent_7 (account :  Account, initialSelectedDisbursement :  Disbursement) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.AccountDetailDisbursements {
      return super.CurrentLocation as pcf.AccountDetailDisbursements
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get disbursementUtil () : gw.api.web.disbursement.DisbursementUtil {
      return getVariableValue("disbursementUtil", 0) as gw.api.web.disbursement.DisbursementUtil
    }
    
    property set disbursementUtil ($arg :  gw.api.web.disbursement.DisbursementUtil) {
      setVariableValue("disbursementUtil", 0, $arg)
    }
    
    property get disbursements () : java.util.List<entity.Disbursement> {
      return getVariableValue("disbursements", 0) as java.util.List<entity.Disbursement>
    }
    
    property set disbursements ($arg :  java.util.List<entity.Disbursement>) {
      setVariableValue("disbursements", 0, $arg)
    }
    
    property get initialSelectedDisbursement () : Disbursement {
      return getVariableValue("initialSelectedDisbursement", 0) as Disbursement
    }
    
    property set initialSelectedDisbursement ($arg :  Disbursement) {
      setVariableValue("initialSelectedDisbursement", 0, $arg)
    }
    
    
  }
  
  
}