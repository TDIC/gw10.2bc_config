package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.LocationUtil
uses gw.plugin.contact.DuplicateContactResult
uses java.lang.Throwable
@javax.annotation.Generated("config/web/pcf/contact/DuplicateContactsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DuplicateContactsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/contact/DuplicateContactsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DuplicateContactsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (duplicateContactsPopupNavigator :  gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Popup (id=DuplicateContactsPopup) at DuplicateContactsPopup.pcf: line 8, column 74
    function afterCancel_46 () : void {
      displayWarningIfUserCanceledOutOfPageWithoutSelectingAnything()
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at DuplicateContactsPopup.pcf: line 50, column 47
    function def_onEnter_10 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at DuplicateContactsPopup.pcf: line 50, column 47
    function def_onEnter_12 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef at DuplicateContactsPopup.pcf: line 41, column 50
    function def_onEnter_3 (def :  pcf.ContactNameInputSet_company) : void {
      def.onEnter(originalContact)
    }
    
    // 'def' attribute on InputSetRef at DuplicateContactsPopup.pcf: line 41, column 50
    function def_onEnter_5 (def :  pcf.ContactNameInputSet_person) : void {
      def.onEnter(originalContact)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at DuplicateContactsPopup.pcf: line 50, column 47
    function def_onEnter_8 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at DuplicateContactsPopup.pcf: line 50, column 47
    function def_refreshVariables_11 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at DuplicateContactsPopup.pcf: line 50, column 47
    function def_refreshVariables_13 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef at DuplicateContactsPopup.pcf: line 41, column 50
    function def_refreshVariables_4 (def :  pcf.ContactNameInputSet_company) : void {
      def.refreshVariables(originalContact)
    }
    
    // 'def' attribute on InputSetRef at DuplicateContactsPopup.pcf: line 41, column 50
    function def_refreshVariables_6 (def :  pcf.ContactNameInputSet_person) : void {
      def.refreshVariables(originalContact)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at DuplicateContactsPopup.pcf: line 50, column 47
    function def_refreshVariables_9 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'initialValue' attribute on Variable at DuplicateContactsPopup.pcf: line 17, column 23
    function initialValue_0 () : Contact {
      return duplicateContactsPopupNavigator.ContactBeingCreatedThroughUI
    }
    
    // 'initialValue' attribute on Variable at DuplicateContactsPopup.pcf: line 21, column 62
    function initialValue_1 () : gw.pcf.duplicatecontacts.PotentialMatchChecker {
      return new gw.pcf.duplicatecontacts.PotentialMatchChecker(duplicateContactsPopupNavigator.DuplicateResultsContainer)
    }
    
    // 'initialValue' attribute on Variable at DuplicateContactsPopup.pcf: line 29, column 43
    function initialValue_2 () : gw.api.address.AddressOwner {
      return new gw.api.address.ContactAddressOwner(originalContact)
    }
    
    // 'label' attribute on Verbatim at DuplicateContactsPopup.pcf: line 68, column 42
    function label_21 () : java.lang.String {
      return getInstructionsText()
    }
    
    // 'mode' attribute on InputSetRef (id=globalAddressContainer) at DuplicateContactsPopup.pcf: line 50, column 47
    function mode_14 () : java.lang.Object {
      return gw.api.address.AddressCountrySettings.getSettings(addressOwner.SelectedCountry).PCFMode
    }
    
    // 'mode' attribute on InputSetRef at DuplicateContactsPopup.pcf: line 41, column 50
    function mode_7 () : java.lang.Object {
      return originalContact.Subtype.Code
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at DuplicateContactsPopup.pcf: line 93, column 52
    function sortValue_22 (duplicateResult :  gw.plugin.contact.DuplicateContactResult) : java.lang.Object {
      return duplicateResult.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Exact_Cell) at DuplicateContactsPopup.pcf: line 99, column 50
    function sortValue_23 (duplicateResult :  gw.plugin.contact.DuplicateContactResult) : java.lang.Object {
      return duplicateResult.MatchType
    }
    
    // 'value' attribute on TextCell (id=Address_Cell) at DuplicateContactsPopup.pcf: line 103, column 55
    function sortValue_24 (duplicateResult :  gw.plugin.contact.DuplicateContactResult) : java.lang.Object {
      return duplicateResult.DisplayAddress
    }
    
    // 'value' attribute on TextCell (id=PrimaryPhone_Cell) at DuplicateContactsPopup.pcf: line 107, column 58
    function sortValue_25 (duplicateResult :  gw.plugin.contact.DuplicateContactResult) : java.lang.Object {
      return duplicateResult.PrimaryPhoneValue
    }
    
    // 'value' attribute on DateCell (id=DOB_Cell) at DuplicateContactsPopup.pcf: line 112, column 60
    function sortValue_26 (duplicateResult :  gw.plugin.contact.DuplicateContactResult) : java.lang.Object {
      return duplicateResult.DateOfBirth
    }
    
    // 'value' attribute on TypeKeyInput (id=AddressType_Input) at DuplicateContactsPopup.pcf: line 57, column 46
    function valueRoot_16 () : java.lang.Object {
      return originalContact.PrimaryAddress
    }
    
    // 'value' attribute on TypeKeyInput (id=AddressType_Input) at DuplicateContactsPopup.pcf: line 57, column 46
    function value_15 () : typekey.AddressType {
      return originalContact.PrimaryAddress.AddressType
    }
    
    // 'value' attribute on TextInput (id=AddressDescription_Input) at DuplicateContactsPopup.pcf: line 62, column 65
    function value_18 () : java.lang.String {
      return originalContact.PrimaryAddress.Description
    }
    
    // 'value' attribute on RowIterator (id=ResultsList) at DuplicateContactsPopup.pcf: line 82, column 86
    function value_45 () : java.util.List<gw.plugin.contact.DuplicateContactResult> {
      return duplicateContactsPopupNavigator.DuplicateResultsContainer.Results
    }
    
    // 'visible' attribute on DateCell (id=DOB_Cell) at DuplicateContactsPopup.pcf: line 112, column 60
    function visible_27 () : java.lang.Boolean {
      return typeof(originalContact) == Person
    }
    
    override property get CurrentLocation () : pcf.DuplicateContactsPopup {
      return super.CurrentLocation as pcf.DuplicateContactsPopup
    }
    
    property get addressOwner () : gw.api.address.AddressOwner {
      return getVariableValue("addressOwner", 0) as gw.api.address.AddressOwner
    }
    
    property set addressOwner ($arg :  gw.api.address.AddressOwner) {
      setVariableValue("addressOwner", 0, $arg)
    }
    
    property get duplicateContactsPopupNavigator () : gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator {
      return getVariableValue("duplicateContactsPopupNavigator", 0) as gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator
    }
    
    property set duplicateContactsPopupNavigator ($arg :  gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator) {
      setVariableValue("duplicateContactsPopupNavigator", 0, $arg)
    }
    
    property get originalContact () : Contact {
      return getVariableValue("originalContact", 0) as Contact
    }
    
    property set originalContact ($arg :  Contact) {
      setVariableValue("originalContact", 0, $arg)
    }
    
    property get potentialMatchChecker () : gw.pcf.duplicatecontacts.PotentialMatchChecker {
      return getVariableValue("potentialMatchChecker", 0) as gw.pcf.duplicatecontacts.PotentialMatchChecker
    }
    
    property set potentialMatchChecker ($arg :  gw.pcf.duplicatecontacts.PotentialMatchChecker) {
      setVariableValue("potentialMatchChecker", 0, $arg)
    }
    
    property get selectionWarningAlreadyIssued () : boolean {
      return getVariableValue("selectionWarningAlreadyIssued", 0) as java.lang.Boolean
    }
    
    property set selectionWarningAlreadyIssued ($arg :  boolean) {
      setVariableValue("selectionWarningAlreadyIssued", 0, $arg)
    }
    
    
    
    function importContactData(duplicate : DuplicateContactResult) {
      
      if (duplicateContactsPopupNavigator.selectedDuplicateContactMatchesContactThatAlreadyExistsOnAccountOrPolicy(duplicate)) {
        displayErrorInformingUserTheyAttemptedToAddContactThatWasAlreadyOnThePolicyOrAccount()
        return
      }
      
      if (not selectionWarningAlreadyIssued and potentialMatchChecker.userSelectedNonExactMatchEvenThoughExactMatchesWereAvailable(duplicate)) {
        warnUserTheySelectedNonExactMatchEvenThoughExactMatchWasAvailable()
        return
      }
      
      try {
        duplicateContactsPopupNavigator.copyDataFromDuplicateABContactToBCContact(duplicate)
      } catch (e : Throwable) {
        LocationUtil.addRequestScopedErrorMessage(e.Message)
      }
      
      CurrentLocation.commit() // have to do this to roll the change up to the parent page and close this popup
    }
    
    function displayWarningIfUserCanceledOutOfPageWithoutSelectingAnything() {
      if (originalContact.AddressBookUID == null) {
        LocationUtil.addRequestScopedWarningMessage(DisplayKey.get("Web.DuplicateContactsPopup.Warning.NoSelection"))
      }
    }
    
    function warnUserTheySelectedNonExactMatchEvenThoughExactMatchWasAvailable() {
      LocationUtil.addRequestScopedWarningMessage(DisplayKey.get("Web.DuplicateContactsPopup.Warning.ExactNotSelected"))
      selectionWarningAlreadyIssued = true
    }
    
    function displayErrorInformingUserTheyAttemptedToAddContactThatWasAlreadyOnThePolicyOrAccount() {
      LocationUtil.addRequestScopedWarningMessage(DisplayKey.get("Web.DuplicateContactsPopup.ErrorContactAlreadyExistsOnContactHolder", originalContact, duplicateContactsPopupNavigator.ContactHolderType))
    }
    
    function getInstructionsText() : String {
      if (duplicateContactsPopupNavigator.ContactHolderType == Account) {
        return DisplayKey.get("Web.DuplicateContactsPopup.InstructionsForAccountContact")
      } else if (duplicateContactsPopupNavigator.ContactHolderType == PolicyPeriod) {
        return DisplayKey.get("Web.DuplicateContactsPopup.InstructionsForPolicyPeriodContact")
      } else {
        throw DisplayKey.get("Web.DuplicateContactsPopup.ErrorUnsupportedContactHolderType")
      }
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/contact/DuplicateContactsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DuplicateContactsPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Select_Cell) at DuplicateContactsPopup.pcf: line 88, column 86
    function action_28 () : void {
      importContactData(duplicateResult)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at DuplicateContactsPopup.pcf: line 93, column 52
    function valueRoot_30 () : java.lang.Object {
      return duplicateResult
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at DuplicateContactsPopup.pcf: line 93, column 52
    function value_29 () : java.lang.String {
      return duplicateResult.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Exact_Cell) at DuplicateContactsPopup.pcf: line 99, column 50
    function value_32 () : java.lang.String {
      return duplicateResult.MatchType
    }
    
    // 'value' attribute on TextCell (id=Address_Cell) at DuplicateContactsPopup.pcf: line 103, column 55
    function value_35 () : java.lang.String {
      return duplicateResult.DisplayAddress
    }
    
    // 'value' attribute on TextCell (id=PrimaryPhone_Cell) at DuplicateContactsPopup.pcf: line 107, column 58
    function value_38 () : java.lang.String {
      return duplicateResult.PrimaryPhoneValue
    }
    
    // 'value' attribute on DateCell (id=DOB_Cell) at DuplicateContactsPopup.pcf: line 112, column 60
    function value_41 () : java.util.Date {
      return duplicateResult.DateOfBirth
    }
    
    // 'visible' attribute on DateCell (id=DOB_Cell) at DuplicateContactsPopup.pcf: line 112, column 60
    function visible_43 () : java.lang.Boolean {
      return typeof(originalContact) == Person
    }
    
    property get duplicateResult () : gw.plugin.contact.DuplicateContactResult {
      return getIteratedValue(1) as gw.plugin.contact.DuplicateContactResult
    }
    
    
  }
  
  
}