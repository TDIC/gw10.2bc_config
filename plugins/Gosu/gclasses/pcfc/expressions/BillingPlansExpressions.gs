package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/businesssettings/BillingPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BillingPlansExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/BillingPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BillingPlansExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=BillingPlans) at BillingPlans.pcf: line 8, column 64
    static function canVisit_31 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.billplanview
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at BillingPlans.pcf: line 28, column 80
    function filters_1 () : gw.api.filters.IFilter[] {
      return new gw.api.web.plan.PlanListFilterSet().FilterOptions
    }
    
    // 'initialValue' attribute on Variable at BillingPlans.pcf: line 12, column 42
    function initialValue_0 () : gw.api.web.plan.PlanHelper {
      return new gw.api.web.plan.PlanHelper()
    }
    
    // Page (id=BillingPlans) at BillingPlans.pcf: line 8, column 64
    static function parent_32 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at BillingPlans.pcf: line 37, column 46
    function sortValue_2 (billingPlan :  entity.BillingPlan) : java.lang.Object {
      return billingPlan.PlanOrder
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at BillingPlans.pcf: line 43, column 41
    function sortValue_3 (billingPlan :  entity.BillingPlan) : java.lang.Object {
      return billingPlan.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at BillingPlans.pcf: line 48, column 48
    function sortValue_4 (billingPlan :  entity.BillingPlan) : java.lang.Object {
      return billingPlan.Description
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at BillingPlans.pcf: line 52, column 50
    function sortValue_5 (billingPlan :  entity.BillingPlan) : java.lang.Object {
      return billingPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at BillingPlans.pcf: line 56, column 51
    function sortValue_6 (billingPlan :  entity.BillingPlan) : java.lang.Object {
      return billingPlan.ExpirationDate
    }
    
    // 'value' attribute on RowIterator at BillingPlans.pcf: line 23, column 82
    function value_30 () : gw.api.database.IQueryBeanResult<entity.BillingPlan> {
      return gw.api.database.Query.make(BillingPlan).select()
    }
    
    override property get CurrentLocation () : pcf.BillingPlans {
      return super.CurrentLocation as pcf.BillingPlans
    }
    
    property get PlanHelper () : gw.api.web.plan.PlanHelper {
      return getVariableValue("PlanHelper", 0) as gw.api.web.plan.PlanHelper
    }
    
    property set PlanHelper ($arg :  gw.api.web.plan.PlanHelper) {
      setVariableValue("PlanHelper", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/BillingPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends BillingPlansExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at BillingPlans.pcf: line 43, column 41
    function action_10 () : void {
      BillingPlanDetail.go(billingPlan)
    }
    
    // 'action' attribute on Link (id=MoveUp) at BillingPlans.pcf: line 67, column 57
    function action_26 () : void {
      PlanHelper.moveUp(billingPlan);
    }
    
    // 'action' attribute on Link (id=MoveDown) at BillingPlans.pcf: line 75, column 63
    function action_29 () : void {
      PlanHelper.moveDown(billingPlan);
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at BillingPlans.pcf: line 43, column 41
    function action_dest_11 () : pcf.api.Destination {
      return pcf.BillingPlanDetail.createDestination(billingPlan)
    }
    
    // 'available' attribute on Link (id=MoveUp) at BillingPlans.pcf: line 67, column 57
    function available_24 () : java.lang.Boolean {
      return perm.System.billplanedit
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at BillingPlans.pcf: line 37, column 46
    function valueRoot_8 () : java.lang.Object {
      return billingPlan
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at BillingPlans.pcf: line 43, column 41
    function value_12 () : java.lang.String {
      return billingPlan.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at BillingPlans.pcf: line 48, column 48
    function value_15 () : java.lang.String {
      return billingPlan.Description
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at BillingPlans.pcf: line 52, column 50
    function value_18 () : java.util.Date {
      return billingPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at BillingPlans.pcf: line 56, column 51
    function value_21 () : java.util.Date {
      return billingPlan.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at BillingPlans.pcf: line 37, column 46
    function value_7 () : java.lang.Integer {
      return billingPlan.PlanOrder
    }
    
    // 'visible' attribute on Link (id=MoveUp) at BillingPlans.pcf: line 67, column 57
    function visible_25 () : java.lang.Boolean {
      return billingPlan.PlanOrder > 1
    }
    
    // 'visible' attribute on Link (id=MoveDown) at BillingPlans.pcf: line 75, column 63
    function visible_28 () : java.lang.Boolean {
      return !billingPlan.hasHighestPlanOrder()
    }
    
    property get billingPlan () : entity.BillingPlan {
      return getIteratedValue(1) as entity.BillingPlan
    }
    
    
  }
  
  
}