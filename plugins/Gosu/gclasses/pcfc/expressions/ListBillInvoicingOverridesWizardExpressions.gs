package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/ListBillInvoicingOverridesWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ListBillInvoicingOverridesWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/ListBillInvoicingOverridesWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListBillInvoicingOverridesWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Wizard (id=ListBillInvoicingOverridesWizard) at ListBillInvoicingOverridesWizard.pcf: line 7, column 43
    function beforeCommit_5 (pickedValue :  java.lang.Object) : void {
      listBillHelper.update()
    }
    
    // 'initialValue' attribute on Variable at ListBillInvoicingOverridesWizard.pcf: line 16, column 66
    function initialValue_0 () : gw.api.web.policy.ListBillInvoicingOverridesHelper {
      return new gw.api.web.policy.ListBillInvoicingOverridesHelper(policyPeriod)
    }
    
    // 'screen' attribute on WizardStep (id=ListBillChooseInvoicingOverridesStep) at ListBillInvoicingOverridesWizard.pcf: line 21, column 103
    function screen_onEnter_1 (def :  pcf.ListBillChooseInvoicingOverridesStepScreen) : void {
      def.onEnter(policyPeriod, listBillHelper)
    }
    
    // 'screen' attribute on WizardStep (id=ListBillInvoicingOverridesInvoiceItemsStep) at ListBillInvoicingOverridesWizard.pcf: line 26, column 106
    function screen_onEnter_3 (def :  pcf.ListBillInvoicingOverridesInvoiceItemsStepScreen) : void {
      def.onEnter(policyPeriod, listBillHelper)
    }
    
    // 'screen' attribute on WizardStep (id=ListBillChooseInvoicingOverridesStep) at ListBillInvoicingOverridesWizard.pcf: line 21, column 103
    function screen_refreshVariables_2 (def :  pcf.ListBillChooseInvoicingOverridesStepScreen) : void {
      def.refreshVariables(policyPeriod, listBillHelper)
    }
    
    // 'screen' attribute on WizardStep (id=ListBillInvoicingOverridesInvoiceItemsStep) at ListBillInvoicingOverridesWizard.pcf: line 26, column 106
    function screen_refreshVariables_4 (def :  pcf.ListBillInvoicingOverridesInvoiceItemsStepScreen) : void {
      def.refreshVariables(policyPeriod, listBillHelper)
    }
    
    // 'tabBar' attribute on Wizard (id=ListBillInvoicingOverridesWizard) at ListBillInvoicingOverridesWizard.pcf: line 7, column 43
    function tabBar_onEnter_6 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=ListBillInvoicingOverridesWizard) at ListBillInvoicingOverridesWizard.pcf: line 7, column 43
    function tabBar_refreshVariables_7 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.ListBillInvoicingOverridesWizard {
      return super.CurrentLocation as pcf.ListBillInvoicingOverridesWizard
    }
    
    property get listBillHelper () : gw.api.web.policy.ListBillInvoicingOverridesHelper {
      return getVariableValue("listBillHelper", 0) as gw.api.web.policy.ListBillInvoicingOverridesHelper
    }
    
    property set listBillHelper ($arg :  gw.api.web.policy.ListBillInvoicingOverridesHelper) {
      setVariableValue("listBillHelper", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}