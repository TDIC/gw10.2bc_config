package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerDetailHistory.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerDetailHistoryExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerDetailHistory.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ProducerDetailHistoryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Transaction_Cell) at ProducerDetailHistory.pcf: line 64, column 70
    function action_17 () : void {
      TransactionDetailPopup.push(producerHistory.Transaction)
    }
    
    // 'action' attribute on TextCell (id=User_Cell) at ProducerDetailHistory.pcf: line 89, column 40
    function action_32 () : void {
      UserDetailPage.push(producerHistory.User)
    }
    
    // 'action' attribute on TextCell (id=Transaction_Cell) at ProducerDetailHistory.pcf: line 64, column 70
    function action_dest_18 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(producerHistory.Transaction)
    }
    
    // 'action' attribute on TextCell (id=User_Cell) at ProducerDetailHistory.pcf: line 89, column 40
    function action_dest_33 () : pcf.api.Destination {
      return pcf.UserDetailPage.createDestination(producerHistory.User)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=TransactionAmount_Cell) at ProducerDetailHistory.pcf: line 83, column 47
    function currency_30 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at ProducerDetailHistory.pcf: line 58, column 50
    function valueRoot_15 () : java.lang.Object {
      return producerHistory
    }
    
    // 'value' attribute on TextCell (id=Transaction_Cell) at ProducerDetailHistory.pcf: line 64, column 70
    function valueRoot_20 () : java.lang.Object {
      return producerHistory.Transaction
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at ProducerDetailHistory.pcf: line 58, column 50
    function value_14 () : java.util.Date {
      return producerHistory.EventDate
    }
    
    // 'value' attribute on TextCell (id=Transaction_Cell) at ProducerDetailHistory.pcf: line 64, column 70
    function value_19 () : java.lang.String {
      return producerHistory.Transaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at ProducerDetailHistory.pcf: line 70, column 52
    function value_22 () : java.lang.String {
      return producerHistory.Description
    }
    
    // 'value' attribute on TextCell (id=RefNumber_Cell) at ProducerDetailHistory.pcf: line 75, column 50
    function value_25 () : java.lang.String {
      return producerHistory.RefNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TransactionAmount_Cell) at ProducerDetailHistory.pcf: line 83, column 47
    function value_28 () : gw.pl.currency.MonetaryAmount {
      return producerHistory.Amount
    }
    
    // 'value' attribute on TextCell (id=User_Cell) at ProducerDetailHistory.pcf: line 89, column 40
    function value_34 () : entity.User {
      return producerHistory.User
    }
    
    property get producerHistory () : entity.ProducerHistory {
      return getIteratedValue(1) as entity.ProducerHistory
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerDetailHistory.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerDetailHistoryExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=ProducerDetailHistory) at ProducerDetailHistory.pcf: line 9, column 73
    static function canVisit_38 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodhistview
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerDetailHistory.pcf: line 30, column 86
    function filter_0 () : gw.api.filters.IFilter {
      return new gw.api.web.history.ProducerHistoriesFilters.Last30Days()
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerDetailHistory.pcf: line 32, column 86
    function filter_1 () : gw.api.filters.IFilter {
      return new gw.api.web.history.ProducerHistoriesFilters.Last60Days()
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerDetailHistory.pcf: line 34, column 86
    function filter_2 () : gw.api.filters.IFilter {
      return new gw.api.web.history.ProducerHistoriesFilters.Last90Days()
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerDetailHistory.pcf: line 36, column 87
    function filter_3 () : gw.api.filters.IFilter {
      return new gw.api.web.history.ProducerHistoriesFilters.Last120Days()
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerDetailHistory.pcf: line 39, column 37
    function filter_4 () : gw.api.filters.IFilter {
      return new gw.api.web.history.ProducerHistoriesFilters.Last180Days()
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerDetailHistory.pcf: line 41, column 84
    function filter_5 () : gw.api.filters.IFilter {
      return new gw.api.web.history.ProducerHistoriesFilters.LastYear()
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerDetailHistory.pcf: line 43, column 86
    function filter_6 () : gw.api.filters.IFilter {
      return new gw.api.web.history.ProducerHistoriesFilters.Last3Years()
    }
    
    // 'filter' attribute on ToolbarFilterOption at ProducerDetailHistory.pcf: line 45, column 79
    function filter_7 () : gw.api.filters.IFilter {
      return new gw.api.web.history.ProducerHistoriesFilters.All()
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at ProducerDetailHistory.pcf: line 51, column 143
    function filters_8 () : gw.api.filters.IFilter[] {
      return new gw.api.filters.TypeKeyFilterSet( History.Type.TypeInfo.getProperty( "EventType" ) ).getFilterOptions()
    }
    
    // Page (id=ProducerDetailHistory) at ProducerDetailHistory.pcf: line 9, column 73
    static function parent_39 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    // 'value' attribute on TextCell (id=Transaction_Cell) at ProducerDetailHistory.pcf: line 64, column 70
    function sortValue_10 (producerHistory :  entity.ProducerHistory) : java.lang.Object {
      return producerHistory.Transaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at ProducerDetailHistory.pcf: line 70, column 52
    function sortValue_11 (producerHistory :  entity.ProducerHistory) : java.lang.Object {
      return producerHistory.Description
    }
    
    // 'value' attribute on TextCell (id=RefNumber_Cell) at ProducerDetailHistory.pcf: line 75, column 50
    function sortValue_12 (producerHistory :  entity.ProducerHistory) : java.lang.Object {
      return producerHistory.RefNumber
    }
    
    // 'value' attribute on TextCell (id=User_Cell) at ProducerDetailHistory.pcf: line 89, column 40
    function sortValue_13 (producerHistory :  entity.ProducerHistory) : java.lang.Object {
      return producerHistory.User
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at ProducerDetailHistory.pcf: line 58, column 50
    function sortValue_9 (producerHistory :  entity.ProducerHistory) : java.lang.Object {
      return producerHistory.EventDate
    }
    
    // 'value' attribute on RowIterator at ProducerDetailHistory.pcf: line 25, column 48
    function value_37 () : entity.ProducerHistory[] {
      return producer.History
    }
    
    override property get CurrentLocation () : pcf.ProducerDetailHistory {
      return super.CurrentLocation as pcf.ProducerDetailHistory
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}