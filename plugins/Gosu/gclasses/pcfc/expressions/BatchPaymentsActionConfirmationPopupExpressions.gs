package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/BatchPaymentsActionConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BatchPaymentsActionConfirmationPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/BatchPaymentsActionConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BatchPaymentsActionConfirmationPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (selectedBatches :  BatchPayment[], action :  gw.web.payment.batch.BatchPaymentPopupActions) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=ConfirmAction) at BatchPaymentsActionConfirmationPopup.pcf: line 23, column 58
    function action_0 () : void {
      action.ToConfirmFor(selectedBatches); CurrentLocation.commit()
    }
    
    // 'action' attribute on ToolbarButton (id=RejectAction) at BatchPaymentsActionConfirmationPopup.pcf: line 27, column 62
    function action_1 () : void {
      CurrentLocation.cancel()
    }
    
    // 'label' attribute on AlertBar (id=Warning) at BatchPaymentsActionConfirmationPopup.pcf: line 33, column 93
    function label_3 () : java.lang.Object {
      return action.WarningFor(selectedBatches)
    }
    
    // 'value' attribute on TextCell (id=BatchNumber_Cell) at BatchPaymentsActionConfirmationPopup.pcf: line 51, column 33
    function sortValue_4 (batchPayment :  BatchPayment) : java.lang.Object {
      return batchPayment.BatchNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=BatchStatus_Cell) at BatchPaymentsActionConfirmationPopup.pcf: line 57, column 46
    function sortValue_5 (batchPayment :  BatchPayment) : java.lang.Object {
      return batchPayment.BatchStatus
    }
    
    // 'value' attribute on MonetaryAmountCell (id=BatchAmount_Cell) at BatchPaymentsActionConfirmationPopup.pcf: line 62, column 42
    function sortValue_6 (batchPayment :  BatchPayment) : java.lang.Object {
      return batchPayment.Amount
    }
    
    // 'value' attribute on TextCell (id=BatchPaymentCount_Cell) at BatchPaymentsActionConfirmationPopup.pcf: line 68, column 34
    function sortValue_7 (batchPayment :  BatchPayment) : java.lang.Object {
      return batchPayment.Payments.Count
    }
    
    // 'value' attribute on TextCell (id=CreatedBy_Cell) at BatchPaymentsActionConfirmationPopup.pcf: line 74, column 31
    function sortValue_8 (batchPayment :  BatchPayment) : java.lang.Object {
      return batchPayment.CreateUser
    }
    
    // 'value' attribute on TextCell (id=LastEditedBy_Cell) at BatchPaymentsActionConfirmationPopup.pcf: line 80, column 31
    function sortValue_9 (batchPayment :  BatchPayment) : java.lang.Object {
      return batchPayment.UpdateUser
    }
    
    // 'title' attribute on Popup (id=BatchPaymentsActionConfirmationPopup) at BatchPaymentsActionConfirmationPopup.pcf: line 9, column 31
    static function title_30 (action :  gw.web.payment.batch.BatchPaymentPopupActions, selectedBatches :  BatchPayment[]) : java.lang.Object {
      return action.TitleLabel
    }
    
    // 'value' attribute on RowIterator at BatchPaymentsActionConfirmationPopup.pcf: line 43, column 36
    function value_29 () : BatchPayment[] {
      return selectedBatches
    }
    
    // 'visible' attribute on AlertBar (id=Warning) at BatchPaymentsActionConfirmationPopup.pcf: line 33, column 93
    function visible_2 () : java.lang.Boolean {
      return action.WarningFor != null and action.WarningFor(selectedBatches) != null
    }
    
    override property get CurrentLocation () : pcf.BatchPaymentsActionConfirmationPopup {
      return super.CurrentLocation as pcf.BatchPaymentsActionConfirmationPopup
    }
    
    property get action () : gw.web.payment.batch.BatchPaymentPopupActions {
      return getVariableValue("action", 0) as gw.web.payment.batch.BatchPaymentPopupActions
    }
    
    property set action ($arg :  gw.web.payment.batch.BatchPaymentPopupActions) {
      setVariableValue("action", 0, $arg)
    }
    
    property get selectedBatches () : BatchPayment[] {
      return getVariableValue("selectedBatches", 0) as BatchPayment[]
    }
    
    property set selectedBatches ($arg :  BatchPayment[]) {
      setVariableValue("selectedBatches", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/payment/BatchPaymentsActionConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends BatchPaymentsActionConfirmationPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=BatchNumber_Cell) at BatchPaymentsActionConfirmationPopup.pcf: line 51, column 33
    function valueRoot_11 () : java.lang.Object {
      return batchPayment
    }
    
    // 'value' attribute on TextCell (id=BatchPaymentCount_Cell) at BatchPaymentsActionConfirmationPopup.pcf: line 68, column 34
    function valueRoot_21 () : java.lang.Object {
      return batchPayment.Payments
    }
    
    // 'value' attribute on TextCell (id=BatchNumber_Cell) at BatchPaymentsActionConfirmationPopup.pcf: line 51, column 33
    function value_10 () : String {
      return batchPayment.BatchNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=BatchStatus_Cell) at BatchPaymentsActionConfirmationPopup.pcf: line 57, column 46
    function value_13 () : BatchPaymentsStatus {
      return batchPayment.BatchStatus
    }
    
    // 'value' attribute on MonetaryAmountCell (id=BatchAmount_Cell) at BatchPaymentsActionConfirmationPopup.pcf: line 62, column 42
    function value_17 () : gw.pl.currency.MonetaryAmount {
      return batchPayment.Amount
    }
    
    // 'value' attribute on TextCell (id=BatchPaymentCount_Cell) at BatchPaymentsActionConfirmationPopup.pcf: line 68, column 34
    function value_20 () : Integer {
      return batchPayment.Payments.Count
    }
    
    // 'value' attribute on TextCell (id=CreatedBy_Cell) at BatchPaymentsActionConfirmationPopup.pcf: line 74, column 31
    function value_23 () : User {
      return batchPayment.CreateUser
    }
    
    // 'value' attribute on TextCell (id=LastEditedBy_Cell) at BatchPaymentsActionConfirmationPopup.pcf: line 80, column 31
    function value_26 () : User {
      return batchPayment.UpdateUser
    }
    
    // 'valueType' attribute on TypeKeyCell (id=BatchStatus_Cell) at BatchPaymentsActionConfirmationPopup.pcf: line 57, column 46
    function verifyValueType_16 () : void {
      var __valueTypeArg : BatchPaymentsStatus
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : gw.entity.TypeKey = __valueTypeArg
    }
    
    property get batchPayment () : BatchPayment {
      return getIteratedValue(1) as BatchPayment
    }
    
    
  }
  
  
}