package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/ChargeDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChargeDetailsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/ChargeDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChargeDetailsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at ChargeDetailsScreen.pcf: line 46, column 52
    function currency_20 () : typekey.Currency {
      return helper.ChargeInitializer.Amount.Currency
    }
    
    // 'value' attribute on TextInput (id=claimID_Input) at ChargeDetailsScreen.pcf: line 39, column 98
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      helper.ClaimID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at ChargeDetailsScreen.pcf: line 46, column 52
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      helper.ChargeInitializer.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on RangeInput (id=Type_Input) at ChargeDetailsScreen.pcf: line 30, column 44
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      helper.ChargeInitializer.ChargePattern = (__VALUE_TO_SET as entity.ChargePattern)
    }
    
    // 'initialValue' attribute on Variable at ChargeDetailsScreen.pcf: line 13, column 23
    function initialValue_0 () : Boolean {
      return helper.ChargeInitializer.ChargePattern.Category == ChargeCategory.TC_GENERAL
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=Amount_Input) at ChargeDetailsScreen.pcf: line 46, column 52
    function validationExpression_16 () : java.lang.Object {
      return helper.ChargeInitializer.Amount.IsZero ? DisplayKey.get("Web.Charge.ChargeAmountCannotBeZero") : null
    }
    
    // 'valueRange' attribute on RangeInput (id=Type_Input) at ChargeDetailsScreen.pcf: line 30, column 44
    function valueRange_6 () : java.lang.Object {
      return helper.ChargePatternValues
    }
    
    // 'value' attribute on TextInput (id=claimID_Input) at ChargeDetailsScreen.pcf: line 39, column 98
    function valueRoot_13 () : java.lang.Object {
      return helper
    }
    
    // 'value' attribute on RangeInput (id=Type_Input) at ChargeDetailsScreen.pcf: line 30, column 44
    function valueRoot_5 () : java.lang.Object {
      return helper.ChargeInitializer
    }
    
    // 'value' attribute on TextInput (id=Target_Input) at ChargeDetailsScreen.pcf: line 22, column 44
    function value_1 () : java.lang.String {
      return helper.getDisplayName()
    }
    
    // 'value' attribute on TextInput (id=claimID_Input) at ChargeDetailsScreen.pcf: line 39, column 98
    function value_11 () : java.lang.String {
      return helper.ClaimID
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at ChargeDetailsScreen.pcf: line 46, column 52
    function value_17 () : gw.pl.currency.MonetaryAmount {
      return helper.ChargeInitializer.Amount
    }
    
    // 'value' attribute on RangeInput (id=Type_Input) at ChargeDetailsScreen.pcf: line 30, column 44
    function value_3 () : entity.ChargePattern {
      return helper.ChargeInitializer.ChargePattern
    }
    
    // 'valueRange' attribute on RangeInput (id=Type_Input) at ChargeDetailsScreen.pcf: line 30, column 44
    function verifyValueRangeIsAllowedType_7 ($$arg :  entity.ChargePattern[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Type_Input) at ChargeDetailsScreen.pcf: line 30, column 44
    function verifyValueRangeIsAllowedType_7 ($$arg :  gw.api.database.IQueryBeanResult<entity.ChargePattern>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Type_Input) at ChargeDetailsScreen.pcf: line 30, column 44
    function verifyValueRangeIsAllowedType_7 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Type_Input) at ChargeDetailsScreen.pcf: line 30, column 44
    function verifyValueRange_8 () : void {
      var __valueRangeArg = helper.ChargePatternValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_7(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=claimID_Input) at ChargeDetailsScreen.pcf: line 39, column 98
    function visible_10 () : java.lang.Boolean {
      return helper.ChargeInitializer.ChargePattern.ChargeCode == "Deductible"
    }
    
    property get helper () : gw.accounting.NewGeneralSingleChargeHelper {
      return getRequireValue("helper", 0) as gw.accounting.NewGeneralSingleChargeHelper
    }
    
    property set helper ($arg :  gw.accounting.NewGeneralSingleChargeHelper) {
      setRequireValue("helper", 0, $arg)
    }
    
    property get isGeneral () : Boolean {
      return getVariableValue("isGeneral", 0) as Boolean
    }
    
    property set isGeneral ($arg :  Boolean) {
      setVariableValue("isGeneral", 0, $arg)
    }
    
    
  }
  
  
}