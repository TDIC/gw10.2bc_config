package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopDelinquencies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopDelinquenciesExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopDelinquencies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopDelinquenciesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=DesktopDelinquencies) at DesktopDelinquencies.pcf: line 9, column 66
    static function canVisit_58 () : java.lang.Boolean {
      return perm.System.mydelinquenciesview and perm.System.viewdesktop
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopDelinquencies.pcf: line 45, column 97
    function filter_2 () : gw.api.filters.IFilter {
      return new gw.api.web.delinquency.DelinquencyProcessFilterSet.OpenedThisWeek()
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopDelinquencies.pcf: line 48, column 37
    function filter_3 () : gw.api.filters.IFilter {
      return new gw.api.web.delinquency.DelinquencyProcessFilterSet.AllOpen()
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopDelinquencies.pcf: line 50, column 181
    function filter_4 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("All open owned", \ qf -> {qf.compare(DelinquencyProcess#AssignedUser, Equals, User.util.CurrentUser)})
    }
    
    // 'initialValue' attribute on Variable at DesktopDelinquencies.pcf: line 15, column 30
    function initialValue_0 () : java.util.Date {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'initialValue' attribute on Variable at DesktopDelinquencies.pcf: line 21, column 74
    function initialValue_1 () : gw.api.database.IQueryBeanResult<DelinquencyProcess> {
      return gw.api.database.Query.make(DelinquencyProcess).select()
    }
    
    // Page (id=DesktopDelinquencies) at DesktopDelinquencies.pcf: line 9, column 66
    static function parent_59 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'value' attribute on DateCell (id=delinquencyDate_Cell) at DesktopDelinquencies.pcf: line 59, column 53
    function sortValue_5 (delinquencyProcess :  entity.DelinquencyProcess) : java.lang.Object {
      return delinquencyProcess.StartDate
    }
    
    // 'value' attribute on TextCell (id=assignedUser_Cell) at DesktopDelinquencies.pcf: line 64, column 40
    function sortValue_6 (delinquencyProcess :  entity.DelinquencyProcess) : java.lang.Object {
      return delinquencyProcess.AssignedUser
    }
    
    // 'value' attribute on RowIterator at DesktopDelinquencies.pcf: line 37, column 89
    function value_57 () : gw.api.database.IQueryBeanResult<entity.DelinquencyProcess> {
      return delinquencyProcesses
    }
    
    override property get CurrentLocation () : pcf.DesktopDelinquencies {
      return super.CurrentLocation as pcf.DesktopDelinquencies
    }
    
    property get delinquencyProcesses () : gw.api.database.IQueryBeanResult<DelinquencyProcess> {
      return getVariableValue("delinquencyProcesses", 0) as gw.api.database.IQueryBeanResult<DelinquencyProcess>
    }
    
    property set delinquencyProcesses ($arg :  gw.api.database.IQueryBeanResult<DelinquencyProcess>) {
      setVariableValue("delinquencyProcesses", 0, $arg)
    }
    
    property get today () : java.util.Date {
      return getVariableValue("today", 0) as java.util.Date
    }
    
    property set today ($arg :  java.util.Date) {
      setVariableValue("today", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopDelinquencies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DesktopDelinquenciesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=account_Cell) at DesktopDelinquencies.pcf: line 72, column 29
    function action_16 () : void {
      AccountSummary.push(delinquencyProcess.Account)
    }
    
    // 'action' attribute on TextCell (id=DelinquencyTarget_Cell) at DesktopDelinquencies.pcf: line 78, column 68
    function action_21 () : void {
      DelinquencyTargetDetailsForward.go(delinquencyProcess.Target)
    }
    
    // 'action' attribute on DateCell (id=delinquencyDate_Cell) at DesktopDelinquencies.pcf: line 59, column 53
    function action_8 () : void {
      AccountDetailDelinquencies.push(delinquencyProcess.Account)
    }
    
    // 'action' attribute on TextCell (id=account_Cell) at DesktopDelinquencies.pcf: line 72, column 29
    function action_dest_17 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination(delinquencyProcess.Account)
    }
    
    // 'action' attribute on TextCell (id=DelinquencyTarget_Cell) at DesktopDelinquencies.pcf: line 78, column 68
    function action_dest_22 () : pcf.api.Destination {
      return pcf.DelinquencyTargetDetailsForward.createDestination(delinquencyProcess.Target)
    }
    
    // 'action' attribute on DateCell (id=delinquencyDate_Cell) at DesktopDelinquencies.pcf: line 59, column 53
    function action_dest_9 () : pcf.api.Destination {
      return pcf.AccountDetailDelinquencies.createDestination(delinquencyProcess.Account)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=totalUnbilled_Cell) at DesktopDelinquencies.pcf: line 119, column 56
    function currency_42 () : typekey.Currency {
      return delinquencyProcess.Currency
    }
    
    // 'initialValue' attribute on Variable at DesktopDelinquencies.pcf: line 41, column 67
    function initialValue_7 () : gw.web.account.AccountSummaryFinancialsHelper {
      return new gw.web.account.AccountSummaryFinancialsHelper(delinquencyProcess.Account)
    }
    
    // RowIterator at DesktopDelinquencies.pcf: line 37, column 89
    function initializeVariables_56 () : void {
        financialsHelper = new gw.web.account.AccountSummaryFinancialsHelper(delinquencyProcess.Account);

    }
    
    // 'outputConversion' attribute on TextCell (id=age_Cell) at DesktopDelinquencies.pcf: line 104, column 29
    function outputConversion_33 (VALUE :  java.math.BigDecimal) : java.lang.String {
      return DisplayKey.get("Web.DesktopDelinquencies.AgeInDays", VALUE)
    }
    
    // 'value' attribute on DateCell (id=delinquencyDate_Cell) at DesktopDelinquencies.pcf: line 59, column 53
    function valueRoot_11 () : java.lang.Object {
      return delinquencyProcess
    }
    
    // 'value' attribute on TextCell (id=account_Cell) at DesktopDelinquencies.pcf: line 72, column 29
    function valueRoot_19 () : java.lang.Object {
      return delinquencyProcess.Account
    }
    
    // 'value' attribute on TextCell (id=DelinquencyTarget_Cell) at DesktopDelinquencies.pcf: line 78, column 68
    function valueRoot_24 () : java.lang.Object {
      return delinquencyProcess.Target
    }
    
    // 'value' attribute on MonetaryAmountCell (id=totalUnbilled_Cell) at DesktopDelinquencies.pcf: line 119, column 56
    function valueRoot_41 () : java.lang.Object {
      return financialsHelper
    }
    
    // 'value' attribute on DateCell (id=delinquencyDate_Cell) at DesktopDelinquencies.pcf: line 59, column 53
    function value_10 () : java.util.Date {
      return delinquencyProcess.StartDate
    }
    
    // 'value' attribute on TextCell (id=assignedUser_Cell) at DesktopDelinquencies.pcf: line 64, column 40
    function value_13 () : entity.User {
      return delinquencyProcess.AssignedUser
    }
    
    // 'value' attribute on TextCell (id=account_Cell) at DesktopDelinquencies.pcf: line 72, column 29
    function value_18 () : java.lang.String {
      return delinquencyProcess.Account.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=DelinquencyTarget_Cell) at DesktopDelinquencies.pcf: line 78, column 68
    function value_23 () : java.lang.String {
      return delinquencyProcess.Target.TargetDisplayName
    }
    
    // 'value' attribute on TextCell (id=PolicyState_Cell) at DesktopDelinquencies.pcf: line 84, column 49
    function value_26 () : typekey.Jurisdiction {
      return delinquencyProcess.Target typeis PolicyPeriod ? delinquencyProcess.Target.RiskJurisdiction : null
    }
    
    // 'value' attribute on TextCell (id=ProductLOB_Cell) at DesktopDelinquencies.pcf: line 90, column 44
    function value_28 () : typekey.LOBCode {
      return delinquencyProcess.Target typeis PolicyPeriod ? delinquencyProcess.Target.Policy.LOBCode : null
    }
    
    // 'value' attribute on TextCell (id=insured_Cell) at DesktopDelinquencies.pcf: line 96, column 72
    function value_30 () : java.lang.String {
      return delinquencyProcess.Account.AccountNameLocalized
    }
    
    // 'value' attribute on TextCell (id=age_Cell) at DesktopDelinquencies.pcf: line 104, column 29
    function value_34 () : java.math.BigDecimal {
      return delinquencyProcess.Age
    }
    
    // 'value' attribute on TextCell (id=currentEvent_Cell) at DesktopDelinquencies.pcf: line 111, column 29
    function value_37 () : entity.DelinquencyProcessEvent {
      return delinquencyProcess.PreviousEvent
    }
    
    // 'value' attribute on MonetaryAmountCell (id=totalUnbilled_Cell) at DesktopDelinquencies.pcf: line 119, column 56
    function value_40 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.UnbilledAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=billedCurrent_Cell) at DesktopDelinquencies.pcf: line 127, column 54
    function value_44 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.BilledAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=pastDue_Cell) at DesktopDelinquencies.pcf: line 135, column 58
    function value_48 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.DelinquentAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=total_Cell) at DesktopDelinquencies.pcf: line 143, column 52
    function value_52 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.TotalValue
    }
    
    property get delinquencyProcess () : entity.DelinquencyProcess {
      return getIteratedValue(1) as entity.DelinquencyProcess
    }
    
    property get financialsHelper () : gw.web.account.AccountSummaryFinancialsHelper {
      return getVariableValue("financialsHelper", 1) as gw.web.account.AccountSummaryFinancialsHelper
    }
    
    property set financialsHelper ($arg :  gw.web.account.AccountSummaryFinancialsHelper) {
      setVariableValue("financialsHelper", 1, $arg)
    }
    
    
  }
  
  
}