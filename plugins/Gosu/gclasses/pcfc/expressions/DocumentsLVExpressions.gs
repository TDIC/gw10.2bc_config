package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/DocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DocumentsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/document/DocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DocumentsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at DocumentsLV.pcf: line 24, column 52
    function initialValue_0 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // 'sortBy' attribute on LinkCell (id=Name) at DocumentsLV.pcf: line 57, column 25
    function sortValue_1 (document :  entity.Document) : java.lang.Object {
      return document.Name
    }
    
    // 'sortBy' attribute on LinkCell (id=HiddenDocument) at DocumentsLV.pcf: line 174, column 61
    function sortValue_10 (document :  entity.Document) : java.lang.Object {
      return document.Obsolete
    }
    
    // 'value' attribute on TextCell (id=Event_Cell) at DocumentsLV.pcf: line 130, column 40
    function sortValue_3 (document :  entity.Document) : java.lang.Object {
      return document.Event_TDIC
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at DocumentsLV.pcf: line 137, column 25
    function sortValue_4 (document :  entity.Document) : java.lang.Object {
      return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=Subtype_Cell) at DocumentsLV.pcf: line 142, column 58
    function sortValue_5 (document :  entity.Document) : java.lang.Object {
      return document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DocumentsLV.pcf: line 148, column 25
    function sortValue_6 (document :  entity.Document) : java.lang.Object {
      return document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at DocumentsLV.pcf: line 154, column 36
    function sortValue_7 (document :  entity.Document) : java.lang.Object {
      return document.Author
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DocumentsLV.pcf: line 158, column 41
    function sortValue_8 (document :  entity.Document) : java.lang.Object {
      return document.Description
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at DocumentsLV.pcf: line 167, column 25
    function sortValue_9 (document :  entity.Document) : java.lang.Object {
      return document.DateModified
    }
    
    // 'value' attribute on RowIterator at DocumentsLV.pcf: line 31, column 75
    function value_71 () : gw.api.database.IQueryBeanResult<entity.Document> {
      return Documents
    }
    
    // 'visible' attribute on LinkCell (id=HiddenDocument) at DocumentsLV.pcf: line 174, column 61
    function visible_11 () : java.lang.Boolean {
      return documentSearchCriteria.IncludeObsoletes
    }
    
    // 'visible' attribute on LinkCell (id=Actions) at DocumentsLV.pcf: line 79, column 42
    function visible_2 () : java.lang.Boolean {
      return activityReadonlyFlag
    }
    
    property get Documents () : gw.api.database.IQueryBeanResult<Document> {
      return getRequireValue("Documents", 0) as gw.api.database.IQueryBeanResult<Document>
    }
    
    property set Documents ($arg :  gw.api.database.IQueryBeanResult<Document>) {
      setRequireValue("Documents", 0, $arg)
    }
    
    property get activityReadonlyFlag () : boolean {
      return getRequireValue("activityReadonlyFlag", 0) as java.lang.Boolean
    }
    
    property set activityReadonlyFlag ($arg :  boolean) {
      setRequireValue("activityReadonlyFlag", 0, $arg)
    }
    
    property get documentSearchCriteria () : DocumentSearchCriteria {
      return getRequireValue("documentSearchCriteria", 0) as DocumentSearchCriteria
    }
    
    property set documentSearchCriteria ($arg :  DocumentSearchCriteria) {
      setRequireValue("documentSearchCriteria", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/document/DocumentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DocumentsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=UnityLink) at DocumentsLV.pcf: line 65, column 147
    function action_17 () : void {
      document.viewOnBaseDocument()
    }
    
    // 'action' attribute on Link (id=NameLink) at DocumentsLV.pcf: line 74, column 145
    function action_22 () : void {
      document.downloadContent()
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at DocumentsLV.pcf: line 87, column 29
    function action_26 () : void {
      DocumentDetailsPopup.push(document)
    }
    
    // 'action' attribute on Link (id=DownloadLink) at DocumentsLV.pcf: line 96, column 90
    function action_31 () : void {
      document.downloadFileToBrowser()
    }
    
    // 'action' attribute on Link (id=UploadLink) at DocumentsLV.pcf: line 104, column 88
    function action_35 () : void {
      UploadDocumentContentPopup.push(document)
    }
    
    // 'action' attribute on Link (id=DeleteLink) at DocumentsLV.pcf: line 113, column 85
    function action_40 () : void {
      gw.api.web.document.DocumentsHelper.deleteDocument(document)
    }
    
    // 'action' attribute on Link (id=DocumentsLV_FinalizeLink) at DocumentsLV.pcf: line 125, column 124
    function action_46 () : void {
      new tdic.bc.integ.plugins.hpexstream.util.TDIC_BCExstreamHelper ().finalizeLiveDocument(document)
    }
    
    // 'action' attribute on Link (id=ViewPropertiesLink) at DocumentsLV.pcf: line 87, column 29
    function action_dest_27 () : pcf.api.Destination {
      return pcf.DocumentDetailsPopup.createDestination(document)
    }
    
    // 'action' attribute on Link (id=UploadLink) at DocumentsLV.pcf: line 104, column 88
    function action_dest_36 () : pcf.api.Destination {
      return pcf.UploadDocumentContentPopup.createDestination(document)
    }
    
    // 'available' attribute on Link (id=UnityLink) at DocumentsLV.pcf: line 65, column 147
    function available_15 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document) 
    }
    
    // 'available' attribute on Link (id=NameLink) at DocumentsLV.pcf: line 74, column 145
    function available_20 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document) and activityReadonlyFlag
    }
    
    // 'available' attribute on Link (id=ViewPropertiesLink) at DocumentsLV.pcf: line 87, column 29
    function available_25 () : java.lang.Boolean {
      return documentsActionsHelper.DocumentMetadataActionsAvailable
    }
    
    // 'available' attribute on Link (id=DownloadLink) at DocumentsLV.pcf: line 96, column 90
    function available_29 () : java.lang.Boolean {
      return documentsActionsHelper.isDownloadDocumentContentAvailable(document)
    }
    
    // 'available' attribute on Link (id=UploadLink) at DocumentsLV.pcf: line 104, column 88
    function available_33 () : java.lang.Boolean {
      return documentsActionsHelper.isUploadDocumentContentAvailable(document)
    }
    
    // 'available' attribute on Link (id=DeleteLink) at DocumentsLV.pcf: line 113, column 85
    function available_38 () : java.lang.Boolean {
      return documentsActionsHelper.isDeleteDocumentLinkAvailable(document)
    }
    
    // 'condition' attribute on ToolbarFlag at DocumentsLV.pcf: line 39, column 24
    function condition_12 () : java.lang.Boolean {
      return document.Obsolete
    }
    
    // 'condition' attribute on ToolbarFlag at DocumentsLV.pcf: line 42, column 35
    function condition_13 () : java.lang.Boolean {
      return documentsActionsHelper.isDeleteDocumentLinkVisible(document) and documentsActionsHelper.isDeleteDocumentLinkAvailable(document)
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at DocumentsLV.pcf: line 50, column 32
    function icon_14 () : java.lang.String {
      return document.Icon
    }
    
    // 'icon' attribute on Link (id=DeleteLink) at DocumentsLV.pcf: line 113, column 85
    function icon_42 () : java.lang.String {
      return "delete" 
    }
    
    // 'label' attribute on Link (id=UnityLink) at DocumentsLV.pcf: line 65, column 147
    function label_18 () : java.lang.Object {
      return document.Name
    }
    
    // 'label' attribute on Link (id=ActionsDisabled) at DocumentsLV.pcf: line 118, column 75
    function label_44 () : java.lang.Object {
      return documentsActionsHelper.AsynchronousActionsMessage
    }
    
    // 'tooltip' attribute on Link (id=UnityLink) at DocumentsLV.pcf: line 65, column 147
    function tooltip_19 () : java.lang.String {
      return documentsActionsHelper.getViewDocumentContentTooltip(document)
    }
    
    // 'tooltip' attribute on Link (id=ViewPropertiesLink) at DocumentsLV.pcf: line 87, column 29
    function tooltip_28 () : java.lang.String {
      return documentsActionsHelper.ViewDocumentPropertiesTooltip
    }
    
    // 'tooltip' attribute on Link (id=DownloadLink) at DocumentsLV.pcf: line 96, column 90
    function tooltip_32 () : java.lang.String {
      return documentsActionsHelper.DownloadDocumentContentTooltip
    }
    
    // 'tooltip' attribute on Link (id=UploadLink) at DocumentsLV.pcf: line 104, column 88
    function tooltip_37 () : java.lang.String {
      return documentsActionsHelper.UploadDocumentContentTooltip
    }
    
    // 'tooltip' attribute on Link (id=DeleteLink) at DocumentsLV.pcf: line 113, column 85
    function tooltip_41 () : java.lang.String {
      return documentsActionsHelper.DeleteDocumentTooltip(document)
    }
    
    // 'value' attribute on TextCell (id=Event_Cell) at DocumentsLV.pcf: line 130, column 40
    function valueRoot_49 () : java.lang.Object {
      return document
    }
    
    // 'value' attribute on TextCell (id=Event_Cell) at DocumentsLV.pcf: line 130, column 40
    function value_48 () : java.lang.String {
      return document.Event_TDIC
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at DocumentsLV.pcf: line 137, column 25
    function value_51 () : typekey.DocumentType {
      return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=Subtype_Cell) at DocumentsLV.pcf: line 142, column 58
    function value_54 () : typekey.OnBaseDocumentSubtype_Ext {
      return document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DocumentsLV.pcf: line 148, column 25
    function value_57 () : typekey.DocumentStatusType {
      return document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at DocumentsLV.pcf: line 154, column 36
    function value_60 () : java.lang.String {
      return document.Author
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DocumentsLV.pcf: line 158, column 41
    function value_63 () : java.lang.String {
      return document.Description
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at DocumentsLV.pcf: line 167, column 25
    function value_66 () : java.util.Date {
      return document.DateModified
    }
    
    // 'visible' attribute on Link (id=UnityLink) at DocumentsLV.pcf: line 65, column 147
    function visible_16 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    // 'visible' attribute on Link (id=NameLink) at DocumentsLV.pcf: line 74, column 145
    function visible_21 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    // 'visible' attribute on Link (id=DownloadLink) at DocumentsLV.pcf: line 96, column 90
    function visible_30 () : java.lang.Boolean {
      return documentsActionsHelper.isDownloadDocumentContentVisible(document)
    }
    
    // 'visible' attribute on Link (id=UploadLink) at DocumentsLV.pcf: line 104, column 88
    function visible_34 () : java.lang.Boolean {
      return documentsActionsHelper.isUploadDocumentContentVisible(document)
    }
    
    // 'visible' attribute on Link (id=DeleteLink) at DocumentsLV.pcf: line 113, column 85
    function visible_39 () : java.lang.Boolean {
      return documentsActionsHelper.isDeleteDocumentLinkVisible(document)
    }
    
    // 'visible' attribute on Link (id=ActionsDisabled) at DocumentsLV.pcf: line 118, column 75
    function visible_43 () : java.lang.Boolean {
      return documentsActionsHelper.isDocumentPending(document)
    }
    
    // 'visible' attribute on Link (id=DocumentsLV_FinalizeLink) at DocumentsLV.pcf: line 125, column 124
    function visible_45 () : java.lang.Boolean {
      return document.MimeType == "application/dlf" and document.Status == DocumentStatusType.TC_DRAFT
    }
    
    // 'visible' attribute on LinkCell (id=Actions) at DocumentsLV.pcf: line 79, column 42
    function visible_47 () : java.lang.Boolean {
      return activityReadonlyFlag
    }
    
    // 'visible' attribute on LinkCell (id=HiddenDocument) at DocumentsLV.pcf: line 174, column 61
    function visible_70 () : java.lang.Boolean {
      return documentSearchCriteria.IncludeObsoletes
    }
    
    property get document () : entity.Document {
      return getIteratedValue(1) as entity.Document
    }
    
    
  }
  
  
}