package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/RenewPolicyWizardChargeStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class RenewPolicyWizardChargeStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/RenewPolicyWizardChargeStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RenewPolicyWizardChargeStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=DiscountedPayment_Input) at RenewPolicyWizardChargeStepScreen.pcf: line 33, column 71
    function currency_7 () : typekey.Currency {
      return renewal.Currency
    }
    
    // 'def' attribute on PanelRef at RenewPolicyWizardChargeStepScreen.pcf: line 17, column 83
    function def_onEnter_0 (def :  pcf.PolicyAddChargesListDetailPanel) : void {
      def.onEnter(renewal, renewal.PolicyPeriod, null)
    }
    
    // 'def' attribute on PanelRef at RenewPolicyWizardChargeStepScreen.pcf: line 17, column 83
    function def_refreshVariables_1 (def :  pcf.PolicyAddChargesListDetailPanel) : void {
      def.refreshVariables(renewal, renewal.PolicyPeriod, null)
    }
    
    // 'value' attribute on DateInput (id=FullPayDate_Input) at RenewPolicyWizardChargeStepScreen.pcf: line 39, column 65
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      renewal.NewPolicyPeriod.FullPayDiscountUntil = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=DiscountedPayment_Input) at RenewPolicyWizardChargeStepScreen.pcf: line 33, column 71
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      renewal.NewPolicyPeriod.DiscountedPaymentThreshold = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'required' attribute on MonetaryAmountInput (id=DiscountedPayment_Input) at RenewPolicyWizardChargeStepScreen.pcf: line 33, column 71
    function required_3 () : java.lang.Boolean {
      return renewal.NewPolicyPeriod.EligibleForFullPayDiscount
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=DiscountedPayment_Input) at RenewPolicyWizardChargeStepScreen.pcf: line 33, column 71
    function validationExpression_2 () : java.lang.Object {
      return view.isDiscountedPaymentValid() ? null : DisplayKey.get("Web.NewPolicyReportingChargeDV.DiscountedPaymentNotValid")
    }
    
    // 'value' attribute on MonetaryAmountInput (id=DiscountedPayment_Input) at RenewPolicyWizardChargeStepScreen.pcf: line 33, column 71
    function valueRoot_6 () : java.lang.Object {
      return renewal.NewPolicyPeriod
    }
    
    // 'value' attribute on DateInput (id=FullPayDate_Input) at RenewPolicyWizardChargeStepScreen.pcf: line 39, column 65
    function value_11 () : java.util.Date {
      return renewal.NewPolicyPeriod.FullPayDiscountUntil
    }
    
    // 'value' attribute on MonetaryAmountInput (id=DiscountedPayment_Input) at RenewPolicyWizardChargeStepScreen.pcf: line 33, column 71
    function value_4 () : gw.pl.currency.MonetaryAmount {
      return renewal.NewPolicyPeriod.DiscountedPaymentThreshold
    }
    
    property get renewal () : Renewal {
      return getRequireValue("renewal", 0) as Renewal
    }
    
    property set renewal ($arg :  Renewal) {
      setRequireValue("renewal", 0, $arg)
    }
    
    property get view () : gw.web.policy.PolicyWizardChargeStepScreenView {
      return getRequireValue("view", 0) as gw.web.policy.PolicyWizardChargeStepScreenView
    }
    
    property set view ($arg :  gw.web.policy.PolicyWizardChargeStepScreenView) {
      setRequireValue("view", 0, $arg)
    }
    
    
  }
  
  
}