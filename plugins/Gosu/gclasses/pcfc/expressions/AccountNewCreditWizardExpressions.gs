package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/AccountNewCreditWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountNewCreditWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/AccountNewCreditWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountNewCreditWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Wizard (id=AccountNewCreditWizard) at AccountNewCreditWizard.pcf: line 9, column 33
    function beforeCommit_6 (pickedValue :  java.lang.Object) : void {
      credit.execute()
    }
    
    // 'canVisit' attribute on Wizard (id=AccountNewCreditWizard) at AccountNewCreditWizard.pcf: line 9, column 33
    static function canVisit_7 (account :  Account) : java.lang.Boolean {
      return perm.Transaction.gentxn
    }
    
    // 'initialValue' attribute on Variable at AccountNewCreditWizard.pcf: line 18, column 22
    function initialValue_0 () : Credit {
      return createCredit()
    }
    
    // 'onExit' attribute on WizardStep (id=Step1) at AccountNewCreditWizard.pcf: line 24, column 77
    function onExit_1 () : void {
      credit.initiateApprovalActivityIfUserLacksAuthority()
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountNewCreditWizard.pcf: line 24, column 77
    function screen_onEnter_2 (def :  pcf.CreditDetailsScreen) : void {
      def.onEnter(account, credit)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountNewCreditWizard.pcf: line 29, column 83
    function screen_onEnter_4 (def :  pcf.CreditDetailsConfirmationScreen) : void {
      def.onEnter(account, credit)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountNewCreditWizard.pcf: line 24, column 77
    function screen_refreshVariables_3 (def :  pcf.CreditDetailsScreen) : void {
      def.refreshVariables(account, credit)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountNewCreditWizard.pcf: line 29, column 83
    function screen_refreshVariables_5 (def :  pcf.CreditDetailsConfirmationScreen) : void {
      def.refreshVariables(account, credit)
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewCreditWizard) at AccountNewCreditWizard.pcf: line 9, column 33
    function tabBar_onEnter_8 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewCreditWizard) at AccountNewCreditWizard.pcf: line 9, column 33
    function tabBar_refreshVariables_9 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AccountNewCreditWizard {
      return super.CurrentLocation as pcf.AccountNewCreditWizard
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get credit () : Credit {
      return getVariableValue("credit", 0) as Credit
    }
    
    property set credit ($arg :  Credit) {
      setVariableValue("credit", 0, $arg)
    }
    
    function createCredit() : Credit {
      var theCredit = new entity.Credit(account.Currency)
      theCredit.Account = account
      return theCredit
      }
    
    
  }
  
  
}