package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.web.payment.DirectBillPaymentView
@javax.annotation.Generated("config/web/pcf/payment/NewDirectBillPayment.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewDirectBillPaymentExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/NewDirectBillPayment.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewDirectBillPaymentExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'afterReturnFromPopup' attribute on Page (id=NewDirectBillPayment) at NewDirectBillPayment.pcf: line 16, column 72
    function afterReturnFromPopup_4 (popupCommitted :  boolean) : void {
      invalidateDistItemsIterator()
    }
    
    // 'beforeCancel' attribute on Page (id=NewDirectBillPayment) at NewDirectBillPayment.pcf: line 16, column 72
    function beforeCancel_5 () : void {
      if (creditCardHandler != null) creditCardHandler.finishCreditCardProcess()
    }
    
    // 'beforeCommit' attribute on Page (id=NewDirectBillPayment) at NewDirectBillPayment.pcf: line 16, column 72
    function beforeCommit_6 (pickedValue :  java.lang.Object) : void {
      if (creditCardHandler != null) tdic.bc.integ.services.creditcard.TDIC_PCFHelper.makeCreditCardPayment(paymentView, creditCardHandler); paymentView.execute( CurrentLocation )
    }
    
    // 'canVisit' attribute on Page (id=NewDirectBillPayment) at NewDirectBillPayment.pcf: line 16, column 72
    static function canVisit_7 (account :  Account) : java.lang.Boolean {
      return perm.DirectBillMoneyRcvd.pmntmanproc or perm.System.pmntmanproc_tdic
    }
    
    // 'def' attribute on ScreenRef at NewDirectBillPayment.pcf: line 31, column 66
    function def_onEnter_2 (def :  pcf.EditDBPaymentScreen) : void {
      def.onEnter(paymentView, creditCardHandler)
    }
    
    // 'def' attribute on ScreenRef at NewDirectBillPayment.pcf: line 31, column 66
    function def_refreshVariables_3 (def :  pcf.EditDBPaymentScreen) : void {
      def.refreshVariables(paymentView, creditCardHandler)
    }
    
    // 'initialValue' attribute on Variable at NewDirectBillPayment.pcf: line 25, column 72
    function initialValue_0 () : tdic.bc.integ.services.creditcard.TDIC_CreditCardHandler {
      return new tdic.bc.integ.services.creditcard.TDIC_AuthorizeNetCreditCardHandler()
    }
    
    // 'initialValue' attribute on Variable at NewDirectBillPayment.pcf: line 29, column 56
    function initialValue_1 () : gw.api.web.payment.DirectBillPaymentView {
      return tdic.bc.integ.services.creditcard.TDIC_PCFHelper.createNewPaymentView(account, creditCardHandler)
    }
    
    // 'parent' attribute on Page (id=NewDirectBillPayment) at NewDirectBillPayment.pcf: line 16, column 72
    static function parent_8 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.NewDirectBillPayment {
      return super.CurrentLocation as pcf.NewDirectBillPayment
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get creditCardHandler () : tdic.bc.integ.services.creditcard.TDIC_CreditCardHandler {
      return getVariableValue("creditCardHandler", 0) as tdic.bc.integ.services.creditcard.TDIC_CreditCardHandler
    }
    
    property set creditCardHandler ($arg :  tdic.bc.integ.services.creditcard.TDIC_CreditCardHandler) {
      setVariableValue("creditCardHandler", 0, $arg)
    }
    
    property get paymentView () : gw.api.web.payment.DirectBillPaymentView {
      return getVariableValue("paymentView", 0) as gw.api.web.payment.DirectBillPaymentView
    }
    
    property set paymentView ($arg :  gw.api.web.payment.DirectBillPaymentView) {
      setVariableValue("paymentView", 0, $arg)
    }
    
    
        function createDirectBillPaymentView() : DirectBillPaymentView {
          var view = gw.api.web.payment.DirectBillPaymentView.createView(account,false);
          if (account.ListBill) {
            view.IncludeOnlyCriteria = DistributionLimitType.TC_OUTSTANDING
          } else {
            view.IncludeOnlyCriteria = account.DistributionLimitTypeFromPlan;
          }
          return view
        }
        
        function invalidateDistItemsIterator(){
      gw.api.web.PebblesUtil.invalidateIterators(CurrentLocation, BaseDistItem)
    }
    
    
  }
  
  
}