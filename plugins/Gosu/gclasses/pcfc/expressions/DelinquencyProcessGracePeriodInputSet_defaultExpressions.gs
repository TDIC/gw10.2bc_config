package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessGracePeriodInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyProcessGracePeriodInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessGracePeriodInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyProcessGracePeriodInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyInput (id=GracePeriodDayUnit_Input) at DelinquencyProcessGracePeriodInputSet.default.pcf: line 22, column 36
    function valueRoot_3 () : java.lang.Object {
      return delinquencyProcess.DelinquencyPlan
    }
    
    // 'value' attribute on DateInput (id=GracePeriodEndDate_Input) at DelinquencyProcessGracePeriodInputSet.default.pcf: line 26, column 54
    function valueRoot_6 () : java.lang.Object {
      return delinquencyProcess
    }
    
    // 'value' attribute on TextInput (id=GracePeriodDays_Input) at DelinquencyProcessGracePeriodInputSet.default.pcf: line 17, column 38
    function value_0 () : java.lang.Integer {
      return gw.api.domain.invoice.FeesThresholdsManager.getGracePeriodDays(delinquencyProcess)
    }
    
    // 'value' attribute on TypeKeyInput (id=GracePeriodDayUnit_Input) at DelinquencyProcessGracePeriodInputSet.default.pcf: line 22, column 36
    function value_2 () : typekey.DayUnit {
      return delinquencyProcess.DelinquencyPlan.GracePeriodDayUnit
    }
    
    // 'value' attribute on DateInput (id=GracePeriodEndDate_Input) at DelinquencyProcessGracePeriodInputSet.default.pcf: line 26, column 54
    function value_5 () : java.util.Date {
      return delinquencyProcess.GracePeriodEndDate
    }
    
    property get delinquencyProcess () : entity.DelinquencyProcess {
      return getRequireValue("delinquencyProcess", 0) as entity.DelinquencyProcess
    }
    
    property set delinquencyProcess ($arg :  entity.DelinquencyProcess) {
      setRequireValue("delinquencyProcess", 0, $arg)
    }
    
    
  }
  
  
}