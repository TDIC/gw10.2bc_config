package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/activityenhancement/ProducerActivitiesPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerActivitiesPageExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/activityenhancement/ProducerActivitiesPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ProducerActivitiesPageExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=subject_Cell) at ProducerActivitiesPage.pcf: line 71, column 43
    function action_29 () : void {
      ActivityDetailWorksheet.goInWorkspace(anActivity)
    }
    
    // 'action' attribute on TextCell (id=subject_Cell) at ProducerActivitiesPage.pcf: line 71, column 43
    function action_dest_30 () : pcf.api.Destination {
      return pcf.ActivityDetailWorksheet.createDestination(anActivity)
    }
    
    // 'fontColor' attribute on DateCell (id=duedate_Cell) at ProducerActivitiesPage.pcf: line 56, column 46
    function fontColor_15 () : java.lang.Object {
      return anActivity.Overdue == true ? "Red" : ""
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at ProducerActivitiesPage.pcf: line 45, column 46
    function valueRoot_10 () : java.lang.Object {
      return anActivity
    }
    
    // 'value' attribute on DateCell (id=CreateDate_Cell) at ProducerActivitiesPage.pcf: line 51, column 46
    function value_12 () : java.util.Date {
      return anActivity.CreateTime
    }
    
    // 'value' attribute on DateCell (id=duedate_Cell) at ProducerActivitiesPage.pcf: line 56, column 46
    function value_16 () : java.util.Date {
      return anActivity.TargetDate
    }
    
    // 'value' attribute on TypeKeyCell (id=priority_Cell) at ProducerActivitiesPage.pcf: line 61, column 37
    function value_21 () : Priority {
      return anActivity.Priority
    }
    
    // 'value' attribute on TypeKeyCell (id=status_Cell) at ProducerActivitiesPage.pcf: line 66, column 43
    function value_25 () : ActivityStatus {
      return anActivity.Status
    }
    
    // 'value' attribute on TextCell (id=subject_Cell) at ProducerActivitiesPage.pcf: line 71, column 43
    function value_31 () : java.lang.String {
      return anActivity.Subject
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at ProducerActivitiesPage.pcf: line 45, column 46
    function value_9 () : java.lang.Boolean {
      return anActivity.Escalated
    }
    
    // 'fontColor' attribute on DateCell (id=duedate_Cell) at ProducerActivitiesPage.pcf: line 56, column 46
    function verifyFontColorIsAllowedType_18 ($$arg :  gw.api.web.color.GWColor) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=duedate_Cell) at ProducerActivitiesPage.pcf: line 56, column 46
    function verifyFontColorIsAllowedType_18 ($$arg :  java.lang.String) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=duedate_Cell) at ProducerActivitiesPage.pcf: line 56, column 46
    function verifyFontColor_19 () : void {
      var __fontColorArg = anActivity.Overdue == true ? "Red" : ""
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the fontColor return type is not a valid type for use with a ValueWidget's fontColor
      // It needs to return a String or a GWColor.
      verifyFontColorIsAllowedType_18(__fontColorArg)
    }
    
    // 'valueType' attribute on TypeKeyCell (id=priority_Cell) at ProducerActivitiesPage.pcf: line 61, column 37
    function verifyValueType_24 () : void {
      var __valueTypeArg : Priority
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : gw.entity.TypeKey = __valueTypeArg
    }
    
    // 'valueType' attribute on TypeKeyCell (id=status_Cell) at ProducerActivitiesPage.pcf: line 66, column 43
    function verifyValueType_28 () : void {
      var __valueTypeArg : ActivityStatus
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : gw.entity.TypeKey = __valueTypeArg
    }
    
    property get anActivity () : Activity {
      return getIteratedValue(1) as Activity
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/activityenhancement/ProducerActivitiesPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerActivitiesPageExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=ProducerActivitiesPage) at ProducerActivitiesPage.pcf: line 8, column 80
    static function canVisit_35 (producer :  Producer) : java.lang.Boolean {
      return !perm.System.hideprodactivt_TDIC
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at ProducerActivitiesPage.pcf: line 26, column 94
    function filters_0 () : gw.api.filters.IFilter[] {
      return gw.acc.activityenhancement.ActivityFilterUtil.ActivityFilterOptions
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at ProducerActivitiesPage.pcf: line 31, column 102
    function filters_1 () : gw.api.filters.IFilter[] {
      return gw.acc.activityenhancement.ActivityFilterUtil.ActivityPatternsFilterOptions
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at ProducerActivitiesPage.pcf: line 36, column 110
    function filters_2 () : gw.api.filters.IFilter[] {
      return gw.acc.activityenhancement.ActivityFilterUtil.ProducerActivitySubtypesFilterOptions
    }
    
    // Page (id=ProducerActivitiesPage) at ProducerActivitiesPage.pcf: line 8, column 80
    static function parent_36 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at ProducerActivitiesPage.pcf: line 45, column 46
    function sortValue_3 (anActivity :  Activity) : java.lang.Object {
      return anActivity.Escalated
    }
    
    // 'value' attribute on DateCell (id=CreateDate_Cell) at ProducerActivitiesPage.pcf: line 51, column 46
    function sortValue_4 (anActivity :  Activity) : java.lang.Object {
      return anActivity.CreateTime
    }
    
    // 'value' attribute on DateCell (id=duedate_Cell) at ProducerActivitiesPage.pcf: line 56, column 46
    function sortValue_5 (anActivity :  Activity) : java.lang.Object {
      return anActivity.TargetDate
    }
    
    // 'value' attribute on TypeKeyCell (id=priority_Cell) at ProducerActivitiesPage.pcf: line 61, column 37
    function sortValue_6 (anActivity :  Activity) : java.lang.Object {
      return anActivity.Priority
    }
    
    // 'value' attribute on TypeKeyCell (id=status_Cell) at ProducerActivitiesPage.pcf: line 66, column 43
    function sortValue_7 (anActivity :  Activity) : java.lang.Object {
      return anActivity.Status
    }
    
    // 'value' attribute on TextCell (id=subject_Cell) at ProducerActivitiesPage.pcf: line 71, column 43
    function sortValue_8 (anActivity :  Activity) : java.lang.Object {
      return anActivity.Subject
    }
    
    // 'value' attribute on RowIterator (id=id) at ProducerActivitiesPage.pcf: line 22, column 34
    function value_34 () : Activity[] {
      return producer.Activities_Ext
    }
    
    override property get CurrentLocation () : pcf.ProducerActivitiesPage {
      return super.CurrentLocation as pcf.ProducerActivitiesPage
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}