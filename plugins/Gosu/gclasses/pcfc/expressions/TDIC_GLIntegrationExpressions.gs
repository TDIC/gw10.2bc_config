package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/businesssettings/TDIC_GLIntegration.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_GLIntegrationExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/TDIC_GLIntegration.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends TDIC_GLIntegrationExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=transactionTypeId_Cell) at TDIC_GLIntegration.pcf: line 76, column 54
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentMapping.OriginalTransactionName = (__VALUE_TO_SET as typekey.Transaction)
    }
    
    // 'value' attribute on TextCell (id=glMappingCodeId_Cell) at TDIC_GLIntegration.pcf: line 82, column 51
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentMapping.MappedTransactionName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyCell (id=transactionTypeId_Cell) at TDIC_GLIntegration.pcf: line 76, column 54
    function valueRoot_14 () : java.lang.Object {
      return currentMapping
    }
    
    // 'value' attribute on TypeKeyCell (id=transactionTypeId_Cell) at TDIC_GLIntegration.pcf: line 76, column 54
    function value_12 () : typekey.Transaction {
      return currentMapping.OriginalTransactionName
    }
    
    // 'value' attribute on TextCell (id=glMappingCodeId_Cell) at TDIC_GLIntegration.pcf: line 82, column 51
    function value_16 () : java.lang.String {
      return currentMapping.MappedTransactionName
    }
    
    property get currentMapping () : entity.GLIntegrationTransactionMapping_TDIC {
      return getIteratedValue(1) as entity.GLIntegrationTransactionMapping_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/TDIC_GLIntegration.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends TDIC_GLIntegrationExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=TransactionName_Cell) at TDIC_GLIntegration.pcf: line 117, column 54
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentTAccountMapping.TranasctionType = (__VALUE_TO_SET as typekey.Transaction)
    }
    
    // 'value' attribute on TypeKeyCell (id=LineItemType_Cell) at TDIC_GLIntegration.pcf: line 123, column 53
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentTAccountMapping.LineItemType = (__VALUE_TO_SET as typekey.LedgerSide)
    }
    
    // 'value' attribute on TextCell (id=FutureTerm_Cell) at TDIC_GLIntegration.pcf: line 129, column 52
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentTAccountMapping.FutureTerm = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyCell (id=InvoiceStatus_Cell) at TDIC_GLIntegration.pcf: line 135, column 56
    function defaultSetter_42 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentTAccountMapping.InvoiceStatus = (__VALUE_TO_SET as typekey.InvoiceStatus)
    }
    
    // 'value' attribute on TypeKeyCell (id=OriginalTAccountName_Cell) at TDIC_GLIntegration.pcf: line 141, column 66
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentTAccountMapping.OriginalTAccountName = (__VALUE_TO_SET as typekey.GLOriginalTAccount_TDIC)
    }
    
    // 'value' attribute on TextCell (id=MappedTAccountName_Cell) at TDIC_GLIntegration.pcf: line 146, column 72
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentTAccountMapping.MappedTAccountName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyCell (id=TransactionName_Cell) at TDIC_GLIntegration.pcf: line 117, column 54
    function valueRoot_31 () : java.lang.Object {
      return currentTAccountMapping
    }
    
    // 'value' attribute on TypeKeyCell (id=TransactionName_Cell) at TDIC_GLIntegration.pcf: line 117, column 54
    function value_29 () : typekey.Transaction {
      return currentTAccountMapping.TranasctionType
    }
    
    // 'value' attribute on TypeKeyCell (id=LineItemType_Cell) at TDIC_GLIntegration.pcf: line 123, column 53
    function value_33 () : typekey.LedgerSide {
      return currentTAccountMapping.LineItemType
    }
    
    // 'value' attribute on TextCell (id=FutureTerm_Cell) at TDIC_GLIntegration.pcf: line 129, column 52
    function value_37 () : java.lang.Boolean {
      return currentTAccountMapping.FutureTerm
    }
    
    // 'value' attribute on TypeKeyCell (id=InvoiceStatus_Cell) at TDIC_GLIntegration.pcf: line 135, column 56
    function value_41 () : typekey.InvoiceStatus {
      return currentTAccountMapping.InvoiceStatus
    }
    
    // 'value' attribute on TypeKeyCell (id=OriginalTAccountName_Cell) at TDIC_GLIntegration.pcf: line 141, column 66
    function value_45 () : typekey.GLOriginalTAccount_TDIC {
      return currentTAccountMapping.OriginalTAccountName
    }
    
    // 'value' attribute on TextCell (id=MappedTAccountName_Cell) at TDIC_GLIntegration.pcf: line 146, column 72
    function value_49 () : java.lang.String {
      return currentTAccountMapping.MappedTAccountName
    }
    
    property get currentTAccountMapping () : entity.GLIntegrationTAccountMapping_TDIC {
      return getIteratedValue(1) as entity.GLIntegrationTAccountMapping_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/TDIC_GLIntegration.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_GLIntegrationExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=transactionTypeId_Cell) at TDIC_GLIntegration.pcf: line 41, column 54
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentFilter.TransactionType = (__VALUE_TO_SET as typekey.Transaction)
    }
    
    // 'value' attribute on TypeKeyCell (id=transactionTypeId_Cell) at TDIC_GLIntegration.pcf: line 41, column 54
    function valueRoot_4 () : java.lang.Object {
      return currentFilter
    }
    
    // 'value' attribute on TypeKeyCell (id=transactionTypeId_Cell) at TDIC_GLIntegration.pcf: line 41, column 54
    function value_2 () : typekey.Transaction {
      return currentFilter.TransactionType
    }
    
    property get currentFilter () : entity.GLIntegrationTransactionFilter_TDIC {
      return getIteratedValue(1) as entity.GLIntegrationTransactionFilter_TDIC
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/TDIC_GLIntegration.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_GLIntegrationExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=TDIC_GLIntegration) at TDIC_GLIntegration.pcf: line 9, column 80
    static function canVisit_56 () : java.lang.Boolean {
      return perm.System.admintabview
    }
    
    // EditButtons at TDIC_GLIntegration.pcf: line 18, column 29
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // Page (id=TDIC_GLIntegration) at TDIC_GLIntegration.pcf: line 9, column 80
    static function parent_57 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    // 'value' attribute on TypeKeyCell (id=transactionTypeId_Cell) at TDIC_GLIntegration.pcf: line 41, column 54
    function sortValue_1 (currentFilter :  entity.GLIntegrationTransactionFilter_TDIC) : java.lang.Object {
      return currentFilter.TransactionType
    }
    
    // 'value' attribute on TypeKeyCell (id=transactionTypeId_Cell) at TDIC_GLIntegration.pcf: line 76, column 54
    function sortValue_10 (currentMapping :  entity.GLIntegrationTransactionMapping_TDIC) : java.lang.Object {
      return currentMapping.OriginalTransactionName
    }
    
    // 'value' attribute on TextCell (id=glMappingCodeId_Cell) at TDIC_GLIntegration.pcf: line 82, column 51
    function sortValue_11 (currentMapping :  entity.GLIntegrationTransactionMapping_TDIC) : java.lang.Object {
      return currentMapping.MappedTransactionName
    }
    
    // 'value' attribute on TypeKeyCell (id=LineItemType_Cell) at TDIC_GLIntegration.pcf: line 123, column 53
    function sortValue_24 (currentTAccountMapping :  entity.GLIntegrationTAccountMapping_TDIC) : java.lang.Object {
      return currentTAccountMapping.LineItemType
    }
    
    // 'value' attribute on TextCell (id=FutureTerm_Cell) at TDIC_GLIntegration.pcf: line 129, column 52
    function sortValue_25 (currentTAccountMapping :  entity.GLIntegrationTAccountMapping_TDIC) : java.lang.Object {
      return currentTAccountMapping.FutureTerm
    }
    
    // 'value' attribute on TypeKeyCell (id=InvoiceStatus_Cell) at TDIC_GLIntegration.pcf: line 135, column 56
    function sortValue_26 (currentTAccountMapping :  entity.GLIntegrationTAccountMapping_TDIC) : java.lang.Object {
      return currentTAccountMapping.InvoiceStatus
    }
    
    // 'value' attribute on TypeKeyCell (id=OriginalTAccountName_Cell) at TDIC_GLIntegration.pcf: line 141, column 66
    function sortValue_27 (currentTAccountMapping :  entity.GLIntegrationTAccountMapping_TDIC) : java.lang.Object {
      return currentTAccountMapping.OriginalTAccountName
    }
    
    // 'value' attribute on TextCell (id=MappedTAccountName_Cell) at TDIC_GLIntegration.pcf: line 146, column 72
    function sortValue_28 (currentTAccountMapping :  entity.GLIntegrationTAccountMapping_TDIC) : java.lang.Object {
      return currentTAccountMapping.MappedTAccountName
    }
    
    // 'toAdd' attribute on RowIterator (id=mappingsRowIteratorId) at TDIC_GLIntegration.pcf: line 69, column 113
    function toAdd_20 (currentMapping :  entity.GLIntegrationTransactionMapping_TDIC) : void {
      tdic.bc.integ.plugins.generalledger.helper.TDIC_GLIntegrationHelper.addEntity(currentMapping)
    }
    
    // 'toAdd' attribute on RowIterator (id=GLTAccountMappingRowIteratorId) at TDIC_GLIntegration.pcf: line 109, column 110
    function toAdd_53 (currentTAccountMapping :  entity.GLIntegrationTAccountMapping_TDIC) : void {
      tdic.bc.integ.plugins.generalledger.helper.TDIC_GLIntegrationHelper.addEntity(currentTAccountMapping)
    }
    
    // 'toAdd' attribute on RowIterator (id=filtersRowIteratorId) at TDIC_GLIntegration.pcf: line 34, column 112
    function toAdd_6 (currentFilter :  entity.GLIntegrationTransactionFilter_TDIC) : void {
      tdic.bc.integ.plugins.generalledger.helper.TDIC_GLIntegrationHelper.addEntity(currentFilter)
    }
    
    // 'toRemove' attribute on RowIterator (id=mappingsRowIteratorId) at TDIC_GLIntegration.pcf: line 69, column 113
    function toRemove_21 (currentMapping :  entity.GLIntegrationTransactionMapping_TDIC) : void {
      tdic.bc.integ.plugins.generalledger.helper.TDIC_GLIntegrationHelper.deleteEntity(currentMapping)
    }
    
    // 'toRemove' attribute on RowIterator (id=GLTAccountMappingRowIteratorId) at TDIC_GLIntegration.pcf: line 109, column 110
    function toRemove_54 (currentTAccountMapping :  entity.GLIntegrationTAccountMapping_TDIC) : void {
      tdic.bc.integ.plugins.generalledger.helper.TDIC_GLIntegrationHelper.deleteEntity(currentTAccountMapping)
    }
    
    // 'toRemove' attribute on RowIterator (id=filtersRowIteratorId) at TDIC_GLIntegration.pcf: line 34, column 112
    function toRemove_7 (currentFilter :  entity.GLIntegrationTransactionFilter_TDIC) : void {
      tdic.bc.integ.plugins.generalledger.helper.TDIC_GLIntegrationHelper.deleteEntity(currentFilter)
    }
    
    // 'value' attribute on RowIterator (id=mappingsRowIteratorId) at TDIC_GLIntegration.pcf: line 69, column 113
    function value_22 () : gw.api.database.IQueryBeanResult<entity.GLIntegrationTransactionMapping_TDIC> {
      return gw.api.database.Query.make(entity.GLIntegrationTransactionMapping_TDIC).select()
    }
    
    // 'value' attribute on RowIterator (id=GLTAccountMappingRowIteratorId) at TDIC_GLIntegration.pcf: line 109, column 110
    function value_55 () : gw.api.database.IQueryBeanResult<entity.GLIntegrationTAccountMapping_TDIC> {
      return gw.api.database.Query.make(entity.GLIntegrationTAccountMapping_TDIC).select()
    }
    
    // 'value' attribute on RowIterator (id=filtersRowIteratorId) at TDIC_GLIntegration.pcf: line 34, column 112
    function value_8 () : gw.api.database.IQueryBeanResult<entity.GLIntegrationTransactionFilter_TDIC> {
      return gw.api.database.Query.make(entity.GLIntegrationTransactionFilter_TDIC).select()
    }
    
    override property get CurrentLocation () : pcf.TDIC_GLIntegration {
      return super.CurrentLocation as pcf.TDIC_GLIntegration
    }
    
    
  }
  
  
}