package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/accounting/TAccountOwnersLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TAccountOwnersLVExpressions {
  @javax.annotation.Generated("config/web/pcf/accounting/TAccountOwnersLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TAccountOwnersLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Type_Cell) at TAccountOwnersLV.pcf: line 30, column 55
    function valueRoot_3 () : java.lang.Object {
      return (typeof tAccountOwner)
    }
    
    // 'value' attribute on TextCell (id=TAccountOwner_Cell) at TAccountOwnersLV.pcf: line 25, column 45
    function value_0 () : entity.TAccountOwner {
      return tAccountOwner
    }
    
    // 'value' attribute on TextCell (id=Type_Cell) at TAccountOwnersLV.pcf: line 30, column 55
    function value_2 () : java.lang.String {
      return (typeof tAccountOwner).DisplayName
    }
    
    property get tAccountOwner () : entity.TAccountOwner {
      return getIteratedValue(1) as entity.TAccountOwner
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/accounting/TAccountOwnersLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TAccountOwnersLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RowIterator (id=TAccountOwners) at TAccountOwnersLV.pcf: line 18, column 42
    function value_5 () : entity.TAccountOwner[] {
      return AllTAccountOwners
    }
    
    property get primaryTAccountOwner () : TAccountOwner {
      return getRequireValue("primaryTAccountOwner", 0) as TAccountOwner
    }
    
    property set primaryTAccountOwner ($arg :  TAccountOwner) {
      setRequireValue("primaryTAccountOwner", 0, $arg)
    }
    
    property get relatedTAccountOwners () : TAccountOwner[] {
      return getRequireValue("relatedTAccountOwners", 0) as TAccountOwner[]
    }
    
    property set relatedTAccountOwners ($arg :  TAccountOwner[]) {
      setRequireValue("relatedTAccountOwners", 0, $arg)
    }
    
    
    property get AllTAccountOwners() : TAccountOwner[] {
            var result = new java.util.ArrayList();
            result.add(primaryTAccountOwner);
            for (relatedTAccountOwner in relatedTAccountOwners) {
              result.add(relatedTAccountOwner);
            }
            return gw.api.upgrade.Coercions.makeArray<entity.TAccountOwner>(result);
          }
        
    
    
  }
  
  
}