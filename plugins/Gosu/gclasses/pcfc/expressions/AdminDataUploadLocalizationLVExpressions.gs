package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadLocalizationLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadLocalizationLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadLocalizationLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadLocalizationLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadLocalizationLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=activityPattern_Cell) at AdminDataUploadLocalizationLV.pcf: line 48, column 41
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.ActivityPattern")
    }
    
    // 'label' attribute on TextCell (id=activityPatternSubject_Cell) at AdminDataUploadLocalizationLV.pcf: line 53, column 41
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.ActivityPatternSubject")
    }
    
    // 'label' attribute on TextCell (id=activityPatternDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 58, column 41
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.ActivityPatternDesc")
    }
    
    // 'label' attribute on TextCell (id=secZone_Cell) at AdminDataUploadLocalizationLV.pcf: line 63, column 41
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.SecZone")
    }
    
    // 'label' attribute on TextCell (id=secZoneName_Cell) at AdminDataUploadLocalizationLV.pcf: line 68, column 41
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.SecZoneName")
    }
    
    // 'label' attribute on TextCell (id=secZoneDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 73, column 41
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.SecZoneDesc")
    }
    
    // 'label' attribute on TextCell (id=authLimitProfile_Cell) at AdminDataUploadLocalizationLV.pcf: line 78, column 41
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.AuthLimitProfile")
    }
    
    // 'label' attribute on TextCell (id=authLimitProfileName_Cell) at AdminDataUploadLocalizationLV.pcf: line 83, column 41
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.AuthLimitProfileName")
    }
    
    // 'label' attribute on TextCell (id=authLimitProfileDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 88, column 41
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.AuthLimitProfileDesc")
    }
    
    // 'label' attribute on TextCell (id=group_Cell) at AdminDataUploadLocalizationLV.pcf: line 93, column 41
    function label_29 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.Group")
    }
    
    // 'label' attribute on TypeKeyCell (id=language_Cell) at AdminDataUploadLocalizationLV.pcf: line 28, column 37
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.Language")
    }
    
    // 'label' attribute on TextCell (id=groupName_Cell) at AdminDataUploadLocalizationLV.pcf: line 98, column 41
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.GroupName")
    }
    
    // 'label' attribute on TextCell (id=region_Cell) at AdminDataUploadLocalizationLV.pcf: line 103, column 41
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.Region")
    }
    
    // 'label' attribute on TextCell (id=regionName_Cell) at AdminDataUploadLocalizationLV.pcf: line 108, column 41
    function label_35 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.RegionName")
    }
    
    // 'label' attribute on TextCell (id=attribute_Cell) at AdminDataUploadLocalizationLV.pcf: line 113, column 41
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.Attribute")
    }
    
    // 'label' attribute on TextCell (id=attributeName_Cell) at AdminDataUploadLocalizationLV.pcf: line 118, column 41
    function label_39 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.AttributeName")
    }
    
    // 'label' attribute on TextCell (id=attributeDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 123, column 41
    function label_41 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.AttributeDesc")
    }
    
    // 'label' attribute on TextCell (id=holiday_Cell) at AdminDataUploadLocalizationLV.pcf: line 128, column 41
    function label_43 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.Holiday")
    }
    
    // 'label' attribute on TextCell (id=holidayName_Cell) at AdminDataUploadLocalizationLV.pcf: line 133, column 41
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.HolidayName")
    }
    
    // 'label' attribute on TextCell (id=role_Cell) at AdminDataUploadLocalizationLV.pcf: line 33, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.Role")
    }
    
    // 'label' attribute on TextCell (id=roleName_Cell) at AdminDataUploadLocalizationLV.pcf: line 38, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.RoleName")
    }
    
    // 'label' attribute on TextCell (id=roleDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 43, column 41
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.RoleDesc")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadLocalizationLV.pcf: line 23, column 46
    function sortValue_1 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return processor.getLoadStatus(localization)
    }
    
    // 'value' attribute on TextCell (id=roleDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 43, column 41
    function sortValue_10 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.RoleDesc
    }
    
    // 'value' attribute on TextCell (id=activityPattern_Cell) at AdminDataUploadLocalizationLV.pcf: line 48, column 41
    function sortValue_12 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.ActivityPattern
    }
    
    // 'value' attribute on TextCell (id=activityPatternSubject_Cell) at AdminDataUploadLocalizationLV.pcf: line 53, column 41
    function sortValue_14 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.ActvPatternSubj
    }
    
    // 'value' attribute on TextCell (id=activityPatternDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 58, column 41
    function sortValue_16 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.ActvPatternDesc
    }
    
    // 'value' attribute on TextCell (id=secZone_Cell) at AdminDataUploadLocalizationLV.pcf: line 63, column 41
    function sortValue_18 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.SecZone
    }
    
    // 'value' attribute on TextCell (id=secZoneName_Cell) at AdminDataUploadLocalizationLV.pcf: line 68, column 41
    function sortValue_20 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.SecZoneName
    }
    
    // 'value' attribute on TextCell (id=secZoneDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 73, column 41
    function sortValue_22 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.SecZoneDesc
    }
    
    // 'value' attribute on TextCell (id=authLimitProfile_Cell) at AdminDataUploadLocalizationLV.pcf: line 78, column 41
    function sortValue_24 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.AuthLimitProfile
    }
    
    // 'value' attribute on TextCell (id=authLimitProfileName_Cell) at AdminDataUploadLocalizationLV.pcf: line 83, column 41
    function sortValue_26 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.AuthLimitProfileName
    }
    
    // 'value' attribute on TextCell (id=authLimitProfileDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 88, column 41
    function sortValue_28 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.AuthLimitProfileDesc
    }
    
    // 'value' attribute on TextCell (id=group_Cell) at AdminDataUploadLocalizationLV.pcf: line 93, column 41
    function sortValue_30 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.Group
    }
    
    // 'value' attribute on TextCell (id=groupName_Cell) at AdminDataUploadLocalizationLV.pcf: line 98, column 41
    function sortValue_32 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.GroupName
    }
    
    // 'value' attribute on TextCell (id=region_Cell) at AdminDataUploadLocalizationLV.pcf: line 103, column 41
    function sortValue_34 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.Region
    }
    
    // 'value' attribute on TextCell (id=regionName_Cell) at AdminDataUploadLocalizationLV.pcf: line 108, column 41
    function sortValue_36 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.RegionName
    }
    
    // 'value' attribute on TextCell (id=attribute_Cell) at AdminDataUploadLocalizationLV.pcf: line 113, column 41
    function sortValue_38 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.Attribute
    }
    
    // 'value' attribute on TypeKeyCell (id=language_Cell) at AdminDataUploadLocalizationLV.pcf: line 28, column 37
    function sortValue_4 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.Language
    }
    
    // 'value' attribute on TextCell (id=attributeName_Cell) at AdminDataUploadLocalizationLV.pcf: line 118, column 41
    function sortValue_40 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.AttributeName
    }
    
    // 'value' attribute on TextCell (id=attributeDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 123, column 41
    function sortValue_42 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.AttributeDesc
    }
    
    // 'value' attribute on TextCell (id=holiday_Cell) at AdminDataUploadLocalizationLV.pcf: line 128, column 41
    function sortValue_44 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.Holiday
    }
    
    // 'value' attribute on TextCell (id=holidayName_Cell) at AdminDataUploadLocalizationLV.pcf: line 133, column 41
    function sortValue_46 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.HolidayName
    }
    
    // 'value' attribute on TextCell (id=role_Cell) at AdminDataUploadLocalizationLV.pcf: line 33, column 41
    function sortValue_6 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.Role
    }
    
    // 'value' attribute on TextCell (id=roleName_Cell) at AdminDataUploadLocalizationLV.pcf: line 38, column 41
    function sortValue_8 (localization :  tdic.util.dataloader.data.admindata.AdminLocalizationData) : java.lang.Object {
      return localization.RoleName
    }
    
    // 'value' attribute on RowIterator (id=localization) at AdminDataUploadLocalizationLV.pcf: line 15, column 104
    function value_164 () : java.util.ArrayList<tdic.util.dataloader.data.admindata.AdminLocalizationData> {
      return processor.LocalizationArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadLocalizationLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadLocalizationLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminDataUploadLocalizationLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at AdminDataUploadLocalizationLV.pcf: line 17, column 96
    function highlighted_163 () : java.lang.Boolean {
      return (localization.Error or localization.Skipped) and processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=authLimitProfile_Cell) at AdminDataUploadLocalizationLV.pcf: line 78, column 41
    function label_103 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.AuthLimitProfile")
    }
    
    // 'label' attribute on TextCell (id=authLimitProfileName_Cell) at AdminDataUploadLocalizationLV.pcf: line 83, column 41
    function label_108 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.AuthLimitProfileName")
    }
    
    // 'label' attribute on TextCell (id=authLimitProfileDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 88, column 41
    function label_113 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.AuthLimitProfileDesc")
    }
    
    // 'label' attribute on TextCell (id=group_Cell) at AdminDataUploadLocalizationLV.pcf: line 93, column 41
    function label_118 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.Group")
    }
    
    // 'label' attribute on TextCell (id=groupName_Cell) at AdminDataUploadLocalizationLV.pcf: line 98, column 41
    function label_123 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.GroupName")
    }
    
    // 'label' attribute on TextCell (id=region_Cell) at AdminDataUploadLocalizationLV.pcf: line 103, column 41
    function label_128 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.Region")
    }
    
    // 'label' attribute on TextCell (id=regionName_Cell) at AdminDataUploadLocalizationLV.pcf: line 108, column 41
    function label_133 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.RegionName")
    }
    
    // 'label' attribute on TextCell (id=attribute_Cell) at AdminDataUploadLocalizationLV.pcf: line 113, column 41
    function label_138 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.Attribute")
    }
    
    // 'label' attribute on TextCell (id=attributeName_Cell) at AdminDataUploadLocalizationLV.pcf: line 118, column 41
    function label_143 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.AttributeName")
    }
    
    // 'label' attribute on TextCell (id=attributeDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 123, column 41
    function label_148 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.AttributeDesc")
    }
    
    // 'label' attribute on TextCell (id=holiday_Cell) at AdminDataUploadLocalizationLV.pcf: line 128, column 41
    function label_153 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.Holiday")
    }
    
    // 'label' attribute on TextCell (id=holidayName_Cell) at AdminDataUploadLocalizationLV.pcf: line 133, column 41
    function label_158 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.HolidayName")
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadLocalizationLV.pcf: line 23, column 46
    function label_47 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TypeKeyCell (id=language_Cell) at AdminDataUploadLocalizationLV.pcf: line 28, column 37
    function label_52 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.Language")
    }
    
    // 'label' attribute on TextCell (id=role_Cell) at AdminDataUploadLocalizationLV.pcf: line 33, column 41
    function label_58 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.Role")
    }
    
    // 'label' attribute on TextCell (id=roleName_Cell) at AdminDataUploadLocalizationLV.pcf: line 38, column 41
    function label_63 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.RoleName")
    }
    
    // 'label' attribute on TextCell (id=roleDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 43, column 41
    function label_68 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.RoleDesc")
    }
    
    // 'label' attribute on TextCell (id=activityPattern_Cell) at AdminDataUploadLocalizationLV.pcf: line 48, column 41
    function label_73 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.ActivityPattern")
    }
    
    // 'label' attribute on TextCell (id=activityPatternSubject_Cell) at AdminDataUploadLocalizationLV.pcf: line 53, column 41
    function label_78 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.ActivityPatternSubject")
    }
    
    // 'label' attribute on TextCell (id=activityPatternDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 58, column 41
    function label_83 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.ActivityPatternDesc")
    }
    
    // 'label' attribute on TextCell (id=secZone_Cell) at AdminDataUploadLocalizationLV.pcf: line 63, column 41
    function label_88 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.SecZone")
    }
    
    // 'label' attribute on TextCell (id=secZoneName_Cell) at AdminDataUploadLocalizationLV.pcf: line 68, column 41
    function label_93 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.SecZoneName")
    }
    
    // 'label' attribute on TextCell (id=secZoneDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 73, column 41
    function label_98 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations.SecZoneDesc")
    }
    
    // 'value' attribute on TypeKeyCell (id=language_Cell) at AdminDataUploadLocalizationLV.pcf: line 28, column 37
    function valueRoot_54 () : java.lang.Object {
      return localization
    }
    
    // 'value' attribute on TextCell (id=authLimitProfile_Cell) at AdminDataUploadLocalizationLV.pcf: line 78, column 41
    function value_104 () : java.lang.String {
      return localization.AuthLimitProfile
    }
    
    // 'value' attribute on TextCell (id=authLimitProfileName_Cell) at AdminDataUploadLocalizationLV.pcf: line 83, column 41
    function value_109 () : java.lang.String {
      return localization.AuthLimitProfileName
    }
    
    // 'value' attribute on TextCell (id=authLimitProfileDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 88, column 41
    function value_114 () : java.lang.String {
      return localization.AuthLimitProfileDesc
    }
    
    // 'value' attribute on TextCell (id=group_Cell) at AdminDataUploadLocalizationLV.pcf: line 93, column 41
    function value_119 () : java.lang.String {
      return localization.Group
    }
    
    // 'value' attribute on TextCell (id=groupName_Cell) at AdminDataUploadLocalizationLV.pcf: line 98, column 41
    function value_124 () : java.lang.String {
      return localization.GroupName
    }
    
    // 'value' attribute on TextCell (id=region_Cell) at AdminDataUploadLocalizationLV.pcf: line 103, column 41
    function value_129 () : java.lang.String {
      return localization.Region
    }
    
    // 'value' attribute on TextCell (id=regionName_Cell) at AdminDataUploadLocalizationLV.pcf: line 108, column 41
    function value_134 () : java.lang.String {
      return localization.RegionName
    }
    
    // 'value' attribute on TextCell (id=attribute_Cell) at AdminDataUploadLocalizationLV.pcf: line 113, column 41
    function value_139 () : java.lang.String {
      return localization.Attribute
    }
    
    // 'value' attribute on TextCell (id=attributeName_Cell) at AdminDataUploadLocalizationLV.pcf: line 118, column 41
    function value_144 () : java.lang.String {
      return localization.AttributeName
    }
    
    // 'value' attribute on TextCell (id=attributeDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 123, column 41
    function value_149 () : java.lang.String {
      return localization.AttributeDesc
    }
    
    // 'value' attribute on TextCell (id=holiday_Cell) at AdminDataUploadLocalizationLV.pcf: line 128, column 41
    function value_154 () : java.lang.String {
      return localization.Holiday
    }
    
    // 'value' attribute on TextCell (id=holidayName_Cell) at AdminDataUploadLocalizationLV.pcf: line 133, column 41
    function value_159 () : java.lang.String {
      return localization.HolidayName
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadLocalizationLV.pcf: line 23, column 46
    function value_48 () : java.lang.String {
      return processor.getLoadStatus(localization)
    }
    
    // 'value' attribute on TypeKeyCell (id=language_Cell) at AdminDataUploadLocalizationLV.pcf: line 28, column 37
    function value_53 () : LanguageType {
      return localization.Language
    }
    
    // 'value' attribute on TextCell (id=role_Cell) at AdminDataUploadLocalizationLV.pcf: line 33, column 41
    function value_59 () : java.lang.String {
      return localization.Role
    }
    
    // 'value' attribute on TextCell (id=roleName_Cell) at AdminDataUploadLocalizationLV.pcf: line 38, column 41
    function value_64 () : java.lang.String {
      return localization.RoleName
    }
    
    // 'value' attribute on TextCell (id=roleDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 43, column 41
    function value_69 () : java.lang.String {
      return localization.RoleDesc
    }
    
    // 'value' attribute on TextCell (id=activityPattern_Cell) at AdminDataUploadLocalizationLV.pcf: line 48, column 41
    function value_74 () : java.lang.String {
      return localization.ActivityPattern
    }
    
    // 'value' attribute on TextCell (id=activityPatternSubject_Cell) at AdminDataUploadLocalizationLV.pcf: line 53, column 41
    function value_79 () : java.lang.String {
      return localization.ActvPatternSubj
    }
    
    // 'value' attribute on TextCell (id=activityPatternDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 58, column 41
    function value_84 () : java.lang.String {
      return localization.ActvPatternDesc
    }
    
    // 'value' attribute on TextCell (id=secZone_Cell) at AdminDataUploadLocalizationLV.pcf: line 63, column 41
    function value_89 () : java.lang.String {
      return localization.SecZone
    }
    
    // 'value' attribute on TextCell (id=secZoneName_Cell) at AdminDataUploadLocalizationLV.pcf: line 68, column 41
    function value_94 () : java.lang.String {
      return localization.SecZoneName
    }
    
    // 'value' attribute on TextCell (id=secZoneDesc_Cell) at AdminDataUploadLocalizationLV.pcf: line 73, column 41
    function value_99 () : java.lang.String {
      return localization.SecZoneDesc
    }
    
    // 'valueType' attribute on TypeKeyCell (id=language_Cell) at AdminDataUploadLocalizationLV.pcf: line 28, column 37
    function verifyValueType_57 () : void {
      var __valueTypeArg : LanguageType
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : gw.entity.TypeKey = __valueTypeArg
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadLocalizationLV.pcf: line 23, column 46
    function visible_49 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get localization () : tdic.util.dataloader.data.admindata.AdminLocalizationData {
      return getIteratedValue(1) as tdic.util.dataloader.data.admindata.AdminLocalizationData
    }
    
    
  }
  
  
}