package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.CurrencyUtil
@javax.annotation.Generated("config/web/pcf/admin/payment/NewPaymentPlanWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewPaymentPlanWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/payment/NewPaymentPlanWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewPaymentPlanWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (currency :  Currency) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=NewPaymentPlanWizard) at NewPaymentPlanWizard.pcf: line 11, column 72
    function afterCancel_5 () : void {
      PaymentPlans.go()
    }
    
    // 'afterCancel' attribute on Wizard (id=NewPaymentPlanWizard) at NewPaymentPlanWizard.pcf: line 11, column 72
    function afterCancel_dest_6 () : pcf.api.Destination {
      return pcf.PaymentPlans.createDestination()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewPaymentPlanWizard) at NewPaymentPlanWizard.pcf: line 11, column 72
    function afterFinish_10 () : void {
      PaymentPlanDetail.go(paymentPlanHelper.PaymentPlan)
    }
    
    // 'afterFinish' attribute on Wizard (id=NewPaymentPlanWizard) at NewPaymentPlanWizard.pcf: line 11, column 72
    function afterFinish_dest_11 () : pcf.api.Destination {
      return pcf.PaymentPlanDetail.createDestination(paymentPlanHelper.PaymentPlan)
    }
    
    // 'canVisit' attribute on Wizard (id=NewPaymentPlanWizard) at NewPaymentPlanWizard.pcf: line 11, column 72
    static function canVisit_7 (currency :  Currency) : java.lang.Boolean {
      return perm.System.admintabview and perm.System.pmntplancreate
    }
    
    // 'initialValue' attribute on Variable at NewPaymentPlanWizard.pcf: line 20, column 58
    function initialValue_0 () : gw.admin.paymentplan.PaymentPlanViewHelper {
      return new gw.admin.paymentplan.PaymentPlanViewHelper(initPaymentPlan())
    }
    
    // 'screen' attribute on WizardStep (id=FirstStep) at NewPaymentPlanWizard.pcf: line 25, column 90
    function screen_onEnter_1 (def :  pcf.NewPaymentPlan_FirstStepScreen) : void {
      def.onEnter(paymentPlanHelper.PaymentPlan)
    }
    
    // 'screen' attribute on WizardStep (id=SecondStep) at NewPaymentPlanWizard.pcf: line 30, column 96
    function screen_onEnter_3 (def :  pcf.NewPaymentPlan_SecondStepScreen) : void {
      def.onEnter(paymentPlanHelper)
    }
    
    // 'screen' attribute on WizardStep (id=FirstStep) at NewPaymentPlanWizard.pcf: line 25, column 90
    function screen_refreshVariables_2 (def :  pcf.NewPaymentPlan_FirstStepScreen) : void {
      def.refreshVariables(paymentPlanHelper.PaymentPlan)
    }
    
    // 'screen' attribute on WizardStep (id=SecondStep) at NewPaymentPlanWizard.pcf: line 30, column 96
    function screen_refreshVariables_4 (def :  pcf.NewPaymentPlan_SecondStepScreen) : void {
      def.refreshVariables(paymentPlanHelper)
    }
    
    // 'tabBar' attribute on Wizard (id=NewPaymentPlanWizard) at NewPaymentPlanWizard.pcf: line 11, column 72
    function tabBar_onEnter_8 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=NewPaymentPlanWizard) at NewPaymentPlanWizard.pcf: line 11, column 72
    function tabBar_refreshVariables_9 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.NewPaymentPlanWizard {
      return super.CurrentLocation as pcf.NewPaymentPlanWizard
    }
    
    property get currency () : Currency {
      return getVariableValue("currency", 0) as Currency
    }
    
    property set currency ($arg :  Currency) {
      setVariableValue("currency", 0, $arg)
    }
    
    property get paymentPlanHelper () : gw.admin.paymentplan.PaymentPlanViewHelper {
      return getVariableValue("paymentPlanHelper", 0) as gw.admin.paymentplan.PaymentPlanViewHelper
    }
    
    property set paymentPlanHelper ($arg :  gw.admin.paymentplan.PaymentPlanViewHelper) {
      setVariableValue("paymentPlanHelper", 0, $arg)
    }
    
    
    function initPaymentPlan() : PaymentPlan {
      var newPaymentPlan = new PaymentPlan(CurrentLocation);
      if (not isMultiCurrencyMode()) {
        newPaymentPlan.addToCurrencies(currency)
      }
      newPaymentPlan.EffectiveDate = gw.api.util.DateUtil.currentDate();
      return newPaymentPlan;
    }
    
    function isMultiCurrencyMode(): boolean {
      return CurrencyUtil.isMultiCurrencyMode() and Currency.getTypeKeys(true).Count > 1
    }
    
    
  }
  
  
}