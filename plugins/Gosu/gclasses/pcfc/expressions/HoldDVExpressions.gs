package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/HoldDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class HoldDVExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/HoldDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class HoldDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=EntityDisplayName_Cell) at HoldDV.pcf: line 57, column 118
    function sortValue_13 (ticketRelatedEntity :  gw.pl.persistence.core.Bean) : java.lang.Object {
      return gw.api.web.troubleticket.TroubleTicketUtil.getRelatedEntityDisplayName(ticketRelatedEntity)
    }
    
    // 'value' attribute on InputIterator (id=HoldTypes) at HoldDV.pcf: line 21, column 54
    function value_12 () : gw.troubleticket.ApplyHoldType[] {
      return gw.troubleticket.ApplyHoldType.getRows(hold)
    }
    
    // 'value' attribute on RowIterator at HoldDV.pcf: line 52, column 55
    function value_16 () : gw.pl.persistence.core.Bean[] {
      return hold.TroubleTicket.TicketRelatedEntities
    }
    
    // 'visible' attribute on ListViewInput at HoldDV.pcf: line 43, column 39
    function visible_17 () : java.lang.Boolean {
      return ShowRelatedEntities
    }
    
    property get ShowRelatedEntities () : Boolean {
      return getRequireValue("ShowRelatedEntities", 0) as Boolean
    }
    
    property set ShowRelatedEntities ($arg :  Boolean) {
      setRequireValue("ShowRelatedEntities", 0, $arg)
    }
    
    property get hold () : Hold {
      return getRequireValue("hold", 0) as Hold
    }
    
    property set hold ($arg :  Hold) {
      setRequireValue("hold", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/troubleticket/HoldDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends HoldDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=EntityDisplayName_Cell) at HoldDV.pcf: line 57, column 118
    function value_14 () : java.lang.String {
      return gw.api.web.troubleticket.TroubleTicketUtil.getRelatedEntityDisplayName(ticketRelatedEntity)
    }
    
    property get ticketRelatedEntity () : gw.pl.persistence.core.Bean {
      return getIteratedValue(1) as gw.pl.persistence.core.Bean
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/troubleticket/HoldDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends HoldDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on CheckBoxInput (id=HoldType_Input) at HoldDV.pcf: line 27, column 39
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      holdTypeRow.Applied = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateInput (id=ReleaseDate_Input) at HoldDV.pcf: line 35, column 44
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      holdTypeRow.ReleaseDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'label' attribute on CheckBoxInput (id=HoldType_Input) at HoldDV.pcf: line 27, column 39
    function label_0 () : java.lang.Object {
      return holdTypeRow.HoldType
    }
    
    // 'value' attribute on CheckBoxInput (id=HoldType_Input) at HoldDV.pcf: line 27, column 39
    function valueRoot_3 () : java.lang.Object {
      return holdTypeRow
    }
    
    // 'value' attribute on CheckBoxInput (id=HoldType_Input) at HoldDV.pcf: line 27, column 39
    function value_1 () : java.lang.Boolean {
      return holdTypeRow.Applied
    }
    
    // 'value' attribute on DateInput (id=ReleaseDate_Input) at HoldDV.pcf: line 35, column 44
    function value_7 () : java.util.Date {
      return holdTypeRow.ReleaseDate
    }
    
    property get holdTypeRow () : gw.troubleticket.ApplyHoldType {
      return getIteratedValue(1) as gw.troubleticket.ApplyHoldType
    }
    
    
  }
  
  
}