package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalChargesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewChargeReversalChargesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalChargesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends NewChargeReversalChargesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=Select) at NewChargeReversalChargesLV.pcf: line 29, column 44
    function action_1 () : void {
      reversal.setChargeAndAddToBundle(charge); (CurrentLocation as pcf.api.Wizard).next();
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at NewChargeReversalChargesLV.pcf: line 53, column 34
    function currency_11 () : typekey.Currency {
      return charge.Currency
    }
    
    // 'value' attribute on DateCell (id=ChargeDate_Cell) at NewChargeReversalChargesLV.pcf: line 35, column 38
    function valueRoot_3 () : java.lang.Object {
      return charge
    }
    
    // 'value' attribute on DateCell (id=ChargeDate_Cell) at NewChargeReversalChargesLV.pcf: line 35, column 38
    function value_2 () : java.util.Date {
      return charge.ChargeDate
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at NewChargeReversalChargesLV.pcf: line 41, column 38
    function value_5 () : entity.Charge {
      return charge
    }
    
    // 'value' attribute on TextCell (id=Owner_Cell) at NewChargeReversalChargesLV.pcf: line 46, column 181
    function value_7 () : java.lang.String {
      return DisplayKey.get("Java.TAccountOwner.TAccountOwnerTypeAndDisplayName", charge.TAccountOwner.IntrinsicType.DisplayName, charge.TAccountOwner.DisplayName)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at NewChargeReversalChargesLV.pcf: line 53, column 34
    function value_9 () : gw.pl.currency.MonetaryAmount {
      return charge.Amount
    }
    
    // 'visible' attribute on Link (id=Select) at NewChargeReversalChargesLV.pcf: line 29, column 44
    function visible_0 () : java.lang.Boolean {
      return charge.canReverse()
    }
    
    property get charge () : Charge {
      return getIteratedValue(1) as Charge
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalChargesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewChargeReversalChargesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RowIterator (id=Charges) at NewChargeReversalChargesLV.pcf: line 19, column 66
    function value_13 () : gw.api.database.IQueryBeanResult<Charge> {
      return charges
    }
    
    property get charges () : gw.api.database.IQueryBeanResult<Charge> {
      return getRequireValue("charges", 0) as gw.api.database.IQueryBeanResult<Charge>
    }
    
    property set charges ($arg :  gw.api.database.IQueryBeanResult<Charge>) {
      setRequireValue("charges", 0, $arg)
    }
    
    property get reversal () : ChargeReversal {
      return getRequireValue("reversal", 0) as ChargeReversal
    }
    
    property set reversal ($arg :  ChargeReversal) {
      setRequireValue("reversal", 0, $arg)
    }
    
    
  }
  
  
}