package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyNewNoteWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyNewNoteWorksheetExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyNewNoteWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyNewNoteWorksheetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod, noteToEdit :  Note) : int {
      return 1
    }
    
    // 'action' attribute on PickerToolbarButton (id=linkDocsPickerButton) at PolicyNewNoteWorksheet.pcf: line 47, column 88
    function action_3 () : void {
      PickExistingDocumentPopup.push(policyPeriod.Policy)
    }
    
    // 'action' attribute on PickerToolbarButton (id=linkDocsPickerButton) at PolicyNewNoteWorksheet.pcf: line 47, column 88
    function action_dest_4 () : pcf.api.Destination {
      return pcf.PickExistingDocumentPopup.createDestination(policyPeriod.Policy)
    }
    
    // 'beforeCommit' attribute on Worksheet (id=PolicyNewNoteWorksheet) at PolicyNewNoteWorksheet.pcf: line 12, column 65
    function beforeCommit_8 (pickedValue :  java.lang.Object) : void {
      policyPeriod.Policy.addNote(newNote)
    }
    
    // 'canVisit' attribute on Worksheet (id=PolicyNewNoteWorksheet) at PolicyNewNoteWorksheet.pcf: line 12, column 65
    static function canVisit_9 (noteToEdit :  Note, policyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.Note.create
    }
    
    // 'def' attribute on PanelRef at PolicyNewNoteWorksheet.pcf: line 50, column 35
    function def_onEnter_6 (def :  pcf.NewNoteDV) : void {
      def.onEnter(newNote)
    }
    
    // 'def' attribute on PanelRef at PolicyNewNoteWorksheet.pcf: line 50, column 35
    function def_refreshVariables_7 (def :  pcf.NewNoteDV) : void {
      def.refreshVariables(newNote)
    }
    
    // 'initialValue' attribute on Variable at PolicyNewNoteWorksheet.pcf: line 28, column 20
    function initialValue_0 () : Note {
      if (noteToEdit != null) return noteToEdit else return gw.pcf.note.NoteHelper.createNoteWithCurrentUsersLanguage()
    }
    
    // 'initialValue' attribute on Variable at PolicyNewNoteWorksheet.pcf: line 32, column 51
    function initialValue_1 () : java.util.Map<String, Object> {
      return gw.api.util.SymbolTableUtil.populateBeans( policyPeriod )
    }
    
    // EditButtons at PolicyNewNoteWorksheet.pcf: line 36, column 23
    function label_2 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=linkDocsPickerButton) at PolicyNewNoteWorksheet.pcf: line 47, column 88
    function onPick_5 (PickedValue :  Document) : void {
      acc.onbase.util.NotesUtil.linkDocumentToNote(PickedValue, newNote)
    }
    
    override property get CurrentLocation () : pcf.PolicyNewNoteWorksheet {
      return super.CurrentLocation as pcf.PolicyNewNoteWorksheet
    }
    
    property get newNote () : Note {
      return getVariableValue("newNote", 0) as Note
    }
    
    property set newNote ($arg :  Note) {
      setVariableValue("newNote", 0, $arg)
    }
    
    property get noteToEdit () : Note {
      return getVariableValue("noteToEdit", 0) as Note
    }
    
    property set noteToEdit ($arg :  Note) {
      setVariableValue("noteToEdit", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get symbolTable () : java.util.Map<String, Object> {
      return getVariableValue("symbolTable", 0) as java.util.Map<String, Object>
    }
    
    property set symbolTable ($arg :  java.util.Map<String, Object>) {
      setVariableValue("symbolTable", 0, $arg)
    }
    
    function createSearchCriteria() : NoteTemplateSearchCriteria {
      var rtn = new NoteTemplateSearchCriteria()
      // rtn.Language = Account.AccountHolder.Language 
      rtn.AvailableSymbols = symbolTable.Keys.join( "," )
      var policy = symbolTable.get( "policy" ) as Policy
      if (policy.LOBCode != null) {
        rtn.LOB = policy.LOBCode
      }
      return rtn  
    }
    
    
  }
  
  
}