package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/NewPolicyContactPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewPolicyContactPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/NewPolicyContactPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewPolicyContactPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod, contactSubtype :  Type<Contact>) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=UpdateButtonThatForcesCheckForDuplicates) at NewPolicyContactPopup.pcf: line 36, column 83
    function action_3 () : void {
      duplicateContactsPopupNavigator.checkForDuplicatesOrUpdate(\ -> CurrentLocation.commit())
    }
    
    // 'action' attribute on ToolbarButton (id=CheckForDuplicates) at NewPolicyContactPopup.pcf: line 44, column 83
    function action_8 () : void {
      duplicateContactsPopupNavigator.pushFromCurrentLocationToDuplicateContactsPopup()
    }
    
    // 'beforeCommit' attribute on Popup (id=NewPolicyContactPopup) at NewPolicyContactPopup.pcf: line 12, column 68
    function beforeCommit_11 (pickedValue :  PolicyPeriodContact) : void {
      duplicateContactsPopupNavigator.beforeCommitOfContactCreationPage(\ c -> {policyPeriodContact.Contact = c})
    }
    
    // 'canVisit' attribute on Popup (id=NewPolicyContactPopup) at NewPolicyContactPopup.pcf: line 12, column 68
    static function canVisit_12 (contactSubtype :  Type<Contact>, policyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.PolicyPeriodContact.create
    }
    
    // 'def' attribute on PanelRef at NewPolicyContactPopup.pcf: line 47, column 65
    function def_onEnter_9 (def :  pcf.PolicyContactDetailDV) : void {
      def.onEnter(policyPeriodContact, true)
    }
    
    // 'def' attribute on PanelRef at NewPolicyContactPopup.pcf: line 47, column 65
    function def_refreshVariables_10 (def :  pcf.PolicyContactDetailDV) : void {
      def.refreshVariables(policyPeriodContact, true)
    }
    
    // 'initialValue' attribute on Variable at NewPolicyContactPopup.pcf: line 21, column 35
    function initialValue_0 () : PolicyPeriodContact {
      return initNewPolicyContact()
    }
    
    // 'initialValue' attribute on Variable at NewPolicyContactPopup.pcf: line 28, column 72
    function initialValue_1 () : gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator {
      return createDuplicateContactsPopupNavigator()
    }
    
    // EditButtons at NewPolicyContactPopup.pcf: line 39, column 93
    function label_6 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'pickValue' attribute on EditButtons at NewPolicyContactPopup.pcf: line 39, column 93
    function pickValue_4 () : PolicyPeriodContact {
      return policyPeriodContact
    }
    
    // 'visible' attribute on ToolbarButton (id=UpdateButtonThatForcesCheckForDuplicates) at NewPolicyContactPopup.pcf: line 36, column 83
    function visible_2 () : java.lang.Boolean {
      return duplicateContactsPopupNavigator.ShowCheckForDuplicatesButton
    }
    
    // 'updateVisible' attribute on EditButtons at NewPolicyContactPopup.pcf: line 39, column 93
    function visible_5 () : java.lang.Boolean {
      return not duplicateContactsPopupNavigator.ShowCheckForDuplicatesButton
    }
    
    override property get CurrentLocation () : pcf.NewPolicyContactPopup {
      return super.CurrentLocation as pcf.NewPolicyContactPopup
    }
    
    property get contactSubtype () : Type<Contact> {
      return getVariableValue("contactSubtype", 0) as Type<Contact>
    }
    
    property set contactSubtype ($arg :  Type<Contact>) {
      setVariableValue("contactSubtype", 0, $arg)
    }
    
    property get duplicateContactsPopupNavigator () : gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator {
      return getVariableValue("duplicateContactsPopupNavigator", 0) as gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator
    }
    
    property set duplicateContactsPopupNavigator ($arg :  gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator) {
      setVariableValue("duplicateContactsPopupNavigator", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get policyPeriodContact () : PolicyPeriodContact {
      return getVariableValue("policyPeriodContact", 0) as PolicyPeriodContact
    }
    
    property set policyPeriodContact ($arg :  PolicyPeriodContact) {
      setVariableValue("policyPeriodContact", 0, $arg)
    }
    
    function initNewPolicyContact() : PolicyPeriodContact {
      var newPolicyPeriodContact = new PolicyPeriodContact();
      var newContact = instantiateContact()
      newPolicyPeriodContact.Contact = newContact
      policyPeriod.addToContacts(newPolicyPeriodContact)
      return newPolicyPeriodContact;
    }
    
    private function instantiateContact() : Contact {
      // Instantiate the appropriate contact subtype (e.g. a Person or a Company)
      return contactSubtype.TypeInfo.getConstructor(null).Constructor.newInstance(null) as Contact  
    }
    
    function createDuplicateContactsPopupNavigator() : gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigator {
      var newContactBeingCreatedOnThisPage = policyPeriodContact.Contact
      var existingContactsOnThisAccount = policyPeriod.Contacts.map(\ ac -> ac.Contact).where(\ c -> c != newContactBeingCreatedOnThisPage)
      return new gw.pcf.duplicatecontacts.DuplicateContactsPopupNavigatorImpl(newContactBeingCreatedOnThisPage, existingContactsOnThisAccount, entity.PolicyPeriod)
    }
    
    
  }
  
  
}