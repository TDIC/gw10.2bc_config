package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.web.payment.MoveDirectBillPaymentView
@javax.annotation.Generated("config/web/pcf/payment/MoveDirectBillPayment.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class MoveDirectBillPaymentExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/MoveDirectBillPayment.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class MoveDirectBillPaymentExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account, originalMoney :  DirectBillMoneyRcvd) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Page (id=MoveDirectBillPayment) at MoveDirectBillPayment.pcf: line 12, column 73
    function beforeCommit_3 (pickedValue :  java.lang.Object) : void {
      paymentView.execute( CurrentLocation );
    }
    
    // 'canVisit' attribute on Page (id=MoveDirectBillPayment) at MoveDirectBillPayment.pcf: line 12, column 73
    static function canVisit_4 (account :  Account, originalMoney :  DirectBillMoneyRcvd) : java.lang.Boolean {
      return perm.DirectBillMoneyRcvd.pmntmanproc
    }
    
    // 'def' attribute on ScreenRef at MoveDirectBillPayment.pcf: line 26, column 54
    function def_onEnter_1 (def :  pcf.EditDBPaymentScreen) : void {
      def.onEnter(paymentView, null) 
    }
    
    // 'def' attribute on ScreenRef at MoveDirectBillPayment.pcf: line 26, column 54
    function def_refreshVariables_2 (def :  pcf.EditDBPaymentScreen) : void {
      def.refreshVariables(paymentView, null) 
    }
    
    // 'initialValue' attribute on Variable at MoveDirectBillPayment.pcf: line 24, column 60
    function initialValue_0 () : gw.api.web.payment.MoveDirectBillPaymentView {
      return createPaymentView()
    }
    
    // 'parent' attribute on Page (id=MoveDirectBillPayment) at MoveDirectBillPayment.pcf: line 12, column 73
    static function parent_5 (account :  Account, originalMoney :  DirectBillMoneyRcvd) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination( account )
    }
    
    override property get CurrentLocation () : pcf.MoveDirectBillPayment {
      return super.CurrentLocation as pcf.MoveDirectBillPayment
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get originalMoney () : DirectBillMoneyRcvd {
      return getVariableValue("originalMoney", 0) as DirectBillMoneyRcvd
    }
    
    property set originalMoney ($arg :  DirectBillMoneyRcvd) {
      setVariableValue("originalMoney", 0, $arg)
    }
    
    property get paymentView () : gw.api.web.payment.MoveDirectBillPaymentView {
      return getVariableValue("paymentView", 0) as gw.api.web.payment.MoveDirectBillPaymentView
    }
    
    property set paymentView ($arg :  gw.api.web.payment.MoveDirectBillPaymentView) {
      setVariableValue("paymentView", 0, $arg)
    }
    
    
    function createPaymentView() : MoveDirectBillPaymentView {
      var view = new gw.api.web.payment.MoveDirectBillPaymentView(originalMoney, DBPmntGroupingCriteria.TC_INVOICE, DBPmntAggregateCriteria.TC_ITEM)
      view.IncludeOnlyCriteria = account.DistributionLimitTypeFromPlan
      return view
    }
    
    
  }
  
  
}