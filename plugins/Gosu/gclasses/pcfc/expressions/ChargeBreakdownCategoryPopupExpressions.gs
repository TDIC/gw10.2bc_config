package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/ChargeBreakdownCategoryPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChargeBreakdownCategoryPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/ChargeBreakdownCategoryPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChargeBreakdownCategoryPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (chargeBreakdownItem :  ChargeBreakdownItem, view :  gw.web.policy.ChargeBreakdownItemsLvView) : int {
      return 0
    }
    
    // EditButtons at ChargeBreakdownCategoryPopup.pcf: line 21, column 23
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'value' attribute on RangeCell (id=CategoryType_Cell) at ChargeBreakdownCategoryPopup.pcf: line 48, column 30
    function sortValue_1 (breakdownCategory :  entity.ChargeBreakdownCategory) : java.lang.Object {
      return breakdownCategory.CategoryType
    }
    
    // 'value' attribute on TextCell (id=CategoryIdentifier_Cell) at ChargeBreakdownCategoryPopup.pcf: line 58, column 30
    function sortValue_2 (breakdownCategory :  entity.ChargeBreakdownCategory) : java.lang.Object {
      return breakdownCategory.CategoryIdentifier
    }
    
    // 'value' attribute on TextCell (id=CategoryName_Cell) at ChargeBreakdownCategoryPopup.pcf: line 68, column 29
    function sortValue_3 (breakdownCategory :  entity.ChargeBreakdownCategory) : java.lang.Object {
      return breakdownCategory.CategoryName
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=Categories) at ChargeBreakdownCategoryPopup.pcf: line 37, column 78
    function toCreateAndAdd_25 () : entity.ChargeBreakdownCategory {
      return view.createAndAddBreakdownCategory(chargeBreakdownItem)
    }
    
    // 'value' attribute on RowIterator (id=Categories) at ChargeBreakdownCategoryPopup.pcf: line 37, column 78
    function value_26 () : java.util.List<entity.ChargeBreakdownCategory> {
      return chargeBreakdownItem.Categories
    }
    
    override property get CurrentLocation () : pcf.ChargeBreakdownCategoryPopup {
      return super.CurrentLocation as pcf.ChargeBreakdownCategoryPopup
    }
    
    property get chargeBreakdownItem () : ChargeBreakdownItem {
      return getVariableValue("chargeBreakdownItem", 0) as ChargeBreakdownItem
    }
    
    property set chargeBreakdownItem ($arg :  ChargeBreakdownItem) {
      setVariableValue("chargeBreakdownItem", 0, $arg)
    }
    
    property get view () : gw.web.policy.ChargeBreakdownItemsLvView {
      return getVariableValue("view", 0) as gw.web.policy.ChargeBreakdownItemsLvView
    }
    
    property set view ($arg :  gw.web.policy.ChargeBreakdownItemsLvView) {
      setVariableValue("view", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/ChargeBreakdownCategoryPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ChargeBreakdownCategoryPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ButtonCell (id=Remove_Cell) at ChargeBreakdownCategoryPopup.pcf: line 76, column 41
    function action_24 () : void {
      view.removeBreakdownCategory(chargeBreakdownItem, breakdownCategory)
    }
    
    // 'value' attribute on TextCell (id=CategoryIdentifier_Cell) at ChargeBreakdownCategoryPopup.pcf: line 58, column 30
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      breakdownCategory.CategoryIdentifier = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=CategoryName_Cell) at ChargeBreakdownCategoryPopup.pcf: line 68, column 29
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      breakdownCategory.CategoryName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeCell (id=CategoryType_Cell) at ChargeBreakdownCategoryPopup.pcf: line 48, column 30
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      breakdownCategory.CategoryType = (__VALUE_TO_SET as ChargeBreakdownCategoryType)
    }
    
    // 'editable' attribute on TextCell (id=CategoryName_Cell) at ChargeBreakdownCategoryPopup.pcf: line 68, column 29
    function editable_18 () : java.lang.Boolean {
      return !view.isSavedCategory(breakdownCategory)
    }
    
    // 'onChange' attribute on PostOnChange at ChargeBreakdownCategoryPopup.pcf: line 60, column 72
    function onChange_12 () : void {
      view.onCategorySwitch(breakdownCategory)
    }
    
    // 'onChange' attribute on PostOnChange at ChargeBreakdownCategoryPopup.pcf: line 70, column 76
    function onChange_17 () : void {
      view.onCategoryNameChange(breakdownCategory)
    }
    
    // 'onChange' attribute on PostOnChange at ChargeBreakdownCategoryPopup.pcf: line 50, column 72
    function onChange_4 () : void {
      view.onCategorySwitch(breakdownCategory)
    }
    
    // 'valueRange' attribute on RangeCell (id=CategoryType_Cell) at ChargeBreakdownCategoryPopup.pcf: line 48, column 30
    function valueRange_8 () : java.lang.Object {
      return view.allCategoryTypes()
    }
    
    // 'value' attribute on RangeCell (id=CategoryType_Cell) at ChargeBreakdownCategoryPopup.pcf: line 48, column 30
    function valueRoot_7 () : java.lang.Object {
      return breakdownCategory
    }
    
    // 'value' attribute on TextCell (id=CategoryIdentifier_Cell) at ChargeBreakdownCategoryPopup.pcf: line 58, column 30
    function value_13 () : java.lang.String {
      return breakdownCategory.CategoryIdentifier
    }
    
    // 'value' attribute on TextCell (id=CategoryName_Cell) at ChargeBreakdownCategoryPopup.pcf: line 68, column 29
    function value_19 () : java.lang.String {
      return breakdownCategory.CategoryName
    }
    
    // 'value' attribute on RangeCell (id=CategoryType_Cell) at ChargeBreakdownCategoryPopup.pcf: line 48, column 30
    function value_5 () : ChargeBreakdownCategoryType {
      return breakdownCategory.CategoryType
    }
    
    // 'valueRange' attribute on RangeCell (id=CategoryType_Cell) at ChargeBreakdownCategoryPopup.pcf: line 48, column 30
    function verifyValueRangeIsAllowedType_9 ($$arg :  ChargeBreakdownCategoryType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=CategoryType_Cell) at ChargeBreakdownCategoryPopup.pcf: line 48, column 30
    function verifyValueRangeIsAllowedType_9 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=CategoryType_Cell) at ChargeBreakdownCategoryPopup.pcf: line 48, column 30
    function verifyValueRangeIsAllowedType_9 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=CategoryType_Cell) at ChargeBreakdownCategoryPopup.pcf: line 48, column 30
    function verifyValueRange_10 () : void {
      var __valueRangeArg = view.allCategoryTypes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_9(__valueRangeArg)
    }
    
    property get breakdownCategory () : entity.ChargeBreakdownCategory {
      return getIteratedValue(1) as entity.ChargeBreakdownCategory
    }
    
    
  }
  
  
}