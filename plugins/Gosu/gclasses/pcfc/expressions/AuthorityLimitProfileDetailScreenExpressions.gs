package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/authoritylimits/AuthorityLimitProfileDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AuthorityLimitProfileDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/authoritylimits/AuthorityLimitProfileDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AuthorityLimitProfileDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at AuthorityLimitProfileDetailScreen.pcf: line 15, column 61
    function def_onEnter_2 (def :  pcf.AuthorityLimitProfileDV) : void {
      def.onEnter(authorityLimitProfile)
    }
    
    // 'def' attribute on PanelRef at AuthorityLimitProfileDetailScreen.pcf: line 17, column 248
    function def_onEnter_4 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(authorityLimitProfile, { "Name", "Description"}, { DisplayKey.get("Web.Admin.AuthorityLimitProfileDV.Name"), DisplayKey.get("Web.Admin.AuthorityLimitProfileDV.Description") })
    }
    
    // 'def' attribute on PanelRef at AuthorityLimitProfileDetailScreen.pcf: line 15, column 61
    function def_refreshVariables_3 (def :  pcf.AuthorityLimitProfileDV) : void {
      def.refreshVariables(authorityLimitProfile)
    }
    
    // 'def' attribute on PanelRef at AuthorityLimitProfileDetailScreen.pcf: line 17, column 248
    function def_refreshVariables_5 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(authorityLimitProfile, { "Name", "Description"}, { DisplayKey.get("Web.Admin.AuthorityLimitProfileDV.Name"), DisplayKey.get("Web.Admin.AuthorityLimitProfileDV.Description") })
    }
    
    // EditButtons at AuthorityLimitProfileDetailScreen.pcf: line 12, column 21
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'visible' attribute on Toolbar at AuthorityLimitProfileDetailScreen.pcf: line 11, column 39
    function visible_1 () : java.lang.Boolean {
      return perm.System.alpmanage
    }
    
    property get authorityLimitProfile () : AuthorityLimitProfile {
      return getRequireValue("authorityLimitProfile", 0) as AuthorityLimitProfile
    }
    
    property set authorityLimitProfile ($arg :  AuthorityLimitProfile) {
      setRequireValue("authorityLimitProfile", 0, $arg)
    }
    
    
  }
  
  
}