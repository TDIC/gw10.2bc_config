package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyPlanDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyPlanDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at DelinquencyPlanDetailDV.pcf: line 65, column 59
    function def_onEnter_25 (def :  pcf.PlanMultiCurrencyInputSet) : void {
      def.onEnter(delinquencyPlan)
    }
    
    // 'def' attribute on InputSetRef at DelinquencyPlanDetailDV.pcf: line 85, column 79
    function def_onEnter_42 (def :  pcf.DelinquencyPlanThresholdHandlingInputSet_default) : void {
      def.onEnter(delinquencyPlan, planNotInUse)
    }
    
    // 'def' attribute on InputSetRef (id=LateFeeAmountDefaults) at DelinquencyPlanDetailDV.pcf: line 91, column 37
    function def_onEnter_45 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(delinquencyPlan, DelinquencyPlan#LateFeeAmountDefaults.PropertyInfo, DisplayKey.get("Java.DelinquencyPlan.Field.LateFeeAmount"), true, false, null)
    }
    
    // 'def' attribute on InputSetRef (id=ReinstatementFeeAmountDefaults) at DelinquencyPlanDetailDV.pcf: line 94, column 46
    function def_onEnter_47 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(delinquencyPlan, DelinquencyPlan#ReinstatementFeeAmountDefaults.PropertyInfo, DisplayKey.get("Java.DelinquencyPlan.Field.ReinstatementFeeAmount"), true, false, null)
    }
    
    // 'def' attribute on InputSetRef (id=WriteoffThresholdDefaults) at DelinquencyPlanDetailDV.pcf: line 101, column 41
    function def_onEnter_49 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(delinquencyPlan, DelinquencyPlan#WriteoffThresholdDefaults.PropertyInfo, DisplayKey.get("Java.DelinquencyPlan.Field.WriteoffThreshold"), true, false, writeoffThresholdValidationExp)
    }
    
    // 'def' attribute on InputSetRef (id=AcctEnterDelinquencyThresholdDefaults) at DelinquencyPlanDetailDV.pcf: line 104, column 53
    function def_onEnter_51 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(delinquencyPlan, DelinquencyPlan#AcctEnterDelinquencyThresholdDefaults.PropertyInfo, DisplayKey.get("Java.DelinquencyPlan.Field.AcctEnterDelinquencyThreshold"), true, false, acctEnterDelinquencyThresholdValidationExp)
    }
    
    // 'def' attribute on InputSetRef (id=PolEnterDelinquencyThresholdDefaults) at DelinquencyPlanDetailDV.pcf: line 107, column 52
    function def_onEnter_53 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(delinquencyPlan, DelinquencyPlan#PolEnterDelinquencyThresholdDefaults.PropertyInfo, DisplayKey.get("Java.DelinquencyPlan.Field.PolEnterDelinquencyThreshold"), true, false, polEnterDelinquencyThresholdValidationExp)
    }
    
    // 'def' attribute on InputSetRef (id=CancellationThresholdDefaults) at DelinquencyPlanDetailDV.pcf: line 110, column 45
    function def_onEnter_55 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(delinquencyPlan, DelinquencyPlan#CancellationThresholdDefaults.PropertyInfo, DisplayKey.get("Java.DelinquencyPlan.Field.CancellationThreshold"), true, false, cancellationThresholdValidationExp)
    }
    
    // 'def' attribute on InputSetRef (id=ExitDelinquencyThresholdDefaults) at DelinquencyPlanDetailDV.pcf: line 113, column 48
    function def_onEnter_57 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(delinquencyPlan, DelinquencyPlan#ExitDelinquencyThresholdDefaults.PropertyInfo, DisplayKey.get("Java.DelinquencyPlan.Field.ExitDelinquencyThreshold"), true, false, exitDelinquencyThresholdValidationExp)
    }
    
    // 'def' attribute on InputSetRef at DelinquencyPlanDetailDV.pcf: line 65, column 59
    function def_refreshVariables_26 (def :  pcf.PlanMultiCurrencyInputSet) : void {
      def.refreshVariables(delinquencyPlan)
    }
    
    // 'def' attribute on InputSetRef at DelinquencyPlanDetailDV.pcf: line 85, column 79
    function def_refreshVariables_43 (def :  pcf.DelinquencyPlanThresholdHandlingInputSet_default) : void {
      def.refreshVariables(delinquencyPlan, planNotInUse)
    }
    
    // 'def' attribute on InputSetRef (id=LateFeeAmountDefaults) at DelinquencyPlanDetailDV.pcf: line 91, column 37
    function def_refreshVariables_46 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(delinquencyPlan, DelinquencyPlan#LateFeeAmountDefaults.PropertyInfo, DisplayKey.get("Java.DelinquencyPlan.Field.LateFeeAmount"), true, false, null)
    }
    
    // 'def' attribute on InputSetRef (id=ReinstatementFeeAmountDefaults) at DelinquencyPlanDetailDV.pcf: line 94, column 46
    function def_refreshVariables_48 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(delinquencyPlan, DelinquencyPlan#ReinstatementFeeAmountDefaults.PropertyInfo, DisplayKey.get("Java.DelinquencyPlan.Field.ReinstatementFeeAmount"), true, false, null)
    }
    
    // 'def' attribute on InputSetRef (id=WriteoffThresholdDefaults) at DelinquencyPlanDetailDV.pcf: line 101, column 41
    function def_refreshVariables_50 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(delinquencyPlan, DelinquencyPlan#WriteoffThresholdDefaults.PropertyInfo, DisplayKey.get("Java.DelinquencyPlan.Field.WriteoffThreshold"), true, false, writeoffThresholdValidationExp)
    }
    
    // 'def' attribute on InputSetRef (id=AcctEnterDelinquencyThresholdDefaults) at DelinquencyPlanDetailDV.pcf: line 104, column 53
    function def_refreshVariables_52 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(delinquencyPlan, DelinquencyPlan#AcctEnterDelinquencyThresholdDefaults.PropertyInfo, DisplayKey.get("Java.DelinquencyPlan.Field.AcctEnterDelinquencyThreshold"), true, false, acctEnterDelinquencyThresholdValidationExp)
    }
    
    // 'def' attribute on InputSetRef (id=PolEnterDelinquencyThresholdDefaults) at DelinquencyPlanDetailDV.pcf: line 107, column 52
    function def_refreshVariables_54 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(delinquencyPlan, DelinquencyPlan#PolEnterDelinquencyThresholdDefaults.PropertyInfo, DisplayKey.get("Java.DelinquencyPlan.Field.PolEnterDelinquencyThreshold"), true, false, polEnterDelinquencyThresholdValidationExp)
    }
    
    // 'def' attribute on InputSetRef (id=CancellationThresholdDefaults) at DelinquencyPlanDetailDV.pcf: line 110, column 45
    function def_refreshVariables_56 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(delinquencyPlan, DelinquencyPlan#CancellationThresholdDefaults.PropertyInfo, DisplayKey.get("Java.DelinquencyPlan.Field.CancellationThreshold"), true, false, cancellationThresholdValidationExp)
    }
    
    // 'def' attribute on InputSetRef (id=ExitDelinquencyThresholdDefaults) at DelinquencyPlanDetailDV.pcf: line 113, column 48
    function def_refreshVariables_58 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(delinquencyPlan, DelinquencyPlan#ExitDelinquencyThresholdDefaults.PropertyInfo, DisplayKey.get("Java.DelinquencyPlan.Field.ExitDelinquencyThreshold"), true, false, exitDelinquencyThresholdValidationExp)
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at DelinquencyPlanDetailDV.pcf: line 57, column 48
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      delinquencyPlan.EffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at DelinquencyPlanDetailDV.pcf: line 63, column 49
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      delinquencyPlan.ExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeRadioInput (id=CancellationTarget_Input) at DelinquencyPlanDetailDV.pcf: line 75, column 49
    function defaultSetter_29 (__VALUE_TO_SET :  java.lang.Object) : void {
      delinquencyPlan.CancellationTarget = (__VALUE_TO_SET as typekey.CancellationTarget)
    }
    
    // 'value' attribute on BooleanRadioInput (id=HoldInvoicing_Input) at DelinquencyPlanDetailDV.pcf: line 80, column 62
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      delinquencyPlan.HoldInvoicingOnDlnqPolicies = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at DelinquencyPlanDetailDV.pcf: line 49, column 38
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      delinquencyPlan.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextInput (id=Name_Input) at DelinquencyPlanDetailDV.pcf: line 49, column 38
    function editable_7 () : java.lang.Boolean {
      return planNotInUse
    }
    
    // 'initialValue' attribute on Variable at DelinquencyPlanDetailDV.pcf: line 15, column 23
    function initialValue_0 () : boolean {
      return not delinquencyPlan.InUse
    }
    
    // 'initialValue' attribute on Variable at DelinquencyPlanDetailDV.pcf: line 19, column 23
    function initialValue_1 () : boolean {
      return gw.api.util.LocaleUtil.getAllLanguages().Count > 1
    }
    
    // 'initialValue' attribute on Variable at DelinquencyPlanDetailDV.pcf: line 23, column 39
    function initialValue_2 () : gw.lang.function.IBlock {
      return \plan : DelinquencyPlan, currency : Currency -> gw.api.web.delinquency.DelinquencyPlanUtil.validateCancellationThreshold(plan, currency)
    }
    
    // 'initialValue' attribute on Variable at DelinquencyPlanDetailDV.pcf: line 27, column 39
    function initialValue_3 () : gw.lang.function.IBlock {
      return \plan : DelinquencyPlan, currency : Currency -> gw.api.web.delinquency.DelinquencyPlanUtil.validateExitDelinquencyThreshold(plan, currency)
    }
    
    // 'initialValue' attribute on Variable at DelinquencyPlanDetailDV.pcf: line 31, column 39
    function initialValue_4 () : gw.lang.function.IBlock {
      return \plan : DelinquencyPlan, currency : Currency -> gw.api.web.delinquency.DelinquencyPlanUtil.validateWriteoffThreshold(plan, currency)
    }
    
    // 'initialValue' attribute on Variable at DelinquencyPlanDetailDV.pcf: line 35, column 39
    function initialValue_5 () : gw.lang.function.IBlock {
      return \plan : DelinquencyPlan, currency : Currency -> gw.api.web.delinquency.DelinquencyPlanUtil.validateAcctEnterDelinquencyThreshold(plan, currency)
    }
    
    // 'initialValue' attribute on Variable at DelinquencyPlanDetailDV.pcf: line 39, column 39
    function initialValue_6 () : gw.lang.function.IBlock {
      return \plan : DelinquencyPlan, currency : Currency -> gw.api.web.delinquency.DelinquencyPlanUtil.validatePolEnterDelinquencyThreshold(plan, currency)
    }
    
    // 'mode' attribute on InputSetRef at DelinquencyPlanDetailDV.pcf: line 85, column 79
    function mode_44 () : java.lang.Object {
      return gw.plugin.Plugins.get(gw.module.IFeesThresholds).getPCFMode()
    }
    
    // 'validationExpression' attribute on DateInput (id=ExpirationDate_Input) at DelinquencyPlanDetailDV.pcf: line 63, column 49
    function validationExpression_19 () : java.lang.Object {
      return delinquencyPlan.hasValidExpirationDate() ? null : DisplayKey.get("Web.Plan.ExpirationDate.ValidationError")
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=CancellationTarget_Input) at DelinquencyPlanDetailDV.pcf: line 75, column 49
    function valueRange_31 () : java.lang.Object {
      return typekey.CancellationTarget.getTypeKeys(false)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at DelinquencyPlanDetailDV.pcf: line 49, column 38
    function valueRoot_10 () : java.lang.Object {
      return delinquencyPlan
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at DelinquencyPlanDetailDV.pcf: line 57, column 48
    function value_14 () : java.util.Date {
      return delinquencyPlan.EffectiveDate
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at DelinquencyPlanDetailDV.pcf: line 63, column 49
    function value_20 () : java.util.Date {
      return delinquencyPlan.ExpirationDate
    }
    
    // 'value' attribute on RangeRadioInput (id=CancellationTarget_Input) at DelinquencyPlanDetailDV.pcf: line 75, column 49
    function value_28 () : typekey.CancellationTarget {
      return delinquencyPlan.CancellationTarget
    }
    
    // 'value' attribute on BooleanRadioInput (id=HoldInvoicing_Input) at DelinquencyPlanDetailDV.pcf: line 80, column 62
    function value_37 () : java.lang.Boolean {
      return delinquencyPlan.HoldInvoicingOnDlnqPolicies
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at DelinquencyPlanDetailDV.pcf: line 49, column 38
    function value_8 () : java.lang.String {
      return delinquencyPlan.Name
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=CancellationTarget_Input) at DelinquencyPlanDetailDV.pcf: line 75, column 49
    function verifyValueRangeIsAllowedType_32 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=CancellationTarget_Input) at DelinquencyPlanDetailDV.pcf: line 75, column 49
    function verifyValueRangeIsAllowedType_32 ($$arg :  typekey.CancellationTarget[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=CancellationTarget_Input) at DelinquencyPlanDetailDV.pcf: line 75, column 49
    function verifyValueRange_33 () : void {
      var __valueRangeArg = typekey.CancellationTarget.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_32(__valueRangeArg)
    }
    
    property get acctEnterDelinquencyThresholdValidationExp () : gw.lang.function.IBlock {
      return getVariableValue("acctEnterDelinquencyThresholdValidationExp", 0) as gw.lang.function.IBlock
    }
    
    property set acctEnterDelinquencyThresholdValidationExp ($arg :  gw.lang.function.IBlock) {
      setVariableValue("acctEnterDelinquencyThresholdValidationExp", 0, $arg)
    }
    
    property get cancellationThresholdValidationExp () : gw.lang.function.IBlock {
      return getVariableValue("cancellationThresholdValidationExp", 0) as gw.lang.function.IBlock
    }
    
    property set cancellationThresholdValidationExp ($arg :  gw.lang.function.IBlock) {
      setVariableValue("cancellationThresholdValidationExp", 0, $arg)
    }
    
    property get delinquencyPlan () : DelinquencyPlan {
      return getRequireValue("delinquencyPlan", 0) as DelinquencyPlan
    }
    
    property set delinquencyPlan ($arg :  DelinquencyPlan) {
      setRequireValue("delinquencyPlan", 0, $arg)
    }
    
    property get exitDelinquencyThresholdValidationExp () : gw.lang.function.IBlock {
      return getVariableValue("exitDelinquencyThresholdValidationExp", 0) as gw.lang.function.IBlock
    }
    
    property set exitDelinquencyThresholdValidationExp ($arg :  gw.lang.function.IBlock) {
      setVariableValue("exitDelinquencyThresholdValidationExp", 0, $arg)
    }
    
    property get hasMultipleLanguages () : boolean {
      return getVariableValue("hasMultipleLanguages", 0) as java.lang.Boolean
    }
    
    property set hasMultipleLanguages ($arg :  boolean) {
      setVariableValue("hasMultipleLanguages", 0, $arg)
    }
    
    property get planNotInUse () : boolean {
      return getVariableValue("planNotInUse", 0) as java.lang.Boolean
    }
    
    property set planNotInUse ($arg :  boolean) {
      setVariableValue("planNotInUse", 0, $arg)
    }
    
    property get polEnterDelinquencyThresholdValidationExp () : gw.lang.function.IBlock {
      return getVariableValue("polEnterDelinquencyThresholdValidationExp", 0) as gw.lang.function.IBlock
    }
    
    property set polEnterDelinquencyThresholdValidationExp ($arg :  gw.lang.function.IBlock) {
      setVariableValue("polEnterDelinquencyThresholdValidationExp", 0, $arg)
    }
    
    property get writeoffThresholdValidationExp () : gw.lang.function.IBlock {
      return getVariableValue("writeoffThresholdValidationExp", 0) as gw.lang.function.IBlock
    }
    
    property set writeoffThresholdValidationExp ($arg :  gw.lang.function.IBlock) {
      setVariableValue("writeoffThresholdValidationExp", 0, $arg)
    }
    
    
  }
  
  
}