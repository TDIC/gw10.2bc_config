package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentAdvanceWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewCommissionPaymentAdvanceWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentAdvanceWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewCommissionPaymentAdvanceWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Wizard (id=NewCommissionPaymentAdvanceWizard) at NewCommissionPaymentAdvanceWizard.pcf: line 9, column 44
    function beforeCommit_6 (pickedValue :  java.lang.Object) : void {
      gw.api.web.producer.ProducerUtil.schedule(advancePayment)
    }
    
    // 'initialValue' attribute on Variable at NewCommissionPaymentAdvanceWizard.pcf: line 18, column 41
    function initialValue_0 () : entity.AdvanceCmsnPayment {
      return newAdvancePayment()
    }
    
    // 'onExit' attribute on WizardStep (id=DetailsStep) at NewCommissionPaymentAdvanceWizard.pcf: line 24, column 96
    function onExit_1 () : void {
      advancePayment.initiateApprovalActivityIfUserLacksAuthority()
    }
    
    // 'screen' attribute on WizardStep (id=DetailsStep) at NewCommissionPaymentAdvanceWizard.pcf: line 24, column 96
    function screen_onEnter_2 (def :  pcf.NewCommissionPaymentAdvanceDetailsScreen) : void {
      def.onEnter(advancePayment)
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at NewCommissionPaymentAdvanceWizard.pcf: line 30, column 101
    function screen_onEnter_4 (def :  pcf.NewCommissionPaymentAdvanceConfirmationScreen) : void {
      def.onEnter(advancePayment)
    }
    
    // 'screen' attribute on WizardStep (id=DetailsStep) at NewCommissionPaymentAdvanceWizard.pcf: line 24, column 96
    function screen_refreshVariables_3 (def :  pcf.NewCommissionPaymentAdvanceDetailsScreen) : void {
      def.refreshVariables(advancePayment)
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at NewCommissionPaymentAdvanceWizard.pcf: line 30, column 101
    function screen_refreshVariables_5 (def :  pcf.NewCommissionPaymentAdvanceConfirmationScreen) : void {
      def.refreshVariables(advancePayment)
    }
    
    // 'tabBar' attribute on Wizard (id=NewCommissionPaymentAdvanceWizard) at NewCommissionPaymentAdvanceWizard.pcf: line 9, column 44
    function tabBar_onEnter_7 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=NewCommissionPaymentAdvanceWizard) at NewCommissionPaymentAdvanceWizard.pcf: line 9, column 44
    function tabBar_refreshVariables_8 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.NewCommissionPaymentAdvanceWizard {
      return super.CurrentLocation as pcf.NewCommissionPaymentAdvanceWizard
    }
    
    property get advancePayment () : entity.AdvanceCmsnPayment {
      return getVariableValue("advancePayment", 0) as entity.AdvanceCmsnPayment
    }
    
    property set advancePayment ($arg :  entity.AdvanceCmsnPayment) {
      setVariableValue("advancePayment", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    function newAdvancePayment() : AdvanceCmsnPayment {
            var acp = new AdvanceCmsnPayment(producer.Currency)
            acp.Producer = producer;
            acp.PaymentTime = TC_IMMEDIATELY;
            return acp;
          }
    
    
  }
  
  
}