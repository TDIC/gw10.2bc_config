package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/HoldDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class HoldDetailsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/HoldDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class HoldDetailsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (hold :  Hold) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=HoldDetailsPopup) at HoldDetailsPopup.pcf: line 10, column 63
    function beforeCommit_4 (pickedValue :  java.lang.Object) : void {
      hold.checkForHoldReleases(); hold.checkForHoldAdditions()
    }
    
    // 'def' attribute on PanelRef at HoldDetailsPopup.pcf: line 26, column 35
    function def_onEnter_2 (def :  pcf.HoldDV) : void {
      def.onEnter(hold, true)
    }
    
    // 'def' attribute on PanelRef at HoldDetailsPopup.pcf: line 26, column 35
    function def_refreshVariables_3 (def :  pcf.HoldDV) : void {
      def.refreshVariables(hold, true)
    }
    
    // EditButtons at HoldDetailsPopup.pcf: line 19, column 23
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'visible' attribute on AlertBar (id=HoldDetailsPopup_NoRelatedEntitiesAlertBar) at HoldDetailsPopup.pcf: line 24, column 61
    function visible_1 () : java.lang.Boolean {
      return !hold.TroubleTicket.hasRelatedEntities()
    }
    
    override property get CurrentLocation () : pcf.HoldDetailsPopup {
      return super.CurrentLocation as pcf.HoldDetailsPopup
    }
    
    property get hold () : Hold {
      return getVariableValue("hold", 0) as Hold
    }
    
    property set hold ($arg :  Hold) {
      setVariableValue("hold", 0, $arg)
    }
    
    
  }
  
  
}