package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/NewDocumentFromTemplateWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewDocumentFromTemplateWorksheetExpressions {
  @javax.annotation.Generated("config/web/pcf/document/NewDocumentFromTemplateWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewDocumentFromTemplateWorksheetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (documentContainer :  DocumentContainer) : int {
      return 0
    }
    
    static function __constructorIndex (documentContainer :  DocumentContainer, templateName :  String) : int {
      return 1
    }
    
    // 'beforeCommit' attribute on Worksheet (id=NewDocumentFromTemplateWorksheet) at NewDocumentFromTemplateWorksheet.pcf: line 12, column 90
    function beforeCommit_4 (pickedValue :  java.lang.Object) : void {
      gw.document.DocumentContainerMethods.addHistoryIfDCIsAccount(documentContainer)
    }
    
    // 'def' attribute on ScreenRef at NewDocumentFromTemplateWorksheet.pcf: line 35, column 85
    function def_onEnter_2 (def :  pcf.NewDocumentFromTemplateScreen) : void {
      def.onEnter(documentCreationInfo, documentContainer)
    }
    
    // 'def' attribute on ScreenRef at NewDocumentFromTemplateWorksheet.pcf: line 35, column 85
    function def_refreshVariables_3 (def :  pcf.NewDocumentFromTemplateScreen) : void {
      def.refreshVariables(documentCreationInfo, documentContainer)
    }
    
    // 'initialValue' attribute on Variable at NewDocumentFromTemplateWorksheet.pcf: line 28, column 48
    function initialValue_0 () : gw.document.DocumentCreationInfo {
      return gw.api.web.document.DocumentsHelper.createDocumentCreationInfo(documentContainer, templateName, currentLanguage)
    }
    
    // 'initialValue' attribute on Variable at NewDocumentFromTemplateWorksheet.pcf: line 33, column 31
    function initialValue_1 () : gw.i18n.ILocale {
      return gw.api.util.LocaleUtil.toLanguage(gw.api.util.LocaleUtil.CurrentLanguageType)
    }
    
    override property get CurrentLocation () : pcf.NewDocumentFromTemplateWorksheet {
      return super.CurrentLocation as pcf.NewDocumentFromTemplateWorksheet
    }
    
    property get currentLanguage () : gw.i18n.ILocale {
      return getVariableValue("currentLanguage", 0) as gw.i18n.ILocale
    }
    
    property set currentLanguage ($arg :  gw.i18n.ILocale) {
      setVariableValue("currentLanguage", 0, $arg)
    }
    
    property get documentContainer () : DocumentContainer {
      return getVariableValue("documentContainer", 0) as DocumentContainer
    }
    
    property set documentContainer ($arg :  DocumentContainer) {
      setVariableValue("documentContainer", 0, $arg)
    }
    
    property get documentCreationInfo () : gw.document.DocumentCreationInfo {
      return getVariableValue("documentCreationInfo", 0) as gw.document.DocumentCreationInfo
    }
    
    property set documentCreationInfo ($arg :  gw.document.DocumentCreationInfo) {
      setVariableValue("documentCreationInfo", 0, $arg)
    }
    
    property get templateName () : String {
      return getVariableValue("templateName", 0) as String
    }
    
    property set templateName ($arg :  String) {
      setVariableValue("templateName", 0, $arg)
    }
    
    
  }
  
  
}