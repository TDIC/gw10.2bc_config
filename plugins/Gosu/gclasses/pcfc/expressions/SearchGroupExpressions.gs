package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/SearchGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SearchGroupExpressions {
  @javax.annotation.Generated("config/web/pcf/search/SearchGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchGroupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 15, column 35
    function action_0 () : void {
      pcf.AccountSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 30, column 36
    function action_10 () : void {
      pcf.ProducerSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 33, column 39
    function action_12 () : void {
      pcf.TransactionSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 36, column 36
    function action_14 () : void {
      pcf.ActivitySearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 39, column 41
    function action_16 () : void {
      pcf.TroubleTicketSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 42, column 46
    function action_18 () : void {
      pcf.DelinquencyProcessSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 18, column 34
    function action_2 () : void {
      pcf.PolicySearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 45, column 40
    function action_20 () : void {
      pcf.DisbursementSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 48, column 51
    function action_22 () : void {
      pcf.OutgoingProducerPaymentSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 51, column 42
    function action_24 () : void {
      pcf.PaymentRequestSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 54, column 50
    function action_26 () : void {
      pcf.DirectBillSuspenseItemSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 21, column 35
    function action_4 () : void {
      pcf.ContactSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 24, column 35
    function action_6 () : void {
      pcf.InvoiceSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 27, column 35
    function action_8 () : void {
      pcf.PaymentSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 15, column 35
    function action_dest_1 () : pcf.api.Destination {
      return pcf.AccountSearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 30, column 36
    function action_dest_11 () : pcf.api.Destination {
      return pcf.ProducerSearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 33, column 39
    function action_dest_13 () : pcf.api.Destination {
      return pcf.TransactionSearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 36, column 36
    function action_dest_15 () : pcf.api.Destination {
      return pcf.ActivitySearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 39, column 41
    function action_dest_17 () : pcf.api.Destination {
      return pcf.TroubleTicketSearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 42, column 46
    function action_dest_19 () : pcf.api.Destination {
      return pcf.DelinquencyProcessSearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 45, column 40
    function action_dest_21 () : pcf.api.Destination {
      return pcf.DisbursementSearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 48, column 51
    function action_dest_23 () : pcf.api.Destination {
      return pcf.OutgoingProducerPaymentSearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 51, column 42
    function action_dest_25 () : pcf.api.Destination {
      return pcf.PaymentRequestSearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 54, column 50
    function action_dest_27 () : pcf.api.Destination {
      return pcf.DirectBillSuspenseItemSearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 18, column 34
    function action_dest_3 () : pcf.api.Destination {
      return pcf.PolicySearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 21, column 35
    function action_dest_5 () : pcf.api.Destination {
      return pcf.ContactSearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 24, column 35
    function action_dest_7 () : pcf.api.Destination {
      return pcf.InvoiceSearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 27, column 35
    function action_dest_9 () : pcf.api.Destination {
      return pcf.PaymentSearch.createDestination()
    }
    
    // 'canVisit' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 10, column 26
    static function canVisit_28 () : java.lang.Boolean {
      return perm.System.viewsearch
    }
    
    // LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 10, column 26
    static function firstVisitableChildDestinationMethod_33 () : pcf.api.Destination {
      var dest : pcf.api.Destination
      dest = pcf.AccountSearch.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PolicySearch.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ContactSearch.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.InvoiceSearch.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PaymentSearch.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ProducerSearch.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.TransactionSearch.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ActivitySearch.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.TroubleTicketSearch.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.DelinquencyProcessSearch.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.DisbursementSearch.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.OutgoingProducerPaymentSearch.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PaymentRequestSearch.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.DirectBillSuspenseItemSearch.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      return null
    }
    
    // 'infoBar' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 10, column 26
    function infoBar_onEnter_29 (def :  pcf.CurrentDateInfoBar) : void {
      def.onEnter()
    }
    
    // 'infoBar' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 10, column 26
    function infoBar_refreshVariables_30 (def :  pcf.CurrentDateInfoBar) : void {
      def.refreshVariables()
    }
    
    // 'tabBar' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 10, column 26
    function tabBar_onEnter_31 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on LocationGroup (id=SearchGroup) at SearchGroup.pcf: line 10, column 26
    function tabBar_refreshVariables_32 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.SearchGroup {
      return super.CurrentLocation as pcf.SearchGroup
    }
    
    
  }
  
  
}