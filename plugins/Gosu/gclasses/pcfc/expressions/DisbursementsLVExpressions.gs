package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/DisbursementsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DisbursementsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/disbursement/DisbursementsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DisbursementsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=Approve) at DisbursementsLV.pcf: line 122, column 83
    function action_51 () : void {
      selectedDisbursement.getOpenApprovalActivity().approve(); selectedDisbursement.setDueDate(gw.api.util.DateUtil.currentDate());CurrentLocation.commit();
    }
    
    // 'action' attribute on ToolbarButton (id=Reject) at DisbursementsLV.pcf: line 129, column 82
    function action_53 () : void {
      selectedDisbursement.rejectDisbursement(); CurrentLocation.commit();
    }
    
    // 'action' attribute on ToolbarButton (id=RejectWithHold) at DisbursementsLV.pcf: line 136, column 90
    function action_55 () : void {
      selectedDisbursement.rejectDisbursementAndHold(); CurrentLocation.commit();
    }
    
    // 'action' attribute on ToolbarButton (id=Void) at DisbursementsLV.pcf: line 143, column 80
    function action_57 () : void {
      VoidDisbursementPopup.push(selectedDisbursement)
    }
    
    // 'action' attribute on ToolbarButton (id=Void) at DisbursementsLV.pcf: line 143, column 80
    function action_dest_58 () : pcf.api.Destination {
      return pcf.VoidDisbursementPopup.createDestination(selectedDisbursement)
    }
    
    // 'available' attribute on ToolbarButton (id=Approve) at DisbursementsLV.pcf: line 122, column 83
    function available_50 () : java.lang.Boolean {
      return selectedDisbursement.canApprove()
    }
    
    // 'available' attribute on ToolbarButton (id=Reject) at DisbursementsLV.pcf: line 129, column 82
    function available_52 () : java.lang.Boolean {
      return selectedDisbursement.canReject()
    }
    
    // 'available' attribute on ToolbarButton (id=RejectWithHold) at DisbursementsLV.pcf: line 136, column 90
    function available_54 () : java.lang.Boolean {
      return selectedDisbursement.canRejectAndHold()
    }
    
    // 'available' attribute on ToolbarButton (id=Void) at DisbursementsLV.pcf: line 143, column 80
    function available_56 () : java.lang.Boolean {
      return selectedDisbursement.canVoid()
    }
    
    // 'def' attribute on PanelRef at DisbursementsLV.pcf: line 112, column 60
    function def_onEnter_60 (def :  pcf.DisbursementDetailDV) : void {
      def.onEnter(selectedDisbursement)
    }
    
    // 'def' attribute on PanelRef at DisbursementsLV.pcf: line 112, column 60
    function def_refreshVariables_61 (def :  pcf.DisbursementDetailDV) : void {
      def.refreshVariables(selectedDisbursement)
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at DisbursementsLV.pcf: line 26, column 100
    function filters_0 () : gw.api.filters.IFilter[] {
      return new gw.api.web.disbursement.DisbursementsCloseDateFilterSet().FilterOptions
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at DisbursementsLV.pcf: line 31, column 97
    function filters_1 () : gw.api.filters.IFilter[] {
      return new gw.api.web.disbursement.DisbursementsStatusFilterSet().FilterOptions
    }
    
    // EditButtons at DisbursementsLV.pcf: line 115, column 27
    function label_49 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'selectionOnEnter' attribute on ListDetailPanel (id=DisbursementsLV) at DisbursementsLV.pcf: line 9, column 34
    function selectionOnEnter_62 () : java.lang.Object {
      return initialSelectedDisbursement
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentMethod_Cell) at DisbursementsLV.pcf: line 95, column 48
    function sortValue_10 (disbursement :  Disbursement) : java.lang.Object {
      return disbursement.OutgoingPayment.PaymentInstrument.PaymentMethod
    }
    
    // 'value' attribute on TextCell (id=RefNumber_Cell) at DisbursementsLV.pcf: line 99, column 61
    function sortValue_11 (disbursement :  Disbursement) : java.lang.Object {
      return disbursement.OutgoingPayment.RefNumber
    }
    
    // 'value' attribute on TextCell (id=assignedUser_Cell) at DisbursementsLV.pcf: line 103, column 54
    function sortValue_12 (disbursement :  Disbursement) : java.lang.Object {
      return getAssignedUser( disbursement )
    }
    
    // 'sortBy' attribute on DateCell (id=DateIssued_Cell) at DisbursementsLV.pcf: line 39, column 51
    function sortValue_2 (disbursement :  Disbursement) : java.lang.Object {
      return disbursement.CloseDate
    }
    
    // 'sortBy' attribute on DateCell (id=DateRejected_Cell) at DisbursementsLV.pcf: line 44, column 53
    function sortValue_3 (disbursement :  Disbursement) : java.lang.Object {
      return disbursement.CloseDate
    }
    
    // 'value' attribute on TextCell (id=DisbursementNumber_Cell) at DisbursementsLV.pcf: line 48, column 54
    function sortValue_4 (disbursement :  Disbursement) : java.lang.Object {
      return disbursement.DisbursementNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=DisbursementStatus_Cell) at DisbursementsLV.pcf: line 71, column 53
    function sortValue_5 (disbursement :  Disbursement) : java.lang.Object {
      return disbursement.Status
    }
    
    // 'value' attribute on TypeKeyCell (id=OutgoingPaymentStatus_Cell) at DisbursementsLV.pcf: line 76, column 56
    function sortValue_6 (disbursement :  Disbursement) : java.lang.Object {
      return disbursement.OutgoingPayment.Status
    }
    
    // 'value' attribute on DateCell (id=StatusUpdateDate_Cell) at DisbursementsLV.pcf: line 81, column 29
    function sortValue_7 (disbursement :  Disbursement) : java.lang.Object {
      return disbursement.OutgoingPayment.StatusUpdateDate_TDIC
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at DisbursementsLV.pcf: line 86, column 42
    function sortValue_8 (disbursement :  Disbursement) : java.lang.Object {
      return disbursement.Amount
    }
    
    // 'value' attribute on TextCell (id=PayTo_Cell) at DisbursementsLV.pcf: line 90, column 41
    function sortValue_9 (disbursement :  Disbursement) : java.lang.Object {
      return disbursement.PayTo
    }
    
    // 'value' attribute on RowIterator at DisbursementsLV.pcf: line 22, column 40
    function value_48 () : java.lang.Object[] {
      return disbursements.toArray()
    }
    
    // 'visible' attribute on Toolbar (id=DisbursementDetailDV_tb) at DisbursementsLV.pcf: line 114, column 230
    function visible_59 () : java.lang.Boolean {
      return selectedDisbursement.Status != DisbursementStatus.TC_REAPPLIED && selectedDisbursement.Status != DisbursementStatus.TC_REJECTED && selectedDisbursement.Status != DisbursementStatus.TC_VOIDED
    }
    
    property get disbursements () : List<Disbursement> {
      return getRequireValue("disbursements", 0) as List<Disbursement>
    }
    
    property set disbursements ($arg :  List<Disbursement>) {
      setRequireValue("disbursements", 0, $arg)
    }
    
    property get initialSelectedDisbursement () : Disbursement {
      return getRequireValue("initialSelectedDisbursement", 0) as Disbursement
    }
    
    property set initialSelectedDisbursement ($arg :  Disbursement) {
      setRequireValue("initialSelectedDisbursement", 0, $arg)
    }
    
    property get selectedDisbursement () : Disbursement {
      return getCurrentSelection(0) as Disbursement
    }
    
    property set selectedDisbursement ($arg :  Disbursement) {
      setCurrentSelection(0, $arg)
    }
    
    function getAssignedUser(disbursementVar : Disbursement) : String {
      if (disbursementVar.OpenApprovalActivity != null) {
        return disbursementVar.OpenApprovalActivity.AssignedGroup.DisplayName
      }
      else if (disbursementVar.Status != DisbursementStatus.TC_AWAITINGAPPROVAL and disbursementVar.Approver_TDIC != null) {
        return disbursementVar.Approver_TDIC.DisplayName
      }
      else if (disbursementVar.Status != DisbursementStatus.TC_AWAITINGAPPROVAL ) {
        return getApproverName(disbursementVar)
      }
      else{
        return null
      }
    
    }
    
    function getApproverName(disbursement: entity.Disbursement): String {
    
      var activitDetails=gw.api.database.Query.make(DisbApprActivity).select()
      var approver:String
      activitDetails.each(\elt -> {
        var disNumner=elt.Disbursement?.DisbursementNumber
        if(disNumner!=null and disNumner==disbursement.DisbursementNumber){
          approver=activitDetails.where(\elt1 -> elt1.Disbursement.DisbursementNumber==disNumner)?.first()?.Approver_TDIC.DisplayName
        }
      })
      return approver
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/disbursement/DisbursementsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DisbursementsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at DisbursementsLV.pcf: line 86, column 42
    function currency_35 () : typekey.Currency {
      return disbursement.Currency
    }
    
    // 'value' attribute on TextCell (id=DisbursementNumber_Cell) at DisbursementsLV.pcf: line 48, column 54
    function valueRoot_18 () : java.lang.Object {
      return disbursement
    }
    
    // 'value' attribute on TypeKeyCell (id=OutgoingPaymentStatus_Cell) at DisbursementsLV.pcf: line 76, column 56
    function valueRoot_28 () : java.lang.Object {
      return disbursement.OutgoingPayment
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentMethod_Cell) at DisbursementsLV.pcf: line 95, column 48
    function valueRoot_41 () : java.lang.Object {
      return disbursement.OutgoingPayment.PaymentInstrument
    }
    
    // 'value' attribute on DateCell (id=DateIssued_Cell) at DisbursementsLV.pcf: line 39, column 51
    function value_13 () : java.util.Date {
      return disbursement.getDateIssued()
    }
    
    // 'value' attribute on DateCell (id=DateRejected_Cell) at DisbursementsLV.pcf: line 44, column 53
    function value_15 () : java.util.Date {
      return disbursement.getDateRejected()
    }
    
    // 'value' attribute on TextCell (id=DisbursementNumber_Cell) at DisbursementsLV.pcf: line 48, column 54
    function value_17 () : java.lang.String {
      return disbursement.DisbursementNumber
    }
    
    // 'value' attribute on TextCell (id=UnappliedFund_Cell) at DisbursementsLV.pcf: line 60, column 47
    function value_20 () : entity.UnappliedFund {
      return disbursement typeis AccountDisbursement ? disbursement.UnappliedFund : null
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at DisbursementsLV.pcf: line 66, column 47
    function value_22 () : typekey.Jurisdiction {
      return disbursement typeis AccountDisbursement && disbursement.UnappliedFund.Policy?.LatestPolicyPeriod != null ? disbursement.UnappliedFund.Policy.LatestPolicyPeriod.RiskJurisdiction : null
    }
    
    // 'value' attribute on TypeKeyCell (id=DisbursementStatus_Cell) at DisbursementsLV.pcf: line 71, column 53
    function value_24 () : typekey.DisbursementStatus {
      return disbursement.Status
    }
    
    // 'value' attribute on TypeKeyCell (id=OutgoingPaymentStatus_Cell) at DisbursementsLV.pcf: line 76, column 56
    function value_27 () : typekey.OutgoingPaymentStatus {
      return disbursement.OutgoingPayment.Status
    }
    
    // 'value' attribute on DateCell (id=StatusUpdateDate_Cell) at DisbursementsLV.pcf: line 81, column 29
    function value_30 () : java.util.Date {
      return disbursement.OutgoingPayment.StatusUpdateDate_TDIC
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at DisbursementsLV.pcf: line 86, column 42
    function value_33 () : gw.pl.currency.MonetaryAmount {
      return disbursement.Amount
    }
    
    // 'value' attribute on TextCell (id=PayTo_Cell) at DisbursementsLV.pcf: line 90, column 41
    function value_37 () : java.lang.String {
      return disbursement.PayTo
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentMethod_Cell) at DisbursementsLV.pcf: line 95, column 48
    function value_40 () : typekey.PaymentMethod {
      return disbursement.OutgoingPayment.PaymentInstrument.PaymentMethod
    }
    
    // 'value' attribute on TextCell (id=RefNumber_Cell) at DisbursementsLV.pcf: line 99, column 61
    function value_43 () : java.lang.String {
      return disbursement.OutgoingPayment.RefNumber
    }
    
    // 'value' attribute on TextCell (id=assignedUser_Cell) at DisbursementsLV.pcf: line 103, column 54
    function value_46 () : java.lang.String {
      return getAssignedUser( disbursement )
    }
    
    property get disbursement () : Disbursement {
      return getIteratedValue(1) as Disbursement
    }
    
    
  }
  
  
}