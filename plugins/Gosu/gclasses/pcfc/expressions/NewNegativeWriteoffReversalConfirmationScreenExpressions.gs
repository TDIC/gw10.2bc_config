package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffReversalConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewNegativeWriteoffReversalConfirmationScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffReversalConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewNegativeWriteoffReversalConfirmationScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at NewNegativeWriteoffReversalConfirmationScreen.pcf: line 32, column 75
    function currency_7 () : typekey.Currency {
      return negativeWriteoffToReverse.getNegativeWriteoff().Currency
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffReversalConfirmationScreen.pcf: line 42, column 53
    function def_onEnter_13 (def :  pcf.ApprovalActivityDV) : void {
      def.onEnter(openNegWoffRevApprActivity)
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffReversalConfirmationScreen.pcf: line 42, column 53
    function def_refreshVariables_14 (def :  pcf.ApprovalActivityDV) : void {
      def.refreshVariables(openNegWoffRevApprActivity)
    }
    
    // 'initialValue' attribute on Variable at NewNegativeWriteoffReversalConfirmationScreen.pcf: line 13, column 38
    function initialValue_0 () : NegWoffRevApprActivity {
      return negativeWriteoffToReverse.OpenApprovalActivity
    }
    
    // 'value' attribute on DateInput (id=TransactionDate_Input) at NewNegativeWriteoffReversalConfirmationScreen.pcf: line 27, column 79
    function valueRoot_3 () : java.lang.Object {
      return negativeWriteoffToReverse.getNegativeWriteoff()
    }
    
    // 'value' attribute on DateInput (id=TransactionDate_Input) at NewNegativeWriteoffReversalConfirmationScreen.pcf: line 27, column 79
    function value_2 () : java.util.Date {
      return negativeWriteoffToReverse.getNegativeWriteoff().CreateTime
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at NewNegativeWriteoffReversalConfirmationScreen.pcf: line 32, column 75
    function value_5 () : gw.pl.currency.MonetaryAmount {
      return negativeWriteoffToReverse.getNegativeWriteoff().Amount
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at NewNegativeWriteoffReversalConfirmationScreen.pcf: line 36, column 80
    function value_9 () : java.lang.String {
      return negativeWriteoffToReverse.getNegativeWriteoff().DisplayName
    }
    
    // 'visible' attribute on AlertBar (id=ApprovalActivityAlertBar) at NewNegativeWriteoffReversalConfirmationScreen.pcf: line 20, column 53
    function visible_1 () : java.lang.Boolean {
      return openNegWoffRevApprActivity != null
    }
    
    property get negativeWriteoffToReverse () : NegativeWriteoffRev {
      return getRequireValue("negativeWriteoffToReverse", 0) as NegativeWriteoffRev
    }
    
    property set negativeWriteoffToReverse ($arg :  NegativeWriteoffRev) {
      setRequireValue("negativeWriteoffToReverse", 0, $arg)
    }
    
    property get openNegWoffRevApprActivity () : NegWoffRevApprActivity {
      return getVariableValue("openNegWoffRevApprActivity", 0) as NegWoffRevApprActivity
    }
    
    property set openNegWoffRevApprActivity ($arg :  NegWoffRevApprActivity) {
      setVariableValue("openNegWoffRevApprActivity", 0, $arg)
    }
    
    
  }
  
  
}