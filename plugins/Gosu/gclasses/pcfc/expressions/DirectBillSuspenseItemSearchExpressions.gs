package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DirectBillSuspenseItemSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DirectBillSuspenseItemSearchExpressions {
  @javax.annotation.Generated("config/web/pcf/search/DirectBillSuspenseItemSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DirectBillSuspenseItemSearchExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=DirectBillSuspenseItemSearch) at DirectBillSuspenseItemSearch.pcf: line 8, column 82
    static function canVisit_2 () : java.lang.Boolean {
      return perm.System.viewsearch and perm.System.pmntsearch
    }
    
    // 'def' attribute on ScreenRef at DirectBillSuspenseItemSearch.pcf: line 10, column 51
    function def_onEnter_0 (def :  pcf.DirectBillSuspenseItemSearchScreen) : void {
      def.onEnter()
    }
    
    // 'def' attribute on ScreenRef at DirectBillSuspenseItemSearch.pcf: line 10, column 51
    function def_refreshVariables_1 (def :  pcf.DirectBillSuspenseItemSearchScreen) : void {
      def.refreshVariables()
    }
    
    // Page (id=DirectBillSuspenseItemSearch) at DirectBillSuspenseItemSearch.pcf: line 8, column 82
    static function parent_3 () : pcf.api.Destination {
      return pcf.SearchGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.DirectBillSuspenseItemSearch {
      return super.CurrentLocation as pcf.DirectBillSuspenseItemSearch
    }
    
    
  }
  
  
}