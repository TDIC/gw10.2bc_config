package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/PolicyPeriodPanelLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyPeriodPanelLVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/PolicyPeriodPanelLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyPeriodPanelLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Number_Cell) at PolicyPeriodPanelLV.pcf: line 25, column 25
    function action_15 () : void {
      PolicyOverview.go(policyPeriod)
    }
    
    // 'action' attribute on TextCell (id=Number_Cell) at PolicyPeriodPanelLV.pcf: line 25, column 25
    function action_dest_16 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(policyPeriod)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=TotalValue_Cell) at PolicyPeriodPanelLV.pcf: line 63, column 44
    function currency_41 () : typekey.Currency {
      return policyPeriod.Currency
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at PolicyPeriodPanelLV.pcf: line 25, column 25
    function valueRoot_18 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at PolicyPeriodPanelLV.pcf: line 25, column 25
    function value_17 () : java.lang.String {
      return policyPeriod.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Insured_Cell) at PolicyPeriodPanelLV.pcf: line 32, column 25
    function value_20 () : entity.PolicyPeriodContact {
      return policyPeriod.PrimaryInsured
    }
    
    // 'value' attribute on DateCell (id=PolicyPerEffDate_Cell) at PolicyPeriodPanelLV.pcf: line 36, column 50
    function value_23 () : java.util.Date {
      return policyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=PolicyPerExpirDate_Cell) at PolicyPeriodPanelLV.pcf: line 40, column 52
    function value_26 () : java.util.Date {
      return policyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on TextCell (id=StatusTDIC_Cell) at PolicyPeriodPanelLV.pcf: line 45, column 41
    function value_29 () : java.lang.String {
      return policyPeriod.CancelStatus == PolicyCancelStatus.TC_OPEN ? policyPeriod.Status.DisplayName : policyPeriod.CancelStatus.DisplayName
    }
    
    // 'value' attribute on TypeKeyCell (id=ArchiveStatus_Cell) at PolicyPeriodPanelLV.pcf: line 51, column 76
    function value_31 () : typekey.ArchiveState {
      return policyPeriod.ArchiveState
    }
    
    // 'value' attribute on CheckBoxCell (id=ArchiveRetrieved_Cell) at PolicyPeriodPanelLV.pcf: line 56, column 76
    function value_35 () : java.lang.Boolean {
      return policyPeriod.Retrieved
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalValue_Cell) at PolicyPeriodPanelLV.pcf: line 63, column 44
    function value_39 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.TotalValue
    }
    
    // 'visible' attribute on TypeKeyCell (id=ArchiveStatus_Cell) at PolicyPeriodPanelLV.pcf: line 51, column 76
    function visible_33 () : java.lang.Boolean {
      return gw.api.system.PLConfigParameters.ArchiveEnabled.Value
    }
    
    property get policyPeriod () : entity.PolicyPeriod {
      return getIteratedValue(1) as entity.PolicyPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/PolicyPeriodPanelLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyPeriodPanelLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on TextCell (id=Number_Cell) at PolicyPeriodPanelLV.pcf: line 25, column 25
    function sortValue_0 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PolicyNumber
    }
    
    // 'sortBy' attribute on TextCell (id=Number_Cell) at PolicyPeriodPanelLV.pcf: line 25, column 25
    function sortValue_1 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return  policyPeriod.TermNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalValue_Cell) at PolicyPeriodPanelLV.pcf: line 63, column 44
    function sortValue_10 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.TotalValue
    }
    
    // 'value' attribute on TextCell (id=Insured_Cell) at PolicyPeriodPanelLV.pcf: line 32, column 25
    function sortValue_2 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PrimaryInsured
    }
    
    // 'value' attribute on DateCell (id=PolicyPerEffDate_Cell) at PolicyPeriodPanelLV.pcf: line 36, column 50
    function sortValue_3 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=PolicyPerExpirDate_Cell) at PolicyPeriodPanelLV.pcf: line 40, column 52
    function sortValue_4 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on TextCell (id=StatusTDIC_Cell) at PolicyPeriodPanelLV.pcf: line 45, column 41
    function sortValue_5 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.CancelStatus == PolicyCancelStatus.TC_OPEN ? policyPeriod.Status.DisplayName : policyPeriod.CancelStatus.DisplayName
    }
    
    // 'value' attribute on TypeKeyCell (id=ArchiveStatus_Cell) at PolicyPeriodPanelLV.pcf: line 51, column 76
    function sortValue_6 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.ArchiveState
    }
    
    // 'value' attribute on CheckBoxCell (id=ArchiveRetrieved_Cell) at PolicyPeriodPanelLV.pcf: line 56, column 76
    function sortValue_8 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.Retrieved
    }
    
    // '$$sumValue' attribute on RowIterator at PolicyPeriodPanelLV.pcf: line 63, column 44
    function sumValueRoot_14 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod
    }
    
    // 'footerSumValue' attribute on RowIterator at PolicyPeriodPanelLV.pcf: line 63, column 44
    function sumValue_13 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.TotalValue
    }
    
    // 'value' attribute on RowIterator at PolicyPeriodPanelLV.pcf: line 15, column 61
    function value_43 () : java.util.List<entity.PolicyPeriod> {
      return policyPeriods
    }
    
    // 'visible' attribute on TypeKeyCell (id=ArchiveStatus_Cell) at PolicyPeriodPanelLV.pcf: line 51, column 76
    function visible_7 () : java.lang.Boolean {
      return gw.api.system.PLConfigParameters.ArchiveEnabled.Value
    }
    
    property get policyPeriods () : List<PolicyPeriod> {
      return getRequireValue("policyPeriods", 0) as List<PolicyPeriod>
    }
    
    property set policyPeriods ($arg :  List<PolicyPeriod>) {
      setRequireValue("policyPeriods", 0, $arg)
    }
    
    
  }
  
  
}