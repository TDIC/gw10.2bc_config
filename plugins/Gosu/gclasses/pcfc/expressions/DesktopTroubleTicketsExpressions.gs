package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopTroubleTickets.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopTroubleTicketsExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopTroubleTickets.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopTroubleTicketsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=NewTroubleTicket) at DesktopTroubleTickets.pcf: line 42, column 50
    function action_5 () : void {
      CreateTroubleTicketWizard.push(null)
    }
    
    // 'action' attribute on ToolbarButton (id=NewTroubleTicket) at DesktopTroubleTickets.pcf: line 42, column 50
    function action_dest_6 () : pcf.api.Destination {
      return pcf.CreateTroubleTicketWizard.createDestination(null)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=DesktopTroubleTickets_AssignButton) at DesktopTroubleTickets.pcf: line 28, column 50
    function allCheckedRowsAction_2 (CheckedValues :  entity.TroubleTicket[], CheckedValuesErrors :  java.util.Map) : void {
      AssignTroubleTicketsPopup.push(new gw.api.web.troubleticket.TroubleTicketAssignmentPopup(CheckedValues), CheckedValues)
    }
    
    // 'canVisit' attribute on Page (id=DesktopTroubleTickets) at DesktopTroubleTickets.pcf: line 9, column 67
    static function canVisit_53 () : java.lang.Boolean {
      return perm.System.myttktview and perm.System.viewdesktop
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=DesktopTroubleTickets_CloseButton) at DesktopTroubleTickets.pcf: line 36, column 27
    function checkedRowAction_3 (troubleTicket :  entity.TroubleTicket, CheckedValue :  entity.TroubleTicket) : void {
      CheckedValue.close()
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopTroubleTickets.pcf: line 64, column 99
    function filter_10 () : gw.api.filters.IFilter {
      return new gw.api.web.troubleticket.TroubleTicketListFilterSet.AllOpenUrgent()
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopTroubleTickets.pcf: line 66, column 101
    function filter_11 () : gw.api.filters.IFilter {
      return new gw.api.web.troubleticket.TroubleTicketListFilterSet.AllOpenLastYear()
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopTroubleTickets.pcf: line 68, column 107
    function filter_12 () : gw.api.filters.IFilter {
      return new gw.api.web.troubleticket.TroubleTicketListFilterSet.AllClosedInLast30Days()
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopTroubleTickets.pcf: line 70, column 103
    function filter_13 () : gw.api.filters.IFilter {
      return new gw.api.web.troubleticket.TroubleTicketListFilterSet.AllClosedLastYear()
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopTroubleTickets.pcf: line 57, column 67
    function filter_7 () : gw.api.filters.IFilter {
      return new gw.api.util.CoreFilters.AllFilter()
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopTroubleTickets.pcf: line 60, column 39
    function filter_8 () : gw.api.filters.IFilter {
      return new gw.api.web.troubleticket.TroubleTicketListFilterSet.AllOpen()
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopTroubleTickets.pcf: line 62, column 103
    function filter_9 () : gw.api.filters.IFilter {
      return new gw.api.web.troubleticket.TroubleTicketListFilterSet.AllOpenedThisWeek()
    }
    
    // 'iconColor' attribute on BooleanRadioCell (id=Escalated_Cell) at DesktopTroubleTickets.pcf: line 81, column 50
    function iconColor_14 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_URGENCY_HIGH
    }
    
    // 'initialValue' attribute on Variable at DesktopTroubleTickets.pcf: line 15, column 69
    function initialValue_0 () : gw.api.database.IQueryBeanResult<TroubleTicket> {
      return (User.util.CurrentUser as User).TroubleTickets
    }
    
    // Page (id=DesktopTroubleTickets) at DesktopTroubleTickets.pcf: line 9, column 67
    static function parent_54 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at DesktopTroubleTickets.pcf: line 81, column 50
    function sortValue_15 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.Escalated
    }
    
    // 'value' attribute on TextCell (id=TicketCreateUser_Cell) at DesktopTroubleTickets.pcf: line 93, column 42
    function sortValue_16 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.CreateUser
    }
    
    // 'value' attribute on DateCell (id=CreateTime_Cell) at DesktopTroubleTickets.pcf: line 97, column 51
    function sortValue_17 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.CreateTime
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at DesktopTroubleTickets.pcf: line 102, column 51
    function sortValue_18 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.TargetDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at DesktopTroubleTickets.pcf: line 107, column 47
    function sortValue_19 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.Priority
    }
    
    // 'sortBy' attribute on TextCell (id=Status_Cell) at DesktopTroubleTickets.pcf: line 112, column 53
    function sortValue_20 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.CloseDate
    }
    
    // 'value' attribute on TextCell (id=Title_Cell) at DesktopTroubleTickets.pcf: line 117, column 46
    function sortValue_21 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.Title
    }
    
    // 'value' attribute on RowIterator at DesktopTroubleTickets.pcf: line 53, column 86
    function value_52 () : gw.api.database.IQueryBeanResult<entity.TroubleTicket> {
      return troubleTickets
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=DesktopTroubleTickets_AssignButton) at DesktopTroubleTickets.pcf: line 28, column 50
    function visible_1 () : java.lang.Boolean {
      return perm.System.ttktassignown
    }
    
    // 'visible' attribute on ToolbarButton (id=NewTroubleTicket) at DesktopTroubleTickets.pcf: line 42, column 50
    function visible_4 () : java.lang.Boolean {
      return perm.TroubleTicket.create
    }
    
    override property get CurrentLocation () : pcf.DesktopTroubleTickets {
      return super.CurrentLocation as pcf.DesktopTroubleTickets
    }
    
    property get troubleTickets () : gw.api.database.IQueryBeanResult<TroubleTicket> {
      return getVariableValue("troubleTickets", 0) as gw.api.database.IQueryBeanResult<TroubleTicket>
    }
    
    property set troubleTickets ($arg :  gw.api.database.IQueryBeanResult<TroubleTicket>) {
      setVariableValue("troubleTickets", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopTroubleTickets.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DesktopTroubleTicketsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Number_Cell) at DesktopTroubleTickets.pcf: line 88, column 60
    function action_26 () : void {
      TroubleTicketDetailsPopup.push(troubleTicket)
    }
    
    // 'action' attribute on TextCell (id=Number_Cell) at DesktopTroubleTickets.pcf: line 88, column 60
    function action_dest_27 () : pcf.api.Destination {
      return pcf.TroubleTicketDetailsPopup.createDestination(troubleTicket)
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at DesktopTroubleTickets.pcf: line 102, column 51
    function fontColor_37 () : java.lang.Object {
      return troubleTicket.Overdue == true ? gw.api.web.color.GWColor.THEME_URGENCY_HIGH : null
    }
    
    // 'iconColor' attribute on BooleanRadioCell (id=Escalated_Cell) at DesktopTroubleTickets.pcf: line 81, column 50
    function iconColor_24 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_URGENCY_HIGH
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at DesktopTroubleTickets.pcf: line 81, column 50
    function valueRoot_23 () : java.lang.Object {
      return troubleTicket
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at DesktopTroubleTickets.pcf: line 81, column 50
    function value_22 () : java.lang.Boolean {
      return troubleTicket.Escalated
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at DesktopTroubleTickets.pcf: line 88, column 60
    function value_28 () : java.lang.String {
      return troubleTicket.TroubleTicketNumber
    }
    
    // 'value' attribute on TextCell (id=TicketCreateUser_Cell) at DesktopTroubleTickets.pcf: line 93, column 42
    function value_31 () : entity.User {
      return troubleTicket.CreateUser
    }
    
    // 'value' attribute on DateCell (id=CreateTime_Cell) at DesktopTroubleTickets.pcf: line 97, column 51
    function value_34 () : java.util.Date {
      return troubleTicket.CreateTime
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at DesktopTroubleTickets.pcf: line 102, column 51
    function value_38 () : java.util.Date {
      return troubleTicket.TargetDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at DesktopTroubleTickets.pcf: line 107, column 47
    function value_43 () : typekey.Priority {
      return troubleTicket.Priority
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at DesktopTroubleTickets.pcf: line 112, column 53
    function value_46 () : java.lang.String {
      return troubleTicket.TicketStatus
    }
    
    // 'value' attribute on TextCell (id=Title_Cell) at DesktopTroubleTickets.pcf: line 117, column 46
    function value_49 () : java.lang.String {
      return troubleTicket.Title
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at DesktopTroubleTickets.pcf: line 102, column 51
    function verifyFontColorIsAllowedType_40 ($$arg :  gw.api.web.color.GWColor) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at DesktopTroubleTickets.pcf: line 102, column 51
    function verifyFontColorIsAllowedType_40 ($$arg :  java.lang.String) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at DesktopTroubleTickets.pcf: line 102, column 51
    function verifyFontColor_41 () : void {
      var __fontColorArg = troubleTicket.Overdue == true ? gw.api.web.color.GWColor.THEME_URGENCY_HIGH : null
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the fontColor return type is not a valid type for use with a ValueWidget's fontColor
      // It needs to return a String or a GWColor.
      verifyFontColorIsAllowedType_40(__fontColorArg)
    }
    
    property get troubleTicket () : entity.TroubleTicket {
      return getIteratedValue(1) as entity.TroubleTicket
    }
    
    
  }
  
  
}