package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/UnappliedFundsDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class UnappliedFundsDetailLVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/UnappliedFundsDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends UnappliedFundsDetailLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=SourcesAmount_Cell) at UnappliedFundsDetailLV.pcf: line 45, column 95
    function currency_19 () : typekey.Currency {
      return fundsTracker.Currency
    }
    
    // 'value' attribute on TextCell (id=SourcesUses_Cell) at UnappliedFundsDetailLV.pcf: line 22, column 45
    function valueRoot_9 () : java.lang.Object {
      return fundsTracker
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at UnappliedFundsDetailLV.pcf: line 28, column 43
    function value_11 () : java.util.Date {
      return fundsTracker.EventDate
    }
    
    // 'value' attribute on TextCell (id=PaymentInstrument_Cell) at UnappliedFundsDetailLV.pcf: line 33, column 159
    function value_14 () : java.lang.String {
      return (fundsTracker.Trackable typeis DirectBillMoneyRcvd) ? (fundsTracker.Trackable as DirectBillMoneyRcvd).PaymentInstrument.DisplayName : null
    }
    
    // 'value' attribute on TextCell (id=CheckRef_Cell) at UnappliedFundsDetailLV.pcf: line 38, column 139
    function value_16 () : java.lang.String {
      return (fundsTracker.Trackable typeis DirectBillMoneyRcvd) ? (fundsTracker.Trackable as DirectBillMoneyRcvd).RefNumber : null
    }
    
    // 'value' attribute on MonetaryAmountCell (id=SourcesAmount_Cell) at UnappliedFundsDetailLV.pcf: line 45, column 95
    function value_18 () : gw.pl.currency.MonetaryAmount {
      return (fundsTracker typeis FundsSourceTracker) ? fundsTracker.TotalAmount : null
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UsesAmount_Cell) at UnappliedFundsDetailLV.pcf: line 52, column 101
    function value_21 () : gw.pl.currency.MonetaryAmount {
      return (fundsTracker typeis FundsUseTracker) ? fundsTracker.TotalAmount.negate() : null
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Unallotted_Cell) at UnappliedFundsDetailLV.pcf: line 60, column 134
    function value_24 () : gw.pl.currency.MonetaryAmount {
      return (fundsTracker typeis FundsSourceTracker) ? fundsTracker.UnallottedAmount : fundsTracker.UnallottedAmount.negate()
    }
    
    // 'value' attribute on TextCell (id=SourcesUses_Cell) at UnappliedFundsDetailLV.pcf: line 22, column 45
    function value_8 () : java.lang.String {
      return fundsTracker.Description
    }
    
    property get fundsTracker () : entity.FundsTracker {
      return getIteratedValue(1) as entity.FundsTracker
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/UnappliedFundsDetailLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class UnappliedFundsDetailLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=SourcesUses_Cell) at UnappliedFundsDetailLV.pcf: line 22, column 45
    function sortValue_0 (fundsTracker :  entity.FundsTracker) : java.lang.Object {
      return fundsTracker.Description
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at UnappliedFundsDetailLV.pcf: line 28, column 43
    function sortValue_1 (fundsTracker :  entity.FundsTracker) : java.lang.Object {
      return fundsTracker.EventDate
    }
    
    // 'value' attribute on TextCell (id=PaymentInstrument_Cell) at UnappliedFundsDetailLV.pcf: line 33, column 159
    function sortValue_2 (fundsTracker :  entity.FundsTracker) : java.lang.Object {
      return (fundsTracker.Trackable typeis DirectBillMoneyRcvd) ? (fundsTracker.Trackable as DirectBillMoneyRcvd).PaymentInstrument.DisplayName : null
    }
    
    // 'value' attribute on TextCell (id=CheckRef_Cell) at UnappliedFundsDetailLV.pcf: line 38, column 139
    function sortValue_3 (fundsTracker :  entity.FundsTracker) : java.lang.Object {
      return (fundsTracker.Trackable typeis DirectBillMoneyRcvd) ? (fundsTracker.Trackable as DirectBillMoneyRcvd).RefNumber : null
    }
    
    // 'value' attribute on MonetaryAmountCell (id=SourcesAmount_Cell) at UnappliedFundsDetailLV.pcf: line 45, column 95
    function sortValue_4 (fundsTracker :  entity.FundsTracker) : java.lang.Object {
      return (fundsTracker typeis FundsSourceTracker) ? fundsTracker.TotalAmount : null
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UsesAmount_Cell) at UnappliedFundsDetailLV.pcf: line 52, column 101
    function sortValue_5 (fundsTracker :  entity.FundsTracker) : java.lang.Object {
      return (fundsTracker typeis FundsUseTracker) ? fundsTracker.TotalAmount.negate() : null
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Unallotted_Cell) at UnappliedFundsDetailLV.pcf: line 60, column 134
    function sortValue_6 (fundsTracker :  entity.FundsTracker) : java.lang.Object {
      return (fundsTracker typeis FundsSourceTracker) ? fundsTracker.UnallottedAmount : fundsTracker.UnallottedAmount.negate()
    }
    
    // 'footerSumValue' attribute on RowIterator (id=FundsIterator) at UnappliedFundsDetailLV.pcf: line 60, column 134
    function sumValue_7 (fundsTracker :  entity.FundsTracker) : java.lang.Object {
      return (fundsTracker typeis FundsSourceTracker) ? fundsTracker.UnallottedAmount : fundsTracker.UnallottedAmount.negate()
    }
    
    // 'value' attribute on RowIterator (id=FundsIterator) at UnappliedFundsDetailLV.pcf: line 15, column 61
    function value_27 () : java.util.List<entity.FundsTracker> {
      return FundsTrackerList
    }
    
    property get unappliedFund () : UnappliedFund {
      return getRequireValue("unappliedFund", 0) as UnappliedFund
    }
    
    property set unappliedFund ($arg :  UnappliedFund) {
      setRequireValue("unappliedFund", 0, $arg)
    }
    
    property get FundsTrackerList() : List<entity.FundsTracker> {
      var trackers = unappliedFund.getFundsSourceTrackers() as List<FundsTracker>
      trackers.addAll(unappliedFund.getFundsUseTrackers() as List<FundsTracker>)
      //Filter out Unallotted = 0
      trackers.removeWhere(\e -> e.UnallottedAmount.IsZero)
      return trackers
    }
    
    
  }
  
  
}