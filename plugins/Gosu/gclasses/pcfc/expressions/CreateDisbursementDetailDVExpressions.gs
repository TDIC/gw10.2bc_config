package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/CreateDisbursementDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreateDisbursementDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/disbursement/CreateDisbursementDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreateDisbursementDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=amount_Input) at CreateDisbursementDetailDV.pcf: line 26, column 41
    function currency_6 () : typekey.Currency {
      return disbursementVar.Currency
    }
    
    // 'value' attribute on DateInput (id=effectiveDate_Input) at CreateDisbursementDetailDV.pcf: line 32, column 42
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      disbursementVar.DueDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeInput (id=reason_Input) at CreateDisbursementDetailDV.pcf: line 39, column 37
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      disbursementVar.Reason = (__VALUE_TO_SET as typekey.Reason)
    }
    
    // 'value' attribute on TextInput (id=payTo_Input) at CreateDisbursementDetailDV.pcf: line 47, column 40
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      disbursementVar.PayTo = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=mailTo_Input) at CreateDisbursementDetailDV.pcf: line 53, column 41
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      disbursementVar.MailTo = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextAreaInput (id=mailToAddress_Input) at CreateDisbursementDetailDV.pcf: line 60, column 42
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      disbursementVar.Address = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amount_Input) at CreateDisbursementDetailDV.pcf: line 26, column 41
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      disbursementVar.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextInput (id=memo_Input) at CreateDisbursementDetailDV.pcf: line 65, column 39
    function defaultSetter_43 (__VALUE_TO_SET :  java.lang.Object) : void {
      disbursementVar.Memo = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=internalComment_Input) at CreateDisbursementDetailDV.pcf: line 70, column 50
    function defaultSetter_49 (__VALUE_TO_SET :  java.lang.Object) : void {
      disbursementVar.InternalComment = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on MonetaryAmountInput (id=amount_Input) at CreateDisbursementDetailDV.pcf: line 26, column 41
    function editable_1 () : java.lang.Boolean {
      return isAmountEditable
    }
    
    // 'editable' attribute on TextInput (id=payTo_Input) at CreateDisbursementDetailDV.pcf: line 47, column 40
    function editable_23 () : java.lang.Boolean {
      return CanEditDisbursementPayee
    }
    
    // 'initialValue' attribute on Variable at CreateDisbursementDetailDV.pcf: line 13, column 23
    function initialValue_0 () : Boolean {
      return perm.Transaction.disbpayeeedit
    }
    
    // 'validationExpression' attribute on DateInput (id=effectiveDate_Input) at CreateDisbursementDetailDV.pcf: line 32, column 42
    function validationExpression_10 () : java.lang.Object {
      return validateDueDate()
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=amount_Input) at CreateDisbursementDetailDV.pcf: line 26, column 41
    function validationExpression_2 () : java.lang.Object {
      return disbursementVar.validateManualDisbursement()
    }
    
    // 'valueRange' attribute on RangeInput (id=reason_Input) at CreateDisbursementDetailDV.pcf: line 39, column 37
    function valueRange_19 () : java.lang.Object {
      return disbursementVar.VisibleDisbursementReasons
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amount_Input) at CreateDisbursementDetailDV.pcf: line 26, column 41
    function valueRoot_5 () : java.lang.Object {
      return disbursementVar
    }
    
    // 'value' attribute on DateInput (id=effectiveDate_Input) at CreateDisbursementDetailDV.pcf: line 32, column 42
    function value_11 () : java.util.Date {
      return disbursementVar.DueDate
    }
    
    // 'value' attribute on RangeInput (id=reason_Input) at CreateDisbursementDetailDV.pcf: line 39, column 37
    function value_16 () : typekey.Reason {
      return disbursementVar.Reason
    }
    
    // 'value' attribute on TextInput (id=payTo_Input) at CreateDisbursementDetailDV.pcf: line 47, column 40
    function value_24 () : java.lang.String {
      return disbursementVar.PayTo
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amount_Input) at CreateDisbursementDetailDV.pcf: line 26, column 41
    function value_3 () : gw.pl.currency.MonetaryAmount {
      return disbursementVar.Amount
    }
    
    // 'value' attribute on TextInput (id=mailTo_Input) at CreateDisbursementDetailDV.pcf: line 53, column 41
    function value_30 () : java.lang.String {
      return disbursementVar.MailTo
    }
    
    // 'value' attribute on TextAreaInput (id=mailToAddress_Input) at CreateDisbursementDetailDV.pcf: line 60, column 42
    function value_36 () : java.lang.String {
      return disbursementVar.Address
    }
    
    // 'value' attribute on TextInput (id=memo_Input) at CreateDisbursementDetailDV.pcf: line 65, column 39
    function value_42 () : java.lang.String {
      return disbursementVar.Memo
    }
    
    // 'value' attribute on TextInput (id=internalComment_Input) at CreateDisbursementDetailDV.pcf: line 70, column 50
    function value_48 () : java.lang.String {
      return disbursementVar.InternalComment
    }
    
    // 'valueRange' attribute on RangeInput (id=reason_Input) at CreateDisbursementDetailDV.pcf: line 39, column 37
    function verifyValueRangeIsAllowedType_20 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=reason_Input) at CreateDisbursementDetailDV.pcf: line 39, column 37
    function verifyValueRangeIsAllowedType_20 ($$arg :  typekey.Reason[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=reason_Input) at CreateDisbursementDetailDV.pcf: line 39, column 37
    function verifyValueRange_21 () : void {
      var __valueRangeArg = disbursementVar.VisibleDisbursementReasons
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_20(__valueRangeArg)
    }
    
    property get CanEditDisbursementPayee () : Boolean {
      return getVariableValue("CanEditDisbursementPayee", 0) as Boolean
    }
    
    property set CanEditDisbursementPayee ($arg :  Boolean) {
      setVariableValue("CanEditDisbursementPayee", 0, $arg)
    }
    
    property get disbursementVar () : Disbursement {
      return getRequireValue("disbursementVar", 0) as Disbursement
    }
    
    property set disbursementVar ($arg :  Disbursement) {
      setRequireValue("disbursementVar", 0, $arg)
    }
    
    property get isAmountEditable () : boolean {
      return getRequireValue("isAmountEditable", 0) as java.lang.Boolean
    }
    
    property set isAmountEditable ($arg :  boolean) {
      setRequireValue("isAmountEditable", 0, $arg)
    }
    
    function validateDueDate() : String {
      if (gw.api.util.DateUtil.verifyDateOnOrAfterToday(disbursementVar.DueDate)) {
        if (disbursementVar typeis SuspenseDisbursement == false or gw.api.util.DateUtil.compareIgnoreTime((disbursementVar as SuspenseDisbursement).SuspensePayment.PaymentDate, disbursementVar.DueDate) <= 0) {
          return null
        }
        return DisplayKey.get("Web.CreateDisbursementDetailScreen.DueDateBeforePaymentDate")
      }
      return DisplayKey.get("Web.CreateDisbursementDetailScreen.DueDateInPast")
    }
    
    
  }
  
  
}