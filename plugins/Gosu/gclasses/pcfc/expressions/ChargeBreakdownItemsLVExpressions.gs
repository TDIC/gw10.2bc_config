package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/ChargeBreakdownItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChargeBreakdownItemsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/ChargeBreakdownItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChargeBreakdownItemsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at ChargeBreakdownItemsLV.pcf: line 18, column 56
    function initialValue_0 () : gw.web.policy.ChargeBreakdownItemsLvView {
      return new gw.web.policy.ChargeBreakdownItemsLvView(initializer)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at ChargeBreakdownItemsLV.pcf: line 54, column 24
    function sortValue_1 (item :  ChargeBreakdownItem) : java.lang.Object {
      return item.Description
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at ChargeBreakdownItemsLV.pcf: line 64, column 34
    function sortValue_2 (item :  ChargeBreakdownItem) : java.lang.Object {
      return item.Amount
    }
    
    // '$$sumValue' attribute on RowIterator (id=ItemIterator) at ChargeBreakdownItemsLV.pcf: line 64, column 34
    function sumValueRoot_4 (item :  ChargeBreakdownItem) : java.lang.Object {
      return item
    }
    
    // 'footerSumValue' attribute on RowIterator (id=ItemIterator) at ChargeBreakdownItemsLV.pcf: line 64, column 34
    function sumValue_3 (item :  ChargeBreakdownItem) : java.lang.Object {
      return item.Amount
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=ItemIterator) at ChargeBreakdownItemsLV.pcf: line 27, column 61
    function toCreateAndAdd_27 () : ChargeBreakdownItem {
      return view.createAndAddItem()
    }
    
    // 'validationExpression' attribute on ListViewPanel (id=ChargeBreakdownItemsLV) at ChargeBreakdownItemsLV.pcf: line 7, column 69
    function validationExpression_29 () : java.lang.Object {
      return view.validateItemsHaveUniqueCategories()
    }
    
    // 'value' attribute on RowIterator (id=ItemIterator) at ChargeBreakdownItemsLV.pcf: line 27, column 61
    function value_28 () : java.util.List<ChargeBreakdownItem> {
      return initializer.BreakdownItems
    }
    
    property get initializer () : gw.api.domain.charge.ChargeInitializer {
      return getRequireValue("initializer", 0) as gw.api.domain.charge.ChargeInitializer
    }
    
    property set initializer ($arg :  gw.api.domain.charge.ChargeInitializer) {
      setRequireValue("initializer", 0, $arg)
    }
    
    property get view () : gw.web.policy.ChargeBreakdownItemsLvView {
      return getVariableValue("view", 0) as gw.web.policy.ChargeBreakdownItemsLvView
    }
    
    property set view ($arg :  gw.web.policy.ChargeBreakdownItemsLvView) {
      setVariableValue("view", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/ChargeBreakdownItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=Category_Input) at ChargeBreakdownItemsLV.pcf: line 45, column 45
    function valueRoot_9 () : java.lang.Object {
      return category
    }
    
    // 'value' attribute on TextInput (id=Category_Input) at ChargeBreakdownItemsLV.pcf: line 45, column 45
    function value_8 () : java.lang.String {
      return category.DisplayName
    }
    
    property get category () : entity.ChargeBreakdownCategory {
      return getIteratedValue(2) as entity.ChargeBreakdownCategory
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/ChargeBreakdownItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ChargeBreakdownItemsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ButtonCell (id=Remove_Cell) at ChargeBreakdownItemsLV.pcf: line 72, column 35
    function action_26 () : void {
      view.removeItem(item)
    }
    
    // 'action' attribute on Link (id=CategoryLink) at ChargeBreakdownItemsLV.pcf: line 33, column 70
    function action_5 () : void {
      ChargeBreakdownCategoryPopup.push(item, view)
    }
    
    // 'action' attribute on Link (id=CategoryLink) at ChargeBreakdownItemsLV.pcf: line 33, column 70
    function action_dest_6 () : pcf.api.Destination {
      return pcf.ChargeBreakdownCategoryPopup.createDestination(item, view)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at ChargeBreakdownItemsLV.pcf: line 64, column 34
    function currency_23 () : typekey.Currency {
      return initializer.BillingInstruction.Currency
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at ChargeBreakdownItemsLV.pcf: line 54, column 24
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      item.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at ChargeBreakdownItemsLV.pcf: line 64, column 34
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      item.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'label' attribute on Link (id=CategoryLink) at ChargeBreakdownItemsLV.pcf: line 33, column 70
    function label_7 () : java.lang.Object {
      return view.getCategoryLinkText(item.Categories.Count)
    }
    
    // 'onChange' attribute on PostOnChange at ChargeBreakdownItemsLV.pcf: line 66, column 67
    function onChange_18 () : void {
      view.setChargeAmountFromBreakdownAmount()
    }
    
    // 'validationExpression' attribute on MonetaryAmountCell (id=Amount_Cell) at ChargeBreakdownItemsLV.pcf: line 64, column 34
    function validationExpression_19 () : java.lang.Object {
      return item.Amount.IsZero ? DisplayKey.get("Web.ChargeBreakdownItem.AmountCannotBeZero") : null
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at ChargeBreakdownItemsLV.pcf: line 54, column 24
    function valueRoot_16 () : java.lang.Object {
      return item
    }
    
    // 'value' attribute on InputIterator at ChargeBreakdownItemsLV.pcf: line 42, column 68
    function value_11 () : List<entity.ChargeBreakdownCategory> {
      return item.Categories.sortBy(\category -> category.DisplayName)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at ChargeBreakdownItemsLV.pcf: line 54, column 24
    function value_14 () : java.lang.String {
      return item.Description
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at ChargeBreakdownItemsLV.pcf: line 64, column 34
    function value_20 () : gw.pl.currency.MonetaryAmount {
      return item.Amount
    }
    
    // 'valueType' attribute on InputIterator at ChargeBreakdownItemsLV.pcf: line 42, column 68
    function verifyValueTypeIsAllowedType_12 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on InputIterator at ChargeBreakdownItemsLV.pcf: line 42, column 68
    function verifyValueTypeIsAllowedType_12 ($$arg :  gw.api.iterator.IteratorBackingData) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on InputIterator at ChargeBreakdownItemsLV.pcf: line 42, column 68
    function verifyValueTypeIsAllowedType_12 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on InputIterator at ChargeBreakdownItemsLV.pcf: line 42, column 68
    function verifyValueType_13 () : void {
      var __valueTypeArg : List<entity.ChargeBreakdownCategory>
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the valueType is not a valid type for use with an iterator
      // The valueType for an iterator must be an array or extend from List or IQueryBeanResult
      verifyValueTypeIsAllowedType_12(__valueTypeArg)
    }
    
    property get item () : ChargeBreakdownItem {
      return getIteratedValue(1) as ChargeBreakdownItem
    }
    
    
  }
  
  
}