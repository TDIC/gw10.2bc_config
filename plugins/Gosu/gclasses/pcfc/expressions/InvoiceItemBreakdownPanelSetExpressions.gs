package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/InvoiceItemBreakdownPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceItemBreakdownPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/InvoiceItemBreakdownPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceItemBreakdownPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at InvoiceItemBreakdownPanelSet.pcf: line 48, column 42
    function sortValue_0 (item :  InvoiceItemBreakdownItem) : java.lang.Object {
      return item.Amount
    }
    
    // '$$sumValue' attribute on RowIterator (id=ItemIterator) at InvoiceItemBreakdownPanelSet.pcf: line 48, column 42
    function sumValueRoot_2 (item :  InvoiceItemBreakdownItem) : java.lang.Object {
      return item
    }
    
    // 'footerSumValue' attribute on RowIterator (id=ItemIterator) at InvoiceItemBreakdownPanelSet.pcf: line 48, column 42
    function sumValue_1 (item :  InvoiceItemBreakdownItem) : java.lang.Object {
      return item.Amount
    }
    
    // 'value' attribute on RowIterator (id=ItemIterator) at InvoiceItemBreakdownPanelSet.pcf: line 29, column 56
    function value_11 () : InvoiceItemBreakdownItem[] {
      return invoiceItem.BreakdownItems
    }
    
    // 'visible' attribute on PanelSet (id=InvoiceItemBreakdownPanelSet) at InvoiceItemBreakdownPanelSet.pcf: line 7, column 54
    function visible_12 () : java.lang.Boolean {
      return invoiceItem.BreakdownItems.HasElements
    }
    
    property get invoiceItem () : InvoiceItem {
      return getRequireValue("invoiceItem", 0) as InvoiceItem
    }
    
    property set invoiceItem ($arg :  InvoiceItem) {
      setRequireValue("invoiceItem", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/InvoiceItemBreakdownPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=Category_Input) at InvoiceItemBreakdownPanelSet.pcf: line 40, column 55
    function valueRoot_4 () : java.lang.Object {
      return category
    }
    
    // 'value' attribute on TextInput (id=Category_Input) at InvoiceItemBreakdownPanelSet.pcf: line 40, column 55
    function value_3 () : java.lang.String {
      return category.DisplayName
    }
    
    property get category () : entity.ChargeBreakdownCategory {
      return getIteratedValue(2) as entity.ChargeBreakdownCategory
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/InvoiceItemBreakdownPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends InvoiceItemBreakdownPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at InvoiceItemBreakdownPanelSet.pcf: line 48, column 42
    function currency_9 () : typekey.Currency {
      return item.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at InvoiceItemBreakdownPanelSet.pcf: line 48, column 42
    function valueRoot_8 () : java.lang.Object {
      return item
    }
    
    // 'value' attribute on InputIterator at InvoiceItemBreakdownPanelSet.pcf: line 37, column 88
    function value_6 () : java.util.List<entity.ChargeBreakdownCategory> {
      return item.Categories
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at InvoiceItemBreakdownPanelSet.pcf: line 48, column 42
    function value_7 () : gw.pl.currency.MonetaryAmount {
      return item.Amount
    }
    
    property get item () : InvoiceItemBreakdownItem {
      return getIteratedValue(1) as InvoiceItemBreakdownItem
    }
    
    
  }
  
  
}