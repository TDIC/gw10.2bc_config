package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerHoldStatementInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerHoldStatementInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerHoldStatementInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerHoldStatementInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=StatementHoldNegativeLimit_Input) at ProducerHoldStatementInputSet.pcf: line 25, column 41
    function currency_8 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'value' attribute on BooleanRadioInput (id=HoldStatement_Input) at ProducerHoldStatementInputSet.pcf: line 14, column 38
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      producer.HoldStatement = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=StatementHoldPositiveLimit_Input) at ProducerHoldStatementInputSet.pcf: line 34, column 41
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      producer.StatementHoldPositiveLimit = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=StatementHoldNegativeLimit_Input) at ProducerHoldStatementInputSet.pcf: line 25, column 41
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      producer.StatementHoldNegativeLimit = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on BooleanRadioInput (id=HoldStatement_Input) at ProducerHoldStatementInputSet.pcf: line 14, column 38
    function valueRoot_2 () : java.lang.Object {
      return producer
    }
    
    // 'value' attribute on BooleanRadioInput (id=HoldStatement_Input) at ProducerHoldStatementInputSet.pcf: line 14, column 38
    function value_0 () : java.lang.Boolean {
      return producer.HoldStatement
    }
    
    // 'value' attribute on MonetaryAmountInput (id=StatementHoldPositiveLimit_Input) at ProducerHoldStatementInputSet.pcf: line 34, column 41
    function value_12 () : gw.pl.currency.MonetaryAmount {
      return producer.StatementHoldPositiveLimit
    }
    
    // 'value' attribute on MonetaryAmountInput (id=StatementHoldNegativeLimit_Input) at ProducerHoldStatementInputSet.pcf: line 25, column 41
    function value_5 () : gw.pl.currency.MonetaryAmount {
      return producer.StatementHoldNegativeLimit
    }
    
    property get producer () : Producer {
      return getRequireValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setRequireValue("producer", 0, $arg)
    }
    
    
  }
  
  
}