package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyDistributionWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyDistributionWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer, moneyToEdit :  BaseMoneyReceived) : int {
      return 1
    }
    
    static function __constructorIndex (producer :  Producer, distributionType :  gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=AgencyDistributionWizard) at AgencyDistributionWizard.pcf: line 12, column 44
    function afterCancel_21 () : void {
      ProducerDetail.go(producer)
    }
    
    // 'afterCancel' attribute on Wizard (id=AgencyDistributionWizard) at AgencyDistributionWizard.pcf: line 12, column 44
    function afterCancel_dest_22 () : pcf.api.Destination {
      return pcf.ProducerDetail.createDestination(producer)
    }
    
    // 'afterFinish' attribute on Wizard (id=AgencyDistributionWizard) at AgencyDistributionWizard.pcf: line 12, column 44
    function afterFinish_29 () : void {
      ProducerDetail.go(producer)
    }
    
    // 'afterFinish' attribute on Wizard (id=AgencyDistributionWizard) at AgencyDistributionWizard.pcf: line 12, column 44
    function afterFinish_dest_30 () : pcf.api.Destination {
      return pcf.ProducerDetail.createDestination(producer)
    }
    
    // 'beforeCommit' attribute on Wizard (id=AgencyDistributionWizard) at AgencyDistributionWizard.pcf: line 12, column 44
    function beforeCommit_23 (pickedValue :  java.lang.Object) : void {
      wizardState.beforeCommit()
    }
    
    // 'cancelWarning' attribute on Wizard (id=AgencyDistributionWizard) at AgencyDistributionWizard.pcf: line 12, column 44
    function confirmMessage_20 () : java.lang.String {
      return DisplayKey.get("Web.AgencyDistributionWizard.CancelWarning", moneySetup.DistributionTypeName)
    }
    
    // 'infoBar' attribute on Wizard (id=AgencyDistributionWizard) at AgencyDistributionWizard.pcf: line 12, column 44
    function infoBar_onEnter_24 (def :  pcf.ProducerInfoBar) : void {
      def.onEnter(producer)
    }
    
    // 'infoBar' attribute on Wizard (id=AgencyDistributionWizard) at AgencyDistributionWizard.pcf: line 12, column 44
    function infoBar_refreshVariables_25 (def :  pcf.ProducerInfoBar) : void {
      def.refreshVariables(producer)
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard.pcf: line 20, column 75
    function initialValue_0 () : gw.api.web.producer.agencybill.AgencyBillMoneyReceivedSetup {
      return gw.agencybill.AgencyDistributionWizardHelper.createMoneySetup(producer, CurrentLocation, distributionType, moneyToEdit)
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard.pcf: line 33, column 60
    function initialValue_1 () : gw.agencybill.AgencyDistributionWizardHelper {
      return new gw.agencybill.AgencyDistributionWizardHelper(moneySetup)
    }
    
    // 'label' attribute on WizardStep (id=MoneyDetails) at AgencyDistributionWizard.pcf: line 39, column 34
    function label_2 () : java.lang.String {
      return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.Label", moneySetup.DistributionTypeName)
    }
    
    // 'onExit' attribute on WizardStep (id=Distribution) at AgencyDistributionWizard.pcf: line 54, column 120
    function onExit_12 () : void {
      wizardState.validateFinishDistributionStep()
    }
    
    // 'onExit' attribute on WizardStep (id=MoneyDetails) at AgencyDistributionWizard.pcf: line 39, column 34
    function onExit_3 () : void {
      wizardState.onExitMoneyDetailsScreen()
    }
    
    // 'parent' attribute on Wizard (id=AgencyDistributionWizard) at AgencyDistributionWizard.pcf: line 12, column 44
    static function parent_26 (distributionType :  gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum, moneyToEdit :  BaseMoneyReceived, producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    // 'screen' attribute on WizardStep (id=Distribution) at AgencyDistributionWizard.pcf: line 54, column 120
    function screen_onEnter_13 (def :  pcf.AgencyDistributionWizard_DistributionScreen) : void {
      def.onEnter(wizardState)
    }
    
    // 'screen' attribute on WizardStep (id=Dispositions) at AgencyDistributionWizard.pcf: line 62, column 50
    function screen_onEnter_17 (def :  pcf.AgencyDistributionWizard_DispositionsScreen) : void {
      def.onEnter(wizardState)
    }
    
    // 'screen' attribute on WizardStep (id=MoneyDetails) at AgencyDistributionWizard.pcf: line 39, column 34
    function screen_onEnter_4 (def :  pcf.AgencyDistributionWizard_MoneyDetailsScreen) : void {
      def.onEnter(wizardState)
    }
    
    // 'screen' attribute on WizardStep (id=ReceivePaymentConfirmation) at AgencyDistributionWizard.pcf: line 47, column 49
    function screen_onEnter_8 (def :  pcf.AgencyDistributionWizard_ReceivePaymentConfirmationScreen) : void {
      def.onEnter(wizardState.PaymentMoneySetup.Money)
    }
    
    // 'screen' attribute on WizardStep (id=Distribution) at AgencyDistributionWizard.pcf: line 54, column 120
    function screen_refreshVariables_14 (def :  pcf.AgencyDistributionWizard_DistributionScreen) : void {
      def.refreshVariables(wizardState)
    }
    
    // 'screen' attribute on WizardStep (id=Dispositions) at AgencyDistributionWizard.pcf: line 62, column 50
    function screen_refreshVariables_18 (def :  pcf.AgencyDistributionWizard_DispositionsScreen) : void {
      def.refreshVariables(wizardState)
    }
    
    // 'screen' attribute on WizardStep (id=MoneyDetails) at AgencyDistributionWizard.pcf: line 39, column 34
    function screen_refreshVariables_5 (def :  pcf.AgencyDistributionWizard_MoneyDetailsScreen) : void {
      def.refreshVariables(wizardState)
    }
    
    // 'screen' attribute on WizardStep (id=ReceivePaymentConfirmation) at AgencyDistributionWizard.pcf: line 47, column 49
    function screen_refreshVariables_9 (def :  pcf.AgencyDistributionWizard_ReceivePaymentConfirmationScreen) : void {
      def.refreshVariables(wizardState.PaymentMoneySetup.Money)
    }
    
    // 'tabBar' attribute on Wizard (id=AgencyDistributionWizard) at AgencyDistributionWizard.pcf: line 12, column 44
    function tabBar_onEnter_27 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AgencyDistributionWizard) at AgencyDistributionWizard.pcf: line 12, column 44
    function tabBar_refreshVariables_28 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    // 'title' attribute on WizardStep (id=Distribution) at AgencyDistributionWizard.pcf: line 54, column 120
    function title_15 () : java.lang.String {
      return DistributionTitle
    }
    
    // 'title' attribute on WizardStep (id=Dispositions) at AgencyDistributionWizard.pcf: line 62, column 50
    function title_19 () : java.lang.String {
      return DispositionsTitle
    }
    
    // 'title' attribute on WizardStep (id=MoneyDetails) at AgencyDistributionWizard.pcf: line 39, column 34
    function title_6 () : java.lang.String {
      return MoneyDetailsTitle
    }
    
    // 'visible' attribute on WizardStep (id=Distribution) at AgencyDistributionWizard.pcf: line 54, column 120
    function visible_11 () : java.lang.Boolean {
      return !wizardState.DoNotDistributeNow && !wizardState.SelectedDistributeNetOwedAmountsAutomatically
    }
    
    // 'visible' attribute on WizardStep (id=Dispositions) at AgencyDistributionWizard.pcf: line 62, column 50
    function visible_16 () : java.lang.Boolean {
      return !wizardState.DoNotDistributeNow
    }
    
    // 'visible' attribute on WizardStep (id=ReceivePaymentConfirmation) at AgencyDistributionWizard.pcf: line 47, column 49
    function visible_7 () : java.lang.Boolean {
      return wizardState.DoNotDistributeNow
    }
    
    override property get CurrentLocation () : pcf.AgencyDistributionWizard {
      return super.CurrentLocation as pcf.AgencyDistributionWizard
    }
    
    property get distributionType () : gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum {
      return getVariableValue("distributionType", 0) as gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum
    }
    
    property set distributionType ($arg :  gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum) {
      setVariableValue("distributionType", 0, $arg)
    }
    
    property get moneySetup () : gw.api.web.producer.agencybill.AgencyBillMoneyReceivedSetup {
      return getVariableValue("moneySetup", 0) as gw.api.web.producer.agencybill.AgencyBillMoneyReceivedSetup
    }
    
    property set moneySetup ($arg :  gw.api.web.producer.agencybill.AgencyBillMoneyReceivedSetup) {
      setVariableValue("moneySetup", 0, $arg)
    }
    
    property get moneyToEdit () : BaseMoneyReceived {
      return getVariableValue("moneyToEdit", 0) as BaseMoneyReceived
    }
    
    property set moneyToEdit ($arg :  BaseMoneyReceived) {
      setVariableValue("moneyToEdit", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    property get wizardState () : gw.agencybill.AgencyDistributionWizardHelper {
      return getVariableValue("wizardState", 0) as gw.agencybill.AgencyDistributionWizardHelper
    }
    
    property set wizardState ($arg :  gw.agencybill.AgencyDistributionWizardHelper) {
      setVariableValue("wizardState", 0, $arg)
    }
    
    property get DispositionsTitle() : String {
      if (wizardState.IsCreditDistribution) {
        return DisplayKey.get("Web.AgencyDistributionWizard.Step.Dispositions.Title.CreditDistribution")
      } else if (wizardState.IsPayment) {
        return DisplayKey.get("Web.AgencyDistributionWizard.Step.Dispositions.Title.Payment")
      } else {
        return DisplayKey.get("Web.AgencyDistributionWizard.Step.Dispositions.Title.Promise")
      }
    }
    
    property get MoneyDetailsTitle() : String {
      if (wizardState.IsCreditDistribution) {
        return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.Title.CreditDistribution")
      } else if (wizardState.IsPayment) {
        return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.Title.Payment")
      } else {
        return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.Title.Promise")
      }
    }
    
    property get DistributionTitle() : String {
      if (wizardState.IsCreditDistribution) {
        return DisplayKey.get("Web.AgencyDistributionWizard.Step.Distribution.Title.CreditDistribution")
      } else if (wizardState.IsPayment) {
        return DisplayKey.get("Web.AgencyDistributionWizard.Step.Distribution.Title.Payment")
      } else {
        return DisplayKey.get("Web.AgencyDistributionWizard.Step.Distribution.Title.Promise")
      }
    }
    
    
  }
  
  
}