package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewFundsTransferReversalConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewFundsTransferReversalConfirmationScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewFundsTransferReversalConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewFundsTransferReversalConfirmationScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=amount_Input) at NewFundsTransferReversalConfirmationScreen.pcf: line 42, column 72
    function currency_13 () : typekey.Currency {
      return transferFundsContext.TransferFundTransaction.Currency
    }
    
    // 'value' attribute on TypeKeyInput (id=reason_Input) at NewFundsTransferReversalConfirmationScreen.pcf: line 49, column 46
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      reversalReason = (__VALUE_TO_SET as typekey.ReversalReason)
    }
    
    // 'onChange' attribute on PostOnChange at NewFundsTransferReversalConfirmationScreen.pcf: line 51, column 82
    function onChange_15 () : void {
      transferFundsContext.setReversalReason( reversalReason )
    }
    
    // 'value' attribute on TextInput (id=transactionNumber_Input) at NewFundsTransferReversalConfirmationScreen.pcf: line 25, column 83
    function valueRoot_2 () : java.lang.Object {
      return transferFundsContext.TransferFundTransaction
    }
    
    // 'value' attribute on TextInput (id=transactionNumber_Input) at NewFundsTransferReversalConfirmationScreen.pcf: line 25, column 83
    function value_1 () : java.lang.String {
      return transferFundsContext.TransferFundTransaction.TransactionNumber
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amount_Input) at NewFundsTransferReversalConfirmationScreen.pcf: line 42, column 72
    function value_11 () : gw.pl.currency.MonetaryAmount {
      return transferFundsContext.TransferFundTransaction.Amount
    }
    
    // 'value' attribute on TypeKeyInput (id=reason_Input) at NewFundsTransferReversalConfirmationScreen.pcf: line 49, column 46
    function value_16 () : typekey.ReversalReason {
      return reversalReason
    }
    
    // 'value' attribute on DateInput (id=transactionDate_Input) at NewFundsTransferReversalConfirmationScreen.pcf: line 29, column 81
    function value_4 () : java.util.Date {
      return transferFundsContext.TransferFundTransaction.TransactionDate
    }
    
    // 'value' attribute on TextInput (id=source_Input) at NewFundsTransferReversalConfirmationScreen.pcf: line 33, column 57
    function value_7 () : java.lang.String {
      return transferFundsContext.getSourceName()
    }
    
    // 'value' attribute on TextInput (id=destination_Input) at NewFundsTransferReversalConfirmationScreen.pcf: line 37, column 62
    function value_9 () : java.lang.String {
      return transferFundsContext.getDestinationName()
    }
    
    // 'visible' attribute on AlertBar (id=approvalActivityAlertBar) at NewFundsTransferReversalConfirmationScreen.pcf: line 19, column 91
    function visible_0 () : java.lang.Boolean {
      return transferFundsContext.FundsTraversalReversal.OpenApprovalActivity != null
    }
    
    property get reversalReason () : ReversalReason {
      return getVariableValue("reversalReason", 0) as ReversalReason
    }
    
    property set reversalReason ($arg :  ReversalReason) {
      setVariableValue("reversalReason", 0, $arg)
    }
    
    property get transferFundsContext () : gw.transaction.TransferFundsReversalWizardContext {
      return getRequireValue("transferFundsContext", 0) as gw.transaction.TransferFundsReversalWizardContext
    }
    
    property set transferFundsContext ($arg :  gw.transaction.TransferFundsReversalWizardContext) {
      setRequireValue("transferFundsContext", 0, $arg)
    }
    
    
  }
  
  
}