package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/ApprovalActivityDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ApprovalActivityDVExpressions {
  @javax.annotation.Generated("config/web/pcf/activity/ApprovalActivityDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ApprovalActivityDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at ApprovalActivityDV.pcf: line 35, column 30
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      approvalActivity.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Priority_Input) at ApprovalActivityDV.pcf: line 42, column 30
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      approvalActivity.Priority = (__VALUE_TO_SET as typekey.Priority)
    }
    
    // 'value' attribute on DateInput (id=TargetDate_Input) at ApprovalActivityDV.pcf: line 48, column 30
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      approvalActivity.TargetDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=EscalationDate_Input) at ApprovalActivityDV.pcf: line 54, column 30
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      approvalActivity.EscalationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'initialValue' attribute on Variable at ApprovalActivityDV.pcf: line 14, column 23
    function initialValue_0 () : boolean {
      return approvalActivity != null
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at ApprovalActivityDV.pcf: line 28, column 30
    function valueRoot_5 () : java.lang.Object {
      return approvalActivity
    }
    
    // 'value' attribute on TypeKeyInput (id=Priority_Input) at ApprovalActivityDV.pcf: line 42, column 30
    function value_15 () : typekey.Priority {
      return approvalActivity.Priority
    }
    
    // 'value' attribute on DateInput (id=TargetDate_Input) at ApprovalActivityDV.pcf: line 48, column 30
    function value_21 () : java.util.Date {
      return approvalActivity.TargetDate
    }
    
    // 'value' attribute on DateInput (id=EscalationDate_Input) at ApprovalActivityDV.pcf: line 54, column 30
    function value_27 () : java.util.Date {
      return approvalActivity.EscalationDate
    }
    
    // 'value' attribute on TextInput (id=AssignedTo_Input) at ApprovalActivityDV.pcf: line 60, column 30
    function value_33 () : java.lang.Object {
      return assignGroupToActivity()
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at ApprovalActivityDV.pcf: line 28, column 30
    function value_4 () : java.lang.String {
      return approvalActivity.Subject
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at ApprovalActivityDV.pcf: line 35, column 30
    function value_9 () : java.lang.String {
      return approvalActivity.Description
    }
    
    // 'visible' attribute on Label (id=Label) at ApprovalActivityDV.pcf: line 19, column 30
    function visible_1 () : java.lang.Boolean {
      return isVisible
    }
    
    property get approvalActivity () : Activity {
      return getRequireValue("approvalActivity", 0) as Activity
    }
    
    property set approvalActivity ($arg :  Activity) {
      setRequireValue("approvalActivity", 0, $arg)
    }
    
    property get isVisible () : boolean {
      return getVariableValue("isVisible", 0) as java.lang.Boolean
    }
    
    property set isVisible ($arg :  boolean) {
      setVariableValue("isVisible", 0, $arg)
    }
    
    // Assign all aprroval activities to the Approve Transactions Queue except Disbursement Activities.
    function assignGroupToActivity() : AssignableQueue {
      if(not (approvalActivity typeis DisbApprActivity)) {
        var groupName = gw.command.demo.GeneralUtil.findGroupByUserName("Approve Transactions")
        if (approvalActivity.assignGroup(groupName)) {
          var queueName = approvalActivity.AssignedGroup.getQueue("Approve Transactions")
          if (queueName != null) {
            approvalActivity.setAssignedQueue(queueName)
          }
        }
      }
      return approvalActivity.AssignedQueue
    }
    
    
  }
  
  
}