package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_DetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyDistributionWizard_DetailsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_DetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyDistributionWizard_DetailsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (chargeOwnerView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView, availableAmount :  gw.pl.currency.MonetaryAmount, wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=Autodistribute) at AgencyDistributionWizard_DetailsPopup.pcf: line 71, column 107
    function action_24 () : void {
      gw.plugin.Plugins.get(gw.plugin.agencybill.IAgencyCycleDist).distributeNetAmount(wizardState.distItemsExcludingFrozen(chargeOwnerView), wizardState.amountAvailableToDistributeExcludingFrozen(availableAmount, chargeOwnerView))
    }
    
    // 'action' attribute on ToolbarButton (id=ClearAllAmounts) at AgencyDistributionWizard_DetailsPopup.pcf: line 76, column 108
    function action_26 () : void {
      clearAllAmounts()
    }
    
    // 'available' attribute on ToolbarButton (id=ClearAllAmounts) at AgencyDistributionWizard_DetailsPopup.pcf: line 76, column 108
    function available_25 () : java.lang.Boolean {
      return !wizardState.hasFrozenDistItem(chargeOwnerView)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Available_Input) at AgencyDistributionWizard_DetailsPopup.pcf: line 51, column 38
    function currency_15 () : typekey.Currency {
      return chargeOwnerView.Account.Currency
    }
    
    // EditButtons at AgencyDistributionWizard_DetailsPopup.pcf: line 24, column 31
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'label' attribute on Label at AgencyDistributionWizard_DetailsPopup.pcf: line 29, column 154
    function label_1 () : java.lang.String {
      return DisplayKey.get("Web.AgencyDistributionWizard.DetailsPopup.DistributionInfo", wizardState.MoneySetup.DistributionTypeName)
    }
    
    // 'label' attribute on Label at AgencyDistributionWizard_DetailsPopup.pcf: line 45, column 145
    function label_13 () : java.lang.String {
      return DisplayKey.get("Web.AgencyDistributionWizard.DetailsPopup.ThisChargeOwner", chargeOwnerView.ChargeOwnerTypeName)
    }
    
    // 'label' attribute on TextInput (id=PolicyNumber_Input) at AgencyDistributionWizard_DetailsPopup.pcf: line 35, column 70
    function label_3 () : java.lang.Object {
      return chargeOwnerView.ChargeOwner typeis PolicyPeriod ? DisplayKey.get("Web.AgencyDistributionWizard.DetailsPopup.PolicyNumber") : DisplayKey.get("Web.AgencyDistributionWizard.DetailsPopup.ChargeOwner")
    }
    
    // 'value' attribute on TextCell (id=LineItemNumber_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 106, column 48
    function sortValue_27 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : entity.InvoiceItem = (agencyDistItem.InvoiceItem)
var netToApply : gw.pl.currency.MonetaryAmount = (agencyDistItem.NetAmountToApply)
var percentToApply : java.math.BigDecimal = (gw.agencybill.CommissionCalculator.getCommissionPercentToApply(agencyDistItem))
return invoiceItem.LineItemNumber
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 112, column 60
    function sortValue_28 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : entity.InvoiceItem = (agencyDistItem.InvoiceItem)
var netToApply : gw.pl.currency.MonetaryAmount = (agencyDistItem.NetAmountToApply)
var percentToApply : java.math.BigDecimal = (gw.agencybill.CommissionCalculator.getCommissionPercentToApply(agencyDistItem))
return invoiceItem.Invoice.InvoiceNumber
    }
    
    // 'value' attribute on DateCell (id=StatementDate_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 116, column 56
    function sortValue_29 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : entity.InvoiceItem = (agencyDistItem.InvoiceItem)
var netToApply : gw.pl.currency.MonetaryAmount = (agencyDistItem.NetAmountToApply)
var percentToApply : java.math.BigDecimal = (gw.agencybill.CommissionCalculator.getCommissionPercentToApply(agencyDistItem))
return invoiceItem.Invoice.EventDate
    }
    
    // 'value' attribute on TextCell (id=ItemDescription_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 122, column 31
    function sortValue_30 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : entity.InvoiceItem = (agencyDistItem.InvoiceItem)
var netToApply : gw.pl.currency.MonetaryAmount = (agencyDistItem.NetAmountToApply)
var percentToApply : java.math.BigDecimal = (gw.agencybill.CommissionCalculator.getCommissionPercentToApply(agencyDistItem))
return invoiceItem.DisplayNameAsItemType
    }
    
    // 'value' attribute on TextCell (id=Context_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 127, column 56
    function sortValue_31 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : entity.InvoiceItem = (agencyDistItem.InvoiceItem)
var netToApply : gw.pl.currency.MonetaryAmount = (agencyDistItem.NetAmountToApply)
var percentToApply : java.math.BigDecimal = (gw.agencybill.CommissionCalculator.getCommissionPercentToApply(agencyDistItem))
return invoiceItem.Charge.BillingInstruction
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossOwed_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 134, column 57
    function sortValue_32 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return agencyDistItem.GrossAmountOwed
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionOwed_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 141, column 62
    function sortValue_33 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return agencyDistItem.CommissionAmountOwed
    }
    
    // 'value' attribute on TextCell (id=PercentOwed_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 149, column 51
    function sortValue_34 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return gw.agencybill.CommissionCalculator.getCommissionPercent(agencyDistItem)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetOwed_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 156, column 55
    function sortValue_35 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return agencyDistItem.NetAmountOwed
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 180, column 59
    function sortValue_36 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return agencyDistItem.GrossAmountToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 194, column 64
    function sortValue_37 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return agencyDistItem.CommissionAmountToApply
    }
    
    // 'value' attribute on TextCell (id=PercentToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 206, column 50
    function sortValue_38 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : entity.InvoiceItem = (agencyDistItem.InvoiceItem)
var netToApply : gw.pl.currency.MonetaryAmount = (agencyDistItem.NetAmountToApply)
var percentToApply : java.math.BigDecimal = (gw.agencybill.CommissionCalculator.getCommissionPercentToApply(agencyDistItem))
return percentToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 219, column 36
    function sortValue_39 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : entity.InvoiceItem = (agencyDistItem.InvoiceItem)
var netToApply : gw.pl.currency.MonetaryAmount = (agencyDistItem.NetAmountToApply)
var percentToApply : java.math.BigDecimal = (gw.agencybill.CommissionCalculator.getCommissionPercentToApply(agencyDistItem))
return netToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Difference_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 230, column 89
    function sortValue_40 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return agencyDistItem.NetAmountOwed - agencyDistItem.NetAmountToApply
    }
    
    // 'value' attribute on RangeCell (id=Disposition_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 239, column 92
    function sortValue_41 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return agencyDistItem.Disposition
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 244, column 57
    function sortValue_42 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return agencyDistItem.PaymentComments
    }
    
    // '$$sumValue' attribute on RowIterator at AgencyDistributionWizard_DetailsPopup.pcf: line 134, column 57
    function sumValueRoot_44 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return agencyDistItem
    }
    
    // 'footerSumValue' attribute on RowIterator at AgencyDistributionWizard_DetailsPopup.pcf: line 134, column 57
    function sumValue_43 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return agencyDistItem.GrossAmountOwed
    }
    
    // 'footerSumValue' attribute on RowIterator at AgencyDistributionWizard_DetailsPopup.pcf: line 141, column 62
    function sumValue_45 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return agencyDistItem.CommissionAmountOwed
    }
    
    // 'footerSumValue' attribute on RowIterator at AgencyDistributionWizard_DetailsPopup.pcf: line 156, column 55
    function sumValue_47 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return agencyDistItem.NetAmountOwed
    }
    
    // 'footerSumValue' attribute on RowIterator at AgencyDistributionWizard_DetailsPopup.pcf: line 180, column 59
    function sumValue_49 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return agencyDistItem.GrossAmountToApply
    }
    
    // 'footerSumValue' attribute on RowIterator at AgencyDistributionWizard_DetailsPopup.pcf: line 194, column 64
    function sumValue_51 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return agencyDistItem.CommissionAmountToApply
    }
    
    // 'footerSumValue' attribute on RowIterator at AgencyDistributionWizard_DetailsPopup.pcf: line 219, column 36
    function sumValue_53 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return agencyDistItem.NetAmountToApply
    }
    
    // 'footerSumValue' attribute on RowIterator at AgencyDistributionWizard_DetailsPopup.pcf: line 230, column 89
    function sumValue_55 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return agencyDistItem.NetAmountOwed - agencyDistItem.NetAmountToApply
    }
    
    // 'title' attribute on Popup (id=AgencyDistributionWizard_DetailsPopup) at AgencyDistributionWizard_DetailsPopup.pcf: line 9, column 58
    static function title_149 (availableAmount :  gw.pl.currency.MonetaryAmount, chargeOwnerView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView, wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : java.lang.Object {
      return getWizardTitle(chargeOwnerView, wizardState)
    }
    
    // 'value' attribute on TextInput (id=OwnerAccount_Input) at AgencyDistributionWizard_DetailsPopup.pcf: line 41, column 118
    function valueRoot_10 () : java.lang.Object {
      return chargeOwnerView
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Available_Input) at AgencyDistributionWizard_DetailsPopup.pcf: line 51, column 38
    function value_14 () : gw.pl.currency.MonetaryAmount {
      return availableAmount
    }
    
    // 'value' attribute on RowIterator at AgencyDistributionWizard_DetailsPopup.pcf: line 85, column 67
    function value_148 () : java.util.List<entity.BaseDistItem> {
      return chargeOwnerView.AgencyDistItems
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Distributed_Input) at AgencyDistributionWizard_DetailsPopup.pcf: line 57, column 65
    function value_17 () : gw.pl.currency.MonetaryAmount {
      return chargeOwnerView.TotalNetAmountToDistribute
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Remaining_Input) at AgencyDistributionWizard_DetailsPopup.pcf: line 63, column 84
    function value_21 () : gw.pl.currency.MonetaryAmount {
      return wizardState.remainingAmount(availableAmount, chargeOwnerView)
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at AgencyDistributionWizard_DetailsPopup.pcf: line 35, column 70
    function value_4 () : java.lang.Comparable<java.lang.Object> {
      return chargeOwnerView.ChargeOwner typeis PolicyPeriod ? chargeOwnerView.ChargeOwner : chargeOwnerView.DisplayName
    }
    
    // 'value' attribute on TextInput (id=OwnerAccount_Input) at AgencyDistributionWizard_DetailsPopup.pcf: line 41, column 118
    function value_9 () : entity.Account {
      return chargeOwnerView.Account
    }
    
    // 'visible' attribute on TextInput (id=PolicyNumber_Input) at AgencyDistributionWizard_DetailsPopup.pcf: line 35, column 70
    function visible_2 () : java.lang.Boolean {
      return !(chargeOwnerView.ChargeOwner typeis Account)
    }
    
    // 'visible' attribute on TextInput (id=OwnerAccount_Input) at AgencyDistributionWizard_DetailsPopup.pcf: line 41, column 118
    function visible_8 () : java.lang.Boolean {
      return chargeOwnerView.ChargeOwner typeis PolicyPeriod || chargeOwnerView.ChargeOwner typeis Account
    }
    
    override property get CurrentLocation () : pcf.AgencyDistributionWizard_DetailsPopup {
      return super.CurrentLocation as pcf.AgencyDistributionWizard_DetailsPopup
    }
    
    property get availableAmount () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("availableAmount", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set availableAmount ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("availableAmount", 0, $arg)
    }
    
    property get chargeOwnerView () : gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView {
      return getVariableValue("chargeOwnerView", 0) as gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView
    }
    
    property set chargeOwnerView ($arg :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) {
      setVariableValue("chargeOwnerView", 0, $arg)
    }
    
    property get wizardState () : gw.agencybill.AgencyDistributionWizardHelper {
      return getVariableValue("wizardState", 0) as gw.agencybill.AgencyDistributionWizardHelper
    }
    
    property set wizardState ($arg :  gw.agencybill.AgencyDistributionWizardHelper) {
      setVariableValue("wizardState", 0, $arg)
    }
    
    function clearAllAmounts() {
      for (var distItem in chargeOwnerView.AgencyDistItems) {
        distItem.clear()
      }
    }
    
    static function getWizardTitle(view : gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView,
                             wizardHelper: gw.agencybill.AgencyDistributionWizardHelper) : String {
      if (wizardHelper.IsPayment) {
        if (view.ChargeOwner typeis PolicyPeriod) {
          return DisplayKey.get("Web.AgencyDistributionWizard.DetailsPopup.Title.Payment.PolicyPeriod")
        } else if (view.ChargeOwner typeis Collateral) {
          return DisplayKey.get("Web.AgencyDistributionWizard.DetailsPopup.Title.Payment.Collateral")
        } else if (view.ChargeOwner typeis CollateralRequirement) {
          return DisplayKey.get("Web.AgencyDistributionWizard.DetailsPopup.Title.Payment.CollateralRequirement")
        } else {
          return DisplayKey.get("Web.AgencyDistributionWizard.DetailsPopup.Title.Payment.Account")
        }
      } else if (wizardHelper.IsPromise) {
        if (view.ChargeOwner typeis PolicyPeriod) {
          return DisplayKey.get("Web.AgencyDistributionWizard.DetailsPopup.Title.Promise.PolicyPeriod")
        } else if (view.ChargeOwner typeis Collateral) {
          return DisplayKey.get("Web.AgencyDistributionWizard.DetailsPopup.Title.Promise.Collateral")
        } else if (view.ChargeOwner typeis CollateralRequirement) {
          return DisplayKey.get("Web.AgencyDistributionWizard.DetailsPopup.Title.Promise.CollateralRequirement")
        } else {
          return DisplayKey.get("Web.AgencyDistributionWizard.DetailsPopup.Title.Promise.Account")
        }
      } else {
        if (view.ChargeOwner typeis PolicyPeriod) {
          return DisplayKey.get("Web.AgencyDistributionWizard.DetailsPopup.Title.CreditDistribution.PolicyPeriod")
        } else if (view.ChargeOwner typeis Collateral) {
          return DisplayKey.get("Web.AgencyDistributionWizard.DetailsPopup.Title.CreditDistribution.Collateral")
        } else if (view.ChargeOwner typeis CollateralRequirement) {
          return DisplayKey.get("Web.AgencyDistributionWizard.DetailsPopup.Title.CreditDistribution.CollateralRequirement")
        } else {
          return DisplayKey.get("Web.AgencyDistributionWizard.DetailsPopup.Title.CreditDistribution.Account")
        }
      }
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_DetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AgencyDistributionWizard_DetailsPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 112, column 60
    function actionAvailable_63 () : java.lang.Boolean {
      return invoiceItem.AgencyBill
    }
    
    // 'action' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 112, column 60
    function action_62 () : void {
      invoiceItem.StatementInvoiceDetailViewAction()
    }
    
    // 'action' attribute on TextCell (id=ItemDescription_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 122, column 31
    function action_70 () : void {
      InvoiceItemHistoryPopup.push(invoiceItem)
    }
    
    // 'action' attribute on Link (id=FillDefaultsButton) at AgencyDistributionWizard_DetailsPopup.pcf: line 169, column 136
    function action_95 () : void {
      agencyDistItem.fillUnpaidAmounts()
    }
    
    // 'action' attribute on TextCell (id=ItemDescription_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 122, column 31
    function action_dest_71 () : pcf.api.Destination {
      return pcf.InvoiceItemHistoryPopup.createDestination(invoiceItem)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=GrossOwed_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 134, column 57
    function currency_80 () : typekey.Currency {
      return agencyDistItem.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 180, column 59
    function defaultSetter_100 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyDistItem.GrossAmountToApply = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 194, column 64
    function defaultSetter_110 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyDistItem.CommissionAmountToApply = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextCell (id=PercentToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 206, column 50
    function defaultSetter_120 (__VALUE_TO_SET :  java.lang.Object) : void {
      percentToApply = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 219, column 36
    function defaultSetter_127 (__VALUE_TO_SET :  java.lang.Object) : void {
      netToApply = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on RangeCell (id=Disposition_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 239, column 92
    function defaultSetter_136 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyDistItem.Disposition = (__VALUE_TO_SET as typekey.DistItemDisposition)
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 244, column 57
    function defaultSetter_143 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyDistItem.PaymentComments = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on MonetaryAmountCell (id=CommissionToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 194, column 64
    function editable_106 () : java.lang.Boolean {
      return agencyDistItem.InvoiceItem.PolicyPeriodItem && agencyDistItem.CommissionModifiable
    }
    
    // 'editable' attribute on MonetaryAmountCell (id=NetToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 219, column 36
    function editable_124 () : java.lang.Boolean {
      return agencyDistItem.CommissionModifiable
    }
    
    // 'editable' attribute on Row at AgencyDistributionWizard_DetailsPopup.pcf: line 101, column 116
    function editable_146 () : java.lang.Boolean {
      return !invoiceItem.Frozen && invoiceItem.Type != InvoiceItemType.TC_COMMISSIONREMAINDER
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_DetailsPopup.pcf: line 89, column 42
    function initialValue_56 () : entity.InvoiceItem {
      return agencyDistItem.InvoiceItem
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_DetailsPopup.pcf: line 94, column 53
    function initialValue_57 () : gw.pl.currency.MonetaryAmount {
      return agencyDistItem.NetAmountToApply
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_DetailsPopup.pcf: line 99, column 44
    function initialValue_58 () : java.math.BigDecimal {
      return gw.agencybill.CommissionCalculator.getCommissionPercentToApply(agencyDistItem)
    }
    
    // RowIterator at AgencyDistributionWizard_DetailsPopup.pcf: line 85, column 67
    function initializeVariables_147 () : void {
        invoiceItem = agencyDistItem.InvoiceItem;
  netToApply = agencyDistItem.NetAmountToApply;
  percentToApply = gw.agencybill.CommissionCalculator.getCommissionPercentToApply(agencyDistItem);

    }
    
    // 'inputConversion' attribute on TextCell (id=PercentToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 206, column 50
    function inputConversion_118 (VALUE :  java.lang.String) : java.lang.Object {
      return new java.math.BigDecimal(VALUE == null ? "0" : VALUE)
    }
    
    // 'inputConversion' attribute on MonetaryAmountCell (id=GrossToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 180, column 59
    function inputConversion_98 (VALUE :  java.lang.String) : java.lang.Object {
      return new java.math.BigDecimal(VALUE ?: "0").ofCurrency(availableAmount.Currency)
    }
    
    // 'onChange' attribute on PostOnChange at AgencyDistributionWizard_DetailsPopup.pcf: line 197, column 215
    function onChange_105 () : void {
      if (agencyDistItem.CommissionAmountToApply == null) agencyDistItem.CommissionAmountToApply = 0bd.ofCurrency(availableAmount.Currency) // TODO Per Jira BC-11026, remove this expression
    }
    
    // 'onChange' attribute on PostOnChange at AgencyDistributionWizard_DetailsPopup.pcf: line 209, column 147
    function onChange_116 () : void {
      gw.agencybill.CommissionCalculator.convertPercentToApplyToGrossAndCommissionToApply(percentToApply, agencyDistItem)
    }
    
    // 'onChange' attribute on PostOnChange at AgencyDistributionWizard_DetailsPopup.pcf: line 222, column 139
    function onChange_123 () : void {
      gw.agencybill.CommissionCalculator.convertNetToApplyToGrossAndCommissionToApply(netToApply, agencyDistItem)
    }
    
    // 'onChange' attribute on PostOnChange at AgencyDistributionWizard_DetailsPopup.pcf: line 183, column 254
    function onChange_96 () : void {
      if (agencyDistItem.GrossAmountToApply == null) agencyDistItem.GrossAmountToApply = 0bd.ofCurrency(availableAmount.Currency); gw.agencybill.CommissionCalculator.convertGrossToApplyToGrossAndCommissionToApply(agencyDistItem)
    }
    
    // 'validationExpression' attribute on MonetaryAmountCell (id=CommissionToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 194, column 64
    function validationExpression_107 () : java.lang.Object {
      return wizardState.isCommissionAmountToApplyInvalid(agencyDistItem)?  DisplayKey.get("Java.Error.BaseDist.InvalidCommissionDistributionAmount") : null
    }
    
    // 'validationExpression' attribute on TextCell (id=PercentOwed_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 149, column 51
    function validationExpression_86 () : java.lang.Object {
      return agencyDistItem.GrossAmountOwed.IsNotZero && (percentToApply < 0 || percentToApply > 100)? DisplayKey.get("Java.Error.AgencyCycleDistChargeOwnerView.InvalidCommissionAmount"): null
    }
    
    // 'validationExpression' attribute on MonetaryAmountCell (id=GrossToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 180, column 59
    function validationExpression_97 () : java.lang.Object {
      return wizardState.isGrossAmountToApplyInvalid(agencyDistItem) ?  DisplayKey.get("Java.Error.BaseDist.InvalidGrossDistributionAmount") : null
    }
    
    // 'valueRange' attribute on RangeCell (id=Disposition_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 239, column 92
    function valueRange_138 () : java.lang.Object {
      return DistItemDisposition.getTypeKeys( false )
    }
    
    // 'value' attribute on TextCell (id=LineItemNumber_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 106, column 48
    function valueRoot_60 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 112, column 60
    function valueRoot_65 () : java.lang.Object {
      return invoiceItem.Invoice
    }
    
    // 'value' attribute on TextCell (id=Context_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 127, column 56
    function valueRoot_76 () : java.lang.Object {
      return invoiceItem.Charge
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossOwed_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 134, column 57
    function valueRoot_79 () : java.lang.Object {
      return agencyDistItem
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 194, column 64
    function value_109 () : gw.pl.currency.MonetaryAmount {
      return agencyDistItem.CommissionAmountToApply
    }
    
    // 'value' attribute on TextCell (id=PercentToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 206, column 50
    function value_119 () : java.math.BigDecimal {
      return percentToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 219, column 36
    function value_126 () : gw.pl.currency.MonetaryAmount {
      return netToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Difference_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 230, column 89
    function value_131 () : gw.pl.currency.MonetaryAmount {
      return agencyDistItem.NetAmountOwed - agencyDistItem.NetAmountToApply
    }
    
    // 'value' attribute on RangeCell (id=Disposition_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 239, column 92
    function value_135 () : typekey.DistItemDisposition {
      return agencyDistItem.Disposition
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 244, column 57
    function value_142 () : java.lang.String {
      return agencyDistItem.PaymentComments
    }
    
    // 'value' attribute on TextCell (id=LineItemNumber_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 106, column 48
    function value_59 () : java.lang.Integer {
      return invoiceItem.LineItemNumber
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 112, column 60
    function value_64 () : java.lang.String {
      return invoiceItem.Invoice.InvoiceNumber
    }
    
    // 'value' attribute on DateCell (id=StatementDate_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 116, column 56
    function value_67 () : java.util.Date {
      return invoiceItem.Invoice.EventDate
    }
    
    // 'value' attribute on TextCell (id=ItemDescription_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 122, column 31
    function value_72 () : java.lang.String {
      return invoiceItem.DisplayNameAsItemType
    }
    
    // 'value' attribute on TextCell (id=Context_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 127, column 56
    function value_75 () : entity.BillingInstruction {
      return invoiceItem.Charge.BillingInstruction
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossOwed_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 134, column 57
    function value_78 () : gw.pl.currency.MonetaryAmount {
      return agencyDistItem.GrossAmountOwed
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionOwed_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 141, column 62
    function value_82 () : gw.pl.currency.MonetaryAmount {
      return agencyDistItem.CommissionAmountOwed
    }
    
    // 'value' attribute on TextCell (id=PercentOwed_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 149, column 51
    function value_87 () : java.math.BigDecimal {
      return gw.agencybill.CommissionCalculator.getCommissionPercent(agencyDistItem)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetOwed_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 156, column 55
    function value_90 () : gw.pl.currency.MonetaryAmount {
      return agencyDistItem.NetAmountOwed
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossToApply_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 180, column 59
    function value_99 () : gw.pl.currency.MonetaryAmount {
      return agencyDistItem.GrossAmountToApply
    }
    
    // 'valueRange' attribute on RangeCell (id=Disposition_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 239, column 92
    function verifyValueRangeIsAllowedType_139 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Disposition_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 239, column 92
    function verifyValueRangeIsAllowedType_139 ($$arg :  typekey.DistItemDisposition[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Disposition_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 239, column 92
    function verifyValueRange_140 () : void {
      var __valueRangeArg = DistItemDisposition.getTypeKeys( false )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_139(__valueRangeArg)
    }
    
    // 'valueVisible' attribute on RangeCell (id=Disposition_Cell) at AgencyDistributionWizard_DetailsPopup.pcf: line 239, column 92
    function visible_134 () : java.lang.Boolean {
      return invoiceItem.Type != InvoiceItemType.TC_COMMISSIONREMAINDER
    }
    
    // 'visible' attribute on Link (id=FillDefaultsButton) at AgencyDistributionWizard_DetailsPopup.pcf: line 169, column 136
    function visible_94 () : java.lang.Boolean {
      return invoiceItem.Type != InvoiceItemType.TC_COMMISSIONREMAINDER && agencyDistItem.CommissionModifiable
    }
    
    property get agencyDistItem () : entity.BaseDistItem {
      return getIteratedValue(1) as entity.BaseDistItem
    }
    
    property get invoiceItem () : entity.InvoiceItem {
      return getVariableValue("invoiceItem", 1) as entity.InvoiceItem
    }
    
    property set invoiceItem ($arg :  entity.InvoiceItem) {
      setVariableValue("invoiceItem", 1, $arg)
    }
    
    property get netToApply () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("netToApply", 1) as gw.pl.currency.MonetaryAmount
    }
    
    property set netToApply ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("netToApply", 1, $arg)
    }
    
    property get percentToApply () : java.math.BigDecimal {
      return getVariableValue("percentToApply", 1) as java.math.BigDecimal
    }
    
    property set percentToApply ($arg :  java.math.BigDecimal) {
      setVariableValue("percentToApply", 1, $arg)
    }
    
    
  }
  
  
}