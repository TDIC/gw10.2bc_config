package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/accounting/JournalScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class JournalScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/accounting/JournalScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class JournalScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AlertBar (id=PolicyArchivedAlert) at JournalScreen.pcf: line 17, column 86
    function label_1 () : java.lang.Object {
      return DisplayKey.get("Web.Archive.PolicyPeriod.ArchivedAlertBar", (tAccountOwner as PolicyPeriod).ArchiveDate.AsUIStyle)
    }
    
    // 'visible' attribute on AlertBar (id=PolicyArchivedAlert) at JournalScreen.pcf: line 17, column 86
    function visible_0 () : java.lang.Boolean {
      return tAccountOwner typeis PolicyPeriod && tAccountOwner.Archived
    }
    
    property get relatedTAccountOwners () : TAccountOwner[] {
      return getRequireValue("relatedTAccountOwners", 0) as TAccountOwner[]
    }
    
    property set relatedTAccountOwners ($arg :  TAccountOwner[]) {
      setRequireValue("relatedTAccountOwners", 0, $arg)
    }
    
    property get tAccountOwner () : TAccountOwner {
      return getRequireValue("tAccountOwner", 0) as TAccountOwner
    }
    
    property set tAccountOwner ($arg :  TAccountOwner) {
      setRequireValue("tAccountOwner", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/accounting/JournalScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends JournalScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at JournalScreen.pcf: line 22, column 70
    function def_onEnter_2 (def :  pcf.TAccountOwnersLV) : void {
      def.onEnter(tAccountOwner, relatedTAccountOwners)
    }
    
    // 'def' attribute on PanelRef at JournalScreen.pcf: line 30, column 132
    function def_onEnter_4 (def :  pcf.TransactionsLV) : void {
      def.onEnter(selectedTAccountOwner.Transactions as gw.api.database.IQueryBeanResult<Transaction>, false)
    }
    
    // 'def' attribute on PanelRef at JournalScreen.pcf: line 22, column 70
    function def_refreshVariables_3 (def :  pcf.TAccountOwnersLV) : void {
      def.refreshVariables(tAccountOwner, relatedTAccountOwners)
    }
    
    // 'def' attribute on PanelRef at JournalScreen.pcf: line 30, column 132
    function def_refreshVariables_5 (def :  pcf.TransactionsLV) : void {
      def.refreshVariables(selectedTAccountOwner.Transactions as gw.api.database.IQueryBeanResult<Transaction>, false)
    }
    
    property get selectedTAccountOwner () : TAccountOwner {
      return getCurrentSelection(1) as TAccountOwner
    }
    
    property set selectedTAccountOwner ($arg :  TAccountOwner) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  
}