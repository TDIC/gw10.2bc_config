package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountSummaryPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountSummaryPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountSummaryPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountSummaryPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'canVisit' attribute on Popup (id=AccountSummaryPopup) at AccountSummaryPopup.pcf: line 9, column 72
    static function canVisit_2 (account :  Account) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctsummview
    }
    
    // 'def' attribute on ScreenRef at AccountSummaryPopup.pcf: line 16, column 44
    function def_onEnter_0 (def :  pcf.AccountSummaryScreen) : void {
      def.onEnter(account)
    }
    
    // 'def' attribute on ScreenRef at AccountSummaryPopup.pcf: line 16, column 44
    function def_refreshVariables_1 (def :  pcf.AccountSummaryScreen) : void {
      def.refreshVariables(account)
    }
    
    override property get CurrentLocation () : pcf.AccountSummaryPopup {
      return super.CurrentLocation as pcf.AccountSummaryPopup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  
}