package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountInvoiceForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountInvoiceForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountInvoiceForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountInvoiceForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (invoiceID :  String) : int {
      return 0
    }
    
    // 'action' attribute on ForwardCondition at AccountInvoiceForward.pcf: line 18, column 36
    function action_1 () : void {
      AccountDetailInvoices.go(invoice.Account, invoice)
    }
    
    // 'action' attribute on ForwardCondition at AccountInvoiceForward.pcf: line 21, column 36
    function action_4 () : void {
      InvoiceSearch.go()
    }
    
    // 'action' attribute on ForwardCondition at AccountInvoiceForward.pcf: line 18, column 36
    function action_dest_2 () : pcf.api.Destination {
      return pcf.AccountDetailInvoices.createDestination(invoice.Account, invoice)
    }
    
    // 'action' attribute on ForwardCondition at AccountInvoiceForward.pcf: line 21, column 36
    function action_dest_5 () : pcf.api.Destination {
      return pcf.InvoiceSearch.createDestination()
    }
    
    // 'condition' attribute on ForwardCondition at AccountInvoiceForward.pcf: line 18, column 36
    function condition_3 () : java.lang.Boolean {
      return invoice != null
    }
    
    // 'condition' attribute on ForwardCondition at AccountInvoiceForward.pcf: line 21, column 36
    function condition_6 () : java.lang.Boolean {
      return invoice == null
    }
    
    // 'initialValue' attribute on Variable at AccountInvoiceForward.pcf: line 15, column 30
    function initialValue_0 () : AccountInvoice {
      return gw.api.database.Query.make(AccountInvoice).compare(AccountInvoice#PublicID, Equals, invoiceID).select().AtMostOneRow
    }
    
    override property get CurrentLocation () : pcf.AccountInvoiceForward {
      return super.CurrentLocation as pcf.AccountInvoiceForward
    }
    
    property get invoice () : AccountInvoice {
      return getVariableValue("invoice", 0) as AccountInvoice
    }
    
    property set invoice ($arg :  AccountInvoice) {
      setVariableValue("invoice", 0, $arg)
    }
    
    property get invoiceID () : String {
      return getVariableValue("invoiceID", 0) as String
    }
    
    property set invoiceID ($arg :  String) {
      setVariableValue("invoiceID", 0, $arg)
    }
    
    
  }
  
  
}