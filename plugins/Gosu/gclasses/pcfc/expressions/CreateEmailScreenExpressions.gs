package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/email/CreateEmailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreateEmailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/email/CreateEmailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreateEmailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ButtonInput (id=ShowCCRecipients_Input) at CreateEmailScreen.pcf: line 102, column 30
    function action_19 () : void {
      showCC = true
    }
    
    // 'action' attribute on ButtonInput (id=ShowBCCRecipients_Input) at CreateEmailScreen.pcf: line 113, column 31
    function action_25 () : void {
      showBCC = true
    }
    
    // 'action' attribute on PickerToolbarButton (id=AddDocumentButton) at CreateEmailScreen.pcf: line 164, column 31
    function action_51 () : void {
      PickExistingDocumentPopup.push(docContainer)
    }
    
    // 'action' attribute on ToolbarButton (id=ToolbarButton0) at CreateEmailScreen.pcf: line 58, column 25
    function action_6 () : void {
      sendEmailToRecipients(NewEmail)
    }
    
    // 'action' attribute on ToolbarButton (id=ToolbarButton1) at CreateEmailScreen.pcf: line 64, column 25
    function action_7 () : void {
      CurrentLocation.cancel()
    }
    
    // 'action' attribute on PickerToolbarButton (id=AddDocumentButton) at CreateEmailScreen.pcf: line 164, column 31
    function action_dest_52 () : pcf.api.Destination {
      return pcf.PickExistingDocumentPopup.createDestination(docContainer)
    }
    
    // 'def' attribute on InputSetRef (id=ToRecipientLVInput) at CreateEmailScreen.pcf: line 96, column 36
    function def_onEnter_16 (def :  pcf.CreateEmailScreenRecipientInputSet) : void {
      def.onEnter(DisplayKey.get("Web.Email.ToRecipients"), NewEmail.ToRecipients)
    }
    
    // 'def' attribute on InputSetRef (id=ToCCRecipientLVInput) at CreateEmailScreen.pcf: line 107, column 29
    function def_onEnter_22 (def :  pcf.CreateEmailScreenRecipientInputSet) : void {
      def.onEnter(DisplayKey.get("Web.Email.CCRecipients"), NewEmail.CcRecipients)
    }
    
    // 'def' attribute on InputSetRef (id=ToBCCRecipientLVInput) at CreateEmailScreen.pcf: line 118, column 30
    function def_onEnter_28 (def :  pcf.CreateEmailScreenRecipientInputSet) : void {
      def.onEnter(DisplayKey.get("Web.Email.BCCRecipients"), NewEmail.BccRecipients)
    }
    
    // 'def' attribute on InputSetRef (id=ToRecipientLVInput) at CreateEmailScreen.pcf: line 96, column 36
    function def_refreshVariables_17 (def :  pcf.CreateEmailScreenRecipientInputSet) : void {
      def.refreshVariables(DisplayKey.get("Web.Email.ToRecipients"), NewEmail.ToRecipients)
    }
    
    // 'def' attribute on InputSetRef (id=ToCCRecipientLVInput) at CreateEmailScreen.pcf: line 107, column 29
    function def_refreshVariables_23 (def :  pcf.CreateEmailScreenRecipientInputSet) : void {
      def.refreshVariables(DisplayKey.get("Web.Email.CCRecipients"), NewEmail.CcRecipients)
    }
    
    // 'def' attribute on InputSetRef (id=ToBCCRecipientLVInput) at CreateEmailScreen.pcf: line 118, column 30
    function def_refreshVariables_29 (def :  pcf.CreateEmailScreenRecipientInputSet) : void {
      def.refreshVariables(DisplayKey.get("Web.Email.BCCRecipients"), NewEmail.BccRecipients)
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at CreateEmailScreen.pcf: line 89, column 95
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      language = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'value' attribute on CheckBoxInput (id=SaveAsDocument_Input) at CreateEmailScreen.pcf: line 127, column 143
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      saveAsDocument = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=SenderName_Input) at CreateEmailScreen.pcf: line 134, column 41
    function defaultSetter_36 (__VALUE_TO_SET :  java.lang.Object) : void {
      NewEmail.Sender.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=SenderEmail_Input) at CreateEmailScreen.pcf: line 139, column 49
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      NewEmail.Sender.EmailAddress = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at CreateEmailScreen.pcf: line 145, column 37
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      NewEmail.Subject = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextAreaInput (id=Body_Input) at CreateEmailScreen.pcf: line 152, column 34
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      NewEmail.Body = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at CreateEmailScreen.pcf: line 23, column 24
    function initialValue_0 () : Document {
      return null
    }
    
    // 'initialValue' attribute on Variable at CreateEmailScreen.pcf: line 27, column 23
    function initialValue_1 () : Boolean {
      return emailTemplate == null
    }
    
    // 'initialValue' attribute on Variable at CreateEmailScreen.pcf: line 31, column 52
    function initialValue_2 () : gw.api.contact.ExternalContactSource {
      return gw.api.contact.AddressBookUtil.newAddressBookContactSource()
    }
    
    // 'initialValue' attribute on Variable at CreateEmailScreen.pcf: line 35, column 23
    function initialValue_3 () : boolean {
      return docContainer != null and perm.Document.create and gw.plugin.Plugins.isEnabled(gw.plugin.document.IDocumentContentSource)
    }
    
    // 'initialValue' attribute on Variable at CreateEmailScreen.pcf: line 47, column 28
    function initialValue_4 () : LanguageType {
      return gw.api.util.LocaleUtil.getDefaultLanguageType()
    }
    
    // 'initialValue' attribute on Variable at CreateEmailScreen.pcf: line 51, column 34
    function initialValue_5 () : gw.api.email.Email {
      return initNewEmail()
    }
    
    // 'label' attribute on AlertBar (id=NoDefaultTemplate) at CreateEmailScreen.pcf: line 79, column 62
    function label_9 () : java.lang.Object {
      return DisplayKey.get("Web.Email.Template.NotFound", emailTemplate)
    }
    
    // 'onChange' attribute on PostOnChange at CreateEmailScreen.pcf: line 91, column 51
    function onChange_10 () : void {
      executeTemplate(NewEmail)
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=AddDocumentButton) at CreateEmailScreen.pcf: line 164, column 31
    function onPick_53 (PickedValue :  Document) : void {
      NewEmail.addDocument(PickedValue)
    }
    
    // 'value' attribute on TextCell (id=Document_Cell) at CreateEmailScreen.pcf: line 183, column 50
    function sortValue_54 (AttachedDocument :  entity.Document) : java.lang.Object {
      return AttachedDocument.Name
    }
    
    // 'toRemove' attribute on RowIterator at CreateEmailScreen.pcf: line 177, column 65
    function toRemove_59 (AttachedDocument :  entity.Document) : void {
      NewEmail.removeDocument( AttachedDocument )
    }
    
    // 'value' attribute on TextInput (id=SenderName_Input) at CreateEmailScreen.pcf: line 134, column 41
    function valueRoot_37 () : java.lang.Object {
      return NewEmail.Sender
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at CreateEmailScreen.pcf: line 145, column 37
    function valueRoot_45 () : java.lang.Object {
      return NewEmail
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at CreateEmailScreen.pcf: line 89, column 95
    function value_12 () : typekey.LanguageType {
      return language
    }
    
    // 'value' attribute on CheckBoxInput (id=SaveAsDocument_Input) at CreateEmailScreen.pcf: line 127, column 143
    function value_31 () : java.lang.Boolean {
      return saveAsDocument
    }
    
    // 'value' attribute on TextInput (id=SenderName_Input) at CreateEmailScreen.pcf: line 134, column 41
    function value_35 () : java.lang.String {
      return NewEmail.Sender.Name
    }
    
    // 'value' attribute on TextInput (id=SenderEmail_Input) at CreateEmailScreen.pcf: line 139, column 49
    function value_39 () : java.lang.String {
      return NewEmail.Sender.EmailAddress
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at CreateEmailScreen.pcf: line 145, column 37
    function value_43 () : java.lang.String {
      return NewEmail.Subject
    }
    
    // 'value' attribute on TextAreaInput (id=Body_Input) at CreateEmailScreen.pcf: line 152, column 34
    function value_47 () : java.lang.String {
      return NewEmail.Body
    }
    
    // 'value' attribute on RowIterator at CreateEmailScreen.pcf: line 177, column 65
    function value_60 () : java.util.List<entity.Document> {
      return NewEmail.Documents
    }
    
    // 'visible' attribute on TypeKeyInput (id=Language_Input) at CreateEmailScreen.pcf: line 89, column 95
    function visible_11 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count >  1 and emailTemplate != null
    }
    
    // 'visible' attribute on ButtonInput (id=ShowCCRecipients_Input) at CreateEmailScreen.pcf: line 102, column 30
    function visible_18 () : java.lang.Boolean {
      return !showCC
    }
    
    // 'visible' attribute on InputSetRef (id=ToCCRecipientLVInput) at CreateEmailScreen.pcf: line 107, column 29
    function visible_21 () : java.lang.Boolean {
      return showCC
    }
    
    // 'visible' attribute on ButtonInput (id=ShowBCCRecipients_Input) at CreateEmailScreen.pcf: line 113, column 31
    function visible_24 () : java.lang.Boolean {
      return !showBCC
    }
    
    // 'visible' attribute on InputSetRef (id=ToBCCRecipientLVInput) at CreateEmailScreen.pcf: line 118, column 30
    function visible_27 () : java.lang.Boolean {
      return showBCC
    }
    
    // 'visible' attribute on CheckBoxInput (id=SaveAsDocument_Input) at CreateEmailScreen.pcf: line 127, column 143
    function visible_30 () : java.lang.Boolean {
      return docContainer != null and perm.Document.create and gw.plugin.Plugins.isEnabled(gw.plugin.document.IDocumentContentSource)
    }
    
    // 'visible' attribute on AlertBar (id=NoDefaultTemplate) at CreateEmailScreen.pcf: line 79, column 62
    function visible_8 () : java.lang.Boolean {
      return emailTemplate != null and noDefaultTemplate
    }
    
    property get NewEmail () : gw.api.email.Email {
      return getVariableValue("NewEmail", 0) as gw.api.email.Email
    }
    
    property set NewEmail ($arg :  gw.api.email.Email) {
      setVariableValue("NewEmail", 0, $arg)
    }
    
    property get docContainer () : DocumentContainer {
      return getRequireValue("docContainer", 0) as DocumentContainer
    }
    
    property set docContainer ($arg :  DocumentContainer) {
      setRequireValue("docContainer", 0, $arg)
    }
    
    property get documentToSave () : Document {
      return getVariableValue("documentToSave", 0) as Document
    }
    
    property set documentToSave ($arg :  Document) {
      setVariableValue("documentToSave", 0, $arg)
    }
    
    property get documentsToSend () : Document[] {
      return getRequireValue("documentsToSend", 0) as Document[]
    }
    
    property set documentsToSend ($arg :  Document[]) {
      setRequireValue("documentsToSend", 0, $arg)
    }
    
    property get emailTemplate () : String {
      return getRequireValue("emailTemplate", 0) as String
    }
    
    property set emailTemplate ($arg :  String) {
      setRequireValue("emailTemplate", 0, $arg)
    }
    
    property get externalContactSource () : gw.api.contact.ExternalContactSource {
      return getVariableValue("externalContactSource", 0) as gw.api.contact.ExternalContactSource
    }
    
    property set externalContactSource ($arg :  gw.api.contact.ExternalContactSource) {
      setVariableValue("externalContactSource", 0, $arg)
    }
    
    property get language () : LanguageType {
      return getVariableValue("language", 0) as LanguageType
    }
    
    property set language ($arg :  LanguageType) {
      setVariableValue("language", 0, $arg)
    }
    
    property get noDefaultTemplate () : Boolean {
      return getVariableValue("noDefaultTemplate", 0) as Boolean
    }
    
    property set noDefaultTemplate ($arg :  Boolean) {
      setVariableValue("noDefaultTemplate", 0, $arg)
    }
    
    property get saveAsDocument () : boolean {
      return getVariableValue("saveAsDocument", 0) as java.lang.Boolean
    }
    
    property set saveAsDocument ($arg :  boolean) {
      setVariableValue("saveAsDocument", 0, $arg)
    }
    
    property get showBCC () : boolean {
      return getVariableValue("showBCC", 0) as java.lang.Boolean
    }
    
    property set showBCC ($arg :  boolean) {
      setVariableValue("showBCC", 0, $arg)
    }
    
    property get showCC () : boolean {
      return getVariableValue("showCC", 0) as java.lang.Boolean
    }
    
    property set showCC ($arg :  boolean) {
      setVariableValue("showCC", 0, $arg)
    }
    
    property get symbolTable () : java.util.Map<String, Object> {
      return getRequireValue("symbolTable", 0) as java.util.Map<String, Object>
    }
    
    property set symbolTable ($arg :  java.util.Map<String, Object>) {
      setRequireValue("symbolTable", 0, $arg)
    }
    
    function initNewEmail() : gw.api.email.Email {
      var rtn = new gw.api.email.Email()
      if (emailTemplate != null) {
        executeTemplate(rtn)
      }
      if (documentsToSend != null) {
        for (document in documentsToSend) {
          rtn.addDocument( document )
        }
      }
      return rtn
    }
    
    function executeTemplate(rtn : gw.api.email.Email) {
      var templatePlugin = gw.plugin.Plugins.get(gw.plugin.email.IEmailTemplateSource)
      var template = templatePlugin.getEmailTemplate(gw.api.util.LocaleUtil.toLanguage(language), emailTemplate)
      if (template == null) {
        noDefaultTemplate = true
        throw new gw.api.util.DisplayableException(DisplayKey.get("Web.Activity.EmailTemplate.Language", emailTemplate, language))
      }
      else {
       rtn.useEmailTemplate(template, symbolTable)
      }
    }
    
    function sendEmailToRecipients(emailToSend : gw.api.email.Email) {
      var warnings = gw.api.email.EmailUtil.emailContentsValid(emailToSend)
      if (warnings.length > 0) {
        throw new gw.api.util.DisplayableException(warnings)
      }
      if (saveAsDocument) {
        var templatePlugin = gw.plugin.Plugins.get(gw.plugin.document.IDocumentTemplateSource)
        var template = templatePlugin.getDocumentTemplate("CreateEmailSent.gosu.htm", gw.api.util.LocaleUtil.getDefaultLocale())
        if (template == null) {
          throw new gw.api.util.DisplayableException(DisplayKey.get("Web.CreateEmailScreen.Error.NoManualEmailSentTemplate"))
        } else {
          documentToSave = documentToSave != null ? documentToSave : new Document()
          documentToSave.Name  = emailToSend.Subject
          documentToSave.MimeType = template.MimeType
          documentToSave.Type = typekey.DocumentType.get(template.TemplateType)
          documentToSave.Section = typekey.DocumentSection.get(template.getMetadataPropertyValue( "section" ) as String) // assignment will force it to SectionType
          documentToSave.SecurityType = typekey.DocumentSecurityType.get(template.DefaultSecurityType)
          documentToSave.Status = TC_FINAL
          documentToSave.Recipient = emailToSend.ToRecipients.first().Name
          documentToSave.Author = User.util.CurrentUser.DisplayName
          documentToSave.Inbound = false
          documentToSave.DateCreated = gw.api.util.DateUtil.currentDate()
          docContainer.addDocument( documentToSave )
          
          var paramMap = new java.util.HashMap(symbolTable)
          paramMap.put("User", User.util.CurrentUser)
          paramMap.put("Email", emailToSend)
          paramMap.put("DateSent", gw.api.util.DateUtil.currentDate())
          gw.document.DocumentProduction.createAndStoreDocumentSynchronously(template, paramMap, documentToSave)
    
        }
      } else if (documentToSave != null) {
        documentToSave.remove()
      }
      gw.api.email.EmailUtil.sendEmailWithBody(docContainer as KeyableBean, emailToSend)
      // it didn't throw so reset email template so that other templates can be used
      var actv = symbolTable.get("Activity")
      if (emailTemplate != null and actv typeis Activity) {
        if (actv.EmailTemplate == emailTemplate) {
          actv.EmailTemplate = null
        }
      }
      CurrentLocation.commit()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/email/CreateEmailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CreateEmailScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Document_Cell) at CreateEmailScreen.pcf: line 183, column 50
    function defaultSetter_56 (__VALUE_TO_SET :  java.lang.Object) : void {
      AttachedDocument.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=Document_Cell) at CreateEmailScreen.pcf: line 183, column 50
    function valueRoot_57 () : java.lang.Object {
      return AttachedDocument
    }
    
    // 'value' attribute on TextCell (id=Document_Cell) at CreateEmailScreen.pcf: line 183, column 50
    function value_55 () : java.lang.String {
      return AttachedDocument.Name
    }
    
    property get AttachedDocument () : entity.Document {
      return getIteratedValue(1) as entity.Document
    }
    
    
  }
  
  
}