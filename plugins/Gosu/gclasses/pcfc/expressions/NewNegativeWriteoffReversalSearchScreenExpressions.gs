package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffReversalSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewNegativeWriteoffReversalSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffReversalSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=Select) at NewNegativeWriteoffReversalSearchScreen.pcf: line 127, column 64
    function action_45 () : void {
      negativeWriteoffToReverse.setNegativeWriteoffAndAddToBundle(negativeWriteoff); (CurrentLocation as pcf.api.Wizard).next();
    }
    
    // 'value' attribute on DateCell (id=WriteoffDate_Cell) at NewNegativeWriteoffReversalSearchScreen.pcf: line 133, column 58
    function valueRoot_47 () : java.lang.Object {
      return negativeWriteoff
    }
    
    // 'value' attribute on DateCell (id=WriteoffDate_Cell) at NewNegativeWriteoffReversalSearchScreen.pcf: line 133, column 58
    function value_46 () : java.util.Date {
      return negativeWriteoff.CreateTime
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at NewNegativeWriteoffReversalSearchScreen.pcf: line 139, column 58
    function value_49 () : entity.NegativeWriteoff {
      return negativeWriteoff
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at NewNegativeWriteoffReversalSearchScreen.pcf: line 145, column 54
    function value_51 () : gw.pl.currency.MonetaryAmount {
      return negativeWriteoff.Amount
    }
    
    // 'value' attribute on TextCell (id=UnappliedFund_Cell) at NewNegativeWriteoffReversalSearchScreen.pcf: line 151, column 55
    function value_54 () : entity.UnappliedFund {
      return getUnappliedFund(negativeWriteoff)
    }
    
    // 'visible' attribute on Link (id=Select) at NewNegativeWriteoffReversalSearchScreen.pcf: line 127, column 64
    function visible_44 () : java.lang.Boolean {
      return negativeWriteoff.canReverse()
    }
    
    property get negativeWriteoff () : entity.NegativeWriteoff {
      return getIteratedValue(2) as entity.NegativeWriteoff
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffReversalSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewNegativeWriteoffReversalSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at NewNegativeWriteoffReversalSearchScreen.pcf: line 16, column 57
    function initialValue_0 () : gw.api.web.account.AccountSearchConverter {
      return new gw.api.web.account.AccountSearchConverter()
    }
    
    // 'initialValue' attribute on Variable at NewNegativeWriteoffReversalSearchScreen.pcf: line 20, column 23
    function initialValue_1 () : boolean {
      return account == null
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get accountEditable () : boolean {
      return getVariableValue("accountEditable", 0) as java.lang.Boolean
    }
    
    property set accountEditable ($arg :  boolean) {
      setVariableValue("accountEditable", 0, $arg)
    }
    
    property get accountSearchConverter () : gw.api.web.account.AccountSearchConverter {
      return getVariableValue("accountSearchConverter", 0) as gw.api.web.account.AccountSearchConverter
    }
    
    property set accountSearchConverter ($arg :  gw.api.web.account.AccountSearchConverter) {
      setVariableValue("accountSearchConverter", 0, $arg)
    }
    
    property get negativeWriteoffToReverse () : NegativeWriteoffRev {
      return getRequireValue("negativeWriteoffToReverse", 0) as NegativeWriteoffRev
    }
    
    property set negativeWriteoffToReverse ($arg :  NegativeWriteoffRev) {
      setRequireValue("negativeWriteoffToReverse", 0, $arg)
    }
    
    function initializeSearchCriteria() : NegWriteoffSearchCrit {
      var criteria = new NegWriteoffSearchCrit();
      
      if (!accountEditable) {
        criteria.Account = account;
        criteria.Currency = account.Currency;
      }
      
      return criteria;
    }
    
    function blankMinimumAndMaximumFields(searchCriteria : NegWriteoffSearchCrit) {
      searchCriteria.MinAmount = null
      searchCriteria.MaxAmount = null
    }
    
    function getUnappliedFund(negativeWriteoff : NegativeWriteoff): UnappliedFund {
      if(negativeWriteoff typeis AcctNegativeWriteoff) {
        return negativeWriteoff.getUnappliedFund()
      }
      return null
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffReversalSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends NewNegativeWriteoffReversalSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at NewNegativeWriteoffReversalSearchScreen.pcf: line 96, column 44
    function action_33 () : void {
      AccountSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at NewNegativeWriteoffReversalSearchScreen.pcf: line 96, column 44
    function action_dest_34 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'available' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 57, column 51
    function available_11 () : java.lang.Boolean {
      return searchCriteria.Currency != null || gw.api.util.CurrencyUtil.isSingleCurrencyMode()
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 57, column 51
    function currency_15 () : typekey.Currency {
      return gw.search.SearchHelper.getDefaultCurrencyIfNull(searchCriteria.Currency) 
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 64, column 51
    function currency_22 () : typekey.Currency {
      return gw.search.SearchHelper.getDefaultCurrencyIfNull(searchCriteria.Currency)
    }
    
    // 'def' attribute on InputSetRef at NewNegativeWriteoffReversalSearchScreen.pcf: line 106, column 49
    function def_onEnter_42 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at NewNegativeWriteoffReversalSearchScreen.pcf: line 106, column 49
    function def_refreshVariables_43 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 57, column 51
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MinAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 64, column 51
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MaxAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 72, column 54
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.EarliestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 77, column 52
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.LatestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=AccountCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 96, column 44
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Account = (__VALUE_TO_SET as entity.Account)
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 47, column 74
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Currency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'editable' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 47, column 74
    function editable_3 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode() && accountEditable
    }
    
    // 'editable' attribute on TextInput (id=AccountCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 96, column 44
    function editable_35 () : java.lang.Boolean {
      return accountEditable
    }
    
    // 'inputConversion' attribute on TextInput (id=AccountCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 96, column 44
    function inputConversion_36 (VALUE :  java.lang.String) : java.lang.Object {
      return accountSearchConverter.getAccount(VALUE)
    }
    
    // 'onChange' attribute on PostOnChange at NewNegativeWriteoffReversalSearchScreen.pcf: line 49, column 76
    function onChange_2 () : void {
      blankMinimumAndMaximumFields(searchCriteria)
    }
    
    // 'searchCriteria' attribute on SearchPanel at NewNegativeWriteoffReversalSearchScreen.pcf: line 34, column 88
    function searchCriteria_58 () : entity.NegWriteoffSearchCrit {
      return initializeSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at NewNegativeWriteoffReversalSearchScreen.pcf: line 34, column 88
    function search_57 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria)
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 47, column 74
    function valueRoot_7 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 57, column 51
    function value_12 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MinAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 64, column 51
    function value_19 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MaxAmount
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 72, column 54
    function value_25 () : java.util.Date {
      return searchCriteria.EarliestDate
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 77, column 52
    function value_29 () : java.util.Date {
      return searchCriteria.LatestDate
    }
    
    // 'value' attribute on TextInput (id=AccountCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 96, column 44
    function value_37 () : entity.Account {
      return searchCriteria.Account
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 47, column 74
    function value_5 () : typekey.Currency {
      return searchCriteria.Currency
    }
    
    // 'value' attribute on RowIterator at NewNegativeWriteoffReversalSearchScreen.pcf: line 117, column 93
    function value_56 () : gw.api.database.IQueryBeanResult<entity.NegativeWriteoff> {
      return negativeWriteoffs
    }
    
    // 'visible' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at NewNegativeWriteoffReversalSearchScreen.pcf: line 47, column 74
    function visible_4 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    property get negativeWriteoffs () : gw.api.database.IQueryBeanResult<NegativeWriteoff> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<NegativeWriteoff>
    }
    
    property get searchCriteria () : entity.NegWriteoffSearchCrit {
      return getCriteriaValue(1) as entity.NegWriteoffSearchCrit
    }
    
    property set searchCriteria ($arg :  entity.NegWriteoffSearchCrit) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}