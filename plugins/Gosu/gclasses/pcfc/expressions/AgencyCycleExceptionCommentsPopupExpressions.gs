package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyCycleExceptionCommentsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyCycleExceptionCommentsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyCycleExceptionCommentsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyCycleExceptionCommentsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (agencyCyclesWithException :  AgencyCycleProcess[], isLatePayments :  boolean) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=AgencyCycleExceptionCommentsPopup) at AgencyCycleExceptionCommentsPopup.pcf: line 10, column 85
    function beforeCommit_5 (pickedValue :  java.lang.Object) : void {
      gw.api.web.producer.agencybill.ExceptionItemUtil.applyCommentToCycleExceptions(agencyCyclesWithException, comments, isLatePayments, CurrentLocation)
    }
    
    // 'value' attribute on TextAreaInput (id=exceptionComments_Input) at AgencyCycleExceptionCommentsPopup.pcf: line 36, column 31
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      comments = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at AgencyCycleExceptionCommentsPopup.pcf: line 22, column 32
    function initialValue_0 () : java.lang.String {
      return gw.api.web.producer.agencybill.ExceptionItemUtil.getInitialCycleExceptionComments(agencyCyclesWithException, isLatePayments)
    }
    
    // EditButtons at AgencyCycleExceptionCommentsPopup.pcf: line 25, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'value' attribute on TextAreaInput (id=exceptionComments_Input) at AgencyCycleExceptionCommentsPopup.pcf: line 36, column 31
    function value_2 () : java.lang.String {
      return comments
    }
    
    override property get CurrentLocation () : pcf.AgencyCycleExceptionCommentsPopup {
      return super.CurrentLocation as pcf.AgencyCycleExceptionCommentsPopup
    }
    
    property get agencyCyclesWithException () : AgencyCycleProcess[] {
      return getVariableValue("agencyCyclesWithException", 0) as AgencyCycleProcess[]
    }
    
    property set agencyCyclesWithException ($arg :  AgencyCycleProcess[]) {
      setVariableValue("agencyCyclesWithException", 0, $arg)
    }
    
    property get comments () : java.lang.String {
      return getVariableValue("comments", 0) as java.lang.String
    }
    
    property set comments ($arg :  java.lang.String) {
      setVariableValue("comments", 0, $arg)
    }
    
    property get isLatePayments () : boolean {
      return getVariableValue("isLatePayments", 0) as java.lang.Boolean
    }
    
    property set isLatePayments ($arg :  boolean) {
      setVariableValue("isLatePayments", 0, $arg)
    }
    
    
  }
  
  
}