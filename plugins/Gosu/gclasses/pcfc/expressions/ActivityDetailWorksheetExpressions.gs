package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/ActivityDetailWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ActivityDetailWorksheetExpressions {
  @javax.annotation.Generated("config/web/pcf/activity/ActivityDetailWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ActivityDetailWorksheetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (activity :  Activity) : int {
      return 0
    }
    
    static function __constructorIndex (activity :  Activity, fromApprovalReject :  Boolean) : int {
      return 1
    }
    
    static function __constructorIndex (activity :  Activity, fromApprovalReject :  Boolean, editMode :  Boolean) : int {
      return 2
    }
    
    // 'canEdit' attribute on Worksheet (id=ActivityDetailWorksheet) at ActivityDetailWorksheet.pcf: line 9, column 74
    function canEdit_15 () : java.lang.Boolean {
      return activity.canEdit()
    }
    
    // 'def' attribute on ScreenRef at ActivityDetailWorksheet.pcf: line 56, column 61
    function def_onEnter_13 (def :  pcf.ActivityDetailScreen) : void {
      def.onEnter(activity, assigneeHolder)
    }
    
    // 'def' attribute on ScreenRef at ActivityDetailWorksheet.pcf: line 56, column 61
    function def_refreshVariables_14 (def :  pcf.ActivityDetailScreen) : void {
      def.refreshVariables(activity, assigneeHolder)
    }
    
    // 'initialValue' attribute on Variable at ActivityDetailWorksheet.pcf: line 46, column 44
    function initialValue_12 () : gw.api.assignment.Assignee[] {
      return activity.InitialAssigneeForPicker
    }
    
    // 'location' attribute on Scope at ActivityDetailWorksheet.pcf: line 17, column 69
    function location_0 () : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(getAccountForAccountGroupLocation())
    }
    
    // 'location' attribute on Scope at ActivityDetailWorksheet.pcf: line 19, column 36
    function location_1 () : pcf.api.Destination {
      return pcf.ActivitySearch.createDestination()
    }
    
    // 'location' attribute on Scope at ActivityDetailWorksheet.pcf: line 37, column 57
    function location_10 () : pcf.api.Destination {
      return pcf.ProducerDetail.createDestination(activity.Producer_Ext)
    }
    
    // 'location' attribute on Scope at ActivityDetailWorksheet.pcf: line 39, column 65
    function location_11 () : pcf.api.Destination {
      return pcf.ProducerActivitiesPage.createDestination(activity.Producer_Ext)
    }
    
    // 'location' attribute on Scope at ActivityDetailWorksheet.pcf: line 21, column 78
    function location_2 () : pcf.api.Destination {
      return pcf.DelinquencyProcessDetailPopup.createDestination(activity.DelinquencyProcess)
    }
    
    // 'location' attribute on Scope at ActivityDetailWorksheet.pcf: line 23, column 34
    function location_3 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'location' attribute on Scope at ActivityDetailWorksheet.pcf: line 25, column 69
    function location_4 () : pcf.api.Destination {
      return pcf.TroubleTicketDetailsPopup.createDestination(activity.TroubleTicket)
    }
    
    // 'location' attribute on Scope at ActivityDetailWorksheet.pcf: line 27, column 72
    function location_5 () : pcf.api.Destination {
      return pcf.PolicyGroup.createDestination(getPolicyPeriodForPolicyGroupLocation())
    }
    
    // 'location' attribute on Scope at ActivityDetailWorksheet.pcf: line 29, column 59
    function location_6 () : pcf.api.Destination {
      return pcf.AccountActivitiesPage.createDestination(activity.Account)
    }
    
    // 'location' attribute on Scope at ActivityDetailWorksheet.pcf: line 31, column 58
    function location_7 () : pcf.api.Destination {
      return pcf.AccountDetailSummary.createDestination(activity.Account)
    }
    
    // 'location' attribute on Scope at ActivityDetailWorksheet.pcf: line 33, column 62
    function location_8 () : pcf.api.Destination {
      return pcf.PolicyDetailSummary.createDestination(activity.PolicyPeriod)
    }
    
    // 'location' attribute on Scope at ActivityDetailWorksheet.pcf: line 35, column 69
    function location_9 () : pcf.api.Destination {
      return pcf.PolicyPeriodActivitiesPage.createDestination(activity.PolicyPeriod)
    }
    
    override property get CurrentLocation () : pcf.ActivityDetailWorksheet {
      return super.CurrentLocation as pcf.ActivityDetailWorksheet
    }
    
    property get activity () : Activity {
      return getVariableValue("activity", 0) as Activity
    }
    
    property set activity ($arg :  Activity) {
      setVariableValue("activity", 0, $arg)
    }
    
    property get assigneeHolder () : gw.api.assignment.Assignee[] {
      return getVariableValue("assigneeHolder", 0) as gw.api.assignment.Assignee[]
    }
    
    property set assigneeHolder ($arg :  gw.api.assignment.Assignee[]) {
      setVariableValue("assigneeHolder", 0, $arg)
    }
    
    property get editMode () : Boolean {
      return getVariableValue("editMode", 0) as Boolean
    }
    
    property set editMode ($arg :  Boolean) {
      setVariableValue("editMode", 0, $arg)
    }
    
    property get fromApprovalReject () : Boolean {
      return getVariableValue("fromApprovalReject", 0) as Boolean
    }
    
    property set fromApprovalReject ($arg :  Boolean) {
      setVariableValue("fromApprovalReject", 0, $arg)
    }
    
    function getAccountForAccountGroupLocation() : Account {
      if (activity typeis DisbApprActivity) {
        var disbursementActivity = activity
        if (disbursementActivity.Disbursement typeis AccountDisbursement) {
          return disbursementActivity.Disbursement.Account
        } else if (disbursementActivity.Disbursement typeis CollateralDisbursement) {
          return disbursementActivity.Disbursement.Collateral.Account
        }
      }
      return activity.DelinquencyProcess.Account  
    }
    
    function getPolicyPeriodForPolicyGroupLocation() : PolicyPeriod {
      return (activity.DelinquencyProcess as PolicyDlnqProcess).PolicyPeriod
    }
    
    
  }
  
  
}