package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyPeriodForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyPeriodForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyPeriodForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyPeriodForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyNumber :  String, termNumber :  int) : int {
      return 0
    }
    
    // 'action' attribute on ForwardCondition at PolicyPeriodForward.pcf: line 21, column 41
    function action_1 () : void {
      PolicyOverview.go(policyPeriod)
    }
    
    // 'action' attribute on ForwardCondition at PolicyPeriodForward.pcf: line 24, column 41
    function action_4 () : void {
      PolicySearch.go()
    }
    
    // 'action' attribute on ForwardCondition at PolicyPeriodForward.pcf: line 21, column 41
    function action_dest_2 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(policyPeriod)
    }
    
    // 'action' attribute on ForwardCondition at PolicyPeriodForward.pcf: line 24, column 41
    function action_dest_5 () : pcf.api.Destination {
      return pcf.PolicySearch.createDestination()
    }
    
    // 'condition' attribute on ForwardCondition at PolicyPeriodForward.pcf: line 21, column 41
    function condition_3 () : java.lang.Boolean {
      return policyPeriod != null
    }
    
    // 'condition' attribute on ForwardCondition at PolicyPeriodForward.pcf: line 24, column 41
    function condition_6 () : java.lang.Boolean {
      return policyPeriod == null
    }
    
    // 'initialValue' attribute on Variable at PolicyPeriodForward.pcf: line 18, column 35
    function initialValue_0 () : entity.PolicyPeriod {
      return findPolicyPeriod()
    }
    
    override property get CurrentLocation () : pcf.PolicyPeriodForward {
      return super.CurrentLocation as pcf.PolicyPeriodForward
    }
    
    property get policyNumber () : String {
      return getVariableValue("policyNumber", 0) as String
    }
    
    property set policyNumber ($arg :  String) {
      setVariableValue("policyNumber", 0, $arg)
    }
    
    property get policyPeriod () : entity.PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as entity.PolicyPeriod
    }
    
    property set policyPeriod ($arg :  entity.PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get termNumber () : int {
      return getVariableValue("termNumber", 0) as java.lang.Integer
    }
    
    property set termNumber ($arg :  int) {
      setVariableValue("termNumber", 0, $arg)
    }
    
    function findPolicyPeriod() : PolicyPeriod{
      var q = gw.api.database.Query.make(entity.PolicyPeriod)
      q.compare("PolicyNumber", Equals, policyNumber)
      q.compare("TermNumber", Equals, termNumber)
      return q.select().FirstResult
    }
    
    
  }
  
  
}