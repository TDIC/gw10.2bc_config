package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyChargeOwnerDetailForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyChargeOwnerDetailForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyChargeOwnerDetailForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyChargeOwnerDetailForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (chargeOwnerView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) : int {
      return 0
    }
    
    // 'action' attribute on ForwardCondition at AgencyChargeOwnerDetailForward.pcf: line 14, column 68
    function action_0 () : void {
      PolicySummary.push(chargeOwnerView.ChargeOwner as PolicyPeriod)
    }
    
    // 'action' attribute on ForwardCondition at AgencyChargeOwnerDetailForward.pcf: line 17, column 63
    function action_3 () : void {
      AccountSummaryPopup.push(chargeOwnerView.Account)
    }
    
    // 'action' attribute on ForwardCondition at AgencyChargeOwnerDetailForward.pcf: line 14, column 68
    function action_dest_1 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(chargeOwnerView.ChargeOwner as PolicyPeriod)
    }
    
    // 'action' attribute on ForwardCondition at AgencyChargeOwnerDetailForward.pcf: line 17, column 63
    function action_dest_4 () : pcf.api.Destination {
      return pcf.AccountSummaryPopup.createDestination(chargeOwnerView.Account)
    }
    
    // 'condition' attribute on ForwardCondition at AgencyChargeOwnerDetailForward.pcf: line 14, column 68
    function condition_2 () : java.lang.Boolean {
      return chargeOwnerView.ChargeOwner typeis PolicyPeriod
    }
    
    // 'condition' attribute on ForwardCondition at AgencyChargeOwnerDetailForward.pcf: line 17, column 63
    function condition_5 () : java.lang.Boolean {
      return chargeOwnerView.ChargeOwner typeis Account
    }
    
    override property get CurrentLocation () : pcf.AgencyChargeOwnerDetailForward {
      return super.CurrentLocation as pcf.AgencyChargeOwnerDetailForward
    }
    
    property get chargeOwnerView () : gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView {
      return getVariableValue("chargeOwnerView", 0) as gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView
    }
    
    property set chargeOwnerView ($arg :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) {
      setVariableValue("chargeOwnerView", 0, $arg)
    }
    
    
  }
  
  
}