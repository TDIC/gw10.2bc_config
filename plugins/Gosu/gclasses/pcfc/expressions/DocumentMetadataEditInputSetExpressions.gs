package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/DocumentMetadataEditInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DocumentMetadataEditInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/document/DocumentMetadataEditInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DocumentMetadataEditInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=UnityLink) at DocumentMetadataEditInputSet.pcf: line 70, column 143
    function action_30 () : void {
      document.viewOnBaseDocument()
    }
    
    // 'action' attribute on Link (id=NameLink) at DocumentMetadataEditInputSet.pcf: line 79, column 141
    function action_35 () : void {
      document.downloadContent()
    }
    
    // 'available' attribute on Link (id=UnityLink) at DocumentMetadataEditInputSet.pcf: line 70, column 143
    function available_28 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(document)
    }
    
    // 'available' attribute on CheckBoxInput (id=allEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 32, column 60
    function available_3 () : java.lang.Boolean {
      return documentMetadataBCHelper.AllowFieldsSubset
    }
    
    // 'value' attribute on TextInput (id=Author_Input) at DocumentMetadataEditInputSet.pcf: line 163, column 48
    function defaultSetter_103 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.Author = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CheckBoxInput (id=recipientEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 171, column 60
    function defaultSetter_110 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.RecipientEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=Recipient_Input) at DocumentMetadataEditInputSet.pcf: line 180, column 51
    function defaultSetter_117 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.Recipient = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CheckBoxInput (id=statusEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 196, column 60
    function defaultSetter_126 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.StatusEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=Status_Input) at DocumentMetadataEditInputSet.pcf: line 207, column 47
    function defaultSetter_133 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.Status = (__VALUE_TO_SET as typekey.DocumentStatusType)
    }
    
    // 'value' attribute on CheckBoxInput (id=securityTypeEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 215, column 60
    function defaultSetter_140 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.SecurityTypeEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=SecurityType_Input) at DocumentMetadataEditInputSet.pcf: line 226, column 48
    function defaultSetter_147 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.SecurityType = (__VALUE_TO_SET as typekey.DocumentSecurityType)
    }
    
    // 'value' attribute on CheckBoxInput (id=nameEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 46, column 60
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.DocumentNameEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=typeEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 236, column 60
    function defaultSetter_154 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.TypeEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=Type_Input) at DocumentMetadataEditInputSet.pcf: line 247, column 41
    function defaultSetter_161 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.Type = (__VALUE_TO_SET as typekey.DocumentType)
    }
    
    // 'value' attribute on CheckBoxInput (id=subtypeEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 255, column 60
    function defaultSetter_168 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.SubtypeEnabled_ext = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=Subtype_Input) at DocumentMetadataEditInputSet.pcf: line 267, column 53
    function defaultSetter_178 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.Subtype_ext = (__VALUE_TO_SET as typekey.OnBaseDocumentSubtype_Ext)
    }
    
    // 'value' attribute on TextInput (id=DocumentName_Input) at DocumentMetadataEditInputSet.pcf: line 57, column 45
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CheckBoxInput (id=descriptionEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 88, column 60
    function defaultSetter_42 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.DescriptionEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at DocumentMetadataEditInputSet.pcf: line 97, column 53
    function defaultSetter_49 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CheckBoxInput (id=mimeTypeEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 105, column 60
    function defaultSetter_56 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.MimeTypeEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=allEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 32, column 60
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.AllFieldsEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=MimeType_Input) at DocumentMetadataEditInputSet.pcf: line 118, column 37
    function defaultSetter_64 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.MimeType = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CheckBoxInput (id=languageEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 126, column 118
    function defaultSetter_75 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.LanguageEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=Language_Input) at DocumentMetadataEditInputSet.pcf: line 138, column 65
    function defaultSetter_83 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.Language = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'value' attribute on CheckBoxInput (id=authorEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 154, column 60
    function defaultSetter_96 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentMetadataBCHelper.AuthorEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'initialValue' attribute on Variable at DocumentMetadataEditInputSet.pcf: line 16, column 52
    function initialValue_0 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // 'initialValue' attribute on Variable at DocumentMetadataEditInputSet.pcf: line 20, column 52
    function initialValue_1 () : gw.document.DocumentMetadataBCHelper {
      return documentDetailsApplicationHelper as gw.document.DocumentMetadataBCHelper
    }
    
    // 'initialValue' attribute on Variable at DocumentMetadataEditInputSet.pcf: line 24, column 24
    function initialValue_2 () : Document {
      return documentDetailsApplicationHelper.getDocuments().Count == 1 ? documentDetailsApplicationHelper.getDocuments().single() : null
    }
    
    // 'label' attribute on Link (id=UnityLink) at DocumentMetadataEditInputSet.pcf: line 70, column 143
    function label_31 () : java.lang.Object {
      return documentMetadataBCHelper.Name
    }
    
    // 'optionLabel' attribute on RangeInput (id=MimeType_Input) at DocumentMetadataEditInputSet.pcf: line 118, column 37
    function optionLabel_66 (VALUE :  java.lang.String) : java.lang.String {
      return gw.document.DocumentsUtilBase.getMimeTypeDescription(VALUE)
    }
    
    // 'value' attribute on Reflect at DocumentMetadataEditInputSet.pcf: line 271, column 79
    function reflectionValue_173 (TRIGGER_INDEX :  int, VALUE :  typekey.DocumentType) : java.lang.Object {
      return acc.onbase.util.PCFUtils.getOnlyDocumentSubType(VALUE)
    }
    
    // 'tooltip' attribute on Link (id=UnityLink) at DocumentMetadataEditInputSet.pcf: line 70, column 143
    function tooltip_32 () : java.lang.String {
      return documentsActionsHelper.getViewDocumentContentTooltip(document)
    }
    
    // 'valueRange' attribute on Reflect at DocumentMetadataEditInputSet.pcf: line 271, column 79
    function valueRange_175 (TRIGGER_INDEX :  int, VALUE :  typekey.DocumentType) : java.lang.Object {
      return acc.onbase.util.PCFUtils.getDocumentSubTypeRange(VALUE)
    }
    
    // 'valueRange' attribute on RangeInput (id=Subtype_Input) at DocumentMetadataEditInputSet.pcf: line 267, column 53
    function valueRange_180 () : java.lang.Object {
      return acc.onbase.util.PCFUtils.getDocumentSubTypeRange(documentMetadataBCHelper.Type)
    }
    
    // 'valueRange' attribute on RangeInput (id=MimeType_Input) at DocumentMetadataEditInputSet.pcf: line 118, column 37
    function valueRange_67 () : java.lang.Object {
      return gw.document.DocumentsUtilBase.getMimeTypes()
    }
    
    // 'valueRange' attribute on RangeInput (id=Language_Input) at DocumentMetadataEditInputSet.pcf: line 138, column 65
    function valueRange_85 () : java.lang.Object {
      return LanguageType.getTypeKeys( false )
    }
    
    // 'value' attribute on CheckBoxInput (id=allEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 32, column 60
    function valueRoot_7 () : java.lang.Object {
      return documentMetadataBCHelper
    }
    
    // 'value' attribute on TextInput (id=Author_Input) at DocumentMetadataEditInputSet.pcf: line 163, column 48
    function value_102 () : java.lang.String {
      return documentMetadataBCHelper.Author
    }
    
    // 'value' attribute on CheckBoxInput (id=recipientEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 171, column 60
    function value_109 () : java.lang.Boolean {
      return documentMetadataBCHelper.RecipientEnabled
    }
    
    // 'value' attribute on TextInput (id=Recipient_Input) at DocumentMetadataEditInputSet.pcf: line 180, column 51
    function value_116 () : java.lang.String {
      return documentMetadataBCHelper.Recipient
    }
    
    // 'value' attribute on CheckBoxInput (id=statusEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 196, column 60
    function value_125 () : java.lang.Boolean {
      return documentMetadataBCHelper.StatusEnabled
    }
    
    // 'value' attribute on TypeKeyInput (id=Status_Input) at DocumentMetadataEditInputSet.pcf: line 207, column 47
    function value_132 () : typekey.DocumentStatusType {
      return documentMetadataBCHelper.Status
    }
    
    // 'value' attribute on CheckBoxInput (id=securityTypeEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 215, column 60
    function value_139 () : java.lang.Boolean {
      return documentMetadataBCHelper.SecurityTypeEnabled
    }
    
    // 'value' attribute on CheckBoxInput (id=nameEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 46, column 60
    function value_14 () : java.lang.Boolean {
      return documentMetadataBCHelper.DocumentNameEnabled
    }
    
    // 'value' attribute on TypeKeyInput (id=SecurityType_Input) at DocumentMetadataEditInputSet.pcf: line 226, column 48
    function value_146 () : typekey.DocumentSecurityType {
      return documentMetadataBCHelper.SecurityType
    }
    
    // 'value' attribute on CheckBoxInput (id=typeEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 236, column 60
    function value_153 () : java.lang.Boolean {
      return documentMetadataBCHelper.TypeEnabled
    }
    
    // 'value' attribute on TypeKeyInput (id=Type_Input) at DocumentMetadataEditInputSet.pcf: line 247, column 41
    function value_160 () : typekey.DocumentType {
      return documentMetadataBCHelper.Type
    }
    
    // 'value' attribute on CheckBoxInput (id=subtypeEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 255, column 60
    function value_167 () : java.lang.Boolean {
      return documentMetadataBCHelper.SubtypeEnabled_ext
    }
    
    // 'value' attribute on RangeInput (id=Subtype_Input) at DocumentMetadataEditInputSet.pcf: line 267, column 53
    function value_177 () : typekey.OnBaseDocumentSubtype_Ext {
      return documentMetadataBCHelper.Subtype_ext
    }
    
    // 'value' attribute on TextInput (id=DocumentName_Input) at DocumentMetadataEditInputSet.pcf: line 57, column 45
    function value_22 () : java.lang.String {
      return documentMetadataBCHelper.Name
    }
    
    // 'value' attribute on CheckBoxInput (id=descriptionEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 88, column 60
    function value_41 () : java.lang.Boolean {
      return documentMetadataBCHelper.DescriptionEnabled
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at DocumentMetadataEditInputSet.pcf: line 97, column 53
    function value_48 () : java.lang.String {
      return documentMetadataBCHelper.Description
    }
    
    // 'value' attribute on CheckBoxInput (id=allEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 32, column 60
    function value_5 () : java.lang.Boolean {
      return documentMetadataBCHelper.AllFieldsEnabled
    }
    
    // 'value' attribute on CheckBoxInput (id=mimeTypeEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 105, column 60
    function value_55 () : java.lang.Boolean {
      return documentMetadataBCHelper.MimeTypeEnabled
    }
    
    // 'value' attribute on RangeInput (id=MimeType_Input) at DocumentMetadataEditInputSet.pcf: line 118, column 37
    function value_63 () : java.lang.String {
      return documentMetadataBCHelper.MimeType
    }
    
    // 'value' attribute on CheckBoxInput (id=languageEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 126, column 118
    function value_74 () : java.lang.Boolean {
      return documentMetadataBCHelper.LanguageEnabled
    }
    
    // 'value' attribute on RangeInput (id=Language_Input) at DocumentMetadataEditInputSet.pcf: line 138, column 65
    function value_82 () : typekey.LanguageType {
      return documentMetadataBCHelper.Language
    }
    
    // 'value' attribute on CheckBoxInput (id=authorEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 154, column 60
    function value_95 () : java.lang.Boolean {
      return documentMetadataBCHelper.AuthorEnabled
    }
    
    // 'valueRange' attribute on RangeInput (id=Subtype_Input) at DocumentMetadataEditInputSet.pcf: line 267, column 53
    function verifyValueRangeIsAllowedType_181 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Subtype_Input) at DocumentMetadataEditInputSet.pcf: line 267, column 53
    function verifyValueRangeIsAllowedType_181 ($$arg :  typekey.OnBaseDocumentSubtype_Ext[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=MimeType_Input) at DocumentMetadataEditInputSet.pcf: line 118, column 37
    function verifyValueRangeIsAllowedType_68 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=MimeType_Input) at DocumentMetadataEditInputSet.pcf: line 118, column 37
    function verifyValueRangeIsAllowedType_68 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Language_Input) at DocumentMetadataEditInputSet.pcf: line 138, column 65
    function verifyValueRangeIsAllowedType_86 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Language_Input) at DocumentMetadataEditInputSet.pcf: line 138, column 65
    function verifyValueRangeIsAllowedType_86 ($$arg :  typekey.LanguageType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Subtype_Input) at DocumentMetadataEditInputSet.pcf: line 267, column 53
    function verifyValueRange_182 () : void {
      var __valueRangeArg = acc.onbase.util.PCFUtils.getDocumentSubTypeRange(documentMetadataBCHelper.Type)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_181(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=MimeType_Input) at DocumentMetadataEditInputSet.pcf: line 118, column 37
    function verifyValueRange_69 () : void {
      var __valueRangeArg = gw.document.DocumentsUtilBase.getMimeTypes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_68(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=Language_Input) at DocumentMetadataEditInputSet.pcf: line 138, column 65
    function verifyValueRange_87 () : void {
      var __valueRangeArg = LanguageType.getTypeKeys( false )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_86(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=DocumentName_Input) at DocumentMetadataEditInputSet.pcf: line 57, column 45
    function visible_21 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'visible' attribute on Link (id=UnityLink) at DocumentMetadataEditInputSet.pcf: line 70, column 143
    function visible_29 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    // 'visible' attribute on Link (id=NameLink) at DocumentMetadataEditInputSet.pcf: line 79, column 141
    function visible_34 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    // 'visible' attribute on ContentInput (id=DocumentLink) at DocumentMetadataEditInputSet.pcf: line 61, column 69
    function visible_38 () : java.lang.Boolean {
      return not CurrentLocation.InEditMode and document != null
    }
    
    // 'visible' attribute on CheckBoxInput (id=languageEnabled_Input) at DocumentMetadataEditInputSet.pcf: line 126, column 118
    function visible_73 () : java.lang.Boolean {
      return documentMetadataBCHelper.AllowFieldsSubset && LanguageType.getTypeKeys( false ).Count > 1
    }
    
    // 'visible' attribute on RangeInput (id=Language_Input) at DocumentMetadataEditInputSet.pcf: line 138, column 65
    function visible_81 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count > 1
    }
    
    property get document () : Document {
      return getVariableValue("document", 0) as Document
    }
    
    property set document ($arg :  Document) {
      setVariableValue("document", 0, $arg)
    }
    
    property get documentDetailsApplicationHelper () : gw.document.DocumentDetailsApplicationHelper {
      return getRequireValue("documentDetailsApplicationHelper", 0) as gw.document.DocumentDetailsApplicationHelper
    }
    
    property set documentDetailsApplicationHelper ($arg :  gw.document.DocumentDetailsApplicationHelper) {
      setRequireValue("documentDetailsApplicationHelper", 0, $arg)
    }
    
    property get documentMetadataBCHelper () : gw.document.DocumentMetadataBCHelper {
      return getVariableValue("documentMetadataBCHelper", 0) as gw.document.DocumentMetadataBCHelper
    }
    
    property set documentMetadataBCHelper ($arg :  gw.document.DocumentMetadataBCHelper) {
      setVariableValue("documentMetadataBCHelper", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    property get fromTemplate () : boolean {
      return getRequireValue("fromTemplate", 0) as java.lang.Boolean
    }
    
    property set fromTemplate ($arg :  boolean) {
      setRequireValue("fromTemplate", 0, $arg)
    }
    
    
  }
  
  
}