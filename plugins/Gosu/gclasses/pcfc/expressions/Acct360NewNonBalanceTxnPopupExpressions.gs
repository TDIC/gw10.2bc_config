package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/Acct360NewNonBalanceTxnPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class Acct360NewNonBalanceTxnPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/acct360/Acct360NewNonBalanceTxnPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Acct360NewNonBalanceTxnPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (acctObj :  Account) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=Acct360NewNonBalanceTxnPopup) at Acct360NewNonBalanceTxnPopup.pcf: line 11, column 97
    function beforeCommit_16 (pickedValue :  java.lang.Object) : void {
      gw.acc.acct360.accountview.AccountBalanceTxnTrackUtil.createNonFinancialTransaction(acctObj, polPeriod, transactionType, description)
    }
    
    // 'canVisit' attribute on Popup (id=Acct360NewNonBalanceTxnPopup) at Acct360NewNonBalanceTxnPopup.pcf: line 11, column 97
    static function canVisit_17 (acctObj :  Account) : java.lang.Boolean {
      return User.util.CurrentUser.isUnrestrictedUser()
    }
    
    // 'value' attribute on TextAreaInput (id=description_Input) at Acct360NewNonBalanceTxnPopup.pcf: line 50, column 34
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=polPeriod_Input) at Acct360NewNonBalanceTxnPopup.pcf: line 38, column 39
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      polPeriod = (__VALUE_TO_SET as PolicyPeriod)
    }
    
    // 'value' attribute on RangeInput (id=transactiontype_Input) at Acct360NewNonBalanceTxnPopup.pcf: line 45, column 48
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      transactionType = (__VALUE_TO_SET as AcctBalNonBalType_Ext)
    }
    
    // EditButtons at Acct360NewNonBalanceTxnPopup.pcf: line 28, column 23
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'valueRange' attribute on RangeInput (id=polPeriod_Input) at Acct360NewNonBalanceTxnPopup.pcf: line 38, column 39
    function valueRange_3 () : java.lang.Object {
      return acctObj.getActivePolicyPeriods()
    }
    
    // 'valueRange' attribute on RangeInput (id=transactiontype_Input) at Acct360NewNonBalanceTxnPopup.pcf: line 45, column 48
    function valueRange_9 () : java.lang.Object {
      return AcctBalNonBalType_Ext.getTypeKeys(false)
    }
    
    // 'value' attribute on RangeInput (id=polPeriod_Input) at Acct360NewNonBalanceTxnPopup.pcf: line 38, column 39
    function value_1 () : PolicyPeriod {
      return polPeriod
    }
    
    // 'value' attribute on TextAreaInput (id=description_Input) at Acct360NewNonBalanceTxnPopup.pcf: line 50, column 34
    function value_13 () : java.lang.String {
      return description
    }
    
    // 'value' attribute on RangeInput (id=transactiontype_Input) at Acct360NewNonBalanceTxnPopup.pcf: line 45, column 48
    function value_7 () : AcctBalNonBalType_Ext {
      return transactionType
    }
    
    // 'valueRange' attribute on RangeInput (id=transactiontype_Input) at Acct360NewNonBalanceTxnPopup.pcf: line 45, column 48
    function verifyValueRangeIsAllowedType_10 ($$arg :  AcctBalNonBalType_Ext[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=transactiontype_Input) at Acct360NewNonBalanceTxnPopup.pcf: line 45, column 48
    function verifyValueRangeIsAllowedType_10 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=transactiontype_Input) at Acct360NewNonBalanceTxnPopup.pcf: line 45, column 48
    function verifyValueRangeIsAllowedType_10 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=polPeriod_Input) at Acct360NewNonBalanceTxnPopup.pcf: line 38, column 39
    function verifyValueRangeIsAllowedType_4 ($$arg :  PolicyPeriod[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=polPeriod_Input) at Acct360NewNonBalanceTxnPopup.pcf: line 38, column 39
    function verifyValueRangeIsAllowedType_4 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=polPeriod_Input) at Acct360NewNonBalanceTxnPopup.pcf: line 38, column 39
    function verifyValueRangeIsAllowedType_4 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=transactiontype_Input) at Acct360NewNonBalanceTxnPopup.pcf: line 45, column 48
    function verifyValueRange_11 () : void {
      var __valueRangeArg = AcctBalNonBalType_Ext.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_10(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=polPeriod_Input) at Acct360NewNonBalanceTxnPopup.pcf: line 38, column 39
    function verifyValueRange_5 () : void {
      var __valueRangeArg = acctObj.getActivePolicyPeriods()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_4(__valueRangeArg)
    }
    
    override property get CurrentLocation () : pcf.Acct360NewNonBalanceTxnPopup {
      return super.CurrentLocation as pcf.Acct360NewNonBalanceTxnPopup
    }
    
    property get acctObj () : Account {
      return getVariableValue("acctObj", 0) as Account
    }
    
    property set acctObj ($arg :  Account) {
      setVariableValue("acctObj", 0, $arg)
    }
    
    property get description () : String {
      return getVariableValue("description", 0) as String
    }
    
    property set description ($arg :  String) {
      setVariableValue("description", 0, $arg)
    }
    
    property get polPeriod () : PolicyPeriod {
      return getVariableValue("polPeriod", 0) as PolicyPeriod
    }
    
    property set polPeriod ($arg :  PolicyPeriod) {
      setVariableValue("polPeriod", 0, $arg)
    }
    
    property get transactionType () : AcctBalNonBalType_Ext {
      return getVariableValue("transactionType", 0) as AcctBalNonBalType_Ext
    }
    
    property set transactionType ($arg :  AcctBalNonBalType_Ext) {
      setVariableValue("transactionType", 0, $arg)
    }
    
    
  }
  
  
}