package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DirectBillSuspenseItemSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DirectBillSuspenseItemSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/search/DirectBillSuspenseItemSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DirectBillSuspenseItemSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    function editAssociatedDirectBillPayment(suspenseItemView : DirectSuspPmntItemSearchView) {
      var directBillMoneyReceived = suspenseItemView.BaseDist.BaseMoneyReceived as DirectBillMoneyRcvd;
      ModifyDirectBillPaymentPopup.push(directBillMoneyReceived.Account, directBillMoneyReceived, 
                              gw.api.web.payment.WhenModifyingDirectBillMoney.EditDistribution);
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/DirectBillSuspenseItemSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ButtonCell (id=ReleaseSuspenseItem_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 34, column 116
    function action_7 () : void {
      editAssociatedDirectBillPayment(suspenseItem)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 49, column 92
    function currency_15 () : typekey.Currency {
      return suspenseItem.Currency
    }
    
    // 'value' attribute on TextCell (id=AccountName_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 59, column 78
    function valueRoot_21 () : java.lang.Object {
      return suspenseItem.DirectSuspPmntItem.Account
    }
    
    // 'value' attribute on TypeKeyCell (id=ProductLine_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 65, column 46
    function valueRoot_24 () : java.lang.Object {
      return suspenseItem.Policy_TDIC
    }
    
    // 'value' attribute on TypeKeyCell (id=State_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 71, column 51
    function valueRoot_27 () : java.lang.Object {
      return suspenseItem.Policy_TDIC.LatestPolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 39, column 31
    function valueRoot_9 () : java.lang.Object {
      return suspenseItem
    }
    
    // 'value' attribute on DateCell (id=ExecutedDate_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 43, column 52
    function value_11 () : java.util.Date {
      return suspenseItem.ExecutedDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 49, column 92
    function value_14 () : gw.pl.currency.MonetaryAmount {
      return suspenseItem.GrossAmountToApply.ofCurrency(suspenseItem.Currency)
    }
    
    // 'value' attribute on TextCell (id=PaymentComments_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 54, column 55
    function value_17 () : java.lang.String {
      return suspenseItem.PaymentComments
    }
    
    // 'value' attribute on TextCell (id=AccountName_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 59, column 78
    function value_20 () : java.lang.String {
      return suspenseItem.DirectSuspPmntItem.Account.AccountName
    }
    
    // 'value' attribute on TypeKeyCell (id=ProductLine_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 65, column 46
    function value_23 () : typekey.LOBCode {
      return suspenseItem.Policy_TDIC.LOBCode
    }
    
    // 'value' attribute on TypeKeyCell (id=State_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 71, column 51
    function value_26 () : typekey.Jurisdiction {
      return suspenseItem.Policy_TDIC.LatestPolicyPeriod.RiskJurisdiction
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 39, column 31
    function value_8 () : java.lang.String {
      return suspenseItem.PolicyNumber
    }
    
    property get suspenseItem () : entity.DirectSuspPmntItemSearchView {
      return getIteratedValue(2) as entity.DirectSuspPmntItemSearchView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/DirectBillSuspenseItemSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends DirectBillSuspenseItemSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at DirectBillSuspenseItemSearchScreen.pcf: line 16, column 62
    function def_onEnter_0 (def :  pcf.DirectBillSuspenseItemSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at DirectBillSuspenseItemSearchScreen.pcf: line 16, column 62
    function def_refreshVariables_1 (def :  pcf.DirectBillSuspenseItemSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'maxSearchResults' attribute on SearchPanel at DirectBillSuspenseItemSearchScreen.pcf: line 14, column 96
    function maxSearchResults_30 () : java.lang.Object {
      return gw.api.system.BCConfigParameters.MaxSearchResults.Value
    }
    
    // 'searchCriteria' attribute on SearchPanel at DirectBillSuspenseItemSearchScreen.pcf: line 14, column 96
    function searchCriteria_32 () : gw.search.DirectSuspPmntItemSearchCriteria {
      return new gw.search.DirectSuspPmntItemSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at DirectBillSuspenseItemSearchScreen.pcf: line 14, column 96
    function search_31 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria) as gw.api.database.IQueryBeanResult<DirectSuspPmntItemSearchView>
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 39, column 31
    function sortValue_2 (suspenseItem :  entity.DirectSuspPmntItemSearchView) : java.lang.Object {
      return suspenseItem.PolicyNumber
    }
    
    // 'value' attribute on DateCell (id=ExecutedDate_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 43, column 52
    function sortValue_3 (suspenseItem :  entity.DirectSuspPmntItemSearchView) : java.lang.Object {
      return suspenseItem.ExecutedDate
    }
    
    // 'sortBy' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 49, column 92
    function sortValue_4 (suspenseItem :  entity.DirectSuspPmntItemSearchView) : java.lang.Object {
      return suspenseItem.Currency
    }
    
    // 'sortBy' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 49, column 92
    function sortValue_5 (suspenseItem :  entity.DirectSuspPmntItemSearchView) : java.lang.Object {
      return  suspenseItem.GrossAmountToApply
    }
    
    // 'value' attribute on TextCell (id=PaymentComments_Cell) at DirectBillSuspenseItemSearchScreen.pcf: line 54, column 55
    function sortValue_6 (suspenseItem :  entity.DirectSuspPmntItemSearchView) : java.lang.Object {
      return suspenseItem.PaymentComments
    }
    
    // 'value' attribute on RowIterator at DirectBillSuspenseItemSearchScreen.pcf: line 29, column 101
    function value_29 () : gw.api.database.IQueryBeanResult<entity.DirectSuspPmntItemSearchView> {
      return suspenseItems
    }
    
    property get searchCriteria () : gw.search.DirectSuspPmntItemSearchCriteria {
      return getCriteriaValue(1) as gw.search.DirectSuspPmntItemSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.DirectSuspPmntItemSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    property get suspenseItems () : gw.api.database.IQueryBeanResult<DirectSuspPmntItemSearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<DirectSuspPmntItemSearchView>
    }
    
    
  }
  
  
}