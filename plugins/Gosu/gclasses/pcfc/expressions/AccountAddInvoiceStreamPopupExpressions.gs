package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountAddInvoiceStreamPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountAddInvoiceStreamPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountAddInvoiceStreamPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountAddInvoiceStreamPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'def' attribute on PanelRef at AccountAddInvoiceStreamPopup.pcf: line 26, column 111
    function def_onEnter_3 (def :  pcf.AccountInvoiceStreamDetailDV) : void {
      def.onEnter(account, new gw.api.web.invoice.InvoiceStreamView(invoiceStream))
    }
    
    // 'def' attribute on PanelRef at AccountAddInvoiceStreamPopup.pcf: line 26, column 111
    function def_refreshVariables_4 (def :  pcf.AccountInvoiceStreamDetailDV) : void {
      def.refreshVariables(account, new gw.api.web.invoice.InvoiceStreamView(invoiceStream))
    }
    
    // 'initialValue' attribute on Variable at AccountAddInvoiceStreamPopup.pcf: line 19, column 29
    function initialValue_0 () : InvoiceStream {
      return gw.api.domain.invoice.InvoiceStreamFactory.createInvoiceStreamFor(account.DefaultUnappliedFund, Periodicity.TC_MONTHLY)
    }
    
    // EditButtons at AccountAddInvoiceStreamPopup.pcf: line 23, column 38
    function label_2 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'pickValue' attribute on EditButtons at AccountAddInvoiceStreamPopup.pcf: line 23, column 38
    function pickValue_1 () : InvoiceStream {
      return invoiceStream
    }
    
    override property get CurrentLocation () : pcf.AccountAddInvoiceStreamPopup {
      return super.CurrentLocation as pcf.AccountAddInvoiceStreamPopup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get invoiceStream () : InvoiceStream {
      return getVariableValue("invoiceStream", 0) as InvoiceStream
    }
    
    property set invoiceStream ($arg :  InvoiceStream) {
      setVariableValue("invoiceStream", 0, $arg)
    }
    
    
  }
  
  
}