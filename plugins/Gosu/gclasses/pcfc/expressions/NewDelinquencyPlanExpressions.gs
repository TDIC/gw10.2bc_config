package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.CurrencyUtil
@javax.annotation.Generated("config/web/pcf/admin/delinquency/NewDelinquencyPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewDelinquencyPlanExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/delinquency/NewDelinquencyPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewDelinquencyPlanExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (currency :  Currency) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=NewDelinquencyPlan) at NewDelinquencyPlan.pcf: line 15, column 70
    function afterCancel_3 () : void {
      DelinquencyPlans.go()
    }
    
    // 'afterCancel' attribute on Page (id=NewDelinquencyPlan) at NewDelinquencyPlan.pcf: line 15, column 70
    function afterCancel_dest_4 () : pcf.api.Destination {
      return pcf.DelinquencyPlans.createDestination()
    }
    
    // 'afterCommit' attribute on Page (id=NewDelinquencyPlan) at NewDelinquencyPlan.pcf: line 15, column 70
    function afterCommit_5 (TopLocation :  pcf.api.Location) : void {
      DelinquencyPlans.go()
    }
    
    // 'canVisit' attribute on Page (id=NewDelinquencyPlan) at NewDelinquencyPlan.pcf: line 15, column 70
    static function canVisit_6 (currency :  Currency) : java.lang.Boolean {
      return perm.System.admintabview and perm.System.delplancreate
    }
    
    // 'def' attribute on ScreenRef at NewDelinquencyPlan.pcf: line 26, column 52
    function def_onEnter_1 (def :  pcf.DelinquencyPlanDetailScreen) : void {
      def.onEnter(dlnqPlan)
    }
    
    // 'def' attribute on ScreenRef at NewDelinquencyPlan.pcf: line 26, column 52
    function def_refreshVariables_2 (def :  pcf.DelinquencyPlanDetailScreen) : void {
      def.refreshVariables(dlnqPlan)
    }
    
    // 'initialValue' attribute on Variable at NewDelinquencyPlan.pcf: line 21, column 31
    function initialValue_0 () : DelinquencyPlan {
      return initDelinquencyPlan()
    }
    
    // 'onResume' attribute on Page (id=NewDelinquencyPlan) at NewDelinquencyPlan.pcf: line 15, column 70
    function onResume_7 () : void {
      preValidatePlan( dlnqPlan )
    }
    
    // 'parent' attribute on Page (id=NewDelinquencyPlan) at NewDelinquencyPlan.pcf: line 15, column 70
    static function parent_8 (currency :  Currency) : pcf.api.Destination {
      return pcf.DelinquencyPlans.createDestination()
    }
    
    override property get CurrentLocation () : pcf.NewDelinquencyPlan {
      return super.CurrentLocation as pcf.NewDelinquencyPlan
    }
    
    property get currency () : Currency {
      return getVariableValue("currency", 0) as Currency
    }
    
    property set currency ($arg :  Currency) {
      setVariableValue("currency", 0, $arg)
    }
    
    property get dlnqPlan () : DelinquencyPlan {
      return getVariableValue("dlnqPlan", 0) as DelinquencyPlan
    }
    
    property set dlnqPlan ($arg :  DelinquencyPlan) {
      setVariableValue("dlnqPlan", 0, $arg)
    }
    
    
    function initDelinquencyPlan() : DelinquencyPlan {
      var newDelinquencyPlan = new DelinquencyPlan(CurrentLocation);
      if (not isMultiCurrencyMode()) {
        newDelinquencyPlan.addToCurrencies(currency);
      }
      newDelinquencyPlan.EffectiveDate = gw.api.util.DateUtil.currentDate();
      newDelinquencyPlan.ApplicableSegments = typekey.ApplicableSegments.TC_ALL
      return newDelinquencyPlan;
    }
    
    function preValidatePlan(plan : DelinquencyPlan) {
      validatePlan(plan);
      gw.api.web.delinquency.DelinquencyPlanUtil.validateDelinquencyPlanEvents(plan);
    }
    
    function validatePlan(plan : DelinquencyPlan) {
      gw.api.web.delinquency.DelinquencyPlanUtil.validateDelinquencyPlanReasons(plan);
    }
    
    function isMultiCurrencyMode(): boolean {
      return CurrencyUtil.isMultiCurrencyMode() and Currency.getTypeKeys(true).Count > 1
    }
    
    
  }
  
  
}