package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.pl.currency.MonetaryAmount
@javax.annotation.Generated("config/web/pcf/invoice/ItemEventPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ItemEventPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/invoice/ItemEventPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ItemEventPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=PolicyRoleFilter_Input) at ItemEventPanelSet.pcf: line 33, column 48
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectedPolicyRole = (__VALUE_TO_SET as typekey.PolicyRole)
    }
    
    // 'initialValue' attribute on Variable at ItemEventPanelSet.pcf: line 14, column 37
    function initialValue_0 () : List<ItemEvent> {
      return getItemEvents()
    }
    
    // 'initialValue' attribute on Variable at ItemEventPanelSet.pcf: line 18, column 26
    function initialValue_1 () : PolicyRole {
      return null
    }
    
    // 'value' attribute on TextCell (id=eventTransaction_Cell) at ItemEventPanelSet.pcf: line 61, column 71
    function sortValue_10 (itemEvent :  entity.ItemEvent) : java.lang.Object {
      return itemEvent.Transaction.TransactionNumber 
    }
    
    // 'value' attribute on MonetaryAmountCell (id=gross_Cell) at ItemEventPanelSet.pcf: line 69, column 59
    function sortValue_11 (itemEvent :  entity.ItemEvent) : java.lang.Object {
      return itemEvent.GrossAmountChanged
    }
    
    // 'value' attribute on MonetaryAmountCell (id=reserve_Cell) at ItemEventPanelSet.pcf: line 78, column 86
    function sortValue_12 (itemEvent :  entity.ItemEvent) : java.lang.Object {
      return getReserveAmount(itemEvent)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=writeOff_Cell) at ItemEventPanelSet.pcf: line 86, column 59
    function sortValue_13 (itemEvent :  entity.ItemEvent) : java.lang.Object {
      return itemEvent.CommissionWriteoff
    }
    
    // 'value' attribute on MonetaryAmountCell (id=directBillEarned_Cell) at ItemEventPanelSet.pcf: line 94, column 57
    function sortValue_14 (itemEvent :  entity.ItemEvent) : java.lang.Object {
      return itemEvent.DirectBillEarned
    }
    
    // 'value' attribute on TextCell (id=eventTxnStatement_Cell) at ItemEventPanelSet.pcf: line 101, column 59
    function sortValue_15 (itemEvent :  entity.ItemEvent) : java.lang.Object {
      return itemEvent.Statement  
    }
    
    // 'value' attribute on MonetaryAmountCell (id=agencyBillRetained_Cell) at ItemEventPanelSet.pcf: line 110, column 86
    function sortValue_16 (itemEvent :  entity.ItemEvent) : java.lang.Object {
      return itemEvent.AgencyBillRetained
    }
    
    // 'value' attribute on DateCell (id=eventDate_Cell) at ItemEventPanelSet.pcf: line 51, column 50
    function sortValue_8 (itemEvent :  entity.ItemEvent) : java.lang.Object {
      return itemEvent.EventDate
    }
    
    // 'value' attribute on TextCell (id=eventType_Cell) at ItemEventPanelSet.pcf: line 55, column 54
    function sortValue_9 (itemEvent :  entity.ItemEvent) : java.lang.Object {
      return itemEvent.DisplayString
    }
    
    // '$$sumValue' attribute on RowIterator at ItemEventPanelSet.pcf: line 69, column 59
    function sumValueRoot_18 (itemEvent :  entity.ItemEvent) : java.lang.Object {
      return itemEvent
    }
    
    // 'footerSumValue' attribute on RowIterator at ItemEventPanelSet.pcf: line 69, column 59
    function sumValue_17 (itemEvent :  entity.ItemEvent) : java.lang.Object {
      return itemEvent.GrossAmountChanged
    }
    
    // 'footerSumValue' attribute on RowIterator at ItemEventPanelSet.pcf: line 78, column 86
    function sumValue_19 (itemEvent :  entity.ItemEvent) : java.lang.Object {
      return getReserveAmount(itemEvent)
    }
    
    // 'footerSumValue' attribute on RowIterator at ItemEventPanelSet.pcf: line 86, column 59
    function sumValue_20 (itemEvent :  entity.ItemEvent) : java.lang.Object {
      return itemEvent.CommissionWriteoff
    }
    
    // 'footerSumValue' attribute on RowIterator at ItemEventPanelSet.pcf: line 94, column 57
    function sumValue_22 (itemEvent :  entity.ItemEvent) : java.lang.Object {
      return itemEvent.DirectBillEarned
    }
    
    // 'footerSumValue' attribute on RowIterator at ItemEventPanelSet.pcf: line 110, column 86
    function sumValue_24 (itemEvent :  entity.ItemEvent) : java.lang.Object {
      return itemEvent.AgencyBillRetained
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=PolicyRoleFilter_Input) at ItemEventPanelSet.pcf: line 33, column 48
    function valueRange_4 () : java.lang.Object {
      return PolicyRole.getTypeKeys(false)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=PolicyRoleFilter_Input) at ItemEventPanelSet.pcf: line 33, column 48
    function value_2 () : typekey.PolicyRole {
      return selectedPolicyRole
    }
    
    // 'value' attribute on RowIterator at ItemEventPanelSet.pcf: line 44, column 68
    function value_70 () : java.util.List<entity.ItemEvent> {
      return itemEvents
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=PolicyRoleFilter_Input) at ItemEventPanelSet.pcf: line 33, column 48
    function verifyValueRangeIsAllowedType_5 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=PolicyRoleFilter_Input) at ItemEventPanelSet.pcf: line 33, column 48
    function verifyValueRangeIsAllowedType_5 ($$arg :  typekey.PolicyRole[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=PolicyRoleFilter_Input) at ItemEventPanelSet.pcf: line 33, column 48
    function verifyValueRange_6 () : void {
      var __valueRangeArg = PolicyRole.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_5(__valueRangeArg)
    }
    
    property get invoiceItem () : InvoiceItem {
      return getRequireValue("invoiceItem", 0) as InvoiceItem
    }
    
    property set invoiceItem ($arg :  InvoiceItem) {
      setRequireValue("invoiceItem", 0, $arg)
    }
    
    property get itemEvents () : List<ItemEvent> {
      return getVariableValue("itemEvents", 0) as List<ItemEvent>
    }
    
    property set itemEvents ($arg :  List<ItemEvent>) {
      setVariableValue("itemEvents", 0, $arg)
    }
    
    property get selectedPolicyRole () : PolicyRole {
      return getVariableValue("selectedPolicyRole", 0) as PolicyRole
    }
    
    property set selectedPolicyRole ($arg :  PolicyRole) {
      setVariableValue("selectedPolicyRole", 0, $arg)
    }
    
    
          function isReserveAndPaidVisibleOnPaymentEvent(itemEvent: ItemEvent): Boolean {
            if (!(itemEvent.EventType.equals(ItemEventType.TC_PAYMENT) or itemEvent.EventType.equals(ItemEventType.TC_PAYMENTREVERSED) or itemEvent.EventType.equals(ItemEventType.TC_PAYMENTMOVEDFROM) or itemEvent.EventType.equals(ItemEventType.TC_PAYMENTMOVEDTO))) {
              return true
            }
            return selectedPolicyRole == null or selectedPolicyRole.equals(PolicyRole.TC_PRIMARY)
          }
    
          function getItemEvents(): java.util.List<ItemEvent> {
            // In 9.0, the CmsnPayment item event type is retired, and the item event of this type is no longer made. So we filter out
            // events of retired types for consistent display of pre-9.0 and 9.0 (or later) data.
            return invoiceItem.getItemEventsForRole(selectedPolicyRole).where(\itemEvent -> !itemEvent.EventType.Retired)
          }
    
          function getReserveAmount(itemEvent: ItemEvent): MonetaryAmount {
            return (itemEvent.EventType == ItemEventType.TC_POINTINTIME || itemEvent.EventType == ItemEventType.TC_RETROACTIVE) ? 0bd.ofCurrency(itemEvent.Currency) : itemEvent.CmsnReserveChanged
          }
    
        
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/invoice/ItemEventPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ItemEventPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=eventTransaction_Cell) at ItemEventPanelSet.pcf: line 61, column 71
    function actionAvailable_34 () : java.lang.Boolean {
      return itemEvent.Transaction != null
    }
    
    // 'actionAvailable' attribute on TextCell (id=eventTxnStatement_Cell) at ItemEventPanelSet.pcf: line 101, column 59
    function actionAvailable_60 () : java.lang.Boolean {
      return itemEvent.Statement != null
    }
    
    // 'action' attribute on TextCell (id=eventTransaction_Cell) at ItemEventPanelSet.pcf: line 61, column 71
    function action_32 () : void {
      TransactionDetailPopup.push(itemEvent.Transaction)
    }
    
    // 'action' attribute on TextCell (id=eventTxnStatement_Cell) at ItemEventPanelSet.pcf: line 101, column 59
    function action_58 () : void {
      pcf.ProducerStatement.go(itemEvent.Statement)
    }
    
    // 'action' attribute on TextCell (id=eventTransaction_Cell) at ItemEventPanelSet.pcf: line 61, column 71
    function action_dest_33 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(itemEvent.Transaction)
    }
    
    // 'action' attribute on TextCell (id=eventTxnStatement_Cell) at ItemEventPanelSet.pcf: line 101, column 59
    function action_dest_59 () : pcf.api.Destination {
      return pcf.ProducerStatement.createDestination(itemEvent.Statement)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=gross_Cell) at ItemEventPanelSet.pcf: line 69, column 59
    function currency_41 () : typekey.Currency {
      return invoiceItem.Currency
    }
    
    // 'value' attribute on DateCell (id=eventDate_Cell) at ItemEventPanelSet.pcf: line 51, column 50
    function valueRoot_27 () : java.lang.Object {
      return itemEvent
    }
    
    // 'value' attribute on TextCell (id=eventTransaction_Cell) at ItemEventPanelSet.pcf: line 61, column 71
    function valueRoot_36 () : java.lang.Object {
      return itemEvent.Transaction
    }
    
    // 'value' attribute on DateCell (id=eventDate_Cell) at ItemEventPanelSet.pcf: line 51, column 50
    function value_26 () : java.util.Date {
      return itemEvent.EventDate
    }
    
    // 'value' attribute on TextCell (id=eventType_Cell) at ItemEventPanelSet.pcf: line 55, column 54
    function value_29 () : java.lang.String {
      return itemEvent.DisplayString
    }
    
    // 'value' attribute on TextCell (id=eventTransaction_Cell) at ItemEventPanelSet.pcf: line 61, column 71
    function value_35 () : java.lang.String {
      return itemEvent.Transaction.TransactionNumber 
    }
    
    // 'value' attribute on MonetaryAmountCell (id=gross_Cell) at ItemEventPanelSet.pcf: line 69, column 59
    function value_39 () : gw.pl.currency.MonetaryAmount {
      return itemEvent.GrossAmountChanged
    }
    
    // 'value' attribute on MonetaryAmountCell (id=reserve_Cell) at ItemEventPanelSet.pcf: line 78, column 86
    function value_45 () : gw.pl.currency.MonetaryAmount {
      return getReserveAmount(itemEvent)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=writeOff_Cell) at ItemEventPanelSet.pcf: line 86, column 59
    function value_49 () : gw.pl.currency.MonetaryAmount {
      return itemEvent.CommissionWriteoff
    }
    
    // 'value' attribute on MonetaryAmountCell (id=directBillEarned_Cell) at ItemEventPanelSet.pcf: line 94, column 57
    function value_54 () : gw.pl.currency.MonetaryAmount {
      return itemEvent.DirectBillEarned
    }
    
    // 'value' attribute on TextCell (id=eventTxnStatement_Cell) at ItemEventPanelSet.pcf: line 101, column 59
    function value_61 () : entity.ProducerStatement {
      return itemEvent.Statement  
    }
    
    // 'value' attribute on MonetaryAmountCell (id=agencyBillRetained_Cell) at ItemEventPanelSet.pcf: line 110, column 86
    function value_66 () : gw.pl.currency.MonetaryAmount {
      return itemEvent.AgencyBillRetained
    }
    
    // 'valueVisible' attribute on MonetaryAmountCell (id=reserve_Cell) at ItemEventPanelSet.pcf: line 78, column 86
    function visible_43 () : java.lang.Boolean {
      return isReserveAndPaidVisibleOnPaymentEvent(itemEvent)
    }
    
    property get itemEvent () : entity.ItemEvent {
      return getIteratedValue(1) as entity.ItemEvent
    }
    
    
  }
  
  
}