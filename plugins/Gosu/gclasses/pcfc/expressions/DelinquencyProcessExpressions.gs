package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcess.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyProcessExpressions {
  @javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcess.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyProcessExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account, delinquencyProcess :  DelinquencyProcess) : int {
      return 0
    }
    
    // 'def' attribute on PanelRef at DelinquencyProcess.pcf: line 23, column 69
    function def_onEnter_0 (def :  pcf.DelinquencyProcessDetailPanelSet) : void {
      def.onEnter(delinquencyProcess)
    }
    
    // 'def' attribute on PanelRef at DelinquencyProcess.pcf: line 23, column 69
    function def_refreshVariables_1 (def :  pcf.DelinquencyProcessDetailPanelSet) : void {
      def.refreshVariables(delinquencyProcess)
    }
    
    // 'parent' attribute on Page (id=DelinquencyProcess) at DelinquencyProcess.pcf: line 10, column 70
    static function parent_2 (account :  Account, delinquencyProcess :  DelinquencyProcess) : pcf.api.Destination {
      return pcf.AccountDetailDelinquencies.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.DelinquencyProcess {
      return super.CurrentLocation as pcf.DelinquencyProcess
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get delinquencyProcess () : DelinquencyProcess {
      return getVariableValue("delinquencyProcess", 0) as DelinquencyProcess
    }
    
    property set delinquencyProcess ($arg :  DelinquencyProcess) {
      setVariableValue("delinquencyProcess", 0, $arg)
    }
    
    
  }
  
  
}