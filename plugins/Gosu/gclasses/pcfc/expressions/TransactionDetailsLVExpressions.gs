package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/TransactionDetailsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransactionDetailsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/TransactionDetailsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TransactionDetailsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at TransactionDetailsLV.pcf: line 53, column 56
    function action_14 () : void {
      TransactionDetailPopup.push(policyTransaction)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at TransactionDetailsLV.pcf: line 53, column 56
    function action_dest_15 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(policyTransaction)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Unbilled_Cell) at TransactionDetailsLV.pcf: line 80, column 72
    function currency_27 () : typekey.Currency {
      return policyTransaction.Currency
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at TransactionDetailsLV.pcf: line 47, column 54
    function valueRoot_12 () : java.lang.Object {
      return policyTransaction
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at TransactionDetailsLV.pcf: line 47, column 54
    function value_11 () : java.util.Date {
      return policyTransaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at TransactionDetailsLV.pcf: line 53, column 56
    function value_16 () : java.lang.String {
      return policyTransaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=TransactionDesc_Cell) at TransactionDetailsLV.pcf: line 59, column 54
    function value_19 () : java.lang.String {
      return policyTransaction.LongDisplayName
    }
    
    // 'value' attribute on TextCell (id=ChargeType_Cell) at TransactionDetailsLV.pcf: line 66, column 50
    function value_22 () : entity.BillingInstruction {
      return policyTransaction typeis ChargeInstanceTxn ? policyTransaction.Charge.BillingInstruction : null
    }
    
    // 'value' attribute on TextCell (id=Invoice_Cell) at TransactionDetailsLV.pcf: line 73, column 39
    function value_24 () : entity.Invoice {
      return policyTransaction typeis ChargePaidFromUnapplied ? policyTransaction.InvoiceItem.Invoice : null
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Unbilled_Cell) at TransactionDetailsLV.pcf: line 80, column 72
    function value_26 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.getUnbilledTxAmount(policyTransaction)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Billed_Cell) at TransactionDetailsLV.pcf: line 87, column 70
    function value_29 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.getBilledTxAmount(policyTransaction)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PastDue_Cell) at TransactionDetailsLV.pcf: line 94, column 67
    function value_32 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.getDueTxAmount(policyTransaction)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Paid_Cell) at TransactionDetailsLV.pcf: line 102, column 75
    function value_36 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.getPaidTxAmount(policyTransaction)
    }
    
    // 'valueVisible' attribute on MonetaryAmountCell (id=Paid_Cell) at TransactionDetailsLV.pcf: line 102, column 75
    function visible_35 () : java.lang.Boolean {
      return not (policyTransaction typeis InitialChargeTxn)
    }
    
    property get policyTransaction () : entity.Transaction {
      return getIteratedValue(1) as entity.Transaction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/TransactionDetailsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransactionDetailsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'filter' attribute on ToolbarFilterOption at TransactionDetailsLV.pcf: line 25, column 82
    function filter_1 () : gw.api.filters.IFilter {
      return new gw.api.web.transaction.TransactionFilterSet.Last30Days()
    }
    
    // 'filter' attribute on ToolbarFilterOption at TransactionDetailsLV.pcf: line 27, column 82
    function filter_2 () : gw.api.filters.IFilter {
      return new gw.api.web.transaction.TransactionFilterSet.Last60Days()
    }
    
    // 'filter' attribute on ToolbarFilterOption at TransactionDetailsLV.pcf: line 29, column 82
    function filter_3 () : gw.api.filters.IFilter {
      return new gw.api.web.transaction.TransactionFilterSet.Last90Days()
    }
    
    // 'filter' attribute on ToolbarFilterOption at TransactionDetailsLV.pcf: line 31, column 88
    function filter_4 () : gw.api.filters.IFilter {
      return new gw.api.web.transaction.TransactionFilterSet.Previous12Months()
    }
    
    // 'filter' attribute on ToolbarFilterOption at TransactionDetailsLV.pcf: line 33, column 85
    function filter_5 () : gw.api.filters.IFilter {
      return new gw.api.web.transaction.TransactionFilterSet.PreviousMonth()
    }
    
    // 'filter' attribute on ToolbarFilterOption at TransactionDetailsLV.pcf: line 35, column 81
    function filter_6 () : gw.api.filters.IFilter {
      return new gw.api.web.transaction.TransactionFilterSet.ThisMonth()
    }
    
    // 'filter' attribute on ToolbarFilterOption at TransactionDetailsLV.pcf: line 37, column 82
    function filter_7 () : gw.api.filters.IFilter {
      return new gw.api.web.transaction.TransactionFilterSet.YearToDate()
    }
    
    // 'filter' attribute on ToolbarFilterOption at TransactionDetailsLV.pcf: line 39, column 61
    function filter_8 () : gw.api.filters.IFilter {
      return new gw.api.util.CoreFilters.AllFilter()
    }
    
    // 'initialValue' attribute on Variable at TransactionDetailsLV.pcf: line 15, column 67
    function initialValue_0 () : gw.api.database.IQueryBeanResult<Transaction> {
      return policyPeriod.Transactions as gw.api.database.IQueryBeanResult<Transaction>
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at TransactionDetailsLV.pcf: line 53, column 56
    function sortValue_10 (policyTransaction :  entity.Transaction) : java.lang.Object {
      return policyTransaction.TransactionNumber
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at TransactionDetailsLV.pcf: line 47, column 54
    function sortValue_9 (policyTransaction :  entity.Transaction) : java.lang.Object {
      return policyTransaction.TransactionDate
    }
    
    // 'value' attribute on RowIterator at TransactionDetailsLV.pcf: line 20, column 78
    function value_39 () : gw.api.database.IQueryBeanResult<entity.Transaction> {
      return policyTransactions
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get policyTransactions () : gw.api.database.IQueryBeanResult<Transaction> {
      return getVariableValue("policyTransactions", 0) as gw.api.database.IQueryBeanResult<Transaction>
    }
    
    property set policyTransactions ($arg :  gw.api.database.IQueryBeanResult<Transaction>) {
      setVariableValue("policyTransactions", 0, $arg)
    }
    
    
  }
  
  
}