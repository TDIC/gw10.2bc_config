package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/businesssettings/ClearOpenHandlers.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ClearOpenHandlersExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/ClearOpenHandlers.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClearOpenHandlersExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=clearOpenHandlersButton) at ClearOpenHandlers.pcf: line 27, column 52
    function action_2 () : void {
      handlersCleared = openHandlersClearer.findAndRemoveOpenHandlers(); buttonClicked = true
    }
    
    // 'canVisit' attribute on Page (id=ClearOpenHandlers) at ClearOpenHandlers.pcf: line 9, column 45
    static function canVisit_6 () : java.lang.Boolean {
      return perm.User.ViewDataChange
    }
    
    // 'initialValue' attribute on Variable at ClearOpenHandlers.pcf: line 13, column 85
    function initialValue_0 () : tdic.bc.integ.services.creditcard.TDIC_AuthorizeNetOpenHandlerCleanup {
      return new tdic.bc.integ.services.creditcard.TDIC_AuthorizeNetOpenHandlerCleanup()
    }
    
    // 'initialValue' attribute on Variable at ClearOpenHandlers.pcf: line 17, column 22
    function initialValue_1 () : String {
      return null
    }
    
    // 'label' attribute on Label (id=errorMsgLabel) at ClearOpenHandlers.pcf: line 38, column 48
    function label_5 () : java.lang.String {
      return "Error Clearing Open Handlers :" + handlersCleared
    }
    
    // Page (id=ClearOpenHandlers) at ClearOpenHandlers.pcf: line 9, column 45
    static function parent_7 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    // 'visible' attribute on Label (id=successMsgLabel) at ClearOpenHandlers.pcf: line 34, column 66
    function visible_3 () : java.lang.Boolean {
      return buttonClicked and handlersCleared == null
    }
    
    // 'visible' attribute on Label (id=errorMsgLabel) at ClearOpenHandlers.pcf: line 38, column 48
    function visible_4 () : java.lang.Boolean {
      return handlersCleared != null
    }
    
    override property get CurrentLocation () : pcf.ClearOpenHandlers {
      return super.CurrentLocation as pcf.ClearOpenHandlers
    }
    
    property get buttonClicked () : boolean {
      return getVariableValue("buttonClicked", 0) as java.lang.Boolean
    }
    
    property set buttonClicked ($arg :  boolean) {
      setVariableValue("buttonClicked", 0, $arg)
    }
    
    property get handlersCleared () : String {
      return getVariableValue("handlersCleared", 0) as String
    }
    
    property set handlersCleared ($arg :  String) {
      setVariableValue("handlersCleared", 0, $arg)
    }
    
    property get openHandlersClearer () : tdic.bc.integ.services.creditcard.TDIC_AuthorizeNetOpenHandlerCleanup {
      return getVariableValue("openHandlersClearer", 0) as tdic.bc.integ.services.creditcard.TDIC_AuthorizeNetOpenHandlerCleanup
    }
    
    property set openHandlersClearer ($arg :  tdic.bc.integ.services.creditcard.TDIC_AuthorizeNetOpenHandlerCleanup) {
      setVariableValue("openHandlersClearer", 0, $arg)
    }
    
    
  }
  
  
}