package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailContacts.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailContactsExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailContacts.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailContactsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    static function __constructorIndex (account :  Account, contactToSelectOnEnter :  AccountContact) : int {
      return 1
    }
    
    // 'action' attribute on MenuItem (id=addNewCompany) at AccountDetailContacts.pcf: line 35, column 89
    function action_2 () : void {
      NewAccountContactPopup.push(account, Company)
    }
    
    // 'action' attribute on MenuItem (id=addNewPerson) at AccountDetailContacts.pcf: line 39, column 88
    function action_4 () : void {
      NewAccountContactPopup.push(account, Person)
    }
    
    // 'action' attribute on PickerToolbarButton (id=addExistingContact) at AccountDetailContacts.pcf: line 51, column 96
    function action_8 () : void {
      ContactSearchPopup.push(false)
    }
    
    // 'action' attribute on MenuItem (id=addNewCompany) at AccountDetailContacts.pcf: line 35, column 89
    function action_dest_3 () : pcf.api.Destination {
      return pcf.NewAccountContactPopup.createDestination(account, Company)
    }
    
    // 'action' attribute on MenuItem (id=addNewPerson) at AccountDetailContacts.pcf: line 39, column 88
    function action_dest_5 () : pcf.api.Destination {
      return pcf.NewAccountContactPopup.createDestination(account, Person)
    }
    
    // 'action' attribute on PickerToolbarButton (id=addExistingContact) at AccountDetailContacts.pcf: line 51, column 96
    function action_dest_9 () : pcf.api.Destination {
      return pcf.ContactSearchPopup.createDestination(false)
    }
    
    // 'beforeCommit' attribute on Page (id=AccountDetailContacts) at AccountDetailContacts.pcf: line 11, column 73
    function beforeCommit_31 (pickedValue :  java.lang.Object) : void {
      gw.api.web.account.AccountUtil.validateAccountContacts(account)
    }
    
    // 'canEdit' attribute on Page (id=AccountDetailContacts) at AccountDetailContacts.pcf: line 11, column 73
    function canEdit_32 () : java.lang.Boolean {
      return perm.AccountContact.edit
    }
    
    // 'canVisit' attribute on Page (id=AccountDetailContacts) at AccountDetailContacts.pcf: line 11, column 73
    static function canVisit_33 (account :  Account, contactToSelectOnEnter :  AccountContact) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctcontview
    }
    
    // EditButtons at AccountDetailContacts.pcf: line 26, column 50
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=addExistingContact) at AccountDetailContacts.pcf: line 51, column 96
    function onPick_10 (PickedValue :  gw.plugin.contact.ContactResult) : void {
      gw.contact.ContactConnection.connectContactToAccount(PickedValue, account)
    }
    
    // Page (id=AccountDetailContacts) at AccountDetailContacts.pcf: line 11, column 73
    static function parent_34 (account :  Account, contactToSelectOnEnter :  AccountContact) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    // 'editVisible' attribute on EditButtons at AccountDetailContacts.pcf: line 26, column 50
    function visible_0 () : java.lang.Boolean {
      return perm.System.acctcntedit
    }
    
    // 'visible' attribute on ToolbarButton (id=addNewContact) at AccountDetailContacts.pcf: line 31, column 47
    function visible_6 () : java.lang.Boolean {
      return perm.System.acctcntcreate
    }
    
    // 'removeVisible' attribute on IteratorButtons at AccountDetailContacts.pcf: line 44, column 54
    function visible_7 () : java.lang.Boolean {
      return perm.System.acctcntdelete
    }
    
    override property get CurrentLocation () : pcf.AccountDetailContacts {
      return super.CurrentLocation as pcf.AccountDetailContacts
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get contactToSelectOnEnter () : AccountContact {
      return getVariableValue("contactToSelectOnEnter", 0) as AccountContact
    }
    
    property set contactToSelectOnEnter ($arg :  AccountContact) {
      setVariableValue("contactToSelectOnEnter", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailContacts.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailPanelExpressionsImpl extends AccountDetailContactsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at AccountDetailContacts.pcf: line 93, column 53
    function def_onEnter_28 (def :  pcf.AccountContactCV) : void {
      def.onEnter(contact, false)
    }
    
    // 'def' attribute on PanelRef at AccountDetailContacts.pcf: line 93, column 53
    function def_refreshVariables_29 (def :  pcf.AccountContactCV) : void {
      def.refreshVariables(contact, false)
    }
    
    // 'selectionOnEnter' attribute on ListDetailPanel (id=DetailPanel) at AccountDetailContacts.pcf: line 57, column 40
    function selectionOnEnter_30 () : java.lang.Object {
      return contactToSelectOnEnter
    }
    
    // 'value' attribute on TextCell (id=ContactName_Cell) at AccountDetailContacts.pcf: line 74, column 52
    function sortValue_11 (accountContact :  entity.AccountContact) : java.lang.Object {
      return accountContact
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at AccountDetailContacts.pcf: line 79, column 45
    function sortValue_12 (accountContact :  entity.AccountContact) : java.lang.Object {
      return accountContact.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=ContactRoles_Cell) at AccountDetailContacts.pcf: line 83, column 92
    function sortValue_13 (accountContact :  entity.AccountContact) : java.lang.Object {
      return gw.api.web.account.AccountUtil.getRolesForDisplay(accountContact)
    }
    
    // 'value' attribute on BooleanRadioCell (id=ContactPrimaryPayer_Cell) at AccountDetailContacts.pcf: line 87, column 54
    function sortValue_14 (accountContact :  entity.AccountContact) : java.lang.Object {
      return accountContact.PrimaryPayer
    }
    
    // 'toRemove' attribute on RowIterator (id=accountContactIterator) at AccountDetailContacts.pcf: line 67, column 49
    function toRemove_25 (accountContact :  entity.AccountContact) : void {
      account.removeFromContacts(accountContact)
    }
    
    // 'value' attribute on RowIterator (id=accountContactIterator) at AccountDetailContacts.pcf: line 67, column 49
    function value_26 () : entity.AccountContact[] {
      return account.Contacts
    }
    
    // 'visible' attribute on PanelRef at AccountDetailContacts.pcf: line 93, column 53
    function visible_27 () : java.lang.Boolean {
      return account.Contacts.length > 0
    }
    
    property get contact () : AccountContact {
      return getCurrentSelection(1) as AccountContact
    }
    
    property set contact ($arg :  AccountContact) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailContacts.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at AccountDetailContacts.pcf: line 79, column 45
    function valueRoot_18 () : java.lang.Object {
      return accountContact.Contact
    }
    
    // 'value' attribute on BooleanRadioCell (id=ContactPrimaryPayer_Cell) at AccountDetailContacts.pcf: line 87, column 54
    function valueRoot_23 () : java.lang.Object {
      return accountContact
    }
    
    // 'value' attribute on TextCell (id=ContactName_Cell) at AccountDetailContacts.pcf: line 74, column 52
    function value_15 () : entity.AccountContact {
      return accountContact
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at AccountDetailContacts.pcf: line 79, column 45
    function value_17 () : entity.Address {
      return accountContact.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=ContactRoles_Cell) at AccountDetailContacts.pcf: line 83, column 92
    function value_20 () : java.lang.String {
      return gw.api.web.account.AccountUtil.getRolesForDisplay(accountContact)
    }
    
    // 'value' attribute on BooleanRadioCell (id=ContactPrimaryPayer_Cell) at AccountDetailContacts.pcf: line 87, column 54
    function value_22 () : java.lang.Boolean {
      return accountContact.PrimaryPayer
    }
    
    property get accountContact () : entity.AccountContact {
      return getIteratedValue(2) as entity.AccountContact
    }
    
    
  }
  
  
}