package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.acc.acct360.accountview.AccountBalanceTxnUtil
@javax.annotation.Generated("config/web/pcf/invoice/MoveInvoiceItemsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class MoveInvoiceItemsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/invoice/MoveInvoiceItemsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends MoveInvoiceItemsPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'canPick' attribute on RowIterator at MoveInvoiceItemsPopup.pcf: line 56, column 66
    function canPick_30 () : java.lang.Boolean {
      return !invoice.Frozen
    }
    
    // 'value' attribute on TextCell (id=InvoiceStatus_Cell) at MoveInvoiceItemsPopup.pcf: line 62, column 50
    function valueRoot_11 () : java.lang.Object {
      return invoice
    }
    
    // 'value' attribute on TextCell (id=InvoiceStatus_Cell) at MoveInvoiceItemsPopup.pcf: line 62, column 50
    function value_10 () : java.lang.String {
      return invoice.StatusForUI
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at MoveInvoiceItemsPopup.pcf: line 66, column 48
    function value_13 () : java.util.Date {
      return invoice.EventDate
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at MoveInvoiceItemsPopup.pcf: line 70, column 46
    function value_16 () : java.util.Date {
      return invoice.DueDate
    }
    
    // 'value' attribute on TextCell (id=InvoiceNumber_Cell) at MoveInvoiceItemsPopup.pcf: line 74, column 52
    function value_19 () : java.lang.String {
      return invoice.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=InvoiceStream_Cell) at MoveInvoiceItemsPopup.pcf: line 79, column 55
    function value_22 () : entity.InvoiceStream {
      return invoice.InvoiceStream
    }
    
    // 'value' attribute on BooleanRadioCell (id=InvoiceAdHoc_Cell) at MoveInvoiceItemsPopup.pcf: line 83, column 48
    function value_25 () : java.lang.Boolean {
      return invoice.isAdHoc()
    }
    
    // 'value' attribute on BooleanRadioCell (id=EquityViolation_Cell) at MoveInvoiceItemsPopup.pcf: line 88, column 59
    function value_27 () : java.lang.Boolean {
      return gw.api.util.EquityValidationHelper.isEquityViolated(invoiceItems?.toList(), invoice)
    }
    
    // 'visible' attribute on BooleanRadioCell (id=EquityViolation_Cell) at MoveInvoiceItemsPopup.pcf: line 88, column 59
    function visible_28 () : java.lang.Boolean {
      return isEquityViolationVisible()
    }
    
    property get invoice () : entity.Invoice {
      return getIteratedValue(1) as entity.Invoice
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/invoice/MoveInvoiceItemsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class MoveInvoiceItemsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (invoiceItems :  InvoiceItem[], payer :  gw.api.domain.invoice.InvoicePayer) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=MoveInvoiceItemsPopup) at MoveInvoiceItemsPopup.pcf: line 12, column 73
    function beforeCommit_33 (pickedValue :  Invoice) : void {
      gw.api.web.invoice.InvoiceUtil.validateItemsForMove(invoiceItems); gw.api.web.invoice.InvoiceUtil.moveInvoiceItems(pickedValue,invoiceItems);performAcct360Tracking() 
    }
    
    // 'def' attribute on ListViewInput (id=InvoiceItems) at MoveInvoiceItemsPopup.pcf: line 34, column 31
    function def_onEnter_0 (def :  pcf.InvoiceItemsLV) : void {
      def.onEnter(invoiceItems, null, false)
    }
    
    // 'def' attribute on ListViewInput (id=InvoiceItems) at MoveInvoiceItemsPopup.pcf: line 34, column 31
    function def_refreshVariables_1 (def :  pcf.InvoiceItemsLV) : void {
      def.refreshVariables(invoiceItems, null, false)
    }
    
    // 'sortBy' attribute on TextCell (id=InvoiceStatus_Cell) at MoveInvoiceItemsPopup.pcf: line 62, column 50
    function sortValue_2 (invoice :  entity.Invoice) : java.lang.Object {
      return invoice.Status
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at MoveInvoiceItemsPopup.pcf: line 66, column 48
    function sortValue_3 (invoice :  entity.Invoice) : java.lang.Object {
      return invoice.EventDate
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at MoveInvoiceItemsPopup.pcf: line 70, column 46
    function sortValue_4 (invoice :  entity.Invoice) : java.lang.Object {
      return invoice.DueDate
    }
    
    // 'value' attribute on TextCell (id=InvoiceNumber_Cell) at MoveInvoiceItemsPopup.pcf: line 74, column 52
    function sortValue_5 (invoice :  entity.Invoice) : java.lang.Object {
      return invoice.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=InvoiceStream_Cell) at MoveInvoiceItemsPopup.pcf: line 79, column 55
    function sortValue_6 (invoice :  entity.Invoice) : java.lang.Object {
      return invoice.InvoiceStream
    }
    
    // 'value' attribute on BooleanRadioCell (id=InvoiceAdHoc_Cell) at MoveInvoiceItemsPopup.pcf: line 83, column 48
    function sortValue_7 (invoice :  entity.Invoice) : java.lang.Object {
      return invoice.isAdHoc()
    }
    
    // 'value' attribute on BooleanRadioCell (id=EquityViolation_Cell) at MoveInvoiceItemsPopup.pcf: line 88, column 59
    function sortValue_8 (invoice :  entity.Invoice) : java.lang.Object {
      return gw.api.util.EquityValidationHelper.isEquityViolated(invoiceItems?.toList(), invoice)
    }
    
    // 'value' attribute on RowIterator at MoveInvoiceItemsPopup.pcf: line 56, column 66
    function value_32 () : java.util.List<entity.Invoice> {
      return payer.InvoicesSortedByEventDate
    }
    
    // 'visible' attribute on BooleanRadioCell (id=EquityViolation_Cell) at MoveInvoiceItemsPopup.pcf: line 88, column 59
    function visible_9 () : java.lang.Boolean {
      return isEquityViolationVisible()
    }
    
    override property get CurrentLocation () : pcf.MoveInvoiceItemsPopup {
      return super.CurrentLocation as pcf.MoveInvoiceItemsPopup
    }
    
    property get invoiceItems () : InvoiceItem[] {
      return getVariableValue("invoiceItems", 0) as InvoiceItem[]
    }
    
    property set invoiceItems ($arg :  InvoiceItem[]) {
      setVariableValue("invoiceItems", 0, $arg)
    }
    
    property get payer () : gw.api.domain.invoice.InvoicePayer {
      return getVariableValue("payer", 0) as gw.api.domain.invoice.InvoicePayer
    }
    
    property set payer ($arg :  gw.api.domain.invoice.InvoicePayer) {
      setVariableValue("payer", 0, $arg)
    }
    
    property get targetInvoice () : Invoice {
      return getVariableValue("targetInvoice", 0) as Invoice
    }
    
    property set targetInvoice ($arg :  Invoice) {
      setVariableValue("targetInvoice", 0, $arg)
    }
    
    function isEquityViolationVisible() : boolean {
      if (invoiceItems == null || invoiceItems.IsEmpty || invoiceItems[0].PolicyPeriod == null) {
        return false
      }
      return invoiceItems[0].PolicyPeriod.EquityWarningsEnabled && invoiceItems[0].PolicyPeriod.EquityCharges.contains(invoiceItems[0].Charge)
    }
      function performAcct360Tracking()
      {
        if (payer.Account) {
          AccountBalanceTxnUtil.createMoveItemRecord(payer as Account, invoiceItems.where(\ii -> ii.Invoice.BilledOrDue && !ii.AgencyBill && ii.Payer == payer), AcctBalItemMovedType_Ext.TC_INVOICEITEMMOVED)
        }
      }
    
    
  }
  
  
}