package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.search.StatementInvoiceSearchCriteria
uses gw.search.InvoiceItemQueryHelper.DistributionTypeEnum
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_AddStatementsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyDistributionWizard_AddStatementsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_AddStatementsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyDistributionWizard_AddStatementsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=cancel) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 31, column 62
    function action_2 () : void {
      CurrentLocation.cancel()
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_AddStatementsPopup.pcf: line 19, column 31
    function initialValue_0 () : entity.Producer {
      return wizardState.MoneySetup.Producer
    }
    
    // 'pickValue' attribute on CheckedValuesToolbarButton (id=SelectStatements) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 27, column 38
    function pickValue_1 (CheckedValues :  entity.StatementInvoice[]) : StatementInvoice[] {
      return CheckedValues
    }
    
    override property get CurrentLocation () : pcf.AgencyDistributionWizard_AddStatementsPopup {
      return super.CurrentLocation as pcf.AgencyDistributionWizard_AddStatementsPopup
    }
    
    property get producer () : entity.Producer {
      return getVariableValue("producer", 0) as entity.Producer
    }
    
    property set producer ($arg :  entity.Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    property get wizardState () : gw.agencybill.AgencyDistributionWizardHelper {
      return getVariableValue("wizardState", 0) as gw.agencybill.AgencyDistributionWizardHelper
    }
    
    property set wizardState ($arg :  gw.agencybill.AgencyDistributionWizardHelper) {
      setVariableValue("wizardState", 0, $arg)
    }
    
    
    function createSearchCriteria() : StatementInvoiceSearchCriteria {
      var distributionType = wizardState.IsPromise ? 
        DistributionTypeEnum.Promise : 
        DistributionTypeEnum.Payment
      var searchCriteria = new StatementInvoiceSearchCriteria(distributionType)
      searchCriteria.Producer = producer
      return searchCriteria
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_AddStatementsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SpecificStatementsPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 105, column 52
    function action_21 () : void {
      AgencyBillStatementDetailPopup.push(statement)
    }
    
    // 'action' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 105, column 52
    function action_dest_22 () : pcf.api.Destination {
      return pcf.AgencyBillStatementDetailPopup.createDestination(statement)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=NetOwed_Cell) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 117, column 51
    function currency_31 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 105, column 52
    function valueRoot_24 () : java.lang.Object {
      return statement
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 105, column 52
    function value_23 () : java.lang.String {
      return statement.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 110, column 52
    function value_26 () : java.lang.String {
      return statement.DisplayStatus
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetOwed_Cell) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 117, column 51
    function value_29 () : gw.pl.currency.MonetaryAmount {
      return statement.NetAmountDue
    }
    
    // 'value' attribute on DateCell (id=BillDate_Cell) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 122, column 48
    function value_33 () : java.util.Date {
      return statement.EventDate
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 127, column 46
    function value_36 () : java.util.Date {
      return statement.DueDate
    }
    
    property get statement () : entity.StatementInvoice {
      return getIteratedValue(2) as entity.StatementInvoice
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_AddStatementsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SpecificStatementsPanelSetExpressionsImpl extends AgencyDistributionWizard_AddStatementsPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=Search) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 77, column 43
    function action_17 () : void {
      gw.api.util.SearchUtil.search()
    }
    
    // 'action' attribute on Link (id=Reset) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 83, column 43
    function action_18 () : void {
      gw.api.util.SearchUtil.reset()
    }
    
    // 'value' attribute on RangeInput (id=Status_Input) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 60, column 84
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Status = (__VALUE_TO_SET as gw.agencybill.AgencyDistributionWizardHelper.StatusEnum)
    }
    
    // 'value' attribute on TextInput (id=StatementNumber_Input) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 53, column 55
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.StatementNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'searchCriteria' attribute on SearchPanel (id=SpecificStatementsPanelSet) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 41, column 86
    function searchCriteria_41 () : gw.search.StatementInvoiceSearchCriteria {
      return createSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel (id=SpecificStatementsPanelSet) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 41, column 86
    function search_40 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria)
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 105, column 52
    function sortValue_19 (statement :  entity.StatementInvoice) : java.lang.Object {
      return statement.InvoiceNumber
    }
    
    // 'value' attribute on DateCell (id=BillDate_Cell) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 122, column 48
    function sortValue_20 (statement :  entity.StatementInvoice) : java.lang.Object {
      return statement.EventDate
    }
    
    // 'valueRange' attribute on RangeInput (id=Status_Input) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 60, column 84
    function valueRange_13 () : java.lang.Object {
      return gw.agencybill.AgencyDistributionWizardHelper.StatusEnum.values()
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 48, column 44
    function valueRoot_4 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on RangeInput (id=Status_Input) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 60, column 84
    function value_10 () : gw.agencybill.AgencyDistributionWizardHelper.StatusEnum {
      return searchCriteria.Status
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 48, column 44
    function value_3 () : entity.Producer {
      return searchCriteria.Producer
    }
    
    // 'value' attribute on RowIterator (id=StatementsIterator) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 97, column 91
    function value_39 () : gw.api.database.IQueryBeanResult<entity.StatementInvoice> {
      return foundStatements
    }
    
    // 'value' attribute on TextInput (id=StatementNumber_Input) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 53, column 55
    function value_6 () : java.lang.String {
      return searchCriteria.StatementNumber
    }
    
    // 'valueRange' attribute on RangeInput (id=Status_Input) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 60, column 84
    function verifyValueRangeIsAllowedType_14 ($$arg :  gw.agencybill.AgencyDistributionWizardHelper.StatusEnum[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Status_Input) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 60, column 84
    function verifyValueRangeIsAllowedType_14 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Status_Input) at AgencyDistributionWizard_AddStatementsPopup.pcf: line 60, column 84
    function verifyValueRange_15 () : void {
      var __valueRangeArg = gw.agencybill.AgencyDistributionWizardHelper.StatusEnum.values()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_14(__valueRangeArg)
    }
    
    property get foundStatements () : gw.api.database.IQueryBeanResult<StatementInvoice> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<StatementInvoice>
    }
    
    property get searchCriteria () : gw.search.StatementInvoiceSearchCriteria {
      return getCriteriaValue(1) as gw.search.StatementInvoiceSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.StatementInvoiceSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}