<% uses pcf.* %>
<% uses entity.* %>
<% uses typekey.* %>
<% uses gw.api.locale.DisplayKey %>
<script type="text/javascript">
var tokenElement = document.getElementsByName('NewPaymentInstrumentPopup:Token-inputEl');
tokenElement.style.display = 'none';
var tokenLabelElement = document.getElementsByName('NewPaymentInstrumentPopup:Token-labelEl');
tokenLabelElement.style.display = 'none';
</script>