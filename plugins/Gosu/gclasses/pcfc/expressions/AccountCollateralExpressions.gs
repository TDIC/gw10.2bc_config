package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/AccountCollateral.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountCollateralExpressions {
  @javax.annotation.Generated("config/web/pcf/collateral/AccountCollateral.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountCollateralExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=disbursementButton) at AccountCollateral.pcf: line 238, column 121
    function action_101 () : void {
      CollateralDisbursementWizard.push(collateral)
    }
    
    // 'action' attribute on ToolbarButton (id=addButton) at AccountCollateral.pcf: line 286, column 83
    function action_124 () : void {
      AccountNewLOCPopup.push(collateral)
    }
    
    // 'action' attribute on ToolbarButton (id=addCharge) at AccountCollateral.pcf: line 359, column 85
    function action_154 () : void {
      NewCollateralChargePopup.push( collateral )
    }
    
    // 'action' attribute on ToolbarButton (id=addButton) at AccountCollateral.pcf: line 91, column 80
    function action_28 () : void {
      AccountNewCollateralRequirementPopup.push(collateral)
    }
    
    // 'action' attribute on ToolbarButton (id=drawdownButton) at AccountCollateral.pcf: line 233, column 121
    function action_98 () : void {
      CollateralManualDrawdownWizard.push(collateral)
    }
    
    // 'action' attribute on ToolbarButton (id=disbursementButton) at AccountCollateral.pcf: line 238, column 121
    function action_dest_102 () : pcf.api.Destination {
      return pcf.CollateralDisbursementWizard.createDestination(collateral)
    }
    
    // 'action' attribute on ToolbarButton (id=addButton) at AccountCollateral.pcf: line 286, column 83
    function action_dest_125 () : pcf.api.Destination {
      return pcf.AccountNewLOCPopup.createDestination(collateral)
    }
    
    // 'action' attribute on ToolbarButton (id=addCharge) at AccountCollateral.pcf: line 359, column 85
    function action_dest_155 () : pcf.api.Destination {
      return pcf.NewCollateralChargePopup.createDestination( collateral )
    }
    
    // 'action' attribute on ToolbarButton (id=addButton) at AccountCollateral.pcf: line 91, column 80
    function action_dest_29 () : pcf.api.Destination {
      return pcf.AccountNewCollateralRequirementPopup.createDestination(collateral)
    }
    
    // 'action' attribute on ToolbarButton (id=drawdownButton) at AccountCollateral.pcf: line 233, column 121
    function action_dest_99 () : pcf.api.Destination {
      return pcf.CollateralManualDrawdownWizard.createDestination(collateral)
    }
    
    // 'canEdit' attribute on Page (id=AccountCollateral) at AccountCollateral.pcf: line 9, column 77
    function canEdit_187 () : java.lang.Boolean {
      return perm.Account.collateralreqedit
    }
    
    // 'canVisit' attribute on Page (id=AccountCollateral) at AccountCollateral.pcf: line 9, column 77
    static function canVisit_188 (account :  Account) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctcollview and !account.isListBill()
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=editButton) at AccountCollateral.pcf: line 292, column 84
    function checkedRowAction_126 (loc :  entity.LetterOfCredit, CheckedValue :  entity.LetterOfCredit) : void {
      EditLOCPopup.push(CheckedValue)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=removeButton) at AccountCollateral.pcf: line 299, column 86
    function checkedRowAction_127 (loc :  entity.LetterOfCredit, CheckedValue :  entity.LetterOfCredit) : void {
      CheckedValue.Status = TC_REMOVED
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=editReqButton) at AccountCollateral.pcf: line 98, column 33
    function checkedRowAction_30 (requirement :  gw.api.web.collateral.CollateralViewItem, CheckedValue :  gw.api.web.collateral.CollateralViewItem) : void {
      EditCollateralRequirementPopup.push(CheckedValue.getCollateralRequirement())
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=closeButton) at AccountCollateral.pcf: line 107, column 33
    function checkedRowAction_31 (requirement :  gw.api.web.collateral.CollateralViewItem, CheckedValue :  gw.api.web.collateral.CollateralViewItem) : void {
      collateralUtil.closeRequirement(CurrentLocation, CheckedValue)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=segregateButton) at AccountCollateral.pcf: line 116, column 33
    function checkedRowAction_32 (requirement :  gw.api.web.collateral.CollateralViewItem, CheckedValue :  gw.api.web.collateral.CollateralViewItem) : void {
      CollateralRequirementSegregatePopup.push(CheckedValue.getCollateralRequirement())
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=unSegregateButton) at AccountCollateral.pcf: line 123, column 33
    function checkedRowAction_33 (requirement :  gw.api.web.collateral.CollateralViewItem, CheckedValue :  gw.api.web.collateral.CollateralViewItem) : void {
      CollateralRequirementUnSegregatePopup.push(CheckedValue.getCollateralRequirement())
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=totalRequirement_Input) at AccountCollateral.pcf: line 38, column 55
    function currency_4 () : typekey.Currency {
      return account.Currency
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountCollateral.pcf: line 316, column 89
    function filter_128 () : gw.api.filters.IFilter {
      return new gw.api.web.collateral.LetterOfCreditFilters.Current()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountCollateral.pcf: line 318, column 89
    function filter_129 () : gw.api.filters.IFilter {
      return new gw.api.web.collateral.LetterOfCreditFilters.Expired()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountCollateral.pcf: line 320, column 85
    function filter_130 () : gw.api.filters.IFilter {
      return new gw.api.web.collateral.LetterOfCreditFilters.All()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountCollateral.pcf: line 143, column 93
    function filter_34 () : gw.api.filters.IFilter {
      return new gw.api.web.collateral.CollateralRequirementFilters.Open()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountCollateral.pcf: line 145, column 95
    function filter_35 () : gw.api.filters.IFilter {
      return new gw.api.web.collateral.CollateralRequirementFilters.Closed()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountCollateral.pcf: line 147, column 92
    function filter_36 () : gw.api.filters.IFilter {
      return new gw.api.web.collateral.CollateralRequirementFilters.All()
    }
    
    // 'initialValue' attribute on Variable at AccountCollateral.pcf: line 18, column 26
    function initialValue_0 () : Collateral {
      return account.Collateral
    }
    
    // 'initialValue' attribute on Variable at AccountCollateral.pcf: line 22, column 49
    function initialValue_1 () : gw.api.web.account.CollateralUtil {
      return new gw.api.web.account.CollateralUtil()
    }
    
    // Page (id=AccountCollateral) at AccountCollateral.pcf: line 9, column 77
    static function parent_189 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    // 'value' attribute on DateCell (id=TransactionDate_Cell) at AccountCollateral.pcf: line 255, column 67
    function sortValue_103 (lineItem :  entity.LineItem) : java.lang.Object {
      return lineItem.Transaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=transactionNumber_Cell) at AccountCollateral.pcf: line 260, column 69
    function sortValue_104 (lineItem :  entity.LineItem) : java.lang.Object {
      return lineItem.Transaction.TransactionNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=lineItemAmount_Cell) at AccountCollateral.pcf: line 272, column 52
    function sortValue_105 (lineItem :  entity.LineItem) : java.lang.Object {
      return lineItem.SignedAmount
    }
    
    // 'value' attribute on TextCell (id=idCell_Cell) at AccountCollateral.pcf: line 326, column 40
    function sortValue_131 (loc :  entity.LetterOfCredit) : java.lang.Object {
      return loc.LOCID
    }
    
    // 'value' attribute on TextCell (id=bankNameCell_Cell) at AccountCollateral.pcf: line 331, column 43
    function sortValue_132 (loc :  entity.LetterOfCredit) : java.lang.Object {
      return loc.BankName
    }
    
    // 'value' attribute on DateCell (id=GoodThroughCell_Cell) at AccountCollateral.pcf: line 335, column 49
    function sortValue_133 (loc :  entity.LetterOfCredit) : java.lang.Object {
      return loc.ExpirationDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amountCell_Cell) at AccountCollateral.pcf: line 340, column 41
    function sortValue_134 (loc :  entity.LetterOfCredit) : java.lang.Object {
      return loc.Amount
    }
    
    // 'value' attribute on TypeKeyCell (id=statusCell_Cell) at AccountCollateral.pcf: line 345, column 63
    function sortValue_135 (loc :  entity.LetterOfCredit) : java.lang.Object {
      return loc.Status
    }
    
    // 'value' attribute on TextCell (id=chargeInitialTxn_Cell) at AccountCollateral.pcf: line 375, column 70
    function sortValue_156 (chargePatternOwnerPair :  com.google.gdata.util.common.base.Pair<entity.ChargePattern, entity.TAccountOwner>) : java.lang.Object {
      return chargePatternOwnerPair.First.ChargeName
    }
    
    // 'value' attribute on TextCell (id=owner_Cell) at AccountCollateral.pcf: line 380, column 55
    function sortValue_157 (chargePatternOwnerPair :  com.google.gdata.util.common.base.Pair<entity.ChargePattern, entity.TAccountOwner>) : java.lang.Object {
      return chargePatternOwnerPair.Second
    }
    
    // 'value' attribute on MonetaryAmountCell (id=unbilledCell_Cell) at AccountCollateral.pcf: line 387, column 96
    function sortValue_158 (chargePatternOwnerPair :  com.google.gdata.util.common.base.Pair<entity.ChargePattern, entity.TAccountOwner>) : java.lang.Object {
      return collateralUtil.getUnbilledOnChargePattern(chargePatternOwnerPair)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=billedCell_Cell) at AccountCollateral.pcf: line 394, column 94
    function sortValue_159 (chargePatternOwnerPair :  com.google.gdata.util.common.base.Pair<entity.ChargePattern, entity.TAccountOwner>) : java.lang.Object {
      return collateralUtil.getBilledOnChargePattern(chargePatternOwnerPair)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=dueCell_Cell) at AccountCollateral.pcf: line 401, column 91
    function sortValue_160 (chargePatternOwnerPair :  com.google.gdata.util.common.base.Pair<entity.ChargePattern, entity.TAccountOwner>) : java.lang.Object {
      return collateralUtil.getDueOnChargePattern(chargePatternOwnerPair)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=totalCell_Cell) at AccountCollateral.pcf: line 408, column 93
    function sortValue_161 (chargePatternOwnerPair :  com.google.gdata.util.common.base.Pair<entity.ChargePattern, entity.TAccountOwner>) : java.lang.Object {
      return collateralUtil.getTotalOnChargePattern(chargePatternOwnerPair)
    }
    
    // 'value' attribute on TextCell (id=nameCell_Cell) at AccountCollateral.pcf: line 154, column 52
    function sortValue_37 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement.getName()
    }
    
    // 'value' attribute on TypeKeyCell (id=typeCell_Cell) at AccountCollateral.pcf: line 159, column 68
    function sortValue_38 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement.getType()
    }
    
    // 'value' attribute on TextCell (id=policyNumber_Cell) at AccountCollateral.pcf: line 165, column 60
    function sortValue_39 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement.getPolicyNumber()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=requirementCell_Cell) at AccountCollateral.pcf: line 172, column 56
    function sortValue_40 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement.getRequired()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=cashAllocatedCell_Cell) at AccountCollateral.pcf: line 179, column 61
    function sortValue_41 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement.getCashAllocated()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=locAllocatedCell_Cell) at AccountCollateral.pcf: line 186, column 55
    function sortValue_42 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement.LOCAllocated
    }
    
    // 'value' attribute on MonetaryAmountCell (id=totalHeldCell_Cell) at AccountCollateral.pcf: line 193, column 68
    function sortValue_43 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement.getTotalRequirementHeld()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=surplusCell_Cell) at AccountCollateral.pcf: line 200, column 55
    function sortValue_44 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement.getSurplus()
    }
    
    // 'value' attribute on TypeKeyCell (id=reqStatus_Cell) at AccountCollateral.pcf: line 205, column 59
    function sortValue_45 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement.getStatus()
    }
    
    // 'value' attribute on BooleanRadioCell (id=segregatedCell_Cell) at AccountCollateral.pcf: line 210, column 82
    function sortValue_46 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement.CollateralRequirement.Segregated
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at AccountCollateral.pcf: line 214, column 56
    function sortValue_47 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at AccountCollateral.pcf: line 218, column 57
    function sortValue_48 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement.ExpirationDate
    }
    
    // '$$sumValue' attribute on RowIterator at AccountCollateral.pcf: line 272, column 52
    function sumValueRoot_107 (lineItem :  entity.LineItem) : java.lang.Object {
      return lineItem
    }
    
    // '$$sumValue' attribute on RowIterator (id=requirementList) at AccountCollateral.pcf: line 172, column 56
    function sumValueRoot_50 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement
    }
    
    // 'footerSumValue' attribute on RowIterator at AccountCollateral.pcf: line 272, column 52
    function sumValue_106 (lineItem :  entity.LineItem) : java.lang.Object {
      return lineItem.SignedAmount
    }
    
    // 'footerSumValue' attribute on RowIterator (id=chargesIterator) at AccountCollateral.pcf: line 387, column 96
    function sumValue_162 (chargePatternOwnerPair :  com.google.gdata.util.common.base.Pair<entity.ChargePattern, entity.TAccountOwner>) : java.lang.Object {
      return collateralUtil.getUnbilledOnChargePattern(chargePatternOwnerPair)
    }
    
    // 'footerSumValue' attribute on RowIterator (id=chargesIterator) at AccountCollateral.pcf: line 394, column 94
    function sumValue_163 (chargePatternOwnerPair :  com.google.gdata.util.common.base.Pair<entity.ChargePattern, entity.TAccountOwner>) : java.lang.Object {
      return collateralUtil.getBilledOnChargePattern(chargePatternOwnerPair)
    }
    
    // 'footerSumValue' attribute on RowIterator (id=chargesIterator) at AccountCollateral.pcf: line 401, column 91
    function sumValue_164 (chargePatternOwnerPair :  com.google.gdata.util.common.base.Pair<entity.ChargePattern, entity.TAccountOwner>) : java.lang.Object {
      return collateralUtil.getDueOnChargePattern(chargePatternOwnerPair)
    }
    
    // 'footerSumValue' attribute on RowIterator (id=chargesIterator) at AccountCollateral.pcf: line 408, column 93
    function sumValue_165 (chargePatternOwnerPair :  com.google.gdata.util.common.base.Pair<entity.ChargePattern, entity.TAccountOwner>) : java.lang.Object {
      return collateralUtil.getTotalOnChargePattern(chargePatternOwnerPair)
    }
    
    // 'footerSumValue' attribute on RowIterator (id=requirementList) at AccountCollateral.pcf: line 172, column 56
    function sumValue_49 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement.Required
    }
    
    // 'footerSumValue' attribute on RowIterator (id=requirementList) at AccountCollateral.pcf: line 179, column 61
    function sumValue_51 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement.CashAllocated
    }
    
    // 'footerSumValue' attribute on RowIterator (id=requirementList) at AccountCollateral.pcf: line 186, column 55
    function sumValue_53 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement.LOCAllocated
    }
    
    // 'footerSumValue' attribute on RowIterator (id=requirementList) at AccountCollateral.pcf: line 193, column 68
    function sumValue_55 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement.getTotalRequirementHeld()
    }
    
    // 'footerSumValue' attribute on RowIterator (id=requirementList) at AccountCollateral.pcf: line 200, column 55
    function sumValue_56 (requirement :  gw.api.web.collateral.CollateralViewItem) : java.lang.Object {
      return requirement.getSurplus()
    }
    
    // 'value' attribute on MonetaryAmountInput (id=totalRequirement_Input) at AccountCollateral.pcf: line 38, column 55
    function valueRoot_3 () : java.lang.Object {
      return collateral
    }
    
    // 'value' attribute on RowIterator at AccountCollateral.pcf: line 246, column 47
    function value_123 () : entity.LineItem[] {
      return collateralUtil.getCashLineItems( collateral )
    }
    
    // 'value' attribute on MonetaryAmountInput (id=cashBalance_Input) at AccountCollateral.pcf: line 60, column 66
    function value_13 () : gw.pl.currency.MonetaryAmount {
      return collateralUtil.getTotalCashHeld(collateral)
    }
    
    // 'value' attribute on RowIterator (id=letterOfCreditList) at AccountCollateral.pcf: line 310, column 53
    function value_153 () : entity.LetterOfCredit[] {
      return collateral.LetterOfCredits
    }
    
    // 'value' attribute on MonetaryAmountInput (id=requirementsCashHeld_Input) at AccountCollateral.pcf: line 66, column 62
    function value_16 () : gw.pl.currency.MonetaryAmount {
      return collateral.TotalCashValueAtRequirements
    }
    
    // 'value' attribute on RowIterator (id=chargesIterator) at AccountCollateral.pcf: line 368, column 140
    function value_186 () : java.util.List<com.google.gdata.util.common.base.Pair<entity.ChargePattern, entity.TAccountOwner>> {
      return collateral.ChargePatternsForCollateralAndRequirements
    }
    
    // 'value' attribute on MonetaryAmountInput (id=totalRequirement_Input) at AccountCollateral.pcf: line 38, column 55
    function value_2 () : gw.pl.currency.MonetaryAmount {
      return collateral.TotalRequirementValue
    }
    
    // 'value' attribute on MonetaryAmountInput (id=locBalance_Input) at AccountCollateral.pcf: line 72, column 47
    function value_20 () : gw.pl.currency.MonetaryAmount {
      return collateral.TotalLOCValue
    }
    
    // 'value' attribute on MonetaryAmountInput (id=totalBalance_Input) at AccountCollateral.pcf: line 79, column 54
    function value_24 () : gw.pl.currency.MonetaryAmount {
      return collateral.TotalCollateralValue
    }
    
    // 'value' attribute on TypeKeyInput (id=compliant_Input) at AccountCollateral.pcf: line 43, column 51
    function value_6 () : typekey.ComplianceStatus {
      return collateral.Compliance
    }
    
    // 'value' attribute on MonetaryAmountInput (id=trueExcess_Input) at AccountCollateral.pcf: line 49, column 44
    function value_9 () : gw.pl.currency.MonetaryAmount {
      return collateral.TrueExcess
    }
    
    // 'value' attribute on RowIterator (id=requirementList) at AccountCollateral.pcf: line 134, column 72
    function value_96 () : gw.api.web.collateral.CollateralViewItem[] {
      return collateralUtil.getCollateralRequirementViewItems( collateral )
    }
    
    // 'visible' attribute on ToolbarButton (id=disbursementButton) at AccountCollateral.pcf: line 238, column 121
    function visible_100 () : java.lang.Boolean {
      return perm.Account.collateraldisburse and collateralUtil.getTotalCashHeld( collateral ).IsPositive
    }
    
    // 'visible' attribute on ToolbarButton (id=drawdownButton) at AccountCollateral.pcf: line 233, column 121
    function visible_97 () : java.lang.Boolean {
      return perm.Account.collateraldrawdown and collateralUtil.getTotalCashHeld( collateral ).IsPositive
    }
    
    override property get CurrentLocation () : pcf.AccountCollateral {
      return super.CurrentLocation as pcf.AccountCollateral
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get collateral () : Collateral {
      return getVariableValue("collateral", 0) as Collateral
    }
    
    property set collateral ($arg :  Collateral) {
      setVariableValue("collateral", 0, $arg)
    }
    
    property get collateralUtil () : gw.api.web.account.CollateralUtil {
      return getVariableValue("collateralUtil", 0) as gw.api.web.account.CollateralUtil
    }
    
    property set collateralUtil ($arg :  gw.api.web.account.CollateralUtil) {
      setVariableValue("collateralUtil", 0, $arg)
    }
    
    function isStatusCurrent(loc : LetterOfCredit) : boolean {
           return loc.Status == LetterOfCreditStatus.TC_CURRENT;
          }
    
          function isStatusOpen(requirement : CollateralRequirement) : boolean {
            if(requirement == null){
              return false 
            }
           return requirement.Compliance != ComplianceStatus.TC_CLOSED;
          }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/collateral/AccountCollateral.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends AccountCollateralExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=transactionNumber_Cell) at AccountCollateral.pcf: line 260, column 69
    function action_111 () : void {
      AccountTransactionDetail.push(account, lineItem.Transaction)
    }
    
    // 'action' attribute on TextCell (id=transactionNumber_Cell) at AccountCollateral.pcf: line 260, column 69
    function action_dest_112 () : pcf.api.Destination {
      return pcf.AccountTransactionDetail.createDestination(account, lineItem.Transaction)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=lineItemAmount_Cell) at AccountCollateral.pcf: line 272, column 52
    function currency_121 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on DateCell (id=TransactionDate_Cell) at AccountCollateral.pcf: line 255, column 67
    function valueRoot_109 () : java.lang.Object {
      return lineItem.Transaction
    }
    
    // 'value' attribute on MonetaryAmountCell (id=lineItemAmount_Cell) at AccountCollateral.pcf: line 272, column 52
    function valueRoot_120 () : java.lang.Object {
      return lineItem
    }
    
    // 'value' attribute on DateCell (id=TransactionDate_Cell) at AccountCollateral.pcf: line 255, column 67
    function value_108 () : java.util.Date {
      return lineItem.Transaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=transactionNumber_Cell) at AccountCollateral.pcf: line 260, column 69
    function value_113 () : java.lang.String {
      return lineItem.Transaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AccountCollateral.pcf: line 265, column 68
    function value_116 () : java.lang.String {
      return lineItem.Transaction.ShortDisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=lineItemAmount_Cell) at AccountCollateral.pcf: line 272, column 52
    function value_119 () : gw.pl.currency.MonetaryAmount {
      return lineItem.SignedAmount
    }
    
    property get lineItem () : entity.LineItem {
      return getIteratedValue(1) as entity.LineItem
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/collateral/AccountCollateral.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends AccountCollateralExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'checkBoxVisible' attribute on RowIterator (id=letterOfCreditList) at AccountCollateral.pcf: line 310, column 53
    function checkBoxVisible_152 () : java.lang.Boolean {
      return isStatusCurrent(loc)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=amountCell_Cell) at AccountCollateral.pcf: line 340, column 41
    function currency_147 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on TextCell (id=idCell_Cell) at AccountCollateral.pcf: line 326, column 40
    function valueRoot_137 () : java.lang.Object {
      return loc
    }
    
    // 'value' attribute on TextCell (id=idCell_Cell) at AccountCollateral.pcf: line 326, column 40
    function value_136 () : java.lang.String {
      return loc.LOCID
    }
    
    // 'value' attribute on TextCell (id=bankNameCell_Cell) at AccountCollateral.pcf: line 331, column 43
    function value_139 () : java.lang.String {
      return loc.BankName
    }
    
    // 'value' attribute on DateCell (id=GoodThroughCell_Cell) at AccountCollateral.pcf: line 335, column 49
    function value_142 () : java.util.Date {
      return loc.ExpirationDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amountCell_Cell) at AccountCollateral.pcf: line 340, column 41
    function value_145 () : gw.pl.currency.MonetaryAmount {
      return loc.Amount
    }
    
    // 'value' attribute on TypeKeyCell (id=statusCell_Cell) at AccountCollateral.pcf: line 345, column 63
    function value_149 () : typekey.LetterOfCreditStatus {
      return loc.Status
    }
    
    property get loc () : entity.LetterOfCredit {
      return getIteratedValue(1) as entity.LetterOfCredit
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/collateral/AccountCollateral.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends AccountCollateralExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=chargeInitialTxn_Cell) at AccountCollateral.pcf: line 375, column 70
    function action_166 () : void {
      CollateralChargePopup.push(chargePatternOwnerPair, collateralUtil)
    }
    
    // 'action' attribute on TextCell (id=chargeInitialTxn_Cell) at AccountCollateral.pcf: line 375, column 70
    function action_dest_167 () : pcf.api.Destination {
      return pcf.CollateralChargePopup.createDestination(chargePatternOwnerPair, collateralUtil)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=unbilledCell_Cell) at AccountCollateral.pcf: line 387, column 96
    function currency_175 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on TextCell (id=chargeInitialTxn_Cell) at AccountCollateral.pcf: line 375, column 70
    function valueRoot_169 () : java.lang.Object {
      return chargePatternOwnerPair.First
    }
    
    // 'value' attribute on TextCell (id=owner_Cell) at AccountCollateral.pcf: line 380, column 55
    function valueRoot_172 () : java.lang.Object {
      return chargePatternOwnerPair
    }
    
    // 'value' attribute on TextCell (id=chargeInitialTxn_Cell) at AccountCollateral.pcf: line 375, column 70
    function value_168 () : java.lang.String {
      return chargePatternOwnerPair.First.ChargeName
    }
    
    // 'value' attribute on TextCell (id=owner_Cell) at AccountCollateral.pcf: line 380, column 55
    function value_171 () : entity.TAccountOwner {
      return chargePatternOwnerPair.Second
    }
    
    // 'value' attribute on MonetaryAmountCell (id=unbilledCell_Cell) at AccountCollateral.pcf: line 387, column 96
    function value_174 () : gw.pl.currency.MonetaryAmount {
      return collateralUtil.getUnbilledOnChargePattern(chargePatternOwnerPair)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=billedCell_Cell) at AccountCollateral.pcf: line 394, column 94
    function value_177 () : gw.pl.currency.MonetaryAmount {
      return collateralUtil.getBilledOnChargePattern(chargePatternOwnerPair)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=dueCell_Cell) at AccountCollateral.pcf: line 401, column 91
    function value_180 () : gw.pl.currency.MonetaryAmount {
      return collateralUtil.getDueOnChargePattern(chargePatternOwnerPair)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=totalCell_Cell) at AccountCollateral.pcf: line 408, column 93
    function value_183 () : gw.pl.currency.MonetaryAmount {
      return collateralUtil.getTotalOnChargePattern(chargePatternOwnerPair)
    }
    
    property get chargePatternOwnerPair () : com.google.gdata.util.common.base.Pair<entity.ChargePattern, entity.TAccountOwner> {
      return getIteratedValue(1) as com.google.gdata.util.common.base.Pair<entity.ChargePattern, entity.TAccountOwner>
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/collateral/AccountCollateral.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountCollateralExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=policyNumber_Cell) at AccountCollateral.pcf: line 165, column 60
    function actionAvailable_64 () : java.lang.Boolean {
      return requirement.getPolicyPeriod()!= null
    }
    
    // 'action' attribute on TextCell (id=policyNumber_Cell) at AccountCollateral.pcf: line 165, column 60
    function action_62 () : void {
      PolicyOverview.push(requirement.getPolicyPeriod())
    }
    
    // 'action' attribute on TextCell (id=policyNumber_Cell) at AccountCollateral.pcf: line 165, column 60
    function action_dest_63 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(requirement.getPolicyPeriod())
    }
    
    // 'checkBoxVisible' attribute on RowIterator (id=requirementList) at AccountCollateral.pcf: line 134, column 72
    function checkBoxVisible_95 () : java.lang.Boolean {
      return isStatusOpen(requirement.getCollateralRequirement())
    }
    
    // 'condition' attribute on ToolbarFlag at AccountCollateral.pcf: line 139, column 51
    function condition_57 () : java.lang.Boolean {
      return requirement.getCollateralRequirement().Segregated
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=requirementCell_Cell) at AccountCollateral.pcf: line 172, column 56
    function currency_68 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=locAllocatedCell_Cell) at AccountCollateral.pcf: line 186, column 55
    function valueRoot_74 () : java.lang.Object {
      return requirement
    }
    
    // 'value' attribute on BooleanRadioCell (id=segregatedCell_Cell) at AccountCollateral.pcf: line 210, column 82
    function valueRoot_87 () : java.lang.Object {
      return requirement.CollateralRequirement
    }
    
    // 'value' attribute on TextCell (id=nameCell_Cell) at AccountCollateral.pcf: line 154, column 52
    function value_58 () : java.lang.String {
      return requirement.getName()
    }
    
    // 'value' attribute on TypeKeyCell (id=typeCell_Cell) at AccountCollateral.pcf: line 159, column 68
    function value_60 () : typekey.CollateralRequirementType {
      return requirement.getType()
    }
    
    // 'value' attribute on TextCell (id=policyNumber_Cell) at AccountCollateral.pcf: line 165, column 60
    function value_65 () : java.lang.String {
      return requirement.getPolicyNumber()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=requirementCell_Cell) at AccountCollateral.pcf: line 172, column 56
    function value_67 () : gw.pl.currency.MonetaryAmount {
      return requirement.getRequired()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=cashAllocatedCell_Cell) at AccountCollateral.pcf: line 179, column 61
    function value_70 () : gw.pl.currency.MonetaryAmount {
      return requirement.getCashAllocated()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=locAllocatedCell_Cell) at AccountCollateral.pcf: line 186, column 55
    function value_73 () : gw.pl.currency.MonetaryAmount {
      return requirement.LOCAllocated
    }
    
    // 'value' attribute on MonetaryAmountCell (id=totalHeldCell_Cell) at AccountCollateral.pcf: line 193, column 68
    function value_77 () : gw.pl.currency.MonetaryAmount {
      return requirement.getTotalRequirementHeld()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=surplusCell_Cell) at AccountCollateral.pcf: line 200, column 55
    function value_80 () : gw.pl.currency.MonetaryAmount {
      return requirement.getSurplus()
    }
    
    // 'value' attribute on TypeKeyCell (id=reqStatus_Cell) at AccountCollateral.pcf: line 205, column 59
    function value_83 () : typekey.ComplianceStatus {
      return requirement.getStatus()
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at AccountCollateral.pcf: line 214, column 56
    function value_89 () : java.util.Date {
      return requirement.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at AccountCollateral.pcf: line 218, column 57
    function value_92 () : java.util.Date {
      return requirement.ExpirationDate
    }
    
    // 'valueVisible' attribute on BooleanRadioCell (id=segregatedCell_Cell) at AccountCollateral.pcf: line 210, column 82
    function visible_85 () : java.lang.Boolean {
      return requirement.CollateralRequirement.Segregated
    }
    
    property get requirement () : gw.api.web.collateral.CollateralViewItem {
      return getIteratedValue(1) as gw.api.web.collateral.CollateralViewItem
    }
    
    
  }
  
  
}