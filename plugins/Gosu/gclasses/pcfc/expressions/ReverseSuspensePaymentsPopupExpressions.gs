package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/suspensepayment/ReverseSuspensePaymentsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ReverseSuspensePaymentsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/suspensepayment/ReverseSuspensePaymentsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ReverseSuspensePaymentsPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=transactionNumber_Cell) at ReverseSuspensePaymentsPopup.pcf: line 51, column 78
    function action_8 () : void {
      TransactionDetailPopup.push(suspensePayment.CreationTransaction)
    }
    
    // 'action' attribute on TextCell (id=transactionNumber_Cell) at ReverseSuspensePaymentsPopup.pcf: line 51, column 78
    function action_dest_9 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(suspensePayment.CreationTransaction)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=amount_Cell) at ReverseSuspensePaymentsPopup.pcf: line 69, column 47
    function currency_24 () : typekey.Currency {
      return suspensePayment.Currency
    }
    
    // 'value' attribute on TextCell (id=transactionNumber_Cell) at ReverseSuspensePaymentsPopup.pcf: line 51, column 78
    function valueRoot_11 () : java.lang.Object {
      return suspensePayment.CreationTransaction
    }
    
    // 'value' attribute on DateCell (id=date_Cell) at ReverseSuspensePaymentsPopup.pcf: line 45, column 52
    function valueRoot_6 () : java.lang.Object {
      return suspensePayment
    }
    
    // 'value' attribute on TextCell (id=transactionNumber_Cell) at ReverseSuspensePaymentsPopup.pcf: line 51, column 78
    function value_10 () : java.lang.String {
      return suspensePayment.CreationTransaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=InvoiceNumber_Cell) at ReverseSuspensePaymentsPopup.pcf: line 56, column 54
    function value_13 () : java.lang.String {
      return suspensePayment.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=policyNumber_Cell) at ReverseSuspensePaymentsPopup.pcf: line 60, column 53
    function value_16 () : java.lang.String {
      return suspensePayment.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=accountNumber_Cell) at ReverseSuspensePaymentsPopup.pcf: line 64, column 54
    function value_19 () : java.lang.String {
      return suspensePayment.AccountNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amount_Cell) at ReverseSuspensePaymentsPopup.pcf: line 69, column 47
    function value_22 () : gw.pl.currency.MonetaryAmount {
      return suspensePayment.Amount
    }
    
    // 'value' attribute on DateCell (id=date_Cell) at ReverseSuspensePaymentsPopup.pcf: line 45, column 52
    function value_5 () : java.util.Date {
      return suspensePayment.PaymentDate
    }
    
    property get suspensePayment () : entity.SuspensePayment {
      return getIteratedValue(1) as entity.SuspensePayment
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/suspensepayment/ReverseSuspensePaymentsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ReverseSuspensePaymentsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (suspensePayments :  java.util.List<SuspensePayment>) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=ReverseSuspensePaymentsPopup) at ReverseSuspensePaymentsPopup.pcf: line 11, column 80
    function beforeCommit_27 (pickedValue :  java.lang.Object) : void {
      suspensePaymentUtil.reverse(suspensePayments)
    }
    
    // 'canVisit' attribute on Popup (id=ReverseSuspensePaymentsPopup) at ReverseSuspensePaymentsPopup.pcf: line 11, column 80
    static function canVisit_28 (suspensePayments :  java.util.List<SuspensePayment>) : java.lang.Boolean {
      return perm.SuspensePayment.edit
    }
    
    // 'initialValue' attribute on Variable at ReverseSuspensePaymentsPopup.pcf: line 20, column 54
    function initialValue_0 () : gw.api.web.payment.SuspensePaymentUtil {
      return new gw.api.web.payment.SuspensePaymentUtil()
    }
    
    // 'value' attribute on DateCell (id=date_Cell) at ReverseSuspensePaymentsPopup.pcf: line 45, column 52
    function sortValue_1 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.PaymentDate
    }
    
    // 'value' attribute on TextCell (id=policyNumber_Cell) at ReverseSuspensePaymentsPopup.pcf: line 60, column 53
    function sortValue_2 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=accountNumber_Cell) at ReverseSuspensePaymentsPopup.pcf: line 64, column 54
    function sortValue_3 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.AccountNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amount_Cell) at ReverseSuspensePaymentsPopup.pcf: line 69, column 47
    function sortValue_4 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.Amount
    }
    
    // 'value' attribute on RowIterator (id=SuspensePaymentsIterator) at ReverseSuspensePaymentsPopup.pcf: line 40, column 48
    function value_26 () : entity.SuspensePayment[] {
      return suspensePayments.toArray(new SuspensePayment[suspensePayments.size()])
    }
    
    override property get CurrentLocation () : pcf.ReverseSuspensePaymentsPopup {
      return super.CurrentLocation as pcf.ReverseSuspensePaymentsPopup
    }
    
    property get suspensePaymentUtil () : gw.api.web.payment.SuspensePaymentUtil {
      return getVariableValue("suspensePaymentUtil", 0) as gw.api.web.payment.SuspensePaymentUtil
    }
    
    property set suspensePaymentUtil ($arg :  gw.api.web.payment.SuspensePaymentUtil) {
      setVariableValue("suspensePaymentUtil", 0, $arg)
    }
    
    property get suspensePayments () : java.util.List<SuspensePayment> {
      return getVariableValue("suspensePayments", 0) as java.util.List<SuspensePayment>
    }
    
    property set suspensePayments ($arg :  java.util.List<SuspensePayment>) {
      setVariableValue("suspensePayments", 0, $arg)
    }
    
    
  }
  
  
}