package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/CreateTroubleTicketConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreateTroubleTicketConfirmationScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/CreateTroubleTicketConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreateTroubleTicketConfirmationScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=AssignTicketOwner_PickerButton) at AssigneeWidget.xml: line 7, column 25
    function action_1 () : void {
      AssigneePickerPopup.push(new gw.api.assignment.AssigneePicker(entity.Activity.Type.isAssignableFrom(typeof (troubleTicket))))
    }
    
    // 'action' attribute on UserInput (id=AssignToCreator_Input) at UserWidget.xml: line 9, column 49
    function action_10 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // 'action' attribute on UserInput (id=AssignToCreator_Input) at UserWidget.xml: line 12, column 49
    function action_12 () : void {
      pcf.UserSelectPopup.push()
    }
    
    // 'action' attribute on UserInput (id=AssignToCreator_Input) at UserWidget.xml: line 9, column 49
    function action_dest_11 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'action' attribute on UserInput (id=AssignToCreator_Input) at UserWidget.xml: line 12, column 49
    function action_dest_13 () : pcf.api.Destination {
      return pcf.UserSelectPopup.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=AssignTicketOwner_PickerButton) at AssigneeWidget.xml: line 7, column 25
    function action_dest_2 () : pcf.api.Destination {
      return pcf.AssigneePickerPopup.createDestination(new gw.api.assignment.AssigneePicker(entity.Activity.Type.isAssignableFrom(typeof (troubleTicket))))
    }
    
    // 'value' attribute on AssigneeInput (id=AssignTicketOwner_Input) at AssigneeWidget.xml: line 7, column 25
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      assigneeHolder[0] = (__VALUE_TO_SET as gw.api.assignment.Assignee)
    }
    
    // 'valueRange' attribute on UserInput (id=AssignToCreator_Input) at UserWidget.xml: line 6, column 85
    function valueRange_16 () : java.lang.Object {
      return entity.User.util.getUsersInCurrentUsersGroup()
    }
    
    // 'valueRange' attribute on AssigneeInput (id=AssignTicketOwner_Input) at AssigneeWidget.xml: line 7, column 25
    function valueRange_5 () : java.lang.Object {
      return troubleTicket.SuggestedAssigneesIncludingQueues_Ext
    }
    
    // 'value' attribute on UserInput (id=AssignToCreator_Input) at UserWidget.xml: line 6, column 85
    function valueRoot_15 () : java.lang.Object {
      return gw.plugin.util.CurrentUserUtil.CurrentUser
    }
    
    // 'value' attribute on UserInput (id=AssignToCreator_Input) at UserWidget.xml: line 6, column 85
    function value_14 () : entity.User {
      return gw.plugin.util.CurrentUserUtil.CurrentUser.User
    }
    
    // 'value' attribute on AssigneeInput (id=AssignTicketOwner_Input) at AssigneeWidget.xml: line 7, column 25
    function value_3 () : gw.api.assignment.Assignee {
      return assigneeHolder[0]
    }
    
    // 'valueRange' attribute on UserInput (id=AssignToCreator_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRangeIsAllowedType_17 ($$arg :  entity.User[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on UserInput (id=AssignToCreator_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRangeIsAllowedType_17 ($$arg :  gw.api.database.IQueryBeanResult<entity.User>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on UserInput (id=AssignToCreator_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRangeIsAllowedType_17 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on AssigneeInput (id=AssignTicketOwner_Input) at AssigneeWidget.xml: line 7, column 25
    function verifyValueRangeIsAllowedType_6 ($$arg :  gw.api.assignment.Assignee[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on AssigneeInput (id=AssignTicketOwner_Input) at AssigneeWidget.xml: line 7, column 25
    function verifyValueRangeIsAllowedType_6 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on UserInput (id=AssignToCreator_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRange_18 () : void {
      var __valueRangeArg = entity.User.util.getUsersInCurrentUsersGroup()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_17(__valueRangeArg)
    }
    
    // 'valueRange' attribute on AssigneeInput (id=AssignTicketOwner_Input) at AssigneeWidget.xml: line 7, column 25
    function verifyValueRange_7 () : void {
      var __valueRangeArg = troubleTicket.SuggestedAssigneesIncludingQueues_Ext
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_6(__valueRangeArg)
    }
    
    // 'visible' attribute on AlertBar (id=CreateTroubleTicketConfirmationScreen_HoldsButNoTargetAlertBar) at CreateTroubleTicketConfirmationScreen.pcf: line 25, column 55
    function visible_0 () : java.lang.Boolean {
      return troubleTicket.hasHoldWithNoTargets()
    }
    
    property get assigneeHolder () : gw.api.assignment.Assignee[] {
      return getRequireValue("assigneeHolder", 0) as gw.api.assignment.Assignee[]
    }
    
    property set assigneeHolder ($arg :  gw.api.assignment.Assignee[]) {
      setRequireValue("assigneeHolder", 0, $arg)
    }
    
    property get createTroubleTicketHelper () : CreateTroubleTicketHelper {
      return getRequireValue("createTroubleTicketHelper", 0) as CreateTroubleTicketHelper
    }
    
    property set createTroubleTicketHelper ($arg :  CreateTroubleTicketHelper) {
      setRequireValue("createTroubleTicketHelper", 0, $arg)
    }
    
    property get troubleTicket () : TroubleTicket {
      return getRequireValue("troubleTicket", 0) as TroubleTicket
    }
    
    property set troubleTicket ($arg :  TroubleTicket) {
      setRequireValue("troubleTicket", 0, $arg)
    }
    
    
  }
  
  
}