package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadRoleLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadRoleLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadRoleLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadRoleLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadRoleLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadRoleLV.pcf: line 50, column 42
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadRoleLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Role.PublicID")
    }
    
    // 'label' attribute on TextCell (id=roleName_Cell) at AdminDataUploadRoleLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Role.Name")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at AdminDataUploadRoleLV.pcf: line 40, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Role.Description")
    }
    
    // 'label' attribute on TextCell (id=permissions_Cell) at AdminDataUploadRoleLV.pcf: line 45, column 42
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Role.NumberOfPermissions")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadRoleLV.pcf: line 23, column 46
    function sortValue_1 (role :  tdic.util.dataloader.data.admindata.RoleData) : java.lang.Object {
      return processor.getLoadStatus(role)
    }
    
    // 'value' attribute on TextCell (id=permissions_Cell) at AdminDataUploadRoleLV.pcf: line 45, column 42
    function sortValue_10 (role :  tdic.util.dataloader.data.admindata.RoleData) : java.lang.Object {
      return role.Permissions.Count
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadRoleLV.pcf: line 50, column 42
    function sortValue_12 (role :  tdic.util.dataloader.data.admindata.RoleData) : java.lang.Object {
      return role.Exclude
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadRoleLV.pcf: line 29, column 41
    function sortValue_4 (role :  tdic.util.dataloader.data.admindata.RoleData) : java.lang.Object {
      return role.PublicID
    }
    
    // 'value' attribute on TextCell (id=roleName_Cell) at AdminDataUploadRoleLV.pcf: line 35, column 41
    function sortValue_6 (role :  tdic.util.dataloader.data.admindata.RoleData) : java.lang.Object {
      return role.Name
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at AdminDataUploadRoleLV.pcf: line 40, column 41
    function sortValue_8 (role :  tdic.util.dataloader.data.admindata.RoleData) : java.lang.Object {
      return role.Description
    }
    
    // 'value' attribute on RowIterator (id=roleID) at AdminDataUploadRoleLV.pcf: line 15, column 91
    function value_44 () : java.util.ArrayList<tdic.util.dataloader.data.admindata.RoleData> {
      return processor.RoleArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadRoleLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadRoleLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminDataUploadRoleLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at AdminDataUploadRoleLV.pcf: line 17, column 88
    function highlighted_43 () : java.lang.Boolean {
      return (role.Error or role.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadRoleLV.pcf: line 23, column 46
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadRoleLV.pcf: line 29, column 41
    function label_18 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Role.PublicID")
    }
    
    // 'label' attribute on TextCell (id=roleName_Cell) at AdminDataUploadRoleLV.pcf: line 35, column 41
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Role.Name")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at AdminDataUploadRoleLV.pcf: line 40, column 41
    function label_28 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Role.Description")
    }
    
    // 'label' attribute on TextCell (id=permissions_Cell) at AdminDataUploadRoleLV.pcf: line 45, column 42
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Role.NumberOfPermissions")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadRoleLV.pcf: line 50, column 42
    function label_38 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadRoleLV.pcf: line 29, column 41
    function valueRoot_20 () : java.lang.Object {
      return role
    }
    
    // 'value' attribute on TextCell (id=permissions_Cell) at AdminDataUploadRoleLV.pcf: line 45, column 42
    function valueRoot_35 () : java.lang.Object {
      return role.Permissions
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadRoleLV.pcf: line 23, column 46
    function value_14 () : java.lang.String {
      return processor.getLoadStatus(role)
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadRoleLV.pcf: line 29, column 41
    function value_19 () : java.lang.String {
      return role.PublicID
    }
    
    // 'value' attribute on TextCell (id=roleName_Cell) at AdminDataUploadRoleLV.pcf: line 35, column 41
    function value_24 () : java.lang.String {
      return role.Name
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at AdminDataUploadRoleLV.pcf: line 40, column 41
    function value_29 () : java.lang.String {
      return role.Description
    }
    
    // 'value' attribute on TextCell (id=permissions_Cell) at AdminDataUploadRoleLV.pcf: line 45, column 42
    function value_34 () : java.lang.Integer {
      return role.Permissions.Count
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadRoleLV.pcf: line 50, column 42
    function value_39 () : java.lang.Boolean {
      return role.Exclude
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadRoleLV.pcf: line 23, column 46
    function visible_15 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get role () : tdic.util.dataloader.data.admindata.RoleData {
      return getIteratedValue(1) as tdic.util.dataloader.data.admindata.RoleData
    }
    
    
  }
  
  
}