package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/EditLOCPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class EditLOCPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/collateral/EditLOCPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class EditLOCPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (loc :  LetterOfCredit) : int {
      return 0
    }
    
    // 'def' attribute on PanelRef at EditLOCPopup.pcf: line 21, column 27
    function def_onEnter_1 (def :  pcf.LOCDV) : void {
      def.onEnter(loc)
    }
    
    // 'def' attribute on PanelRef at EditLOCPopup.pcf: line 21, column 27
    function def_refreshVariables_2 (def :  pcf.LOCDV) : void {
      def.refreshVariables(loc)
    }
    
    // EditButtons at EditLOCPopup.pcf: line 18, column 23
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    override property get CurrentLocation () : pcf.EditLOCPopup {
      return super.CurrentLocation as pcf.EditLOCPopup
    }
    
    property get loc () : LetterOfCredit {
      return getVariableValue("loc", 0) as LetterOfCredit
    }
    
    property set loc ($arg :  LetterOfCredit) {
      setVariableValue("loc", 0, $arg)
    }
    
    
  }
  
  
}