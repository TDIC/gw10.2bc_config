package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/VoidDisbursementPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class VoidDisbursementPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/disbursement/VoidDisbursementPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class VoidDisbursementPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (disbursement :  Disbursement) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=VoidDisbursementPopup) at VoidDisbursementPopup.pcf: line 11, column 68
    function beforeCommit_7 (pickedValue :  Disbursement) : void {
      disbursement.voidDisbursement()
    }
    
    // 'def' attribute on PanelRef at VoidDisbursementPopup.pcf: line 36, column 51
    function def_onEnter_5 (def :  pcf.DisbursementDetailDV) : void {
      def.onEnter(disbursement)
    }
    
    // 'def' attribute on PanelRef at VoidDisbursementPopup.pcf: line 36, column 51
    function def_refreshVariables_6 (def :  pcf.DisbursementDetailDV) : void {
      def.refreshVariables(disbursement)
    }
    
    // 'value' attribute on TypeKeyInput (id=VoidReason_Input) at VoidDisbursementPopup.pcf: line 32, column 45
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      disbursement.VoidReason = (__VALUE_TO_SET as typekey.VoidReason)
    }
    
    // EditButtons at VoidDisbursementPopup.pcf: line 20, column 23
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'value' attribute on TypeKeyInput (id=VoidReason_Input) at VoidDisbursementPopup.pcf: line 32, column 45
    function valueRoot_3 () : java.lang.Object {
      return disbursement
    }
    
    // 'value' attribute on TypeKeyInput (id=VoidReason_Input) at VoidDisbursementPopup.pcf: line 32, column 45
    function value_1 () : typekey.VoidReason {
      return disbursement.VoidReason
    }
    
    override property get CurrentLocation () : pcf.VoidDisbursementPopup {
      return super.CurrentLocation as pcf.VoidDisbursementPopup
    }
    
    property get disbursement () : Disbursement {
      return getVariableValue("disbursement", 0) as Disbursement
    }
    
    property set disbursement ($arg :  Disbursement) {
      setVariableValue("disbursement", 0, $arg)
    }
    
    
  }
  
  
}