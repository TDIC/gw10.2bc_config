package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/AccountNewCreditReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountNewCreditReversalWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/AccountNewCreditReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountNewCreditReversalWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'allowNext' attribute on WizardStep (id=Step1) at AccountNewCreditReversalWizard.pcf: line 26, column 88
    function allowNext_1 () : java.lang.Boolean {
      return creditReversal.Credit != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=AccountNewCreditReversalWizard) at AccountNewCreditReversalWizard.pcf: line 10, column 41
    function beforeCommit_7 (pickedValue :  java.lang.Object) : void {
      creditReversal.reverse()
    }
    
    // 'canVisit' attribute on Wizard (id=AccountNewCreditReversalWizard) at AccountNewCreditReversalWizard.pcf: line 10, column 41
    static function canVisit_8 (account :  Account) : java.lang.Boolean {
      return perm.Transaction.revtxn
    }
    
    // 'initialValue' attribute on Variable at AccountNewCreditReversalWizard.pcf: line 19, column 30
    function initialValue_0 () : CreditReversal {
      return new CreditReversal()
    }
    
    // 'onExit' attribute on WizardStep (id=Step1) at AccountNewCreditReversalWizard.pcf: line 26, column 88
    function onExit_2 () : void {
      creditReversal.initiateApprovalActivityIfUserLacksAuthority()
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountNewCreditReversalWizard.pcf: line 26, column 88
    function screen_onEnter_3 (def :  pcf.CreditReversalCreditSearchScreen) : void {
      def.onEnter(account, creditReversal)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountNewCreditReversalWizard.pcf: line 31, column 88
    function screen_onEnter_5 (def :  pcf.CreditReversalConfirmationScreen) : void {
      def.onEnter(account, creditReversal)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountNewCreditReversalWizard.pcf: line 26, column 88
    function screen_refreshVariables_4 (def :  pcf.CreditReversalCreditSearchScreen) : void {
      def.refreshVariables(account, creditReversal)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountNewCreditReversalWizard.pcf: line 31, column 88
    function screen_refreshVariables_6 (def :  pcf.CreditReversalConfirmationScreen) : void {
      def.refreshVariables(account, creditReversal)
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewCreditReversalWizard) at AccountNewCreditReversalWizard.pcf: line 10, column 41
    function tabBar_onEnter_9 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewCreditReversalWizard) at AccountNewCreditReversalWizard.pcf: line 10, column 41
    function tabBar_refreshVariables_10 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AccountNewCreditReversalWizard {
      return super.CurrentLocation as pcf.AccountNewCreditReversalWizard
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get creditReversal () : CreditReversal {
      return getVariableValue("creditReversal", 0) as CreditReversal
    }
    
    property set creditReversal ($arg :  CreditReversal) {
      setVariableValue("creditReversal", 0, $arg)
    }
    
    
  }
  
  
}