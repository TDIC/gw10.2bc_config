package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalConfirmationDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewChargeReversalConfirmationDVExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalConfirmationDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends NewChargeReversalConfirmationDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at NewChargeReversalConfirmationDV.pcf: line 48, column 40
    function currency_17 () : typekey.Currency {
      return lineItem.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at NewChargeReversalConfirmationDV.pcf: line 48, column 40
    function valueRoot_16 () : java.lang.Object {
      return lineItem
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at NewChargeReversalConfirmationDV.pcf: line 57, column 76
    function valueRoot_22 () : java.lang.Object {
      return lineItem.TAccount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at NewChargeReversalConfirmationDV.pcf: line 48, column 40
    function value_15 () : gw.pl.currency.MonetaryAmount {
      return lineItem.Amount
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at NewChargeReversalConfirmationDV.pcf: line 53, column 47
    function value_19 () : typekey.LedgerSide {
      return reversal.getType(lineItem.Type)
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at NewChargeReversalConfirmationDV.pcf: line 57, column 76
    function value_21 () : java.lang.String {
      return lineItem.TAccount.TAccountOwnerTypeNameTAccountName
    }
    
    property get lineItem () : entity.LineItem {
      return getIteratedValue(1) as entity.LineItem
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalConfirmationDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewChargeReversalConfirmationDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyInput (id=Reason_Input) at NewChargeReversalConfirmationDV.pcf: line 29, column 43
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      reversal.Reason = (__VALUE_TO_SET as typekey.ReversalReason)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at NewChargeReversalConfirmationDV.pcf: line 48, column 40
    function sortValue_12 (lineItem :  entity.LineItem) : java.lang.Object {
      return lineItem.Amount
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at NewChargeReversalConfirmationDV.pcf: line 53, column 47
    function sortValue_13 (lineItem :  entity.LineItem) : java.lang.Object {
      return reversal.getType(lineItem.Type)
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at NewChargeReversalConfirmationDV.pcf: line 57, column 76
    function sortValue_14 (lineItem :  entity.LineItem) : java.lang.Object {
      return lineItem.TAccount.TAccountOwnerTypeNameTAccountName
    }
    
    // 'value' attribute on DateInput (id=TransactionDate_Input) at NewChargeReversalConfirmationDV.pcf: line 14, column 43
    function valueRoot_1 () : java.lang.Object {
      return reversal.Charge
    }
    
    // 'value' attribute on TypeKeyInput (id=Reason_Input) at NewChargeReversalConfirmationDV.pcf: line 29, column 43
    function valueRoot_10 () : java.lang.Object {
      return reversal
    }
    
    // 'value' attribute on DateInput (id=TransactionDate_Input) at NewChargeReversalConfirmationDV.pcf: line 14, column 43
    function value_0 () : java.util.Date {
      return reversal.Charge.ChargeDate
    }
    
    // 'value' attribute on RowIterator at NewChargeReversalConfirmationDV.pcf: line 41, column 41
    function value_24 () : entity.LineItem[] {
      return reversal.Charge.ChargeInitialTxn.LineItems
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at NewChargeReversalConfirmationDV.pcf: line 18, column 44
    function value_3 () : java.lang.String {
      return reversal.Charge.DisplayName
    }
    
    // 'value' attribute on TextInput (id=NewCharge_Input) at NewChargeReversalConfirmationDV.pcf: line 23, column 112
    function value_6 () : java.lang.String {
      return DisplayKey.get("Java.ChargeReversal.ChargeReversalName", reversal.Charge.DisplayName)
    }
    
    // 'value' attribute on TypeKeyInput (id=Reason_Input) at NewChargeReversalConfirmationDV.pcf: line 29, column 43
    function value_8 () : typekey.ReversalReason {
      return reversal.Reason
    }
    
    property get reversal () : ChargeReversal {
      return getRequireValue("reversal", 0) as ChargeReversal
    }
    
    property set reversal ($arg :  ChargeReversal) {
      setRequireValue("reversal", 0, $arg)
    }
    
    
  }
  
  
}