package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillPromises_PromiseApplicationInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillPromises_PromiseApplicationInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillPromises_PromiseApplicationInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillPromises_PromiseApplicationInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyBillPromises_PromiseApplicationInputSet.pcf: line 17, column 62
    function currency_2 () : typekey.Currency {
      return selectedPromise.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyBillPromises_PromiseApplicationInputSet.pcf: line 17, column 62
    function valueRoot_1 () : java.lang.Object {
      return selectedPromise.BaseMoneyReceived
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Distributed_Input) at AgencyBillPromises_PromiseApplicationInputSet.pcf: line 23, column 71
    function valueRoot_5 () : java.lang.Object {
      return selectedPromise
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyBillPromises_PromiseApplicationInputSet.pcf: line 17, column 62
    function value_0 () : gw.pl.currency.MonetaryAmount {
      return selectedPromise.BaseMoneyReceived.TotalAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Remaining_Input) at AgencyBillPromises_PromiseApplicationInputSet.pcf: line 35, column 48
    function value_12 () : gw.pl.currency.MonetaryAmount {
      return selectedPromise.RemainingAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Distributed_Input) at AgencyBillPromises_PromiseApplicationInputSet.pcf: line 23, column 71
    function value_4 () : gw.pl.currency.MonetaryAmount {
      return selectedPromise.NetDistributedAmountForSavedOrExecuted
    }
    
    // 'value' attribute on MonetaryAmountInput (id=InSuspense_Input) at AgencyBillPromises_PromiseApplicationInputSet.pcf: line 29, column 68
    function value_8 () : gw.pl.currency.MonetaryAmount {
      return selectedPromise.NetSuspenseAmountForSavedOrExecuted
    }
    
    property get selectedPromise () : AgencyCyclePromise {
      return getRequireValue("selectedPromise", 0) as AgencyCyclePromise
    }
    
    property set selectedPromise ($arg :  AgencyCyclePromise) {
      setRequireValue("selectedPromise", 0, $arg)
    }
    
    
  }
  
  
}