package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TransactionsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransactionsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/TransactionsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransactionsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (transactions :  entity.Transaction[]) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=TransactionsPopup_CancelButton) at TransactionsPopup.pcf: line 29, column 62
    function action_1 () : void {
      CurrentLocation.cancel()
    }
    
    // 'def' attribute on PanelRef at TransactionsPopup.pcf: line 33, column 64
    function def_onEnter_2 (def :  pcf.TroubleTicketTransactionsLV) : void {
      def.onEnter(transactions, true)
    }
    
    // 'def' attribute on PanelRef at TransactionsPopup.pcf: line 33, column 64
    function def_refreshVariables_3 (def :  pcf.TroubleTicketTransactionsLV) : void {
      def.refreshVariables(transactions, true)
    }
    
    // 'pickValue' attribute on CheckedValuesToolbarButton (id=TransactionsPopup_SelectButton) at TransactionsPopup.pcf: line 24, column 38
    function pickValue_0 (CheckedValues :  entity.Transaction[]) : Transaction[] {
      return CheckedValues
    }
    
    override property get CurrentLocation () : pcf.TransactionsPopup {
      return super.CurrentLocation as pcf.TransactionsPopup
    }
    
    property get transactions () : entity.Transaction[] {
      return getVariableValue("transactions", 0) as entity.Transaction[]
    }
    
    property set transactions ($arg :  entity.Transaction[]) {
      setVariableValue("transactions", 0, $arg)
    }
    
    
  }
  
  
}