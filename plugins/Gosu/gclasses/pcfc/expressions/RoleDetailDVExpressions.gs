package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/roles/RoleDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class RoleDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/roles/RoleDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends RoleDetailDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=Permission_Cell) at RoleDetailDV.pcf: line 56, column 58
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      RolePrivilege.Permission = (__VALUE_TO_SET as typekey.SystemPermissionType)
    }
    
    // 'editable' attribute on TypeKeyCell (id=Permission_Cell) at RoleDetailDV.pcf: line 56, column 58
    function editable_13 () : java.lang.Boolean {
      return perm.RolePrivilege.edit
    }
    
    // 'value' attribute on TypeKeyCell (id=Permission_Cell) at RoleDetailDV.pcf: line 56, column 58
    function valueRoot_16 () : java.lang.Object {
      return RolePrivilege
    }
    
    // 'value' attribute on TextCell (id=PermissionCode_Cell) at RoleDetailDV.pcf: line 62, column 56
    function valueRoot_20 () : java.lang.Object {
      return RolePrivilege.Permission
    }
    
    // 'value' attribute on TypeKeyCell (id=Permission_Cell) at RoleDetailDV.pcf: line 56, column 58
    function value_14 () : typekey.SystemPermissionType {
      return RolePrivilege.Permission
    }
    
    // 'value' attribute on TextCell (id=PermissionCode_Cell) at RoleDetailDV.pcf: line 62, column 56
    function value_19 () : java.lang.String {
      return RolePrivilege.Permission.Code
    }
    
    // 'value' attribute on TextCell (id=PermissionDesc_Cell) at RoleDetailDV.pcf: line 67, column 63
    function value_22 () : java.lang.String {
      return RolePrivilege.Permission.Description
    }
    
    property get RolePrivilege () : entity.RolePrivilege {
      return getIteratedValue(1) as entity.RolePrivilege
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/roles/RoleDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RoleDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at RoleDetailDV.pcf: line 18, column 28
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      Role.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at RoleDetailDV.pcf: line 24, column 35
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      Role.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'sortBy' attribute on TypeKeyCell (id=Permission_Cell) at RoleDetailDV.pcf: line 56, column 58
    function sortValue_10 (RolePrivilege :  entity.RolePrivilege) : java.lang.Object {
      return RolePrivilege.Permission
    }
    
    // 'value' attribute on TextCell (id=PermissionCode_Cell) at RoleDetailDV.pcf: line 62, column 56
    function sortValue_11 (RolePrivilege :  entity.RolePrivilege) : java.lang.Object {
      return RolePrivilege.Permission.Code
    }
    
    // 'value' attribute on TextCell (id=PermissionDesc_Cell) at RoleDetailDV.pcf: line 67, column 63
    function sortValue_12 (RolePrivilege :  entity.RolePrivilege) : java.lang.Object {
      return RolePrivilege.Permission.Description
    }
    
    // 'toAdd' attribute on RowIterator at RoleDetailDV.pcf: line 44, column 48
    function toAdd_25 (RolePrivilege :  entity.RolePrivilege) : void {
      Role.addToPrivileges(RolePrivilege)
    }
    
    // 'toRemove' attribute on RowIterator at RoleDetailDV.pcf: line 44, column 48
    function toRemove_26 (RolePrivilege :  entity.RolePrivilege) : void {
      Role.removeFromPrivileges(RolePrivilege)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at RoleDetailDV.pcf: line 18, column 28
    function valueRoot_2 () : java.lang.Object {
      return Role
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at RoleDetailDV.pcf: line 18, column 28
    function value_0 () : java.lang.String {
      return Role.Name
    }
    
    // 'value' attribute on RowIterator at RoleDetailDV.pcf: line 44, column 48
    function value_27 () : entity.RolePrivilege[] {
      return Role.Privileges
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at RoleDetailDV.pcf: line 24, column 35
    function value_4 () : java.lang.String {
      return Role.Description
    }
    
    // 'addVisible' attribute on IteratorButtons at RoleDetailDV.pcf: line 30, column 56
    function visible_8 () : java.lang.Boolean {
      return perm.RolePrivilege.create
    }
    
    // 'removeVisible' attribute on IteratorButtons at RoleDetailDV.pcf: line 30, column 56
    function visible_9 () : java.lang.Boolean {
      return perm.RolePrivilege.delete
    }
    
    property get Role () : Role {
      return getRequireValue("Role", 0) as Role
    }
    
    property set Role ($arg :  Role) {
      setRequireValue("Role", 0, $arg)
    }
    
    
  }
  
  
}