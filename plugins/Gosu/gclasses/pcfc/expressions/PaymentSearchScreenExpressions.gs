package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException
uses gw.pl.currency.MonetaryAmount
uses gw.search.PaymentSearchCriteria
uses gw.api.database.IQueryBeanResult
uses gw.search.SearchMethods
uses com.google.common.collect.Sets
uses gw.search.PaymentSearchType
@javax.annotation.Generated("config/web/pcf/search/PaymentSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/search/PaymentSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at PaymentSearchScreen.pcf: line 170, column 100
    function currency_74 () : typekey.Currency {
      return agencyMoneyReceived.Currency
    }
    
    // 'value' attribute on DateCell (id=AppliedDate_Cell) at PaymentSearchScreen.pcf: line 164, column 65
    function valueRoot_71 () : java.lang.Object {
      return agencyMoneyReceived
    }
    
    // 'value' attribute on TextCell (id=ProducerName_Cell) at PaymentSearchScreen.pcf: line 160, column 156
    function value_68 () : java.lang.String {
      return agencyMoneyReceived.ProducerNameKanji.HasContent ? agencyMoneyReceived.ProducerNameKanji : agencyMoneyReceived.ProducerName
    }
    
    // 'value' attribute on DateCell (id=AppliedDate_Cell) at PaymentSearchScreen.pcf: line 164, column 65
    function value_70 () : java.util.Date {
      return agencyMoneyReceived.ReceivedDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at PaymentSearchScreen.pcf: line 170, column 100
    function value_73 () : gw.pl.currency.MonetaryAmount {
      return agencyMoneyReceived.Amount.ofCurrency(agencyMoneyReceived.Currency)
    }
    
    // 'value' attribute on TypeKeyCell (id=Method_Cell) at PaymentSearchScreen.pcf: line 175, column 58
    function value_76 () : typekey.PaymentMethod {
      return agencyMoneyReceived.PaymentMethod
    }
    
    // 'value' attribute on TextCell (id=CheckRefNumber_Cell) at PaymentSearchScreen.pcf: line 181, column 37
    function value_79 () : java.lang.String {
      return agencyMoneyReceived.RefNumber
    }
    
    property get agencyMoneyReceived () : entity.AgencyMoneyReceivedSearchView {
      return getIteratedValue(2) as entity.AgencyMoneyReceivedSearchView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/PaymentSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on DateCell (id=SuspensePaymentDate_Cell) at PaymentSearchScreen.pcf: line 214, column 60
    function action_93 () : void {
      DesktopSuspensePayments.go()
    }
    
    // 'action' attribute on DateCell (id=SuspensePaymentDate_Cell) at PaymentSearchScreen.pcf: line 214, column 60
    function action_dest_94 () : pcf.api.Destination {
      return pcf.DesktopSuspensePayments.createDestination()
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=SuspenseAmount_Cell) at PaymentSearchScreen.pcf: line 220, column 55
    function currency_100 () : typekey.Currency {
      return suspensePayment.Currency
    }
    
    // 'value' attribute on TypeKeyCell (id=ProductLine_Cell) at PaymentSearchScreen.pcf: line 237, column 52
    function valueRoot_109 () : java.lang.Object {
      return suspensePayment.Policy_TDIC
    }
    
    // 'value' attribute on TypeKeyCell (id=SuspenseMethod_Cell) at PaymentSearchScreen.pcf: line 249, column 37
    function valueRoot_115 () : java.lang.Object {
      return suspensePayment.PaymentInstrument
    }
    
    // 'value' attribute on DateCell (id=SuspensePaymentDate_Cell) at PaymentSearchScreen.pcf: line 214, column 60
    function valueRoot_96 () : java.lang.Object {
      return suspensePayment
    }
    
    // 'value' attribute on TextCell (id=SuspenseAccountNumber_Cell) at PaymentSearchScreen.pcf: line 226, column 37
    function value_102 () : java.lang.String {
      return suspensePayment.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=AccountName_Cell) at PaymentSearchScreen.pcf: line 231, column 65
    function value_105 () : java.lang.String {
      return suspensePayment.AccountName_TDIC
    }
    
    // 'value' attribute on TypeKeyCell (id=ProductLine_Cell) at PaymentSearchScreen.pcf: line 237, column 52
    function value_108 () : typekey.LOBCode {
      return suspensePayment.Policy_TDIC.LOBCode
    }
    
    // 'value' attribute on TextCell (id=SuspensePolicyNumber_Cell) at PaymentSearchScreen.pcf: line 243, column 37
    function value_111 () : java.lang.String {
      return suspensePayment.PolicyNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=SuspenseMethod_Cell) at PaymentSearchScreen.pcf: line 249, column 37
    function value_114 () : typekey.PaymentMethod {
      return suspensePayment.PaymentInstrument.PaymentMethod
    }
    
    // 'value' attribute on TextCell (id=SuspenseCheckRefNumber_Cell) at PaymentSearchScreen.pcf: line 255, column 37
    function value_117 () : java.lang.String {
      return suspensePayment.RefNumber
    }
    
    // 'value' attribute on TextCell (id=InvoiceNumber_Cell) at PaymentSearchScreen.pcf: line 259, column 62
    function value_120 () : java.lang.String {
      return suspensePayment.InvoiceNumber
    }
    
    // 'value' attribute on DateCell (id=SuspensePaymentDate_Cell) at PaymentSearchScreen.pcf: line 214, column 60
    function value_95 () : java.util.Date {
      return suspensePayment.PaymentDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=SuspenseAmount_Cell) at PaymentSearchScreen.pcf: line 220, column 55
    function value_98 () : gw.pl.currency.MonetaryAmount {
      return suspensePayment.Amount
    }
    
    property get suspensePayment () : entity.SuspensePayment {
      return getIteratedValue(2) as entity.SuspensePayment
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/PaymentSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on DateCell (id=PaymentDate_Cell) at PaymentSearchScreen.pcf: line 52, column 63
    function action_12 () : void {
      AccountPayments.go(paymentSearchView.Account, paymentSearchView.DirectBillMoneyRcvd)
    }
    
    // 'action' attribute on MonetaryAmountCell (id=Amount_Cell) at PaymentSearchScreen.pcf: line 59, column 96
    function action_17 () : void {
      AccountPayments.go(paymentSearchView.Account, paymentSearchView.DirectBillMoneyRcvd)
    }
    
    // 'action' attribute on DateCell (id=PaymentDate_Cell) at PaymentSearchScreen.pcf: line 52, column 63
    function action_dest_13 () : pcf.api.Destination {
      return pcf.AccountPayments.createDestination(paymentSearchView.Account, paymentSearchView.DirectBillMoneyRcvd)
    }
    
    // 'action' attribute on MonetaryAmountCell (id=Amount_Cell) at PaymentSearchScreen.pcf: line 59, column 96
    function action_dest_18 () : pcf.api.Destination {
      return pcf.AccountPayments.createDestination(paymentSearchView.Account, paymentSearchView.DirectBillMoneyRcvd)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at PaymentSearchScreen.pcf: line 59, column 96
    function currency_20 () : typekey.Currency {
      return paymentSearchView.Currency
    }
    
    // 'value' attribute on DateCell (id=PaymentDate_Cell) at PaymentSearchScreen.pcf: line 52, column 63
    function valueRoot_15 () : java.lang.Object {
      return paymentSearchView
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at PaymentSearchScreen.pcf: line 65, column 37
    function valueRoot_23 () : java.lang.Object {
      return paymentSearchView.Account
    }
    
    // 'value' attribute on TypeKeyCell (id=ProductLine_Cell) at PaymentSearchScreen.pcf: line 76, column 52
    function valueRoot_29 () : java.lang.Object {
      return paymentSearchView.UnappliedFund.Policy
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at PaymentSearchScreen.pcf: line 87, column 82
    function valueRoot_35 () : java.lang.Object {
      return paymentSearchView.DirectBillMoneyRcvd
    }
    
    // 'value' attribute on TextCell (id=feewaiverdescription_Cell) at PaymentSearchScreen.pcf: line 115, column 53
    function valueRoot_50 () : java.lang.Object {
      return paymentSearchView.DirectBillMoneyRcvd.CCFeeWaiverReason_TDIC
    }
    
    // 'value' attribute on DateCell (id=PaymentDate_Cell) at PaymentSearchScreen.pcf: line 52, column 63
    function value_14 () : java.util.Date {
      return paymentSearchView.ReceivedDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at PaymentSearchScreen.pcf: line 59, column 96
    function value_19 () : gw.pl.currency.MonetaryAmount {
      return paymentSearchView.Amount.ofCurrency(paymentSearchView.Currency)
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at PaymentSearchScreen.pcf: line 65, column 37
    function value_22 () : java.lang.String {
      return paymentSearchView.Account.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=AccountName_Cell) at PaymentSearchScreen.pcf: line 70, column 70
    function value_25 () : java.lang.String {
      return paymentSearchView.Account.AccountName
    }
    
    // 'value' attribute on TypeKeyCell (id=ProductLine_Cell) at PaymentSearchScreen.pcf: line 76, column 52
    function value_28 () : typekey.LOBCode {
      return paymentSearchView.UnappliedFund.Policy.LOBCode
    }
    
    // 'value' attribute on TextCell (id=UnappliedFund_Cell) at PaymentSearchScreen.pcf: line 81, column 57
    function value_31 () : entity.UnappliedFund {
      return paymentSearchView.UnappliedFund
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at PaymentSearchScreen.pcf: line 87, column 82
    function value_34 () : java.lang.String {
      return paymentSearchView.DirectBillMoneyRcvd.Description
    }
    
    // 'value' attribute on BooleanRadioCell (id=Distributed_Cell) at PaymentSearchScreen.pcf: line 92, column 88
    function value_37 () : java.lang.Boolean {
      return paymentSearchView.DirectBillMoneyRcvd.DistributedDenorm
    }
    
    // 'value' attribute on TypeKeyCell (id=Method_Cell) at PaymentSearchScreen.pcf: line 98, column 37
    function value_40 () : typekey.PaymentMethod {
      return paymentSearchView.PaymentMethod
    }
    
    // 'value' attribute on TextCell (id=CheckRefNumber_Cell) at PaymentSearchScreen.pcf: line 103, column 37
    function value_43 () : java.lang.String {
      return paymentSearchView.RefNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=FeeAmount_Cell) at PaymentSearchScreen.pcf: line 109, column 69
    function value_46 () : gw.pl.currency.MonetaryAmount {
      return validateFeeAmount(paymentSearchView)
    }
    
    // 'value' attribute on TextCell (id=feewaiverdescription_Cell) at PaymentSearchScreen.pcf: line 115, column 53
    function value_49 () : java.lang.String {
      return paymentSearchView.DirectBillMoneyRcvd.CCFeeWaiverReason_TDIC.DisplayName
    }
    
    // 'value' attribute on TextCell (id=feewaiverreason_Cell) at PaymentSearchScreen.pcf: line 120, column 97
    function value_52 () : java.lang.String {
      return paymentSearchView.DirectBillMoneyRcvd.CCFeeWaiverReasonDesc_TDIC
    }
    
    // 'value' attribute on TextCell (id=ReceivedBy_Cell) at PaymentSearchScreen.pcf: line 126, column 48
    function value_55 () : entity.User {
      return paymentSearchView.DirectBillMoneyRcvd.CreateUser
    }
    
    property get paymentSearchView () : entity.DirectBillMoneyReceivedSearchView {
      return getIteratedValue(2) as entity.DirectBillMoneyReceivedSearchView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/PaymentSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    
        function validateAndSearch(searchCriteria: PaymentSearchCriteria): IQueryBeanResult<DirectBillMoneyReceivedSearchView> {
          nullOutHiddenSearchCriterionFields(searchCriteria)
          return SearchMethods.validateAndSearch(searchCriteria)
        }
    
        //any hidden search criteria input fields that are non-null will fail validation and throw an exception in PaymentSearchCriteria.gs
        function nullOutHiddenSearchCriterionFields(searchCriteria: PaymentSearchCriteria) {
          if(searchCriteria.Method == null){
            throw new DisplayableException(DisplayKey.get("TDIC.Web.PaymentSearchDV.Error.MissingRequiredField.PaymentMethod"))
          }
          if(searchCriteria.EarliestDate == null or searchCriteria.LatestDate == null ) {
            throw new DisplayableException(DisplayKey.get("TDIC.Web.PaymentSearchDV.Error.MissingRequiredFields.Dates"))
          }
          if ({PaymentSearchType.AGENCYBILL, PaymentSearchType.ALL}.contains(searchCriteria.PaymentType)) {
            searchCriteria.AccountNumber = null
            searchCriteria.PolicyNumber = null
          }
          if ({PaymentSearchType.DIRECTBILL_AND_SUSPENSE, PaymentSearchType.ALL}.contains(searchCriteria.PaymentType)) {
            searchCriteria.ProducerName = null
            searchCriteria.ProducerNameKanji = null
            searchCriteria.ProducerCode = null
          }
        }
    
        function validateFeeAmount(paymentSearchView : DirectBillMoneyReceivedSearchView): MonetaryAmount{
         var ccFeeAmount = ScriptParameters.getParameterValue("TDIC_CreditCardFeeAmount") as double
         var feeAmount = new MonetaryAmount(ccFeeAmount, Currency.TC_USD)  
         if(paymentSearchView.PaymentMethod ==  PaymentMethod.TC_CREDITCARD && paymentSearchView.DirectBillMoneyRcvd.CCFeeWaiverReason_TDIC == null){
          return feeAmount
         }  
         return null
        }
    
       function validateSearchResults(searchCriteria : PaymentSearchCriteria ): gw.api.database.IQueryBeanResult<entity.DirectBillMoneyReceivedSearchView>{
        var searchresults = searchCriteria?.DirectBillMoneyRcvdSearchResults
        if(searchresults !=null && searchresults.Count > 1500) {
          throw new DisplayableException(DisplayKey.get("TDIC.Web.PaymentSearchResultsLV.Error.TooManyResults"))
        }
        return searchresults
       }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/PaymentSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends PaymentSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=printexport) at PaymentSearchScreen.pcf: line 30, column 100
    function action_2 () : void {
      gw.api.print.ListViewPrintOptionPopupAction.printListViewOnlyWithOptions("PaymentSearchResultsLV", DisplayKey.get("TDIC.Web.PaymentSearchResultsLV.PrintExport"), null)
    }
    
    // 'action' attribute on ToolbarButton (id=printexport) at PaymentSearchScreen.pcf: line 141, column 100
    function action_61 () : void {
      gw.api.print.ListViewPrintOptionPopupAction.printListViewOnlyWithOptions("PaymentSearchResultsLV", DisplayKey.get("TDIC.Web.PaymentSearchResultsLV.PrintExport"), null)
    }
    
    // 'action' attribute on ToolbarButton (id=printexport) at PaymentSearchScreen.pcf: line 196, column 100
    function action_85 () : void {
      gw.api.print.ListViewPrintOptionPopupAction.printListViewOnlyWithOptions("PaymentSearchResultsLV", DisplayKey.get("TDIC.Web.PaymentSearchResultsLV.PrintExport"), null)
    }
    
    // 'def' attribute on PanelRef at PaymentSearchScreen.pcf: line 16, column 47
    function def_onEnter_0 (def :  pcf.PaymentSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at PaymentSearchScreen.pcf: line 16, column 47
    function def_refreshVariables_1 (def :  pcf.PaymentSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'searchCriteria' attribute on SearchPanel at PaymentSearchScreen.pcf: line 14, column 101
    function searchCriteria_127 () : gw.search.PaymentSearchCriteria {
      return new gw.search.PaymentSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at PaymentSearchScreen.pcf: line 14, column 101
    function search_126 () : java.lang.Object {
      return validateAndSearch(searchCriteria)
    }
    
    // 'sortBy' attribute on MonetaryAmountCell (id=FeeAmount_Cell) at PaymentSearchScreen.pcf: line 109, column 69
    function sortValue_10 (paymentSearchView :  entity.DirectBillMoneyReceivedSearchView) : java.lang.Object {
      return paymentSearchView.Currency
    }
    
    // 'sortBy' attribute on MonetaryAmountCell (id=FeeAmount_Cell) at PaymentSearchScreen.pcf: line 109, column 69
    function sortValue_11 (paymentSearchView :  entity.DirectBillMoneyReceivedSearchView) : java.lang.Object {
      return  paymentSearchView.Amount
    }
    
    // 'value' attribute on DateCell (id=PaymentDate_Cell) at PaymentSearchScreen.pcf: line 52, column 63
    function sortValue_3 (paymentSearchView :  entity.DirectBillMoneyReceivedSearchView) : java.lang.Object {
      return paymentSearchView.ReceivedDate
    }
    
    // 'sortBy' attribute on MonetaryAmountCell (id=Amount_Cell) at PaymentSearchScreen.pcf: line 59, column 96
    function sortValue_4 (paymentSearchView :  entity.DirectBillMoneyReceivedSearchView) : java.lang.Object {
      return paymentSearchView.Currency
    }
    
    // 'sortBy' attribute on MonetaryAmountCell (id=Amount_Cell) at PaymentSearchScreen.pcf: line 59, column 96
    function sortValue_5 (paymentSearchView :  entity.DirectBillMoneyReceivedSearchView) : java.lang.Object {
      return  paymentSearchView.Amount
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at PaymentSearchScreen.pcf: line 65, column 37
    function sortValue_6 (paymentSearchView :  entity.DirectBillMoneyReceivedSearchView) : java.lang.Object {
      return paymentSearchView.Account.AccountNumber
    }
    
    // 'sortBy' attribute on TextCell (id=ProducerName_Cell) at PaymentSearchScreen.pcf: line 160, column 156
    function sortValue_62 (agencyMoneyReceived :  entity.AgencyMoneyReceivedSearchView) : java.lang.Object {
      return agencyMoneyReceived.ProducerName
    }
    
    // 'value' attribute on DateCell (id=AppliedDate_Cell) at PaymentSearchScreen.pcf: line 164, column 65
    function sortValue_63 (agencyMoneyReceived :  entity.AgencyMoneyReceivedSearchView) : java.lang.Object {
      return agencyMoneyReceived.ReceivedDate
    }
    
    // 'sortBy' attribute on MonetaryAmountCell (id=Amount_Cell) at PaymentSearchScreen.pcf: line 170, column 100
    function sortValue_64 (agencyMoneyReceived :  entity.AgencyMoneyReceivedSearchView) : java.lang.Object {
      return agencyMoneyReceived.Currency
    }
    
    // 'sortBy' attribute on MonetaryAmountCell (id=Amount_Cell) at PaymentSearchScreen.pcf: line 170, column 100
    function sortValue_65 (agencyMoneyReceived :  entity.AgencyMoneyReceivedSearchView) : java.lang.Object {
      return  agencyMoneyReceived.Amount
    }
    
    // 'value' attribute on TypeKeyCell (id=Method_Cell) at PaymentSearchScreen.pcf: line 175, column 58
    function sortValue_66 (agencyMoneyReceived :  entity.AgencyMoneyReceivedSearchView) : java.lang.Object {
      return agencyMoneyReceived.PaymentMethod
    }
    
    // 'value' attribute on TextCell (id=CheckRefNumber_Cell) at PaymentSearchScreen.pcf: line 181, column 37
    function sortValue_67 (agencyMoneyReceived :  entity.AgencyMoneyReceivedSearchView) : java.lang.Object {
      return agencyMoneyReceived.RefNumber
    }
    
    // 'value' attribute on TextCell (id=UnappliedFund_Cell) at PaymentSearchScreen.pcf: line 81, column 57
    function sortValue_7 (paymentSearchView :  entity.DirectBillMoneyReceivedSearchView) : java.lang.Object {
      return paymentSearchView.UnappliedFund
    }
    
    // 'value' attribute on TypeKeyCell (id=Method_Cell) at PaymentSearchScreen.pcf: line 98, column 37
    function sortValue_8 (paymentSearchView :  entity.DirectBillMoneyReceivedSearchView) : java.lang.Object {
      return paymentSearchView.PaymentMethod
    }
    
    // 'value' attribute on DateCell (id=SuspensePaymentDate_Cell) at PaymentSearchScreen.pcf: line 214, column 60
    function sortValue_86 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.PaymentDate
    }
    
    // 'sortBy' attribute on MonetaryAmountCell (id=SuspenseAmount_Cell) at PaymentSearchScreen.pcf: line 220, column 55
    function sortValue_87 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.Amount
    }
    
    // 'value' attribute on TextCell (id=SuspenseAccountNumber_Cell) at PaymentSearchScreen.pcf: line 226, column 37
    function sortValue_88 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=SuspensePolicyNumber_Cell) at PaymentSearchScreen.pcf: line 243, column 37
    function sortValue_89 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=CheckRefNumber_Cell) at PaymentSearchScreen.pcf: line 103, column 37
    function sortValue_9 (paymentSearchView :  entity.DirectBillMoneyReceivedSearchView) : java.lang.Object {
      return paymentSearchView.RefNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=SuspenseMethod_Cell) at PaymentSearchScreen.pcf: line 249, column 37
    function sortValue_90 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.PaymentInstrument.PaymentMethod
    }
    
    // 'value' attribute on TextCell (id=SuspenseCheckRefNumber_Cell) at PaymentSearchScreen.pcf: line 255, column 37
    function sortValue_91 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.RefNumber
    }
    
    // 'value' attribute on TextCell (id=InvoiceNumber_Cell) at PaymentSearchScreen.pcf: line 259, column 62
    function sortValue_92 (suspensePayment :  entity.SuspensePayment) : java.lang.Object {
      return suspensePayment.InvoiceNumber
    }
    
    // 'title' attribute on Card (id=SuspensePaymentSearchResults) at PaymentSearchScreen.pcf: line 190, column 196
    function title_125 () : java.lang.String {
      return DisplayKey.get("Web.Search.Payments.SuspensePaymentResults", searchCriteria.SuspensePaymentSearchResultsCount)
    }
    
    // 'title' attribute on Card (id=PaymentSearchResults) at PaymentSearchScreen.pcf: line 24, column 196
    function title_60 () : java.lang.String {
      return DisplayKey.get("Web.Search.Payments.PaymentResults", searchCriteria.DirectBillMoneyRcvdSearchResultsCount)
    }
    
    // 'title' attribute on Card (id=AgencyMoneyReceivedSearchResults) at PaymentSearchScreen.pcf: line 135, column 183
    function title_84 () : java.lang.String {
      return DisplayKey.get("Web.Search.Payments.AgencyMoneyReceivedResults", searchCriteria.AgencyBillMoneyRcvdSearchResultsCount)
    }
    
    // 'value' attribute on RowIterator at PaymentSearchScreen.pcf: line 207, column 94
    function value_123 () : gw.api.database.IQueryBeanResult<entity.SuspensePayment> {
      return searchCriteria.SuspensePaymentSearchResults as gw.api.database.IQueryBeanResult<SuspensePayment>
    }
    
    // 'value' attribute on RowIterator at PaymentSearchScreen.pcf: line 41, column 112
    function value_58 () : gw.api.database.IQueryBeanResult<entity.DirectBillMoneyReceivedSearchView> {
      return validateSearchResults(searchCriteria)
    }
    
    // 'value' attribute on RowIterator at PaymentSearchScreen.pcf: line 152, column 108
    function value_82 () : gw.api.database.IQueryBeanResult<entity.AgencyMoneyReceivedSearchView> {
      return searchCriteria.AgencyBillMoneyRcvdSearchResults as gw.api.database.IQueryBeanResult<AgencyMoneyReceivedSearchView>
    }
    
    // 'visible' attribute on Card (id=PaymentSearchResults) at PaymentSearchScreen.pcf: line 24, column 196
    function visible_59 () : java.lang.Boolean {
      return (searchCriteria.ResultsTabsVisibleInUI == gw.search.PaymentSearchType.DIRECTBILL_AND_SUSPENSE) || (searchCriteria.ResultsTabsVisibleInUI == gw.search.PaymentSearchType.ALL)
    }
    
    // 'visible' attribute on Card (id=AgencyMoneyReceivedSearchResults) at PaymentSearchScreen.pcf: line 135, column 183
    function visible_83 () : java.lang.Boolean {
      return (searchCriteria.ResultsTabsVisibleInUI == gw.search.PaymentSearchType.AGENCYBILL) || (searchCriteria.ResultsTabsVisibleInUI == gw.search.PaymentSearchType.ALL)
    }
    
    property get paymentSearchViews () : gw.api.database.IQueryBeanResult<DirectBillMoneyReceivedSearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<DirectBillMoneyReceivedSearchView>
    }
    
    property get searchCriteria () : gw.search.PaymentSearchCriteria {
      return getCriteriaValue(1) as gw.search.PaymentSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.PaymentSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}