package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillAddInvoiceItemsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillAddInvoiceItemsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillAddInvoiceItemsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillAddInvoiceItemsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=cancel) at AgencyBillAddInvoiceItemsPopup.pcf: line 38, column 62
    function action_4 () : void {
      CurrentLocation.cancel()
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=SelectInvoiceItems) at AgencyBillAddInvoiceItemsPopup.pcf: line 34, column 97
    function allCheckedRowsAction_3 (CheckedValues :  entity.InvoiceItem[], CheckedValuesErrors :  java.util.Map) : void {
      agencyCycleDistView.addInvoiceItems( CheckedValues, prefill ); CurrentLocation.commit()
    }
    
    // 'def' attribute on PanelRef at AgencyBillAddInvoiceItemsPopup.pcf: line 53, column 95
    function def_onEnter_11 (def :  pcf.InvoiceItemSearchPanelSet) : void {
      def.onEnter(agencyCycleDistView.AgencyCycleDist, null, producer)
    }
    
    // 'def' attribute on PanelRef at AgencyBillAddInvoiceItemsPopup.pcf: line 53, column 95
    function def_refreshVariables_12 (def :  pcf.InvoiceItemSearchPanelSet) : void {
      def.refreshVariables(agencyCycleDistView.AgencyCycleDist, null, producer)
    }
    
    // 'value' attribute on RangeInput (id=Prefill_Input) at AgencyBillAddInvoiceItemsPopup.pcf: line 49, column 57
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      prefill = (__VALUE_TO_SET as typekey.AgencyCycleDistPrefill)
    }
    
    // 'initialValue' attribute on Variable at AgencyBillAddInvoiceItemsPopup.pcf: line 18, column 85
    function initialValue_0 () : gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistView {
      return wizardState.AgencyCycleDistView
    }
    
    // 'initialValue' attribute on Variable at AgencyBillAddInvoiceItemsPopup.pcf: line 22, column 24
    function initialValue_1 () : Producer {
      return wizardState.MoneySetup.Producer
    }
    
    // 'initialValue' attribute on Variable at AgencyBillAddInvoiceItemsPopup.pcf: line 26, column 38
    function initialValue_2 () : AgencyCycleDistPrefill {
      return AgencyCycleDistPrefill.TC_UNPAID
    }
    
    // 'valueRange' attribute on RangeInput (id=Prefill_Input) at AgencyBillAddInvoiceItemsPopup.pcf: line 49, column 57
    function valueRange_7 () : java.lang.Object {
      return new AgencyCycleDistPrefill[]{AgencyCycleDistPrefill.TC_UNPAID, AgencyCycleDistPrefill.TC_ZERO}
    }
    
    // 'value' attribute on RangeInput (id=Prefill_Input) at AgencyBillAddInvoiceItemsPopup.pcf: line 49, column 57
    function value_5 () : typekey.AgencyCycleDistPrefill {
      return prefill
    }
    
    // 'valueRange' attribute on RangeInput (id=Prefill_Input) at AgencyBillAddInvoiceItemsPopup.pcf: line 49, column 57
    function verifyValueRangeIsAllowedType_8 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Prefill_Input) at AgencyBillAddInvoiceItemsPopup.pcf: line 49, column 57
    function verifyValueRangeIsAllowedType_8 ($$arg :  typekey.AgencyCycleDistPrefill[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Prefill_Input) at AgencyBillAddInvoiceItemsPopup.pcf: line 49, column 57
    function verifyValueRange_9 () : void {
      var __valueRangeArg = new AgencyCycleDistPrefill[]{AgencyCycleDistPrefill.TC_UNPAID, AgencyCycleDistPrefill.TC_ZERO}
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_8(__valueRangeArg)
    }
    
    override property get CurrentLocation () : pcf.AgencyBillAddInvoiceItemsPopup {
      return super.CurrentLocation as pcf.AgencyBillAddInvoiceItemsPopup
    }
    
    property get agencyCycleDistView () : gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistView {
      return getVariableValue("agencyCycleDistView", 0) as gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistView
    }
    
    property set agencyCycleDistView ($arg :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistView) {
      setVariableValue("agencyCycleDistView", 0, $arg)
    }
    
    property get prefill () : AgencyCycleDistPrefill {
      return getVariableValue("prefill", 0) as AgencyCycleDistPrefill
    }
    
    property set prefill ($arg :  AgencyCycleDistPrefill) {
      setVariableValue("prefill", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    property get wizardState () : gw.agencybill.AgencyDistributionWizardHelper {
      return getVariableValue("wizardState", 0) as gw.agencybill.AgencyDistributionWizardHelper
    }
    
    property set wizardState ($arg :  gw.agencybill.AgencyDistributionWizardHelper) {
      setVariableValue("wizardState", 0, $arg)
    }
    
    
  }
  
  
}