package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/MinimalTAccountOwnerDetailsDV.policy.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class MinimalTAccountOwnerDetailsDV_policyExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/MinimalTAccountOwnerDetailsDV.policy.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class MinimalTAccountOwnerDetailsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextInput (id=PolicyPeriod_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 33, column 44
    function action_2 () : void {
      pcf.PolicySummary.push(policyPeriod)
    }
    
    // 'action' attribute on TextInput (id=Account_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 42, column 38
    function action_8 () : void {
      pcf.AccountSummaryPopup.push(unapplied.Account)
    }
    
    // 'action' attribute on TextInput (id=PolicyPeriod_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 33, column 44
    function action_dest_3 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(policyPeriod)
    }
    
    // 'action' attribute on TextInput (id=Account_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 42, column 38
    function action_dest_9 () : pcf.api.Destination {
      return pcf.AccountSummaryPopup.createDestination(unapplied.Account)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=BilledAmount_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 58, column 50
    function currency_21 () : typekey.Currency {
      return policyPeriod.Currency
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 96, column 38
    function currency_43 () : typekey.Currency {
      return unapplied.Currency
    }
    
    // 'initialValue' attribute on Variable at MinimalTAccountOwnerDetailsDV.policy.pcf: line 18, column 28
    function initialValue_0 () : PolicyPeriod {
      return (unapplied == null) ? tAccountOwner as PolicyPeriod : null
    }
    
    // 'initialValue' attribute on Variable at MinimalTAccountOwnerDetailsDV.policy.pcf: line 22, column 59
    function initialValue_1 () : gw.web.policy.PolicySummaryFinancialsHelper {
      return new gw.web.policy.PolicySummaryFinancialsHelper(policyPeriod)
    }
    
    // 'value' attribute on TextInput (id=Account_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 42, column 38
    function valueRoot_11 () : java.lang.Object {
      return unapplied
    }
    
    // 'value' attribute on TextInput (id=Insured_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 47, column 38
    function valueRoot_16 () : java.lang.Object {
      return unapplied.Account
    }
    
    // 'value' attribute on MonetaryAmountInput (id=BilledAmount_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 58, column 50
    function valueRoot_20 () : java.lang.Object {
      return financialsHelper
    }
    
    // 'value' attribute on TextInput (id=Account_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 42, column 38
    function value_10 () : entity.Account {
      return unapplied.Account
    }
    
    // 'value' attribute on TextInput (id=Insured_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 47, column 38
    function value_15 () : java.lang.String {
      return unapplied.Account.AccountNameLocalized
    }
    
    // 'value' attribute on MonetaryAmountInput (id=BilledAmount_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 58, column 50
    function value_19 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.BilledAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PastDueAmount_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 65, column 54
    function value_23 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.DelinquentAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=OutstandingAmount_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 72, column 55
    function value_27 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.OutstandingAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnbilledAmount_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 79, column 52
    function value_31 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.UnbilledAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnpaidAmount_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 86, column 50
    function value_35 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.UnpaidAmount
    }
    
    // 'value' attribute on TextInput (id=PolicyPeriod_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 33, column 44
    function value_4 () : entity.PolicyPeriod {
      return policyPeriod
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 96, column 38
    function value_41 () : gw.pl.currency.MonetaryAmount {
      return unapplied.Balance
    }
    
    // 'visible' attribute on InputSet at MinimalTAccountOwnerDetailsDV.policy.pcf: line 25, column 40
    function visible_6 () : java.lang.Boolean {
      return policyPeriod != null
    }
    
    // 'visible' attribute on TextInput (id=Account_Input) at MinimalTAccountOwnerDetailsDV.policy.pcf: line 42, column 38
    function visible_7 () : java.lang.Boolean {
      return unapplied != null
    }
    
    property get financialsHelper () : gw.web.policy.PolicySummaryFinancialsHelper {
      return getVariableValue("financialsHelper", 0) as gw.web.policy.PolicySummaryFinancialsHelper
    }
    
    property set financialsHelper ($arg :  gw.web.policy.PolicySummaryFinancialsHelper) {
      setVariableValue("financialsHelper", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get tAccountOwner () : TAccountOwner {
      return getRequireValue("tAccountOwner", 0) as TAccountOwner
    }
    
    property set tAccountOwner ($arg :  TAccountOwner) {
      setRequireValue("tAccountOwner", 0, $arg)
    }
    
    property get unapplied () : UnappliedFund {
      return getRequireValue("unapplied", 0) as UnappliedFund
    }
    
    property set unapplied ($arg :  UnappliedFund) {
      setRequireValue("unapplied", 0, $arg)
    }
    
    
  }
  
  
}