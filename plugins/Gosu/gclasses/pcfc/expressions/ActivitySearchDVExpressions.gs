package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ActivitySearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ActivitySearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/ActivitySearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ActivitySearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at ActivitySearchDV.pcf: line 34, column 48
    function action_11 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // MenuItem (id=UserBrowseMenuItem) at ActivitySearchDV.pcf: line 34, column 48
    function action_dest_12 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'def' attribute on InputSetRef at ActivitySearchDV.pcf: line 56, column 41
    function def_onEnter_33 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at ActivitySearchDV.pcf: line 56, column 41
    function def_refreshVariables_34 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on RangeInput (id=ActivityPatternCriterion_Input) at ActivitySearchDV.pcf: line 23, column 45
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ActivityPattern = (__VALUE_TO_SET as entity.ActivityPattern)
    }
    
    // 'value' attribute on AltUserInput (id=AssignedToUserCriterion_Input) at ActivitySearchDV.pcf: line 34, column 48
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AssignedToUser = (__VALUE_TO_SET as entity.User)
    }
    
    // 'value' attribute on RangeInput (id=AssignedToQueueCriterion_Input) at ActivitySearchDV.pcf: line 41, column 38
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AssignedToQueue_Ext = (__VALUE_TO_SET as AssignableQueue)
    }
    
    // 'value' attribute on TypeKeyInput (id=StatusCriterion_Input) at ActivitySearchDV.pcf: line 47, column 37
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Status = (__VALUE_TO_SET as ActivityStatus)
    }
    
    // 'value' attribute on BooleanDropdownInput (id=ApprovedCriterion_Input) at ActivitySearchDV.pcf: line 52, column 46
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Approved_Ext = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=ActivitySubtypeCriterion_Input) at ActivitySearchDV.pcf: line 29, column 39
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Subtype_Ext = (__VALUE_TO_SET as typekey.Activity)
    }
    
    // 'valueRange' attribute on RangeInput (id=AssignedToQueueCriterion_Input) at ActivitySearchDV.pcf: line 41, column 38
    function valueRange_20 () : java.lang.Object {
      return gw.api.database.Query.make(AssignableQueue).select()
    }
    
    // 'valueRange' attribute on RangeInput (id=ActivityPatternCriterion_Input) at ActivitySearchDV.pcf: line 23, column 45
    function valueRange_3 () : java.lang.Object {
      return gw.api.web.admin.ActivityPatternsUtil.getAllActivityPatterns()
    }
    
    // 'value' attribute on RangeInput (id=ActivityPatternCriterion_Input) at ActivitySearchDV.pcf: line 23, column 45
    function valueRoot_2 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on RangeInput (id=ActivityPatternCriterion_Input) at ActivitySearchDV.pcf: line 23, column 45
    function value_0 () : entity.ActivityPattern {
      return searchCriteria.ActivityPattern
    }
    
    // 'value' attribute on AltUserInput (id=AssignedToUserCriterion_Input) at ActivitySearchDV.pcf: line 34, column 48
    function value_13 () : entity.User {
      return searchCriteria.AssignedToUser
    }
    
    // 'value' attribute on RangeInput (id=AssignedToQueueCriterion_Input) at ActivitySearchDV.pcf: line 41, column 38
    function value_17 () : AssignableQueue {
      return searchCriteria.AssignedToQueue_Ext
    }
    
    // 'value' attribute on TypeKeyInput (id=StatusCriterion_Input) at ActivitySearchDV.pcf: line 47, column 37
    function value_24 () : ActivityStatus {
      return searchCriteria.Status
    }
    
    // 'value' attribute on BooleanDropdownInput (id=ApprovedCriterion_Input) at ActivitySearchDV.pcf: line 52, column 46
    function value_29 () : java.lang.Boolean {
      return searchCriteria.Approved_Ext
    }
    
    // 'value' attribute on TypeKeyInput (id=ActivitySubtypeCriterion_Input) at ActivitySearchDV.pcf: line 29, column 39
    function value_7 () : typekey.Activity {
      return searchCriteria.Subtype_Ext
    }
    
    // 'valueRange' attribute on RangeInput (id=AssignedToQueueCriterion_Input) at ActivitySearchDV.pcf: line 41, column 38
    function verifyValueRangeIsAllowedType_21 ($$arg :  AssignableQueue[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AssignedToQueueCriterion_Input) at ActivitySearchDV.pcf: line 41, column 38
    function verifyValueRangeIsAllowedType_21 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AssignedToQueueCriterion_Input) at ActivitySearchDV.pcf: line 41, column 38
    function verifyValueRangeIsAllowedType_21 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ActivityPatternCriterion_Input) at ActivitySearchDV.pcf: line 23, column 45
    function verifyValueRangeIsAllowedType_4 ($$arg :  entity.ActivityPattern[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ActivityPatternCriterion_Input) at ActivitySearchDV.pcf: line 23, column 45
    function verifyValueRangeIsAllowedType_4 ($$arg :  gw.api.database.IQueryBeanResult<entity.ActivityPattern>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ActivityPatternCriterion_Input) at ActivitySearchDV.pcf: line 23, column 45
    function verifyValueRangeIsAllowedType_4 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AssignedToQueueCriterion_Input) at ActivitySearchDV.pcf: line 41, column 38
    function verifyValueRange_22 () : void {
      var __valueRangeArg = gw.api.database.Query.make(AssignableQueue).select()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_21(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=ActivityPatternCriterion_Input) at ActivitySearchDV.pcf: line 23, column 45
    function verifyValueRange_5 () : void {
      var __valueRangeArg = gw.api.web.admin.ActivityPatternsUtil.getAllActivityPatterns()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_4(__valueRangeArg)
    }
    
    // 'valueType' attribute on TypeKeyInput (id=StatusCriterion_Input) at ActivitySearchDV.pcf: line 47, column 37
    function verifyValueType_28 () : void {
      var __valueTypeArg : ActivityStatus
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : gw.entity.TypeKey = __valueTypeArg
    }
    
    property get searchCriteria () : gw.search.ActivitySearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.search.ActivitySearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.ActivitySearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    property get user () : entity.User {
      return getVariableValue("user", 0) as entity.User
    }
    
    property set user ($arg :  entity.User) {
      setVariableValue("user", 0, $arg)
    }
    
    
  }
  
  
}