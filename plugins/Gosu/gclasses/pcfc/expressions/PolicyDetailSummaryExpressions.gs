package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailSummary.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailSummaryExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailSummary.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailSummaryExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=PolicyDetailSummary) at PolicyDetailSummary.pcf: line 10, column 71
    function canEdit_2 () : java.lang.Boolean {
      return perm.System.plcydtlsscrnedit_TDIC
    }
    
    // 'canVisit' attribute on Page (id=PolicyDetailSummary) at PolicyDetailSummary.pcf: line 10, column 71
    static function canVisit_3 (plcyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcysummview and not plcyPeriod.Archived
    }
    
    // 'def' attribute on ScreenRef at PolicyDetailSummary.pcf: line 17, column 52
    function def_onEnter_0 (def :  pcf.PolicyDetailSummaryScreen) : void {
      def.onEnter(plcyPeriod)
    }
    
    // 'def' attribute on ScreenRef at PolicyDetailSummary.pcf: line 17, column 52
    function def_refreshVariables_1 (def :  pcf.PolicyDetailSummaryScreen) : void {
      def.refreshVariables(plcyPeriod)
    }
    
    // Page (id=PolicyDetailSummary) at PolicyDetailSummary.pcf: line 10, column 71
    static function parent_4 (plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(plcyPeriod)
    }
    
    override property get CurrentLocation () : pcf.PolicyDetailSummary {
      return super.CurrentLocation as pcf.PolicyDetailSummary
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 0, $arg)
    }
    
    
  }
  
  
}