package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillStatementDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillStatementDetailPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillStatementDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillStatementDetailPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (statement :  StatementInvoice) : int {
      return 0
    }
    
    // 'def' attribute on ScreenRef at AgencyBillStatementDetailPopup.pcf: line 18, column 53
    function def_onEnter_1 (def :  pcf.AgencyBillStatementDetailScreen) : void {
      def.onEnter(cycle)
    }
    
    // 'def' attribute on ScreenRef at AgencyBillStatementDetailPopup.pcf: line 18, column 53
    function def_refreshVariables_2 (def :  pcf.AgencyBillStatementDetailScreen) : void {
      def.refreshVariables(cycle)
    }
    
    // 'initialValue' attribute on Variable at AgencyBillStatementDetailPopup.pcf: line 16, column 31
    function initialValue_0 () : AgencyBillCycle {
      return statement.AgencyBillCycle
    }
    
    // 'title' attribute on Popup (id=AgencyBillStatementDetailPopup) at AgencyBillStatementDetailPopup.pcf: line 7, column 102
    static function title_3 (statement :  StatementInvoice) : java.lang.Object {
      return DisplayKey.get("Web.AgencyBillStatementDetail.Title", statement.InvoiceNumber)
    }
    
    override property get CurrentLocation () : pcf.AgencyBillStatementDetailPopup {
      return super.CurrentLocation as pcf.AgencyBillStatementDetailPopup
    }
    
    property get cycle () : AgencyBillCycle {
      return getVariableValue("cycle", 0) as AgencyBillCycle
    }
    
    property set cycle ($arg :  AgencyBillCycle) {
      setVariableValue("cycle", 0, $arg)
    }
    
    property get statement () : StatementInvoice {
      return getVariableValue("statement", 0) as StatementInvoice
    }
    
    property set statement ($arg :  StatementInvoice) {
      setVariableValue("statement", 0, $arg)
    }
    
    
  }
  
  
}