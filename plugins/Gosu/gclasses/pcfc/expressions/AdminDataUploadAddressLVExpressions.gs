package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadAddressLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadAddressLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadAddressLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadAddressLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadAddressLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=city_Cell) at AdminDataUploadAddressLV.pcf: line 50, column 41
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.City")
    }
    
    // 'label' attribute on TextCell (id=state_Cell) at AdminDataUploadAddressLV.pcf: line 55, column 41
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.State")
    }
    
    // 'label' attribute on TextCell (id=postalCode_Cell) at AdminDataUploadAddressLV.pcf: line 60, column 41
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.PostalCode")
    }
    
    // 'label' attribute on TextCell (id=county_Cell) at AdminDataUploadAddressLV.pcf: line 65, column 41
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.County")
    }
    
    // 'label' attribute on TypeKeyCell (id=country_Cell) at AdminDataUploadAddressLV.pcf: line 70, column 40
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Country")
    }
    
    // 'label' attribute on TextCell (id=addressType_Cell) at AdminDataUploadAddressLV.pcf: line 75, column 41
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.AddressType")
    }
    
    // 'label' attribute on TextCell (id=displayName_Cell) at AdminDataUploadAddressLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.DisplayName")
    }
    
    // 'label' attribute on TextCell (id=addressLine1_Cell) at AdminDataUploadAddressLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Line1")
    }
    
    // 'label' attribute on TextCell (id=addressLine2_Cell) at AdminDataUploadAddressLV.pcf: line 40, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Line2")
    }
    
    // 'label' attribute on TextCell (id=addressLine3_Cell) at AdminDataUploadAddressLV.pcf: line 45, column 41
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Line3")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadAddressLV.pcf: line 23, column 46
    function sortValue_1 (address :  tdic.util.dataloader.data.AddressData) : java.lang.Object {
      return processor.getLoadStatus(address)
    }
    
    // 'value' attribute on TextCell (id=addressLine3_Cell) at AdminDataUploadAddressLV.pcf: line 45, column 41
    function sortValue_10 (address :  tdic.util.dataloader.data.AddressData) : java.lang.Object {
      return address.AddressLine3
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at AdminDataUploadAddressLV.pcf: line 50, column 41
    function sortValue_12 (address :  tdic.util.dataloader.data.AddressData) : java.lang.Object {
      return address.City
    }
    
    // 'value' attribute on TextCell (id=state_Cell) at AdminDataUploadAddressLV.pcf: line 55, column 41
    function sortValue_14 (address :  tdic.util.dataloader.data.AddressData) : java.lang.Object {
      return address.State
    }
    
    // 'value' attribute on TextCell (id=postalCode_Cell) at AdminDataUploadAddressLV.pcf: line 60, column 41
    function sortValue_16 (address :  tdic.util.dataloader.data.AddressData) : java.lang.Object {
      return address.PostalCode
    }
    
    // 'value' attribute on TextCell (id=county_Cell) at AdminDataUploadAddressLV.pcf: line 65, column 41
    function sortValue_18 (address :  tdic.util.dataloader.data.AddressData) : java.lang.Object {
      return address.County
    }
    
    // 'value' attribute on TypeKeyCell (id=country_Cell) at AdminDataUploadAddressLV.pcf: line 70, column 40
    function sortValue_20 (address :  tdic.util.dataloader.data.AddressData) : java.lang.Object {
      return address.Country
    }
    
    // 'value' attribute on TextCell (id=addressType_Cell) at AdminDataUploadAddressLV.pcf: line 75, column 41
    function sortValue_22 (address :  tdic.util.dataloader.data.AddressData) : java.lang.Object {
      return address.AddressType.Code
    }
    
    // 'value' attribute on TextCell (id=displayName_Cell) at AdminDataUploadAddressLV.pcf: line 29, column 41
    function sortValue_4 (address :  tdic.util.dataloader.data.AddressData) : java.lang.Object {
      return address.DisplayName
    }
    
    // 'value' attribute on TextCell (id=addressLine1_Cell) at AdminDataUploadAddressLV.pcf: line 35, column 41
    function sortValue_6 (address :  tdic.util.dataloader.data.AddressData) : java.lang.Object {
      return address.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=addressLine2_Cell) at AdminDataUploadAddressLV.pcf: line 40, column 41
    function sortValue_8 (address :  tdic.util.dataloader.data.AddressData) : java.lang.Object {
      return address.AddressLine2
    }
    
    // 'value' attribute on RowIterator (id=Address) at AdminDataUploadAddressLV.pcf: line 15, column 84
    function value_79 () : java.util.ArrayList<tdic.util.dataloader.data.AddressData> {
      return processor.AddressArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadAddressLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadAddressLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminDataUploadAddressLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at AdminDataUploadAddressLV.pcf: line 17, column 94
    function highlighted_78 () : java.lang.Boolean {
      return (address.Error or address.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadAddressLV.pcf: line 23, column 46
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=displayName_Cell) at AdminDataUploadAddressLV.pcf: line 29, column 41
    function label_28 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.DisplayName")
    }
    
    // 'label' attribute on TextCell (id=addressLine1_Cell) at AdminDataUploadAddressLV.pcf: line 35, column 41
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Line1")
    }
    
    // 'label' attribute on TextCell (id=addressLine2_Cell) at AdminDataUploadAddressLV.pcf: line 40, column 41
    function label_38 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Line2")
    }
    
    // 'label' attribute on TextCell (id=addressLine3_Cell) at AdminDataUploadAddressLV.pcf: line 45, column 41
    function label_43 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Line3")
    }
    
    // 'label' attribute on TextCell (id=city_Cell) at AdminDataUploadAddressLV.pcf: line 50, column 41
    function label_48 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.City")
    }
    
    // 'label' attribute on TextCell (id=state_Cell) at AdminDataUploadAddressLV.pcf: line 55, column 41
    function label_53 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.State")
    }
    
    // 'label' attribute on TextCell (id=postalCode_Cell) at AdminDataUploadAddressLV.pcf: line 60, column 41
    function label_58 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.PostalCode")
    }
    
    // 'label' attribute on TextCell (id=county_Cell) at AdminDataUploadAddressLV.pcf: line 65, column 41
    function label_63 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.County")
    }
    
    // 'label' attribute on TypeKeyCell (id=country_Cell) at AdminDataUploadAddressLV.pcf: line 70, column 40
    function label_68 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Country")
    }
    
    // 'label' attribute on TextCell (id=addressType_Cell) at AdminDataUploadAddressLV.pcf: line 75, column 41
    function label_73 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.AddressType")
    }
    
    // 'value' attribute on TextCell (id=displayName_Cell) at AdminDataUploadAddressLV.pcf: line 29, column 41
    function valueRoot_30 () : java.lang.Object {
      return address
    }
    
    // 'value' attribute on TextCell (id=addressType_Cell) at AdminDataUploadAddressLV.pcf: line 75, column 41
    function valueRoot_75 () : java.lang.Object {
      return address.AddressType
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadAddressLV.pcf: line 23, column 46
    function value_24 () : java.lang.String {
      return processor.getLoadStatus(address)
    }
    
    // 'value' attribute on TextCell (id=displayName_Cell) at AdminDataUploadAddressLV.pcf: line 29, column 41
    function value_29 () : java.lang.String {
      return address.DisplayName
    }
    
    // 'value' attribute on TextCell (id=addressLine1_Cell) at AdminDataUploadAddressLV.pcf: line 35, column 41
    function value_34 () : java.lang.String {
      return address.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=addressLine2_Cell) at AdminDataUploadAddressLV.pcf: line 40, column 41
    function value_39 () : java.lang.String {
      return address.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=addressLine3_Cell) at AdminDataUploadAddressLV.pcf: line 45, column 41
    function value_44 () : java.lang.String {
      return address.AddressLine3
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at AdminDataUploadAddressLV.pcf: line 50, column 41
    function value_49 () : java.lang.String {
      return address.City
    }
    
    // 'value' attribute on TextCell (id=state_Cell) at AdminDataUploadAddressLV.pcf: line 55, column 41
    function value_54 () : java.lang.String {
      return address.State
    }
    
    // 'value' attribute on TextCell (id=postalCode_Cell) at AdminDataUploadAddressLV.pcf: line 60, column 41
    function value_59 () : java.lang.String {
      return address.PostalCode
    }
    
    // 'value' attribute on TextCell (id=county_Cell) at AdminDataUploadAddressLV.pcf: line 65, column 41
    function value_64 () : java.lang.String {
      return address.County
    }
    
    // 'value' attribute on TypeKeyCell (id=country_Cell) at AdminDataUploadAddressLV.pcf: line 70, column 40
    function value_69 () : typekey.Country {
      return address.Country
    }
    
    // 'value' attribute on TextCell (id=addressType_Cell) at AdminDataUploadAddressLV.pcf: line 75, column 41
    function value_74 () : java.lang.String {
      return address.AddressType.Code
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadAddressLV.pcf: line 23, column 46
    function visible_25 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get address () : tdic.util.dataloader.data.AddressData {
      return getIteratedValue(1) as tdic.util.dataloader.data.AddressData
    }
    
    
  }
  
  
}