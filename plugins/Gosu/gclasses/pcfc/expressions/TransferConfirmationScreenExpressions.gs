package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/TransferConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransferConfirmationScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/TransferConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransferConfirmationScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at TransferConfirmationScreen.pcf: line 23, column 58
    function def_onEnter_2 (def :  pcf.TransferDetailsDV) : void {
      def.onEnter(fundsTransferUtil, false)
    }
    
    // 'def' attribute on PanelRef at TransferConfirmationScreen.pcf: line 23, column 58
    function def_refreshVariables_3 (def :  pcf.TransferDetailsDV) : void {
      def.refreshVariables(fundsTransferUtil, false)
    }
    
    // 'visible' attribute on AlertBar (id=ApprovalActivityAlertBar) at TransferConfirmationScreen.pcf: line 17, column 63
    function visible_0 () : java.lang.Boolean {
      return fundsTransferUtil.ApprovalActivitiesRequired
    }
    
    // 'visible' attribute on AlertBar (id=DestinationMissingAlertBar) at TransferConfirmationScreen.pcf: line 21, column 69
    function visible_1 () : java.lang.Boolean {
      return fundsTransferUtil.getTransferTargets().length == 0
    }
    
    property get fundsTransferUtil () : gw.api.web.transaction.FundsTransferUtil {
      return getRequireValue("fundsTransferUtil", 0) as gw.api.web.transaction.FundsTransferUtil
    }
    
    property set fundsTransferUtil ($arg :  gw.api.web.transaction.FundsTransferUtil) {
      setRequireValue("fundsTransferUtil", 0, $arg)
    }
    
    
  }
  
  
}