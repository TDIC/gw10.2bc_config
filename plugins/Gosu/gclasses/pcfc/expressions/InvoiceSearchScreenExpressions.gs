package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/InvoiceSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/search/InvoiceSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get isClearBundle () : boolean {
      return getRequireValue("isClearBundle", 0) as java.lang.Boolean
    }
    
    property set isClearBundle ($arg :  boolean) {
      setRequireValue("isClearBundle", 0, $arg)
    }
    
    property get showHyperlinks () : boolean {
      return getRequireValue("showHyperlinks", 0) as java.lang.Boolean
    }
    
    property set showHyperlinks ($arg :  boolean) {
      setRequireValue("showHyperlinks", 0, $arg)
    }
    
    /**
     * US1158 - APW Payment Reversals
     * 3/11/2015 Alvin Lee
     * 
     * Allow setting the account number in the search criteria ahead of time, if provided.  This allows a user to search for
     * an invoice based on the account number when creating payment requests from the UI. 
     */
    function createSearchCriteria() : gw.search.InvoiceSearchCriteria {
      var invSearchCriteria = new gw.search.InvoiceSearchCriteria()
      if (account != null)
      invSearchCriteria.AccountNumber = account.AccountNumber
      return invSearchCriteria
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/InvoiceSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends InvoiceSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'cachingEnabled' attribute on SearchPanel at InvoiceSearchScreen.pcf: line 25, column 85
    function cachingEnabled_4 () : java.lang.Boolean {
      return account == null
    }
    
    // 'def' attribute on PanelRef at InvoiceSearchScreen.pcf: line 27, column 47
    function def_onEnter_0 (def :  pcf.InvoiceSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at InvoiceSearchScreen.pcf: line 31, column 81
    function def_onEnter_2 (def :  pcf.InvoiceSearchResultsLV) : void {
      def.onEnter(invoiceSearchViews, showHyperlinks, false)
    }
    
    // 'def' attribute on PanelRef at InvoiceSearchScreen.pcf: line 27, column 47
    function def_refreshVariables_1 (def :  pcf.InvoiceSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at InvoiceSearchScreen.pcf: line 31, column 81
    function def_refreshVariables_3 (def :  pcf.InvoiceSearchResultsLV) : void {
      def.refreshVariables(invoiceSearchViews, showHyperlinks, false)
    }
    
    // 'maxSearchResults' attribute on SearchPanel at InvoiceSearchScreen.pcf: line 25, column 85
    function maxSearchResults_5 () : java.lang.Object {
      return gw.api.system.BCConfigParameters.MaxSearchResults.Value
    }
    
    // 'searchCriteria' attribute on SearchPanel at InvoiceSearchScreen.pcf: line 25, column 85
    function searchCriteria_7 () : gw.search.InvoiceSearchCriteria {
      return createSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at InvoiceSearchScreen.pcf: line 25, column 85
    function search_6 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria, StringCriterionMode.TC_STARTSWITH)
    }
    
    property get invoiceSearchViews () : gw.api.database.IQueryBeanResult<InvoiceSearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<InvoiceSearchView>
    }
    
    property get searchCriteria () : gw.search.InvoiceSearchCriteria {
      return getCriteriaValue(1) as gw.search.InvoiceSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.InvoiceSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}