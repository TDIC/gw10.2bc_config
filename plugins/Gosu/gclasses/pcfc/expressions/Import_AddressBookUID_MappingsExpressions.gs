package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses tdic.util.dataloader.AddressBookUIDUpdater
@javax.annotation.Generated("config/web/pcf/admin/importexport/Import_AddressBookUID_Mappings.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class Import_AddressBookUID_MappingsExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/importexport/Import_AddressBookUID_Mappings.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Import_AddressBookUID_MappingsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ButtonInput (id=GoButton_Input) at Import_AddressBookUID_Mappings.pcf: line 39, column 44
    function action_4 () : void {
      fileUploaded()
    }
    
    // 'action' attribute on ButtonInput (id=changeUIDs_Input) at Import_AddressBookUID_Mappings.pcf: line 47, column 50
    function action_7 () : void {
      updateUIDs()
    }
    
    // 'available' attribute on ButtonInput (id=changeUIDs_Input) at Import_AddressBookUID_Mappings.pcf: line 47, column 50
    function available_6 () : java.lang.Boolean {
      return uidMappings.size() > 0
    }
    
    // 'canVisit' attribute on Page (id=Import_AddressBookUID_Mappings) at Import_AddressBookUID_Mappings.pcf: line 11, column 46
    static function canVisit_14 () : java.lang.Boolean {
      return perm.ScriptParameter.view
    }
    
    // 'value' attribute on FileInput (id=MappingFile_Input) at Import_AddressBookUID_Mappings.pcf: line 35, column 33
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      importFile = (__VALUE_TO_SET as gw.api.web.WebFile)
    }
    
    // 'initialValue' attribute on Variable at Import_AddressBookUID_Mappings.pcf: line 18, column 55
    function initialValue_0 () : java.util.HashMap<String, String> {
      return new java.util.HashMap<String, String>()
    }
    
    // 'label' attribute on Label at Import_AddressBookUID_Mappings.pcf: line 41, column 95
    function label_5 () : java.lang.String {
      return "Found " + uidMappings.size() + " records to update"
    }
    
    // 'label' attribute on Label at Import_AddressBookUID_Mappings.pcf: line 49, column 171
    function label_9 () : java.lang.String {
      return "Success :" + abUIDUpdater?.Succeeded + ", Failed :" + abUIDUpdater?.Failed + ", Not Found :" + abUIDUpdater?.NotFound
    }
    
    // 'parent' attribute on Page (id=Import_AddressBookUID_Mappings) at Import_AddressBookUID_Mappings.pcf: line 11, column 46
    static function parent_15 () : pcf.api.Destination {
      return pcf.Admin.createDestination()
    }
    
    // 'value' attribute on TextCell (id=oldUID_Cell) at Import_AddressBookUID_Mappings.pcf: line 64, column 45
    function sortValue_10 (oldUIDKey :  java.lang.Object) : java.lang.Object {
      return oldUIDKey
    }
    
    // 'value' attribute on FileInput (id=MappingFile_Input) at Import_AddressBookUID_Mappings.pcf: line 35, column 33
    function value_1 () : gw.api.web.WebFile {
      return importFile
    }
    
    // 'value' attribute on RowIterator at Import_AddressBookUID_Mappings.pcf: line 58, column 42
    function value_13 () : java.lang.Object[] {
      return uidMappings.keySet().toArray()
    }
    
    override property get CurrentLocation () : pcf.Import_AddressBookUID_Mappings {
      return super.CurrentLocation as pcf.Import_AddressBookUID_Mappings
    }
    
    property get abUIDUpdater () : tdic.util.dataloader.AddressBookUIDUpdater {
      return getVariableValue("abUIDUpdater", 0) as tdic.util.dataloader.AddressBookUIDUpdater
    }
    
    property set abUIDUpdater ($arg :  tdic.util.dataloader.AddressBookUIDUpdater) {
      setVariableValue("abUIDUpdater", 0, $arg)
    }
    
    property get importFile () : gw.api.web.WebFile {
      return getVariableValue("importFile", 0) as gw.api.web.WebFile
    }
    
    property set importFile ($arg :  gw.api.web.WebFile) {
      setVariableValue("importFile", 0, $arg)
    }
    
    property get uidMappings () : java.util.HashMap<String, String> {
      return getVariableValue("uidMappings", 0) as java.util.HashMap<String, String>
    }
    
    property set uidMappings ($arg :  java.util.HashMap<String, String>) {
      setVariableValue("uidMappings", 0, $arg)
    }
    
    
    function fileUploaded() {
      if( importFile == null ) {
        return
      }
    
      uidMappings.clear()//Clear everything first.
      abUIDUpdater = null
    
      var inStream = importFile.InputStream
      var allBytes = new byte[inStream.available()]
      inStream.read(allBytes)
      var contentString = new String(allBytes)
      var contentSplitNewLines = contentString.split("\\r|\\n")
      for(aLine in contentSplitNewLines) {
        if( aLine.trim().length() > 0 ) { //Ignore empty lines
          //print(aLine + " - " + aLine.trim().length() + " - " + aLine.contains("|") )
          if( aLine.contains("|") ) { //Ignore lines that don't have '|'
            var splitted = aLine.split("\\|")
            //print(splitted.length)
            if( splitted.length > 1 ) { //Ignore lines that DONT have two parts to them, i.e, those that are not in the format <OLD_ADDRESSBOOK_UID>|<NEW_ADDRESSBOOK_UID>"
              var oldID = splitted[0]
              var newID = splitted[1]
              if( not oldID.equals(newID) ) { //If the IDs are different, then perform the update.
                uidMappings.put(oldID, newID)
              }
            }
          }
        }
      }
    }
    
    function updateUIDs() {
      if( uidMappings == null ) {
        return
      }
      abUIDUpdater = new AddressBookUIDUpdater(uidMappings);
      abUIDUpdater.performUpdate()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/importexport/Import_AddressBookUID_Mappings.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends Import_AddressBookUID_MappingsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=oldUID_Cell) at Import_AddressBookUID_Mappings.pcf: line 64, column 45
    function value_11 () : java.lang.Object {
      return oldUIDKey
    }
    
    property get oldUIDKey () : java.lang.Object {
      return getIteratedValue(1) as java.lang.Object
    }
    
    
  }
  
  
}