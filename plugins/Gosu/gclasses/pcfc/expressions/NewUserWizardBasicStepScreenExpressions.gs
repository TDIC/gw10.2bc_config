package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/users/NewUserWizardBasicStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewUserWizardBasicStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/users/NewUserWizardBasicStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewUserWizardBasicStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewUserWizardBasicStepScreen.pcf: line 16, column 39
    function def_onEnter_0 (def :  pcf.UserDetailDV) : void {
      def.onEnter(user, true)
    }
    
    // 'def' attribute on PanelRef at NewUserWizardBasicStepScreen.pcf: line 16, column 39
    function def_refreshVariables_1 (def :  pcf.UserDetailDV) : void {
      def.refreshVariables(user, true)
    }
    
    property get user () : User {
      return getRequireValue("user", 0) as User
    }
    
    property set user ($arg :  User) {
      setRequireValue("user", 0, $arg)
    }
    
    
  }
  
  
}