package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/CloneDelinquencyPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CloneDelinquencyPlanExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/delinquency/CloneDelinquencyPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CloneDelinquencyPlanExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (delinquencyPlan :  DelinquencyPlan, includeSpecs :  boolean) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=CloneDelinquencyPlan) at CloneDelinquencyPlan.pcf: line 13, column 89
    function afterCancel_3 () : void {
      DelinquencyPlanDetail.go(delinquencyPlan)
    }
    
    // 'afterCancel' attribute on Page (id=CloneDelinquencyPlan) at CloneDelinquencyPlan.pcf: line 13, column 89
    function afterCancel_dest_4 () : pcf.api.Destination {
      return pcf.DelinquencyPlanDetail.createDestination(delinquencyPlan)
    }
    
    // 'afterCommit' attribute on Page (id=CloneDelinquencyPlan) at CloneDelinquencyPlan.pcf: line 13, column 89
    function afterCommit_5 (TopLocation :  pcf.api.Location) : void {
      DelinquencyPlanDetail.go(clonedDelinquencyPlan)
    }
    
    // 'def' attribute on ScreenRef at CloneDelinquencyPlan.pcf: line 27, column 65
    function def_onEnter_1 (def :  pcf.DelinquencyPlanDetailScreen) : void {
      def.onEnter(clonedDelinquencyPlan)
    }
    
    // 'def' attribute on ScreenRef at CloneDelinquencyPlan.pcf: line 27, column 65
    function def_refreshVariables_2 (def :  pcf.DelinquencyPlanDetailScreen) : void {
      def.refreshVariables(clonedDelinquencyPlan)
    }
    
    // 'initialValue' attribute on Variable at CloneDelinquencyPlan.pcf: line 25, column 38
    function initialValue_0 () : entity.DelinquencyPlan {
      return gw.plugin.Plugins.get(gw.module.IFeesThresholds).cloneDelinquencyPlan(delinquencyPlan, includeSpecs)
    }
    
    // 'parent' attribute on Page (id=CloneDelinquencyPlan) at CloneDelinquencyPlan.pcf: line 13, column 89
    static function parent_6 (delinquencyPlan :  DelinquencyPlan, includeSpecs :  boolean) : pcf.api.Destination {
      return pcf.DelinquencyPlans.createDestination()
    }
    
    // 'title' attribute on Page (id=CloneDelinquencyPlan) at CloneDelinquencyPlan.pcf: line 13, column 89
    static function title_7 (delinquencyPlan :  DelinquencyPlan, includeSpecs :  boolean) : java.lang.Object {
      return DisplayKey.get("Web.CloneDelinquencyPlan.Title", delinquencyPlan)
    }
    
    override property get CurrentLocation () : pcf.CloneDelinquencyPlan {
      return super.CurrentLocation as pcf.CloneDelinquencyPlan
    }
    
    property get clonedDelinquencyPlan () : entity.DelinquencyPlan {
      return getVariableValue("clonedDelinquencyPlan", 0) as entity.DelinquencyPlan
    }
    
    property set clonedDelinquencyPlan ($arg :  entity.DelinquencyPlan) {
      setVariableValue("clonedDelinquencyPlan", 0, $arg)
    }
    
    property get delinquencyPlan () : DelinquencyPlan {
      return getVariableValue("delinquencyPlan", 0) as DelinquencyPlan
    }
    
    property set delinquencyPlan ($arg :  DelinquencyPlan) {
      setVariableValue("delinquencyPlan", 0, $arg)
    }
    
    property get includeSpecs () : boolean {
      return getVariableValue("includeSpecs", 0) as java.lang.Boolean
    }
    
    property set includeSpecs ($arg :  boolean) {
      setVariableValue("includeSpecs", 0, $arg)
    }
    
    
  }
  
  
}