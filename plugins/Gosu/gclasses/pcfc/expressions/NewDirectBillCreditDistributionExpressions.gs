package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.web.payment.DirectBillPaymentView
@javax.annotation.Generated("config/web/pcf/payment/NewDirectBillCreditDistribution.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewDirectBillCreditDistributionExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/NewDirectBillCreditDistribution.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewDirectBillCreditDistributionExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Page (id=NewDirectBillCreditDistribution) at NewDirectBillCreditDistribution.pcf: line 12, column 83
    function beforeCommit_3 (pickedValue :  java.lang.Object) : void {
      paymentView.execute( CurrentLocation )
    }
    
    // 'canVisit' attribute on Page (id=NewDirectBillCreditDistribution) at NewDirectBillCreditDistribution.pcf: line 12, column 83
    static function canVisit_4 (account :  Account) : java.lang.Boolean {
      return perm.DirectBillMoneyRcvd.pmntmanproc or perm.System.pmntmanproc_tdic
    }
    
    // 'def' attribute on ScreenRef at NewDirectBillCreditDistribution.pcf: line 23, column 53
    function def_onEnter_1 (def :  pcf.EditDBPaymentScreen) : void {
      def.onEnter(paymentView, null)
    }
    
    // 'def' attribute on ScreenRef at NewDirectBillCreditDistribution.pcf: line 23, column 53
    function def_refreshVariables_2 (def :  pcf.EditDBPaymentScreen) : void {
      def.refreshVariables(paymentView, null)
    }
    
    // 'initialValue' attribute on Variable at NewDirectBillCreditDistribution.pcf: line 21, column 56
    function initialValue_0 () : gw.api.web.payment.DirectBillPaymentView {
      return createDirectBillPaymentView()
    }
    
    // 'parent' attribute on Page (id=NewDirectBillCreditDistribution) at NewDirectBillCreditDistribution.pcf: line 12, column 83
    static function parent_5 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.NewDirectBillCreditDistribution {
      return super.CurrentLocation as pcf.NewDirectBillCreditDistribution
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get paymentView () : gw.api.web.payment.DirectBillPaymentView {
      return getVariableValue("paymentView", 0) as gw.api.web.payment.DirectBillPaymentView
    }
    
    property set paymentView ($arg :  gw.api.web.payment.DirectBillPaymentView) {
      setVariableValue("paymentView", 0, $arg)
    }
    
    
        function createDirectBillPaymentView() : DirectBillPaymentView {
          var view = gw.api.web.payment.DirectBillPaymentView.createView(account, true);
          if (account.ListBill) {
            view.IncludeOnlyCriteria = DistributionLimitType.TC_OUTSTANDING
          } else {
            view.IncludeOnlyCriteria = account.DistributionLimitTypeFromPlan;
          }
          return view
        }
    
    
  }
  
  
}