package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/users/UserDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class UserDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/users/UserDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends UserDetailDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on RangeCell (id=RoleName_Cell) at UserDetailDV.pcf: line 119, column 41
    function actionAvailable_72 () : java.lang.Boolean {
      return !CurrentLocation.InEditMode
    }
    
    // 'action' attribute on RangeCell (id=RoleName_Cell) at UserDetailDV.pcf: line 119, column 41
    function action_70 () : void {
      RoleDetailPage.go(userRole.Role)
    }
    
    // 'action' attribute on RangeCell (id=RoleName_Cell) at UserDetailDV.pcf: line 119, column 41
    function action_dest_71 () : pcf.api.Destination {
      return pcf.RoleDetailPage.createDestination(userRole.Role)
    }
    
    // 'value' attribute on RangeCell (id=RoleName_Cell) at UserDetailDV.pcf: line 119, column 41
    function defaultSetter_74 (__VALUE_TO_SET :  java.lang.Object) : void {
      userRole.Role = (__VALUE_TO_SET as entity.Role)
    }
    
    // 'valueRange' attribute on RangeCell (id=RoleName_Cell) at UserDetailDV.pcf: line 119, column 41
    function valueRange_76 () : java.lang.Object {
      return allRoles
    }
    
    // 'value' attribute on RangeCell (id=RoleName_Cell) at UserDetailDV.pcf: line 119, column 41
    function valueRoot_75 () : java.lang.Object {
      return userRole
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at UserDetailDV.pcf: line 126, column 52
    function valueRoot_81 () : java.lang.Object {
      return userRole.Role
    }
    
    // 'value' attribute on RangeCell (id=RoleName_Cell) at UserDetailDV.pcf: line 119, column 41
    function value_73 () : entity.Role {
      return userRole.Role
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at UserDetailDV.pcf: line 126, column 52
    function value_80 () : java.lang.String {
      return userRole.Role.Description
    }
    
    // 'valueRange' attribute on RangeCell (id=RoleName_Cell) at UserDetailDV.pcf: line 119, column 41
    function verifyValueRangeIsAllowedType_77 ($$arg :  entity.Role[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=RoleName_Cell) at UserDetailDV.pcf: line 119, column 41
    function verifyValueRangeIsAllowedType_77 ($$arg :  gw.api.database.IQueryBeanResult<entity.Role>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=RoleName_Cell) at UserDetailDV.pcf: line 119, column 41
    function verifyValueRangeIsAllowedType_77 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=RoleName_Cell) at UserDetailDV.pcf: line 119, column 41
    function verifyValueRange_78 () : void {
      var __valueRangeArg = allRoles
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_77(__valueRangeArg)
    }
    
    property get userRole () : entity.UserRole {
      return getIteratedValue(1) as entity.UserRole
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/users/UserDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class UserDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at UserDetailDV.pcf: line 23, column 56
    function def_onEnter_1 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.onEnter(new gw.api.name.BCNameOwner(new gw.api.name.UserContactNameDelegate(user)))
    }
    
    // 'def' attribute on InputSetRef at UserDetailDV.pcf: line 23, column 56
    function def_onEnter_3 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.onEnter(new gw.api.name.BCNameOwner(new gw.api.name.UserContactNameDelegate(user)))
    }
    
    // 'def' attribute on InputSetRef at UserDetailDV.pcf: line 23, column 56
    function def_refreshVariables_2 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.refreshVariables(new gw.api.name.BCNameOwner(new gw.api.name.UserContactNameDelegate(user)))
    }
    
    // 'def' attribute on InputSetRef at UserDetailDV.pcf: line 23, column 56
    function def_refreshVariables_4 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.refreshVariables(new gw.api.name.BCNameOwner(new gw.api.name.UserContactNameDelegate(user)))
    }
    
    // 'value' attribute on ConfirmPasswordInput (id=Password_Input) at UserDetailDV.pcf: line 37, column 43
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      user.Credential.Password = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at UserDetailDV.pcf: line 46, column 65
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      user.UserLanguage = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'value' attribute on TypeKeyInput (id=RegionalFormats_Input) at UserDetailDV.pcf: line 53, column 63
    function defaultSetter_36 (__VALUE_TO_SET :  java.lang.Object) : void {
      user.UserLocale = (__VALUE_TO_SET as typekey.LocaleType)
    }
    
    // 'value' attribute on TypeKeyInput (id=DefaultCountry_Input) at UserDetailDV.pcf: line 59, column 40
    function defaultSetter_41 (__VALUE_TO_SET :  java.lang.Object) : void {
      user.UserDefaultCountry = (__VALUE_TO_SET as typekey.Country)
    }
    
    // 'value' attribute on TypeKeyInput (id=DefaultPhoneCountry_Input) at UserDetailDV.pcf: line 65, column 49
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      user.UserDefaultPhoneCountry = (__VALUE_TO_SET as typekey.PhoneCountryCode)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Active_Input) at UserDetailDV.pcf: line 73, column 30
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      user.Credential.Active = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=AccountLocked_Input) at UserDetailDV.pcf: line 79, column 30
    function defaultSetter_58 (__VALUE_TO_SET :  java.lang.Object) : void {
      user.AccountLocked = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=SessionTimeout_Input) at UserDetailDV.pcf: line 87, column 40
    function defaultSetter_64 (__VALUE_TO_SET :  java.lang.Object) : void {
      user.SessionTimeoutSecs = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=Username_Input) at UserDetailDV.pcf: line 30, column 43
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      user.Credential.UserName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on BooleanRadioInput (id=Active_Input) at UserDetailDV.pcf: line 73, column 30
    function editable_48 () : java.lang.Boolean {
      return user.Credential.canUpdateActive()
    }
    
    // 'initialValue' attribute on Variable at UserDetailDV.pcf: line 19, column 60
    function initialValue_0 () : gw.api.database.IQueryBeanResult<Role> {
      return Role.finder.allOrderedByName()
    }
    
    // 'mode' attribute on InputSetRef at UserDetailDV.pcf: line 23, column 56
    function mode_5 () : java.lang.Object {
      return gw.api.name.NameLocaleSettings.PCFMode
    }
    
    // 'required' attribute on ConfirmPasswordInput (id=Password_Input) at UserDetailDV.pcf: line 37, column 43
    function required_12 () : java.lang.Boolean {
      return isNew
    }
    
    // 'value' attribute on RangeCell (id=RoleName_Cell) at UserDetailDV.pcf: line 119, column 41
    function sortValue_68 (userRole :  entity.UserRole) : java.lang.Object {
      return userRole.Role
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at UserDetailDV.pcf: line 126, column 52
    function sortValue_69 (userRole :  entity.UserRole) : java.lang.Object {
      return userRole.Role.Description
    }
    
    // 'toAdd' attribute on RowIterator at UserDetailDV.pcf: line 107, column 43
    function toAdd_83 (userRole :  entity.UserRole) : void {
      user.addToRoles(userRole)
    }
    
    // 'toRemove' attribute on RowIterator at UserDetailDV.pcf: line 107, column 43
    function toRemove_84 (userRole :  entity.UserRole) : void {
      user.removeFromRoles(userRole)
    }
    
    // 'validationExpression' attribute on TextInput (id=Username_Input) at UserDetailDV.pcf: line 30, column 43
    function validationExpression_6 () : java.lang.Object {
      return gw.api.web.admin.UserUtil.validateUsername(user)
    }
    
    // 'validationExpression' attribute on TextInput (id=SessionTimeout_Input) at UserDetailDV.pcf: line 87, column 40
    function validationExpression_62 () : java.lang.Object {
      return (user.SessionTimeoutSecs == null || (user.SessionTimeoutSecs >= 300 /* 5 minutes */) && (user.SessionTimeoutSecs <= 604800 /* 1 wk */)) ? null : DisplayKey.get("Web.Admin.UserDetailDV.SessionTimeoutSecs.Validation")
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at UserDetailDV.pcf: line 46, column 65
    function valueRoot_31 () : java.lang.Object {
      return user
    }
    
    // 'value' attribute on TextInput (id=Username_Input) at UserDetailDV.pcf: line 30, column 43
    function valueRoot_9 () : java.lang.Object {
      return user.Credential
    }
    
    // 'value' attribute on ConfirmPasswordInput (id=Password_Input) at UserDetailDV.pcf: line 37, column 43
    function value_13 () : java.lang.String {
      return user.Credential.Password
    }
    
    // 'value' attribute on ConfirmPasswordInput (id=Password_Input) at UserDetailDV.pcf: line 37, column 43
    function value_16 () : java.lang.Object {
      return user.Credential.Password
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at UserDetailDV.pcf: line 46, column 65
    function value_29 () : typekey.LanguageType {
      return user.UserLanguage
    }
    
    // 'value' attribute on TypeKeyInput (id=RegionalFormats_Input) at UserDetailDV.pcf: line 53, column 63
    function value_35 () : typekey.LocaleType {
      return user.UserLocale
    }
    
    // 'value' attribute on TypeKeyInput (id=DefaultCountry_Input) at UserDetailDV.pcf: line 59, column 40
    function value_40 () : typekey.Country {
      return user.UserDefaultCountry
    }
    
    // 'value' attribute on TypeKeyInput (id=DefaultPhoneCountry_Input) at UserDetailDV.pcf: line 65, column 49
    function value_44 () : typekey.PhoneCountryCode {
      return user.UserDefaultPhoneCountry
    }
    
    // 'value' attribute on BooleanRadioInput (id=Active_Input) at UserDetailDV.pcf: line 73, column 30
    function value_50 () : java.lang.Boolean {
      return user.Credential.Active
    }
    
    // 'value' attribute on BooleanRadioInput (id=AccountLocked_Input) at UserDetailDV.pcf: line 79, column 30
    function value_57 () : java.lang.Boolean {
      return user.AccountLocked
    }
    
    // 'value' attribute on TextInput (id=SessionTimeout_Input) at UserDetailDV.pcf: line 87, column 40
    function value_63 () : java.lang.Integer {
      return user.SessionTimeoutSecs
    }
    
    // 'value' attribute on TextInput (id=Username_Input) at UserDetailDV.pcf: line 30, column 43
    function value_7 () : java.lang.String {
      return user.Credential.UserName
    }
    
    // 'value' attribute on RowIterator at UserDetailDV.pcf: line 107, column 43
    function value_85 () : entity.UserRole[] {
      return user.Roles
    }
    
    // 'visible' attribute on TypeKeyInput (id=Language_Input) at UserDetailDV.pcf: line 46, column 65
    function visible_28 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.canSwitchLanguage()
    }
    
    // 'visible' attribute on TypeKeyInput (id=RegionalFormats_Input) at UserDetailDV.pcf: line 53, column 63
    function visible_34 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.canSwitchLocale()
    }
    
    // 'visible' attribute on BooleanRadioInput (id=Active_Input) at UserDetailDV.pcf: line 73, column 30
    function visible_49 () : java.lang.Boolean {
      return !user.New
    }
    
    property get allRoles () : gw.api.database.IQueryBeanResult<Role> {
      return getVariableValue("allRoles", 0) as gw.api.database.IQueryBeanResult<Role>
    }
    
    property set allRoles ($arg :  gw.api.database.IQueryBeanResult<Role>) {
      setVariableValue("allRoles", 0, $arg)
    }
    
    property get isNew () : Boolean {
      return getRequireValue("isNew", 0) as Boolean
    }
    
    property set isNew ($arg :  Boolean) {
      setRequireValue("isNew", 0, $arg)
    }
    
    property get user () : User {
      return getRequireValue("user", 0) as User
    }
    
    property set user ($arg :  User) {
      setRequireValue("user", 0, $arg)
    }
    
    
  }
  
  
}