package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses tdic.util.dataloader.data.plandata.PaymentAllocationPlanData
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadPaymentAllocationPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanDataUploadPaymentAllocationPlanLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadPaymentAllocationPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PlanDataUploadPaymentAllocationPlanLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at PlanDataUploadPaymentAllocationPlanLV.pcf: line 17, column 122
    function highlighted_33 () : java.lang.Boolean {
      return (paymentAllocationPlan.Error or paymentAllocationPlan.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 23, column 46
    function label_10 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 28, column 41
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentAllocationPlan.PublicID")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 34, column 41
    function label_20 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentAllocationPlan.Name")
    }
    
    // 'label' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 39, column 39
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentAllocationPlan.EffectiveDate")
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 28, column 41
    function valueRoot_17 () : java.lang.Object {
      return paymentAllocationPlan
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 23, column 46
    function value_11 () : java.lang.String {
      return processor.getLoadStatus(paymentAllocationPlan)
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 28, column 41
    function value_16 () : java.lang.String {
      return paymentAllocationPlan.PublicID
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 34, column 41
    function value_21 () : java.lang.String {
      return paymentAllocationPlan.Name
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 39, column 39
    function value_26 () : java.util.Date {
      return paymentAllocationPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 44, column 39
    function value_30 () : java.util.Date {
      return paymentAllocationPlan.ExpirationDate
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 23, column 46
    function visible_12 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get paymentAllocationPlan () : tdic.util.dataloader.data.plandata.PaymentAllocationPlanData {
      return getIteratedValue(1) as tdic.util.dataloader.data.plandata.PaymentAllocationPlanData
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadPaymentAllocationPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanDataUploadPaymentAllocationPlanLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 28, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentAllocationPlan.PublicID")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 34, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentAllocationPlan.Name")
    }
    
    // 'label' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 39, column 39
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentAllocationPlan.EffectiveDate")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 23, column 46
    function sortValue_1 (paymentAllocationPlan :  tdic.util.dataloader.data.plandata.PaymentAllocationPlanData) : java.lang.Object {
      return processor.getLoadStatus(paymentAllocationPlan)
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 28, column 41
    function sortValue_4 (paymentAllocationPlan :  tdic.util.dataloader.data.plandata.PaymentAllocationPlanData) : java.lang.Object {
      return paymentAllocationPlan.PublicID
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 34, column 41
    function sortValue_6 (paymentAllocationPlan :  tdic.util.dataloader.data.plandata.PaymentAllocationPlanData) : java.lang.Object {
      return paymentAllocationPlan.Name
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 39, column 39
    function sortValue_8 (paymentAllocationPlan :  tdic.util.dataloader.data.plandata.PaymentAllocationPlanData) : java.lang.Object {
      return paymentAllocationPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 44, column 39
    function sortValue_9 (paymentAllocationPlan :  tdic.util.dataloader.data.plandata.PaymentAllocationPlanData) : java.lang.Object {
      return paymentAllocationPlan.ExpirationDate
    }
    
    // 'value' attribute on RowIterator (id=PaymentAllocationPlan) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 15, column 107
    function value_34 () : java.util.ArrayList<tdic.util.dataloader.data.plandata.PaymentAllocationPlanData> {
      return processor.PaymentAllocationPlanArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentAllocationPlanLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
    function getAllFilters(planData : PaymentAllocationPlanData) : String {
      var result = planData.Filters[0]  as String
      for (i in 1..5) {
        if (planData.Filters[i] != null) {
          result = result + "; " + planData.Filters[i]     
        } 
      }
      return result 
    }
    
    function getAllPayOrderTypes(planData : PaymentAllocationPlanData) : String {
      var result = planData.PayOrderTypes[0]  as String
      for (i in 1..9) {
        if (planData.PayOrderTypes[i] != null) {
          result = result + "; " + planData.PayOrderTypes[i]     
        } 
      }
      return result
    }
    
    
  }
  
  
}