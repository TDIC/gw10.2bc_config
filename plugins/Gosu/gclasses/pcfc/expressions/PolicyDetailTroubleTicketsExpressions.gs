package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailTroubleTickets.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailTroubleTicketsExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailTroubleTickets.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailTroubleTicketsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod, relatedToPolicy :  Boolean, relatedToPolicyPeriod :  Boolean) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=NewTroubleTicket) at PolicyDetailTroubleTickets.pcf: line 35, column 50
    function action_2 () : void {
      CreateTroubleTicketWizard.push(plcyPeriod)
    }
    
    // 'action' attribute on ToolbarButton (id=RelatedToPolicyOnly) at PolicyDetailTroubleTickets.pcf: line 40, column 101
    function action_4 () : void {
      relatedToPolicy = true; relatedToPolicyPeriod = false;
    }
    
    // 'action' attribute on ToolbarButton (id=RelatedToPeriodOnly) at PolicyDetailTroubleTickets.pcf: line 44, column 101
    function action_5 () : void {
      relatedToPolicy = false; relatedToPolicyPeriod = true;
    }
    
    // 'action' attribute on ToolbarButton (id=All) at PolicyDetailTroubleTickets.pcf: line 48, column 85
    function action_6 () : void {
      relatedToPolicy = true; relatedToPolicyPeriod = true;
    }
    
    // 'action' attribute on ToolbarButton (id=NewTroubleTicket) at PolicyDetailTroubleTickets.pcf: line 35, column 50
    function action_dest_3 () : pcf.api.Destination {
      return pcf.CreateTroubleTicketWizard.createDestination(plcyPeriod)
    }
    
    // 'canVisit' attribute on Page (id=PolicyDetailTroubleTickets) at PolicyDetailTroubleTickets.pcf: line 9, column 78
    static function canVisit_9 (plcyPeriod :  PolicyPeriod, relatedToPolicy :  Boolean, relatedToPolicyPeriod :  Boolean) : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcyttktview and not plcyPeriod.Archived
    }
    
    // 'def' attribute on PanelRef at PolicyDetailTroubleTickets.pcf: line 29, column 64
    function def_onEnter_7 (def :  pcf.TroubleTicketsLV) : void {
      def.onEnter(troubleTickets?.toTypedArray())
    }
    
    // 'def' attribute on PanelRef at PolicyDetailTroubleTickets.pcf: line 29, column 64
    function def_refreshVariables_8 (def :  pcf.TroubleTicketsLV) : void {
      def.refreshVariables(troubleTickets?.toTypedArray())
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailTroubleTickets.pcf: line 19, column 58
    function initialValue_0 () : java.util.List<entity.TroubleTicket> {
      return getAllTroubleTicket()
    }
    
    // Page (id=PolicyDetailTroubleTickets) at PolicyDetailTroubleTickets.pcf: line 9, column 78
    static function parent_10 (plcyPeriod :  PolicyPeriod, relatedToPolicy :  Boolean, relatedToPolicyPeriod :  Boolean) : pcf.api.Destination {
      return pcf.PolicyGroup.createDestination(plcyPeriod)
    }
    
    // 'visible' attribute on ToolbarButton (id=NewTroubleTicket) at PolicyDetailTroubleTickets.pcf: line 35, column 50
    function visible_1 () : java.lang.Boolean {
      return perm.TroubleTicket.create
    }
    
    override property get CurrentLocation () : pcf.PolicyDetailTroubleTickets {
      return super.CurrentLocation as pcf.PolicyDetailTroubleTickets
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 0, $arg)
    }
    
    property get relatedToPolicy () : Boolean {
      return getVariableValue("relatedToPolicy", 0) as Boolean
    }
    
    property set relatedToPolicy ($arg :  Boolean) {
      setVariableValue("relatedToPolicy", 0, $arg)
    }
    
    property get relatedToPolicyPeriod () : Boolean {
      return getVariableValue("relatedToPolicyPeriod", 0) as Boolean
    }
    
    property set relatedToPolicyPeriod ($arg :  Boolean) {
      setVariableValue("relatedToPolicyPeriod", 0, $arg)
    }
    
    property get troubleTickets () : java.util.List<entity.TroubleTicket> {
      return getVariableValue("troubleTickets", 0) as java.util.List<entity.TroubleTicket>
    }
    
    property set troubleTickets ($arg :  java.util.List<entity.TroubleTicket>) {
      setVariableValue("troubleTickets", 0, $arg)
    }
    
    
    function getAllTroubleTicket() : java.util.List<TroubleTicket> {
      var results = new java.util.ArrayList<TroubleTicket>();
      if(relatedToPolicy) {
        results.addAll(plcyPeriod.Policy.TroubleTickets?.toList());
        if(relatedToPolicyPeriod) {
          for (tt in plcyPeriod.TroubleTickets) {
            if(not results.contains(tt)) {
              results.add(tt);
            }
          }
        }
      }
      else if (relatedToPolicyPeriod) {
        results.addAll(plcyPeriod.TroubleTickets?.toList())
      }
      else {
        throw "Must specified trouble ticket related to policy or policy period";
      }
      
      return results
    }
        
    
    
  }
  
  
}