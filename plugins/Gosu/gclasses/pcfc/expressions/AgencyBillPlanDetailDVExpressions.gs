package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/agencybill/AgencyBillPlanDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillPlanDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/agencybill/AgencyBillPlanDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillPlanDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=ProducerWriteoffThresholdDefaults) at AgencyBillPlanDetailDV.pcf: line 228, column 49
    function def_onEnter_187 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(agencyBillPlan, AgencyBillPlan#ProducerWriteoffThresholdDefaults.PropertyInfo, DisplayKey.get("Web.AgencyBillPlanDetailDV.ProducerWriteoffThreshold"), true, false, null)
    }
    
    // 'def' attribute on InputSetRef (id=ClearCommissionThresholdDefaults) at AgencyBillPlanDetailDV.pcf: line 243, column 56
    function def_onEnter_196 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(agencyBillPlan, AgencyBillPlan#ClearCommissionThresholdDefaults.PropertyInfo, DisplayKey.get("Web.AgencyBillPlanDetailDV.CommissionClearedThreshold"), agencyBillPlan.LowCommissionCleared, false, null)
    }
    
    // 'def' attribute on InputSetRef (id=ClearGrossThresholdDefaults) at AgencyBillPlanDetailDV.pcf: line 255, column 51
    function def_onEnter_205 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(agencyBillPlan, AgencyBillPlan#ClearGrossThresholdDefaults.PropertyInfo, DisplayKey.get("Web.AgencyBillPlanDetailDV.GrossClearedThreshold"), agencyBillPlan.LowGrossCleared, false, null)
    }
    
    // 'def' attribute on InputSetRef at AgencyBillPlanDetailDV.pcf: line 51, column 58
    function def_onEnter_31 (def :  pcf.PlanMultiCurrencyInputSet) : void {
      def.onEnter(agencyBillPlan)
    }
    
    // 'def' attribute on InputSetRef (id=NetThresholdForSuppressingStmtDefaults) at AgencyBillPlanDetailDV.pcf: line 131, column 121
    function def_onEnter_98 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(agencyBillPlan, AgencyBillPlan#NetThresholdForSuppressingStmtDefaults.PropertyInfo, DisplayKey.get("Web.AgencyBillPlanDetailDV.SuppressStatementThreshold"), agencyBillPlan.StatementsWithLowNetSuppressed, false, null)
    }
    
    // 'def' attribute on InputSetRef (id=ProducerWriteoffThresholdDefaults) at AgencyBillPlanDetailDV.pcf: line 228, column 49
    function def_refreshVariables_188 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(agencyBillPlan, AgencyBillPlan#ProducerWriteoffThresholdDefaults.PropertyInfo, DisplayKey.get("Web.AgencyBillPlanDetailDV.ProducerWriteoffThreshold"), true, false, null)
    }
    
    // 'def' attribute on InputSetRef (id=ClearCommissionThresholdDefaults) at AgencyBillPlanDetailDV.pcf: line 243, column 56
    function def_refreshVariables_197 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(agencyBillPlan, AgencyBillPlan#ClearCommissionThresholdDefaults.PropertyInfo, DisplayKey.get("Web.AgencyBillPlanDetailDV.CommissionClearedThreshold"), agencyBillPlan.LowCommissionCleared, false, null)
    }
    
    // 'def' attribute on InputSetRef (id=ClearGrossThresholdDefaults) at AgencyBillPlanDetailDV.pcf: line 255, column 51
    function def_refreshVariables_206 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(agencyBillPlan, AgencyBillPlan#ClearGrossThresholdDefaults.PropertyInfo, DisplayKey.get("Web.AgencyBillPlanDetailDV.GrossClearedThreshold"), agencyBillPlan.LowGrossCleared, false, null)
    }
    
    // 'def' attribute on InputSetRef at AgencyBillPlanDetailDV.pcf: line 51, column 58
    function def_refreshVariables_32 (def :  pcf.PlanMultiCurrencyInputSet) : void {
      def.refreshVariables(agencyBillPlan)
    }
    
    // 'def' attribute on InputSetRef (id=NetThresholdForSuppressingStmtDefaults) at AgencyBillPlanDetailDV.pcf: line 131, column 121
    function def_refreshVariables_99 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(agencyBillPlan, AgencyBillPlan#NetThresholdForSuppressingStmtDefaults.PropertyInfo, DisplayKey.get("Web.AgencyBillPlanDetailDV.SuppressStatementThreshold"), agencyBillPlan.StatementsWithLowNetSuppressed, false, null)
    }
    
    // 'value' attribute on RangeInput (id=WorkflowType_Input) at AgencyBillPlanDetailDV.pcf: line 37, column 39
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.WorkflowPlan = (__VALUE_TO_SET as typekey.Workflow)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SendReminder_Input) at AgencyBillPlanDetailDV.pcf: line 140, column 61
    function defaultSetter_102 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.ReminderSentIfPromiseNotRcvd = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=SendReminderDays_Input) at AgencyBillPlanDetailDV.pcf: line 151, column 64
    function defaultSetter_111 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.DaysUntilPromiseReminderSent = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=PromiseDueDays_Input) at AgencyBillPlanDetailDV.pcf: line 159, column 40
    function defaultSetter_120 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.PromiseDueInDays = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on BooleanRadioInput (id=GeneratePromisePastDueException_Input) at AgencyBillPlanDetailDV.pcf: line 165, column 62
    function defaultSetter_127 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.ExceptionIfPromiseNotReceived = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SendPromiseExceptions_Input) at AgencyBillPlanDetailDV.pcf: line 173, column 55
    function defaultSetter_133 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.PromiseExceptionsSent = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=AutoProcess_Input) at AgencyBillPlanDetailDV.pcf: line 183, column 63
    function defaultSetter_139 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.AutoProcessWhenPaymentMatches = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SendPaymentExceptions_Input) at AgencyBillPlanDetailDV.pcf: line 189, column 55
    function defaultSetter_145 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.PaymentExceptionsSent = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SendDunningOne_Input) at AgencyBillPlanDetailDV.pcf: line 195, column 58
    function defaultSetter_151 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.FirstDunningSentIfNotPaid = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=SendDunningOneDays_Input) at AgencyBillPlanDetailDV.pcf: line 206, column 61
    function defaultSetter_160 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.DaysUntilFirstDunningSent = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SendDunningTwo_Input) at AgencyBillPlanDetailDV.pcf: line 214, column 60
    function defaultSetter_170 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.SecondDunningSentIfNotPaid = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=SendDunningTwoDays_Input) at AgencyBillPlanDetailDV.pcf: line 225, column 114
    function defaultSetter_181 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.DaysUntilSecondDunningSent = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at AgencyBillPlanDetailDV.pcf: line 43, column 47
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.EffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on BooleanRadioInput (id=CommissionCleared_Input) at AgencyBillPlanDetailDV.pcf: line 237, column 53
    function defaultSetter_191 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.LowCommissionCleared = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=GrossCleared_Input) at AgencyBillPlanDetailDV.pcf: line 249, column 48
    function defaultSetter_200 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.LowGrossCleared = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=ClearingLogicTarget_Input) at AgencyBillPlanDetailDV.pcf: line 261, column 50
    function defaultSetter_209 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.ClearingLogicTarget = (__VALUE_TO_SET as typekey.ClearingLogicTarget)
    }
    
    // 'value' attribute on BooleanRadioInput (id=OnItemMoved_Input) at AgencyBillPlanDetailDV.pcf: line 270, column 62
    function defaultSetter_215 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.CreateOffsetsOnBilledInvoices = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=OnPaymentScheduleChange_Input) at AgencyBillPlanDetailDV.pcf: line 278, column 60
    function defaultSetter_221 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.PmntSchdChngOffsetsOnBilled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at AgencyBillPlanDetailDV.pcf: line 49, column 48
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.ExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=CycleCloseDOMLogic_Input) at AgencyBillPlanDetailDV.pcf: line 61, column 45
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.CycleCloseDayOfMonthLogic = (__VALUE_TO_SET as typekey.DayOfMonthLogic)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AgencyBillPlanDetailDV.pcf: line 27, column 37
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=CycleCloseDOM_Input) at AgencyBillPlanDetailDV.pcf: line 72, column 80
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.CycleCloseDayOfMonth = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=PaymentTermsDays_Input) at AgencyBillPlanDetailDV.pcf: line 83, column 40
    function defaultSetter_53 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.PaymentTermsInDays = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on BooleanRadioInput (id=GeneratePastDueException_Input) at AgencyBillPlanDetailDV.pcf: line 89, column 62
    function defaultSetter_60 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.ExceptionForPastDueStatement = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SendStatement_Input) at AgencyBillPlanDetailDV.pcf: line 98, column 61
    function defaultSetter_66 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.StatementSentAfterCycleClose = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=SendStatementDays_Input) at AgencyBillPlanDetailDV.pcf: line 109, column 64
    function defaultSetter_75 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.DaysAfterCycleCloseToSendStmnt = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SnapshotNonPastDueItems_Input) at AgencyBillPlanDetailDV.pcf: line 116, column 63
    function defaultSetter_84 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.SnapshotNonPastDueItems = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SupressStatement_Input) at AgencyBillPlanDetailDV.pcf: line 125, column 63
    function defaultSetter_92 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillPlan.StatementsWithLowNetSuppressed = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'editable' attribute on TextInput (id=Name_Input) at AgencyBillPlanDetailDV.pcf: line 27, column 37
    function editable_2 () : java.lang.Boolean {
      return planNotInUse
    }
    
    // 'initialValue' attribute on Variable at AgencyBillPlanDetailDV.pcf: line 14, column 23
    function initialValue_0 () : Boolean {
      return not agencyBillPlan.InUse
    }
    
    // 'initialValue' attribute on Variable at AgencyBillPlanDetailDV.pcf: line 18, column 23
    function initialValue_1 () : boolean {
      return gw.api.util.LocaleUtil.getAllLanguages().Count > 1
    }
    
    // 'validationExpression' attribute on TextInput (id=SendReminderDays_Input) at AgencyBillPlanDetailDV.pcf: line 151, column 64
    function validationExpression_107 () : java.lang.Object {
      return (agencyBillPlan.DaysUntilPromiseReminderSent >= 0) ? null : DisplayKey.get("Web.AgencyBillPlanDetailDV.PositiveNumberError")
    }
    
    // 'validationExpression' attribute on TextInput (id=PromiseDueDays_Input) at AgencyBillPlanDetailDV.pcf: line 159, column 40
    function validationExpression_118 () : java.lang.Object {
      return (agencyBillPlan.PromiseDueInDays >= 0) ? null : DisplayKey.get("Web.AgencyBillPlanDetailDV.PositiveNumberError")
    }
    
    // 'validationExpression' attribute on TextInput (id=SendDunningOneDays_Input) at AgencyBillPlanDetailDV.pcf: line 206, column 61
    function validationExpression_156 () : java.lang.Object {
      return (agencyBillPlan.DaysUntilFirstDunningSent >= 0) ? null : DisplayKey.get("Web.AgencyBillPlanDetailDV.PositiveNumberError")
    }
    
    // 'validationExpression' attribute on BooleanRadioInput (id=SendDunningTwo_Input) at AgencyBillPlanDetailDV.pcf: line 214, column 60
    function validationExpression_167 () : java.lang.Object {
      return (agencyBillPlan.FirstDunningSentIfNotPaid) ? null : DisplayKey.get("Web.AgencyBillPlanDetailDV.SecondDunningCannotBeTrueWithoutFirstDunning")
    }
    
    // 'validationExpression' attribute on TextInput (id=SendDunningTwoDays_Input) at AgencyBillPlanDetailDV.pcf: line 225, column 114
    function validationExpression_177 () : java.lang.Object {
      return validateSendDunningTwoDays()
    }
    
    // 'validationExpression' attribute on DateInput (id=ExpirationDate_Input) at AgencyBillPlanDetailDV.pcf: line 49, column 48
    function validationExpression_24 () : java.lang.Object {
      return agencyBillPlan.hasValidExpirationDate() ? null : DisplayKey.get("Web.Plan.ExpirationDate.ValidationError")
    }
    
    // 'validationExpression' attribute on TextInput (id=CycleCloseDOM_Input) at AgencyBillPlanDetailDV.pcf: line 72, column 80
    function validationExpression_40 () : java.lang.Object {
      return isValidDayOfMonth(agencyBillPlan.CycleCloseDayOfMonth) ? null : DisplayKey.get("Web.AgencyBillPlanDetailDV.DayOfMonthError")
    }
    
    // 'validationExpression' attribute on TextInput (id=PaymentTermsDays_Input) at AgencyBillPlanDetailDV.pcf: line 83, column 40
    function validationExpression_51 () : java.lang.Object {
      return (agencyBillPlan.PaymentTermsInDays > 0) ? null : DisplayKey.get("Web.AgencyBillPlanDetailDV.GreaterThanZeroError")
    }
    
    // 'validationExpression' attribute on TextInput (id=SendStatementDays_Input) at AgencyBillPlanDetailDV.pcf: line 109, column 64
    function validationExpression_71 () : java.lang.Object {
      return (agencyBillPlan.DaysAfterCycleCloseToSendStmnt >= 0) ? null : DisplayKey.get("Web.AgencyBillPlanDetailDV.PositiveNumberError")
    }
    
    // 'valueRange' attribute on RangeInput (id=WorkflowType_Input) at AgencyBillPlanDetailDV.pcf: line 37, column 39
    function valueRange_12 () : java.lang.Object {
      return gw.api.web.producer.agencybill.AgencyBillPlanUtil.getAgencyBillWorkflows()
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AgencyBillPlanDetailDV.pcf: line 27, column 37
    function valueRoot_5 () : java.lang.Object {
      return agencyBillPlan
    }
    
    // 'value' attribute on BooleanRadioInput (id=SendReminder_Input) at AgencyBillPlanDetailDV.pcf: line 140, column 61
    function value_101 () : java.lang.Boolean {
      return agencyBillPlan.ReminderSentIfPromiseNotRcvd
    }
    
    // 'value' attribute on TextInput (id=SendReminderDays_Input) at AgencyBillPlanDetailDV.pcf: line 151, column 64
    function value_110 () : java.lang.Integer {
      return agencyBillPlan.DaysUntilPromiseReminderSent
    }
    
    // 'value' attribute on TextInput (id=PromiseDueDays_Input) at AgencyBillPlanDetailDV.pcf: line 159, column 40
    function value_119 () : java.lang.Integer {
      return agencyBillPlan.PromiseDueInDays
    }
    
    // 'value' attribute on BooleanRadioInput (id=GeneratePromisePastDueException_Input) at AgencyBillPlanDetailDV.pcf: line 165, column 62
    function value_126 () : java.lang.Boolean {
      return agencyBillPlan.ExceptionIfPromiseNotReceived
    }
    
    // 'value' attribute on BooleanRadioInput (id=SendPromiseExceptions_Input) at AgencyBillPlanDetailDV.pcf: line 173, column 55
    function value_132 () : java.lang.Boolean {
      return agencyBillPlan.PromiseExceptionsSent
    }
    
    // 'value' attribute on BooleanRadioInput (id=AutoProcess_Input) at AgencyBillPlanDetailDV.pcf: line 183, column 63
    function value_138 () : java.lang.Boolean {
      return agencyBillPlan.AutoProcessWhenPaymentMatches
    }
    
    // 'value' attribute on BooleanRadioInput (id=SendPaymentExceptions_Input) at AgencyBillPlanDetailDV.pcf: line 189, column 55
    function value_144 () : java.lang.Boolean {
      return agencyBillPlan.PaymentExceptionsSent
    }
    
    // 'value' attribute on BooleanRadioInput (id=SendDunningOne_Input) at AgencyBillPlanDetailDV.pcf: line 195, column 58
    function value_150 () : java.lang.Boolean {
      return agencyBillPlan.FirstDunningSentIfNotPaid
    }
    
    // 'value' attribute on TextInput (id=SendDunningOneDays_Input) at AgencyBillPlanDetailDV.pcf: line 206, column 61
    function value_159 () : java.lang.Integer {
      return agencyBillPlan.DaysUntilFirstDunningSent
    }
    
    // 'value' attribute on BooleanRadioInput (id=SendDunningTwo_Input) at AgencyBillPlanDetailDV.pcf: line 214, column 60
    function value_169 () : java.lang.Boolean {
      return agencyBillPlan.SecondDunningSentIfNotPaid
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at AgencyBillPlanDetailDV.pcf: line 43, column 47
    function value_18 () : java.util.Date {
      return agencyBillPlan.EffectiveDate
    }
    
    // 'value' attribute on TextInput (id=SendDunningTwoDays_Input) at AgencyBillPlanDetailDV.pcf: line 225, column 114
    function value_180 () : java.lang.Integer {
      return agencyBillPlan.DaysUntilSecondDunningSent
    }
    
    // 'value' attribute on BooleanRadioInput (id=CommissionCleared_Input) at AgencyBillPlanDetailDV.pcf: line 237, column 53
    function value_190 () : java.lang.Boolean {
      return agencyBillPlan.LowCommissionCleared
    }
    
    // 'value' attribute on BooleanRadioInput (id=GrossCleared_Input) at AgencyBillPlanDetailDV.pcf: line 249, column 48
    function value_199 () : java.lang.Boolean {
      return agencyBillPlan.LowGrossCleared
    }
    
    // 'value' attribute on TypeKeyInput (id=ClearingLogicTarget_Input) at AgencyBillPlanDetailDV.pcf: line 261, column 50
    function value_208 () : typekey.ClearingLogicTarget {
      return agencyBillPlan.ClearingLogicTarget
    }
    
    // 'value' attribute on BooleanRadioInput (id=OnItemMoved_Input) at AgencyBillPlanDetailDV.pcf: line 270, column 62
    function value_214 () : java.lang.Boolean {
      return agencyBillPlan.CreateOffsetsOnBilledInvoices
    }
    
    // 'value' attribute on BooleanRadioInput (id=OnPaymentScheduleChange_Input) at AgencyBillPlanDetailDV.pcf: line 278, column 60
    function value_220 () : java.lang.Boolean {
      return agencyBillPlan.PmntSchdChngOffsetsOnBilled
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at AgencyBillPlanDetailDV.pcf: line 49, column 48
    function value_25 () : java.util.Date {
      return agencyBillPlan.ExpirationDate
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AgencyBillPlanDetailDV.pcf: line 27, column 37
    function value_3 () : java.lang.String {
      return agencyBillPlan.Name
    }
    
    // 'value' attribute on TypeKeyInput (id=CycleCloseDOMLogic_Input) at AgencyBillPlanDetailDV.pcf: line 61, column 45
    function value_34 () : typekey.DayOfMonthLogic {
      return agencyBillPlan.CycleCloseDayOfMonthLogic
    }
    
    // 'value' attribute on TextInput (id=CycleCloseDOM_Input) at AgencyBillPlanDetailDV.pcf: line 72, column 80
    function value_43 () : java.lang.Integer {
      return agencyBillPlan.CycleCloseDayOfMonth
    }
    
    // 'value' attribute on TextInput (id=PaymentTermsDays_Input) at AgencyBillPlanDetailDV.pcf: line 83, column 40
    function value_52 () : java.lang.Integer {
      return agencyBillPlan.PaymentTermsInDays
    }
    
    // 'value' attribute on BooleanRadioInput (id=GeneratePastDueException_Input) at AgencyBillPlanDetailDV.pcf: line 89, column 62
    function value_59 () : java.lang.Boolean {
      return agencyBillPlan.ExceptionForPastDueStatement
    }
    
    // 'value' attribute on BooleanRadioInput (id=SendStatement_Input) at AgencyBillPlanDetailDV.pcf: line 98, column 61
    function value_65 () : java.lang.Boolean {
      return agencyBillPlan.StatementSentAfterCycleClose
    }
    
    // 'value' attribute on TextInput (id=SendStatementDays_Input) at AgencyBillPlanDetailDV.pcf: line 109, column 64
    function value_74 () : java.lang.Integer {
      return agencyBillPlan.DaysAfterCycleCloseToSendStmnt
    }
    
    // 'value' attribute on BooleanRadioInput (id=SnapshotNonPastDueItems_Input) at AgencyBillPlanDetailDV.pcf: line 116, column 63
    function value_83 () : java.lang.Boolean {
      return agencyBillPlan.SnapshotNonPastDueItems
    }
    
    // 'value' attribute on RangeInput (id=WorkflowType_Input) at AgencyBillPlanDetailDV.pcf: line 37, column 39
    function value_9 () : typekey.Workflow {
      return agencyBillPlan.WorkflowPlan
    }
    
    // 'value' attribute on BooleanRadioInput (id=SupressStatement_Input) at AgencyBillPlanDetailDV.pcf: line 125, column 63
    function value_91 () : java.lang.Boolean {
      return agencyBillPlan.StatementsWithLowNetSuppressed
    }
    
    // 'valueRange' attribute on RangeInput (id=WorkflowType_Input) at AgencyBillPlanDetailDV.pcf: line 37, column 39
    function verifyValueRangeIsAllowedType_13 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WorkflowType_Input) at AgencyBillPlanDetailDV.pcf: line 37, column 39
    function verifyValueRangeIsAllowedType_13 ($$arg :  typekey.Workflow[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WorkflowType_Input) at AgencyBillPlanDetailDV.pcf: line 37, column 39
    function verifyValueRange_14 () : void {
      var __valueRangeArg = gw.api.web.producer.agencybill.AgencyBillPlanUtil.getAgencyBillWorkflows()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_13(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=SendDunningTwoDays_Input) at AgencyBillPlanDetailDV.pcf: line 225, column 114
    function visible_178 () : java.lang.Boolean {
      return agencyBillPlan.FirstDunningSentIfNotPaid && agencyBillPlan.SecondDunningSentIfNotPaid
    }
    
    // 'visible' attribute on TextInput (id=CycleCloseDOM_Input) at AgencyBillPlanDetailDV.pcf: line 72, column 80
    function visible_41 () : java.lang.Boolean {
      return requireDayOfMonth(agencyBillPlan.CycleCloseDayOfMonthLogic)
    }
    
    // 'visible' attribute on InputSetRef (id=NetThresholdForSuppressingStmtDefaults) at AgencyBillPlanDetailDV.pcf: line 131, column 121
    function visible_97 () : java.lang.Boolean {
      return agencyBillPlan.StatementSentAfterCycleClose && agencyBillPlan.StatementsWithLowNetSuppressed
    }
    
    property get agencyBillPlan () : AgencyBillPlan {
      return getRequireValue("agencyBillPlan", 0) as AgencyBillPlan
    }
    
    property set agencyBillPlan ($arg :  AgencyBillPlan) {
      setRequireValue("agencyBillPlan", 0, $arg)
    }
    
    property get hasMultipleLanguages () : boolean {
      return getVariableValue("hasMultipleLanguages", 0) as java.lang.Boolean
    }
    
    property set hasMultipleLanguages ($arg :  boolean) {
      setVariableValue("hasMultipleLanguages", 0, $arg)
    }
    
    property get planNotInUse () : Boolean {
      return getVariableValue("planNotInUse", 0) as Boolean
    }
    
    property set planNotInUse ($arg :  Boolean) {
      setVariableValue("planNotInUse", 0, $arg)
    }
    
    function validateSendDunningTwoDays() : String {
      if (!agencyBillPlan.FirstDunningSentIfNotPaid) {
        return DisplayKey.get("Web.AgencyBillPlanDetailDV.SecondDunningCannotBeTrueWithoutFirstDunning");
      } else if (agencyBillPlan.DaysUntilSecondDunningSent < agencyBillPlan.DaysUntilFirstDunningSent) {
        return DisplayKey.get("Web.AgencyBillPlanDetailDV.SecondDunningMustBeGreaterThanFirstDunning");
      } else {
        return null;
      }
    }
    
    public static function isValidDayOfMonth(x : java.lang.Double) : boolean {
      return x > 0 and x < 32
    }
    
    function requireDayOfMonth(x : DayOfMonthLogic) : boolean {
      return x != DayOfMonthLogic.TC_LASTBUSINESSDAY and x != DayOfMonthLogic.TC_FIRSTBUSINESSDAY
    }
    
    
  }
  
  
}