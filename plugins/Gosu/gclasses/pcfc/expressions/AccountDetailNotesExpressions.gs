package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailNotes.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailNotesExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailNotes.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailNotesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account, isClearBundle :  Boolean) : int {
      return 0
    }
    
    static function __constructorIndex (account :  Account, isClearBundle :  Boolean, latestNote :  Note) : int {
      return 1
    }
    
    // 'afterReturnFromPopup' attribute on Page (id=AccountDetailNotes) at AccountDetailNotes.pcf: line 12, column 70
    function afterReturnFromPopup_65 (popupCommitted :  boolean) : void {
      if (popupCommitted)     {account.addHistoryFromGosu( java.util.Date.CurrentDate, HistoryEventType.TC_NOTESUPDATED, DisplayKey.get("Java.AccountHistory.NotesUpdated"), null as Transaction, null, true );     account.Bundle.commit()}
    }
    
    // 'canVisit' attribute on Page (id=AccountDetailNotes) at AccountDetailNotes.pcf: line 12, column 70
    static function canVisit_66 (account :  Account, isClearBundle :  Boolean, latestNote :  Note) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctnoteview
    }
    
    // 'parent' attribute on Page (id=AccountDetailNotes) at AccountDetailNotes.pcf: line 12, column 70
    static function parent_67 (account :  Account, isClearBundle :  Boolean, latestNote :  Note) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.AccountDetailNotes {
      return super.CurrentLocation as pcf.AccountDetailNotes
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get isClearBundle () : Boolean {
      return getVariableValue("isClearBundle", 0) as Boolean
    }
    
    property set isClearBundle ($arg :  Boolean) {
      setVariableValue("isClearBundle", 0, $arg)
    }
    
    property get latestNote () : Note {
      return getVariableValue("latestNote", 0) as Note
    }
    
    property set latestNote ($arg :  Note) {
      setVariableValue("latestNote", 0, $arg)
    }
    
    function createNoteSearchCriteria() : NoteSearchCriteria {
      var nsc = new NoteSearchCriteria()
      nsc.Account = account
      if (latestNote != null) {
        var dcc = new DateCriterionChoice();
        dcc.SearchType = SearchObjectType.TC_NOTE
        dcc.DateSearchType = DateSearchType.TC_ENTEREDRANGE
        dcc.StartDate = latestNote.AuthoringDate.addWeeks(-1)
        nsc.DateCriterionChoice = dcc
      }
      return nsc
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailNotes.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends AccountDetailNotesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at AccountDetailNotes.pcf: line 67, column 63
    function available_15 () : java.lang.Boolean {
      return NoteSearchCriteria.DateCriterionChoice.DateSearchType == DateSearchType.TC_FROMLIST
    }
    
    // 'available' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at AccountDetailNotes.pcf: line 67, column 63
    function available_24 () : java.lang.Boolean {
      return NoteSearchCriteria.DateCriterionChoice.DateSearchType == DateSearchType.TC_ENTEREDRANGE
    }
    
    // 'cachingEnabled' attribute on SearchPanel at AccountDetailNotes.pcf: line 37, column 74
    function cachingEnabled_61 () : java.lang.Boolean {
      return latestNote == null
    }
    
    // 'def' attribute on InputSetRef at AccountDetailNotes.pcf: line 95, column 47
    function def_onEnter_57 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on PanelRef at AccountDetailNotes.pcf: line 99, column 40
    function def_onEnter_59 (def :  pcf.NotesLV) : void {
      def.onEnter(notes,account)
    }
    
    // 'def' attribute on InputSetRef at AccountDetailNotes.pcf: line 95, column 47
    function def_refreshVariables_58 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'def' attribute on PanelRef at AccountDetailNotes.pcf: line 99, column 40
    function def_refreshVariables_60 (def :  pcf.NotesLV) : void {
      def.refreshVariables(notes,account)
    }
    
    // 'value' attribute on TextInput (id=TextSearch_Input) at AccountDetailNotes.pcf: line 45, column 48
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.Text = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=RelatedToSearch_Input) at AccountDetailNotes.pcf: line 60, column 46
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.RelatedToEntity = (__VALUE_TO_SET as typekey.RelatedTo)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at AccountDetailNotes.pcf: line 67, column 63
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.DateCriterionChoice.DateRangeChoice = (__VALUE_TO_SET as typekey.DateRangeChoiceType)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at AccountDetailNotes.pcf: line 67, column 63
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.DateCriterionChoice.DateSearchType = (__VALUE_TO_SET as typekey.DateSearchType)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at AccountDetailNotes.pcf: line 67, column 63
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.DateCriterionChoice.StartDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at AccountDetailNotes.pcf: line 67, column 63
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.DateCriterionChoice.EndDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at AccountDetailNotes.pcf: line 67, column 63
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.DateCriterionChoice = (__VALUE_TO_SET as entity.DateCriterionChoice)
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at AccountDetailNotes.pcf: line 75, column 73
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.Language = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'value' attribute on RangeInput (id=Author_Input) at AccountDetailNotes.pcf: line 53, column 40
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.Author = (__VALUE_TO_SET as entity.User)
    }
    
    // 'value' attribute on TypeKeyInput (id=SortByOption_Input) at AccountDetailNotes.pcf: line 84, column 48
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.SortBy = (__VALUE_TO_SET as typekey.SortByRange)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SortAscending_Input) at AccountDetailNotes.pcf: line 91, column 57
    function defaultSetter_54 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.SortAscending = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'searchCriteria' attribute on SearchPanel at AccountDetailNotes.pcf: line 37, column 74
    function searchCriteria_63 () : entity.NoteSearchCriteria {
      return createNoteSearchCriteria()
    }
    
    // 'searchOnEnter' attribute on SearchPanel at AccountDetailNotes.pcf: line 37, column 74
    function searchOnEnter_64 () : java.lang.Boolean {
      return latestNote != null
    }
    
    // 'search' attribute on SearchPanel at AccountDetailNotes.pcf: line 37, column 74
    function search_62 () : java.lang.Object {
      return NoteSearchCriteria.performSearch(isClearBundle)
    }
    
    // 'valueRange' attribute on RangeInput (id=Author_Input) at AccountDetailNotes.pcf: line 53, column 40
    function valueRange_7 () : java.lang.Object {
      return account.NoteAuthors
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at AccountDetailNotes.pcf: line 67, column 63
    function valueRoot_18 () : java.lang.Object {
      return NoteSearchCriteria.DateCriterionChoice
    }
    
    // 'value' attribute on TextInput (id=TextSearch_Input) at AccountDetailNotes.pcf: line 45, column 48
    function valueRoot_2 () : java.lang.Object {
      return NoteSearchCriteria
    }
    
    // 'value' attribute on TextInput (id=TextSearch_Input) at AccountDetailNotes.pcf: line 45, column 48
    function value_0 () : java.lang.String {
      return NoteSearchCriteria.Text
    }
    
    // 'value' attribute on TypeKeyInput (id=RelatedToSearch_Input) at AccountDetailNotes.pcf: line 60, column 46
    function value_11 () : typekey.RelatedTo {
      return NoteSearchCriteria.RelatedToEntity
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at AccountDetailNotes.pcf: line 67, column 63
    function value_16 () : typekey.DateRangeChoiceType {
      return NoteSearchCriteria.DateCriterionChoice.DateRangeChoice
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at AccountDetailNotes.pcf: line 67, column 63
    function value_20 () : java.lang.Object {
      return NoteSearchCriteria.DateCriterionChoice.DateRangeChoice
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at AccountDetailNotes.pcf: line 67, column 63
    function value_21 () : typekey.DateSearchType {
      return NoteSearchCriteria.DateCriterionChoice.DateSearchType
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at AccountDetailNotes.pcf: line 67, column 63
    function value_25 () : java.util.Date {
      return NoteSearchCriteria.DateCriterionChoice.StartDate
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at AccountDetailNotes.pcf: line 67, column 63
    function value_31 () : java.util.Date {
      return NoteSearchCriteria.DateCriterionChoice.EndDate
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at AccountDetailNotes.pcf: line 67, column 63
    function value_39 () : entity.DateCriterionChoice {
      return NoteSearchCriteria.DateCriterionChoice
    }
    
    // 'value' attribute on RangeInput (id=Author_Input) at AccountDetailNotes.pcf: line 53, column 40
    function value_4 () : entity.User {
      return NoteSearchCriteria.Author
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at AccountDetailNotes.pcf: line 75, column 73
    function value_44 () : typekey.LanguageType {
      return NoteSearchCriteria.Language
    }
    
    // 'value' attribute on TypeKeyInput (id=SortByOption_Input) at AccountDetailNotes.pcf: line 84, column 48
    function value_49 () : typekey.SortByRange {
      return NoteSearchCriteria.SortBy
    }
    
    // 'value' attribute on BooleanRadioInput (id=SortAscending_Input) at AccountDetailNotes.pcf: line 91, column 57
    function value_53 () : java.lang.Boolean {
      return NoteSearchCriteria.SortAscending
    }
    
    // 'valueRange' attribute on RangeInput (id=Author_Input) at AccountDetailNotes.pcf: line 53, column 40
    function verifyValueRangeIsAllowedType_8 ($$arg :  entity.User[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Author_Input) at AccountDetailNotes.pcf: line 53, column 40
    function verifyValueRangeIsAllowedType_8 ($$arg :  gw.api.database.IQueryBeanResult<entity.User>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Author_Input) at AccountDetailNotes.pcf: line 53, column 40
    function verifyValueRangeIsAllowedType_8 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Author_Input) at AccountDetailNotes.pcf: line 53, column 40
    function verifyValueRange_9 () : void {
      var __valueRangeArg = account.NoteAuthors
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_8(__valueRangeArg)
    }
    
    // 'visible' attribute on TypeKeyInput (id=Language_Input) at AccountDetailNotes.pcf: line 75, column 73
    function visible_43 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count > 1
    }
    
    property get NoteSearchCriteria () : entity.NoteSearchCriteria {
      return getCriteriaValue(1) as entity.NoteSearchCriteria
    }
    
    property set NoteSearchCriteria ($arg :  entity.NoteSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    property get notes () : gw.api.database.IQueryBeanResult<Note> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<Note>
    }
    
    
  }
  
  
}