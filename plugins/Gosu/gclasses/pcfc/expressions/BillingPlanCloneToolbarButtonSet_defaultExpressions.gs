package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanCloneToolbarButtonSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BillingPlanCloneToolbarButtonSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanCloneToolbarButtonSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BillingPlanCloneToolbarButtonSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=Clone) at BillingPlanCloneToolbarButtonSet.default.pcf: line 16, column 44
    function action_1 () : void {
      CloneBillingPlan.go(billingPlan, false)
    }
    
    // 'action' attribute on ToolbarButton (id=Clone) at BillingPlanCloneToolbarButtonSet.default.pcf: line 16, column 44
    function action_dest_2 () : pcf.api.Destination {
      return pcf.CloneBillingPlan.createDestination(billingPlan, false)
    }
    
    // 'visible' attribute on ToolbarButton (id=Clone) at BillingPlanCloneToolbarButtonSet.default.pcf: line 16, column 44
    function visible_0 () : java.lang.Boolean {
      return perm.System.billplancreate
    }
    
    property get billingPlan () : BillingPlan {
      return getRequireValue("billingPlan", 0) as BillingPlan
    }
    
    property set billingPlan ($arg :  BillingPlan) {
      setRequireValue("billingPlan", 0, $arg)
    }
    
    
  }
  
  
}