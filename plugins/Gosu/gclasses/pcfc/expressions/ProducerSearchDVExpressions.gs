package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ProducerSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/ProducerSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at ProducerSearchDV.pcf: line 46, column 78
    function def_onEnter_22 (def :  pcf.ContactCriteriaInputSet) : void {
      def.onEnter(searchCriteria.ContactCriteria,false)
    }
    
    // 'def' attribute on InputSetRef at ProducerSearchDV.pcf: line 50, column 41
    function def_onEnter_24 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at ProducerSearchDV.pcf: line 46, column 78
    function def_refreshVariables_23 (def :  pcf.ContactCriteriaInputSet) : void {
      def.refreshVariables(searchCriteria.ContactCriteria,false)
    }
    
    // 'def' attribute on InputSetRef at ProducerSearchDV.pcf: line 50, column 41
    function def_refreshVariables_25 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TextInput (id=ProducerCodeCriterion_Input) at ProducerSearchDV.pcf: line 29, column 46
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ProducerCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at ProducerSearchDV.pcf: line 36, column 67
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.CurrencyCriterion = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on TextInput (id=ProducerNameCriterion_Input) at ProducerSearchDV.pcf: line 18, column 46
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ProducerName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=ProducerNameKanjiCriterion_Input) at ProducerSearchDV.pcf: line 24, column 84
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ProducerNameKanji = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'label' attribute on TextInput (id=ProducerNameCriterion_Input) at ProducerSearchDV.pcf: line 18, column 46
    function label_0 () : java.lang.Object {
      return gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP ? DisplayKey.get("Web.ProducerSearchDV.ProducerNamePhonetic") : DisplayKey.get("Web.ProducerSearchDV.ProducerName")
    }
    
    // 'value' attribute on TextInput (id=ProducerNameCriterion_Input) at ProducerSearchDV.pcf: line 18, column 46
    function valueRoot_3 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TextInput (id=ProducerNameCriterion_Input) at ProducerSearchDV.pcf: line 18, column 46
    function value_1 () : java.lang.String {
      return searchCriteria.ProducerName
    }
    
    // 'value' attribute on TextInput (id=ProducerCodeCriterion_Input) at ProducerSearchDV.pcf: line 29, column 46
    function value_12 () : java.lang.String {
      return searchCriteria.ProducerCode
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at ProducerSearchDV.pcf: line 36, column 67
    function value_17 () : typekey.Currency {
      return searchCriteria.CurrencyCriterion
    }
    
    // 'value' attribute on TextInput (id=ProducerNameKanjiCriterion_Input) at ProducerSearchDV.pcf: line 24, column 84
    function value_7 () : java.lang.String {
      return searchCriteria.ProducerNameKanji
    }
    
    // 'visible' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at ProducerSearchDV.pcf: line 36, column 67
    function visible_16 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on TextInput (id=ProducerNameKanjiCriterion_Input) at ProducerSearchDV.pcf: line 24, column 84
    function visible_6 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP
    }
    
    property get searchCriteria () : gw.search.ProducerSearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.search.ProducerSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.ProducerSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    
  }
  
  
}