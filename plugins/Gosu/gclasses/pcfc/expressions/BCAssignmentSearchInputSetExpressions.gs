package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/BCAssignmentSearchInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BCAssignmentSearchInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/activity/BCAssignmentSearchInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BCAssignmentSearchInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at BCAssignmentSearchInputSet.pcf: line 30, column 55
    function def_onEnter_11 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.onEnter( new gw.api.name.SearchNameOwner (searchCriteria.UserCriteria.Contact)) 
    }
    
    // 'def' attribute on InputSetRef at BCAssignmentSearchInputSet.pcf: line 30, column 55
    function def_onEnter_9 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.onEnter( new gw.api.name.SearchNameOwner (searchCriteria.UserCriteria.Contact)) 
    }
    
    // 'def' attribute on InputSetRef at BCAssignmentSearchInputSet.pcf: line 30, column 55
    function def_refreshVariables_10 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.refreshVariables( new gw.api.name.SearchNameOwner (searchCriteria.UserCriteria.Contact)) 
    }
    
    // 'def' attribute on InputSetRef at BCAssignmentSearchInputSet.pcf: line 30, column 55
    function def_refreshVariables_12 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.refreshVariables( new gw.api.name.SearchNameOwner (searchCriteria.UserCriteria.Contact)) 
    }
    
    // 'value' attribute on TextInput (id=Username_Input) at BCAssignmentSearchInputSet.pcf: line 36, column 55
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.UserCriteria.Username = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=SearchFor_Input) at BCAssignmentSearchInputSet.pcf: line 23, column 48
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.SearchType = (__VALUE_TO_SET as typekey.AssignmentSearchType)
    }
    
    // 'value' attribute on TextInput (id=GroupCriteriaName_Input) at BCAssignmentSearchInputSet.pcf: line 42, column 56
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.GroupCriteria.GroupName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=GroupCriteriaNameKanji_Input) at BCAssignmentSearchInputSet.pcf: line 48, column 124
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      (searchCriteria.GroupCriteria as GroupSearchCriteria).GroupNameKanji = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=GroupName_Input) at BCAssignmentSearchInputSet.pcf: line 54, column 56
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.QueueCriteria.QueueGroupName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=GroupNameKanji_Input) at BCAssignmentSearchInputSet.pcf: line 60, column 124
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.QueueCriteria.QueueGroupNameKanji = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=QueueName_Input) at BCAssignmentSearchInputSet.pcf: line 66, column 56
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.QueueCriteria.QueueName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'label' attribute on TextInput (id=GroupCriteriaName_Input) at BCAssignmentSearchInputSet.pcf: line 42, column 56
    function label_21 () : java.lang.Object {
      return (gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP) ? DisplayKey.get("Web.Assignment.Search.GroupNamePhonetic") : DisplayKey.get("Web.Assignment.Search.GroupName")
    }
    
    // 'mode' attribute on InputSetRef at BCAssignmentSearchInputSet.pcf: line 30, column 55
    function mode_13 () : java.lang.Object {
      return gw.api.name.NameLocaleSettings.PCFMode
    }
    
    // 'onChange' attribute on PostOnChange at BCAssignmentSearchInputSet.pcf: line 25, column 72
    function onChange_0 () : void {
      if (searchResult != null) { searchResult.clear() }
    }
    
    // 'valueRange' attribute on RangeInput (id=SearchFor_Input) at BCAssignmentSearchInputSet.pcf: line 23, column 48
    function valueRange_4 () : java.lang.Object {
      return assignmentSearchTypes
    }
    
    // 'value' attribute on TextInput (id=Username_Input) at BCAssignmentSearchInputSet.pcf: line 36, column 55
    function valueRoot_17 () : java.lang.Object {
      return searchCriteria.UserCriteria
    }
    
    // 'value' attribute on TextInput (id=GroupCriteriaName_Input) at BCAssignmentSearchInputSet.pcf: line 42, column 56
    function valueRoot_24 () : java.lang.Object {
      return searchCriteria.GroupCriteria
    }
    
    // 'value' attribute on RangeInput (id=SearchFor_Input) at BCAssignmentSearchInputSet.pcf: line 23, column 48
    function valueRoot_3 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TextInput (id=GroupCriteriaNameKanji_Input) at BCAssignmentSearchInputSet.pcf: line 48, column 124
    function valueRoot_31 () : java.lang.Object {
      return (searchCriteria.GroupCriteria as GroupSearchCriteria)
    }
    
    // 'value' attribute on TextInput (id=GroupName_Input) at BCAssignmentSearchInputSet.pcf: line 54, column 56
    function valueRoot_38 () : java.lang.Object {
      return searchCriteria.QueueCriteria
    }
    
    // 'value' attribute on RangeInput (id=SearchFor_Input) at BCAssignmentSearchInputSet.pcf: line 23, column 48
    function value_1 () : typekey.AssignmentSearchType {
      return searchCriteria.SearchType
    }
    
    // 'value' attribute on TextInput (id=Username_Input) at BCAssignmentSearchInputSet.pcf: line 36, column 55
    function value_15 () : java.lang.String {
      return searchCriteria.UserCriteria.Username
    }
    
    // 'value' attribute on TextInput (id=GroupCriteriaName_Input) at BCAssignmentSearchInputSet.pcf: line 42, column 56
    function value_22 () : java.lang.String {
      return searchCriteria.GroupCriteria.GroupName
    }
    
    // 'value' attribute on TextInput (id=GroupCriteriaNameKanji_Input) at BCAssignmentSearchInputSet.pcf: line 48, column 124
    function value_29 () : java.lang.String {
      return (searchCriteria.GroupCriteria as GroupSearchCriteria).GroupNameKanji
    }
    
    // 'value' attribute on TextInput (id=GroupName_Input) at BCAssignmentSearchInputSet.pcf: line 54, column 56
    function value_36 () : java.lang.String {
      return searchCriteria.QueueCriteria.QueueGroupName
    }
    
    // 'value' attribute on TextInput (id=GroupNameKanji_Input) at BCAssignmentSearchInputSet.pcf: line 60, column 124
    function value_43 () : java.lang.String {
      return searchCriteria.QueueCriteria.QueueGroupNameKanji
    }
    
    // 'value' attribute on TextInput (id=QueueName_Input) at BCAssignmentSearchInputSet.pcf: line 66, column 56
    function value_49 () : java.lang.String {
      return searchCriteria.QueueCriteria.QueueName
    }
    
    // 'valueRange' attribute on RangeInput (id=SearchFor_Input) at BCAssignmentSearchInputSet.pcf: line 23, column 48
    function verifyValueRangeIsAllowedType_5 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SearchFor_Input) at BCAssignmentSearchInputSet.pcf: line 23, column 48
    function verifyValueRangeIsAllowedType_5 ($$arg :  typekey.AssignmentSearchType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SearchFor_Input) at BCAssignmentSearchInputSet.pcf: line 23, column 48
    function verifyValueRange_6 () : void {
      var __valueRangeArg = assignmentSearchTypes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_5(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=GroupCriteriaName_Input) at BCAssignmentSearchInputSet.pcf: line 42, column 56
    function visible_20 () : java.lang.Boolean {
      return searchCriteria.SearchType == TC_GROUP
    }
    
    // 'visible' attribute on TextInput (id=GroupCriteriaNameKanji_Input) at BCAssignmentSearchInputSet.pcf: line 48, column 124
    function visible_28 () : java.lang.Boolean {
      return searchCriteria.SearchType == TC_GROUP and gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP
    }
    
    // 'visible' attribute on TextInput (id=GroupName_Input) at BCAssignmentSearchInputSet.pcf: line 54, column 56
    function visible_34 () : java.lang.Boolean {
      return searchCriteria.SearchType == TC_QUEUE
    }
    
    // 'visible' attribute on TextInput (id=GroupNameKanji_Input) at BCAssignmentSearchInputSet.pcf: line 60, column 124
    function visible_42 () : java.lang.Boolean {
      return searchCriteria.SearchType == TC_QUEUE and gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP
    }
    
    // 'visible' attribute on InputSetRef at BCAssignmentSearchInputSet.pcf: line 30, column 55
    function visible_8 () : java.lang.Boolean {
      return searchCriteria.SearchType == TC_USER
    }
    
    property get assignmentSearchTypes () : AssignmentSearchType[] {
      return getRequireValue("assignmentSearchTypes", 0) as AssignmentSearchType[]
    }
    
    property set assignmentSearchTypes ($arg :  AssignmentSearchType[]) {
      setRequireValue("assignmentSearchTypes", 0, $arg)
    }
    
    property get searchCriteria () : gw.api.assignment.AssignmentSearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.api.assignment.AssignmentSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.api.assignment.AssignmentSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    property get searchResult () : gw.api.assignment.AssignmentSearchResult {
      return getRequireValue("searchResult", 0) as gw.api.assignment.AssignmentSearchResult
    }
    
    property set searchResult ($arg :  gw.api.assignment.AssignmentSearchResult) {
      setRequireValue("searchResult", 0, $arg)
    }
    
    
  }
  
  
}