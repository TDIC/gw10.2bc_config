package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/Acct360ViewTransactionsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class Acct360ViewTransactionsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/acct360/Acct360ViewTransactionsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Acct360ViewTransactionsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (chargeInsttransactions :  ChargeInstanceCtx[]) : int {
      return 0
    }
    
    // 'def' attribute on PanelRef at Acct360ViewTransactionsPopup.pcf: line 19, column 59
    function def_onEnter_1 (def :  pcf.AccountDetailTransactionsLV) : void {
      def.onEnter( transactions )
    }
    
    // 'def' attribute on PanelRef at Acct360ViewTransactionsPopup.pcf: line 19, column 59
    function def_refreshVariables_2 (def :  pcf.AccountDetailTransactionsLV) : void {
      def.refreshVariables( transactions )
    }
    
    // 'initialValue' attribute on Variable at Acct360ViewTransactionsPopup.pcf: line 16, column 29
    function initialValue_0 () : Transaction[] {
      return populateTransaction()
    }
    
    override property get CurrentLocation () : pcf.Acct360ViewTransactionsPopup {
      return super.CurrentLocation as pcf.Acct360ViewTransactionsPopup
    }
    
    property get chargeInsttransactions () : ChargeInstanceCtx[] {
      return getVariableValue("chargeInsttransactions", 0) as ChargeInstanceCtx[]
    }
    
    property set chargeInsttransactions ($arg :  ChargeInstanceCtx[]) {
      setVariableValue("chargeInsttransactions", 0, $arg)
    }
    
    property get transactions () : Transaction[] {
      return getVariableValue("transactions", 0) as Transaction[]
    }
    
    property set transactions ($arg :  Transaction[]) {
      setVariableValue("transactions", 0, $arg)
    }
    
    function populateTransaction () : Transaction[] {
      var trans = new java.util.ArrayList <Transaction>()
      chargeInsttransactions.each (\ ci -> {trans.add(ci.Transaction)})
      return trans.toTypedArray()
    }
    
    
  }
  
  
}