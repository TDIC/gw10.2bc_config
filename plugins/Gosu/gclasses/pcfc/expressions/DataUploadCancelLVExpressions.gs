package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadCancelLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DataUploadCancelLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadCancelLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DataUploadCancelLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadCancelLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=charge1_Cell) at DataUploadCancelLV.pcf: line 50, column 41
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge1")
    }
    
    // 'label' attribute on TextCell (id=amount1_Cell) at DataUploadCancelLV.pcf: line 55, column 45
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount1")
    }
    
    // 'label' attribute on TextCell (id=charge2_Cell) at DataUploadCancelLV.pcf: line 60, column 41
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge2")
    }
    
    // 'label' attribute on TextCell (id=amount2_Cell) at DataUploadCancelLV.pcf: line 65, column 45
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount2")
    }
    
    // 'label' attribute on TextCell (id=charge3_Cell) at DataUploadCancelLV.pcf: line 70, column 41
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge3")
    }
    
    // 'label' attribute on TextCell (id=amount3_Cell) at DataUploadCancelLV.pcf: line 75, column 45
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount3")
    }
    
    // 'label' attribute on TextCell (id=charge4_Cell) at DataUploadCancelLV.pcf: line 80, column 41
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge4")
    }
    
    // 'label' attribute on TextCell (id=amount4_Cell) at DataUploadCancelLV.pcf: line 85, column 45
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount4")
    }
    
    // 'label' attribute on DateCell (id=cancelDate_Cell) at DataUploadCancelLV.pcf: line 29, column 39
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.CancellationDate")
    }
    
    // 'label' attribute on TextCell (id=policyNumber_Cell) at DataUploadCancelLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.PolicyNumber")
    }
    
    // 'label' attribute on TypeKeyCell (id=type_Cell) at DataUploadCancelLV.pcf: line 40, column 49
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.CancellationType")
    }
    
    // 'label' attribute on TextCell (id=reason_Cell) at DataUploadCancelLV.pcf: line 45, column 41
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.CancellationReason")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadCancelLV.pcf: line 23, column 46
    function sortValue_1 (cancel :  tdic.util.dataloader.data.sampledata.CancellationData) : java.lang.Object {
      return processor.getLoadStatus(cancel)
    }
    
    // 'value' attribute on TextCell (id=reason_Cell) at DataUploadCancelLV.pcf: line 45, column 41
    function sortValue_10 (cancel :  tdic.util.dataloader.data.sampledata.CancellationData) : java.lang.Object {
      return cancel.CancellationReason
    }
    
    // 'value' attribute on TextCell (id=charge1_Cell) at DataUploadCancelLV.pcf: line 50, column 41
    function sortValue_12 (cancel :  tdic.util.dataloader.data.sampledata.CancellationData) : java.lang.Object {
      return cancel.Charges[0].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount1_Cell) at DataUploadCancelLV.pcf: line 55, column 45
    function sortValue_14 (cancel :  tdic.util.dataloader.data.sampledata.CancellationData) : java.lang.Object {
      return cancel.Charges[0].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=charge2_Cell) at DataUploadCancelLV.pcf: line 60, column 41
    function sortValue_16 (cancel :  tdic.util.dataloader.data.sampledata.CancellationData) : java.lang.Object {
      return cancel.Charges[1].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount2_Cell) at DataUploadCancelLV.pcf: line 65, column 45
    function sortValue_18 (cancel :  tdic.util.dataloader.data.sampledata.CancellationData) : java.lang.Object {
      return cancel.Charges[1].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=charge3_Cell) at DataUploadCancelLV.pcf: line 70, column 41
    function sortValue_20 (cancel :  tdic.util.dataloader.data.sampledata.CancellationData) : java.lang.Object {
      return cancel.Charges[2].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount3_Cell) at DataUploadCancelLV.pcf: line 75, column 45
    function sortValue_22 (cancel :  tdic.util.dataloader.data.sampledata.CancellationData) : java.lang.Object {
      return cancel.Charges[2].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=charge4_Cell) at DataUploadCancelLV.pcf: line 80, column 41
    function sortValue_24 (cancel :  tdic.util.dataloader.data.sampledata.CancellationData) : java.lang.Object {
      return cancel.Charges[3].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount4_Cell) at DataUploadCancelLV.pcf: line 85, column 45
    function sortValue_26 (cancel :  tdic.util.dataloader.data.sampledata.CancellationData) : java.lang.Object {
      return cancel.Charges[3].ChargeAmount
    }
    
    // 'value' attribute on DateCell (id=cancelDate_Cell) at DataUploadCancelLV.pcf: line 29, column 39
    function sortValue_4 (cancel :  tdic.util.dataloader.data.sampledata.CancellationData) : java.lang.Object {
      return cancel.EntryDate
    }
    
    // 'value' attribute on TextCell (id=policyNumber_Cell) at DataUploadCancelLV.pcf: line 35, column 41
    function sortValue_6 (cancel :  tdic.util.dataloader.data.sampledata.CancellationData) : java.lang.Object {
      return cancel.AssociatedPolicyPeriod
    }
    
    // 'value' attribute on TypeKeyCell (id=type_Cell) at DataUploadCancelLV.pcf: line 40, column 49
    function sortValue_8 (cancel :  tdic.util.dataloader.data.sampledata.CancellationData) : java.lang.Object {
      return cancel.CancellationType
    }
    
    // 'value' attribute on RowIterator (id=cancelID) at DataUploadCancelLV.pcf: line 15, column 100
    function value_85 () : java.util.ArrayList<tdic.util.dataloader.data.sampledata.CancellationData> {
      return processor.CancellationArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadCancelLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCSampleDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCSampleDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadCancelLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DataUploadCancelLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at DataUploadCancelLV.pcf: line 17, column 92
    function highlighted_84 () : java.lang.Boolean {
      return (cancel.Error or cancel.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadCancelLV.pcf: line 23, column 46
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on DateCell (id=cancelDate_Cell) at DataUploadCancelLV.pcf: line 29, column 39
    function label_32 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.CancellationDate")
    }
    
    // 'label' attribute on TextCell (id=policyNumber_Cell) at DataUploadCancelLV.pcf: line 35, column 41
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.PolicyNumber")
    }
    
    // 'label' attribute on TypeKeyCell (id=type_Cell) at DataUploadCancelLV.pcf: line 40, column 49
    function label_42 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.CancellationType")
    }
    
    // 'label' attribute on TextCell (id=reason_Cell) at DataUploadCancelLV.pcf: line 45, column 41
    function label_47 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.CancellationReason")
    }
    
    // 'label' attribute on TextCell (id=charge1_Cell) at DataUploadCancelLV.pcf: line 50, column 41
    function label_52 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge1")
    }
    
    // 'label' attribute on TextCell (id=amount1_Cell) at DataUploadCancelLV.pcf: line 55, column 45
    function label_56 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount1")
    }
    
    // 'label' attribute on TextCell (id=charge2_Cell) at DataUploadCancelLV.pcf: line 60, column 41
    function label_60 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge2")
    }
    
    // 'label' attribute on TextCell (id=amount2_Cell) at DataUploadCancelLV.pcf: line 65, column 45
    function label_64 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount2")
    }
    
    // 'label' attribute on TextCell (id=charge3_Cell) at DataUploadCancelLV.pcf: line 70, column 41
    function label_68 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge3")
    }
    
    // 'label' attribute on TextCell (id=amount3_Cell) at DataUploadCancelLV.pcf: line 75, column 45
    function label_72 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount3")
    }
    
    // 'label' attribute on TextCell (id=charge4_Cell) at DataUploadCancelLV.pcf: line 80, column 41
    function label_76 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge4")
    }
    
    // 'label' attribute on TextCell (id=amount4_Cell) at DataUploadCancelLV.pcf: line 85, column 45
    function label_80 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount4")
    }
    
    // 'value' attribute on DateCell (id=cancelDate_Cell) at DataUploadCancelLV.pcf: line 29, column 39
    function valueRoot_34 () : java.lang.Object {
      return cancel
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadCancelLV.pcf: line 23, column 46
    function value_28 () : java.lang.String {
      return processor.getLoadStatus(cancel)
    }
    
    // 'value' attribute on DateCell (id=cancelDate_Cell) at DataUploadCancelLV.pcf: line 29, column 39
    function value_33 () : java.util.Date {
      return cancel.EntryDate
    }
    
    // 'value' attribute on TextCell (id=policyNumber_Cell) at DataUploadCancelLV.pcf: line 35, column 41
    function value_38 () : java.lang.String {
      return cancel.AssociatedPolicyPeriod
    }
    
    // 'value' attribute on TypeKeyCell (id=type_Cell) at DataUploadCancelLV.pcf: line 40, column 49
    function value_43 () : typekey.CancellationType {
      return cancel.CancellationType
    }
    
    // 'value' attribute on TextCell (id=reason_Cell) at DataUploadCancelLV.pcf: line 45, column 41
    function value_48 () : java.lang.String {
      return cancel.CancellationReason
    }
    
    // 'value' attribute on TextCell (id=charge1_Cell) at DataUploadCancelLV.pcf: line 50, column 41
    function value_53 () : java.lang.String {
      return cancel.Charges[0].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount1_Cell) at DataUploadCancelLV.pcf: line 55, column 45
    function value_57 () : java.math.BigDecimal {
      return cancel.Charges[0].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=charge2_Cell) at DataUploadCancelLV.pcf: line 60, column 41
    function value_61 () : java.lang.String {
      return cancel.Charges[1].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount2_Cell) at DataUploadCancelLV.pcf: line 65, column 45
    function value_65 () : java.math.BigDecimal {
      return cancel.Charges[1].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=charge3_Cell) at DataUploadCancelLV.pcf: line 70, column 41
    function value_69 () : java.lang.String {
      return cancel.Charges[2].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount3_Cell) at DataUploadCancelLV.pcf: line 75, column 45
    function value_73 () : java.math.BigDecimal {
      return cancel.Charges[2].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=charge4_Cell) at DataUploadCancelLV.pcf: line 80, column 41
    function value_77 () : java.lang.String {
      return cancel.Charges[3].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount4_Cell) at DataUploadCancelLV.pcf: line 85, column 45
    function value_81 () : java.math.BigDecimal {
      return cancel.Charges[3].ChargeAmount
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadCancelLV.pcf: line 23, column 46
    function visible_29 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get cancel () : tdic.util.dataloader.data.sampledata.CancellationData {
      return getIteratedValue(1) as tdic.util.dataloader.data.sampledata.CancellationData
    }
    
    
  }
  
  
}