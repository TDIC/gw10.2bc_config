package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyProcessDetailPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyProcessDetailPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (delinquencyProcess :  DelinquencyProcess) : int {
      return 0
    }
    
    // 'def' attribute on PanelRef at DelinquencyProcessDetailPopup.pcf: line 19, column 69
    function def_onEnter_0 (def :  pcf.DelinquencyProcessDetailPanelSet) : void {
      def.onEnter(delinquencyProcess)
    }
    
    // 'def' attribute on PanelRef at DelinquencyProcessDetailPopup.pcf: line 19, column 69
    function def_refreshVariables_1 (def :  pcf.DelinquencyProcessDetailPanelSet) : void {
      def.refreshVariables(delinquencyProcess)
    }
    
    override property get CurrentLocation () : pcf.DelinquencyProcessDetailPopup {
      return super.CurrentLocation as pcf.DelinquencyProcessDetailPopup
    }
    
    property get delinquencyProcess () : DelinquencyProcess {
      return getVariableValue("delinquencyProcess", 0) as DelinquencyProcess
    }
    
    property set delinquencyProcess ($arg :  DelinquencyProcess) {
      setVariableValue("delinquencyProcess", 0, $arg)
    }
    
    
  }
  
  
}