package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerMenuActionsExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerMenuActionsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_NewCommissionPaymentStandard) at ProducerMenuActions.pcf: line 18, column 50
    function action_1 () : void {
      NewCommissionPaymentStandardPopup.push(producer)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_CommissionRecoveryWriteoff) at ProducerMenuActions.pcf: line 37, column 95
    function action_11 () : void {
      CommissionReceivableReductionWizard.push(producer)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_ReturnCommission) at ProducerMenuActions.pcf: line 41, column 85
    function action_13 () : void {
      AgencyCommissionPaymentWizard.push(producer)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_NewAgencyPayment) at ProducerMenuActions.pcf: line 45, column 82
    function action_15 () : void {
      AgencyDistributionWizard.go( producer, gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum.PAYMENT )
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_NewAgencyPromise) at ProducerMenuActions.pcf: line 50, column 82
    function action_18 () : void {
      AgencyDistributionWizard.go( producer, gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum.PROMISE )
    }
    
    // 'action' attribute on MenuItem (id=NewCreditDistribution) at ProducerMenuActions.pcf: line 55, column 82
    function action_21 () : void {
      AgencyDistributionWizard.go( producer, gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum.CREDIT_DISTRIBUTION )
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_Disbursement) at ProducerMenuActions.pcf: line 61, column 46
    function action_25 () : void {
      AgencyDisbursementWizard.push(producer, false)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_DisburseCreditItems) at ProducerMenuActions.pcf: line 67, column 46
    function action_29 () : void {
      AgencyDisbursementWizard.push(producer, true)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_NewNote) at ProducerMenuActions.pcf: line 73, column 35
    function action_32 () : void {
      ProducerNewNoteWorksheet.goInWorkspace(producer)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_TransferFunds) at ProducerMenuActions.pcf: line 85, column 23
    function action_37 () : void {
      ProducerTransferFundsWizard.push( producer )
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_NewCommissionPaymentBonus) at ProducerMenuActions.pcf: line 23, column 51
    function action_4 () : void {
      NewCommissionPaymentBonusWizard.push(producer)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_Writeoff) at ProducerMenuActions.pcf: line 90, column 39
    function action_40 () : void {
      ProducerWriteoffWizard.push(producer)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_NegativeWriteoff) at ProducerMenuActions.pcf: line 95, column 39
    function action_43 () : void {
      ProducerNegativeWriteoffWizard.push(producer)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_NewCommissonPaymentAdvanced) at ProducerMenuActions.pcf: line 28, column 49
    function action_7 () : void {
      NewCommissionPaymentAdvanceWizard.push(producer)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_CommissionWriteoff) at ProducerMenuActions.pcf: line 33, column 87
    function action_9 () : void {
      CommissionPayableReductionWizard.push(producer.ID)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_CommissionWriteoff) at ProducerMenuActions.pcf: line 33, column 87
    function action_dest_10 () : pcf.api.Destination {
      return pcf.CommissionPayableReductionWizard.createDestination(producer.ID)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_CommissionRecoveryWriteoff) at ProducerMenuActions.pcf: line 37, column 95
    function action_dest_12 () : pcf.api.Destination {
      return pcf.CommissionReceivableReductionWizard.createDestination(producer)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_ReturnCommission) at ProducerMenuActions.pcf: line 41, column 85
    function action_dest_14 () : pcf.api.Destination {
      return pcf.AgencyCommissionPaymentWizard.createDestination(producer)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_NewAgencyPayment) at ProducerMenuActions.pcf: line 45, column 82
    function action_dest_16 () : pcf.api.Destination {
      return pcf.AgencyDistributionWizard.createDestination( producer, gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum.PAYMENT )
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_NewAgencyPromise) at ProducerMenuActions.pcf: line 50, column 82
    function action_dest_19 () : pcf.api.Destination {
      return pcf.AgencyDistributionWizard.createDestination( producer, gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum.PROMISE )
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_NewCommissionPaymentStandard) at ProducerMenuActions.pcf: line 18, column 50
    function action_dest_2 () : pcf.api.Destination {
      return pcf.NewCommissionPaymentStandardPopup.createDestination(producer)
    }
    
    // 'action' attribute on MenuItem (id=NewCreditDistribution) at ProducerMenuActions.pcf: line 55, column 82
    function action_dest_22 () : pcf.api.Destination {
      return pcf.AgencyDistributionWizard.createDestination( producer, gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum.CREDIT_DISTRIBUTION )
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_Disbursement) at ProducerMenuActions.pcf: line 61, column 46
    function action_dest_26 () : pcf.api.Destination {
      return pcf.AgencyDisbursementWizard.createDestination(producer, false)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_DisburseCreditItems) at ProducerMenuActions.pcf: line 67, column 46
    function action_dest_30 () : pcf.api.Destination {
      return pcf.AgencyDisbursementWizard.createDestination(producer, true)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_NewNote) at ProducerMenuActions.pcf: line 73, column 35
    function action_dest_33 () : pcf.api.Destination {
      return pcf.ProducerNewNoteWorksheet.createDestination(producer)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_TransferFunds) at ProducerMenuActions.pcf: line 85, column 23
    function action_dest_38 () : pcf.api.Destination {
      return pcf.ProducerTransferFundsWizard.createDestination( producer )
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_Writeoff) at ProducerMenuActions.pcf: line 90, column 39
    function action_dest_41 () : pcf.api.Destination {
      return pcf.ProducerWriteoffWizard.createDestination(producer)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_NegativeWriteoff) at ProducerMenuActions.pcf: line 95, column 39
    function action_dest_44 () : pcf.api.Destination {
      return pcf.ProducerNegativeWriteoffWizard.createDestination(producer)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_NewCommissionPaymentBonus) at ProducerMenuActions.pcf: line 23, column 51
    function action_dest_5 () : pcf.api.Destination {
      return pcf.NewCommissionPaymentBonusWizard.createDestination(producer)
    }
    
    // 'action' attribute on MenuItem (id=ProducerMenuActions_NewCommissonPaymentAdvanced) at ProducerMenuActions.pcf: line 28, column 49
    function action_dest_8 () : pcf.api.Destination {
      return pcf.NewCommissionPaymentAdvanceWizard.createDestination(producer)
    }
    
    // 'available' attribute on MenuItem (id=ProducerMenuActions_NewAgencyPromise) at ProducerMenuActions.pcf: line 50, column 82
    function available_17 () : java.lang.Boolean {
      return producer.AgencyBillCycles.length > 0
    }
    
    // 'def' attribute on MenuItemSetRef at ProducerMenuActions.pcf: line 79, column 49
    function def_onEnter_34 (def :  pcf.NewDocumentMenuItemSet) : void {
      def.onEnter(producer)
    }
    
    // 'def' attribute on MenuItemSetRef at ProducerMenuActions.pcf: line 79, column 49
    function def_refreshVariables_35 (def :  pcf.NewDocumentMenuItemSet) : void {
      def.refreshVariables(producer)
    }
    
    // 'visible' attribute on MenuItem (id=ProducerMenuActions_NewCommissionPaymentStandard) at ProducerMenuActions.pcf: line 18, column 50
    function visible_0 () : java.lang.Boolean {
      return perm.Producer.prodpmntstndman
    }
    
    // 'visible' attribute on MenuItem (id=ProducerMenuActions_Disbursement) at ProducerMenuActions.pcf: line 61, column 46
    function visible_24 () : java.lang.Boolean {
      return perm.Transaction.disbcreate
    }
    
    // 'visible' attribute on MenuItem (id=ProducerMenuActions_NewCommissionPaymentBonus) at ProducerMenuActions.pcf: line 23, column 51
    function visible_3 () : java.lang.Boolean {
      return perm.Producer.prodpmntbonusman
    }
    
    // 'visible' attribute on MenuItem (id=ProducerMenuActions_NewNote) at ProducerMenuActions.pcf: line 73, column 35
    function visible_31 () : java.lang.Boolean {
      return perm.Note.create
    }
    
    // 'visible' attribute on MenuItem (id=NewDocument) at ProducerMenuActions.pcf: line 77, column 38
    function visible_36 () : java.lang.Boolean {
      return perm.Document.create
    }
    
    // 'visible' attribute on MenuItem (id=ProducerMenuActions_Writeoff) at ProducerMenuActions.pcf: line 90, column 39
    function visible_39 () : java.lang.Boolean {
      return perm.System.agencywo
    }
    
    // 'visible' attribute on MenuItem (id=ProducerMenuActions_NewCommissonPaymentAdvanced) at ProducerMenuActions.pcf: line 28, column 49
    function visible_6 () : java.lang.Boolean {
      return perm.Producer.prodpmntadvman
    }
    
    property get producer () : Producer {
      return getRequireValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setRequireValue("producer", 0, $arg)
    }
    
    
  }
  
  
}