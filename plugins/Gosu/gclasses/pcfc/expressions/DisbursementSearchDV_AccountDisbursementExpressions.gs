package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DisbursementSearchDV.AccountDisbursement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DisbursementSearchDV_AccountDisbursementExpressions {
  @javax.annotation.Generated("config/web/pcf/search/DisbursementSearchDV.AccountDisbursement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DisbursementSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountNumberCriterion_Input) at DisbursementSearchDV.AccountDisbursement.pcf: line 24, column 39
    function action_2 () : void {
      AccountSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountNumberCriterion_Input) at DisbursementSearchDV.AccountDisbursement.pcf: line 24, column 39
    function action_dest_3 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'conversionExpression' attribute on PickerInput (id=AccountNumberCriterion_Input) at DisbursementSearchDV.AccountDisbursement.pcf: line 24, column 39
    function conversionExpression_4 (PickedValue :  Account) : java.lang.String {
      return PickedValue.DisplayName
    }
    
    // 'def' attribute on InputSetRef at DisbursementSearchDV.AccountDisbursement.pcf: line 16, column 94
    function def_onEnter_0 (def :  pcf.DisbursementSearchCriteriaInputSet) : void {
      def.onEnter(searchCriteria, disbursementSubtypeHolder)
    }
    
    // 'def' attribute on InputSetRef at DisbursementSearchDV.AccountDisbursement.pcf: line 28, column 41
    function def_onEnter_9 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at DisbursementSearchDV.AccountDisbursement.pcf: line 16, column 94
    function def_refreshVariables_1 (def :  pcf.DisbursementSearchCriteriaInputSet) : void {
      def.refreshVariables(searchCriteria, disbursementSubtypeHolder)
    }
    
    // 'def' attribute on InputSetRef at DisbursementSearchDV.AccountDisbursement.pcf: line 28, column 41
    function def_refreshVariables_10 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at DisbursementSearchDV.AccountDisbursement.pcf: line 24, column 39
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      (searchCriteria as gw.search.AcctDisbSearchCriteria).AccountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at DisbursementSearchDV.AccountDisbursement.pcf: line 24, column 39
    function valueRoot_7 () : java.lang.Object {
      return (searchCriteria as gw.search.AcctDisbSearchCriteria)
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at DisbursementSearchDV.AccountDisbursement.pcf: line 24, column 39
    function value_5 () : java.lang.String {
      return (searchCriteria as gw.search.AcctDisbSearchCriteria).AccountNumber
    }
    
    property get disbursementSubtypeHolder () : typekey.Disbursement[] {
      return getRequireValue("disbursementSubtypeHolder", 0) as typekey.Disbursement[]
    }
    
    property set disbursementSubtypeHolder ($arg :  typekey.Disbursement[]) {
      setRequireValue("disbursementSubtypeHolder", 0, $arg)
    }
    
    property get searchCriteria () : gw.search.DisbSearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.search.DisbSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.DisbSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    
  }
  
  
}