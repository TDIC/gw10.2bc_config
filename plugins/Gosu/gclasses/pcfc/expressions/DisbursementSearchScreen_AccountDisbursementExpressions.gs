package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DisbursementSearchScreen.AccountDisbursement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DisbursementSearchScreen_AccountDisbursementExpressions {
  @javax.annotation.Generated("config/web/pcf/search/DisbursementSearchScreen.AccountDisbursement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DisbursementSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get disbursementSubtypeHolder () : typekey.Disbursement[] {
      return getRequireValue("disbursementSubtypeHolder", 0) as typekey.Disbursement[]
    }
    
    property set disbursementSubtypeHolder ($arg :  typekey.Disbursement[]) {
      setRequireValue("disbursementSubtypeHolder", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/DisbursementSearchScreen.AccountDisbursement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TypeKeyCell (id=Reason_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 53, column 31
    function action_21 () : void {
      DisbursementDetail.go(disbursementSearchView.Disbursement, false)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 87, column 31
    function action_40 () : void {
      AccountOverview.go(disbursementSearchView.Account)
    }
    
    // 'action' attribute on TypeKeyCell (id=Reason_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 53, column 31
    function action_dest_22 () : pcf.api.Destination {
      return pcf.DisbursementDetail.createDestination(disbursementSearchView.Disbursement, false)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 87, column 31
    function action_dest_41 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(disbursementSearchView.Account)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 64, column 100
    function currency_30 () : typekey.Currency {
      return disbursementSearchView.Currency
    }
    
    // 'value' attribute on DateCell (id=CloseDate_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 41, column 59
    function valueRoot_16 () : java.lang.Object {
      return disbursementSearchView
    }
    
    // 'value' attribute on TextCell (id=accountName_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 91, column 69
    function valueRoot_46 () : java.lang.Object {
      return disbursementSearchView.Account
    }
    
    // 'value' attribute on DateCell (id=CloseDate_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 41, column 59
    function value_15 () : java.util.Date {
      return disbursementSearchView.CloseDate
    }
    
    // 'value' attribute on TextCell (id=CheckNumber_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 46, column 61
    function value_18 () : java.lang.String {
      return disbursementSearchView.CheckNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Reason_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 53, column 31
    function value_23 () : typekey.Reason {
      return disbursementSearchView.Reason
    }
    
    // 'value' attribute on TextCell (id=Payee_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 58, column 55
    function value_26 () : java.lang.String {
      return disbursementSearchView.PayTo
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 64, column 100
    function value_29 () : gw.pl.currency.MonetaryAmount {
      return disbursementSearchView.Amount.ofCurrency(disbursementSearchView.Currency)
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentMethod_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 69, column 52
    function value_32 () : typekey.PaymentMethod {
      return disbursementSearchView.PaymentMethod
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 74, column 57
    function value_35 () : typekey.DisbursementStatus {
      return disbursementSearchView.Status
    }
    
    // 'value' attribute on TypeKeyCell (id=productLOB_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 80, column 46
    function value_38 () : typekey.LOBCode {
      return disbursementSearchView.Disbursement typeis AccountDisbursement ? disbursementSearchView.Disbursement.UnappliedFund.Policy.LOBCode : null
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 87, column 31
    function value_42 () : java.lang.String {
      return disbursementSearchView.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=accountName_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 91, column 69
    function value_45 () : java.lang.String {
      return disbursementSearchView.Account.AccountName
    }
    
    // 'value' attribute on TypeKeyCell (id=state_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 97, column 51
    function value_48 () : typekey.Jurisdiction {
      return disbursementSearchView.Disbursement typeis AccountDisbursement ? disbursementSearchView.Disbursement.UnappliedFund.Policy.LatestPolicyPeriod.RiskJurisdiction : null
    }
    
    // 'value' attribute on TextCell (id=UnappliedFund_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 103, column 51
    function value_50 () : entity.UnappliedFund {
      return disbursementSearchView.Disbursement typeis AccountDisbursement ? disbursementSearchView.Disbursement.UnappliedFund : null
    }
    
    property get disbursementSearchView () : entity.AcctDisbSearchView {
      return getIteratedValue(2) as entity.AcctDisbSearchView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/DisbursementSearchScreen.AccountDisbursement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends DisbursementSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at DisbursementSearchScreen.AccountDisbursement.pcf: line 21, column 45
    function def_onEnter_0 (def :  pcf.DisbursementSearchDV_AccountDisbursement) : void {
      def.onEnter(searchCriteria, disbursementSubtypeHolder)
    }
    
    // 'def' attribute on PanelRef at DisbursementSearchScreen.AccountDisbursement.pcf: line 21, column 45
    function def_onEnter_2 (def :  pcf.DisbursementSearchDV_AgencyDisbursement) : void {
      def.onEnter(searchCriteria, disbursementSubtypeHolder)
    }
    
    // 'def' attribute on PanelRef at DisbursementSearchScreen.AccountDisbursement.pcf: line 21, column 45
    function def_refreshVariables_1 (def :  pcf.DisbursementSearchDV_AccountDisbursement) : void {
      def.refreshVariables(searchCriteria, disbursementSubtypeHolder)
    }
    
    // 'def' attribute on PanelRef at DisbursementSearchScreen.AccountDisbursement.pcf: line 21, column 45
    function def_refreshVariables_3 (def :  pcf.DisbursementSearchDV_AgencyDisbursement) : void {
      def.refreshVariables(searchCriteria, disbursementSubtypeHolder)
    }
    
    // 'maxSearchResults' attribute on SearchPanel at DisbursementSearchScreen.AccountDisbursement.pcf: line 18, column 86
    function maxSearchResults_54 () : java.lang.Object {
      return gw.api.system.BCConfigParameters.MaxSearchResults.Value
    }
    
    // 'mode' attribute on PanelRef at DisbursementSearchScreen.AccountDisbursement.pcf: line 21, column 45
    function mode_4 () : java.lang.Object {
      return disbursementSubtypeHolder[0]
    }
    
    // 'searchCriteria' attribute on SearchPanel at DisbursementSearchScreen.AccountDisbursement.pcf: line 18, column 86
    function searchCriteria_56 () : gw.search.AcctDisbSearchCriteria {
      return new gw.search.AcctDisbSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at DisbursementSearchScreen.AccountDisbursement.pcf: line 18, column 86
    function search_55 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria)
    }
    
    // 'sortBy' attribute on MonetaryAmountCell (id=Amount_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 64, column 100
    function sortValue_10 (disbursementSearchView :  entity.AcctDisbSearchView) : java.lang.Object {
      return  disbursementSearchView.Amount
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentMethod_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 69, column 52
    function sortValue_11 (disbursementSearchView :  entity.AcctDisbSearchView) : java.lang.Object {
      return disbursementSearchView.PaymentMethod
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 74, column 57
    function sortValue_12 (disbursementSearchView :  entity.AcctDisbSearchView) : java.lang.Object {
      return disbursementSearchView.Status
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 87, column 31
    function sortValue_13 (disbursementSearchView :  entity.AcctDisbSearchView) : java.lang.Object {
      return disbursementSearchView.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=accountName_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 91, column 69
    function sortValue_14 (disbursementSearchView :  entity.AcctDisbSearchView) : java.lang.Object {
      return disbursementSearchView.Account.AccountName
    }
    
    // 'value' attribute on DateCell (id=CloseDate_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 41, column 59
    function sortValue_5 (disbursementSearchView :  entity.AcctDisbSearchView) : java.lang.Object {
      return disbursementSearchView.CloseDate
    }
    
    // 'value' attribute on TextCell (id=CheckNumber_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 46, column 61
    function sortValue_6 (disbursementSearchView :  entity.AcctDisbSearchView) : java.lang.Object {
      return disbursementSearchView.CheckNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Reason_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 53, column 31
    function sortValue_7 (disbursementSearchView :  entity.AcctDisbSearchView) : java.lang.Object {
      return disbursementSearchView.Reason
    }
    
    // 'value' attribute on TextCell (id=Payee_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 58, column 55
    function sortValue_8 (disbursementSearchView :  entity.AcctDisbSearchView) : java.lang.Object {
      return disbursementSearchView.PayTo
    }
    
    // 'sortBy' attribute on MonetaryAmountCell (id=Amount_Cell) at DisbursementSearchScreen.AccountDisbursement.pcf: line 64, column 100
    function sortValue_9 (disbursementSearchView :  entity.AcctDisbSearchView) : java.lang.Object {
      return disbursementSearchView.Currency
    }
    
    // 'value' attribute on RowIterator at DisbursementSearchScreen.AccountDisbursement.pcf: line 35, column 91
    function value_52 () : gw.api.database.IQueryBeanResult<entity.AcctDisbSearchView> {
      return disbursementSearchViews
    }
    
    property get disbursementSearchViews () : gw.api.database.IQueryBeanResult<AcctDisbSearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<AcctDisbSearchView>
    }
    
    property get searchCriteria () : gw.search.AcctDisbSearchCriteria {
      return getCriteriaValue(1) as gw.search.AcctDisbSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.AcctDisbSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}