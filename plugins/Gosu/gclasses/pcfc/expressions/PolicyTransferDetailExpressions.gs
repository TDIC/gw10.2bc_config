package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyTransferDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyTransferDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyTransferDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyTransferDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyHistory :  PolicyHistory) : int {
      return 0
    }
    
    // 'initialValue' attribute on Variable at PolicyTransferDetail.pcf: line 18, column 28
    function initialValue_0 () : PolicyPeriod {
      return policyHistory.PolicyPeriod
    }
    
    // 'parent' attribute on Page (id=PolicyTransferDetail) at PolicyTransferDetail.pcf: line 9, column 72
    static function parent_23 (policyHistory :  PolicyHistory) : pcf.api.Destination {
      return pcf.PolicyDetailHistory.createDestination(policyHistory.PolicyPeriod)
    }
    
    // 'value' attribute on DateInput (id=TransferDate_Input) at PolicyTransferDetail.pcf: line 44, column 46
    function valueRoot_11 () : java.lang.Object {
      return policyHistory
    }
    
    // 'value' attribute on TextInput (id=OriginalAccountNumber_Input) at PolicyTransferDetail.pcf: line 55, column 63
    function valueRoot_16 () : java.lang.Object {
      return policyHistory.OtherAccount
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at PolicyTransferDetail.pcf: line 30, column 48
    function valueRoot_2 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on TextInput (id=NewAccountNumber_Input) at PolicyTransferDetail.pcf: line 66, column 58
    function valueRoot_21 () : java.lang.Object {
      return policyHistory.Account
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at PolicyTransferDetail.pcf: line 30, column 48
    function value_1 () : java.lang.String {
      return policyPeriod.PolicyNumber
    }
    
    // 'value' attribute on DateInput (id=TransferDate_Input) at PolicyTransferDetail.pcf: line 44, column 46
    function value_10 () : java.util.Date {
      return policyHistory.EventDate
    }
    
    // 'value' attribute on TextInput (id=OriginalAccountInsured_Input) at PolicyTransferDetail.pcf: line 51, column 40
    function value_13 () : entity.Person {
      return policyHistory.OtherAccount.Insured.Contact typeis Person ? policyHistory.OtherAccount.Insured.Contact : null
    }
    
    // 'value' attribute on TextInput (id=OriginalAccountNumber_Input) at PolicyTransferDetail.pcf: line 55, column 63
    function value_15 () : java.lang.String {
      return policyHistory.OtherAccount.AccountNumber
    }
    
    // 'value' attribute on TextInput (id=NewAccountInsured_Input) at PolicyTransferDetail.pcf: line 62, column 40
    function value_18 () : entity.Person {
      return policyHistory.Account.Insured.Contact typeis Person ? policyHistory.Account.Insured.Contact : null
    }
    
    // 'value' attribute on TextInput (id=NewAccountNumber_Input) at PolicyTransferDetail.pcf: line 66, column 58
    function value_20 () : java.lang.String {
      return policyHistory.Account.AccountNumber
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at PolicyTransferDetail.pcf: line 34, column 52
    function value_4 () : java.util.Date {
      return policyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at PolicyTransferDetail.pcf: line 38, column 54
    function value_7 () : java.util.Date {
      return policyPeriod.PolicyPerExpirDate
    }
    
    override property get CurrentLocation () : pcf.PolicyTransferDetail {
      return super.CurrentLocation as pcf.PolicyTransferDetail
    }
    
    property get policyHistory () : PolicyHistory {
      return getVariableValue("policyHistory", 0) as PolicyHistory
    }
    
    property set policyHistory ($arg :  PolicyHistory) {
      setVariableValue("policyHistory", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}