package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadAuthLimitLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadAuthLimitLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadAuthLimitLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadAuthLimitLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadAuthLimitLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=limitAmount_Cell) at AdminDataUploadAuthLimitLV.pcf: line 50, column 45
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.AuthorityLimit.LimitAmount")
    }
    
    // 'label' attribute on TextCell (id=profile_Cell) at AdminDataUploadAuthLimitLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.AuthorityLimit.Profile")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadAuthLimitLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.AuthorityLimit.PublicID")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadAuthLimitLV.pcf: line 40, column 42
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'label' attribute on TypeKeyCell (id=limitType_Cell) at AdminDataUploadAuthLimitLV.pcf: line 45, column 51
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.AuthorityLimit.LimitType")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadAuthLimitLV.pcf: line 23, column 46
    function sortValue_1 (authLimit :  tdic.util.dataloader.data.admindata.AuthorityLimitData) : java.lang.Object {
      return processor.getLoadStatus(authLimit)
    }
    
    // 'value' attribute on TypeKeyCell (id=limitType_Cell) at AdminDataUploadAuthLimitLV.pcf: line 45, column 51
    function sortValue_10 (authLimit :  tdic.util.dataloader.data.admindata.AuthorityLimitData) : java.lang.Object {
      return authLimit.LimitType
    }
    
    // 'value' attribute on TextCell (id=limitAmount_Cell) at AdminDataUploadAuthLimitLV.pcf: line 50, column 45
    function sortValue_12 (authLimit :  tdic.util.dataloader.data.admindata.AuthorityLimitData) : java.lang.Object {
      return authLimit.LimitAmount
    }
    
    // 'value' attribute on TextCell (id=profile_Cell) at AdminDataUploadAuthLimitLV.pcf: line 29, column 41
    function sortValue_4 (authLimit :  tdic.util.dataloader.data.admindata.AuthorityLimitData) : java.lang.Object {
      return authLimit.Profile.Name
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadAuthLimitLV.pcf: line 35, column 41
    function sortValue_6 (authLimit :  tdic.util.dataloader.data.admindata.AuthorityLimitData) : java.lang.Object {
      return authLimit.PublicID
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadAuthLimitLV.pcf: line 40, column 42
    function sortValue_8 (authLimit :  tdic.util.dataloader.data.admindata.AuthorityLimitData) : java.lang.Object {
      return authLimit.Exclude
    }
    
    // 'value' attribute on RowIterator (id=AuthLimit) at AdminDataUploadAuthLimitLV.pcf: line 15, column 101
    function value_44 () : java.util.ArrayList<tdic.util.dataloader.data.admindata.AuthorityLimitData> {
      return processor.AuthorityLimitArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadAuthLimitLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadAuthLimitLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminDataUploadAuthLimitLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at AdminDataUploadAuthLimitLV.pcf: line 17, column 98
    function highlighted_43 () : java.lang.Boolean {
      return (authLimit.Error or authLimit.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadAuthLimitLV.pcf: line 23, column 46
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=profile_Cell) at AdminDataUploadAuthLimitLV.pcf: line 29, column 41
    function label_18 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.AuthorityLimit.Profile")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadAuthLimitLV.pcf: line 35, column 41
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.AuthorityLimit.PublicID")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadAuthLimitLV.pcf: line 40, column 42
    function label_28 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'label' attribute on TypeKeyCell (id=limitType_Cell) at AdminDataUploadAuthLimitLV.pcf: line 45, column 51
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.AuthorityLimit.LimitType")
    }
    
    // 'label' attribute on TextCell (id=limitAmount_Cell) at AdminDataUploadAuthLimitLV.pcf: line 50, column 45
    function label_38 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.AuthorityLimit.LimitAmount")
    }
    
    // 'value' attribute on TextCell (id=profile_Cell) at AdminDataUploadAuthLimitLV.pcf: line 29, column 41
    function valueRoot_20 () : java.lang.Object {
      return authLimit.Profile
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadAuthLimitLV.pcf: line 35, column 41
    function valueRoot_25 () : java.lang.Object {
      return authLimit
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadAuthLimitLV.pcf: line 23, column 46
    function value_14 () : java.lang.String {
      return processor.getLoadStatus(authLimit)
    }
    
    // 'value' attribute on TextCell (id=profile_Cell) at AdminDataUploadAuthLimitLV.pcf: line 29, column 41
    function value_19 () : java.lang.String {
      return authLimit.Profile.Name
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadAuthLimitLV.pcf: line 35, column 41
    function value_24 () : java.lang.String {
      return authLimit.PublicID
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadAuthLimitLV.pcf: line 40, column 42
    function value_29 () : java.lang.Boolean {
      return authLimit.Exclude
    }
    
    // 'value' attribute on TypeKeyCell (id=limitType_Cell) at AdminDataUploadAuthLimitLV.pcf: line 45, column 51
    function value_34 () : typekey.AuthorityLimitType {
      return authLimit.LimitType
    }
    
    // 'value' attribute on TextCell (id=limitAmount_Cell) at AdminDataUploadAuthLimitLV.pcf: line 50, column 45
    function value_39 () : java.math.BigDecimal {
      return authLimit.LimitAmount
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadAuthLimitLV.pcf: line 23, column 46
    function visible_15 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get authLimit () : tdic.util.dataloader.data.admindata.AuthorityLimitData {
      return getIteratedValue(1) as tdic.util.dataloader.data.admindata.AuthorityLimitData
    }
    
    
  }
  
  
}