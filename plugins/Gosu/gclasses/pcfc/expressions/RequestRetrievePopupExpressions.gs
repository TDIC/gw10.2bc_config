package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/archive/RequestRetrievePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class RequestRetrievePopupExpressions {
  @javax.annotation.Generated("config/web/pcf/archive/RequestRetrievePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RequestRetrievePopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'value' attribute on TextAreaInput (id=ReasonText_Input) at RequestRetrievePopup.pcf: line 33, column 47
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      retrieveRequest.Reason = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at RequestRetrievePopup.pcf: line 18, column 43
    function initialValue_0 () : PolicyPeriodRetrieveRequest {
      return policyPeriod.createRetrieveRequest(gw.plugin.util.CurrentUserUtil.getCurrentUser().getUser(), "", true)
    }
    
    // 'value' attribute on TextAreaInput (id=ReasonText_Input) at RequestRetrievePopup.pcf: line 33, column 47
    function valueRoot_3 () : java.lang.Object {
      return retrieveRequest
    }
    
    // 'value' attribute on TextAreaInput (id=ReasonText_Input) at RequestRetrievePopup.pcf: line 33, column 47
    function value_1 () : java.lang.String {
      return retrieveRequest.Reason
    }
    
    // 'visible' attribute on DetailViewPanel at RequestRetrievePopup.pcf: line 26, column 55
    function visible_5 () : java.lang.Boolean {
      return perm.PolicyPeriod.archiveretrieve
    }
    
    override property get CurrentLocation () : pcf.RequestRetrievePopup {
      return super.CurrentLocation as pcf.RequestRetrievePopup
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get retrieveRequest () : PolicyPeriodRetrieveRequest {
      return getVariableValue("retrieveRequest", 0) as PolicyPeriodRetrieveRequest
    }
    
    property set retrieveRequest ($arg :  PolicyPeriodRetrieveRequest) {
      setVariableValue("retrieveRequest", 0, $arg)
    }
    
    
  }
  
  
}