package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadCollectionAgenciesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadCollectionAgenciesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadCollectionAgenciesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadCollectionAgenciesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=email_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 50, column 41
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.CollectionAgency.Email")
    }
    
    // 'label' attribute on TextCell (id=address_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 55, column 41
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.CollectionAgency.Address")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 60, column 42
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.CollectionAgency.PublicID")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.CollectionAgency.Name")
    }
    
    // 'label' attribute on TextCell (id=phone_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 40, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.CollectionAgency.Phone")
    }
    
    // 'label' attribute on TextCell (id=fax_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 45, column 41
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.CollectionAgency.Fax")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 23, column 46
    function sortValue_1 (collectionAgency :  tdic.util.dataloader.data.admindata.CollectionAgencyData) : java.lang.Object {
      return processor.getLoadStatus(collectionAgency)
    }
    
    // 'value' attribute on TextCell (id=fax_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 45, column 41
    function sortValue_10 (collectionAgency :  tdic.util.dataloader.data.admindata.CollectionAgencyData) : java.lang.Object {
      return collectionAgency.Fax
    }
    
    // 'value' attribute on TextCell (id=email_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 50, column 41
    function sortValue_12 (collectionAgency :  tdic.util.dataloader.data.admindata.CollectionAgencyData) : java.lang.Object {
      return collectionAgency.Email
    }
    
    // 'value' attribute on TextCell (id=address_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 55, column 41
    function sortValue_14 (collectionAgency :  tdic.util.dataloader.data.admindata.CollectionAgencyData) : java.lang.Object {
      return collectionAgency.Address.DisplayName
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 60, column 42
    function sortValue_16 (collectionAgency :  tdic.util.dataloader.data.admindata.CollectionAgencyData) : java.lang.Object {
      return collectionAgency.Exclude
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 29, column 41
    function sortValue_4 (collectionAgency :  tdic.util.dataloader.data.admindata.CollectionAgencyData) : java.lang.Object {
      return collectionAgency.PublicID
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 35, column 41
    function sortValue_6 (collectionAgency :  tdic.util.dataloader.data.admindata.CollectionAgencyData) : java.lang.Object {
      return collectionAgency.Name
    }
    
    // 'value' attribute on TextCell (id=phone_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 40, column 41
    function sortValue_8 (collectionAgency :  tdic.util.dataloader.data.admindata.CollectionAgencyData) : java.lang.Object {
      return collectionAgency.Phone
    }
    
    // 'value' attribute on RowIterator (id=CollectionAgency) at AdminDataUploadCollectionAgenciesLV.pcf: line 15, column 103
    function value_58 () : java.util.ArrayList<tdic.util.dataloader.data.admindata.CollectionAgencyData> {
      return processor.CollectionAgencyArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadCollectionAgenciesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminDataUploadCollectionAgenciesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at AdminDataUploadCollectionAgenciesLV.pcf: line 17, column 104
    function highlighted_57 () : java.lang.Boolean {
      return (collectionAgency.Error or collectionAgency.Skipped) and processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 23, column 46
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 29, column 41
    function label_22 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.CollectionAgency.PublicID")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 35, column 41
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.CollectionAgency.Name")
    }
    
    // 'label' attribute on TextCell (id=phone_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 40, column 41
    function label_32 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.CollectionAgency.Phone")
    }
    
    // 'label' attribute on TextCell (id=fax_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 45, column 41
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.CollectionAgency.Fax")
    }
    
    // 'label' attribute on TextCell (id=email_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 50, column 41
    function label_42 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.CollectionAgency.Email")
    }
    
    // 'label' attribute on TextCell (id=address_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 55, column 41
    function label_47 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.CollectionAgency.Address")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 60, column 42
    function label_52 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 29, column 41
    function valueRoot_24 () : java.lang.Object {
      return collectionAgency
    }
    
    // 'value' attribute on TextCell (id=address_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 55, column 41
    function valueRoot_49 () : java.lang.Object {
      return collectionAgency.Address
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 23, column 46
    function value_18 () : java.lang.String {
      return processor.getLoadStatus(collectionAgency)
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 29, column 41
    function value_23 () : java.lang.String {
      return collectionAgency.PublicID
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 35, column 41
    function value_28 () : java.lang.String {
      return collectionAgency.Name
    }
    
    // 'value' attribute on TextCell (id=phone_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 40, column 41
    function value_33 () : java.lang.String {
      return collectionAgency.Phone
    }
    
    // 'value' attribute on TextCell (id=fax_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 45, column 41
    function value_38 () : java.lang.String {
      return collectionAgency.Fax
    }
    
    // 'value' attribute on TextCell (id=email_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 50, column 41
    function value_43 () : java.lang.String {
      return collectionAgency.Email
    }
    
    // 'value' attribute on TextCell (id=address_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 55, column 41
    function value_48 () : java.lang.String {
      return collectionAgency.Address.DisplayName
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 60, column 42
    function value_53 () : java.lang.Boolean {
      return collectionAgency.Exclude
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadCollectionAgenciesLV.pcf: line 23, column 46
    function visible_19 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get collectionAgency () : tdic.util.dataloader.data.admindata.CollectionAgencyData {
      return getIteratedValue(1) as tdic.util.dataloader.data.admindata.CollectionAgencyData
    }
    
    
  }
  
  
}