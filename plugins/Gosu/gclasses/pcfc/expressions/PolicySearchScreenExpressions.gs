package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/PolicySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicySearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/search/PolicySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicySearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get allowsArchiveInclusion () : boolean {
      return getRequireValue("allowsArchiveInclusion", 0) as java.lang.Boolean
    }
    
    property set allowsArchiveInclusion ($arg :  boolean) {
      setRequireValue("allowsArchiveInclusion", 0, $arg)
    }
    
    property get isClearBundle () : boolean {
      return getRequireValue("isClearBundle", 0) as java.lang.Boolean
    }
    
    property set isClearBundle ($arg :  boolean) {
      setRequireValue("isClearBundle", 0, $arg)
    }
    
    property get showHyperlinks () : Boolean {
      return getRequireValue("showHyperlinks", 0) as Boolean
    }
    
    property set showHyperlinks ($arg :  Boolean) {
      setRequireValue("showHyperlinks", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/PolicySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends PolicySearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at PolicySearchScreen.pcf: line 27, column 70
    function def_onEnter_0 (def :  pcf.PolicySearchDV) : void {
      def.onEnter(searchCriteria, allowsArchiveInclusion)
    }
    
    // 'def' attribute on PanelRef at PolicySearchScreen.pcf: line 31, column 92
    function def_onEnter_2 (def :  pcf.PolicySearchResultsLV) : void {
      def.onEnter(policySearchViews, null, false, showHyperlinks, false)
    }
    
    // 'def' attribute on PanelRef at PolicySearchScreen.pcf: line 27, column 70
    function def_refreshVariables_1 (def :  pcf.PolicySearchDV) : void {
      def.refreshVariables(searchCriteria, allowsArchiveInclusion)
    }
    
    // 'def' attribute on PanelRef at PolicySearchScreen.pcf: line 31, column 92
    function def_refreshVariables_3 (def :  pcf.PolicySearchResultsLV) : void {
      def.refreshVariables(policySearchViews, null, false, showHyperlinks, false)
    }
    
    // 'maxSearchResults' attribute on SearchPanel at PolicySearchScreen.pcf: line 25, column 84
    function maxSearchResults_4 () : java.lang.Object {
      return gw.api.system.BCConfigParameters.MaxSearchResults.Value
    }
    
    // 'searchCriteria' attribute on SearchPanel at PolicySearchScreen.pcf: line 25, column 84
    function searchCriteria_6 () : gw.search.PolicySearchCriteria {
      return new gw.search.PolicySearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at PolicySearchScreen.pcf: line 25, column 84
    function search_5 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria, StringCriterionMode.TC_STARTSWITH)
    }
    
    property get policySearchViews () : gw.api.database.IQueryBeanResult<PolicySearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<PolicySearchView>
    }
    
    property get searchCriteria () : gw.search.PolicySearchCriteria {
      return getCriteriaValue(1) as gw.search.PolicySearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.PolicySearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}