package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/PaymentDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentDetailsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/PaymentDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentDetailsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (paymentReceipt :  PaymentReceipt) : int {
      return 0
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at PaymentDetailsPopup.pcf: line 21, column 44
    function currency_2 () : typekey.Currency {
      return paymentReceipt.Amount.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at PaymentDetailsPopup.pcf: line 21, column 44
    function valueRoot_1 () : java.lang.Object {
      return paymentReceipt
    }
    
    // 'value' attribute on TypeKeyInput (id=Method_Input) at PaymentDetailsPopup.pcf: line 26, column 48
    function valueRoot_5 () : java.lang.Object {
      return paymentReceipt.PaymentInstrument
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at PaymentDetailsPopup.pcf: line 21, column 44
    function value_0 () : gw.pl.currency.MonetaryAmount {
      return paymentReceipt.Amount
    }
    
    // 'value' attribute on TypeKeyInput (id=Method_Input) at PaymentDetailsPopup.pcf: line 26, column 48
    function value_4 () : typekey.PaymentMethod {
      return paymentReceipt.PaymentInstrument.PaymentMethod
    }
    
    // 'value' attribute on TextInput (id=RefNumber_Input) at PaymentDetailsPopup.pcf: line 30, column 47
    function value_7 () : java.lang.String {
      return paymentReceipt.RefNumber
    }
    
    override property get CurrentLocation () : pcf.PaymentDetailsPopup {
      return super.CurrentLocation as pcf.PaymentDetailsPopup
    }
    
    property get paymentReceipt () : PaymentReceipt {
      return getVariableValue("paymentReceipt", 0) as PaymentReceipt
    }
    
    property set paymentReceipt ($arg :  PaymentReceipt) {
      setVariableValue("paymentReceipt", 0, $arg)
    }
    
    
  }
  
  
}