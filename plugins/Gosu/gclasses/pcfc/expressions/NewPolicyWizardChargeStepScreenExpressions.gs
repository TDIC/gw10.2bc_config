package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/NewPolicyWizardChargeStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewPolicyWizardChargeStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/NewPolicyWizardChargeStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewPolicyWizardChargeStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=DiscountedPayment_Input) at NewPolicyWizardChargeStepScreen.pcf: line 39, column 60
    function currency_7 () : typekey.Currency {
      return policyPeriod.Currency
    }
    
    // 'def' attribute on PanelRef at NewPolicyWizardChargeStepScreen.pcf: line 23, column 115
    function def_onEnter_0 (def :  pcf.PolicyAddChargesListDetailPanel) : void {
      def.onEnter(billingInstruction, policyPeriod, chargeToInvoicingOverridesViewMap)
    }
    
    // 'def' attribute on PanelRef at NewPolicyWizardChargeStepScreen.pcf: line 23, column 115
    function def_refreshVariables_1 (def :  pcf.PolicyAddChargesListDetailPanel) : void {
      def.refreshVariables(billingInstruction, policyPeriod, chargeToInvoicingOverridesViewMap)
    }
    
    // 'value' attribute on DateInput (id=FullPayDate_Input) at NewPolicyWizardChargeStepScreen.pcf: line 45, column 54
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.FullPayDiscountUntil = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=DiscountedPayment_Input) at NewPolicyWizardChargeStepScreen.pcf: line 39, column 60
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.DiscountedPaymentThreshold = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'required' attribute on MonetaryAmountInput (id=DiscountedPayment_Input) at NewPolicyWizardChargeStepScreen.pcf: line 39, column 60
    function required_3 () : java.lang.Boolean {
      return policyPeriod.EligibleForFullPayDiscount
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=DiscountedPayment_Input) at NewPolicyWizardChargeStepScreen.pcf: line 39, column 60
    function validationExpression_2 () : java.lang.Object {
      return view.isDiscountedPaymentValid() ? null : DisplayKey.get("Web.NewPolicyReportingChargeDV.DiscountedPaymentNotValid")
    }
    
    // 'value' attribute on MonetaryAmountInput (id=DiscountedPayment_Input) at NewPolicyWizardChargeStepScreen.pcf: line 39, column 60
    function valueRoot_6 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on DateInput (id=FullPayDate_Input) at NewPolicyWizardChargeStepScreen.pcf: line 45, column 54
    function value_11 () : java.util.Date {
      return policyPeriod.FullPayDiscountUntil
    }
    
    // 'value' attribute on MonetaryAmountInput (id=DiscountedPayment_Input) at NewPolicyWizardChargeStepScreen.pcf: line 39, column 60
    function value_4 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.DiscountedPaymentThreshold
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get billingInstruction () : NewPlcyPeriodBI {
      return getRequireValue("billingInstruction", 0) as NewPlcyPeriodBI
    }
    
    property set billingInstruction ($arg :  NewPlcyPeriodBI) {
      setRequireValue("billingInstruction", 0, $arg)
    }
    
    property get chargeToInvoicingOverridesViewMap () : java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView> {
      return getRequireValue("chargeToInvoicingOverridesViewMap", 0) as java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>
    }
    
    property set chargeToInvoicingOverridesViewMap ($arg :  java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>) {
      setRequireValue("chargeToInvoicingOverridesViewMap", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get view () : gw.web.policy.PolicyWizardChargeStepScreenView {
      return getRequireValue("view", 0) as gw.web.policy.PolicyWizardChargeStepScreenView
    }
    
    property set view ($arg :  gw.web.policy.PolicyWizardChargeStepScreenView) {
      setRequireValue("view", 0, $arg)
    }
    
    
  }
  
  
}