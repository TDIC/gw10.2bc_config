package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/commission/CommissionSubPlanDetailCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionSubPlanDetailCVExpressions {
  @javax.annotation.Generated("config/web/pcf/commission/CommissionSubPlanDetailCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionSubPlanDetailCVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on BooleanRadioInput (id=InitialBusiness_Input) at CommissionSubPlanDetailCV.pcf: line 276, column 81
    function available_106 () : java.lang.Boolean {
      return !conditionalCommissionSubPlan.AllTerms
    }
    
    // 'available' attribute on ListViewInput at CommissionSubPlanDetailCV.pcf: line 375, column 31
    function available_180 () : java.lang.Boolean {
      return !conditionalCommissionSubPlan.AllJurisdictions
    }
    
    // 'def' attribute on PanelRef at CommissionSubPlanDetailCV.pcf: line 70, column 149
    function def_onEnter_24 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(commissionSubPlan, { "Name"}, { DisplayKey.get("Web.CommissionSubPlan.General.SubPlanName") })
    }
    
    // 'def' attribute on PanelRef at CommissionSubPlanDetailCV.pcf: line 70, column 149
    function def_refreshVariables_25 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(commissionSubPlan, { "Name"}, { DisplayKey.get("Web.CommissionSubPlan.General.SubPlanName") })
    }
    
    // 'value' attribute on BooleanRadioInput (id=AllTerms_Input) at CommissionSubPlanDetailCV.pcf: line 266, column 59
    function defaultSetter_103 (__VALUE_TO_SET :  java.lang.Object) : void {
      conditionalCommissionSubPlan.AllTerms = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=InitialBusiness_Input) at CommissionSubPlanDetailCV.pcf: line 276, column 81
    function defaultSetter_108 (__VALUE_TO_SET :  java.lang.Object) : void {
      conditionalCommissionSubPlan.getRenewalRange(0, 1).Covered = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=FirstRenewal_Input) at CommissionSubPlanDetailCV.pcf: line 282, column 81
    function defaultSetter_114 (__VALUE_TO_SET :  java.lang.Object) : void {
      conditionalCommissionSubPlan.getRenewalRange(1, 2).Covered = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SecondRenewal_Input) at CommissionSubPlanDetailCV.pcf: line 288, column 81
    function defaultSetter_120 (__VALUE_TO_SET :  java.lang.Object) : void {
      conditionalCommissionSubPlan.getRenewalRange(2, 3).Covered = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=ThirdRenewal_Input) at CommissionSubPlanDetailCV.pcf: line 294, column 81
    function defaultSetter_126 (__VALUE_TO_SET :  java.lang.Object) : void {
      conditionalCommissionSubPlan.getRenewalRange(3, 4).Covered = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Thereafter_Input) at CommissionSubPlanDetailCV.pcf: line 300, column 87
    function defaultSetter_132 (__VALUE_TO_SET :  java.lang.Object) : void {
      conditionalCommissionSubPlan.getRenewalRangeOnOrAfter(4).Covered = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=AllowAssignedRisk_Input) at CommissionSubPlanDetailCV.pcf: line 310, column 58
    function defaultSetter_137 (__VALUE_TO_SET :  java.lang.Object) : void {
      conditionalCommissionSubPlan.AssignedRisk = (__VALUE_TO_SET as typekey.AssignedRiskRestriction)
    }
    
    // 'value' attribute on BooleanRadioInput (id=AllSegments_Input) at CommissionSubPlanDetailCV.pcf: line 318, column 62
    function defaultSetter_142 (__VALUE_TO_SET :  java.lang.Object) : void {
      conditionalCommissionSubPlan.AllSegments = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=AllEvaluations_Input) at CommissionSubPlanDetailCV.pcf: line 342, column 65
    function defaultSetter_156 (__VALUE_TO_SET :  java.lang.Object) : void {
      conditionalCommissionSubPlan.AllEvaluations = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=CommissionsPayable_Input) at CommissionSubPlanDetailCV.pcf: line 61, column 50
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      commissionSubPlan.PayableCriteria = (__VALUE_TO_SET as typekey.PayableCriteria)
    }
    
    // 'value' attribute on BooleanRadioInput (id=AllJurisdictions_Input) at CommissionSubPlanDetailCV.pcf: line 369, column 67
    function defaultSetter_169 (__VALUE_TO_SET :  java.lang.Object) : void {
      conditionalCommissionSubPlan.AllJurisdictions = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=AllUWCompanies_Input) at CommissionSubPlanDetailCV.pcf: line 411, column 65
    function defaultSetter_183 (__VALUE_TO_SET :  java.lang.Object) : void {
      conditionalCommissionSubPlan.AllUWCompanies = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SuspendForDelinquency_Input) at CommissionSubPlanDetailCV.pcf: line 66, column 62
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      commissionSubPlan.SuspendForDelinquency = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=SubPlanName_Input) at CommissionSubPlanDetailCV.pcf: line 34, column 44
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      commissionSubPlan.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioInput (id=AllLOBCodes_Input) at CommissionSubPlanDetailCV.pcf: line 242, column 62
    function defaultSetter_89 (__VALUE_TO_SET :  java.lang.Object) : void {
      conditionalCommissionSubPlan.AllLOBCodes = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'editable' attribute on TextInput (id=SubPlanName_Input) at CommissionSubPlanDetailCV.pcf: line 34, column 44
    function editable_2 () : java.lang.Boolean {
      return commissionSubPlan typeis CondCmsnSubPlan
    }
    
    // 'editable' attribute on DetailViewPanel at CommissionSubPlanDetailCV.pcf: line 23, column 61
    function editable_23 () : java.lang.Boolean {
      return subPlanNotInUse or commissionSubPlan.New
    }
    
    // 'initialValue' attribute on Variable at CommissionSubPlanDetailCV.pcf: line 13, column 57
    function initialValue_0 () : gw.api.web.accounting.ChargePatternHelper {
      return new gw.api.web.accounting.ChargePatternHelper()
    }
    
    // 'initialValue' attribute on Variable at CommissionSubPlanDetailCV.pcf: line 18, column 23
    function initialValue_1 () : boolean {
      return not commissionSubPlan.CommissionPlan.InUse
    }
    
    // 'onChange' attribute on PostOnChange at CommissionSubPlanDetailCV.pcf: line 269, column 137
    function onChange_101 () : void {
      if (conditionalCommissionSubPlan.AllTerms) conditionalCommissionSubPlan.getRenewalRange(0, -1).Covered = true
    }
    
    // 'onChange' attribute on PostOnChange at CommissionSubPlanDetailCV.pcf: line 321, column 167
    function onChange_140 () : void {
      if (conditionalCommissionSubPlan.AllSegments) conditionalCommissionSubPlan.SelectedSegments.each( \ segment-> {segment.Selected = true})
    }
    
    // 'onChange' attribute on PostOnChange at CommissionSubPlanDetailCV.pcf: line 345, column 180
    function onChange_154 () : void {
      if (conditionalCommissionSubPlan.AllEvaluations) conditionalCommissionSubPlan.SelectedEvaluations.each( \ evaluation -> {evaluation.Selected = true})
    }
    
    // 'onChange' attribute on PostOnChange at CommissionSubPlanDetailCV.pcf: line 414, column 172
    function onChange_181 () : void {
      if (conditionalCommissionSubPlan.AllUWCompanies) conditionalCommissionSubPlan.SelectedUWCompanies.each( \ uwComp -> {uwComp.Selected = true})
    }
    
    // 'onChange' attribute on PostOnChange at CommissionSubPlanDetailCV.pcf: line 245, column 152
    function onChange_87 () : void {
      if (conditionalCommissionSubPlan.AllLOBCodes) conditionalCommissionSubPlan.LOBCodes.each( \ lob -> {lob.Selected = true})
    }
    
    // 'value' attribute on TypeKeyCell (id=Jurisdiction_Cell) at CommissionSubPlanDetailCV.pcf: line 399, column 55
    function sortValue_172 (condCmsnSubPlanJurisdiction :  entity.CondCmsnSubPlanJurisdiction) : java.lang.Object {
      return condCmsnSubPlanJurisdiction.Jurisdiction
    }
    
    // 'value' attribute on RangeCell (id=Item_Cell) at CommissionSubPlanDetailCV.pcf: line 105, column 55
    function sortValue_26 (commissionableChargeItem :  entity.CommissionableChargeItem) : java.lang.Object {
      return commissionableChargeItem.ChargePattern
    }
    
    // 'value' attribute on RangeCell (id=ChargePattern_Cell) at CommissionSubPlanDetailCV.pcf: line 147, column 55
    function sortValue_38 (specialRate :  entity.CommissionSubPlanChargePatternRate) : java.lang.Object {
      return specialRate.ChargePattern
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at CommissionSubPlanDetailCV.pcf: line 155, column 53
    function sortValue_39 (specialRate :  entity.CommissionSubPlanChargePatternRate) : java.lang.Object {
      return specialRate.Role
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at CommissionSubPlanDetailCV.pcf: line 162, column 55
    function sortValue_40 (specialRate :  entity.CommissionSubPlanChargePatternRate) : java.lang.Object {
      return specialRate.Rate
    }
    
    // 'toAdd' attribute on RowIterator at CommissionSubPlanDetailCV.pcf: line 389, column 66
    function toAdd_177 (condCmsnSubPlanJurisdiction :  entity.CondCmsnSubPlanJurisdiction) : void {
      conditionalCommissionSubPlan.addToIncludedJurisdictions(condCmsnSubPlanJurisdiction)
    }
    
    // 'toAdd' attribute on RowIterator at CommissionSubPlanDetailCV.pcf: line 94, column 63
    function toAdd_34 (commissionableChargeItem :  entity.CommissionableChargeItem) : void {
      commissionSubPlan.addToCommissionableChargeItems(commissionableChargeItem)
    }
    
    // 'toAdd' attribute on RowIterator at CommissionSubPlanDetailCV.pcf: line 136, column 73
    function toAdd_58 (specialRate :  entity.CommissionSubPlanChargePatternRate) : void {
      commissionSubPlan.addToSpecialChargePatternRates(specialRate)
    }
    
    // 'toRemove' attribute on RowIterator at CommissionSubPlanDetailCV.pcf: line 389, column 66
    function toRemove_178 (condCmsnSubPlanJurisdiction :  entity.CondCmsnSubPlanJurisdiction) : void {
      conditionalCommissionSubPlan.removeFromIncludedJurisdictions(condCmsnSubPlanJurisdiction)
    }
    
    // 'toRemove' attribute on RowIterator at CommissionSubPlanDetailCV.pcf: line 94, column 63
    function toRemove_35 (commissionableChargeItem :  entity.CommissionableChargeItem) : void {
      commissionSubPlan.removeFromCommissionableChargeItems(commissionableChargeItem)
    }
    
    // 'toRemove' attribute on RowIterator at CommissionSubPlanDetailCV.pcf: line 136, column 73
    function toRemove_59 (specialRate :  entity.CommissionSubPlanChargePatternRate) : void {
      commissionSubPlan.removeFromSpecialChargePatternRates(specialRate)
    }
    
    // 'value' attribute on BooleanRadioInput (id=InitialBusiness_Input) at CommissionSubPlanDetailCV.pcf: line 276, column 81
    function valueRoot_109 () : java.lang.Object {
      return conditionalCommissionSubPlan.getRenewalRange(0, 1)
    }
    
    // 'value' attribute on BooleanRadioInput (id=FirstRenewal_Input) at CommissionSubPlanDetailCV.pcf: line 282, column 81
    function valueRoot_115 () : java.lang.Object {
      return conditionalCommissionSubPlan.getRenewalRange(1, 2)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SecondRenewal_Input) at CommissionSubPlanDetailCV.pcf: line 288, column 81
    function valueRoot_121 () : java.lang.Object {
      return conditionalCommissionSubPlan.getRenewalRange(2, 3)
    }
    
    // 'value' attribute on BooleanRadioInput (id=ThirdRenewal_Input) at CommissionSubPlanDetailCV.pcf: line 294, column 81
    function valueRoot_127 () : java.lang.Object {
      return conditionalCommissionSubPlan.getRenewalRange(3, 4)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Thereafter_Input) at CommissionSubPlanDetailCV.pcf: line 300, column 87
    function valueRoot_133 () : java.lang.Object {
      return conditionalCommissionSubPlan.getRenewalRangeOnOrAfter(4)
    }
    
    // 'value' attribute on TextInput (id=SubPlanName_Input) at CommissionSubPlanDetailCV.pcf: line 34, column 44
    function valueRoot_5 () : java.lang.Object {
      return commissionSubPlan
    }
    
    // 'value' attribute on BooleanRadioInput (id=AllLOBCodes_Input) at CommissionSubPlanDetailCV.pcf: line 242, column 62
    function valueRoot_90 () : java.lang.Object {
      return conditionalCommissionSubPlan
    }
    
    // 'value' attribute on InputIterator (id=LOBCodes) at CommissionSubPlanDetailCV.pcf: line 251, column 68
    function value_100 () : gw.api.domain.commission.ExcludedLOBCode[] {
      return conditionalCommissionSubPlan.LOBCodes
    }
    
    // 'value' attribute on BooleanRadioInput (id=AllTerms_Input) at CommissionSubPlanDetailCV.pcf: line 266, column 59
    function value_102 () : java.lang.Boolean {
      return conditionalCommissionSubPlan.AllTerms
    }
    
    // 'value' attribute on BooleanRadioInput (id=InitialBusiness_Input) at CommissionSubPlanDetailCV.pcf: line 276, column 81
    function value_107 () : java.lang.Boolean {
      return conditionalCommissionSubPlan.getRenewalRange(0, 1).Covered
    }
    
    // 'value' attribute on BooleanRadioInput (id=FirstRenewal_Input) at CommissionSubPlanDetailCV.pcf: line 282, column 81
    function value_113 () : java.lang.Boolean {
      return conditionalCommissionSubPlan.getRenewalRange(1, 2).Covered
    }
    
    // 'value' attribute on BooleanRadioInput (id=SecondRenewal_Input) at CommissionSubPlanDetailCV.pcf: line 288, column 81
    function value_119 () : java.lang.Boolean {
      return conditionalCommissionSubPlan.getRenewalRange(2, 3).Covered
    }
    
    // 'value' attribute on BooleanRadioInput (id=ThirdRenewal_Input) at CommissionSubPlanDetailCV.pcf: line 294, column 81
    function value_125 () : java.lang.Boolean {
      return conditionalCommissionSubPlan.getRenewalRange(3, 4).Covered
    }
    
    // 'value' attribute on BooleanRadioInput (id=Thereafter_Input) at CommissionSubPlanDetailCV.pcf: line 300, column 87
    function value_131 () : java.lang.Boolean {
      return conditionalCommissionSubPlan.getRenewalRangeOnOrAfter(4).Covered
    }
    
    // 'value' attribute on TypeKeyInput (id=AllowAssignedRisk_Input) at CommissionSubPlanDetailCV.pcf: line 310, column 58
    function value_136 () : typekey.AssignedRiskRestriction {
      return conditionalCommissionSubPlan.AssignedRisk
    }
    
    // 'value' attribute on InputIterator at CommissionSubPlanDetailCV.pcf: line 43, column 61
    function value_14 () : entity.CommissionSubPlanRateEntry[] {
      return commissionSubPlan.RateEntries
    }
    
    // 'value' attribute on BooleanRadioInput (id=AllSegments_Input) at CommissionSubPlanDetailCV.pcf: line 318, column 62
    function value_141 () : java.lang.Boolean {
      return conditionalCommissionSubPlan.AllSegments
    }
    
    // 'value' attribute on TypeKeyInput (id=CommissionsPayable_Input) at CommissionSubPlanDetailCV.pcf: line 61, column 50
    function value_15 () : typekey.PayableCriteria {
      return commissionSubPlan.PayableCriteria
    }
    
    // 'value' attribute on InputIterator (id=Segments) at CommissionSubPlanDetailCV.pcf: line 327, column 68
    function value_153 () : gw.api.domain.commission.SelectedSegment[] {
      return conditionalCommissionSubPlan.SelectedSegments
    }
    
    // 'value' attribute on BooleanRadioInput (id=AllEvaluations_Input) at CommissionSubPlanDetailCV.pcf: line 342, column 65
    function value_155 () : java.lang.Boolean {
      return conditionalCommissionSubPlan.AllEvaluations
    }
    
    // 'value' attribute on InputIterator (id=Evaluations) at CommissionSubPlanDetailCV.pcf: line 351, column 71
    function value_167 () : gw.api.domain.commission.SelectedEvaluation[] {
      return conditionalCommissionSubPlan.SelectedEvaluations
    }
    
    // 'value' attribute on BooleanRadioInput (id=AllJurisdictions_Input) at CommissionSubPlanDetailCV.pcf: line 369, column 67
    function value_168 () : java.lang.Boolean {
      return conditionalCommissionSubPlan.AllJurisdictions
    }
    
    // 'value' attribute on RowIterator at CommissionSubPlanDetailCV.pcf: line 389, column 66
    function value_179 () : entity.CondCmsnSubPlanJurisdiction[] {
      return conditionalCommissionSubPlan.IncludedJurisdictions
    }
    
    // 'value' attribute on BooleanRadioInput (id=AllUWCompanies_Input) at CommissionSubPlanDetailCV.pcf: line 411, column 65
    function value_182 () : java.lang.Boolean {
      return conditionalCommissionSubPlan.AllUWCompanies
    }
    
    // 'value' attribute on BooleanRadioInput (id=SuspendForDelinquency_Input) at CommissionSubPlanDetailCV.pcf: line 66, column 62
    function value_19 () : java.lang.Boolean {
      return commissionSubPlan.SuspendForDelinquency
    }
    
    // 'value' attribute on InputIterator (id=UWCompanies) at CommissionSubPlanDetailCV.pcf: line 420, column 70
    function value_194 () : gw.api.domain.commission.SelectedUWCompany[] {
      return conditionalCommissionSubPlan.SelectedUWCompanies
    }
    
    // 'value' attribute on TextInput (id=SubPlanName_Input) at CommissionSubPlanDetailCV.pcf: line 34, column 44
    function value_3 () : java.lang.String {
      return commissionSubPlan.Name
    }
    
    // 'value' attribute on RowIterator at CommissionSubPlanDetailCV.pcf: line 94, column 63
    function value_36 () : entity.CommissionableChargeItem[] {
      return commissionSubPlan.CommissionableChargeItems
    }
    
    // 'value' attribute on RowIterator at CommissionSubPlanDetailCV.pcf: line 136, column 73
    function value_60 () : entity.CommissionSubPlanChargePatternRate[] {
      return commissionSubPlan.SpecialChargePatternRates
    }
    
    // 'value' attribute on BooleanRadioInput (id=AllLOBCodes_Input) at CommissionSubPlanDetailCV.pcf: line 242, column 62
    function value_88 () : java.lang.Boolean {
      return conditionalCommissionSubPlan.AllLOBCodes
    }
    
    property get chargePatternHelper () : gw.api.web.accounting.ChargePatternHelper {
      return getVariableValue("chargePatternHelper", 0) as gw.api.web.accounting.ChargePatternHelper
    }
    
    property set chargePatternHelper ($arg :  gw.api.web.accounting.ChargePatternHelper) {
      setVariableValue("chargePatternHelper", 0, $arg)
    }
    
    property get commissionSubPlan () : CommissionSubPlan {
      return getRequireValue("commissionSubPlan", 0) as CommissionSubPlan
    }
    
    property set commissionSubPlan ($arg :  CommissionSubPlan) {
      setRequireValue("commissionSubPlan", 0, $arg)
    }
    
    property get subPlanNotInUse () : boolean {
      return getVariableValue("subPlanNotInUse", 0) as java.lang.Boolean
    }
    
    property set subPlanNotInUse ($arg :  boolean) {
      setVariableValue("subPlanNotInUse", 0, $arg)
    }
    
    
    property get conditionalCommissionSubPlan() : CondCmsnSubPlan {
            return commissionSubPlan as CondCmsnSubPlan;
          }
    
    function getErrorMessageIfDuplicateSpecialRate( specialRate : CommissionSubPlanChargePatternRate) : String {
      return isDuplicated( specialRate )
      ? DisplayKey.get("Web.CommissionSubPlan.SpecialChargePatternRates.ErrorDuplicateChargePatternRate",  specialRate.ChargePattern, specialRate.Role )
      : null;
    }
    
    function isDuplicated( specialRate : CommissionSubPlanChargePatternRate ) : Boolean {
      return commissionSubPlan.SpecialChargePatternRates.countWhere(\ eachRate -> areSameSpecialRates(specialRate, eachRate)) > 1;
    }
    
    function areSameSpecialRates(specialRate1 : CommissionSubPlanChargePatternRate, specialRate2 : CommissionSubPlanChargePatternRate) : Boolean {
      return specialRate1.ChargePattern == specialRate2.ChargePattern
        && specialRate1.Role == specialRate2.Role
    }
          
        
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/commission/CommissionSubPlanDetailCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanelExpressionsImpl extends CommissionSubPlanDetailCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=EditButton) at CommissionSubPlanDetailCV.pcf: line 198, column 66
    function action_71 () : void {
      EditIncentivesPopup.push(commissionSubPlan, newIncentiveType)
    }
    
    // 'action' attribute on ToolbarButton (id=EditButton) at CommissionSubPlanDetailCV.pcf: line 198, column 66
    function action_dest_72 () : pcf.api.Destination {
      return pcf.EditIncentivesPopup.createDestination(commissionSubPlan, newIncentiveType)
    }
    
    // 'available' attribute on ToolbarButton (id=EditButton) at CommissionSubPlanDetailCV.pcf: line 198, column 66
    function available_70 () : java.lang.Boolean {
      return newIncentiveType != null
    }
    
    // 'value' attribute on ToolbarRangeInput (id=IncentiveType_Input) at CommissionSubPlanDetailCV.pcf: line 190, column 54
    function defaultSetter_64 (__VALUE_TO_SET :  java.lang.Object) : void {
      newIncentiveType = (__VALUE_TO_SET as typekey.Incentive)
    }
    
    // 'editable' attribute on DetailViewPanel at CommissionSubPlanDetailCV.pcf: line 174, column 61
    function editable_86 () : java.lang.Boolean {
      return subPlanNotInUse or commissionSubPlan.New
    }
    
    // 'value' attribute on TextCell (id=IncentiveName_Cell) at CommissionSubPlanDetailCV.pcf: line 211, column 52
    function sortValue_73 (incentive :  entity.Incentive) : java.lang.Object {
      return incentive.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at CommissionSubPlanDetailCV.pcf: line 215, column 52
    function sortValue_74 (incentive :  entity.Incentive) : java.lang.Object {
      return incentive.Description
    }
    
    // 'value' attribute on TextCell (id=Application_Cell) at CommissionSubPlanDetailCV.pcf: line 219, column 52
    function sortValue_75 (incentive :  entity.Incentive) : java.lang.Object {
      return incentive.Application
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=IncentiveType_Input) at CommissionSubPlanDetailCV.pcf: line 190, column 54
    function valueRange_65 () : java.lang.Object {
      return commissionSubPlan.AvailableIncentiveTypes
    }
    
    // 'value' attribute on ToolbarRangeInput (id=IncentiveType_Input) at CommissionSubPlanDetailCV.pcf: line 190, column 54
    function value_63 () : typekey.Incentive {
      return newIncentiveType
    }
    
    // 'value' attribute on RowIterator at CommissionSubPlanDetailCV.pcf: line 206, column 48
    function value_85 () : entity.Incentive[] {
      return commissionSubPlan.Incentives
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=IncentiveType_Input) at CommissionSubPlanDetailCV.pcf: line 190, column 54
    function verifyValueRangeIsAllowedType_66 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=IncentiveType_Input) at CommissionSubPlanDetailCV.pcf: line 190, column 54
    function verifyValueRangeIsAllowedType_66 ($$arg :  typekey.Incentive[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=IncentiveType_Input) at CommissionSubPlanDetailCV.pcf: line 190, column 54
    function verifyValueRange_67 () : void {
      var __valueRangeArg = commissionSubPlan.AvailableIncentiveTypes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_66(__valueRangeArg)
    }
    
    // 'visible' attribute on ToolbarRangeInput (id=IncentiveType_Input) at CommissionSubPlanDetailCV.pcf: line 190, column 54
    function visible_62 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    property get newIncentiveType () : typekey.Incentive {
      return getVariableValue("newIncentiveType", 1) as typekey.Incentive
    }
    
    property set newIncentiveType ($arg :  typekey.Incentive) {
      setVariableValue("newIncentiveType", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/commission/CommissionSubPlanDetailCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends CommissionSubPlanDetailCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeCell (id=Item_Cell) at CommissionSubPlanDetailCV.pcf: line 105, column 55
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      commissionableChargeItem.ChargePattern = (__VALUE_TO_SET as entity.ChargePattern)
    }
    
    // 'valueRange' attribute on RangeCell (id=Item_Cell) at CommissionSubPlanDetailCV.pcf: line 105, column 55
    function valueRange_30 () : java.lang.Object {
      return chargePatternHelper.getChargePatterns( entity.PolicyPeriod, ChargeCategory.TC_PREMIUM )
    }
    
    // 'value' attribute on RangeCell (id=Item_Cell) at CommissionSubPlanDetailCV.pcf: line 105, column 55
    function valueRoot_29 () : java.lang.Object {
      return commissionableChargeItem
    }
    
    // 'value' attribute on RangeCell (id=Item_Cell) at CommissionSubPlanDetailCV.pcf: line 105, column 55
    function value_27 () : entity.ChargePattern {
      return commissionableChargeItem.ChargePattern
    }
    
    // 'valueRange' attribute on RangeCell (id=Item_Cell) at CommissionSubPlanDetailCV.pcf: line 105, column 55
    function verifyValueRangeIsAllowedType_31 ($$arg :  entity.ChargePattern[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Item_Cell) at CommissionSubPlanDetailCV.pcf: line 105, column 55
    function verifyValueRangeIsAllowedType_31 ($$arg :  gw.api.database.IQueryBeanResult<entity.ChargePattern>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Item_Cell) at CommissionSubPlanDetailCV.pcf: line 105, column 55
    function verifyValueRangeIsAllowedType_31 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Item_Cell) at CommissionSubPlanDetailCV.pcf: line 105, column 55
    function verifyValueRange_32 () : void {
      var __valueRangeArg = chargePatternHelper.getChargePatterns( entity.PolicyPeriod, ChargeCategory.TC_PREMIUM )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_31(__valueRangeArg)
    }
    
    property get commissionableChargeItem () : entity.CommissionableChargeItem {
      return getIteratedValue(1) as entity.CommissionableChargeItem
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/commission/CommissionSubPlanDetailCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends CommissionSubPlanDetailCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeCell (id=ChargePattern_Cell) at CommissionSubPlanDetailCV.pcf: line 147, column 55
    function defaultSetter_43 (__VALUE_TO_SET :  java.lang.Object) : void {
      specialRate.ChargePattern = (__VALUE_TO_SET as entity.ChargePattern)
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at CommissionSubPlanDetailCV.pcf: line 155, column 53
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      specialRate.Role = (__VALUE_TO_SET as typekey.PolicyRole)
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at CommissionSubPlanDetailCV.pcf: line 162, column 55
    function defaultSetter_55 (__VALUE_TO_SET :  java.lang.Object) : void {
      specialRate.Rate = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'validationExpression' attribute on RangeCell (id=ChargePattern_Cell) at CommissionSubPlanDetailCV.pcf: line 147, column 55
    function validationExpression_41 () : java.lang.Object {
      return getErrorMessageIfDuplicateSpecialRate(specialRate)
    }
    
    // 'valueRange' attribute on RangeCell (id=ChargePattern_Cell) at CommissionSubPlanDetailCV.pcf: line 147, column 55
    function valueRange_45 () : java.lang.Object {
      return chargePatternHelper.getChargePatterns( entity.PolicyPeriod )
    }
    
    // 'value' attribute on RangeCell (id=ChargePattern_Cell) at CommissionSubPlanDetailCV.pcf: line 147, column 55
    function valueRoot_44 () : java.lang.Object {
      return specialRate
    }
    
    // 'value' attribute on RangeCell (id=ChargePattern_Cell) at CommissionSubPlanDetailCV.pcf: line 147, column 55
    function value_42 () : entity.ChargePattern {
      return specialRate.ChargePattern
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at CommissionSubPlanDetailCV.pcf: line 155, column 53
    function value_50 () : typekey.PolicyRole {
      return specialRate.Role
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at CommissionSubPlanDetailCV.pcf: line 162, column 55
    function value_54 () : java.math.BigDecimal {
      return specialRate.Rate
    }
    
    // 'valueRange' attribute on RangeCell (id=ChargePattern_Cell) at CommissionSubPlanDetailCV.pcf: line 147, column 55
    function verifyValueRangeIsAllowedType_46 ($$arg :  entity.ChargePattern[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ChargePattern_Cell) at CommissionSubPlanDetailCV.pcf: line 147, column 55
    function verifyValueRangeIsAllowedType_46 ($$arg :  gw.api.database.IQueryBeanResult<entity.ChargePattern>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ChargePattern_Cell) at CommissionSubPlanDetailCV.pcf: line 147, column 55
    function verifyValueRangeIsAllowedType_46 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ChargePattern_Cell) at CommissionSubPlanDetailCV.pcf: line 147, column 55
    function verifyValueRange_47 () : void {
      var __valueRangeArg = chargePatternHelper.getChargePatterns( entity.PolicyPeriod )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_46(__valueRangeArg)
    }
    
    property get specialRate () : entity.CommissionSubPlanChargePatternRate {
      return getIteratedValue(1) as entity.CommissionSubPlanChargePatternRate
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/commission/CommissionSubPlanDetailCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends DetailViewPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=IncentiveName_Cell) at CommissionSubPlanDetailCV.pcf: line 211, column 52
    function valueRoot_77 () : java.lang.Object {
      return incentive
    }
    
    // 'value' attribute on TextCell (id=IncentiveName_Cell) at CommissionSubPlanDetailCV.pcf: line 211, column 52
    function value_76 () : java.lang.String {
      return incentive.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at CommissionSubPlanDetailCV.pcf: line 215, column 52
    function value_79 () : java.lang.String {
      return incentive.Description
    }
    
    // 'value' attribute on TextCell (id=Application_Cell) at CommissionSubPlanDetailCV.pcf: line 219, column 52
    function value_82 () : java.lang.String {
      return incentive.Application
    }
    
    property get incentive () : entity.Incentive {
      return getIteratedValue(2) as entity.Incentive
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/commission/CommissionSubPlanDetailCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry5ExpressionsImpl extends CommissionSubPlanDetailCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on BooleanRadioInput (id=LOBCode_Input) at CommissionSubPlanDetailCV.pcf: line 257, column 41
    function available_92 () : java.lang.Boolean {
      return !conditionalCommissionSubPlan.AllLOBCodes
    }
    
    // 'value' attribute on BooleanRadioInput (id=LOBCode_Input) at CommissionSubPlanDetailCV.pcf: line 257, column 41
    function defaultSetter_95 (__VALUE_TO_SET :  java.lang.Object) : void {
      lobCode.Selected = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'label' attribute on BooleanRadioInput (id=LOBCode_Input) at CommissionSubPlanDetailCV.pcf: line 257, column 41
    function label_93 () : java.lang.Object {
      return lobCode
    }
    
    // 'value' attribute on BooleanRadioInput (id=LOBCode_Input) at CommissionSubPlanDetailCV.pcf: line 257, column 41
    function value_94 () : java.lang.Boolean {
      return lobCode.Selected
    }
    
    property get lobCode () : gw.api.domain.commission.ExcludedLOBCode {
      return getIteratedValue(1) as gw.api.domain.commission.ExcludedLOBCode
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/commission/CommissionSubPlanDetailCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry6ExpressionsImpl extends CommissionSubPlanDetailCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on BooleanRadioInput (id=Segment_Input) at CommissionSubPlanDetailCV.pcf: line 333, column 48
    function available_145 () : java.lang.Boolean {
      return !conditionalCommissionSubPlan.AllSegments
    }
    
    // 'value' attribute on BooleanRadioInput (id=Segment_Input) at CommissionSubPlanDetailCV.pcf: line 333, column 48
    function defaultSetter_148 (__VALUE_TO_SET :  java.lang.Object) : void {
      accountSegment.Selected = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'label' attribute on BooleanRadioInput (id=Segment_Input) at CommissionSubPlanDetailCV.pcf: line 333, column 48
    function label_146 () : java.lang.Object {
      return accountSegment
    }
    
    // 'value' attribute on BooleanRadioInput (id=Segment_Input) at CommissionSubPlanDetailCV.pcf: line 333, column 48
    function value_147 () : java.lang.Boolean {
      return accountSegment.Selected
    }
    
    property get accountSegment () : gw.api.domain.commission.SelectedSegment {
      return getIteratedValue(1) as gw.api.domain.commission.SelectedSegment
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/commission/CommissionSubPlanDetailCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry7ExpressionsImpl extends CommissionSubPlanDetailCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on BooleanRadioInput (id=Evaluation_Input) at CommissionSubPlanDetailCV.pcf: line 357, column 51
    function available_159 () : java.lang.Boolean {
      return !conditionalCommissionSubPlan.AllEvaluations
    }
    
    // 'value' attribute on BooleanRadioInput (id=Evaluation_Input) at CommissionSubPlanDetailCV.pcf: line 357, column 51
    function defaultSetter_162 (__VALUE_TO_SET :  java.lang.Object) : void {
      accountEvaluation.Selected = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'label' attribute on BooleanRadioInput (id=Evaluation_Input) at CommissionSubPlanDetailCV.pcf: line 357, column 51
    function label_160 () : java.lang.Object {
      return accountEvaluation
    }
    
    // 'value' attribute on BooleanRadioInput (id=Evaluation_Input) at CommissionSubPlanDetailCV.pcf: line 357, column 51
    function value_161 () : java.lang.Boolean {
      return accountEvaluation.Selected
    }
    
    property get accountEvaluation () : gw.api.domain.commission.SelectedEvaluation {
      return getIteratedValue(1) as gw.api.domain.commission.SelectedEvaluation
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/commission/CommissionSubPlanDetailCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry8ExpressionsImpl extends CommissionSubPlanDetailCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=Jurisdiction_Cell) at CommissionSubPlanDetailCV.pcf: line 399, column 55
    function defaultSetter_174 (__VALUE_TO_SET :  java.lang.Object) : void {
      condCmsnSubPlanJurisdiction.Jurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on TypeKeyCell (id=Jurisdiction_Cell) at CommissionSubPlanDetailCV.pcf: line 399, column 55
    function valueRoot_175 () : java.lang.Object {
      return condCmsnSubPlanJurisdiction
    }
    
    // 'value' attribute on TypeKeyCell (id=Jurisdiction_Cell) at CommissionSubPlanDetailCV.pcf: line 399, column 55
    function value_173 () : typekey.Jurisdiction {
      return condCmsnSubPlanJurisdiction.Jurisdiction
    }
    
    property get condCmsnSubPlanJurisdiction () : entity.CondCmsnSubPlanJurisdiction {
      return getIteratedValue(1) as entity.CondCmsnSubPlanJurisdiction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/commission/CommissionSubPlanDetailCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry9ExpressionsImpl extends CommissionSubPlanDetailCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on BooleanRadioInput (id=UWCompany_Input) at CommissionSubPlanDetailCV.pcf: line 426, column 43
    function available_186 () : java.lang.Boolean {
      return !conditionalCommissionSubPlan.AllUWCompanies
    }
    
    // 'value' attribute on BooleanRadioInput (id=UWCompany_Input) at CommissionSubPlanDetailCV.pcf: line 426, column 43
    function defaultSetter_189 (__VALUE_TO_SET :  java.lang.Object) : void {
      UWCompany.Selected = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'label' attribute on BooleanRadioInput (id=UWCompany_Input) at CommissionSubPlanDetailCV.pcf: line 426, column 43
    function label_187 () : java.lang.Object {
      return UWCompany
    }
    
    // 'value' attribute on BooleanRadioInput (id=UWCompany_Input) at CommissionSubPlanDetailCV.pcf: line 426, column 43
    function value_188 () : java.lang.Boolean {
      return UWCompany.Selected
    }
    
    property get UWCompany () : gw.api.domain.commission.SelectedUWCompany {
      return getIteratedValue(1) as gw.api.domain.commission.SelectedUWCompany
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/commission/CommissionSubPlanDetailCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CommissionSubPlanDetailCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=Rate_Input) at CommissionSubPlanDetailCV.pcf: line 50, column 49
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      rateEntry.Rate = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'label' attribute on TextInput (id=Rate_Input) at CommissionSubPlanDetailCV.pcf: line 50, column 49
    function label_8 () : java.lang.Object {
      return rateEntry.Role
    }
    
    // 'value' attribute on TextInput (id=Rate_Input) at CommissionSubPlanDetailCV.pcf: line 50, column 49
    function valueRoot_11 () : java.lang.Object {
      return rateEntry
    }
    
    // 'value' attribute on TextInput (id=Rate_Input) at CommissionSubPlanDetailCV.pcf: line 50, column 49
    function value_9 () : java.math.BigDecimal {
      return rateEntry.Rate
    }
    
    property get rateEntry () : entity.CommissionSubPlanRateEntry {
      return getIteratedValue(1) as entity.CommissionSubPlanRateEntry
    }
    
    
  }
  
  
}