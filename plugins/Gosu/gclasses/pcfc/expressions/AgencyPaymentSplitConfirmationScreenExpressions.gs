package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyPaymentSplitConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyPaymentSplitConfirmationScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyPaymentSplitConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyPaymentSplitConfirmationScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyPaymentSplitConfirmationScreen.pcf: line 43, column 69
    function currency_8 () : typekey.Currency {
      return agencySplitPayment.OriginalPaymentReceipt.Amount.Currency
    }
    
    // 'initialValue' attribute on Variable at AgencyPaymentSplitConfirmationScreen.pcf: line 10, column 59
    function initialValue_0 () : gw.api.web.producer.ProducerSearchConverter {
      return new gw.api.web.producer.ProducerSearchConverter()
    }
    
    // 'label' attribute on AlertBar (id=MessageAlertBar) at AgencyPaymentSplitConfirmationScreen.pcf: line 27, column 46
    function label_2 () : java.lang.Object {
      return alertBarMessageText
    }
    
    // 'value' attribute on PickerCell (id=Producer_Cell) at AgencyPaymentSplitConfirmationScreen.pcf: line 75, column 48
    function sortValue_10 (splitItem :  gw.api.web.producer.agencybill.AgencySplitPayment.AgencySplitPaymentItem) : java.lang.Object {
      return splitItem.Producer
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AgencyPaymentSplitConfirmationScreen.pcf: line 84, column 44
    function sortValue_11 (splitItem :  gw.api.web.producer.agencybill.AgencySplitPayment.AgencySplitPaymentItem) : java.lang.Object {
      return splitItem.Amount
    }
    
    // '$$sumValue' attribute on RowIterator at AgencyPaymentSplitConfirmationScreen.pcf: line 84, column 44
    function sumValueRoot_13 (splitItem :  gw.api.web.producer.agencybill.AgencySplitPayment.AgencySplitPaymentItem) : java.lang.Object {
      return splitItem
    }
    
    // 'footerSumValue' attribute on RowIterator at AgencyPaymentSplitConfirmationScreen.pcf: line 84, column 44
    function sumValue_12 (splitItem :  gw.api.web.producer.agencybill.AgencySplitPayment.AgencySplitPaymentItem) : java.lang.Object {
      return splitItem.Amount
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at AgencyPaymentSplitConfirmationScreen.pcf: line 64, column 102
    function toCreateAndAdd_30 () : gw.api.web.producer.agencybill.AgencySplitPayment.AgencySplitPaymentItem {
      return agencySplitPayment.createAndAddToSplit()
    }
    
    // 'toRemove' attribute on RowIterator at AgencyPaymentSplitConfirmationScreen.pcf: line 64, column 102
    function toRemove_31 (splitItem :  gw.api.web.producer.agencybill.AgencySplitPayment.AgencySplitPaymentItem) : void {
      agencySplitPayment.removeFromSplit( splitItem )
    }
    
    // 'value' attribute on TextInput (id=CheckRef_Input) at AgencyPaymentSplitConfirmationScreen.pcf: line 37, column 72
    function valueRoot_4 () : java.lang.Object {
      return agencySplitPayment.OriginalPaymentReceipt
    }
    
    // 'value' attribute on TextInput (id=CheckRef_Input) at AgencyPaymentSplitConfirmationScreen.pcf: line 37, column 72
    function value_3 () : java.lang.String {
      return agencySplitPayment.OriginalPaymentReceipt.RefNumber
    }
    
    // 'value' attribute on RowIterator at AgencyPaymentSplitConfirmationScreen.pcf: line 64, column 102
    function value_32 () : gw.api.web.producer.agencybill.AgencySplitPayment.AgencySplitPaymentItem[] {
      return agencySplitPayment.Split
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnallocatedAmount_Cell) at AgencyPaymentSplitConfirmationScreen.pcf: line 101, column 105
    function value_33 () : gw.pl.currency.MonetaryAmount {
      return agencySplitPayment.OriginalPaymentReceipt.Amount - agencySplitPayment.SplitSum
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyPaymentSplitConfirmationScreen.pcf: line 43, column 69
    function value_6 () : gw.pl.currency.MonetaryAmount {
      return agencySplitPayment.OriginalPaymentReceipt.Amount
    }
    
    // 'visible' attribute on AlertBar (id=MessageAlertBar) at AgencyPaymentSplitConfirmationScreen.pcf: line 27, column 46
    function visible_1 () : java.lang.Boolean {
      return alertBarMessageText != null
    }
    
    // 'visible' attribute on Row at AgencyPaymentSplitConfirmationScreen.pcf: line 91, column 116
    function visible_36 () : java.lang.Boolean {
      return (agencySplitPayment.OriginalPaymentReceipt.Amount - agencySplitPayment.SplitSum).IsNotZero
    }
    
    property get agencySplitPayment () : gw.api.web.producer.agencybill.AgencySplitPayment {
      return getRequireValue("agencySplitPayment", 0) as gw.api.web.producer.agencybill.AgencySplitPayment
    }
    
    property set agencySplitPayment ($arg :  gw.api.web.producer.agencybill.AgencySplitPayment) {
      setRequireValue("agencySplitPayment", 0, $arg)
    }
    
    property get alertBarMessageText () : String {
      return getRequireValue("alertBarMessageText", 0) as String
    }
    
    property set alertBarMessageText ($arg :  String) {
      setRequireValue("alertBarMessageText", 0, $arg)
    }
    
    property get producerSearchConverter () : gw.api.web.producer.ProducerSearchConverter {
      return getVariableValue("producerSearchConverter", 0) as gw.api.web.producer.ProducerSearchConverter
    }
    
    property set producerSearchConverter ($arg :  gw.api.web.producer.ProducerSearchConverter) {
      setVariableValue("producerSearchConverter", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyPaymentSplitConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AgencyPaymentSplitConfirmationScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=Producer_Cell) at AgencyPaymentSplitConfirmationScreen.pcf: line 75, column 48
    function action_14 () : void {
      ProducerSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerCell (id=Producer_Cell) at AgencyPaymentSplitConfirmationScreen.pcf: line 75, column 48
    function action_dest_15 () : pcf.api.Destination {
      return pcf.ProducerSearchPopup.createDestination()
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at AgencyPaymentSplitConfirmationScreen.pcf: line 84, column 44
    function currency_27 () : typekey.Currency {
      return agencySplitPayment.OriginalPaymentReceipt.Amount.Currency
    }
    
    // 'value' attribute on PickerCell (id=Producer_Cell) at AgencyPaymentSplitConfirmationScreen.pcf: line 75, column 48
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      splitItem.Producer = (__VALUE_TO_SET as entity.Producer)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AgencyPaymentSplitConfirmationScreen.pcf: line 84, column 44
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      splitItem.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'inputConversion' attribute on PickerCell (id=Producer_Cell) at AgencyPaymentSplitConfirmationScreen.pcf: line 75, column 48
    function inputConversion_17 (VALUE :  java.lang.String) : java.lang.Object {
      return producerSearchConverter.getProducer(VALUE)
    }
    
    // 'validationExpression' attribute on PickerCell (id=Producer_Cell) at AgencyPaymentSplitConfirmationScreen.pcf: line 75, column 48
    function validationExpression_16 () : java.lang.Object {
      return splitItem.Producer != null ? null : DisplayKey.get("Web.AgencyPaymentSplitConfirmationPopup.AgencyPaymentSplitLV.Producer.Error")
    }
    
    // 'validationExpression' attribute on MonetaryAmountCell (id=Amount_Cell) at AgencyPaymentSplitConfirmationScreen.pcf: line 84, column 44
    function validationExpression_23 () : java.lang.Object {
      return agencySplitPayment.verifySplitAmounts()
    }
    
    // 'value' attribute on PickerCell (id=Producer_Cell) at AgencyPaymentSplitConfirmationScreen.pcf: line 75, column 48
    function valueRoot_20 () : java.lang.Object {
      return splitItem
    }
    
    // 'value' attribute on PickerCell (id=Producer_Cell) at AgencyPaymentSplitConfirmationScreen.pcf: line 75, column 48
    function value_18 () : entity.Producer {
      return splitItem.Producer
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AgencyPaymentSplitConfirmationScreen.pcf: line 84, column 44
    function value_24 () : gw.pl.currency.MonetaryAmount {
      return splitItem.Amount
    }
    
    property get splitItem () : gw.api.web.producer.agencybill.AgencySplitPayment.AgencySplitPaymentItem {
      return getIteratedValue(1) as gw.api.web.producer.agencybill.AgencySplitPayment.AgencySplitPaymentItem
    }
    
    
  }
  
  
}