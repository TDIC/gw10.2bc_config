package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on AlertBar (id=ProducerDetail_TroubleTicketAlertAlertBar) at ProducerDetailScreen.pcf: line 18, column 51
    function action_3 () : void {
      TroubleTicketAlertForward.push(producer)
    }
    
    // 'action' attribute on AlertBar (id=ProducerDetail_ActivitiesAlertBar) at ProducerDetailScreen.pcf: line 27, column 116
    function action_8 () : void {
      ProducerActivitiesPage.go(producer)
    }
    
    // 'action' attribute on AlertBar (id=ProducerDetail_TroubleTicketAlertAlertBar) at ProducerDetailScreen.pcf: line 18, column 51
    function action_dest_4 () : pcf.api.Destination {
      return pcf.TroubleTicketAlertForward.createDestination(producer)
    }
    
    // 'action' attribute on AlertBar (id=ProducerDetail_ActivitiesAlertBar) at ProducerDetailScreen.pcf: line 27, column 116
    function action_dest_9 () : pcf.api.Destination {
      return pcf.ProducerActivitiesPage.createDestination(producer)
    }
    
    // 'available' attribute on AlertBar (id=ProducerDetail_TroubleTicketAlertAlertBar) at ProducerDetailScreen.pcf: line 18, column 51
    function available_1 () : java.lang.Boolean {
      return perm.System.prodttktview
    }
    
    // 'def' attribute on PanelRef at ProducerDetailScreen.pcf: line 29, column 41
    function def_onEnter_10 (def :  pcf.ProducerDetailDV) : void {
      def.onEnter(producer)
    }
    
    // 'def' attribute on PanelRef at ProducerDetailScreen.pcf: line 31, column 46
    function def_onEnter_12 (def :  pcf.ProducerCodesLV) : void {
      def.onEnter(producer, false)
    }
    
    // 'def' attribute on PanelRef at ProducerDetailScreen.pcf: line 29, column 41
    function def_refreshVariables_11 (def :  pcf.ProducerDetailDV) : void {
      def.refreshVariables(producer)
    }
    
    // 'def' attribute on PanelRef at ProducerDetailScreen.pcf: line 31, column 46
    function def_refreshVariables_13 (def :  pcf.ProducerCodesLV) : void {
      def.refreshVariables(producer, false)
    }
    
    // EditButtons at ProducerDetailScreen.pcf: line 11, column 21
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'label' attribute on AlertBar (id=ProducerDetail_TroubleTicketAlertAlertBar) at ProducerDetailScreen.pcf: line 18, column 51
    function label_5 () : java.lang.Object {
      return producer.AlertBarDisplayText
    }
    
    // 'visible' attribute on AlertBar (id=ProducerDetail_TroubleTicketAlertAlertBar) at ProducerDetailScreen.pcf: line 18, column 51
    function visible_2 () : java.lang.Boolean {
      return producer.HasActiveTroubleTickets
    }
    
    // 'visible' attribute on AlertBar (id=ProducerDetail_ArchiveWarningAlertBar) at ProducerDetailScreen.pcf: line 22, column 71
    function visible_6 () : java.lang.Boolean {
      return producer.getArchivedPolicyCommissions().Count > 0
    }
    
    // 'visible' attribute on AlertBar (id=ProducerDetail_ActivitiesAlertBar) at ProducerDetailScreen.pcf: line 27, column 116
    function visible_7 () : java.lang.Boolean {
      return producer.Activities_Ext.where(\ a -> a.Status == typekey.ActivityStatus.TC_OPEN).length > 0
    }
    
    property get producer () : Producer {
      return getRequireValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setRequireValue("producer", 0, $arg)
    }
    
    
  }
  
  
}