package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/entrypoints/Invoice.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceEntryPointExpressions {
  @javax.annotation.Generated("config/web/pcf/entrypoints/Invoice.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on EntryPoint (id=Invoice) at Invoice.pcf: line 7, column 49
    function location_0 () : pcf.api.Destination {
      return pcf.AccountInvoiceForward.createDestination(invoiceID)
    }
    
    property get invoiceID () : String {
      return getVariableValue("invoiceID", 0) as String
    }
    
    property set invoiceID ($arg :  String) {
      setVariableValue("invoiceID", 0, $arg)
    }
    
    
  }
  
  
}