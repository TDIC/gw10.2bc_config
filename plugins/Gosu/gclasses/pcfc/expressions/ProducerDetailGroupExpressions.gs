package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerDetailGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerDetailGroupExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerDetailGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationGroupMenuItem2ExpressionsImpl extends ProducerDetailGroupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at AgencyBillPromises.pcf: line 14, column 56
    function action_20 () : void {
      pcf.AgencyBillExecutedPromises.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at AgencyBillPromises.pcf: line 16, column 53
    function action_22 () : void {
      pcf.AgencyBillSavedPromises.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at AgencyBillPromises.pcf: line 14, column 56
    function action_dest_21 () : pcf.api.Destination {
      return pcf.AgencyBillExecutedPromises.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at AgencyBillPromises.pcf: line 16, column 53
    function action_dest_23 () : pcf.api.Destination {
      return pcf.AgencyBillSavedPromises.createDestination(producer)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 1) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerDetailGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationGroupMenuItemExpressionsImpl extends ProducerDetailGroupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at AgencyBillPayments.pcf: line 14, column 56
    function action_12 () : void {
      pcf.AgencyBillExecutedPayments.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at AgencyBillPayments.pcf: line 16, column 53
    function action_14 () : void {
      pcf.AgencyBillSavedPayments.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at AgencyBillPayments.pcf: line 18, column 67
    function action_16 () : void {
      pcf.AgencyBillExecutedCreditDistributions.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at AgencyBillPayments.pcf: line 14, column 56
    function action_dest_13 () : pcf.api.Destination {
      return pcf.AgencyBillExecutedPayments.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at AgencyBillPayments.pcf: line 16, column 53
    function action_dest_15 () : pcf.api.Destination {
      return pcf.AgencyBillSavedPayments.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at AgencyBillPayments.pcf: line 18, column 67
    function action_dest_17 () : pcf.api.Destination {
      return pcf.AgencyBillExecutedCreditDistributions.createDestination(producer)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 1) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerDetailGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerDetailGroupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 20, column 44
    function action_0 () : void {
      pcf.ProducerDetail.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 35, column 54
    function action_10 () : void {
      pcf.ProducerAgencyBillCycles.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 38, column 48
    function action_18 () : void {
      pcf.AgencyBillPayments.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 23, column 46
    function action_2 () : void {
      pcf.ProducerContacts.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 41, column 48
    function action_24 () : void {
      pcf.AgencyBillPromises.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 44, column 53
    function action_26 () : void {
      pcf.AgencyBillSuspenseItems.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 47, column 49
    function action_28 () : void {
      pcf.AgencyBillOpenItems.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 50, column 50
    function action_30 () : void {
      pcf.AgencyBillExceptions.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 53, column 51
    function action_32 () : void {
      pcf.ProducerDisbursements.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 56, column 51
    function action_34 () : void {
      pcf.ProducerDetailHistory.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 59, column 58
    function action_36 () : void {
      pcf.ProducerDetailTroubleTickets.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 62, column 53
    function action_38 () : void {
      pcf.ProducerDetailDocuments.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 26, column 46
    function action_4 () : void {
      pcf.ProducerPolicies.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 65, column 55
    function action_40 () : void {
      pcf.ProducerDetailNotes.go(producer, true)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 68, column 50
    function action_42 () : void {
      pcf.ProducerTransactions.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 71, column 50
    function action_44 () : void {
      pcf.ProducerDetailLedger.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 74, column 51
    function action_46 () : void {
      pcf.ProducerDetailJournal.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 76, column 52
    function action_48 () : void {
      pcf.ProducerActivitiesPage.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 29, column 47
    function action_6 () : void {
      pcf.ProducerWriteOffs.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 32, column 55
    function action_8 () : void {
      pcf.ProducerStatementOverview.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 20, column 44
    function action_dest_1 () : pcf.api.Destination {
      return pcf.ProducerDetail.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 35, column 54
    function action_dest_11 () : pcf.api.Destination {
      return pcf.ProducerAgencyBillCycles.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 38, column 48
    function action_dest_19 () : pcf.api.Destination {
      return pcf.AgencyBillPayments.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 41, column 48
    function action_dest_25 () : pcf.api.Destination {
      return pcf.AgencyBillPromises.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 44, column 53
    function action_dest_27 () : pcf.api.Destination {
      return pcf.AgencyBillSuspenseItems.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 47, column 49
    function action_dest_29 () : pcf.api.Destination {
      return pcf.AgencyBillOpenItems.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 23, column 46
    function action_dest_3 () : pcf.api.Destination {
      return pcf.ProducerContacts.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 50, column 50
    function action_dest_31 () : pcf.api.Destination {
      return pcf.AgencyBillExceptions.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 53, column 51
    function action_dest_33 () : pcf.api.Destination {
      return pcf.ProducerDisbursements.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 56, column 51
    function action_dest_35 () : pcf.api.Destination {
      return pcf.ProducerDetailHistory.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 59, column 58
    function action_dest_37 () : pcf.api.Destination {
      return pcf.ProducerDetailTroubleTickets.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 62, column 53
    function action_dest_39 () : pcf.api.Destination {
      return pcf.ProducerDetailDocuments.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 65, column 55
    function action_dest_41 () : pcf.api.Destination {
      return pcf.ProducerDetailNotes.createDestination(producer, true)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 68, column 50
    function action_dest_43 () : pcf.api.Destination {
      return pcf.ProducerTransactions.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 71, column 50
    function action_dest_45 () : pcf.api.Destination {
      return pcf.ProducerDetailLedger.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 74, column 51
    function action_dest_47 () : pcf.api.Destination {
      return pcf.ProducerDetailJournal.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 76, column 52
    function action_dest_49 () : pcf.api.Destination {
      return pcf.ProducerActivitiesPage.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 26, column 46
    function action_dest_5 () : pcf.api.Destination {
      return pcf.ProducerPolicies.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 29, column 47
    function action_dest_7 () : pcf.api.Destination {
      return pcf.ProducerWriteOffs.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 32, column 55
    function action_dest_9 () : pcf.api.Destination {
      return pcf.ProducerStatementOverview.createDestination(producer)
    }
    
    // 'afterEnter' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 12, column 27
    function afterEnter_50 () : void {
      gw.api.web.producer.ProducerUtil.addToRecentlyViewedProducers(producer)
    }
    
    // 'canVisit' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 12, column 27
    static function canVisit_51 (producer :  Producer) : java.lang.Boolean {
      return producer.ViewableByCurrentUser
    }
    
    // LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 12, column 27
    static function firstVisitableChildDestinationMethod_60 (producer :  Producer) : pcf.api.Destination {
      var dest : pcf.api.Destination
      dest = pcf.ProducerDetail.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ProducerContacts.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ProducerPolicies.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ProducerWriteOffs.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ProducerStatementOverview.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ProducerAgencyBillCycles.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AgencyBillPayments.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AgencyBillPromises.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AgencyBillSuspenseItems.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AgencyBillOpenItems.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AgencyBillExceptions.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ProducerDisbursements.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ProducerDetailHistory.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ProducerDetailTroubleTickets.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ProducerDetailDocuments.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ProducerDetailNotes.createDestination(producer, true)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ProducerTransactions.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ProducerDetailLedger.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ProducerDetailJournal.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ProducerActivitiesPage.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      return null
    }
    
    // 'infoBar' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 12, column 27
    function infoBar_onEnter_52 (def :  pcf.ProducerInfoBar) : void {
      def.onEnter(producer)
    }
    
    // 'infoBar' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 12, column 27
    function infoBar_refreshVariables_53 (def :  pcf.ProducerInfoBar) : void {
      def.refreshVariables(producer)
    }
    
    // 'menuActions' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 12, column 27
    function menuActions_onEnter_54 (def :  pcf.ProducerMenuActions) : void {
      def.onEnter(producer)
    }
    
    // 'menuActions' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 12, column 27
    function menuActions_refreshVariables_55 (def :  pcf.ProducerMenuActions) : void {
      def.refreshVariables(producer)
    }
    
    // 'parent' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 12, column 27
    static function parent_56 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerForward.createDestination()
    }
    
    // 'tabBar' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 12, column 27
    function tabBar_onEnter_57 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 12, column 27
    function tabBar_refreshVariables_58 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    // 'title' attribute on LocationGroup (id=ProducerDetailGroup) at ProducerDetailGroup.pcf: line 12, column 27
    static function title_59 (producer :  Producer) : java.lang.Object {
      return producer.Name
    }
    
    override property get CurrentLocation () : pcf.ProducerDetailGroup {
      return super.CurrentLocation as pcf.ProducerDetailGroup
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}