package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadConfirmation.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadConfirmationExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadConfirmation.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadConfirmationExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (processor :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) : int {
      return 0
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 104, column 52
    function def_onEnter_57 (def :  pcf.AdminDataUploadRoleLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 112, column 58
    function def_onEnter_60 (def :  pcf.AdminDataUploadActPatternLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 120, column 55
    function def_onEnter_63 (def :  pcf.AdminDataUploadSecZoneLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 128, column 57
    function def_onEnter_66 (def :  pcf.AdminDataUploadAuthLimitLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 136, column 61
    function def_onEnter_69 (def :  pcf.AdminDataUploadUserAuthLimitLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 144, column 52
    function def_onEnter_72 (def :  pcf.AdminDataUploadUserLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 152, column 53
    function def_onEnter_75 (def :  pcf.AdminDataUploadGroupLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 160, column 54
    function def_onEnter_78 (def :  pcf.AdminDataUploadRegionLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 168, column 57
    function def_onEnter_81 (def :  pcf.AdminDataUploadAttributeLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 176, column 57
    function def_onEnter_84 (def :  pcf.AdminDataUploadUserGroupLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 184, column 53
    function def_onEnter_87 (def :  pcf.AdminDataUploadQueueLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 192, column 55
    function def_onEnter_90 (def :  pcf.AdminDataUploadHolidayLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 200, column 60
    function def_onEnter_93 (def :  pcf.AdminDataUploadLocalizationLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 208, column 66
    function def_onEnter_96 (def :  pcf.AdminDataUploadCollectionAgenciesLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 104, column 52
    function def_refreshVariables_58 (def :  pcf.AdminDataUploadRoleLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 112, column 58
    function def_refreshVariables_61 (def :  pcf.AdminDataUploadActPatternLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 120, column 55
    function def_refreshVariables_64 (def :  pcf.AdminDataUploadSecZoneLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 128, column 57
    function def_refreshVariables_67 (def :  pcf.AdminDataUploadAuthLimitLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 136, column 61
    function def_refreshVariables_70 (def :  pcf.AdminDataUploadUserAuthLimitLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 144, column 52
    function def_refreshVariables_73 (def :  pcf.AdminDataUploadUserLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 152, column 53
    function def_refreshVariables_76 (def :  pcf.AdminDataUploadGroupLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 160, column 54
    function def_refreshVariables_79 (def :  pcf.AdminDataUploadRegionLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 168, column 57
    function def_refreshVariables_82 (def :  pcf.AdminDataUploadAttributeLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 176, column 57
    function def_refreshVariables_85 (def :  pcf.AdminDataUploadUserGroupLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 184, column 53
    function def_refreshVariables_88 (def :  pcf.AdminDataUploadQueueLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 192, column 55
    function def_refreshVariables_91 (def :  pcf.AdminDataUploadHolidayLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 200, column 60
    function def_refreshVariables_94 (def :  pcf.AdminDataUploadLocalizationLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at AdminDataUploadConfirmation.pcf: line 208, column 66
    function def_refreshVariables_97 (def :  pcf.AdminDataUploadCollectionAgenciesLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'label' attribute on Verbatim at AdminDataUploadConfirmation.pcf: line 18, column 113
    function label_0 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationResultsLabel")
    }
    
    // 'label' attribute on TextInput (id=noRoles_Input) at AdminDataUploadConfirmation.pcf: line 25, column 44
    function label_1 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Roles"))
    }
    
    // 'label' attribute on TextInput (id=noAuthLimit_Input) at AdminDataUploadConfirmation.pcf: line 40, column 44
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.AuthorityLimits"))
    }
    
    // 'label' attribute on TextInput (id=noUser_Input) at AdminDataUploadConfirmation.pcf: line 52, column 44
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Users"))
    }
    
    // 'label' attribute on TextInput (id=noGroup_Input) at AdminDataUploadConfirmation.pcf: line 57, column 44
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Groups"))
    }
    
    // 'label' attribute on TextInput (id=noUserGroup_Input) at AdminDataUploadConfirmation.pcf: line 62, column 44
    function label_29 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserGroups"))
    }
    
    // 'label' attribute on TextInput (id=noAttribute_Input) at AdminDataUploadConfirmation.pcf: line 69, column 44
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Attributes"))
    }
    
    // 'label' attribute on TextInput (id=noRegion_Input) at AdminDataUploadConfirmation.pcf: line 74, column 44
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Regions"))
    }
    
    // 'label' attribute on TextInput (id=noQueue_Input) at AdminDataUploadConfirmation.pcf: line 79, column 44
    function label_41 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Queues"))
    }
    
    // 'label' attribute on TextInput (id=noHoliday_Input) at AdminDataUploadConfirmation.pcf: line 84, column 44
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Holidays"))
    }
    
    // 'label' attribute on TextInput (id=noCollectionAgencies_Input) at AdminDataUploadConfirmation.pcf: line 91, column 44
    function label_49 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.CollectionAgencies"))
    }
    
    // 'label' attribute on TextInput (id=noActvPattern_Input) at AdminDataUploadConfirmation.pcf: line 30, column 44
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPatterns"))
    }
    
    // 'label' attribute on TextInput (id=noLocalizations_Input) at AdminDataUploadConfirmation.pcf: line 96, column 44
    function label_53 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations"))
    }
    
    // 'label' attribute on TextInput (id=noSecZone_Input) at AdminDataUploadConfirmation.pcf: line 35, column 44
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.SecurityZones"))
    }
    
    // 'parent' attribute on Page (id=AdminDataUploadConfirmation) at AdminDataUploadConfirmation.pcf: line 8, column 85
    static function parent_99 (processor :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) : pcf.api.Destination {
      return pcf.ServerTools.createDestination()
    }
    
    // 'title' attribute on Card (id=role) at AdminDataUploadConfirmation.pcf: line 102, column 98
    function title_59 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Roles")
    }
    
    // 'title' attribute on Card (id=ActvityPattern) at AdminDataUploadConfirmation.pcf: line 110, column 109
    function title_62 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.ActivityPatterns")
    }
    
    // 'title' attribute on Card (id=SecurityZone) at AdminDataUploadConfirmation.pcf: line 118, column 106
    function title_65 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.SecurityZones")
    }
    
    // 'title' attribute on Card (id=AuthorityLimit) at AdminDataUploadConfirmation.pcf: line 126, column 108
    function title_68 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.AuthorityLimits")
    }
    
    // 'title' attribute on Card (id=UserAuthLimit) at AdminDataUploadConfirmation.pcf: line 134, column 112
    function title_71 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserAuthorityLimits")
    }
    
    // 'title' attribute on Card (id=User) at AdminDataUploadConfirmation.pcf: line 142, column 98
    function title_74 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Users")
    }
    
    // 'title' attribute on Card (id=group) at AdminDataUploadConfirmation.pcf: line 150, column 99
    function title_77 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Groups")
    }
    
    // 'title' attribute on Card (id=region) at AdminDataUploadConfirmation.pcf: line 158, column 100
    function title_80 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Regions")
    }
    
    // 'title' attribute on Card (id=attribute) at AdminDataUploadConfirmation.pcf: line 166, column 103
    function title_83 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Attributes")
    }
    
    // 'title' attribute on Card (id=userGroup) at AdminDataUploadConfirmation.pcf: line 174, column 103
    function title_86 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserGroups")
    }
    
    // 'title' attribute on Card (id=queue) at AdminDataUploadConfirmation.pcf: line 182, column 99
    function title_89 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Queues")
    }
    
    // 'title' attribute on Card (id=holiday) at AdminDataUploadConfirmation.pcf: line 190, column 101
    function title_92 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Holidays")
    }
    
    // 'title' attribute on Card (id=localizations) at AdminDataUploadConfirmation.pcf: line 198, column 106
    function title_95 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Localizations")
    }
    
    // 'title' attribute on Card (id=collectionAgencies) at AdminDataUploadConfirmation.pcf: line 206, column 111
    function title_98 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.CollectionAgencies")
    }
    
    // 'value' attribute on TextInput (id=noSecZone_Input) at AdminDataUploadConfirmation.pcf: line 35, column 44
    function value_10 () : java.lang.Integer {
      return processor.SecurityZoneArray == null ? 0 : processor.SecurityZoneArray.countWhere(\ i -> i.Skipped == false and i.Error == false )
    }
    
    // 'value' attribute on TextInput (id=noAuthLimit_Input) at AdminDataUploadConfirmation.pcf: line 40, column 44
    function value_14 () : java.lang.Integer {
      return processor.AuthorityLimitArray == null ? 0 : processor.AuthorityLimitArray.countWhere(\ i -> i.Skipped == false and i.Error == false )
    }
    
    // 'value' attribute on TextInput (id=noUserAuthLimit_Input) at AdminDataUploadConfirmation.pcf: line 47, column 44
    function value_18 () : java.lang.Integer {
      return processor.UserAuthorityLimitArray == null ? 0 : processor.UserAuthorityLimitArray.countWhere(\ a -> a.Skipped == false and a.Error== false )
    }
    
    // 'value' attribute on TextInput (id=noRoles_Input) at AdminDataUploadConfirmation.pcf: line 25, column 44
    function value_2 () : java.lang.Integer {
      return processor.RoleArray == null ? 0 : processor.RoleArray.countWhere(\ a -> a.Skipped == false and a.Error== false )
    }
    
    // 'value' attribute on TextInput (id=noUser_Input) at AdminDataUploadConfirmation.pcf: line 52, column 44
    function value_22 () : java.lang.Integer {
      return processor.UserArray == null ? 0 : processor.UserArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    // 'value' attribute on TextInput (id=noGroup_Input) at AdminDataUploadConfirmation.pcf: line 57, column 44
    function value_26 () : java.lang.Integer {
      return processor.GroupArray == null ? 0 : processor.GroupArray.countWhere(\ i -> i.Skipped == false and i.Error == false )
    }
    
    // 'value' attribute on TextInput (id=noUserGroup_Input) at AdminDataUploadConfirmation.pcf: line 62, column 44
    function value_30 () : java.lang.Integer {
      return processor.UserGroupArray == null ? 0 : processor.UserGroupArray.countWhere(\ i -> i.Skipped == false and i.Error == false )
    }
    
    // 'value' attribute on TextInput (id=noAttribute_Input) at AdminDataUploadConfirmation.pcf: line 69, column 44
    function value_34 () : java.lang.Integer {
      return processor.AttributeArray == null ? 0 : processor.AttributeArray.countWhere(\ a -> a.Skipped == false and a.Error== false )
    }
    
    // 'value' attribute on TextInput (id=noRegion_Input) at AdminDataUploadConfirmation.pcf: line 74, column 44
    function value_38 () : java.lang.Integer {
      return processor.RegionArray == null ? 0 : processor.RegionArray.countWhere(\ i -> i.Skipped == false and i.Error == false )
    }
    
    // 'value' attribute on TextInput (id=noQueue_Input) at AdminDataUploadConfirmation.pcf: line 79, column 44
    function value_42 () : java.lang.Integer {
      return processor.QueueArray == null ? 0 : processor.QueueArray.countWhere(\ i -> i.Skipped == false and i.Error == false )
    }
    
    // 'value' attribute on TextInput (id=noHoliday_Input) at AdminDataUploadConfirmation.pcf: line 84, column 44
    function value_46 () : java.lang.Integer {
      return processor.HolidayArray == null ? 0 : processor.HolidayArray.countWhere(\ i -> i.Skipped == false and i.Error == false )
    }
    
    // 'value' attribute on TextInput (id=noCollectionAgencies_Input) at AdminDataUploadConfirmation.pcf: line 91, column 44
    function value_50 () : java.lang.Integer {
      return processor.CollectionAgencyArray == null ? 0 : processor.CollectionAgencyArray.countWhere(\ a -> a.Skipped == false and a.Error== false )
    }
    
    // 'value' attribute on TextInput (id=noLocalizations_Input) at AdminDataUploadConfirmation.pcf: line 96, column 44
    function value_54 () : java.lang.Integer {
      return processor.LocalizationArray == null ? 0 : processor.LocalizationArray.countWhere(\ a -> a.Skipped == false and a.Error== false )
    }
    
    // 'value' attribute on TextInput (id=noActvPattern_Input) at AdminDataUploadConfirmation.pcf: line 30, column 44
    function value_6 () : java.lang.Integer {
      return processor.ActivityPatternArray == null ? 0 : processor.ActivityPatternArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    override property get CurrentLocation () : pcf.AdminDataUploadConfirmation {
      return super.CurrentLocation as pcf.AdminDataUploadConfirmation
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getVariableValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setVariableValue("processor", 0, $arg)
    }
    
    
  }
  
  
}