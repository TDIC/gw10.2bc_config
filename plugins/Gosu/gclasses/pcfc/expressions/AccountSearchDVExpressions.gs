package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/AccountSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/AccountSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerInput (id=PolicyNumberCriterion_Input) at AccountSearchDV.pcf: line 37, column 39
    function action_16 () : void {
      PolicySearchPopup.push(true)
    }
    
    // 'pickLocation' attribute on PickerInput (id=PolicyNumberCriterion_Input) at AccountSearchDV.pcf: line 37, column 39
    function action_dest_17 () : pcf.api.Destination {
      return pcf.PolicySearchPopup.createDestination(true)
    }
    
    // 'conversionExpression' attribute on PickerInput (id=PolicyNumberCriterion_Input) at AccountSearchDV.pcf: line 37, column 39
    function conversionExpression_18 (PickedValue :  PolicyPeriod) : java.lang.String {
      return PickedValue.DisplayName
    }
    
    // 'def' attribute on InputSetRef at AccountSearchDV.pcf: line 85, column 77
    function def_onEnter_48 (def :  pcf.ContactCriteriaInputSet) : void {
      def.onEnter(searchCriteria.ContactCriteria,true)
    }
    
    // 'def' attribute on InputSetRef at AccountSearchDV.pcf: line 89, column 41
    function def_onEnter_50 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at AccountSearchDV.pcf: line 85, column 77
    function def_refreshVariables_49 (def :  pcf.ContactCriteriaInputSet) : void {
      def.refreshVariables(searchCriteria.ContactCriteria,true)
    }
    
    // 'def' attribute on InputSetRef at AccountSearchDV.pcf: line 89, column 41
    function def_refreshVariables_51 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TextInput (id=AccountNumberCriterion_Input) at AccountSearchDV.pcf: line 18, column 47
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AccountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=AccountNameKanjiCriterion_Input) at AccountSearchDV.pcf: line 29, column 84
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AccountNameKanji = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PickerInput (id=PolicyNumberCriterion_Input) at AccountSearchDV.pcf: line 37, column 39
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at AccountSearchDV.pcf: line 44, column 67
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.CurrencyCriterion = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on TypeKeyInput (id=AccountSegmentCriterion_Input) at AccountSearchDV.pcf: line 50, column 45
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Segment = (__VALUE_TO_SET as typekey.AccountSegment)
    }
    
    // 'value' attribute on TypeKeyInput (id=DelinquencyStatusCriterion_Input) at AccountSearchDV.pcf: line 56, column 48
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.DelinquencyStatus = (__VALUE_TO_SET as typekey.DelinquencyStatus)
    }
    
    // 'value' attribute on RangeInput (id=AccountTypeCriterion_Input) at AccountSearchDV.pcf: line 64, column 42
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AccountType = (__VALUE_TO_SET as typekey.AccountType)
    }
    
    // 'value' attribute on CheckBoxInput (id=HasChargeHoldsCriterion_Input) at AccountSearchDV.pcf: line 81, column 44
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ChargeHeld = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=AccountNameCriterion_Input) at AccountSearchDV.pcf: line 23, column 45
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AccountName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'label' attribute on TextInput (id=AccountNameCriterion_Input) at AccountSearchDV.pcf: line 23, column 45
    function label_4 () : java.lang.Object {
      return (gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP) ? DisplayKey.get("Web.AccountSearchDV.AccountNamePhonetic") : DisplayKey.get("Web.AccountSearchDV.AccountName")
    }
    
    // 'valueRange' attribute on RangeInput (id=AccountTypeCriterion_Input) at AccountSearchDV.pcf: line 64, column 42
    function valueRange_40 () : java.lang.Object {
      return AccountType.getTypeKeys(false)
    }
    
    // 'value' attribute on TextInput (id=AccountNumberCriterion_Input) at AccountSearchDV.pcf: line 18, column 47
    function valueRoot_2 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TextInput (id=AccountNumberCriterion_Input) at AccountSearchDV.pcf: line 18, column 47
    function value_0 () : java.lang.String {
      return searchCriteria.AccountNumber
    }
    
    // 'value' attribute on TextInput (id=AccountNameKanjiCriterion_Input) at AccountSearchDV.pcf: line 29, column 84
    function value_11 () : java.lang.String {
      return searchCriteria.AccountNameKanji
    }
    
    // 'value' attribute on PickerInput (id=PolicyNumberCriterion_Input) at AccountSearchDV.pcf: line 37, column 39
    function value_19 () : java.lang.String {
      return searchCriteria.PolicyNumber
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at AccountSearchDV.pcf: line 44, column 67
    function value_24 () : typekey.Currency {
      return searchCriteria.CurrencyCriterion
    }
    
    // 'value' attribute on TypeKeyInput (id=AccountSegmentCriterion_Input) at AccountSearchDV.pcf: line 50, column 45
    function value_29 () : typekey.AccountSegment {
      return searchCriteria.Segment
    }
    
    // 'value' attribute on TypeKeyInput (id=DelinquencyStatusCriterion_Input) at AccountSearchDV.pcf: line 56, column 48
    function value_33 () : typekey.DelinquencyStatus {
      return searchCriteria.DelinquencyStatus
    }
    
    // 'value' attribute on RangeInput (id=AccountTypeCriterion_Input) at AccountSearchDV.pcf: line 64, column 42
    function value_37 () : typekey.AccountType {
      return searchCriteria.AccountType
    }
    
    // 'value' attribute on CheckBoxInput (id=HasChargeHoldsCriterion_Input) at AccountSearchDV.pcf: line 81, column 44
    function value_44 () : java.lang.Boolean {
      return searchCriteria.ChargeHeld
    }
    
    // 'value' attribute on TextInput (id=AccountNameCriterion_Input) at AccountSearchDV.pcf: line 23, column 45
    function value_5 () : java.lang.String {
      return searchCriteria.AccountName
    }
    
    // 'valueRange' attribute on RangeInput (id=AccountTypeCriterion_Input) at AccountSearchDV.pcf: line 64, column 42
    function verifyValueRangeIsAllowedType_41 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AccountTypeCriterion_Input) at AccountSearchDV.pcf: line 64, column 42
    function verifyValueRangeIsAllowedType_41 ($$arg :  typekey.AccountType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AccountTypeCriterion_Input) at AccountSearchDV.pcf: line 64, column 42
    function verifyValueRange_42 () : void {
      var __valueRangeArg = AccountType.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_41(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=AccountNameKanjiCriterion_Input) at AccountSearchDV.pcf: line 29, column 84
    function visible_10 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP
    }
    
    // 'visible' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at AccountSearchDV.pcf: line 44, column 67
    function visible_23 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    property get searchCriteria () : gw.search.AccountSearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.search.AccountSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.AccountSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    
  }
  
  
}