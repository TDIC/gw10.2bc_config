package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyChangeWizardChargesStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyChangeWizardChargesStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyChangeWizardChargesStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyChangeWizardChargesStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at PolicyChangeWizardChargesStepScreen.pcf: line 20, column 105
    function def_onEnter_0 (def :  pcf.PolicyAddChargesListDetailPanel) : void {
      def.onEnter(policyChange, policyPeriod, chargeToInvoicingOverridesMap)
    }
    
    // 'def' attribute on PanelRef at PolicyChangeWizardChargesStepScreen.pcf: line 20, column 105
    function def_refreshVariables_1 (def :  pcf.PolicyAddChargesListDetailPanel) : void {
      def.refreshVariables(policyChange, policyPeriod, chargeToInvoicingOverridesMap)
    }
    
    property get chargeToInvoicingOverridesMap () : java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView> {
      return getRequireValue("chargeToInvoicingOverridesMap", 0) as java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>
    }
    
    property set chargeToInvoicingOverridesMap ($arg :  java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>) {
      setRequireValue("chargeToInvoicingOverridesMap", 0, $arg)
    }
    
    property get policyChange () : PolicyChange {
      return getRequireValue("policyChange", 0) as PolicyChange
    }
    
    property set policyChange ($arg :  PolicyChange) {
      setRequireValue("policyChange", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get view () : gw.web.policy.PolicyWizardChargeStepScreenView {
      return getRequireValue("view", 0) as gw.web.policy.PolicyWizardChargeStepScreenView
    }
    
    property set view ($arg :  gw.web.policy.PolicyWizardChargeStepScreenView) {
      setRequireValue("view", 0, $arg)
    }
    
    
  }
  
  
}