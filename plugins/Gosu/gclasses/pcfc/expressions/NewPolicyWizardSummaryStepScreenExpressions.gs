package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/NewPolicyWizardSummaryStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewPolicyWizardSummaryStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/NewPolicyWizardSummaryStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends NewPolicyWizardSummaryStepScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at NewPolicyWizardSummaryStepScreen.pcf: line 268, column 47
    function valueRoot_150 () : java.lang.Object {
      return policyContact.Contact
    }
    
    // 'value' attribute on TextCell (id=ContactName_Cell) at NewPolicyWizardSummaryStepScreen.pcf: line 263, column 59
    function value_147 () : entity.PolicyPeriodContact {
      return policyContact
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at NewPolicyWizardSummaryStepScreen.pcf: line 268, column 47
    function value_149 () : entity.Address {
      return policyContact.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=ContactRoles_Cell) at NewPolicyWizardSummaryStepScreen.pcf: line 272, column 97
    function value_152 () : java.lang.String {
      return gw.api.web.policy.PolicyPeriodUtil.getRolesForDisplay(policyContact)
    }
    
    property get policyContact () : entity.PolicyPeriodContact {
      return getIteratedValue(1) as entity.PolicyPeriodContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/NewPolicyWizardSummaryStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends NewPolicyWizardSummaryStepScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=ProducerPicker) at NewPolicyWizardSummaryStepScreen.pcf: line 198, column 41
    function action_115 () : void {
      ProducerSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=ProducerPicker) at NewPolicyWizardSummaryStepScreen.pcf: line 198, column 41
    function action_dest_116 () : pcf.api.Destination {
      return pcf.ProducerSearchPopup.createDestination()
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 198, column 41
    function defaultSetter_123 (__VALUE_TO_SET :  java.lang.Object) : void {
      producerCodeRoleEntry.Producer = (__VALUE_TO_SET as entity.Producer)
    }
    
    // 'value' attribute on RangeInput (id=ProducerCode_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 215, column 46
    function defaultSetter_129 (__VALUE_TO_SET :  java.lang.Object) : void {
      producerCodeRoleEntry.ProducerCode = (__VALUE_TO_SET as entity.ProducerCode)
    }
    
    // 'inputConversion' attribute on TextInput (id=Producer_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 198, column 41
    function inputConversion_119 (VALUE :  java.lang.String) : java.lang.Object {
      return producerSearchConverter.getProducer(VALUE)
    }
    
    // 'label' attribute on TextInput (id=Producer_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 198, column 41
    function label_117 () : java.lang.Object {
      return producerCodeRoleEntry.Role
    }
    
    // 'onChange' attribute on PostOnChange at NewPolicyWizardSummaryStepScreen.pcf: line 205, column 77
    function onChange_114 () : void {
      autoSelectSoleProducerCode(producerCodeRoleEntry)
    }
    
    // 'onPick' attribute on TextInput (id=Producer_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 198, column 41
    function onPick_118 (PickedValue :  Producer) : void {
      autoSelectSoleProducerCode(producerCodeRoleEntry)
    }
    
    // 'optionLabel' attribute on RangeInput (id=ProducerCode_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 215, column 46
    function optionLabel_131 (VALUE :  entity.ProducerCode) : java.lang.String {
      return VALUE.Code
    }
    
    // 'requestValidationExpression' attribute on TextInput (id=Producer_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 198, column 41
    function requestValidationExpression_120 (VALUE :  entity.Producer) : java.lang.Object {
      return validateProducer(VALUE, producerCodeRoleEntry.Role)
    }
    
    // 'required' attribute on TextInput (id=Producer_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 198, column 41
    function required_121 () : java.lang.Boolean {
      return policyPeriod.AgencyBill and producerCodeRoleEntry.Role == PolicyRole.TC_PRIMARY
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 215, column 46
    function valueRange_132 () : java.lang.Object {
      return producerCodeRoleEntry.Producer.ActiveProducerCodes
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 198, column 41
    function valueRoot_124 () : java.lang.Object {
      return producerCodeRoleEntry
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 198, column 41
    function value_122 () : entity.Producer {
      return producerCodeRoleEntry.Producer
    }
    
    // 'value' attribute on RangeInput (id=ProducerCode_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 215, column 46
    function value_128 () : entity.ProducerCode {
      return producerCodeRoleEntry.ProducerCode
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 215, column 46
    function verifyValueRangeIsAllowedType_133 ($$arg :  entity.ProducerCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 215, column 46
    function verifyValueRangeIsAllowedType_133 ($$arg :  gw.api.database.IQueryBeanResult<entity.ProducerCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 215, column 46
    function verifyValueRangeIsAllowedType_133 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 215, column 46
    function verifyValueRange_134 () : void {
      var __valueRangeArg = producerCodeRoleEntry.Producer.ActiveProducerCodes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_133(__valueRangeArg)
    }
    
    property get producerCodeRoleEntry () : entity.ProducerCodeRoleEntry {
      return getIteratedValue(1) as entity.ProducerCodeRoleEntry
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/NewPolicyWizardSummaryStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewPolicyWizardSummaryStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=addNewCompany) at NewPolicyWizardSummaryStepScreen.pcf: line 231, column 93
    function action_137 () : void {
      NewPolicyContactPopup.push(policyPeriod, Company)
    }
    
    // 'action' attribute on MenuItem (id=addNewPerson) at NewPolicyWizardSummaryStepScreen.pcf: line 236, column 92
    function action_139 () : void {
      NewPolicyContactPopup.push(policyPeriod, Person)
    }
    
    // 'action' attribute on PickerToolbarButton (id=addExistingContact) at NewPolicyWizardSummaryStepScreen.pcf: line 247, column 104
    function action_141 () : void {
      ContactSearchPopup.push(false)
    }
    
    // 'pickLocation' attribute on PickerInput (id=OverridingPayerAccount_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 141, column 46
    function action_70 () : void {
      AccountSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=addNewCompany) at NewPolicyWizardSummaryStepScreen.pcf: line 231, column 93
    function action_dest_138 () : pcf.api.Destination {
      return pcf.NewPolicyContactPopup.createDestination(policyPeriod, Company)
    }
    
    // 'action' attribute on MenuItem (id=addNewPerson) at NewPolicyWizardSummaryStepScreen.pcf: line 236, column 92
    function action_dest_140 () : pcf.api.Destination {
      return pcf.NewPolicyContactPopup.createDestination(policyPeriod, Person)
    }
    
    // 'action' attribute on PickerToolbarButton (id=addExistingContact) at NewPolicyWizardSummaryStepScreen.pcf: line 247, column 104
    function action_dest_142 () : pcf.api.Destination {
      return pcf.ContactSearchPopup.createDestination(false)
    }
    
    // 'pickLocation' attribute on PickerInput (id=OverridingPayerAccount_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 141, column 46
    function action_dest_71 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'available' attribute on BooleanRadioInput (id=EligibleForFullPayDiscount_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 111, column 47
    function available_49 () : java.lang.Boolean {
      return !policyPeriod.AgencyBill
    }
    
    // 'value' attribute on RangeInput (id=DelinquencyPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 174, column 47
    function defaultSetter_101 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.PolicyPeriodDelinquencyPlan = (__VALUE_TO_SET as entity.DelinquencyPlan)
    }
    
    // 'value' attribute on RangeInput (id=ReturnPremiumPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 182, column 49
    function defaultSetter_108 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.ReturnPremiumPlan = (__VALUE_TO_SET as entity.ReturnPremiumPlan)
    }
    
    // 'value' attribute on DateInput (id=PolicyPerEffDate_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 62, column 50
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.PolicyPerEffDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=PolicyPerExpirDate_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 69, column 52
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.PolicyPerExpirDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=PolicyLOB_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 75, column 40
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.Policy.LOBCode = (__VALUE_TO_SET as typekey.LOBCode)
    }
    
    // 'value' attribute on BooleanRadioInput (id=AssignedRisk_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 80, column 46
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.AssignedRisk = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=RiskJurisdiction_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 86, column 45
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.RiskJurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on TypeKeyInput (id=UWCompany_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 92, column 42
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.UWCompany = (__VALUE_TO_SET as typekey.UWCompany)
    }
    
    // 'value' attribute on RangeInput (id=SecurityZone_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 99, column 44
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.SecurityZone = (__VALUE_TO_SET as entity.SecurityZone)
    }
    
    // 'value' attribute on TextInput (id=Underwriter_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 104, column 45
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.Underwriter = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 51, column 46
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioInput (id=EligibleForFullPayDiscount_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 111, column 47
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.EligibleForFullPayDiscount = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=RequireFinalAudit_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 117, column 51
    function defaultSetter_58 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.RequireFinalAudit = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=BillingMethod_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 127, column 57
    function defaultSetter_63 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.BillingMethod = (__VALUE_TO_SET as typekey.PolicyPeriodBillingMethod)
    }
    
    // 'value' attribute on PickerInput (id=OverridingPayerAccount_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 141, column 46
    function defaultSetter_77 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoicingOverridesView.OverridingPayerAccount = (__VALUE_TO_SET as entity.Account)
    }
    
    // 'value' attribute on RangeInput (id=PaymentPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 154, column 42
    function defaultSetter_84 (__VALUE_TO_SET :  java.lang.Object) : void {
      issuance.PolicyPaymentPlan = (__VALUE_TO_SET as entity.PaymentPlan)
    }
    
    // 'value' attribute on TextInput (id=DBA_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 56, column 37
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.DBA = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=OverridingInvoiceStream_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 166, column 47
    function defaultSetter_93 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoicingOverridesView.OverridingInvoiceStream = (__VALUE_TO_SET as entity.InvoiceStream)
    }
    
    // 'initialValue' attribute on Variable at NewPolicyWizardSummaryStepScreen.pcf: line 25, column 59
    function initialValue_0 () : gw.api.web.producer.ProducerSearchConverter {
      return new gw.api.web.producer.ProducerSearchConverter()
    }
    
    // 'initialValue' attribute on Variable at NewPolicyWizardSummaryStepScreen.pcf: line 29, column 68
    function initialValue_1 () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return gw.api.database.Query.make(SecurityZone).select()
    }
    
    // 'initialValue' attribute on Variable at NewPolicyWizardSummaryStepScreen.pcf: line 33, column 57
    function initialValue_2 () : gw.api.web.account.AccountSearchConverter {
      return new gw.api.web.account.AccountSearchConverter()
    }
    
    // 'initialValue' attribute on Variable at NewPolicyWizardSummaryStepScreen.pcf: line 37, column 80
    function initialValue_3 () : gw.api.database.IQueryBeanResult<entity.ReturnPremiumPlan> {
      return gw.api.database.Query.make(entity.ReturnPremiumPlan).select()
    }
    
    // 'inputConversion' attribute on PickerInput (id=OverridingPayerAccount_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 141, column 46
    function inputConversion_74 (VALUE :  java.lang.String) : java.lang.Object {
      return accountSearchConverter.getAccount(VALUE)
    }
    
    // 'onChange' attribute on PostOnChange at NewPolicyWizardSummaryStepScreen.pcf: line 129, column 211
    function onChange_61 () : void {
      if (policyPeriod.AgencyBill) {policyPeriod.EligibleForFullPayDiscount = false; invoicingOverridesView.OverridingPayerAccount =null; invoicingOverridesView.OverridingInvoiceStream =null}
    }
    
    // 'onChange' attribute on PostOnChange at NewPolicyWizardSummaryStepScreen.pcf: line 143, column 49
    function onChange_69 () : void {
      maybeResetPaymentPlan()
    }
    
    // 'onChange' attribute on PostOnChange at NewPolicyWizardSummaryStepScreen.pcf: line 156, column 135
    function onChange_82 () : void {
      invoicingOverridesView.clearOverridingInvoiceStreamIfIncompatibleWithPaymentPlan(issuance.PolicyPaymentPlan);
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=addExistingContact) at NewPolicyWizardSummaryStepScreen.pcf: line 247, column 104
    function onPick_143 (PickedValue :  gw.plugin.contact.ContactResult) : void {
      gw.contact.ContactConnection.connectContactToPolicy(PickedValue, policyPeriod)
    }
    
    // 'required' attribute on PickerInput (id=OverridingPayerAccount_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 141, column 46
    function required_75 () : java.lang.Boolean {
      return policyPeriod.isListBill()
    }
    
    // 'value' attribute on TextCell (id=ContactName_Cell) at NewPolicyWizardSummaryStepScreen.pcf: line 263, column 59
    function sortValue_144 (policyContact :  entity.PolicyPeriodContact) : java.lang.Object {
      return policyContact
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at NewPolicyWizardSummaryStepScreen.pcf: line 268, column 47
    function sortValue_145 (policyContact :  entity.PolicyPeriodContact) : java.lang.Object {
      return policyContact.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=ContactRoles_Cell) at NewPolicyWizardSummaryStepScreen.pcf: line 272, column 97
    function sortValue_146 (policyContact :  entity.PolicyPeriodContact) : java.lang.Object {
      return gw.api.web.policy.PolicyPeriodUtil.getRolesForDisplay(policyContact)
    }
    
    // 'toRemove' attribute on RowIterator (id=policyContactIterator) at NewPolicyWizardSummaryStepScreen.pcf: line 257, column 56
    function toRemove_154 (policyContact :  entity.PolicyPeriodContact) : void {
      policyPeriod.removeFromContacts(policyContact)
    }
    
    // 'validationExpression' attribute on DateInput (id=PolicyPerExpirDate_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 69, column 52
    function validationExpression_16 () : java.lang.Object {
      return policyPeriod.PolicyPerExpirDate >= policyPeriod.PolicyPerEffDate ? null : DisplayKey.get("Web.NewPolicyDV.ExpirationDateError")
    }
    
    // 'validationExpression' attribute on PickerInput (id=OverridingPayerAccount_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 141, column 46
    function validationExpression_72 () : java.lang.Object {
      return policyPeriod.isListBill() and invoicingOverridesView.OverridingPayerAccount.isListBill() and policyPeriod.PaymentPlan == null ? null : gw.api.web.account.PolicyPeriods.checkForOverridingPayerAccountError(policyPeriod, invoicingOverridesView.OverridingPayerAccount)
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 174, column 47
    function valueRange_103 () : java.lang.Object {
      return policyPeriod.getApplicableDelinquencyPlans()
    }
    
    // 'valueRange' attribute on RangeInput (id=ReturnPremiumPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 182, column 49
    function valueRange_110 () : java.lang.Object {
      return allReturnPremiumPlans
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 99, column 44
    function valueRange_41 () : java.lang.Object {
      return allSecurityZones
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingMethod_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 127, column 57
    function valueRange_65 () : java.lang.Object {
      return PolicyPeriodBillingMethod.getTypeKeys(false)
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 154, column 42
    function valueRange_86 () : java.lang.Object {
      return invoicingOverridesView.RelatedPaymentPlans
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingInvoiceStream_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 166, column 47
    function valueRange_95 () : java.lang.Object {
      return policyPeriod.AgencyBill ? {} : gw.api.domain.invoice.InvoiceStreams.getCompatibleInvoiceStreams(invoicingOverridesView.DefaultPayer, issuance.PolicyPaymentPlan)
    }
    
    // 'value' attribute on TypeKeyInput (id=PolicyLOB_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 75, column 40
    function valueRoot_24 () : java.lang.Object {
      return policyPeriod.Policy
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 51, column 46
    function valueRoot_6 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on PickerInput (id=OverridingPayerAccount_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 141, column 46
    function valueRoot_78 () : java.lang.Object {
      return invoicingOverridesView
    }
    
    // 'value' attribute on RangeInput (id=PaymentPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 154, column 42
    function valueRoot_85 () : java.lang.Object {
      return issuance
    }
    
    // 'value' attribute on RangeInput (id=DelinquencyPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 174, column 47
    function value_100 () : entity.DelinquencyPlan {
      return policyPeriod.PolicyPeriodDelinquencyPlan
    }
    
    // 'value' attribute on RangeInput (id=ReturnPremiumPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 182, column 49
    function value_107 () : entity.ReturnPremiumPlan {
      return policyPeriod.ReturnPremiumPlan
    }
    
    // 'value' attribute on DateInput (id=PolicyPerEffDate_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 62, column 50
    function value_12 () : java.util.Date {
      return policyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on InputIterator at NewPolicyWizardSummaryStepScreen.pcf: line 188, column 54
    function value_136 () : entity.ProducerCodeRoleEntry[] {
      return producerCodeRoleEntries
    }
    
    // 'value' attribute on RowIterator (id=policyContactIterator) at NewPolicyWizardSummaryStepScreen.pcf: line 257, column 56
    function value_155 () : entity.PolicyPeriodContact[] {
      return policyPeriod.Contacts
    }
    
    // 'value' attribute on DateInput (id=PolicyPerExpirDate_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 69, column 52
    function value_17 () : java.util.Date {
      return policyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on TypeKeyInput (id=PolicyLOB_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 75, column 40
    function value_22 () : typekey.LOBCode {
      return policyPeriod.Policy.LOBCode
    }
    
    // 'value' attribute on BooleanRadioInput (id=AssignedRisk_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 80, column 46
    function value_26 () : java.lang.Boolean {
      return policyPeriod.AssignedRisk
    }
    
    // 'value' attribute on TypeKeyInput (id=RiskJurisdiction_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 86, column 45
    function value_30 () : typekey.Jurisdiction {
      return policyPeriod.RiskJurisdiction
    }
    
    // 'value' attribute on TypeKeyInput (id=UWCompany_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 92, column 42
    function value_34 () : typekey.UWCompany {
      return policyPeriod.UWCompany
    }
    
    // 'value' attribute on RangeInput (id=SecurityZone_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 99, column 44
    function value_38 () : entity.SecurityZone {
      return policyPeriod.SecurityZone
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 51, column 46
    function value_4 () : java.lang.String {
      return policyPeriod.PolicyNumber
    }
    
    // 'value' attribute on TextInput (id=Underwriter_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 104, column 45
    function value_45 () : java.lang.String {
      return policyPeriod.Underwriter
    }
    
    // 'value' attribute on BooleanRadioInput (id=EligibleForFullPayDiscount_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 111, column 47
    function value_51 () : java.lang.Boolean {
      return policyPeriod.EligibleForFullPayDiscount
    }
    
    // 'value' attribute on BooleanRadioInput (id=RequireFinalAudit_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 117, column 51
    function value_57 () : java.lang.Boolean {
      return policyPeriod.RequireFinalAudit
    }
    
    // 'value' attribute on RangeInput (id=BillingMethod_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 127, column 57
    function value_62 () : typekey.PolicyPeriodBillingMethod {
      return policyPeriod.BillingMethod
    }
    
    // 'value' attribute on PickerInput (id=OverridingPayerAccount_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 141, column 46
    function value_76 () : entity.Account {
      return invoicingOverridesView.OverridingPayerAccount
    }
    
    // 'value' attribute on TextInput (id=DBA_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 56, column 37
    function value_8 () : java.lang.String {
      return policyPeriod.DBA
    }
    
    // 'value' attribute on RangeInput (id=PaymentPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 154, column 42
    function value_83 () : entity.PaymentPlan {
      return issuance.PolicyPaymentPlan
    }
    
    // 'value' attribute on RangeInput (id=OverridingInvoiceStream_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 166, column 47
    function value_92 () : entity.InvoiceStream {
      return invoicingOverridesView.OverridingInvoiceStream
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 174, column 47
    function verifyValueRangeIsAllowedType_104 ($$arg :  entity.DelinquencyPlan[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 174, column 47
    function verifyValueRangeIsAllowedType_104 ($$arg :  gw.api.database.IQueryBeanResult<entity.DelinquencyPlan>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 174, column 47
    function verifyValueRangeIsAllowedType_104 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ReturnPremiumPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 182, column 49
    function verifyValueRangeIsAllowedType_111 ($$arg :  entity.ReturnPremiumPlan[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ReturnPremiumPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 182, column 49
    function verifyValueRangeIsAllowedType_111 ($$arg :  gw.api.database.IQueryBeanResult<entity.ReturnPremiumPlan>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ReturnPremiumPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 182, column 49
    function verifyValueRangeIsAllowedType_111 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 99, column 44
    function verifyValueRangeIsAllowedType_42 ($$arg :  entity.SecurityZone[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 99, column 44
    function verifyValueRangeIsAllowedType_42 ($$arg :  gw.api.database.IQueryBeanResult<entity.SecurityZone>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 99, column 44
    function verifyValueRangeIsAllowedType_42 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingMethod_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 127, column 57
    function verifyValueRangeIsAllowedType_66 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingMethod_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 127, column 57
    function verifyValueRangeIsAllowedType_66 ($$arg :  typekey.PolicyPeriodBillingMethod[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 154, column 42
    function verifyValueRangeIsAllowedType_87 ($$arg :  entity.PaymentPlan[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 154, column 42
    function verifyValueRangeIsAllowedType_87 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentPlan>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 154, column 42
    function verifyValueRangeIsAllowedType_87 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingInvoiceStream_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 166, column 47
    function verifyValueRangeIsAllowedType_96 ($$arg :  entity.InvoiceStream[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingInvoiceStream_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 166, column 47
    function verifyValueRangeIsAllowedType_96 ($$arg :  gw.api.database.IQueryBeanResult<entity.InvoiceStream>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingInvoiceStream_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 166, column 47
    function verifyValueRangeIsAllowedType_96 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 174, column 47
    function verifyValueRange_105 () : void {
      var __valueRangeArg = policyPeriod.getApplicableDelinquencyPlans()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_104(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=ReturnPremiumPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 182, column 49
    function verifyValueRange_112 () : void {
      var __valueRangeArg = allReturnPremiumPlans
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_111(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 99, column 44
    function verifyValueRange_43 () : void {
      var __valueRangeArg = allSecurityZones
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_42(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingMethod_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 127, column 57
    function verifyValueRange_67 () : void {
      var __valueRangeArg = PolicyPeriodBillingMethod.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_66(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 154, column 42
    function verifyValueRange_88 () : void {
      var __valueRangeArg = invoicingOverridesView.RelatedPaymentPlans
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_87(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingInvoiceStream_Input) at NewPolicyWizardSummaryStepScreen.pcf: line 166, column 47
    function verifyValueRange_97 () : void {
      var __valueRangeArg = policyPeriod.AgencyBill ? {} : gw.api.domain.invoice.InvoiceStreams.getCompatibleInvoiceStreams(invoicingOverridesView.DefaultPayer, issuance.PolicyPaymentPlan)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_96(__valueRangeArg)
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get accountSearchConverter () : gw.api.web.account.AccountSearchConverter {
      return getVariableValue("accountSearchConverter", 0) as gw.api.web.account.AccountSearchConverter
    }
    
    property set accountSearchConverter ($arg :  gw.api.web.account.AccountSearchConverter) {
      setVariableValue("accountSearchConverter", 0, $arg)
    }
    
    property get allReturnPremiumPlans () : gw.api.database.IQueryBeanResult<entity.ReturnPremiumPlan> {
      return getVariableValue("allReturnPremiumPlans", 0) as gw.api.database.IQueryBeanResult<entity.ReturnPremiumPlan>
    }
    
    property set allReturnPremiumPlans ($arg :  gw.api.database.IQueryBeanResult<entity.ReturnPremiumPlan>) {
      setVariableValue("allReturnPremiumPlans", 0, $arg)
    }
    
    property get allSecurityZones () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return getVariableValue("allSecurityZones", 0) as gw.api.database.IQueryBeanResult<SecurityZone>
    }
    
    property set allSecurityZones ($arg :  gw.api.database.IQueryBeanResult<SecurityZone>) {
      setVariableValue("allSecurityZones", 0, $arg)
    }
    
    property get invoicingOverridesView () : gw.invoice.InvoicingOverridesView {
      return getRequireValue("invoicingOverridesView", 0) as gw.invoice.InvoicingOverridesView
    }
    
    property set invoicingOverridesView ($arg :  gw.invoice.InvoicingOverridesView) {
      setRequireValue("invoicingOverridesView", 0, $arg)
    }
    
    property get issuance () : Issuance {
      return getRequireValue("issuance", 0) as Issuance
    }
    
    property set issuance ($arg :  Issuance) {
      setRequireValue("issuance", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get producerCodeRoleEntries () : ProducerCodeRoleEntry[] {
      return getRequireValue("producerCodeRoleEntries", 0) as ProducerCodeRoleEntry[]
    }
    
    property set producerCodeRoleEntries ($arg :  ProducerCodeRoleEntry[]) {
      setRequireValue("producerCodeRoleEntries", 0, $arg)
    }
    
    property get producerSearchConverter () : gw.api.web.producer.ProducerSearchConverter {
      return getVariableValue("producerSearchConverter", 0) as gw.api.web.producer.ProducerSearchConverter
    }
    
    property set producerSearchConverter ($arg :  gw.api.web.producer.ProducerSearchConverter) {
      setVariableValue("producerSearchConverter", 0, $arg)
    }
    
    function autoSelectSoleProducerCode(producerCodeRoleEntry : ProducerCodeRoleEntry) {
            var activeProducerCodes = producerCodeRoleEntry.Producer.ActiveProducerCodes;
            if (activeProducerCodes.length == 1) {
              producerCodeRoleEntry.ProducerCode = activeProducerCodes[0];
            }
          }
    function validatePrimaryProducer(producer : Producer) : String{
      if(not policyPeriod.AgencyBill){
        return null
      }
      return producer.AgencyBillPlan == null ? DisplayKey.get("Web.NewPolicyDV.InvalidPrimaryProducer") : null
    }
    
    function maybeResetPaymentPlan() {
      var overridingPayerAccount = invoicingOverridesView.OverridingPayerAccount
      if (policyPeriod.ListBill && 
          overridingPayerAccount != null &&
          !overridingPayerAccount.PaymentPlans.contains(issuance.PolicyPaymentPlan)) {
        issuance.PolicyPaymentPlan = null
      }
    }
    
    function validateProducer(producer : Producer, role : PolicyRole) : String{
      if(producer.Currency != account.Currency){
        return DisplayKey.get("Web.NewPolicyDV.InvalidProducer", account.Currency)
      }
      if(role == PolicyRole.TC_PRIMARY){
       return validatePrimaryProducer(producer)
      }
      return null
    }
      
    
    
  }
  
  
}