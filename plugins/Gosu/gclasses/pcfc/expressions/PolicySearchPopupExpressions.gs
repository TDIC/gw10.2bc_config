package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/PolicySearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicySearchPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/search/PolicySearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicySearchPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    static function __constructorIndex (policyForRelatedLookup :  boolean) : int {
      return 1
    }
    
    // 'def' attribute on ScreenRef at PolicySearchPopup.pcf: line 18, column 97
    function def_onEnter_0 (def :  pcf.PolicySearchScreen) : void {
      def.onEnter(false, false, /*allowsArchiveInclusion*/policyForRelatedLookup)
    }
    
    // 'def' attribute on ScreenRef at PolicySearchPopup.pcf: line 18, column 97
    function def_refreshVariables_1 (def :  pcf.PolicySearchScreen) : void {
      def.refreshVariables(false, false, /*allowsArchiveInclusion*/policyForRelatedLookup)
    }
    
    override property get CurrentLocation () : pcf.PolicySearchPopup {
      return super.CurrentLocation as pcf.PolicySearchPopup
    }
    
    property get policyForRelatedLookup () : boolean {
      return getVariableValue("policyForRelatedLookup", 0) as java.lang.Boolean
    }
    
    property set policyForRelatedLookup ($arg :  boolean) {
      setVariableValue("policyForRelatedLookup", 0, $arg)
    }
    
    
  }
  
  
}