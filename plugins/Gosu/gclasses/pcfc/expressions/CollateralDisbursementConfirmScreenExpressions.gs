package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralDisbursementConfirmScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CollateralDisbursementConfirmScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/collateral/CollateralDisbursementConfirmScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CollateralDisbursementConfirmScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=shortfall_Input) at CollateralDisbursementConfirmScreen.pcf: line 43, column 62
    function currency_8 () : typekey.Currency {
      return disbursement.Currency
    }
    
    // 'def' attribute on PanelRef at CollateralDisbursementConfirmScreen.pcf: line 54, column 75
    function def_onEnter_16 (def :  pcf.CreateDisbursementWizardApprovalActivityDV) : void {
      def.onEnter(approvalActivity)
    }
    
    // 'def' attribute on PanelRef at CollateralDisbursementConfirmScreen.pcf: line 54, column 75
    function def_refreshVariables_17 (def :  pcf.CreateDisbursementWizardApprovalActivityDV) : void {
      def.refreshVariables(approvalActivity)
    }
    
    // 'initialValue' attribute on Variable at CollateralDisbursementConfirmScreen.pcf: line 13, column 60
    function initialValue_0 () : gw.api.web.collateral.CollateralDrawdownUtil {
      return calculateShortfall()
    }
    
    // 'initialValue' attribute on Variable at CollateralDisbursementConfirmScreen.pcf: line 17, column 39
    function initialValue_1 () : entity.DisbApprActivity {
      return disbursement.getOpenApprovalActivity()
    }
    
    // 'label' attribute on Label at CollateralDisbursementConfirmScreen.pcf: line 31, column 210
    function label_3 () : java.lang.String {
      return DisplayKey.get("Web.CollateralDisbursementConfirmScreen.CollateralDisbursement", disbursement.Amount.render(), disbursement.Collateral.Account.AccountNumber, disbursement.DueDate)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=shortfallCovered_Input) at CollateralDisbursementConfirmScreen.pcf: line 50, column 62
    function value_12 () : gw.pl.currency.MonetaryAmount {
      return collateralShortfall.getShortfallCoveredByLOC()
    }
    
    // 'value' attribute on MonetaryAmountInput (id=shortfall_Input) at CollateralDisbursementConfirmScreen.pcf: line 43, column 62
    function value_7 () : gw.pl.currency.MonetaryAmount {
      return collateralShortfall.getShortfall()
    }
    
    // 'visible' attribute on AlertBar (id=ApprovalActivityAlertBar) at CollateralDisbursementConfirmScreen.pcf: line 24, column 43
    function visible_2 () : java.lang.Boolean {
      return approvalActivity != null
    }
    
    // 'visible' attribute on InputDivider at CollateralDisbursementConfirmScreen.pcf: line 33, column 62
    function visible_4 () : java.lang.Boolean {
      return collateralShortfall.isCheckboxVisible()
    }
    
    property get approvalActivity () : entity.DisbApprActivity {
      return getVariableValue("approvalActivity", 0) as entity.DisbApprActivity
    }
    
    property set approvalActivity ($arg :  entity.DisbApprActivity) {
      setVariableValue("approvalActivity", 0, $arg)
    }
    
    property get collateralShortfall () : gw.api.web.collateral.CollateralDrawdownUtil {
      return getVariableValue("collateralShortfall", 0) as gw.api.web.collateral.CollateralDrawdownUtil
    }
    
    property set collateralShortfall ($arg :  gw.api.web.collateral.CollateralDrawdownUtil) {
      setVariableValue("collateralShortfall", 0, $arg)
    }
    
    property get disbursement () : CollateralDisbursement {
      return getRequireValue("disbursement", 0) as CollateralDisbursement
    }
    
    property set disbursement ($arg :  CollateralDisbursement) {
      setRequireValue("disbursement", 0, $arg)
    }
    
    
    function calculateShortfall(): gw.api.web.collateral.CollateralDrawdownUtil{
      var collShort = new gw.api.web.collateral.CollateralDrawdownUtil(disbursement.Collateral);
      
      collShort.setAmount(disbursement.Amount);
      collShort.calculateShortfall();
      return collShort;
    }
        
    
    
  }
  
  
}