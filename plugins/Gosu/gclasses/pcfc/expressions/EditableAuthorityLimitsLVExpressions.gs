package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/authoritylimits/EditableAuthorityLimitsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class EditableAuthorityLimitsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/authoritylimits/EditableAuthorityLimitsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class EditableAuthorityLimitsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=LimitType_Cell) at EditableAuthorityLimitsLV.pcf: line 34, column 51
    function sortValue_0 (authorityLimit :  entity.AuthorityLimit) : java.lang.Object {
      return authorityLimit.LimitType
    }
    
    // 'value' attribute on TypeKeyCell (id=LimitCurrency_Cell) at EditableAuthorityLimitsLV.pcf: line 42, column 68
    function sortValue_1 (authorityLimit :  entity.AuthorityLimit) : java.lang.Object {
      var authorityLimitWrapper : gw.admin.AuthorityLimitWrapper = (new gw.admin.AuthorityLimitWrapper(authorityLimit))
return authorityLimitWrapper.Currency
    }
    
    // 'value' attribute on TextCell (id=LimitAmount_Cell) at EditableAuthorityLimitsLV.pcf: line 53, column 48
    function sortValue_3 (authorityLimit :  entity.AuthorityLimit) : java.lang.Object {
      var authorityLimitWrapper : gw.admin.AuthorityLimitWrapper = (new gw.admin.AuthorityLimitWrapper(authorityLimit))
return authorityLimitWrapper.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=LimitMonetaryAmount_Cell) at EditableAuthorityLimitsLV.pcf: line 62, column 25
    function sortValue_5 (authorityLimit :  entity.AuthorityLimit) : java.lang.Object {
      return authorityLimit.LimitAmount
    }
    
    // 'toAdd' attribute on RowIterator at EditableAuthorityLimitsLV.pcf: line 21, column 43
    function toAdd_26 (authorityLimit :  entity.AuthorityLimit) : void {
      user.AuthorityProfile.addToLimits(createAuthorityLimit(authorityLimit))
    }
    
    // 'toRemove' attribute on RowIterator at EditableAuthorityLimitsLV.pcf: line 21, column 43
    function toRemove_27 (authorityLimit :  entity.AuthorityLimit) : void {
      user.AuthorityProfile.removeFromLimits(authorityLimit)
    }
    
    // 'value' attribute on RowIterator at EditableAuthorityLimitsLV.pcf: line 21, column 43
    function value_28 () : entity.AuthorityLimit[] {
      return user.AuthorityProfile.Limits
    }
    
    // 'visible' attribute on TypeKeyCell (id=LimitCurrency_Cell) at EditableAuthorityLimitsLV.pcf: line 42, column 68
    function visible_2 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on TextCell (id=LimitAmount_Cell) at EditableAuthorityLimitsLV.pcf: line 53, column 48
    function visible_4 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    property get user () : User {
      return getRequireValue("user", 0) as User
    }
    
    property set user ($arg :  User) {
      setRequireValue("user", 0, $arg)
    }
    
    function createAuthorityLimit(authorityLimit : AuthorityLimit) : AuthorityLimit {
      var defautCurrency = gw.api.util.CurrencyUtil.getDefaultCurrency()
      authorityLimit.LimitAmount = gw.api.financials.MonetaryAmounts.zeroIfNull(authorityLimit.LimitAmount, defautCurrency)
      return authorityLimit
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/authoritylimits/EditableAuthorityLimitsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends EditableAuthorityLimitsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=LimitCurrency_Cell) at EditableAuthorityLimitsLV.pcf: line 42, column 68
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      authorityLimitWrapper.Currency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on TextCell (id=LimitAmount_Cell) at EditableAuthorityLimitsLV.pcf: line 53, column 48
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      authorityLimitWrapper.Amount = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on TypeKeyCell (id=LimitType_Cell) at EditableAuthorityLimitsLV.pcf: line 34, column 51
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      authorityLimit.LimitType = (__VALUE_TO_SET as typekey.AuthorityLimitType)
    }
    
    // 'initialValue' attribute on Variable at EditableAuthorityLimitsLV.pcf: line 25, column 48
    function initialValue_6 () : gw.admin.AuthorityLimitWrapper {
      return new gw.admin.AuthorityLimitWrapper(authorityLimit)
    }
    
    // RowIterator at EditableAuthorityLimitsLV.pcf: line 21, column 43
    function initializeVariables_25 () : void {
        authorityLimitWrapper = new gw.admin.AuthorityLimitWrapper(authorityLimit);

    }
    
    // 'inputConversion' attribute on TextCell (id=LimitAmount_Cell) at EditableAuthorityLimitsLV.pcf: line 53, column 48
    function inputConversion_16 (VALUE :  java.lang.String) : java.lang.Object {
      return VALUE == null ? 0 : VALUE
    }
    
    // 'value' attribute on TypeKeyCell (id=LimitCurrency_Cell) at EditableAuthorityLimitsLV.pcf: line 42, column 68
    function valueRoot_13 () : java.lang.Object {
      return authorityLimitWrapper
    }
    
    // 'value' attribute on TypeKeyCell (id=LimitType_Cell) at EditableAuthorityLimitsLV.pcf: line 34, column 51
    function valueRoot_9 () : java.lang.Object {
      return authorityLimit
    }
    
    // 'value' attribute on TypeKeyCell (id=LimitCurrency_Cell) at EditableAuthorityLimitsLV.pcf: line 42, column 68
    function value_11 () : typekey.Currency {
      return authorityLimitWrapper.Currency
    }
    
    // 'value' attribute on TextCell (id=LimitAmount_Cell) at EditableAuthorityLimitsLV.pcf: line 53, column 48
    function value_17 () : java.math.BigDecimal {
      return authorityLimitWrapper.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=LimitMonetaryAmount_Cell) at EditableAuthorityLimitsLV.pcf: line 62, column 25
    function value_22 () : gw.pl.currency.MonetaryAmount {
      return authorityLimit.LimitAmount
    }
    
    // 'value' attribute on TypeKeyCell (id=LimitType_Cell) at EditableAuthorityLimitsLV.pcf: line 34, column 51
    function value_7 () : typekey.AuthorityLimitType {
      return authorityLimit.LimitType
    }
    
    // 'visible' attribute on TypeKeyCell (id=LimitCurrency_Cell) at EditableAuthorityLimitsLV.pcf: line 42, column 68
    function visible_14 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on TextCell (id=LimitAmount_Cell) at EditableAuthorityLimitsLV.pcf: line 53, column 48
    function visible_20 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    property get authorityLimit () : entity.AuthorityLimit {
      return getIteratedValue(1) as entity.AuthorityLimit
    }
    
    property get authorityLimitWrapper () : gw.admin.AuthorityLimitWrapper {
      return getVariableValue("authorityLimitWrapper", 1) as gw.admin.AuthorityLimitWrapper
    }
    
    property set authorityLimitWrapper ($arg :  gw.admin.AuthorityLimitWrapper) {
      setVariableValue("authorityLimitWrapper", 1, $arg)
    }
    
    
  }
  
  
}