package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/PolicySearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicySearchExpressions {
  @javax.annotation.Generated("config/web/pcf/search/PolicySearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicySearchExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=PolicySearch) at PolicySearch.pcf: line 8, column 67
    static function canVisit_2 () : java.lang.Boolean {
      return perm.System.viewsearch and perm.System.plcysearch
    }
    
    // 'def' attribute on ScreenRef at PolicySearch.pcf: line 10, column 51
    function def_onEnter_0 (def :  pcf.PolicySearchScreen) : void {
      def.onEnter(true, true, true)
    }
    
    // 'def' attribute on ScreenRef at PolicySearch.pcf: line 10, column 51
    function def_refreshVariables_1 (def :  pcf.PolicySearchScreen) : void {
      def.refreshVariables(true, true, true)
    }
    
    // Page (id=PolicySearch) at PolicySearch.pcf: line 8, column 67
    static function parent_3 () : pcf.api.Destination {
      return pcf.SearchGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.PolicySearch {
      return super.CurrentLocation as pcf.PolicySearch
    }
    
    
  }
  
  
}