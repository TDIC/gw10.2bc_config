package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanReasonsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyPlanReasonsDVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanReasonsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyPlanReasonsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef (id=PlanReasonPanel) at DelinquencyPlanReasonsDV.pcf: line 22, column 28
    function def_onEnter_1 (def :  pcf.DelinquencyPlanReasonsLV) : void {
      def.onEnter(dlnqPlan, notInUse)
    }
    
    // 'def' attribute on PanelRef at DelinquencyPlanReasonsDV.pcf: line 67, column 56
    function def_onEnter_20 (def :  pcf.DelinquencyPlanEventsLV) : void {
      def.onEnter(currentReason)
    }
    
    // 'def' attribute on PanelRef (id=PlanReasonPanel) at DelinquencyPlanReasonsDV.pcf: line 22, column 28
    function def_refreshVariables_2 (def :  pcf.DelinquencyPlanReasonsLV) : void {
      def.refreshVariables(dlnqPlan, notInUse)
    }
    
    // 'def' attribute on PanelRef at DelinquencyPlanReasonsDV.pcf: line 67, column 56
    function def_refreshVariables_21 (def :  pcf.DelinquencyPlanEventsLV) : void {
      def.refreshVariables(currentReason)
    }
    
    // 'value' attribute on RangeInput (id=WorkflowType_Input) at DelinquencyPlanReasonsDV.pcf: line 58, column 45
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentReason.WorkflowType = (__VALUE_TO_SET as typekey.Workflow)
    }
    
    // 'value' attribute on TypeKeyInput (id=DelinquencyReason_Input) at DelinquencyPlanReasonsDV.pcf: line 47, column 54
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      currentReason.DelinquencyReason = (__VALUE_TO_SET as typekey.DelinquencyReason)
    }
    
    // 'editable' attribute on TypeKeyInput (id=DelinquencyReason_Input) at DelinquencyPlanReasonsDV.pcf: line 47, column 54
    function editable_3 () : java.lang.Boolean {
      return isFieldEditable(currentReason)
    }
    
    // 'initialValue' attribute on Variable at DelinquencyPlanReasonsDV.pcf: line 18, column 23
    function initialValue_0 () : boolean {
      return not dlnqPlan.InUse
    }
    
    // 'selectionOnEnter' attribute on ListDetailPanel (id=DelinquencyPlanReasonsDV) at DelinquencyPlanReasonsDV.pcf: line 10, column 43
    function selectionOnEnter_22 () : java.lang.Object {
      return ( dlnqPlan.DelinquencyPlanReasons.length > 0 ? dlnqPlan.DelinquencyPlanReasons[0] : null )
    }
    
    // 'validationExpression' attribute on TypeKeyInput (id=DelinquencyReason_Input) at DelinquencyPlanReasonsDV.pcf: line 47, column 54
    function validationExpression_4 () : java.lang.Object {
      return gw.api.web.delinquency.DelinquencyPlanUtil.validateDelinquencyPlanReasonUnique(dlnqPlan, currentReason)
    }
    
    // 'valueRange' attribute on RangeInput (id=WorkflowType_Input) at DelinquencyPlanReasonsDV.pcf: line 58, column 45
    function valueRange_15 () : java.lang.Object {
      return gw.api.web.delinquency.DelinquencyPlanUtil.getDelinquencyWorkflows()
    }
    
    // 'value' attribute on TypeKeyInput (id=DelinquencyReason_Input) at DelinquencyPlanReasonsDV.pcf: line 47, column 54
    function valueRoot_7 () : java.lang.Object {
      return currentReason
    }
    
    // 'value' attribute on RangeInput (id=WorkflowType_Input) at DelinquencyPlanReasonsDV.pcf: line 58, column 45
    function value_12 () : typekey.Workflow {
      return currentReason.WorkflowType
    }
    
    // 'value' attribute on TypeKeyInput (id=DelinquencyReason_Input) at DelinquencyPlanReasonsDV.pcf: line 47, column 54
    function value_5 () : typekey.DelinquencyReason {
      return currentReason.DelinquencyReason
    }
    
    // 'valueRange' attribute on RangeInput (id=WorkflowType_Input) at DelinquencyPlanReasonsDV.pcf: line 58, column 45
    function verifyValueRangeIsAllowedType_16 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WorkflowType_Input) at DelinquencyPlanReasonsDV.pcf: line 58, column 45
    function verifyValueRangeIsAllowedType_16 ($$arg :  typekey.Workflow[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=WorkflowType_Input) at DelinquencyPlanReasonsDV.pcf: line 58, column 45
    function verifyValueRange_17 () : void {
      var __valueRangeArg = gw.api.web.delinquency.DelinquencyPlanUtil.getDelinquencyWorkflows()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_16(__valueRangeArg)
    }
    
    property get currentReason () : DelinquencyPlanReason {
      return getCurrentSelection(0) as DelinquencyPlanReason
    }
    
    property set currentReason ($arg :  DelinquencyPlanReason) {
      setCurrentSelection(0, $arg)
    }
    
    property get dlnqPlan () : DelinquencyPlan {
      return getRequireValue("dlnqPlan", 0) as DelinquencyPlan
    }
    
    property set dlnqPlan ($arg :  DelinquencyPlan) {
      setRequireValue("dlnqPlan", 0, $arg)
    }
    
    property get notInUse () : boolean {
      return getVariableValue("notInUse", 0) as java.lang.Boolean
    }
    
    property set notInUse ($arg :  boolean) {
      setVariableValue("notInUse", 0, $arg)
    }
    
    
    function isFieldEditable(planReason : DelinquencyPlanReason) : Boolean {
            if (null == planReason) {
              return false
            }
            if (planReason.New) {
              return true
            }
            return notInUse
          }
        
    
    
  }
  
  
}