package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_DispositionsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyDistributionWizard_DispositionsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_DispositionsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyDistributionWizard_DispositionsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=SavePayment) at AgencyDistributionWizard_DispositionsScreen.pcf: line 26, column 23
    function action_3 () : void {
      wizardState.save(CurrentLocation as pcf.api.Wizard)
    }
    
    // 'available' attribute on ToolbarButton (id=SavePayment) at AgencyDistributionWizard_DispositionsScreen.pcf: line 26, column 23
    function available_2 () : java.lang.Boolean {
      return !wizardState.AgencyCycleDistView.AgencyCycleDist.Modifying
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=BeforeThisPayment_Input) at AgencyDistributionWizard_DispositionsScreen.pcf: line 67, column 57
    function currency_20 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'def' attribute on PanelRef at AgencyDistributionWizard_DispositionsScreen.pcf: line 34, column 77
    function def_onEnter_6 (def :  pcf.AgencyDistributionWizard_DistributionInfoPanelSet) : void {
      def.onEnter(wizardState)
    }
    
    // 'def' attribute on PanelRef at AgencyDistributionWizard_DispositionsScreen.pcf: line 34, column 77
    function def_refreshVariables_7 (def :  pcf.AgencyDistributionWizard_DistributionInfoPanelSet) : void {
      def.refreshVariables(wizardState)
    }
    
    // 'value' attribute on RangeRadioInput (id=DistributionDifferenceMethod_Input) at AgencyDistributionWizard_DispositionsScreen.pcf: line 53, column 103
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      wizardState.DistributionDifferenceMethod = (__VALUE_TO_SET as gw.agencybill.AgencyDistributionWizardHelper.DistributionDifferenceMethodEnum)
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_DispositionsScreen.pcf: line 13, column 31
    function initialValue_0 () : entity.Producer {
      return wizardState.MoneySetup.Producer
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_DispositionsScreen.pcf: line 17, column 37
    function initialValue_1 () : entity.BaseDistItem[] {
      return wizardState.ExceptionDistItems
    }
    
    // 'label' attribute on MonetaryAmountInput (id=BeforeThisPayment_Input) at AgencyDistributionWizard_DispositionsScreen.pcf: line 67, column 57
    function label_17 () : java.lang.Object {
      return DisplayKey.get("Web.AgencyDistributionWizard.Step.Distribution.WriteOffDV.BeforeThisDistribution", wizardState.DistributionTypeName)
    }
    
    // 'label' attribute on MonetaryAmountInput (id=UnappliedBalanceThisPayment_Input) at AgencyDistributionWizard_DispositionsScreen.pcf: line 73, column 57
    function label_23 () : java.lang.Object {
      return DisplayKey.get("Web.AgencyDistributionWizard.Step.Distribution.WriteOffDV.UnappliedBalanceThisDistribution", wizardState.DistributionTypeName)
    }
    
    // 'label' attribute on MonetaryAmountInput (id=AfterThisPayment_Input) at AgencyDistributionWizard_DispositionsScreen.pcf: line 80, column 56
    function label_28 () : java.lang.Object {
      return DisplayKey.get("Web.AgencyDistributionWizard.Step.Distribution.WriteOffDV.AfterThisDistribution", wizardState.DistributionTypeName)
    }
    
    // 'label' attribute on MonetaryAmountInput (id=WriteOffExpenseThisPayment_Input) at AgencyDistributionWizard_DispositionsScreen.pcf: line 90, column 50
    function label_34 () : java.lang.Object {
      return DisplayKey.get("Web.AgencyDistributionWizard.Step.Distribution.WriteOffDV.WriteOffExpenseThisDistribution", wizardState.DistributionTypeName)
    }
    
    // 'label' attribute on AlertBar (id=ExceptionsDescription) at AgencyDistributionWizard_DispositionsScreen.pcf: line 32, column 46
    function label_5 () : java.lang.Object {
      return wizardState.getExceptionsDescription(exceptionDistItems)
    }
    
    // 'label' attribute on RangeRadioInput (id=DistributionDifferenceMethod_Input) at AgencyDistributionWizard_DispositionsScreen.pcf: line 53, column 103
    function label_8 () : java.lang.Object {
      return wizardState.DistributionDifferenceMethodLabel
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 119, column 58
    function sortValue_40 (exceptionDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : entity.InvoiceItem = (exceptionDistItem.InvoiceItem)
return invoiceItem.Invoice.InvoiceNumber
    }
    
    // 'value' attribute on DateCell (id=StatementDate_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 123, column 54
    function sortValue_41 (exceptionDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : entity.InvoiceItem = (exceptionDistItem.InvoiceItem)
return invoiceItem.Invoice.EventDate
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 128, column 66
    function sortValue_42 (exceptionDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : entity.InvoiceItem = (exceptionDistItem.InvoiceItem)
return invoiceItem.PolicyPeriod.PolicyNumberLong
    }
    
    // 'value' attribute on TextCell (id=Item_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 133, column 58
    function sortValue_43 (exceptionDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : entity.InvoiceItem = (exceptionDistItem.InvoiceItem)
return invoiceItem.DisplayNameAsItemType
    }
    
    // 'value' attribute on TextCell (id=Issue_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 137, column 63
    function sortValue_44 (exceptionDistItem :  entity.BaseDistItem) : java.lang.Object {
      return exceptionDistItem.ExceptionDescription
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Difference_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 143, column 62
    function sortValue_45 (exceptionDistItem :  entity.BaseDistItem) : java.lang.Object {
      return exceptionDistItem.NetDifferenceAmount
    }
    
    // 'value' attribute on RangeCell (id=Disposition_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 152, column 90
    function sortValue_46 (exceptionDistItem :  entity.BaseDistItem) : java.lang.Object {
      return exceptionDistItem.Disposition
    }
    
    // 'value' attribute on TextCell (id=OwnerAccount_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 158, column 43
    function sortValue_47 (exceptionDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : entity.InvoiceItem = (exceptionDistItem.InvoiceItem)
return invoiceItem.Owner
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DistributionDifferenceMethod_Input) at AgencyDistributionWizard_DispositionsScreen.pcf: line 53, column 103
    function valueRange_12 () : java.lang.Object {
      return wizardState.DistributionDifferenceMethodValues
    }
    
    // 'value' attribute on RangeRadioInput (id=DistributionDifferenceMethod_Input) at AgencyDistributionWizard_DispositionsScreen.pcf: line 53, column 103
    function valueRoot_11 () : java.lang.Object {
      return wizardState
    }
    
    // 'value' attribute on MonetaryAmountInput (id=BeforeThisPayment_Input) at AgencyDistributionWizard_DispositionsScreen.pcf: line 67, column 57
    function value_18 () : gw.pl.currency.MonetaryAmount {
      return wizardState.UnappliedBalanceBefore
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnappliedBalanceThisPayment_Input) at AgencyDistributionWizard_DispositionsScreen.pcf: line 73, column 57
    function value_24 () : gw.pl.currency.MonetaryAmount {
      return wizardState.UnappliedBalanceChange
    }
    
    // 'value' attribute on MonetaryAmountInput (id=AfterThisPayment_Input) at AgencyDistributionWizard_DispositionsScreen.pcf: line 80, column 56
    function value_29 () : gw.pl.currency.MonetaryAmount {
      return wizardState.UnappliedBalanceAfter
    }
    
    // 'value' attribute on MonetaryAmountInput (id=WriteOffExpenseThisPayment_Input) at AgencyDistributionWizard_DispositionsScreen.pcf: line 90, column 50
    function value_35 () : gw.pl.currency.MonetaryAmount {
      return wizardState.WriteOffExpense
    }
    
    // 'value' attribute on RowIterator at AgencyDistributionWizard_DispositionsScreen.pcf: line 107, column 45
    function value_89 () : entity.BaseDistItem[] {
      return exceptionDistItems
    }
    
    // 'value' attribute on RangeRadioInput (id=DistributionDifferenceMethod_Input) at AgencyDistributionWizard_DispositionsScreen.pcf: line 53, column 103
    function value_9 () : gw.agencybill.AgencyDistributionWizardHelper.DistributionDifferenceMethodEnum {
      return wizardState.DistributionDifferenceMethod
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DistributionDifferenceMethod_Input) at AgencyDistributionWizard_DispositionsScreen.pcf: line 53, column 103
    function verifyValueRangeIsAllowedType_13 ($$arg :  gw.agencybill.AgencyDistributionWizardHelper.DistributionDifferenceMethodEnum[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DistributionDifferenceMethod_Input) at AgencyDistributionWizard_DispositionsScreen.pcf: line 53, column 103
    function verifyValueRangeIsAllowedType_13 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DistributionDifferenceMethod_Input) at AgencyDistributionWizard_DispositionsScreen.pcf: line 53, column 103
    function verifyValueRange_14 () : void {
      var __valueRangeArg = wizardState.DistributionDifferenceMethodValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_13(__valueRangeArg)
    }
    
    // 'visible' attribute on PanelRef (id=WriteOffPanel) at AgencyDistributionWizard_DispositionsScreen.pcf: line 37, column 120
    function visible_39 () : java.lang.Boolean {
      return (wizardState.Remaining.IsNotZero && wizardState.IsPayment) || wizardState.IsCreditDistribution
    }
    
    // 'visible' attribute on AlertBar (id=ExceptionsDescription) at AgencyDistributionWizard_DispositionsScreen.pcf: line 32, column 46
    function visible_4 () : java.lang.Boolean {
      return !exceptionDistItems.IsEmpty
    }
    
    property get exceptionDistItems () : entity.BaseDistItem[] {
      return getVariableValue("exceptionDistItems", 0) as entity.BaseDistItem[]
    }
    
    property set exceptionDistItems ($arg :  entity.BaseDistItem[]) {
      setVariableValue("exceptionDistItems", 0, $arg)
    }
    
    property get producer () : entity.Producer {
      return getVariableValue("producer", 0) as entity.Producer
    }
    
    property set producer ($arg :  entity.Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    property get wizardState () : gw.agencybill.AgencyDistributionWizardHelper {
      return getRequireValue("wizardState", 0) as gw.agencybill.AgencyDistributionWizardHelper
    }
    
    property set wizardState ($arg :  gw.agencybill.AgencyDistributionWizardHelper) {
      setRequireValue("wizardState", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_DispositionsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AgencyDistributionWizard_DispositionsScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 119, column 58
    function actionAvailable_50 () : java.lang.Boolean {
      return invoiceItem.Invoice typeis StatementInvoice
    }
    
    // 'action' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 119, column 58
    function action_49 () : void {
      invoiceItem.StatementInvoiceDetailViewAction()
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 128, column 66
    function action_57 () : void {
      PolicySummary.push(invoiceItem.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=Item_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 133, column 58
    function action_62 () : void {
      InvoiceItemHistoryPopup.push(invoiceItem)
    }
    
    // 'action' attribute on TextCell (id=OwnerAccount_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 158, column 43
    function action_82 () : void {
      AccountSummaryPopup.push(invoiceItem.Owner)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 128, column 66
    function action_dest_58 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(invoiceItem.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=Item_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 133, column 58
    function action_dest_63 () : pcf.api.Destination {
      return pcf.InvoiceItemHistoryPopup.createDestination(invoiceItem)
    }
    
    // 'action' attribute on TextCell (id=OwnerAccount_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 158, column 43
    function action_dest_83 () : pcf.api.Destination {
      return pcf.AccountSummaryPopup.createDestination(invoiceItem.Owner)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Difference_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 143, column 62
    function currency_72 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'value' attribute on RangeCell (id=Disposition_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 152, column 90
    function defaultSetter_76 (__VALUE_TO_SET :  java.lang.Object) : void {
      exceptionDistItem.Disposition = (__VALUE_TO_SET as typekey.DistItemDisposition)
    }
    
    // 'editable' attribute on Row at AgencyDistributionWizard_DispositionsScreen.pcf: line 113, column 114
    function editable_87 () : java.lang.Boolean {
      return !invoiceItem.Frozen && invoiceItem.Type != InvoiceItemType.TC_COMMISSIONREMAINDER
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_DispositionsScreen.pcf: line 111, column 40
    function initialValue_48 () : entity.InvoiceItem {
      return exceptionDistItem.InvoiceItem
    }
    
    // RowIterator at AgencyDistributionWizard_DispositionsScreen.pcf: line 107, column 45
    function initializeVariables_88 () : void {
        invoiceItem = exceptionDistItem.InvoiceItem;

    }
    
    // 'valueRange' attribute on RangeCell (id=Disposition_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 152, column 90
    function valueRange_78 () : java.lang.Object {
      return DistItemDisposition.getTypeKeys( false )
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 119, column 58
    function valueRoot_52 () : java.lang.Object {
      return invoiceItem.Invoice
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 128, column 66
    function valueRoot_60 () : java.lang.Object {
      return invoiceItem.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=Item_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 133, column 58
    function valueRoot_65 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on TextCell (id=Issue_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 137, column 63
    function valueRoot_68 () : java.lang.Object {
      return exceptionDistItem
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 119, column 58
    function value_51 () : java.lang.String {
      return invoiceItem.Invoice.InvoiceNumber
    }
    
    // 'value' attribute on DateCell (id=StatementDate_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 123, column 54
    function value_54 () : java.util.Date {
      return invoiceItem.Invoice.EventDate
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 128, column 66
    function value_59 () : java.lang.String {
      return invoiceItem.PolicyPeriod.PolicyNumberLong
    }
    
    // 'value' attribute on TextCell (id=Item_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 133, column 58
    function value_64 () : java.lang.String {
      return invoiceItem.DisplayNameAsItemType
    }
    
    // 'value' attribute on TextCell (id=Issue_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 137, column 63
    function value_67 () : java.lang.String {
      return exceptionDistItem.ExceptionDescription
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Difference_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 143, column 62
    function value_70 () : gw.pl.currency.MonetaryAmount {
      return exceptionDistItem.NetDifferenceAmount
    }
    
    // 'value' attribute on RangeCell (id=Disposition_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 152, column 90
    function value_75 () : typekey.DistItemDisposition {
      return exceptionDistItem.Disposition
    }
    
    // 'value' attribute on TextCell (id=OwnerAccount_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 158, column 43
    function value_84 () : entity.Account {
      return invoiceItem.Owner
    }
    
    // 'valueRange' attribute on RangeCell (id=Disposition_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 152, column 90
    function verifyValueRangeIsAllowedType_79 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Disposition_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 152, column 90
    function verifyValueRangeIsAllowedType_79 ($$arg :  typekey.DistItemDisposition[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Disposition_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 152, column 90
    function verifyValueRange_80 () : void {
      var __valueRangeArg = DistItemDisposition.getTypeKeys( false )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_79(__valueRangeArg)
    }
    
    // 'valueVisible' attribute on RangeCell (id=Disposition_Cell) at AgencyDistributionWizard_DispositionsScreen.pcf: line 152, column 90
    function visible_74 () : java.lang.Boolean {
      return invoiceItem.Type != InvoiceItemType.TC_COMMISSIONREMAINDER
    }
    
    property get exceptionDistItem () : entity.BaseDistItem {
      return getIteratedValue(1) as entity.BaseDistItem
    }
    
    property get invoiceItem () : entity.InvoiceItem {
      return getVariableValue("invoiceItem", 1) as entity.InvoiceItem
    }
    
    property set invoiceItem ($arg :  entity.InvoiceItem) {
      setVariableValue("invoiceItem", 1, $arg)
    }
    
    
  }
  
  
}