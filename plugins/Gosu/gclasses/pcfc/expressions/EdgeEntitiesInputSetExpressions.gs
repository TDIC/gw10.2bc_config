package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/activityenhancement/EdgeEntitiesInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class EdgeEntitiesInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/activityenhancement/EdgeEntitiesInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class EdgeEntitiesInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=acctPicker) at EdgeEntitiesInputSet.pcf: line 17, column 27
    function action_0 () : void {
      AccountSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=producer) at EdgeEntitiesInputSet.pcf: line 43, column 28
    function action_16 () : void {
      ProducerSearchPopup.push()
    }
    
    // 'action' attribute on TextInput (id=account_Input) at EdgeEntitiesInputSet.pcf: line 17, column 27
    function action_2 () : void {
      AccountSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=ppPicker) at EdgeEntitiesInputSet.pcf: line 30, column 32
    function action_9 () : void {
      PolicySearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=acctPicker) at EdgeEntitiesInputSet.pcf: line 17, column 27
    function action_dest_1 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=ppPicker) at EdgeEntitiesInputSet.pcf: line 30, column 32
    function action_dest_10 () : pcf.api.Destination {
      return pcf.PolicySearchPopup.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=producer) at EdgeEntitiesInputSet.pcf: line 43, column 28
    function action_dest_17 () : pcf.api.Destination {
      return pcf.ProducerSearchPopup.createDestination()
    }
    
    // 'action' attribute on TextInput (id=account_Input) at EdgeEntitiesInputSet.pcf: line 17, column 27
    function action_dest_3 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'value' attribute on TextInput (id=policyPeriod_Input) at EdgeEntitiesInputSet.pcf: line 30, column 32
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      activity.PolicyPeriod = (__VALUE_TO_SET as PolicyPeriod)
    }
    
    // 'value' attribute on TextInput (id=producer_Input) at EdgeEntitiesInputSet.pcf: line 43, column 28
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      activity.Producer_Ext = (__VALUE_TO_SET as Producer)
    }
    
    // 'value' attribute on TextInput (id=account_Input) at EdgeEntitiesInputSet.pcf: line 17, column 27
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      activity.Account = (__VALUE_TO_SET as Account)
    }
    
    // 'inputConversion' attribute on TextInput (id=policyPeriod_Input) at EdgeEntitiesInputSet.pcf: line 30, column 32
    function inputConversion_11 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.api.database.Query.make(PolicyPeriod).compare(PolicyPeriod#PolicyNumber, Equals, VALUE).select().first()
    }
    
    // 'inputConversion' attribute on TextInput (id=producer_Input) at EdgeEntitiesInputSet.pcf: line 43, column 28
    function inputConversion_18 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.api.database.Query.make(Producer).compare(Producer#Name, Equals, VALUE).select().first()
    }
    
    // 'inputConversion' attribute on TextInput (id=account_Input) at EdgeEntitiesInputSet.pcf: line 17, column 27
    function inputConversion_4 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.api.database.Query.make(Account).compare(Account#AccountNumber, Equals, VALUE).select().first()
    }
    
    // 'value' attribute on TextInput (id=account_Input) at EdgeEntitiesInputSet.pcf: line 17, column 27
    function valueRoot_7 () : java.lang.Object {
      return activity
    }
    
    // 'value' attribute on TextInput (id=policyPeriod_Input) at EdgeEntitiesInputSet.pcf: line 30, column 32
    function value_12 () : PolicyPeriod {
      return activity.PolicyPeriod
    }
    
    // 'value' attribute on TextInput (id=producer_Input) at EdgeEntitiesInputSet.pcf: line 43, column 28
    function value_19 () : Producer {
      return activity.Producer_Ext
    }
    
    // 'value' attribute on TextInput (id=account_Input) at EdgeEntitiesInputSet.pcf: line 17, column 27
    function value_5 () : Account {
      return activity.Account
    }
    
    property get activity () : Activity {
      return getRequireValue("activity", 0) as Activity
    }
    
    property set activity ($arg :  Activity) {
      setRequireValue("activity", 0, $arg)
    }
    
    
  }
  
  
}