package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlanDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionPlanDetailPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlanDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionPlanDetailPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (commissionPlan :  CommissionPlan) : int {
      return 0
    }
    
    // 'canEdit' attribute on Popup (id=CommissionPlanDetailPopup) at CommissionPlanDetailPopup.pcf: line 8, column 33
    function canEdit_3 () : java.lang.Boolean {
      return perm.System.commplanedit
    }
    
    // 'def' attribute on PanelRef at CommissionPlanDetailPopup.pcf: line 20, column 61
    function def_onEnter_1 (def :  pcf.CommissionPlanDetailPanelSet) : void {
      def.onEnter(commissionPlan)
    }
    
    // 'def' attribute on PanelRef at CommissionPlanDetailPopup.pcf: line 20, column 61
    function def_refreshVariables_2 (def :  pcf.CommissionPlanDetailPanelSet) : void {
      def.refreshVariables(commissionPlan)
    }
    
    // EditButtons at CommissionPlanDetailPopup.pcf: line 17, column 23
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'title' attribute on Popup (id=CommissionPlanDetailPopup) at CommissionPlanDetailPopup.pcf: line 8, column 33
    static function title_4 (commissionPlan :  CommissionPlan) : java.lang.Object {
      return commissionPlan.Name
    }
    
    override property get CurrentLocation () : pcf.CommissionPlanDetailPopup {
      return super.CurrentLocation as pcf.CommissionPlanDetailPopup
    }
    
    property get commissionPlan () : CommissionPlan {
      return getVariableValue("commissionPlan", 0) as CommissionPlan
    }
    
    property set commissionPlan ($arg :  CommissionPlan) {
      setVariableValue("commissionPlan", 0, $arg)
    }
    
    
  }
  
  
}