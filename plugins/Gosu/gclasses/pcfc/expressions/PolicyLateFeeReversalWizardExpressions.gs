package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyLateFeeReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyLateFeeReversalWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyLateFeeReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyLateFeeReversalWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=PolicyLateFeeReversalWizard) at PolicyLateFeeReversalWizard.pcf: line 13, column 23
    function afterCancel_7 () : void {
      PolicyOverview.go(policyPeriod)
    }
    
    // 'afterCancel' attribute on Wizard (id=PolicyLateFeeReversalWizard) at PolicyLateFeeReversalWizard.pcf: line 13, column 23
    function afterCancel_dest_8 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(policyPeriod)
    }
    
    // 'afterFinish' attribute on Wizard (id=PolicyLateFeeReversalWizard) at PolicyLateFeeReversalWizard.pcf: line 13, column 23
    function afterFinish_12 () : void {
      PolicyOverview.go(policyPeriod)
    }
    
    // 'afterFinish' attribute on Wizard (id=PolicyLateFeeReversalWizard) at PolicyLateFeeReversalWizard.pcf: line 13, column 23
    function afterFinish_dest_13 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(policyPeriod)
    }
    
    // 'allowNext' attribute on WizardStep (id=Step1) at PolicyLateFeeReversalWizard.pcf: line 28, column 95
    function allowNext_1 () : java.lang.Boolean {
      return reversal.Charge != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=PolicyLateFeeReversalWizard) at PolicyLateFeeReversalWizard.pcf: line 13, column 23
    function beforeCommit_9 (pickedValue :  java.lang.Object) : void {
      reversal.reverse()
    }
    
    // 'initialValue' attribute on Variable at PolicyLateFeeReversalWizard.pcf: line 22, column 30
    function initialValue_0 () : ChargeReversal {
      return new ChargeReversal()
    }
    
    // 'onExit' attribute on WizardStep (id=Step2) at PolicyLateFeeReversalWizard.pcf: line 34, column 95
    function onExit_4 () : void {
      reversal.initiateApprovalActivityIfUserLacksAuthority()
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at PolicyLateFeeReversalWizard.pcf: line 28, column 95
    function screen_onEnter_2 (def :  pcf.NewChargeReversalChargeSelectScreen) : void {
      def.onEnter(reversal, policyPeriod.ReversibleLateFeeSearchCriteria)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at PolicyLateFeeReversalWizard.pcf: line 34, column 95
    function screen_onEnter_5 (def :  pcf.NewChargeReversalConfirmationScreen) : void {
      def.onEnter(reversal)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at PolicyLateFeeReversalWizard.pcf: line 28, column 95
    function screen_refreshVariables_3 (def :  pcf.NewChargeReversalChargeSelectScreen) : void {
      def.refreshVariables(reversal, policyPeriod.ReversibleLateFeeSearchCriteria)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at PolicyLateFeeReversalWizard.pcf: line 34, column 95
    function screen_refreshVariables_6 (def :  pcf.NewChargeReversalConfirmationScreen) : void {
      def.refreshVariables(reversal)
    }
    
    // 'tabBar' attribute on Wizard (id=PolicyLateFeeReversalWizard) at PolicyLateFeeReversalWizard.pcf: line 13, column 23
    function tabBar_onEnter_10 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=PolicyLateFeeReversalWizard) at PolicyLateFeeReversalWizard.pcf: line 13, column 23
    function tabBar_refreshVariables_11 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.PolicyLateFeeReversalWizard {
      return super.CurrentLocation as pcf.PolicyLateFeeReversalWizard
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get reversal () : ChargeReversal {
      return getVariableValue("reversal", 0) as ChargeReversal
    }
    
    property set reversal ($arg :  ChargeReversal) {
      setVariableValue("reversal", 0, $arg)
    }
    
    
  }
  
  
}