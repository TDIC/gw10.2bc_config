package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountInfoBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountInfoBarExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountInfoBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountInfoBarExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on InfoBarElement (id=AccountName) at AccountInfoBar.pcf: line 31, column 36
    function action_4 () : void {
      AccountOverview.go(account)
    }
    
    // 'action' attribute on InfoBarElement (id=AccountNumber) at AccountInfoBar.pcf: line 36, column 38
    function action_7 () : void {
      AccountOverview.go(account)
    }
    
    // 'action' attribute on InfoBarElement (id=AccountName) at AccountInfoBar.pcf: line 31, column 36
    function action_dest_5 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(account)
    }
    
    // 'action' attribute on InfoBarElement (id=AccountNumber) at AccountInfoBar.pcf: line 36, column 38
    function action_dest_8 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(account)
    }
    
    // 'icon' attribute on InfoBarElement (id=ListBillFlag) at AccountInfoBar.pcf: line 26, column 50
    function icon_3 () : java.lang.String {
      return account.AccountType == TC_LISTBILL ? "list_account" : "account"
    }
    
    // 'tooltip' attribute on InfoBarElement (id=ListBillFlag) at AccountInfoBar.pcf: line 26, column 50
    function tooltip_2 () : java.lang.String {
      return account.AccountType.DisplayName
    }
    
    // 'value' attribute on InfoBarElement (id=CurrentDate) at AccountInfoBar.pcf: line 39, column 51
    function value_10 () : java.lang.Object {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'value' attribute on InfoBarElement (id=AccountName) at AccountInfoBar.pcf: line 31, column 36
    function value_6 () : java.lang.Object {
      return account.AccountName
    }
    
    // 'value' attribute on InfoBarElement (id=AccountNumber) at AccountInfoBar.pcf: line 36, column 38
    function value_9 () : java.lang.Object {
      return account.AccountNumber
    }
    
    // 'visible' attribute on InfoBarElement (id=DelinquencyFlag) at AccountInfoBar.pcf: line 15, column 56
    function visible_0 () : java.lang.Boolean {
      return account.hasActiveDelinquencyProcess()
    }
    
    // 'visible' attribute on InfoBarElement (id=TroubleTicketsFlag) at AccountInfoBar.pcf: line 21, column 50
    function visible_1 () : java.lang.Boolean {
      return account.HasActiveTroubleTickets
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    
  }
  
  
}