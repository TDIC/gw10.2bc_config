package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/commission/IncentivesLV.PremiumIncentive.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class IncentivesLV_PremiumIncentiveExpressions {
  @javax.annotation.Generated("config/web/pcf/commission/IncentivesLV.PremiumIncentive.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IncentivesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on TypeKeyCell (id=Currency_Cell) at IncentivesLV.PremiumIncentive.pcf: line 28, column 41
    function sortValue_0 (incentive :  PremiumIncentive) : java.lang.Object {
      return incentive.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Threshold_Cell) at IncentivesLV.PremiumIncentive.pcf: line 36, column 40
    function sortValue_1 (incentive :  PremiumIncentive) : java.lang.Object {
      return incentive.Threshold
    }
    
    // 'value' attribute on TextCell (id=BonusPercentage_Cell) at IncentivesLV.PremiumIncentive.pcf: line 44, column 45
    function sortValue_2 (incentive :  PremiumIncentive) : java.lang.Object {
      return incentive.BonusPercentage
    }
    
    // 'toRemove' attribute on RowIterator (id=Incentives) at IncentivesLV.PremiumIncentive.pcf: line 20, column 38
    function toRemove_15 (incentive :  PremiumIncentive) : void {
      commissionSubPlan.removeFromIncentives(incentive)
    }
    
    // 'value' attribute on RowIterator (id=Incentives) at IncentivesLV.PremiumIncentive.pcf: line 20, column 38
    function value_16 () : PremiumIncentive[] {
      return commissionSubPlan.PremiumIncentives
    }
    
    property get commissionSubPlan () : CommissionSubPlan {
      return getRequireValue("commissionSubPlan", 0) as CommissionSubPlan
    }
    
    property set commissionSubPlan ($arg :  CommissionSubPlan) {
      setRequireValue("commissionSubPlan", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/commission/IncentivesLV.PremiumIncentive.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends IncentivesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=BonusPercentage_Cell) at IncentivesLV.PremiumIncentive.pcf: line 44, column 45
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      incentive.BonusPercentage = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Threshold_Cell) at IncentivesLV.PremiumIncentive.pcf: line 36, column 40
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      incentive.Threshold = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TypeKeyCell (id=Currency_Cell) at IncentivesLV.PremiumIncentive.pcf: line 28, column 41
    function valueRoot_4 () : java.lang.Object {
      return incentive
    }
    
    // 'value' attribute on TextCell (id=BonusPercentage_Cell) at IncentivesLV.PremiumIncentive.pcf: line 44, column 45
    function value_11 () : java.math.BigDecimal {
      return incentive.BonusPercentage
    }
    
    // 'value' attribute on TypeKeyCell (id=Currency_Cell) at IncentivesLV.PremiumIncentive.pcf: line 28, column 41
    function value_3 () : typekey.Currency {
      return incentive.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Threshold_Cell) at IncentivesLV.PremiumIncentive.pcf: line 36, column 40
    function value_6 () : gw.pl.currency.MonetaryAmount {
      return incentive.Threshold
    }
    
    property get incentive () : PremiumIncentive {
      return getIteratedValue(1) as PremiumIncentive
    }
    
    
  }
  
  
}