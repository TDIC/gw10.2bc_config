package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewChargeReversalConfirmationScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewChargeReversalConfirmationScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalConfirmationScreen.pcf: line 24, column 56
    function def_onEnter_2 (def :  pcf.NewChargeReversalConfirmationDV) : void {
      def.onEnter(reversal)
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalConfirmationScreen.pcf: line 26, column 51
    function def_onEnter_4 (def :  pcf.ApprovalActivityDV) : void {
      def.onEnter(approvalActivity)
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalConfirmationScreen.pcf: line 24, column 56
    function def_refreshVariables_3 (def :  pcf.NewChargeReversalConfirmationDV) : void {
      def.refreshVariables(reversal)
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalConfirmationScreen.pcf: line 26, column 51
    function def_refreshVariables_5 (def :  pcf.ApprovalActivityDV) : void {
      def.refreshVariables(approvalActivity)
    }
    
    // 'initialValue' attribute on Variable at NewChargeReversalConfirmationScreen.pcf: line 15, column 43
    function initialValue_0 () : entity.ChrgRvslApprActivity {
      return reversal.OpenApprovalActivity
    }
    
    // 'visible' attribute on AlertBar (id=ApprovalActivityAlertBar) at NewChargeReversalConfirmationScreen.pcf: line 22, column 43
    function visible_1 () : java.lang.Boolean {
      return approvalActivity != null
    }
    
    property get approvalActivity () : entity.ChrgRvslApprActivity {
      return getVariableValue("approvalActivity", 0) as entity.ChrgRvslApprActivity
    }
    
    property set approvalActivity ($arg :  entity.ChrgRvslApprActivity) {
      setVariableValue("approvalActivity", 0, $arg)
    }
    
    property get reversal () : ChargeReversal {
      return getRequireValue("reversal", 0) as ChargeReversal
    }
    
    property set reversal ($arg :  ChargeReversal) {
      setRequireValue("reversal", 0, $arg)
    }
    
    
  }
  
  
}