package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerTransactions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerTransactionsExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerTransactions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends ProducerTransactionsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at ProducerTransactions.pcf: line 208, column 35
    function action_71 () : void {
      TransactionDetailPopup.push(producerTransaction)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at ProducerTransactions.pcf: line 217, column 35
    function action_76 () : void {
      PolicySummary.push(producerTransaction.PolicyCommission.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at ProducerTransactions.pcf: line 208, column 35
    function action_dest_72 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(producerTransaction)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at ProducerTransactions.pcf: line 217, column 35
    function action_dest_77 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(producerTransaction.PolicyCommission.PolicyPeriod)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Basis_Cell) at ProducerTransactions.pcf: line 242, column 56
    function currency_92 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'outputConversion' attribute on TextCell (id=Rate_Cell) at ProducerTransactions.pcf: line 257, column 51
    function outputConversion_98 (VALUE :  java.lang.Number) : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(VALUE, 2)
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at ProducerTransactions.pcf: line 200, column 66
    function valueRoot_69 () : java.lang.Object {
      return producerTransaction
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ProducerTransactions.pcf: line 217, column 35
    function valueRoot_79 () : java.lang.Object {
      return producerTransaction.PolicyCommission
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at ProducerTransactions.pcf: line 222, column 97
    function valueRoot_82 () : java.lang.Object {
      return producerTransaction.PolicyCommission.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at ProducerTransactions.pcf: line 229, column 35
    function valueRoot_85 () : java.lang.Object {
      return producerTransaction.ProducerCode
    }
    
    // 'value' attribute on TextCell (id=Charge_Cell) at ProducerTransactions.pcf: line 235, column 48
    function valueRoot_88 () : java.lang.Object {
      return producerTransaction.ChargeCommission
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at ProducerTransactions.pcf: line 200, column 66
    function value_68 () : java.util.Date {
      return producerTransaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at ProducerTransactions.pcf: line 208, column 35
    function value_73 () : java.lang.String {
      return producerTransaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ProducerTransactions.pcf: line 217, column 35
    function value_78 () : entity.PolicyPeriod {
      return producerTransaction.PolicyCommission.PolicyPeriod
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at ProducerTransactions.pcf: line 222, column 97
    function value_81 () : java.util.Date {
      return producerTransaction.PolicyCommission.PolicyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at ProducerTransactions.pcf: line 229, column 35
    function value_84 () : java.lang.String {
      return producerTransaction.ProducerCode.Code
    }
    
    // 'value' attribute on TextCell (id=Charge_Cell) at ProducerTransactions.pcf: line 235, column 48
    function value_87 () : entity.Charge {
      return producerTransaction.ChargeCommission.Charge
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Basis_Cell) at ProducerTransactions.pcf: line 242, column 56
    function value_90 () : gw.pl.currency.MonetaryAmount {
      return producerTransaction.Basis
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Earned_Cell) at ProducerTransactions.pcf: line 250, column 64
    function value_94 () : gw.pl.currency.MonetaryAmount {
      return producerTransaction.PayableAmount
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at ProducerTransactions.pcf: line 257, column 51
    function value_99 () : java.lang.Number {
      return producerTransaction.Rate
    }
    
    property get producerTransaction () : entity.CommissionEarned {
      return getIteratedValue(1) as entity.CommissionEarned
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerTransactions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends ProducerTransactionsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=ReversePaymentOfModification) at ProducerTransactions.pcf: line 298, column 149
    function action_114 () : void {
      ProducerPaymentReversalConfirmationPopup.push(producerTransaction as ProducerPaymentRecd)
    }
    
    // 'action' attribute on Link (id=ReverseOutgoingProducerPayment) at ProducerTransactions.pcf: line 304, column 149
    function action_117 () : void {
      OutgoingProducerPaymentReversalConfirmationPopup.push(producerTransaction as ProducerPaymentSent)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at ProducerTransactions.pcf: line 319, column 35
    function action_122 () : void {
      TransactionDetailPopup.push(producerTransaction)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at ProducerTransactions.pcf: line 328, column 35
    function action_127 () : void {
      PolicySummary.push(producerTransaction.PolicyCommission.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=CheckRefNumber_Cell) at ProducerTransactions.pcf: line 343, column 35
    function action_135 () : void {
      PaymentDetailForward.push(producerTransaction.PaymentReceipt)
    }
    
    // 'action' attribute on Link (id=ReversePaymentOfModification) at ProducerTransactions.pcf: line 298, column 149
    function action_dest_115 () : pcf.api.Destination {
      return pcf.ProducerPaymentReversalConfirmationPopup.createDestination(producerTransaction as ProducerPaymentRecd)
    }
    
    // 'action' attribute on Link (id=ReverseOutgoingProducerPayment) at ProducerTransactions.pcf: line 304, column 149
    function action_dest_118 () : pcf.api.Destination {
      return pcf.OutgoingProducerPaymentReversalConfirmationPopup.createDestination(producerTransaction as ProducerPaymentSent)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at ProducerTransactions.pcf: line 319, column 35
    function action_dest_123 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(producerTransaction)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at ProducerTransactions.pcf: line 328, column 35
    function action_dest_128 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(producerTransaction.PolicyCommission.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=CheckRefNumber_Cell) at ProducerTransactions.pcf: line 343, column 35
    function action_dest_136 () : pcf.api.Destination {
      return pcf.PaymentDetailForward.createDestination(producerTransaction.PaymentReceipt)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=ToAgent_Cell) at ProducerTransactions.pcf: line 351, column 67
    function currency_141 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at ProducerTransactions.pcf: line 311, column 66
    function valueRoot_120 () : java.lang.Object {
      return producerTransaction
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ProducerTransactions.pcf: line 328, column 35
    function valueRoot_130 () : java.lang.Object {
      return producerTransaction.PolicyCommission
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at ProducerTransactions.pcf: line 311, column 66
    function value_119 () : java.util.Date {
      return producerTransaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at ProducerTransactions.pcf: line 319, column 35
    function value_124 () : java.lang.String {
      return producerTransaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ProducerTransactions.pcf: line 328, column 35
    function value_129 () : entity.PolicyPeriod {
      return producerTransaction.PolicyCommission.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=Context_Cell) at ProducerTransactions.pcf: line 335, column 35
    function value_132 () : java.lang.String {
      return producerTransaction.ShortDisplayName
    }
    
    // 'value' attribute on TextCell (id=CheckRefNumber_Cell) at ProducerTransactions.pcf: line 343, column 35
    function value_137 () : java.lang.String {
      return getCheckRefNumber(producerTransaction.PaymentReceipt)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ToAgent_Cell) at ProducerTransactions.pcf: line 351, column 67
    function value_139 () : gw.pl.currency.MonetaryAmount {
      return producerTransaction.ToProducerAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=FromAgent_Cell) at ProducerTransactions.pcf: line 359, column 69
    function value_143 () : gw.pl.currency.MonetaryAmount {
      return producerTransaction.FromProducerAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Payment_Cell) at ProducerTransactions.pcf: line 367, column 61
    function value_147 () : gw.pl.currency.MonetaryAmount {
      return producerTransaction.PaidAmount
    }
    
    // 'visible' attribute on Link (id=ReversePaymentOfModification) at ProducerTransactions.pcf: line 298, column 149
    function visible_113 () : java.lang.Boolean {
      return !producerTransaction.Reversed and !producerTransaction.Reversal and producerTransaction typeis ProducerPaymentRecd
    }
    
    // 'visible' attribute on Link (id=ReverseOutgoingProducerPayment) at ProducerTransactions.pcf: line 304, column 149
    function visible_116 () : java.lang.Boolean {
      return !producerTransaction.Reversed and !producerTransaction.Reversal and producerTransaction typeis ProducerPaymentSent
    }
    
    property get producerTransaction () : entity.ProducerPaymentTxn {
      return getIteratedValue(1) as entity.ProducerPaymentTxn
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerTransactions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends ProducerTransactionsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at ProducerTransactions.pcf: line 413, column 35
    function action_167 () : void {
      TransactionDetailPopup.push(producerTransaction)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at ProducerTransactions.pcf: line 422, column 35
    function action_172 () : void {
      PolicySummary.push(producerTransaction.PolicyCommission.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=CheckRefNumber_Cell) at ProducerTransactions.pcf: line 455, column 35
    function action_189 () : void {
      PaymentDetailForward.push(producerTransaction.PaymentReceipt)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at ProducerTransactions.pcf: line 413, column 35
    function action_dest_168 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(producerTransaction)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at ProducerTransactions.pcf: line 422, column 35
    function action_dest_173 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(producerTransaction.PolicyCommission.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=CheckRefNumber_Cell) at ProducerTransactions.pcf: line 455, column 35
    function action_dest_190 () : pcf.api.Destination {
      return pcf.PaymentDetailForward.createDestination(producerTransaction.PaymentReceipt)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Reserve_Cell) at ProducerTransactions.pcf: line 463, column 65
    function currency_195 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at ProducerTransactions.pcf: line 405, column 66
    function valueRoot_165 () : java.lang.Object {
      return producerTransaction
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ProducerTransactions.pcf: line 422, column 35
    function valueRoot_175 () : java.lang.Object {
      return producerTransaction.PolicyCommission
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at ProducerTransactions.pcf: line 427, column 97
    function valueRoot_178 () : java.lang.Object {
      return producerTransaction.PolicyCommission.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=Charge_Cell) at ProducerTransactions.pcf: line 440, column 48
    function valueRoot_184 () : java.lang.Object {
      return producerTransaction.ChargeCommission
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at ProducerTransactions.pcf: line 447, column 35
    function valueRoot_187 () : java.lang.Object {
      return producerTransaction.ProducerCode
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at ProducerTransactions.pcf: line 405, column 66
    function value_164 () : java.util.Date {
      return producerTransaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at ProducerTransactions.pcf: line 413, column 35
    function value_169 () : java.lang.String {
      return producerTransaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ProducerTransactions.pcf: line 422, column 35
    function value_174 () : entity.PolicyPeriod {
      return producerTransaction.PolicyCommission.PolicyPeriod
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at ProducerTransactions.pcf: line 427, column 97
    function value_177 () : java.util.Date {
      return producerTransaction.PolicyCommission.PolicyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on TextCell (id=Context_Cell) at ProducerTransactions.pcf: line 434, column 35
    function value_180 () : java.lang.String {
      return producerTransaction.ShortDisplayName
    }
    
    // 'value' attribute on TextCell (id=Charge_Cell) at ProducerTransactions.pcf: line 440, column 48
    function value_183 () : entity.Charge {
      return producerTransaction.ChargeCommission.Charge
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at ProducerTransactions.pcf: line 447, column 35
    function value_186 () : java.lang.String {
      return producerTransaction.ProducerCode.Code
    }
    
    // 'value' attribute on TextCell (id=CheckRefNumber_Cell) at ProducerTransactions.pcf: line 455, column 35
    function value_191 () : java.lang.String {
      return getCheckRefNumber(producerTransaction.PaymentReceipt)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Reserve_Cell) at ProducerTransactions.pcf: line 463, column 65
    function value_193 () : gw.pl.currency.MonetaryAmount {
      return producerTransaction.ReservedAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Earned_Cell) at ProducerTransactions.pcf: line 471, column 64
    function value_197 () : gw.pl.currency.MonetaryAmount {
      return producerTransaction.PayableAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Payment_Cell) at ProducerTransactions.pcf: line 488, column 61
    function value_205 () : gw.pl.currency.MonetaryAmount {
      return producerTransaction.PaidAmount
    }
    
    property get producerTransaction () : entity.ProducerTransaction {
      return getIteratedValue(1) as entity.ProducerTransaction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerTransactions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ProducerTransactionsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at ProducerTransactions.pcf: line 97, column 68
    function action_27 () : void {
      TransactionDetailPopup.push(producerTransaction)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at ProducerTransactions.pcf: line 106, column 35
    function action_32 () : void {
      PolicyOverview.push(producerTransaction.PolicyCommission.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at ProducerTransactions.pcf: line 97, column 68
    function action_dest_28 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(producerTransaction)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at ProducerTransactions.pcf: line 106, column 35
    function action_dest_33 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(producerTransaction.PolicyCommission.PolicyPeriod)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Basis_Cell) at ProducerTransactions.pcf: line 138, column 56
    function currency_51 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'outputConversion' attribute on TextCell (id=Rate_Cell) at ProducerTransactions.pcf: line 153, column 50
    function outputConversion_57 (VALUE :  java.lang.Number) : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(VALUE, 2)
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at ProducerTransactions.pcf: line 90, column 66
    function valueRoot_25 () : java.lang.Object {
      return producerTransaction
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ProducerTransactions.pcf: line 106, column 35
    function valueRoot_35 () : java.lang.Object {
      return producerTransaction.PolicyCommission
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at ProducerTransactions.pcf: line 111, column 97
    function valueRoot_38 () : java.lang.Object {
      return producerTransaction.PolicyCommission.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=Charge_Cell) at ProducerTransactions.pcf: line 124, column 48
    function valueRoot_44 () : java.lang.Object {
      return producerTransaction.ChargeCommission
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at ProducerTransactions.pcf: line 130, column 35
    function valueRoot_47 () : java.lang.Object {
      return producerTransaction.ProducerCode
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at ProducerTransactions.pcf: line 90, column 66
    function value_24 () : java.util.Date {
      return producerTransaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at ProducerTransactions.pcf: line 97, column 68
    function value_29 () : java.lang.String {
      return producerTransaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ProducerTransactions.pcf: line 106, column 35
    function value_34 () : entity.PolicyPeriod {
      return producerTransaction.PolicyCommission.PolicyPeriod
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at ProducerTransactions.pcf: line 111, column 97
    function value_37 () : java.util.Date {
      return producerTransaction.PolicyCommission.PolicyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on TextCell (id=Context_Cell) at ProducerTransactions.pcf: line 118, column 35
    function value_40 () : java.lang.String {
      return producerTransaction.ShortDisplayName
    }
    
    // 'value' attribute on TextCell (id=Charge_Cell) at ProducerTransactions.pcf: line 124, column 48
    function value_43 () : entity.Charge {
      return producerTransaction.ChargeCommission.Charge
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at ProducerTransactions.pcf: line 130, column 35
    function value_46 () : java.lang.String {
      return producerTransaction.ProducerCode.Code
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Basis_Cell) at ProducerTransactions.pcf: line 138, column 56
    function value_49 () : gw.pl.currency.MonetaryAmount {
      return producerTransaction.Basis
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Reserve_Cell) at ProducerTransactions.pcf: line 146, column 65
    function value_53 () : gw.pl.currency.MonetaryAmount {
      return producerTransaction.ReservedAmount
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at ProducerTransactions.pcf: line 153, column 50
    function value_58 () : java.lang.Number {
      return producerTransaction.Rate
    }
    
    property get producerTransaction () : entity.CommissionsReserveTxn {
      return getIteratedValue(1) as entity.CommissionsReserveTxn
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerTransactions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerTransactionsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'action' attribute on AlertBar (id=ProducerTransactions_TroubleTicketAlertAlertBar) at ProducerTransactions.pcf: line 56, column 53
    function action_10 () : void {
      TroubleTicketAlertForward.push(producer)
    }
    
    // 'action' attribute on ToolbarButton (id=PolicyFilterGoButton) at ProducerTransactions.pcf: line 45, column 120
    function action_6 () : void {
      
    }
    
    // 'action' attribute on ToolbarButton (id=PolicyFilterClearButton) at ProducerTransactions.pcf: line 49, column 123
    function action_7 () : void {
      policyStartsWith = null
    }
    
    // 'action' attribute on AlertBar (id=ProducerTransactions_TroubleTicketAlertAlertBar) at ProducerTransactions.pcf: line 56, column 53
    function action_dest_11 () : pcf.api.Destination {
      return pcf.TroubleTicketAlertForward.createDestination(producer)
    }
    
    // 'available' attribute on AlertBar (id=ProducerTransactions_TroubleTicketAlertAlertBar) at ProducerTransactions.pcf: line 56, column 53
    function available_8 () : java.lang.Boolean {
      return perm.System.prodttktview
    }
    
    // 'canVisit' attribute on Page (id=ProducerTransactions) at ProducerTransactions.pcf: line 9, column 72
    static function canVisit_211 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.proddbtxnview
    }
    
    // 'value' attribute on TextValue (id=PolicyPeriodFilter) at ProducerTransactions.pcf: line 40, column 41
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyStartsWith = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at ProducerTransactions.pcf: line 76, column 105
    function filters_13 () : gw.api.filters.IFilter[] {
      return new gw.api.web.producer.ProducerCommissionsShowFilterSet().FilterOptions
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at ProducerTransactions.pcf: line 82, column 68
    function filters_14 () : gw.api.filters.IFilter[] {
      return producerCodeFilterSet.FilterOptions
    }
    
    // 'initialValue' attribute on Variable at ProducerTransactions.pcf: line 18, column 79
    function initialValue_0 () : gw.api.database.IQueryBeanResult<entity.PolicyCommission> {
      return producer.PolicyCommissions
    }
    
    // 'initialValue' attribute on Variable at ProducerTransactions.pcf: line 22, column 22
    function initialValue_1 () : String {
      return null
    }
    
    // 'initialValue' attribute on Variable at ProducerTransactions.pcf: line 26, column 76
    function initialValue_2 () : gw.api.web.producer.ProducerCommissionsProducerCodeFilterSet {
      return new gw.api.web.producer.ProducerCommissionsProducerCodeFilterSet(producer)
    }
    
    // 'label' attribute on AlertBar (id=ProducerTransactions_TroubleTicketAlertAlertBar) at ProducerTransactions.pcf: line 56, column 53
    function label_12 () : java.lang.Object {
      return producer.AlertBarDisplayText
    }
    
    // 'onSelect' attribute on Card (id=EarningsCard) at ProducerTransactions.pcf: line 171, column 87
    function onSelect_103 () : void {
      currentTab = "Earnings"
    }
    
    // 'onSelect' attribute on Card (id=PaymentsCard) at ProducerTransactions.pcf: line 266, column 87
    function onSelect_152 () : void {
      currentTab = "Payments"
    }
    
    // 'onSelect' attribute on Card (id=CombinedCard) at ProducerTransactions.pcf: line 376, column 87
    function onSelect_210 () : void {
      currentTab = "Combined"
    }
    
    // 'onSelect' attribute on Card (id=ReservesCard) at ProducerTransactions.pcf: line 61, column 87
    function onSelect_62 () : void {
      currentTab = "Reserves"
    }
    
    // 'outputConversion' attribute on TextCell (id=RateFooter_Cell) at ProducerTransactions.pcf: line 160, column 55
    function outputConversion_20 (VALUE :  java.lang.Number) : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(VALUE, 2)
    }
    
    // Page (id=ProducerTransactions) at ProducerTransactions.pcf: line 9, column 72
    static function parent_212 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at ProducerTransactions.pcf: line 311, column 66
    function sortValue_106 (producerTransaction :  entity.ProducerPaymentTxn) : java.lang.Object {
      return producerTransaction.TransactionDate
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at ProducerTransactions.pcf: line 90, column 66
    function sortValue_15 (producerTransaction :  entity.CommissionsReserveTxn) : java.lang.Object {
      return producerTransaction.TransactionDate
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at ProducerTransactions.pcf: line 405, column 66
    function sortValue_155 (producerTransaction :  entity.ProducerTransaction) : java.lang.Object {
      return producerTransaction.TransactionDate
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at ProducerTransactions.pcf: line 200, column 66
    function sortValue_65 (producerTransaction :  entity.CommissionEarned) : java.lang.Object {
      return producerTransaction.TransactionDate
    }
    
    // '$$sumValue' attribute on RowIterator at ProducerTransactions.pcf: line 351, column 67
    function sumValueRoot_108 (producerTransaction :  entity.ProducerPaymentTxn) : java.lang.Object {
      return producerTransaction
    }
    
    // '$$sumValue' attribute on RowIterator at ProducerTransactions.pcf: line 463, column 65
    function sumValueRoot_157 (producerTransaction :  entity.ProducerTransaction) : java.lang.Object {
      return producerTransaction
    }
    
    // '$$sumValue' attribute on RowIterator at ProducerTransactions.pcf: line 138, column 56
    function sumValueRoot_17 (producerTransaction :  entity.CommissionsReserveTxn) : java.lang.Object {
      return producerTransaction
    }
    
    // '$$sumValue' attribute on RowIterator at ProducerTransactions.pcf: line 250, column 64
    function sumValueRoot_67 (producerTransaction :  entity.CommissionEarned) : java.lang.Object {
      return producerTransaction
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerTransactions.pcf: line 351, column 67
    function sumValue_107 (producerTransaction :  entity.ProducerPaymentTxn) : java.lang.Object {
      return producerTransaction.ToProducerAmount
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerTransactions.pcf: line 359, column 69
    function sumValue_109 (producerTransaction :  entity.ProducerPaymentTxn) : java.lang.Object {
      return producerTransaction.FromProducerAmount
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerTransactions.pcf: line 367, column 61
    function sumValue_111 (producerTransaction :  entity.ProducerPaymentTxn) : java.lang.Object {
      return producerTransaction.PaidAmount
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerTransactions.pcf: line 463, column 65
    function sumValue_156 (producerTransaction :  entity.ProducerTransaction) : java.lang.Object {
      return producerTransaction.ReservedAmount
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerTransactions.pcf: line 471, column 64
    function sumValue_158 (producerTransaction :  entity.ProducerTransaction) : java.lang.Object {
      return producerTransaction.PayableAmount
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerTransactions.pcf: line 138, column 56
    function sumValue_16 (producerTransaction :  entity.CommissionsReserveTxn) : java.lang.Object {
      return producerTransaction.Basis
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerTransactions.pcf: line 488, column 61
    function sumValue_162 (producerTransaction :  entity.ProducerTransaction) : java.lang.Object {
      return producerTransaction.PaidAmount
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerTransactions.pcf: line 146, column 65
    function sumValue_18 (producerTransaction :  entity.CommissionsReserveTxn) : java.lang.Object {
      return producerTransaction.ReservedAmount
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerTransactions.pcf: line 250, column 64
    function sumValue_66 (producerTransaction :  entity.CommissionEarned) : java.lang.Object {
      return producerTransaction.PayableAmount
    }
    
    // 'value' attribute on TextCell (id=RateFooter_Cell) at ProducerTransactions.pcf: line 160, column 55
    function valueRoot_22 () : java.lang.Object {
      return producer.findCommissionsReserveTxns(policyStartsWith)
    }
    
    // 'value' attribute on RowIterator at ProducerTransactions.pcf: line 181, column 93
    function value_102 () : gw.api.database.IQueryBeanResult<entity.CommissionEarned> {
      return gw.api.util.BCUtils.conditionalQueryResult(currentTab == "Earnings", producer.findAllCommissionEarnedTransactions(policyStartsWith))
    }
    
    // 'value' attribute on RowIterator at ProducerTransactions.pcf: line 276, column 95
    function value_151 () : gw.api.database.IQueryBeanResult<entity.ProducerPaymentTxn> {
      return gw.api.util.BCUtils.conditionalQueryResult(currentTab == "Payments", producer.findProducerPaymentTransactions(policyStartsWith))
    }
    
    // 'value' attribute on RowIterator at ProducerTransactions.pcf: line 386, column 96
    function value_209 () : gw.api.database.IQueryBeanResult<entity.ProducerTransaction> {
      return gw.api.util.BCUtils.conditionalQueryResult(currentTab == "Combined", producer.findCommissionTransactionsExcludingPayable(policyStartsWith))
    }
    
    // 'value' attribute on TextCell (id=RateFooter_Cell) at ProducerTransactions.pcf: line 160, column 55
    function value_21 () : java.lang.Number {
      return producer.findCommissionsReserveTxns(policyStartsWith).AggregateRate
    }
    
    // 'value' attribute on TextValue (id=PolicyPeriodFilter) at ProducerTransactions.pcf: line 40, column 41
    function value_3 () : java.lang.String {
      return policyStartsWith
    }
    
    // 'value' attribute on RowIterator at ProducerTransactions.pcf: line 71, column 98
    function value_61 () : gw.api.database.IQueryBeanResult<entity.CommissionsReserveTxn> {
      return gw.api.util.BCUtils.conditionalQueryResult(currentTab == "Reserves", producer.findCommissionsReserveTxns(policyStartsWith).Query)
    }
    
    // 'visible' attribute on AlertBar (id=ProducerTransactions_TroubleTicketAlertAlertBar) at ProducerTransactions.pcf: line 56, column 53
    function visible_9 () : java.lang.Boolean {
      return producer.HasActiveTroubleTickets
    }
    
    override property get CurrentLocation () : pcf.ProducerTransactions {
      return super.CurrentLocation as pcf.ProducerTransactions
    }
    
    property get currentTab () : String {
      return getVariableValue("currentTab", 0) as String
    }
    
    property set currentTab ($arg :  String) {
      setVariableValue("currentTab", 0, $arg)
    }
    
    property get policyCommissions () : gw.api.database.IQueryBeanResult<entity.PolicyCommission> {
      return getVariableValue("policyCommissions", 0) as gw.api.database.IQueryBeanResult<entity.PolicyCommission>
    }
    
    property set policyCommissions ($arg :  gw.api.database.IQueryBeanResult<entity.PolicyCommission>) {
      setVariableValue("policyCommissions", 0, $arg)
    }
    
    property get policyStartsWith () : String {
      return getVariableValue("policyStartsWith", 0) as String
    }
    
    property set policyStartsWith ($arg :  String) {
      setVariableValue("policyStartsWith", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    property get producerCodeFilterSet () : gw.api.web.producer.ProducerCommissionsProducerCodeFilterSet {
      return getVariableValue("producerCodeFilterSet", 0) as gw.api.web.producer.ProducerCommissionsProducerCodeFilterSet
    }
    
    property set producerCodeFilterSet ($arg :  gw.api.web.producer.ProducerCommissionsProducerCodeFilterSet) {
      setVariableValue("producerCodeFilterSet", 0, $arg)
    }
    
    function getCheckRefNumber(paymentReceipt : PaymentReceipt) : String {
            if (paymentReceipt == null) {
              return null;
            } else if (paymentReceipt.RefNumber == null) {
              return DisplayKey.get("Web.ProducerTransactions.Payments.Pending");
            } else {
              return paymentReceipt.RefNumber;
            }
          }
    
    
  }
  
  
}