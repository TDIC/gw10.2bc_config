package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.web.policy.PolicyInfoBarHelper
@javax.annotation.Generated("config/web/pcf/policy/PolicyInfoBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyInfoBarExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyInfoBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyInfoBarExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on InfoBarElement (id=AccountNumber) at PolicyInfoBar.pcf: line 46, column 51
    function action_12 () : void {
      AccountSummary.push(policyPeriod.Account)
    }
    
    // 'action' attribute on InfoBarElement (id=ProducerName) at PolicyInfoBar.pcf: line 53, column 59
    function action_16 () : void {
      ProducerDetail.go(policyPeriod.PrimaryProducerCode.Producer)
    }
    
    // 'action' attribute on InfoBarElement (id=PolicyNumber) at PolicyInfoBar.pcf: line 41, column 41
    function action_9 () : void {
      PolicyOverview.go(policyPeriod)
    }
    
    // 'action' attribute on InfoBarElement (id=PolicyNumber) at PolicyInfoBar.pcf: line 41, column 41
    function action_dest_10 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(policyPeriod)
    }
    
    // 'action' attribute on InfoBarElement (id=AccountNumber) at PolicyInfoBar.pcf: line 46, column 51
    function action_dest_13 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination(policyPeriod.Account)
    }
    
    // 'action' attribute on InfoBarElement (id=ProducerName) at PolicyInfoBar.pcf: line 53, column 59
    function action_dest_17 () : pcf.api.Destination {
      return pcf.ProducerDetail.createDestination(policyPeriod.PrimaryProducerCode.Producer)
    }
    
    // 'icon' attribute on InfoBarElement (id=InForceFlag) at PolicyInfoBar.pcf: line 14, column 47
    function icon_1 () : java.lang.String {
      return InfoBarHelper.InForceIcon
    }
    
    // 'icon' attribute on InfoBarElement (id=DelinquencyFlag) at PolicyInfoBar.pcf: line 20, column 61
    function icon_3 () : java.lang.String {
      return "delinquent" 
    }
    
    // 'icon' attribute on InfoBarElement (id=ProductType) at PolicyInfoBar.pcf: line 31, column 219
    function icon_6 () : java.lang.String {
      return InfoBarHelper.LineOfBusinessIcon
    }
    
    // 'icon' attribute on InfoBarElement (id=BillingType) at PolicyInfoBar.pcf: line 36, column 57
    function icon_8 () : java.lang.String {
      return InfoBarHelper.PolicyBillingMethodIcon
    }
    
    // 'tooltip' attribute on InfoBarElement (id=InForceFlag) at PolicyInfoBar.pcf: line 14, column 47
    function tooltip_0 () : java.lang.String {
      return InfoBarHelper.InForceTooltip
    }
    
    // 'tooltip' attribute on InfoBarElement (id=ProducerName) at PolicyInfoBar.pcf: line 53, column 59
    function tooltip_18 () : java.lang.String {
      return DisplayKey.get("Web.PolicyInfoBar.ProducerCode", policyPeriod.PrimaryProducerCode)
    }
    
    // 'tooltip' attribute on InfoBarElement (id=ProductType) at PolicyInfoBar.pcf: line 31, column 219
    function tooltip_5 () : java.lang.String {
      return policyPeriod.Policy.LOBCode == null ? DisplayKey.get("Web.PolicyInfoBar.PolicyNullProductType") : DisplayKey.get("Web.PolicyInfoBar.PolicyProductType", policyPeriod.Policy.LOBCode)
    }
    
    // 'tooltip' attribute on InfoBarElement (id=BillingType) at PolicyInfoBar.pcf: line 36, column 57
    function tooltip_7 () : java.lang.String {
      return policyPeriod.BillingMethod.DisplayName
    }
    
    // 'value' attribute on InfoBarElement (id=PolicyNumber) at PolicyInfoBar.pcf: line 41, column 41
    function value_11 () : java.lang.Object {
      return policyPeriod.DisplayName
    }
    
    // 'value' attribute on InfoBarElement (id=AccountNumber) at PolicyInfoBar.pcf: line 46, column 51
    function value_14 () : java.lang.Object {
      return policyPeriod.Account.AccountNumber
    }
    
    // 'value' attribute on InfoBarElement (id=ProducerName) at PolicyInfoBar.pcf: line 53, column 59
    function value_19 () : java.lang.Object {
      return policyPeriod.PrimaryProducerCode.Producer.Name
    }
    
    // 'value' attribute on InfoBarElement (id=CurrentDate) at PolicyInfoBar.pcf: line 56, column 37
    function value_20 () : java.lang.Object {
      return java.util.Date.Today
    }
    
    // 'visible' attribute on InfoBarElement (id=ProducerName) at PolicyInfoBar.pcf: line 53, column 59
    function visible_15 () : java.lang.Boolean {
      return policyPeriod.PrimaryProducerCode != null
    }
    
    // 'visible' attribute on InfoBarElement (id=DelinquencyFlag) at PolicyInfoBar.pcf: line 20, column 61
    function visible_2 () : java.lang.Boolean {
      return policyPeriod.hasActiveDelinquencyProcess()
    }
    
    // 'visible' attribute on InfoBarElement (id=TroubleTicketsFlag) at PolicyInfoBar.pcf: line 26, column 56
    function visible_4 () : java.lang.Boolean {
      return InfoBarHelper.HasActiveTroubleTickets
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    
    var _helper : PolicyInfoBarHelper
    
    property get InfoBarHelper() : PolicyInfoBarHelper {
      if (_helper == null or _helper.PolicyPeriod != policyPeriod) {
        _helper = new PolicyInfoBarHelper(policyPeriod)  
      }
      return _helper
    }
    
    
  }
  
  
}