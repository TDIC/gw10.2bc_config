package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ActivitySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ActivitySearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/search/ActivitySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ActivitySearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/ActivitySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Subject_Cell) at ActivitySearchScreen.pcf: line 60, column 53
    function action_25 () : void {
      gw.activity.ActivityMethods.viewActivityDetails(activitySearchView.Activity)
    }
    
    // 'action' attribute on TextCell (id=Account_Ext_Cell) at ActivitySearchScreen.pcf: line 92, column 45
    function action_44 () : void {
      AccountDetailSummary.go(activitySearchView.Account_Ext)
    }
    
    // 'action' attribute on TextCell (id=PolicyPeriod_Ext_Cell) at ActivitySearchScreen.pcf: line 98, column 50
    function action_49 () : void {
      PolicyDetailSummary.go(activitySearchView.PolicyPeriod_Ext)
    }
    
    // 'action' attribute on TextCell (id=Producer_Ext_Cell) at ActivitySearchScreen.pcf: line 104, column 46
    function action_54 () : void {
      ProducerDetail.go(activitySearchView.Producer_Ext)
    }
    
    // 'action' attribute on TextCell (id=Account_Ext_Cell) at ActivitySearchScreen.pcf: line 92, column 45
    function action_dest_45 () : pcf.api.Destination {
      return pcf.AccountDetailSummary.createDestination(activitySearchView.Account_Ext)
    }
    
    // 'action' attribute on TextCell (id=PolicyPeriod_Ext_Cell) at ActivitySearchScreen.pcf: line 98, column 50
    function action_dest_50 () : pcf.api.Destination {
      return pcf.PolicyDetailSummary.createDestination(activitySearchView.PolicyPeriod_Ext)
    }
    
    // 'action' attribute on TextCell (id=Producer_Ext_Cell) at ActivitySearchScreen.pcf: line 104, column 46
    function action_dest_55 () : pcf.api.Destination {
      return pcf.ProducerDetail.createDestination(activitySearchView.Producer_Ext)
    }
    
    // 'iconColor' attribute on BooleanRadioCell (id=Escalated_Cell) at ActivitySearchScreen.pcf: line 44, column 55
    function iconColor_17 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_URGENCY_HIGH
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at ActivitySearchScreen.pcf: line 44, column 55
    function valueRoot_16 () : java.lang.Object {
      return activitySearchView
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at ActivitySearchScreen.pcf: line 44, column 55
    function value_15 () : java.lang.Boolean {
      return activitySearchView.Escalated
    }
    
    // 'value' attribute on DateCell (id=CreateDate_Cell) at ActivitySearchScreen.pcf: line 48, column 56
    function value_19 () : java.util.Date {
      return activitySearchView.CreateDate
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at ActivitySearchScreen.pcf: line 54, column 56
    function value_22 () : java.util.Date {
      return activitySearchView.TargetDate
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at ActivitySearchScreen.pcf: line 60, column 53
    function value_26 () : java.lang.String {
      return activitySearchView.Subject
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at ActivitySearchScreen.pcf: line 65, column 47
    function value_29 () : typekey.Priority {
      return activitySearchView.Priority
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at ActivitySearchScreen.pcf: line 70, column 53
    function value_32 () : typekey.ActivityStatus {
      return activitySearchView.Status
    }
    
    // 'value' attribute on TextCell (id=ActivityPattern_Cell) at ActivitySearchScreen.pcf: line 76, column 31
    function value_35 () : entity.ActivityPattern {
      return activitySearchView.ActivityPattern
    }
    
    // 'value' attribute on TextCell (id=AssignedTo_Cell) at ActivitySearchScreen.pcf: line 81, column 42
    function value_38 () : entity.User {
      return activitySearchView.AssignedUser
    }
    
    // 'value' attribute on TextCell (id=AssignedQueue_Ext_Cell) at ActivitySearchScreen.pcf: line 86, column 53
    function value_41 () : entity.AssignableQueue {
      return activitySearchView.AssignedQueue_Ext
    }
    
    // 'value' attribute on TextCell (id=Account_Ext_Cell) at ActivitySearchScreen.pcf: line 92, column 45
    function value_46 () : entity.Account {
      return activitySearchView.Account_Ext
    }
    
    // 'value' attribute on TextCell (id=PolicyPeriod_Ext_Cell) at ActivitySearchScreen.pcf: line 98, column 50
    function value_51 () : entity.PolicyPeriod {
      return activitySearchView.PolicyPeriod_Ext
    }
    
    // 'value' attribute on TextCell (id=Producer_Ext_Cell) at ActivitySearchScreen.pcf: line 104, column 46
    function value_56 () : entity.Producer {
      return activitySearchView.Producer_Ext
    }
    
    property get activitySearchView () : entity.ActivitySearchView {
      return getIteratedValue(2) as entity.ActivitySearchView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/ActivitySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends ActivitySearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at ActivitySearchScreen.pcf: line 16, column 48
    function def_onEnter_0 (def :  pcf.ActivitySearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at ActivitySearchScreen.pcf: line 16, column 48
    function def_refreshVariables_1 (def :  pcf.ActivitySearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'iconColor' attribute on BooleanRadioCell (id=Escalated_Cell) at ActivitySearchScreen.pcf: line 44, column 55
    function iconColor_2 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_URGENCY_HIGH
    }
    
    // 'maxSearchResults' attribute on SearchPanel at ActivitySearchScreen.pcf: line 14, column 86
    function maxSearchResults_61 () : java.lang.Object {
      return gw.api.system.BCConfigParameters.MaxSearchResults.Value
    }
    
    // 'searchCriteria' attribute on SearchPanel at ActivitySearchScreen.pcf: line 14, column 86
    function searchCriteria_63 () : gw.search.ActivitySearchCriteria {
      return new gw.search.ActivitySearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at ActivitySearchScreen.pcf: line 14, column 86
    function search_62 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria)
    }
    
    // 'value' attribute on TextCell (id=AssignedTo_Cell) at ActivitySearchScreen.pcf: line 81, column 42
    function sortValue_10 (activitySearchView :  entity.ActivitySearchView) : java.lang.Object {
      return activitySearchView.AssignedUser
    }
    
    // 'value' attribute on TextCell (id=AssignedQueue_Ext_Cell) at ActivitySearchScreen.pcf: line 86, column 53
    function sortValue_11 (activitySearchView :  entity.ActivitySearchView) : java.lang.Object {
      return activitySearchView.AssignedQueue_Ext
    }
    
    // 'value' attribute on TextCell (id=Account_Ext_Cell) at ActivitySearchScreen.pcf: line 92, column 45
    function sortValue_12 (activitySearchView :  entity.ActivitySearchView) : java.lang.Object {
      return activitySearchView.Account_Ext
    }
    
    // 'value' attribute on TextCell (id=PolicyPeriod_Ext_Cell) at ActivitySearchScreen.pcf: line 98, column 50
    function sortValue_13 (activitySearchView :  entity.ActivitySearchView) : java.lang.Object {
      return activitySearchView.PolicyPeriod_Ext
    }
    
    // 'value' attribute on TextCell (id=Producer_Ext_Cell) at ActivitySearchScreen.pcf: line 104, column 46
    function sortValue_14 (activitySearchView :  entity.ActivitySearchView) : java.lang.Object {
      return activitySearchView.Producer_Ext
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at ActivitySearchScreen.pcf: line 44, column 55
    function sortValue_3 (activitySearchView :  entity.ActivitySearchView) : java.lang.Object {
      return activitySearchView.Escalated
    }
    
    // 'value' attribute on DateCell (id=CreateDate_Cell) at ActivitySearchScreen.pcf: line 48, column 56
    function sortValue_4 (activitySearchView :  entity.ActivitySearchView) : java.lang.Object {
      return activitySearchView.CreateDate
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at ActivitySearchScreen.pcf: line 54, column 56
    function sortValue_5 (activitySearchView :  entity.ActivitySearchView) : java.lang.Object {
      return activitySearchView.TargetDate
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at ActivitySearchScreen.pcf: line 60, column 53
    function sortValue_6 (activitySearchView :  entity.ActivitySearchView) : java.lang.Object {
      return activitySearchView.Subject
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at ActivitySearchScreen.pcf: line 65, column 47
    function sortValue_7 (activitySearchView :  entity.ActivitySearchView) : java.lang.Object {
      return activitySearchView.Priority
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at ActivitySearchScreen.pcf: line 70, column 53
    function sortValue_8 (activitySearchView :  entity.ActivitySearchView) : java.lang.Object {
      return activitySearchView.Status
    }
    
    // 'value' attribute on TextCell (id=ActivityPattern_Cell) at ActivitySearchScreen.pcf: line 76, column 31
    function sortValue_9 (activitySearchView :  entity.ActivitySearchView) : java.lang.Object {
      return activitySearchView.ActivityPattern
    }
    
    // 'value' attribute on RowIterator at ActivitySearchScreen.pcf: line 30, column 91
    function value_60 () : gw.api.database.IQueryBeanResult<entity.ActivitySearchView> {
      return activitySearchViews
    }
    
    property get activitySearchViews () : gw.api.database.IQueryBeanResult<ActivitySearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<ActivitySearchView>
    }
    
    property get searchCriteria () : gw.search.ActivitySearchCriteria {
      return getCriteriaValue(1) as gw.search.ActivitySearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.ActivitySearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}