package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/ActivityDetailToolbarButtonSet.disbursementapproval.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ActivityDetailToolbarButtonSet_disbursementapprovalExpressions {
  @javax.annotation.Generated("config/web/pcf/activity/ActivityDetailToolbarButtonSet.disbursementapproval.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ActivityDetailToolbarButtonSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=ActivityDetailToolbarButtonSet_UpdateButton) at ActivityDetailToolbarButtonSet.disbursementapproval.pcf: line 19, column 29
    function action_1 () : void {
      gw.acc.activityenhancement.DesktopQueuedActivitiesUtil.assignActivity(assigneeHolder[0] , activity); CurrentLocation.commit()
    }
    
    // 'action' attribute on ToolbarButton (id=ActivityDetailToolbarButtonSet_CancelButton) at ActivityDetailToolbarButtonSet.disbursementapproval.pcf: line 44, column 29
    function action_11 () : void {
      CurrentLocation.cancel()
    }
    
    // 'action' attribute on ToolbarButton (id=ActivityDetailToolbarButtonSet_CloseButton) at ActivityDetailToolbarButtonSet.disbursementapproval.pcf: line 49, column 33
    function action_13 () : void {
      CurrentLocation.cancel()
    }
    
    // 'action' attribute on ToolbarButton (id=ActivityDetailToolbarButtonSet_EditButton) at ActivityDetailToolbarButtonSet.disbursementapproval.pcf: line 24, column 56
    function action_3 () : void {
      CurrentLocation.startEditing()
    }
    
    // 'action' attribute on ToolbarButton (id=ActivityDetailToolbarButtonSet_ApproveButton) at ActivityDetailToolbarButtonSet.disbursementapproval.pcf: line 29, column 59
    function action_5 () : void {
      if (not CurrentLocation.InEditMode) {CurrentLocation.startEditing()}; gw.api.web.activity.ActivityUtil.approveActivity(activity); activity.SetApproverName() CurrentLocation.commit();
    }
    
    // 'action' attribute on ToolbarButton (id=ActivityDetailToolbarButtonSet_RejectButton) at ActivityDetailToolbarButtonSet.disbursementapproval.pcf: line 34, column 59
    function action_7 () : void {
      if (not CurrentLocation.InEditMode) {CurrentLocation.startEditing()}; gw.api.web.activity.DisbApprActivityUtil.rejectActivity(activity as DisbApprActivity); CurrentLocation.commit();
    }
    
    // 'action' attribute on ToolbarButton (id=ActivityDetailToolbarButtonSet_RejectWithHold) at ActivityDetailToolbarButtonSet.disbursementapproval.pcf: line 39, column 59
    function action_9 () : void {
      if (not CurrentLocation.InEditMode) {CurrentLocation.startEditing()}; gw.api.web.activity.DisbApprActivityUtil.rejectActivityWithHold(activity as DisbApprActivity); CurrentLocation.commit();
    }
    
    // 'visible' attribute on ToolbarButton (id=ActivityDetailToolbarButtonSet_UpdateButton) at ActivityDetailToolbarButtonSet.disbursementapproval.pcf: line 19, column 29
    function visible_0 () : java.lang.Boolean {
      return InEditMode
    }
    
    // 'visible' attribute on ToolbarButton (id=ActivityDetailToolbarButtonSet_CloseButton) at ActivityDetailToolbarButtonSet.disbursementapproval.pcf: line 49, column 33
    function visible_12 () : java.lang.Boolean {
      return not InEditMode
    }
    
    // 'visible' attribute on ToolbarButton (id=ActivityDetailToolbarButtonSet_EditButton) at ActivityDetailToolbarButtonSet.disbursementapproval.pcf: line 24, column 56
    function visible_2 () : java.lang.Boolean {
      return activity.canEdit() and not InEditMode
    }
    
    // 'visible' attribute on ToolbarButton (id=ActivityDetailToolbarButtonSet_ApproveButton) at ActivityDetailToolbarButtonSet.disbursementapproval.pcf: line 29, column 59
    function visible_4 () : java.lang.Boolean {
      return activity.canApprove() and not InEditMode
    }
    
    property get activity () : Activity {
      return getRequireValue("activity", 0) as Activity
    }
    
    property set activity ($arg :  Activity) {
      setRequireValue("activity", 0, $arg)
    }
    
    property get assigneeHolder () : gw.api.assignment.Assignee[] {
      return getRequireValue("assigneeHolder", 0) as gw.api.assignment.Assignee[]
    }
    
    property set assigneeHolder ($arg :  gw.api.assignment.Assignee[]) {
      setRequireValue("assigneeHolder", 0, $arg)
    }
    
    
  }
  
  
}