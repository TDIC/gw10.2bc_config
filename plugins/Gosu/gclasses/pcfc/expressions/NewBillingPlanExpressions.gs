package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.CurrencyUtil
@javax.annotation.Generated("config/web/pcf/admin/billing/NewBillingPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewBillingPlanExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/billing/NewBillingPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewBillingPlanExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (currency :  Currency) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=NewBillingPlan) at NewBillingPlan.pcf: line 14, column 66
    function afterCancel_6 () : void {
      BillingPlans.go()
    }
    
    // 'afterCancel' attribute on Page (id=NewBillingPlan) at NewBillingPlan.pcf: line 14, column 66
    function afterCancel_dest_7 () : pcf.api.Destination {
      return pcf.BillingPlans.createDestination()
    }
    
    // 'afterCommit' attribute on Page (id=NewBillingPlan) at NewBillingPlan.pcf: line 14, column 66
    function afterCommit_8 (TopLocation :  pcf.api.Location) : void {
      BillingPlans.go()
    }
    
    // 'canVisit' attribute on Page (id=NewBillingPlan) at NewBillingPlan.pcf: line 14, column 66
    static function canVisit_9 (currency :  Currency) : java.lang.Boolean {
      return perm.System.admintabview and perm.System.billplancreate
    }
    
    // 'def' attribute on PanelRef at NewBillingPlan.pcf: line 30, column 49
    function def_onEnter_2 (def :  pcf.BillingPlanDetailDV) : void {
      def.onEnter(billingPlan)
    }
    
    // 'def' attribute on PanelRef at NewBillingPlan.pcf: line 32, column 121
    function def_onEnter_4 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(billingPlan, { "Name"}, { DisplayKey.get("Web.Admin.Plan.Name") })
    }
    
    // 'def' attribute on PanelRef at NewBillingPlan.pcf: line 30, column 49
    function def_refreshVariables_3 (def :  pcf.BillingPlanDetailDV) : void {
      def.refreshVariables(billingPlan)
    }
    
    // 'def' attribute on PanelRef at NewBillingPlan.pcf: line 32, column 121
    function def_refreshVariables_5 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(billingPlan, { "Name"}, { DisplayKey.get("Web.Admin.Plan.Name") })
    }
    
    // 'initialValue' attribute on Variable at NewBillingPlan.pcf: line 20, column 27
    function initialValue_0 () : BillingPlan {
      return initBillingPlan()
    }
    
    // EditButtons at NewBillingPlan.pcf: line 27, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'parent' attribute on Page (id=NewBillingPlan) at NewBillingPlan.pcf: line 14, column 66
    static function parent_10 (currency :  Currency) : pcf.api.Destination {
      return pcf.BillingPlans.createDestination()
    }
    
    override property get CurrentLocation () : pcf.NewBillingPlan {
      return super.CurrentLocation as pcf.NewBillingPlan
    }
    
    property get billingPlan () : BillingPlan {
      return getVariableValue("billingPlan", 0) as BillingPlan
    }
    
    property set billingPlan ($arg :  BillingPlan) {
      setVariableValue("billingPlan", 0, $arg)
    }
    
    property get currency () : Currency {
      return getVariableValue("currency", 0) as Currency
    }
    
    property set currency ($arg :  Currency) {
      setVariableValue("currency", 0, $arg)
    }
    
    
    function initBillingPlan() : BillingPlan {
      var newBillingPlan = new BillingPlan(CurrentLocation)
      if (not isMultiCurrencyMode()) {
        newBillingPlan.addToCurrencies(currency)
      }
      newBillingPlan.EffectiveDate = gw.api.util.DateUtil.currentDate();
      return newBillingPlan;
    }
    
    function isMultiCurrencyMode(): boolean {
      return CurrencyUtil.isMultiCurrencyMode() and Currency.getTypeKeys(true).Count > 1
    }
    
    
  }
  
  
}