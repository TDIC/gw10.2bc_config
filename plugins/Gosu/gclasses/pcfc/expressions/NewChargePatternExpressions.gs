package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/chargepatterns/NewChargePattern.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewChargePatternExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/chargepatterns/NewChargePattern.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewChargePatternExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (patternType :  String) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=NewChargePattern) at NewChargePattern.pcf: line 14, column 68
    function afterCancel_6 () : void {
      ChargePatterns.go()
    }
    
    // 'afterCancel' attribute on Page (id=NewChargePattern) at NewChargePattern.pcf: line 14, column 68
    function afterCancel_dest_7 () : pcf.api.Destination {
      return pcf.ChargePatterns.createDestination()
    }
    
    // 'afterCommit' attribute on Page (id=NewChargePattern) at NewChargePattern.pcf: line 14, column 68
    function afterCommit_8 (TopLocation :  pcf.api.Location) : void {
      ChargePatterns.go()
    }
    
    // 'canVisit' attribute on Page (id=NewChargePattern) at NewChargePattern.pcf: line 14, column 68
    static function canVisit_9 (patternType :  String) : java.lang.Boolean {
      return perm.System.chargepatterncreate
    }
    
    // 'def' attribute on PanelRef at NewChargePattern.pcf: line 30, column 53
    function def_onEnter_2 (def :  pcf.ChargePatternDetailDV) : void {
      def.onEnter(chargePattern)
    }
    
    // 'def' attribute on PanelRef at NewChargePattern.pcf: line 32, column 147
    function def_onEnter_4 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(chargePattern, { "ChargeName" }, { DisplayKey.get("Web.ChargePatternDetailDV.ChargeName") })
    }
    
    // 'def' attribute on PanelRef at NewChargePattern.pcf: line 30, column 53
    function def_refreshVariables_3 (def :  pcf.ChargePatternDetailDV) : void {
      def.refreshVariables(chargePattern)
    }
    
    // 'def' attribute on PanelRef at NewChargePattern.pcf: line 32, column 147
    function def_refreshVariables_5 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(chargePattern, { "ChargeName" }, { DisplayKey.get("Web.ChargePatternDetailDV.ChargeName") })
    }
    
    // 'initialValue' attribute on Variable at NewChargePattern.pcf: line 23, column 29
    function initialValue_0 () : ChargePattern {
      return createChargePattern()
    }
    
    // EditButtons at NewChargePattern.pcf: line 27, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'parent' attribute on Page (id=NewChargePattern) at NewChargePattern.pcf: line 14, column 68
    static function parent_10 (patternType :  String) : pcf.api.Destination {
      return pcf.ChargePatterns.createDestination()
    }
    
    override property get CurrentLocation () : pcf.NewChargePattern {
      return super.CurrentLocation as pcf.NewChargePattern
    }
    
    property get chargePattern () : ChargePattern {
      return getVariableValue("chargePattern", 0) as ChargePattern
    }
    
    property set chargePattern ($arg :  ChargePattern) {
      setVariableValue("chargePattern", 0, $arg)
    }
    
    property get patternType () : String {
      return getVariableValue("patternType", 0) as String
    }
    
    property set patternType ($arg :  String) {
      setVariableValue("patternType", 0, $arg)
    }
    
    function createChargePattern() : ChargePattern {
            if (patternType == "ImmediateCharge") {
              var immediate = new ImmediateCharge();
              immediate.InvoiceTreatment = TC_ONETIME
              return immediate;
            } else if (patternType == "PassThroughCharge") {
              var passThrough = new PassThroughCharge();
              passThrough.InvoiceTreatment = TC_ONETIME
              return passThrough;
            } else if (patternType == "RecaptureCharge") {
              var recapture = new RecaptureCharge();
              recapture.InvoiceTreatment = TC_ONETIME
              recapture.Category = ChargeCategory.TC_RECAPTURE
              recapture.setTAccountOwnerPatternFromString(entity.Account.Type.RelativeName);
              return recapture;
            } else {
              var proRata = new ProRataCharge();
              proRata.InvoiceTreatment = TC_ONETIME
              proRata.setTAccountOwnerPatternFromString(entity.PolicyPeriod.Type.RelativeName);
              return proRata;
            }
          }
    
    
  }
  
  
}