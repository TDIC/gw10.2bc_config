package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/archive/ArchiveSuspendResumePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ArchiveSuspendResumePopupExpressions {
  @javax.annotation.Generated("config/web/pcf/archive/ArchiveSuspendResumePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ArchiveSuspendResumePopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policy :  Policy) : int {
      return 0
    }
    
    // 'available' attribute on CheckBoxInput (id=ToggleDoNotArchive_Input) at ArchiveSuspendResumePopup.pcf: line 46, column 40
    function available_5 () : java.lang.Boolean {
      return PolicyDoNotArchiveHelper.ToggleAvailable
    }
    
    // 'beforeCommit' attribute on Popup (id=ArchiveSuspendResumePopup) at ArchiveSuspendResumePopup.pcf: line 11, column 59
    function beforeCommit_12 (pickedValue :  java.lang.Object) : void {
      PolicyDoNotArchiveHelper.toggleDoNotArchive(true, null)
    }
    
    // 'value' attribute on CheckBoxInput (id=ToggleDoNotArchive_Input) at ArchiveSuspendResumePopup.pcf: line 46, column 40
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      ChangeDoNotArchive = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'initialValue' attribute on Variable at ArchiveSuspendResumePopup.pcf: line 24, column 55
    function initialValue_0 () : gw.web.archive.PolicyDoNotArchiveHelper {
      return new gw.web.archive.PolicyDoNotArchiveHelper(policy)
    }
    
    // EditButtons (id=EditButtons) at ArchiveSuspendResumePopup.pcf: line 29, column 47
    function label_2 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'label' attribute on Label at ArchiveSuspendResumePopup.pcf: line 40, column 29
    function label_4 () : java.lang.String {
      return PolicyDoNotArchiveHelper.DoNotArchiveLabel
    }
    
    // 'label' attribute on CheckBoxInput (id=ToggleDoNotArchive_Input) at ArchiveSuspendResumePopup.pcf: line 46, column 40
    function label_6 () : java.lang.Object {
      return PolicyDoNotArchiveHelper.ChangeDoNotArchiveLabel
    }
    
    // 'updateVisible' attribute on EditButtons (id=EditButtons) at ArchiveSuspendResumePopup.pcf: line 29, column 47
    function visible_1 () : java.lang.Boolean {
      return ChangeDoNotArchive
    }
    
    // 'visible' attribute on AlertBar (id=CurrentlyArchivedPeriodsWarning) at ArchiveSuspendResumePopup.pcf: line 35, column 157
    function visible_3 () : java.lang.Boolean {
      return policy.PolicyPeriods.hasMatch(\ p -> p.ArchiveState == ArchiveState.TC_ARCHIVED) and (not policy.DoNotArchive) and ChangeDoNotArchive
    }
    
    property get ChangeDoNotArchive () : boolean {
      return getVariableValue("ChangeDoNotArchive", 0) as java.lang.Boolean
    }
    
    property set ChangeDoNotArchive ($arg :  boolean) {
      setVariableValue("ChangeDoNotArchive", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.ArchiveSuspendResumePopup {
      return super.CurrentLocation as pcf.ArchiveSuspendResumePopup
    }
    
    property get PolicyDoNotArchiveHelper () : gw.web.archive.PolicyDoNotArchiveHelper {
      return getVariableValue("PolicyDoNotArchiveHelper", 0) as gw.web.archive.PolicyDoNotArchiveHelper
    }
    
    property set PolicyDoNotArchiveHelper ($arg :  gw.web.archive.PolicyDoNotArchiveHelper) {
      setVariableValue("PolicyDoNotArchiveHelper", 0, $arg)
    }
    
    property get policy () : Policy {
      return getVariableValue("policy", 0) as Policy
    }
    
    property set policy ($arg :  Policy) {
      setVariableValue("policy", 0, $arg)
    }
    
    
  }
  
  
}