package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/InvoiceSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceSearchPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/search/InvoiceSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceSearchPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 1
    }
    
    // 'def' attribute on ScreenRef at InvoiceSearchPopup.pcf: line 17, column 57
    function def_onEnter_0 (def :  pcf.InvoiceSearchScreen) : void {
      def.onEnter(false, false, account)
    }
    
    // 'def' attribute on ScreenRef at InvoiceSearchPopup.pcf: line 17, column 57
    function def_refreshVariables_1 (def :  pcf.InvoiceSearchScreen) : void {
      def.refreshVariables(false, false, account)
    }
    
    override property get CurrentLocation () : pcf.InvoiceSearchPopup {
      return super.CurrentLocation as pcf.InvoiceSearchPopup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  
}