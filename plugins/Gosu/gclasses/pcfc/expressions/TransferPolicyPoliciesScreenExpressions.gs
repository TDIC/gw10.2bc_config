package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyPoliciesScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransferPolicyPoliciesScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyPoliciesScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TransferPolicyPoliciesScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at TransferPolicyPoliciesScreen.pcf: line 66, column 53
    function action_17 () : void {
      PolicySummary.push(policyPeriod)
    }
    
    // 'action' attribute on TextCell (id=Account_Cell) at TransferPolicyPoliciesScreen.pcf: line 81, column 70
    function action_27 () : void {
      AccountSummaryPopup.push(policyPeriod.Policy.Account)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at TransferPolicyPoliciesScreen.pcf: line 66, column 53
    function action_dest_18 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(policyPeriod)
    }
    
    // 'action' attribute on TextCell (id=Account_Cell) at TransferPolicyPoliciesScreen.pcf: line 81, column 70
    function action_dest_28 () : pcf.api.Destination {
      return pcf.AccountSummaryPopup.createDestination(policyPeriod.Policy.Account)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=PolValue_Cell) at TransferPolicyPoliciesScreen.pcf: line 100, column 52
    function currency_43 () : typekey.Currency {
      return policyPeriod.Currency
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at TransferPolicyPoliciesScreen.pcf: line 66, column 53
    function valueRoot_20 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at TransferPolicyPoliciesScreen.pcf: line 71, column 55
    function valueRoot_23 () : java.lang.Object {
      return policyPeriod.Policy.Account
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at TransferPolicyPoliciesScreen.pcf: line 66, column 53
    function value_19 () : java.lang.String {
      return policyPeriod.DisplayName
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at TransferPolicyPoliciesScreen.pcf: line 71, column 55
    function value_22 () : typekey.AccountSegment {
      return policyPeriod.Policy.Account.Segment
    }
    
    // 'value' attribute on TextCell (id=Role_Cell) at TransferPolicyPoliciesScreen.pcf: line 76, column 54
    function value_25 () : java.lang.String {
      return formatRoles(policyPeriod)
    }
    
    // 'value' attribute on TextCell (id=Account_Cell) at TransferPolicyPoliciesScreen.pcf: line 81, column 70
    function value_29 () : java.lang.String {
      return policyPeriod.Policy.Account.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=Insured_Cell) at TransferPolicyPoliciesScreen.pcf: line 85, column 77
    function value_32 () : java.lang.String {
      return policyPeriod.Policy.Account.AccountNameLocalized
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at TransferPolicyPoliciesScreen.pcf: line 89, column 58
    function value_35 () : java.util.Date {
      return policyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at TransferPolicyPoliciesScreen.pcf: line 93, column 60
    function value_38 () : java.util.Date {
      return policyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PolValue_Cell) at TransferPolicyPoliciesScreen.pcf: line 100, column 52
    function value_41 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.TotalValue
    }
    
    property get policyPeriod () : entity.PolicyPeriod {
      return getIteratedValue(1) as entity.PolicyPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyPoliciesScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransferPolicyPoliciesScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=Cancel) at TransferPolicyPoliciesScreen.pcf: line 18, column 60
    function action_1 () : void {
      CurrentLocation.cancel()
    }
    
    // 'action' attribute on ToolbarButton (id=Previous) at TransferPolicyPoliciesScreen.pcf: line 23, column 62
    function action_2 () : void {
      (CurrentLocation as pcf.api.Wizard).previous()
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=Next) at TransferPolicyPoliciesScreen.pcf: line 28, column 58
    function allCheckedRowsAction_3 (CheckedValues :  entity.PolicyPeriod[], CheckedValuesErrors :  java.util.Map) : void {
      selectPolicyPeriods(CheckedValues)
    }
    
    // 'initialValue' attribute on Variable at TransferPolicyPoliciesScreen.pcf: line 13, column 68
    function initialValue_0 () : gw.api.database.IQueryBeanResult<PolicyPeriod> {
      return policyTransfer.SourceProducerCode.OpenCurrentlyRelatedPolicyPeriods
    }
    
    // 'sortBy' attribute on TextCell (id=PolicyNumber_Cell) at TransferPolicyPoliciesScreen.pcf: line 66, column 53
    function sortValue_10 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PolicyNumber
    }
    
    // 'sortBy' attribute on TextCell (id=PolicyNumber_Cell) at TransferPolicyPoliciesScreen.pcf: line 66, column 53
    function sortValue_11 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return  policyPeriod.TermNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at TransferPolicyPoliciesScreen.pcf: line 71, column 55
    function sortValue_12 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.Policy.Account.Segment
    }
    
    // 'value' attribute on TextCell (id=Account_Cell) at TransferPolicyPoliciesScreen.pcf: line 81, column 70
    function sortValue_13 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.Policy.Account.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=Insured_Cell) at TransferPolicyPoliciesScreen.pcf: line 85, column 77
    function sortValue_14 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.Policy.Account.AccountNameLocalized
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at TransferPolicyPoliciesScreen.pcf: line 89, column 58
    function sortValue_15 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at TransferPolicyPoliciesScreen.pcf: line 93, column 60
    function sortValue_16 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on TextInput (id=producer_Input) at TransferPolicyPoliciesScreen.pcf: line 39, column 40
    function valueRoot_5 () : java.lang.Object {
      return policyTransfer.SourceProducerCode
    }
    
    // 'value' attribute on TextInput (id=producer_Input) at TransferPolicyPoliciesScreen.pcf: line 39, column 40
    function value_4 () : entity.Producer {
      return policyTransfer.SourceProducerCode.Producer
    }
    
    // 'value' attribute on RowIterator at TransferPolicyPoliciesScreen.pcf: line 58, column 87
    function value_45 () : gw.api.database.IQueryBeanResult<entity.PolicyPeriod> {
      return availablePolicyPeriods
    }
    
    // 'value' attribute on TextInput (id=producerCode_Input) at TransferPolicyPoliciesScreen.pcf: line 43, column 59
    function value_7 () : java.lang.String {
      return policyTransfer.SourceProducerCode.Code
    }
    
    property get availablePolicyPeriods () : gw.api.database.IQueryBeanResult<PolicyPeriod> {
      return getVariableValue("availablePolicyPeriods", 0) as gw.api.database.IQueryBeanResult<PolicyPeriod>
    }
    
    property set availablePolicyPeriods ($arg :  gw.api.database.IQueryBeanResult<PolicyPeriod>) {
      setVariableValue("availablePolicyPeriods", 0, $arg)
    }
    
    property get policyTransfer () : PolTransferByProdCode {
      return getRequireValue("policyTransfer", 0) as PolTransferByProdCode
    }
    
    property set policyTransfer ($arg :  PolTransferByProdCode) {
      setRequireValue("policyTransfer", 0, $arg)
    }
    
    function formatRoles(policyPeriod : PolicyPeriod) : String {
      var roles = policyPeriod.getDefaultRolesForProducerCode(policyTransfer.SourceProducerCode);
      return roles*.DisplayName.join(", ")
    }
    
        function selectPolicyPeriods(policyPeriods : PolicyPeriod[]){
          if (policyPeriods.length > 0) {
            policyTransfer.setPolicyPeriods( policyPeriods )
            (CurrentLocation as pcf.api.Wizard).next()
          } else {
            throw new gw.api.util.DisplayableException(DisplayKey.get("Web.TransferPolicyPolicies.NoPolicySelected"))
          }
        }
    
    
  }
  
  
}