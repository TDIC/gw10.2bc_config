package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/collection/CollectionAgencyDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CollectionAgencyDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/collection/CollectionAgencyDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CollectionAgencyDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at CollectionAgencyDetailDV.pcf: line 17, column 40
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      collectionAgency.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at CollectionAgencyDetailDV.pcf: line 17, column 40
    function valueRoot_2 () : java.lang.Object {
      return collectionAgency
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at CollectionAgencyDetailDV.pcf: line 17, column 40
    function value_0 () : java.lang.String {
      return collectionAgency.Name
    }
    
    property get collectionAgency () : CollectionAgency {
      return getRequireValue("collectionAgency", 0) as CollectionAgency
    }
    
    property set collectionAgency ($arg :  CollectionAgency) {
      setRequireValue("collectionAgency", 0, $arg)
    }
    
    
  }
  
  
}