package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/DisbursementDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DisbursementDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/disbursement/DisbursementDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DisbursementDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextInput (id=accountNumber_Input) at DisbursementDetailDV.pcf: line 31, column 102
    function action_6 () : void {
      AccountSummary.push((disbursement as AccountDisbursement).Account)
    }
    
    // 'action' attribute on TextInput (id=accountNumber_Input) at DisbursementDetailDV.pcf: line 31, column 102
    function action_dest_7 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination((disbursement as AccountDisbursement).Account)
    }
    
    // 'available' attribute on TextInput (id=accountNumber_Input) at DisbursementDetailDV.pcf: line 31, column 102
    function available_5 () : java.lang.Boolean {
      return disbursement typeis AccountDisbursement
    }
    
    // 'def' attribute on InputSetRef at DisbursementDetailDV.pcf: line 23, column 79
    function def_onEnter_2 (def :  pcf.DisbursementDetailsInputSet_default) : void {
      def.onEnter(disbursement)
    }
    
    // 'def' attribute on InputSetRef at DisbursementDetailDV.pcf: line 23, column 79
    function def_refreshVariables_3 (def :  pcf.DisbursementDetailsInputSet_default) : void {
      def.refreshVariables(disbursement)
    }
    
    // 'value' attribute on DateInput (id=dueDate_Input) at DisbursementDetailDV.pcf: line 51, column 54
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      disbursement.DueDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=payTo_Input) at DisbursementDetailDV.pcf: line 60, column 36
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      disbursement.PayTo = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=mailTo_Input) at DisbursementDetailDV.pcf: line 68, column 37
    function defaultSetter_36 (__VALUE_TO_SET :  java.lang.Object) : void {
      disbursement.MailTo = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextAreaInput (id=mailToAddress_Input) at DisbursementDetailDV.pcf: line 77, column 38
    function defaultSetter_42 (__VALUE_TO_SET :  java.lang.Object) : void {
      disbursement.Address = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=memo_Input) at DisbursementDetailDV.pcf: line 84, column 36
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      disbursement.Memo = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on DateInput (id=dueDate_Input) at DisbursementDetailDV.pcf: line 51, column 54
    function editable_18 () : java.lang.Boolean {
      return isEditable and disbursement.Status != DisbursementStatus.TC_SENT
    }
    
    // 'editable' attribute on TextInput (id=payTo_Input) at DisbursementDetailDV.pcf: line 60, column 36
    function editable_28 () : java.lang.Boolean {
      return canEditDisbursementPayee and disbursement.Status == DisbursementStatus.TC_AWAITINGAPPROVAL
    }
    
    // 'initialValue' attribute on Variable at DisbursementDetailDV.pcf: line 13, column 23
    function initialValue_0 () : Boolean {
      return perm.Transaction.disbpayeeedit
    }
    
    // 'initialValue' attribute on Variable at DisbursementDetailDV.pcf: line 17, column 23
    function initialValue_1 () : Boolean {
      return disbursement.isEditable()
    }
    
    // 'mode' attribute on InputSetRef at DisbursementDetailDV.pcf: line 23, column 79
    function mode_4 () : java.lang.Object {
      return gw.plugin.Plugins.get(gw.module.IFeesThresholds).getPCFMode()
    }
    
    // 'validationExpression' attribute on DateInput (id=dueDate_Input) at DisbursementDetailDV.pcf: line 51, column 54
    function validationExpression_19 () : java.lang.Object {
      return gw.api.util.DateUtil.verifyDateOnOrAfterToday(disbursement.DueDate)? null : DisplayKey.get("Web.DisbursementDetail.DueDateInPast")
    }
    
    // 'value' attribute on TextInput (id=refNumber_Input) at DisbursementDetailDV.pcf: line 44, column 51
    function valueRoot_15 () : java.lang.Object {
      return disbursement.OutgoingPayment
    }
    
    // 'value' attribute on DateInput (id=dueDate_Input) at DisbursementDetailDV.pcf: line 51, column 54
    function valueRoot_23 () : java.lang.Object {
      return disbursement
    }
    
    // 'value' attribute on TextInput (id=insured_Input) at DisbursementDetailDV.pcf: line 35, column 109
    function value_11 () : java.lang.String {
      return disbursement typeis AccountDisbursement ? disbursement.Account.AccountNameLocalized : null
    }
    
    // 'value' attribute on TextInput (id=refNumber_Input) at DisbursementDetailDV.pcf: line 44, column 51
    function value_14 () : java.lang.String {
      return disbursement.OutgoingPayment.RefNumber
    }
    
    // 'value' attribute on DateInput (id=dueDate_Input) at DisbursementDetailDV.pcf: line 51, column 54
    function value_21 () : java.util.Date {
      return disbursement.DueDate
    }
    
    // 'value' attribute on TextInput (id=payTo_Input) at DisbursementDetailDV.pcf: line 60, column 36
    function value_29 () : java.lang.String {
      return disbursement.PayTo
    }
    
    // 'value' attribute on TextInput (id=mailTo_Input) at DisbursementDetailDV.pcf: line 68, column 37
    function value_35 () : java.lang.String {
      return disbursement.MailTo
    }
    
    // 'value' attribute on TextAreaInput (id=mailToAddress_Input) at DisbursementDetailDV.pcf: line 77, column 38
    function value_41 () : java.lang.String {
      return disbursement.Address
    }
    
    // 'value' attribute on TextInput (id=memo_Input) at DisbursementDetailDV.pcf: line 84, column 36
    function value_47 () : java.lang.String {
      return disbursement.Memo
    }
    
    // 'value' attribute on TextInput (id=internalComment_Input) at DisbursementDetailDV.pcf: line 88, column 47
    function value_52 () : java.lang.String {
      return disbursement.InternalComment
    }
    
    // 'value' attribute on TextInput (id=accountNumber_Input) at DisbursementDetailDV.pcf: line 31, column 102
    function value_8 () : java.lang.String {
      return disbursement typeis AccountDisbursement ? disbursement.Account.AccountNumber : null
    }
    
    // 'visible' attribute on TextInput (id=refNumber_Input) at DisbursementDetailDV.pcf: line 44, column 51
    function visible_13 () : java.lang.Boolean {
      return disbursement.Status == TC_SENT
    }
    
    // 'visible' attribute on DateInput (id=dueDate_Input) at DisbursementDetailDV.pcf: line 51, column 54
    function visible_20 () : java.lang.Boolean {
      return disbursement.Status != TC_REJECTED
    }
    
    property get canEditDisbursementPayee () : Boolean {
      return getVariableValue("canEditDisbursementPayee", 0) as Boolean
    }
    
    property set canEditDisbursementPayee ($arg :  Boolean) {
      setVariableValue("canEditDisbursementPayee", 0, $arg)
    }
    
    property get disbursement () : Disbursement {
      return getRequireValue("disbursement", 0) as Disbursement
    }
    
    property set disbursement ($arg :  Disbursement) {
      setRequireValue("disbursement", 0, $arg)
    }
    
    property get isEditable () : Boolean {
      return getVariableValue("isEditable", 0) as Boolean
    }
    
    property set isEditable ($arg :  Boolean) {
      setVariableValue("isEditable", 0, $arg)
    }
    
    
  }
  
  
}