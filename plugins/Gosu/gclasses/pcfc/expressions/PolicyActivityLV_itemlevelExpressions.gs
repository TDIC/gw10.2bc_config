package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.web.invoice.AgencyBillInvoiceItemView
@javax.annotation.Generated("config/web/pcf/agencybill/PolicyActivityLV.itemlevel.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyActivityLV_itemlevelExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/PolicyActivityLV.itemlevel.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyActivityLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=StatementNumber_Cell) at PolicyActivityLV.itemlevel.pcf: line 45, column 52
    function actionAvailable_31 () : java.lang.Boolean {
      return invoiceItemView.Invoice typeis StatementInvoice
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at PolicyActivityLV.itemlevel.pcf: line 26, column 74
    function action_16 () : void {
      pushPolicyPeriodDetails(invoiceItemView.InvoiceItem.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=StatementNumber_Cell) at PolicyActivityLV.itemlevel.pcf: line 45, column 52
    function action_29 () : void {
      AgencyBillStatementDetail.go((invoiceItemView.Invoice as StatementInvoice).AgencyBillCycle)
    }
    
    // 'action' attribute on TextCell (id=ItemDescription_Cell) at PolicyActivityLV.itemlevel.pcf: line 60, column 25
    function action_41 () : void {
      InvoiceItemHistoryPopup.push( invoiceItemView.InvoiceItem )
    }
    
    // 'action' attribute on MonetaryAmountCell (id=Gross_Cell) at PolicyActivityLV.itemlevel.pcf: line 73, column 59
    function action_49 () : void {
      InvoiceItemHistoryPopup.push( invoiceItemView.InvoiceItem )
    }
    
    // 'action' attribute on TextCell (id=Comments_Cell) at PolicyActivityLV.itemlevel.pcf: line 100, column 56
    function action_63 () : void {
      InvoiceItemCommentsPopup.push(invoiceItemView.InvoiceItem, agencyBillStatementView)
    }
    
    // 'action' attribute on TextCell (id=StatementNumber_Cell) at PolicyActivityLV.itemlevel.pcf: line 45, column 52
    function action_dest_30 () : pcf.api.Destination {
      return pcf.AgencyBillStatementDetail.createDestination((invoiceItemView.Invoice as StatementInvoice).AgencyBillCycle)
    }
    
    // 'action' attribute on TextCell (id=ItemDescription_Cell) at PolicyActivityLV.itemlevel.pcf: line 60, column 25
    function action_dest_42 () : pcf.api.Destination {
      return pcf.InvoiceItemHistoryPopup.createDestination( invoiceItemView.InvoiceItem )
    }
    
    // 'action' attribute on MonetaryAmountCell (id=Gross_Cell) at PolicyActivityLV.itemlevel.pcf: line 73, column 59
    function action_dest_50 () : pcf.api.Destination {
      return pcf.InvoiceItemHistoryPopup.createDestination( invoiceItemView.InvoiceItem )
    }
    
    // 'action' attribute on TextCell (id=Comments_Cell) at PolicyActivityLV.itemlevel.pcf: line 100, column 56
    function action_dest_64 () : pcf.api.Destination {
      return pcf.InvoiceItemCommentsPopup.createDestination(invoiceItemView.InvoiceItem, agencyBillStatementView)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Gross_Cell) at PolicyActivityLV.itemlevel.pcf: line 73, column 59
    function currency_52 () : typekey.Currency {
      return invoiceItemView.InvoiceItem.Currency
    }
    
    // 'iconColor' attribute on BooleanRadioCell (id=ExceptionFlag_Cell) at PolicyActivityLV.itemlevel.pcf: line 34, column 60
    function iconColor_24 () : gw.api.web.color.GWColor {
      return gw.web.util.BCIconHelper.getAgencyBillPaymentExceptionIconColor(invoiceItemView.InvoiceItem)
    }
    
    // 'iconLabel' attribute on BooleanRadioCell (id=ExceptionFlag_Cell) at PolicyActivityLV.itemlevel.pcf: line 34, column 60
    function iconLabel_20 () : java.lang.String {
      return gw.web.util.BCIconHelper.getAgencyBillPaymentExceptionIconLabel(invoiceItemView.InvoiceItem)
    }
    
    // 'icon' attribute on BooleanRadioCell (id=ExceptionFlag_Cell) at PolicyActivityLV.itemlevel.pcf: line 34, column 60
    function icon_23 () : java.lang.String {
      return gw.web.util.BCIconHelper.getAgencyBillPaymentExceptionIcon(invoiceItemView.InvoiceItem)
    }
    
    // 'outputConversion' attribute on TextCell (id=CommissionPercent_Cell) at PolicyActivityLV.itemlevel.pcf: line 87, column 45
    function outputConversion_57 (VALUE :  java.math.BigDecimal) : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(VALUE, 3)
    }
    
    // 'outputConversion' attribute on TextCell (id=Comments_Cell) at PolicyActivityLV.itemlevel.pcf: line 100, column 56
    function outputConversion_65 (VALUE :  java.lang.String) : java.lang.String {
      return VALUE == null ? "..." : VALUE.summarize(20)
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at PolicyActivityLV.itemlevel.pcf: line 26, column 74
    function valueRoot_18 () : java.lang.Object {
      return invoiceItemView.InvoiceItem.PolicyPeriod
    }
    
    // 'value' attribute on BooleanRadioCell (id=ExceptionFlag_Cell) at PolicyActivityLV.itemlevel.pcf: line 34, column 60
    function valueRoot_22 () : java.lang.Object {
      return invoiceItemView
    }
    
    // 'value' attribute on DateCell (id=StatementDate_Cell) at PolicyActivityLV.itemlevel.pcf: line 54, column 54
    function valueRoot_39 () : java.lang.Object {
      return invoiceItemView.Invoice
    }
    
    // 'value' attribute on TextCell (id=ItemContext_Cell) at PolicyActivityLV.itemlevel.pcf: line 65, column 50
    function valueRoot_47 () : java.lang.Object {
      return invoiceItemView.InvoiceItem.Charge
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at PolicyActivityLV.itemlevel.pcf: line 100, column 56
    function valueRoot_67 () : java.lang.Object {
      return invoiceItemView.InvoiceItem
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at PolicyActivityLV.itemlevel.pcf: line 26, column 74
    function value_17 () : java.lang.String {
      return invoiceItemView.InvoiceItem.PolicyPeriod.PolicyNumber
    }
    
    // 'value' attribute on BooleanRadioCell (id=ExceptionFlag_Cell) at PolicyActivityLV.itemlevel.pcf: line 34, column 60
    function value_21 () : java.lang.Boolean {
      return invoiceItemView.HasBeenPaymentException
    }
    
    // 'value' attribute on TextCell (id=LineNumber_Cell) at PolicyActivityLV.itemlevel.pcf: line 39, column 42
    function value_26 () : java.lang.Integer {
      return invoiceItemView.LineNumber
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at PolicyActivityLV.itemlevel.pcf: line 45, column 52
    function value_32 () : java.lang.String {
      return invoiceItemView.StatementNumber
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at PolicyActivityLV.itemlevel.pcf: line 49, column 43
    function value_35 () : java.lang.String {
      return invoiceItemView.Status
    }
    
    // 'value' attribute on DateCell (id=StatementDate_Cell) at PolicyActivityLV.itemlevel.pcf: line 54, column 54
    function value_38 () : java.util.Date {
      return invoiceItemView.Invoice.EventDate
    }
    
    // 'value' attribute on TextCell (id=ItemDescription_Cell) at PolicyActivityLV.itemlevel.pcf: line 60, column 25
    function value_43 () : java.lang.String {
      return invoiceItemView.ItemDescription
    }
    
    // 'value' attribute on TextCell (id=ItemContext_Cell) at PolicyActivityLV.itemlevel.pcf: line 65, column 50
    function value_46 () : entity.BillingInstruction {
      return invoiceItemView.InvoiceItem.Charge.BillingInstruction
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Gross_Cell) at PolicyActivityLV.itemlevel.pcf: line 73, column 59
    function value_51 () : gw.pl.currency.MonetaryAmount {
      return invoiceItemView.getGross( viewOption )
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionAmount_Cell) at PolicyActivityLV.itemlevel.pcf: line 80, column 64
    function value_54 () : gw.pl.currency.MonetaryAmount {
      return invoiceItemView.getCommission( viewOption )
    }
    
    // 'value' attribute on TextCell (id=CommissionPercent_Cell) at PolicyActivityLV.itemlevel.pcf: line 87, column 45
    function value_58 () : java.math.BigDecimal {
      return invoiceItemView.getCommissionPercentAsBigDecimal( viewOption )
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetAmount_Cell) at PolicyActivityLV.itemlevel.pcf: line 94, column 57
    function value_60 () : gw.pl.currency.MonetaryAmount {
      return invoiceItemView.getNet( viewOption )
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at PolicyActivityLV.itemlevel.pcf: line 100, column 56
    function value_66 () : java.lang.String {
      return invoiceItemView.InvoiceItem.Comments
    }
    
    // 'value' attribute on TextCell (id=InsuredName_Cell) at PolicyActivityLV.itemlevel.pcf: line 106, column 48
    function value_69 () : java.lang.String {
      return invoiceItemView.InsuredName
    }
    
    property get invoiceItemView () : gw.api.web.invoice.AgencyBillInvoiceItemView {
      return getIteratedValue(1) as gw.api.web.invoice.AgencyBillInvoiceItemView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/PolicyActivityLV.itemlevel.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyActivityLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at PolicyActivityLV.itemlevel.pcf: line 26, column 74
    function sortValue_0 (invoiceItemView :  gw.api.web.invoice.AgencyBillInvoiceItemView) : java.lang.Object {
      return invoiceItemView.InvoiceItem.PolicyPeriod.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=LineNumber_Cell) at PolicyActivityLV.itemlevel.pcf: line 39, column 42
    function sortValue_1 (invoiceItemView :  gw.api.web.invoice.AgencyBillInvoiceItemView) : java.lang.Object {
      return invoiceItemView.LineNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetAmount_Cell) at PolicyActivityLV.itemlevel.pcf: line 94, column 57
    function sortValue_10 (invoiceItemView :  gw.api.web.invoice.AgencyBillInvoiceItemView) : java.lang.Object {
      return invoiceItemView.getNet( viewOption )
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at PolicyActivityLV.itemlevel.pcf: line 100, column 56
    function sortValue_11 (invoiceItemView :  gw.api.web.invoice.AgencyBillInvoiceItemView) : java.lang.Object {
      return invoiceItemView.InvoiceItem.Comments
    }
    
    // 'value' attribute on TextCell (id=InsuredName_Cell) at PolicyActivityLV.itemlevel.pcf: line 106, column 48
    function sortValue_12 (invoiceItemView :  gw.api.web.invoice.AgencyBillInvoiceItemView) : java.lang.Object {
      return invoiceItemView.InsuredName
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at PolicyActivityLV.itemlevel.pcf: line 45, column 52
    function sortValue_2 (invoiceItemView :  gw.api.web.invoice.AgencyBillInvoiceItemView) : java.lang.Object {
      return invoiceItemView.StatementNumber
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at PolicyActivityLV.itemlevel.pcf: line 49, column 43
    function sortValue_3 (invoiceItemView :  gw.api.web.invoice.AgencyBillInvoiceItemView) : java.lang.Object {
      return invoiceItemView.Status
    }
    
    // 'value' attribute on DateCell (id=StatementDate_Cell) at PolicyActivityLV.itemlevel.pcf: line 54, column 54
    function sortValue_4 (invoiceItemView :  gw.api.web.invoice.AgencyBillInvoiceItemView) : java.lang.Object {
      return invoiceItemView.Invoice.EventDate
    }
    
    // 'value' attribute on TextCell (id=ItemDescription_Cell) at PolicyActivityLV.itemlevel.pcf: line 60, column 25
    function sortValue_5 (invoiceItemView :  gw.api.web.invoice.AgencyBillInvoiceItemView) : java.lang.Object {
      return invoiceItemView.ItemDescription
    }
    
    // 'value' attribute on TextCell (id=ItemContext_Cell) at PolicyActivityLV.itemlevel.pcf: line 65, column 50
    function sortValue_6 (invoiceItemView :  gw.api.web.invoice.AgencyBillInvoiceItemView) : java.lang.Object {
      return invoiceItemView.InvoiceItem.Charge.BillingInstruction
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Gross_Cell) at PolicyActivityLV.itemlevel.pcf: line 73, column 59
    function sortValue_7 (invoiceItemView :  gw.api.web.invoice.AgencyBillInvoiceItemView) : java.lang.Object {
      return invoiceItemView.getGross( viewOption )
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionAmount_Cell) at PolicyActivityLV.itemlevel.pcf: line 80, column 64
    function sortValue_8 (invoiceItemView :  gw.api.web.invoice.AgencyBillInvoiceItemView) : java.lang.Object {
      return invoiceItemView.getCommission( viewOption )
    }
    
    // 'value' attribute on TextCell (id=CommissionPercent_Cell) at PolicyActivityLV.itemlevel.pcf: line 87, column 45
    function sortValue_9 (invoiceItemView :  gw.api.web.invoice.AgencyBillInvoiceItemView) : java.lang.Object {
      return invoiceItemView.getCommissionPercentAsBigDecimal( viewOption )
    }
    
    // 'footerSumValue' attribute on RowIterator at PolicyActivityLV.itemlevel.pcf: line 73, column 59
    function sumValue_13 (invoiceItemView :  gw.api.web.invoice.AgencyBillInvoiceItemView) : java.lang.Object {
      return invoiceItemView.getGross( viewOption )
    }
    
    // 'footerSumValue' attribute on RowIterator at PolicyActivityLV.itemlevel.pcf: line 80, column 64
    function sumValue_14 (invoiceItemView :  gw.api.web.invoice.AgencyBillInvoiceItemView) : java.lang.Object {
      return invoiceItemView.getCommission( viewOption )
    }
    
    // 'footerSumValue' attribute on RowIterator at PolicyActivityLV.itemlevel.pcf: line 94, column 57
    function sumValue_15 (invoiceItemView :  gw.api.web.invoice.AgencyBillInvoiceItemView) : java.lang.Object {
      return invoiceItemView.getNet( viewOption )
    }
    
    // 'value' attribute on RowIterator at PolicyActivityLV.itemlevel.pcf: line 18, column 66
    function value_72 () : gw.api.web.invoice.AgencyBillInvoiceItemView[] {
      return getInvoiceItemViews()
    }
    
    property get agencyBillStatementView () : gw.api.web.invoice.AgencyBillStatementView {
      return getRequireValue("agencyBillStatementView", 0) as gw.api.web.invoice.AgencyBillStatementView
    }
    
    property set agencyBillStatementView ($arg :  gw.api.web.invoice.AgencyBillStatementView) {
      setRequireValue("agencyBillStatementView", 0, $arg)
    }
    
    property get viewOption () : InvoiceItemViewOption {
      return getRequireValue("viewOption", 0) as InvoiceItemViewOption
    }
    
    property set viewOption ($arg :  InvoiceItemViewOption) {
      setRequireValue("viewOption", 0, $arg)
    }
    
    
    function getInvoiceItemViews() : AgencyBillInvoiceItemView[] {
     if (agencyBillStatementView == null) {
       return null
     } else {
       return agencyBillStatementView.getPolicyPeriodInvoiceItemsFilteredAsArray(viewOption).flatMap( \ policyPeriodInfo ->  policyPeriodInfo.FilteredInvoiceItemViewsArray)
     }
    }
    
    function pushPolicyPeriodDetails(policyPeriod : PolicyPeriod) {
      if (policyPeriod.AgencyBill) {
        AgencyBillPolicyDetailsPopup.push(policyPeriod)
      } else {
        pcf.PolicySummary.push(policyPeriod)
      }
    }
    
    
  }
  
  
}