package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.lang.reflect.IType
uses gw.pl.currency.MonetaryAmount
uses gw.api.domain.accounting.ChargePatternKey
@javax.annotation.Generated("config/web/pcf/transaction/RecaptureDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class RecaptureDetailsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/RecaptureDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RecaptureDetailsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at RecaptureDetailsScreen.pcf: line 44, column 58
    function currency_14 () : typekey.Currency {
      return account.UnappliedAmount.Currency
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at RecaptureDetailsScreen.pcf: line 59, column 61
    function currency_29 () : typekey.Currency {
      return helper.RecaptureChargeInitializer.Amount.Currency
    }
    
    // 'value' attribute on RangeInput (id=Type_Input) at RecaptureDetailsScreen.pcf: line 52, column 45
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      helper.RecaptureChargeInitializer.ChargePattern = (__VALUE_TO_SET as entity.ChargePattern)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at RecaptureDetailsScreen.pcf: line 59, column 61
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      helper.RecaptureChargeInitializer.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on RangeInput (id=UnappliedFund_Input) at RecaptureDetailsScreen.pcf: line 35, column 109
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      helper.RecaptureUnappliedFund = (__VALUE_TO_SET as entity.UnappliedFund)
    }
    
    // 'editable' attribute on RangeInput (id=Type_Input) at RecaptureDetailsScreen.pcf: line 52, column 45
    function editable_16 () : java.lang.Boolean {
      return chargePatternHelper.getChargePatterns(helper.RecaptureChargeInitializer.TAccountOwner, gw.transaction.UserTransactionType.RECAPTURE).Count > 1
    }
    
    // 'initialValue' attribute on Variable at RecaptureDetailsScreen.pcf: line 16, column 50
    function initialValue_0 () : gw.transaction.ChargePatternHelper {
      return new gw.transaction.ChargePatternHelper()
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=Amount_Input) at RecaptureDetailsScreen.pcf: line 59, column 61
    function validationExpression_25 () : java.lang.Object {
      return validateChargeAmount()
    }
    
    // 'valueRange' attribute on RangeInput (id=Type_Input) at RecaptureDetailsScreen.pcf: line 52, column 45
    function valueRange_20 () : java.lang.Object {
      return chargePatternHelper.getChargePatterns(helper.RecaptureChargeInitializer.TAccountOwner, gw.transaction.UserTransactionType.RECAPTURE)
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFund_Input) at RecaptureDetailsScreen.pcf: line 35, column 109
    function valueRange_7 () : java.lang.Object {
      return account.UnappliedFundsSortedByDisplayName
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at RecaptureDetailsScreen.pcf: line 44, column 58
    function valueRoot_13 () : java.lang.Object {
      return helper.RecaptureUnappliedFund
    }
    
    // 'value' attribute on RangeInput (id=Type_Input) at RecaptureDetailsScreen.pcf: line 52, column 45
    function valueRoot_19 () : java.lang.Object {
      return helper.RecaptureChargeInitializer
    }
    
    // 'value' attribute on RangeInput (id=UnappliedFund_Input) at RecaptureDetailsScreen.pcf: line 35, column 109
    function valueRoot_6 () : java.lang.Object {
      return helper
    }
    
    // 'value' attribute on TextInput (id=Account_Input) at RecaptureDetailsScreen.pcf: line 26, column 39
    function value_1 () : entity.Account {
      return account
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at RecaptureDetailsScreen.pcf: line 44, column 58
    function value_12 () : gw.pl.currency.MonetaryAmount {
      return helper.RecaptureUnappliedFund.Balance
    }
    
    // 'value' attribute on RangeInput (id=Type_Input) at RecaptureDetailsScreen.pcf: line 52, column 45
    function value_17 () : entity.ChargePattern {
      return helper.RecaptureChargeInitializer.ChargePattern
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at RecaptureDetailsScreen.pcf: line 59, column 61
    function value_26 () : gw.pl.currency.MonetaryAmount {
      return helper.RecaptureChargeInitializer.Amount
    }
    
    // 'value' attribute on RangeInput (id=UnappliedFund_Input) at RecaptureDetailsScreen.pcf: line 35, column 109
    function value_4 () : entity.UnappliedFund {
      return helper.RecaptureUnappliedFund
    }
    
    // 'valueRange' attribute on RangeInput (id=Type_Input) at RecaptureDetailsScreen.pcf: line 52, column 45
    function verifyValueRangeIsAllowedType_21 ($$arg :  entity.ChargePattern[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Type_Input) at RecaptureDetailsScreen.pcf: line 52, column 45
    function verifyValueRangeIsAllowedType_21 ($$arg :  gw.api.database.IQueryBeanResult<entity.ChargePattern>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Type_Input) at RecaptureDetailsScreen.pcf: line 52, column 45
    function verifyValueRangeIsAllowedType_21 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFund_Input) at RecaptureDetailsScreen.pcf: line 35, column 109
    function verifyValueRangeIsAllowedType_8 ($$arg :  entity.UnappliedFund[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFund_Input) at RecaptureDetailsScreen.pcf: line 35, column 109
    function verifyValueRangeIsAllowedType_8 ($$arg :  gw.api.database.IQueryBeanResult<entity.UnappliedFund>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFund_Input) at RecaptureDetailsScreen.pcf: line 35, column 109
    function verifyValueRangeIsAllowedType_8 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Type_Input) at RecaptureDetailsScreen.pcf: line 52, column 45
    function verifyValueRange_22 () : void {
      var __valueRangeArg = chargePatternHelper.getChargePatterns(helper.RecaptureChargeInitializer.TAccountOwner, gw.transaction.UserTransactionType.RECAPTURE)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_21(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFund_Input) at RecaptureDetailsScreen.pcf: line 35, column 109
    function verifyValueRange_9 () : void {
      var __valueRangeArg = account.UnappliedFundsSortedByDisplayName
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_8(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=UnappliedFund_Input) at RecaptureDetailsScreen.pcf: line 35, column 109
    function visible_3 () : java.lang.Boolean {
      return helper.BillingInstruction.ChargeInitializers[0].OwnerAccount.HasDesignatedUnappliedFund
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get chargePatternHelper () : gw.transaction.ChargePatternHelper {
      return getVariableValue("chargePatternHelper", 0) as gw.transaction.ChargePatternHelper
    }
    
    property set chargePatternHelper ($arg :  gw.transaction.ChargePatternHelper) {
      setVariableValue("chargePatternHelper", 0, $arg)
    }
    
    property get helper () : gw.accounting.NewRecaptureChargeHelper {
      return getRequireValue("helper", 0) as gw.accounting.NewRecaptureChargeHelper
    }
    
    property set helper ($arg :  gw.accounting.NewRecaptureChargeHelper) {
      setRequireValue("helper", 0, $arg)
    }
    
    
    
                    function validateChargeAmount(): String {
                        var currentAmount = helper.RecaptureChargeInitializer.Amount
                        if (!currentAmount.IsPositive) {
                            return DisplayKey.get("Web.ChargeDetailsScreen.RecaptureChargeInvalidAmount")
                        }
                        if (!helper.RecaptureUnappliedFund.Balance.IsNegative) {
                            return DisplayKey.get("Web.ChargeDetailsScreen.UnappliedNotNegativeForRecaptureCharge")
                        }
                        return ((currentAmount <= helper.RecaptureUnappliedFund.Balance.abs()) ? null : DisplayKey.get("Web.ChargeDetailsScreen.RecaptureChargeLargerNegUnapplied"))
                    }
            
    
    
  }
  
  
}