package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyDistItemsReadOnlyCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyDistItemsReadOnlyCVExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyDistItemsReadOnlyCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyDistItemsReadOnlyCVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=Statement_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 35, column 56
    function sortValue_0 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (agencyDistItem.InvoiceItem)
return invoiceItem.Invoice.EventDate
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 45, column 31
    function sortValue_1 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (agencyDistItem.InvoiceItem)
return invoiceItem.PolicyPeriod.PolicyNumberLong
    }
    
    // 'value' attribute on TextCell (id=OwnerAccount_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 54, column 31
    function sortValue_2 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (agencyDistItem.InvoiceItem)
return invoiceItem.Owner.AccountNameLocalized
    }
    
    // 'value' attribute on TextCell (id=ItemDescription_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 61, column 31
    function sortValue_3 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (agencyDistItem.InvoiceItem)
return invoiceItem.DisplayNameAsItemType
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Difference_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 96, column 234
    function sortValue_4 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (agencyDistItem.InvoiceItem)
return invoiceItem.Amount.subtract(agencyDistItem.ItemCommissionToTarget == null ? 0bd.ofCurrency(agencyDistItem.Currency) : agencyDistItem.ItemCommissionToTarget.CommissionAmount) - agencyDistItem.NetAmountToApply
    }
    
    // 'value' attribute on TextCell (id=LineItemNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 136, column 48
    function sortValue_46 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (agencyDistItem.InvoiceItem)
return invoiceItem.LineItemNumber
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 143, column 60
    function sortValue_47 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (agencyDistItem.InvoiceItem)
return invoiceItem.Invoice.InvoiceNumber
    }
    
    // 'value' attribute on DateCell (id=Statement_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 147, column 56
    function sortValue_48 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (agencyDistItem.InvoiceItem)
return invoiceItem.Invoice.EventDate
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 155, column 31
    function sortValue_49 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (agencyDistItem.InvoiceItem)
return invoiceItem.PolicyPeriod.PolicyNumberLong
    }
    
    // 'sortBy' attribute on TextCell (id=Account_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 162, column 31
    function sortValue_50 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (agencyDistItem.InvoiceItem)
return invoiceItem.Owner.AccountName
    }
    
    // 'value' attribute on TextCell (id=ItemDescription_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 168, column 31
    function sortValue_51 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (agencyDistItem.InvoiceItem)
return invoiceItem.DisplayNameAsItemType
    }
    
    // 'value' attribute on TextCell (id=Context_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 173, column 56
    function sortValue_52 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (agencyDistItem.InvoiceItem)
return invoiceItem.Charge.BillingInstruction
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Difference_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 235, column 234
    function sortValue_53 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (agencyDistItem.InvoiceItem)
return invoiceItem.Amount.subtract(agencyDistItem.ItemCommissionToTarget == null ? 0bd.ofCurrency(agencyDistItem.Currency) : agencyDistItem.ItemCommissionToTarget.CommissionAmount) - agencyDistItem.NetAmountToApply
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 239, column 57
    function sortValue_54 (agencyDistItem :  entity.BaseDistItem) : java.lang.Object {
      return agencyDistItem.PaymentComments
    }
    
    // 'value' attribute on RowIterator (id=AgencyDistItemsLV) at AgencyDistItemsReadOnlyCV.pcf: line 25, column 47
    function value_45 () : entity.BaseDistItem[] {
      return agencyCycleDist.DistItems
    }
    
    property get agencyCycleDist () : AgencyCycleDist {
      return getRequireValue("agencyCycleDist", 0) as AgencyCycleDist
    }
    
    property set agencyCycleDist ($arg :  AgencyCycleDist) {
      setRequireValue("agencyCycleDist", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyDistItemsReadOnlyCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends AgencyDistItemsReadOnlyCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 143, column 60
    function actionAvailable_66 () : java.lang.Boolean {
      return invoiceItem.Invoice typeis StatementInvoice
    }
    
    // 'actionAvailable' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 155, column 31
    function actionAvailable_75 () : java.lang.Boolean {
      return invoiceItem.PolicyPeriod != null
    }
    
    // 'action' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 143, column 60
    function action_65 () : void {
      invoiceItem.StatementInvoiceDetailViewAction()
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 155, column 31
    function action_73 () : void {
      PolicyDetailSummaryPopup.push(invoiceItem.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=Account_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 162, column 31
    function action_79 () : void {
      AccountSummaryPopup.push(invoiceItem.Owner)
    }
    
    // 'action' attribute on TextCell (id=ItemDescription_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 168, column 31
    function action_84 () : void {
      InvoiceItemHistoryPopup.push(invoiceItem)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 155, column 31
    function action_dest_74 () : pcf.api.Destination {
      return pcf.PolicyDetailSummaryPopup.createDestination(invoiceItem.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=Account_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 162, column 31
    function action_dest_80 () : pcf.api.Destination {
      return pcf.AccountSummaryPopup.createDestination(invoiceItem.Owner)
    }
    
    // 'action' attribute on TextCell (id=ItemDescription_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 168, column 31
    function action_dest_85 () : pcf.api.Destination {
      return pcf.InvoiceItemHistoryPopup.createDestination(invoiceItem)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=GrossOwed_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 180, column 45
    function currency_94 () : typekey.Currency {
      return agencyDistItem.Currency
    }
    
    // 'editable' attribute on Row at AgencyDistItemsReadOnlyCV.pcf: line 122, column 85
    function editable_124 () : java.lang.Boolean {
      return invoiceItem.Type != InvoiceItemType.TC_COMMISSIONREMAINDER
    }
    
    // 'iconColor' attribute on BooleanRadioCell (id=ExceptionFlag_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 130, column 62
    function iconColor_60 () : gw.api.web.color.GWColor {
      return gw.web.util.BCIconHelper.getAgencyBillPaymentExceptionIconColor(invoiceItem)
    }
    
    // 'iconLabel' attribute on BooleanRadioCell (id=ExceptionFlag_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 130, column 62
    function iconLabel_56 () : java.lang.String {
      return gw.web.util.BCIconHelper.getAgencyBillPaymentExceptionIconLabel(invoiceItem)
    }
    
    // 'icon' attribute on BooleanRadioCell (id=ExceptionFlag_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 130, column 62
    function icon_59 () : java.lang.String {
      return gw.web.util.BCIconHelper.getAgencyBillPaymentExceptionIcon(invoiceItem)
    }
    
    // 'initialValue' attribute on Variable at AgencyDistItemsReadOnlyCV.pcf: line 120, column 35
    function initialValue_55 () : InvoiceItem {
      return agencyDistItem.InvoiceItem
    }
    
    // RowIterator (id=AgencyDistItemsLV) at AgencyDistItemsReadOnlyCV.pcf: line 116, column 47
    function initializeVariables_125 () : void {
        invoiceItem = agencyDistItem.InvoiceItem;

    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossToApply_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 208, column 60
    function valueRoot_105 () : java.lang.Object {
      return agencyDistItem
    }
    
    // 'value' attribute on BooleanRadioCell (id=ExceptionFlag_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 130, column 62
    function valueRoot_58 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 143, column 60
    function valueRoot_68 () : java.lang.Object {
      return invoiceItem.Invoice
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 155, column 31
    function valueRoot_77 () : java.lang.Object {
      return invoiceItem.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=Account_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 162, column 31
    function valueRoot_82 () : java.lang.Object {
      return invoiceItem.Owner
    }
    
    // 'value' attribute on TextCell (id=Context_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 173, column 56
    function valueRoot_90 () : java.lang.Object {
      return invoiceItem.Charge
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetOwed_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 201, column 200
    function value_101 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.Amount.subtract(agencyDistItem.ItemCommissionToTarget == null ? 0bd.ofCurrency(agencyDistItem.Currency) : agencyDistItem.ItemCommissionToTarget.CommissionAmount)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossToApply_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 208, column 60
    function value_104 () : gw.pl.currency.MonetaryAmount {
      return agencyDistItem.GrossAmountToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionToApply_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 215, column 65
    function value_108 () : gw.pl.currency.MonetaryAmount {
      return agencyDistItem.CommissionAmountToApply
    }
    
    // 'value' attribute on TextCell (id=PercentToApply_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 222, column 51
    function value_112 () : java.math.BigDecimal {
      return gw.agencybill.CommissionCalculator.getCommissionPercentToApply(agencyDistItem)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetApplied_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 229, column 58
    function value_114 () : gw.pl.currency.MonetaryAmount {
      return agencyDistItem.NetAmountToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Difference_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 235, column 234
    function value_118 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.Amount.subtract(agencyDistItem.ItemCommissionToTarget == null ? 0bd.ofCurrency(agencyDistItem.Currency) : agencyDistItem.ItemCommissionToTarget.CommissionAmount) - agencyDistItem.NetAmountToApply
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 239, column 57
    function value_121 () : java.lang.String {
      return agencyDistItem.PaymentComments
    }
    
    // 'value' attribute on BooleanRadioCell (id=ExceptionFlag_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 130, column 62
    function value_57 () : java.lang.Boolean {
      return invoiceItem.HasBeenPaymentException
    }
    
    // 'value' attribute on TextCell (id=LineItemNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 136, column 48
    function value_62 () : java.lang.Integer {
      return invoiceItem.LineItemNumber
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 143, column 60
    function value_67 () : java.lang.String {
      return invoiceItem.Invoice.InvoiceNumber
    }
    
    // 'value' attribute on DateCell (id=Statement_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 147, column 56
    function value_70 () : java.util.Date {
      return invoiceItem.Invoice.EventDate
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 155, column 31
    function value_76 () : java.lang.String {
      return invoiceItem.PolicyPeriod.PolicyNumberLong
    }
    
    // 'value' attribute on TextCell (id=Account_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 162, column 31
    function value_81 () : java.lang.String {
      return invoiceItem.Owner.AccountNameLocalized
    }
    
    // 'value' attribute on TextCell (id=ItemDescription_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 168, column 31
    function value_86 () : java.lang.String {
      return invoiceItem.DisplayNameAsItemType
    }
    
    // 'value' attribute on TextCell (id=Context_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 173, column 56
    function value_89 () : entity.BillingInstruction {
      return invoiceItem.Charge.BillingInstruction
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossOwed_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 180, column 45
    function value_92 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionOwed_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 187, column 171
    function value_96 () : gw.pl.currency.MonetaryAmount {
      return agencyDistItem.ItemCommissionToTarget == null ? 0bd.ofCurrency(agencyDistItem.Currency) : agencyDistItem.ItemCommissionToTarget.CommissionAmount
    }
    
    // 'value' attribute on TextCell (id=PercentOwed_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 194, column 51
    function value_99 () : java.math.BigDecimal {
      return gw.agencybill.CommissionCalculator.getCommissionPercent(agencyDistItem)
    }
    
    property get agencyDistItem () : entity.BaseDistItem {
      return getIteratedValue(1) as entity.BaseDistItem
    }
    
    property get invoiceItem () : InvoiceItem {
      return getVariableValue("invoiceItem", 1) as InvoiceItem
    }
    
    property set invoiceItem ($arg :  InvoiceItem) {
      setVariableValue("invoiceItem", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyDistItemsReadOnlyCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AgencyDistItemsReadOnlyCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 45, column 31
    function actionAvailable_11 () : java.lang.Boolean {
      return invoiceItem.PolicyPeriod != null
    }
    
    // 'actionAvailable' attribute on TextCell (id=OwnerAccount_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 54, column 31
    function actionAvailable_17 () : java.lang.Boolean {
      return invoiceItem.Owner != null
    }
    
    // 'action' attribute on TextCell (id=OwnerAccount_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 54, column 31
    function action_15 () : void {
      AccountSummaryPopup.push(invoiceItem.Owner)
    }
    
    // 'action' attribute on TextCell (id=ItemDescription_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 61, column 31
    function action_21 () : void {
      InvoiceItemHistoryPopup.push(invoiceItem)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 45, column 31
    function action_9 () : void {
      PolicyDetailSummaryPopup.push(invoiceItem.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 45, column 31
    function action_dest_10 () : pcf.api.Destination {
      return pcf.PolicyDetailSummaryPopup.createDestination(invoiceItem.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=OwnerAccount_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 54, column 31
    function action_dest_16 () : pcf.api.Destination {
      return pcf.AccountSummaryPopup.createDestination(invoiceItem.Owner)
    }
    
    // 'action' attribute on TextCell (id=ItemDescription_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 61, column 31
    function action_dest_22 () : pcf.api.Destination {
      return pcf.InvoiceItemHistoryPopup.createDestination(invoiceItem)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=GrossToApply_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 68, column 60
    function currency_28 () : typekey.Currency {
      return agencyDistItem.Currency
    }
    
    // 'editable' attribute on Row at AgencyDistItemsReadOnlyCV.pcf: line 31, column 85
    function editable_43 () : java.lang.Boolean {
      return invoiceItem.Type != InvoiceItemType.TC_COMMISSIONREMAINDER
    }
    
    // 'initialValue' attribute on Variable at AgencyDistItemsReadOnlyCV.pcf: line 29, column 35
    function initialValue_5 () : InvoiceItem {
      return agencyDistItem.InvoiceItem
    }
    
    // RowIterator (id=AgencyDistItemsLV) at AgencyDistItemsReadOnlyCV.pcf: line 25, column 47
    function initializeVariables_44 () : void {
        invoiceItem = agencyDistItem.InvoiceItem;

    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 45, column 31
    function valueRoot_13 () : java.lang.Object {
      return invoiceItem.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=OwnerAccount_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 54, column 31
    function valueRoot_19 () : java.lang.Object {
      return invoiceItem.Owner
    }
    
    // 'value' attribute on TextCell (id=ItemDescription_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 61, column 31
    function valueRoot_24 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossToApply_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 68, column 60
    function valueRoot_27 () : java.lang.Object {
      return agencyDistItem
    }
    
    // 'value' attribute on DateCell (id=Statement_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 35, column 56
    function valueRoot_7 () : java.lang.Object {
      return invoiceItem.Invoice
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 45, column 31
    function value_12 () : java.lang.String {
      return invoiceItem.PolicyPeriod.PolicyNumberLong
    }
    
    // 'value' attribute on TextCell (id=OwnerAccount_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 54, column 31
    function value_18 () : java.lang.String {
      return invoiceItem.Owner.AccountNameLocalized
    }
    
    // 'value' attribute on TextCell (id=ItemDescription_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 61, column 31
    function value_23 () : java.lang.String {
      return invoiceItem.DisplayNameAsItemType
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossToApply_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 68, column 60
    function value_26 () : gw.pl.currency.MonetaryAmount {
      return agencyDistItem.GrossAmountToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionToApply_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 75, column 65
    function value_30 () : gw.pl.currency.MonetaryAmount {
      return agencyDistItem.CommissionAmountToApply
    }
    
    // 'value' attribute on TextCell (id=PercentToApply_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 83, column 51
    function value_34 () : java.math.BigDecimal {
      return gw.agencybill.CommissionCalculator.getCommissionPercentToApply(agencyDistItem)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetApplied_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 90, column 58
    function value_36 () : gw.pl.currency.MonetaryAmount {
      return agencyDistItem.NetAmountToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Difference_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 96, column 234
    function value_40 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.Amount.subtract(agencyDistItem.ItemCommissionToTarget == null ? 0bd.ofCurrency(agencyDistItem.Currency) : agencyDistItem.ItemCommissionToTarget.CommissionAmount) - agencyDistItem.NetAmountToApply
    }
    
    // 'value' attribute on DateCell (id=Statement_Cell) at AgencyDistItemsReadOnlyCV.pcf: line 35, column 56
    function value_6 () : java.util.Date {
      return invoiceItem.Invoice.EventDate
    }
    
    property get agencyDistItem () : entity.BaseDistItem {
      return getIteratedValue(1) as entity.BaseDistItem
    }
    
    property get invoiceItem () : InvoiceItem {
      return getVariableValue("invoiceItem", 1) as InvoiceItem
    }
    
    property set invoiceItem ($arg :  InvoiceItem) {
      setVariableValue("invoiceItem", 1, $arg)
    }
    
    
  }
  
  
}