package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/InvoiceSearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceSearchResultsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/InvoiceSearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceSearchResultsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at InvoiceSearchResultsLV.pcf: line 46, column 48
    function sortValue_1 (invoiceSearchView :  entity.InvoiceSearchView) : java.lang.Object {
      return invoiceSearchView.EventDate
    }
    
    // 'value' attribute on DateCell (id=PaymentDueDate_Cell) at InvoiceSearchResultsLV.pcf: line 50, column 53
    function sortValue_2 (invoiceSearchView :  entity.InvoiceSearchView) : java.lang.Object {
      return invoiceSearchView.PaymentDueDate
    }
    
    // 'value' attribute on TextCell (id=InvoiceNumber_Cell) at InvoiceSearchResultsLV.pcf: line 58, column 25
    function sortValue_3 (invoiceSearchView :  entity.InvoiceSearchView) : java.lang.Object {
      return invoiceSearchView.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at InvoiceSearchResultsLV.pcf: line 64, column 25
    function sortValue_4 (invoiceSearchView :  entity.InvoiceSearchView) : java.lang.Object {
      return invoiceSearchView.AccountNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at InvoiceSearchResultsLV.pcf: line 70, column 46
    function sortValue_5 (invoiceSearchView :  entity.InvoiceSearchView) : java.lang.Object {
      return invoiceSearchView.Status
    }
    
    // 'value' attribute on RowIterator at InvoiceSearchResultsLV.pcf: line 26, column 84
    function value_46 () : gw.api.database.IQueryBeanResult<entity.InvoiceSearchView> {
      return invoiceSearchViews
    }
    
    // 'visible' attribute on LinkCell (id=SelectCell) at InvoiceSearchResultsLV.pcf: line 35, column 30
    function visible_0 () : java.lang.Boolean {
      return isWizard
    }
    
    property get invoiceSearchViews () : gw.api.database.IQueryBeanResult<InvoiceSearchView> {
      return getRequireValue("invoiceSearchViews", 0) as gw.api.database.IQueryBeanResult<InvoiceSearchView>
    }
    
    property set invoiceSearchViews ($arg :  gw.api.database.IQueryBeanResult<InvoiceSearchView>) {
      setRequireValue("invoiceSearchViews", 0, $arg)
    }
    
    property get isWizard () : boolean {
      return getRequireValue("isWizard", 0) as java.lang.Boolean
    }
    
    property set isWizard ($arg :  boolean) {
      setRequireValue("isWizard", 0, $arg)
    }
    
    property get showHyperlinks () : boolean {
      return getRequireValue("showHyperlinks", 0) as java.lang.Boolean
    }
    
    property set showHyperlinks ($arg :  boolean) {
      setRequireValue("showHyperlinks", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/InvoiceSearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends InvoiceSearchResultsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=InvoiceNumber_Cell) at InvoiceSearchResultsLV.pcf: line 58, column 25
    function action_15 () : void {
      AccountDetailInvoices.go(invoiceSearchView.Account, invoiceSearchView.Invoice)
    }
    
    // 'action' attribute on Link (id=Select) at InvoiceSearchResultsLV.pcf: line 40, column 38
    function action_6 () : void {
      (CurrentLocation as pcf.api.Wizard).next()
    }
    
    // 'action' attribute on TextCell (id=InvoiceNumber_Cell) at InvoiceSearchResultsLV.pcf: line 58, column 25
    function action_dest_16 () : pcf.api.Destination {
      return pcf.AccountDetailInvoices.createDestination(invoiceSearchView.Account, invoiceSearchView.Invoice)
    }
    
    // 'available' attribute on TextCell (id=InvoiceNumber_Cell) at InvoiceSearchResultsLV.pcf: line 58, column 25
    function available_14 () : java.lang.Boolean {
      return showHyperlinks
    }
    
    // 'canPick' attribute on RowIterator at InvoiceSearchResultsLV.pcf: line 26, column 84
    function canPick_44 () : java.lang.Boolean {
      return !isWizard
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=BillAmount_Cell) at InvoiceSearchResultsLV.pcf: line 77, column 84
    function currency_30 () : typekey.Currency {
      return invoiceSearchView.Currency
    }
    
    // 'pickValue' attribute on RowIterator at InvoiceSearchResultsLV.pcf: line 26, column 84
    function pickValue_45 () : java.lang.Object {
      return gw.api.web.search.SearchPopupUtil.getInvoice(invoiceSearchView)
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at InvoiceSearchResultsLV.pcf: line 95, column 45
    function valueRoot_39 () : java.lang.Object {
      return invoiceSearchView.Invoice.InvoiceStream.Policy.LatestPolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=ProductLine_Cell) at InvoiceSearchResultsLV.pcf: line 101, column 40
    function valueRoot_42 () : java.lang.Object {
      return invoiceSearchView.Invoice.InvoiceStream.Policy
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at InvoiceSearchResultsLV.pcf: line 46, column 48
    function valueRoot_9 () : java.lang.Object {
      return invoiceSearchView
    }
    
    // 'value' attribute on DateCell (id=PaymentDueDate_Cell) at InvoiceSearchResultsLV.pcf: line 50, column 53
    function value_11 () : java.util.Date {
      return invoiceSearchView.PaymentDueDate
    }
    
    // 'value' attribute on TextCell (id=InvoiceNumber_Cell) at InvoiceSearchResultsLV.pcf: line 58, column 25
    function value_17 () : java.lang.String {
      return invoiceSearchView.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at InvoiceSearchResultsLV.pcf: line 64, column 25
    function value_21 () : java.lang.String {
      return invoiceSearchView.AccountNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at InvoiceSearchResultsLV.pcf: line 70, column 46
    function value_25 () : typekey.InvoiceStatus {
      return invoiceSearchView.Status
    }
    
    // 'value' attribute on MonetaryAmountCell (id=BillAmount_Cell) at InvoiceSearchResultsLV.pcf: line 77, column 84
    function value_29 () : gw.pl.currency.MonetaryAmount {
      return invoiceSearchView.Amount.ofCurrency(invoiceSearchView.Currency)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=DueAmount_Cell) at InvoiceSearchResultsLV.pcf: line 84, column 87
    function value_32 () : gw.pl.currency.MonetaryAmount {
      return invoiceSearchView.AmountDue.ofCurrency(invoiceSearchView.Currency)
    }
    
    // 'value' attribute on TextCell (id=AccountName_Cell) at InvoiceSearchResultsLV.pcf: line 89, column 50
    function value_35 () : java.lang.String {
      return invoiceSearchView.AccountName
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at InvoiceSearchResultsLV.pcf: line 95, column 45
    function value_38 () : typekey.Jurisdiction {
      return invoiceSearchView.Invoice.InvoiceStream.Policy.LatestPolicyPeriod.RiskJurisdiction
    }
    
    // 'value' attribute on TextCell (id=ProductLine_Cell) at InvoiceSearchResultsLV.pcf: line 101, column 40
    function value_41 () : typekey.LOBCode {
      return invoiceSearchView.Invoice.InvoiceStream.Policy.LOBCode
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at InvoiceSearchResultsLV.pcf: line 46, column 48
    function value_8 () : java.util.Date {
      return invoiceSearchView.EventDate
    }
    
    // 'visible' attribute on LinkCell (id=SelectCell) at InvoiceSearchResultsLV.pcf: line 35, column 30
    function visible_7 () : java.lang.Boolean {
      return isWizard
    }
    
    property get invoiceSearchView () : entity.InvoiceSearchView {
      return getIteratedValue(1) as entity.InvoiceSearchView
    }
    
    
  }
  
  
}