package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerCodesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerCodesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerCodesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ProducerCodesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=CommissionPlan_Cell) at ProducerCodesLV.pcf: line 60, column 45
    function action_17 () : void {
      CommissionPlanSearchPopup.push(producer.Tier)
    }
    
    // 'action' attribute on PickerCell (id=CommissionPlan_Cell) at ProducerCodesLV.pcf: line 60, column 45
    function action_21 () : void {
      CommissionPlanDetailPopup.push(producerCode.CommissionPlan)
    }
    
    // 'pickLocation' attribute on PickerCell (id=CommissionPlan_Cell) at ProducerCodesLV.pcf: line 60, column 45
    function action_dest_18 () : pcf.api.Destination {
      return pcf.CommissionPlanSearchPopup.createDestination(producer.Tier)
    }
    
    // 'action' attribute on PickerCell (id=CommissionPlan_Cell) at ProducerCodesLV.pcf: line 60, column 45
    function action_dest_22 () : pcf.api.Destination {
      return pcf.CommissionPlanDetailPopup.createDestination(producerCode.CommissionPlan)
    }
    
    // 'condition' attribute on ToolbarFlag at ProducerCodesLV.pcf: line 32, column 33
    function condition_5 () : java.lang.Boolean {
      return producerCode.New
    }
    
    // 'value' attribute on BooleanRadioCell (id=Active_Cell) at ProducerCodesLV.pcf: line 43, column 40
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      producerCode.Active = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on PickerCell (id=CommissionPlan_Cell) at ProducerCodesLV.pcf: line 60, column 45
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      producerCode.CommissionPlan = (__VALUE_TO_SET as entity.CommissionPlan)
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at ProducerCodesLV.pcf: line 38, column 38
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      producerCode.Code = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on PickerCell (id=CommissionPlan_Cell) at ProducerCodesLV.pcf: line 60, column 45
    function editable_19 () : java.lang.Boolean {
      return isNew or perm.Producer.commplanassnedit
    }
    
    // 'inputConversion' attribute on PickerCell (id=CommissionPlan_Cell) at ProducerCodesLV.pcf: line 60, column 45
    function inputConversion_23 (VALUE :  java.lang.String) : java.lang.Object {
      return gw.api.web.plan.commission.CommissionPlanUtil.getActiveCommissionPlan(VALUE, producer.Tier)
    }
    
    // 'outputConversion' attribute on TextCell (id=CommissionRate_Cell) at ProducerCodesLV.pcf: line 50, column 45
    function outputConversion_14 (VALUE :  java.math.BigDecimal) : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(VALUE, 2)
    }
    
    // 'validationExpression' attribute on PickerCell (id=CommissionPlan_Cell) at ProducerCodesLV.pcf: line 60, column 45
    function validationExpression_20 () : java.lang.Object {
      return (producerCode.CommissionPlan != null and producerCode.CommissionPlan.isAllowedTier(producer.Tier)) ? null : DisplayKey.get("Web.ProducerDetail.ProducerCodes.Validation.HasTierInProducerCode", producer.Tier)
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at ProducerCodesLV.pcf: line 38, column 38
    function valueRoot_8 () : java.lang.Object {
      return producerCode
    }
    
    // 'value' attribute on BooleanRadioCell (id=Active_Cell) at ProducerCodesLV.pcf: line 43, column 40
    function value_10 () : java.lang.Boolean {
      return producerCode.Active
    }
    
    // 'value' attribute on TextCell (id=CommissionRate_Cell) at ProducerCodesLV.pcf: line 50, column 45
    function value_15 () : java.math.BigDecimal {
      return producerCode.CommissionPlan.DefaultSubPlan != null ? producerCode.CommissionPlan.DefaultSubPlan.getBaseRate(TC_PRIMARY) : null
    }
    
    // 'value' attribute on PickerCell (id=CommissionPlan_Cell) at ProducerCodesLV.pcf: line 60, column 45
    function value_24 () : entity.CommissionPlan {
      return producerCode.CommissionPlan
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at ProducerCodesLV.pcf: line 38, column 38
    function value_6 () : java.lang.String {
      return producerCode.Code
    }
    
    property get producerCode () : entity.ProducerCode {
      return getIteratedValue(1) as entity.ProducerCode
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerCodesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerCodesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'autoAdd' attribute on RowIterator at ProducerCodesLV.pcf: line 29, column 41
    function autoAdd_4 () : java.lang.Boolean {
      return producer.ProducerCodes.length == 0
    }
    
    // 'value' attribute on TextCell (id=Code_Cell) at ProducerCodesLV.pcf: line 38, column 38
    function sortValue_0 (producerCode :  entity.ProducerCode) : java.lang.Object {
      return producerCode.Code
    }
    
    // 'value' attribute on BooleanRadioCell (id=Active_Cell) at ProducerCodesLV.pcf: line 43, column 40
    function sortValue_1 (producerCode :  entity.ProducerCode) : java.lang.Object {
      return producerCode.Active
    }
    
    // 'value' attribute on TextCell (id=CommissionRate_Cell) at ProducerCodesLV.pcf: line 50, column 45
    function sortValue_2 (producerCode :  entity.ProducerCode) : java.lang.Object {
      return producerCode.CommissionPlan.DefaultSubPlan != null ? producerCode.CommissionPlan.DefaultSubPlan.getBaseRate(TC_PRIMARY) : null
    }
    
    // 'value' attribute on PickerCell (id=CommissionPlan_Cell) at ProducerCodesLV.pcf: line 60, column 45
    function sortValue_3 (producerCode :  entity.ProducerCode) : java.lang.Object {
      return producerCode.CommissionPlan
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at ProducerCodesLV.pcf: line 29, column 41
    function toCreateAndAdd_30 () : entity.ProducerCode {
      return addNewProducerCode()
    }
    
    // 'toRemove' attribute on RowIterator at ProducerCodesLV.pcf: line 29, column 41
    function toRemove_31 (producerCode :  entity.ProducerCode) : void {
      producer.removeFromProducerCodes(producerCode)
    }
    
    // 'validationExpression' attribute on ListViewPanel (id=ProducerCodesLV) at ProducerCodesLV.pcf: line 9, column 19
    function validationExpression_33 () : java.lang.Object {
      return producer.ActiveProducerCodes.length > 0 ? null : DisplayKey.get("Web.ProducerDetail.ProducerCodes.Validation.AtLeastOneActiveProducerCode")
    }
    
    // 'value' attribute on RowIterator at ProducerCodesLV.pcf: line 29, column 41
    function value_32 () : entity.ProducerCode[] {
      return producer.ProducerCodes
    }
    
    property get isNew () : Boolean {
      return getRequireValue("isNew", 0) as Boolean
    }
    
    property set isNew ($arg :  Boolean) {
      setRequireValue("isNew", 0, $arg)
    }
    
    property get producer () : Producer {
      return getRequireValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setRequireValue("producer", 0, $arg)
    }
    
    function addNewProducerCode() : ProducerCode {
      var producerCode = new entity.ProducerCode(producer.Currency)
      producer.addToProducerCodes(producerCode)
      return producerCode
    }
    
    
  }
  
  
}