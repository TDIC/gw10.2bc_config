package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailMenuActionsExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailMenuActionsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_NewPolicy) at AccountDetailMenuActions.pcf: line 15, column 83
    function action_1 () : void {
      NewPolicyWizard.go(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_NewDirectBillCreditDistribution) at AccountDetailMenuActions.pcf: line 38, column 89
    function action_10 () : void {
      NewDirectBillCreditDistribution.push(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_NewPaymentRequest) at AccountDetailMenuActions.pcf: line 43, column 58
    function action_13 () : void {
      AccountDetailNewPaymentRequestWizard.push(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_Disbursement) at AccountDetailMenuActions.pcf: line 52, column 48
    function action_16 () : void {
      AccountCreateDisbursementWizard.push(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_Transfer) at AccountDetailMenuActions.pcf: line 57, column 45
    function action_19 () : void {
      AccountNewTransferWizard.push(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_Writeoff) at AccountDetailMenuActions.pcf: line 62, column 44
    function action_22 () : void {
      AccountNewWriteoffWizard.push(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_NegativeWriteoff) at AccountDetailMenuActions.pcf: line 67, column 44
    function action_25 () : void {
      AccountNewNegativeWriteoffWizard.push(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_WriteoffReversal) at AccountDetailMenuActions.pcf: line 82, column 44
    function action_28 () : void {
      AccountNewWriteoffReversalWizard.push(account.ID)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_NegativeWriteoffReversal) at AccountDetailMenuActions.pcf: line 87, column 44
    function action_31 () : void {
      AccountNewNegativeWriteoffReversalWizard.push(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_Recapture) at AccountDetailMenuActions.pcf: line 97, column 44
    function action_34 () : void {
      AccountNewRecaptureWizard.push(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_Credit) at AccountDetailMenuActions.pcf: line 102, column 44
    function action_37 () : void {
      AccountNewCreditWizard.push(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_CreditReversal) at AccountDetailMenuActions.pcf: line 107, column 44
    function action_40 () : void {
      AccountNewCreditReversalWizard.push(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_General) at AccountDetailMenuActions.pcf: line 112, column 44
    function action_43 () : void {
      AccountNewTransactionWizard.push(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountsMenuActions_NewNote) at AccountDetailMenuActions.pcf: line 126, column 35
    function action_46 () : void {
      AccountNewNoteWorksheet.goInWorkspace(account)
    }
    
    // 'action' attribute on MenuItem (id=NewEmail) at AccountDetailMenuActions.pcf: line 141, column 84
    function action_52 () : void {
      SampleEmailWorksheet.goInWorkspace(account, account)
    }
    
    // 'action' attribute on MenuItem (id=NewEmailWithTemplate) at AccountDetailMenuActions.pcf: line 145, column 96
    function action_54 () : void {
      SampleEmailWorksheet.goInWorkspace(account, account, "EmailReceived.gosu")
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_NewDirectBillPayment) at AccountDetailMenuActions.pcf: line 32, column 89
    function action_7 () : void {
      NewDirectBillPayment.push(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_NewDirectBillCreditDistribution) at AccountDetailMenuActions.pcf: line 38, column 89
    function action_dest_11 () : pcf.api.Destination {
      return pcf.NewDirectBillCreditDistribution.createDestination(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_NewPaymentRequest) at AccountDetailMenuActions.pcf: line 43, column 58
    function action_dest_14 () : pcf.api.Destination {
      return pcf.AccountDetailNewPaymentRequestWizard.createDestination(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_Disbursement) at AccountDetailMenuActions.pcf: line 52, column 48
    function action_dest_17 () : pcf.api.Destination {
      return pcf.AccountCreateDisbursementWizard.createDestination(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_NewPolicy) at AccountDetailMenuActions.pcf: line 15, column 83
    function action_dest_2 () : pcf.api.Destination {
      return pcf.NewPolicyWizard.createDestination(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_Transfer) at AccountDetailMenuActions.pcf: line 57, column 45
    function action_dest_20 () : pcf.api.Destination {
      return pcf.AccountNewTransferWizard.createDestination(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_Writeoff) at AccountDetailMenuActions.pcf: line 62, column 44
    function action_dest_23 () : pcf.api.Destination {
      return pcf.AccountNewWriteoffWizard.createDestination(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_NegativeWriteoff) at AccountDetailMenuActions.pcf: line 67, column 44
    function action_dest_26 () : pcf.api.Destination {
      return pcf.AccountNewNegativeWriteoffWizard.createDestination(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_WriteoffReversal) at AccountDetailMenuActions.pcf: line 82, column 44
    function action_dest_29 () : pcf.api.Destination {
      return pcf.AccountNewWriteoffReversalWizard.createDestination(account.ID)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_NegativeWriteoffReversal) at AccountDetailMenuActions.pcf: line 87, column 44
    function action_dest_32 () : pcf.api.Destination {
      return pcf.AccountNewNegativeWriteoffReversalWizard.createDestination(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_Recapture) at AccountDetailMenuActions.pcf: line 97, column 44
    function action_dest_35 () : pcf.api.Destination {
      return pcf.AccountNewRecaptureWizard.createDestination(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_Credit) at AccountDetailMenuActions.pcf: line 102, column 44
    function action_dest_38 () : pcf.api.Destination {
      return pcf.AccountNewCreditWizard.createDestination(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_CreditReversal) at AccountDetailMenuActions.pcf: line 107, column 44
    function action_dest_41 () : pcf.api.Destination {
      return pcf.AccountNewCreditReversalWizard.createDestination(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_General) at AccountDetailMenuActions.pcf: line 112, column 44
    function action_dest_44 () : pcf.api.Destination {
      return pcf.AccountNewTransactionWizard.createDestination(account)
    }
    
    // 'action' attribute on MenuItem (id=AccountsMenuActions_NewNote) at AccountDetailMenuActions.pcf: line 126, column 35
    function action_dest_47 () : pcf.api.Destination {
      return pcf.AccountNewNoteWorksheet.createDestination(account)
    }
    
    // 'action' attribute on MenuItem (id=NewEmail) at AccountDetailMenuActions.pcf: line 141, column 84
    function action_dest_53 () : pcf.api.Destination {
      return pcf.SampleEmailWorksheet.createDestination(account, account)
    }
    
    // 'action' attribute on MenuItem (id=NewEmailWithTemplate) at AccountDetailMenuActions.pcf: line 145, column 96
    function action_dest_55 () : pcf.api.Destination {
      return pcf.SampleEmailWorksheet.createDestination(account, account, "EmailReceived.gosu")
    }
    
    // 'action' attribute on MenuItem (id=AccountDetailMenuActions_NewDirectBillPayment) at AccountDetailMenuActions.pcf: line 32, column 89
    function action_dest_8 () : pcf.api.Destination {
      return pcf.NewDirectBillPayment.createDestination(account)
    }
    
    // 'available' attribute on MenuItem (id=AccountDetailMenuActions_NewPolicy) at AccountDetailMenuActions.pcf: line 15, column 83
    function available_0 () : java.lang.Boolean {
      return !account.isListBill()
    }
    
    // 'available' attribute on MenuItem (id=NewDocument) at AccountDetailMenuActions.pcf: line 131, column 38
    function available_50 () : java.lang.Boolean {
      return gw.plugin.Plugins.isEnabled(gw.plugin.document.IDocumentContentSource)
    }
    
    // 'def' attribute on MenuItemSetRef at AccountDetailMenuActions.pcf: line 22, column 67
    function def_onEnter_3 (def :  pcf.NewActivityMenuItemSet) : void {
      def.onEnter(false, null, account, null)
    }
    
    // 'def' attribute on MenuItemSetRef at AccountDetailMenuActions.pcf: line 133, column 48
    function def_onEnter_48 (def :  pcf.NewDocumentMenuItemSet) : void {
      def.onEnter(account)
    }
    
    // 'def' attribute on MenuItemSetRef at AccountDetailMenuActions.pcf: line 22, column 67
    function def_refreshVariables_4 (def :  pcf.NewActivityMenuItemSet) : void {
      def.refreshVariables(false, null, account, null)
    }
    
    // 'def' attribute on MenuItemSetRef at AccountDetailMenuActions.pcf: line 133, column 48
    function def_refreshVariables_49 (def :  pcf.NewDocumentMenuItemSet) : void {
      def.refreshVariables(account)
    }
    
    // 'visible' attribute on MenuItem (id=AccountDetailMenuActions_NewPaymentRequest) at AccountDetailMenuActions.pcf: line 43, column 58
    function visible_12 () : java.lang.Boolean {
      return perm.System.pmntreqonetimecreate_TDIC
    }
    
    // 'visible' attribute on MenuItem (id=AccountDetailMenuActions_Disbursement) at AccountDetailMenuActions.pcf: line 52, column 48
    function visible_15 () : java.lang.Boolean {
      return perm.Transaction.disbcreate
    }
    
    // 'visible' attribute on MenuItem (id=AccountDetailMenuActions_Transfer) at AccountDetailMenuActions.pcf: line 57, column 45
    function visible_18 () : java.lang.Boolean {
      return perm.Transaction.cashtx 
    }
    
    // 'visible' attribute on MenuItem (id=AccountDetailMenuActions_Writeoff) at AccountDetailMenuActions.pcf: line 62, column 44
    function visible_21 () : java.lang.Boolean {
      return perm.Transaction.acctwo
    }
    
    // 'visible' attribute on MenuItem (id=AccountDetailMenuActions_WriteoffReversal) at AccountDetailMenuActions.pcf: line 82, column 44
    function visible_27 () : java.lang.Boolean {
      return perm.Transaction.revtxn
    }
    
    // 'visible' attribute on MenuItem (id=AccountDetailMenuActions_Recapture) at AccountDetailMenuActions.pcf: line 97, column 44
    function visible_33 () : java.lang.Boolean {
      return perm.Transaction.gentxn
    }
    
    // 'visible' attribute on MenuItem (id=AccountDetailMenuActions_Credit) at AccountDetailMenuActions.pcf: line 102, column 44
    function visible_36 () : java.lang.Boolean {
      return perm.Transaction.credit
    }
    
    // 'visible' attribute on MenuItem (id=AccountsMenuActions_NewNote) at AccountDetailMenuActions.pcf: line 126, column 35
    function visible_45 () : java.lang.Boolean {
      return perm.Note.create
    }
    
    // 'visible' attribute on MenuItem (id=NewAssignedActivity) at AccountDetailMenuActions.pcf: line 20, column 38
    function visible_5 () : java.lang.Boolean {
      return perm.Activity.create
    }
    
    // 'visible' attribute on MenuItem (id=NewDocument) at AccountDetailMenuActions.pcf: line 131, column 38
    function visible_51 () : java.lang.Boolean {
      return perm.Document.create
    }
    
    // 'visible' attribute on MenuItem (id=AccountDetailMenuActions_NewDirectBillPayment) at AccountDetailMenuActions.pcf: line 32, column 89
    function visible_6 () : java.lang.Boolean {
      return perm.DirectBillMoneyRcvd.pmntmanproc or perm.System.pmntmanproc_tdic
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    
  }
  
  
}