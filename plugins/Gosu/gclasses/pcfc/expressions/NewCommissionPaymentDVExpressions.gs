package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewCommissionPaymentDVExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewCommissionPaymentDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=OtherAmount_Input) at NewCommissionPaymentDV.pcf: line 43, column 43
    function currency_12 () : typekey.Currency {
      return standardPayment.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=OtherAmount_Input) at NewCommissionPaymentDV.pcf: line 43, column 43
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      standardPayment.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on Choice (id=AllCurrentChoice) at NewCommissionPaymentDV.pcf: line 24, column 51
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      standardPayment.PaymentType = (__VALUE_TO_SET as typekey.CommissionPaymentType)
    }
    
    // 'initialValue' attribute on Variable at NewCommissionPaymentDV.pcf: line 15, column 87
    function initialValue_0 () : gw.util.concurrent.LocklessLazyVar<gw.pl.currency.MonetaryAmount> {
      return gw.util.concurrent.LocklessLazyVar.make(\ -> standardPayment.StandardProducerCode.TotalCommissionPayable)
    }
    
    // 'label' attribute on TextInput (id=AllCurrent_Input) at NewCommissionPaymentDV.pcf: line 29, column 41
    function label_1 () : java.lang.Object {
      return DisplayKey.get("Web.NewCommissionPaymentDV.AllCurrent", currentPayable.get().render())
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=OtherAmount_Input) at NewCommissionPaymentDV.pcf: line 43, column 43
    function validationExpression_8 () : java.lang.Object {
      return standardPayment.Amount == null or standardPayment.Amount <= currentPayable.get() ? null : DisplayKey.get("Web.NewCommissionPaymentDV.OtherAmountInvalid", standardPayment.Amount.render(), currentPayable.get().render())
    }
    
    // 'value' attribute on Choice (id=AllCurrentChoice) at NewCommissionPaymentDV.pcf: line 24, column 51
    function valueRoot_7 () : java.lang.Object {
      return standardPayment
    }
    
    // 'value' attribute on TextInput (id=AllCurrent_Input) at NewCommissionPaymentDV.pcf: line 29, column 41
    function value_2 () : java.lang.Object {
      return null
    }
    
    // 'value' attribute on Choice (id=AllCurrentChoice) at NewCommissionPaymentDV.pcf: line 24, column 51
    function value_5 () : typekey.CommissionPaymentType {
      return standardPayment.PaymentType
    }
    
    // 'value' attribute on MonetaryAmountInput (id=OtherAmount_Input) at NewCommissionPaymentDV.pcf: line 43, column 43
    function value_9 () : gw.pl.currency.MonetaryAmount {
      return standardPayment.Amount
    }
    
    property get currentPayable () : gw.util.concurrent.LocklessLazyVar<gw.pl.currency.MonetaryAmount> {
      return getVariableValue("currentPayable", 0) as gw.util.concurrent.LocklessLazyVar<gw.pl.currency.MonetaryAmount>
    }
    
    property set currentPayable ($arg :  gw.util.concurrent.LocklessLazyVar<gw.pl.currency.MonetaryAmount>) {
      setVariableValue("currentPayable", 0, $arg)
    }
    
    property get standardPayment () : StandardCmsnPayment {
      return getRequireValue("standardPayment", 0) as StandardCmsnPayment
    }
    
    property set standardPayment ($arg :  StandardCmsnPayment) {
      setRequireValue("standardPayment", 0, $arg)
    }
    
    
  }
  
  
}