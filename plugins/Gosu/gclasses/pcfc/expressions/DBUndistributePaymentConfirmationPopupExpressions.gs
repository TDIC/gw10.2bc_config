package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/DBUndistributePaymentConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DBUndistributePaymentConfirmationPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/DBUndistributePaymentConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DBUndistributePaymentConfirmationPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (payment :  DirectBillPayment) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=DBUndistributePaymentConfirmationPopup) at DBUndistributePaymentConfirmationPopup.pcf: line 11, column 85
    function beforeCommit_2 (pickedValue :  java.lang.Object) : void {
      payment.reverseDistOnly()
    }
    
    // 'canVisit' attribute on Popup (id=DBUndistributePaymentConfirmationPopup) at DBUndistributePaymentConfirmationPopup.pcf: line 11, column 85
    static function canVisit_3 (payment :  DirectBillPayment) : java.lang.Boolean {
      return perm.System.acctpmntview and perm.DirectBillMoneyRcvd.pmntreverse
    }
    
    // 'initialValue' attribute on Variable at DBUndistributePaymentConfirmationPopup.pcf: line 20, column 36
    function initialValue_0 () : entity.UnappliedFund {
      return (payment.BaseMoneyReceived as DirectBillMoneyRcvd).UnappliedFund
    }
    
    // 'label' attribute on Label (id=Confirmation) at DBUndistributePaymentConfirmationPopup.pcf: line 30, column 174
    function label_1 () : java.lang.String {
      return DisplayKey.get("Web.DBUndistributePaymentConfirmation.Confirmation", (payment.NetDistributedToInvoiceItems + payment.NetInSuspense).render())
    }
    
    override property get CurrentLocation () : pcf.DBUndistributePaymentConfirmationPopup {
      return super.CurrentLocation as pcf.DBUndistributePaymentConfirmationPopup
    }
    
    property get payment () : DirectBillPayment {
      return getVariableValue("payment", 0) as DirectBillPayment
    }
    
    property set payment ($arg :  DirectBillPayment) {
      setVariableValue("payment", 0, $arg)
    }
    
    property get unapplied () : entity.UnappliedFund {
      return getVariableValue("unapplied", 0) as entity.UnappliedFund
    }
    
    property set unapplied ($arg :  entity.UnappliedFund) {
      setVariableValue("unapplied", 0, $arg)
    }
    
    
  }
  
  
}