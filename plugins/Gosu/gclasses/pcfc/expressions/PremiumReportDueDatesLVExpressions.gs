package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PremiumReportDueDatesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PremiumReportDueDatesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PremiumReportDueDatesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PremiumReportDueDatesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=PeriodStartDate_Cell) at PremiumReportDueDatesLV.pcf: line 19, column 57
    function valueRoot_4 () : java.lang.Object {
      return premiumReportDueDate
    }
    
    // 'value' attribute on DateCell (id=PeriodStartDate_Cell) at PremiumReportDueDatesLV.pcf: line 19, column 57
    function value_3 () : java.util.Date {
      return premiumReportDueDate.PeriodStartDate
    }
    
    // 'value' attribute on DateCell (id=PeriodEndDate_Cell) at PremiumReportDueDatesLV.pcf: line 23, column 55
    function value_6 () : java.util.Date {
      return premiumReportDueDate.PeriodEndDate
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at PremiumReportDueDatesLV.pcf: line 27, column 49
    function value_9 () : java.util.Date {
      return premiumReportDueDate.DueDate
    }
    
    property get premiumReportDueDate () : entity.PremiumReportDueDate {
      return getIteratedValue(1) as entity.PremiumReportDueDate
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PremiumReportDueDatesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PremiumReportDueDatesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=PeriodStartDate_Cell) at PremiumReportDueDatesLV.pcf: line 19, column 57
    function sortValue_0 (premiumReportDueDate :  entity.PremiumReportDueDate) : java.lang.Object {
      return premiumReportDueDate.PeriodStartDate
    }
    
    // 'value' attribute on DateCell (id=PeriodEndDate_Cell) at PremiumReportDueDatesLV.pcf: line 23, column 55
    function sortValue_1 (premiumReportDueDate :  entity.PremiumReportDueDate) : java.lang.Object {
      return premiumReportDueDate.PeriodEndDate
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at PremiumReportDueDatesLV.pcf: line 27, column 49
    function sortValue_2 (premiumReportDueDate :  entity.PremiumReportDueDate) : java.lang.Object {
      return premiumReportDueDate.DueDate
    }
    
    // 'value' attribute on RowIterator at PremiumReportDueDatesLV.pcf: line 14, column 49
    function value_12 () : entity.PremiumReportDueDate[] {
      return policyPeriod.getUnmatchedPremiumReportDueDates()
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}