package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerStatementOverview.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerStatementOverviewExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerStatementOverview.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ProducerStatementOverviewExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=number_Cell) at ProducerStatementOverview.pcf: line 38, column 55
    function action_8 () : void {
      pcf.ProducerStatement.drilldown(statement)
    }
    
    // 'action' attribute on TextCell (id=number_Cell) at ProducerStatementOverview.pcf: line 38, column 55
    function action_dest_9 () : pcf.api.Destination {
      return pcf.ProducerStatement.createDestination(statement)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=payment_Cell) at ProducerStatementOverview.pcf: line 45, column 54
    function currency_14 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'value' attribute on DateCell (id=statementDate_Cell) at ProducerStatementOverview.pcf: line 32, column 50
    function valueRoot_6 () : java.lang.Object {
      return statement
    }
    
    // 'value' attribute on TextCell (id=number_Cell) at ProducerStatementOverview.pcf: line 38, column 55
    function value_10 () : entity.ProducerStatement {
      return statement
    }
    
    // 'value' attribute on MonetaryAmountCell (id=payment_Cell) at ProducerStatementOverview.pcf: line 45, column 54
    function value_12 () : gw.pl.currency.MonetaryAmount {
      return statement.AutoPaymentAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=netAmountSentAndReceived_Cell) at ProducerStatementOverview.pcf: line 51, column 61
    function value_16 () : gw.pl.currency.MonetaryAmount {
      return statement.NetAmountSentAndReceived
    }
    
    // 'value' attribute on MonetaryAmountCell (id=balance_Cell) at ProducerStatementOverview.pcf: line 57, column 44
    function value_20 () : gw.pl.currency.MonetaryAmount {
      return statement.Balance
    }
    
    // 'value' attribute on TypeKeyCell (id=type_Cell) at ProducerStatementOverview.pcf: line 62, column 60
    function value_24 () : typekey.ProducerStatementType {
      return statement.Type
    }
    
    // 'value' attribute on DateCell (id=statementDate_Cell) at ProducerStatementOverview.pcf: line 32, column 50
    function value_5 () : java.util.Date {
      return statement.StatementDate
    }
    
    property get statement () : entity.ProducerStatement {
      return getIteratedValue(1) as entity.ProducerStatement
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerStatementOverview.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerStatementOverviewExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=ProducerStatementOverview) at ProducerStatementOverview.pcf: line 10, column 77
    static function canVisit_28 (producer :  Producer) : java.lang.Boolean {
      return perm.System.proddbstmtview
    }
    
    // Page (id=ProducerStatementOverview) at ProducerStatementOverview.pcf: line 10, column 77
    static function parent_29 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    // 'value' attribute on DateCell (id=statementDate_Cell) at ProducerStatementOverview.pcf: line 32, column 50
    function sortValue_0 (statement :  entity.ProducerStatement) : java.lang.Object {
      return statement.StatementDate
    }
    
    // 'value' attribute on TextCell (id=number_Cell) at ProducerStatementOverview.pcf: line 38, column 55
    function sortValue_1 (statement :  entity.ProducerStatement) : java.lang.Object {
      return statement
    }
    
    // 'value' attribute on MonetaryAmountCell (id=netAmountSentAndReceived_Cell) at ProducerStatementOverview.pcf: line 51, column 61
    function sortValue_2 (statement :  entity.ProducerStatement) : java.lang.Object {
      return statement.NetAmountSentAndReceived
    }
    
    // 'value' attribute on MonetaryAmountCell (id=balance_Cell) at ProducerStatementOverview.pcf: line 57, column 44
    function sortValue_3 (statement :  entity.ProducerStatement) : java.lang.Object {
      return statement.Balance
    }
    
    // 'value' attribute on TypeKeyCell (id=type_Cell) at ProducerStatementOverview.pcf: line 62, column 60
    function sortValue_4 (statement :  entity.ProducerStatement) : java.lang.Object {
      return statement.Type
    }
    
    // 'value' attribute on RowIterator (id=producerStatements) at ProducerStatementOverview.pcf: line 26, column 90
    function value_27 () : gw.api.database.IQueryBeanResult<entity.ProducerStatement> {
      return producer.findStatements()
    }
    
    override property get CurrentLocation () : pcf.ProducerStatementOverview {
      return super.CurrentLocation as pcf.ProducerStatementOverview
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}