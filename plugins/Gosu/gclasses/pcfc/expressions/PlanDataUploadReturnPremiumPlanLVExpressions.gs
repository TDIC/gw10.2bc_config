package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadReturnPremiumPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanDataUploadReturnPremiumPlanLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadReturnPremiumPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PlanDataUploadReturnPremiumPlanLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at PlanDataUploadReturnPremiumPlanLV.pcf: line 17, column 114
    function highlighted_50 () : java.lang.Boolean {
      return (returnPremiumPlan.Error or returnPremiumPlan.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 23, column 46
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 28, column 41
    function label_20 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumPlan.PublicID")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 34, column 41
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumPlan.Name")
    }
    
    // 'label' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 39, column 39
    function label_30 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumPlan.EffectiveDate")
    }
    
    // 'label' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 44, column 39
    function label_35 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumPlan.ExpirationDate")
    }
    
    // 'label' attribute on TypeKeyCell (id=qualifier_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 49, column 65
    function label_40 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumPlan.Qualifier")
    }
    
    // 'label' attribute on TypeKeyCell (id=listBillExcess_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 54, column 54
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumPlan.ListBillExcess")
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 28, column 41
    function valueRoot_22 () : java.lang.Object {
      return returnPremiumPlan
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 23, column 46
    function value_16 () : java.lang.String {
      return processor.getLoadStatus(returnPremiumPlan)
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 28, column 41
    function value_21 () : java.lang.String {
      return returnPremiumPlan.PublicID
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 34, column 41
    function value_26 () : java.lang.String {
      return returnPremiumPlan.Name
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 39, column 39
    function value_31 () : java.util.Date {
      return returnPremiumPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 44, column 39
    function value_36 () : java.util.Date {
      return returnPremiumPlan.ExpirationDate
    }
    
    // 'value' attribute on TypeKeyCell (id=qualifier_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 49, column 65
    function value_41 () : typekey.ReturnPremiumChargeQualification {
      return returnPremiumPlan.Qualifier
    }
    
    // 'value' attribute on TypeKeyCell (id=listBillExcess_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 54, column 54
    function value_46 () : typekey.ListBillAccountExcess {
      return returnPremiumPlan.ListBillExcess
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 23, column 46
    function visible_17 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get returnPremiumPlan () : tdic.util.dataloader.data.plandata.ReturnPremiumPlanData {
      return getIteratedValue(1) as tdic.util.dataloader.data.plandata.ReturnPremiumPlanData
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadReturnPremiumPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanDataUploadReturnPremiumPlanLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TypeKeyCell (id=qualifier_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 49, column 65
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumPlan.Qualifier")
    }
    
    // 'label' attribute on TypeKeyCell (id=listBillExcess_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 54, column 54
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumPlan.ListBillExcess")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 28, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumPlan.PublicID")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 34, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumPlan.Name")
    }
    
    // 'label' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 39, column 39
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumPlan.EffectiveDate")
    }
    
    // 'label' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 44, column 39
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumPlan.ExpirationDate")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 23, column 46
    function sortValue_1 (returnPremiumPlan :  tdic.util.dataloader.data.plandata.ReturnPremiumPlanData) : java.lang.Object {
      return processor.getLoadStatus(returnPremiumPlan)
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 44, column 39
    function sortValue_10 (returnPremiumPlan :  tdic.util.dataloader.data.plandata.ReturnPremiumPlanData) : java.lang.Object {
      return returnPremiumPlan.ExpirationDate
    }
    
    // 'value' attribute on TypeKeyCell (id=qualifier_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 49, column 65
    function sortValue_12 (returnPremiumPlan :  tdic.util.dataloader.data.plandata.ReturnPremiumPlanData) : java.lang.Object {
      return returnPremiumPlan.Qualifier
    }
    
    // 'value' attribute on TypeKeyCell (id=listBillExcess_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 54, column 54
    function sortValue_14 (returnPremiumPlan :  tdic.util.dataloader.data.plandata.ReturnPremiumPlanData) : java.lang.Object {
      return returnPremiumPlan.ListBillExcess
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 28, column 41
    function sortValue_4 (returnPremiumPlan :  tdic.util.dataloader.data.plandata.ReturnPremiumPlanData) : java.lang.Object {
      return returnPremiumPlan.PublicID
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 34, column 41
    function sortValue_6 (returnPremiumPlan :  tdic.util.dataloader.data.plandata.ReturnPremiumPlanData) : java.lang.Object {
      return returnPremiumPlan.Name
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 39, column 39
    function sortValue_8 (returnPremiumPlan :  tdic.util.dataloader.data.plandata.ReturnPremiumPlanData) : java.lang.Object {
      return returnPremiumPlan.EffectiveDate
    }
    
    // 'value' attribute on RowIterator (id=ReturnPremiumPlan) at PlanDataUploadReturnPremiumPlanLV.pcf: line 15, column 103
    function value_51 () : java.util.ArrayList<tdic.util.dataloader.data.plandata.ReturnPremiumPlanData> {
      return processor.ReturnPremiumPlanArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadReturnPremiumPlanLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}