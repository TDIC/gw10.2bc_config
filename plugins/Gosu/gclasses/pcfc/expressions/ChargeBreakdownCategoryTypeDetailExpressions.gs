package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/chargepatterns/ChargeBreakdownCategoryTypeDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChargeBreakdownCategoryTypeDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/chargepatterns/ChargeBreakdownCategoryTypeDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChargeBreakdownCategoryTypeDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (categoryType :  ChargeBreakdownCategoryType) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=ChargeBreakdownCategoryTypeDetail) at ChargeBreakdownCategoryTypeDetail.pcf: line 11, column 123
    function canEdit_5 () : java.lang.Boolean {
      return perm.System.chargebreakdowncategorytypeedit
    }
    
    // 'canVisit' attribute on Page (id=ChargeBreakdownCategoryTypeDetail) at ChargeBreakdownCategoryTypeDetail.pcf: line 11, column 123
    static function canVisit_6 (categoryType :  ChargeBreakdownCategoryType) : java.lang.Boolean {
      return perm.System.admintabview and perm.System.chargebreakdowncategorytypeview
    }
    
    // 'def' attribute on PanelRef at ChargeBreakdownCategoryTypeDetail.pcf: line 22, column 74
    function def_onEnter_1 (def :  pcf.ChargeBreakdownCategoryTypeDetailDV) : void {
      def.onEnter(categoryType)
    }
    
    // 'def' attribute on PanelRef at ChargeBreakdownCategoryTypeDetail.pcf: line 24, column 138
    function def_onEnter_3 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(categoryType, {"Name"}, { DisplayKey.get("Web.ChargeBreakdownCategoryType.Name") })
    }
    
    // 'def' attribute on PanelRef at ChargeBreakdownCategoryTypeDetail.pcf: line 22, column 74
    function def_refreshVariables_2 (def :  pcf.ChargeBreakdownCategoryTypeDetailDV) : void {
      def.refreshVariables(categoryType)
    }
    
    // 'def' attribute on PanelRef at ChargeBreakdownCategoryTypeDetail.pcf: line 24, column 138
    function def_refreshVariables_4 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(categoryType, {"Name"}, { DisplayKey.get("Web.ChargeBreakdownCategoryType.Name") })
    }
    
    // EditButtons at ChargeBreakdownCategoryTypeDetail.pcf: line 20, column 23
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'parent' attribute on Page (id=ChargeBreakdownCategoryTypeDetail) at ChargeBreakdownCategoryTypeDetail.pcf: line 11, column 123
    static function parent_7 (categoryType :  ChargeBreakdownCategoryType) : pcf.api.Destination {
      return pcf.ChargeBreakdownCategoryTypes.createDestination()
    }
    
    // 'title' attribute on Page (id=ChargeBreakdownCategoryTypeDetail) at ChargeBreakdownCategoryTypeDetail.pcf: line 11, column 123
    static function title_8 (categoryType :  ChargeBreakdownCategoryType) : java.lang.Object {
      return DisplayKey.get("Web.ChargeBreakdownCategoryTypeDetail.Title", categoryType.Code, categoryType.Name)
    }
    
    override property get CurrentLocation () : pcf.ChargeBreakdownCategoryTypeDetail {
      return super.CurrentLocation as pcf.ChargeBreakdownCategoryTypeDetail
    }
    
    property get categoryType () : ChargeBreakdownCategoryType {
      return getVariableValue("categoryType", 0) as ChargeBreakdownCategoryType
    }
    
    property set categoryType ($arg :  ChargeBreakdownCategoryType) {
      setVariableValue("categoryType", 0, $arg)
    }
    
    
  }
  
  
}