package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadHolidayLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadHolidayLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadHolidayLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadHolidayLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadHolidayLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadHolidayLV.pcf: line 50, column 42
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadHolidayLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Holiday.PublicID")
    }
    
    // 'label' attribute on TextCell (id=roleName_Cell) at AdminDataUploadHolidayLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Holiday.Name")
    }
    
    // 'label' attribute on DateCell (id=occurence_date_Cell) at AdminDataUploadHolidayLV.pcf: line 40, column 39
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Holiday.OccurrenceDate")
    }
    
    // 'label' attribute on BooleanRadioCell (id=permissions_Cell) at AdminDataUploadHolidayLV.pcf: line 45, column 42
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Holiday.AllZones")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadHolidayLV.pcf: line 23, column 46
    function sortValue_1 (holiday :  tdic.util.dataloader.data.admindata.HolidayData) : java.lang.Object {
      return processor.getLoadStatus(holiday)
    }
    
    // 'value' attribute on BooleanRadioCell (id=permissions_Cell) at AdminDataUploadHolidayLV.pcf: line 45, column 42
    function sortValue_10 (holiday :  tdic.util.dataloader.data.admindata.HolidayData) : java.lang.Object {
      return holiday.AppliesToAllZones
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadHolidayLV.pcf: line 50, column 42
    function sortValue_12 (holiday :  tdic.util.dataloader.data.admindata.HolidayData) : java.lang.Object {
      return holiday.Exclude
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadHolidayLV.pcf: line 29, column 41
    function sortValue_4 (holiday :  tdic.util.dataloader.data.admindata.HolidayData) : java.lang.Object {
      return holiday.PublicID
    }
    
    // 'value' attribute on TextCell (id=roleName_Cell) at AdminDataUploadHolidayLV.pcf: line 35, column 41
    function sortValue_6 (holiday :  tdic.util.dataloader.data.admindata.HolidayData) : java.lang.Object {
      return holiday.Name
    }
    
    // 'value' attribute on DateCell (id=occurence_date_Cell) at AdminDataUploadHolidayLV.pcf: line 40, column 39
    function sortValue_8 (holiday :  tdic.util.dataloader.data.admindata.HolidayData) : java.lang.Object {
      return holiday.OccurrenceDate
    }
    
    // 'value' attribute on RowIterator (id=holidayID) at AdminDataUploadHolidayLV.pcf: line 15, column 94
    function value_44 () : java.util.ArrayList<tdic.util.dataloader.data.admindata.HolidayData> {
      return processor.HolidayArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadHolidayLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadHolidayLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminDataUploadHolidayLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at AdminDataUploadHolidayLV.pcf: line 17, column 94
    function highlighted_43 () : java.lang.Boolean {
      return (holiday.Error or holiday.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadHolidayLV.pcf: line 23, column 46
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadHolidayLV.pcf: line 29, column 41
    function label_18 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Holiday.PublicID")
    }
    
    // 'label' attribute on TextCell (id=roleName_Cell) at AdminDataUploadHolidayLV.pcf: line 35, column 41
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Holiday.Name")
    }
    
    // 'label' attribute on DateCell (id=occurence_date_Cell) at AdminDataUploadHolidayLV.pcf: line 40, column 39
    function label_28 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Holiday.OccurrenceDate")
    }
    
    // 'label' attribute on BooleanRadioCell (id=permissions_Cell) at AdminDataUploadHolidayLV.pcf: line 45, column 42
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Holiday.AllZones")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadHolidayLV.pcf: line 50, column 42
    function label_38 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadHolidayLV.pcf: line 29, column 41
    function valueRoot_20 () : java.lang.Object {
      return holiday
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadHolidayLV.pcf: line 23, column 46
    function value_14 () : java.lang.String {
      return processor.getLoadStatus(holiday)
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadHolidayLV.pcf: line 29, column 41
    function value_19 () : java.lang.String {
      return holiday.PublicID
    }
    
    // 'value' attribute on TextCell (id=roleName_Cell) at AdminDataUploadHolidayLV.pcf: line 35, column 41
    function value_24 () : java.lang.String {
      return holiday.Name
    }
    
    // 'value' attribute on DateCell (id=occurence_date_Cell) at AdminDataUploadHolidayLV.pcf: line 40, column 39
    function value_29 () : java.util.Date {
      return holiday.OccurrenceDate
    }
    
    // 'value' attribute on BooleanRadioCell (id=permissions_Cell) at AdminDataUploadHolidayLV.pcf: line 45, column 42
    function value_34 () : java.lang.Boolean {
      return holiday.AppliesToAllZones
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadHolidayLV.pcf: line 50, column 42
    function value_39 () : java.lang.Boolean {
      return holiday.Exclude
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadHolidayLV.pcf: line 23, column 46
    function visible_15 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get holiday () : tdic.util.dataloader.data.admindata.HolidayData {
      return getIteratedValue(1) as tdic.util.dataloader.data.admindata.HolidayData
    }
    
    
  }
  
  
}