package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/PaymentRequestSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentRequestSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/PaymentRequestSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentRequestSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountNumberCriterion_Input) at PaymentRequestSearchDV.pcf: line 21, column 39
    function action_0 () : void {
      AccountSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountNumberCriterion_Input) at PaymentRequestSearchDV.pcf: line 21, column 39
    function action_dest_1 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'available' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at PaymentRequestSearchDV.pcf: line 57, column 43
    function available_25 () : java.lang.Boolean {
      return (searchCriteria.Currency != null or gw.api.util.CurrencyUtil.isSingleCurrencyMode())
    }
    
    // 'conversionExpression' attribute on PickerInput (id=AccountNumberCriterion_Input) at PaymentRequestSearchDV.pcf: line 21, column 39
    function conversionExpression_2 (PickedValue :  Account) : java.lang.String {
      return PickedValue.DisplayName
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at PaymentRequestSearchDV.pcf: line 57, column 43
    function currency_29 () : typekey.Currency {
      return gw.search.SearchHelper.getDefaultCurrencyIfNull(searchCriteria.Currency)
    }
    
    // 'def' attribute on InputSetRef at PaymentRequestSearchDV.pcf: line 100, column 41
    function def_onEnter_63 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at PaymentRequestSearchDV.pcf: line 100, column 41
    function def_refreshVariables_64 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on RangeInput (id=MethodCriterion_Input) at PaymentRequestSearchDV.pcf: line 34, column 44
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Method = (__VALUE_TO_SET as typekey.PaymentMethod)
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at PaymentRequestSearchDV.pcf: line 47, column 66
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Currency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at PaymentRequestSearchDV.pcf: line 57, column 43
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MinAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at PaymentRequestSearchDV.pcf: line 64, column 43
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MaxAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at PaymentRequestSearchDV.pcf: line 21, column 39
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AccountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=EarliestDraftDateCriterion_Input) at PaymentRequestSearchDV.pcf: line 71, column 51
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.EarliestDraftDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=LatestDraftDateCriterion_Input) at PaymentRequestSearchDV.pcf: line 76, column 49
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.LatestDraftDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=EarliestRequestDateCriterion_Input) at PaymentRequestSearchDV.pcf: line 81, column 53
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.EarliestRequestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=LatestRequestDateCriterion_Input) at PaymentRequestSearchDV.pcf: line 86, column 51
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.LatestRequestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=EarliestStatusDateCriterion_Input) at PaymentRequestSearchDV.pcf: line 91, column 52
    function defaultSetter_56 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.EarliestStatusDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=LatestStatusDateCriterion_Input) at PaymentRequestSearchDV.pcf: line 96, column 50
    function defaultSetter_60 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.LatestStatusDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=StatusCriterion_Input) at PaymentRequestSearchDV.pcf: line 27, column 51
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Status = (__VALUE_TO_SET as typekey.PaymentRequestStatus)
    }
    
    // 'onChange' attribute on PostOnChange at PaymentRequestSearchDV.pcf: line 49, column 54
    function onChange_18 () : void {
      blankMinimumAndMaximumFields()
    }
    
    // 'valueRange' attribute on RangeInput (id=MethodCriterion_Input) at PaymentRequestSearchDV.pcf: line 34, column 44
    function valueRange_14 () : java.lang.Object {
      return getPaymentMethodRange()
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at PaymentRequestSearchDV.pcf: line 21, column 39
    function valueRoot_5 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on RangeInput (id=MethodCriterion_Input) at PaymentRequestSearchDV.pcf: line 34, column 44
    function value_11 () : typekey.PaymentMethod {
      return searchCriteria.Method
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at PaymentRequestSearchDV.pcf: line 47, column 66
    function value_20 () : typekey.Currency {
      return searchCriteria.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at PaymentRequestSearchDV.pcf: line 57, column 43
    function value_26 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MinAmount
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at PaymentRequestSearchDV.pcf: line 21, column 39
    function value_3 () : java.lang.String {
      return searchCriteria.AccountNumber
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at PaymentRequestSearchDV.pcf: line 64, column 43
    function value_33 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MaxAmount
    }
    
    // 'value' attribute on DateInput (id=EarliestDraftDateCriterion_Input) at PaymentRequestSearchDV.pcf: line 71, column 51
    function value_39 () : java.util.Date {
      return searchCriteria.EarliestDraftDate
    }
    
    // 'value' attribute on DateInput (id=LatestDraftDateCriterion_Input) at PaymentRequestSearchDV.pcf: line 76, column 49
    function value_43 () : java.util.Date {
      return searchCriteria.LatestDraftDate
    }
    
    // 'value' attribute on DateInput (id=EarliestRequestDateCriterion_Input) at PaymentRequestSearchDV.pcf: line 81, column 53
    function value_47 () : java.util.Date {
      return searchCriteria.EarliestRequestDate
    }
    
    // 'value' attribute on DateInput (id=LatestRequestDateCriterion_Input) at PaymentRequestSearchDV.pcf: line 86, column 51
    function value_51 () : java.util.Date {
      return searchCriteria.LatestRequestDate
    }
    
    // 'value' attribute on DateInput (id=EarliestStatusDateCriterion_Input) at PaymentRequestSearchDV.pcf: line 91, column 52
    function value_55 () : java.util.Date {
      return searchCriteria.EarliestStatusDate
    }
    
    // 'value' attribute on DateInput (id=LatestStatusDateCriterion_Input) at PaymentRequestSearchDV.pcf: line 96, column 50
    function value_59 () : java.util.Date {
      return searchCriteria.LatestStatusDate
    }
    
    // 'value' attribute on TypeKeyInput (id=StatusCriterion_Input) at PaymentRequestSearchDV.pcf: line 27, column 51
    function value_7 () : typekey.PaymentRequestStatus {
      return searchCriteria.Status
    }
    
    // 'valueRange' attribute on RangeInput (id=MethodCriterion_Input) at PaymentRequestSearchDV.pcf: line 34, column 44
    function verifyValueRangeIsAllowedType_15 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=MethodCriterion_Input) at PaymentRequestSearchDV.pcf: line 34, column 44
    function verifyValueRangeIsAllowedType_15 ($$arg :  typekey.PaymentMethod[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=MethodCriterion_Input) at PaymentRequestSearchDV.pcf: line 34, column 44
    function verifyValueRange_16 () : void {
      var __valueRangeArg = getPaymentMethodRange()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_15(__valueRangeArg)
    }
    
    // 'visible' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at PaymentRequestSearchDV.pcf: line 47, column 66
    function visible_19 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    property get searchCriteria () : gw.search.PaymentRequestSearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.search.PaymentRequestSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.PaymentRequestSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    function getPaymentMethodRange() : java.util.List<PaymentMethod> {
      var paymentMethodRange = new java.util.ArrayList<PaymentMethod>();
      //US169: Search Payment Requests, 12/17/2014, Vicente, Credit Card removed
      paymentMethodRange.add( PaymentMethod.TC_ACH );
      return paymentMethodRange;
    }
    
    function blankMinimumAndMaximumFields() {
      searchCriteria.MinAmount = null
      searchCriteria.MaxAmount = null
    }
    
    
  }
  
  
}