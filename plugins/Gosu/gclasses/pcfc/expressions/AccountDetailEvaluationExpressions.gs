package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailEvaluation.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailEvaluationExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailEvaluation.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailEvaluationExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=AccountDetailEvaluation) at AccountDetailEvaluation.pcf: line 9, column 75
    static function canVisit_2 (account :  Account) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctevalview
    }
    
    // 'def' attribute on InputSetRef at AccountDetailEvaluation.pcf: line 22, column 61
    function def_onEnter_0 (def :  pcf.AccountEvaluationInputSet) : void {
      def.onEnter(account, null)
    }
    
    // 'def' attribute on InputSetRef at AccountDetailEvaluation.pcf: line 22, column 61
    function def_refreshVariables_1 (def :  pcf.AccountEvaluationInputSet) : void {
      def.refreshVariables(account, null)
    }
    
    // Page (id=AccountDetailEvaluation) at AccountDetailEvaluation.pcf: line 9, column 75
    static function parent_3 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.AccountDetailEvaluation {
      return super.CurrentLocation as pcf.AccountDetailEvaluation
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  
}