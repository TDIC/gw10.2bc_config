package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.database.Query
@javax.annotation.Generated("config/web/pcf/troubleticket/CreateTroubleTicketWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreateTroubleTicketWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/CreateTroubleTicketWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreateTroubleTicketWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (relatedEntity :  KeyableBean) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=CreateTroubleTicketWizard) at CreateTroubleTicketWizard.pcf: line 11, column 36
    function afterCancel_15 () : void {
      navigateAfterCancel(relatedEntity)
    }
    
    // 'beforeCommit' attribute on Wizard (id=CreateTroubleTicketWizard) at CreateTroubleTicketWizard.pcf: line 11, column 36
    function beforeCommit_16 (pickedValue :  java.lang.Object) : void {
      TroubleTicket.Hold.checkForHoldAdditions();  TroubleTicket.assignTo_Ext(AssigneeHolder[0])
    }
    
    // 'canVisit' attribute on Wizard (id=CreateTroubleTicketWizard) at CreateTroubleTicketWizard.pcf: line 11, column 36
    static function canVisit_17 (relatedEntity :  KeyableBean) : java.lang.Boolean {
      return perm.TroubleTicket.create
    }
    
    // 'initialValue' attribute on Variable at CreateTroubleTicketWizard.pcf: line 20, column 41
    function initialValue_0 () : CreateTroubleTicketHelper {
      return new CreateTroubleTicketHelper()
    }
    
    // 'initialValue' attribute on Variable at CreateTroubleTicketWizard.pcf: line 24, column 29
    function initialValue_1 () : TroubleTicket {
      return CreateTroubleTicketHelper.createTroubleTicket(relatedEntity)
    }
    
    // 'initialValue' attribute on Variable at CreateTroubleTicketWizard.pcf: line 28, column 44
    function initialValue_2 () : gw.api.assignment.Assignee[] {
      return TroubleTicket.InitialAssigneeForPicker
    }
    
    // 'initialValue' attribute on Variable at CreateTroubleTicketWizard.pcf: line 32, column 52
    function initialValue_3 () : gw.troubleticket.TroubleTicketHelper {
      return new gw.troubleticket.TroubleTicketHelper(TroubleTicket)
    }
    
    // 'onExit' attribute on WizardStep (id=Step2) at CreateTroubleTicketWizard.pcf: line 43, column 92
    function onExit_6 () : void {
      wizardState.onExitCreateTroubleTicketEntitiesScreen()
    }
    
    // 'screen' attribute on WizardStep (id=Step4) at CreateTroubleTicketWizard.pcf: line 53, column 96
    function screen_onEnter_11 (def :  pcf.CreateTroubleTicketTransactionsScreen) : void {
      def.onEnter(TroubleTicket, AssigneeHolder, CreateTroubleTicketHelper)
    }
    
    // 'screen' attribute on WizardStep (id=Step5) at CreateTroubleTicketWizard.pcf: line 58, column 96
    function screen_onEnter_13 (def :  pcf.CreateTroubleTicketConfirmationScreen) : void {
      def.onEnter(TroubleTicket, AssigneeHolder, CreateTroubleTicketHelper)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at CreateTroubleTicketWizard.pcf: line 37, column 88
    function screen_onEnter_4 (def :  pcf.CreateTroubleTicketInfoScreen) : void {
      def.onEnter(TroubleTicket, AssigneeHolder, CreateTroubleTicketHelper)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at CreateTroubleTicketWizard.pcf: line 43, column 92
    function screen_onEnter_7 (def :  pcf.CreateTroubleTicketEntitiesScreen) : void {
      def.onEnter(TroubleTicket, AssigneeHolder, CreateTroubleTicketHelper)
    }
    
    // 'screen' attribute on WizardStep (id=Step3) at CreateTroubleTicketWizard.pcf: line 48, column 89
    function screen_onEnter_9 (def :  pcf.CreateTroubleTicketHoldsScreen) : void {
      def.onEnter(TroubleTicket, AssigneeHolder, CreateTroubleTicketHelper)
    }
    
    // 'screen' attribute on WizardStep (id=Step3) at CreateTroubleTicketWizard.pcf: line 48, column 89
    function screen_refreshVariables_10 (def :  pcf.CreateTroubleTicketHoldsScreen) : void {
      def.refreshVariables(TroubleTicket, AssigneeHolder, CreateTroubleTicketHelper)
    }
    
    // 'screen' attribute on WizardStep (id=Step4) at CreateTroubleTicketWizard.pcf: line 53, column 96
    function screen_refreshVariables_12 (def :  pcf.CreateTroubleTicketTransactionsScreen) : void {
      def.refreshVariables(TroubleTicket, AssigneeHolder, CreateTroubleTicketHelper)
    }
    
    // 'screen' attribute on WizardStep (id=Step5) at CreateTroubleTicketWizard.pcf: line 58, column 96
    function screen_refreshVariables_14 (def :  pcf.CreateTroubleTicketConfirmationScreen) : void {
      def.refreshVariables(TroubleTicket, AssigneeHolder, CreateTroubleTicketHelper)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at CreateTroubleTicketWizard.pcf: line 37, column 88
    function screen_refreshVariables_5 (def :  pcf.CreateTroubleTicketInfoScreen) : void {
      def.refreshVariables(TroubleTicket, AssigneeHolder, CreateTroubleTicketHelper)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at CreateTroubleTicketWizard.pcf: line 43, column 92
    function screen_refreshVariables_8 (def :  pcf.CreateTroubleTicketEntitiesScreen) : void {
      def.refreshVariables(TroubleTicket, AssigneeHolder, CreateTroubleTicketHelper)
    }
    
    // 'tabBar' attribute on Wizard (id=CreateTroubleTicketWizard) at CreateTroubleTicketWizard.pcf: line 11, column 36
    function tabBar_onEnter_18 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=CreateTroubleTicketWizard) at CreateTroubleTicketWizard.pcf: line 11, column 36
    function tabBar_refreshVariables_19 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    property get AssigneeHolder () : gw.api.assignment.Assignee[] {
      return getVariableValue("AssigneeHolder", 0) as gw.api.assignment.Assignee[]
    }
    
    property set AssigneeHolder ($arg :  gw.api.assignment.Assignee[]) {
      setVariableValue("AssigneeHolder", 0, $arg)
    }
    
    property get CreateTroubleTicketHelper () : CreateTroubleTicketHelper {
      return getVariableValue("CreateTroubleTicketHelper", 0) as CreateTroubleTicketHelper
    }
    
    property set CreateTroubleTicketHelper ($arg :  CreateTroubleTicketHelper) {
      setVariableValue("CreateTroubleTicketHelper", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.CreateTroubleTicketWizard {
      return super.CurrentLocation as pcf.CreateTroubleTicketWizard
    }
    
    property get TroubleTicket () : TroubleTicket {
      return getVariableValue("TroubleTicket", 0) as TroubleTicket
    }
    
    property set TroubleTicket ($arg :  TroubleTicket) {
      setVariableValue("TroubleTicket", 0, $arg)
    }
    
    property get relatedEntity () : KeyableBean {
      return getVariableValue("relatedEntity", 0) as KeyableBean
    }
    
    property set relatedEntity ($arg :  KeyableBean) {
      setVariableValue("relatedEntity", 0, $arg)
    }
    
    property get wizardState () : gw.troubleticket.TroubleTicketHelper {
      return getVariableValue("wizardState", 0) as gw.troubleticket.TroubleTicketHelper
    }
    
    property set wizardState ($arg :  gw.troubleticket.TroubleTicketHelper) {
      setVariableValue("wizardState", 0, $arg)
    }
    
    
    function navigateAfterCancel(item : KeyableBean) {
      if (item typeis Account) {
        item = Query.make(Account).compare("PublicID", Equals, item.PublicID).select().FirstResult
        AccountDetailTroubleTickets.go(item);
      } else if (item typeis PolicyPeriod) {
        item = Query.make(PolicyPeriod).compare("PublicID", Equals, item.PublicID).select().FirstResult
        PolicyDetailTroubleTickets.go(item, true, true);
      } else if (item typeis Producer) {
        item = Query.make(Producer).compare("PublicID", Equals, item.PublicID).select().FirstResult
        ProducerDetailTroubleTickets.go(item);
      }
    }
    
    
  }
  
  
}