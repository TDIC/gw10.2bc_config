package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/usersandsecurity/UsersAndSecurity.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class UsersAndSecurityExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/usersandsecurity/UsersAndSecurity.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class UsersAndSecurityExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'location' attribute on LocationGroup (id=UsersAndSecurity) at UsersAndSecurity.pcf: line 11, column 32
    function action_0 () : void {
      pcf.UserSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=UsersAndSecurity) at UsersAndSecurity.pcf: line 13, column 28
    function action_2 () : void {
      pcf.Groups.go()
    }
    
    // 'location' attribute on LocationGroup (id=UsersAndSecurity) at UsersAndSecurity.pcf: line 15, column 27
    function action_4 () : void {
      pcf.Roles.go()
    }
    
    // 'location' attribute on LocationGroup (id=UsersAndSecurity) at UsersAndSecurity.pcf: line 17, column 35
    function action_6 () : void {
      pcf.SecurityZones.go()
    }
    
    // 'location' attribute on LocationGroup (id=UsersAndSecurity) at UsersAndSecurity.pcf: line 19, column 44
    function action_8 () : void {
      pcf.AuthorityLimitProfiles.go()
    }
    
    // 'location' attribute on LocationGroup (id=UsersAndSecurity) at UsersAndSecurity.pcf: line 11, column 32
    function action_dest_1 () : pcf.api.Destination {
      return pcf.UserSearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=UsersAndSecurity) at UsersAndSecurity.pcf: line 13, column 28
    function action_dest_3 () : pcf.api.Destination {
      return pcf.Groups.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=UsersAndSecurity) at UsersAndSecurity.pcf: line 15, column 27
    function action_dest_5 () : pcf.api.Destination {
      return pcf.Roles.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=UsersAndSecurity) at UsersAndSecurity.pcf: line 17, column 35
    function action_dest_7 () : pcf.api.Destination {
      return pcf.SecurityZones.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=UsersAndSecurity) at UsersAndSecurity.pcf: line 19, column 44
    function action_dest_9 () : pcf.api.Destination {
      return pcf.AuthorityLimitProfiles.createDestination()
    }
    
    // 'canVisit' attribute on LocationGroup (id=UsersAndSecurity) at UsersAndSecurity.pcf: line 8, column 68
    static function canVisit_10 () : java.lang.Boolean {
      return perm.System.useradmin
    }
    
    // LocationGroup (id=UsersAndSecurity) at UsersAndSecurity.pcf: line 8, column 68
    static function firstVisitableChildDestinationMethod_14 () : pcf.api.Destination {
      var dest : pcf.api.Destination
      dest = pcf.UserSearch.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.Groups.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.Roles.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.SecurityZones.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AuthorityLimitProfiles.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      return null
    }
    
    // LocationGroup (id=UsersAndSecurity) at UsersAndSecurity.pcf: line 8, column 68
    static function parent_11 () : pcf.api.Destination {
      return pcf.Admin.createDestination()
    }
    
    // 'tabBar' attribute on LocationGroup (id=UsersAndSecurity) at UsersAndSecurity.pcf: line 8, column 68
    function tabBar_onEnter_12 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on LocationGroup (id=UsersAndSecurity) at UsersAndSecurity.pcf: line 8, column 68
    function tabBar_refreshVariables_13 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.UsersAndSecurity {
      return super.CurrentLocation as pcf.UsersAndSecurity
    }
    
    
  }
  
  
}