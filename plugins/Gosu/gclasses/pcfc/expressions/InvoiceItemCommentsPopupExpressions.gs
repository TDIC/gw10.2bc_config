package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/InvoiceItemCommentsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceItemCommentsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/InvoiceItemCommentsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceItemCommentsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (item :  InvoiceItem, agencyBillStatementView :  gw.api.web.invoice.AgencyBillStatementView) : int {
      return 0
    }
    
    // 'afterCommit' attribute on Popup (id=InvoiceItemCommentsPopup) at InvoiceItemCommentsPopup.pcf: line 10, column 76
    function afterCommit_5 (TopLocation :  pcf.api.Location) : void {
      agencyBillStatementView.PolicyPeriodInvoiceItems.get(0).InvoiceItemViews.get(0).updateItem(item)
    }
    
    // 'value' attribute on TextAreaInput (id=Comments_Input) at InvoiceItemCommentsPopup.pcf: line 33, column 42
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      item.Comments = (__VALUE_TO_SET as java.lang.String)
    }
    
    // EditButtons at InvoiceItemCommentsPopup.pcf: line 21, column 23
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'value' attribute on TextAreaInput (id=Comments_Input) at InvoiceItemCommentsPopup.pcf: line 33, column 42
    function valueRoot_3 () : java.lang.Object {
      return item
    }
    
    // 'value' attribute on TextAreaInput (id=Comments_Input) at InvoiceItemCommentsPopup.pcf: line 33, column 42
    function value_1 () : java.lang.String {
      return item.Comments
    }
    
    override property get CurrentLocation () : pcf.InvoiceItemCommentsPopup {
      return super.CurrentLocation as pcf.InvoiceItemCommentsPopup
    }
    
    property get agencyBillStatementView () : gw.api.web.invoice.AgencyBillStatementView {
      return getVariableValue("agencyBillStatementView", 0) as gw.api.web.invoice.AgencyBillStatementView
    }
    
    property set agencyBillStatementView ($arg :  gw.api.web.invoice.AgencyBillStatementView) {
      setVariableValue("agencyBillStatementView", 0, $arg)
    }
    
    property get item () : InvoiceItem {
      return getVariableValue("item", 0) as InvoiceItem
    }
    
    property set item ($arg :  InvoiceItem) {
      setVariableValue("item", 0, $arg)
    }
    
    
  }
  
  
}