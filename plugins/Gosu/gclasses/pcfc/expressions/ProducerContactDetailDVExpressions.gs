package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.LocaleUtil
uses gw.api.util.LocationUtil
@javax.annotation.Generated("config/web/pcf/producer/ProducerContactDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerContactDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerContactDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ProducerContactDetailDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at ProducerContactDetailDV.pcf: line 85, column 51
    function defaultSetter_41 (__VALUE_TO_SET :  java.lang.Object) : void {
      producerContactRole.Role = (__VALUE_TO_SET as typekey.ProducerRole)
    }
    
    // 'requestValidationExpression' attribute on TypeKeyCell (id=Role_Cell) at ProducerContactDetailDV.pcf: line 85, column 51
    function requestValidationExpression_39 (VALUE :  typekey.ProducerRole) : java.lang.Object {
      return VALUE != ProducerRole.TC_PRIMARY || hasPrimaryContact()== null ? null : DisplayKey.get("Web.ProducerContacts.ErrorMessage.ProducerCannotHaveMoreThanOnePrimaryContact", hasPrimaryContact())
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at ProducerContactDetailDV.pcf: line 85, column 51
    function valueRoot_42 () : java.lang.Object {
      return producerContactRole
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at ProducerContactDetailDV.pcf: line 85, column 51
    function value_40 () : typekey.ProducerRole {
      return producerContactRole.Role
    }
    
    property get producerContactRole () : entity.ProducerContactRole {
      return getIteratedValue(1) as entity.ProducerContactRole
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerContactDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerContactDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ProducerContactDetailDV.pcf: line 36, column 43
    function def_onEnter_10 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ProducerContactDetailDV.pcf: line 36, column 43
    function def_onEnter_12 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ProducerContactDetailDV.pcf: line 36, column 43
    function def_onEnter_14 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef at ProducerContactDetailDV.pcf: line 55, column 218
    function def_onEnter_32 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.BusinessPhoneOwner(new gw.api.phone.ContactPhoneDelegate(producerContact.Contact, Contact#WorkPhone), DisplayKey.get("Web.ContactDetailDV.Phone"), false))
    }
    
    // 'def' attribute on InputSetRef at ProducerContactDetailDV.pcf: line 29, column 98
    function def_onEnter_4 (def :  pcf.NameInputSet_Contact) : void {
      def.onEnter(new gw.api.name.BCNameOwner(producerContact.Contact))
    }
    
    // 'def' attribute on InputSetRef at ProducerContactDetailDV.pcf: line 29, column 98
    function def_onEnter_6 (def :  pcf.NameInputSet_Person) : void {
      def.onEnter(new gw.api.name.BCNameOwner(producerContact.Contact))
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ProducerContactDetailDV.pcf: line 36, column 43
    function def_refreshVariables_11 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ProducerContactDetailDV.pcf: line 36, column 43
    function def_refreshVariables_13 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at ProducerContactDetailDV.pcf: line 36, column 43
    function def_refreshVariables_15 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef at ProducerContactDetailDV.pcf: line 55, column 218
    function def_refreshVariables_33 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.BusinessPhoneOwner(new gw.api.phone.ContactPhoneDelegate(producerContact.Contact, Contact#WorkPhone), DisplayKey.get("Web.ContactDetailDV.Phone"), false))
    }
    
    // 'def' attribute on InputSetRef at ProducerContactDetailDV.pcf: line 29, column 98
    function def_refreshVariables_5 (def :  pcf.NameInputSet_Contact) : void {
      def.refreshVariables(new gw.api.name.BCNameOwner(producerContact.Contact))
    }
    
    // 'def' attribute on InputSetRef at ProducerContactDetailDV.pcf: line 29, column 98
    function def_refreshVariables_7 (def :  pcf.NameInputSet_Person) : void {
      def.refreshVariables(new gw.api.name.BCNameOwner(producerContact.Contact))
    }
    
    // 'value' attribute on TextInput (id=Email_Input) at ProducerContactDetailDV.pcf: line 60, column 56
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      producerContact.Contact.EmailAddress1 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'encryptionExpression' attribute on PrivacyInput (id=SSN_Input) at ProducerContactDetailDV.pcf: line 44, column 66
    function encryptionExpression_21 (VALUE :  java.lang.String) : java.lang.String {
      return gw.api.privacy.EncryptionMaskExpressions.maskTaxId(VALUE)
    }
    
    // 'initialValue' attribute on Variable at ProducerContactDetailDV.pcf: line 17, column 43
    function initialValue_0 () : gw.api.address.AddressOwner {
      return new gw.api.address.ContactAddressOwner(producerContact.Contact)
    }
    
    // 'mode' attribute on InputSetRef (id=globalAddressContainer) at ProducerContactDetailDV.pcf: line 36, column 43
    function mode_16 () : java.lang.Object {
      return gw.api.address.AddressCountrySettings.getSettings(addressOwner.SelectedCountry).PCFMode
    }
    
    // 'mode' attribute on InputSetRef at ProducerContactDetailDV.pcf: line 29, column 98
    function mode_8 () : java.lang.Object {
      return producerContact.Contact typeis Person ? "Person" : "Contact"
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at ProducerContactDetailDV.pcf: line 85, column 51
    function sortValue_38 (producerContactRole :  entity.ProducerContactRole) : java.lang.Object {
      return producerContactRole.Role
    }
    
    // 'toAdd' attribute on RowIterator at ProducerContactDetailDV.pcf: line 76, column 54
    function toAdd_44 (producerContactRole :  entity.ProducerContactRole) : void {
      producerContact.addToRoles(producerContactRole)
    }
    
    // 'toRemove' attribute on RowIterator at ProducerContactDetailDV.pcf: line 76, column 54
    function toRemove_45 (producerContactRole :  entity.ProducerContactRole) : void {
      producerContact.removeFromRoles(producerContactRole)
    }
    
    // 'value' attribute on TextInput (id=Type_Input) at ProducerContactDetailDV.pcf: line 24, column 61
    function valueRoot_2 () : java.lang.Object {
      return producerContact.Contact.Subtype
    }
    
    // 'value' attribute on PrivacyInput (id=SSN_Input) at ProducerContactDetailDV.pcf: line 44, column 66
    function valueRoot_20 () : java.lang.Object {
      return producerContact.Contact
    }
    
    // 'value' attribute on TextInput (id=Type_Input) at ProducerContactDetailDV.pcf: line 24, column 61
    function value_1 () : java.lang.String {
      return producerContact.Contact.Subtype.DisplayName
    }
    
    // 'value' attribute on PrivacyInput (id=SSN_Input) at ProducerContactDetailDV.pcf: line 44, column 66
    function value_19 () : java.lang.String {
      return producerContact.Contact.SSNOfficialID
    }
    
    // 'value' attribute on TextInput (id=FEIN_Input) at ProducerContactDetailDV.pcf: line 49, column 67
    function value_25 () : java.lang.String {
      return producerContact.Contact.FEINOfficialID
    }
    
    // 'value' attribute on TextInput (id=VendorNumber_Input) at ProducerContactDetailDV.pcf: line 53, column 55
    function value_29 () : java.lang.String {
      return producerContact.Contact.VendorNumber
    }
    
    // 'value' attribute on TextInput (id=Email_Input) at ProducerContactDetailDV.pcf: line 60, column 56
    function value_34 () : java.lang.String {
      return producerContact.Contact.EmailAddress1
    }
    
    // 'value' attribute on RowIterator at ProducerContactDetailDV.pcf: line 76, column 54
    function value_46 () : entity.ProducerContactRole[] {
      return producerContact.Roles
    }
    
    // 'visible' attribute on PrivacyInput (id=SSN_Input) at ProducerContactDetailDV.pcf: line 44, column 66
    function visible_18 () : java.lang.Boolean {
      return producerContact.Contact.SSNOfficialID != null
    }
    
    // 'visible' attribute on TextInput (id=FEIN_Input) at ProducerContactDetailDV.pcf: line 49, column 67
    function visible_24 () : java.lang.Boolean {
      return producerContact.Contact.FEINOfficialID != null
    }
    
    // 'visible' attribute on InputDivider at ProducerContactDetailDV.pcf: line 31, column 47
    function visible_9 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    property get addressOwner () : gw.api.address.AddressOwner {
      return getVariableValue("addressOwner", 0) as gw.api.address.AddressOwner
    }
    
    property set addressOwner ($arg :  gw.api.address.AddressOwner) {
      setVariableValue("addressOwner", 0, $arg)
    }
    
    property get isNew () : Boolean {
      return getRequireValue("isNew", 0) as Boolean
    }
    
    property set isNew ($arg :  Boolean) {
      setRequireValue("isNew", 0, $arg)
    }
    
    property get producerContact () : ProducerContact {
      return getRequireValue("producerContact", 0) as ProducerContact
    }
    
    property set producerContact ($arg :  ProducerContact) {
      setRequireValue("producerContact", 0, $arg)
    }
    
    
    function hasPrimaryContact() : String {
      var producer = producerContact.getProducer()
      for(c in producer.getContacts()){
        if(c != producerContact && c.PrimaryProducer == producer){
          return c.getContact().DisplayName
        }
      }
      return null
    }
    
    
  }
  
  
}