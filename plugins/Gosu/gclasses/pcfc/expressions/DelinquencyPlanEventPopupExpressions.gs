package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanEventPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyPlanEventPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanEventPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyPlanEventPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (dlnqPlanReason :  DelinquencyPlanReason) : int {
      return 0
    }
    
    // 'value' attribute on BooleanRadioInput (id=Automatic_Input) at DelinquencyPlanEventPopup.pcf: line 44, column 53
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      delinquencyPlanEvent.Automatic = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=OffsetDays_Input) at DelinquencyPlanEventPopup.pcf: line 50, column 44
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      delinquencyPlanEvent.OffsetDays = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeInput (id=TriggerBasis_Input) at DelinquencyPlanEventPopup.pcf: line 57, column 58
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      delinquencyPlanEvent.TriggerBasis = (__VALUE_TO_SET as typekey.DelinquencyTriggerBasis)
    }
    
    // 'value' attribute on TextInput (id=RelativeOrder_Input) at DelinquencyPlanEventPopup.pcf: line 63, column 44
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      delinquencyPlanEvent.RelativeOrder = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TypeKeyInput (id=Name_Input) at DelinquencyPlanEventPopup.pcf: line 35, column 55
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      delinquencyPlanEvent.EventName = (__VALUE_TO_SET as typekey.DelinquencyEventName)
    }
    
    // 'initialValue' attribute on Variable at DelinquencyPlanEventPopup.pcf: line 19, column 36
    function initialValue_0 () : DelinquencyPlanEvent {
      return newDelinquencyPlanEvent(dlnqPlanReason)
    }
    
    // EditButtons at DelinquencyPlanEventPopup.pcf: line 23, column 45
    function label_2 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'pickValue' attribute on EditButtons at DelinquencyPlanEventPopup.pcf: line 23, column 45
    function pickValue_1 () : DelinquencyPlanEvent {
      return delinquencyPlanEvent
    }
    
    // 'title' attribute on Popup (id=DelinquencyPlanEventPopup) at DelinquencyPlanEventPopup.pcf: line 10, column 101
    static function title_31 (dlnqPlanReason :  DelinquencyPlanReason) : java.lang.Object {
      return DisplayKey.get("Web.DelinquencyPlan.Title",  dlnqPlanReason.DelinquencyPlan )
    }
    
    // 'validationExpression' attribute on TypeKeyInput (id=Name_Input) at DelinquencyPlanEventPopup.pcf: line 35, column 55
    function validationExpression_3 () : java.lang.Object {
      return gw.api.web.delinquency.DelinquencyPlanUtil.validateDelinquencyPlanEventUnique( delinquencyPlanEvent )
    }
    
    // 'valueRange' attribute on RangeInput (id=TriggerBasis_Input) at DelinquencyPlanEventPopup.pcf: line 57, column 58
    function valueRange_23 () : java.lang.Object {
      return DelinquencyTriggerBasis.getTypeKeys(false)
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at DelinquencyPlanEventPopup.pcf: line 39, column 65
    function valueRoot_10 () : java.lang.Object {
      return delinquencyPlanEvent.EventName
    }
    
    // 'value' attribute on TypeKeyInput (id=Name_Input) at DelinquencyPlanEventPopup.pcf: line 35, column 55
    function valueRoot_6 () : java.lang.Object {
      return delinquencyPlanEvent
    }
    
    // 'value' attribute on BooleanRadioInput (id=Automatic_Input) at DelinquencyPlanEventPopup.pcf: line 44, column 53
    function value_12 () : java.lang.Boolean {
      return delinquencyPlanEvent.Automatic
    }
    
    // 'value' attribute on TextInput (id=OffsetDays_Input) at DelinquencyPlanEventPopup.pcf: line 50, column 44
    function value_16 () : java.lang.Integer {
      return delinquencyPlanEvent.OffsetDays
    }
    
    // 'value' attribute on RangeInput (id=TriggerBasis_Input) at DelinquencyPlanEventPopup.pcf: line 57, column 58
    function value_20 () : typekey.DelinquencyTriggerBasis {
      return delinquencyPlanEvent.TriggerBasis
    }
    
    // 'value' attribute on TextInput (id=RelativeOrder_Input) at DelinquencyPlanEventPopup.pcf: line 63, column 44
    function value_27 () : java.lang.Integer {
      return delinquencyPlanEvent.RelativeOrder
    }
    
    // 'value' attribute on TypeKeyInput (id=Name_Input) at DelinquencyPlanEventPopup.pcf: line 35, column 55
    function value_4 () : typekey.DelinquencyEventName {
      return delinquencyPlanEvent.EventName
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at DelinquencyPlanEventPopup.pcf: line 39, column 65
    function value_9 () : java.lang.String {
      return delinquencyPlanEvent.EventName.Description
    }
    
    // 'valueRange' attribute on RangeInput (id=TriggerBasis_Input) at DelinquencyPlanEventPopup.pcf: line 57, column 58
    function verifyValueRangeIsAllowedType_24 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=TriggerBasis_Input) at DelinquencyPlanEventPopup.pcf: line 57, column 58
    function verifyValueRangeIsAllowedType_24 ($$arg :  typekey.DelinquencyTriggerBasis[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=TriggerBasis_Input) at DelinquencyPlanEventPopup.pcf: line 57, column 58
    function verifyValueRange_25 () : void {
      var __valueRangeArg = DelinquencyTriggerBasis.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_24(__valueRangeArg)
    }
    
    override property get CurrentLocation () : pcf.DelinquencyPlanEventPopup {
      return super.CurrentLocation as pcf.DelinquencyPlanEventPopup
    }
    
    property get delinquencyPlanEvent () : DelinquencyPlanEvent {
      return getVariableValue("delinquencyPlanEvent", 0) as DelinquencyPlanEvent
    }
    
    property set delinquencyPlanEvent ($arg :  DelinquencyPlanEvent) {
      setVariableValue("delinquencyPlanEvent", 0, $arg)
    }
    
    property get dlnqPlanReason () : DelinquencyPlanReason {
      return getVariableValue("dlnqPlanReason", 0) as DelinquencyPlanReason
    }
    
    property set dlnqPlanReason ($arg :  DelinquencyPlanReason) {
      setVariableValue("dlnqPlanReason", 0, $arg)
    }
    
    
    /**
     */
          function newDelinquencyPlanEvent(planReason : DelinquencyPlanReason) : DelinquencyPlanEvent {
            var newPlanEvent = new DelinquencyPlanEvent()
            newPlanEvent.DelinquencyPlanReason = planReason
            return newPlanEvent
          }
        
    
    
  }
  
  
}