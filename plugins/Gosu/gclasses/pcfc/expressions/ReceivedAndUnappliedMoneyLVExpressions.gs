package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/ReceivedAndUnappliedMoneyLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ReceivedAndUnappliedMoneyLVExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/ReceivedAndUnappliedMoneyLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ReceivedAndUnappliedMoneyLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Agency_Cell) at ReceivedAndUnappliedMoneyLV.pcf: line 43, column 40
    function action_10 () : void {
      ProducerDetailPopup.push(receivedAndUnappliedMoney.Producer)
    }
    
    // 'action' attribute on TextCell (id=Agency_Cell) at ReceivedAndUnappliedMoneyLV.pcf: line 43, column 40
    function action_dest_11 () : pcf.api.Destination {
      return pcf.ProducerDetailPopup.createDestination(receivedAndUnappliedMoney.Producer)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=amount_Cell) at ReceivedAndUnappliedMoneyLV.pcf: line 36, column 53
    function currency_8 () : typekey.Currency {
      return receivedAndUnappliedMoney.Currency
    }
    
    // 'value' attribute on DateCell (id=agencyItemDate_Cell) at ReceivedAndUnappliedMoneyLV.pcf: line 30, column 59
    function valueRoot_4 () : java.lang.Object {
      return receivedAndUnappliedMoney
    }
    
    // 'value' attribute on TextCell (id=Agency_Cell) at ReceivedAndUnappliedMoneyLV.pcf: line 43, column 40
    function value_12 () : entity.Producer {
      return receivedAndUnappliedMoney.Producer
    }
    
    // 'value' attribute on DateCell (id=agencyItemDate_Cell) at ReceivedAndUnappliedMoneyLV.pcf: line 30, column 59
    function value_3 () : java.util.Date {
      return receivedAndUnappliedMoney.ReceivedDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amount_Cell) at ReceivedAndUnappliedMoneyLV.pcf: line 36, column 53
    function value_6 () : gw.pl.currency.MonetaryAmount {
      return receivedAndUnappliedMoney.Amount
    }
    
    property get receivedAndUnappliedMoney () : AgencyBillMoneyRcvd {
      return getIteratedValue(1) as AgencyBillMoneyRcvd
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/ReceivedAndUnappliedMoneyLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ReceivedAndUnappliedMoneyLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=agencyItemDate_Cell) at ReceivedAndUnappliedMoneyLV.pcf: line 30, column 59
    function sortValue_0 (receivedAndUnappliedMoney :  AgencyBillMoneyRcvd) : java.lang.Object {
      return receivedAndUnappliedMoney.ReceivedDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amount_Cell) at ReceivedAndUnappliedMoneyLV.pcf: line 36, column 53
    function sortValue_1 (receivedAndUnappliedMoney :  AgencyBillMoneyRcvd) : java.lang.Object {
      return receivedAndUnappliedMoney.Amount
    }
    
    // 'value' attribute on TextCell (id=Agency_Cell) at ReceivedAndUnappliedMoneyLV.pcf: line 43, column 40
    function sortValue_2 (receivedAndUnappliedMoney :  AgencyBillMoneyRcvd) : java.lang.Object {
      return receivedAndUnappliedMoney.Producer
    }
    
    // 'value' attribute on RowIterator at ReceivedAndUnappliedMoneyLV.pcf: line 23, column 87
    function value_15 () : gw.api.database.IQueryBeanResult<entity.PaymentMoneyReceived> {
      return unappliedPayments
    }
    
    // 'type' attribute on RowIterator at ReceivedAndUnappliedMoneyLV.pcf: line 23, column 87
    function verifyIteratorType_16 () : void {
      var entry : entity.PaymentMoneyReceived = null
      var typedEntry : AgencyBillMoneyRcvd
      // If this cast fails to compile then the type specified by the 'type' attribute on
      // 'RowIterator' is not compatible with the member type of the 'valueType' attribute
      typedEntry = entry as AgencyBillMoneyRcvd
    }
    
    property get unappliedPayments () : gw.api.database.IQueryBeanResult<PaymentMoneyReceived> {
      return getRequireValue("unappliedPayments", 0) as gw.api.database.IQueryBeanResult<PaymentMoneyReceived>
    }
    
    property set unappliedPayments ($arg :  gw.api.database.IQueryBeanResult<PaymentMoneyReceived>) {
      setRequireValue("unappliedPayments", 0, $arg)
    }
    
    
  }
  
  
}