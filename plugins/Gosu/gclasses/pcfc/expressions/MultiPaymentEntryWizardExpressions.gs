package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/MultiPaymentEntryWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class MultiPaymentEntryWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/MultiPaymentEntryWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class MultiPaymentEntryWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (currency :  Currency) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=MultiPaymentEntryWizard) at MultiPaymentEntryWizard.pcf: line 12, column 29
    function afterCancel_6 () : void {
      DesktopGroup.go()
    }
    
    // 'afterCancel' attribute on Wizard (id=MultiPaymentEntryWizard) at MultiPaymentEntryWizard.pcf: line 12, column 29
    function afterCancel_dest_7 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'afterFinish' attribute on Wizard (id=MultiPaymentEntryWizard) at MultiPaymentEntryWizard.pcf: line 12, column 29
    function afterFinish_13 () : void {
      DesktopGroup.go()
    }
    
    // 'afterFinish' attribute on Wizard (id=MultiPaymentEntryWizard) at MultiPaymentEntryWizard.pcf: line 12, column 29
    function afterFinish_dest_14 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'beforeCommit' attribute on Wizard (id=MultiPaymentEntryWizard) at MultiPaymentEntryWizard.pcf: line 12, column 29
    function beforeCommit_8 (pickedValue :  java.lang.Object) : void {
      newMultiPayment.createPayments()
    }
    
    // 'canVisit' attribute on Wizard (id=MultiPaymentEntryWizard) at MultiPaymentEntryWizard.pcf: line 12, column 29
    static function canVisit_9 (currency :  Currency) : java.lang.Boolean {
      return perm.DirectBillMoneyRcvd.pmntmanmultproc
    }
    
    // 'initialValue' attribute on Variable at MultiPaymentEntryWizard.pcf: line 18, column 31
    function initialValue_0 () : NewMultiPayment {
      return createNewMultiPayment()
    }
    
    // 'onExit' attribute on WizardStep (id=EnterInformation) at MultiPaymentEntryWizard.pcf: line 26, column 95
    function onExit_1 () : void {
      validateTotal_TDIC()
    }
    
    // 'parent' attribute on Wizard (id=MultiPaymentEntryWizard) at MultiPaymentEntryWizard.pcf: line 12, column 29
    static function parent_10 (currency :  Currency) : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'screen' attribute on WizardStep (id=EnterInformation) at MultiPaymentEntryWizard.pcf: line 26, column 95
    function screen_onEnter_2 (def :  pcf.NewMultiPaymentScreen) : void {
      def.onEnter(newMultiPayment, currency, true)
    }
    
    // 'screen' attribute on WizardStep (id=Confirmation) at MultiPaymentEntryWizard.pcf: line 30, column 91
    function screen_onEnter_4 (def :  pcf.NewMultiPaymentScreen) : void {
      def.onEnter(newMultiPayment, currency, false)
    }
    
    // 'screen' attribute on WizardStep (id=EnterInformation) at MultiPaymentEntryWizard.pcf: line 26, column 95
    function screen_refreshVariables_3 (def :  pcf.NewMultiPaymentScreen) : void {
      def.refreshVariables(newMultiPayment, currency, true)
    }
    
    // 'screen' attribute on WizardStep (id=Confirmation) at MultiPaymentEntryWizard.pcf: line 30, column 91
    function screen_refreshVariables_5 (def :  pcf.NewMultiPaymentScreen) : void {
      def.refreshVariables(newMultiPayment, currency, false)
    }
    
    // 'tabBar' attribute on Wizard (id=MultiPaymentEntryWizard) at MultiPaymentEntryWizard.pcf: line 12, column 29
    function tabBar_onEnter_11 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=MultiPaymentEntryWizard) at MultiPaymentEntryWizard.pcf: line 12, column 29
    function tabBar_refreshVariables_12 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.MultiPaymentEntryWizard {
      return super.CurrentLocation as pcf.MultiPaymentEntryWizard
    }
    
    property get currency () : Currency {
      return getVariableValue("currency", 0) as Currency
    }
    
    property set currency ($arg :  Currency) {
      setVariableValue("currency", 0, $arg)
    }
    
    property get newMultiPayment () : NewMultiPayment {
      return getVariableValue("newMultiPayment", 0) as NewMultiPayment
    }
    
    property set newMultiPayment ($arg :  NewMultiPayment) {
      setVariableValue("newMultiPayment", 0, $arg)
    }
    
    function createNewMultiPayment() : NewMultiPayment {
      var multiPayment = new NewMultiPayment();
      multiPayment.addEmptyPaymentRows()  
      return multiPayment;
    }
    
    /**
     * US75 - Validate control amount (total amount of payment lines)
     * 10/10/2014 Alvin Lee
     */
    function validateTotal_TDIC() : void {
      if (newMultiPayment.ControlAmountValidation != newMultiPayment.NonBlankPaymentEntries.sum( \ elt -> elt.Amount)) {
        throw new com.guidewire.pl.web.controller.UserDisplayableException(DisplayKey.get("TDIC.Validation.NewMultiPaymentScreen.TotalAmountValidationError"))
      }
    }
    
    
  }
  
  
}