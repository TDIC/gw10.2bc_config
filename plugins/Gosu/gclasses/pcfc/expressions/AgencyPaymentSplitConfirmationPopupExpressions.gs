package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyPaymentSplitConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyPaymentSplitConfirmationPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyPaymentSplitConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyPaymentSplitConfirmationPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (moneyReceived :  AgencyBillMoneyRcvd) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=AgencyPaymentSplitConfirmationPopup) at AgencyPaymentSplitConfirmationPopup.pcf: line 10, column 87
    function beforeCommit_3 (pickedValue :  java.lang.Object) : void {
      agencyPaymentSplit.splitPayment(CurrentLocation)
    }
    
    // 'def' attribute on ScreenRef at AgencyPaymentSplitConfirmationPopup.pcf: line 21, column 77
    function def_onEnter_1 (def :  pcf.AgencyPaymentSplitConfirmationScreen) : void {
      def.onEnter(agencyPaymentSplit, null)
    }
    
    // 'def' attribute on ScreenRef at AgencyPaymentSplitConfirmationPopup.pcf: line 21, column 77
    function def_refreshVariables_2 (def :  pcf.AgencyPaymentSplitConfirmationScreen) : void {
      def.refreshVariables(agencyPaymentSplit, null)
    }
    
    // 'initialValue' attribute on Variable at AgencyPaymentSplitConfirmationPopup.pcf: line 19, column 65
    function initialValue_0 () : gw.api.web.producer.agencybill.AgencySplitPayment {
      return createAgencyPaymentSplit()
    }
    
    override property get CurrentLocation () : pcf.AgencyPaymentSplitConfirmationPopup {
      return super.CurrentLocation as pcf.AgencyPaymentSplitConfirmationPopup
    }
    
    property get agencyPaymentSplit () : gw.api.web.producer.agencybill.AgencySplitPayment {
      return getVariableValue("agencyPaymentSplit", 0) as gw.api.web.producer.agencybill.AgencySplitPayment
    }
    
    property set agencyPaymentSplit ($arg :  gw.api.web.producer.agencybill.AgencySplitPayment) {
      setVariableValue("agencyPaymentSplit", 0, $arg)
    }
    
    property get moneyReceived () : AgencyBillMoneyRcvd {
      return getVariableValue("moneyReceived", 0) as AgencyBillMoneyRcvd
    }
    
    property set moneyReceived ($arg :  AgencyBillMoneyRcvd) {
      setVariableValue("moneyReceived", 0, $arg)
    }
    
    // Create non persistent AgencySplitPayment object used to manage the split of the payment in the UI
    function createAgencyPaymentSplit() : gw.api.web.producer.agencybill.AgencySplitPayment {
      if (moneyReceived.DistributedDenorm) {
        return new gw.api.web.producer.agencybill.AgencySplitPayment(moneyReceived.AgencyCyclePayment);
      }
      else {
        return new gw.api.web.producer.agencybill.AgencySplitPayment(moneyReceived);
      }
    }
    
    
  }
  
  
}