package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.document.SymbolProvider
uses gw.document.SimpleSymbol
uses gw.document.GosuCaseInsensitiveBackwardsCompatibleSymbolProvider
@javax.annotation.Generated("config/web/pcf/admin/activitypatterns/ActivityPatternDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ActivityPatternDVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/activitypatterns/ActivityPatternDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ActivityPatternDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyInput (id=Category_Input) at ActivityPatternDV.pcf: line 41, column 47
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      activityPattern.Category = (__VALUE_TO_SET as typekey.ActivityCategory)
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at ActivityPatternDV.pcf: line 25, column 42
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      activityPattern.Subject = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at ActivityPatternDV.pcf: line 47, column 39
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      activityPattern.Code = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Priority_Input) at ActivityPatternDV.pcf: line 54, column 39
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      activityPattern.Priority = (__VALUE_TO_SET as typekey.Priority)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Mandatory_Input) at ActivityPatternDV.pcf: line 60, column 44
    function defaultSetter_29 (__VALUE_TO_SET :  java.lang.Object) : void {
      activityPattern.Mandatory = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at ActivityPatternDV.pcf: line 66, column 46
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      activityPattern.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=AssignableQueue_Input) at ActivityPatternDV.pcf: line 73, column 38
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      activityPattern.AssignableQueue_Ext = (__VALUE_TO_SET as AssignableQueue)
    }
    
    // 'value' attribute on TextInput (id=TargetDays_Input) at ActivityPatternDV.pcf: line 84, column 61
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      activityPattern.TargetDays = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=TargetHours_Input) at ActivityPatternDV.pcf: line 91, column 61
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      activityPattern.TargetHours = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TypeKeyInput (id=TargetStartPoint_Input) at ActivityPatternDV.pcf: line 98, column 61
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      activityPattern.TargetStartPoint = (__VALUE_TO_SET as typekey.StartPointType)
    }
    
    // 'value' attribute on TextInput (id=ShortSubject_Input) at ActivityPatternDV.pcf: line 30, column 47
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      activityPattern.ShortSubject = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=TargetIncludeDays_Input) at ActivityPatternDV.pcf: line 105, column 61
    function defaultSetter_63 (__VALUE_TO_SET :  java.lang.Object) : void {
      activityPattern.TargetIncludeDays = (__VALUE_TO_SET as typekey.IncludeDaysType)
    }
    
    // 'value' attribute on TextInput (id=EscalationDays_Input) at ActivityPatternDV.pcf: line 113, column 40
    function defaultSetter_68 (__VALUE_TO_SET :  java.lang.Object) : void {
      activityPattern.EscalationDays = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=EscalationHours_Input) at ActivityPatternDV.pcf: line 119, column 40
    function defaultSetter_72 (__VALUE_TO_SET :  java.lang.Object) : void {
      activityPattern.EscalationHours = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TypeKeyInput (id=EscalationStartPoint_Input) at ActivityPatternDV.pcf: line 125, column 45
    function defaultSetter_76 (__VALUE_TO_SET :  java.lang.Object) : void {
      activityPattern.EscalationStartPt = (__VALUE_TO_SET as typekey.StartPointType)
    }
    
    // 'value' attribute on TypeKeyInput (id=EscalationIncludeDays_Input) at ActivityPatternDV.pcf: line 131, column 46
    function defaultSetter_80 (__VALUE_TO_SET :  java.lang.Object) : void {
      activityPattern.EscalationInclDays = (__VALUE_TO_SET as typekey.IncludeDaysType)
    }
    
    // 'editable' attribute on TypeKeyInput (id=Category_Input) at ActivityPatternDV.pcf: line 41, column 47
    function editable_12 () : java.lang.Boolean {
      return !activityPattern.SystemPattern
    }
    
    // 'editable' attribute on TextInput (id=Code_Input) at ActivityPatternDV.pcf: line 47, column 39
    function editable_18 () : java.lang.Boolean {
      return isNew
    }
    
    // 'initialValue' attribute on Variable at ActivityPatternDV.pcf: line 16, column 42
    function initialValue_0 () : gw.document.SymbolProvider {
      return createSymbolProvider()
    }
    
    // 'valueRange' attribute on RangeInput (id=AssignableQueue_Input) at ActivityPatternDV.pcf: line 73, column 38
    function valueRange_39 () : java.lang.Object {
      return gw.api.database.Query.make(AssignableQueue).select()
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at ActivityPatternDV.pcf: line 25, column 42
    function valueRoot_3 () : java.lang.Object {
      return activityPattern
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at ActivityPatternDV.pcf: line 25, column 42
    function value_1 () : java.lang.String {
      return activityPattern.Subject
    }
    
    // 'value' attribute on TypeKeyInput (id=Category_Input) at ActivityPatternDV.pcf: line 41, column 47
    function value_13 () : typekey.ActivityCategory {
      return activityPattern.Category
    }
    
    // 'value' attribute on TextInput (id=Code_Input) at ActivityPatternDV.pcf: line 47, column 39
    function value_19 () : java.lang.String {
      return activityPattern.Code
    }
    
    // 'value' attribute on TypeKeyInput (id=Priority_Input) at ActivityPatternDV.pcf: line 54, column 39
    function value_24 () : typekey.Priority {
      return activityPattern.Priority
    }
    
    // 'value' attribute on BooleanRadioInput (id=Mandatory_Input) at ActivityPatternDV.pcf: line 60, column 44
    function value_28 () : java.lang.Boolean {
      return activityPattern.Mandatory
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at ActivityPatternDV.pcf: line 66, column 46
    function value_32 () : java.lang.String {
      return activityPattern.Description
    }
    
    // 'value' attribute on RangeInput (id=AssignableQueue_Input) at ActivityPatternDV.pcf: line 73, column 38
    function value_36 () : AssignableQueue {
      return activityPattern.AssignableQueue_Ext
    }
    
    // 'value' attribute on TextInput (id=TargetDays_Input) at ActivityPatternDV.pcf: line 84, column 61
    function value_44 () : java.lang.Integer {
      return activityPattern.TargetDays
    }
    
    // 'value' attribute on TextInput (id=ShortSubject_Input) at ActivityPatternDV.pcf: line 30, column 47
    function value_5 () : java.lang.String {
      return activityPattern.ShortSubject
    }
    
    // 'value' attribute on TextInput (id=TargetHours_Input) at ActivityPatternDV.pcf: line 91, column 61
    function value_50 () : java.lang.Integer {
      return activityPattern.TargetHours
    }
    
    // 'value' attribute on TypeKeyInput (id=TargetStartPoint_Input) at ActivityPatternDV.pcf: line 98, column 61
    function value_56 () : typekey.StartPointType {
      return activityPattern.TargetStartPoint
    }
    
    // 'value' attribute on TypeKeyInput (id=TargetIncludeDays_Input) at ActivityPatternDV.pcf: line 105, column 61
    function value_62 () : typekey.IncludeDaysType {
      return activityPattern.TargetIncludeDays
    }
    
    // 'value' attribute on TextInput (id=EscalationDays_Input) at ActivityPatternDV.pcf: line 113, column 40
    function value_67 () : java.lang.Integer {
      return activityPattern.EscalationDays
    }
    
    // 'value' attribute on TextInput (id=EscalationHours_Input) at ActivityPatternDV.pcf: line 119, column 40
    function value_71 () : java.lang.Integer {
      return activityPattern.EscalationHours
    }
    
    // 'value' attribute on TypeKeyInput (id=EscalationStartPoint_Input) at ActivityPatternDV.pcf: line 125, column 45
    function value_75 () : typekey.StartPointType {
      return activityPattern.EscalationStartPt
    }
    
    // 'value' attribute on TypeKeyInput (id=EscalationIncludeDays_Input) at ActivityPatternDV.pcf: line 131, column 46
    function value_79 () : typekey.IncludeDaysType {
      return activityPattern.EscalationInclDays
    }
    
    // 'value' attribute on TypeKeyInput (id=Type_Input) at ActivityPatternDV.pcf: line 35, column 43
    function value_9 () : typekey.ActivityType {
      return activityPattern.Type
    }
    
    // 'valueRange' attribute on RangeInput (id=AssignableQueue_Input) at ActivityPatternDV.pcf: line 73, column 38
    function verifyValueRangeIsAllowedType_40 ($$arg :  AssignableQueue[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AssignableQueue_Input) at ActivityPatternDV.pcf: line 73, column 38
    function verifyValueRangeIsAllowedType_40 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AssignableQueue_Input) at ActivityPatternDV.pcf: line 73, column 38
    function verifyValueRangeIsAllowedType_40 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AssignableQueue_Input) at ActivityPatternDV.pcf: line 73, column 38
    function verifyValueRange_41 () : void {
      var __valueRangeArg = gw.api.database.Query.make(AssignableQueue).select()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_40(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=TargetDays_Input) at ActivityPatternDV.pcf: line 84, column 61
    function visible_43 () : java.lang.Boolean {
      return activityPattern.ActivityClass == TC_TASK
    }
    
    property get activityPattern () : ActivityPattern {
      return getRequireValue("activityPattern", 0) as ActivityPattern
    }
    
    property set activityPattern ($arg :  ActivityPattern) {
      setRequireValue("activityPattern", 0, $arg)
    }
    
    property get isNew () : boolean {
      return getRequireValue("isNew", 0) as java.lang.Boolean
    }
    
    property set isNew ($arg :  boolean) {
      setRequireValue("isNew", 0, $arg)
    }
    
    property get symbolProvider () : gw.document.SymbolProvider {
      return getVariableValue("symbolProvider", 0) as gw.document.SymbolProvider
    }
    
    property set symbolProvider ($arg :  gw.document.SymbolProvider) {
      setVariableValue("symbolProvider", 0, $arg)
    }
    
    
    
        function createSymbolProvider(): SymbolProvider {
          return new GosuCaseInsensitiveBackwardsCompatibleSymbolProvider({
             "Activity"->SimpleSymbol.PLACEHOLDER
          })
        }
    
        function getDisplayName(templateFilename : String) : String {
        if (templateFilename == null) {
          return null
        }
        var ets = gw.plugin.Plugins.get(gw.plugin.email.IEmailTemplateSource)
        return ets.getEmailTemplate(templateFilename).getName();
      }
          
        
    
    
  }
  
  
}