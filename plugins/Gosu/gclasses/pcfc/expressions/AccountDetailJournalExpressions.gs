package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.database.Query
@javax.annotation.Generated("config/web/pcf/account/AccountDetailJournal.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailJournalExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailJournal.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailJournalExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=AccountDetailJournal) at AccountDetailJournal.pcf: line 9, column 72
    static function canVisit_3 (account :  Account) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctjournalview
    }
    
    // 'def' attribute on ScreenRef at AccountDetailJournal.pcf: line 20, column 59
    function def_onEnter_1 (def :  pcf.JournalScreen) : void {
      def.onEnter(account, relatedTAccountOwner)
    }
    
    // 'def' attribute on ScreenRef at AccountDetailJournal.pcf: line 20, column 59
    function def_refreshVariables_2 (def :  pcf.JournalScreen) : void {
      def.refreshVariables(account, relatedTAccountOwner)
    }
    
    // 'initialValue' attribute on Variable at AccountDetailJournal.pcf: line 18, column 31
    function initialValue_0 () : TAccountOwner[] {
      return AllRelatedTAccountOwners
    }
    
    // Page (id=AccountDetailJournal) at AccountDetailJournal.pcf: line 9, column 72
    static function parent_4 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.AccountDetailJournal {
      return super.CurrentLocation as pcf.AccountDetailJournal
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get relatedTAccountOwner () : TAccountOwner[] {
      return getVariableValue("relatedTAccountOwner", 0) as TAccountOwner[]
    }
    
    property set relatedTAccountOwner ($arg :  TAccountOwner[]) {
      setVariableValue("relatedTAccountOwner", 0, $arg)
    }
    
    
    property get AllRelatedTAccountOwners() : TAccountOwner[]{
      var result = new java.util.ArrayList()
      result.add(account.Collateral)
      var q = Query.make(CollateralRequirement)
      q.compare("Collateral", Equals, account.Collateral)
      q.compare("Segregated", Equals, true)
      var segregatedCashCollatealRequirements = q.select()
      for (requirement in segregatedCashCollatealRequirements) {
        result.add(requirement)
      }
      return gw.api.upgrade.Coercions.makeArray<entity.TAccountOwner>(result)
    }
    
    
  }
  
  
}