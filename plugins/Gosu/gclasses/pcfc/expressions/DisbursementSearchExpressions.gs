package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DisbursementSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DisbursementSearchExpressions {
  @javax.annotation.Generated("config/web/pcf/search/DisbursementSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DisbursementSearchExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=DisbursementSearch) at DisbursementSearch.pcf: line 8, column 72
    static function canVisit_10 () : java.lang.Boolean {
      return perm.System.viewsearch and perm.System.disbsearch
    }
    
    // 'def' attribute on ScreenRef at DisbursementSearch.pcf: line 15, column 44
    function def_onEnter_1 (def :  pcf.DisbursementSearchScreen_AccountDisbursement) : void {
      def.onEnter(disbursementSubtypeHolder)
    }
    
    // 'def' attribute on ScreenRef at DisbursementSearch.pcf: line 15, column 44
    function def_onEnter_3 (def :  pcf.DisbursementSearchScreen_AgencyDisbursement) : void {
      def.onEnter(disbursementSubtypeHolder)
    }
    
    // 'def' attribute on ScreenRef at DisbursementSearch.pcf: line 15, column 44
    function def_onEnter_5 (def :  pcf.DisbursementSearchScreen_CollateralDisbursement) : void {
      def.onEnter(disbursementSubtypeHolder)
    }
    
    // 'def' attribute on ScreenRef at DisbursementSearch.pcf: line 15, column 44
    function def_onEnter_7 (def :  pcf.DisbursementSearchScreen_SuspenseDisbursement) : void {
      def.onEnter(disbursementSubtypeHolder)
    }
    
    // 'def' attribute on ScreenRef at DisbursementSearch.pcf: line 15, column 44
    function def_refreshVariables_2 (def :  pcf.DisbursementSearchScreen_AccountDisbursement) : void {
      def.refreshVariables(disbursementSubtypeHolder)
    }
    
    // 'def' attribute on ScreenRef at DisbursementSearch.pcf: line 15, column 44
    function def_refreshVariables_4 (def :  pcf.DisbursementSearchScreen_AgencyDisbursement) : void {
      def.refreshVariables(disbursementSubtypeHolder)
    }
    
    // 'def' attribute on ScreenRef at DisbursementSearch.pcf: line 15, column 44
    function def_refreshVariables_6 (def :  pcf.DisbursementSearchScreen_CollateralDisbursement) : void {
      def.refreshVariables(disbursementSubtypeHolder)
    }
    
    // 'def' attribute on ScreenRef at DisbursementSearch.pcf: line 15, column 44
    function def_refreshVariables_8 (def :  pcf.DisbursementSearchScreen_SuspenseDisbursement) : void {
      def.refreshVariables(disbursementSubtypeHolder)
    }
    
    // 'initialValue' attribute on Variable at DisbursementSearch.pcf: line 12, column 38
    function initialValue_0 () : typekey.Disbursement[] {
      return gw.api.web.disbursement.DisbursementUtil.getInitialDisbursementSubtypeHolder()
    }
    
    // 'mode' attribute on ScreenRef at DisbursementSearch.pcf: line 15, column 44
    function mode_9 () : java.lang.Object {
      return disbursementSubtypeHolder[0]
    }
    
    // Page (id=DisbursementSearch) at DisbursementSearch.pcf: line 8, column 72
    static function parent_11 () : pcf.api.Destination {
      return pcf.SearchGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.DisbursementSearch {
      return super.CurrentLocation as pcf.DisbursementSearch
    }
    
    property get disbursementSubtypeHolder () : typekey.Disbursement[] {
      return getVariableValue("disbursementSubtypeHolder", 0) as typekey.Disbursement[]
    }
    
    property set disbursementSubtypeHolder ($arg :  typekey.Disbursement[]) {
      setVariableValue("disbursementSubtypeHolder", 0, $arg)
    }
    
    
  }
  
  
}