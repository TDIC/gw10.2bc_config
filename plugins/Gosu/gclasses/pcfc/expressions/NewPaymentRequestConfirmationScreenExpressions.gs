package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/NewPaymentRequestConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewPaymentRequestConfirmationScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/NewPaymentRequestConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewPaymentRequestConfirmationScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=amount_Input) at NewPaymentRequestConfirmationScreen.pcf: line 52, column 42
    function currency_16 () : typekey.Currency {
      return account.Currency
    }
    
    // 'filter' attribute on TypeKeyInput (id=TypeKeyInput0_Input) at NewPaymentRequestConfirmationScreen.pcf: line 37, column 45
    function filter_6 (VALUE :  typekey.PaymentMethod, VALUES :  typekey.PaymentMethod[]) : java.lang.Boolean {
      return VALUE == TC_CREDITCARD or VALUE == TC_ACH or VALUE ==TC_WIRE
    }
    
    // 'initialValue' attribute on Variable at NewPaymentRequestConfirmationScreen.pcf: line 14, column 23
    function initialValue_0 () : Account {
      return paymentRequest.Account
    }
    
    // 'value' attribute on TextInput (id=PaymentInstrument_Input) at NewPaymentRequestConfirmationScreen.pcf: line 27, column 49
    function valueRoot_2 () : java.lang.Object {
      return paymentRequest
    }
    
    // 'value' attribute on TextInput (id=invoiceNumber_Input) at NewPaymentRequestConfirmationScreen.pcf: line 64, column 57
    function valueRoot_25 () : java.lang.Object {
      return paymentRequest.Invoice
    }
    
    // 'value' attribute on TypeKeyInput (id=TypeKeyInput0_Input) at NewPaymentRequestConfirmationScreen.pcf: line 37, column 45
    function valueRoot_5 () : java.lang.Object {
      return paymentRequest.PaymentInstrument
    }
    
    // 'value' attribute on TextInput (id=accountNumber_Input) at NewPaymentRequestConfirmationScreen.pcf: line 43, column 42
    function valueRoot_9 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on TextInput (id=PaymentInstrument_Input) at NewPaymentRequestConfirmationScreen.pcf: line 27, column 49
    function value_1 () : entity.PaymentInstrument {
      return paymentRequest.PaymentInstrument
    }
    
    // 'value' attribute on TextInput (id=accountName_Input) at NewPaymentRequestConfirmationScreen.pcf: line 47, column 49
    function value_11 () : java.lang.String {
      return account.AccountNameLocalized
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amount_Input) at NewPaymentRequestConfirmationScreen.pcf: line 52, column 42
    function value_14 () : gw.pl.currency.MonetaryAmount {
      return paymentRequest.Amount
    }
    
    // 'value' attribute on DateInput (id=paymentDate_Input) at NewPaymentRequestConfirmationScreen.pcf: line 56, column 45
    function value_18 () : java.util.Date {
      return paymentRequest.DraftDate
    }
    
    // 'value' attribute on DateInput (id=paymentDueDate_Input) at NewPaymentRequestConfirmationScreen.pcf: line 60, column 43
    function value_21 () : java.util.Date {
      return paymentRequest.DueDate
    }
    
    // 'value' attribute on TextInput (id=invoiceNumber_Input) at NewPaymentRequestConfirmationScreen.pcf: line 64, column 57
    function value_24 () : java.lang.String {
      return paymentRequest.Invoice.InvoiceNumber
    }
    
    // 'value' attribute on TypeKeyInput (id=TypeKeyInput0_Input) at NewPaymentRequestConfirmationScreen.pcf: line 37, column 45
    function value_4 () : typekey.PaymentMethod {
      return paymentRequest.PaymentInstrument.PaymentMethod
    }
    
    // 'value' attribute on TextInput (id=accountNumber_Input) at NewPaymentRequestConfirmationScreen.pcf: line 43, column 42
    function value_8 () : java.lang.String {
      return account.AccountNumber
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get payMethod () : PaymentMethod {
      return getVariableValue("payMethod", 0) as PaymentMethod
    }
    
    property set payMethod ($arg :  PaymentMethod) {
      setVariableValue("payMethod", 0, $arg)
    }
    
    property get paymentRequest () : PaymentRequest {
      return getRequireValue("paymentRequest", 0) as PaymentRequest
    }
    
    property set paymentRequest ($arg :  PaymentRequest) {
      setRequireValue("paymentRequest", 0, $arg)
    }
    
    
  }
  
  
}