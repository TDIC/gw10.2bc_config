package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/users/NewUserWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewUserWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/users/NewUserWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewUserWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=NewUserWizard) at NewUserWizard.pcf: line 9, column 24
    function afterCancel_8 () : void {
      Admin.go()
    }
    
    // 'afterCancel' attribute on Wizard (id=NewUserWizard) at NewUserWizard.pcf: line 9, column 24
    function afterCancel_dest_9 () : pcf.api.Destination {
      return pcf.Admin.createDestination()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewUserWizard) at NewUserWizard.pcf: line 9, column 24
    function afterFinish_13 () : void {
      UserDetailPage.go(user)
    }
    
    // 'afterFinish' attribute on Wizard (id=NewUserWizard) at NewUserWizard.pcf: line 9, column 24
    function afterFinish_dest_14 () : pcf.api.Destination {
      return pcf.UserDetailPage.createDestination(user)
    }
    
    // 'canVisit' attribute on Wizard (id=NewUserWizard) at NewUserWizard.pcf: line 9, column 24
    static function canVisit_10 () : java.lang.Boolean {
      return perm.User.create and perm.System.useradmin
    }
    
    // 'initialValue' attribute on Variable at NewUserWizard.pcf: line 15, column 20
    function initialValue_0 () : User {
      return createUser()
    }
    
    // 'onFirstEnter' attribute on WizardStep (id=ProfileStep) at NewUserWizard.pcf: line 26, column 76
    function onFirstEnter_3 () : void {
      user.initPrimaryAddress()
    }
    
    // 'screen' attribute on WizardStep (id=BasicsStep) at NewUserWizard.pcf: line 20, column 75
    function screen_onEnter_1 (def :  pcf.NewUserWizardBasicStepScreen) : void {
      def.onEnter(user)
    }
    
    // 'screen' attribute on WizardStep (id=ProfileStep) at NewUserWizard.pcf: line 26, column 76
    function screen_onEnter_4 (def :  pcf.NewUserWizardProfileStepScreen) : void {
      def.onEnter(user)
    }
    
    // 'screen' attribute on WizardStep (id=AuthorityLimitStep) at NewUserWizard.pcf: line 32, column 83
    function screen_onEnter_6 (def :  pcf.NewUserWizardAuthorityLimitStepScreen) : void {
      def.onEnter(user)
    }
    
    // 'screen' attribute on WizardStep (id=BasicsStep) at NewUserWizard.pcf: line 20, column 75
    function screen_refreshVariables_2 (def :  pcf.NewUserWizardBasicStepScreen) : void {
      def.refreshVariables(user)
    }
    
    // 'screen' attribute on WizardStep (id=ProfileStep) at NewUserWizard.pcf: line 26, column 76
    function screen_refreshVariables_5 (def :  pcf.NewUserWizardProfileStepScreen) : void {
      def.refreshVariables(user)
    }
    
    // 'screen' attribute on WizardStep (id=AuthorityLimitStep) at NewUserWizard.pcf: line 32, column 83
    function screen_refreshVariables_7 (def :  pcf.NewUserWizardAuthorityLimitStepScreen) : void {
      def.refreshVariables(user)
    }
    
    // 'tabBar' attribute on Wizard (id=NewUserWizard) at NewUserWizard.pcf: line 9, column 24
    function tabBar_onEnter_11 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=NewUserWizard) at NewUserWizard.pcf: line 9, column 24
    function tabBar_refreshVariables_12 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.NewUserWizard {
      return super.CurrentLocation as pcf.NewUserWizard
    }
    
    property get user () : User {
      return getVariableValue("user", 0) as User
    }
    
    property set user ($arg :  User) {
      setVariableValue("user", 0, $arg)
    }
    
    /**
     * US424/434 - Define/Maintain BC Users
     * 3/2/2015 Alvin Lee
     * 
     * All users created through the Admin UI should have the startup page default to Desktop. 
     */
    function createUser() : User {
      var newUser = new User()
      var settings = new UserSettings()
      settings.StartupPage = StartupPage.TC_DESKTOPGROUP
      newUser.UserSettings = settings
      return newUser
    }
    
    
  }
  
  
}