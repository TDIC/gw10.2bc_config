package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralRequirementSegregatePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CollateralRequirementSegregatePopupExpressions {
  @javax.annotation.Generated("config/web/pcf/collateral/CollateralRequirementSegregatePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CollateralRequirementSegregatePopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (collateralRequirement :  CollateralRequirement) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=CollateralRequirementSegregatePopup) at CollateralRequirementSegregatePopup.pcf: line 10, column 82
    function beforeCommit_18 (pickedValue :  java.lang.Object) : void {
      collateralRequirement.addToSegregated(amount)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=amountAvailable_Input) at CollateralRequirementSegregatePopup.pcf: line 33, column 70
    function currency_4 () : typekey.Currency {
      return collateralRequirement.Currency
    }
    
    // 'def' attribute on PanelRef at CollateralRequirementSegregatePopup.pcf: line 55, column 27
    function def_onEnter_16 (def :  pcf.CollateralRequirementDV) : void {
      def.onEnter(collateralRequirement, false)
    }
    
    // 'def' attribute on PanelRef at CollateralRequirementSegregatePopup.pcf: line 55, column 27
    function def_refreshVariables_17 (def :  pcf.CollateralRequirementDV) : void {
      def.refreshVariables(collateralRequirement, false)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amountInput_Input) at CollateralRequirementSegregatePopup.pcf: line 48, column 28
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'initialValue' attribute on Variable at CollateralRequirementSegregatePopup.pcf: line 19, column 45
    function initialValue_0 () : gw.pl.currency.MonetaryAmount {
      return 0bd.ofCurrency(collateralRequirement.Currency)
    }
    
    // EditButtons at CollateralRequirementSegregatePopup.pcf: line 24, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=amountInput_Input) at CollateralRequirementSegregatePopup.pcf: line 48, column 28
    function validationExpression_10 () : java.lang.Object {
      return amount > collateralRequirement.Collateral.TotalCashValue or amount.IsZero ?  DisplayKey.get("Web.CollateralRequirementSegregate.AmountError") : null
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amountAvailable_Input) at CollateralRequirementSegregatePopup.pcf: line 33, column 70
    function valueRoot_3 () : java.lang.Object {
      return collateralRequirement.Collateral
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amountSegregated_Input) at CollateralRequirementSegregatePopup.pcf: line 39, column 59
    function valueRoot_7 () : java.lang.Object {
      return collateralRequirement
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amountInput_Input) at CollateralRequirementSegregatePopup.pcf: line 48, column 28
    function value_11 () : gw.pl.currency.MonetaryAmount {
      return amount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amountAvailable_Input) at CollateralRequirementSegregatePopup.pcf: line 33, column 70
    function value_2 () : gw.pl.currency.MonetaryAmount {
      return collateralRequirement.Collateral.TotalCashValue
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amountSegregated_Input) at CollateralRequirementSegregatePopup.pcf: line 39, column 59
    function value_6 () : gw.pl.currency.MonetaryAmount {
      return collateralRequirement.TotalCashValue
    }
    
    override property get CurrentLocation () : pcf.CollateralRequirementSegregatePopup {
      return super.CurrentLocation as pcf.CollateralRequirementSegregatePopup
    }
    
    property get amount () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("amount", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set amount ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("amount", 0, $arg)
    }
    
    property get collateralRequirement () : CollateralRequirement {
      return getVariableValue("collateralRequirement", 0) as CollateralRequirement
    }
    
    property set collateralRequirement ($arg :  CollateralRequirement) {
      setVariableValue("collateralRequirement", 0, $arg)
    }
    
    
  }
  
  
}