package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/activityenhancement/groups/NewGroupDetailPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewGroupDetailPageExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/activityenhancement/groups/NewGroupDetailPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewGroupDetailPageExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (Group :  Group) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=NewGroupDetailPage) at NewGroupDetailPage.pcf: line 10, column 38
    function canEdit_10 () : java.lang.Boolean {
      return perm.Group.edit
    }
    
    // 'canVisit' attribute on Page (id=NewGroupDetailPage) at NewGroupDetailPage.pcf: line 10, column 38
    static function canVisit_11 (Group :  Group) : java.lang.Boolean {
      return perm.Group.edit
    }
    
    // 'def' attribute on PanelRef at NewGroupDetailPage.pcf: line 36, column 41
    function def_onEnter_4 (def :  pcf.GroupDetailDV) : void {
      def.onEnter(Group)
    }
    
    // 'def' attribute on PanelRef at NewGroupDetailPage.pcf: line 43, column 40
    function def_onEnter_6 (def :  pcf.GroupQueuesDV) : void {
      def.onEnter(Group)
    }
    
    // 'def' attribute on PanelRef (id=GroupRegionLVRef) at NewGroupDetailPage.pcf: line 53, column 35
    function def_onEnter_8 (def :  pcf.GroupRegionLV) : void {
      def.onEnter(Group)
    }
    
    // 'def' attribute on PanelRef at NewGroupDetailPage.pcf: line 36, column 41
    function def_refreshVariables_5 (def :  pcf.GroupDetailDV) : void {
      def.refreshVariables(Group)
    }
    
    // 'def' attribute on PanelRef at NewGroupDetailPage.pcf: line 43, column 40
    function def_refreshVariables_7 (def :  pcf.GroupQueuesDV) : void {
      def.refreshVariables(Group)
    }
    
    // 'def' attribute on PanelRef (id=GroupRegionLVRef) at NewGroupDetailPage.pcf: line 53, column 35
    function def_refreshVariables_9 (def :  pcf.GroupRegionLV) : void {
      def.refreshVariables(Group)
    }
    
    // 'initialValue' attribute on Variable at NewGroupDetailPage.pcf: line 19, column 21
    function initialValue_0 () : Group {
      return Group
    }
    
    // 'initialValue' attribute on Variable at NewGroupDetailPage.pcf: line 23, column 71
    function initialValue_1 () : gw.api.database.IQueryBeanResult<AssignableQueue> {
      return AssignableQueue.finder.findVisibleQueuesForUser(User.util.CurrentUser) as gw.api.database.IQueryBeanResult<AssignableQueue>
    }
    
    // 'parent' attribute on Page (id=NewGroupDetailPage) at NewGroupDetailPage.pcf: line 10, column 38
    static function parent_12 (Group :  Group) : pcf.api.Destination {
      return pcf.Admin.createDestination()
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at NewGroupDetailPage.pcf: line 28, column 66
    function toolbarButtonSet_onEnter_2 (def :  pcf.GroupDetailToolbarButtonSet) : void {
      def.onEnter(Group)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at NewGroupDetailPage.pcf: line 28, column 66
    function toolbarButtonSet_refreshVariables_3 (def :  pcf.GroupDetailToolbarButtonSet) : void {
      def.refreshVariables(Group)
    }
    
    property get AssignableQueues () : gw.api.database.IQueryBeanResult<AssignableQueue> {
      return getVariableValue("AssignableQueues", 0) as gw.api.database.IQueryBeanResult<AssignableQueue>
    }
    
    property set AssignableQueues ($arg :  gw.api.database.IQueryBeanResult<AssignableQueue>) {
      setVariableValue("AssignableQueues", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.NewGroupDetailPage {
      return super.CurrentLocation as pcf.NewGroupDetailPage
    }
    
    property get Group () : Group {
      return getVariableValue("Group", 0) as Group
    }
    
    property set Group ($arg :  Group) {
      setVariableValue("Group", 0, $arg)
    }
    
    property get Parent () : Group {
      return getVariableValue("Parent", 0) as Group
    }
    
    property set Parent ($arg :  Group) {
      setVariableValue("Parent", 0, $arg)
    }
    
    
  }
  
  
}