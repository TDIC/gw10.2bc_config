package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountOverview.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountOverviewExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountOverview.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountOverviewExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'location' attribute on LocationGroup (id=AccountOverview) at AccountOverview.pcf: line 16, column 43
    function action_0 () : void {
      pcf.AccountSummary.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountOverview) at AccountOverview.pcf: line 19, column 49
    function action_2 () : void {
      pcf.AccountDetailSummary.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountOverview) at AccountOverview.pcf: line 16, column 43
    function action_dest_1 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountOverview) at AccountOverview.pcf: line 19, column 49
    function action_dest_3 () : pcf.api.Destination {
      return pcf.AccountDetailSummary.createDestination(account)
    }
    
    // 'canVisit' attribute on LocationGroup (id=AccountOverview) at AccountOverview.pcf: line 8, column 67
    static function canVisit_4 (account :  Account) : java.lang.Boolean {
      return account.ViewableByCurrentUser
    }
    
    // LocationGroup (id=AccountOverview) at AccountOverview.pcf: line 8, column 67
    static function firstVisitableChildDestinationMethod_8 (account :  Account) : pcf.api.Destination {
      var dest : pcf.api.Destination
      dest = pcf.AccountSummary.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailSummary.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      return null
    }
    
    // LocationGroup (id=AccountOverview) at AccountOverview.pcf: line 8, column 67
    static function parent_5 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    // 'tabBar' attribute on LocationGroup (id=AccountOverview) at AccountOverview.pcf: line 8, column 67
    function tabBar_onEnter_6 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on LocationGroup (id=AccountOverview) at AccountOverview.pcf: line 8, column 67
    function tabBar_refreshVariables_7 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AccountOverview {
      return super.CurrentLocation as pcf.AccountOverview
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  
}