package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/CreateDisbursementConfirmScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreateDisbursementConfirmScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/disbursement/CreateDisbursementConfirmScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreateDisbursementConfirmScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at CreateDisbursementConfirmScreen.pcf: line 30, column 36
    function def_onEnter_2 (def :  pcf.DisbursementConfirmPanelSet_AccountDisbursement) : void {
      def.onEnter(disbursement)
    }
    
    // 'def' attribute on PanelRef at CreateDisbursementConfirmScreen.pcf: line 30, column 36
    function def_onEnter_4 (def :  pcf.DisbursementConfirmPanelSet_default) : void {
      def.onEnter(disbursement)
    }
    
    // 'def' attribute on PanelRef at CreateDisbursementConfirmScreen.pcf: line 32, column 75
    function def_onEnter_7 (def :  pcf.CreateDisbursementWizardApprovalActivityDV) : void {
      def.onEnter(approvalActivity)
    }
    
    // 'def' attribute on PanelRef at CreateDisbursementConfirmScreen.pcf: line 30, column 36
    function def_refreshVariables_3 (def :  pcf.DisbursementConfirmPanelSet_AccountDisbursement) : void {
      def.refreshVariables(disbursement)
    }
    
    // 'def' attribute on PanelRef at CreateDisbursementConfirmScreen.pcf: line 30, column 36
    function def_refreshVariables_5 (def :  pcf.DisbursementConfirmPanelSet_default) : void {
      def.refreshVariables(disbursement)
    }
    
    // 'def' attribute on PanelRef at CreateDisbursementConfirmScreen.pcf: line 32, column 75
    function def_refreshVariables_8 (def :  pcf.CreateDisbursementWizardApprovalActivityDV) : void {
      def.refreshVariables(approvalActivity)
    }
    
    // 'initialValue' attribute on Variable at CreateDisbursementConfirmScreen.pcf: line 13, column 39
    function initialValue_0 () : entity.DisbApprActivity {
      return disbursement.getOpenApprovalActivity()
    }
    
    // 'mode' attribute on PanelRef at CreateDisbursementConfirmScreen.pcf: line 30, column 36
    function mode_6 () : java.lang.Object {
      return disbursement.Subtype
    }
    
    // 'visible' attribute on AlertBar (id=ApprovalActivityAlertBar) at CreateDisbursementConfirmScreen.pcf: line 20, column 43
    function visible_1 () : java.lang.Boolean {
      return approvalActivity != null
    }
    
    property get approvalActivity () : entity.DisbApprActivity {
      return getVariableValue("approvalActivity", 0) as entity.DisbApprActivity
    }
    
    property set approvalActivity ($arg :  entity.DisbApprActivity) {
      setVariableValue("approvalActivity", 0, $arg)
    }
    
    property get disbursement () : Disbursement {
      return getRequireValue("disbursement", 0) as Disbursement
    }
    
    property set disbursement ($arg :  Disbursement) {
      setRequireValue("disbursement", 0, $arg)
    }
    
    
  }
  
  
}