package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewWriteoffReversalWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewWriteoffReversalWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=NewWriteoffReversalWizard) at NewWriteoffReversalWizard.pcf: line 12, column 36
    function afterCancel_7 () : void {
      DesktopGroup.go()
    }
    
    // 'afterCancel' attribute on Wizard (id=NewWriteoffReversalWizard) at NewWriteoffReversalWizard.pcf: line 12, column 36
    function afterCancel_dest_8 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewWriteoffReversalWizard) at NewWriteoffReversalWizard.pcf: line 12, column 36
    function afterFinish_12 () : void {
      DesktopGroup.go()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewWriteoffReversalWizard) at NewWriteoffReversalWizard.pcf: line 12, column 36
    function afterFinish_dest_13 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'allowNext' attribute on WizardStep (id=Step1) at NewWriteoffReversalWizard.pcf: line 25, column 95
    function allowNext_1 () : java.lang.Boolean {
      return reversal.Writeoff != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=NewWriteoffReversalWizard) at NewWriteoffReversalWizard.pcf: line 12, column 36
    function beforeCommit_9 (pickedValue :  java.lang.Object) : void {
      reversal.reverse()
    }
    
    // 'initialValue' attribute on Variable at NewWriteoffReversalWizard.pcf: line 18, column 32
    function initialValue_0 () : WriteoffReversal {
      return new WriteoffReversal()
    }
    
    // 'onExit' attribute on WizardStep (id=Step1) at NewWriteoffReversalWizard.pcf: line 25, column 95
    function onExit_2 () : void {
      reversal.initiateApprovalActivityIfUserLacksAuthority()
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at NewWriteoffReversalWizard.pcf: line 25, column 95
    function screen_onEnter_3 (def :  pcf.NewWriteoffReversalWriteoffSearchScreen) : void {
      def.onEnter(reversal, null)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at NewWriteoffReversalWizard.pcf: line 30, column 93
    function screen_onEnter_5 (def :  pcf.NewWriteoffReversalConfirmationScreen) : void {
      def.onEnter(reversal)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at NewWriteoffReversalWizard.pcf: line 25, column 95
    function screen_refreshVariables_4 (def :  pcf.NewWriteoffReversalWriteoffSearchScreen) : void {
      def.refreshVariables(reversal, null)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at NewWriteoffReversalWizard.pcf: line 30, column 93
    function screen_refreshVariables_6 (def :  pcf.NewWriteoffReversalConfirmationScreen) : void {
      def.refreshVariables(reversal)
    }
    
    // 'tabBar' attribute on Wizard (id=NewWriteoffReversalWizard) at NewWriteoffReversalWizard.pcf: line 12, column 36
    function tabBar_onEnter_10 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=NewWriteoffReversalWizard) at NewWriteoffReversalWizard.pcf: line 12, column 36
    function tabBar_refreshVariables_11 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.NewWriteoffReversalWizard {
      return super.CurrentLocation as pcf.NewWriteoffReversalWizard
    }
    
    property get reversal () : WriteoffReversal {
      return getVariableValue("reversal", 0) as WriteoffReversal
    }
    
    property set reversal ($arg :  WriteoffReversal) {
      setVariableValue("reversal", 0, $arg)
    }
    
    
  }
  
  
}