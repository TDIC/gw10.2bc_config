package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/TransferDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransferDetailsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/TransferDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransferDetailsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at TransferDetailsScreen.pcf: line 15, column 57
    function def_onEnter_0 (def :  pcf.TransferDetailsDV) : void {
      def.onEnter(fundsTransferUtil, true)
    }
    
    // 'def' attribute on PanelRef at TransferDetailsScreen.pcf: line 15, column 57
    function def_refreshVariables_1 (def :  pcf.TransferDetailsDV) : void {
      def.refreshVariables(fundsTransferUtil, true)
    }
    
    property get fundsTransferUtil () : gw.api.web.transaction.FundsTransferUtil {
      return getRequireValue("fundsTransferUtil", 0) as gw.api.web.transaction.FundsTransferUtil
    }
    
    property set fundsTransferUtil ($arg :  gw.api.web.transaction.FundsTransferUtil) {
      setRequireValue("fundsTransferUtil", 0, $arg)
    }
    
    
  }
  
  
}