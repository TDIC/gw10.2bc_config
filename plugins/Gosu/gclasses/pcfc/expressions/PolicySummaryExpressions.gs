package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicySummary.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicySummaryExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicySummary.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicySummaryExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'def' attribute on ScreenRef at PolicySummary.pcf: line 14, column 46
    function def_onEnter_0 (def :  pcf.PolicySummaryScreen) : void {
      def.onEnter(plcyPeriod)
    }
    
    // 'def' attribute on ScreenRef at PolicySummary.pcf: line 14, column 46
    function def_refreshVariables_1 (def :  pcf.PolicySummaryScreen) : void {
      def.refreshVariables(plcyPeriod)
    }
    
    // Page (id=PolicySummary) at PolicySummary.pcf: line 7, column 65
    static function parent_2 (plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(plcyPeriod)
    }
    
    override property get CurrentLocation () : pcf.PolicySummary {
      return super.CurrentLocation as pcf.PolicySummary
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 0, $arg)
    }
    
    
  }
  
  
}