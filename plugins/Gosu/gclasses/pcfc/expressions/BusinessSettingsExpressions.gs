package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/businesssettings/BusinessSettings.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BusinessSettingsExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/BusinessSettings.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BusinessSettingsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 10, column 38
    function action_0 () : void {
      pcf.ActivityPatterns.go()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 20, column 40
    function action_10 () : void {
      pcf.CollectionAgencies.go()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 22, column 37
    function action_12 () : void {
      pcf.CommissionPlans.go()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 24, column 38
    function action_14 () : void {
      pcf.DelinquencyPlans.go()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 26, column 40
    function action_16 () : void {
      pcf.TDIC_GLIntegration.go()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 28, column 30
    function action_18 () : void {
      pcf.Holidays.go()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 12, column 37
    function action_2 () : void {
      pcf.AgencyBillPlans.go()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 30, column 44
    function action_20 () : void {
      pcf.PaymentAllocationPlans.go()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 32, column 34
    function action_22 () : void {
      pcf.PaymentPlans.go()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 34, column 40
    function action_24 () : void {
      pcf.ReturnPremiumPlans.go()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 36, column 39
    function action_26 () : void {
      pcf.ClearOpenHandlers.go()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 14, column 34
    function action_4 () : void {
      pcf.BillingPlans.go()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 16, column 50
    function action_6 () : void {
      pcf.ChargeBreakdownCategoryTypes.go()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 18, column 36
    function action_8 () : void {
      pcf.ChargePatterns.go()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 10, column 38
    function action_dest_1 () : pcf.api.Destination {
      return pcf.ActivityPatterns.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 20, column 40
    function action_dest_11 () : pcf.api.Destination {
      return pcf.CollectionAgencies.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 22, column 37
    function action_dest_13 () : pcf.api.Destination {
      return pcf.CommissionPlans.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 24, column 38
    function action_dest_15 () : pcf.api.Destination {
      return pcf.DelinquencyPlans.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 26, column 40
    function action_dest_17 () : pcf.api.Destination {
      return pcf.TDIC_GLIntegration.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 28, column 30
    function action_dest_19 () : pcf.api.Destination {
      return pcf.Holidays.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 30, column 44
    function action_dest_21 () : pcf.api.Destination {
      return pcf.PaymentAllocationPlans.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 32, column 34
    function action_dest_23 () : pcf.api.Destination {
      return pcf.PaymentPlans.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 34, column 40
    function action_dest_25 () : pcf.api.Destination {
      return pcf.ReturnPremiumPlans.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 36, column 39
    function action_dest_27 () : pcf.api.Destination {
      return pcf.ClearOpenHandlers.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 12, column 37
    function action_dest_3 () : pcf.api.Destination {
      return pcf.AgencyBillPlans.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 14, column 34
    function action_dest_5 () : pcf.api.Destination {
      return pcf.BillingPlans.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 16, column 50
    function action_dest_7 () : pcf.api.Destination {
      return pcf.ChargeBreakdownCategoryTypes.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 18, column 36
    function action_dest_9 () : pcf.api.Destination {
      return pcf.ChargePatterns.createDestination()
    }
    
    // 'canVisit' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 8, column 68
    static function canVisit_28 () : java.lang.Boolean {
      return perm.System.businessadmin
    }
    
    // LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 8, column 68
    static function firstVisitableChildDestinationMethod_32 () : pcf.api.Destination {
      var dest : pcf.api.Destination
      dest = pcf.ActivityPatterns.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AgencyBillPlans.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.BillingPlans.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ChargeBreakdownCategoryTypes.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ChargePatterns.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.CollectionAgencies.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.CommissionPlans.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.DelinquencyPlans.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.TDIC_GLIntegration.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.Holidays.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PaymentAllocationPlans.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PaymentPlans.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ReturnPremiumPlans.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.ClearOpenHandlers.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      return null
    }
    
    // LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 8, column 68
    static function parent_29 () : pcf.api.Destination {
      return pcf.Admin.createDestination()
    }
    
    // 'tabBar' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 8, column 68
    function tabBar_onEnter_30 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on LocationGroup (id=BusinessSettings) at BusinessSettings.pcf: line 8, column 68
    function tabBar_refreshVariables_31 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.BusinessSettings {
      return super.CurrentLocation as pcf.BusinessSettings
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/BusinessSettings.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationGroupMenuItemExpressionsImpl extends BusinessSettingsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    
  }
  
  
}