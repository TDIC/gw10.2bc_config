package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/CreditReversalCreditSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreditReversalCreditSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/CreditReversalCreditSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreditReversalCreditSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at CreditReversalCreditSearchScreen.pcf: line 16, column 57
    function initialValue_0 () : gw.api.web.account.AccountSearchConverter {
      return new gw.api.web.account.AccountSearchConverter()
    }
    
    // 'initialValue' attribute on Variable at CreditReversalCreditSearchScreen.pcf: line 20, column 23
    function initialValue_1 () : Boolean {
      return account == null
    }
    
    // 'value' attribute on TextInput (id=Credit_Input) at CreditReversalCreditSearchScreen.pcf: line 32, column 38
    function valueRoot_3 () : java.lang.Object {
      return creditReversal
    }
    
    // 'value' attribute on TextInput (id=Credit_Input) at CreditReversalCreditSearchScreen.pcf: line 32, column 38
    function value_2 () : entity.Credit {
      return creditReversal.Credit
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get accountEditable () : Boolean {
      return getVariableValue("accountEditable", 0) as Boolean
    }
    
    property set accountEditable ($arg :  Boolean) {
      setVariableValue("accountEditable", 0, $arg)
    }
    
    property get accountSearchConverter () : gw.api.web.account.AccountSearchConverter {
      return getVariableValue("accountSearchConverter", 0) as gw.api.web.account.AccountSearchConverter
    }
    
    property set accountSearchConverter ($arg :  gw.api.web.account.AccountSearchConverter) {
      setVariableValue("accountSearchConverter", 0, $arg)
    }
    
    property get creditReversal () : CreditReversal {
      return getRequireValue("creditReversal", 0) as CreditReversal
    }
    
    property set creditReversal ($arg :  CreditReversal) {
      setRequireValue("creditReversal", 0, $arg)
    }
    
    function initializeSearchCriteria() : CreditSearchCriteria {
      var criteria = new CreditSearchCriteria();
      
      if (!accountEditable) {
        criteria.Account = account;
        criteria.Currency = account.Currency
      }
      return criteria;
    }
    
    function blankMinimumAndMaximumFields(searchCriteria : CreditSearchCriteria) {
      searchCriteria.MinAmount = null
      searchCriteria.MaxAmount = null
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/CreditReversalCreditSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=Select) at CreditReversalCreditSearchScreen.pcf: line 139, column 54
    function action_53 () : void {
      creditReversal.setCreditAndAddToBundle( credit ); (CurrentLocation as pcf.api.Wizard).next();
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at CreditReversalCreditSearchScreen.pcf: line 158, column 44
    function currency_61 () : typekey.Currency {
      return credit.Currency
    }
    
    // 'value' attribute on DateCell (id=CreditDate_Cell) at CreditReversalCreditSearchScreen.pcf: line 145, column 48
    function valueRoot_55 () : java.lang.Object {
      return credit
    }
    
    // 'value' attribute on DateCell (id=CreditDate_Cell) at CreditReversalCreditSearchScreen.pcf: line 145, column 48
    function value_54 () : java.util.Date {
      return credit.CreateTime
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at CreditReversalCreditSearchScreen.pcf: line 151, column 48
    function value_57 () : entity.Credit {
      return credit
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at CreditReversalCreditSearchScreen.pcf: line 158, column 44
    function value_59 () : gw.pl.currency.MonetaryAmount {
      return credit.Amount
    }
    
    // 'value' attribute on TextCell (id=unappliedFund_Cell) at CreditReversalCreditSearchScreen.pcf: line 163, column 55
    function value_63 () : entity.UnappliedFund {
      return credit.UnappliedFund
    }
    
    // 'visible' attribute on Link (id=Select) at CreditReversalCreditSearchScreen.pcf: line 139, column 54
    function visible_52 () : java.lang.Boolean {
      return credit.canReverse()
    }
    
    property get credit () : entity.Credit {
      return getIteratedValue(2) as entity.Credit
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/CreditReversalCreditSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends CreditReversalCreditSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at CreditReversalCreditSearchScreen.pcf: line 107, column 44
    function action_40 () : void {
      AccountSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at CreditReversalCreditSearchScreen.pcf: line 107, column 44
    function action_dest_41 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'available' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 75, column 51
    function available_18 () : java.lang.Boolean {
      return searchCriteria.Currency != null || !accountEditable
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 75, column 51
    function currency_22 () : typekey.Currency {
      return gw.search.SearchHelper.getDefaultCurrencyIfNull(searchCriteria.Currency)
    }
    
    // 'def' attribute on InputSetRef at CreditReversalCreditSearchScreen.pcf: line 117, column 49
    function def_onEnter_49 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at CreditReversalCreditSearchScreen.pcf: line 117, column 49
    function def_refreshVariables_50 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 65, column 74
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Currency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 75, column 51
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MinAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 82, column 51
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MaxAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 90, column 54
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.EarliestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 95, column 52
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.LatestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=AccountCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 107, column 44
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Account = (__VALUE_TO_SET as entity.Account)
    }
    
    // 'value' attribute on TypeKeyInput (id=ReasonCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 58, column 49
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Reason = (__VALUE_TO_SET as typekey.CreditType)
    }
    
    // 'editable' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 65, column 74
    function editable_10 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode() && accountEditable
    }
    
    // 'editable' attribute on TextInput (id=AccountCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 107, column 44
    function editable_42 () : java.lang.Boolean {
      return accountEditable
    }
    
    // 'inputConversion' attribute on TextInput (id=AccountCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 107, column 44
    function inputConversion_43 (VALUE :  java.lang.String) : java.lang.Object {
      return accountSearchConverter.getAccount(VALUE)
    }
    
    // 'onChange' attribute on PostOnChange at CreditReversalCreditSearchScreen.pcf: line 67, column 76
    function onChange_9 () : void {
      blankMinimumAndMaximumFields(searchCriteria)
    }
    
    // 'searchCriteria' attribute on SearchPanel at CreditReversalCreditSearchScreen.pcf: line 45, column 78
    function searchCriteria_68 () : entity.CreditSearchCriteria {
      return initializeSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at CreditReversalCreditSearchScreen.pcf: line 45, column 78
    function search_67 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria)
    }
    
    // 'value' attribute on TextCell (id=unappliedFund_Cell) at CreditReversalCreditSearchScreen.pcf: line 163, column 55
    function sortValue_51 (credit :  entity.Credit) : java.lang.Object {
      return credit.UnappliedFund
    }
    
    // 'value' attribute on TypeKeyInput (id=ReasonCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 58, column 49
    function valueRoot_7 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 65, column 74
    function value_12 () : typekey.Currency {
      return searchCriteria.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 75, column 51
    function value_19 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MinAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 82, column 51
    function value_26 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MaxAmount
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 90, column 54
    function value_32 () : java.util.Date {
      return searchCriteria.EarliestDate
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 95, column 52
    function value_36 () : java.util.Date {
      return searchCriteria.LatestDate
    }
    
    // 'value' attribute on TextInput (id=AccountCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 107, column 44
    function value_44 () : entity.Account {
      return searchCriteria.Account
    }
    
    // 'value' attribute on TypeKeyInput (id=ReasonCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 58, column 49
    function value_5 () : typekey.CreditType {
      return searchCriteria.Reason
    }
    
    // 'value' attribute on RowIterator at CreditReversalCreditSearchScreen.pcf: line 129, column 83
    function value_66 () : gw.api.database.IQueryBeanResult<entity.Credit> {
      return credits
    }
    
    // 'visible' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at CreditReversalCreditSearchScreen.pcf: line 65, column 74
    function visible_11 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    property get credits () : gw.api.database.IQueryBeanResult<Credit> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<Credit>
    }
    
    property get searchCriteria () : entity.CreditSearchCriteria {
      return getCriteriaValue(1) as entity.CreditSearchCriteria
    }
    
    property set searchCriteria ($arg :  entity.CreditSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}