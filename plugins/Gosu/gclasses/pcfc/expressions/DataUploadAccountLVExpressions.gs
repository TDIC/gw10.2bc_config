package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadAccountLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DataUploadAccountLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadAccountLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DataUploadAccountLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadAccountLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=fein_Cell) at DataUploadAccountLV.pcf: line 50, column 41
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.FEIN")
    }
    
    // 'label' attribute on TextCell (id=billingPlan_Cell) at DataUploadAccountLV.pcf: line 55, column 41
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.BillingPlan")
    }
    
    // 'label' attribute on TextCell (id=delinquencyPlan_Cell) at DataUploadAccountLV.pcf: line 60, column 41
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.DelinquencyPlan")
    }
    
    // 'label' attribute on TextCell (id=dueOrInvoiceDay_Cell) at DataUploadAccountLV.pcf: line 65, column 41
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.DueOrInvoiceDay")
    }
    
    // 'label' attribute on TextCell (id=dayOfMonth_Cell) at DataUploadAccountLV.pcf: line 70, column 42
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.InvoiceDayOfMonth")
    }
    
    // 'label' attribute on TextCell (id=twiceFirst_Cell) at DataUploadAccountLV.pcf: line 75, column 42
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.InvoiceTwiceAMonthFirstDay")
    }
    
    // 'label' attribute on TextCell (id=twiceSecond_Cell) at DataUploadAccountLV.pcf: line 80, column 42
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.InvoiceTwiceAMonthSecondDay")
    }
    
    // 'label' attribute on TextCell (id=invoiceDayOfWeek_Cell) at DataUploadAccountLV.pcf: line 85, column 41
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.InvoiceDayOfWeek")
    }
    
    // 'label' attribute on DateCell (id=anchorDate_Cell) at DataUploadAccountLV.pcf: line 90, column 39
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.InvoiceEveryOtherWeekAnchorDate")
    }
    
    // 'label' attribute on TypeKeyCell (id=contactType_Cell) at DataUploadAccountLV.pcf: line 95, column 40
    function label_29 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.ContactType")
    }
    
    // 'label' attribute on DateCell (id=createDate_Cell) at DataUploadAccountLV.pcf: line 29, column 39
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.EntryDate")
    }
    
    // 'label' attribute on TextCell (id=contactName_Cell) at DataUploadAccountLV.pcf: line 100, column 41
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.Name")
    }
    
    // 'label' attribute on TextCell (id=firstName_Cell) at DataUploadAccountLV.pcf: line 105, column 41
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.FirstName")
    }
    
    // 'label' attribute on TextCell (id=lastName_Cell) at DataUploadAccountLV.pcf: line 110, column 41
    function label_35 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.LastName")
    }
    
    // 'label' attribute on TextCell (id=addressLine1_Cell) at DataUploadAccountLV.pcf: line 115, column 41
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Line1")
    }
    
    // 'label' attribute on TextCell (id=city_Cell) at DataUploadAccountLV.pcf: line 125, column 41
    function label_41 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.City")
    }
    
    // 'label' attribute on TextCell (id=state_Cell) at DataUploadAccountLV.pcf: line 130, column 41
    function label_43 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.State")
    }
    
    // 'label' attribute on TextCell (id=postalCode_Cell) at DataUploadAccountLV.pcf: line 135, column 41
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.PostalCode")
    }
    
    // 'label' attribute on TypeKeyCell (id=country_Cell) at DataUploadAccountLV.pcf: line 140, column 40
    function label_47 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Country")
    }
    
    // 'label' attribute on TextCell (id=phone_Cell) at DataUploadAccountLV.pcf: line 145, column 41
    function label_49 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.Phone")
    }
    
    // 'label' attribute on TextCell (id=accountName_Cell) at DataUploadAccountLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.AccountName")
    }
    
    // 'label' attribute on TextCell (id=fax_Cell) at DataUploadAccountLV.pcf: line 150, column 41
    function label_51 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.Fax")
    }
    
    // 'label' attribute on TextCell (id=email_Cell) at DataUploadAccountLV.pcf: line 155, column 41
    function label_53 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.Email")
    }
    
    // 'label' attribute on BooleanRadioCell (id=primaryPayer_Cell) at DataUploadAccountLV.pcf: line 160, column 42
    function label_55 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.PrimaryPayer")
    }
    
    // 'label' attribute on TextCell (id=contactRole_Cell) at DataUploadAccountLV.pcf: line 165, column 41
    function label_57 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.ContactRole")
    }
    
    // 'label' attribute on TypeKeyCell (id=invoiceDelivery_Cell) at DataUploadAccountLV.pcf: line 170, column 54
    function label_59 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.InvoiceDelivery")
    }
    
    // 'label' attribute on TypeKeyCell (id=distributionLimit_Cell) at DataUploadAccountLV.pcf: line 175, column 54
    function label_61 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.DistributionLimit")
    }
    
    // 'label' attribute on TypeKeyCell (id=accountType_Cell) at DataUploadAccountLV.pcf: line 180, column 44
    function label_63 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.AccountType")
    }
    
    // 'label' attribute on TypeKeyCell (id=paymentMethod_Cell) at DataUploadAccountLV.pcf: line 185, column 46
    function label_65 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.PaymentMethod")
    }
    
    // 'label' attribute on TextCell (id=paymentPlan_Cell) at DataUploadAccountLV.pcf: line 190, column 41
    function label_67 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.PaymentPlan")
    }
    
    // 'label' attribute on TextCell (id=invoiceStream_Cell) at DataUploadAccountLV.pcf: line 195, column 41
    function label_69 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.InvoiceStream")
    }
    
    // 'label' attribute on TextCell (id=accountNumber_Cell) at DataUploadAccountLV.pcf: line 40, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.AccountNumber")
    }
    
    // 'label' attribute on TextCell (id=accountPolicyLevelBilling_Cell) at DataUploadAccountLV.pcf: line 45, column 41
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.BillingLevel")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadAccountLV.pcf: line 23, column 46
    function sortValue_1 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return processor.getLoadStatus(account)
    }
    
    // 'value' attribute on TextCell (id=accountPolicyLevelBilling_Cell) at DataUploadAccountLV.pcf: line 45, column 41
    function sortValue_10 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.BillingLevel
    }
    
    // 'value' attribute on TextCell (id=fein_Cell) at DataUploadAccountLV.pcf: line 50, column 41
    function sortValue_12 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.FEIN
    }
    
    // 'value' attribute on TextCell (id=billingPlan_Cell) at DataUploadAccountLV.pcf: line 55, column 41
    function sortValue_14 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.BillPlan
    }
    
    // 'value' attribute on TextCell (id=delinquencyPlan_Cell) at DataUploadAccountLV.pcf: line 60, column 41
    function sortValue_16 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.DelinquencyPlan
    }
    
    // 'value' attribute on TextCell (id=dueOrInvoiceDay_Cell) at DataUploadAccountLV.pcf: line 65, column 41
    function sortValue_18 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.DueOrInvoiceInd
    }
    
    // 'value' attribute on TextCell (id=dayOfMonth_Cell) at DataUploadAccountLV.pcf: line 70, column 42
    function sortValue_20 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.InvoiceDOM
    }
    
    // 'value' attribute on TextCell (id=twiceFirst_Cell) at DataUploadAccountLV.pcf: line 75, column 42
    function sortValue_22 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.InvoiceTwiceFirst
    }
    
    // 'value' attribute on TextCell (id=twiceSecond_Cell) at DataUploadAccountLV.pcf: line 80, column 42
    function sortValue_24 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.InvoiceTwiceSecond
    }
    
    // 'value' attribute on TextCell (id=invoiceDayOfWeek_Cell) at DataUploadAccountLV.pcf: line 85, column 41
    function sortValue_26 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.InvoiceDayofWeek
    }
    
    // 'value' attribute on DateCell (id=anchorDate_Cell) at DataUploadAccountLV.pcf: line 90, column 39
    function sortValue_28 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.InvoiceWeekAnchorDate
    }
    
    // 'value' attribute on TypeKeyCell (id=contactType_Cell) at DataUploadAccountLV.pcf: line 95, column 40
    function sortValue_30 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.Contact.ContactType
    }
    
    // 'value' attribute on TextCell (id=contactName_Cell) at DataUploadAccountLV.pcf: line 100, column 41
    function sortValue_32 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.Contact.Name
    }
    
    // 'value' attribute on TextCell (id=firstName_Cell) at DataUploadAccountLV.pcf: line 105, column 41
    function sortValue_34 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.Contact.FirstName
    }
    
    // 'value' attribute on TextCell (id=lastName_Cell) at DataUploadAccountLV.pcf: line 110, column 41
    function sortValue_36 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.Contact.LastName
    }
    
    // 'value' attribute on TextCell (id=addressLine1_Cell) at DataUploadAccountLV.pcf: line 115, column 41
    function sortValue_38 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.Contact.PrimaryAddress.AddressLine1
    }
    
    // 'value' attribute on DateCell (id=createDate_Cell) at DataUploadAccountLV.pcf: line 29, column 39
    function sortValue_4 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.EntryDate
    }
    
    // 'value' attribute on TextCell (id=addressLine2_Cell) at DataUploadAccountLV.pcf: line 120, column 41
    function sortValue_40 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.Contact.PrimaryAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at DataUploadAccountLV.pcf: line 125, column 41
    function sortValue_42 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.Contact.PrimaryAddress.City
    }
    
    // 'value' attribute on TextCell (id=state_Cell) at DataUploadAccountLV.pcf: line 130, column 41
    function sortValue_44 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.Contact.PrimaryAddress.State
    }
    
    // 'value' attribute on TextCell (id=postalCode_Cell) at DataUploadAccountLV.pcf: line 135, column 41
    function sortValue_46 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.Contact.PrimaryAddress.PostalCode
    }
    
    // 'value' attribute on TypeKeyCell (id=country_Cell) at DataUploadAccountLV.pcf: line 140, column 40
    function sortValue_48 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.Contact.PrimaryAddress.Country
    }
    
    // 'value' attribute on TextCell (id=phone_Cell) at DataUploadAccountLV.pcf: line 145, column 41
    function sortValue_50 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.Contact.WorkPhone
    }
    
    // 'value' attribute on TextCell (id=fax_Cell) at DataUploadAccountLV.pcf: line 150, column 41
    function sortValue_52 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.Contact.FaxPhone
    }
    
    // 'value' attribute on TextCell (id=email_Cell) at DataUploadAccountLV.pcf: line 155, column 41
    function sortValue_54 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.Contact.Email
    }
    
    // 'value' attribute on BooleanRadioCell (id=primaryPayer_Cell) at DataUploadAccountLV.pcf: line 160, column 42
    function sortValue_56 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.PrimaryPayer
    }
    
    // 'value' attribute on TextCell (id=contactRole_Cell) at DataUploadAccountLV.pcf: line 165, column 41
    function sortValue_58 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.Role.Code
    }
    
    // 'value' attribute on TextCell (id=accountName_Cell) at DataUploadAccountLV.pcf: line 35, column 41
    function sortValue_6 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.AccountName
    }
    
    // 'value' attribute on TypeKeyCell (id=invoiceDelivery_Cell) at DataUploadAccountLV.pcf: line 170, column 54
    function sortValue_60 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.InvoiceDelivery
    }
    
    // 'value' attribute on TypeKeyCell (id=distributionLimit_Cell) at DataUploadAccountLV.pcf: line 175, column 54
    function sortValue_62 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.DistributionLimitType
    }
    
    // 'value' attribute on TypeKeyCell (id=accountType_Cell) at DataUploadAccountLV.pcf: line 180, column 44
    function sortValue_64 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.AccountType
    }
    
    // 'value' attribute on TypeKeyCell (id=paymentMethod_Cell) at DataUploadAccountLV.pcf: line 185, column 46
    function sortValue_66 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.PaymentMethod
    }
    
    // 'value' attribute on TextCell (id=paymentPlan_Cell) at DataUploadAccountLV.pcf: line 190, column 41
    function sortValue_68 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.PaymentPlan
    }
    
    // 'value' attribute on TextCell (id=invoiceStream_Cell) at DataUploadAccountLV.pcf: line 195, column 41
    function sortValue_70 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=accountNumber_Cell) at DataUploadAccountLV.pcf: line 40, column 41
    function sortValue_8 (account :  tdic.util.dataloader.data.sampledata.AccountData) : java.lang.Object {
      return account.AccountNumber
    }
    
    // 'value' attribute on RowIterator (id=accountsID) at DataUploadAccountLV.pcf: line 15, column 95
    function value_247 () : java.util.ArrayList<tdic.util.dataloader.data.sampledata.AccountData> {
      return processor.AccountArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadAccountLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCSampleDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCSampleDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadAccountLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DataUploadAccountLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at DataUploadAccountLV.pcf: line 17, column 94
    function highlighted_246 () : java.lang.Boolean {
      return (account.Error or account.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=billingPlan_Cell) at DataUploadAccountLV.pcf: line 55, column 41
    function label_101 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.BillingPlan")
    }
    
    // 'label' attribute on TextCell (id=delinquencyPlan_Cell) at DataUploadAccountLV.pcf: line 60, column 41
    function label_106 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.DelinquencyPlan")
    }
    
    // 'label' attribute on TextCell (id=dueOrInvoiceDay_Cell) at DataUploadAccountLV.pcf: line 65, column 41
    function label_111 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.DueOrInvoiceDay")
    }
    
    // 'label' attribute on TextCell (id=dayOfMonth_Cell) at DataUploadAccountLV.pcf: line 70, column 42
    function label_116 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.InvoiceDayOfMonth")
    }
    
    // 'label' attribute on TextCell (id=twiceFirst_Cell) at DataUploadAccountLV.pcf: line 75, column 42
    function label_121 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.InvoiceTwiceAMonthFirstDay")
    }
    
    // 'label' attribute on TextCell (id=twiceSecond_Cell) at DataUploadAccountLV.pcf: line 80, column 42
    function label_126 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.InvoiceTwiceAMonthSecondDay")
    }
    
    // 'label' attribute on TextCell (id=invoiceDayOfWeek_Cell) at DataUploadAccountLV.pcf: line 85, column 41
    function label_131 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.InvoiceDayOfWeek")
    }
    
    // 'label' attribute on DateCell (id=anchorDate_Cell) at DataUploadAccountLV.pcf: line 90, column 39
    function label_136 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.InvoiceEveryOtherWeekAnchorDate")
    }
    
    // 'label' attribute on TypeKeyCell (id=contactType_Cell) at DataUploadAccountLV.pcf: line 95, column 40
    function label_141 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.ContactType")
    }
    
    // 'label' attribute on TextCell (id=contactName_Cell) at DataUploadAccountLV.pcf: line 100, column 41
    function label_146 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.Name")
    }
    
    // 'label' attribute on TextCell (id=firstName_Cell) at DataUploadAccountLV.pcf: line 105, column 41
    function label_151 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.FirstName")
    }
    
    // 'label' attribute on TextCell (id=lastName_Cell) at DataUploadAccountLV.pcf: line 110, column 41
    function label_156 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.LastName")
    }
    
    // 'label' attribute on TextCell (id=addressLine1_Cell) at DataUploadAccountLV.pcf: line 115, column 41
    function label_161 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Line1")
    }
    
    // 'label' attribute on TextCell (id=city_Cell) at DataUploadAccountLV.pcf: line 125, column 41
    function label_171 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.City")
    }
    
    // 'label' attribute on TextCell (id=state_Cell) at DataUploadAccountLV.pcf: line 130, column 41
    function label_176 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.State")
    }
    
    // 'label' attribute on TextCell (id=postalCode_Cell) at DataUploadAccountLV.pcf: line 135, column 41
    function label_181 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.PostalCode")
    }
    
    // 'label' attribute on TypeKeyCell (id=country_Cell) at DataUploadAccountLV.pcf: line 140, column 40
    function label_186 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Country")
    }
    
    // 'label' attribute on TextCell (id=phone_Cell) at DataUploadAccountLV.pcf: line 145, column 41
    function label_191 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.Phone")
    }
    
    // 'label' attribute on TextCell (id=fax_Cell) at DataUploadAccountLV.pcf: line 150, column 41
    function label_196 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.Fax")
    }
    
    // 'label' attribute on TextCell (id=email_Cell) at DataUploadAccountLV.pcf: line 155, column 41
    function label_201 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.Email")
    }
    
    // 'label' attribute on BooleanRadioCell (id=primaryPayer_Cell) at DataUploadAccountLV.pcf: line 160, column 42
    function label_206 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.PrimaryPayer")
    }
    
    // 'label' attribute on TextCell (id=contactRole_Cell) at DataUploadAccountLV.pcf: line 165, column 41
    function label_211 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.ContactRole")
    }
    
    // 'label' attribute on TypeKeyCell (id=invoiceDelivery_Cell) at DataUploadAccountLV.pcf: line 170, column 54
    function label_216 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.InvoiceDelivery")
    }
    
    // 'label' attribute on TypeKeyCell (id=distributionLimit_Cell) at DataUploadAccountLV.pcf: line 175, column 54
    function label_221 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.DistributionLimit")
    }
    
    // 'label' attribute on TypeKeyCell (id=accountType_Cell) at DataUploadAccountLV.pcf: line 180, column 44
    function label_226 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.AccountType")
    }
    
    // 'label' attribute on TypeKeyCell (id=paymentMethod_Cell) at DataUploadAccountLV.pcf: line 185, column 46
    function label_231 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.PaymentMethod")
    }
    
    // 'label' attribute on TextCell (id=paymentPlan_Cell) at DataUploadAccountLV.pcf: line 190, column 41
    function label_236 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.PaymentPlan")
    }
    
    // 'label' attribute on TextCell (id=invoiceStream_Cell) at DataUploadAccountLV.pcf: line 195, column 41
    function label_241 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.InvoiceStream")
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadAccountLV.pcf: line 23, column 46
    function label_71 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on DateCell (id=createDate_Cell) at DataUploadAccountLV.pcf: line 29, column 39
    function label_76 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.EntryDate")
    }
    
    // 'label' attribute on TextCell (id=accountName_Cell) at DataUploadAccountLV.pcf: line 35, column 41
    function label_81 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.AccountName")
    }
    
    // 'label' attribute on TextCell (id=accountNumber_Cell) at DataUploadAccountLV.pcf: line 40, column 41
    function label_86 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.AccountNumber")
    }
    
    // 'label' attribute on TextCell (id=accountPolicyLevelBilling_Cell) at DataUploadAccountLV.pcf: line 45, column 41
    function label_91 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.BillingLevel")
    }
    
    // 'label' attribute on TextCell (id=fein_Cell) at DataUploadAccountLV.pcf: line 50, column 41
    function label_96 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Account.FEIN")
    }
    
    // 'value' attribute on TypeKeyCell (id=contactType_Cell) at DataUploadAccountLV.pcf: line 95, column 40
    function valueRoot_143 () : java.lang.Object {
      return account.Contact
    }
    
    // 'value' attribute on TextCell (id=addressLine1_Cell) at DataUploadAccountLV.pcf: line 115, column 41
    function valueRoot_163 () : java.lang.Object {
      return account.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=contactRole_Cell) at DataUploadAccountLV.pcf: line 165, column 41
    function valueRoot_213 () : java.lang.Object {
      return account.Role
    }
    
    // 'value' attribute on DateCell (id=createDate_Cell) at DataUploadAccountLV.pcf: line 29, column 39
    function valueRoot_78 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on TextCell (id=billingPlan_Cell) at DataUploadAccountLV.pcf: line 55, column 41
    function value_102 () : java.lang.String {
      return account.BillPlan
    }
    
    // 'value' attribute on TextCell (id=delinquencyPlan_Cell) at DataUploadAccountLV.pcf: line 60, column 41
    function value_107 () : java.lang.String {
      return account.DelinquencyPlan
    }
    
    // 'value' attribute on TextCell (id=dueOrInvoiceDay_Cell) at DataUploadAccountLV.pcf: line 65, column 41
    function value_112 () : java.lang.String {
      return account.DueOrInvoiceInd
    }
    
    // 'value' attribute on TextCell (id=dayOfMonth_Cell) at DataUploadAccountLV.pcf: line 70, column 42
    function value_117 () : java.lang.Integer {
      return account.InvoiceDOM
    }
    
    // 'value' attribute on TextCell (id=twiceFirst_Cell) at DataUploadAccountLV.pcf: line 75, column 42
    function value_122 () : java.lang.Integer {
      return account.InvoiceTwiceFirst
    }
    
    // 'value' attribute on TextCell (id=twiceSecond_Cell) at DataUploadAccountLV.pcf: line 80, column 42
    function value_127 () : java.lang.Integer {
      return account.InvoiceTwiceSecond
    }
    
    // 'value' attribute on TextCell (id=invoiceDayOfWeek_Cell) at DataUploadAccountLV.pcf: line 85, column 41
    function value_132 () : java.lang.String {
      return account.InvoiceDayofWeek
    }
    
    // 'value' attribute on DateCell (id=anchorDate_Cell) at DataUploadAccountLV.pcf: line 90, column 39
    function value_137 () : java.util.Date {
      return account.InvoiceWeekAnchorDate
    }
    
    // 'value' attribute on TypeKeyCell (id=contactType_Cell) at DataUploadAccountLV.pcf: line 95, column 40
    function value_142 () : typekey.Contact {
      return account.Contact.ContactType
    }
    
    // 'value' attribute on TextCell (id=contactName_Cell) at DataUploadAccountLV.pcf: line 100, column 41
    function value_147 () : java.lang.String {
      return account.Contact.Name
    }
    
    // 'value' attribute on TextCell (id=firstName_Cell) at DataUploadAccountLV.pcf: line 105, column 41
    function value_152 () : java.lang.String {
      return account.Contact.FirstName
    }
    
    // 'value' attribute on TextCell (id=lastName_Cell) at DataUploadAccountLV.pcf: line 110, column 41
    function value_157 () : java.lang.String {
      return account.Contact.LastName
    }
    
    // 'value' attribute on TextCell (id=addressLine1_Cell) at DataUploadAccountLV.pcf: line 115, column 41
    function value_162 () : java.lang.String {
      return account.Contact.PrimaryAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=addressLine2_Cell) at DataUploadAccountLV.pcf: line 120, column 41
    function value_167 () : java.lang.String {
      return account.Contact.PrimaryAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at DataUploadAccountLV.pcf: line 125, column 41
    function value_172 () : java.lang.String {
      return account.Contact.PrimaryAddress.City
    }
    
    // 'value' attribute on TextCell (id=state_Cell) at DataUploadAccountLV.pcf: line 130, column 41
    function value_177 () : java.lang.String {
      return account.Contact.PrimaryAddress.State
    }
    
    // 'value' attribute on TextCell (id=postalCode_Cell) at DataUploadAccountLV.pcf: line 135, column 41
    function value_182 () : java.lang.String {
      return account.Contact.PrimaryAddress.PostalCode
    }
    
    // 'value' attribute on TypeKeyCell (id=country_Cell) at DataUploadAccountLV.pcf: line 140, column 40
    function value_187 () : typekey.Country {
      return account.Contact.PrimaryAddress.Country
    }
    
    // 'value' attribute on TextCell (id=phone_Cell) at DataUploadAccountLV.pcf: line 145, column 41
    function value_192 () : java.lang.String {
      return account.Contact.WorkPhone
    }
    
    // 'value' attribute on TextCell (id=fax_Cell) at DataUploadAccountLV.pcf: line 150, column 41
    function value_197 () : java.lang.String {
      return account.Contact.FaxPhone
    }
    
    // 'value' attribute on TextCell (id=email_Cell) at DataUploadAccountLV.pcf: line 155, column 41
    function value_202 () : java.lang.String {
      return account.Contact.Email
    }
    
    // 'value' attribute on BooleanRadioCell (id=primaryPayer_Cell) at DataUploadAccountLV.pcf: line 160, column 42
    function value_207 () : java.lang.Boolean {
      return account.PrimaryPayer
    }
    
    // 'value' attribute on TextCell (id=contactRole_Cell) at DataUploadAccountLV.pcf: line 165, column 41
    function value_212 () : java.lang.String {
      return account.Role.Code
    }
    
    // 'value' attribute on TypeKeyCell (id=invoiceDelivery_Cell) at DataUploadAccountLV.pcf: line 170, column 54
    function value_217 () : typekey.InvoiceDeliveryMethod {
      return account.InvoiceDelivery
    }
    
    // 'value' attribute on TypeKeyCell (id=distributionLimit_Cell) at DataUploadAccountLV.pcf: line 175, column 54
    function value_222 () : typekey.DistributionLimitType {
      return account.DistributionLimitType
    }
    
    // 'value' attribute on TypeKeyCell (id=accountType_Cell) at DataUploadAccountLV.pcf: line 180, column 44
    function value_227 () : typekey.AccountType {
      return account.AccountType
    }
    
    // 'value' attribute on TypeKeyCell (id=paymentMethod_Cell) at DataUploadAccountLV.pcf: line 185, column 46
    function value_232 () : typekey.PaymentMethod {
      return account.PaymentMethod
    }
    
    // 'value' attribute on TextCell (id=paymentPlan_Cell) at DataUploadAccountLV.pcf: line 190, column 41
    function value_237 () : java.lang.String {
      return account.PaymentPlan
    }
    
    // 'value' attribute on TextCell (id=invoiceStream_Cell) at DataUploadAccountLV.pcf: line 195, column 41
    function value_242 () : java.lang.String {
      return account.InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadAccountLV.pcf: line 23, column 46
    function value_72 () : java.lang.String {
      return processor.getLoadStatus(account)
    }
    
    // 'value' attribute on DateCell (id=createDate_Cell) at DataUploadAccountLV.pcf: line 29, column 39
    function value_77 () : java.util.Date {
      return account.EntryDate
    }
    
    // 'value' attribute on TextCell (id=accountName_Cell) at DataUploadAccountLV.pcf: line 35, column 41
    function value_82 () : java.lang.String {
      return account.AccountName
    }
    
    // 'value' attribute on TextCell (id=accountNumber_Cell) at DataUploadAccountLV.pcf: line 40, column 41
    function value_87 () : java.lang.String {
      return account.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=accountPolicyLevelBilling_Cell) at DataUploadAccountLV.pcf: line 45, column 41
    function value_92 () : java.lang.String {
      return account.BillingLevel
    }
    
    // 'value' attribute on TextCell (id=fein_Cell) at DataUploadAccountLV.pcf: line 50, column 41
    function value_97 () : java.lang.String {
      return account.FEIN
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadAccountLV.pcf: line 23, column 46
    function visible_73 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get account () : tdic.util.dataloader.data.sampledata.AccountData {
      return getIteratedValue(1) as tdic.util.dataloader.data.sampledata.AccountData
    }
    
    
  }
  
  
}