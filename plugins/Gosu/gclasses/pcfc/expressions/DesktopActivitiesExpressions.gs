package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopActivities.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopActivitiesExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopActivities.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopActivitiesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=DesktopActivities_AssignButton) at DesktopActivities.pcf: line 47, column 45
    function allCheckedRowsAction_6 (CheckedValues :  entity.Activity[], CheckedValuesErrors :  java.util.Map) : void {
      AssignActivitiesPopup.push(new gw.api.web.activity.ActivityAssignmentPopup(CheckedValues), CheckedValues)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=DesktopActivities_CompleteButton) at DesktopActivities.pcf: line 56, column 50
    function allCheckedRowsAction_8 (CheckedValues :  entity.Activity[], CheckedValuesErrors :  java.util.Map) : void {
      gw.api.web.activity.ActivityUtil.completeActivities(CheckedValues); CurrentLocation.startEditing(); CurrentLocation.commit()
    }
    
    // 'canVisit' attribute on Page (id=DesktopActivities) at DesktopActivities.pcf: line 9, column 63
    static function canVisit_91 () : java.lang.Boolean {
      return perm.System.actview and perm.System.viewdesktop
    }
    
    // 'value' attribute on ToolbarRangeInput (id=UsersList_Input) at DesktopActivities.pcf: line 62, column 37
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectedUser = (__VALUE_TO_SET as entity.User)
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopActivities.pcf: line 80, column 85
    function filter_15 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("AllOpen", \ qf -> {qf.compare("Status", Equals, ActivityStatus.TC_OPEN)})
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopActivities.pcf: line 83, column 92
    function filter_16 () : gw.api.filters.IFilter {
      var localWeekStart = weekStart; return new gw.api.filters.StandardQueryFilter("OpenedThisWeek", \ qf -> {qf.compare("Status", Equals, ActivityStatus.TC_OPEN); qf.compare("CreateTime", GreaterThanOrEquals, localWeekStart)})
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopActivities.pcf: line 86, column 102
    function filter_17 () : gw.api.filters.IFilter {
      var localLastMonth = lastMonth; return new gw.api.filters.StandardQueryFilter("CompletedInLast30Days", \ qf -> {qf.compare("Status", Equals, ActivityStatus.TC_COMPLETE); qf.compare("CloseDate", GreaterThanOrEquals, localLastMonth)})
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopActivities.pcf: line 89, column 88
    function filter_19 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("OpenUrgent", \ qf -> {qf.compare("Status", Equals, ActivityStatus.TC_OPEN); qf.compare("Priority", Equals, Priority.TC_URGENT)})
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopActivities.pcf: line 92, column 93
    function filter_20 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("EscalatedFilter", \ qf -> {qf.compare("Escalated", Equals, Boolean.TRUE)})
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopActivities.pcf: line 99, column 53
    function filter_21 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("All", \ qf -> {})
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopActivities.pcf: line 102, column 83
    function filter_23 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("Owned", \ qf -> {qf.compare("AssignedUser", Equals, User.util.CurrentUser)})
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopActivities.pcf: line 106, column 53
    function filter_24 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("Shared", \ qf -> {qf.compare("Subtype", Equals, typekey.Activity.TC_SHAREDACTIVITY)})
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at DesktopActivities.pcf: line 111, column 85
    function filters_26 () : gw.api.filters.IFilter[] {
      return new gw.activity.ActivityPatternFilterSet().FilterOptions
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at DesktopActivities.pcf: line 124, column 111
    function filters_27 () : gw.api.filters.IFilter[] {
      return gw.acc.activityenhancement.ActivityFilterUtil.DesktopActivitySubtypesFilterOptions
    }
    
    // 'iconColor' attribute on BooleanRadioCell (id=Escalated_Cell) at DesktopActivities.pcf: line 143, column 45
    function iconColor_28 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_URGENCY_HIGH
    }
    
    // 'initialValue' attribute on Variable at DesktopActivities.pcf: line 16, column 71
    function initialValue_0 () : gw.api.database.IQueryBeanResult<entity.Activity> {
      return getActivities()
    }
    
    // 'initialValue' attribute on Variable at DesktopActivities.pcf: line 20, column 60
    function initialValue_1 () : gw.api.web.activity.DesktopActivityFilterSet {
      return new gw.api.web.activity.DesktopActivityFilterSet()
    }
    
    // 'initialValue' attribute on Variable at DesktopActivities.pcf: line 24, column 30
    function initialValue_2 () : java.util.Date {
      return gw.api.util.BCDateUtil.getWeekStartDate()
    }
    
    // 'initialValue' attribute on Variable at DesktopActivities.pcf: line 28, column 30
    function initialValue_3 () : java.util.Date {
      return gw.api.util.DateUtil.currentDate().addMonths( -1 )
    }
    
    // 'initialValue' attribute on Variable at DesktopActivities.pcf: line 32, column 20
    function initialValue_4 () : User {
      return User.util.CurrentUser
    }
    
    // 'label' attribute on ToolbarFilterOption at DesktopActivities.pcf: line 86, column 102
    function label_18 () : java.lang.Object {
      return DisplayKey.get("Java.ActivityFilterSet.CompletedInLastNDays", 30)
    }
    
    // Page (id=DesktopActivities) at DesktopActivities.pcf: line 9, column 63
    static function parent_92 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at DesktopActivities.pcf: line 143, column 45
    function sortValue_29 (activity :  entity.Activity) : java.lang.Object {
      return activity.Escalated
    }
    
    // 'value' attribute on DateCell (id=CreateDate_Cell) at DesktopActivities.pcf: line 147, column 46
    function sortValue_30 (activity :  entity.Activity) : java.lang.Object {
      return activity.CreateTime
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at DesktopActivities.pcf: line 153, column 46
    function sortValue_31 (activity :  entity.Activity) : java.lang.Object {
      return activity.TargetDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at DesktopActivities.pcf: line 158, column 47
    function sortValue_32 (activity :  entity.Activity) : java.lang.Object {
      return activity.Priority
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DesktopActivities.pcf: line 163, column 53
    function sortValue_33 (activity :  entity.Activity) : java.lang.Object {
      return activity.Status
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at DesktopActivities.pcf: line 169, column 43
    function sortValue_34 (activity :  entity.Activity) : java.lang.Object {
      return activity.Subject
    }
    
    // 'value' attribute on TextCell (id=TroubleTicketNumber_Cell) at DesktopActivities.pcf: line 174, column 69
    function sortValue_35 (activity :  entity.Activity) : java.lang.Object {
      return activity.TroubleTicket.TroubleTicketNumber
    }
    
    // 'value' attribute on TextCell (id=account_Cell) at DesktopActivities.pcf: line 181, column 38
    function sortValue_36 (activity :  entity.Activity) : java.lang.Object {
      return activity.Account
    }
    
    // 'value' attribute on TextCell (id=policyPeriod_Cell) at DesktopActivities.pcf: line 188, column 43
    function sortValue_37 (activity :  entity.Activity) : java.lang.Object {
      return activity.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=producer_Cell) at DesktopActivities.pcf: line 195, column 39
    function sortValue_38 (activity :  entity.Activity) : java.lang.Object {
      return activity.Producer_Ext
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=UsersList_Input) at DesktopActivities.pcf: line 62, column 37
    function valueRange_11 () : java.lang.Object {
      return gw.acc.activityenhancement.ActivityFilterUtil.UsersActivitiesList
    }
    
    // 'value' attribute on ToolbarRangeInput (id=UsersList_Input) at DesktopActivities.pcf: line 62, column 37
    function value_9 () : entity.User {
      return selectedUser
    }
    
    // 'value' attribute on RowIterator at DesktopActivities.pcf: line 75, column 81
    function value_90 () : gw.api.database.IQueryBeanResult<entity.Activity> {
      return activities
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=UsersList_Input) at DesktopActivities.pcf: line 62, column 37
    function verifyValueRangeIsAllowedType_12 ($$arg :  entity.User[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=UsersList_Input) at DesktopActivities.pcf: line 62, column 37
    function verifyValueRangeIsAllowedType_12 ($$arg :  gw.api.database.IQueryBeanResult<entity.User>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=UsersList_Input) at DesktopActivities.pcf: line 62, column 37
    function verifyValueRangeIsAllowedType_12 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=UsersList_Input) at DesktopActivities.pcf: line 62, column 37
    function verifyValueRange_13 () : void {
      var __valueRangeArg = gw.acc.activityenhancement.ActivityFilterUtil.UsersActivitiesList
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_12(__valueRangeArg)
    }
    
    // 'visible' attribute on ToolbarFilterOption at DesktopActivities.pcf: line 99, column 53
    function visible_22 () : java.lang.Boolean {
      return perm.SharedActivity.view
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=DesktopActivities_AssignButton) at DesktopActivities.pcf: line 47, column 45
    function visible_5 () : java.lang.Boolean {
      return perm.System.actraown
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=DesktopActivities_CompleteButton) at DesktopActivities.pcf: line 56, column 50
    function visible_7 () : java.lang.Boolean {
      return perm.System.actapproveany
    }
    
    override property get CurrentLocation () : pcf.DesktopActivities {
      return super.CurrentLocation as pcf.DesktopActivities
    }
    
    property get activities () : gw.api.database.IQueryBeanResult<entity.Activity> {
      return getVariableValue("activities", 0) as gw.api.database.IQueryBeanResult<entity.Activity>
    }
    
    property set activities ($arg :  gw.api.database.IQueryBeanResult<entity.Activity>) {
      setVariableValue("activities", 0, $arg)
    }
    
    property get filterSet () : gw.api.web.activity.DesktopActivityFilterSet {
      return getVariableValue("filterSet", 0) as gw.api.web.activity.DesktopActivityFilterSet
    }
    
    property set filterSet ($arg :  gw.api.web.activity.DesktopActivityFilterSet) {
      setVariableValue("filterSet", 0, $arg)
    }
    
    property get lastMonth () : java.util.Date {
      return getVariableValue("lastMonth", 0) as java.util.Date
    }
    
    property set lastMonth ($arg :  java.util.Date) {
      setVariableValue("lastMonth", 0, $arg)
    }
    
    property get selectedUser () : User {
      return getVariableValue("selectedUser", 0) as User
    }
    
    property set selectedUser ($arg :  User) {
      setVariableValue("selectedUser", 0, $arg)
    }
    
    property get weekStart () : java.util.Date {
      return getVariableValue("weekStart", 0) as java.util.Date
    }
    
    property set weekStart ($arg :  java.util.Date) {
      setVariableValue("weekStart", 0, $arg)
    }
    
    function getActivities(): gw.api.database.IQueryBeanResult<entity.Activity> {
      return Activity.finder.findActivitiesByAssignedUserOrShared(selectedUser)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopActivities.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DesktopActivitiesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=account_Cell) at DesktopActivities.pcf: line 181, column 38
    function actionAvailable_74 () : java.lang.Boolean {
      return activity.Account != null
    }
    
    // 'actionAvailable' attribute on TextCell (id=policyPeriod_Cell) at DesktopActivities.pcf: line 188, column 43
    function actionAvailable_80 () : java.lang.Boolean {
      return activity.PolicyPeriod != null
    }
    
    // 'actionAvailable' attribute on TextCell (id=producer_Cell) at DesktopActivities.pcf: line 195, column 39
    function actionAvailable_86 () : java.lang.Boolean {
      return activity.Producer_Ext != null
    }
    
    // 'action' attribute on TextCell (id=Subject_Cell) at DesktopActivities.pcf: line 169, column 43
    function action_63 () : void {
      gw.activity.ActivityMethods.viewActivityDetails(activity)
    }
    
    // 'action' attribute on TextCell (id=TroubleTicketNumber_Cell) at DesktopActivities.pcf: line 174, column 69
    function action_67 () : void {
      TroubleTicketDetailsPopup.push(activity.TroubleTicket)
    }
    
    // 'action' attribute on TextCell (id=account_Cell) at DesktopActivities.pcf: line 181, column 38
    function action_72 () : void {
      AccountDetailSummary.push(activity.Account)
    }
    
    // 'action' attribute on TextCell (id=policyPeriod_Cell) at DesktopActivities.pcf: line 188, column 43
    function action_78 () : void {
      PolicyDetailSummaryPopup.push(activity.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=producer_Cell) at DesktopActivities.pcf: line 195, column 39
    function action_84 () : void {
      ProducerDetailPopup.push(activity.Producer_Ext)
    }
    
    // 'action' attribute on TextCell (id=TroubleTicketNumber_Cell) at DesktopActivities.pcf: line 174, column 69
    function action_dest_68 () : pcf.api.Destination {
      return pcf.TroubleTicketDetailsPopup.createDestination(activity.TroubleTicket)
    }
    
    // 'action' attribute on TextCell (id=account_Cell) at DesktopActivities.pcf: line 181, column 38
    function action_dest_73 () : pcf.api.Destination {
      return pcf.AccountDetailSummary.createDestination(activity.Account)
    }
    
    // 'action' attribute on TextCell (id=policyPeriod_Cell) at DesktopActivities.pcf: line 188, column 43
    function action_dest_79 () : pcf.api.Destination {
      return pcf.PolicyDetailSummaryPopup.createDestination(activity.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=producer_Cell) at DesktopActivities.pcf: line 195, column 39
    function action_dest_85 () : pcf.api.Destination {
      return pcf.ProducerDetailPopup.createDestination(activity.Producer_Ext)
    }
    
    // 'condition' attribute on ToolbarFlag at DesktopActivities.pcf: line 117, column 41
    function condition_39 () : java.lang.Boolean {
      return activity.canAssign() and !(activity typeis SharedActivity)
    }
    
    // 'condition' attribute on ToolbarFlag at DesktopActivities.pcf: line 120, column 43
    function condition_40 () : java.lang.Boolean {
      return activity.canComplete()
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at DesktopActivities.pcf: line 153, column 46
    function fontColor_51 () : java.lang.Object {
      return activity.Overdue == true ? gw.api.web.color.GWColor.THEME_URGENCY_HIGH : null
    }
    
    // 'iconColor' attribute on BooleanRadioCell (id=Escalated_Cell) at DesktopActivities.pcf: line 143, column 45
    function iconColor_46 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_URGENCY_HIGH
    }
    
    // 'value' attribute on BooleanRadioCell (id=UpdatedSinceLastViewed_Cell) at DesktopActivities.pcf: line 134, column 58
    function valueRoot_42 () : java.lang.Object {
      return activity
    }
    
    // 'value' attribute on TextCell (id=TroubleTicketNumber_Cell) at DesktopActivities.pcf: line 174, column 69
    function valueRoot_70 () : java.lang.Object {
      return activity.TroubleTicket
    }
    
    // 'value' attribute on BooleanRadioCell (id=UpdatedSinceLastViewed_Cell) at DesktopActivities.pcf: line 134, column 58
    function value_41 () : java.lang.Boolean {
      return activity.UpdatedSinceLastViewed
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at DesktopActivities.pcf: line 143, column 45
    function value_44 () : java.lang.Boolean {
      return activity.Escalated
    }
    
    // 'value' attribute on DateCell (id=CreateDate_Cell) at DesktopActivities.pcf: line 147, column 46
    function value_48 () : java.util.Date {
      return activity.CreateTime
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at DesktopActivities.pcf: line 153, column 46
    function value_52 () : java.util.Date {
      return activity.TargetDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at DesktopActivities.pcf: line 158, column 47
    function value_57 () : typekey.Priority {
      return activity.Priority
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DesktopActivities.pcf: line 163, column 53
    function value_60 () : typekey.ActivityStatus {
      return activity.Status
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at DesktopActivities.pcf: line 169, column 43
    function value_64 () : java.lang.String {
      return activity.Subject
    }
    
    // 'value' attribute on TextCell (id=TroubleTicketNumber_Cell) at DesktopActivities.pcf: line 174, column 69
    function value_69 () : java.lang.String {
      return activity.TroubleTicket.TroubleTicketNumber
    }
    
    // 'value' attribute on TextCell (id=account_Cell) at DesktopActivities.pcf: line 181, column 38
    function value_75 () : Account {
      return activity.Account
    }
    
    // 'value' attribute on TextCell (id=policyPeriod_Cell) at DesktopActivities.pcf: line 188, column 43
    function value_81 () : PolicyPeriod {
      return activity.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=producer_Cell) at DesktopActivities.pcf: line 195, column 39
    function value_87 () : Producer {
      return activity.Producer_Ext
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at DesktopActivities.pcf: line 153, column 46
    function verifyFontColorIsAllowedType_54 ($$arg :  gw.api.web.color.GWColor) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at DesktopActivities.pcf: line 153, column 46
    function verifyFontColorIsAllowedType_54 ($$arg :  java.lang.String) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at DesktopActivities.pcf: line 153, column 46
    function verifyFontColor_55 () : void {
      var __fontColorArg = activity.Overdue == true ? gw.api.web.color.GWColor.THEME_URGENCY_HIGH : null
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the fontColor return type is not a valid type for use with a ValueWidget's fontColor
      // It needs to return a String or a GWColor.
      verifyFontColorIsAllowedType_54(__fontColorArg)
    }
    
    property get activity () : entity.Activity {
      return getIteratedValue(1) as entity.Activity
    }
    
    
  }
  
  
}