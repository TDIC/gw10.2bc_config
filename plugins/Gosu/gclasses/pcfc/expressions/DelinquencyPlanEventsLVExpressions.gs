package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanEventsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyPlanEventsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanEventsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyPlanEventsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at DelinquencyPlanEventsLV.pcf: line 18, column 23
    function initialValue_0 () : boolean {
      return not dlnqPlanReason.DelinquencyPlan.InUse
    }
    
    // 'pickLocation' attribute on RowIterator at DelinquencyPlanEventsLV.pcf: line 28, column 49
    function pickLocation_31 () : void {
      DelinquencyPlanEventPopup.push( dlnqPlanReason )
    }
    
    // 'value' attribute on TypeKeyCell (id=EventName_Cell) at DelinquencyPlanEventsLV.pcf: line 39, column 53
    function sortValue_1 (planEvent :  entity.DelinquencyPlanEvent) : java.lang.Object {
      return planEvent.EventName
    }
    
    // 'value' attribute on RangeCell (id=TriggerBasis_Cell) at DelinquencyPlanEventsLV.pcf: line 47, column 25
    function sortValue_2 (planEvent :  entity.DelinquencyPlanEvent) : java.lang.Object {
      return planEvent.TriggerBasis
    }
    
    // 'value' attribute on TextCell (id=OffsetDays_Cell) at DelinquencyPlanEventsLV.pcf: line 53, column 42
    function sortValue_3 (planEvent :  entity.DelinquencyPlanEvent) : java.lang.Object {
      return planEvent.OffsetDays
    }
    
    // 'value' attribute on TextCell (id=RelativeOrder_Cell) at DelinquencyPlanEventsLV.pcf: line 59, column 42
    function sortValue_4 (planEvent :  entity.DelinquencyPlanEvent) : java.lang.Object {
      return planEvent.RelativeOrder
    }
    
    // 'value' attribute on BooleanRadioCell (id=Automatic_Cell) at DelinquencyPlanEventsLV.pcf: line 64, column 40
    function sortValue_5 (planEvent :  entity.DelinquencyPlanEvent) : java.lang.Object {
      return planEvent.Automatic
    }
    
    // 'toAdd' attribute on RowIterator at DelinquencyPlanEventsLV.pcf: line 28, column 49
    function toAdd_32 (planEvent :  entity.DelinquencyPlanEvent) : void {
      planEvent.DelinquencyPlanReason.addToPlanEvents( planEvent )
    }
    
    // 'toRemove' attribute on RowIterator at DelinquencyPlanEventsLV.pcf: line 28, column 49
    function toRemove_33 (planEvent :  entity.DelinquencyPlanEvent) : void {
      dlnqPlanReason.removeFromPlanEvents(planEvent)
    }
    
    // 'value' attribute on RowIterator at DelinquencyPlanEventsLV.pcf: line 28, column 49
    function value_34 () : entity.DelinquencyPlanEvent[] {
      return dlnqPlanReason.getOrderedEvents()?.toTypedArray()
    }
    
    property get dlnqPlanReason () : DelinquencyPlanReason {
      return getRequireValue("dlnqPlanReason", 0) as DelinquencyPlanReason
    }
    
    property set dlnqPlanReason ($arg :  DelinquencyPlanReason) {
      setRequireValue("dlnqPlanReason", 0, $arg)
    }
    
    property get planNotInUse () : boolean {
      return getVariableValue("planNotInUse", 0) as java.lang.Boolean
    }
    
    property set planNotInUse ($arg :  boolean) {
      setVariableValue("planNotInUse", 0, $arg)
    }
    
    
    /**
           */
          function newDelinquencyPlanEvent() : DelinquencyPlanEvent {
            var newDelinquencyPlanEvent = new DelinquencyPlanEvent()
            newDelinquencyPlanEvent.DelinquencyPlanReason = dlnqPlanReason
            return newDelinquencyPlanEvent
          }
        
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanEventsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DelinquencyPlanEventsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeCell (id=TriggerBasis_Cell) at DelinquencyPlanEventsLV.pcf: line 47, column 25
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      planEvent.TriggerBasis = (__VALUE_TO_SET as typekey.DelinquencyTriggerBasis)
    }
    
    // 'value' attribute on TextCell (id=OffsetDays_Cell) at DelinquencyPlanEventsLV.pcf: line 53, column 42
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      planEvent.OffsetDays = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextCell (id=RelativeOrder_Cell) at DelinquencyPlanEventsLV.pcf: line 59, column 42
    function defaultSetter_24 (__VALUE_TO_SET :  java.lang.Object) : void {
      planEvent.RelativeOrder = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on BooleanRadioCell (id=Automatic_Cell) at DelinquencyPlanEventsLV.pcf: line 64, column 40
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      planEvent.Automatic = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyCell (id=EventName_Cell) at DelinquencyPlanEventsLV.pcf: line 39, column 53
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      planEvent.EventName = (__VALUE_TO_SET as typekey.DelinquencyEventName)
    }
    
    // 'validationExpression' attribute on TypeKeyCell (id=EventName_Cell) at DelinquencyPlanEventsLV.pcf: line 39, column 53
    function validationExpression_6 () : java.lang.Object {
      return gw.api.web.delinquency.DelinquencyPlanUtil.validateDelinquencyPlanEventUnique( planEvent )
    }
    
    // 'valueRange' attribute on RangeCell (id=TriggerBasis_Cell) at DelinquencyPlanEventsLV.pcf: line 47, column 25
    function valueRange_15 () : java.lang.Object {
      return DelinquencyTriggerBasis.getTypeKeys(false)
    }
    
    // 'value' attribute on TypeKeyCell (id=EventName_Cell) at DelinquencyPlanEventsLV.pcf: line 39, column 53
    function valueRoot_9 () : java.lang.Object {
      return planEvent
    }
    
    // 'value' attribute on RangeCell (id=TriggerBasis_Cell) at DelinquencyPlanEventsLV.pcf: line 47, column 25
    function value_12 () : typekey.DelinquencyTriggerBasis {
      return planEvent.TriggerBasis
    }
    
    // 'value' attribute on TextCell (id=OffsetDays_Cell) at DelinquencyPlanEventsLV.pcf: line 53, column 42
    function value_19 () : java.lang.Integer {
      return planEvent.OffsetDays
    }
    
    // 'value' attribute on TextCell (id=RelativeOrder_Cell) at DelinquencyPlanEventsLV.pcf: line 59, column 42
    function value_23 () : java.lang.Integer {
      return planEvent.RelativeOrder
    }
    
    // 'value' attribute on BooleanRadioCell (id=Automatic_Cell) at DelinquencyPlanEventsLV.pcf: line 64, column 40
    function value_27 () : java.lang.Boolean {
      return planEvent.Automatic
    }
    
    // 'value' attribute on TypeKeyCell (id=EventName_Cell) at DelinquencyPlanEventsLV.pcf: line 39, column 53
    function value_7 () : typekey.DelinquencyEventName {
      return planEvent.EventName
    }
    
    // 'valueRange' attribute on RangeCell (id=TriggerBasis_Cell) at DelinquencyPlanEventsLV.pcf: line 47, column 25
    function verifyValueRangeIsAllowedType_16 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=TriggerBasis_Cell) at DelinquencyPlanEventsLV.pcf: line 47, column 25
    function verifyValueRangeIsAllowedType_16 ($$arg :  typekey.DelinquencyTriggerBasis[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=TriggerBasis_Cell) at DelinquencyPlanEventsLV.pcf: line 47, column 25
    function verifyValueRange_17 () : void {
      var __valueRangeArg = DelinquencyTriggerBasis.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_16(__valueRangeArg)
    }
    
    property get planEvent () : entity.DelinquencyPlanEvent {
      return getIteratedValue(1) as entity.DelinquencyPlanEvent
    }
    
    
  }
  
  
}