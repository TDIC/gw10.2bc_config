package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DelinquencyProcessSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyProcessSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/search/DelinquencyProcessSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyProcessSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    function generateContactDisplayName(dsv : DelinquencySearchView) : String {
      return dsv.InsuredName;
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/DelinquencyProcessSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at DelinquencyProcessSearchScreen.pcf: line 47, column 31
    function action_13 () : void {
      AccountOverview.go(delinquencySearchView.Account)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at DelinquencyProcessSearchScreen.pcf: line 56, column 31
    function action_19 () : void {
      DelinquencyTargetDetailsForward.go( delinquencySearchView.Target )
    }
    
    // 'action' attribute on TypeKeyCell (id=Status_Cell) at DelinquencyProcessSearchScreen.pcf: line 62, column 63
    function action_24 () : void {
      pcf.DelinquencyProcess.go(delinquencySearchView.Account, delinquencySearchView.DelinquencyProcess)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at DelinquencyProcessSearchScreen.pcf: line 47, column 31
    function action_dest_14 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(delinquencySearchView.Account)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at DelinquencyProcessSearchScreen.pcf: line 56, column 31
    function action_dest_20 () : pcf.api.Destination {
      return pcf.DelinquencyTargetDetailsForward.createDestination( delinquencySearchView.Target )
    }
    
    // 'action' attribute on TypeKeyCell (id=Status_Cell) at DelinquencyProcessSearchScreen.pcf: line 62, column 63
    function action_dest_25 () : pcf.api.Destination {
      return pcf.DelinquencyProcess.createDestination(delinquencySearchView.Account, delinquencySearchView.DelinquencyProcess)
    }
    
    // 'available' attribute on TextCell (id=AccountNumber_Cell) at DelinquencyProcessSearchScreen.pcf: line 47, column 31
    function available_12 () : java.lang.Boolean {
      return delinquencySearchView.Account.ViewableByCurrentUser
    }
    
    // 'value' attribute on TextCell (id=AccountName_Cell) at DelinquencyProcessSearchScreen.pcf: line 80, column 68
    function valueRoot_35 () : java.lang.Object {
      return delinquencySearchView.Account
    }
    
    // 'value' attribute on DateCell (id=StartDate_Cell) at DelinquencyProcessSearchScreen.pcf: line 34, column 58
    function valueRoot_7 () : java.lang.Object {
      return delinquencySearchView
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at DelinquencyProcessSearchScreen.pcf: line 47, column 31
    function value_15 () : java.lang.String {
      return delinquencySearchView.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at DelinquencyProcessSearchScreen.pcf: line 56, column 31
    function value_21 () : gw.api.domain.delinquency.DelinquencyTarget {
      return delinquencySearchView.Target
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DelinquencyProcessSearchScreen.pcf: line 62, column 63
    function value_26 () : typekey.DelinquencyProcessStatus {
      return delinquencySearchView.Status
    }
    
    // 'value' attribute on TypeKeyCell (id=CurrentEvent_Cell) at DelinquencyProcessSearchScreen.pcf: line 69, column 31
    function value_29 () : typekey.DelinquencyEventName {
      return delinquencySearchView.CurrentEvent
    }
    
    // 'value' attribute on TextCell (id=Insured_Cell) at DelinquencyProcessSearchScreen.pcf: line 75, column 76
    function value_32 () : java.lang.String {
      return generateContactDisplayName(delinquencySearchView)
    }
    
    // 'value' attribute on TextCell (id=AccountName_Cell) at DelinquencyProcessSearchScreen.pcf: line 80, column 68
    function value_34 () : java.lang.String {
      return delinquencySearchView.Account.AccountName
    }
    
    // 'value' attribute on TextCell (id=State_Cell) at DelinquencyProcessSearchScreen.pcf: line 85, column 153
    function value_37 () : java.lang.String {
      return (delinquencySearchView.Target typeis PolicyPeriod) ? delinquencySearchView.Target.RiskJurisdiction.DisplayName : "" 
    }
    
    // 'value' attribute on DateCell (id=StartDate_Cell) at DelinquencyProcessSearchScreen.pcf: line 34, column 58
    function value_6 () : java.util.Date {
      return delinquencySearchView.StartDate
    }
    
    // 'value' attribute on TextCell (id=DelinquencyPlan_Cell) at DelinquencyProcessSearchScreen.pcf: line 40, column 53
    function value_9 () : entity.DelinquencyPlan {
      return delinquencySearchView.DelinquencyPlan
    }
    
    property get delinquencySearchView () : entity.DelinquencySearchView {
      return getIteratedValue(2) as entity.DelinquencySearchView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/DelinquencyProcessSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends DelinquencyProcessSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at DelinquencyProcessSearchScreen.pcf: line 17, column 58
    function def_onEnter_0 (def :  pcf.DelinquencyProcessSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at DelinquencyProcessSearchScreen.pcf: line 17, column 58
    function def_refreshVariables_1 (def :  pcf.DelinquencyProcessSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'maxSearchResults' attribute on SearchPanel at DelinquencyProcessSearchScreen.pcf: line 15, column 89
    function maxSearchResults_40 () : java.lang.Object {
      return gw.api.system.BCConfigParameters.MaxSearchResults.Value
    }
    
    // 'searchCriteria' attribute on SearchPanel at DelinquencyProcessSearchScreen.pcf: line 15, column 89
    function searchCriteria_42 () : gw.search.DelinquencySearchCriteria {
      return new gw.search.DelinquencySearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at DelinquencyProcessSearchScreen.pcf: line 15, column 89
    function search_41 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria)
    }
    
    // 'value' attribute on DateCell (id=StartDate_Cell) at DelinquencyProcessSearchScreen.pcf: line 34, column 58
    function sortValue_2 (delinquencySearchView :  entity.DelinquencySearchView) : java.lang.Object {
      return delinquencySearchView.StartDate
    }
    
    // 'value' attribute on TextCell (id=DelinquencyPlan_Cell) at DelinquencyProcessSearchScreen.pcf: line 40, column 53
    function sortValue_3 (delinquencySearchView :  entity.DelinquencySearchView) : java.lang.Object {
      return delinquencySearchView.DelinquencyPlan
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at DelinquencyProcessSearchScreen.pcf: line 47, column 31
    function sortValue_4 (delinquencySearchView :  entity.DelinquencySearchView) : java.lang.Object {
      return delinquencySearchView.AccountNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DelinquencyProcessSearchScreen.pcf: line 62, column 63
    function sortValue_5 (delinquencySearchView :  entity.DelinquencySearchView) : java.lang.Object {
      return delinquencySearchView.Status
    }
    
    // 'value' attribute on RowIterator at DelinquencyProcessSearchScreen.pcf: line 29, column 94
    function value_39 () : gw.api.database.IQueryBeanResult<entity.DelinquencySearchView> {
      return delinquencySearchViews
    }
    
    property get delinquencySearchViews () : gw.api.database.IQueryBeanResult<DelinquencySearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<DelinquencySearchView>
    }
    
    property get searchCriteria () : gw.search.DelinquencySearchCriteria {
      return getCriteriaValue(1) as gw.search.DelinquencySearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.DelinquencySearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}