package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopApprovals.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopApprovalsExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopApprovals.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopApprovalsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=DesktopApprovals) at DesktopApprovals.pcf: line 9, column 62
    static function canVisit_26 () : java.lang.Boolean {
      return perm.System.viewdesktop
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at DesktopApprovals.pcf: line 35, column 92
    function filters_1 () : gw.api.filters.IFilter[] {
      return new gw.api.web.approval.DesktopApprovalsFilterSet().FilterOptions
    }
    
    // 'initialValue' attribute on Variable at DesktopApprovals.pcf: line 15, column 56
    function initialValue_0 () : gw.api.web.approval.DesktopApprovalsView {
      return new gw.api.web.approval.DesktopApprovalsView()
    }
    
    // Page (id=DesktopApprovals) at DesktopApprovals.pcf: line 9, column 62
    static function parent_27 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'value' attribute on DateCell (id=DateCreated_Cell) at DesktopApprovals.pcf: line 42, column 45
    function sortValue_2 (approval :  gw.api.domain.approval.DesktopApprovalsViewItem) : java.lang.Object {
      return approval.DateCreated
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at DesktopApprovals.pcf: line 47, column 41
    function sortValue_3 (approval :  gw.api.domain.approval.DesktopApprovalsViewItem) : java.lang.Object {
      return approval.Subject
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at DesktopApprovals.pcf: line 53, column 40
    function sortValue_4 (approval :  gw.api.domain.approval.DesktopApprovalsViewItem) : java.lang.Object {
      return approval.Amount
    }
    
    // 'value' attribute on TypeKeyCell (id=ApprovalStatus_Cell) at DesktopApprovals.pcf: line 58, column 51
    function sortValue_5 (approval :  gw.api.domain.approval.DesktopApprovalsViewItem) : java.lang.Object {
      return approval.ApprovalStatus
    }
    
    // 'value' attribute on AltUserCell (id=ApprovingUser_Cell) at DesktopApprovals.pcf: line 62, column 47
    function sortValue_6 (approval :  gw.api.domain.approval.DesktopApprovalsViewItem) : java.lang.Object {
      return approval.ApprovingUser
    }
    
    // 'value' attribute on RowIterator at DesktopApprovals.pcf: line 31, column 73
    function value_25 () : gw.api.domain.approval.DesktopApprovalsViewItem[] {
      return approvals.DesktopApprovalsViewItems
    }
    
    override property get CurrentLocation () : pcf.DesktopApprovals {
      return super.CurrentLocation as pcf.DesktopApprovals
    }
    
    property get approvals () : gw.api.web.approval.DesktopApprovalsView {
      return getVariableValue("approvals", 0) as gw.api.web.approval.DesktopApprovalsView
    }
    
    property set approvals ($arg :  gw.api.web.approval.DesktopApprovalsView) {
      setVariableValue("approvals", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopApprovals.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DesktopApprovalsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at DesktopApprovals.pcf: line 62, column 47
    function action_20 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // MenuItem (id=UserBrowseMenuItem) at DesktopApprovals.pcf: line 62, column 47
    function action_dest_21 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at DesktopApprovals.pcf: line 53, column 40
    function currency_15 () : typekey.Currency {
      return approval.Amount.Currency
    }
    
    // 'value' attribute on DateCell (id=DateCreated_Cell) at DesktopApprovals.pcf: line 42, column 45
    function valueRoot_8 () : java.lang.Object {
      return approval
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at DesktopApprovals.pcf: line 47, column 41
    function value_10 () : java.lang.String {
      return approval.Subject
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at DesktopApprovals.pcf: line 53, column 40
    function value_13 () : gw.pl.currency.MonetaryAmount {
      return approval.Amount
    }
    
    // 'value' attribute on TypeKeyCell (id=ApprovalStatus_Cell) at DesktopApprovals.pcf: line 58, column 51
    function value_17 () : typekey.ApprovalStatus {
      return approval.ApprovalStatus
    }
    
    // 'value' attribute on AltUserCell (id=ApprovingUser_Cell) at DesktopApprovals.pcf: line 62, column 47
    function value_22 () : entity.User {
      return approval.ApprovingUser
    }
    
    // 'value' attribute on DateCell (id=DateCreated_Cell) at DesktopApprovals.pcf: line 42, column 45
    function value_7 () : java.util.Date {
      return approval.DateCreated
    }
    
    property get approval () : gw.api.domain.approval.DesktopApprovalsViewItem {
      return getIteratedValue(1) as gw.api.domain.approval.DesktopApprovalsViewItem
    }
    
    
  }
  
  
}