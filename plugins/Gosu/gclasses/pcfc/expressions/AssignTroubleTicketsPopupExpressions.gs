package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/AssignTroubleTicketsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AssignTroubleTicketsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/AssignTroubleTicketsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AssignTroubleTicketsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (assignmentPopup :  gw.api.web.troubleticket.TroubleTicketAssignmentPopup, selectedTroubleTickets :  TroubleTicket[]) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=AssignTroubleTicketsPopup) at AssignTroubleTicketsPopup.pcf: line 11, column 77
    function beforeCommit_2 (pickedValue :  gw.api.assignment.Assignee) : void {
      assignmentPopup.Picker.setSelection(pickedValue);gw.acc.activityenhancement.DesktopQueuedTroubleTicketsUtil.assignTroubleTickets(assignmentPopup, selectedTroubleTickets)
    }
    
    // 'def' attribute on ScreenRef at AssignTroubleTicketsPopup.pcf: line 21, column 77
    function def_onEnter_0 (def :  pcf.AssignmentPopupScreen) : void {
      def.onEnter(assignmentPopup, selectedTroubleTickets)
    }
    
    // 'def' attribute on ScreenRef at AssignTroubleTicketsPopup.pcf: line 21, column 77
    function def_refreshVariables_1 (def :  pcf.AssignmentPopupScreen) : void {
      def.refreshVariables(assignmentPopup, selectedTroubleTickets)
    }
    
    override property get CurrentLocation () : pcf.AssignTroubleTicketsPopup {
      return super.CurrentLocation as pcf.AssignTroubleTicketsPopup
    }
    
    property get assignmentPopup () : gw.api.web.troubleticket.TroubleTicketAssignmentPopup {
      return getVariableValue("assignmentPopup", 0) as gw.api.web.troubleticket.TroubleTicketAssignmentPopup
    }
    
    property set assignmentPopup ($arg :  gw.api.web.troubleticket.TroubleTicketAssignmentPopup) {
      setVariableValue("assignmentPopup", 0, $arg)
    }
    
    property get selectedTroubleTickets () : TroubleTicket[] {
      return getVariableValue("selectedTroubleTickets", 0) as TroubleTicket[]
    }
    
    property set selectedTroubleTickets ($arg :  TroubleTicket[]) {
      setVariableValue("selectedTroubleTickets", 0, $arg)
    }
    
    
  }
  
  
}