package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountAddPaymentPlanPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountAddPaymentPlanPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountAddPaymentPlanPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountAddPaymentPlanPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account, selectedPlans :  List<PaymentPlan>) : int {
      return 0
    }
    
    // 'initialValue' attribute on Variable at AccountAddPaymentPlanPopup.pcf: line 23, column 39
    function initialValue_0 () : List<PaymentPlan> {
      return gw.api.web.plan.PaymentPlans.findVisiblePaymentPlans(account.Currency).toList().where(\elt -> not selectedPlans.contains(elt))
    }
    
    // EditButtons (id=toolbarButtons) at AccountAddPaymentPlanPopup.pcf: line 35, column 34
    function label_2 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'pickValue' attribute on CheckedValuesToolbarButton (id=AddSelectedPaymentPlans) at AccountAddPaymentPlanPopup.pcf: line 30, column 38
    function pickValue_1 (CheckedValues :  entity.PaymentPlan[]) : PaymentPlan[] {
      return CheckedValues
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AccountAddPaymentPlanPopup.pcf: line 51, column 48
    function sortValue_3 (paymentPlan :  entity.PaymentPlan) : java.lang.Object {
      return paymentPlan.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AccountAddPaymentPlanPopup.pcf: line 55, column 48
    function sortValue_4 (paymentPlan :  entity.PaymentPlan) : java.lang.Object {
      return paymentPlan.Description
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at AccountAddPaymentPlanPopup.pcf: line 59, column 50
    function sortValue_5 (paymentPlan :  entity.PaymentPlan) : java.lang.Object {
      return paymentPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at AccountAddPaymentPlanPopup.pcf: line 63, column 51
    function sortValue_6 (paymentPlan :  entity.PaymentPlan) : java.lang.Object {
      return paymentPlan.ExpirationDate
    }
    
    // 'value' attribute on RowIterator (id=PaymentPlans) at AccountAddPaymentPlanPopup.pcf: line 45, column 64
    function value_21 () : java.util.List<entity.PaymentPlan> {
      return availablePlans
    }
    
    override property get CurrentLocation () : pcf.AccountAddPaymentPlanPopup {
      return super.CurrentLocation as pcf.AccountAddPaymentPlanPopup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get availablePlans () : List<PaymentPlan> {
      return getVariableValue("availablePlans", 0) as List<PaymentPlan>
    }
    
    property set availablePlans ($arg :  List<PaymentPlan>) {
      setVariableValue("availablePlans", 0, $arg)
    }
    
    property get selectedPlans () : List<PaymentPlan> {
      return getVariableValue("selectedPlans", 0) as List<PaymentPlan>
    }
    
    property set selectedPlans ($arg :  List<PaymentPlan>) {
      setVariableValue("selectedPlans", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountAddPaymentPlanPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountAddPaymentPlanPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at AccountAddPaymentPlanPopup.pcf: line 51, column 48
    function action_7 () : void {
      PaymentPlanDetail.go(paymentPlan)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at AccountAddPaymentPlanPopup.pcf: line 51, column 48
    function action_dest_8 () : pcf.api.Destination {
      return pcf.PaymentPlanDetail.createDestination(paymentPlan)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AccountAddPaymentPlanPopup.pcf: line 51, column 48
    function valueRoot_10 () : java.lang.Object {
      return paymentPlan
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AccountAddPaymentPlanPopup.pcf: line 55, column 48
    function value_12 () : java.lang.String {
      return paymentPlan.Description
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at AccountAddPaymentPlanPopup.pcf: line 59, column 50
    function value_15 () : java.util.Date {
      return paymentPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at AccountAddPaymentPlanPopup.pcf: line 63, column 51
    function value_18 () : java.util.Date {
      return paymentPlan.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AccountAddPaymentPlanPopup.pcf: line 51, column 48
    function value_9 () : java.lang.String {
      return paymentPlan.DisplayName
    }
    
    property get paymentPlan () : entity.PaymentPlan {
      return getIteratedValue(1) as entity.PaymentPlan
    }
    
    
  }
  
  
}