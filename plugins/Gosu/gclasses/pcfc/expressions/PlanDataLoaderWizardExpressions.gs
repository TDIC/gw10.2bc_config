package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataLoaderWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanDataLoaderWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataLoaderWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanDataLoaderWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterFinish' attribute on Wizard (id=PlanDataLoaderWizard) at PlanDataLoaderWizard.pcf: line 8, column 36
    function afterFinish_10 () : void {
      PlanDataUploadConfirmation.go(processor)
    }
    
    // 'afterFinish' attribute on Wizard (id=PlanDataLoaderWizard) at PlanDataLoaderWizard.pcf: line 8, column 36
    function afterFinish_dest_11 () : pcf.api.Destination {
      return pcf.PlanDataUploadConfirmation.createDestination(processor)
    }
    
    // 'initialValue' attribute on Variable at PlanDataLoaderWizard.pcf: line 12, column 72
    function initialValue_0 () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return new tdic.util.dataloader.processor.BCPlanDataLoaderProcessor()
    }
    
    // 'onExit' attribute on WizardStep (id=upload) at PlanDataLoaderWizard.pcf: line 17, column 81
    function onExit_1 () : void {
      processor.readAndImportFile()
    }
    
    // 'onExit' attribute on WizardStep (id=review) at PlanDataLoaderWizard.pcf: line 23, column 81
    function onExit_4 () : void {
      processor.loadData()
    }
    
    // 'screen' attribute on WizardStep (id=upload) at PlanDataLoaderWizard.pcf: line 17, column 81
    function screen_onEnter_2 (def :  pcf.PlanDataUploadImportScreen) : void {
      def.onEnter(processor)
    }
    
    // 'screen' attribute on WizardStep (id=review) at PlanDataLoaderWizard.pcf: line 23, column 81
    function screen_onEnter_5 (def :  pcf.PlanDataUploadReviewScreen) : void {
      def.onEnter(processor)
    }
    
    // 'screen' attribute on WizardStep (id=upload) at PlanDataLoaderWizard.pcf: line 17, column 81
    function screen_refreshVariables_3 (def :  pcf.PlanDataUploadImportScreen) : void {
      def.refreshVariables(processor)
    }
    
    // 'screen' attribute on WizardStep (id=review) at PlanDataLoaderWizard.pcf: line 23, column 81
    function screen_refreshVariables_6 (def :  pcf.PlanDataUploadReviewScreen) : void {
      def.refreshVariables(processor)
    }
    
    // 'tabBar' attribute on Wizard (id=PlanDataLoaderWizard) at PlanDataLoaderWizard.pcf: line 8, column 36
    function tabBar_onEnter_8 (def :  pcf.InternalToolsTabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=PlanDataLoaderWizard) at PlanDataLoaderWizard.pcf: line 8, column 36
    function tabBar_refreshVariables_9 (def :  pcf.InternalToolsTabBar) : void {
      def.refreshVariables()
    }
    
    // '$$wizardStepAvailable' attribute on WizardStep (id=review) at PlanDataLoaderWizard.pcf: line 23, column 81
    function wizardStepAvailable_7 () : java.lang.Boolean {
      return processor != null
    }
    
    override property get CurrentLocation () : pcf.PlanDataLoaderWizard {
      return super.CurrentLocation as pcf.PlanDataLoaderWizard
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getVariableValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setVariableValue("processor", 0, $arg)
    }
    
    
  }
  
  
}