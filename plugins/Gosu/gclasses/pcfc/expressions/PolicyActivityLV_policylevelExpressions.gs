package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/PolicyActivityLV.policylevel.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyActivityLV_policylevelExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/PolicyActivityLV.policylevel.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyActivityLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Gross_Cell) at PolicyActivityLV.policylevel.pcf: line 47, column 151
    function currency_21 () : typekey.Currency {
      return tAccountOwner.Currency
    }
    
    // 'initialValue' attribute on Variable at PolicyActivityLV.policylevel.pcf: line 26, column 38
    function initialValue_10 () : entity.TAccountOwner {
      return policyPeriodInfo.TAccountOwner
    }
    
    // 'initialValue' attribute on Variable at PolicyActivityLV.policylevel.pcf: line 22, column 30
    function initialValue_9 () : PolicyPeriod {
      return policyPeriodInfo.TAccountOwner typeis PolicyPeriod ? policyPeriodInfo.TAccountOwner : null
    }
    
    // RowIterator at PolicyActivityLV.policylevel.pcf: line 18, column 72
    function initializeVariables_29 () : void {
        policyPeriod = policyPeriodInfo.TAccountOwner typeis PolicyPeriod ? policyPeriodInfo.TAccountOwner : null;
  tAccountOwner = policyPeriodInfo.TAccountOwner;

    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at PolicyActivityLV.policylevel.pcf: line 31, column 46
    function valueRoot_12 () : java.lang.Object {
      return tAccountOwner
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at PolicyActivityLV.policylevel.pcf: line 36, column 50
    function valueRoot_15 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at PolicyActivityLV.policylevel.pcf: line 31, column 46
    function value_11 () : java.lang.String {
      return tAccountOwner.DisplayName
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at PolicyActivityLV.policylevel.pcf: line 36, column 50
    function value_14 () : java.util.Date {
      return policyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at PolicyActivityLV.policylevel.pcf: line 40, column 52
    function value_17 () : java.util.Date {
      return policyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Gross_Cell) at PolicyActivityLV.policylevel.pcf: line 47, column 151
    function value_20 () : gw.pl.currency.MonetaryAmount {
      return policyPeriodInfo.FilteredInvoiceItemViewsArray.sum( agencyBillStatementView.Producer.Currency, \ b -> b.getGross( viewOption ))
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Commission_Cell) at PolicyActivityLV.policylevel.pcf: line 54, column 156
    function value_23 () : gw.pl.currency.MonetaryAmount {
      return policyPeriodInfo.FilteredInvoiceItemViewsArray.sum( agencyBillStatementView.Producer.Currency, \ b -> b.getCommission( viewOption ))
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Net_Cell) at PolicyActivityLV.policylevel.pcf: line 61, column 149
    function value_26 () : gw.pl.currency.MonetaryAmount {
      return policyPeriodInfo.FilteredInvoiceItemViewsArray.sum( agencyBillStatementView.Producer.Currency, \ b -> b.getNet( viewOption ))
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 1) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 1, $arg)
    }
    
    property get policyPeriodInfo () : gw.api.web.invoice.ChargeOwnerWithInvoiceItemsView {
      return getIteratedValue(1) as gw.api.web.invoice.ChargeOwnerWithInvoiceItemsView
    }
    
    property get tAccountOwner () : entity.TAccountOwner {
      return getVariableValue("tAccountOwner", 1) as entity.TAccountOwner
    }
    
    property set tAccountOwner ($arg :  entity.TAccountOwner) {
      setVariableValue("tAccountOwner", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/PolicyActivityLV.policylevel.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyActivityLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at PolicyActivityLV.policylevel.pcf: line 31, column 46
    function sortValue_0 (policyPeriodInfo :  gw.api.web.invoice.ChargeOwnerWithInvoiceItemsView) : java.lang.Object {
      var policyPeriod : PolicyPeriod = (policyPeriodInfo.TAccountOwner typeis PolicyPeriod ? policyPeriodInfo.TAccountOwner : null)
var tAccountOwner : entity.TAccountOwner = (policyPeriodInfo.TAccountOwner)
return tAccountOwner.DisplayName
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at PolicyActivityLV.policylevel.pcf: line 36, column 50
    function sortValue_1 (policyPeriodInfo :  gw.api.web.invoice.ChargeOwnerWithInvoiceItemsView) : java.lang.Object {
      var policyPeriod : PolicyPeriod = (policyPeriodInfo.TAccountOwner typeis PolicyPeriod ? policyPeriodInfo.TAccountOwner : null)
var tAccountOwner : entity.TAccountOwner = (policyPeriodInfo.TAccountOwner)
return policyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at PolicyActivityLV.policylevel.pcf: line 40, column 52
    function sortValue_2 (policyPeriodInfo :  gw.api.web.invoice.ChargeOwnerWithInvoiceItemsView) : java.lang.Object {
      var policyPeriod : PolicyPeriod = (policyPeriodInfo.TAccountOwner typeis PolicyPeriod ? policyPeriodInfo.TAccountOwner : null)
var tAccountOwner : entity.TAccountOwner = (policyPeriodInfo.TAccountOwner)
return policyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Gross_Cell) at PolicyActivityLV.policylevel.pcf: line 47, column 151
    function sortValue_3 (policyPeriodInfo :  gw.api.web.invoice.ChargeOwnerWithInvoiceItemsView) : java.lang.Object {
      var policyPeriod : PolicyPeriod = (policyPeriodInfo.TAccountOwner typeis PolicyPeriod ? policyPeriodInfo.TAccountOwner : null)
var tAccountOwner : entity.TAccountOwner = (policyPeriodInfo.TAccountOwner)
return policyPeriodInfo.FilteredInvoiceItemViewsArray.sum( agencyBillStatementView.Producer.Currency, \ b -> b.getGross( viewOption ))
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Commission_Cell) at PolicyActivityLV.policylevel.pcf: line 54, column 156
    function sortValue_4 (policyPeriodInfo :  gw.api.web.invoice.ChargeOwnerWithInvoiceItemsView) : java.lang.Object {
      var policyPeriod : PolicyPeriod = (policyPeriodInfo.TAccountOwner typeis PolicyPeriod ? policyPeriodInfo.TAccountOwner : null)
var tAccountOwner : entity.TAccountOwner = (policyPeriodInfo.TAccountOwner)
return policyPeriodInfo.FilteredInvoiceItemViewsArray.sum( agencyBillStatementView.Producer.Currency, \ b -> b.getCommission( viewOption ))
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Net_Cell) at PolicyActivityLV.policylevel.pcf: line 61, column 149
    function sortValue_5 (policyPeriodInfo :  gw.api.web.invoice.ChargeOwnerWithInvoiceItemsView) : java.lang.Object {
      var policyPeriod : PolicyPeriod = (policyPeriodInfo.TAccountOwner typeis PolicyPeriod ? policyPeriodInfo.TAccountOwner : null)
var tAccountOwner : entity.TAccountOwner = (policyPeriodInfo.TAccountOwner)
return policyPeriodInfo.FilteredInvoiceItemViewsArray.sum( agencyBillStatementView.Producer.Currency, \ b -> b.getNet( viewOption ))
    }
    
    // 'footerSumValue' attribute on RowIterator at PolicyActivityLV.policylevel.pcf: line 47, column 151
    function sumValue_6 (policyPeriodInfo :  gw.api.web.invoice.ChargeOwnerWithInvoiceItemsView) : java.lang.Object {
      return policyPeriodInfo.FilteredInvoiceItemViewsArray.sum( agencyBillStatementView.Producer.Currency, \ b -> b.getGross( viewOption ))
    }
    
    // 'footerSumValue' attribute on RowIterator at PolicyActivityLV.policylevel.pcf: line 54, column 156
    function sumValue_7 (policyPeriodInfo :  gw.api.web.invoice.ChargeOwnerWithInvoiceItemsView) : java.lang.Object {
      return policyPeriodInfo.FilteredInvoiceItemViewsArray.sum( agencyBillStatementView.Producer.Currency, \ b -> b.getCommission( viewOption ))
    }
    
    // 'footerSumValue' attribute on RowIterator at PolicyActivityLV.policylevel.pcf: line 61, column 149
    function sumValue_8 (policyPeriodInfo :  gw.api.web.invoice.ChargeOwnerWithInvoiceItemsView) : java.lang.Object {
      return policyPeriodInfo.FilteredInvoiceItemViewsArray.sum( agencyBillStatementView.Producer.Currency, \ b -> b.getNet( viewOption ))
    }
    
    // 'value' attribute on RowIterator at PolicyActivityLV.policylevel.pcf: line 18, column 72
    function value_30 () : gw.api.web.invoice.ChargeOwnerWithInvoiceItemsView[] {
      return agencyBillStatementView.getPolicyPeriodInvoiceItemsFilteredAsArray(viewOption)
    }
    
    property get agencyBillStatementView () : gw.api.web.invoice.AgencyBillStatementView {
      return getRequireValue("agencyBillStatementView", 0) as gw.api.web.invoice.AgencyBillStatementView
    }
    
    property set agencyBillStatementView ($arg :  gw.api.web.invoice.AgencyBillStatementView) {
      setRequireValue("agencyBillStatementView", 0, $arg)
    }
    
    property get viewOption () : InvoiceItemViewOption {
      return getRequireValue("viewOption", 0) as InvoiceItemViewOption
    }
    
    property set viewOption ($arg :  InvoiceItemViewOption) {
      setRequireValue("viewOption", 0, $arg)
    }
    
    
  }
  
  
}