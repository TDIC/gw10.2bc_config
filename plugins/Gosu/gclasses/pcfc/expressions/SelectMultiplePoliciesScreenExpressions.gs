package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultiplePoliciesScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SelectMultiplePoliciesScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultiplePoliciesScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends SelectMultiplePoliciesScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=CancelButton) at SelectMultiplePoliciesScreen.pcf: line 37, column 64
    function action_1 () : void {
      CurrentLocation.cancel()
    }
    
    // 'def' attribute on PanelRef at SelectMultiplePoliciesScreen.pcf: line 27, column 53
    function def_onEnter_2 (def :  pcf.PolicySearchDV) : void {
      def.onEnter(searchCriteria, false)
    }
    
    // 'def' attribute on PanelRef at SelectMultiplePoliciesScreen.pcf: line 41, column 104
    function def_onEnter_4 (def :  pcf.PolicySearchResultsLV) : void {
      def.onEnter(policySearchViews, null, isWizard, showHyperlinks, showCheckboxes)
    }
    
    // 'def' attribute on PanelRef at SelectMultiplePoliciesScreen.pcf: line 27, column 53
    function def_refreshVariables_3 (def :  pcf.PolicySearchDV) : void {
      def.refreshVariables(searchCriteria, false)
    }
    
    // 'def' attribute on PanelRef at SelectMultiplePoliciesScreen.pcf: line 41, column 104
    function def_refreshVariables_5 (def :  pcf.PolicySearchResultsLV) : void {
      def.refreshVariables(policySearchViews, null, isWizard, showHyperlinks, showCheckboxes)
    }
    
    // 'pickValue' attribute on CheckedValuesToolbarButton (id=addbutton) at SelectMultiplePoliciesScreen.pcf: line 33, column 90
    function pickValue_0 (CheckedValues :  entity.PolicySearchView[]) : Policy[] {
      return gw.api.web.search.SearchPopupUtil.getPolicyArray(CheckedValues)
    }
    
    // 'searchCriteria' attribute on SearchPanel at SelectMultiplePoliciesScreen.pcf: line 25, column 84
    function searchCriteria_7 () : gw.search.PolicySearchCriteria {
      return new gw.search.PolicySearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at SelectMultiplePoliciesScreen.pcf: line 25, column 84
    function search_6 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria, StringCriterionMode.TC_STARTSWITH)
    }
    
    property get policySearchViews () : gw.api.database.IQueryBeanResult<PolicySearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<PolicySearchView>
    }
    
    property get searchCriteria () : gw.search.PolicySearchCriteria {
      return getCriteriaValue(1) as gw.search.PolicySearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.PolicySearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultiplePoliciesScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SelectMultiplePoliciesScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get isWizard () : Boolean {
      return getVariableValue("isWizard", 0) as Boolean
    }
    
    property set isWizard ($arg :  Boolean) {
      setVariableValue("isWizard", 0, $arg)
    }
    
    property get showCheckboxes () : Boolean {
      return getVariableValue("showCheckboxes", 0) as Boolean
    }
    
    property set showCheckboxes ($arg :  Boolean) {
      setVariableValue("showCheckboxes", 0, $arg)
    }
    
    property get showHyperlinks () : Boolean {
      return getVariableValue("showHyperlinks", 0) as Boolean
    }
    
    property set showHyperlinks ($arg :  Boolean) {
      setVariableValue("showHyperlinks", 0, $arg)
    }
    
    
  }
  
  
}