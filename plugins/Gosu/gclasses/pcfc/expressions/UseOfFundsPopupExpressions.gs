package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/UseOfFundsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class UseOfFundsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/UseOfFundsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class UseOfFundsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (fundsUseTracker :  FundsUseTracker) : int {
      return 0
    }
    
    // 'actionAvailable' attribute on TextInput (id=source_Input) at UseOfFundsPopup.pcf: line 21, column 199
    function actionAvailable_1 () : java.lang.Boolean {
      return fundsUseTracker.TypeBeingTracked != FundsUseType.TC_UNAPPLIEDFUND
    }
    
    // 'action' attribute on TextInput (id=source_Input) at UseOfFundsPopup.pcf: line 21, column 199
    function action_0 () : void {
      fundsUseTracker.openTrackableDetails()
    }
    
    // 'def' attribute on PanelRef at UseOfFundsPopup.pcf: line 25, column 49
    function def_onEnter_4 (def :  pcf.FundsAllotmentLV) : void {
      def.onEnter(fundsUseTracker)
    }
    
    // 'def' attribute on PanelRef at UseOfFundsPopup.pcf: line 25, column 49
    function def_refreshVariables_5 (def :  pcf.FundsAllotmentLV) : void {
      def.refreshVariables(fundsUseTracker)
    }
    
    // 'value' attribute on TextInput (id=source_Input) at UseOfFundsPopup.pcf: line 21, column 199
    function value_2 () : java.lang.String {
      return DisplayKey.get("Web.FundsUse.LongDescription", fundsUseTracker.Description, fundsUseTracker.EventDate.format("short"), fundsUseTracker.TotalAmount.render())
    }
    
    override property get CurrentLocation () : pcf.UseOfFundsPopup {
      return super.CurrentLocation as pcf.UseOfFundsPopup
    }
    
    property get fundsUseTracker () : FundsUseTracker {
      return getVariableValue("fundsUseTracker", 0) as FundsUseTracker
    }
    
    property set fundsUseTracker ($arg :  FundsUseTracker) {
      setVariableValue("fundsUseTracker", 0, $arg)
    }
    
    
  }
  
  
}