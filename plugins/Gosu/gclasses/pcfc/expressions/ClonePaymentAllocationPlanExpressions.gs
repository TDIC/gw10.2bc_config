package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/allocationplan/ClonePaymentAllocationPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ClonePaymentAllocationPlanExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/allocationplan/ClonePaymentAllocationPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClonePaymentAllocationPlanExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (paymentAllocationPlan :  PaymentAllocationPlan) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=ClonePaymentAllocationPlan) at ClonePaymentAllocationPlan.pcf: line 13, column 78
    function afterCancel_3 () : void {
      PaymentAllocationPlanDetail.go(paymentAllocationPlan)
    }
    
    // 'afterCancel' attribute on Page (id=ClonePaymentAllocationPlan) at ClonePaymentAllocationPlan.pcf: line 13, column 78
    function afterCancel_dest_4 () : pcf.api.Destination {
      return pcf.PaymentAllocationPlanDetail.createDestination(paymentAllocationPlan)
    }
    
    // 'afterCommit' attribute on Page (id=ClonePaymentAllocationPlan) at ClonePaymentAllocationPlan.pcf: line 13, column 78
    function afterCommit_5 (TopLocation :  pcf.api.Location) : void {
      PaymentAllocationPlanDetail.go(clonedPaymentAllocationPlan)
    }
    
    // 'def' attribute on ScreenRef at ClonePaymentAllocationPlan.pcf: line 24, column 77
    function def_onEnter_1 (def :  pcf.PaymentAllocationPlanDetailScreen) : void {
      def.onEnter(clonedPaymentAllocationPlan)
    }
    
    // 'def' attribute on ScreenRef at ClonePaymentAllocationPlan.pcf: line 24, column 77
    function def_refreshVariables_2 (def :  pcf.PaymentAllocationPlanDetailScreen) : void {
      def.refreshVariables(clonedPaymentAllocationPlan)
    }
    
    // 'initialValue' attribute on Variable at ClonePaymentAllocationPlan.pcf: line 22, column 44
    function initialValue_0 () : entity.PaymentAllocationPlan {
      return paymentAllocationPlan.makeClone() as PaymentAllocationPlan
    }
    
    // 'parent' attribute on Page (id=ClonePaymentAllocationPlan) at ClonePaymentAllocationPlan.pcf: line 13, column 78
    static function parent_6 (paymentAllocationPlan :  PaymentAllocationPlan) : pcf.api.Destination {
      return pcf.PaymentAllocationPlans.createDestination()
    }
    
    override property get CurrentLocation () : pcf.ClonePaymentAllocationPlan {
      return super.CurrentLocation as pcf.ClonePaymentAllocationPlan
    }
    
    property get clonedPaymentAllocationPlan () : entity.PaymentAllocationPlan {
      return getVariableValue("clonedPaymentAllocationPlan", 0) as entity.PaymentAllocationPlan
    }
    
    property set clonedPaymentAllocationPlan ($arg :  entity.PaymentAllocationPlan) {
      setVariableValue("clonedPaymentAllocationPlan", 0, $arg)
    }
    
    property get paymentAllocationPlan () : PaymentAllocationPlan {
      return getVariableValue("paymentAllocationPlan", 0) as PaymentAllocationPlan
    }
    
    property set paymentAllocationPlan ($arg :  PaymentAllocationPlan) {
      setVariableValue("paymentAllocationPlan", 0, $arg)
    }
    
    
  }
  
  
}