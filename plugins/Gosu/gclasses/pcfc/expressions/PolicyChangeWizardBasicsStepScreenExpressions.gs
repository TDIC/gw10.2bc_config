package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyChangeWizardBasicsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyChangeWizardBasicsStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyChangeWizardBasicsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyChangeWizardBasicsStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateInput (id=PolicyChangeDate_Input) at PolicyChangeWizardBasicsStepScreen.pcf: line 21, column 50
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyChange.ModificationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at PolicyChangeWizardBasicsStepScreen.pcf: line 26, column 45
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyChange.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=SpecialHandling_Input) at PolicyChangeWizardBasicsStepScreen.pcf: line 32, column 48
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyChange.SpecialHandling = (__VALUE_TO_SET as typekey.SpecialHandling)
    }
    
    // 'value' attribute on DateInput (id=PolicyChangeDate_Input) at PolicyChangeWizardBasicsStepScreen.pcf: line 21, column 50
    function valueRoot_2 () : java.lang.Object {
      return policyChange
    }
    
    // 'value' attribute on DateInput (id=PolicyChangeDate_Input) at PolicyChangeWizardBasicsStepScreen.pcf: line 21, column 50
    function value_0 () : java.util.Date {
      return policyChange.ModificationDate
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at PolicyChangeWizardBasicsStepScreen.pcf: line 26, column 45
    function value_4 () : java.lang.String {
      return policyChange.Description
    }
    
    // 'value' attribute on TypeKeyInput (id=SpecialHandling_Input) at PolicyChangeWizardBasicsStepScreen.pcf: line 32, column 48
    function value_8 () : typekey.SpecialHandling {
      return policyChange.SpecialHandling
    }
    
    property get policyChange () : PolicyChange {
      return getRequireValue("policyChange", 0) as PolicyChange
    }
    
    property set policyChange ($arg :  PolicyChange) {
      setRequireValue("policyChange", 0, $arg)
    }
    
    
  }
  
  
}