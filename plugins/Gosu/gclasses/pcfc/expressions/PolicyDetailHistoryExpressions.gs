package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailHistory.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailHistoryExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailHistory.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyDetailHistoryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Transaction_Cell) at PolicyDetailHistory.pcf: line 49, column 29
    function action_10 () : void {
      TransactionDetailPopup.push(policyHistory.Transaction)
    }
    
    // 'action' attribute on TextCell (id=Description_Cell) at PolicyDetailHistory.pcf: line 57, column 50
    function action_16 () : void {
      goToTransferDetail(policyHistory)
    }
    
    // 'action' attribute on TextCell (id=PolicyPeriod_Cell) at PolicyDetailHistory.pcf: line 73, column 29
    function action_25 () : void {
      PolicyOverview.push(policyHistory.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=User_Cell) at PolicyDetailHistory.pcf: line 81, column 29
    function action_30 () : void {
      UserDetailPage.push(policyHistory.User)
    }
    
    // 'action' attribute on TextCell (id=Transaction_Cell) at PolicyDetailHistory.pcf: line 49, column 29
    function action_dest_11 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(policyHistory.Transaction)
    }
    
    // 'action' attribute on TextCell (id=PolicyPeriod_Cell) at PolicyDetailHistory.pcf: line 73, column 29
    function action_dest_26 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(policyHistory.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=User_Cell) at PolicyDetailHistory.pcf: line 81, column 29
    function action_dest_31 () : pcf.api.Destination {
      return pcf.UserDetailPage.createDestination(policyHistory.User)
    }
    
    // 'available' attribute on TextCell (id=Description_Cell) at PolicyDetailHistory.pcf: line 57, column 50
    function available_15 () : java.lang.Boolean {
      return isTransfer(policyHistory)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=TransactionAmount_Cell) at PolicyDetailHistory.pcf: line 65, column 45
    function currency_23 () : typekey.Currency {
      return policyHistory.Amount.Currency
    }
    
    // 'value' attribute on TextCell (id=Transaction_Cell) at PolicyDetailHistory.pcf: line 49, column 29
    function valueRoot_13 () : java.lang.Object {
      return policyHistory.Transaction
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at PolicyDetailHistory.pcf: line 42, column 48
    function valueRoot_8 () : java.lang.Object {
      return policyHistory
    }
    
    // 'value' attribute on TextCell (id=Transaction_Cell) at PolicyDetailHistory.pcf: line 49, column 29
    function value_12 () : java.lang.String {
      return policyHistory.Transaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at PolicyDetailHistory.pcf: line 57, column 50
    function value_17 () : java.lang.String {
      return policyHistory.Description
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TransactionAmount_Cell) at PolicyDetailHistory.pcf: line 65, column 45
    function value_21 () : gw.pl.currency.MonetaryAmount {
      return policyHistory.Amount
    }
    
    // 'value' attribute on TextCell (id=PolicyPeriod_Cell) at PolicyDetailHistory.pcf: line 73, column 29
    function value_27 () : entity.PolicyPeriod {
      return policyHistory.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=User_Cell) at PolicyDetailHistory.pcf: line 81, column 29
    function value_32 () : entity.User {
      return policyHistory.User
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at PolicyDetailHistory.pcf: line 42, column 48
    function value_7 () : java.util.Date {
      return policyHistory.EventDate
    }
    
    property get policyHistory () : entity.PolicyHistory {
      return getIteratedValue(1) as entity.PolicyHistory
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailHistory.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailHistoryExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=PolicyDetailHistory) at PolicyDetailHistory.pcf: line 9, column 71
    static function canVisit_36 (plcyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcyhistview and not plcyPeriod.Archived
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at PolicyDetailHistory.pcf: line 35, column 93
    function filters_1 () : gw.api.filters.IFilter[] {
      return new gw.api.web.history.PolicyHistoriesFilters().getFilterOptions()
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailHistory.pcf: line 18, column 38
    function initialValue_0 () : entity.PolicyHistory[] {
      return plcyPeriod.PolicyHistories?.toTypedArray()
    }
    
    // Page (id=PolicyDetailHistory) at PolicyDetailHistory.pcf: line 9, column 71
    static function parent_37 (plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyGroup.createDestination(plcyPeriod)
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at PolicyDetailHistory.pcf: line 42, column 48
    function sortValue_2 (policyHistory :  entity.PolicyHistory) : java.lang.Object {
      return policyHistory.EventDate
    }
    
    // 'value' attribute on TextCell (id=Transaction_Cell) at PolicyDetailHistory.pcf: line 49, column 29
    function sortValue_3 (policyHistory :  entity.PolicyHistory) : java.lang.Object {
      return policyHistory.Transaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at PolicyDetailHistory.pcf: line 57, column 50
    function sortValue_4 (policyHistory :  entity.PolicyHistory) : java.lang.Object {
      return policyHistory.Description
    }
    
    // 'value' attribute on TextCell (id=PolicyPeriod_Cell) at PolicyDetailHistory.pcf: line 73, column 29
    function sortValue_5 (policyHistory :  entity.PolicyHistory) : java.lang.Object {
      return policyHistory.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=User_Cell) at PolicyDetailHistory.pcf: line 81, column 29
    function sortValue_6 (policyHistory :  entity.PolicyHistory) : java.lang.Object {
      return policyHistory.User
    }
    
    // 'value' attribute on RowIterator at PolicyDetailHistory.pcf: line 30, column 46
    function value_35 () : entity.PolicyHistory[] {
      return policyHistories
    }
    
    override property get CurrentLocation () : pcf.PolicyDetailHistory {
      return super.CurrentLocation as pcf.PolicyDetailHistory
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 0, $arg)
    }
    
    property get policyHistories () : entity.PolicyHistory[] {
      return getVariableValue("policyHistories", 0) as entity.PolicyHistory[]
    }
    
    property set policyHistories ($arg :  entity.PolicyHistory[]) {
      setVariableValue("policyHistories", 0, $arg)
    }
    
    
    function goToTransferDetail( item : PolicyHistory ) {
          if ( item.EventType == TC_POLICYTRANSFERRED )
          PolicyTransferDetail.go( item );
          if ( item.EventType == TC_PRODUCERTRANSFERRED )
          ProducerTransferDetail.go( item as ProducerPolicyHistory );
          }
    
          function isTransfer( item : PolicyHistory ) : Boolean {
          return ( item.EventType == TC_POLICYTRANSFERRED || item.EventType == TC_PRODUCERTRANSFERRED )
          }
        
    
    
  }
  
  
}