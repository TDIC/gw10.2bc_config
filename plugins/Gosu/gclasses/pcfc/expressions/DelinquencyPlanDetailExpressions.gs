package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyPlanDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyPlanDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (dlnqPlan :  DelinquencyPlan) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Page (id=DelinquencyPlanDetail) at DelinquencyPlanDetail.pcf: line 14, column 79
    function beforeCommit_2 (pickedValue :  java.lang.Object) : void {
      validatePlan( dlnqPlan )
    }
    
    // 'canEdit' attribute on Page (id=DelinquencyPlanDetail) at DelinquencyPlanDetail.pcf: line 14, column 79
    function canEdit_3 () : java.lang.Boolean {
      return perm.System.delplanedit
    }
    
    // 'canVisit' attribute on Page (id=DelinquencyPlanDetail) at DelinquencyPlanDetail.pcf: line 14, column 79
    static function canVisit_4 (dlnqPlan :  DelinquencyPlan) : java.lang.Boolean {
      return perm.System.admintabview
    }
    
    // 'def' attribute on ScreenRef at DelinquencyPlanDetail.pcf: line 21, column 52
    function def_onEnter_0 (def :  pcf.DelinquencyPlanDetailScreen) : void {
      def.onEnter(dlnqPlan)
    }
    
    // 'def' attribute on ScreenRef at DelinquencyPlanDetail.pcf: line 21, column 52
    function def_refreshVariables_1 (def :  pcf.DelinquencyPlanDetailScreen) : void {
      def.refreshVariables(dlnqPlan)
    }
    
    // 'onResume' attribute on Page (id=DelinquencyPlanDetail) at DelinquencyPlanDetail.pcf: line 14, column 79
    function onResume_5 () : void {
      preValidatePlan( dlnqPlan )
    }
    
    // 'parent' attribute on Page (id=DelinquencyPlanDetail) at DelinquencyPlanDetail.pcf: line 14, column 79
    static function parent_6 (dlnqPlan :  DelinquencyPlan) : pcf.api.Destination {
      return pcf.DelinquencyPlans.createDestination()
    }
    
    // 'startEditing' attribute on Page (id=DelinquencyPlanDetail) at DelinquencyPlanDetail.pcf: line 14, column 79
    function startEditing_7 () : void {
      preValidatePlan( dlnqPlan )
    }
    
    // 'title' attribute on Page (id=DelinquencyPlanDetail) at DelinquencyPlanDetail.pcf: line 14, column 79
    static function title_8 (dlnqPlan :  DelinquencyPlan) : java.lang.Object {
      return DisplayKey.get("Web.DelinquencyPlan.Title",  dlnqPlan )
    }
    
    override property get CurrentLocation () : pcf.DelinquencyPlanDetail {
      return super.CurrentLocation as pcf.DelinquencyPlanDetail
    }
    
    property get dlnqPlan () : DelinquencyPlan {
      return getVariableValue("dlnqPlan", 0) as DelinquencyPlan
    }
    
    property set dlnqPlan ($arg :  DelinquencyPlan) {
      setVariableValue("dlnqPlan", 0, $arg)
    }
    
    
    /**
     */
          function preValidatePlan( plan : DelinquencyPlan ) {
              validatePlan( plan );
              gw.api.web.delinquency.DelinquencyPlanUtil.validateDelinquencyPlanEvents( plan );
            }
            function validatePlan( plan : DelinquencyPlan ) {
              gw.api.web.delinquency.DelinquencyPlanUtil.validateDelinquencyPlanReasons( plan );
            }
        
    
    
  }
  
  
}