package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ContactSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ContactSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/search/ContactSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ContactSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get isProducerContact () : boolean {
      return getRequireValue("isProducerContact", 0) as java.lang.Boolean
    }
    
    property set isProducerContact ($arg :  boolean) {
      setRequireValue("isProducerContact", 0, $arg)
    }
    
    function createSearchCriteria() : ContactSearchCriteria{
      var c = new ContactSearchCriteria()
      c.ContactSubtype = TC_COMPANY
      return c
    }
    
    function doSearch(criteria : ContactSearchCriteria) : gw.plugin.contact.impl.ContactResultWrapper {
      var result = criteria.performSearch()
      if (result.warningMessage.HasContent ) {
        gw.api.util.LocationUtil.addRequestScopedWarningMessage(result.warningMessage)
      } else if (result.contactResults == null or result.contactResults.IsEmpty) {
        gw.api.util.LocationUtil.addRequestScopedWarningMessage(DisplayKey.get("Java.Search.NoResults"))
      }
      return result
    }
    
    /*Read: if you are implementing external ProducerContact from CM. Simply do 2 steps: 
     1. take this function out.
     2. remove the isProducerContact boolean from: ContactSearchScreen, ContactSearchPopup, AccountDetailContacts, PolicyDetailContacts
        and ProducerContacts pcfs.
      This temp function is added only to in-line with PC since they are not implementing external ProducerContact for 7.x version now.
    */
    function doProducerContactInternalSearch(criteria : ContactSearchCriteria) : gw.plugin.contact.impl.ContactResultWrapper {
      var result = criteria.performProducerContactInternalSearch()
      if (result.contactResults == null or result.contactResults.IsEmpty) {
        gw.api.util.LocationUtil.addRequestScopedWarningMessage(DisplayKey.get("Java.Search.NoResults"))
      }
      return result
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/ContactSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickValue' attribute on RowIterator at ContactSearchScreen.pcf: line 32, column 59
    function pickValue_22 () : gw.plugin.contact.ContactResult {
      return contactSearchResultEntry
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at ContactSearchScreen.pcf: line 39, column 63
    function valueRoot_8 () : java.lang.Object {
      return contactSearchResultEntry
    }
    
    // 'value' attribute on TextCell (id=Address_Cell) at ContactSearchScreen.pcf: line 44, column 66
    function value_10 () : java.lang.String {
      return contactSearchResultEntry.DisplayAddress
    }
    
    // 'value' attribute on TextCell (id=Phone_Cell) at ContactSearchScreen.pcf: line 49, column 31
    function value_13 () : java.lang.String {
      return contactSearchResultEntry.PrimaryPhoneValue
    }
    
    // 'value' attribute on TextCell (id=Email_Cell) at ContactSearchScreen.pcf: line 54, column 31
    function value_16 () : java.lang.String {
      return contactSearchResultEntry.EmailAddress1
    }
    
    // 'value' attribute on TypeKeyCell (id=ContactType_Cell) at ContactSearchScreen.pcf: line 64, column 46
    function value_19 () : typekey.Contact {
      return contactSearchResultEntry.ContactType
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at ContactSearchScreen.pcf: line 39, column 63
    function value_7 () : java.lang.String {
      return contactSearchResultEntry.DisplayName
    }
    
    property get contactSearchResultEntry () : gw.plugin.contact.ContactResult {
      return getIteratedValue(2) as gw.plugin.contact.ContactResult
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/ContactSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends ContactSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at ContactSearchScreen.pcf: line 20, column 47
    function def_onEnter_0 (def :  pcf.ContactSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at ContactSearchScreen.pcf: line 20, column 47
    function def_refreshVariables_1 (def :  pcf.ContactSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'searchCriteria' attribute on SearchPanel at ContactSearchScreen.pcf: line 18, column 71
    function searchCriteria_25 () : entity.ContactSearchCriteria {
      return createSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at ContactSearchScreen.pcf: line 18, column 71
    function search_24 () : java.lang.Object {
      return isProducerContact? doProducerContactInternalSearch(searchCriteria) : doSearch(searchCriteria)
    }
    
    // 'sortBy' attribute on TextCell (id=Name_Cell) at ContactSearchScreen.pcf: line 39, column 63
    function sortValue_2 (contactSearchResultEntry :  gw.plugin.contact.ContactResult) : java.lang.Object {
      return contactSearchResultEntry.SortByName
    }
    
    // 'value' attribute on TextCell (id=Address_Cell) at ContactSearchScreen.pcf: line 44, column 66
    function sortValue_3 (contactSearchResultEntry :  gw.plugin.contact.ContactResult) : java.lang.Object {
      return contactSearchResultEntry.DisplayAddress
    }
    
    // 'value' attribute on TextCell (id=Phone_Cell) at ContactSearchScreen.pcf: line 49, column 31
    function sortValue_4 (contactSearchResultEntry :  gw.plugin.contact.ContactResult) : java.lang.Object {
      return contactSearchResultEntry.PrimaryPhoneValue
    }
    
    // 'value' attribute on TextCell (id=Email_Cell) at ContactSearchScreen.pcf: line 54, column 31
    function sortValue_5 (contactSearchResultEntry :  gw.plugin.contact.ContactResult) : java.lang.Object {
      return contactSearchResultEntry.EmailAddress1
    }
    
    // 'value' attribute on TypeKeyCell (id=ContactType_Cell) at ContactSearchScreen.pcf: line 64, column 46
    function sortValue_6 (contactSearchResultEntry :  gw.plugin.contact.ContactResult) : java.lang.Object {
      return contactSearchResultEntry.ContactType
    }
    
    // 'value' attribute on RowIterator at ContactSearchScreen.pcf: line 32, column 59
    function value_23 () : gw.plugin.contact.ContactResult[] {
      return contactSearchResult.contactResults
    }
    
    property get contactSearchResult () : gw.plugin.contact.impl.ContactResultWrapper {
      return getResultsValue(1) as gw.plugin.contact.impl.ContactResultWrapper
    }
    
    property get searchCriteria () : entity.ContactSearchCriteria {
      return getCriteriaValue(1) as entity.ContactSearchCriteria
    }
    
    property set searchCriteria ($arg :  entity.ContactSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}