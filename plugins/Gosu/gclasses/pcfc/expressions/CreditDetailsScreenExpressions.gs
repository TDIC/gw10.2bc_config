package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/CreditDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreditDetailsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/CreditDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreditDetailsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at CreditDetailsScreen.pcf: line 53, column 34
    function currency_23 () : typekey.Currency {
      return credit.Currency
    }
    
    // 'value' attribute on RangeInput (id=creditType_Input) at CreditDetailsScreen.pcf: line 46, column 43
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      credit.CreditType = (__VALUE_TO_SET as typekey.CreditType)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at CreditDetailsScreen.pcf: line 53, column 34
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      credit.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on RangeInput (id=DesignatedUnapplieds_Input) at CreditDetailsScreen.pcf: line 35, column 56
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      targetUnapplied = (__VALUE_TO_SET as entity.UnappliedFund)
    }
    
    // 'initialValue' attribute on Variable at CreditDetailsScreen.pcf: line 16, column 29
    function initialValue_0 () : UnappliedFund {
      return (credit.UnappliedFund != null and credit.UnappliedFund != account.DefaultUnappliedFund) ? credit.UnappliedFund : null
    }
    
    // 'onChange' attribute on PostOnChange at CreditDetailsScreen.pcf: line 37, column 64
    function onChange_3 () : void {
      credit.UnappliedFund = targetUnapplied
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=Amount_Input) at CreditDetailsScreen.pcf: line 53, column 34
    function validationExpression_19 () : java.lang.Object {
      return validateCreditAmount()
    }
    
    // 'valueRange' attribute on RangeInput (id=creditType_Input) at CreditDetailsScreen.pcf: line 46, column 43
    function valueRange_15 () : java.lang.Object {
      return CreditType.getTypeKeys(false)
    }
    
    // 'valueRange' attribute on RangeInput (id=DesignatedUnapplieds_Input) at CreditDetailsScreen.pcf: line 35, column 56
    function valueRange_7 () : java.lang.Object {
      return account.UnappliedFundsSortedByDisplayName
    }
    
    // 'value' attribute on RangeInput (id=creditType_Input) at CreditDetailsScreen.pcf: line 46, column 43
    function valueRoot_14 () : java.lang.Object {
      return credit
    }
    
    // 'value' attribute on TextInput (id=Account_Input) at CreditDetailsScreen.pcf: line 26, column 39
    function value_1 () : entity.Account {
      return account
    }
    
    // 'value' attribute on RangeInput (id=creditType_Input) at CreditDetailsScreen.pcf: line 46, column 43
    function value_12 () : typekey.CreditType {
      return credit.CreditType
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at CreditDetailsScreen.pcf: line 53, column 34
    function value_20 () : gw.pl.currency.MonetaryAmount {
      return credit.Amount
    }
    
    // 'value' attribute on RangeInput (id=DesignatedUnapplieds_Input) at CreditDetailsScreen.pcf: line 35, column 56
    function value_5 () : entity.UnappliedFund {
      return targetUnapplied
    }
    
    // 'valueRange' attribute on RangeInput (id=creditType_Input) at CreditDetailsScreen.pcf: line 46, column 43
    function verifyValueRangeIsAllowedType_16 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=creditType_Input) at CreditDetailsScreen.pcf: line 46, column 43
    function verifyValueRangeIsAllowedType_16 ($$arg :  typekey.CreditType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DesignatedUnapplieds_Input) at CreditDetailsScreen.pcf: line 35, column 56
    function verifyValueRangeIsAllowedType_8 ($$arg :  entity.UnappliedFund[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DesignatedUnapplieds_Input) at CreditDetailsScreen.pcf: line 35, column 56
    function verifyValueRangeIsAllowedType_8 ($$arg :  gw.api.database.IQueryBeanResult<entity.UnappliedFund>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DesignatedUnapplieds_Input) at CreditDetailsScreen.pcf: line 35, column 56
    function verifyValueRangeIsAllowedType_8 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=creditType_Input) at CreditDetailsScreen.pcf: line 46, column 43
    function verifyValueRange_17 () : void {
      var __valueRangeArg = CreditType.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_16(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=DesignatedUnapplieds_Input) at CreditDetailsScreen.pcf: line 35, column 56
    function verifyValueRange_9 () : void {
      var __valueRangeArg = account.UnappliedFundsSortedByDisplayName
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_8(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=DesignatedUnapplieds_Input) at CreditDetailsScreen.pcf: line 35, column 56
    function visible_4 () : java.lang.Boolean {
      return account.HasDesignatedUnappliedFund
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get credit () : Credit {
      return getRequireValue("credit", 0) as Credit
    }
    
    property set credit ($arg :  Credit) {
      setRequireValue("credit", 0, $arg)
    }
    
    property get targetUnapplied () : UnappliedFund {
      return getVariableValue("targetUnapplied", 0) as UnappliedFund
    }
    
    property set targetUnapplied ($arg :  UnappliedFund) {
      setVariableValue("targetUnapplied", 0, $arg)
    }
    
    function validateCreditAmount(): String {
                        if (!credit.Amount.IsPositive) {
                            return DisplayKey.get("Web.NewCreditWizard.NonPositiveCreditAmount")
                        }
                        return null;
                    }
    
                    function updateUnapplied() {
                      if (targetUnapplied == null) {
                        targetUnapplied = account.DefaultUnappliedFund
                      }
                      credit.UnappliedFund = targetUnapplied
                    }
    
    
  }
  
  
}