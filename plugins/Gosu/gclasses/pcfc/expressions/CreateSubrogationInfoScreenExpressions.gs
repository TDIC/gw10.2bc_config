package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/CreateSubrogationInfoScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreateSubrogationInfoScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/account/CreateSubrogationInfoScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreateSubrogationInfoScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at CreateSubrogationInfoScreen.pcf: line 44, column 39
    function currency_14 () : typekey.Currency {
      return subrogation.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at CreateSubrogationInfoScreen.pcf: line 44, column 39
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      subrogation.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextInput (id=ClaimNumber_Input) at CreateSubrogationInfoScreen.pcf: line 32, column 44
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      subrogation.ClaimNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=Amount_Input) at CreateSubrogationInfoScreen.pcf: line 44, column 39
    function validationExpression_10 () : java.lang.Object {
      return subrogation.Amount.IsZero ? DisplayKey.get("Web.CreateSubrogationInfoScreen.NonZeroAmount") : null
    }
    
    // 'value' attribute on TextInput (id=Account_Input) at CreateSubrogationInfoScreen.pcf: line 21, column 60
    function valueRoot_1 () : java.lang.Object {
      return subrogation.SourceAccount
    }
    
    // 'value' attribute on TextInput (id=ClaimNumber_Input) at CreateSubrogationInfoScreen.pcf: line 32, column 44
    function valueRoot_8 () : java.lang.Object {
      return subrogation
    }
    
    // 'value' attribute on TextInput (id=Account_Input) at CreateSubrogationInfoScreen.pcf: line 21, column 60
    function value_0 () : java.lang.String {
      return subrogation.SourceAccount.AccountNumber
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at CreateSubrogationInfoScreen.pcf: line 44, column 39
    function value_11 () : gw.pl.currency.MonetaryAmount {
      return subrogation.Amount
    }
    
    // 'value' attribute on TextInput (id=AccountInformation_Input) at CreateSubrogationInfoScreen.pcf: line 25, column 67
    function value_3 () : java.lang.String {
      return subrogation.SourceAccount.AccountNameLocalized
    }
    
    // 'value' attribute on TextInput (id=ClaimNumber_Input) at CreateSubrogationInfoScreen.pcf: line 32, column 44
    function value_6 () : java.lang.String {
      return subrogation.ClaimNumber
    }
    
    property get subrogation () : Subrogation {
      return getRequireValue("subrogation", 0) as Subrogation
    }
    
    property set subrogation ($arg :  Subrogation) {
      setRequireValue("subrogation", 0, $arg)
    }
    
    
  }
  
  
}