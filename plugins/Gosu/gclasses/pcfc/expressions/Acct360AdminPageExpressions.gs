package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/systemAdmin/Acct360AdminPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class Acct360AdminPageExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/acct360/systemAdmin/Acct360AdminPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Acct360AdminPageExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'pickLocation' attribute on PickerInput (id=findacct_Input) at Acct360AdminPage.pcf: line 33, column 34
    function action_0 () : void {
      AccountSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=findacct_Input) at Acct360AdminPage.pcf: line 33, column 34
    function action_dest_1 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'afterReturnFromPopup' attribute on Page (id=Acct360AdminPage) at Acct360AdminPage.pcf: line 11, column 84
    function afterReturnFromPopup_63 (popupCommitted :  boolean) : void {
      transactions = gw.acc.acct360.accountview.AccountBalanceTxnUtil.createTransactionRecords(account,null,null,null)
    }
    
    // 'value' attribute on PickerInput (id=findacct_Input) at Acct360AdminPage.pcf: line 33, column 34
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      account = (__VALUE_TO_SET as Account)
    }
    
    // 'onPick' attribute on PickerInput (id=findacct_Input) at Acct360AdminPage.pcf: line 33, column 34
    function onPick_2 (PickedValue :  Account) : void {
      transactions = gw.acc.acct360.accountview.AccountBalanceTxnUtil.createTransactionRecords(account,null,null,null)
    }
    
    // Page (id=Acct360AdminPage) at Acct360AdminPage.pcf: line 11, column 84
    static function parent_64 () : pcf.api.Destination {
      return pcf.ServerTools.createDestination()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=paidamt_Cell) at Acct360AdminPage.pcf: line 114, column 57
    function sortValue_10 (trans :  gw.acc.acct360.accountview.Acct360TranDetail) : java.lang.Object {
      return trans.AcctBalanceTran.PaidAmount
    }
    
    // 'value' attribute on TextCell (id=newdesc_Cell) at Acct360AdminPage.pcf: line 91, column 35
    function sortValue_6 (trans :  gw.acc.acct360.accountview.Acct360TranDetail) : java.lang.Object {
      return trans.Description
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amt_Cell) at Acct360AdminPage.pcf: line 99, column 40
    function sortValue_7 (trans :  gw.acc.acct360.accountview.Acct360TranDetail) : java.lang.Object {
      return trans.NetAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=balanceamt_Cell) at Acct360AdminPage.pcf: line 104, column 60
    function sortValue_8 (trans :  gw.acc.acct360.accountview.Acct360TranDetail) : java.lang.Object {
      return trans.AcctBalanceTran.BalanceAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=dueeamt_Cell) at Acct360AdminPage.pcf: line 109, column 56
    function sortValue_9 (trans :  gw.acc.acct360.accountview.Acct360TranDetail) : java.lang.Object {
      return trans.AcctBalanceTran.DueAmount
    }
    
    // 'value' attribute on PickerInput (id=findacct_Input) at Acct360AdminPage.pcf: line 33, column 34
    function value_3 () : Account {
      return account
    }
    
    // 'value' attribute on RowIterator (id=TransactionActivitiesDetailsIterator) at Acct360AdminPage.pcf: line 44, column 70
    function value_62 () : gw.acc.acct360.accountview.Acct360TranDetail[] {
      return transactions
    }
    
    override property get CurrentLocation () : pcf.Acct360AdminPage {
      return super.CurrentLocation as pcf.Acct360AdminPage
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get transactions () : gw.acc.acct360.accountview.Acct360TranDetail[] {
      return getVariableValue("transactions", 0) as gw.acc.acct360.accountview.Acct360TranDetail[]
    }
    
    property set transactions ($arg :  gw.acc.acct360.accountview.Acct360TranDetail[]) {
      setVariableValue("transactions", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/acct360/systemAdmin/Acct360AdminPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends Acct360AdminPageExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=PolicyNumber_Cell) at Acct360AdminPage.pcf: line 73, column 35
    function actionAvailable_21 () : java.lang.Boolean {
      return trans.PolicyPeriod!=null
    }
    
    // 'actionAvailable' attribute on MonetaryAmountCell (id=amt_Cell) at Acct360AdminPage.pcf: line 99, column 40
    function actionAvailable_37 () : java.lang.Boolean {
      return trans.AcctBalanceTran.Transactions.Count > 0
    }
    
    // 'action' attribute on Link (id=edit) at Acct360AdminPage.pcf: line 53, column 42
    function action_11 () : void {
      Acct360EditRowPopup.push(trans.AcctBalanceTran)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at Acct360AdminPage.pcf: line 73, column 35
    function action_19 () : void {
      PolicyDetailSummaryPopup.push(trans.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=newdesc_Cell) at Acct360AdminPage.pcf: line 91, column 35
    function action_31 () : void {
      gw.acc.acct360.accountview.AccountBalanceTxnUtil.navigate360(account, trans)
    }
    
    // 'action' attribute on MonetaryAmountCell (id=amt_Cell) at Acct360AdminPage.pcf: line 99, column 40
    function action_35 () : void {
      Acct360ViewTransactionsPopup.push(trans.AcctBalanceTran.Transactions)
    }
    
    // 'action' attribute on Link (id=edit) at Acct360AdminPage.pcf: line 53, column 42
    function action_dest_12 () : pcf.api.Destination {
      return pcf.Acct360EditRowPopup.createDestination(trans.AcctBalanceTran)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at Acct360AdminPage.pcf: line 73, column 35
    function action_dest_20 () : pcf.api.Destination {
      return pcf.PolicyDetailSummaryPopup.createDestination(trans.PolicyPeriod)
    }
    
    // 'action' attribute on MonetaryAmountCell (id=amt_Cell) at Acct360AdminPage.pcf: line 99, column 40
    function action_dest_36 () : pcf.api.Destination {
      return pcf.Acct360ViewTransactionsPopup.createDestination(trans.AcctBalanceTran.Transactions)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=amt_Cell) at Acct360AdminPage.pcf: line 99, column 40
    function currency_40 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on DateCell (id=TransactionDate_Cell) at Acct360AdminPage.pcf: line 59, column 46
    function valueRoot_14 () : java.lang.Object {
      return trans
    }
    
    // 'value' attribute on TextCell (id=product_Cell) at Acct360AdminPage.pcf: line 79, column 36
    function valueRoot_26 () : java.lang.Object {
      return trans.PolicyPeriod.Policy
    }
    
    // 'value' attribute on TextCell (id=jurisdiction_Cell) at Acct360AdminPage.pcf: line 85, column 41
    function valueRoot_29 () : java.lang.Object {
      return trans.PolicyPeriod
    }
    
    // 'value' attribute on MonetaryAmountCell (id=balanceamt_Cell) at Acct360AdminPage.pcf: line 104, column 60
    function valueRoot_43 () : java.lang.Object {
      return trans.AcctBalanceTran
    }
    
    // 'value' attribute on DateCell (id=TransactionDate_Cell) at Acct360AdminPage.pcf: line 59, column 46
    function value_13 () : java.util.Date {
      return trans.TransactionDate
    }
    
    // 'value' attribute on DateCell (id=effDate_Cell) at Acct360AdminPage.pcf: line 64, column 44
    function value_16 () : java.util.Date {
      return trans.EffectiveDate
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at Acct360AdminPage.pcf: line 73, column 35
    function value_22 () : String {
      return trans.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=product_Cell) at Acct360AdminPage.pcf: line 79, column 36
    function value_25 () : LOBCode {
      return trans.PolicyPeriod.Policy.LOBCode
    }
    
    // 'value' attribute on TextCell (id=jurisdiction_Cell) at Acct360AdminPage.pcf: line 85, column 41
    function value_28 () : Jurisdiction {
      return trans.PolicyPeriod.RiskJurisdiction
    }
    
    // 'value' attribute on TextCell (id=newdesc_Cell) at Acct360AdminPage.pcf: line 91, column 35
    function value_32 () : String {
      return trans.Description
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amt_Cell) at Acct360AdminPage.pcf: line 99, column 40
    function value_38 () : gw.pl.currency.MonetaryAmount {
      return trans.NetAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=balanceamt_Cell) at Acct360AdminPage.pcf: line 104, column 60
    function value_42 () : gw.pl.currency.MonetaryAmount {
      return trans.AcctBalanceTran.BalanceAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=dueeamt_Cell) at Acct360AdminPage.pcf: line 109, column 56
    function value_46 () : gw.pl.currency.MonetaryAmount {
      return trans.AcctBalanceTran.DueAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=paidamt_Cell) at Acct360AdminPage.pcf: line 114, column 57
    function value_50 () : gw.pl.currency.MonetaryAmount {
      return trans.AcctBalanceTran.PaidAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OutstandingAmount_Cell) at Acct360AdminPage.pcf: line 121, column 49
    function value_54 () : gw.pl.currency.MonetaryAmount {
      return trans.OutstandingBalance
    }
    
    // 'value' attribute on MonetaryAmountCell (id=balance_Cell) at Acct360AdminPage.pcf: line 127, column 44
    function value_58 () : gw.pl.currency.MonetaryAmount {
      return trans.BalanceAmount
    }
    
    property get trans () : gw.acc.acct360.accountview.Acct360TranDetail {
      return getIteratedValue(1) as gw.acc.acct360.accountview.Acct360TranDetail
    }
    
    
  }
  
  
}