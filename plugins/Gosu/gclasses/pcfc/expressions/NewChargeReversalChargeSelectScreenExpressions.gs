package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalChargeSelectScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewChargeReversalChargeSelectScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalChargeSelectScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewChargeReversalChargeSelectScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalChargeSelectScreen.pcf: line 26, column 64
    function def_onEnter_1 (def :  pcf.NewChargeReversalChargesLV) : void {
      def.onEnter(Reversal, Charges)
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalChargeSelectScreen.pcf: line 26, column 64
    function def_refreshVariables_2 (def :  pcf.NewChargeReversalChargesLV) : void {
      def.refreshVariables(Reversal, Charges)
    }
    
    // 'initialValue' attribute on Variable at NewChargeReversalChargeSelectScreen.pcf: line 17, column 62
    function initialValue_0 () : gw.api.database.IQueryBeanResult<Charge> {
      return SearchCriteria.performSearch()
    }
    
    property get Charges () : gw.api.database.IQueryBeanResult<Charge> {
      return getVariableValue("Charges", 0) as gw.api.database.IQueryBeanResult<Charge>
    }
    
    property set Charges ($arg :  gw.api.database.IQueryBeanResult<Charge>) {
      setVariableValue("Charges", 0, $arg)
    }
    
    property get Reversal () : ChargeReversal {
      return getRequireValue("Reversal", 0) as ChargeReversal
    }
    
    property set Reversal ($arg :  ChargeReversal) {
      setRequireValue("Reversal", 0, $arg)
    }
    
    property get SearchCriteria () : gw.search.ReversibleChargeSearchCriteria {
      return getRequireValue("SearchCriteria", 0) as gw.search.ReversibleChargeSearchCriteria
    }
    
    property set SearchCriteria ($arg :  gw.search.ReversibleChargeSearchCriteria) {
      setRequireValue("SearchCriteria", 0, $arg)
    }
    
    
  }
  
  
}