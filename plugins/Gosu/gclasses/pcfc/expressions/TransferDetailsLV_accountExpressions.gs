package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/TransferDetailsLV.Account.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransferDetailsLV_accountExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/TransferDetailsLV.Account.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TransferDetailsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=TargetAccount_Cell) at TransferDetailsLV.Account.pcf: line 42, column 38
    function action_6 () : void {
      AccountSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerCell (id=TargetAccount_Cell) at TransferDetailsLV.Account.pcf: line 42, column 38
    function action_dest_7 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at TransferDetailsLV.Account.pcf: line 60, column 42
    function currency_28 () : typekey.Currency {
      return singleTransfer.Currency
    }
    
    // 'value' attribute on PickerCell (id=TargetAccount_Cell) at TransferDetailsLV.Account.pcf: line 42, column 38
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      singleTransfer.TargetAccount = (__VALUE_TO_SET as entity.Account)
    }
    
    // 'value' attribute on RangeCell (id=TargetUnappliedFund_Cell) at TransferDetailsLV.Account.pcf: line 52, column 45
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      singleTransfer.TargetUnapplied = (__VALUE_TO_SET as entity.UnappliedFund)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at TransferDetailsLV.Account.pcf: line 60, column 42
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      singleTransfer.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'editable' attribute on PickerCell (id=TargetAccount_Cell) at TransferDetailsLV.Account.pcf: line 42, column 38
    function editable_8 () : java.lang.Boolean {
      return canEditPage
    }
    
    // 'highlighted' attribute on Row at TransferDetailsLV.Account.pcf: line 32, column 67
    function highlighted_34 () : java.lang.Boolean {
      return singleTransfer.OpenApprovalActivity != null
    }
    
    // 'inputConversion' attribute on PickerCell (id=TargetAccount_Cell) at TransferDetailsLV.Account.pcf: line 42, column 38
    function inputConversion_9 (VALUE :  java.lang.String) : java.lang.Object {
      return accountSearchConverter.getAccount(VALUE)
    }
    
    // 'valueRange' attribute on RangeCell (id=TargetUnappliedFund_Cell) at TransferDetailsLV.Account.pcf: line 52, column 45
    function valueRange_19 () : java.lang.Object {
      return singleTransfer.TargetAccount.UnappliedFundsSortedByDisplayName
    }
    
    // 'value' attribute on PickerCell (id=TargetAccount_Cell) at TransferDetailsLV.Account.pcf: line 42, column 38
    function valueRoot_12 () : java.lang.Object {
      return singleTransfer
    }
    
    // 'value' attribute on PickerCell (id=TargetAccount_Cell) at TransferDetailsLV.Account.pcf: line 42, column 38
    function value_10 () : entity.Account {
      return singleTransfer.TargetAccount
    }
    
    // 'value' attribute on RangeCell (id=TargetUnappliedFund_Cell) at TransferDetailsLV.Account.pcf: line 52, column 45
    function value_16 () : entity.UnappliedFund {
      return singleTransfer.TargetUnapplied
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at TransferDetailsLV.Account.pcf: line 60, column 42
    function value_25 () : gw.pl.currency.MonetaryAmount {
      return singleTransfer.Amount
    }
    
    // 'value' attribute on TextCell (id=Activity_Cell) at TransferDetailsLV.Account.pcf: line 73, column 38
    function value_31 () : java.lang.String {
      return singleTransfer.ApprovalRequiredForCurrentUser ? DisplayKey.get("Web.TransferDetailsDV.ApprovalRequired") : null
    }
    
    // 'valueRange' attribute on RangeCell (id=TargetUnappliedFund_Cell) at TransferDetailsLV.Account.pcf: line 52, column 45
    function verifyValueRangeIsAllowedType_20 ($$arg :  entity.UnappliedFund[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=TargetUnappliedFund_Cell) at TransferDetailsLV.Account.pcf: line 52, column 45
    function verifyValueRangeIsAllowedType_20 ($$arg :  gw.api.database.IQueryBeanResult<entity.UnappliedFund>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=TargetUnappliedFund_Cell) at TransferDetailsLV.Account.pcf: line 52, column 45
    function verifyValueRangeIsAllowedType_20 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=TargetUnappliedFund_Cell) at TransferDetailsLV.Account.pcf: line 52, column 45
    function verifyValueRange_21 () : void {
      var __valueRangeArg = singleTransfer.TargetAccount.UnappliedFundsSortedByDisplayName
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_20(__valueRangeArg)
    }
    
    // 'visible' attribute on TextCell (id=Activity_Cell) at TransferDetailsLV.Account.pcf: line 73, column 38
    function visible_32 () : java.lang.Boolean {
      return not canEditPage
    }
    
    property get singleTransfer () : entity.FundsTransfer {
      return getIteratedValue(1) as entity.FundsTransfer
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/TransferDetailsLV.Account.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransferDetailsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'editable' attribute on RowIterator at TransferDetailsLV.Account.pcf: line 30, column 42
    function editable_5 () : java.lang.Boolean {
      return canEditPage
    }
    
    // 'initialValue' attribute on Variable at TransferDetailsLV.Account.pcf: line 22, column 57
    function initialValue_0 () : gw.api.web.account.AccountSearchConverter {
      return new gw.api.web.account.AccountSearchConverter(StringCriterionMode.TC_STARTSWITH)
    }
    
    // 'value' attribute on RangeCell (id=TargetUnappliedFund_Cell) at TransferDetailsLV.Account.pcf: line 52, column 45
    function sortValue_1 (singleTransfer :  entity.FundsTransfer) : java.lang.Object {
      return singleTransfer.TargetUnapplied
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at TransferDetailsLV.Account.pcf: line 60, column 42
    function sortValue_2 (singleTransfer :  entity.FundsTransfer) : java.lang.Object {
      return singleTransfer.Amount
    }
    
    // 'value' attribute on TextCell (id=Activity_Cell) at TransferDetailsLV.Account.pcf: line 73, column 38
    function sortValue_3 (singleTransfer :  entity.FundsTransfer) : java.lang.Object {
      return singleTransfer.ApprovalRequiredForCurrentUser ? DisplayKey.get("Web.TransferDetailsDV.ApprovalRequired") : null
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at TransferDetailsLV.Account.pcf: line 30, column 42
    function toCreateAndAdd_35 () : entity.FundsTransfer {
      return createAndAddTransfer()
    }
    
    // 'toRemove' attribute on RowIterator at TransferDetailsLV.Account.pcf: line 30, column 42
    function toRemove_36 (singleTransfer :  entity.FundsTransfer) : void {
      removeRow( singleTransfer )
    }
    
    // 'value' attribute on RowIterator at TransferDetailsLV.Account.pcf: line 30, column 42
    function value_37 () : entity.FundsTransfer[] {
      return fundsTransferUtil.TransferTargets
    }
    
    // 'visible' attribute on TextCell (id=Activity_Cell) at TransferDetailsLV.Account.pcf: line 73, column 38
    function visible_4 () : java.lang.Boolean {
      return not canEditPage
    }
    
    property get accountSearchConverter () : gw.api.web.account.AccountSearchConverter {
      return getVariableValue("accountSearchConverter", 0) as gw.api.web.account.AccountSearchConverter
    }
    
    property set accountSearchConverter ($arg :  gw.api.web.account.AccountSearchConverter) {
      setVariableValue("accountSearchConverter", 0, $arg)
    }
    
    property get canEditPage () : boolean {
      return getRequireValue("canEditPage", 0) as java.lang.Boolean
    }
    
    property set canEditPage ($arg :  boolean) {
      setRequireValue("canEditPage", 0, $arg)
    }
    
    property get fundsTransferUtil () : gw.api.web.transaction.FundsTransferUtil {
      return getRequireValue("fundsTransferUtil", 0) as gw.api.web.transaction.FundsTransferUtil
    }
    
    property set fundsTransferUtil ($arg :  gw.api.web.transaction.FundsTransferUtil) {
      setRequireValue("fundsTransferUtil", 0, $arg)
    }
    
    function removeRow(transfer : FundsTransfer) {
      transfer.remove();
      fundsTransferUtil.removeFromTransfers(transfer);
    }
    
    function createAndAddTransfer() : FundsTransfer {
      var transfer =  new FundsTransfer(fundsTransferUtil.SourceOwner.Currency)
      fundsTransferUtil.addToTransfers( transfer )
      return transfer
    }
    
    
  }
  
  
}