package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralManualDrawdownAmountScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CollateralManualDrawdownAmountScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/collateral/CollateralManualDrawdownAmountScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CollateralManualDrawdownAmountScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=cashHeld_Input) at CollateralManualDrawdownAmountScreen.pcf: line 23, column 58
    function currency_1 () : typekey.Currency {
      return collateralDrawdown.Collateral.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=drawdownAmount_Input) at CollateralManualDrawdownAmountScreen.pcf: line 44, column 45
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      collateralDrawdown.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'onChange' attribute on PostOnChange at CollateralManualDrawdownAmountScreen.pcf: line 46, column 65
    function onChange_9 () : void {
      collateralDrawdown.calculateShortfall()
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=drawdownAmount_Input) at CollateralManualDrawdownAmountScreen.pcf: line 44, column 45
    function validationExpression_10 () : java.lang.Object {
      return collateralDrawdown.isAmountValid() ? null : DisplayKey.get("Web.CollateralManualDrawdownAmountScreen.AmountWarning")
    }
    
    // 'value' attribute on MonetaryAmountInput (id=drawdownAmount_Input) at CollateralManualDrawdownAmountScreen.pcf: line 44, column 45
    function valueRoot_13 () : java.lang.Object {
      return collateralDrawdown
    }
    
    // 'value' attribute on MonetaryAmountInput (id=cashHeld_Input) at CollateralManualDrawdownAmountScreen.pcf: line 23, column 58
    function value_0 () : gw.pl.currency.MonetaryAmount {
      return collateralDrawdown.getCashAvailable()
    }
    
    // 'value' attribute on MonetaryAmountInput (id=drawdownAmount_Input) at CollateralManualDrawdownAmountScreen.pcf: line 44, column 45
    function value_11 () : gw.pl.currency.MonetaryAmount {
      return collateralDrawdown.Amount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=disbursementPending_Input) at CollateralManualDrawdownAmountScreen.pcf: line 29, column 64
    function value_3 () : gw.pl.currency.MonetaryAmount {
      return collateralDrawdown.getDisbursementPending()
    }
    
    // 'value' attribute on MonetaryAmountInput (id=cash_Input) at CollateralManualDrawdownAmountScreen.pcf: line 35, column 71
    function value_6 () : gw.pl.currency.MonetaryAmount {
      return collateralDrawdown.getAmountAvailableForDrawdown()
    }
    
    property get collateralDrawdown () : gw.api.web.collateral.CollateralDrawdownUtil {
      return getRequireValue("collateralDrawdown", 0) as gw.api.web.collateral.CollateralDrawdownUtil
    }
    
    property set collateralDrawdown ($arg :  gw.api.web.collateral.CollateralDrawdownUtil) {
      setRequireValue("collateralDrawdown", 0, $arg)
    }
    
    
  }
  
  
}