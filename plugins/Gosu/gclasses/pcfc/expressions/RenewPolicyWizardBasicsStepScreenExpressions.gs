package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/RenewPolicyWizardBasicsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class RenewPolicyWizardBasicsStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/RenewPolicyWizardBasicsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends RenewPolicyWizardBasicsStepScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at RenewPolicyWizardBasicsStepScreen.pcf: line 249, column 47
    function valueRoot_139 () : java.lang.Object {
      return policyContact.Contact
    }
    
    // 'value' attribute on TextCell (id=ContactName_Cell) at RenewPolicyWizardBasicsStepScreen.pcf: line 244, column 59
    function value_136 () : entity.PolicyPeriodContact {
      return policyContact
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at RenewPolicyWizardBasicsStepScreen.pcf: line 249, column 47
    function value_138 () : entity.Address {
      return policyContact.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=ContactRoles_Cell) at RenewPolicyWizardBasicsStepScreen.pcf: line 253, column 97
    function value_141 () : java.lang.String {
      return gw.api.web.policy.PolicyPeriodUtil.getRolesForDisplay(policyContact)
    }
    
    property get policyContact () : entity.PolicyPeriodContact {
      return getIteratedValue(1) as entity.PolicyPeriodContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/RenewPolicyWizardBasicsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends RenewPolicyWizardBasicsStepScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=ProducerPicker) at RenewPolicyWizardBasicsStepScreen.pcf: line 185, column 41
    function action_108 () : void {
      ProducerSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=ProducerPicker) at RenewPolicyWizardBasicsStepScreen.pcf: line 185, column 41
    function action_dest_109 () : pcf.api.Destination {
      return pcf.ProducerSearchPopup.createDestination()
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 185, column 41
    function defaultSetter_115 (__VALUE_TO_SET :  java.lang.Object) : void {
      producerCodeRoleEntry.Producer = (__VALUE_TO_SET as entity.Producer)
    }
    
    // 'value' attribute on RangeInput (id=ProducerCode_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 202, column 46
    function defaultSetter_121 (__VALUE_TO_SET :  java.lang.Object) : void {
      producerCodeRoleEntry.ProducerCode = (__VALUE_TO_SET as entity.ProducerCode)
    }
    
    // 'inputConversion' attribute on TextInput (id=Producer_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 185, column 41
    function inputConversion_112 (VALUE :  java.lang.String) : java.lang.Object {
      return producerSearchConverter.getProducer(VALUE)
    }
    
    // 'label' attribute on TextInput (id=Producer_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 185, column 41
    function label_110 () : java.lang.Object {
      return producerCodeRoleEntry.Role
    }
    
    // 'onChange' attribute on PostOnChange at RenewPolicyWizardBasicsStepScreen.pcf: line 192, column 77
    function onChange_107 () : void {
      autoSelectSoleProducerCode(producerCodeRoleEntry)
    }
    
    // 'onPick' attribute on TextInput (id=Producer_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 185, column 41
    function onPick_111 (PickedValue :  Producer) : void {
      autoSelectSoleProducerCode(producerCodeRoleEntry)
    }
    
    // 'optionLabel' attribute on RangeInput (id=ProducerCode_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 202, column 46
    function optionLabel_123 (VALUE :  entity.ProducerCode) : java.lang.String {
      return VALUE.Code
    }
    
    // 'required' attribute on TextInput (id=Producer_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 185, column 41
    function required_113 () : java.lang.Boolean {
      return newPolicyPeriod.AgencyBill and producerCodeRoleEntry.Role == PolicyRole.TC_PRIMARY
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 202, column 46
    function valueRange_124 () : java.lang.Object {
      return producerCodeRoleEntry.Producer.ActiveProducerCodes
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 185, column 41
    function valueRoot_116 () : java.lang.Object {
      return producerCodeRoleEntry
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 185, column 41
    function value_114 () : entity.Producer {
      return producerCodeRoleEntry.Producer
    }
    
    // 'value' attribute on RangeInput (id=ProducerCode_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 202, column 46
    function value_120 () : entity.ProducerCode {
      return producerCodeRoleEntry.ProducerCode
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 202, column 46
    function verifyValueRangeIsAllowedType_125 ($$arg :  entity.ProducerCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 202, column 46
    function verifyValueRangeIsAllowedType_125 ($$arg :  gw.api.database.IQueryBeanResult<entity.ProducerCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 202, column 46
    function verifyValueRangeIsAllowedType_125 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 202, column 46
    function verifyValueRange_126 () : void {
      var __valueRangeArg = producerCodeRoleEntry.Producer.ActiveProducerCodes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_125(__valueRangeArg)
    }
    
    property get producerCodeRoleEntry () : entity.ProducerCodeRoleEntry {
      return getIteratedValue(1) as entity.ProducerCodeRoleEntry
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/RenewPolicyWizardBasicsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RenewPolicyWizardBasicsStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=addNewCompany) at RenewPolicyWizardBasicsStepScreen.pcf: line 218, column 93
    function action_129 () : void {
      NewPolicyContactPopup.push(newPolicyPeriod, Company)
    }
    
    // 'action' attribute on MenuItem (id=addNewPerson) at RenewPolicyWizardBasicsStepScreen.pcf: line 223, column 92
    function action_131 () : void {
      NewPolicyContactPopup.push(newPolicyPeriod, Person)
    }
    
    // 'pickLocation' attribute on PickerInput (id=OverridingPayerAccount_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 137, column 49
    function action_70 () : void {
      AccountSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=addNewCompany) at RenewPolicyWizardBasicsStepScreen.pcf: line 218, column 93
    function action_dest_130 () : pcf.api.Destination {
      return pcf.NewPolicyContactPopup.createDestination(newPolicyPeriod, Company)
    }
    
    // 'action' attribute on MenuItem (id=addNewPerson) at RenewPolicyWizardBasicsStepScreen.pcf: line 223, column 92
    function action_dest_132 () : pcf.api.Destination {
      return pcf.NewPolicyContactPopup.createDestination(newPolicyPeriod, Person)
    }
    
    // 'pickLocation' attribute on PickerInput (id=OverridingPayerAccount_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 137, column 49
    function action_dest_71 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'available' attribute on BooleanRadioInput (id=EligibleForFullPayDiscount_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 107, column 50
    function available_49 () : java.lang.Boolean {
      return !newPolicyPeriod.AgencyBill
    }
    
    // 'value' attribute on RangeInput (id=DelinquencyPlan_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 170, column 47
    function defaultSetter_101 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPolicyPeriod.PolicyPeriodDelinquencyPlan = (__VALUE_TO_SET as entity.DelinquencyPlan)
    }
    
    // 'value' attribute on DateInput (id=PolicyPerEffDate_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 58, column 53
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPolicyPeriod.PolicyPerEffDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=PolicyPerExpirDate_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 65, column 55
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPolicyPeriod.PolicyPerExpirDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=PolicyLOB_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 71, column 40
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPolicyPeriod.Policy.LOBCode = (__VALUE_TO_SET as typekey.LOBCode)
    }
    
    // 'value' attribute on BooleanRadioInput (id=AssignedRisk_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 76, column 49
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPolicyPeriod.AssignedRisk = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=RiskJurisdiction_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 82, column 45
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPolicyPeriod.RiskJurisdiction = (__VALUE_TO_SET as typekey.Jurisdiction)
    }
    
    // 'value' attribute on TypeKeyInput (id=UWCompany_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 88, column 42
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPolicyPeriod.UWCompany = (__VALUE_TO_SET as typekey.UWCompany)
    }
    
    // 'value' attribute on TextInput (id=Underwriter_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 93, column 48
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPolicyPeriod.Underwriter = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=SecurityZone_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 100, column 44
    function defaultSetter_43 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPolicyPeriod.SecurityZone = (__VALUE_TO_SET as entity.SecurityZone)
    }
    
    // 'value' attribute on BooleanRadioInput (id=EligibleForFullPayDiscount_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 107, column 50
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPolicyPeriod.EligibleForFullPayDiscount = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=RequireFinalAudit_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 113, column 54
    function defaultSetter_58 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPolicyPeriod.RequireFinalAudit = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=BillingMethod_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 123, column 57
    function defaultSetter_63 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPolicyPeriod.BillingMethod = (__VALUE_TO_SET as typekey.PolicyPeriodBillingMethod)
    }
    
    // 'value' attribute on TextInput (id=DBA_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 51, column 40
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPolicyPeriod.DBA = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PickerInput (id=OverridingPayerAccount_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 137, column 49
    function defaultSetter_77 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoicingOverridesView.OverridingPayerAccount = (__VALUE_TO_SET as entity.Account)
    }
    
    // 'value' attribute on RangeInput (id=PaymentPlan_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 150, column 42
    function defaultSetter_84 (__VALUE_TO_SET :  java.lang.Object) : void {
      renewal.PolicyPaymentPlan = (__VALUE_TO_SET as entity.PaymentPlan)
    }
    
    // 'value' attribute on RangeInput (id=OverridingInvoiceStream_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 162, column 50
    function defaultSetter_93 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoicingOverridesView.OverridingInvoiceStream = (__VALUE_TO_SET as entity.InvoiceStream)
    }
    
    // 'initialValue' attribute on Variable at RenewPolicyWizardBasicsStepScreen.pcf: line 25, column 59
    function initialValue_0 () : gw.api.web.producer.ProducerSearchConverter {
      return new gw.api.web.producer.ProducerSearchConverter()
    }
    
    // 'initialValue' attribute on Variable at RenewPolicyWizardBasicsStepScreen.pcf: line 29, column 68
    function initialValue_1 () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return gw.api.database.Query.make(SecurityZone).select()
    }
    
    // 'initialValue' attribute on Variable at RenewPolicyWizardBasicsStepScreen.pcf: line 33, column 57
    function initialValue_2 () : gw.api.web.account.AccountSearchConverter {
      return new gw.api.web.account.AccountSearchConverter()
    }
    
    // 'inputConversion' attribute on PickerInput (id=OverridingPayerAccount_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 137, column 49
    function inputConversion_74 (VALUE :  java.lang.String) : java.lang.Object {
      return accountSearchConverter.getAccount(VALUE)
    }
    
    // 'onChange' attribute on PostOnChange at RenewPolicyWizardBasicsStepScreen.pcf: line 125, column 216
    function onChange_61 () : void {
      if(newPolicyPeriod.AgencyBill){newPolicyPeriod.EligibleForFullPayDiscount = false; invoicingOverridesView.OverridingInvoiceStream = null; invoicingOverridesView.OverridingPayerAccount =null}
    }
    
    // 'onChange' attribute on PostOnChange at RenewPolicyWizardBasicsStepScreen.pcf: line 139, column 49
    function onChange_69 () : void {
      maybeResetPaymentPlan()
    }
    
    // 'onChange' attribute on PostOnChange at RenewPolicyWizardBasicsStepScreen.pcf: line 152, column 133
    function onChange_82 () : void {
      invoicingOverridesView.clearOverridingInvoiceStreamIfIncompatibleWithPaymentPlan(renewal.PolicyPaymentPlan)
    }
    
    // 'required' attribute on PickerInput (id=OverridingPayerAccount_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 137, column 49
    function required_75 () : java.lang.Boolean {
      return newPolicyPeriod.isListBill()
    }
    
    // 'required' attribute on RangeInput (id=OverridingInvoiceStream_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 162, column 50
    function required_91 () : java.lang.Boolean {
      return newPolicyPeriod.ListBill
    }
    
    // 'value' attribute on TextCell (id=ContactName_Cell) at RenewPolicyWizardBasicsStepScreen.pcf: line 244, column 59
    function sortValue_133 (policyContact :  entity.PolicyPeriodContact) : java.lang.Object {
      return policyContact
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at RenewPolicyWizardBasicsStepScreen.pcf: line 249, column 47
    function sortValue_134 (policyContact :  entity.PolicyPeriodContact) : java.lang.Object {
      return policyContact.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=ContactRoles_Cell) at RenewPolicyWizardBasicsStepScreen.pcf: line 253, column 97
    function sortValue_135 (policyContact :  entity.PolicyPeriodContact) : java.lang.Object {
      return gw.api.web.policy.PolicyPeriodUtil.getRolesForDisplay(policyContact)
    }
    
    // 'toAdd' attribute on RowIterator (id=policyContactIterator) at RenewPolicyWizardBasicsStepScreen.pcf: line 238, column 56
    function toAdd_143 (policyContact :  entity.PolicyPeriodContact) : void {
      newPolicyPeriod.addToContacts(policyContact)
    }
    
    // 'toRemove' attribute on RowIterator (id=policyContactIterator) at RenewPolicyWizardBasicsStepScreen.pcf: line 238, column 56
    function toRemove_144 (policyContact :  entity.PolicyPeriodContact) : void {
      newPolicyPeriod.removeFromContacts(policyContact)
    }
    
    // 'validationExpression' attribute on DateInput (id=PolicyPerEffDate_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 58, column 53
    function validationExpression_10 () : java.lang.Object {
      return gw.api.web.policy.RenewPolicyUtil.verifyDateOnOrAfterPrevExpirDate(newPolicyPeriod, prevPolicyPeriod)
    }
    
    // 'validationExpression' attribute on DateInput (id=PolicyPerExpirDate_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 65, column 55
    function validationExpression_16 () : java.lang.Object {
      return newPolicyPeriod.PolicyPerEffDate >= newPolicyPeriod.PolicyPerExpirDate ? DisplayKey.get("Web.Error.EffDateBeforeExpirDate") : null
    }
    
    // 'validationExpression' attribute on PickerInput (id=OverridingPayerAccount_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 137, column 49
    function validationExpression_72 () : java.lang.Object {
      return newPolicyPeriod.isListBill() and invoicingOverridesView.OverridingPayerAccount.isListBill() and newPolicyPeriod.PaymentPlan == null ? null : gw.api.web.account.PolicyPeriods.checkForOverridingPayerAccountError(newPolicyPeriod, invoicingOverridesView.OverridingPayerAccount)
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 170, column 47
    function valueRange_103 () : java.lang.Object {
      return newPolicyPeriod.getApplicableDelinquencyPlans()
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 100, column 44
    function valueRange_45 () : java.lang.Object {
      return allSecurityZones
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingMethod_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 123, column 57
    function valueRange_65 () : java.lang.Object {
      return PolicyPeriodBillingMethod.getTypeKeys(false)
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 150, column 42
    function valueRange_86 () : java.lang.Object {
      return invoicingOverridesView.RelatedPaymentPlans
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingInvoiceStream_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 162, column 50
    function valueRange_95 () : java.lang.Object {
      return newPolicyPeriod.AgencyBill ? {} : gw.api.domain.invoice.InvoiceStreams.getCompatibleInvoiceStreams(invoicingOverridesView.DefaultPayer, renewal.PolicyPaymentPlan)
    }
    
    // 'value' attribute on TypeKeyInput (id=PolicyLOB_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 71, column 40
    function valueRoot_24 () : java.lang.Object {
      return newPolicyPeriod.Policy
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 46, column 49
    function valueRoot_4 () : java.lang.Object {
      return newPolicyPeriod
    }
    
    // 'value' attribute on PickerInput (id=OverridingPayerAccount_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 137, column 49
    function valueRoot_78 () : java.lang.Object {
      return invoicingOverridesView
    }
    
    // 'value' attribute on RangeInput (id=PaymentPlan_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 150, column 42
    function valueRoot_85 () : java.lang.Object {
      return renewal
    }
    
    // 'value' attribute on RangeInput (id=DelinquencyPlan_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 170, column 47
    function value_100 () : entity.DelinquencyPlan {
      return newPolicyPeriod.PolicyPeriodDelinquencyPlan
    }
    
    // 'value' attribute on DateInput (id=PolicyPerEffDate_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 58, column 53
    function value_11 () : java.util.Date {
      return newPolicyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on InputIterator at RenewPolicyWizardBasicsStepScreen.pcf: line 176, column 54
    function value_128 () : entity.ProducerCodeRoleEntry[] {
      return producerCodeRoleEntries
    }
    
    // 'value' attribute on RowIterator (id=policyContactIterator) at RenewPolicyWizardBasicsStepScreen.pcf: line 238, column 56
    function value_145 () : entity.PolicyPeriodContact[] {
      return newPolicyPeriod.Contacts
    }
    
    // 'value' attribute on DateInput (id=PolicyPerExpirDate_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 65, column 55
    function value_17 () : java.util.Date {
      return newPolicyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on TypeKeyInput (id=PolicyLOB_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 71, column 40
    function value_22 () : typekey.LOBCode {
      return newPolicyPeriod.Policy.LOBCode
    }
    
    // 'value' attribute on BooleanRadioInput (id=AssignedRisk_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 76, column 49
    function value_26 () : java.lang.Boolean {
      return newPolicyPeriod.AssignedRisk
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 46, column 49
    function value_3 () : java.lang.String {
      return newPolicyPeriod.PolicyNumber
    }
    
    // 'value' attribute on TypeKeyInput (id=RiskJurisdiction_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 82, column 45
    function value_30 () : typekey.Jurisdiction {
      return newPolicyPeriod.RiskJurisdiction
    }
    
    // 'value' attribute on TypeKeyInput (id=UWCompany_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 88, column 42
    function value_34 () : typekey.UWCompany {
      return newPolicyPeriod.UWCompany
    }
    
    // 'value' attribute on TextInput (id=Underwriter_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 93, column 48
    function value_38 () : java.lang.String {
      return newPolicyPeriod.Underwriter
    }
    
    // 'value' attribute on RangeInput (id=SecurityZone_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 100, column 44
    function value_42 () : entity.SecurityZone {
      return newPolicyPeriod.SecurityZone
    }
    
    // 'value' attribute on BooleanRadioInput (id=EligibleForFullPayDiscount_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 107, column 50
    function value_51 () : java.lang.Boolean {
      return newPolicyPeriod.EligibleForFullPayDiscount
    }
    
    // 'value' attribute on BooleanRadioInput (id=RequireFinalAudit_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 113, column 54
    function value_57 () : java.lang.Boolean {
      return newPolicyPeriod.RequireFinalAudit
    }
    
    // 'value' attribute on TextInput (id=DBA_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 51, column 40
    function value_6 () : java.lang.String {
      return newPolicyPeriod.DBA
    }
    
    // 'value' attribute on RangeInput (id=BillingMethod_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 123, column 57
    function value_62 () : typekey.PolicyPeriodBillingMethod {
      return newPolicyPeriod.BillingMethod
    }
    
    // 'value' attribute on PickerInput (id=OverridingPayerAccount_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 137, column 49
    function value_76 () : entity.Account {
      return invoicingOverridesView.OverridingPayerAccount
    }
    
    // 'value' attribute on RangeInput (id=PaymentPlan_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 150, column 42
    function value_83 () : entity.PaymentPlan {
      return renewal.PolicyPaymentPlan
    }
    
    // 'value' attribute on RangeInput (id=OverridingInvoiceStream_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 162, column 50
    function value_92 () : entity.InvoiceStream {
      return invoicingOverridesView.OverridingInvoiceStream
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 170, column 47
    function verifyValueRangeIsAllowedType_104 ($$arg :  entity.DelinquencyPlan[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 170, column 47
    function verifyValueRangeIsAllowedType_104 ($$arg :  gw.api.database.IQueryBeanResult<entity.DelinquencyPlan>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 170, column 47
    function verifyValueRangeIsAllowedType_104 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 100, column 44
    function verifyValueRangeIsAllowedType_46 ($$arg :  entity.SecurityZone[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 100, column 44
    function verifyValueRangeIsAllowedType_46 ($$arg :  gw.api.database.IQueryBeanResult<entity.SecurityZone>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 100, column 44
    function verifyValueRangeIsAllowedType_46 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingMethod_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 123, column 57
    function verifyValueRangeIsAllowedType_66 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingMethod_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 123, column 57
    function verifyValueRangeIsAllowedType_66 ($$arg :  typekey.PolicyPeriodBillingMethod[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 150, column 42
    function verifyValueRangeIsAllowedType_87 ($$arg :  entity.PaymentPlan[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 150, column 42
    function verifyValueRangeIsAllowedType_87 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentPlan>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 150, column 42
    function verifyValueRangeIsAllowedType_87 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingInvoiceStream_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 162, column 50
    function verifyValueRangeIsAllowedType_96 ($$arg :  entity.InvoiceStream[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingInvoiceStream_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 162, column 50
    function verifyValueRangeIsAllowedType_96 ($$arg :  gw.api.database.IQueryBeanResult<entity.InvoiceStream>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingInvoiceStream_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 162, column 50
    function verifyValueRangeIsAllowedType_96 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 170, column 47
    function verifyValueRange_105 () : void {
      var __valueRangeArg = newPolicyPeriod.getApplicableDelinquencyPlans()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_104(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 100, column 44
    function verifyValueRange_47 () : void {
      var __valueRangeArg = allSecurityZones
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_46(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingMethod_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 123, column 57
    function verifyValueRange_67 () : void {
      var __valueRangeArg = PolicyPeriodBillingMethod.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_66(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 150, column 42
    function verifyValueRange_88 () : void {
      var __valueRangeArg = invoicingOverridesView.RelatedPaymentPlans
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_87(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingInvoiceStream_Input) at RenewPolicyWizardBasicsStepScreen.pcf: line 162, column 50
    function verifyValueRange_97 () : void {
      var __valueRangeArg = newPolicyPeriod.AgencyBill ? {} : gw.api.domain.invoice.InvoiceStreams.getCompatibleInvoiceStreams(invoicingOverridesView.DefaultPayer, renewal.PolicyPaymentPlan)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_96(__valueRangeArg)
    }
    
    property get accountSearchConverter () : gw.api.web.account.AccountSearchConverter {
      return getVariableValue("accountSearchConverter", 0) as gw.api.web.account.AccountSearchConverter
    }
    
    property set accountSearchConverter ($arg :  gw.api.web.account.AccountSearchConverter) {
      setVariableValue("accountSearchConverter", 0, $arg)
    }
    
    property get allSecurityZones () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return getVariableValue("allSecurityZones", 0) as gw.api.database.IQueryBeanResult<SecurityZone>
    }
    
    property set allSecurityZones ($arg :  gw.api.database.IQueryBeanResult<SecurityZone>) {
      setVariableValue("allSecurityZones", 0, $arg)
    }
    
    property get invoicingOverridesView () : gw.invoice.InvoicingOverridesView {
      return getRequireValue("invoicingOverridesView", 0) as gw.invoice.InvoicingOverridesView
    }
    
    property set invoicingOverridesView ($arg :  gw.invoice.InvoicingOverridesView) {
      setRequireValue("invoicingOverridesView", 0, $arg)
    }
    
    property get newPolicyPeriod () : PolicyPeriod {
      return getRequireValue("newPolicyPeriod", 0) as PolicyPeriod
    }
    
    property set newPolicyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("newPolicyPeriod", 0, $arg)
    }
    
    property get prevPolicyPeriod () : PolicyPeriod {
      return getRequireValue("prevPolicyPeriod", 0) as PolicyPeriod
    }
    
    property set prevPolicyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("prevPolicyPeriod", 0, $arg)
    }
    
    property get producerCodeRoleEntries () : ProducerCodeRoleEntry[] {
      return getRequireValue("producerCodeRoleEntries", 0) as ProducerCodeRoleEntry[]
    }
    
    property set producerCodeRoleEntries ($arg :  ProducerCodeRoleEntry[]) {
      setRequireValue("producerCodeRoleEntries", 0, $arg)
    }
    
    property get producerSearchConverter () : gw.api.web.producer.ProducerSearchConverter {
      return getVariableValue("producerSearchConverter", 0) as gw.api.web.producer.ProducerSearchConverter
    }
    
    property set producerSearchConverter ($arg :  gw.api.web.producer.ProducerSearchConverter) {
      setVariableValue("producerSearchConverter", 0, $arg)
    }
    
    property get renewal () : Renewal {
      return getRequireValue("renewal", 0) as Renewal
    }
    
    property set renewal ($arg :  Renewal) {
      setRequireValue("renewal", 0, $arg)
    }
    
    function autoSelectSoleProducerCode(producerCodeRoleEntry : ProducerCodeRoleEntry) {
            var activeProducerCodes = producerCodeRoleEntry.Producer.ActiveProducerCodes;
            if (activeProducerCodes.length == 1) {
              producerCodeRoleEntry.ProducerCode = activeProducerCodes[0];
            }
          }
    
    function maybeResetPaymentPlan() {
      var overridingPayerAccount = invoicingOverridesView.OverridingPayerAccount
      if (newPolicyPeriod.ListBill && 
          overridingPayerAccount != null &&
          !overridingPayerAccount.PaymentPlans.contains(renewal.PolicyPaymentPlan)) {
        renewal.PolicyPaymentPlan = null
      }
    }
    
    
  }
  
  
}