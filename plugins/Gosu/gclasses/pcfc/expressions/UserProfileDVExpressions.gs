package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/users/UserProfileDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class UserProfileDVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/users/UserProfileDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class UserProfileDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at UserProfileDV.pcf: line 26, column 43
    function def_onEnter_11 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=HomePhone) at UserProfileDV.pcf: line 41, column 25
    function def_onEnter_22 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(User.Contact, Contact#HomePhone), DisplayKey.get("Web.Admin.UserProfileDV.HomePhone"), User.Contact.PrimaryPhone == PrimaryPhoneType.TC_HOME))
    }
    
    // 'def' attribute on InputSetRef (id=WorkPhone) at UserProfileDV.pcf: line 45, column 25
    function def_onEnter_25 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.BusinessPhoneOwner(new gw.api.phone.ContactPhoneDelegate(User.Contact,  Contact#WorkPhone), DisplayKey.get("Web.Admin.UserProfileDV.WorkPhone"), User.Contact.PrimaryPhone == PrimaryPhoneType.TC_WORK))
    }
    
    // 'def' attribute on InputSetRef (id=CellPhone) at UserProfileDV.pcf: line 49, column 25
    function def_onEnter_28 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(User.Contact, Person#CellPhone), DisplayKey.get("Web.Admin.UserProfileDV.CellPhone"), User.Contact.PrimaryPhone == PrimaryPhoneType.TC_MOBILE))
    }
    
    // 'def' attribute on InputSetRef (id=FaxPhone) at UserProfileDV.pcf: line 53, column 24
    function def_onEnter_31 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(User.Contact, Contact#FaxPhone), false))
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at UserProfileDV.pcf: line 26, column 43
    function def_onEnter_7 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at UserProfileDV.pcf: line 26, column 43
    function def_onEnter_9 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at UserProfileDV.pcf: line 26, column 43
    function def_refreshVariables_10 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at UserProfileDV.pcf: line 26, column 43
    function def_refreshVariables_12 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=HomePhone) at UserProfileDV.pcf: line 41, column 25
    function def_refreshVariables_23 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(User.Contact, Contact#HomePhone), DisplayKey.get("Web.Admin.UserProfileDV.HomePhone"), User.Contact.PrimaryPhone == PrimaryPhoneType.TC_HOME))
    }
    
    // 'def' attribute on InputSetRef (id=WorkPhone) at UserProfileDV.pcf: line 45, column 25
    function def_refreshVariables_26 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.BusinessPhoneOwner(new gw.api.phone.ContactPhoneDelegate(User.Contact,  Contact#WorkPhone), DisplayKey.get("Web.Admin.UserProfileDV.WorkPhone"), User.Contact.PrimaryPhone == PrimaryPhoneType.TC_WORK))
    }
    
    // 'def' attribute on InputSetRef (id=CellPhone) at UserProfileDV.pcf: line 49, column 25
    function def_refreshVariables_29 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(User.Contact, Person#CellPhone), DisplayKey.get("Web.Admin.UserProfileDV.CellPhone"), User.Contact.PrimaryPhone == PrimaryPhoneType.TC_MOBILE))
    }
    
    // 'def' attribute on InputSetRef (id=FaxPhone) at UserProfileDV.pcf: line 53, column 24
    function def_refreshVariables_32 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.StandardPhoneOwner(new gw.api.phone.ContactPhoneDelegate(User.Contact, Contact#FaxPhone), false))
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at UserProfileDV.pcf: line 26, column 43
    function def_refreshVariables_8 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'value' attribute on TypeKeyInput (id=PrimaryPhone_Input) at UserProfileDV.pcf: line 35, column 46
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      User.Contact.PrimaryPhone = (__VALUE_TO_SET as typekey.PrimaryPhoneType)
    }
    
    // 'value' attribute on TextInput (id=EmployeeNumber_Input) at UserProfileDV.pcf: line 21, column 46
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      User.Contact.EmployeeNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=EmailAddress1_Input) at UserProfileDV.pcf: line 58, column 45
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      User.Contact.EmailAddress1 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=EmailAddress2_Input) at UserProfileDV.pcf: line 63, column 45
    function defaultSetter_41 (__VALUE_TO_SET :  java.lang.Object) : void {
      User.Contact.EmailAddress2 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextInput (id=EmployeeNumber_Input) at UserProfileDV.pcf: line 21, column 46
    function editable_1 () : java.lang.Boolean {
      return perm.User.edit
    }
    
    // 'initialValue' attribute on Variable at UserProfileDV.pcf: line 13, column 43
    function initialValue_0 () : gw.api.address.AddressOwner {
      return new gw.api.address.ContactAddressOwner(User.Contact) { :Editable = true }
    }
    
    // 'mode' attribute on InputSetRef (id=globalAddressContainer) at UserProfileDV.pcf: line 26, column 43
    function mode_13 () : java.lang.Object {
      return gw.api.address.AddressCountrySettings.getSettings(addressOwner.SelectedCountry).PCFMode
    }
    
    // 'value' attribute on TextInput (id=EmployeeNumber_Input) at UserProfileDV.pcf: line 21, column 46
    function valueRoot_4 () : java.lang.Object {
      return User.Contact
    }
    
    // 'value' attribute on TypeKeyInput (id=PrimaryPhone_Input) at UserProfileDV.pcf: line 35, column 46
    function value_16 () : typekey.PrimaryPhoneType {
      return User.Contact.PrimaryPhone
    }
    
    // 'value' attribute on TextInput (id=EmployeeNumber_Input) at UserProfileDV.pcf: line 21, column 46
    function value_2 () : java.lang.String {
      return User.Contact.EmployeeNumber
    }
    
    // 'value' attribute on TextInput (id=EmailAddress1_Input) at UserProfileDV.pcf: line 58, column 45
    function value_34 () : java.lang.String {
      return User.Contact.EmailAddress1
    }
    
    // 'value' attribute on TextInput (id=EmailAddress2_Input) at UserProfileDV.pcf: line 63, column 45
    function value_40 () : java.lang.String {
      return User.Contact.EmailAddress2
    }
    
    // 'visible' attribute on InputDivider at UserProfileDV.pcf: line 28, column 47
    function visible_14 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    property get User () : User {
      return getRequireValue("User", 0) as User
    }
    
    property set User ($arg :  User) {
      setRequireValue("User", 0, $arg)
    }
    
    property get addressOwner () : gw.api.address.AddressOwner {
      return getVariableValue("addressOwner", 0) as gw.api.address.AddressOwner
    }
    
    property set addressOwner ($arg :  gw.api.address.AddressOwner) {
      setVariableValue("addressOwner", 0, $arg)
    }
    
    
  }
  
  
}