package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.database.Query
@javax.annotation.Generated("config/web/pcf/account/AccountDetailLedger.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailLedgerExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailLedger.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailLedgerExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=AccountDetailLedger) at AccountDetailLedger.pcf: line 9, column 71
    static function canVisit_3 (account :  Account) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctledgerview
    }
    
    // 'def' attribute on ScreenRef at AccountDetailLedger.pcf: line 20, column 58
    function def_onEnter_1 (def :  pcf.LedgerScreen) : void {
      def.onEnter(account, relatedTAccountOwner)
    }
    
    // 'def' attribute on ScreenRef at AccountDetailLedger.pcf: line 20, column 58
    function def_refreshVariables_2 (def :  pcf.LedgerScreen) : void {
      def.refreshVariables(account, relatedTAccountOwner)
    }
    
    // 'initialValue' attribute on Variable at AccountDetailLedger.pcf: line 18, column 31
    function initialValue_0 () : TAccountOwner[] {
      return AllRelatedTAccountOwners
    }
    
    // Page (id=AccountDetailLedger) at AccountDetailLedger.pcf: line 9, column 71
    static function parent_4 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.AccountDetailLedger {
      return super.CurrentLocation as pcf.AccountDetailLedger
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get relatedTAccountOwner () : TAccountOwner[] {
      return getVariableValue("relatedTAccountOwner", 0) as TAccountOwner[]
    }
    
    property set relatedTAccountOwner ($arg :  TAccountOwner[]) {
      setVariableValue("relatedTAccountOwner", 0, $arg)
    }
    
    
    property get AllRelatedTAccountOwners() : TAccountOwner[]{
      var result = new java.util.ArrayList()
      result.add(account.Collateral)
      var q = Query.make(CollateralRequirement)
      q.compare("Collateral", Equals, account.Collateral)
      q.compare("Segregated", Equals, true)
      var segregatedCashCollatealRequirements = q.select()
      for (requirement in segregatedCashCollatealRequirements) {
        result.add(requirement)
      }
      return gw.api.upgrade.Coercions.makeArray<entity.TAccountOwner>(result)
    }
    
    
  }
  
  
}