package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ForwardCondition at PolicyForward.pcf: line 18, column 136
    function action_0 () : void {
      gw.api.web.policy.PolicyPeriodUtil.getMostRecentlyViewedPolicyPeriodInfo().goToDestination()
    }
    
    // 'action' attribute on ForwardCondition at PolicyForward.pcf: line 20, column 36
    function action_2 () : void {
      PoliciesGroup.go()
    }
    
    // 'action' attribute on ForwardCondition at PolicyForward.pcf: line 20, column 36
    function action_dest_3 () : pcf.api.Destination {
      return pcf.PoliciesGroup.createDestination()
    }
    
    // 'canVisit' attribute on Forward (id=PolicyForward) at PolicyForward.pcf: line 7, column 24
    static function canVisit_4 () : java.lang.Boolean {
      return perm.System.plcytabview
    }
    
    // 'condition' attribute on ForwardCondition at PolicyForward.pcf: line 18, column 136
    function condition_1 () : java.lang.Boolean {
      return gotoMostRecentPolicyPeriod and (gw.api.web.policy.PolicyPeriodUtil.getMostRecentlyViewedPolicyPeriodInfo() != null)
    }
    
    override property get CurrentLocation () : pcf.PolicyForward {
      return super.CurrentLocation as pcf.PolicyForward
    }
    
    property get gotoMostRecentPolicyPeriod () : Boolean {
      return getVariableValue("gotoMostRecentPolicyPeriod", 0) as Boolean
    }
    
    property set gotoMostRecentPolicyPeriod ($arg :  Boolean) {
      setVariableValue("gotoMostRecentPolicyPeriod", 0, $arg)
    }
    
    
  }
  
  
}