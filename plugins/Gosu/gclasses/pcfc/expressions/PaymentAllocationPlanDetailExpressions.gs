package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/allocationplan/PaymentAllocationPlanDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentAllocationPlanDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/allocationplan/PaymentAllocationPlanDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentAllocationPlanDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (paymentAllocationPlan :  PaymentAllocationPlan) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=PaymentAllocationPlanDetail) at PaymentAllocationPlanDetail.pcf: line 10, column 107
    function canEdit_2 () : java.lang.Boolean {
      return perm.System.payallocplanedit
    }
    
    // 'def' attribute on ScreenRef at PaymentAllocationPlanDetail.pcf: line 17, column 71
    function def_onEnter_0 (def :  pcf.PaymentAllocationPlanDetailScreen) : void {
      def.onEnter(paymentAllocationPlan)
    }
    
    // 'def' attribute on ScreenRef at PaymentAllocationPlanDetail.pcf: line 17, column 71
    function def_refreshVariables_1 (def :  pcf.PaymentAllocationPlanDetailScreen) : void {
      def.refreshVariables(paymentAllocationPlan)
    }
    
    // 'parent' attribute on Page (id=PaymentAllocationPlanDetail) at PaymentAllocationPlanDetail.pcf: line 10, column 107
    static function parent_3 (paymentAllocationPlan :  PaymentAllocationPlan) : pcf.api.Destination {
      return pcf.PaymentAllocationPlans.createDestination()
    }
    
    // 'title' attribute on Page (id=PaymentAllocationPlanDetail) at PaymentAllocationPlanDetail.pcf: line 10, column 107
    static function title_4 (paymentAllocationPlan :  PaymentAllocationPlan) : java.lang.Object {
      return DisplayKey.get("Web.PaymentAllocationPlanDetail.Title", paymentAllocationPlan.Name)
    }
    
    override property get CurrentLocation () : pcf.PaymentAllocationPlanDetail {
      return super.CurrentLocation as pcf.PaymentAllocationPlanDetail
    }
    
    property get paymentAllocationPlan () : PaymentAllocationPlan {
      return getVariableValue("paymentAllocationPlan", 0) as PaymentAllocationPlan
    }
    
    property set paymentAllocationPlan ($arg :  PaymentAllocationPlan) {
      setVariableValue("paymentAllocationPlan", 0, $arg)
    }
    
    
  }
  
  
}