package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyMultiPaymentSplitConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyMultiPaymentSplitConfirmationPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyMultiPaymentSplitConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyMultiPaymentSplitConfirmationPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (originalPaymentEntry :  PaymentEntry) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=AgencyMultiPaymentSplitConfirmationPopup) at AgencyMultiPaymentSplitConfirmationPopup.pcf: line 10, column 87
    function beforeCommit_3 (pickedValue :  java.lang.Object) : void {
      validateSplitPayment();agencySplitPayment.splitMultiPayment()
    }
    
    // 'def' attribute on ScreenRef at AgencyMultiPaymentSplitConfirmationPopup.pcf: line 21, column 101
    function def_onEnter_1 (def :  pcf.AgencyPaymentSplitConfirmationScreen) : void {
      def.onEnter(agencySplitPayment, getTextToDisplayInAlertBar())
    }
    
    // 'def' attribute on ScreenRef at AgencyMultiPaymentSplitConfirmationPopup.pcf: line 21, column 101
    function def_refreshVariables_2 (def :  pcf.AgencyPaymentSplitConfirmationScreen) : void {
      def.refreshVariables(agencySplitPayment, getTextToDisplayInAlertBar())
    }
    
    // 'initialValue' attribute on Variable at AgencyMultiPaymentSplitConfirmationPopup.pcf: line 16, column 65
    function initialValue_0 () : gw.api.web.producer.agencybill.AgencySplitPayment {
      return createAgencyPaymentSplit()
    }
    
    override property get CurrentLocation () : pcf.AgencyMultiPaymentSplitConfirmationPopup {
      return super.CurrentLocation as pcf.AgencyMultiPaymentSplitConfirmationPopup
    }
    
    property get agencySplitPayment () : gw.api.web.producer.agencybill.AgencySplitPayment {
      return getVariableValue("agencySplitPayment", 0) as gw.api.web.producer.agencybill.AgencySplitPayment
    }
    
    property set agencySplitPayment ($arg :  gw.api.web.producer.agencybill.AgencySplitPayment) {
      setVariableValue("agencySplitPayment", 0, $arg)
    }
    
    property get originalPaymentEntry () : PaymentEntry {
      return getVariableValue("originalPaymentEntry", 0) as PaymentEntry
    }
    
    property set originalPaymentEntry ($arg :  PaymentEntry) {
      setVariableValue("originalPaymentEntry", 0, $arg)
    }
    
    /**
     * Create non persistent AgencySplitPayment object used to manage the split of the multipayment entry in the UI
     */
    function createAgencyPaymentSplit() : gw.api.web.producer.agencybill.AgencySplitPayment {
      return new gw.api.web.producer.agencybill.AgencySplitPayment(originalPaymentEntry);
    }
    
    /**
     * If there are not very many empty rows back on the multipayment screen, display an alert bar warning message telling the user that they
     * may want to cancel out of this screen and go back to the multipayment screen and add some more blank rows there.  Reason we need this is
     * that there isn't a way currently in the UI framework to go off to a popup like this split payment popup screen and create multiple NEW
     * rows and then add them back to an LV on a screen that we came from (like the multipayment screen).  So the workaround is to have the
     * user go back to the multipayment screen and add some new blank rows
     */
    function getTextToDisplayInAlertBar() : String {
      var numberOfAvailableEmptyPaymentRows = getNumberOfEmptyPaymentRowsOnMultiPaymentScreen(originalPaymentEntry.PaymentInstrument.PaymentMethod);
      if (numberOfAvailableEmptyPaymentRows < 5) {
        return DisplayKey.get("Web.AgencyMultiPaymentSplitConfirmationPopup.AlertBarMessageMaxNumberOfSplitRows",  numberOfAvailableEmptyPaymentRows );
      }
      else {
        return null;
      }
    }
    
    /**
     * Perform validation on the AgencySplitPayment before we commit.  Throw a DisplayableException if validation fails
     */
    function validateSplitPayment() {
    
      // The number of unused rows that will be filled back on the multipayment screen by doing a split payment on this screen is equal to the number of split rows minus one.
      // That's because we re-use the original row on the multipayment screen (the one being split) by overwriting its information with the information in one of the split rows
      // (so that doesn't use any new rows) and then we fill up empty rows for the rest of the split payment rows.  So if the split payment would end up using more empty rows
      // back on the multipayment screen than are available, we will display an error message (because given the ui framework its impossible for us to create new rows)
      var numberOfSplitRows = agencySplitPayment.getSplit().length
      if (numberOfSplitRows - 1 > getNumberOfEmptyPaymentRowsOnMultiPaymentScreen(originalPaymentEntry.PaymentInstrument.PaymentMethod)) {
        throw new gw.api.util.DisplayableException(DisplayKey.get("Web.AgencyMultiPaymentSplitConfirmationPopup.ErrorMessageTooManySplitRows"))
      }
    
    }
    
    /**
     * @return the number of blank rows available back on the multipayment screen (screen from which user came to this popup)
     */
    private function getNumberOfEmptyPaymentRowsOnMultiPaymentScreen(paymentMethod : PaymentMethod) : int {
      return originalPaymentEntry.NewMultiPayment.Payments.where( \ paymentRow -> paymentMethod == paymentRow.PaymentInstrument.PaymentMethod && paymentRow.isUnusedEntry()).length;
    }
    
    
  }
  
  
}