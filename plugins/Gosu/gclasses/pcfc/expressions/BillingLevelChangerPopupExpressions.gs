package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/BillingLevelChangerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BillingLevelChangerPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/BillingLevelChangerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BillingLevelChangerPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=BillingLevelChangerPopup) at BillingLevelChangerPopup.pcf: line 10, column 88
    function beforeCommit_17 (pickedValue :  java.lang.Object) : void {
      billingLevelChangeHelper.updateBillingLevel()
    }
    
    // 'value' attribute on BooleanRadioInput (id=BillingLevelSeparateIncomingFundsByAccount_Input) at BillingLevelChangerPopup.pcf: line 46, column 46
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingLevelChangeHelper.isSeparateIncomingFunds = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=BillingLevelInvoiceByAccount_Input) at BillingLevelChangerPopup.pcf: line 34, column 45
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingLevelChangeHelper.isInvoiceBy = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'editable' attribute on BooleanRadioInput (id=BillingLevelSeparateIncomingFundsByAccount_Input) at BillingLevelChangerPopup.pcf: line 46, column 46
    function editable_9 () : java.lang.Boolean {
      return !billingLevelChangeHelper.isInvoiceBy
    }
    
    // 'initialValue' attribute on Variable at BillingLevelChangerPopup.pcf: line 19, column 51
    function initialValue_0 () : gw.account.BillingLevelChangeHelper {
      return new gw.account.BillingLevelChangeHelper(account)
    }
    
    // EditButtons at BillingLevelChangerPopup.pcf: line 22, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'onChange' attribute on PostOnChange at BillingLevelChangerPopup.pcf: line 36, column 127
    function onChange_2 () : void {
      if (billingLevelChangeHelper.isInvoiceBy) {billingLevelChangeHelper.isSeparateIncomingFunds = true}
    }
    
    // 'value' attribute on BooleanRadioInput (id=BillingLevelInvoiceByAccount_Input) at BillingLevelChangerPopup.pcf: line 34, column 45
    function valueRoot_6 () : java.lang.Object {
      return billingLevelChangeHelper
    }
    
    // 'value' attribute on BooleanRadioInput (id=BillingLevelSeparateIncomingFundsByAccount_Input) at BillingLevelChangerPopup.pcf: line 46, column 46
    function value_11 () : java.lang.Boolean {
      return billingLevelChangeHelper.isSeparateIncomingFunds
    }
    
    // 'value' attribute on BooleanRadioInput (id=BillingLevelInvoiceByAccount_Input) at BillingLevelChangerPopup.pcf: line 34, column 45
    function value_4 () : java.lang.Boolean {
      return billingLevelChangeHelper.isInvoiceBy
    }
    
    // 'visible' attribute on BooleanRadioInput (id=BillingLevelInvoiceByAccount_Input) at BillingLevelChangerPopup.pcf: line 34, column 45
    function visible_3 () : java.lang.Boolean {
      return !account.isListBill()
    }
    
    override property get CurrentLocation () : pcf.BillingLevelChangerPopup {
      return super.CurrentLocation as pcf.BillingLevelChangerPopup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get billingLevelChangeHelper () : gw.account.BillingLevelChangeHelper {
      return getVariableValue("billingLevelChangeHelper", 0) as gw.account.BillingLevelChangeHelper
    }
    
    property set billingLevelChangeHelper ($arg :  gw.account.BillingLevelChangeHelper) {
      setVariableValue("billingLevelChangeHelper", 0, $arg)
    }
    
    
  }
  
  
}