package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/PaymentReceiptInputSet.check.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentReceiptInputSet_checkExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/PaymentReceiptInputSet.check.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentReceiptInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.check.pcf: line 13, column 38
    function def_onEnter_0 (def :  pcf.PaymentInstrumentInputSet_ach) : void {
      def.onEnter(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.check.pcf: line 13, column 38
    function def_onEnter_2 (def :  pcf.PaymentInstrumentInputSet_creditcard) : void {
      def.onEnter(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.check.pcf: line 13, column 38
    function def_onEnter_4 (def :  pcf.PaymentInstrumentInputSet_default) : void {
      def.onEnter(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.check.pcf: line 13, column 38
    function def_onEnter_6 (def :  pcf.PaymentInstrumentInputSet_misc) : void {
      def.onEnter(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.check.pcf: line 13, column 38
    function def_onEnter_8 (def :  pcf.PaymentInstrumentInputSet_wire) : void {
      def.onEnter(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.check.pcf: line 13, column 38
    function def_refreshVariables_1 (def :  pcf.PaymentInstrumentInputSet_ach) : void {
      def.refreshVariables(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.check.pcf: line 13, column 38
    function def_refreshVariables_3 (def :  pcf.PaymentInstrumentInputSet_creditcard) : void {
      def.refreshVariables(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.check.pcf: line 13, column 38
    function def_refreshVariables_5 (def :  pcf.PaymentInstrumentInputSet_default) : void {
      def.refreshVariables(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.check.pcf: line 13, column 38
    function def_refreshVariables_7 (def :  pcf.PaymentInstrumentInputSet_misc) : void {
      def.refreshVariables(paymentReceipt.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at PaymentReceiptInputSet.check.pcf: line 13, column 38
    function def_refreshVariables_9 (def :  pcf.PaymentInstrumentInputSet_wire) : void {
      def.refreshVariables(paymentReceipt.PaymentInstrument)
    }
    
    // 'value' attribute on TextInput (id=RefNumber_Input) at PaymentReceiptInputSet.check.pcf: line 18, column 41
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentReceipt.RefNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'mode' attribute on InputSetRef at PaymentReceiptInputSet.check.pcf: line 13, column 38
    function mode_10 () : java.lang.Object {
      return PaymentMethod.TC_CHECK
    }
    
    // 'value' attribute on TextInput (id=RefNumber_Input) at PaymentReceiptInputSet.check.pcf: line 18, column 41
    function valueRoot_13 () : java.lang.Object {
      return paymentReceipt
    }
    
    // 'value' attribute on TextInput (id=RefNumber_Input) at PaymentReceiptInputSet.check.pcf: line 18, column 41
    function value_11 () : java.lang.String {
      return paymentReceipt.RefNumber
    }
    
    property get paymentReceipt () : PaymentReceipt {
      return getRequireValue("paymentReceipt", 0) as PaymentReceipt
    }
    
    property set paymentReceipt ($arg :  PaymentReceipt) {
      setRequireValue("paymentReceipt", 0, $arg)
    }
    
    
  }
  
  
}