package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/CommissionPayableReductionWizardConfirmationStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionPayableReductionWizardConfirmationStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/CommissionPayableReductionWizardConfirmationStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionPayableReductionWizardConfirmationStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=Cancel) at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 20, column 60
    function action_0 () : void {
      (CurrentLocation as pcf.api.Wizard).cancel()
    }
    
    // 'action' attribute on ToolbarButton (id=Finish) at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 24, column 60
    function action_1 () : void {
      (CurrentLocation as pcf.api.Wizard).finish()
    }
    
    // 'label' attribute on AlertBar (id=alertBarForWriteoffsAwaitingApproval) at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 29, column 47
    function label_3 () : java.lang.Object {
      return DisplayKey.get("Web.CommissionPayableReductionWizardConfirmationStepScreen.ApprovalRequiredForWriteoff", getAuthorityLimit().render())
    }
    
    // 'sortBy' attribute on TextCell (id=ProducerCode_Cell) at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 52, column 80
    function sortValue_4 (chargeCommission :  entity.ChargeCommission) : java.lang.Object {
      return chargeCommission.PolicyCommission.ProducerCode.Code
    }
    
    // 'sortBy' attribute on TextCell (id=PolicyNumber_Cell) at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 58, column 88
    function sortValue_5 (chargeCommission :  entity.ChargeCommission) : java.lang.Object {
      return chargeCommission.PolicyCommission.PolicyPeriod.PolicyNumber
    }
    
    // 'sortBy' attribute on TextCell (id=Charge_Cell) at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 65, column 46
    function sortValue_6 (chargeCommission :  entity.ChargeCommission) : java.lang.Object {
      return chargeCommission.Charge
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AmountWrittenOff_Cell) at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 72, column 62
    function sortValue_7 (chargeCommission :  entity.ChargeCommission) : java.lang.Object {
      return chargeCommission.AmountWrittenOff
    }
    
    // 'value' attribute on RowIterator at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 44, column 53
    function value_24 () : entity.ChargeCommission[] {
      return chargeCommissions
    }
    
    // 'visible' attribute on AlertBar (id=alertBarForWriteoffsAwaitingApproval) at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 29, column 47
    function visible_2 () : java.lang.Boolean {
      return showApprovalRequiredAlertBar
    }
    
    property get chargeCommissions () : ChargeCommission[] {
      return getRequireValue("chargeCommissions", 0) as ChargeCommission[]
    }
    
    property set chargeCommissions ($arg :  ChargeCommission[]) {
      setRequireValue("chargeCommissions", 0, $arg)
    }
    
    property get currency () : Currency {
      return getRequireValue("currency", 0) as Currency
    }
    
    property set currency ($arg :  Currency) {
      setRequireValue("currency", 0, $arg)
    }
    
    property get showApprovalRequiredAlertBar () : Boolean {
      return getRequireValue("showApprovalRequiredAlertBar", 0) as Boolean
    }
    
    property set showApprovalRequiredAlertBar ($arg :  Boolean) {
      setRequireValue("showApprovalRequiredAlertBar", 0, $arg)
    }
    
    public function getAuthorityLimit() : gw.pl.currency.MonetaryAmount {
      return (User.util.CurrentUser as User).AuthorityProfileNonNull.getMaximumAuthorizedAmount(
       AuthorityLimitType.TC_WRITEOFFCHARGECOMMISSION, currency )
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/CommissionPayableReductionWizardConfirmationStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CommissionPayableReductionWizardConfirmationStepScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on MonetaryAmountCell (id=AmountWrittenOff_Cell) at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 72, column 62
    function available_17 () : java.lang.Boolean {
      return chargeCommission.SelectedForWriteoff
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=AmountWrittenOff_Cell) at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 72, column 62
    function currency_20 () : typekey.Currency {
      return currency
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 58, column 88
    function valueRoot_12 () : java.lang.Object {
      return chargeCommission.PolicyCommission.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=Charge_Cell) at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 65, column 46
    function valueRoot_15 () : java.lang.Object {
      return chargeCommission
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 52, column 80
    function valueRoot_9 () : java.lang.Object {
      return chargeCommission.PolicyCommission.ProducerCode
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 58, column 88
    function value_11 () : java.lang.String {
      return chargeCommission.PolicyCommission.PolicyPeriod.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=Charge_Cell) at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 65, column 46
    function value_14 () : entity.Charge {
      return chargeCommission.Charge
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AmountWrittenOff_Cell) at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 72, column 62
    function value_18 () : gw.pl.currency.MonetaryAmount {
      return chargeCommission.AmountWrittenOff
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at CommissionPayableReductionWizardConfirmationStepScreen.pcf: line 52, column 80
    function value_8 () : java.lang.String {
      return chargeCommission.PolicyCommission.ProducerCode.Code
    }
    
    property get chargeCommission () : entity.ChargeCommission {
      return getIteratedValue(1) as entity.ChargeCommission
    }
    
    
  }
  
  
}