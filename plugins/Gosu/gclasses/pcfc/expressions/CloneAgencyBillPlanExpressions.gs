package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/agencybill/CloneAgencyBillPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CloneAgencyBillPlanExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/agencybill/CloneAgencyBillPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CloneAgencyBillPlanExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (agencyBillPlan :  AgencyBillPlan) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=CloneAgencyBillPlan) at CloneAgencyBillPlan.pcf: line 13, column 87
    function afterCancel_3 () : void {
      AgencyBillPlanDetail.go(agencyBillPlan)
    }
    
    // 'afterCancel' attribute on Page (id=CloneAgencyBillPlan) at CloneAgencyBillPlan.pcf: line 13, column 87
    function afterCancel_dest_4 () : pcf.api.Destination {
      return pcf.AgencyBillPlanDetail.createDestination(agencyBillPlan)
    }
    
    // 'afterCommit' attribute on Page (id=CloneAgencyBillPlan) at CloneAgencyBillPlan.pcf: line 13, column 87
    function afterCommit_5 (TopLocation :  pcf.api.Location) : void {
      AgencyBillPlanDetail.go(clonedAgencyBillPlan)
    }
    
    // 'def' attribute on ScreenRef at CloneAgencyBillPlan.pcf: line 24, column 63
    function def_onEnter_1 (def :  pcf.AgencyBillPlanDetailScreen) : void {
      def.onEnter(clonedAgencyBillPlan)
    }
    
    // 'def' attribute on ScreenRef at CloneAgencyBillPlan.pcf: line 24, column 63
    function def_refreshVariables_2 (def :  pcf.AgencyBillPlanDetailScreen) : void {
      def.refreshVariables(clonedAgencyBillPlan)
    }
    
    // 'initialValue' attribute on Variable at CloneAgencyBillPlan.pcf: line 22, column 37
    function initialValue_0 () : entity.AgencyBillPlan {
      return agencyBillPlan.makeClone() as AgencyBillPlan
    }
    
    // 'parent' attribute on Page (id=CloneAgencyBillPlan) at CloneAgencyBillPlan.pcf: line 13, column 87
    static function parent_6 (agencyBillPlan :  AgencyBillPlan) : pcf.api.Destination {
      return pcf.AgencyBillPlans.createDestination()
    }
    
    // 'title' attribute on Page (id=CloneAgencyBillPlan) at CloneAgencyBillPlan.pcf: line 13, column 87
    static function title_7 (agencyBillPlan :  AgencyBillPlan) : java.lang.Object {
      return DisplayKey.get("Web.CloneAgencyBillPlan.Title", agencyBillPlan)
    }
    
    override property get CurrentLocation () : pcf.CloneAgencyBillPlan {
      return super.CurrentLocation as pcf.CloneAgencyBillPlan
    }
    
    property get agencyBillPlan () : AgencyBillPlan {
      return getVariableValue("agencyBillPlan", 0) as AgencyBillPlan
    }
    
    property set agencyBillPlan ($arg :  AgencyBillPlan) {
      setVariableValue("agencyBillPlan", 0, $arg)
    }
    
    property get clonedAgencyBillPlan () : entity.AgencyBillPlan {
      return getVariableValue("clonedAgencyBillPlan", 0) as entity.AgencyBillPlan
    }
    
    property set clonedAgencyBillPlan ($arg :  entity.AgencyBillPlan) {
      setVariableValue("clonedAgencyBillPlan", 0, $arg)
    }
    
    
  }
  
  
}