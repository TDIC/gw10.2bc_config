package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillPolicyTxnsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillPolicyTxnsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillPolicyTxnsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillPolicyTxnsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at AgencyBillPolicyTxnsLV.pcf: line 13, column 45
    function initialValue_0 () : libraries.TransactionRelation {
      return libraries.TransactionRelation.TC_POLICY
    }
    
    // 'value' attribute on TextCell (id=TransactionType_Cell) at AgencyBillPolicyTxnsLV.pcf: line 26, column 74
    function sortValue_1 (transactionWrapper :  gw.transaction.TransactionWrapper) : java.lang.Object {
      return transactionWrapper.transactionType
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AgencyBillPolicyTxnsLV.pcf: line 31, column 67
    function sortValue_2 (transactionWrapper :  gw.transaction.TransactionWrapper) : java.lang.Object {
      return transactionWrapper.transaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at AgencyBillPolicyTxnsLV.pcf: line 36, column 69
    function sortValue_3 (transactionWrapper :  gw.transaction.TransactionWrapper) : java.lang.Object {
      return transactionWrapper.transaction.TransactionNumber
    }
    
    // 'value' attribute on RowIterator (id=ChargesLV) at AgencyBillPolicyTxnsLV.pcf: line 20, column 55
    function value_33 () : gw.transaction.TransactionWrapper[] {
      return policyPeriod.getChargeTransactionWrappers()
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get transactionContext () : libraries.TransactionRelation {
      return getVariableValue("transactionContext", 0) as libraries.TransactionRelation
    }
    
    property set transactionContext ($arg :  libraries.TransactionRelation) {
      setVariableValue("transactionContext", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillPolicyTxnsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AgencyBillPolicyTxnsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at AgencyBillPolicyTxnsLV.pcf: line 36, column 69
    function action_10 () : void {
      TransactionDetailPopup.push(transactionWrapper.transaction)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at AgencyBillPolicyTxnsLV.pcf: line 36, column 69
    function action_dest_11 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(transactionWrapper.transaction)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Gross_Cell) at AgencyBillPolicyTxnsLV.pcf: line 54, column 88
    function currency_22 () : typekey.Currency {
      return transactionWrapper.transaction.Currency
    }
    
    // 'outputConversion' attribute on TextCell (id=Rate_Cell) at AgencyBillPolicyTxnsLV.pcf: line 69, column 45
    function outputConversion_27 (VALUE :  java.math.BigDecimal) : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(VALUE, 2)
    }
    
    // 'value' attribute on TextCell (id=TransactionType_Cell) at AgencyBillPolicyTxnsLV.pcf: line 26, column 74
    function valueRoot_5 () : java.lang.Object {
      return transactionWrapper
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AgencyBillPolicyTxnsLV.pcf: line 31, column 67
    function valueRoot_8 () : java.lang.Object {
      return transactionWrapper.transaction
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at AgencyBillPolicyTxnsLV.pcf: line 36, column 69
    function value_12 () : java.lang.String {
      return transactionWrapper.transaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=Item_Cell) at AgencyBillPolicyTxnsLV.pcf: line 41, column 68
    function value_15 () : java.lang.String {
      return transactionWrapper.transaction.ShortDisplayName
    }
    
    // 'value' attribute on TypeKeyCell (id=Event_Cell) at AgencyBillPolicyTxnsLV.pcf: line 47, column 44
    function value_18 () : typekey.Transaction {
      return transactionWrapper.transaction.Subtype
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Gross_Cell) at AgencyBillPolicyTxnsLV.pcf: line 54, column 88
    function value_21 () : gw.pl.currency.MonetaryAmount {
      return transactionWrapper.transaction.getGrossAmount( transactionContext )
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Commission_Cell) at AgencyBillPolicyTxnsLV.pcf: line 61, column 93
    function value_24 () : gw.pl.currency.MonetaryAmount {
      return transactionWrapper.transaction.getCommissionAmount( transactionContext )
    }
    
    // 'value' attribute on TextCell (id=Rate_Cell) at AgencyBillPolicyTxnsLV.pcf: line 69, column 45
    function value_28 () : java.math.BigDecimal {
      return transactionWrapper.transaction.getCommissionRate( transactionContext )
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Net_Cell) at AgencyBillPolicyTxnsLV.pcf: line 76, column 86
    function value_30 () : gw.pl.currency.MonetaryAmount {
      return transactionWrapper.transaction.getNetAmount( transactionContext )
    }
    
    // 'value' attribute on TextCell (id=TransactionType_Cell) at AgencyBillPolicyTxnsLV.pcf: line 26, column 74
    function value_4 () : gw.transaction.TransactionWrapper.TransactionType {
      return transactionWrapper.transactionType
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AgencyBillPolicyTxnsLV.pcf: line 31, column 67
    function value_7 () : java.util.Date {
      return transactionWrapper.transaction.TransactionDate
    }
    
    property get transactionWrapper () : gw.transaction.TransactionWrapper {
      return getIteratedValue(1) as gw.transaction.TransactionWrapper
    }
    
    
  }
  
  
}