package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/commission/CloneCommissionPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CloneCommissionPlanExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/commission/CloneCommissionPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CloneCommissionPlanExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (commissionPlan :  CommissionPlan) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=CloneCommissionPlan) at CloneCommissionPlan.pcf: line 13, column 87
    function afterCancel_3 () : void {
      CommissionPlanDetail.go(commissionPlan)
    }
    
    // 'afterCancel' attribute on Page (id=CloneCommissionPlan) at CloneCommissionPlan.pcf: line 13, column 87
    function afterCancel_dest_4 () : pcf.api.Destination {
      return pcf.CommissionPlanDetail.createDestination(commissionPlan)
    }
    
    // 'afterCommit' attribute on Page (id=CloneCommissionPlan) at CloneCommissionPlan.pcf: line 13, column 87
    function afterCommit_5 (TopLocation :  pcf.api.Location) : void {
      CommissionPlanDetail.go(clonedCommissionPlan)
    }
    
    // 'def' attribute on ScreenRef at CloneCommissionPlan.pcf: line 24, column 63
    function def_onEnter_1 (def :  pcf.CommissionPlanDetailScreen) : void {
      def.onEnter(clonedCommissionPlan)
    }
    
    // 'def' attribute on ScreenRef at CloneCommissionPlan.pcf: line 24, column 63
    function def_refreshVariables_2 (def :  pcf.CommissionPlanDetailScreen) : void {
      def.refreshVariables(clonedCommissionPlan)
    }
    
    // 'initialValue' attribute on Variable at CloneCommissionPlan.pcf: line 22, column 37
    function initialValue_0 () : entity.CommissionPlan {
      return commissionPlan.makeClone() as CommissionPlan
    }
    
    // 'parent' attribute on Page (id=CloneCommissionPlan) at CloneCommissionPlan.pcf: line 13, column 87
    static function parent_6 (commissionPlan :  CommissionPlan) : pcf.api.Destination {
      return pcf.CommissionPlans.createDestination()
    }
    
    // 'title' attribute on Page (id=CloneCommissionPlan) at CloneCommissionPlan.pcf: line 13, column 87
    static function title_7 (commissionPlan :  CommissionPlan) : java.lang.Object {
      return DisplayKey.get("Web.CloneCommissionPlan.Title", commissionPlan)
    }
    
    override property get CurrentLocation () : pcf.CloneCommissionPlan {
      return super.CurrentLocation as pcf.CloneCommissionPlan
    }
    
    property get clonedCommissionPlan () : entity.CommissionPlan {
      return getVariableValue("clonedCommissionPlan", 0) as entity.CommissionPlan
    }
    
    property set clonedCommissionPlan ($arg :  entity.CommissionPlan) {
      setVariableValue("clonedCommissionPlan", 0, $arg)
    }
    
    property get commissionPlan () : CommissionPlan {
      return getVariableValue("commissionPlan", 0) as CommissionPlan
    }
    
    property set commissionPlan ($arg :  CommissionPlan) {
      setVariableValue("commissionPlan", 0, $arg)
    }
    
    
  }
  
  
}