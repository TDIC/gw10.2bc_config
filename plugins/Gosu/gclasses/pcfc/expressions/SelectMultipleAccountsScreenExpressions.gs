package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultipleAccountsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SelectMultipleAccountsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultipleAccountsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends SelectMultipleAccountsScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=CancelButton) at SelectMultipleAccountsScreen.pcf: line 37, column 64
    function action_1 () : void {
      CurrentLocation.cancel()
    }
    
    // 'def' attribute on PanelRef at SelectMultipleAccountsScreen.pcf: line 27, column 47
    function def_onEnter_2 (def :  pcf.AccountSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at SelectMultipleAccountsScreen.pcf: line 41, column 106
    function def_onEnter_4 (def :  pcf.AccountSearchResultsLV) : void {
      def.onEnter(accountSearchViews, null, isWizard, showHyperlinks, showCheckboxes)
    }
    
    // 'def' attribute on PanelRef at SelectMultipleAccountsScreen.pcf: line 27, column 47
    function def_refreshVariables_3 (def :  pcf.AccountSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at SelectMultipleAccountsScreen.pcf: line 41, column 106
    function def_refreshVariables_5 (def :  pcf.AccountSearchResultsLV) : void {
      def.refreshVariables(accountSearchViews, null, isWizard, showHyperlinks, showCheckboxes)
    }
    
    // 'pickValue' attribute on CheckedValuesToolbarButton (id=addbutton) at SelectMultipleAccountsScreen.pcf: line 33, column 91
    function pickValue_0 (CheckedValues :  entity.AccountSearchView[]) : Account[] {
      return gw.api.web.search.SearchPopupUtil.getAccountArray(CheckedValues)
    }
    
    // 'searchCriteria' attribute on SearchPanel at SelectMultipleAccountsScreen.pcf: line 25, column 85
    function searchCriteria_7 () : gw.search.AccountSearchCriteria {
      return new gw.search.AccountSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at SelectMultipleAccountsScreen.pcf: line 25, column 85
    function search_6 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria, StringCriterionMode.TC_STARTSWITH)
    }
    
    property get accountSearchViews () : gw.api.database.IQueryBeanResult<AccountSearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<AccountSearchView>
    }
    
    property get searchCriteria () : gw.search.AccountSearchCriteria {
      return getCriteriaValue(1) as gw.search.AccountSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.AccountSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultipleAccountsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SelectMultipleAccountsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get isWizard () : Boolean {
      return getVariableValue("isWizard", 0) as Boolean
    }
    
    property set isWizard ($arg :  Boolean) {
      setVariableValue("isWizard", 0, $arg)
    }
    
    property get showCheckboxes () : Boolean {
      return getVariableValue("showCheckboxes", 0) as Boolean
    }
    
    property set showCheckboxes ($arg :  Boolean) {
      setVariableValue("showCheckboxes", 0, $arg)
    }
    
    property get showHyperlinks () : Boolean {
      return getVariableValue("showHyperlinks", 0) as Boolean
    }
    
    property set showHyperlinks ($arg :  Boolean) {
      setVariableValue("showHyperlinks", 0, $arg)
    }
    
    
  }
  
  
}