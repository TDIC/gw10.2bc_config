package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/usersandsecurity/SecurityZones.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SecurityZonesExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/usersandsecurity/SecurityZones.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SecurityZonesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=SecurityZones_NewSecurityZoneButton) at SecurityZones.pcf: line 23, column 27
    function action_1 () : void {
      NewSecurityZone.go()
    }
    
    // 'action' attribute on ToolbarButton (id=SecurityZones_NewSecurityZoneButton) at SecurityZones.pcf: line 23, column 27
    function action_dest_2 () : pcf.api.Destination {
      return pcf.NewSecurityZone.createDestination()
    }
    
    // 'canEdit' attribute on Page (id=SecurityZones) at SecurityZones.pcf: line 9, column 65
    function canEdit_5 () : java.lang.Boolean {
      return perm.Group.edit
    }
    
    // 'canVisit' attribute on Page (id=SecurityZones) at SecurityZones.pcf: line 9, column 65
    static function canVisit_6 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.useradmin
    }
    
    // 'def' attribute on PanelRef at SecurityZones.pcf: line 17, column 46
    function def_onEnter_3 (def :  pcf.SecurityZonesLV) : void {
      def.onEnter(SecurityZones)
    }
    
    // 'def' attribute on PanelRef at SecurityZones.pcf: line 17, column 46
    function def_refreshVariables_4 (def :  pcf.SecurityZonesLV) : void {
      def.refreshVariables(SecurityZones)
    }
    
    // 'initialValue' attribute on Variable at SecurityZones.pcf: line 13, column 68
    function initialValue_0 () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return gw.api.database.Query.make(SecurityZone).select()
    }
    
    // Page (id=SecurityZones) at SecurityZones.pcf: line 9, column 65
    static function parent_7 () : pcf.api.Destination {
      return pcf.UsersAndSecurity.createDestination()
    }
    
    override property get CurrentLocation () : pcf.SecurityZones {
      return super.CurrentLocation as pcf.SecurityZones
    }
    
    property get SecurityZones () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return getVariableValue("SecurityZones", 0) as gw.api.database.IQueryBeanResult<SecurityZone>
    }
    
    property set SecurityZones ($arg :  gw.api.database.IQueryBeanResult<SecurityZone>) {
      setVariableValue("SecurityZones", 0, $arg)
    }
    
    
  }
  
  
}