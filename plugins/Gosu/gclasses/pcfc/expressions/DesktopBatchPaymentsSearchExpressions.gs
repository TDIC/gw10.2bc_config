package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopBatchPaymentsSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopBatchPaymentsSearchExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopBatchPaymentsSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopBatchPaymentsSearchExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=DesktopBatchPaymentsSearch) at DesktopBatchPaymentsSearch.pcf: line 9, column 78
    static function canVisit_3 () : java.lang.Boolean {
      return gw.api.system.BCConfigParameters.EnableBatchPayments.Value and perm.System.viewdesktop and perm.BatchPayment.view
    }
    
    // 'def' attribute on ScreenRef at DesktopBatchPaymentsSearch.pcf: line 17, column 67
    function def_onEnter_1 (def :  pcf.DesktopBatchPaymentsScreen) : void {
      def.onEnter(desktopBatchPaymentsView)
    }
    
    // 'def' attribute on ScreenRef at DesktopBatchPaymentsSearch.pcf: line 17, column 67
    function def_refreshVariables_2 (def :  pcf.DesktopBatchPaymentsScreen) : void {
      def.refreshVariables(desktopBatchPaymentsView)
    }
    
    // 'initialValue' attribute on Variable at DesktopBatchPaymentsSearch.pcf: line 15, column 61
    function initialValue_0 () : gw.web.payment.batch.DesktopBatchPaymentsView {
      return new gw.web.payment.batch.DesktopBatchPaymentsView(gw.api.util.CurrencyUtil.getDefaultCurrency())
    }
    
    // Page (id=DesktopBatchPaymentsSearch) at DesktopBatchPaymentsSearch.pcf: line 9, column 78
    static function parent_4 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.DesktopBatchPaymentsSearch {
      return super.CurrentLocation as pcf.DesktopBatchPaymentsSearch
    }
    
    property get desktopBatchPaymentsView () : gw.web.payment.batch.DesktopBatchPaymentsView {
      return getVariableValue("desktopBatchPaymentsView", 0) as gw.web.payment.batch.DesktopBatchPaymentsView
    }
    
    property set desktopBatchPaymentsView ($arg :  gw.web.payment.batch.DesktopBatchPaymentsView) {
      setVariableValue("desktopBatchPaymentsView", 0, $arg)
    }
    
    
  }
  
  
}