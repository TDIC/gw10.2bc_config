package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailPaymentsExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyDetailPaymentsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=amount_Cell) at PolicyDetailPayments.pcf: line 170, column 61
    function currency_67 () : typekey.Currency {
      return paymentItem.GrossAmountToApply.Currency
    }
    
    // 'value' attribute on DateCell (id=payment_Cell) at PolicyDetailPayments.pcf: line 150, column 67
    function valueRoot_55 () : java.lang.Object {
      return paymentItem.BaseDist
    }
    
    // 'value' attribute on TextCell (id=invoiceItem_Cell) at PolicyDetailPayments.pcf: line 155, column 53
    function valueRoot_58 () : java.lang.Object {
      return paymentItem
    }
    
    // 'value' attribute on TextCell (id=charge_Cell) at PolicyDetailPayments.pcf: line 160, column 48
    function valueRoot_61 () : java.lang.Object {
      return paymentItem.InvoiceItem
    }
    
    // 'value' attribute on DateCell (id=payment_Cell) at PolicyDetailPayments.pcf: line 150, column 67
    function value_54 () : java.util.Date {
      return paymentItem.BaseDist.DistributedDate
    }
    
    // 'value' attribute on TextCell (id=invoiceItem_Cell) at PolicyDetailPayments.pcf: line 155, column 53
    function value_57 () : entity.InvoiceItem {
      return paymentItem.InvoiceItem
    }
    
    // 'value' attribute on TextCell (id=charge_Cell) at PolicyDetailPayments.pcf: line 160, column 48
    function value_60 () : entity.Charge {
      return paymentItem.InvoiceItem.Charge
    }
    
    // 'value' attribute on TextCell (id=payer_Cell) at PolicyDetailPayments.pcf: line 165, column 69
    function value_63 () : gw.api.domain.invoice.InvoicePayer {
      return getPayer(paymentItem)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amount_Cell) at PolicyDetailPayments.pcf: line 170, column 61
    function value_65 () : gw.pl.currency.MonetaryAmount {
      return paymentItem.GrossAmountToApply
    }
    
    property get paymentItem () : entity.BasePaymentItem {
      return getIteratedValue(1) as entity.BasePaymentItem
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailPaymentsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod, scheduled :  boolean) : int {
      return 1
    }
    
    // 'action' attribute on ToolbarButton (id=ChangePaymentPlan) at PolicyDetailPayments.pcf: line 32, column 86
    function action_1 () : void {
      ChangePaymentPlanPopup.push(plcyPeriod)
    }
    
    // 'action' attribute on TextInput (id=AccountNumber_Input) at PolicyDetailPayments.pcf: line 42, column 62
    function action_3 () : void {
      AccountOverview.go(plcyPeriod.Policy.Account)
    }
    
    // 'action' attribute on TextInput (id=AccountName_Input) at PolicyDetailPayments.pcf: line 47, column 69
    function action_8 () : void {
      AccountOverview.go(plcyPeriod.Policy.Account)
    }
    
    // 'action' attribute on ToolbarButton (id=ChangePaymentPlan) at PolicyDetailPayments.pcf: line 32, column 86
    function action_dest_2 () : pcf.api.Destination {
      return pcf.ChangePaymentPlanPopup.createDestination(plcyPeriod)
    }
    
    // 'action' attribute on TextInput (id=AccountNumber_Input) at PolicyDetailPayments.pcf: line 42, column 62
    function action_dest_4 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(plcyPeriod.Policy.Account)
    }
    
    // 'action' attribute on TextInput (id=AccountName_Input) at PolicyDetailPayments.pcf: line 47, column 69
    function action_dest_9 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(plcyPeriod.Policy.Account)
    }
    
    // 'canVisit' attribute on Page (id=PolicyDetailPayments) at PolicyDetailPayments.pcf: line 10, column 72
    static function canVisit_71 (plcyPeriod :  PolicyPeriod, scheduled :  boolean) : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcypmntview and not plcyPeriod.Archived
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=TotalScheduledPayments_Input) at PolicyDetailPayments.pcf: line 86, column 50
    function currency_27 () : typekey.Currency {
      return plcyPeriod.Currency
    }
    
    // 'def' attribute on PanelRef at PolicyDetailPayments.pcf: line 129, column 76
    function def_onEnter_47 (def :  pcf.InvoiceItemsLV) : void {
      def.onEnter(plcyPeriod.ScheduledPayments, null, true)
    }
    
    // 'def' attribute on PanelRef at PolicyDetailPayments.pcf: line 129, column 76
    function def_refreshVariables_48 (def :  pcf.InvoiceItemsLV) : void {
      def.refreshVariables(plcyPeriod.ScheduledPayments, null, true)
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailPayments.pcf: line 25, column 59
    function initialValue_0 () : gw.web.policy.PolicySummaryFinancialsHelper {
      return new gw.web.policy.PolicySummaryFinancialsHelper(plcyPeriod)
    }
    
    // Page (id=PolicyDetailPayments) at PolicyDetailPayments.pcf: line 10, column 72
    static function parent_72 (plcyPeriod :  PolicyPeriod, scheduled :  boolean) : pcf.api.Destination {
      return pcf.PolicyGroup.createDestination(plcyPeriod)
    }
    
    // 'selectOnEnter' attribute on Card (id=ActualPaymentsCard) at PolicyDetailPayments.pcf: line 136, column 87
    function selectOnEnter_70 () : java.lang.Boolean {
      return not scheduled
    }
    
    // 'value' attribute on DateCell (id=payment_Cell) at PolicyDetailPayments.pcf: line 150, column 67
    function sortValue_49 (paymentItem :  entity.BasePaymentItem) : java.lang.Object {
      return paymentItem.BaseDist.DistributedDate
    }
    
    // 'value' attribute on TextCell (id=invoiceItem_Cell) at PolicyDetailPayments.pcf: line 155, column 53
    function sortValue_50 (paymentItem :  entity.BasePaymentItem) : java.lang.Object {
      return paymentItem.InvoiceItem
    }
    
    // 'value' attribute on TextCell (id=charge_Cell) at PolicyDetailPayments.pcf: line 160, column 48
    function sortValue_51 (paymentItem :  entity.BasePaymentItem) : java.lang.Object {
      return paymentItem.InvoiceItem.Charge
    }
    
    // 'value' attribute on TextCell (id=payer_Cell) at PolicyDetailPayments.pcf: line 165, column 69
    function sortValue_52 (paymentItem :  entity.BasePaymentItem) : java.lang.Object {
      return getPayer(paymentItem)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amount_Cell) at PolicyDetailPayments.pcf: line 170, column 61
    function sortValue_53 (paymentItem :  entity.BasePaymentItem) : java.lang.Object {
      return paymentItem.GrossAmountToApply
    }
    
    // 'value' attribute on TextInput (id=PaymentPlan_Input) at PolicyDetailPayments.pcf: line 60, column 45
    function valueRoot_14 () : java.lang.Object {
      return plcyPeriod
    }
    
    // 'value' attribute on TextInput (id=DepositPercent_Input) at PolicyDetailPayments.pcf: line 65, column 47
    function valueRoot_17 () : java.lang.Object {
      return plcyPeriod.PaymentPlan
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalScheduledPayments_Input) at PolicyDetailPayments.pcf: line 86, column 50
    function valueRoot_26 () : java.lang.Object {
      return financialsHelper
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at PolicyDetailPayments.pcf: line 42, column 62
    function valueRoot_6 () : java.lang.Object {
      return plcyPeriod.Policy.Account
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at PolicyDetailPayments.pcf: line 47, column 69
    function value_10 () : java.lang.String {
      return plcyPeriod.Policy.Account.AccountNameLocalized
    }
    
    // 'value' attribute on TextInput (id=PaymentPlan_Input) at PolicyDetailPayments.pcf: line 60, column 45
    function value_13 () : entity.PaymentPlan {
      return plcyPeriod.PaymentPlan
    }
    
    // 'value' attribute on TextInput (id=DepositPercent_Input) at PolicyDetailPayments.pcf: line 65, column 47
    function value_16 () : java.math.BigDecimal {
      return plcyPeriod.PaymentPlan.DownPaymentPercent
    }
    
    // 'value' attribute on TextInput (id=NumPayments_Input) at PolicyDetailPayments.pcf: line 70, column 44
    function value_19 () : java.lang.Integer {
      return plcyPeriod.PaymentPlan.MaximumNumberOfInstallments
    }
    
    // 'value' attribute on TextInput (id=LatestLastInvoiceSent_Input) at PolicyDetailPayments.pcf: line 75, column 44
    function value_22 () : java.lang.Integer {
      return plcyPeriod.PaymentPlan.DaysBeforePolicyExpirationForInvoicingBlackout
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalScheduledPayments_Input) at PolicyDetailPayments.pcf: line 86, column 50
    function value_25 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.TotalValue
    }
    
    // 'value' attribute on MonetaryAmountInput (id=AmountReceived_Input) at PolicyDetailPayments.pcf: line 93, column 50
    function value_29 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.PaidAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Balance_Input) at PolicyDetailPayments.pcf: line 100, column 56
    function value_33 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.RemainingBalance
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnbilledBalance_Input) at PolicyDetailPayments.pcf: line 107, column 54
    function value_37 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.UnbilledAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Deposit_Input) at PolicyDetailPayments.pcf: line 114, column 77
    function value_41 () : gw.pl.currency.MonetaryAmount {
      return plcyPeriod.getDepositAmount(getPremiumChargePattern())
    }
    
    // 'value' attribute on MonetaryAmountInput (id=AverageInstallmentAmount_Input) at PolicyDetailPayments.pcf: line 121, column 84
    function value_44 () : gw.pl.currency.MonetaryAmount {
      return plcyPeriod.getAvgInstallmentAmount(getPremiumChargePattern())
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at PolicyDetailPayments.pcf: line 42, column 62
    function value_5 () : java.lang.String {
      return plcyPeriod.Policy.Account.AccountNumber
    }
    
    // 'value' attribute on RowIterator at PolicyDetailPayments.pcf: line 145, column 79
    function value_69 () : java.util.ArrayList<entity.BasePaymentItem> {
      return com.google.common.collect.Lists.newArrayList(plcyPeriod.ActualPayments)
    }
    
    override property get CurrentLocation () : pcf.PolicyDetailPayments {
      return super.CurrentLocation as pcf.PolicyDetailPayments
    }
    
    property get financialsHelper () : gw.web.policy.PolicySummaryFinancialsHelper {
      return getVariableValue("financialsHelper", 0) as gw.web.policy.PolicySummaryFinancialsHelper
    }
    
    property set financialsHelper ($arg :  gw.web.policy.PolicySummaryFinancialsHelper) {
      setVariableValue("financialsHelper", 0, $arg)
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 0, $arg)
    }
    
    property get scheduled () : boolean {
      return getVariableValue("scheduled", 0) as java.lang.Boolean
    }
    
    property set scheduled ($arg :  boolean) {
      setVariableValue("scheduled", 0, $arg)
    }
    
    function getPremiumChargePattern() : ChargePattern {
          return gw.api.web.accounting.ChargePatternHelper.getChargePattern("Premium");
          }
    
    function getPayer(paymentItem: BasePaymentItem) : gw.api.domain.invoice.InvoicePayer{
      if(paymentItem typeis AgencyPaymentItem){
        return (paymentItem.BaseDist.BaseMoneyReceived as AgencyBillMoneyRcvd).Producer
      } else if (paymentItem typeis DirectBillPaymentItem){
        return (paymentItem.BaseDist.BaseMoneyReceived as DirectBillMoneyRcvd).Account
      }
      return null
    }
    
    
  }
  
  
}