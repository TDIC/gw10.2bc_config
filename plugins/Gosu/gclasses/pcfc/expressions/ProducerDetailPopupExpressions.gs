package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerDetailPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerDetailPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'canVisit' attribute on Popup (id=ProducerDetailPopup) at ProducerDetailPopup.pcf: line 9, column 66
    static function canVisit_2 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodsummview and producer.ViewableByCurrentUser
    }
    
    // 'def' attribute on ScreenRef at ProducerDetailPopup.pcf: line 16, column 45
    function def_onEnter_0 (def :  pcf.ProducerDetailScreen) : void {
      def.onEnter(producer)
    }
    
    // 'def' attribute on ScreenRef at ProducerDetailPopup.pcf: line 16, column 45
    function def_refreshVariables_1 (def :  pcf.ProducerDetailScreen) : void {
      def.refreshVariables(producer)
    }
    
    override property get CurrentLocation () : pcf.ProducerDetailPopup {
      return super.CurrentLocation as pcf.ProducerDetailPopup
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}