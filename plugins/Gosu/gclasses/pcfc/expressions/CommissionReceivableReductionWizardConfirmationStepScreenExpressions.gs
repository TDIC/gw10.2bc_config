package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/CommissionReceivableReductionWizardConfirmationStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionReceivableReductionWizardConfirmationStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/CommissionReceivableReductionWizardConfirmationStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionReceivableReductionWizardConfirmationStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=PayableBalance_Input) at CommissionReceivableReductionWizardConfirmationStepScreen.pcf: line 22, column 63
    function currency_2 () : typekey.Currency {
      return helper.Producer.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PayableBalance_Input) at CommissionReceivableReductionWizardConfirmationStepScreen.pcf: line 22, column 63
    function valueRoot_1 () : java.lang.Object {
      return helper.ProducerCode
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at CommissionReceivableReductionWizardConfirmationStepScreen.pcf: line 28, column 34
    function valueRoot_5 () : java.lang.Object {
      return helper
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PayableBalance_Input) at CommissionReceivableReductionWizardConfirmationStepScreen.pcf: line 22, column 63
    function value_0 () : gw.pl.currency.MonetaryAmount {
      return helper.ProducerCode.TotalCommissionPayable
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at CommissionReceivableReductionWizardConfirmationStepScreen.pcf: line 28, column 34
    function value_4 () : gw.pl.currency.MonetaryAmount {
      return helper.Amount
    }
    
    // 'value' attribute on TextInput (id=ProducerCode_Input) at CommissionReceivableReductionWizardConfirmationStepScreen.pcf: line 33, column 44
    function value_8 () : entity.ProducerCode {
      return helper.ProducerCode
    }
    
    property get helper () : gw.producer.CommissionReceivableReductionHelper {
      return getRequireValue("helper", 0) as gw.producer.CommissionReceivableReductionHelper
    }
    
    property set helper ($arg :  gw.producer.CommissionReceivableReductionHelper) {
      setRequireValue("helper", 0, $arg)
    }
    
    
  }
  
  
}