package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/DocumentDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DocumentDetailsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/document/DocumentDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DocumentDetailsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (document :  Document) : int {
      return 0
    }
    
    static function __constructorIndex (document :  Document, editMetadata :  boolean) : int {
      return 1
    }
    
    // 'action' attribute on Link (id=DownloadLink) at DocumentDetailsPopup.pcf: line 69, column 178
    function action_11 () : void {
      DocumentDetailsInfo.Document.downloadFileToBrowser()
    }
    
    // 'action' attribute on Link (id=UploadLink) at DocumentDetailsPopup.pcf: line 90, column 176
    function action_19 () : void {
      UploadDocumentContentPopup.push(DocumentDetailsInfo.Document)
    }
    
    // 'action' attribute on ToolbarButton (id=CancelButton) at DocumentDetailsPopup.pcf: line 54, column 51
    function action_8 () : void {
      CurrentLocation.cancel()
    }
    
    // 'action' attribute on Link (id=UploadLink) at DocumentDetailsPopup.pcf: line 90, column 176
    function action_dest_20 () : pcf.api.Destination {
      return pcf.UploadDocumentContentPopup.createDestination(DocumentDetailsInfo.Document)
    }
    
    // 'afterCommit' attribute on Popup (id=DocumentDetailsPopup) at DocumentDetailsPopup.pcf: line 9, column 67
    function afterCommit_32 (TopLocation :  pcf.api.Location) : void {
      DocumentDetailsPopup.push(DocumentDetailsInfo.Document, editMetadata)
    }
    
    // 'available' attribute on Link (id=DownloadLink) at DocumentDetailsPopup.pcf: line 69, column 178
    function available_9 () : java.lang.Boolean {
      return documentActionsHelper.isDocumentContentActionsAvailableInDocumentProperties(CurrentLocation.InEditMode, DocumentDetailsInfo.Document)
    }
    
    // 'canEdit' attribute on Popup (id=DocumentDetailsPopup) at DocumentDetailsPopup.pcf: line 9, column 67
    function canEdit_33 () : java.lang.Boolean {
      return perm.Document.edit(document)
    }
    
    // 'def' attribute on PanelRef at DocumentDetailsPopup.pcf: line 99, column 33
    function def_onEnter_22 (def :  pcf.DocumentMetadataEditDV_default) : void {
      def.onEnter(new gw.document.DocumentMetadataBCHelper(document), false)
    }
    
    // 'def' attribute on PanelRef at DocumentDetailsPopup.pcf: line 99, column 33
    function def_onEnter_24 (def :  pcf.DocumentMetadataEditDV_email_sent) : void {
      def.onEnter(new gw.document.DocumentMetadataBCHelper(document), false)
    }
    
    // 'def' attribute on PanelRef at DocumentDetailsPopup.pcf: line 103, column 33
    function def_onEnter_27 (def :  pcf.DocumentDetailsDV_default) : void {
      def.onEnter(document)
    }
    
    // 'def' attribute on PanelRef at DocumentDetailsPopup.pcf: line 103, column 33
    function def_onEnter_29 (def :  pcf.DocumentDetailsDV_email_sent) : void {
      def.onEnter(document)
    }
    
    // 'def' attribute on PanelRef at DocumentDetailsPopup.pcf: line 99, column 33
    function def_refreshVariables_23 (def :  pcf.DocumentMetadataEditDV_default) : void {
      def.refreshVariables(new gw.document.DocumentMetadataBCHelper(document), false)
    }
    
    // 'def' attribute on PanelRef at DocumentDetailsPopup.pcf: line 99, column 33
    function def_refreshVariables_25 (def :  pcf.DocumentMetadataEditDV_email_sent) : void {
      def.refreshVariables(new gw.document.DocumentMetadataBCHelper(document), false)
    }
    
    // 'def' attribute on PanelRef at DocumentDetailsPopup.pcf: line 103, column 33
    function def_refreshVariables_28 (def :  pcf.DocumentDetailsDV_default) : void {
      def.refreshVariables(document)
    }
    
    // 'def' attribute on PanelRef at DocumentDetailsPopup.pcf: line 103, column 33
    function def_refreshVariables_30 (def :  pcf.DocumentDetailsDV_email_sent) : void {
      def.refreshVariables(document)
    }
    
    // 'icon' attribute on Link (id=DownloadLink) at DocumentDetailsPopup.pcf: line 69, column 178
    function icon_13 () : java.lang.String {
      return "document_download" 
    }
    
    // 'initialValue' attribute on Variable at DocumentDetailsPopup.pcf: line 20, column 47
    function initialValue_0 () : gw.document.DocumentDetailsInfo {
      return new gw.document.DocumentDetailsInfo(document)
    }
    
    // 'initialValue' attribute on Variable at DocumentDetailsPopup.pcf: line 24, column 52
    function initialValue_1 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // EditButtons at DocumentDetailsPopup.pcf: line 47, column 147
    function label_6 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'mode' attribute on PanelRef at DocumentDetailsPopup.pcf: line 99, column 33
    function mode_26 () : java.lang.Object {
      return document.Type
    }
    
    // 'tooltip' attribute on Link (id=DownloadLink) at DocumentDetailsPopup.pcf: line 69, column 178
    function tooltip_12 () : java.lang.String {
      return documentActionsHelper.DownloadDocumentContentTooltip
    }
    
    // 'tooltip' attribute on Link (id=UploadLink) at DocumentDetailsPopup.pcf: line 90, column 176
    function tooltip_21 () : java.lang.String {
      return documentActionsHelper.UploadDocumentContentTooltip
    }
    
    // 'visible' attribute on Link (id=DownloadLink) at DocumentDetailsPopup.pcf: line 69, column 178
    function visible_10 () : java.lang.Boolean {
      return editMetadata and documentActionsHelper.isDownloadDocumentContentVisibleInDocumentProperties(CurrentLocation.InEditMode, DocumentDetailsInfo.Document)
    }
    
    // 'visible' attribute on Link (id=Spacer1) at DocumentDetailsPopup.pcf: line 73, column 176
    function visible_14 () : java.lang.Boolean {
      return editMetadata and documentActionsHelper.isUploadDocumentContentVisibleInDocumentProperties(CurrentLocation.InEditMode, DocumentDetailsInfo.Document)
    }
    
    // 'visible' attribute on AlertBar (id=DocumentStoreDestinationDisabledWarning) at DocumentDetailsPopup.pcf: line 38, column 132
    function visible_3 () : java.lang.Boolean {
      return documentsActionsHelper.ShowDocumentStoreSuspendedWarning and DocumentDetailsInfo.Document.PendingDocUID != null
    }
    
    // 'visible' attribute on AlertBar (id=ContentlessNoActionsText) at DocumentDetailsPopup.pcf: line 42, column 54
    function visible_4 () : java.lang.Boolean {
      return !DocumentDetailsInfo.Document.DMS
    }
    
    // 'editVisible' attribute on EditButtons at DocumentDetailsPopup.pcf: line 47, column 147
    function visible_5 () : java.lang.Boolean {
      return editMetadata and documentsActionsHelper.isEditDocumentDetailsVisibleInDocumentProperties(DocumentDetailsInfo.Document)
    }
    
    // 'visible' attribute on ToolbarButton (id=CancelButton) at DocumentDetailsPopup.pcf: line 54, column 51
    function visible_7 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    override property get CurrentLocation () : pcf.DocumentDetailsPopup {
      return super.CurrentLocation as pcf.DocumentDetailsPopup
    }
    
    property get DocumentDetailsInfo () : gw.document.DocumentDetailsInfo {
      return getVariableValue("DocumentDetailsInfo", 0) as gw.document.DocumentDetailsInfo
    }
    
    property set DocumentDetailsInfo ($arg :  gw.document.DocumentDetailsInfo) {
      setVariableValue("DocumentDetailsInfo", 0, $arg)
    }
    
    property get document () : Document {
      return getVariableValue("document", 0) as Document
    }
    
    property set document ($arg :  Document) {
      setVariableValue("document", 0, $arg)
    }
    
    property get documentActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentActionsHelper", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    property get editMetadata () : boolean {
      return getVariableValue("editMetadata", 0) as java.lang.Boolean
    }
    
    property set editMetadata ($arg :  boolean) {
      setVariableValue("editMetadata", 0, $arg)
    }
    
    
  }
  
  
}