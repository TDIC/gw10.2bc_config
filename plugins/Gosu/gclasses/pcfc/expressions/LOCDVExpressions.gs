package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/LOCDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LOCDVExpressions {
  @javax.annotation.Generated("config/web/pcf/collateral/LOCDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LOCDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=amount_Input) at LOCDV.pcf: line 38, column 29
    function currency_17 () : typekey.Currency {
      return loc.Currency
    }
    
    // 'value' attribute on TextInput (id=idInput_Input) at LOCDV.pcf: line 18, column 28
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      loc.LOCID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at LOCDV.pcf: line 30, column 37
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      loc.ExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amount_Input) at LOCDV.pcf: line 38, column 29
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      loc.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextInput (id=bankName_Input) at LOCDV.pcf: line 24, column 31
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      loc.BankName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'validationExpression' attribute on DateInput (id=ExpirationDate_Input) at LOCDV.pcf: line 30, column 37
    function validationExpression_8 () : java.lang.Object {
      return gw.api.util.DateUtil.verifyDateOnOrAfterToday(loc.ExpirationDate) or loc.ExpirationDate == null ? null : DisplayKey.get("Web.LOCDV.ExpirationDateError")
    }
    
    // 'value' attribute on TextInput (id=idInput_Input) at LOCDV.pcf: line 18, column 28
    function valueRoot_2 () : java.lang.Object {
      return loc
    }
    
    // 'value' attribute on TextInput (id=idInput_Input) at LOCDV.pcf: line 18, column 28
    function value_0 () : java.lang.String {
      return loc.LOCID
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amount_Input) at LOCDV.pcf: line 38, column 29
    function value_14 () : gw.pl.currency.MonetaryAmount {
      return loc.Amount
    }
    
    // 'value' attribute on TextInput (id=bankName_Input) at LOCDV.pcf: line 24, column 31
    function value_4 () : java.lang.String {
      return loc.BankName
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at LOCDV.pcf: line 30, column 37
    function value_9 () : java.util.Date {
      return loc.ExpirationDate
    }
    
    property get loc () : LetterOfCredit {
      return getRequireValue("loc", 0) as LetterOfCredit
    }
    
    property set loc ($arg :  LetterOfCredit) {
      setRequireValue("loc", 0, $arg)
    }
    
    
  }
  
  
}