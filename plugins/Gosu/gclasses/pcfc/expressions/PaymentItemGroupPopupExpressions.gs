package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/PaymentItemGroupPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentItemGroupPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/PaymentItemGroupPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PaymentItemGroupPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=itemOwner_Cell) at PaymentItemGroupPopup.pcf: line 69, column 45
    function action_27 () : void {
      AccountSummary.push(invoiceItem.Owner)
    }
    
    // 'action' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at PaymentItemGroupPopup.pcf: line 90, column 51
    function action_38 () : void {
      AccountPayments.push(paymentItemGroup.UnappliedFund.Account, paymentItemAction.Item.DirectBillPayment.DirectBillMoneyRcvd)
    }
    
    // 'action' attribute on TextCell (id=itemOwner_Cell) at PaymentItemGroupPopup.pcf: line 69, column 45
    function action_dest_28 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination(invoiceItem.Owner)
    }
    
    // 'action' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at PaymentItemGroupPopup.pcf: line 90, column 51
    function action_dest_39 () : pcf.api.Destination {
      return pcf.AccountPayments.createDestination(paymentItemGroup.UnappliedFund.Account, paymentItemAction.Item.DirectBillPayment.DirectBillMoneyRcvd)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at PaymentItemGroupPopup.pcf: line 81, column 44
    function currency_36 () : typekey.Currency {
      return invoiceItem.Currency
    }
    
    // 'initialValue' attribute on Variable at PaymentItemGroupPopup.pcf: line 46, column 35
    function initialValue_17 () : InvoiceItem {
      return  paymentItemAction.Item typeis DirectBillPaymentItem ? paymentItemAction.Item.InvoiceItem : null
    }
    
    // RowIterator at PaymentItemGroupPopup.pcf: line 42, column 52
    function initializeVariables_44 () : void {
        invoiceItem =  paymentItemAction.Item typeis DirectBillPaymentItem ? paymentItemAction.Item.InvoiceItem : null;

    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at PaymentItemGroupPopup.pcf: line 52, column 60
    function valueRoot_19 () : java.lang.Object {
      return invoiceItem.Invoice
    }
    
    // 'value' attribute on TextCell (id=Invoice_Cell) at PaymentItemGroupPopup.pcf: line 58, column 45
    function valueRoot_22 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at PaymentItemGroupPopup.pcf: line 90, column 51
    function valueRoot_41 () : java.lang.Object {
      return paymentItemAction
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at PaymentItemGroupPopup.pcf: line 52, column 60
    function value_18 () : java.lang.String {
      return invoiceItem.Invoice.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=Invoice_Cell) at PaymentItemGroupPopup.pcf: line 58, column 45
    function value_21 () : entity.Invoice {
      return invoiceItem.Invoice
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at PaymentItemGroupPopup.pcf: line 62, column 53
    function value_24 () : java.util.Date {
      return invoiceItem.InvoiceDueDate
    }
    
    // 'value' attribute on TextCell (id=itemOwner_Cell) at PaymentItemGroupPopup.pcf: line 69, column 45
    function value_29 () : entity.Account {
      return invoiceItem.Owner
    }
    
    // 'value' attribute on TextCell (id=itremType_Cell) at PaymentItemGroupPopup.pcf: line 73, column 102
    function value_32 () : java.lang.String {
      return invoiceItem != null ? invoiceItem.Type.DisplayName : "Collateral"
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at PaymentItemGroupPopup.pcf: line 81, column 44
    function value_34 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at PaymentItemGroupPopup.pcf: line 90, column 51
    function value_40 () : gw.pl.currency.MonetaryAmount {
      return paymentItemAction.Amount
    }
    
    property get invoiceItem () : InvoiceItem {
      return getVariableValue("invoiceItem", 1) as InvoiceItem
    }
    
    property set invoiceItem ($arg :  InvoiceItem) {
      setVariableValue("invoiceItem", 1, $arg)
    }
    
    property get paymentItemAction () : entity.PaymentItemAction {
      return getIteratedValue(1) as entity.PaymentItemAction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/PaymentItemGroupPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentItemGroupPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (paymentItemGroup :  PaymentItemGroup) : int {
      return 0
    }
    
    // 'action' attribute on TextInput (id=policyPeriodDisplayName_Input) at PaymentItemGroupPopup.pcf: line 30, column 62
    function action_5 () : void {
      PolicyOverview.push(paymentItemGroup.PolicyPeriod)
    }
    
    // 'action' attribute on TextInput (id=policyPeriodDisplayName_Input) at PaymentItemGroupPopup.pcf: line 30, column 62
    function action_dest_6 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(paymentItemGroup.PolicyPeriod)
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at PaymentItemGroupPopup.pcf: line 52, column 60
    function sortValue_11 (paymentItemAction :  entity.PaymentItemAction) : java.lang.Object {
      var invoiceItem : InvoiceItem = ( paymentItemAction.Item typeis DirectBillPaymentItem ? paymentItemAction.Item.InvoiceItem : null)
return invoiceItem.Invoice.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=Invoice_Cell) at PaymentItemGroupPopup.pcf: line 58, column 45
    function sortValue_12 (paymentItemAction :  entity.PaymentItemAction) : java.lang.Object {
      var invoiceItem : InvoiceItem = ( paymentItemAction.Item typeis DirectBillPaymentItem ? paymentItemAction.Item.InvoiceItem : null)
return invoiceItem.Invoice
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at PaymentItemGroupPopup.pcf: line 62, column 53
    function sortValue_13 (paymentItemAction :  entity.PaymentItemAction) : java.lang.Object {
      var invoiceItem : InvoiceItem = ( paymentItemAction.Item typeis DirectBillPaymentItem ? paymentItemAction.Item.InvoiceItem : null)
return invoiceItem.InvoiceDueDate
    }
    
    // 'value' attribute on TextCell (id=itremType_Cell) at PaymentItemGroupPopup.pcf: line 73, column 102
    function sortValue_14 (paymentItemAction :  entity.PaymentItemAction) : java.lang.Object {
      var invoiceItem : InvoiceItem = ( paymentItemAction.Item typeis DirectBillPaymentItem ? paymentItemAction.Item.InvoiceItem : null)
return invoiceItem != null ? invoiceItem.Type.DisplayName : "Collateral"
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at PaymentItemGroupPopup.pcf: line 81, column 44
    function sortValue_15 (paymentItemAction :  entity.PaymentItemAction) : java.lang.Object {
      var invoiceItem : InvoiceItem = ( paymentItemAction.Item typeis DirectBillPaymentItem ? paymentItemAction.Item.InvoiceItem : null)
return invoiceItem.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at PaymentItemGroupPopup.pcf: line 90, column 51
    function sortValue_16 (paymentItemAction :  entity.PaymentItemAction) : java.lang.Object {
      return paymentItemAction.Amount
    }
    
    // 'value' attribute on TextInput (id=paymentItemGroupDisplayName_Input) at PaymentItemGroupPopup.pcf: line 24, column 51
    function valueRoot_2 () : java.lang.Object {
      return paymentItemGroup
    }
    
    // 'value' attribute on TextInput (id=policyPeriodDisplayName_Input) at PaymentItemGroupPopup.pcf: line 30, column 62
    function valueRoot_8 () : java.lang.Object {
      return paymentItemGroup.PolicyPeriod
    }
    
    // 'value' attribute on TextInput (id=paymentItemGroupDisplayName_Input) at PaymentItemGroupPopup.pcf: line 24, column 51
    function value_1 () : java.lang.String {
      return paymentItemGroup.DisplayName
    }
    
    // 'value' attribute on RowIterator at PaymentItemGroupPopup.pcf: line 42, column 52
    function value_45 () : entity.PaymentItemAction[] {
      return paymentItemGroup.PaymentItemActions
    }
    
    // 'value' attribute on TextInput (id=policyPeriodDisplayName_Input) at PaymentItemGroupPopup.pcf: line 30, column 62
    function value_7 () : java.lang.String {
      return paymentItemGroup.PolicyPeriod.DisplayName
    }
    
    // 'visible' attribute on AlertBar (id=PaymentItemGroupArchiveWarning) at PaymentItemGroupPopup.pcf: line 18, column 57
    function visible_0 () : java.lang.Boolean {
      return paymentItemGroup.ImpactedByArchiving
    }
    
    // 'visible' attribute on TextInput (id=policyPeriodDisplayName_Input) at PaymentItemGroupPopup.pcf: line 30, column 62
    function visible_4 () : java.lang.Boolean {
      return paymentItemGroup.PolicyPeriod != null
    }
    
    override property get CurrentLocation () : pcf.PaymentItemGroupPopup {
      return super.CurrentLocation as pcf.PaymentItemGroupPopup
    }
    
    property get paymentItemGroup () : PaymentItemGroup {
      return getVariableValue("paymentItemGroup", 0) as PaymentItemGroup
    }
    
    property set paymentItemGroup ($arg :  PaymentItemGroup) {
      setVariableValue("paymentItemGroup", 0, $arg)
    }
    
    
  }
  
  
}