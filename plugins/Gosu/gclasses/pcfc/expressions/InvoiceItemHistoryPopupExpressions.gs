package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/InvoiceItemHistoryPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceItemHistoryPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/InvoiceItemHistoryPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceItemHistoryPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (invoiceItem :  InvoiceItem) : int {
      return 0
    }
    
    // 'def' attribute on PanelRef at InvoiceItemHistoryPopup.pcf: line 67, column 58
    function def_onEnter_15 (def :  pcf.InvoiceItemBreakdownPanelSet) : void {
      def.onEnter(invoiceItem)
    }
    
    // 'def' attribute on PanelRef (id=itemEvents) at InvoiceItemHistoryPopup.pcf: line 70, column 26
    function def_onEnter_17 (def :  pcf.ItemEventPanelSet) : void {
      def.onEnter(invoiceItem)
    }
    
    // 'def' attribute on InputSetRef at InvoiceItemHistoryPopup.pcf: line 29, column 50
    function def_onEnter_2 (def :  pcf.ChargeInputSet) : void {
      def.onEnter( invoiceItem )
    }
    
    // 'def' attribute on InputSetRef at InvoiceItemHistoryPopup.pcf: line 35, column 54
    function def_onEnter_4 (def :  pcf.AgencyItemInputSet) : void {
      def.onEnter( invoiceItem )
    }
    
    // 'def' attribute on InputSetRef at InvoiceItemHistoryPopup.pcf: line 41, column 61
    function def_onEnter_6 (def :  pcf.AgencyItemCreditsInputSet) : void {
      def.onEnter( invoiceItem )
    }
    
    // 'def' attribute on InputSetRef at InvoiceItemHistoryPopup.pcf: line 47, column 59
    function def_onEnter_8 (def :  pcf.AgencyItemBalanceInputSet) : void {
      def.onEnter(invoiceItem)
    }
    
    // 'def' attribute on PanelRef at InvoiceItemHistoryPopup.pcf: line 67, column 58
    function def_refreshVariables_16 (def :  pcf.InvoiceItemBreakdownPanelSet) : void {
      def.refreshVariables(invoiceItem)
    }
    
    // 'def' attribute on PanelRef (id=itemEvents) at InvoiceItemHistoryPopup.pcf: line 70, column 26
    function def_refreshVariables_18 (def :  pcf.ItemEventPanelSet) : void {
      def.refreshVariables(invoiceItem)
    }
    
    // 'def' attribute on InputSetRef at InvoiceItemHistoryPopup.pcf: line 29, column 50
    function def_refreshVariables_3 (def :  pcf.ChargeInputSet) : void {
      def.refreshVariables( invoiceItem )
    }
    
    // 'def' attribute on InputSetRef at InvoiceItemHistoryPopup.pcf: line 35, column 54
    function def_refreshVariables_5 (def :  pcf.AgencyItemInputSet) : void {
      def.refreshVariables( invoiceItem )
    }
    
    // 'def' attribute on InputSetRef at InvoiceItemHistoryPopup.pcf: line 41, column 61
    function def_refreshVariables_7 (def :  pcf.AgencyItemCreditsInputSet) : void {
      def.refreshVariables( invoiceItem )
    }
    
    // 'def' attribute on InputSetRef at InvoiceItemHistoryPopup.pcf: line 47, column 59
    function def_refreshVariables_9 (def :  pcf.AgencyItemBalanceInputSet) : void {
      def.refreshVariables(invoiceItem)
    }
    
    // 'label' attribute on AlertBar (id=CarriedForwardHistory) at InvoiceItemHistoryPopup.pcf: line 23, column 65
    function label_1 () : java.lang.Object {
      return DisplayKey.get("Web.InvoiceItemHistoryPopup.HasBeenCarriedForward", invoiceItem.PaymentExceptionLockDate.AsUIStyle)
    }
    
    // 'value' attribute on TextAreaInput (id=ExceptionComments_Input) at InvoiceItemHistoryPopup.pcf: line 61, column 103
    function valueRoot_12 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on TextAreaInput (id=ExceptionComments_Input) at InvoiceItemHistoryPopup.pcf: line 61, column 103
    function value_11 () : java.lang.String {
      return invoiceItem.ExceptionComments
    }
    
    // 'visible' attribute on AlertBar (id=CarriedForwardHistory) at InvoiceItemHistoryPopup.pcf: line 23, column 65
    function visible_0 () : java.lang.Boolean {
      return invoiceItem.PaymentExceptionLockDate != null
    }
    
    // 'visible' attribute on TextAreaInput (id=ExceptionComments_Input) at InvoiceItemHistoryPopup.pcf: line 61, column 103
    function visible_10 () : java.lang.Boolean {
      return org.apache.commons.lang.StringUtils.isEmpty(invoiceItem.ExceptionComments)
    }
    
    override property get CurrentLocation () : pcf.InvoiceItemHistoryPopup {
      return super.CurrentLocation as pcf.InvoiceItemHistoryPopup
    }
    
    property get changed () : boolean {
      return getVariableValue("changed", 0) as java.lang.Boolean
    }
    
    property set changed ($arg :  boolean) {
      setVariableValue("changed", 0, $arg)
    }
    
    property get invoiceItem () : InvoiceItem {
      return getVariableValue("invoiceItem", 0) as InvoiceItem
    }
    
    property set invoiceItem ($arg :  InvoiceItem) {
      setVariableValue("invoiceItem", 0, $arg)
    }
    
    
                    function findCommissionTransactions(): CommissionsReserveTxn[] {
                        // todo - dwsmith
                        var transactions = invoiceItem.Charge.ChargeCommissions.first().Transactions.toList()
                                .whereTypeIs(CommissionsReserveTxn).where(\t -> not t.Reversal and (t typeis CommissionsReserveTxn))
                        return transactions.toTypedArray()
                    }
    
    function getCommissionAdjustedMessage() : String{
      var commissionOverride = invoiceItem.Charge.CommissionRateOverrides[0]
      return DisplayKey.get("Web.InvoiceItemHistoryPopup.CommissionAdjusted", commissionOverride.CreateTime)
    }
    
                    function isCommissionRateAdjusted(): boolean {
                        var commissionOverrides = invoiceItem.Charge.CommissionRateOverrides
                        return commissionOverrides.length > 0;
                    }
            
    
    
  }
  
  
}