package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerPolicies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerPoliciesExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerPolicies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends ProducerPoliciesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at ProducerPolicies.pcf: line 163, column 35
    function action_70 () : void {
      PolicyOverview.push(policyCommission.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at ProducerPolicies.pcf: line 198, column 35
    function action_92 () : void {
      AccountSummary.push(policyCommission.PolicyPeriod.Policy.Account)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at ProducerPolicies.pcf: line 163, column 35
    function action_dest_71 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(policyCommission.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at ProducerPolicies.pcf: line 198, column 35
    function action_dest_93 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination(policyCommission.PolicyPeriod.Policy.Account)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=PolicyValue_Cell) at ProducerPolicies.pcf: line 205, column 71
    function currency_99 () : typekey.Currency {
      return policyCommission.Currency
    }
    
    // 'initialValue' attribute on Variable at ProducerPolicies.pcf: line 148, column 57
    function initialValue_69 () : gw.pl.currency.MonetaryAmount {
      return policyCommission.calculateCommissionBasis()
    }
    
    // RowIterator at ProducerPolicies.pcf: line 143, column 93
    function initializeVariables_127 () : void {
        commissionBasis = policyCommission.calculateCommissionBasis();

    }
    
    // 'outputConversion' attribute on TextCell (id=CommissionRate_Cell) at ProducerPolicies.pcf: line 259, column 55
    function outputConversion_124 (VALUE :  java.math.BigDecimal) : java.lang.String {
      return gw.api.util.StringUtil.formatPercentageValue(VALUE, 2)
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ProducerPolicies.pcf: line 163, column 35
    function valueRoot_73 () : java.lang.Object {
      return policyCommission
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at ProducerPolicies.pcf: line 167, column 77
    function valueRoot_76 () : java.lang.Object {
      return policyCommission.PolicyPeriod
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at ProducerPolicies.pcf: line 177, column 35
    function valueRoot_82 () : java.lang.Object {
      return policyCommission.PolicyPeriod.Policy
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at ProducerPolicies.pcf: line 187, column 65
    function valueRoot_87 () : java.lang.Object {
      return policyCommission.ProducerCode
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at ProducerPolicies.pcf: line 198, column 35
    function valueRoot_95 () : java.lang.Object {
      return policyCommission.PolicyPeriod.Policy.Account
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalCommission_Cell) at ProducerPolicies.pcf: line 213, column 72
    function value_101 () : gw.pl.currency.MonetaryAmount {
      return policyCommission.CommissionExpenseBalance
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionReserve_Cell) at ProducerPolicies.pcf: line 221, column 72
    function value_105 () : gw.pl.currency.MonetaryAmount {
      return policyCommission.CommissionReserveBalance
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionEarnedRetained_Cell) at ProducerPolicies.pcf: line 229, column 72
    function value_109 () : gw.pl.currency.MonetaryAmount {
      return policyCommission.CommissionEarnedRetained
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionWrittenOff_Cell) at ProducerPolicies.pcf: line 237, column 68
    function value_113 () : gw.pl.currency.MonetaryAmount {
      return policyCommission.CommissionWrittenOff
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionPaid_Cell) at ProducerPolicies.pcf: line 245, column 62
    function value_117 () : gw.pl.currency.MonetaryAmount {
      return policyCommission.PaidCommission
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionBasis_Cell) at ProducerPolicies.pcf: line 252, column 46
    function value_121 () : gw.pl.currency.MonetaryAmount {
      return commissionBasis
    }
    
    // 'value' attribute on TextCell (id=CommissionRate_Cell) at ProducerPolicies.pcf: line 259, column 55
    function value_125 () : java.math.BigDecimal {
      return commissionBasis.IsNotZero ? policyCommission.CommissionExpenseBalance / commissionBasis : null
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ProducerPolicies.pcf: line 163, column 35
    function value_72 () : entity.PolicyPeriod {
      return policyCommission.PolicyPeriod
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at ProducerPolicies.pcf: line 167, column 77
    function value_75 () : java.util.Date {
      return policyCommission.PolicyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at ProducerPolicies.pcf: line 171, column 79
    function value_78 () : java.util.Date {
      return policyCommission.PolicyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at ProducerPolicies.pcf: line 177, column 35
    function value_81 () : typekey.LOBCode {
      return policyCommission.PolicyPeriod.Policy.LOBCode
    }
    
    // 'value' attribute on TextCell (id=Insured_Cell) at ProducerPolicies.pcf: line 183, column 48
    function value_84 () : entity.Person {
      return policyCommission.PolicyPeriod.PrimaryInsured.Contact typeis Person ? policyCommission.PolicyPeriod.PrimaryInsured.Contact : null
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at ProducerPolicies.pcf: line 187, column 65
    function value_86 () : java.lang.String {
      return policyCommission.ProducerCode.Code
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at ProducerPolicies.pcf: line 192, column 53
    function value_89 () : typekey.PolicyRole {
      return policyCommission.Role
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at ProducerPolicies.pcf: line 198, column 35
    function value_94 () : java.lang.String {
      return policyCommission.PolicyPeriod.Policy.Account.AccountNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PolicyValue_Cell) at ProducerPolicies.pcf: line 205, column 71
    function value_97 () : gw.pl.currency.MonetaryAmount {
      return policyCommission.PolicyPeriod.TotalValue
    }
    
    property get commissionBasis () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("commissionBasis", 1) as gw.pl.currency.MonetaryAmount
    }
    
    property set commissionBasis ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("commissionBasis", 1, $arg)
    }
    
    property get policyCommission () : entity.PolicyCommission {
      return getIteratedValue(1) as entity.PolicyCommission
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerPolicies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends ProducerPoliciesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at ProducerPolicies.pcf: line 292, column 35
    function action_142 () : void {
      PolicyOverview.push(archivedPolicyCommissionSummary.PolicyPeriodArchiveSummary.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at ProducerPolicies.pcf: line 292, column 35
    function action_dest_143 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(archivedPolicyCommissionSummary.PolicyPeriodArchiveSummary.PolicyPeriod)
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ProducerPolicies.pcf: line 292, column 35
    function valueRoot_145 () : java.lang.Object {
      return archivedPolicyCommissionSummary.PolicyPeriodArchiveSummary
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at ProducerPolicies.pcf: line 296, column 80
    function valueRoot_148 () : java.lang.Object {
      return archivedPolicyCommissionSummary.ProducerCode
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at ProducerPolicies.pcf: line 301, column 53
    function valueRoot_151 () : java.lang.Object {
      return archivedPolicyCommissionSummary
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ProducerPolicies.pcf: line 292, column 35
    function value_144 () : entity.PolicyPeriod {
      return archivedPolicyCommissionSummary.PolicyPeriodArchiveSummary.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at ProducerPolicies.pcf: line 296, column 80
    function value_147 () : java.lang.String {
      return archivedPolicyCommissionSummary.ProducerCode.Code
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at ProducerPolicies.pcf: line 301, column 53
    function value_150 () : typekey.PolicyRole {
      return archivedPolicyCommissionSummary.Role
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PolicyCmsnExpense_Cell) at ProducerPolicies.pcf: line 307, column 80
    function value_153 () : gw.pl.currency.MonetaryAmount {
      return archivedPolicyCommissionSummary.PolicyCmsnExpense
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionEarnedRetained_Cell) at ProducerPolicies.pcf: line 313, column 87
    function value_156 () : gw.pl.currency.MonetaryAmount {
      return archivedPolicyCommissionSummary.ChargeCmsnEarnedRetained
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionWrittenOff_Cell) at ProducerPolicies.pcf: line 319, column 83
    function value_159 () : gw.pl.currency.MonetaryAmount {
      return archivedPolicyCommissionSummary.ChargeCmsnWrittenOff
    }
    
    property get archivedPolicyCommissionSummary () : entity.PolicyCommissionArchiveSummary {
      return getIteratedValue(1) as entity.PolicyCommissionArchiveSummary
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerPolicies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ProducerPoliciesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at ProducerPolicies.pcf: line 88, column 35
    function action_24 () : void {
      PolicyOverview.push(policyCommission.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at ProducerPolicies.pcf: line 126, column 35
    function action_47 () : void {
      AccountSummary.push(policyCommission.PolicyPeriod.Policy.Account)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at ProducerPolicies.pcf: line 88, column 35
    function action_dest_25 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(policyCommission.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at ProducerPolicies.pcf: line 126, column 35
    function action_dest_48 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination(policyCommission.PolicyPeriod.Policy.Account)
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ProducerPolicies.pcf: line 88, column 35
    function valueRoot_27 () : java.lang.Object {
      return policyCommission
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at ProducerPolicies.pcf: line 92, column 77
    function valueRoot_30 () : java.lang.Object {
      return policyCommission.PolicyPeriod
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at ProducerPolicies.pcf: line 102, column 35
    function valueRoot_36 () : java.lang.Object {
      return policyCommission.PolicyPeriod.Policy
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at ProducerPolicies.pcf: line 114, column 35
    function valueRoot_42 () : java.lang.Object {
      return policyCommission.ProducerCode
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at ProducerPolicies.pcf: line 126, column 35
    function valueRoot_50 () : java.lang.Object {
      return policyCommission.PolicyPeriod.Policy.Account
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ProducerPolicies.pcf: line 88, column 35
    function value_26 () : entity.PolicyPeriod {
      return policyCommission.PolicyPeriod
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at ProducerPolicies.pcf: line 92, column 77
    function value_29 () : java.util.Date {
      return policyCommission.PolicyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at ProducerPolicies.pcf: line 96, column 79
    function value_32 () : java.util.Date {
      return policyCommission.PolicyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at ProducerPolicies.pcf: line 102, column 35
    function value_35 () : typekey.LOBCode {
      return policyCommission.PolicyPeriod.Policy.LOBCode
    }
    
    // 'value' attribute on TextCell (id=Insured_Cell) at ProducerPolicies.pcf: line 109, column 61
    function value_38 () : entity.PolicyPeriodContact {
      return policyCommission.PolicyPeriod.PrimaryInsured
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at ProducerPolicies.pcf: line 114, column 35
    function value_41 () : java.lang.String {
      return policyCommission.ProducerCode.Code
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at ProducerPolicies.pcf: line 119, column 53
    function value_44 () : typekey.PolicyRole {
      return policyCommission.Role
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at ProducerPolicies.pcf: line 126, column 35
    function value_49 () : java.lang.String {
      return policyCommission.PolicyPeriod.Policy.Account.AccountNumber
    }
    
    property get policyCommission () : entity.PolicyCommission {
      return getIteratedValue(1) as entity.PolicyCommission
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerPolicies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerPoliciesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=PolicyFilterClearButton) at ProducerPolicies.pcf: line 52, column 123
    function action_10 () : void {
      policyStartsWith = null; policyCommissions = producer.getPolicyCommissionsWithBalances(policyStartsWith)
    }
    
    // 'action' attribute on AlertBar (id=ProducerPolicies_TroubleTicketAlertAlertBar) at ProducerPolicies.pcf: line 59, column 53
    function action_13 () : void {
      TroubleTicketAlertForward.push(producer)
    }
    
    // 'action' attribute on ToolbarButton (id=transfer) at ProducerPolicies.pcf: line 36, column 84
    function action_4 () : void {
      TransferProducerPoliciesWizard.push(producer)
    }
    
    // 'action' attribute on ToolbarButton (id=PolicyFilterGoButton) at ProducerPolicies.pcf: line 48, column 120
    function action_9 () : void {
      policyCommissions = producer.getPolicyCommissionsWithBalances(policyStartsWith); archivedPolicyCommissions = producer.getArchivedPolicyCommissionsWithBalances(policyStartsWith);
    }
    
    // 'action' attribute on AlertBar (id=ProducerPolicies_TroubleTicketAlertAlertBar) at ProducerPolicies.pcf: line 59, column 53
    function action_dest_14 () : pcf.api.Destination {
      return pcf.TroubleTicketAlertForward.createDestination(producer)
    }
    
    // 'action' attribute on ToolbarButton (id=transfer) at ProducerPolicies.pcf: line 36, column 84
    function action_dest_5 () : pcf.api.Destination {
      return pcf.TransferProducerPoliciesWizard.createDestination(producer)
    }
    
    // 'available' attribute on AlertBar (id=ProducerPolicies_TroubleTicketAlertAlertBar) at ProducerPolicies.pcf: line 59, column 53
    function available_11 () : java.lang.Boolean {
      return perm.System.prodttktview
    }
    
    // 'available' attribute on ToolbarButton (id=transfer) at ProducerPolicies.pcf: line 36, column 84
    function available_3 () : java.lang.Boolean {
      return perm.PolicyPeriod.plcyprodtx
    }
    
    // 'canVisit' attribute on Page (id=ProducerPolicies) at ProducerPolicies.pcf: line 10, column 68
    static function canVisit_164 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodplcyview
    }
    
    // 'value' attribute on TextValue (id=PolicyPeriodFilter) at ProducerPolicies.pcf: line 43, column 41
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyStartsWith = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at ProducerPolicies.pcf: line 282, column 124
    function filters_129 () : gw.api.filters.IFilter[] {
      return new gw.api.web.producer.ProducerPoliciesProducerCodeFilterSet(producer, true).FilterOptions
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at ProducerPolicies.pcf: line 78, column 125
    function filters_16 () : gw.api.filters.IFilter[] {
      return new gw.api.web.producer.ProducerPoliciesProducerCodeFilterSet(producer, false).FilterOptions
    }
    
    // 'initialValue' attribute on Variable at ProducerPolicies.pcf: line 16, column 22
    function initialValue_0 () : String {
      return null
    }
    
    // 'initialValue' attribute on Variable at ProducerPolicies.pcf: line 23, column 72
    function initialValue_1 () : gw.api.database.IQueryBeanResult<PolicyCommission> {
      return producer.getPolicyCommissionsWhereDefaultInAtLeastOneRole(policyStartsWith)
    }
    
    // 'initialValue' attribute on Variable at ProducerPolicies.pcf: line 27, column 86
    function initialValue_2 () : gw.api.database.IQueryBeanResult<PolicyCommissionArchiveSummary> {
      return producer.getArchivedPolicyCommissionsWhereDefaultInAtLeastOneRole(policyStartsWith)
    }
    
    // 'label' attribute on AlertBar (id=ProducerPolicies_TroubleTicketAlertAlertBar) at ProducerPolicies.pcf: line 59, column 53
    function label_15 () : java.lang.Object {
      return producer.AlertBarDisplayText
    }
    
    // Page (id=ProducerPolicies) at ProducerPolicies.pcf: line 10, column 68
    static function parent_165 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ProducerPolicies.pcf: line 292, column 35
    function sortValue_130 (archivedPolicyCommissionSummary :  entity.PolicyCommissionArchiveSummary) : java.lang.Object {
      return archivedPolicyCommissionSummary.PolicyPeriodArchiveSummary.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at ProducerPolicies.pcf: line 296, column 80
    function sortValue_131 (archivedPolicyCommissionSummary :  entity.PolicyCommissionArchiveSummary) : java.lang.Object {
      return archivedPolicyCommissionSummary.ProducerCode.Code
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at ProducerPolicies.pcf: line 301, column 53
    function sortValue_132 (archivedPolicyCommissionSummary :  entity.PolicyCommissionArchiveSummary) : java.lang.Object {
      return archivedPolicyCommissionSummary.Role
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PolicyCmsnExpense_Cell) at ProducerPolicies.pcf: line 307, column 80
    function sortValue_133 (archivedPolicyCommissionSummary :  entity.PolicyCommissionArchiveSummary) : java.lang.Object {
      return archivedPolicyCommissionSummary.PolicyCmsnExpense
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionEarnedRetained_Cell) at ProducerPolicies.pcf: line 313, column 87
    function sortValue_134 (archivedPolicyCommissionSummary :  entity.PolicyCommissionArchiveSummary) : java.lang.Object {
      return archivedPolicyCommissionSummary.ChargeCmsnEarnedRetained
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionWrittenOff_Cell) at ProducerPolicies.pcf: line 319, column 83
    function sortValue_135 (archivedPolicyCommissionSummary :  entity.PolicyCommissionArchiveSummary) : java.lang.Object {
      return archivedPolicyCommissionSummary.ChargeCmsnWrittenOff
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ProducerPolicies.pcf: line 88, column 35
    function sortValue_17 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.PolicyPeriod
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at ProducerPolicies.pcf: line 92, column 77
    function sortValue_18 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.PolicyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at ProducerPolicies.pcf: line 96, column 79
    function sortValue_19 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.PolicyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at ProducerPolicies.pcf: line 102, column 35
    function sortValue_20 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.PolicyPeriod.Policy.LOBCode
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at ProducerPolicies.pcf: line 114, column 35
    function sortValue_21 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.ProducerCode.Code
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at ProducerPolicies.pcf: line 119, column 53
    function sortValue_22 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.Role
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at ProducerPolicies.pcf: line 126, column 35
    function sortValue_23 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.PolicyPeriod.Policy.Account.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at ProducerPolicies.pcf: line 163, column 35
    function sortValue_54 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.PolicyPeriod
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at ProducerPolicies.pcf: line 167, column 77
    function sortValue_55 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.PolicyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at ProducerPolicies.pcf: line 171, column 79
    function sortValue_56 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.PolicyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at ProducerPolicies.pcf: line 177, column 35
    function sortValue_57 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.PolicyPeriod.Policy.LOBCode
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at ProducerPolicies.pcf: line 187, column 65
    function sortValue_58 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.ProducerCode.Code
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at ProducerPolicies.pcf: line 192, column 53
    function sortValue_59 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.Role
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at ProducerPolicies.pcf: line 198, column 35
    function sortValue_60 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.PolicyPeriod.Policy.Account.AccountNumber
    }
    
    // '$$sumValue' attribute on RowIterator at ProducerPolicies.pcf: line 307, column 80
    function sumValueRoot_137 (archivedPolicyCommissionSummary :  entity.PolicyCommissionArchiveSummary) : java.lang.Object {
      return archivedPolicyCommissionSummary
    }
    
    // '$$sumValue' attribute on RowIterator at ProducerPolicies.pcf: line 213, column 72
    function sumValueRoot_62 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerPolicies.pcf: line 307, column 80
    function sumValue_136 (archivedPolicyCommissionSummary :  entity.PolicyCommissionArchiveSummary) : java.lang.Object {
      return archivedPolicyCommissionSummary.PolicyCmsnExpense
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerPolicies.pcf: line 313, column 87
    function sumValue_138 (archivedPolicyCommissionSummary :  entity.PolicyCommissionArchiveSummary) : java.lang.Object {
      return archivedPolicyCommissionSummary.ChargeCmsnEarnedRetained
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerPolicies.pcf: line 319, column 83
    function sumValue_140 (archivedPolicyCommissionSummary :  entity.PolicyCommissionArchiveSummary) : java.lang.Object {
      return archivedPolicyCommissionSummary.ChargeCmsnWrittenOff
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerPolicies.pcf: line 213, column 72
    function sumValue_61 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.CommissionExpenseBalance
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerPolicies.pcf: line 221, column 72
    function sumValue_63 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.CommissionReserveBalance
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerPolicies.pcf: line 229, column 72
    function sumValue_65 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.CommissionEarnedRetained
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerPolicies.pcf: line 237, column 68
    function sumValue_67 (policyCommission :  entity.PolicyCommission) : java.lang.Object {
      return policyCommission.CommissionWrittenOff
    }
    
    // 'value' attribute on RowIterator at ProducerPolicies.pcf: line 277, column 107
    function value_162 () : gw.api.database.IQueryBeanResult<entity.PolicyCommissionArchiveSummary> {
      return archivedPolicyCommissions
    }
    
    // 'value' attribute on RowIterator at ProducerPolicies.pcf: line 73, column 93
    function value_52 () : gw.api.database.IQueryBeanResult<entity.PolicyCommission> {
      return policyCommissions
    }
    
    // 'value' attribute on TextValue (id=PolicyPeriodFilter) at ProducerPolicies.pcf: line 43, column 41
    function value_6 () : java.lang.String {
      return policyStartsWith
    }
    
    // 'visible' attribute on AlertBar (id=ProducerPolicies_TroubleTicketAlertAlertBar) at ProducerPolicies.pcf: line 59, column 53
    function visible_12 () : java.lang.Boolean {
      return producer.HasActiveTroubleTickets
    }
    
    // 'visible' attribute on Card (id=ArchiveSummaryCard) at ProducerPolicies.pcf: line 268, column 75
    function visible_163 () : java.lang.Boolean {
      return gw.api.system.PLConfigParameters.ArchiveEnabled.Value
    }
    
    override property get CurrentLocation () : pcf.ProducerPolicies {
      return super.CurrentLocation as pcf.ProducerPolicies
    }
    
    property get archivedPolicyCommissions () : gw.api.database.IQueryBeanResult<PolicyCommissionArchiveSummary> {
      return getVariableValue("archivedPolicyCommissions", 0) as gw.api.database.IQueryBeanResult<PolicyCommissionArchiveSummary>
    }
    
    property set archivedPolicyCommissions ($arg :  gw.api.database.IQueryBeanResult<PolicyCommissionArchiveSummary>) {
      setVariableValue("archivedPolicyCommissions", 0, $arg)
    }
    
    property get policyCommissions () : gw.api.database.IQueryBeanResult<PolicyCommission> {
      return getVariableValue("policyCommissions", 0) as gw.api.database.IQueryBeanResult<PolicyCommission>
    }
    
    property set policyCommissions ($arg :  gw.api.database.IQueryBeanResult<PolicyCommission>) {
      setVariableValue("policyCommissions", 0, $arg)
    }
    
    property get policyStartsWith () : String {
      return getVariableValue("policyStartsWith", 0) as String
    }
    
    property set policyStartsWith ($arg :  String) {
      setVariableValue("policyStartsWith", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}