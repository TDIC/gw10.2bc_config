package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/ChangeBillingMethodToDirectBillPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChangeBillingMethodToDirectBillPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/ChangeBillingMethodToDirectBillPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChangeBillingMethodToDirectBillPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=ChangeBillingMethodToDirectBillPopup) at ChangeBillingMethodToDirectBillPopup.pcf: line 10, column 77
    function beforeCommit_15 (pickedValue :  java.lang.Object) : void {
      policyPeriod.changeBillingMethodToDirectBill(createInvoiceForToday, reversePayments);gw.acc.acct360.accountview.AccountBalanceTxnUtil.createABToDBTransferRecord(policyPeriod)
    }
    
    // 'value' attribute on BooleanRadioInput (id=CreateInvoiceForToday_Input) at ChangeBillingMethodToDirectBillPopup.pcf: line 54, column 59
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      createInvoiceForToday = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=reversePayments_Input) at ChangeBillingMethodToDirectBillPopup.pcf: line 62, column 63
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      reversePayments = (__VALUE_TO_SET as gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem)
    }
    
    // 'initialValue' attribute on Variable at ChangeBillingMethodToDirectBillPopup.pcf: line 23, column 74
    function initialValue_0 () : gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem {
      return gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem.No
    }
    
    // 'valueRange' attribute on RangeInput (id=reversePayments_Input) at ChangeBillingMethodToDirectBillPopup.pcf: line 62, column 63
    function valueRange_10 () : java.lang.Object {
      return gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem.values()
    }
    
    // 'value' attribute on BooleanRadioInput (id=CreateInvoiceForToday_Input) at ChangeBillingMethodToDirectBillPopup.pcf: line 54, column 59
    function value_3 () : java.lang.Boolean {
      return createInvoiceForToday
    }
    
    // 'value' attribute on RangeInput (id=reversePayments_Input) at ChangeBillingMethodToDirectBillPopup.pcf: line 62, column 63
    function value_8 () : gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem {
      return reversePayments
    }
    
    // 'valueRange' attribute on RangeInput (id=reversePayments_Input) at ChangeBillingMethodToDirectBillPopup.pcf: line 62, column 63
    function verifyValueRangeIsAllowedType_11 ($$arg :  gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=reversePayments_Input) at ChangeBillingMethodToDirectBillPopup.pcf: line 62, column 63
    function verifyValueRangeIsAllowedType_11 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=reversePayments_Input) at ChangeBillingMethodToDirectBillPopup.pcf: line 62, column 63
    function verifyValueRange_12 () : void {
      var __valueRangeArg = gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem.values()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_11(__valueRangeArg)
    }
    
    // 'visible' attribute on AlertBar (id=ChangeCommissionReservedAlert) at ChangeBillingMethodToDirectBillPopup.pcf: line 38, column 48
    function visible_1 () : java.lang.Boolean {
      return policyPeriod.isAgencyBill()
    }
    
    // 'visible' attribute on BooleanRadioInput (id=CreateInvoiceForToday_Input) at ChangeBillingMethodToDirectBillPopup.pcf: line 54, column 59
    function visible_2 () : java.lang.Boolean {
      return policyPeriod.hasPastInvoiceItems()
    }
    
    // 'visible' attribute on RangeInput (id=reversePayments_Input) at ChangeBillingMethodToDirectBillPopup.pcf: line 62, column 63
    function visible_7 () : java.lang.Boolean {
      return policyPeriod.isPaymentHasBeenApplied()
    }
    
    override property get CurrentLocation () : pcf.ChangeBillingMethodToDirectBillPopup {
      return super.CurrentLocation as pcf.ChangeBillingMethodToDirectBillPopup
    }
    
    property get createInvoiceForToday () : boolean {
      return getVariableValue("createInvoiceForToday", 0) as java.lang.Boolean
    }
    
    property set createInvoiceForToday ($arg :  boolean) {
      setVariableValue("createInvoiceForToday", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get reversePayments () : gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem {
      return getVariableValue("reversePayments", 0) as gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem
    }
    
    property set reversePayments ($arg :  gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem) {
      setVariableValue("reversePayments", 0, $arg)
    }
    
    
  }
  
  
}