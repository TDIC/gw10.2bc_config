package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/activityenhancement/desktop/DesktopQueues.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopQueuesExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/activityenhancement/desktop/DesktopQueues.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopQueuesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=NextItemButton) at DesktopQueues.pcf: line 51, column 55
    function action_13 () : void {
      selectedQueue.assignNextQueuedActivityToMe_Ext()
    }
    
    // 'action' attribute on ToolbarButton (id=NextItemButton) at DesktopQueues.pcf: line 87, column 55
    function action_29 () : void {
      selectedQueue.assignNextQueuedTroubleTicketToMe_Ext()
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=DesktopActivities_AssignButton) at DesktopQueues.pcf: line 45, column 55
    function allCheckedRowsAction_10 (CheckedValues :  entity.Activity[], CheckedValuesErrors :  java.util.Map) : void {
      AssignActivitiesPopup.push(new gw.api.web.activity.ActivityAssignmentPopup(CheckedValues),CheckedValues)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=assigneSelectedBtn) at DesktopQueues.pcf: line 74, column 55
    function allCheckedRowsAction_24 (CheckedValues :  entity.TroubleTicket[], CheckedValuesErrors :  java.util.Map) : void {
      gw.acc.activityenhancement.DesktopQueuedTroubleTicketsUtil.assignTroubleTicketsToMe(CheckedValues)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=DesktopActivities_AssignButton) at DesktopQueues.pcf: line 81, column 55
    function allCheckedRowsAction_26 (CheckedValues :  entity.TroubleTicket[], CheckedValuesErrors :  java.util.Map) : void {
      AssignTroubleTicketsPopup.push(new gw.api.web.troubleticket.TroubleTicketAssignmentPopup(CheckedValues), CheckedValues)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=assigneSelectedBtn) at DesktopQueues.pcf: line 38, column 55
    function allCheckedRowsAction_8 (CheckedValues :  entity.Activity[], CheckedValuesErrors :  java.util.Map) : void {
      gw.acc.activityenhancement.DesktopQueuedActivitiesUtil.assignActivitiesToMe(CheckedValues)
    }
    
    // 'available' attribute on ToolbarButton (id=NextItemButton) at DesktopQueues.pcf: line 51, column 55
    function available_11 () : java.lang.Boolean {
      return selectedQueue.QueueContainsActivities_Ext
    }
    
    // 'available' attribute on ToolbarButton (id=NextItemButton) at DesktopQueues.pcf: line 87, column 55
    function available_27 () : java.lang.Boolean {
      return selectedQueue.QueueContainsTroubleTickets_Ext
    }
    
    // 'def' attribute on PanelRef (id=ActivityLVRef) at DesktopQueues.pcf: line 22, column 32
    function def_onEnter_14 (def :  pcf.ActivityLV) : void {
      def.onEnter(selectedQueue.Activities_Ext)
    }
    
    // 'def' attribute on PanelRef (id=ActivityLVRef) at DesktopQueues.pcf: line 22, column 32
    function def_refreshVariables_15 (def :  pcf.ActivityLV) : void {
      def.refreshVariables(selectedQueue.Activities_Ext)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=AvailableQueues_Input) at DesktopQueues.pcf: line 29, column 45
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectedQueue = (__VALUE_TO_SET as AssignableQueue)
    }
    
    // 'initialValue' attribute on Variable at DesktopQueues.pcf: line 12, column 31
    function initialValue_0 () : AssignableQueue {
      return AssignableQueue.finder.findVisibleQueuesForUser(User.util.CurrentUser).FirstResult as AssignableQueue
    }
    
    // Page (id=DesktopQueues) at DesktopQueues.pcf: line 8, column 71
    static function parent_73 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at DesktopQueues.pcf: line 104, column 54
    function sortValue_30 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.Escalated
    }
    
    // 'value' attribute on TextCell (id=TypeExt_Cell) at DesktopQueues.pcf: line 116, column 41
    function sortValue_31 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.TicketRelatedEntities.first().IntrinsicType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=NameExt_Cell) at DesktopQueues.pcf: line 120, column 86
    function sortValue_32 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.TicketRelatedEntities.first().DisplayName
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at DesktopQueues.pcf: line 125, column 55
    function sortValue_33 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.TargetDate
    }
    
    // 'value' attribute on DateCell (id=CreateTime_Cell) at DesktopQueues.pcf: line 129, column 55
    function sortValue_34 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.CreateTime
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at DesktopQueues.pcf: line 134, column 51
    function sortValue_35 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.Priority
    }
    
    // 'sortBy' attribute on TextCell (id=Status_Cell) at DesktopQueues.pcf: line 139, column 57
    function sortValue_36 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.CloseDate
    }
    
    // 'value' attribute on TextCell (id=Title_Cell) at DesktopQueues.pcf: line 143, column 50
    function sortValue_37 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.Title
    }
    
    // 'title' attribute on Card (id=Activities) at DesktopQueues.pcf: line 18, column 171
    function title_16 () : java.lang.String {
      return DisplayKey.get("Accelerator.Activity.activities", gw.acc.activityenhancement.DesktopQueuedActivitiesUtil.getTotalNumberOfActivitiesForUser())
    }
    
    // 'title' attribute on Card (id=TroubleTickets) at DesktopQueues.pcf: line 57, column 183
    function title_72 () : java.lang.String {
      return DisplayKey.get("Accelerator.Activity.TroubleTickets", gw.acc.activityenhancement.DesktopQueuedTroubleTicketsUtil.getTotalNumberOfTroubleTicketsForUser())
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AvailableQueues_Input) at DesktopQueues.pcf: line 29, column 45
    function valueRange_3 () : java.lang.Object {
      return gw.acc.activityenhancement.DesktopQueuedActivitiesUtil.getQueues()
    }
    
    // 'value' attribute on ToolbarRangeInput (id=AvailableQueues_Input) at DesktopQueues.pcf: line 29, column 45
    function value_1 () : AssignableQueue {
      return selectedQueue
    }
    
    // 'value' attribute on RowIterator at DesktopQueues.pcf: line 97, column 72
    function value_71 () : java.util.List<entity.TroubleTicket> {
      return selectedQueue.TroubleTickets_Ext
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AvailableQueues_Input) at DesktopQueues.pcf: line 65, column 45
    function verifyValueRangeIsAllowedType_20 ($$arg :  AssignableQueue[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AvailableQueues_Input) at DesktopQueues.pcf: line 65, column 45
    function verifyValueRangeIsAllowedType_20 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AvailableQueues_Input) at DesktopQueues.pcf: line 65, column 45
    function verifyValueRangeIsAllowedType_20 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AvailableQueues_Input) at DesktopQueues.pcf: line 29, column 45
    function verifyValueRangeIsAllowedType_4 ($$arg :  AssignableQueue[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AvailableQueues_Input) at DesktopQueues.pcf: line 29, column 45
    function verifyValueRangeIsAllowedType_4 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AvailableQueues_Input) at DesktopQueues.pcf: line 29, column 45
    function verifyValueRangeIsAllowedType_4 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AvailableQueues_Input) at DesktopQueues.pcf: line 65, column 45
    function verifyValueRange_21 () : void {
      var __valueRangeArg = gw.acc.activityenhancement.DesktopQueuedActivitiesUtil.getQueues()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_20(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AvailableQueues_Input) at DesktopQueues.pcf: line 29, column 45
    function verifyValueRange_5 () : void {
      var __valueRangeArg = gw.acc.activityenhancement.DesktopQueuedActivitiesUtil.getQueues()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_4(__valueRangeArg)
    }
    
    // 'visible' attribute on ToolbarButton (id=NextItemButton) at DesktopQueues.pcf: line 51, column 55
    function visible_12 () : java.lang.Boolean {
      return perm.System.actquenext_Ext
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=assigneSelectedBtn) at DesktopQueues.pcf: line 38, column 55
    function visible_7 () : java.lang.Boolean {
      return perm.System.actquepick_Ext
    }
    
    override property get CurrentLocation () : pcf.DesktopQueues {
      return super.CurrentLocation as pcf.DesktopQueues
    }
    
    property get selectedQueue () : AssignableQueue {
      return getVariableValue("selectedQueue", 0) as AssignableQueue
    }
    
    property set selectedQueue ($arg :  AssignableQueue) {
      setVariableValue("selectedQueue", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/activityenhancement/desktop/DesktopQueues.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DesktopQueuesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Number_Cell) at DesktopQueues.pcf: line 111, column 64
    function action_41 () : void {
      TroubleTicketDetailsPopup.push(troubleTicket)
    }
    
    // 'action' attribute on TextCell (id=Number_Cell) at DesktopQueues.pcf: line 111, column 64
    function action_dest_42 () : pcf.api.Destination {
      return pcf.TroubleTicketDetailsPopup.createDestination(troubleTicket)
    }
    
    // 'checkBoxVisible' attribute on RowIterator at DesktopQueues.pcf: line 97, column 72
    function checkBoxVisible_70 () : java.lang.Boolean {
      return perm.System.actquepick_Ext
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at DesktopQueues.pcf: line 125, column 55
    function fontColor_52 () : java.lang.Object {
      return troubleTicket.Overdue == true ? "Red" : ""
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at DesktopQueues.pcf: line 104, column 54
    function valueRoot_39 () : java.lang.Object {
      return troubleTicket
    }
    
    // 'value' attribute on TextCell (id=TypeExt_Cell) at DesktopQueues.pcf: line 116, column 41
    function valueRoot_47 () : java.lang.Object {
      return troubleTicket.TicketRelatedEntities.first().IntrinsicType
    }
    
    // 'value' attribute on TextCell (id=NameExt_Cell) at DesktopQueues.pcf: line 120, column 86
    function valueRoot_50 () : java.lang.Object {
      return troubleTicket.TicketRelatedEntities.first()
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at DesktopQueues.pcf: line 104, column 54
    function value_38 () : java.lang.Boolean {
      return troubleTicket.Escalated
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at DesktopQueues.pcf: line 111, column 64
    function value_43 () : java.lang.String {
      return troubleTicket.TroubleTicketNumber
    }
    
    // 'value' attribute on TextCell (id=TypeExt_Cell) at DesktopQueues.pcf: line 116, column 41
    function value_46 () : String {
      return troubleTicket.TicketRelatedEntities.first().IntrinsicType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=NameExt_Cell) at DesktopQueues.pcf: line 120, column 86
    function value_49 () : java.lang.String {
      return troubleTicket.TicketRelatedEntities.first().DisplayName
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at DesktopQueues.pcf: line 125, column 55
    function value_53 () : java.util.Date {
      return troubleTicket.TargetDate
    }
    
    // 'value' attribute on DateCell (id=CreateTime_Cell) at DesktopQueues.pcf: line 129, column 55
    function value_58 () : java.util.Date {
      return troubleTicket.CreateTime
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at DesktopQueues.pcf: line 134, column 51
    function value_61 () : typekey.Priority {
      return troubleTicket.Priority
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at DesktopQueues.pcf: line 139, column 57
    function value_64 () : java.lang.String {
      return troubleTicket.TicketStatus
    }
    
    // 'value' attribute on TextCell (id=Title_Cell) at DesktopQueues.pcf: line 143, column 50
    function value_67 () : java.lang.String {
      return troubleTicket.Title
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at DesktopQueues.pcf: line 125, column 55
    function verifyFontColorIsAllowedType_55 ($$arg :  gw.api.web.color.GWColor) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at DesktopQueues.pcf: line 125, column 55
    function verifyFontColorIsAllowedType_55 ($$arg :  java.lang.String) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at DesktopQueues.pcf: line 125, column 55
    function verifyFontColor_56 () : void {
      var __fontColorArg = troubleTicket.Overdue == true ? "Red" : ""
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the fontColor return type is not a valid type for use with a ValueWidget's fontColor
      // It needs to return a String or a GWColor.
      verifyFontColorIsAllowedType_55(__fontColorArg)
    }
    
    property get troubleTicket () : entity.TroubleTicket {
      return getIteratedValue(1) as entity.TroubleTicket
    }
    
    
  }
  
  
}