package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillSelectPaymentForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillSelectPaymentForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillSelectPaymentForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillSelectPaymentForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer, agencyMoneyReceived :  AgencyBillMoneyRcvd) : int {
      return 0
    }
    
    // 'action' attribute on ForwardCondition at AgencyBillSelectPaymentForward.pcf: line 16, column 78
    function action_0 () : void {
      AgencyBillExecutedPayments.go(producer, agencyMoneyReceived)
    }
    
    // 'action' attribute on ForwardCondition at AgencyBillSelectPaymentForward.pcf: line 16, column 78
    function action_dest_1 () : pcf.api.Destination {
      return pcf.AgencyBillExecutedPayments.createDestination(producer, agencyMoneyReceived)
    }
    
    override property get CurrentLocation () : pcf.AgencyBillSelectPaymentForward {
      return super.CurrentLocation as pcf.AgencyBillSelectPaymentForward
    }
    
    property get agencyMoneyReceived () : AgencyBillMoneyRcvd {
      return getVariableValue("agencyMoneyReceived", 0) as AgencyBillMoneyRcvd
    }
    
    property set agencyMoneyReceived ($arg :  AgencyBillMoneyRcvd) {
      setVariableValue("agencyMoneyReceived", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}