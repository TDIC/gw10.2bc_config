package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailCommissions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailCommissionsExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailCommissions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends TransactionsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=TransactionsLV_TransactionNumber_Cell) at PolicyDetailCommissions.pcf: line 84, column 31
    function action_19 () : void {
      TransactionDetailPopup.push(producerTransaction)
    }
    
    // 'action' attribute on TextCell (id=TransactionsLV_Producer_Cell) at PolicyDetailCommissions.pcf: line 106, column 31
    function action_30 () : void {
      ProducerDetailPopup.push(producerTransaction.Producer)
    }
    
    // 'action' attribute on TextCell (id=TransactionsLV_TransactionNumber_Cell) at PolicyDetailCommissions.pcf: line 84, column 31
    function action_dest_20 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(producerTransaction)
    }
    
    // 'action' attribute on TextCell (id=TransactionsLV_Producer_Cell) at PolicyDetailCommissions.pcf: line 106, column 31
    function action_dest_31 () : pcf.api.Destination {
      return pcf.ProducerDetailPopup.createDestination(producerTransaction.Producer)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=TransactionsLV_Basis_Cell) at PolicyDetailCommissions.pcf: line 120, column 52
    function currency_40 () : typekey.Currency {
      return producerTransaction.Currency
    }
    
    // 'outputConversion' attribute on TextCell (id=TransactionsLV_Rate_Cell) at PolicyDetailCommissions.pcf: line 127, column 47
    function outputConversion_42 (VALUE :  java.lang.Number) : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(VALUE, 2)
    }
    
    // 'value' attribute on DateCell (id=TransactionsLV_Date_Cell) at PolicyDetailCommissions.pcf: line 77, column 62
    function valueRoot_17 () : java.lang.Object {
      return producerTransaction
    }
    
    // 'value' attribute on TextCell (id=TransactionsLV_Charge_Cell) at PolicyDetailCommissions.pcf: line 97, column 44
    function valueRoot_28 () : java.lang.Object {
      return producerTransaction.ChargeCommission
    }
    
    // 'value' attribute on TextCell (id=TransactionsLV_ProducerCode_Cell) at PolicyDetailCommissions.pcf: line 113, column 31
    function valueRoot_36 () : java.lang.Object {
      return producerTransaction.ProducerCode
    }
    
    // 'value' attribute on DateCell (id=TransactionsLV_Date_Cell) at PolicyDetailCommissions.pcf: line 77, column 62
    function value_16 () : java.util.Date {
      return producerTransaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=TransactionsLV_TransactionNumber_Cell) at PolicyDetailCommissions.pcf: line 84, column 31
    function value_21 () : java.lang.String {
      return producerTransaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=TransactionsLV_Type_Cell) at PolicyDetailCommissions.pcf: line 91, column 31
    function value_24 () : java.lang.String {
      return producerTransaction.ShortDisplayName
    }
    
    // 'value' attribute on TextCell (id=TransactionsLV_Charge_Cell) at PolicyDetailCommissions.pcf: line 97, column 44
    function value_27 () : entity.Charge {
      return producerTransaction.ChargeCommission.Charge
    }
    
    // 'value' attribute on TextCell (id=TransactionsLV_Producer_Cell) at PolicyDetailCommissions.pcf: line 106, column 31
    function value_32 () : entity.Producer {
      return producerTransaction.Producer
    }
    
    // 'value' attribute on TextCell (id=TransactionsLV_ProducerCode_Cell) at PolicyDetailCommissions.pcf: line 113, column 31
    function value_35 () : java.lang.String {
      return producerTransaction.ProducerCode.Code
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TransactionsLV_Basis_Cell) at PolicyDetailCommissions.pcf: line 120, column 52
    function value_38 () : gw.pl.currency.MonetaryAmount {
      return producerTransaction.Basis
    }
    
    // 'value' attribute on TextCell (id=TransactionsLV_Rate_Cell) at PolicyDetailCommissions.pcf: line 127, column 47
    function value_43 () : java.lang.Number {
      return producerTransaction.Rate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TransactionsLV_Amount_Cell) at PolicyDetailCommissions.pcf: line 136, column 63
    function value_46 () : gw.pl.currency.MonetaryAmount {
      return producerTransaction.CommissionAmount
    }
    
    property get producerTransaction () : entity.ProducerTransaction {
      return getIteratedValue(2) as entity.ProducerTransaction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailCommissions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PolicyDetailCommissionsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at PolicyDetailCommissions.pcf: line 39, column 92
    function def_onEnter_6 (def :  pcf.PolicyDetailCommissionsProducerInfoInputSet) : void {
      def.onEnter(plcyPeriod, policyRole)
    }
    
    // 'def' attribute on InputSetRef at PolicyDetailCommissions.pcf: line 39, column 92
    function def_refreshVariables_7 (def :  pcf.PolicyDetailCommissionsProducerInfoInputSet) : void {
      def.refreshVariables(plcyPeriod, policyRole)
    }
    
    property get policyRole () : typekey.PolicyRole {
      return getIteratedValue(1) as typekey.PolicyRole
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailCommissions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailCommissionsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=TransferPolicy) at PolicyDetailCommissions.pcf: line 23, column 98
    function action_1 () : void {
      TransferPolicyWizard.push(plcyPeriod)
    }
    
    // 'action' attribute on ToolbarButton (id=OverridePlan) at PolicyDetailCommissions.pcf: line 28, column 96
    function action_4 () : void {
      OverrideCommissionPlanPopup.push(plcyPeriod)
    }
    
    // 'action' attribute on ToolbarButton (id=TransferPolicy) at PolicyDetailCommissions.pcf: line 23, column 98
    function action_dest_2 () : pcf.api.Destination {
      return pcf.TransferPolicyWizard.createDestination(plcyPeriod)
    }
    
    // 'action' attribute on ToolbarButton (id=OverridePlan) at PolicyDetailCommissions.pcf: line 28, column 96
    function action_dest_5 () : pcf.api.Destination {
      return pcf.OverrideCommissionPlanPopup.createDestination(plcyPeriod)
    }
    
    // 'available' attribute on ToolbarButton (id=TransferPolicy) at PolicyDetailCommissions.pcf: line 23, column 98
    function available_0 () : java.lang.Boolean {
      return perm.PolicyPeriod.plcyprodtx
    }
    
    // 'available' attribute on ToolbarButton (id=OverridePlan) at PolicyDetailCommissions.pcf: line 28, column 96
    function available_3 () : java.lang.Boolean {
      return plcyPeriod.OverrideCommissionPlanSettings.Settings.length > 0 and perm.Producer.commplanoride and !plcyPeriod.hasFrozenInvoiceItems()
    }
    
    // 'canVisit' attribute on Page (id=PolicyDetailCommissions) at PolicyDetailCommissions.pcf: line 9, column 75
    static function canVisit_52 (plcyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcycommview and not plcyPeriod.Archived
    }
    
    // Page (id=PolicyDetailCommissions) at PolicyDetailCommissions.pcf: line 9, column 75
    static function parent_53 (plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyGroup.createDestination(plcyPeriod)
    }
    
    // 'value' attribute on InputIterator at PolicyDetailCommissions.pcf: line 37, column 48
    function value_8 () : typekey.PolicyRole[] {
      return typekey.PolicyRole.getTypeKeys(false).toTypedArray()
    }
    
    override property get CurrentLocation () : pcf.PolicyDetailCommissions {
      return super.CurrentLocation as pcf.PolicyDetailCommissions
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailCommissions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransactionsLVExpressionsImpl extends PolicyDetailCommissionsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at PolicyDetailCommissions.pcf: line 69, column 115
    function filters_11 () : gw.api.filters.IFilter[] {
      return new gw.api.web.policy.PolicyCommissionsProducerCodeFilterSet(plcyPeriod).FilterOptions
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailCommissions.pcf: line 58, column 83
    function initialValue_10 () : gw.api.database.IQueryBeanResult<CommissionsReserveTxn> {
      return commissionTransactions.Query
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailCommissions.pcf: line 54, column 108
    function initialValue_9 () : gw.api.domain.commission.ProducerTransactionQueryWithRate<CommissionsReserveTxn> {
      return plcyPeriod.findCommissionsReserveTxns()
    }
    
    // 'value' attribute on DateCell (id=TransactionsLV_Date_Cell) at PolicyDetailCommissions.pcf: line 77, column 62
    function sortValue_12 (producerTransaction :  entity.ProducerTransaction) : java.lang.Object {
      return producerTransaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=TransactionsLV_TransactionNumber_Cell) at PolicyDetailCommissions.pcf: line 84, column 31
    function sortValue_13 (producerTransaction :  entity.ProducerTransaction) : java.lang.Object {
      return producerTransaction.TransactionNumber
    }
    
    // '$$sumValue' attribute on RowIterator at PolicyDetailCommissions.pcf: line 136, column 63
    function sumValueRoot_15 (producerTransaction :  entity.ProducerTransaction) : java.lang.Object {
      return producerTransaction
    }
    
    // 'footerSumValue' attribute on RowIterator at PolicyDetailCommissions.pcf: line 136, column 63
    function sumValue_14 (producerTransaction :  entity.ProducerTransaction) : java.lang.Object {
      return producerTransaction.CommissionAmount
    }
    
    // 'value' attribute on RowIterator at PolicyDetailCommissions.pcf: line 64, column 94
    function value_50 () : gw.api.database.IQueryBeanResult<entity.CommissionsReserveTxn> {
      return commissionTransactionQuery
    }
    
    // 'type' attribute on RowIterator at PolicyDetailCommissions.pcf: line 64, column 94
    function verifyIteratorType_51 () : void {
      var entry : entity.CommissionsReserveTxn = null
      var typedEntry : entity.ProducerTransaction
      // If this cast fails to compile then the type specified by the 'type' attribute on
      // 'RowIterator' is not compatible with the member type of the 'valueType' attribute
      typedEntry = entry as entity.ProducerTransaction
    }
    
    property get commissionTransactionQuery () : gw.api.database.IQueryBeanResult<CommissionsReserveTxn> {
      return getVariableValue("commissionTransactionQuery", 1) as gw.api.database.IQueryBeanResult<CommissionsReserveTxn>
    }
    
    property set commissionTransactionQuery ($arg :  gw.api.database.IQueryBeanResult<CommissionsReserveTxn>) {
      setVariableValue("commissionTransactionQuery", 1, $arg)
    }
    
    property get commissionTransactions () : gw.api.domain.commission.ProducerTransactionQueryWithRate<CommissionsReserveTxn> {
      return getVariableValue("commissionTransactions", 1) as gw.api.domain.commission.ProducerTransactionQueryWithRate<CommissionsReserveTxn>
    }
    
    property set commissionTransactions ($arg :  gw.api.domain.commission.ProducerTransactionQueryWithRate<CommissionsReserveTxn>) {
      setVariableValue("commissionTransactions", 1, $arg)
    }
    
    
  }
  
  
}