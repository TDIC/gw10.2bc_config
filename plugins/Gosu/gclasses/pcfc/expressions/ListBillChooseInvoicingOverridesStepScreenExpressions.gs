package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/ListBillChooseInvoicingOverridesStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ListBillChooseInvoicingOverridesStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/ListBillChooseInvoicingOverridesStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListBillChooseInvoicingOverridesStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerInput (id=ListBillPayer_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 27, column 38
    function action_1 () : void {
      AccountSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=ListBillPayer_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 27, column 38
    function action_dest_2 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'value' attribute on RangeInput (id=PaymentPlan_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 38, column 42
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      listBillHelper.PaymentPlan = (__VALUE_TO_SET as entity.PaymentPlan)
    }
    
    // 'value' attribute on RangeInput (id=InvoiceStream_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 49, column 45
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      listBillHelper.InvoiceStream = (__VALUE_TO_SET as entity.InvoiceStream)
    }
    
    // 'value' attribute on PickerInput (id=ListBillPayer_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 27, column 38
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      listBillHelper.ListBillAccount = (__VALUE_TO_SET as entity.Account)
    }
    
    // 'inputConversion' attribute on PickerInput (id=ListBillPayer_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 27, column 38
    function inputConversion_4 (VALUE :  java.lang.String) : java.lang.Object {
      return (VALUE==null or VALUE =="") ?  null : gw.api.database.Query.make(Account).compare("AccountName", Equals, VALUE).select().AtMostOneRow 
    }
    
    // 'onChange' attribute on PostOnChange at ListBillChooseInvoicingOverridesStepScreen.pcf: line 29, column 63
    function onChange_0 () : void {
      clearOverridesOnListBillPayerChange()
    }
    
    // 'onChange' attribute on PostOnChange at ListBillChooseInvoicingOverridesStepScreen.pcf: line 40, column 233
    function onChange_10 () : void {
      if (!gw.api.domain.invoice.InvoiceStreams.getCompatibleInvoiceStreams(listBillHelper.ListBillAccount, listBillHelper.PaymentPlan).contains(listBillHelper.InvoiceStream)) {listBillHelper.InvoiceStream = null}
    }
    
    // 'validationExpression' attribute on PickerInput (id=ListBillPayer_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 27, column 38
    function validationExpression_3 () : java.lang.Object {
      return gw.api.web.account.PolicyPeriods.checkListBillPolicyPeriodForOverridingPayerError(policyPeriod, listBillHelper.ListBillAccount, listBillHelper.PaymentPlan)
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 38, column 42
    function valueRange_14 () : java.lang.Object {
      return listBillHelper.ListBillAccount.PaymentPlans
    }
    
    // 'valueRange' attribute on RangeInput (id=InvoiceStream_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 49, column 45
    function valueRange_21 () : java.lang.Object {
      return gw.api.domain.invoice.InvoiceStreams.getCompatibleInvoiceStreams(listBillHelper.ListBillAccount, listBillHelper.PaymentPlan)
    }
    
    // 'value' attribute on PickerInput (id=ListBillPayer_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 27, column 38
    function valueRoot_7 () : java.lang.Object {
      return listBillHelper
    }
    
    // 'value' attribute on RangeInput (id=PaymentPlan_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 38, column 42
    function value_11 () : entity.PaymentPlan {
      return listBillHelper.PaymentPlan
    }
    
    // 'value' attribute on RangeInput (id=InvoiceStream_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 49, column 45
    function value_18 () : entity.InvoiceStream {
      return listBillHelper.InvoiceStream
    }
    
    // 'value' attribute on PickerInput (id=ListBillPayer_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 27, column 38
    function value_5 () : entity.Account {
      return listBillHelper.ListBillAccount
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 38, column 42
    function verifyValueRangeIsAllowedType_15 ($$arg :  entity.PaymentPlan[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 38, column 42
    function verifyValueRangeIsAllowedType_15 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentPlan>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 38, column 42
    function verifyValueRangeIsAllowedType_15 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=InvoiceStream_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 49, column 45
    function verifyValueRangeIsAllowedType_22 ($$arg :  entity.InvoiceStream[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=InvoiceStream_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 49, column 45
    function verifyValueRangeIsAllowedType_22 ($$arg :  gw.api.database.IQueryBeanResult<entity.InvoiceStream>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=InvoiceStream_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 49, column 45
    function verifyValueRangeIsAllowedType_22 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentPlan_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 38, column 42
    function verifyValueRange_16 () : void {
      var __valueRangeArg = listBillHelper.ListBillAccount.PaymentPlans
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_15(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=InvoiceStream_Input) at ListBillChooseInvoicingOverridesStepScreen.pcf: line 49, column 45
    function verifyValueRange_23 () : void {
      var __valueRangeArg = gw.api.domain.invoice.InvoiceStreams.getCompatibleInvoiceStreams(listBillHelper.ListBillAccount, listBillHelper.PaymentPlan)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_22(__valueRangeArg)
    }
    
    property get listBillHelper () : gw.api.web.policy.ListBillInvoicingOverridesHelper {
      return getRequireValue("listBillHelper", 0) as gw.api.web.policy.ListBillInvoicingOverridesHelper
    }
    
    property set listBillHelper ($arg :  gw.api.web.policy.ListBillInvoicingOverridesHelper) {
      setRequireValue("listBillHelper", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    function clearOverridesOnListBillPayerChange() {
      if(listBillHelper.ListBillAccount != null and !listBillHelper.ListBillAccount.PaymentPlans.contains(listBillHelper.PaymentPlan)){
         listBillHelper.PaymentPlan = null
      }
       listBillHelper.InvoiceStream = null
    }
    
    
  }
  
  
}