package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/entrypoints/PolicyPeriodEntry.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyPeriodEntryEntryPointExpressions {
  @javax.annotation.Generated("config/web/pcf/entrypoints/PolicyPeriodEntry.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyPeriodEntryExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on EntryPoint (id=PolicyPeriodEntry) at PolicyPeriodEntry.pcf: line 7, column 62
    function location_0 () : pcf.api.Destination {
      return pcf.PolicyPeriodForward.createDestination(policyNumber, termNumber)
    }
    
    property get policyNumber () : String {
      return getVariableValue("policyNumber", 0) as String
    }
    
    property set policyNumber ($arg :  String) {
      setVariableValue("policyNumber", 0, $arg)
    }
    
    property get termNumber () : int {
      return getVariableValue("termNumber", 0) as java.lang.Integer
    }
    
    property set termNumber ($arg :  int) {
      setVariableValue("termNumber", 0, $arg)
    }
    
    
  }
  
  
}