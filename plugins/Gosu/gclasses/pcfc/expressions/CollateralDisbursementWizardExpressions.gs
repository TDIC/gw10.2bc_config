package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralDisbursementWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CollateralDisbursementWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/collateral/CollateralDisbursementWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CollateralDisbursementWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (collateral :  Collateral) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Wizard (id=CollateralDisbursementWizard) at CollateralDisbursementWizard.pcf: line 10, column 39
    function beforeCommit_6 (pickedValue :  java.lang.Object) : void {
      disbursement.executeDisbursementOrCreateApprovalActivityIfNeeded()
    }
    
    // 'canVisit' attribute on Wizard (id=CollateralDisbursementWizard) at CollateralDisbursementWizard.pcf: line 10, column 39
    static function canVisit_7 (collateral :  Collateral) : java.lang.Boolean {
      return perm.Transaction.disbcreate
    }
    
    // 'initialValue' attribute on Variable at CollateralDisbursementWizard.pcf: line 19, column 38
    function initialValue_0 () : CollateralDisbursement {
      return newDisbursement()
    }
    
    // 'onExit' attribute on WizardStep (id=Step1) at CollateralDisbursementWizard.pcf: line 25, column 93
    function onExit_1 () : void {
      disbursement.initiateApprovalActivityIfUserLacksAuthority()
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at CollateralDisbursementWizard.pcf: line 25, column 93
    function screen_onEnter_2 (def :  pcf.CollateralDisbursementDetailScreen) : void {
      def.onEnter(disbursement)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at CollateralDisbursementWizard.pcf: line 30, column 94
    function screen_onEnter_4 (def :  pcf.CollateralDisbursementConfirmScreen) : void {
      def.onEnter(disbursement)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at CollateralDisbursementWizard.pcf: line 25, column 93
    function screen_refreshVariables_3 (def :  pcf.CollateralDisbursementDetailScreen) : void {
      def.refreshVariables(disbursement)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at CollateralDisbursementWizard.pcf: line 30, column 94
    function screen_refreshVariables_5 (def :  pcf.CollateralDisbursementConfirmScreen) : void {
      def.refreshVariables(disbursement)
    }
    
    // 'tabBar' attribute on Wizard (id=CollateralDisbursementWizard) at CollateralDisbursementWizard.pcf: line 10, column 39
    function tabBar_onEnter_8 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=CollateralDisbursementWizard) at CollateralDisbursementWizard.pcf: line 10, column 39
    function tabBar_refreshVariables_9 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.CollateralDisbursementWizard {
      return super.CurrentLocation as pcf.CollateralDisbursementWizard
    }
    
    property get collateral () : Collateral {
      return getVariableValue("collateral", 0) as Collateral
    }
    
    property set collateral ($arg :  Collateral) {
      setVariableValue("collateral", 0, $arg)
    }
    
    property get disbursement () : CollateralDisbursement {
      return getVariableValue("disbursement", 0) as CollateralDisbursement
    }
    
    property set disbursement ($arg :  CollateralDisbursement) {
      setVariableValue("disbursement", 0, $arg)
    }
    
    function newDisbursement() : CollateralDisbursement{
            var disb = new CollateralDisbursement(collateral.Currency)
            disb.setCollateralAndFields(collateral);
            return disb;
          }
    
    
  }
  
  
}