package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountCreateSubrogationTargetsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountCreateSubrogationTargetsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountCreateSubrogationTargetsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountCreateSubrogationTargetsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=Account_Input) at AccountCreateSubrogationTargetsScreen.pcf: line 22, column 39
    function valueRoot_1 () : java.lang.Object {
      return subrogation
    }
    
    // 'value' attribute on TextInput (id=Account_Input) at AccountCreateSubrogationTargetsScreen.pcf: line 22, column 39
    function value_0 () : entity.Account {
      return subrogation.SourceAccount
    }
    
    property get subrogation () : Subrogation {
      return getRequireValue("subrogation", 0) as Subrogation
    }
    
    property set subrogation ($arg :  Subrogation) {
      setRequireValue("subrogation", 0, $arg)
    }
    
    
  }
  
  
}