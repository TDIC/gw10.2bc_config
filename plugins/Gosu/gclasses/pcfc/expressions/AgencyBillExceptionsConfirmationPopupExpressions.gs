package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExceptionsConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillExceptionsConfirmationPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExceptionsConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillExceptionsConfirmationPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (exceptionItemViews :  gw.api.web.invoice.ExceptionItemView[], writeOffType :  AgencyWriteoffType) : int {
      return 0
    }
    
    // 'afterEnter' attribute on Popup (id=AgencyBillExceptionsConfirmationPopup) at AgencyBillExceptionsConfirmationPopup.pcf: line 10, column 84
    function afterEnter_4 () : void {
      gw.api.web.producer.agencybill.ExceptionItemUtil.writeoffInvoiceItems(exceptionItemViews, writeOffType, CurrentLocation)
    }
    
    // 'def' attribute on PanelRef at AgencyBillExceptionsConfirmationPopup.pcf: line 37, column 72
    function def_onEnter_2 (def :  pcf.AgencyBillExceptionsLV) : void {
      def.onEnter(exceptionItemViews, false, true)
    }
    
    // 'def' attribute on PanelRef at AgencyBillExceptionsConfirmationPopup.pcf: line 37, column 72
    function def_refreshVariables_3 (def :  pcf.AgencyBillExceptionsLV) : void {
      def.refreshVariables(exceptionItemViews, false, true)
    }
    
    // 'label' attribute on Label at AgencyBillExceptionsConfirmationPopup.pcf: line 33, column 119
    function label_1 () : java.lang.String {
      return DisplayKey.get("Web.AgencyBillExceptionsConfirmation.ConfirmExceptions", writeOffType)
    }
    
    // 'visible' attribute on AlertBar (id=ApprovalActivityAlertBar) at AgencyBillExceptionsConfirmationPopup.pcf: line 29, column 51
    function visible_0 () : java.lang.Boolean {
      return isApprovalRequiredToWriteoff()
    }
    
    override property get CurrentLocation () : pcf.AgencyBillExceptionsConfirmationPopup {
      return super.CurrentLocation as pcf.AgencyBillExceptionsConfirmationPopup
    }
    
    property get exceptionItemViews () : gw.api.web.invoice.ExceptionItemView[] {
      return getVariableValue("exceptionItemViews", 0) as gw.api.web.invoice.ExceptionItemView[]
    }
    
    property set exceptionItemViews ($arg :  gw.api.web.invoice.ExceptionItemView[]) {
      setVariableValue("exceptionItemViews", 0, $arg)
    }
    
    property get writeOffType () : AgencyWriteoffType {
      return getVariableValue("writeOffType", 0) as AgencyWriteoffType
    }
    
    property set writeOffType ($arg :  AgencyWriteoffType) {
      setVariableValue("writeOffType", 0, $arg)
    }
    
    function isApprovalRequiredToWriteoff() : Boolean {
      
      return exceptionItemViews.hasMatch( \ eachExceptionItemView -> eachExceptionItemView.ApprovalRequiredToWriteoff)
    }
    
    
  }
  
  
}