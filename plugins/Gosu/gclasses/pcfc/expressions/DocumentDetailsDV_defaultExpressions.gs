package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/DocumentDetailsDV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DocumentDetailsDV_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/document/DocumentDetailsDV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DocumentDetailsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'columns' attribute on Layout at DocumentDetailsDV.default.pcf: line 17, column 21
    function columns_1 () : java.lang.Double {
      return 1
    }
    
    // 'value' attribute on BooleanRadioInput (id=Hidden_Input) at DocumentDetailsDV.default.pcf: line 22, column 34
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Obsolete = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'initialValue' attribute on Variable at DocumentDetailsDV.default.pcf: line 14, column 47
    function initialValue_0 () : gw.document.DocumentDetailsInfo {
      return new gw.document.DocumentDetailsInfo(document)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Hidden_Input) at DocumentDetailsDV.default.pcf: line 22, column 34
    function valueRoot_4 () : java.lang.Object {
      return document
    }
    
    // 'value' attribute on BooleanRadioInput (id=Hidden_Input) at DocumentDetailsDV.default.pcf: line 22, column 34
    function value_2 () : java.lang.Boolean {
      return document.Obsolete
    }
    
    property get document () : Document {
      return getRequireValue("document", 0) as Document
    }
    
    property set document ($arg :  Document) {
      setRequireValue("document", 0, $arg)
    }
    
    property get documentDetailsInfo () : gw.document.DocumentDetailsInfo {
      return getVariableValue("documentDetailsInfo", 0) as gw.document.DocumentDetailsInfo
    }
    
    property set documentDetailsInfo ($arg :  gw.document.DocumentDetailsInfo) {
      setVariableValue("documentDetailsInfo", 0, $arg)
    }
    
    
  }
  
  
}