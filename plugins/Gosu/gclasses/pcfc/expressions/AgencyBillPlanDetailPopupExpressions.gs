package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/agencybill/AgencyBillPlanDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillPlanDetailPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/agencybill/AgencyBillPlanDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillPlanDetailPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (agencyBillPlan :  AgencyBillPlan) : int {
      return 0
    }
    
    // 'canEdit' attribute on Popup (id=AgencyBillPlanDetailPopup) at AgencyBillPlanDetailPopup.pcf: line 8, column 93
    function canEdit_2 () : java.lang.Boolean {
      return perm.System.agencybillplanedit
    }
    
    // 'def' attribute on ScreenRef at AgencyBillPlanDetailPopup.pcf: line 15, column 57
    function def_onEnter_0 (def :  pcf.AgencyBillPlanDetailScreen) : void {
      def.onEnter(agencyBillPlan)
    }
    
    // 'def' attribute on ScreenRef at AgencyBillPlanDetailPopup.pcf: line 15, column 57
    function def_refreshVariables_1 (def :  pcf.AgencyBillPlanDetailScreen) : void {
      def.refreshVariables(agencyBillPlan)
    }
    
    // 'title' attribute on Popup (id=AgencyBillPlanDetailPopup) at AgencyBillPlanDetailPopup.pcf: line 8, column 93
    static function title_3 (agencyBillPlan :  AgencyBillPlan) : java.lang.Object {
      return DisplayKey.get("Web.AgencyBillPlanDetail.Title", agencyBillPlan.Name)
    }
    
    override property get CurrentLocation () : pcf.AgencyBillPlanDetailPopup {
      return super.CurrentLocation as pcf.AgencyBillPlanDetailPopup
    }
    
    property get agencyBillPlan () : AgencyBillPlan {
      return getVariableValue("agencyBillPlan", 0) as AgencyBillPlan
    }
    
    property set agencyBillPlan ($arg :  AgencyBillPlan) {
      setVariableValue("agencyBillPlan", 0, $arg)
    }
    
    
  }
  
  
}