package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/accounting/ChargeHoldsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChargeHoldsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/accounting/ChargeHoldsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChargeHoldsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (charges :  Charge[]) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=ChargeHoldsPopup) at ChargeHoldsPopup.pcf: line 14, column 68
    function beforeCommit_7 (pickedValue :  java.lang.Object) : void {
      updateHolds()
    }
    
    // 'canVisit' attribute on Popup (id=ChargeHoldsPopup) at ChargeHoldsPopup.pcf: line 14, column 68
    static function canVisit_8 (charges :  Charge[]) : java.lang.Boolean {
      return perm.Transaction.chargeholdcreate
    }
    
    // 'def' attribute on PanelRef at ChargeHoldsPopup.pcf: line 49, column 64
    function def_onEnter_5 (def :  pcf.ChargesLV) : void {
      def.onEnter(charges, false, 1, true, false, true)
    }
    
    // 'def' attribute on PanelRef at ChargeHoldsPopup.pcf: line 49, column 64
    function def_refreshVariables_6 (def :  pcf.ChargesLV) : void {
      def.refreshVariables(charges, false, 1, true, false, true)
    }
    
    // 'value' attribute on CheckBoxInput (id=carryForwardExceptions_Input) at ChargeHoldsPopup.pcf: line 44, column 46
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      carryForwardExceptions = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=carryForwardExceptions_Input) at ChargeHoldsPopup.pcf: line 44, column 46
    function value_1 () : java.lang.Boolean {
      return carryForwardExceptions
    }
    
    // 'visible' attribute on CheckBoxInput (id=carryForwardExceptions_Input) at ChargeHoldsPopup.pcf: line 44, column 46
    function visible_0 () : java.lang.Boolean {
      return hasAgencyBillCharge()
    }
    
    override property get CurrentLocation () : pcf.ChargeHoldsPopup {
      return super.CurrentLocation as pcf.ChargeHoldsPopup
    }
    
    property get carryForwardExceptions () : boolean {
      return getVariableValue("carryForwardExceptions", 0) as java.lang.Boolean
    }
    
    property set carryForwardExceptions ($arg :  boolean) {
      setVariableValue("carryForwardExceptions", 0, $arg)
    }
    
    property get charges () : Charge[] {
      return getVariableValue("charges", 0) as Charge[]
    }
    
    property set charges ($arg :  Charge[]) {
      setVariableValue("charges", 0, $arg)
    }
    
    
    function updateHolds() {
            for(charge in charges)
            {
              charge.updateHold(carryForwardExceptions);
            }
          }
    
    function hasAgencyBillCharge() : boolean{
      for(charge in charges){
        if(charge.AgencyBill){
          return true
        }
      }
      return false
    }
        
    
    
  }
  
  
}