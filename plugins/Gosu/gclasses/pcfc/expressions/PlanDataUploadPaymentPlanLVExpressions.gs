package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadPaymentPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanDataUploadPaymentPlanLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadPaymentPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PlanDataUploadPaymentPlanLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at PlanDataUploadPaymentPlanLV.pcf: line 17, column 102
    function highlighted_190 () : java.lang.Boolean {
      return (paymentPlan.Error or paymentPlan.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TypeKeyCell (id=periodicity_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 69, column 44
    function label_100 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.PaymentInterval")
    }
    
    // 'label' attribute on TypeKeyCell (id=invoiceItemPlacementCutoffType_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 74, column 63
    function label_105 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.ItemPlacementCutoffType")
    }
    
    // 'label' attribute on TypeKeyCell (id=invoicingBlackoutType_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 79, column 54
    function label_110 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.MakeLastInvoice")
    }
    
    // 'label' attribute on TextCell (id=daysBeforePolicyExpiration_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 84, column 42
    function label_115 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.DaysBeforePolicyExpiration")
    }
    
    // 'label' attribute on TextCell (id=installmentFee_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 89, column 45
    function label_120 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.InvoicingFee")
    }
    
    // 'label' attribute on BooleanRadioCell (id=skipFeeForDownPayment_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 94, column 42
    function label_125 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.SkipFeeForDownPayment")
    }
    
    // 'label' attribute on TextCell (id=downPaymentDays_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 99, column 42
    function label_130 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.DaysToDownPayment")
    }
    
    // 'label' attribute on TypeKeyCell (id=downPaymentAfter_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 104, column 54
    function label_135 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.DownPaymentDaysAfter")
    }
    
    // 'label' attribute on TextCell (id=daysToFirstInstallment_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 109, column 42
    function label_140 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.DaysToFirstInstallment")
    }
    
    // 'label' attribute on TypeKeyCell (id=FirstInstallmentAfter_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 114, column 54
    function label_145 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.FirstInstallmentDaysAfter")
    }
    
    // 'label' attribute on TextCell (id=daysToSecondInstallment_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 119, column 42
    function label_150 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.DaysToSecondInstallment")
    }
    
    // 'label' attribute on TypeKeyCell (id=SecondInstallmentAfter_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 124, column 54
    function label_155 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.SecondInstallmentDaysAfter")
    }
    
    // 'label' attribute on TextCell (id=daysToOneTimeCharge_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 129, column 42
    function label_160 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.DaysToOneTimeCharge")
    }
    
    // 'label' attribute on TypeKeyCell (id=oneTimeChargeAfter_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 134, column 54
    function label_165 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.OneTimeChargeDaysAfter")
    }
    
    // 'label' attribute on TextCell (id=equityBuffer_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 139, column 42
    function label_170 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.EquityBuffer")
    }
    
    // 'label' attribute on BooleanRadioCell (id=equityWarningsEnabled_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 144, column 42
    function label_175 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.EquityWarningsEnabled")
    }
    
    // 'label' attribute on TypeKeyCell (id=billDateOrDueDateBilling_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 149, column 57
    function label_180 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.BillDateOrDueDateBilling")
    }
    
    // 'label' attribute on BooleanRadioCell (id=alignInstallmentsToInvoices_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 154, column 42
    function label_185 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.AlignInstallmentsToInvoices")
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 23, column 46
    function label_55 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 28, column 41
    function label_60 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.PublicID")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 34, column 41
    function label_65 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.PlanName")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 39, column 41
    function label_70 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.Description")
    }
    
    // 'label' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 44, column 39
    function label_75 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.EffectiveDate")
    }
    
    // 'label' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 49, column 39
    function label_80 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.ExpirationDate")
    }
    
    // 'label' attribute on BooleanRadioCell (id=isReporting_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 54, column 42
    function label_85 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.IsReporting")
    }
    
    // 'label' attribute on TextCell (id=downPaymentPercent_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 59, column 45
    function label_90 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.DownPaymentPercentage")
    }
    
    // 'label' attribute on TextCell (id=maximumNumberOfInstallments_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 64, column 42
    function label_95 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.MaxInstallments")
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 28, column 41
    function valueRoot_62 () : java.lang.Object {
      return paymentPlan
    }
    
    // 'value' attribute on TypeKeyCell (id=periodicity_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 69, column 44
    function value_101 () : typekey.Periodicity {
      return paymentPlan.Periodicity
    }
    
    // 'value' attribute on TypeKeyCell (id=invoiceItemPlacementCutoffType_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 74, column 63
    function value_106 () : typekey.InvoiceItemPlacementCutoffType {
      return paymentPlan.InvoiceItemPlacementCutoffType
    }
    
    // 'value' attribute on TypeKeyCell (id=invoicingBlackoutType_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 79, column 54
    function value_111 () : typekey.InvoicingBlackoutType {
      return paymentPlan.InvoicingBlackoutType
    }
    
    // 'value' attribute on TextCell (id=daysBeforePolicyExpiration_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 84, column 42
    function value_116 () : java.lang.Integer {
      return paymentPlan.DaysBeforePolicyExpirationForInvoicingBlackout
    }
    
    // 'value' attribute on TextCell (id=installmentFee_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 89, column 45
    function value_121 () : java.math.BigDecimal {
      return paymentPlan.InstallmentFee
    }
    
    // 'value' attribute on BooleanRadioCell (id=skipFeeForDownPayment_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 94, column 42
    function value_126 () : java.lang.Boolean {
      return paymentPlan.SkipFeeForDownPayment
    }
    
    // 'value' attribute on TextCell (id=downPaymentDays_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 99, column 42
    function value_131 () : java.lang.Integer {
      return paymentPlan.DaysFromReferenceDateToDownPayment
    }
    
    // 'value' attribute on TypeKeyCell (id=downPaymentAfter_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 104, column 54
    function value_136 () : typekey.PaymentScheduledAfter {
      return paymentPlan.DownPaymentAfter
    }
    
    // 'value' attribute on TextCell (id=daysToFirstInstallment_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 109, column 42
    function value_141 () : java.lang.Integer {
      return paymentPlan.DaysFromReferenceDateToFirstInstallment
    }
    
    // 'value' attribute on TypeKeyCell (id=FirstInstallmentAfter_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 114, column 54
    function value_146 () : typekey.PaymentScheduledAfter {
      return paymentPlan.FirstInstallmentAfter
    }
    
    // 'value' attribute on TextCell (id=daysToSecondInstallment_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 119, column 42
    function value_151 () : java.lang.Integer {
      return paymentPlan.DaysFromReferenceDateToSecondInstallment
    }
    
    // 'value' attribute on TypeKeyCell (id=SecondInstallmentAfter_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 124, column 54
    function value_156 () : typekey.PaymentScheduledAfter {
      return paymentPlan.SecondInstallmentAfter
    }
    
    // 'value' attribute on TextCell (id=daysToOneTimeCharge_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 129, column 42
    function value_161 () : java.lang.Integer {
      return paymentPlan.DaysFromReferenceDateToOneTimeCharge
    }
    
    // 'value' attribute on TypeKeyCell (id=oneTimeChargeAfter_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 134, column 54
    function value_166 () : typekey.PaymentScheduledAfter {
      return paymentPlan.OneTimeChargeAfter
    }
    
    // 'value' attribute on TextCell (id=equityBuffer_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 139, column 42
    function value_171 () : java.lang.Integer {
      return paymentPlan.EquityBuffer
    }
    
    // 'value' attribute on BooleanRadioCell (id=equityWarningsEnabled_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 144, column 42
    function value_176 () : java.lang.Boolean {
      return paymentPlan.EquityWarningsEnabled
    }
    
    // 'value' attribute on TypeKeyCell (id=billDateOrDueDateBilling_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 149, column 57
    function value_181 () : typekey.BillDateOrDueDateBilling {
      return paymentPlan.PolicyLevelBillingBillDateOrDueDateBilling
    }
    
    // 'value' attribute on BooleanRadioCell (id=alignInstallmentsToInvoices_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 154, column 42
    function value_186 () : java.lang.Boolean {
      return paymentPlan.AlignInstallmentsToInvoices
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 23, column 46
    function value_56 () : java.lang.String {
      return processor.getLoadStatus(paymentPlan)
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 28, column 41
    function value_61 () : java.lang.String {
      return paymentPlan.PublicID
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 34, column 41
    function value_66 () : java.lang.String {
      return paymentPlan.Name
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 39, column 41
    function value_71 () : java.lang.String {
      return paymentPlan.Description
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 44, column 39
    function value_76 () : java.util.Date {
      return paymentPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 49, column 39
    function value_81 () : java.util.Date {
      return paymentPlan.ExpirationDate
    }
    
    // 'value' attribute on BooleanRadioCell (id=isReporting_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 54, column 42
    function value_86 () : java.lang.Boolean {
      return paymentPlan.Reporting
    }
    
    // 'value' attribute on TextCell (id=downPaymentPercent_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 59, column 45
    function value_91 () : java.math.BigDecimal {
      return paymentPlan.DownPaymentPercent
    }
    
    // 'value' attribute on TextCell (id=maximumNumberOfInstallments_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 64, column 42
    function value_96 () : java.lang.Integer {
      return paymentPlan.MaximumNumberOfInstallments
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 23, column 46
    function visible_57 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get paymentPlan () : tdic.util.dataloader.data.plandata.PaymentPlanData {
      return getIteratedValue(1) as tdic.util.dataloader.data.plandata.PaymentPlanData
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadPaymentPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanDataUploadPaymentPlanLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 49, column 39
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.ExpirationDate")
    }
    
    // 'label' attribute on BooleanRadioCell (id=isReporting_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 54, column 42
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.IsReporting")
    }
    
    // 'label' attribute on TextCell (id=downPaymentPercent_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 59, column 45
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.DownPaymentPercentage")
    }
    
    // 'label' attribute on TextCell (id=maximumNumberOfInstallments_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 64, column 42
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.MaxInstallments")
    }
    
    // 'label' attribute on TypeKeyCell (id=periodicity_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 69, column 44
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.PaymentInterval")
    }
    
    // 'label' attribute on TypeKeyCell (id=invoiceItemPlacementCutoffType_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 74, column 63
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.ItemPlacementCutoffType")
    }
    
    // 'label' attribute on TypeKeyCell (id=invoicingBlackoutType_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 79, column 54
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.MakeLastInvoice")
    }
    
    // 'label' attribute on TextCell (id=daysBeforePolicyExpiration_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 84, column 42
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.DaysBeforePolicyExpiration")
    }
    
    // 'label' attribute on TextCell (id=installmentFee_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 89, column 45
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.InvoicingFee")
    }
    
    // 'label' attribute on BooleanRadioCell (id=skipFeeForDownPayment_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 94, column 42
    function label_29 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.SkipFeeForDownPayment")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 28, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.PublicID")
    }
    
    // 'label' attribute on TextCell (id=downPaymentDays_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 99, column 42
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.DaysToDownPayment")
    }
    
    // 'label' attribute on TypeKeyCell (id=downPaymentAfter_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 104, column 54
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.DownPaymentDaysAfter")
    }
    
    // 'label' attribute on TextCell (id=daysToFirstInstallment_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 109, column 42
    function label_35 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.DaysToFirstInstallment")
    }
    
    // 'label' attribute on TypeKeyCell (id=FirstInstallmentAfter_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 114, column 54
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.FirstInstallmentDaysAfter")
    }
    
    // 'label' attribute on TextCell (id=daysToSecondInstallment_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 119, column 42
    function label_39 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.DaysToSecondInstallment")
    }
    
    // 'label' attribute on TypeKeyCell (id=SecondInstallmentAfter_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 124, column 54
    function label_41 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.SecondInstallmentDaysAfter")
    }
    
    // 'label' attribute on TextCell (id=daysToOneTimeCharge_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 129, column 42
    function label_43 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.DaysToOneTimeCharge")
    }
    
    // 'label' attribute on TypeKeyCell (id=oneTimeChargeAfter_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 134, column 54
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.OneTimeChargeDaysAfter")
    }
    
    // 'label' attribute on TextCell (id=equityBuffer_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 139, column 42
    function label_47 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.EquityBuffer")
    }
    
    // 'label' attribute on BooleanRadioCell (id=equityWarningsEnabled_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 144, column 42
    function label_49 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.EquityWarningsEnabled")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 34, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.PlanName")
    }
    
    // 'label' attribute on TypeKeyCell (id=billDateOrDueDateBilling_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 149, column 57
    function label_51 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.BillDateOrDueDateBilling")
    }
    
    // 'label' attribute on BooleanRadioCell (id=alignInstallmentsToInvoices_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 154, column 42
    function label_53 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.AlignInstallmentsToInvoices")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 39, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.Description")
    }
    
    // 'label' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 44, column 39
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlan.EffectiveDate")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 23, column 46
    function sortValue_1 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return processor.getLoadStatus(paymentPlan)
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 44, column 39
    function sortValue_10 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 49, column 39
    function sortValue_12 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.ExpirationDate
    }
    
    // 'value' attribute on BooleanRadioCell (id=isReporting_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 54, column 42
    function sortValue_14 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.Reporting
    }
    
    // 'value' attribute on TextCell (id=downPaymentPercent_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 59, column 45
    function sortValue_16 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.DownPaymentPercent
    }
    
    // 'value' attribute on TextCell (id=maximumNumberOfInstallments_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 64, column 42
    function sortValue_18 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.MaximumNumberOfInstallments
    }
    
    // 'value' attribute on TypeKeyCell (id=periodicity_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 69, column 44
    function sortValue_20 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.Periodicity
    }
    
    // 'value' attribute on TypeKeyCell (id=invoiceItemPlacementCutoffType_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 74, column 63
    function sortValue_22 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.InvoiceItemPlacementCutoffType
    }
    
    // 'value' attribute on TypeKeyCell (id=invoicingBlackoutType_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 79, column 54
    function sortValue_24 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.InvoicingBlackoutType
    }
    
    // 'value' attribute on TextCell (id=daysBeforePolicyExpiration_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 84, column 42
    function sortValue_26 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.DaysBeforePolicyExpirationForInvoicingBlackout
    }
    
    // 'value' attribute on TextCell (id=installmentFee_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 89, column 45
    function sortValue_28 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.InstallmentFee
    }
    
    // 'value' attribute on BooleanRadioCell (id=skipFeeForDownPayment_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 94, column 42
    function sortValue_30 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.SkipFeeForDownPayment
    }
    
    // 'value' attribute on TextCell (id=downPaymentDays_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 99, column 42
    function sortValue_32 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.DaysFromReferenceDateToDownPayment
    }
    
    // 'value' attribute on TypeKeyCell (id=downPaymentAfter_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 104, column 54
    function sortValue_34 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.DownPaymentAfter
    }
    
    // 'value' attribute on TextCell (id=daysToFirstInstallment_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 109, column 42
    function sortValue_36 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.DaysFromReferenceDateToFirstInstallment
    }
    
    // 'value' attribute on TypeKeyCell (id=FirstInstallmentAfter_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 114, column 54
    function sortValue_38 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.FirstInstallmentAfter
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 28, column 41
    function sortValue_4 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.PublicID
    }
    
    // 'value' attribute on TextCell (id=daysToSecondInstallment_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 119, column 42
    function sortValue_40 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.DaysFromReferenceDateToSecondInstallment
    }
    
    // 'value' attribute on TypeKeyCell (id=SecondInstallmentAfter_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 124, column 54
    function sortValue_42 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.SecondInstallmentAfter
    }
    
    // 'value' attribute on TextCell (id=daysToOneTimeCharge_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 129, column 42
    function sortValue_44 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.DaysFromReferenceDateToOneTimeCharge
    }
    
    // 'value' attribute on TypeKeyCell (id=oneTimeChargeAfter_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 134, column 54
    function sortValue_46 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.OneTimeChargeAfter
    }
    
    // 'value' attribute on TextCell (id=equityBuffer_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 139, column 42
    function sortValue_48 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.EquityBuffer
    }
    
    // 'value' attribute on BooleanRadioCell (id=equityWarningsEnabled_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 144, column 42
    function sortValue_50 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.EquityWarningsEnabled
    }
    
    // 'value' attribute on TypeKeyCell (id=billDateOrDueDateBilling_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 149, column 57
    function sortValue_52 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.PolicyLevelBillingBillDateOrDueDateBilling
    }
    
    // 'value' attribute on BooleanRadioCell (id=alignInstallmentsToInvoices_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 154, column 42
    function sortValue_54 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.AlignInstallmentsToInvoices
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 34, column 41
    function sortValue_6 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.Name
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 39, column 41
    function sortValue_8 (paymentPlan :  tdic.util.dataloader.data.plandata.PaymentPlanData) : java.lang.Object {
      return paymentPlan.Description
    }
    
    // 'value' attribute on RowIterator (id=PaymentPlan) at PlanDataUploadPaymentPlanLV.pcf: line 15, column 97
    function value_191 () : java.util.ArrayList<tdic.util.dataloader.data.plandata.PaymentPlanData> {
      return processor.PaymentPlanArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentPlanLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}