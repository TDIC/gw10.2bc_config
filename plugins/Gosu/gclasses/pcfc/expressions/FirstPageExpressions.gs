package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/FirstPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class FirstPageExpressions {
  @javax.annotation.Generated("config/web/pcf/FirstPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FirstPageExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 17, column 62
    function action_1 () : void {
      Admin.go()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 26, column 65
    function action_10 () : void {
      Policies.go()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 29, column 66
    function action_13 () : void {
      Producers.go()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 32, column 63
    function action_16 () : void {
      SearchGroup.go()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 35, column 64
    function action_19 () : void {
      Reports.go()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 38, column 28
    function action_22 () : void {
      Admin.go()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 40, column 35
    function action_24 () : void {
      DesktopGroup.go()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 42, column 31
    function action_26 () : void {
      Accounts.go()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 44, column 31
    function action_28 () : void {
      Policies.go()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 46, column 32
    function action_30 () : void {
      Producers.go()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 48, column 30
    function action_32 () : void {
      Reports.go()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 50, column 34
    function action_34 () : void {
      SearchGroup.go()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 52, column 40
    function action_36 () : void {
      NoPermissionsPage.go()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 20, column 69
    function action_4 () : void {
      DesktopGroup.go()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 23, column 65
    function action_7 () : void {
      Accounts.go()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 26, column 65
    function action_dest_11 () : pcf.api.Destination {
      return pcf.Policies.createDestination()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 29, column 66
    function action_dest_14 () : pcf.api.Destination {
      return pcf.Producers.createDestination()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 32, column 63
    function action_dest_17 () : pcf.api.Destination {
      return pcf.SearchGroup.createDestination()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 17, column 62
    function action_dest_2 () : pcf.api.Destination {
      return pcf.Admin.createDestination()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 35, column 64
    function action_dest_20 () : pcf.api.Destination {
      return pcf.Reports.createDestination()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 38, column 28
    function action_dest_23 () : pcf.api.Destination {
      return pcf.Admin.createDestination()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 40, column 35
    function action_dest_25 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 42, column 31
    function action_dest_27 () : pcf.api.Destination {
      return pcf.Accounts.createDestination()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 44, column 31
    function action_dest_29 () : pcf.api.Destination {
      return pcf.Policies.createDestination()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 46, column 32
    function action_dest_31 () : pcf.api.Destination {
      return pcf.Producers.createDestination()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 48, column 30
    function action_dest_33 () : pcf.api.Destination {
      return pcf.Reports.createDestination()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 50, column 34
    function action_dest_35 () : pcf.api.Destination {
      return pcf.SearchGroup.createDestination()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 52, column 40
    function action_dest_37 () : pcf.api.Destination {
      return pcf.NoPermissionsPage.createDestination()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 20, column 69
    function action_dest_5 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'action' attribute on ForwardCondition at FirstPage.pcf: line 23, column 65
    function action_dest_8 () : pcf.api.Destination {
      return pcf.Accounts.createDestination()
    }
    
    // 'condition' attribute on ForwardCondition at FirstPage.pcf: line 26, column 65
    function condition_12 () : java.lang.Boolean {
      return user.UserSettings.StartupPage == TC_POLICIES
    }
    
    // 'condition' attribute on ForwardCondition at FirstPage.pcf: line 29, column 66
    function condition_15 () : java.lang.Boolean {
      return user.UserSettings.StartupPage == TC_PRODUCERS
    }
    
    // 'condition' attribute on ForwardCondition at FirstPage.pcf: line 32, column 63
    function condition_18 () : java.lang.Boolean {
      return user.UserSettings.StartupPage == TC_SEARCH
    }
    
    // 'condition' attribute on ForwardCondition at FirstPage.pcf: line 35, column 64
    function condition_21 () : java.lang.Boolean {
      return user.UserSettings.StartupPage == TC_REPORTS
    }
    
    // 'condition' attribute on ForwardCondition at FirstPage.pcf: line 17, column 62
    function condition_3 () : java.lang.Boolean {
      return user.UserSettings.StartupPage == TC_ADMIN
    }
    
    // 'condition' attribute on ForwardCondition at FirstPage.pcf: line 20, column 69
    function condition_6 () : java.lang.Boolean {
      return user.UserSettings.StartupPage == TC_DESKTOPGROUP
    }
    
    // 'condition' attribute on ForwardCondition at FirstPage.pcf: line 23, column 65
    function condition_9 () : java.lang.Boolean {
      return user.UserSettings.StartupPage == TC_ACCOUNTS
    }
    
    // 'initialValue' attribute on Variable at FirstPage.pcf: line 13, column 27
    function initialValue_0 () : entity.User {
      return entity.User.util.CurrentUser
    }
    
    override property get CurrentLocation () : pcf.FirstPage {
      return super.CurrentLocation as pcf.FirstPage
    }
    
    property get user () : entity.User {
      return getVariableValue("user", 0) as entity.User
    }
    
    property set user ($arg :  entity.User) {
      setVariableValue("user", 0, $arg)
    }
    
    
  }
  
  
}