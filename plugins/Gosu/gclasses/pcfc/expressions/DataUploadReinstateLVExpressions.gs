package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadReinstateLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DataUploadReinstateLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadReinstateLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DataUploadReinstateLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=amount1_Cell) at DataUploadReinstateLV.pcf: line 50, column 45
    function label_10 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount1")
    }
    
    // 'label' attribute on TextCell (id=payer1_Cell) at DataUploadReinstateLV.pcf: line 55, column 41
    function label_12 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Payer1")
    }
    
    // 'label' attribute on TextCell (id=invoiceStream1_Cell) at DataUploadReinstateLV.pcf: line 60, column 41
    function label_14 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.InvoiceStream1")
    }
    
    // 'label' attribute on TextCell (id=charge2_Cell) at DataUploadReinstateLV.pcf: line 65, column 41
    function label_16 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge2")
    }
    
    // 'label' attribute on TextCell (id=amount2_Cell) at DataUploadReinstateLV.pcf: line 70, column 45
    function label_18 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount2")
    }
    
    // 'label' attribute on DateCell (id=reinstateDate_Cell) at DataUploadReinstateLV.pcf: line 29, column 39
    function label_2 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ReinstatementDate")
    }
    
    // 'label' attribute on TextCell (id=payer2_Cell) at DataUploadReinstateLV.pcf: line 75, column 41
    function label_20 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Payer2")
    }
    
    // 'label' attribute on TextCell (id=invoiceStream2_Cell) at DataUploadReinstateLV.pcf: line 80, column 41
    function label_22 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.InvoiceStream2")
    }
    
    // 'label' attribute on TextCell (id=charge3_Cell) at DataUploadReinstateLV.pcf: line 85, column 41
    function label_24 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge3")
    }
    
    // 'label' attribute on TextCell (id=amount3_Cell) at DataUploadReinstateLV.pcf: line 90, column 45
    function label_26 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount3")
    }
    
    // 'label' attribute on TextCell (id=payer3_Cell) at DataUploadReinstateLV.pcf: line 95, column 41
    function label_28 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Payer3")
    }
    
    // 'label' attribute on TextCell (id=invoiceStream3_Cell) at DataUploadReinstateLV.pcf: line 100, column 41
    function label_30 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.InvoiceStream3")
    }
    
    // 'label' attribute on TextCell (id=charge4_Cell) at DataUploadReinstateLV.pcf: line 105, column 41
    function label_32 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge4")
    }
    
    // 'label' attribute on TextCell (id=amount4_Cell) at DataUploadReinstateLV.pcf: line 110, column 45
    function label_34 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount4")
    }
    
    // 'label' attribute on TextCell (id=payer4_Cell) at DataUploadReinstateLV.pcf: line 115, column 41
    function label_36 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Payer4")
    }
    
    // 'label' attribute on TextCell (id=invoiceStream4_Cell) at DataUploadReinstateLV.pcf: line 120, column 41
    function label_38 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.InvoiceStream4")
    }
    
    // 'label' attribute on TextCell (id=policyNumber_Cell) at DataUploadReinstateLV.pcf: line 35, column 41
    function label_4 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.PolicyNumber")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at DataUploadReinstateLV.pcf: line 40, column 41
    function label_6 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Description")
    }
    
    // 'label' attribute on TextCell (id=charge1_Cell) at DataUploadReinstateLV.pcf: line 45, column 41
    function label_8 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge1")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadReinstateLV.pcf: line 23, column 46
    function sortValue_0 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return processor.getLoadStatus(reinstate)
    }
    
    // 'value' attribute on TextCell (id=amount1_Cell) at DataUploadReinstateLV.pcf: line 50, column 45
    function sortValue_11 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.Charges[0].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=payer1_Cell) at DataUploadReinstateLV.pcf: line 55, column 41
    function sortValue_13 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.Charges[0].Payer
    }
    
    // 'value' attribute on TextCell (id=invoiceStream1_Cell) at DataUploadReinstateLV.pcf: line 60, column 41
    function sortValue_15 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.Charges[0].InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=charge2_Cell) at DataUploadReinstateLV.pcf: line 65, column 41
    function sortValue_17 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.Charges[1].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount2_Cell) at DataUploadReinstateLV.pcf: line 70, column 45
    function sortValue_19 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.Charges[1].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=payer2_Cell) at DataUploadReinstateLV.pcf: line 75, column 41
    function sortValue_21 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.Charges[1].Payer
    }
    
    // 'value' attribute on TextCell (id=invoiceStream2_Cell) at DataUploadReinstateLV.pcf: line 80, column 41
    function sortValue_23 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.Charges[1].InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=charge3_Cell) at DataUploadReinstateLV.pcf: line 85, column 41
    function sortValue_25 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.Charges[2].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount3_Cell) at DataUploadReinstateLV.pcf: line 90, column 45
    function sortValue_27 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.Charges[2].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=payer3_Cell) at DataUploadReinstateLV.pcf: line 95, column 41
    function sortValue_29 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.Charges[2].Payer
    }
    
    // 'value' attribute on DateCell (id=reinstateDate_Cell) at DataUploadReinstateLV.pcf: line 29, column 39
    function sortValue_3 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.EntryDate
    }
    
    // 'value' attribute on TextCell (id=invoiceStream3_Cell) at DataUploadReinstateLV.pcf: line 100, column 41
    function sortValue_31 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.Charges[2].InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=charge4_Cell) at DataUploadReinstateLV.pcf: line 105, column 41
    function sortValue_33 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.Charges[3].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount4_Cell) at DataUploadReinstateLV.pcf: line 110, column 45
    function sortValue_35 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.Charges[3].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=payer4_Cell) at DataUploadReinstateLV.pcf: line 115, column 41
    function sortValue_37 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.Charges[3].Payer
    }
    
    // 'value' attribute on TextCell (id=invoiceStream4_Cell) at DataUploadReinstateLV.pcf: line 120, column 41
    function sortValue_39 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.Charges[3].InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=policyNumber_Cell) at DataUploadReinstateLV.pcf: line 35, column 41
    function sortValue_5 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.AssociatedPolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at DataUploadReinstateLV.pcf: line 40, column 41
    function sortValue_7 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.Description
    }
    
    // 'value' attribute on TextCell (id=charge1_Cell) at DataUploadReinstateLV.pcf: line 45, column 41
    function sortValue_9 (reinstate :  tdic.util.dataloader.data.sampledata.ReinstatementData) : java.lang.Object {
      return reinstate.Charges[0].ChargePattern
    }
    
    // 'value' attribute on RowIterator (id=reinstateID) at DataUploadReinstateLV.pcf: line 15, column 101
    function value_123 () : java.util.ArrayList<tdic.util.dataloader.data.sampledata.ReinstatementData> {
      return processor.ReinstatementArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadReinstateLV.pcf: line 23, column 46
    function visible_1 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCSampleDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCSampleDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadReinstateLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DataUploadReinstateLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at DataUploadReinstateLV.pcf: line 17, column 98
    function highlighted_122 () : java.lang.Boolean {
      return (reinstate.Error or reinstate.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=invoiceStream3_Cell) at DataUploadReinstateLV.pcf: line 100, column 41
    function label_102 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.InvoiceStream3")
    }
    
    // 'label' attribute on TextCell (id=charge4_Cell) at DataUploadReinstateLV.pcf: line 105, column 41
    function label_106 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge4")
    }
    
    // 'label' attribute on TextCell (id=amount4_Cell) at DataUploadReinstateLV.pcf: line 110, column 45
    function label_110 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount4")
    }
    
    // 'label' attribute on TextCell (id=payer4_Cell) at DataUploadReinstateLV.pcf: line 115, column 41
    function label_114 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Payer4")
    }
    
    // 'label' attribute on TextCell (id=invoiceStream4_Cell) at DataUploadReinstateLV.pcf: line 120, column 41
    function label_118 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.InvoiceStream4")
    }
    
    // 'label' attribute on DateCell (id=reinstateDate_Cell) at DataUploadReinstateLV.pcf: line 29, column 39
    function label_43 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ReinstatementDate")
    }
    
    // 'label' attribute on TextCell (id=policyNumber_Cell) at DataUploadReinstateLV.pcf: line 35, column 41
    function label_48 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.PolicyNumber")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at DataUploadReinstateLV.pcf: line 40, column 41
    function label_53 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Description")
    }
    
    // 'label' attribute on TextCell (id=charge1_Cell) at DataUploadReinstateLV.pcf: line 45, column 41
    function label_58 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge1")
    }
    
    // 'label' attribute on TextCell (id=amount1_Cell) at DataUploadReinstateLV.pcf: line 50, column 45
    function label_62 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount1")
    }
    
    // 'label' attribute on TextCell (id=payer1_Cell) at DataUploadReinstateLV.pcf: line 55, column 41
    function label_66 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Payer1")
    }
    
    // 'label' attribute on TextCell (id=invoiceStream1_Cell) at DataUploadReinstateLV.pcf: line 60, column 41
    function label_70 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.InvoiceStream1")
    }
    
    // 'label' attribute on TextCell (id=charge2_Cell) at DataUploadReinstateLV.pcf: line 65, column 41
    function label_74 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge2")
    }
    
    // 'label' attribute on TextCell (id=amount2_Cell) at DataUploadReinstateLV.pcf: line 70, column 45
    function label_78 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount2")
    }
    
    // 'label' attribute on TextCell (id=payer2_Cell) at DataUploadReinstateLV.pcf: line 75, column 41
    function label_82 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Payer2")
    }
    
    // 'label' attribute on TextCell (id=invoiceStream2_Cell) at DataUploadReinstateLV.pcf: line 80, column 41
    function label_86 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.InvoiceStream2")
    }
    
    // 'label' attribute on TextCell (id=charge3_Cell) at DataUploadReinstateLV.pcf: line 85, column 41
    function label_90 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Charge3")
    }
    
    // 'label' attribute on TextCell (id=amount3_Cell) at DataUploadReinstateLV.pcf: line 90, column 45
    function label_94 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.ChargeAmount3")
    }
    
    // 'label' attribute on TextCell (id=payer3_Cell) at DataUploadReinstateLV.pcf: line 95, column 41
    function label_98 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policy.Payer3")
    }
    
    // 'value' attribute on DateCell (id=reinstateDate_Cell) at DataUploadReinstateLV.pcf: line 29, column 39
    function valueRoot_45 () : java.lang.Object {
      return reinstate
    }
    
    // 'value' attribute on TextCell (id=invoiceStream3_Cell) at DataUploadReinstateLV.pcf: line 100, column 41
    function value_103 () : java.lang.String {
      return reinstate.Charges[2].InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=charge4_Cell) at DataUploadReinstateLV.pcf: line 105, column 41
    function value_107 () : java.lang.String {
      return reinstate.Charges[3].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount4_Cell) at DataUploadReinstateLV.pcf: line 110, column 45
    function value_111 () : java.math.BigDecimal {
      return reinstate.Charges[3].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=payer4_Cell) at DataUploadReinstateLV.pcf: line 115, column 41
    function value_115 () : java.lang.String {
      return reinstate.Charges[3].Payer
    }
    
    // 'value' attribute on TextCell (id=invoiceStream4_Cell) at DataUploadReinstateLV.pcf: line 120, column 41
    function value_119 () : java.lang.String {
      return reinstate.Charges[3].InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadReinstateLV.pcf: line 23, column 46
    function value_40 () : java.lang.String {
      return processor.getLoadStatus(reinstate)
    }
    
    // 'value' attribute on DateCell (id=reinstateDate_Cell) at DataUploadReinstateLV.pcf: line 29, column 39
    function value_44 () : java.util.Date {
      return reinstate.EntryDate
    }
    
    // 'value' attribute on TextCell (id=policyNumber_Cell) at DataUploadReinstateLV.pcf: line 35, column 41
    function value_49 () : java.lang.String {
      return reinstate.AssociatedPolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at DataUploadReinstateLV.pcf: line 40, column 41
    function value_54 () : java.lang.String {
      return reinstate.Description
    }
    
    // 'value' attribute on TextCell (id=charge1_Cell) at DataUploadReinstateLV.pcf: line 45, column 41
    function value_59 () : java.lang.String {
      return reinstate.Charges[0].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount1_Cell) at DataUploadReinstateLV.pcf: line 50, column 45
    function value_63 () : java.math.BigDecimal {
      return reinstate.Charges[0].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=payer1_Cell) at DataUploadReinstateLV.pcf: line 55, column 41
    function value_67 () : java.lang.String {
      return reinstate.Charges[0].Payer
    }
    
    // 'value' attribute on TextCell (id=invoiceStream1_Cell) at DataUploadReinstateLV.pcf: line 60, column 41
    function value_71 () : java.lang.String {
      return reinstate.Charges[0].InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=charge2_Cell) at DataUploadReinstateLV.pcf: line 65, column 41
    function value_75 () : java.lang.String {
      return reinstate.Charges[1].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount2_Cell) at DataUploadReinstateLV.pcf: line 70, column 45
    function value_79 () : java.math.BigDecimal {
      return reinstate.Charges[1].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=payer2_Cell) at DataUploadReinstateLV.pcf: line 75, column 41
    function value_83 () : java.lang.String {
      return reinstate.Charges[1].Payer
    }
    
    // 'value' attribute on TextCell (id=invoiceStream2_Cell) at DataUploadReinstateLV.pcf: line 80, column 41
    function value_87 () : java.lang.String {
      return reinstate.Charges[1].InvoiceStream
    }
    
    // 'value' attribute on TextCell (id=charge3_Cell) at DataUploadReinstateLV.pcf: line 85, column 41
    function value_91 () : java.lang.String {
      return reinstate.Charges[2].ChargePattern
    }
    
    // 'value' attribute on TextCell (id=amount3_Cell) at DataUploadReinstateLV.pcf: line 90, column 45
    function value_95 () : java.math.BigDecimal {
      return reinstate.Charges[2].ChargeAmount
    }
    
    // 'value' attribute on TextCell (id=payer3_Cell) at DataUploadReinstateLV.pcf: line 95, column 41
    function value_99 () : java.lang.String {
      return reinstate.Charges[2].Payer
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadReinstateLV.pcf: line 23, column 46
    function visible_41 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get reinstate () : tdic.util.dataloader.data.sampledata.ReinstatementData {
      return getIteratedValue(1) as tdic.util.dataloader.data.sampledata.ReinstatementData
    }
    
    
  }
  
  
}