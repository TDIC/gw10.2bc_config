package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/PaymentInstrumentInputSet.misc.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentInstrumentInputSet_miscExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/PaymentInstrumentInputSet.misc.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentInstrumentInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=TokenForMisc_Input) at PaymentInstrumentInputSet.misc.pcf: line 15, column 40
    function valueRoot_1 () : java.lang.Object {
      return paymentInstrument
    }
    
    // 'value' attribute on TextInput (id=TokenForMisc_Input) at PaymentInstrumentInputSet.misc.pcf: line 15, column 40
    function value_0 () : java.lang.String {
      return paymentInstrument.Token
    }
    
    property get paymentInstrument () : PaymentInstrument {
      return getRequireValue("paymentInstrument", 0) as PaymentInstrument
    }
    
    property set paymentInstrument ($arg :  PaymentInstrument) {
      setRequireValue("paymentInstrument", 0, $arg)
    }
    
    
  }
  
  
}