package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailSummary.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailSummaryExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailSummary.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailSummaryExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=AccountDetailSummary) at AccountDetailSummary.pcf: line 10, column 72
    function canEdit_2 () : java.lang.Boolean {
      return perm.System.acctdetedit
    }
    
    // 'canVisit' attribute on Page (id=AccountDetailSummary) at AccountDetailSummary.pcf: line 10, column 72
    static function canVisit_3 (account :  Account) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctsummview
    }
    
    // 'def' attribute on ScreenRef at AccountDetailSummary.pcf: line 17, column 50
    function def_onEnter_0 (def :  pcf.AccountDetailSummaryScreen) : void {
      def.onEnter(account)
    }
    
    // 'def' attribute on ScreenRef at AccountDetailSummary.pcf: line 17, column 50
    function def_refreshVariables_1 (def :  pcf.AccountDetailSummaryScreen) : void {
      def.refreshVariables(account)
    }
    
    // Page (id=AccountDetailSummary) at AccountDetailSummary.pcf: line 10, column 72
    static function parent_4 (account :  Account) : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.AccountDetailSummary {
      return super.CurrentLocation as pcf.AccountDetailSummary
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  
}