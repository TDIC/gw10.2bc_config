package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountTransactionDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountTransactionDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountTransactionDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountTransactionDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (Account :  Account, Transaction :  Transaction) : int {
      return 0
    }
    
    // 'def' attribute on PanelRef at AccountTransactionDetail.pcf: line 22, column 49
    function def_onEnter_0 (def :  pcf.TransactionDetailDV) : void {
      def.onEnter(Transaction)
    }
    
    // 'def' attribute on PanelRef at AccountTransactionDetail.pcf: line 22, column 49
    function def_refreshVariables_1 (def :  pcf.TransactionDetailDV) : void {
      def.refreshVariables(Transaction)
    }
    
    // 'parent' attribute on Page (id=AccountTransactionDetail) at AccountTransactionDetail.pcf: line 9, column 105
    static function parent_2 (Account :  Account, Transaction :  Transaction) : pcf.api.Destination {
      return pcf.AccountSummary.createDestination(Account)
    }
    
    // 'title' attribute on Page (id=AccountTransactionDetail) at AccountTransactionDetail.pcf: line 9, column 105
    static function title_3 (Account :  Account, Transaction :  Transaction) : java.lang.Object {
      return DisplayKey.get("Web.AccountTransactionDetail.Title", Transaction.LongDisplayName)
    }
    
    property get Account () : Account {
      return getVariableValue("Account", 0) as Account
    }
    
    property set Account ($arg :  Account) {
      setVariableValue("Account", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.AccountTransactionDetail {
      return super.CurrentLocation as pcf.AccountTransactionDetail
    }
    
    property get Transaction () : Transaction {
      return getVariableValue("Transaction", 0) as Transaction
    }
    
    property set Transaction ($arg :  Transaction) {
      setVariableValue("Transaction", 0, $arg)
    }
    
    
  }
  
  
}