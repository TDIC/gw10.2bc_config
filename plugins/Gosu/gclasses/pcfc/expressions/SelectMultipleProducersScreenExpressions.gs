package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultipleProducersScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SelectMultipleProducersScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultipleProducersScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends SelectMultipleProducersScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=CancelButton) at SelectMultipleProducersScreen.pcf: line 39, column 64
    function action_1 () : void {
      CurrentLocation.cancel()
    }
    
    // 'def' attribute on PanelRef at SelectMultipleProducersScreen.pcf: line 29, column 48
    function def_onEnter_2 (def :  pcf.ProducerSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at SelectMultipleProducersScreen.pcf: line 43, column 108
    function def_onEnter_4 (def :  pcf.ProducerSearchResultsLV) : void {
      def.onEnter(producerSearchViews, null, isWizard, showHyperlinks, showCheckboxes)
    }
    
    // 'def' attribute on PanelRef at SelectMultipleProducersScreen.pcf: line 29, column 48
    function def_refreshVariables_3 (def :  pcf.ProducerSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at SelectMultipleProducersScreen.pcf: line 43, column 108
    function def_refreshVariables_5 (def :  pcf.ProducerSearchResultsLV) : void {
      def.refreshVariables(producerSearchViews, null, isWizard, showHyperlinks, showCheckboxes)
    }
    
    // 'pickValue' attribute on CheckedValuesToolbarButton (id=addbutton) at SelectMultipleProducersScreen.pcf: line 35, column 92
    function pickValue_0 (CheckedValues :  entity.ProducerSearchView[]) : Producer[] {
      return gw.api.web.search.SearchPopupUtil.getProducerArray(CheckedValues)
    }
    
    // 'searchCriteria' attribute on SearchPanel at SelectMultipleProducersScreen.pcf: line 27, column 86
    function searchCriteria_7 () : gw.search.ProducerSearchCriteria {
      return new gw.search.ProducerSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at SelectMultipleProducersScreen.pcf: line 27, column 86
    function search_6 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria, StringCriterionMode.TC_STARTSWITH)
    }
    
    property get producerSearchViews () : gw.api.database.IQueryBeanResult<ProducerSearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<ProducerSearchView>
    }
    
    property get searchCriteria () : gw.search.ProducerSearchCriteria {
      return getCriteriaValue(1) as gw.search.ProducerSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.ProducerSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultipleProducersScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SelectMultipleProducersScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get isWizard () : Boolean {
      return getVariableValue("isWizard", 0) as Boolean
    }
    
    property set isWizard ($arg :  Boolean) {
      setVariableValue("isWizard", 0, $arg)
    }
    
    property get showCheckboxes () : Boolean {
      return getVariableValue("showCheckboxes", 0) as Boolean
    }
    
    property set showCheckboxes ($arg :  Boolean) {
      setVariableValue("showCheckboxes", 0, $arg)
    }
    
    property get showHyperlinks () : Boolean {
      return getVariableValue("showHyperlinks", 0) as Boolean
    }
    
    property set showHyperlinks ($arg :  Boolean) {
      setVariableValue("showHyperlinks", 0, $arg)
    }
    
    
  }
  
  
}