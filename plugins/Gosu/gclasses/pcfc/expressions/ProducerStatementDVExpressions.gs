package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerStatementDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerStatementDVExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerStatementDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerStatementDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at ProducerStatementDV.pcf: line 19, column 38
    function valueRoot_1 () : java.lang.Object {
      return producerStatement
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at ProducerStatementDV.pcf: line 19, column 38
    function value_0 () : entity.Producer {
      return producerStatement.Producer
    }
    
    // 'value' attribute on TextInput (id=address_Input) at ProducerStatementDV.pcf: line 24, column 218
    function value_3 () : java.lang.String {
      return producerStatement.Producer.PrimaryContact.Contact typeis Person ? new gw.api.address.AddressFormatter().format(producerStatement.Producer.PrimaryContact.Contact.PrimaryAddress, "\n") : null
    }
    
    // 'value' attribute on TextInput (id=statementNumber_Input) at ProducerStatementDV.pcf: line 28, column 53
    function value_5 () : java.lang.String {
      return producerStatement.getDisplayName()
    }
    
    // 'value' attribute on TextInput (id=producerSupport_Input) at ProducerStatementDV.pcf: line 32, column 156
    function value_7 () : java.lang.String {
      return producerStatement.Producer.PrimaryContact.Contact typeis Person ? producerStatement.Producer.PrimaryContact.Contact.WorkPhoneValue : null
    }
    
    property get producerStatement () : ProducerStatement {
      return getRequireValue("producerStatement", 0) as ProducerStatement
    }
    
    property set producerStatement ($arg :  ProducerStatement) {
      setRequireValue("producerStatement", 0, $arg)
    }
    
    
  }
  
  
}