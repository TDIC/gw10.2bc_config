package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/chargepatterns/NewChargeBreakdownCategoryType.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewChargeBreakdownCategoryTypeExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/chargepatterns/NewChargeBreakdownCategoryType.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewChargeBreakdownCategoryTypeExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=NewChargeBreakdownCategoryType) at NewChargeBreakdownCategoryType.pcf: line 14, column 82
    function afterCancel_6 () : void {
      pcf.ChargeBreakdownCategoryTypes.go()
    }
    
    // 'afterCancel' attribute on Page (id=NewChargeBreakdownCategoryType) at NewChargeBreakdownCategoryType.pcf: line 14, column 82
    function afterCancel_dest_7 () : pcf.api.Destination {
      return pcf.ChargeBreakdownCategoryTypes.createDestination()
    }
    
    // 'afterCommit' attribute on Page (id=NewChargeBreakdownCategoryType) at NewChargeBreakdownCategoryType.pcf: line 14, column 82
    function afterCommit_8 (TopLocation :  pcf.api.Location) : void {
      pcf.ChargeBreakdownCategoryTypes.go()
    }
    
    // 'canVisit' attribute on Page (id=NewChargeBreakdownCategoryType) at NewChargeBreakdownCategoryType.pcf: line 14, column 82
    static function canVisit_9 () : java.lang.Boolean {
      return perm.System.chargebreakdowncategorytypecreate
    }
    
    // 'def' attribute on PanelRef at NewChargeBreakdownCategoryType.pcf: line 27, column 66
    function def_onEnter_2 (def :  pcf.ChargeBreakdownCategoryTypeDetailDV) : void {
      def.onEnter(categoryType)
    }
    
    // 'def' attribute on PanelRef at NewChargeBreakdownCategoryType.pcf: line 29, column 138
    function def_onEnter_4 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(categoryType, {"Name"}, { DisplayKey.get("Web.ChargeBreakdownCategoryType.Name") })
    }
    
    // 'def' attribute on PanelRef at NewChargeBreakdownCategoryType.pcf: line 27, column 66
    function def_refreshVariables_3 (def :  pcf.ChargeBreakdownCategoryTypeDetailDV) : void {
      def.refreshVariables(categoryType)
    }
    
    // 'def' attribute on PanelRef at NewChargeBreakdownCategoryType.pcf: line 29, column 138
    function def_refreshVariables_5 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(categoryType, {"Name"}, { DisplayKey.get("Web.ChargeBreakdownCategoryType.Name") })
    }
    
    // 'initialValue' attribute on Variable at NewChargeBreakdownCategoryType.pcf: line 20, column 43
    function initialValue_0 () : ChargeBreakdownCategoryType {
      return new ChargeBreakdownCategoryType()
    }
    
    // EditButtons at NewChargeBreakdownCategoryType.pcf: line 24, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'parent' attribute on Page (id=NewChargeBreakdownCategoryType) at NewChargeBreakdownCategoryType.pcf: line 14, column 82
    static function parent_10 () : pcf.api.Destination {
      return pcf.ChargeBreakdownCategoryTypes.createDestination()
    }
    
    override property get CurrentLocation () : pcf.NewChargeBreakdownCategoryType {
      return super.CurrentLocation as pcf.NewChargeBreakdownCategoryType
    }
    
    property get categoryType () : ChargeBreakdownCategoryType {
      return getVariableValue("categoryType", 0) as ChargeBreakdownCategoryType
    }
    
    property set categoryType ($arg :  ChargeBreakdownCategoryType) {
      setVariableValue("categoryType", 0, $arg)
    }
    
    
  }
  
  
}