package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/ActivityDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ActivityDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/activity/ActivityDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ActivityDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on PickerToolbarButton (id=ActivityDetailToolbarButtons_LinkDocumentButton) at ActivityDetailScreen.pcf: line 44, column 26
    function action_10 () : void {
      PickExistingDocumentPopup.push(documentContainer)
    }
    
    // 'action' attribute on PickerToolbarButton (id=ActivityDetailToolbarButtons_LinkDocumentButton) at ActivityDetailScreen.pcf: line 44, column 26
    function action_dest_11 () : pcf.api.Destination {
      return pcf.PickExistingDocumentPopup.createDestination(documentContainer)
    }
    
    // 'available' attribute on PickerToolbarButton (id=ActivityDetailToolbarButtons_LinkDocumentButton) at ActivityDetailScreen.pcf: line 44, column 26
    function available_9 () : java.lang.Boolean {
      return documentsActionsHelper.DocumentLinkActionsAvailable
    }
    
    // 'def' attribute on PanelRef at ActivityDetailScreen.pcf: line 53, column 36
    function def_onEnter_14 (def :  pcf.ActivityDetailDV_default) : void {
      def.onEnter(activity, assigneeHolder)
    }
    
    // 'def' attribute on PanelRef at ActivityDetailScreen.pcf: line 53, column 36
    function def_onEnter_16 (def :  pcf.ActivityDetailDV_disbappractivity) : void {
      def.onEnter(activity, assigneeHolder)
    }
    
    // 'def' attribute on PanelRef at ActivityDetailScreen.pcf: line 53, column 36
    function def_refreshVariables_15 (def :  pcf.ActivityDetailDV_default) : void {
      def.refreshVariables(activity, assigneeHolder)
    }
    
    // 'def' attribute on PanelRef at ActivityDetailScreen.pcf: line 53, column 36
    function def_refreshVariables_17 (def :  pcf.ActivityDetailDV_disbappractivity) : void {
      def.refreshVariables(activity, assigneeHolder)
    }
    
    // 'initialValue' attribute on Variable at ActivityDetailScreen.pcf: line 16, column 33
    function initialValue_0 () : DocumentContainer {
      return activity.DocumentContainer
    }
    
    // 'initialValue' attribute on Variable at ActivityDetailScreen.pcf: line 20, column 52
    function initialValue_1 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // 'mode' attribute on PanelRef at ActivityDetailScreen.pcf: line 53, column 36
    function mode_18 () : java.lang.Object {
      return activity.Subtype
    }
    
    // 'mode' attribute on ToolbarButtonSetRef at ActivityDetailScreen.pcf: line 24, column 86
    function mode_2 () : java.lang.Object {
      return getMode()
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=ActivityDetailToolbarButtons_LinkDocumentButton) at ActivityDetailScreen.pcf: line 44, column 26
    function onPick_12 (PickedValue :  Document) : void {
      activity.DocumentContainer.addDocument(PickedValue)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ActivityDetailScreen.pcf: line 24, column 86
    function toolbarButtonSet_onEnter_3 (def :  pcf.ActivityDetailToolbarButtonSet_approval) : void {
      def.onEnter(activity, assigneeHolder)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ActivityDetailScreen.pcf: line 24, column 86
    function toolbarButtonSet_onEnter_5 (def :  pcf.ActivityDetailToolbarButtonSet_default) : void {
      def.onEnter(activity, assigneeHolder)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ActivityDetailScreen.pcf: line 24, column 86
    function toolbarButtonSet_onEnter_7 (def :  pcf.ActivityDetailToolbarButtonSet_disbursementapproval) : void {
      def.onEnter(activity, assigneeHolder)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ActivityDetailScreen.pcf: line 24, column 86
    function toolbarButtonSet_refreshVariables_4 (def :  pcf.ActivityDetailToolbarButtonSet_approval) : void {
      def.refreshVariables(activity, assigneeHolder)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ActivityDetailScreen.pcf: line 24, column 86
    function toolbarButtonSet_refreshVariables_6 (def :  pcf.ActivityDetailToolbarButtonSet_default) : void {
      def.refreshVariables(activity, assigneeHolder)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at ActivityDetailScreen.pcf: line 24, column 86
    function toolbarButtonSet_refreshVariables_8 (def :  pcf.ActivityDetailToolbarButtonSet_disbursementapproval) : void {
      def.refreshVariables(activity, assigneeHolder)
    }
    
    // 'visible' attribute on Verbatim at ActivityDetailScreen.pcf: line 48, column 49
    function visible_13 () : java.lang.Boolean {
      return activity.Status == TC_CANCELED
    }
    
    property get activity () : Activity {
      return getRequireValue("activity", 0) as Activity
    }
    
    property set activity ($arg :  Activity) {
      setRequireValue("activity", 0, $arg)
    }
    
    property get assigneeHolder () : gw.api.assignment.Assignee[] {
      return getRequireValue("assigneeHolder", 0) as gw.api.assignment.Assignee[]
    }
    
    property set assigneeHolder ($arg :  gw.api.assignment.Assignee[]) {
      setRequireValue("assigneeHolder", 0, $arg)
    }
    
    property get documentContainer () : DocumentContainer {
      return getVariableValue("documentContainer", 0) as DocumentContainer
    }
    
    property set documentContainer ($arg :  DocumentContainer) {
      setVariableValue("documentContainer", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    function getMode(): String {
      if (activity typeis DisbApprActivity) {
        return "disbursementapproval"
      }
      return activity.ActivityPattern.Code
    }
    
    function createSearchCriteria(): NoteTemplateSearchCriteria {
      var rtn = new NoteTemplateSearchCriteria()
      // rtn.Language = Account.AccountHolder.Language
      rtn.AvailableSymbols = "policyperiod,policy,account"
      return rtn
    }
    
    
  }
  
  
}