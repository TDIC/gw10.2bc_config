package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/activityenhancement/AccountActivitiesPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountActivitiesPageExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/activityenhancement/AccountActivitiesPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountActivitiesPageExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at AccountActivitiesPage.pcf: line 24, column 94
    function filters_0 () : gw.api.filters.IFilter[] {
      return gw.acc.activityenhancement.ActivityFilterUtil.ActivityFilterOptions
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at AccountActivitiesPage.pcf: line 29, column 102
    function filters_1 () : gw.api.filters.IFilter[] {
      return gw.acc.activityenhancement.ActivityFilterUtil.ActivityPatternsFilterOptions
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at AccountActivitiesPage.pcf: line 34, column 109
    function filters_2 () : gw.api.filters.IFilter[] {
      return gw.acc.activityenhancement.ActivityFilterUtil.AccountActivitySubtypesFilterOptions
    }
    
    // Page (id=AccountActivitiesPage) at AccountActivitiesPage.pcf: line 7, column 80
    static function parent_38 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at AccountActivitiesPage.pcf: line 43, column 46
    function sortValue_3 (anActivity :  Activity) : java.lang.Object {
      return anActivity.Escalated
    }
    
    // 'value' attribute on TypeKeyCell (id=status_Cell) at AccountActivitiesPage.pcf: line 67, column 43
    function sortValue_4 (anActivity :  Activity) : java.lang.Object {
      return anActivity.Status
    }
    
    // 'value' attribute on TextCell (id=subject_Cell) at AccountActivitiesPage.pcf: line 72, column 43
    function sortValue_5 (anActivity :  Activity) : java.lang.Object {
      return anActivity.Subject
    }
    
    // 'value' attribute on DateCell (id=ActivityTargetDate_Cell) at AccountActivitiesPage.pcf: line 81, column 46
    function sortValue_6 (anActivity :  Activity) : java.lang.Object {
      return anActivity.TargetDate
    }
    
    // 'value' attribute on DateCell (id=ActivityCreateTime_Cell) at AccountActivitiesPage.pcf: line 87, column 46
    function sortValue_7 (anActivity :  Activity) : java.lang.Object {
      return anActivity.CreateTime
    }
    
    // 'value' attribute on RowIterator at AccountActivitiesPage.pcf: line 20, column 34
    function value_37 () : Activity[] {
      return account.Activities_Ext
    }
    
    override property get CurrentLocation () : pcf.AccountActivitiesPage {
      return super.CurrentLocation as pcf.AccountActivitiesPage
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/activityenhancement/AccountActivitiesPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountActivitiesPageExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=subject_Cell) at AccountActivitiesPage.pcf: line 72, column 43
    function action_15 () : void {
      ActivityDetailWorksheet.goInWorkspace(anActivity)
    }
    
    // 'action' attribute on TextCell (id=ActivityPolicyPeriod_Cell) at AccountActivitiesPage.pcf: line 99, column 48
    function action_32 () : void {
      PolicyDetailSummary.go(anActivity.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=subject_Cell) at AccountActivitiesPage.pcf: line 72, column 43
    function action_dest_16 () : pcf.api.Destination {
      return pcf.ActivityDetailWorksheet.createDestination(anActivity)
    }
    
    // 'action' attribute on TextCell (id=ActivityPolicyPeriod_Cell) at AccountActivitiesPage.pcf: line 99, column 48
    function action_dest_33 () : pcf.api.Destination {
      return pcf.PolicyDetailSummary.createDestination(anActivity.PolicyPeriod)
    }
    
    // 'value' attribute on TextCell (id=ActivityAssignByUser_Cell) at AccountActivitiesPage.pcf: line 77, column 58
    function valueRoot_21 () : java.lang.Object {
      return anActivity.CreateUser
    }
    
    // 'value' attribute on TextCell (id=ActivityAssignedUser_Cell) at AccountActivitiesPage.pcf: line 92, column 60
    function valueRoot_30 () : java.lang.Object {
      return anActivity.AssignedUser
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at AccountActivitiesPage.pcf: line 43, column 46
    function valueRoot_9 () : java.lang.Object {
      return anActivity
    }
    
    // 'value' attribute on TypeKeyCell (id=status_Cell) at AccountActivitiesPage.pcf: line 67, column 43
    function value_11 () : ActivityStatus {
      return anActivity.Status
    }
    
    // 'value' attribute on TextCell (id=subject_Cell) at AccountActivitiesPage.pcf: line 72, column 43
    function value_17 () : java.lang.String {
      return anActivity.Subject
    }
    
    // 'value' attribute on TextCell (id=ActivityAssignByUser_Cell) at AccountActivitiesPage.pcf: line 77, column 58
    function value_20 () : java.lang.String {
      return anActivity.CreateUser.DisplayName
    }
    
    // 'value' attribute on DateCell (id=ActivityTargetDate_Cell) at AccountActivitiesPage.pcf: line 81, column 46
    function value_23 () : java.util.Date {
      return anActivity.TargetDate
    }
    
    // 'value' attribute on DateCell (id=ActivityCreateTime_Cell) at AccountActivitiesPage.pcf: line 87, column 46
    function value_26 () : java.util.Date {
      return anActivity.CreateTime
    }
    
    // 'value' attribute on TextCell (id=ActivityAssignedUser_Cell) at AccountActivitiesPage.pcf: line 92, column 60
    function value_29 () : java.lang.String {
      return anActivity.AssignedUser.DisplayName
    }
    
    // 'value' attribute on TextCell (id=ActivityPolicyPeriod_Cell) at AccountActivitiesPage.pcf: line 99, column 48
    function value_34 () : entity.PolicyPeriod {
      return anActivity.PolicyPeriod
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at AccountActivitiesPage.pcf: line 43, column 46
    function value_8 () : java.lang.Boolean {
      return anActivity.Escalated
    }
    
    // 'valueType' attribute on TypeKeyCell (id=status_Cell) at AccountActivitiesPage.pcf: line 67, column 43
    function verifyValueType_14 () : void {
      var __valueTypeArg : ActivityStatus
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : gw.entity.TypeKey = __valueTypeArg
    }
    
    property get anActivity () : Activity {
      return getIteratedValue(1) as Activity
    }
    
    
  }
  
  
}