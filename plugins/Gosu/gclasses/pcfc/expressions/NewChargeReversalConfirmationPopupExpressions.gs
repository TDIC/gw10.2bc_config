package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/accounting/NewChargeReversalConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewChargeReversalConfirmationPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/accounting/NewChargeReversalConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewChargeReversalConfirmationPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (charge :  Charge) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=NewChargeReversalConfirmationPopup) at NewChargeReversalConfirmationPopup.pcf: line 12, column 106
    function beforeCommit_5 (pickedValue :  java.lang.Object) : void {
      reversal.reverse()
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalConfirmationPopup.pcf: line 31, column 58
    function def_onEnter_1 (def :  pcf.NewChargeReversalConfirmationDV) : void {
      def.onEnter(reversal)
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalConfirmationPopup.pcf: line 33, column 66
    function def_onEnter_3 (def :  pcf.ApprovalActivityDV) : void {
      def.onEnter(reversal.OpenApprovalActivity)
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalConfirmationPopup.pcf: line 31, column 58
    function def_refreshVariables_2 (def :  pcf.NewChargeReversalConfirmationDV) : void {
      def.refreshVariables(reversal)
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalConfirmationPopup.pcf: line 33, column 66
    function def_refreshVariables_4 (def :  pcf.ApprovalActivityDV) : void {
      def.refreshVariables(reversal.OpenApprovalActivity)
    }
    
    // 'startEditing' attribute on Popup (id=NewChargeReversalConfirmationPopup) at NewChargeReversalConfirmationPopup.pcf: line 12, column 106
    function startEditing_6 () : void {
      reversal = new ChargeReversal(); reversal.setChargeAndAddToBundle(charge); reversal.initiateApprovalActivityIfUserLacksAuthority()
    }
    
    // 'title' attribute on Popup (id=NewChargeReversalConfirmationPopup) at NewChargeReversalConfirmationPopup.pcf: line 12, column 106
    static function title_7 (charge :  Charge) : java.lang.Object {
      return DisplayKey.get("Web.NewChargeReversalConfirmationPopup.Title", charge.DisplayName)
    }
    
    // 'visible' attribute on AlertBar (id=ApprovalActivityAlertBar) at NewChargeReversalConfirmationPopup.pcf: line 29, column 58
    function visible_0 () : java.lang.Boolean {
      return reversal.OpenApprovalActivity != null
    }
    
    override property get CurrentLocation () : pcf.NewChargeReversalConfirmationPopup {
      return super.CurrentLocation as pcf.NewChargeReversalConfirmationPopup
    }
    
    property get charge () : Charge {
      return getVariableValue("charge", 0) as Charge
    }
    
    property set charge ($arg :  Charge) {
      setVariableValue("charge", 0, $arg)
    }
    
    property get reversal () : ChargeReversal {
      return getVariableValue("reversal", 0) as ChargeReversal
    }
    
    property set reversal ($arg :  ChargeReversal) {
      setVariableValue("reversal", 0, $arg)
    }
    
    
  }
  
  
}