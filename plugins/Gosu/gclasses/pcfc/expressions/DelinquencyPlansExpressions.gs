package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/businesssettings/DelinquencyPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyPlansExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/DelinquencyPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyPlansExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=DelinquencyPlans) at DelinquencyPlans.pcf: line 9, column 68
    static function canVisit_27 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.delplanview
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at DelinquencyPlans.pcf: line 29, column 80
    function filters_1 () : gw.api.filters.IFilter[] {
      return new gw.api.web.plan.PlanListFilterSet().FilterOptions
    }
    
    // 'initialValue' attribute on Variable at DelinquencyPlans.pcf: line 13, column 42
    function initialValue_0 () : gw.api.web.plan.PlanHelper {
      return new gw.api.web.plan.PlanHelper()
    }
    
    // Page (id=DelinquencyPlans) at DelinquencyPlans.pcf: line 9, column 68
    static function parent_28 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at DelinquencyPlans.pcf: line 38, column 46
    function sortValue_2 (delinquencyPlan :  entity.DelinquencyPlan) : java.lang.Object {
      return delinquencyPlan.PlanOrder
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at DelinquencyPlans.pcf: line 44, column 45
    function sortValue_3 (delinquencyPlan :  entity.DelinquencyPlan) : java.lang.Object {
      return delinquencyPlan.Name
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at DelinquencyPlans.pcf: line 54, column 54
    function sortValue_4 (delinquencyPlan :  entity.DelinquencyPlan) : java.lang.Object {
      return delinquencyPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at DelinquencyPlans.pcf: line 58, column 55
    function sortValue_5 (delinquencyPlan :  entity.DelinquencyPlan) : java.lang.Object {
      return delinquencyPlan.ExpirationDate
    }
    
    // 'value' attribute on RowIterator at DelinquencyPlans.pcf: line 24, column 86
    function value_26 () : gw.api.database.IQueryBeanResult<entity.DelinquencyPlan> {
      return gw.api.database.Query.make(DelinquencyPlan).select()
    }
    
    override property get CurrentLocation () : pcf.DelinquencyPlans {
      return super.CurrentLocation as pcf.DelinquencyPlans
    }
    
    property get PlanHelper () : gw.api.web.plan.PlanHelper {
      return getVariableValue("PlanHelper", 0) as gw.api.web.plan.PlanHelper
    }
    
    property set PlanHelper ($arg :  gw.api.web.plan.PlanHelper) {
      setVariableValue("PlanHelper", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/DelinquencyPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DelinquencyPlansExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=MoveUp) at DelinquencyPlans.pcf: line 69, column 61
    function action_22 () : void {
      PlanHelper.moveUp(delinquencyPlan);
    }
    
    // 'action' attribute on Link (id=MoveDown) at DelinquencyPlans.pcf: line 77, column 67
    function action_25 () : void {
      PlanHelper.moveDown(delinquencyPlan);
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at DelinquencyPlans.pcf: line 44, column 45
    function action_9 () : void {
      DelinquencyPlanDetail.go(delinquencyPlan)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at DelinquencyPlans.pcf: line 44, column 45
    function action_dest_10 () : pcf.api.Destination {
      return pcf.DelinquencyPlanDetail.createDestination(delinquencyPlan)
    }
    
    // 'available' attribute on Link (id=MoveUp) at DelinquencyPlans.pcf: line 69, column 61
    function available_20 () : java.lang.Boolean {
      return perm.System.delplanedit
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at DelinquencyPlans.pcf: line 38, column 46
    function valueRoot_7 () : java.lang.Object {
      return delinquencyPlan
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at DelinquencyPlans.pcf: line 44, column 45
    function value_11 () : java.lang.String {
      return delinquencyPlan.Name
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at DelinquencyPlans.pcf: line 54, column 54
    function value_14 () : java.util.Date {
      return delinquencyPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at DelinquencyPlans.pcf: line 58, column 55
    function value_17 () : java.util.Date {
      return delinquencyPlan.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at DelinquencyPlans.pcf: line 38, column 46
    function value_6 () : java.lang.Integer {
      return delinquencyPlan.PlanOrder
    }
    
    // 'visible' attribute on Link (id=MoveUp) at DelinquencyPlans.pcf: line 69, column 61
    function visible_21 () : java.lang.Boolean {
      return delinquencyPlan.PlanOrder > 1
    }
    
    // 'visible' attribute on Link (id=MoveDown) at DelinquencyPlans.pcf: line 77, column 67
    function visible_24 () : java.lang.Boolean {
      return !delinquencyPlan.hasHighestPlanOrder()
    }
    
    property get delinquencyPlan () : entity.DelinquencyPlan {
      return getIteratedValue(1) as entity.DelinquencyPlan
    }
    
    
  }
  
  
}