package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/AccountInvoiceItemsLV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountInvoiceItemsLV_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/invoice/AccountInvoiceItemsLV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountInvoiceItemsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_InstallmentNumber_Cell) at AccountInvoiceItemsLV.default.pcf: line 27, column 42
    function sortValue_0 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.InstallmentNumber
    }
    
    // 'value' attribute on DateCell (id=InvoiceItemsLV_EventDate_Cell) at AccountInvoiceItemsLV.default.pcf: line 35, column 62
    function sortValue_1 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.Date
    }
    
    // 'value' attribute on TypeKeyCell (id=InvoiceItemsLV_Product_Cell) at AccountInvoiceItemsLV.default.pcf: line 72, column 121
    function sortValue_11 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.PolicyPeriod.Policy.LOBCode
    }
    
    // 'value' attribute on TextCell (id=ClaimID_Cell) at AccountInvoiceItemsLV.default.pcf: line 78, column 25
    function sortValue_13 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.Charge.ClaimID_TDIC
    }
    
    // 'value' attribute on MonetaryAmountCell (id=InvoiceItemsLV_Amount_Cell) at AccountInvoiceItemsLV.default.pcf: line 86, column 39
    function sortValue_15 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=InvoiceItemsLV_PaidAmount_Cell) at AccountInvoiceItemsLV.default.pcf: line 91, column 43
    function sortValue_16 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.PaidAmount
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_Charge_Cell) at AccountInvoiceItemsLV.default.pcf: line 49, column 108
    function sortValue_3 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.Charge
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_Context_Cell) at AccountInvoiceItemsLV.default.pcf: line 55, column 152
    function sortValue_5 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.Charge.BillingInstruction
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_ChargeGroup_Cell) at AccountInvoiceItemsLV.default.pcf: line 60, column 67
    function sortValue_7 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.Charge.ChargeGroup
    }
    
    // 'value' attribute on TypeKeyCell (id=InvoiceItemsLV_Description_Cell) at AccountInvoiceItemsLV.default.pcf: line 66, column 62
    function sortValue_9 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.Type
    }
    
    // '$$sumValue' attribute on RowIterator at AccountInvoiceItemsLV.default.pcf: line 86, column 39
    function sumValueRoot_25 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem
    }
    
    // 'footerSumValue' attribute on RowIterator at AccountInvoiceItemsLV.default.pcf: line 86, column 39
    function sumValue_24 (invoiceItem :  entity.InvoiceLineItem) : java.lang.Object {
      return invoiceItem.Amount
    }
    
    // 'value' attribute on RowIterator at AccountInvoiceItemsLV.default.pcf: line 19, column 44
    function value_74 () : entity.InvoiceLineItem[] {
      return invoice.getAggregatedInvoiceItems(aggregation)
    }
    
    // 'visible' attribute on TypeKeyCell (id=InvoiceItemsLV_Product_Cell) at AccountInvoiceItemsLV.default.pcf: line 72, column 121
    function visible_12 () : java.lang.Boolean {
      return aggregation != AggregationType.TC_CATEGORIES && aggregation != AggregationType.TC_CONTEXTS
    }
    
    // 'visible' attribute on TextCell (id=ClaimID_Cell) at AccountInvoiceItemsLV.default.pcf: line 78, column 25
    function visible_14 () : java.lang.Boolean {
      return aggregation == AggregationType.TC_CHARGES
    }
    
    // 'visible' attribute on DateCell (id=InvoiceItemsLV_EventDate_Cell) at AccountInvoiceItemsLV.default.pcf: line 35, column 62
    function visible_2 () : java.lang.Boolean {
      return aggregation==AggregationType.TC_CHARGES
    }
    
    // 'visible' attribute on TextCell (id=InvoiceItemsLV_Charge_Cell) at AccountInvoiceItemsLV.default.pcf: line 49, column 108
    function visible_4 () : java.lang.Boolean {
      return aggregation==AggregationType.TC_CATEGORIES or aggregation==AggregationType.TC_CHARGES
    }
    
    // 'visible' attribute on TextCell (id=InvoiceItemsLV_Context_Cell) at AccountInvoiceItemsLV.default.pcf: line 55, column 152
    function visible_6 () : java.lang.Boolean {
      return aggregation==AggregationType.TC_CONTEXTS or aggregation==AggregationType.TC_CATEGORIES or aggregation==AggregationType.TC_CHARGES
    }
    
    // 'visible' attribute on TextCell (id=InvoiceItemsLV_ChargeGroup_Cell) at AccountInvoiceItemsLV.default.pcf: line 60, column 67
    function visible_8 () : java.lang.Boolean {
      return aggregation==AggregationType.TC_CHARGEGROUPS
    }
    
    property get aggregation () : typekey.AggregationType {
      return getRequireValue("aggregation", 0) as typekey.AggregationType
    }
    
    property set aggregation ($arg :  typekey.AggregationType) {
      setRequireValue("aggregation", 0, $arg)
    }
    
    property get invoice () : AccountInvoice {
      return getRequireValue("invoice", 0) as AccountInvoice
    }
    
    property set invoice ($arg :  AccountInvoice) {
      setRequireValue("invoice", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/invoice/AccountInvoiceItemsLV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountInvoiceItemsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=InvoiceItemsLV_Policy_Cell) at AccountInvoiceItemsLV.default.pcf: line 43, column 25
    function actionAvailable_37 () : java.lang.Boolean {
      return invoiceItem.PolicyPeriod != null
    }
    
    // 'action' attribute on DateCell (id=InvoiceItemsLV_EventDate_Cell) at AccountInvoiceItemsLV.default.pcf: line 35, column 62
    function action_29 () : void {
      InvoiceItemDetailPopup.push(invoiceItem.InvoiceItem)
    }
    
    // 'action' attribute on TextCell (id=InvoiceItemsLV_Policy_Cell) at AccountInvoiceItemsLV.default.pcf: line 43, column 25
    function action_35 () : void {
      PolicySummary.push(invoiceItem.PolicyPeriod)
    }
    
    // 'action' attribute on MonetaryAmountCell (id=InvoiceItemsLV_Amount_Cell) at AccountInvoiceItemsLV.default.pcf: line 86, column 39
    function action_64 () : void {
      InvoiceItemDetailPopup.push(invoiceItem.InvoiceItem)
    }
    
    // 'action' attribute on DateCell (id=InvoiceItemsLV_EventDate_Cell) at AccountInvoiceItemsLV.default.pcf: line 35, column 62
    function action_dest_30 () : pcf.api.Destination {
      return pcf.InvoiceItemDetailPopup.createDestination(invoiceItem.InvoiceItem)
    }
    
    // 'action' attribute on TextCell (id=InvoiceItemsLV_Policy_Cell) at AccountInvoiceItemsLV.default.pcf: line 43, column 25
    function action_dest_36 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(invoiceItem.PolicyPeriod)
    }
    
    // 'action' attribute on MonetaryAmountCell (id=InvoiceItemsLV_Amount_Cell) at AccountInvoiceItemsLV.default.pcf: line 86, column 39
    function action_dest_65 () : pcf.api.Destination {
      return pcf.InvoiceItemDetailPopup.createDestination(invoiceItem.InvoiceItem)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=InvoiceItemsLV_Amount_Cell) at AccountInvoiceItemsLV.default.pcf: line 86, column 39
    function currency_68 () : typekey.Currency {
      return invoiceItem.InvoiceItem.Currency
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_InstallmentNumber_Cell) at AccountInvoiceItemsLV.default.pcf: line 27, column 42
    function valueRoot_27 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_Context_Cell) at AccountInvoiceItemsLV.default.pcf: line 55, column 152
    function valueRoot_45 () : java.lang.Object {
      return invoiceItem.Charge
    }
    
    // 'value' attribute on TypeKeyCell (id=InvoiceItemsLV_Product_Cell) at AccountInvoiceItemsLV.default.pcf: line 72, column 121
    function valueRoot_57 () : java.lang.Object {
      return invoiceItem.PolicyPeriod.Policy
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_InstallmentNumber_Cell) at AccountInvoiceItemsLV.default.pcf: line 27, column 42
    function value_26 () : java.lang.Integer {
      return invoiceItem.InstallmentNumber
    }
    
    // 'value' attribute on DateCell (id=InvoiceItemsLV_EventDate_Cell) at AccountInvoiceItemsLV.default.pcf: line 35, column 62
    function value_31 () : java.util.Date {
      return invoiceItem.Date
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_Policy_Cell) at AccountInvoiceItemsLV.default.pcf: line 43, column 25
    function value_38 () : java.lang.String {
      return invoiceItem.PolicyPeriod == null ? DisplayKey.get("Web.InvoiceItemsLV.AccountCharge") : invoiceItem.PolicyPeriod.DisplayName
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_Charge_Cell) at AccountInvoiceItemsLV.default.pcf: line 49, column 108
    function value_40 () : entity.Charge {
      return invoiceItem.Charge
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_Context_Cell) at AccountInvoiceItemsLV.default.pcf: line 55, column 152
    function value_44 () : entity.BillingInstruction {
      return invoiceItem.Charge.BillingInstruction
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemsLV_ChargeGroup_Cell) at AccountInvoiceItemsLV.default.pcf: line 60, column 67
    function value_48 () : java.lang.String {
      return invoiceItem.Charge.ChargeGroup
    }
    
    // 'value' attribute on TypeKeyCell (id=InvoiceItemsLV_Description_Cell) at AccountInvoiceItemsLV.default.pcf: line 66, column 62
    function value_52 () : typekey.InvoiceItemType {
      return invoiceItem.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=InvoiceItemsLV_Product_Cell) at AccountInvoiceItemsLV.default.pcf: line 72, column 121
    function value_56 () : typekey.LOBCode {
      return invoiceItem.PolicyPeriod.Policy.LOBCode
    }
    
    // 'value' attribute on TextCell (id=ClaimID_Cell) at AccountInvoiceItemsLV.default.pcf: line 78, column 25
    function value_60 () : java.lang.String {
      return invoiceItem.Charge.ClaimID_TDIC
    }
    
    // 'value' attribute on MonetaryAmountCell (id=InvoiceItemsLV_Amount_Cell) at AccountInvoiceItemsLV.default.pcf: line 86, column 39
    function value_66 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=InvoiceItemsLV_PaidAmount_Cell) at AccountInvoiceItemsLV.default.pcf: line 91, column 43
    function value_70 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.PaidAmount
    }
    
    // 'visible' attribute on DateCell (id=InvoiceItemsLV_EventDate_Cell) at AccountInvoiceItemsLV.default.pcf: line 35, column 62
    function visible_33 () : java.lang.Boolean {
      return aggregation==AggregationType.TC_CHARGES
    }
    
    // 'visible' attribute on TextCell (id=InvoiceItemsLV_Charge_Cell) at AccountInvoiceItemsLV.default.pcf: line 49, column 108
    function visible_42 () : java.lang.Boolean {
      return aggregation==AggregationType.TC_CATEGORIES or aggregation==AggregationType.TC_CHARGES
    }
    
    // 'visible' attribute on TextCell (id=InvoiceItemsLV_Context_Cell) at AccountInvoiceItemsLV.default.pcf: line 55, column 152
    function visible_46 () : java.lang.Boolean {
      return aggregation==AggregationType.TC_CONTEXTS or aggregation==AggregationType.TC_CATEGORIES or aggregation==AggregationType.TC_CHARGES
    }
    
    // 'visible' attribute on TextCell (id=InvoiceItemsLV_ChargeGroup_Cell) at AccountInvoiceItemsLV.default.pcf: line 60, column 67
    function visible_50 () : java.lang.Boolean {
      return aggregation==AggregationType.TC_CHARGEGROUPS
    }
    
    // 'visible' attribute on TypeKeyCell (id=InvoiceItemsLV_Product_Cell) at AccountInvoiceItemsLV.default.pcf: line 72, column 121
    function visible_58 () : java.lang.Boolean {
      return aggregation != AggregationType.TC_CATEGORIES && aggregation != AggregationType.TC_CONTEXTS
    }
    
    // 'visible' attribute on TextCell (id=ClaimID_Cell) at AccountInvoiceItemsLV.default.pcf: line 78, column 25
    function visible_62 () : java.lang.Boolean {
      return aggregation == AggregationType.TC_CHARGES
    }
    
    property get invoiceItem () : entity.InvoiceLineItem {
      return getIteratedValue(1) as entity.InvoiceLineItem
    }
    
    
  }
  
  
}