package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/ChangeInvoicingOverridesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChangeInvoicingOverridesPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/ChangeInvoicingOverridesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChangeInvoicingOverridesPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'pickLocation' attribute on PickerInput (id=OverridingPayerAccount_Input) at ChangeInvoicingOverridesPopup.pcf: line 35, column 40
    function action_2 () : void {
      AccountSearchPopup.push()
    }
    
    // 'action' attribute on PickerInput (id=OverridingPayerAccount_Input) at ChangeInvoicingOverridesPopup.pcf: line 35, column 40
    function action_5 () : void {
      AccountOverview.go(policyPeriod.OverridingPayerAccount)
    }
    
    // 'pickLocation' attribute on PickerInput (id=OverridingPayerAccount_Input) at ChangeInvoicingOverridesPopup.pcf: line 35, column 40
    function action_dest_3 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'action' attribute on PickerInput (id=OverridingPayerAccount_Input) at ChangeInvoicingOverridesPopup.pcf: line 35, column 40
    function action_dest_6 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(policyPeriod.OverridingPayerAccount)
    }
    
    // 'beforeCommit' attribute on Popup (id=ChangeInvoicingOverridesPopup) at ChangeInvoicingOverridesPopup.pcf: line 10, column 81
    function beforeCommit_20 (pickedValue :  java.lang.Object) : void {
      invoicingOverridesView.update()
    }
    
    // 'value' attribute on RangeInput (id=OverridingInvoiceStream_Input) at ChangeInvoicingOverridesPopup.pcf: line 44, column 47
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoicingOverridesView.OverridingInvoiceStream = (__VALUE_TO_SET as entity.InvoiceStream)
    }
    
    // 'value' attribute on PickerInput (id=OverridingPayerAccount_Input) at ChangeInvoicingOverridesPopup.pcf: line 35, column 40
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoicingOverridesView.OverridingPayerAccount = (__VALUE_TO_SET as entity.Account)
    }
    
    // 'initialValue' attribute on Variable at ChangeInvoicingOverridesPopup.pcf: line 19, column 49
    function initialValue_0 () : gw.invoice.InvoicingOverridesView {
      return new gw.invoice.InvoicingOverridesView(policyPeriod)
    }
    
    // 'inputConversion' attribute on PickerInput (id=OverridingPayerAccount_Input) at ChangeInvoicingOverridesPopup.pcf: line 35, column 40
    function inputConversion_7 (VALUE :  java.lang.String) : java.lang.Object {
      return (VALUE==null or VALUE =="") ?  null : gw.api.database.Query.make(Account).compare("AccountName", Equals, VALUE).select().AtMostOneRow 
    }
    
    // EditButtons at ChangeInvoicingOverridesPopup.pcf: line 22, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'validationExpression' attribute on PickerInput (id=OverridingPayerAccount_Input) at ChangeInvoicingOverridesPopup.pcf: line 35, column 40
    function validationExpression_4 () : java.lang.Object {
      return validateOverridingPayer()
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingInvoiceStream_Input) at ChangeInvoicingOverridesPopup.pcf: line 44, column 47
    function valueRange_16 () : java.lang.Object {
      return gw.api.domain.invoice.InvoiceStreams.getCompatibleInvoiceStreams(invoicingOverridesView.DefaultPayer, policyPeriod.PaymentPlan)
    }
    
    // 'value' attribute on PickerInput (id=OverridingPayerAccount_Input) at ChangeInvoicingOverridesPopup.pcf: line 35, column 40
    function valueRoot_10 () : java.lang.Object {
      return invoicingOverridesView
    }
    
    // 'value' attribute on RangeInput (id=OverridingInvoiceStream_Input) at ChangeInvoicingOverridesPopup.pcf: line 44, column 47
    function value_13 () : entity.InvoiceStream {
      return invoicingOverridesView.OverridingInvoiceStream
    }
    
    // 'value' attribute on PickerInput (id=OverridingPayerAccount_Input) at ChangeInvoicingOverridesPopup.pcf: line 35, column 40
    function value_8 () : entity.Account {
      return invoicingOverridesView.OverridingPayerAccount
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingInvoiceStream_Input) at ChangeInvoicingOverridesPopup.pcf: line 44, column 47
    function verifyValueRangeIsAllowedType_17 ($$arg :  entity.InvoiceStream[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingInvoiceStream_Input) at ChangeInvoicingOverridesPopup.pcf: line 44, column 47
    function verifyValueRangeIsAllowedType_17 ($$arg :  gw.api.database.IQueryBeanResult<entity.InvoiceStream>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingInvoiceStream_Input) at ChangeInvoicingOverridesPopup.pcf: line 44, column 47
    function verifyValueRangeIsAllowedType_17 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OverridingInvoiceStream_Input) at ChangeInvoicingOverridesPopup.pcf: line 44, column 47
    function verifyValueRange_18 () : void {
      var __valueRangeArg = gw.api.domain.invoice.InvoiceStreams.getCompatibleInvoiceStreams(invoicingOverridesView.DefaultPayer, policyPeriod.PaymentPlan)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_17(__valueRangeArg)
    }
    
    override property get CurrentLocation () : pcf.ChangeInvoicingOverridesPopup {
      return super.CurrentLocation as pcf.ChangeInvoicingOverridesPopup
    }
    
    property get invoicingOverridesView () : gw.invoice.InvoicingOverridesView {
      return getVariableValue("invoicingOverridesView", 0) as gw.invoice.InvoicingOverridesView
    }
    
    property set invoicingOverridesView ($arg :  gw.invoice.InvoicingOverridesView) {
      setVariableValue("invoicingOverridesView", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    function validateOverridingPayer(): String {
      
      if (invoicingOverridesView.OverridingPayerAccount != null && policyPeriod.getCurrency()!= invoicingOverridesView.OverridingPayerAccount.getCurrency()){
        return DisplayKey.get("Java.Error.Currency.Mismatched",
            policyPeriod.getIntrinsicType().getDisplayName(), policyPeriod.getDisplayName(),
                policyPeriod.getCurrency(), DisplayKey.get("Web.ChangeInvoicingOverridesPopup.OverridingPayerAccount"), invoicingOverridesView.OverridingPayerAccount.getCurrency());
      }
    
      return gw.api.web.account.PolicyPeriods.checkForOverridingPayerAccountError(policyPeriod, invoicingOverridesView.OverridingPayerAccount)
    }
    
    
  }
  
  
}