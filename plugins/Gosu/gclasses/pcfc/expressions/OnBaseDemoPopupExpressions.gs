package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/demo/OnBaseDemoPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OnBaseDemoPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/demo/OnBaseDemoPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OnBaseDemoPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=customQueryButton) at OnBaseDemoPopup.pcf: line 34, column 51
    function action_2 () : void {
      scrapeXml[0] = acc.onbase.util.OnBaseDemoUtils.constructCustomQueryURL(account)
    }
    
    // 'action' attribute on ToolbarButton (id=onFoldersButton) at OnBaseDemoPopup.pcf: line 39, column 53
    function action_3 () : void {
      scrapeXml[0] = acc.onbase.util.OnBaseDemoUtils.constructFolderURL(account)
    }
    
    // 'action' attribute on ToolbarButton (id=uploadButton) at OnBaseDemoPopup.pcf: line 44, column 54
    function action_4 () : void {
      scrapeXml[0] = acc.onbase.util.OnBaseDemoUtils.uploadDocURL(account)
    }
    
    // 'action' attribute on ToolbarButton (id=uFormButton) at OnBaseDemoPopup.pcf: line 49, column 56
    function action_5 () : void {
      scrapeXml[0] = acc.onbase.util.OnBaseDemoUtils.unityFormURL(account)
    }
    
    // 'action' attribute on ToolbarButton (id=docPacketButton) at OnBaseDemoPopup.pcf: line 54, column 58
    function action_6 () : void {
      scrapeXml[0] = acc.onbase.util.OnBaseDemoUtils.generateDocPacketURL(account, "BIL - Account Packet")
    }
    
    // 'def' attribute on PanelRef at OnBaseDemoPopup.pcf: line 27, column 55
    function def_onEnter_7 (def :  pcf.OnBaseAePopFramePanelSet) : void {
      def.onEnter(scrapeXml)
    }
    
    // 'def' attribute on PanelRef at OnBaseDemoPopup.pcf: line 27, column 55
    function def_refreshVariables_8 (def :  pcf.OnBaseAePopFramePanelSet) : void {
      def.refreshVariables(scrapeXml)
    }
    
    // 'initialValue' attribute on Variable at OnBaseDemoPopup.pcf: line 16, column 24
    function initialValue_0 () : String[] {
      return new String[]{""}
    }
    
    // 'initialValue' attribute on Variable at OnBaseDemoPopup.pcf: line 20, column 23
    function initialValue_1 () : Account {
      return account
    }
    
    override property get CurrentLocation () : pcf.OnBaseDemoPopup {
      return super.CurrentLocation as pcf.OnBaseDemoPopup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get scrapeXml () : String[] {
      return getVariableValue("scrapeXml", 0) as String[]
    }
    
    property set scrapeXml ($arg :  String[]) {
      setVariableValue("scrapeXml", 0, $arg)
    }
    
    
  }
  
  
}