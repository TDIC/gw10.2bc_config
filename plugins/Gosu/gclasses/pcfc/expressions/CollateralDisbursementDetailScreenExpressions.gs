package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralDisbursementDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CollateralDisbursementDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/collateral/CollateralDisbursementDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CollateralDisbursementDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=unapplied_Input) at CollateralDisbursementDetailScreen.pcf: line 44, column 80
    function currency_10 () : typekey.Currency {
      return disbursementVar.Currency
    }
    
    // 'def' attribute on PanelRef at CollateralDisbursementDetailScreen.pcf: line 55, column 64
    function def_onEnter_16 (def :  pcf.CreateDisbursementDetailDV) : void {
      def.onEnter(disbursementVar, true)
    }
    
    // 'def' attribute on PanelRef at CollateralDisbursementDetailScreen.pcf: line 55, column 64
    function def_refreshVariables_17 (def :  pcf.CreateDisbursementDetailDV) : void {
      def.refreshVariables(disbursementVar, true)
    }
    
    // 'initialValue' attribute on Variable at CollateralDisbursementDetailScreen.pcf: line 13, column 30
    function initialValue_0 () : java.util.Date {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'initialValue' attribute on Variable at CollateralDisbursementDetailScreen.pcf: line 17, column 23
    function initialValue_1 () : Boolean {
      return perm.Transaction.disbpayeeedit
    }
    
    // 'initialValue' attribute on Variable at CollateralDisbursementDetailScreen.pcf: line 21, column 49
    function initialValue_2 () : gw.api.web.account.CollateralUtil {
      return new gw.api.web.account.CollateralUtil()
    }
    
    // 'value' attribute on MonetaryAmountInput (id=pending_Input) at CollateralDisbursementDetailScreen.pcf: line 51, column 67
    function valueRoot_13 () : java.lang.Object {
      return disbursementVar.Collateral
    }
    
    // 'value' attribute on TextInput (id=account_Input) at CollateralDisbursementDetailScreen.pcf: line 33, column 69
    function valueRoot_4 () : java.lang.Object {
      return disbursementVar.Collateral.Account
    }
    
    // 'value' attribute on MonetaryAmountInput (id=pending_Input) at CollateralDisbursementDetailScreen.pcf: line 51, column 67
    function value_12 () : gw.pl.currency.MonetaryAmount {
      return disbursementVar.Collateral.PendingDisbursement
    }
    
    // 'value' attribute on TextInput (id=account_Input) at CollateralDisbursementDetailScreen.pcf: line 33, column 69
    function value_3 () : java.lang.String {
      return disbursementVar.Collateral.Account.AccountNumber
    }
    
    // 'value' attribute on TextInput (id=insured_Input) at CollateralDisbursementDetailScreen.pcf: line 37, column 76
    function value_6 () : java.lang.String {
      return disbursementVar.Collateral.Account.AccountNameLocalized
    }
    
    // 'value' attribute on MonetaryAmountInput (id=unapplied_Input) at CollateralDisbursementDetailScreen.pcf: line 44, column 80
    function value_9 () : gw.pl.currency.MonetaryAmount {
      return collateralUtil.getTotalCashHeld(disbursementVar.Collateral)
    }
    
    property get CanEditDisbursementPayee () : Boolean {
      return getVariableValue("CanEditDisbursementPayee", 0) as Boolean
    }
    
    property set CanEditDisbursementPayee ($arg :  Boolean) {
      setVariableValue("CanEditDisbursementPayee", 0, $arg)
    }
    
    property get collateralUtil () : gw.api.web.account.CollateralUtil {
      return getVariableValue("collateralUtil", 0) as gw.api.web.account.CollateralUtil
    }
    
    property set collateralUtil ($arg :  gw.api.web.account.CollateralUtil) {
      setVariableValue("collateralUtil", 0, $arg)
    }
    
    property get disbursementVar () : CollateralDisbursement {
      return getRequireValue("disbursementVar", 0) as CollateralDisbursement
    }
    
    property set disbursementVar ($arg :  CollateralDisbursement) {
      setRequireValue("disbursementVar", 0, $arg)
    }
    
    property get today () : java.util.Date {
      return getVariableValue("today", 0) as java.util.Date
    }
    
    property set today ($arg :  java.util.Date) {
      setVariableValue("today", 0, $arg)
    }
    
    
  }
  
  
}