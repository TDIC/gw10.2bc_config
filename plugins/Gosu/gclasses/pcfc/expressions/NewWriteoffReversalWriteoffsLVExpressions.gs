package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalWriteoffsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewWriteoffReversalWriteoffsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalWriteoffsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends NewWriteoffReversalWriteoffsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=Select) at NewWriteoffReversalWriteoffsLV.pcf: line 31, column 46
    function action_2 () : void {
      reversal.setWriteoffAndAddToBundle(writeOff); reversal.initiateApprovalActivityIfUserLacksAuthority(); (CurrentLocation as pcf.api.Wizard).next();
    }
    
    // 'available' attribute on Link (id=Select) at NewWriteoffReversalWriteoffsLV.pcf: line 31, column 46
    function available_0 () : java.lang.Boolean {
      return writeOff.canReverse()
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at NewWriteoffReversalWriteoffsLV.pcf: line 55, column 36
    function currency_13 () : typekey.Currency {
      return writeOff.Currency
    }
    
    // 'value' attribute on DateCell (id=WriteoffDate_Cell) at NewWriteoffReversalWriteoffsLV.pcf: line 37, column 40
    function valueRoot_5 () : java.lang.Object {
      return writeOff
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at NewWriteoffReversalWriteoffsLV.pcf: line 55, column 36
    function value_11 () : gw.pl.currency.MonetaryAmount {
      return writeOff.Amount
    }
    
    // 'value' attribute on DateCell (id=WriteoffDate_Cell) at NewWriteoffReversalWriteoffsLV.pcf: line 37, column 40
    function value_4 () : java.util.Date {
      return writeOff.CreateTime
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at NewWriteoffReversalWriteoffsLV.pcf: line 43, column 40
    function value_7 () : entity.Writeoff {
      return writeOff
    }
    
    // 'value' attribute on TextCell (id=Owner_Cell) at NewWriteoffReversalWriteoffsLV.pcf: line 48, column 66
    function value_9 () : java.lang.String {
      return writeOff.getTAccountOwnerTypeAndDisplayName()
    }
    
    property get writeOff () : entity.Writeoff {
      return getIteratedValue(1) as entity.Writeoff
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalWriteoffsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewWriteoffReversalWriteoffsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RowIterator (id=Writeoffs) at NewWriteoffReversalWriteoffsLV.pcf: line 19, column 75
    function value_15 () : gw.api.database.IQueryBeanResult<entity.Writeoff> {
      return writeOffs
    }
    
    property get reversal () : WriteoffReversal {
      return getRequireValue("reversal", 0) as WriteoffReversal
    }
    
    property set reversal ($arg :  WriteoffReversal) {
      setRequireValue("reversal", 0, $arg)
    }
    
    property get writeOffs () : gw.api.database.IQueryBeanResult<Writeoff> {
      return getRequireValue("writeOffs", 0) as gw.api.database.IQueryBeanResult<Writeoff>
    }
    
    property set writeOffs ($arg :  gw.api.database.IQueryBeanResult<Writeoff>) {
      setRequireValue("writeOffs", 0, $arg)
    }
    
    
  }
  
  
}