package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/billing/CloneBillingPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CloneBillingPlanExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/billing/CloneBillingPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CloneBillingPlanExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (billingPlan :  BillingPlan, includeSpecs :  boolean) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=CloneBillingPlan) at CloneBillingPlan.pcf: line 13, column 81
    function afterCancel_3 () : void {
      BillingPlanDetail.go(billingPlan)
    }
    
    // 'afterCancel' attribute on Page (id=CloneBillingPlan) at CloneBillingPlan.pcf: line 13, column 81
    function afterCancel_dest_4 () : pcf.api.Destination {
      return pcf.BillingPlanDetail.createDestination(billingPlan)
    }
    
    // 'afterCommit' attribute on Page (id=CloneBillingPlan) at CloneBillingPlan.pcf: line 13, column 81
    function afterCommit_5 (TopLocation :  pcf.api.Location) : void {
      BillingPlanDetail.go(clonedBillingPlan)
    }
    
    // 'def' attribute on ScreenRef at CloneBillingPlan.pcf: line 27, column 57
    function def_onEnter_1 (def :  pcf.BillingPlanDetailScreen) : void {
      def.onEnter(clonedBillingPlan)
    }
    
    // 'def' attribute on ScreenRef at CloneBillingPlan.pcf: line 27, column 57
    function def_refreshVariables_2 (def :  pcf.BillingPlanDetailScreen) : void {
      def.refreshVariables(clonedBillingPlan)
    }
    
    // 'initialValue' attribute on Variable at CloneBillingPlan.pcf: line 25, column 34
    function initialValue_0 () : entity.BillingPlan {
      return gw.plugin.Plugins.get(gw.module.IFeesThresholds).cloneBillingPlan(billingPlan, includeSpecs)
    }
    
    // 'parent' attribute on Page (id=CloneBillingPlan) at CloneBillingPlan.pcf: line 13, column 81
    static function parent_6 (billingPlan :  BillingPlan, includeSpecs :  boolean) : pcf.api.Destination {
      return pcf.BillingPlans.createDestination()
    }
    
    // 'title' attribute on Page (id=CloneBillingPlan) at CloneBillingPlan.pcf: line 13, column 81
    static function title_7 (billingPlan :  BillingPlan, includeSpecs :  boolean) : java.lang.Object {
      return DisplayKey.get("Web.CloneBillingPlan.Title", billingPlan)
    }
    
    override property get CurrentLocation () : pcf.CloneBillingPlan {
      return super.CurrentLocation as pcf.CloneBillingPlan
    }
    
    property get billingPlan () : BillingPlan {
      return getVariableValue("billingPlan", 0) as BillingPlan
    }
    
    property set billingPlan ($arg :  BillingPlan) {
      setVariableValue("billingPlan", 0, $arg)
    }
    
    property get clonedBillingPlan () : entity.BillingPlan {
      return getVariableValue("clonedBillingPlan", 0) as entity.BillingPlan
    }
    
    property set clonedBillingPlan ($arg :  entity.BillingPlan) {
      setVariableValue("clonedBillingPlan", 0, $arg)
    }
    
    property get includeSpecs () : boolean {
      return getVariableValue("includeSpecs", 0) as java.lang.Boolean
    }
    
    property set includeSpecs ($arg :  boolean) {
      setVariableValue("includeSpecs", 0, $arg)
    }
    
    
  }
  
  
}