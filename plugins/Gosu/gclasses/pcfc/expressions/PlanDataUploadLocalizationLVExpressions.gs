package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadLocalizationLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanDataUploadLocalizationLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadLocalizationLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PlanDataUploadLocalizationLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=commPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 72, column 41
    function label_100 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.CommissionPlan")
    }
    
    // 'label' attribute on TextCell (id=commPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 77, column 41
    function label_105 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.CommissionPlanName")
    }
    
    // 'label' attribute on TextCell (id=commSubPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 82, column 41
    function label_110 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.CommissionSubPlan")
    }
    
    // 'label' attribute on TextCell (id=commSubPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 87, column 41
    function label_115 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.CommissionSubPlanName")
    }
    
    // 'label' attribute on TextCell (id=agencyBillPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 92, column 41
    function label_120 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.AgencyBillPlan")
    }
    
    // 'label' attribute on TextCell (id=agencyBillPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 97, column 41
    function label_125 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.AgencyBillPlanName")
    }
    
    // 'label' attribute on TextCell (id=payAllocationPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 102, column 41
    function label_130 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.PayAllocationPlan")
    }
    
    // 'label' attribute on TextCell (id=payAllocationPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 107, column 41
    function label_135 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.PayAllocationPlanName")
    }
    
    // 'label' attribute on TextCell (id=payAllocationPlanDesc_Cell) at PlanDataUploadLocalizationLV.pcf: line 112, column 41
    function label_140 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.PayAllocationPlanDesc")
    }
    
    // 'label' attribute on TextCell (id=returnPremiumPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 117, column 41
    function label_145 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.ReturnPremiumPlan")
    }
    
    // 'label' attribute on TextCell (id=returnPremiumPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 122, column 41
    function label_150 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.ReturnPremiumPlanName")
    }
    
    // 'label' attribute on TextCell (id=returnPremiumPlanDesc_Cell) at PlanDataUploadLocalizationLV.pcf: line 127, column 41
    function label_155 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.ReturnPremiumPlanDesc")
    }
    
    // 'label' attribute on TextCell (id=chargePattern_Cell) at PlanDataUploadLocalizationLV.pcf: line 132, column 41
    function label_160 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.ChargePattern")
    }
    
    // 'label' attribute on TextCell (id=chargePatternName_Cell) at PlanDataUploadLocalizationLV.pcf: line 137, column 41
    function label_165 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.ChargePatternName")
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadLocalizationLV.pcf: line 22, column 46
    function label_49 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TypeKeyCell (id=language_Cell) at PlanDataUploadLocalizationLV.pcf: line 27, column 37
    function label_54 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.Language")
    }
    
    // 'label' attribute on TextCell (id=billPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 32, column 41
    function label_60 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.BillingPlan")
    }
    
    // 'label' attribute on TextCell (id=billPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 37, column 41
    function label_65 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.BillingPlanName")
    }
    
    // 'label' attribute on TextCell (id=billPlanDesc_Cell) at PlanDataUploadLocalizationLV.pcf: line 42, column 41
    function label_70 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.BillingPlanDesc")
    }
    
    // 'label' attribute on TextCell (id=paymentPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 47, column 41
    function label_75 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.PaymentPlan")
    }
    
    // 'label' attribute on TextCell (id=payPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 52, column 41
    function label_80 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.PaymentPlanName")
    }
    
    // 'label' attribute on TextCell (id=payPlanDesc_Cell) at PlanDataUploadLocalizationLV.pcf: line 57, column 41
    function label_85 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.PaymentPlanDesc")
    }
    
    // 'label' attribute on TextCell (id=delqPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 62, column 41
    function label_90 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.DelinquencyPlan")
    }
    
    // 'label' attribute on TextCell (id=delqPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 67, column 41
    function label_95 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.DelinquencyPlanName")
    }
    
    // 'value' attribute on TypeKeyCell (id=language_Cell) at PlanDataUploadLocalizationLV.pcf: line 27, column 37
    function valueRoot_56 () : java.lang.Object {
      return localization
    }
    
    // 'value' attribute on TextCell (id=commPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 72, column 41
    function value_101 () : java.lang.String {
      return localization.CommissionPlan
    }
    
    // 'value' attribute on TextCell (id=commPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 77, column 41
    function value_106 () : java.lang.String {
      return localization.CommissionPlanName
    }
    
    // 'value' attribute on TextCell (id=commSubPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 82, column 41
    function value_111 () : java.lang.String {
      return localization.CommissionSubPlan
    }
    
    // 'value' attribute on TextCell (id=commSubPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 87, column 41
    function value_116 () : java.lang.String {
      return localization.CommissionSubPlanName
    }
    
    // 'value' attribute on TextCell (id=agencyBillPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 92, column 41
    function value_121 () : java.lang.String {
      return localization.AgencyBillPlan
    }
    
    // 'value' attribute on TextCell (id=agencyBillPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 97, column 41
    function value_126 () : java.lang.String {
      return localization.AgencyBillPlanName
    }
    
    // 'value' attribute on TextCell (id=payAllocationPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 102, column 41
    function value_131 () : java.lang.String {
      return localization.PayAllocationPlan
    }
    
    // 'value' attribute on TextCell (id=payAllocationPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 107, column 41
    function value_136 () : java.lang.String {
      return localization.PayAllocationPlanName
    }
    
    // 'value' attribute on TextCell (id=payAllocationPlanDesc_Cell) at PlanDataUploadLocalizationLV.pcf: line 112, column 41
    function value_141 () : java.lang.String {
      return localization.PayAllocationPlanDesc
    }
    
    // 'value' attribute on TextCell (id=returnPremiumPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 117, column 41
    function value_146 () : java.lang.String {
      return localization.ReturnPremiumPlan
    }
    
    // 'value' attribute on TextCell (id=returnPremiumPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 122, column 41
    function value_151 () : java.lang.String {
      return localization.ReturnPremiumPlanName
    }
    
    // 'value' attribute on TextCell (id=returnPremiumPlanDesc_Cell) at PlanDataUploadLocalizationLV.pcf: line 127, column 41
    function value_156 () : java.lang.String {
      return localization.ReturnPremiumPlanDesc
    }
    
    // 'value' attribute on TextCell (id=chargePattern_Cell) at PlanDataUploadLocalizationLV.pcf: line 132, column 41
    function value_161 () : java.lang.String {
      return localization.ChargePattern
    }
    
    // 'value' attribute on TextCell (id=chargePatternName_Cell) at PlanDataUploadLocalizationLV.pcf: line 137, column 41
    function value_166 () : java.lang.String {
      return localization.ChargePatternName
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadLocalizationLV.pcf: line 22, column 46
    function value_50 () : java.lang.String {
      return processor.getLoadStatus(localization)
    }
    
    // 'value' attribute on TypeKeyCell (id=language_Cell) at PlanDataUploadLocalizationLV.pcf: line 27, column 37
    function value_55 () : LanguageType {
      return localization.Language
    }
    
    // 'value' attribute on TextCell (id=billPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 32, column 41
    function value_61 () : java.lang.String {
      return localization.BillingPlan
    }
    
    // 'value' attribute on TextCell (id=billPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 37, column 41
    function value_66 () : java.lang.String {
      return localization.BillingPlanName
    }
    
    // 'value' attribute on TextCell (id=billPlanDesc_Cell) at PlanDataUploadLocalizationLV.pcf: line 42, column 41
    function value_71 () : java.lang.String {
      return localization.BillingPlanDesc
    }
    
    // 'value' attribute on TextCell (id=paymentPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 47, column 41
    function value_76 () : java.lang.String {
      return localization.PaymentPlan
    }
    
    // 'value' attribute on TextCell (id=payPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 52, column 41
    function value_81 () : java.lang.String {
      return localization.PaymentPlanName
    }
    
    // 'value' attribute on TextCell (id=payPlanDesc_Cell) at PlanDataUploadLocalizationLV.pcf: line 57, column 41
    function value_86 () : java.lang.String {
      return localization.PaymentPlanDesc
    }
    
    // 'value' attribute on TextCell (id=delqPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 62, column 41
    function value_91 () : java.lang.String {
      return localization.DelinquencyPlan
    }
    
    // 'value' attribute on TextCell (id=delqPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 67, column 41
    function value_96 () : java.lang.String {
      return localization.DelinquencyPlanName
    }
    
    // 'valueType' attribute on TypeKeyCell (id=language_Cell) at PlanDataUploadLocalizationLV.pcf: line 27, column 37
    function verifyValueType_59 () : void {
      var __valueTypeArg : LanguageType
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : gw.entity.TypeKey = __valueTypeArg
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadLocalizationLV.pcf: line 22, column 46
    function visible_51 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get localization () : tdic.util.dataloader.data.plandata.PlanLocalizationData {
      return getIteratedValue(1) as tdic.util.dataloader.data.plandata.PlanLocalizationData
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadLocalizationLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanDataUploadLocalizationLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadLocalizationLV.pcf: line 22, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=paymentPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 47, column 41
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.PaymentPlan")
    }
    
    // 'label' attribute on TextCell (id=payPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 52, column 41
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.PaymentPlanName")
    }
    
    // 'label' attribute on TextCell (id=payPlanDesc_Cell) at PlanDataUploadLocalizationLV.pcf: line 57, column 41
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.PaymentPlanDesc")
    }
    
    // 'label' attribute on TextCell (id=delqPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 62, column 41
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.DelinquencyPlan")
    }
    
    // 'label' attribute on TextCell (id=delqPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 67, column 41
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.DelinquencyPlanName")
    }
    
    // 'label' attribute on TextCell (id=commPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 72, column 41
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.CommissionPlan")
    }
    
    // 'label' attribute on TextCell (id=commPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 77, column 41
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.CommissionPlanName")
    }
    
    // 'label' attribute on TextCell (id=commSubPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 82, column 41
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.CommissionSubPlan")
    }
    
    // 'label' attribute on TextCell (id=commSubPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 87, column 41
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.CommissionSubPlanName")
    }
    
    // 'label' attribute on TextCell (id=agencyBillPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 92, column 41
    function label_29 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.AgencyBillPlan")
    }
    
    // 'label' attribute on TypeKeyCell (id=language_Cell) at PlanDataUploadLocalizationLV.pcf: line 27, column 37
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.Language")
    }
    
    // 'label' attribute on TextCell (id=agencyBillPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 97, column 41
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.AgencyBillPlanName")
    }
    
    // 'label' attribute on TextCell (id=payAllocationPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 102, column 41
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.PayAllocationPlan")
    }
    
    // 'label' attribute on TextCell (id=payAllocationPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 107, column 41
    function label_35 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.PayAllocationPlanName")
    }
    
    // 'label' attribute on TextCell (id=payAllocationPlanDesc_Cell) at PlanDataUploadLocalizationLV.pcf: line 112, column 41
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.PayAllocationPlanDesc")
    }
    
    // 'label' attribute on TextCell (id=returnPremiumPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 117, column 41
    function label_39 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.ReturnPremiumPlan")
    }
    
    // 'label' attribute on TextCell (id=returnPremiumPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 122, column 41
    function label_41 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.ReturnPremiumPlanName")
    }
    
    // 'label' attribute on TextCell (id=returnPremiumPlanDesc_Cell) at PlanDataUploadLocalizationLV.pcf: line 127, column 41
    function label_43 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.ReturnPremiumPlanDesc")
    }
    
    // 'label' attribute on TextCell (id=chargePattern_Cell) at PlanDataUploadLocalizationLV.pcf: line 132, column 41
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.ChargePattern")
    }
    
    // 'label' attribute on TextCell (id=chargePatternName_Cell) at PlanDataUploadLocalizationLV.pcf: line 137, column 41
    function label_47 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.ChargePatternName")
    }
    
    // 'label' attribute on TextCell (id=billPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 32, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.BillingPlan")
    }
    
    // 'label' attribute on TextCell (id=billPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 37, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.BillingPlanName")
    }
    
    // 'label' attribute on TextCell (id=billPlanDesc_Cell) at PlanDataUploadLocalizationLV.pcf: line 42, column 41
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations.BillingPlanDesc")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadLocalizationLV.pcf: line 22, column 46
    function sortValue_1 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return processor.getLoadStatus(localization)
    }
    
    // 'value' attribute on TextCell (id=billPlanDesc_Cell) at PlanDataUploadLocalizationLV.pcf: line 42, column 41
    function sortValue_10 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.BillingPlanDesc
    }
    
    // 'value' attribute on TextCell (id=paymentPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 47, column 41
    function sortValue_12 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.PaymentPlan
    }
    
    // 'value' attribute on TextCell (id=payPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 52, column 41
    function sortValue_14 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.PaymentPlanName
    }
    
    // 'value' attribute on TextCell (id=payPlanDesc_Cell) at PlanDataUploadLocalizationLV.pcf: line 57, column 41
    function sortValue_16 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.PaymentPlanDesc
    }
    
    // 'value' attribute on TextCell (id=delqPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 62, column 41
    function sortValue_18 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.DelinquencyPlan
    }
    
    // 'value' attribute on TextCell (id=delqPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 67, column 41
    function sortValue_20 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.DelinquencyPlanName
    }
    
    // 'value' attribute on TextCell (id=commPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 72, column 41
    function sortValue_22 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.CommissionPlan
    }
    
    // 'value' attribute on TextCell (id=commPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 77, column 41
    function sortValue_24 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.CommissionPlanName
    }
    
    // 'value' attribute on TextCell (id=commSubPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 82, column 41
    function sortValue_26 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.CommissionSubPlan
    }
    
    // 'value' attribute on TextCell (id=commSubPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 87, column 41
    function sortValue_28 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.CommissionSubPlanName
    }
    
    // 'value' attribute on TextCell (id=agencyBillPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 92, column 41
    function sortValue_30 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.AgencyBillPlan
    }
    
    // 'value' attribute on TextCell (id=agencyBillPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 97, column 41
    function sortValue_32 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.AgencyBillPlanName
    }
    
    // 'value' attribute on TextCell (id=payAllocationPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 102, column 41
    function sortValue_34 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.PayAllocationPlan
    }
    
    // 'value' attribute on TextCell (id=payAllocationPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 107, column 41
    function sortValue_36 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.PayAllocationPlanName
    }
    
    // 'value' attribute on TextCell (id=payAllocationPlanDesc_Cell) at PlanDataUploadLocalizationLV.pcf: line 112, column 41
    function sortValue_38 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.PayAllocationPlanDesc
    }
    
    // 'value' attribute on TypeKeyCell (id=language_Cell) at PlanDataUploadLocalizationLV.pcf: line 27, column 37
    function sortValue_4 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.Language
    }
    
    // 'value' attribute on TextCell (id=returnPremiumPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 117, column 41
    function sortValue_40 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.ReturnPremiumPlan
    }
    
    // 'value' attribute on TextCell (id=returnPremiumPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 122, column 41
    function sortValue_42 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.ReturnPremiumPlanName
    }
    
    // 'value' attribute on TextCell (id=returnPremiumPlanDesc_Cell) at PlanDataUploadLocalizationLV.pcf: line 127, column 41
    function sortValue_44 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.ReturnPremiumPlanDesc
    }
    
    // 'value' attribute on TextCell (id=chargePattern_Cell) at PlanDataUploadLocalizationLV.pcf: line 132, column 41
    function sortValue_46 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.ChargePattern
    }
    
    // 'value' attribute on TextCell (id=chargePatternName_Cell) at PlanDataUploadLocalizationLV.pcf: line 137, column 41
    function sortValue_48 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.ChargePatternName
    }
    
    // 'value' attribute on TextCell (id=billPlan_Cell) at PlanDataUploadLocalizationLV.pcf: line 32, column 41
    function sortValue_6 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.BillingPlan
    }
    
    // 'value' attribute on TextCell (id=billPlanName_Cell) at PlanDataUploadLocalizationLV.pcf: line 37, column 41
    function sortValue_8 (localization :  tdic.util.dataloader.data.plandata.PlanLocalizationData) : java.lang.Object {
      return localization.BillingPlanName
    }
    
    // 'value' attribute on RowIterator (id=localizationID) at PlanDataUploadLocalizationLV.pcf: line 15, column 102
    function value_170 () : java.util.ArrayList<tdic.util.dataloader.data.plandata.PlanLocalizationData> {
      return processor.LocalizationArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadLocalizationLV.pcf: line 22, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}