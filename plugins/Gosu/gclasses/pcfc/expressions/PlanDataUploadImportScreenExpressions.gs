package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadImportScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanDataUploadImportScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadImportScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanDataUploadImportScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on BooleanRadioInput (id=IsLoadingFromConfig_Input) at PlanDataUploadImportScreen.pcf: line 23, column 51
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.IsLoadingFromConfig = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on FileInput (id=xlsFile_Input) at PlanDataUploadImportScreen.pcf: line 37, column 55
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.MyFile = (__VALUE_TO_SET as gw.api.web.WebFile)
    }
    
    // 'value' attribute on TextInput (id=PathToExcelFile_Input) at PlanDataUploadImportScreen.pcf: line 31, column 54
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.RelativePath = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'label' attribute on TextInput (id=PathToExcelFile_Input) at PlanDataUploadImportScreen.pcf: line 31, column 54
    function label_5 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.UploadRelativePathToExcelFile", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ExcelFile"))
    }
    
    // 'value' attribute on BooleanRadioInput (id=IsLoadingFromConfig_Input) at PlanDataUploadImportScreen.pcf: line 23, column 51
    function valueRoot_2 () : java.lang.Object {
      return processor
    }
    
    // 'value' attribute on BooleanRadioInput (id=IsLoadingFromConfig_Input) at PlanDataUploadImportScreen.pcf: line 23, column 51
    function value_0 () : java.lang.Boolean {
      return processor.IsLoadingFromConfig
    }
    
    // 'value' attribute on FileInput (id=xlsFile_Input) at PlanDataUploadImportScreen.pcf: line 37, column 55
    function value_13 () : gw.api.web.WebFile {
      return processor.MyFile
    }
    
    // 'value' attribute on TextInput (id=PathToExcelFile_Input) at PlanDataUploadImportScreen.pcf: line 31, column 54
    function value_6 () : java.lang.String {
      return processor.RelativePath
    }
    
    // 'visible' attribute on FileInput (id=xlsFile_Input) at PlanDataUploadImportScreen.pcf: line 37, column 55
    function visible_12 () : java.lang.Boolean {
      return !processor.IsLoadingFromConfig
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}