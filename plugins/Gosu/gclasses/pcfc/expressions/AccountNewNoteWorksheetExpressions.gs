package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountNewNoteWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountNewNoteWorksheetExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountNewNoteWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountNewNoteWorksheetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    static function __constructorIndex (account :  Account, noteToEdit :  Note) : int {
      return 1
    }
    
    // 'action' attribute on PickerToolbarButton (id=linkDocsPickerButton) at AccountNewNoteWorksheet.pcf: line 48, column 88
    function action_3 () : void {
      PickExistingDocumentPopup.push(account)
    }
    
    // 'action' attribute on PickerToolbarButton (id=linkDocsPickerButton) at AccountNewNoteWorksheet.pcf: line 48, column 88
    function action_dest_4 () : pcf.api.Destination {
      return pcf.PickExistingDocumentPopup.createDestination(account)
    }
    
    // 'beforeCommit' attribute on Worksheet (id=AccountNewNoteWorksheet) at AccountNewNoteWorksheet.pcf: line 13, column 66
    function beforeCommit_8 (pickedValue :  java.lang.Object) : void {
      account.addNote(newNote); account.addHistoryFromGosu( java.util.Date.CurrentDate, HistoryEventType.TC_NOTESUPDATED, DisplayKey.get("Java.AccountHistory.NotesUpdated"), null as Transaction, null, true )
    }
    
    // 'canVisit' attribute on Worksheet (id=AccountNewNoteWorksheet) at AccountNewNoteWorksheet.pcf: line 13, column 66
    static function canVisit_9 (account :  Account, noteToEdit :  Note) : java.lang.Boolean {
      if (noteToEdit != null) return perm.Note.edit(noteToEdit) else return perm.Note.create
    }
    
    // 'def' attribute on PanelRef at AccountNewNoteWorksheet.pcf: line 51, column 35
    function def_onEnter_6 (def :  pcf.NewNoteDV) : void {
      def.onEnter(newNote)
    }
    
    // 'def' attribute on PanelRef at AccountNewNoteWorksheet.pcf: line 51, column 35
    function def_refreshVariables_7 (def :  pcf.NewNoteDV) : void {
      def.refreshVariables(newNote)
    }
    
    // 'initialValue' attribute on Variable at AccountNewNoteWorksheet.pcf: line 29, column 20
    function initialValue_0 () : Note {
      if (noteToEdit != null) return noteToEdit else return gw.pcf.note.NoteHelper.createNoteWithCurrentUsersLanguage()
    }
    
    // 'initialValue' attribute on Variable at AccountNewNoteWorksheet.pcf: line 33, column 51
    function initialValue_1 () : java.util.Map<String, Object> {
      return gw.api.util.SymbolTableUtil.populateBeans( account )
    }
    
    // EditButtons at AccountNewNoteWorksheet.pcf: line 37, column 23
    function label_2 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=linkDocsPickerButton) at AccountNewNoteWorksheet.pcf: line 48, column 88
    function onPick_5 (PickedValue :  Document) : void {
      acc.onbase.util.NotesUtil.linkDocumentToNote(PickedValue, newNote)
    }
    
    // 'parent' attribute on Worksheet (id=AccountNewNoteWorksheet) at AccountNewNoteWorksheet.pcf: line 13, column 66
    static function parent_10 (account :  Account, noteToEdit :  Note) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.AccountNewNoteWorksheet {
      return super.CurrentLocation as pcf.AccountNewNoteWorksheet
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get newNote () : Note {
      return getVariableValue("newNote", 0) as Note
    }
    
    property set newNote ($arg :  Note) {
      setVariableValue("newNote", 0, $arg)
    }
    
    property get noteToEdit () : Note {
      return getVariableValue("noteToEdit", 0) as Note
    }
    
    property set noteToEdit ($arg :  Note) {
      setVariableValue("noteToEdit", 0, $arg)
    }
    
    property get symbolTable () : java.util.Map<String, Object> {
      return getVariableValue("symbolTable", 0) as java.util.Map<String, Object>
    }
    
    property set symbolTable ($arg :  java.util.Map<String, Object>) {
      setVariableValue("symbolTable", 0, $arg)
    }
    
    function createSearchCriteria() : NoteTemplateSearchCriteria {
      var rtn = new NoteTemplateSearchCriteria()
      // rtn.Language = Account.AccountHolder.Language 
      rtn.AvailableSymbols = symbolTable.Keys.join( "," )
      var policy = symbolTable.get( "policy" ) as Policy
      if (policy.LOBCode != null) {
        rtn.LOB = policy.LOBCode
      }
      return rtn  
    }
    
    
  }
  
  
}