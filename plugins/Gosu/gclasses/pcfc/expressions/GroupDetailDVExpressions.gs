package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/groups/GroupDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class GroupDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/groups/GroupDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class GroupDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on GroupInput (id=Parent_Input) at GroupWidget.xml: line 10, column 49
    function action_13 () : void {
      pcf.GroupSearchPopup.push()
    }
    
    // 'action' attribute on GroupInput (id=Parent_Input) at GroupWidget.xml: line 13, column 49
    function action_15 () : void {
      pcf.OrganizationGroupTreePopup.push()
    }
    
    // 'action' attribute on GroupInput (id=Parent_Input) at GroupWidget.xml: line 7, column 52
    function action_17 () : void {
      GroupDetailPage.push(Group.Parent)
    }
    
    // 'action' attribute on GroupInput (id=Parent_Input) at GroupDetailDV.pcf: line 36, column 31
    function action_25 () : void {
      GroupDetailPage.push(Group.Parent)
    }
    
    // 'action' attribute on UserInput (id=Supervisor_Input) at UserWidget.xml: line 9, column 49
    function action_30 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // 'action' attribute on UserInput (id=Supervisor_Input) at UserWidget.xml: line 12, column 49
    function action_32 () : void {
      pcf.UserSelectPopup.push()
    }
    
    // 'action' attribute on UserInput (id=Supervisor_Input) at UserWidget.xml: line 6, column 85
    function action_34 () : void {
      UserDetailPage.go(Group.Supervisor)
    }
    
    // 'action' attribute on UserInput (id=Supervisor_Input) at GroupDetailDV.pcf: line 44, column 34
    function action_42 () : void {
      UserDetailPage.go(Group.Supervisor)
    }
    
    // 'action' attribute on RangeInput (id=SecurityZone_Input) at GroupDetailDV.pcf: line 53, column 42
    function action_47 () : void {
      SecurityZoneDetail.go(Group.SecurityZone)
    }
    
    // 'action' attribute on GroupInput (id=Parent_Input) at GroupWidget.xml: line 10, column 49
    function action_dest_14 () : pcf.api.Destination {
      return pcf.GroupSearchPopup.createDestination()
    }
    
    // 'action' attribute on GroupInput (id=Parent_Input) at GroupWidget.xml: line 13, column 49
    function action_dest_16 () : pcf.api.Destination {
      return pcf.OrganizationGroupTreePopup.createDestination()
    }
    
    // 'action' attribute on GroupInput (id=Parent_Input) at GroupWidget.xml: line 7, column 52
    function action_dest_18 () : pcf.api.Destination {
      return pcf.GroupDetailPage.createDestination(Group.Parent)
    }
    
    // 'action' attribute on GroupInput (id=Parent_Input) at GroupDetailDV.pcf: line 36, column 31
    function action_dest_26 () : pcf.api.Destination {
      return pcf.GroupDetailPage.createDestination(Group.Parent)
    }
    
    // 'action' attribute on UserInput (id=Supervisor_Input) at UserWidget.xml: line 9, column 49
    function action_dest_31 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'action' attribute on UserInput (id=Supervisor_Input) at UserWidget.xml: line 12, column 49
    function action_dest_33 () : pcf.api.Destination {
      return pcf.UserSelectPopup.createDestination()
    }
    
    // 'action' attribute on UserInput (id=Supervisor_Input) at UserWidget.xml: line 6, column 85
    function action_dest_35 () : pcf.api.Destination {
      return pcf.UserDetailPage.createDestination(Group.Supervisor)
    }
    
    // 'action' attribute on UserInput (id=Supervisor_Input) at GroupDetailDV.pcf: line 44, column 34
    function action_dest_43 () : pcf.api.Destination {
      return pcf.UserDetailPage.createDestination(Group.Supervisor)
    }
    
    // 'action' attribute on RangeInput (id=SecurityZone_Input) at GroupDetailDV.pcf: line 53, column 42
    function action_dest_48 () : pcf.api.Destination {
      return pcf.SecurityZoneDetail.createDestination(Group.SecurityZone)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=GroupDetailDV_ClearBackupUserButton) at GroupDetailDV.pcf: line 67, column 35
    function checkedRowAction_57 (element :  entity.GroupUser, CheckedValue :  entity.GroupUser) : void {
      CheckedValue.User.BackupUser = null
    }
    
    // 'def' attribute on ListViewInput at GroupDetailDV.pcf: line 57, column 81
    function def_onEnter_58 (def :  pcf.GroupUsersLV) : void {
      def.onEnter(Group)
    }
    
    // 'def' attribute on ListViewInput at GroupDetailDV.pcf: line 57, column 81
    function def_refreshVariables_59 (def :  pcf.GroupUsersLV) : void {
      def.refreshVariables(Group)
    }
    
    // 'value' attribute on GroupInput (id=Parent_Input) at GroupWidget.xml: line 7, column 52
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      Group.Parent = (__VALUE_TO_SET as entity.Group)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at GroupDetailDV.pcf: line 21, column 29
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      Group.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on UserInput (id=Supervisor_Input) at UserWidget.xml: line 6, column 85
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      Group.Supervisor = (__VALUE_TO_SET as entity.User)
    }
    
    // 'value' attribute on RangeInput (id=SecurityZone_Input) at GroupDetailDV.pcf: line 53, column 42
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      Group.SecurityZone = (__VALUE_TO_SET as entity.SecurityZone)
    }
    
    // 'value' attribute on TextInput (id=NameKanji_Input) at GroupDetailDV.pcf: line 29, column 84
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      Group.NameKanji = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at GroupDetailDV.pcf: line 13, column 68
    function initialValue_0 () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return gw.api.database.Query.make(SecurityZone).select()
    }
    
    // 'label' attribute on TextInput (id=Name_Input) at GroupDetailDV.pcf: line 21, column 29
    function label_1 () : java.lang.Object {
      return (gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP) ? DisplayKey.get("Web.Admin.GroupDetail.BasicDV.NamePhonetic") : DisplayKey.get("Web.Admin.GroupDetail.BasicDV.Name")
    }
    
    // 'valueRange' attribute on GroupInput (id=Parent_Input) at GroupWidget.xml: line 7, column 52
    function valueRange_22 () : java.lang.Object {
      return gw.api.admin.BaseAdminUtil.getGroupsForCurrentUser()
    }
    
    // 'valueRange' attribute on UserInput (id=Supervisor_Input) at UserWidget.xml: line 6, column 85
    function valueRange_39 () : java.lang.Object {
      return entity.User.util.getUsersInCurrentUsersGroup()
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at GroupDetailDV.pcf: line 53, column 42
    function valueRange_52 () : java.lang.Object {
      return allSecurityZones
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at GroupDetailDV.pcf: line 21, column 29
    function valueRoot_4 () : java.lang.Object {
      return Group
    }
    
    // 'value' attribute on GroupInput (id=Parent_Input) at GroupWidget.xml: line 7, column 52
    function value_19 () : entity.Group {
      return Group.Parent
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at GroupDetailDV.pcf: line 21, column 29
    function value_2 () : java.lang.String {
      return Group.Name
    }
    
    // 'value' attribute on UserInput (id=Supervisor_Input) at UserWidget.xml: line 6, column 85
    function value_36 () : entity.User {
      return Group.Supervisor
    }
    
    // 'value' attribute on RangeInput (id=SecurityZone_Input) at GroupDetailDV.pcf: line 53, column 42
    function value_49 () : entity.SecurityZone {
      return Group.SecurityZone
    }
    
    // 'value' attribute on TextInput (id=NameKanji_Input) at GroupDetailDV.pcf: line 29, column 84
    function value_8 () : java.lang.String {
      return Group.NameKanji
    }
    
    // 'valueRange' attribute on GroupInput (id=Parent_Input) at GroupWidget.xml: line 7, column 52
    function verifyValueRangeIsAllowedType_23 ($$arg :  entity.Group[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on GroupInput (id=Parent_Input) at GroupWidget.xml: line 7, column 52
    function verifyValueRangeIsAllowedType_23 ($$arg :  gw.api.database.IQueryBeanResult<entity.Group>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on GroupInput (id=Parent_Input) at GroupWidget.xml: line 7, column 52
    function verifyValueRangeIsAllowedType_23 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on UserInput (id=Supervisor_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRangeIsAllowedType_40 ($$arg :  entity.User[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on UserInput (id=Supervisor_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRangeIsAllowedType_40 ($$arg :  gw.api.database.IQueryBeanResult<entity.User>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on UserInput (id=Supervisor_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRangeIsAllowedType_40 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at GroupDetailDV.pcf: line 53, column 42
    function verifyValueRangeIsAllowedType_53 ($$arg :  entity.SecurityZone[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at GroupDetailDV.pcf: line 53, column 42
    function verifyValueRangeIsAllowedType_53 ($$arg :  gw.api.database.IQueryBeanResult<entity.SecurityZone>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at GroupDetailDV.pcf: line 53, column 42
    function verifyValueRangeIsAllowedType_53 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on GroupInput (id=Parent_Input) at GroupWidget.xml: line 7, column 52
    function verifyValueRange_24 () : void {
      var __valueRangeArg = gw.api.admin.BaseAdminUtil.getGroupsForCurrentUser()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_23(__valueRangeArg)
    }
    
    // 'valueRange' attribute on UserInput (id=Supervisor_Input) at UserWidget.xml: line 6, column 85
    function verifyValueRange_41 () : void {
      var __valueRangeArg = entity.User.util.getUsersInCurrentUsersGroup()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_40(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at GroupDetailDV.pcf: line 53, column 42
    function verifyValueRange_54 () : void {
      var __valueRangeArg = allSecurityZones
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_53(__valueRangeArg)
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=GroupDetailDV_ClearBackupUserButton) at GroupDetailDV.pcf: line 67, column 35
    function visible_56 () : java.lang.Boolean {
      return !Group.New
    }
    
    // 'visible' attribute on TextInput (id=NameKanji_Input) at GroupDetailDV.pcf: line 29, column 84
    function visible_7 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP
    }
    
    property get Group () : Group {
      return getRequireValue("Group", 0) as Group
    }
    
    property set Group ($arg :  Group) {
      setRequireValue("Group", 0, $arg)
    }
    
    property get allSecurityZones () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return getVariableValue("allSecurityZones", 0) as gw.api.database.IQueryBeanResult<SecurityZone>
    }
    
    property set allSecurityZones ($arg :  gw.api.database.IQueryBeanResult<SecurityZone>) {
      setVariableValue("allSecurityZones", 0, $arg)
    }
    
    
  }
  
  
}