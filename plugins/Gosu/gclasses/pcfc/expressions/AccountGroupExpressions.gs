package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountGroupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountGroupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 27, column 50
    function action_16 () : void {
      pcf.AccountDetailPayments.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 29, column 49
    function action_18 () : void {
      pcf.AccountDetailCharges.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 31, column 55
    function action_20 () : void {
      pcf.AccountDetailDisbursements.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 33, column 54
    function action_22 () : void {
      pcf.AccountDetailTransactions.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 36, column 46
    function action_24 () : void {
      pcf.AccountCollateral.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 39, column 50
    function action_26 () : void {
      pcf.AccountDetailPolicies.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 41, column 49
    function action_28 () : void {
      pcf.AccountDetailHistory.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 43, column 52
    function action_30 () : void {
      pcf.AccountDetailEvaluation.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 46, column 50
    function action_32 () : void {
      pcf.AccountDetailInvoices.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 49, column 56
    function action_34 () : void {
      pcf.AccountDetailInvoiceStreams.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 52, column 56
    function action_36 () : void {
      pcf.AccountDetailTroubleTickets.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 54, column 51
    function action_38 () : void {
      pcf.AccountDetailDocuments.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 20, column 44
    function action_4 () : void {
      pcf.AccountOverview.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 56, column 53
    function action_40 () : void {
      pcf.AccountDetailNotes.go(account, true)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 59, column 48
    function action_42 () : void {
      pcf.AccountDetailLedger.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 61, column 49
    function action_44 () : void {
      pcf.AccountDetailJournal.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 63, column 55
    function action_46 () : void {
      pcf.AccountDetailDelinquencies.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 65, column 40
    function action_48 () : void {
      pcf.AccountView.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 67, column 50
    function action_50 () : void {
      pcf.AccountActivitiesPage.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 22, column 50
    function action_6 () : void {
      pcf.AccountDetailContacts.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 24, column 55
    function action_8 () : void {
      pcf.AccountDetailFundsTracking.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 27, column 50
    function action_dest_17 () : pcf.api.Destination {
      return pcf.AccountDetailPayments.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 29, column 49
    function action_dest_19 () : pcf.api.Destination {
      return pcf.AccountDetailCharges.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 31, column 55
    function action_dest_21 () : pcf.api.Destination {
      return pcf.AccountDetailDisbursements.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 33, column 54
    function action_dest_23 () : pcf.api.Destination {
      return pcf.AccountDetailTransactions.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 36, column 46
    function action_dest_25 () : pcf.api.Destination {
      return pcf.AccountCollateral.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 39, column 50
    function action_dest_27 () : pcf.api.Destination {
      return pcf.AccountDetailPolicies.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 41, column 49
    function action_dest_29 () : pcf.api.Destination {
      return pcf.AccountDetailHistory.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 43, column 52
    function action_dest_31 () : pcf.api.Destination {
      return pcf.AccountDetailEvaluation.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 46, column 50
    function action_dest_33 () : pcf.api.Destination {
      return pcf.AccountDetailInvoices.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 49, column 56
    function action_dest_35 () : pcf.api.Destination {
      return pcf.AccountDetailInvoiceStreams.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 52, column 56
    function action_dest_37 () : pcf.api.Destination {
      return pcf.AccountDetailTroubleTickets.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 54, column 51
    function action_dest_39 () : pcf.api.Destination {
      return pcf.AccountDetailDocuments.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 56, column 53
    function action_dest_41 () : pcf.api.Destination {
      return pcf.AccountDetailNotes.createDestination(account, true)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 59, column 48
    function action_dest_43 () : pcf.api.Destination {
      return pcf.AccountDetailLedger.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 61, column 49
    function action_dest_45 () : pcf.api.Destination {
      return pcf.AccountDetailJournal.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 63, column 55
    function action_dest_47 () : pcf.api.Destination {
      return pcf.AccountDetailDelinquencies.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 65, column 40
    function action_dest_49 () : pcf.api.Destination {
      return pcf.AccountView.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 20, column 44
    function action_dest_5 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 67, column 50
    function action_dest_51 () : pcf.api.Destination {
      return pcf.AccountActivitiesPage.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 22, column 50
    function action_dest_7 () : pcf.api.Destination {
      return pcf.AccountDetailContacts.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 24, column 55
    function action_dest_9 () : pcf.api.Destination {
      return pcf.AccountDetailFundsTracking.createDestination(account)
    }
    
    // 'afterEnter' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 13, column 35
    function afterEnter_52 () : void {
      gw.api.web.account.AccountUtil.addToRecentlyViewedAccounts(account)
    }
    
    // 'canVisit' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 13, column 35
    static function canVisit_53 (account :  Account) : java.lang.Boolean {
      return account.ViewableByCurrentUser
    }
    
    // LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 13, column 35
    static function firstVisitableChildDestinationMethod_62 (account :  Account) : pcf.api.Destination {
      var dest : pcf.api.Destination
      dest = pcf.AccountOverview.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailContacts.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailFundsTracking.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailPayments.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailCharges.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailDisbursements.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailTransactions.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountCollateral.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailPolicies.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailHistory.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailEvaluation.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailInvoices.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailInvoiceStreams.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailTroubleTickets.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailDocuments.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailNotes.createDestination(account, true)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailLedger.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailJournal.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountDetailDelinquencies.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountView.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AccountActivitiesPage.createDestination(account)
      if (dest.canVisitSelf()) {
        return dest
      }
      return null
    }
    
    // 'infoBar' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 13, column 35
    function infoBar_onEnter_54 (def :  pcf.AccountInfoBar) : void {
      def.onEnter(account)
    }
    
    // 'infoBar' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 13, column 35
    function infoBar_refreshVariables_55 (def :  pcf.AccountInfoBar) : void {
      def.refreshVariables(account)
    }
    
    // 'menuActions' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 13, column 35
    function menuActions_onEnter_56 (def :  pcf.AccountDetailMenuActions) : void {
      def.onEnter(account)
    }
    
    // 'menuActions' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 13, column 35
    function menuActions_refreshVariables_57 (def :  pcf.AccountDetailMenuActions) : void {
      def.refreshVariables(account)
    }
    
    // 'parent' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 13, column 35
    static function parent_58 (account :  Account) : pcf.api.Destination {
      return pcf.AccountForward.createDestination()
    }
    
    // 'tabBar' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 13, column 35
    function tabBar_onEnter_59 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 13, column 35
    function tabBar_refreshVariables_60 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    // 'title' attribute on LocationGroup (id=AccountGroup) at AccountGroup.pcf: line 13, column 35
    static function title_61 (account :  Account) : java.lang.Object {
      return account.AccountNumber
    }
    
    override property get CurrentLocation () : pcf.AccountGroup {
      return super.CurrentLocation as pcf.AccountGroup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationGroupMenuItem2ExpressionsImpl extends AccountGroupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountDetailPayments.pcf: line 14, column 44
    function action_10 () : void {
      pcf.AccountPayments.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountDetailPayments.pcf: line 16, column 55
    function action_12 () : void {
      pcf.AccountCreditDistributions.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountDetailPayments.pcf: line 18, column 51
    function action_14 () : void {
      pcf.AccountPaymentRequests.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountDetailPayments.pcf: line 14, column 44
    function action_dest_11 () : pcf.api.Destination {
      return pcf.AccountPayments.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountDetailPayments.pcf: line 16, column 55
    function action_dest_13 () : pcf.api.Destination {
      return pcf.AccountCreditDistributions.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountDetailPayments.pcf: line 18, column 51
    function action_dest_15 () : pcf.api.Destination {
      return pcf.AccountPaymentRequests.createDestination(account)
    }
    
    property get account () : Account {
      return getVariableValue("account", 1) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationGroupMenuItemExpressionsImpl extends AccountGroupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountOverview.pcf: line 16, column 43
    function action_0 () : void {
      pcf.AccountSummary.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountOverview.pcf: line 19, column 49
    function action_2 () : void {
      pcf.AccountDetailSummary.go(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountOverview.pcf: line 16, column 43
    function action_dest_1 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination(account)
    }
    
    // 'location' attribute on LocationGroup (id=AccountGroup) at AccountOverview.pcf: line 19, column 49
    function action_dest_3 () : pcf.api.Destination {
      return pcf.AccountDetailSummary.createDestination(account)
    }
    
    property get account () : Account {
      return getVariableValue("account", 1) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 1, $arg)
    }
    
    
  }
  
  
}