package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/AccountNewTransactionWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountNewTransactionWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/AccountNewTransactionWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountNewTransactionWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Wizard (id=AccountNewTransactionWizard) at AccountNewTransactionWizard.pcf: line 11, column 38
    function beforeCommit_5 (pickedValue :  java.lang.Object) : void {
      setClaimNumber(); helper.BillingInstruction.execute()
    }
    
    // 'canVisit' attribute on Wizard (id=AccountNewTransactionWizard) at AccountNewTransactionWizard.pcf: line 11, column 38
    static function canVisit_6 (account :  Account) : java.lang.Boolean {
      return perm.Transaction.gentxn
    }
    
    // 'initialValue' attribute on Variable at AccountNewTransactionWizard.pcf: line 20, column 58
    function initialValue_0 () : gw.accounting.NewGeneralSingleChargeHelper {
      return new gw.accounting.NewGeneralSingleChargeHelper (account, gw.transaction.UserTransactionType.FEE_OR_GENERAL)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountNewTransactionWizard.pcf: line 26, column 95
    function screen_onEnter_1 (def :  pcf.NewTransactionAccountPoliciesScreen) : void {
      def.onEnter(helper)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountNewTransactionWizard.pcf: line 31, column 83
    function screen_onEnter_3 (def :  pcf.ChargeDetailsScreen) : void {
      def.onEnter(helper)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountNewTransactionWizard.pcf: line 26, column 95
    function screen_refreshVariables_2 (def :  pcf.NewTransactionAccountPoliciesScreen) : void {
      def.refreshVariables(helper)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountNewTransactionWizard.pcf: line 31, column 83
    function screen_refreshVariables_4 (def :  pcf.ChargeDetailsScreen) : void {
      def.refreshVariables(helper)
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewTransactionWizard) at AccountNewTransactionWizard.pcf: line 11, column 38
    function tabBar_onEnter_7 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewTransactionWizard) at AccountNewTransactionWizard.pcf: line 11, column 38
    function tabBar_refreshVariables_8 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AccountNewTransactionWizard {
      return super.CurrentLocation as pcf.AccountNewTransactionWizard
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get helper () : gw.accounting.NewGeneralSingleChargeHelper {
      return getVariableValue("helper", 0) as gw.accounting.NewGeneralSingleChargeHelper
    }
    
    property set helper ($arg :  gw.accounting.NewGeneralSingleChargeHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    function setClaimNumber() {
      helper.ChargeInitializer.setExtensionProperty(Charge#ClaimID_TDIC, helper.ClaimID)
    }
    
    
  }
  
  
}