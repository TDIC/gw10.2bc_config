package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/allocationplan/AddDistributionFilterPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AddDistributionFilterPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/allocationplan/AddDistributionFilterPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AddDistributionFilterPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (paymentAllocationPlan :  PaymentAllocationPlan) : int {
      return 0
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=AddSelectedDistributionFilters) at AddDistributionFilterPopup.pcf: line 21, column 83
    function allCheckedRowsAction_0 (CheckedValues :  typekey.DistributionFilterType[], CheckedValuesErrors :  java.util.Map) : void {
      CheckedValues.each(\ filter -> paymentAllocationPlan.addDistributionCriteriaFilter(filter)); CurrentLocation.commit()
    }
    
    // 'value' attribute on TextCell (id=DistributionFilterName_Cell) at AddDistributionFilterPopup.pcf: line 36, column 55
    function sortValue_1 (distributionFilter :  typekey.DistributionFilterType) : java.lang.Object {
      return distributionFilter.DisplayName
    }
    
    // 'value' attribute on TextCell (id=DistributionFilterDescription_Cell) at AddDistributionFilterPopup.pcf: line 40, column 55
    function sortValue_2 (distributionFilter :  typekey.DistributionFilterType) : java.lang.Object {
      return distributionFilter.Description
    }
    
    // 'value' attribute on RowIterator (id=DistributionFilters) at AddDistributionFilterPopup.pcf: line 31, column 76
    function value_9 () : java.util.List<typekey.DistributionFilterType> {
      return FiltersNotOnPlan
    }
    
    override property get CurrentLocation () : pcf.AddDistributionFilterPopup {
      return super.CurrentLocation as pcf.AddDistributionFilterPopup
    }
    
    property get paymentAllocationPlan () : PaymentAllocationPlan {
      return getVariableValue("paymentAllocationPlan", 0) as PaymentAllocationPlan
    }
    
    property set paymentAllocationPlan ($arg :  PaymentAllocationPlan) {
      setVariableValue("paymentAllocationPlan", 0, $arg)
    }
    
    property get FiltersNotOnPlan() : java.util.List<DistributionFilterType> {
      return DistributionFilterType.getTypeKeys(false).where( \ filter -> paymentAllocationPlan.getDistributionCriterionByFilter(filter) == null)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/allocationplan/AddDistributionFilterPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AddDistributionFilterPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=DistributionFilterName_Cell) at AddDistributionFilterPopup.pcf: line 36, column 55
    function valueRoot_4 () : java.lang.Object {
      return distributionFilter
    }
    
    // 'value' attribute on TextCell (id=DistributionFilterName_Cell) at AddDistributionFilterPopup.pcf: line 36, column 55
    function value_3 () : java.lang.String {
      return distributionFilter.DisplayName
    }
    
    // 'value' attribute on TextCell (id=DistributionFilterDescription_Cell) at AddDistributionFilterPopup.pcf: line 40, column 55
    function value_6 () : java.lang.String {
      return distributionFilter.Description
    }
    
    property get distributionFilter () : typekey.DistributionFilterType {
      return getIteratedValue(1) as typekey.DistributionFilterType
    }
    
    
  }
  
  
}