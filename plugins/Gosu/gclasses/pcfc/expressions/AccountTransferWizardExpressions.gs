package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.acc.acct360.accountview.AccountBalanceTxnUtil
uses gw.api.database.Query
@javax.annotation.Generated("config/web/pcf/transfer/AccountTransferWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountTransferWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transfer/AccountTransferWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountTransferWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (accountID :  gw.pl.persistence.core.Key) : int {
      return 0
    }
    
    // 'allowFinish' attribute on WizardStep (id=TransferStep) at AccountTransferWizard.pcf: line 37, column 85
    function allowFinish_7 () : java.lang.Boolean {
      return !accountTransfer.ToAccount.ListBill && accountTransfer.ToAccount!=null
    }
    
    // 'beforeCommit' attribute on Wizard (id=AccountTransferWizard) at AccountTransferWizard.pcf: line 7, column 32
    function beforeCommit_10 (pickedValue :  java.lang.Object) : void {
      performAcct360Tracking();accountTransfer.doTransfer()
    }
    
    // 'initialValue' attribute on Variable at AccountTransferWizard.pcf: line 16, column 23
    function initialValue_0 () : Account {
      return loadAccount(accountID)
    }
    
    // 'initialValue' attribute on Variable at AccountTransferWizard.pcf: line 20, column 60
    function initialValue_1 () : gw.api.web.accounting.TAccountOwnerReference {
      return new gw.api.web.accounting.TAccountOwnerReference()
    }
    
    // 'initialValue' attribute on Variable at AccountTransferWizard.pcf: line 24, column 53
    function initialValue_2 () : gw.api.domain.account.AccountTransfer {
      return new gw.api.domain.account.AccountTransfer(CurrentLocation)
    }
    
    // 'onExit' attribute on WizardStep (id=AccountSearchStep) at AccountTransferWizard.pcf: line 31, column 90
    function onExit_3 () : void {
      accountTransfer.ToAccount = tAccountOwnerReference.TAccountOwner as Account
    }
    
    // 'onFirstEnter' attribute on WizardStep (id=AccountSearchStep) at AccountTransferWizard.pcf: line 31, column 90
    function onFirstEnter_4 () : void {
      accountTransfer.FromAccount = account
    }
    
    // 'screen' attribute on WizardStep (id=AccountSearchStep) at AccountTransferWizard.pcf: line 31, column 90
    function screen_onEnter_5 (def :  pcf.WizardsStep1AccountPolicySearchScreen) : void {
      def.onEnter(tAccountOwnerReference, TAccountOwnerType.TC_ACCOUNT, false, true)
    }
    
    // 'screen' attribute on WizardStep (id=TransferStep) at AccountTransferWizard.pcf: line 37, column 85
    function screen_onEnter_8 (def :  pcf.AccountTransferWizardTransferScreen) : void {
      def.onEnter(accountTransfer)
    }
    
    // 'screen' attribute on WizardStep (id=AccountSearchStep) at AccountTransferWizard.pcf: line 31, column 90
    function screen_refreshVariables_6 (def :  pcf.WizardsStep1AccountPolicySearchScreen) : void {
      def.refreshVariables(tAccountOwnerReference, TAccountOwnerType.TC_ACCOUNT, false, true)
    }
    
    // 'screen' attribute on WizardStep (id=TransferStep) at AccountTransferWizard.pcf: line 37, column 85
    function screen_refreshVariables_9 (def :  pcf.AccountTransferWizardTransferScreen) : void {
      def.refreshVariables(accountTransfer)
    }
    
    // 'tabBar' attribute on Wizard (id=AccountTransferWizard) at AccountTransferWizard.pcf: line 7, column 32
    function tabBar_onEnter_11 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AccountTransferWizard) at AccountTransferWizard.pcf: line 7, column 32
    function tabBar_refreshVariables_12 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AccountTransferWizard {
      return super.CurrentLocation as pcf.AccountTransferWizard
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get accountID () : gw.pl.persistence.core.Key {
      return getVariableValue("accountID", 0) as gw.pl.persistence.core.Key
    }
    
    property set accountID ($arg :  gw.pl.persistence.core.Key) {
      setVariableValue("accountID", 0, $arg)
    }
    
    property get accountTransfer () : gw.api.domain.account.AccountTransfer {
      return getVariableValue("accountTransfer", 0) as gw.api.domain.account.AccountTransfer
    }
    
    property set accountTransfer ($arg :  gw.api.domain.account.AccountTransfer) {
      setVariableValue("accountTransfer", 0, $arg)
    }
    
    property get tAccountOwnerReference () : gw.api.web.accounting.TAccountOwnerReference {
      return getVariableValue("tAccountOwnerReference", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set tAccountOwnerReference ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setVariableValue("tAccountOwnerReference", 0, $arg)
    }
    
    
        function loadAccount(id : gw.pl.persistence.core.Key) : Account {
          return Query<Account>.make(entity.Account).compare("ID", Equals, id).select().AtMostOneRow
        }
    
        function performAcct360Tracking() {
          var polPeriods = accountTransfer.PolicyPeriods.where(\p -> p.Transfer)*.PolicyPeriod
          var invItems = polPeriods*.InvoiceItems.where(\ii -> ii.Invoice.BilledOrDue
            && !ii.AgencyBill
            && !ii.Reversal
            && !ii.Reversed)
          AccountBalanceTxnUtil.createTransferRecord(accountTransfer)
          AccountBalanceTxnUtil.createMoveItemRecord(accountTransfer.FromAccount, invItems, AcctBalItemMovedType_Ext.TC_POLICYTRANSFERED)
        }
    
    
  }
  
  
}