package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.web.color.GWColor
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessEventsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyProcessEventsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessEventsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyProcessEventsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at DelinquencyProcessEventsLV.pcf: line 14, column 30
    function initialValue_0 () : java.util.Date {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'value' attribute on CheckBoxCell (id=Completed_Cell) at DelinquencyProcessEventsLV.pcf: line 25, column 36
    function sortValue_1 (event :  entity.DelinquencyProcessEvent) : java.lang.Object {
      return event.Completed
    }
    
    // 'value' attribute on DateCell (id=TargetDate_Cell) at DelinquencyProcessEventsLV.pcf: line 31, column 49
    function sortValue_2 (event :  entity.DelinquencyProcessEvent) : java.lang.Object {
      return event.TargetDate
    }
    
    // 'value' attribute on DateCell (id=CompletionDate_Cell) at DelinquencyProcessEventsLV.pcf: line 36, column 43
    function sortValue_3 (event :  entity.DelinquencyProcessEvent) : java.lang.Object {
      return event.CompletionTime
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at DelinquencyProcessEventsLV.pcf: line 41, column 190
    function sortValue_4 (event :  entity.DelinquencyProcessEvent) : java.lang.Object {
      return event.EventName == DelinquencyEventName.TC_DUNNINGLETTER1 ? DisplayKey.get("TDIC.Web.DelinquencyProcessEventsLV.EventName.Label") : event.EventName.Description
    }
    
    // 'value' attribute on TextCell (id=Trigger_Cell) at DelinquencyProcessEventsLV.pcf: line 47, column 25
    function sortValue_5 (event :  entity.DelinquencyProcessEvent) : java.lang.Object {
      return getTriggerBasisMessage( event )
    }
    
    // 'value' attribute on RowIterator at DelinquencyProcessEventsLV.pcf: line 20, column 52
    function value_24 () : entity.DelinquencyProcessEvent[] {
      return DelinquencyProcess.OrderedEvents?.toTypedArray()
    }
    
    property get DelinquencyProcess () : DelinquencyProcess {
      return getRequireValue("DelinquencyProcess", 0) as DelinquencyProcess
    }
    
    property set DelinquencyProcess ($arg :  DelinquencyProcess) {
      setRequireValue("DelinquencyProcess", 0, $arg)
    }
    
    property get today () : java.util.Date {
      return getVariableValue("today", 0) as java.util.Date
    }
    
    property set today ($arg :  java.util.Date) {
      setVariableValue("today", 0, $arg)
    }
    
    // helper functions
        function getEventTargetDateColor(event : entity.DelinquencyProcessEvent) : GWColor {
          if (!event.Completed && event.TargetDate <= today) return GWColor.THEME_PROGRESS_OVERDUE;
          return null;
        }
    
        function getTriggerBasisMessage(event : entity.DelinquencyProcessEvent) : String {
          if ((event.OffsetDays == null) || (event.OffsetDays == null) || (event.OffsetDays == 0)) {
            return event.TriggerBasis.DisplayName
          } else if (event.OffsetDays < -1) {
            return DisplayKey.get("Web.DelinquencyProcessEventsLV.TriggerBasis.WithNegativeOffsetDays", event.TriggerBasis, (-event.OffsetDays))
          } else if (event.OffsetDays < 0) {
            return DisplayKey.get("Web.DelinquencyProcessEventsLV.TriggerBasis.WithNegativeOffsetDay", event.TriggerBasis, (-event.OffsetDays))
          } else if (event.OffsetDays < 2) {
            return DisplayKey.get("Web.DelinquencyProcessEventsLV.TriggerBasis.WithPositiveOffsetDay", event.TriggerBasis, event.OffsetDays)
          }
          return DisplayKey.get("Web.DelinquencyProcessEventsLV.TriggerBasis.WithPositiveOffsetDays", event.TriggerBasis, event.OffsetDays)
        }
        
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessEventsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DelinquencyProcessEventsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'fontColor' attribute on DateCell (id=TargetDate_Cell) at DelinquencyProcessEventsLV.pcf: line 31, column 49
    function fontColor_10 () : java.lang.Object {
      return getEventTargetDateColor(event)
    }
    
    // 'value' attribute on CheckBoxCell (id=Completed_Cell) at DelinquencyProcessEventsLV.pcf: line 25, column 36
    function valueRoot_7 () : java.lang.Object {
      return event
    }
    
    // 'value' attribute on DateCell (id=TargetDate_Cell) at DelinquencyProcessEventsLV.pcf: line 31, column 49
    function value_11 () : java.util.Date {
      return event.TargetDate
    }
    
    // 'value' attribute on DateCell (id=CompletionDate_Cell) at DelinquencyProcessEventsLV.pcf: line 36, column 43
    function value_17 () : java.util.Date {
      return event.CompletionTime
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at DelinquencyProcessEventsLV.pcf: line 41, column 190
    function value_20 () : java.lang.String {
      return event.EventName == DelinquencyEventName.TC_DUNNINGLETTER1 ? DisplayKey.get("TDIC.Web.DelinquencyProcessEventsLV.EventName.Label") : event.EventName.Description
    }
    
    // 'value' attribute on TextCell (id=Trigger_Cell) at DelinquencyProcessEventsLV.pcf: line 47, column 25
    function value_22 () : java.lang.String {
      return getTriggerBasisMessage( event )
    }
    
    // 'value' attribute on CheckBoxCell (id=Completed_Cell) at DelinquencyProcessEventsLV.pcf: line 25, column 36
    function value_6 () : java.lang.Boolean {
      return event.Completed
    }
    
    // 'fontColor' attribute on DateCell (id=TargetDate_Cell) at DelinquencyProcessEventsLV.pcf: line 31, column 49
    function verifyFontColorIsAllowedType_13 ($$arg :  gw.api.web.color.GWColor) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=TargetDate_Cell) at DelinquencyProcessEventsLV.pcf: line 31, column 49
    function verifyFontColorIsAllowedType_13 ($$arg :  java.lang.String) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=TargetDate_Cell) at DelinquencyProcessEventsLV.pcf: line 31, column 49
    function verifyFontColor_14 () : void {
      var __fontColorArg = getEventTargetDateColor(event)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the fontColor return type is not a valid type for use with a ValueWidget's fontColor
      // It needs to return a String or a GWColor.
      verifyFontColorIsAllowedType_13(__fontColorArg)
    }
    
    // 'valueVisible' attribute on DateCell (id=TargetDate_Cell) at DelinquencyProcessEventsLV.pcf: line 31, column 49
    function visible_9 () : java.lang.Boolean {
      return event.hasTargetDate()
    }
    
    property get event () : entity.DelinquencyProcessEvent {
      return getIteratedValue(1) as entity.DelinquencyProcessEvent
    }
    
    
  }
  
  
}