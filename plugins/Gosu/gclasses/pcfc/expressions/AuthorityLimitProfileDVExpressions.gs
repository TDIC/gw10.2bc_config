package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/authoritylimits/AuthorityLimitProfileDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AuthorityLimitProfileDVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/authoritylimits/AuthorityLimitProfileDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AuthorityLimitProfileDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AuthorityLimitProfileDV.pcf: line 16, column 45
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      authorityLimitProfile.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at AuthorityLimitProfileDV.pcf: line 21, column 52
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      authorityLimitProfile.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=LimitAmount_Cell) at AuthorityLimitProfileDV.pcf: line 72, column 54
    function sortValue_11 (authorityLimit :  entity.AuthorityLimit) : java.lang.Object {
      var authorityLimitWrapper : gw.admin.AuthorityLimitWrapper = (new gw.admin.AuthorityLimitWrapper(authorityLimit))
return authorityLimitWrapper.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=LimitMonetaryAmount_Cell) at AuthorityLimitProfileDV.pcf: line 81, column 31
    function sortValue_13 (authorityLimit :  entity.AuthorityLimit) : java.lang.Object {
      return authorityLimit.LimitAmount
    }
    
    // 'value' attribute on TypeKeyCell (id=LimitType_Cell) at AuthorityLimitProfileDV.pcf: line 53, column 57
    function sortValue_8 (authorityLimit :  entity.AuthorityLimit) : java.lang.Object {
      return authorityLimit.LimitType
    }
    
    // 'value' attribute on TypeKeyCell (id=LimitCurrency_Cell) at AuthorityLimitProfileDV.pcf: line 61, column 74
    function sortValue_9 (authorityLimit :  entity.AuthorityLimit) : java.lang.Object {
      var authorityLimitWrapper : gw.admin.AuthorityLimitWrapper = (new gw.admin.AuthorityLimitWrapper(authorityLimit))
return authorityLimitWrapper.Currency
    }
    
    // 'toAdd' attribute on RowIterator at AuthorityLimitProfileDV.pcf: line 40, column 49
    function toAdd_34 (authorityLimit :  entity.AuthorityLimit) : void {
      authorityLimitProfile.addToLimits(createAuthorityLimit(authorityLimit))
    }
    
    // 'toRemove' attribute on RowIterator at AuthorityLimitProfileDV.pcf: line 40, column 49
    function toRemove_35 (authorityLimit :  entity.AuthorityLimit) : void {
      authorityLimitProfile.removeFromLimits(authorityLimit)
    }
    
    // 'validationExpression' attribute on ListViewInput at AuthorityLimitProfileDV.pcf: line 25, column 143
    function validationExpression_37 () : java.lang.Object {
      return !authorityLimitProfile.resolveAuthorityLimits() ? DisplayKey.get("Java.Error.AuthorityLimit") : null
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AuthorityLimitProfileDV.pcf: line 16, column 45
    function valueRoot_2 () : java.lang.Object {
      return authorityLimitProfile
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AuthorityLimitProfileDV.pcf: line 16, column 45
    function value_0 () : java.lang.String {
      return authorityLimitProfile.Name
    }
    
    // 'value' attribute on RowIterator at AuthorityLimitProfileDV.pcf: line 40, column 49
    function value_36 () : entity.AuthorityLimit[] {
      return authorityLimitProfile.Limits
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at AuthorityLimitProfileDV.pcf: line 21, column 52
    function value_4 () : java.lang.String {
      return authorityLimitProfile.Description
    }
    
    // 'visible' attribute on TypeKeyCell (id=LimitCurrency_Cell) at AuthorityLimitProfileDV.pcf: line 61, column 74
    function visible_10 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on TextCell (id=LimitAmount_Cell) at AuthorityLimitProfileDV.pcf: line 72, column 54
    function visible_12 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    property get authorityLimitProfile () : AuthorityLimitProfile {
      return getRequireValue("authorityLimitProfile", 0) as AuthorityLimitProfile
    }
    
    property set authorityLimitProfile ($arg :  AuthorityLimitProfile) {
      setRequireValue("authorityLimitProfile", 0, $arg)
    }
    
    function createAuthorityLimit(authorityLimit : AuthorityLimit) : AuthorityLimit {
      var defautCurrency = gw.api.util.CurrencyUtil.getDefaultCurrency()
      authorityLimit.LimitAmount = gw.api.financials.MonetaryAmounts.zeroIfNull(authorityLimit.LimitAmount, defautCurrency)
      return authorityLimit
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/authoritylimits/AuthorityLimitProfileDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AuthorityLimitProfileDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=LimitType_Cell) at AuthorityLimitProfileDV.pcf: line 53, column 57
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      authorityLimit.LimitType = (__VALUE_TO_SET as typekey.AuthorityLimitType)
    }
    
    // 'value' attribute on TypeKeyCell (id=LimitCurrency_Cell) at AuthorityLimitProfileDV.pcf: line 61, column 74
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      authorityLimitWrapper.Currency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on TextCell (id=LimitAmount_Cell) at AuthorityLimitProfileDV.pcf: line 72, column 54
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      authorityLimitWrapper.Amount = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'initialValue' attribute on Variable at AuthorityLimitProfileDV.pcf: line 44, column 54
    function initialValue_14 () : gw.admin.AuthorityLimitWrapper {
      return new gw.admin.AuthorityLimitWrapper(authorityLimit)
    }
    
    // RowIterator at AuthorityLimitProfileDV.pcf: line 40, column 49
    function initializeVariables_33 () : void {
        authorityLimitWrapper = new gw.admin.AuthorityLimitWrapper(authorityLimit);

    }
    
    // 'inputConversion' attribute on TextCell (id=LimitAmount_Cell) at AuthorityLimitProfileDV.pcf: line 72, column 54
    function inputConversion_24 (VALUE :  java.lang.String) : java.lang.Object {
      return VALUE == null ? 0 : VALUE
    }
    
    // 'value' attribute on TypeKeyCell (id=LimitType_Cell) at AuthorityLimitProfileDV.pcf: line 53, column 57
    function valueRoot_17 () : java.lang.Object {
      return authorityLimit
    }
    
    // 'value' attribute on TypeKeyCell (id=LimitCurrency_Cell) at AuthorityLimitProfileDV.pcf: line 61, column 74
    function valueRoot_21 () : java.lang.Object {
      return authorityLimitWrapper
    }
    
    // 'value' attribute on TypeKeyCell (id=LimitType_Cell) at AuthorityLimitProfileDV.pcf: line 53, column 57
    function value_15 () : typekey.AuthorityLimitType {
      return authorityLimit.LimitType
    }
    
    // 'value' attribute on TypeKeyCell (id=LimitCurrency_Cell) at AuthorityLimitProfileDV.pcf: line 61, column 74
    function value_19 () : typekey.Currency {
      return authorityLimitWrapper.Currency
    }
    
    // 'value' attribute on TextCell (id=LimitAmount_Cell) at AuthorityLimitProfileDV.pcf: line 72, column 54
    function value_25 () : java.math.BigDecimal {
      return authorityLimitWrapper.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=LimitMonetaryAmount_Cell) at AuthorityLimitProfileDV.pcf: line 81, column 31
    function value_30 () : gw.pl.currency.MonetaryAmount {
      return authorityLimit.LimitAmount
    }
    
    // 'visible' attribute on TypeKeyCell (id=LimitCurrency_Cell) at AuthorityLimitProfileDV.pcf: line 61, column 74
    function visible_22 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on TextCell (id=LimitAmount_Cell) at AuthorityLimitProfileDV.pcf: line 72, column 54
    function visible_28 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    property get authorityLimit () : entity.AuthorityLimit {
      return getIteratedValue(1) as entity.AuthorityLimit
    }
    
    property get authorityLimitWrapper () : gw.admin.AuthorityLimitWrapper {
      return getVariableValue("authorityLimitWrapper", 1) as gw.admin.AuthorityLimitWrapper
    }
    
    property set authorityLimitWrapper ($arg :  gw.admin.AuthorityLimitWrapper) {
      setVariableValue("authorityLimitWrapper", 1, $arg)
    }
    
    
  }
  
  
}