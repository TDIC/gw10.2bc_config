package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewProducerWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewProducerWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/NewProducerWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewProducerWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (currency :  Currency) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=NewProducerWizard) at NewProducerWizard.pcf: line 13, column 63
    function afterCancel_5 () : void {
      Producers.go()
    }
    
    // 'afterCancel' attribute on Wizard (id=NewProducerWizard) at NewProducerWizard.pcf: line 13, column 63
    function afterCancel_dest_6 () : pcf.api.Destination {
      return pcf.Producers.createDestination()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewProducerWizard) at NewProducerWizard.pcf: line 13, column 63
    function afterFinish_10 () : void {
      ProducerDetail.go(producer)
    }
    
    // 'afterFinish' attribute on Wizard (id=NewProducerWizard) at NewProducerWizard.pcf: line 13, column 63
    function afterFinish_dest_11 () : pcf.api.Destination {
      return pcf.ProducerDetail.createDestination(producer)
    }
    
    // 'initialValue' attribute on Variable at NewProducerWizard.pcf: line 19, column 24
    function initialValue_0 () : Producer {
      return createNewProducer()
    }
    
    // 'parent' attribute on Wizard (id=NewProducerWizard) at NewProducerWizard.pcf: line 13, column 63
    static function parent_7 (currency :  Currency) : pcf.api.Destination {
      return pcf.Producers.createDestination()
    }
    
    // 'screen' attribute on WizardStep (id=BasicsStep) at NewProducerWizard.pcf: line 27, column 79
    function screen_onEnter_1 (def :  pcf.NewProducerWizardBasicStepScreen) : void {
      def.onEnter(producer)
    }
    
    // 'screen' attribute on WizardStep (id=CodesStep) at NewProducerWizard.pcf: line 33, column 78
    function screen_onEnter_3 (def :  pcf.NewProducerWizardCodeStepScreen) : void {
      def.onEnter(producer)
    }
    
    // 'screen' attribute on WizardStep (id=BasicsStep) at NewProducerWizard.pcf: line 27, column 79
    function screen_refreshVariables_2 (def :  pcf.NewProducerWizardBasicStepScreen) : void {
      def.refreshVariables(producer)
    }
    
    // 'screen' attribute on WizardStep (id=CodesStep) at NewProducerWizard.pcf: line 33, column 78
    function screen_refreshVariables_4 (def :  pcf.NewProducerWizardCodeStepScreen) : void {
      def.refreshVariables(producer)
    }
    
    // 'tabBar' attribute on Wizard (id=NewProducerWizard) at NewProducerWizard.pcf: line 13, column 63
    function tabBar_onEnter_8 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=NewProducerWizard) at NewProducerWizard.pcf: line 13, column 63
    function tabBar_refreshVariables_9 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.NewProducerWizard {
      return super.CurrentLocation as pcf.NewProducerWizard
    }
    
    property get currency () : Currency {
      return getVariableValue("currency", 0) as Currency
    }
    
    property set currency ($arg :  Currency) {
      setVariableValue("currency", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    function createNewProducer() : Producer {
      return new Producer(CurrentLocation, currency)
    }
    
    
  }
  
  
}