package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/DBPaymentReversalConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DBPaymentReversalConfirmationPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/DBPaymentReversalConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DBPaymentReversalConfirmationPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (moneyReceived :  DirectBillMoneyRcvd) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=DBPaymentReversalConfirmationPopup) at DBPaymentReversalConfirmationPopup.pcf: line 11, column 81
    function beforeCommit_9 (pickedValue :  java.lang.Object) : void {
      reverseMoneyAndDist()
    }
    
    // 'canVisit' attribute on Popup (id=DBPaymentReversalConfirmationPopup) at DBPaymentReversalConfirmationPopup.pcf: line 11, column 81
    static function canVisit_10 (moneyReceived :  DirectBillMoneyRcvd) : java.lang.Boolean {
      return perm.System.acctpmntview and perm.DirectBillMoneyRcvd.pmntreverse
    }
    
    // 'value' attribute on TypeKeyInput (id=Reason_Input) at DBPaymentReversalConfirmationPopup.pcf: line 50, column 56
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      reason = (__VALUE_TO_SET as typekey.PaymentReversalReason)
    }
    
    // 'filter' attribute on TypeKeyInput (id=Reason_Input) at DBPaymentReversalConfirmationPopup.pcf: line 50, column 56
    function filter_7 (VALUE :  typekey.PaymentReversalReason, VALUES :  typekey.PaymentReversalReason[]) : java.lang.Boolean {
      return PaymentReversalReason.TF_UIREVERSALS.TypeKeys.contains(VALUE)
    }
    
    // 'initialValue' attribute on Variable at DBPaymentReversalConfirmationPopup.pcf: line 23, column 42
    function initialValue_0 () : gw.payment.DBPaymentHelper {
      return new gw.payment.DBPaymentHelper()
    }
    
    // 'initialValue' attribute on Variable at DBPaymentReversalConfirmationPopup.pcf: line 27, column 52
    function initialValue_1 () : tdic.payment.DBPaymentReversalHelper {
      return new tdic.payment.DBPaymentReversalHelper()
    }
    
    // 'label' attribute on AlertBar (id=negativeWarning) at DBPaymentReversalConfirmationPopup.pcf: line 37, column 53
    function label_3 () : java.lang.Object {
      return helper.processWarningMessage()
    }
    
    // 'label' attribute on Label (id=Confirmation) at DBPaymentReversalConfirmationPopup.pcf: line 42, column 371
    function label_4 () : java.lang.String {
      return (moneyReceived typeis ZeroDollarDMR) ? DisplayKey.get("Web.DBPaymentReversalConfirmation.CreditDistribution", (moneyReceived.DirectBillPayment.NetDistributedToInvoiceItems + moneyReceived.DirectBillPayment.NetInSuspense).render()) : DisplayKey.get("Web.DBPaymentReversalConfirmation.Confirmation", moneyReceived.Amount.render())
    }
    
    // 'value' attribute on TypeKeyInput (id=Reason_Input) at DBPaymentReversalConfirmationPopup.pcf: line 50, column 56
    function value_5 () : typekey.PaymentReversalReason {
      return reason
    }
    
    // 'visible' attribute on AlertBar (id=negativeWarning) at DBPaymentReversalConfirmationPopup.pcf: line 37, column 53
    function visible_2 () : java.lang.Boolean {
      return helper.isNegative(moneyReceived)
    }
    
    override property get CurrentLocation () : pcf.DBPaymentReversalConfirmationPopup {
      return super.CurrentLocation as pcf.DBPaymentReversalConfirmationPopup
    }
    
    property get helper () : gw.payment.DBPaymentHelper {
      return getVariableValue("helper", 0) as gw.payment.DBPaymentHelper
    }
    
    property set helper ($arg :  gw.payment.DBPaymentHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get helperTDIC () : tdic.payment.DBPaymentReversalHelper {
      return getVariableValue("helperTDIC", 0) as tdic.payment.DBPaymentReversalHelper
    }
    
    property set helperTDIC ($arg :  tdic.payment.DBPaymentReversalHelper) {
      setVariableValue("helperTDIC", 0, $arg)
    }
    
    property get moneyReceived () : DirectBillMoneyRcvd {
      return getVariableValue("moneyReceived", 0) as DirectBillMoneyRcvd
    }
    
    property set moneyReceived ($arg :  DirectBillMoneyRcvd) {
      setVariableValue("moneyReceived", 0, $arg)
    }
    
    property get reason () : PaymentReversalReason {
      return getVariableValue("reason", 0) as PaymentReversalReason
    }
    
    property set reason ($arg :  PaymentReversalReason) {
      setVariableValue("reason", 0, $arg)
    }
    
    function reverseMoneyAndDist() {
      if (moneyReceived.BaseDist != null ) {
        moneyReceived.BaseDist.reverse(reason)
      } else {
        moneyReceived.reverse(reason)
      }
      helperTDIC.createReversalFees(moneyReceived, reason)
    }
    
    
  }
  
  
}