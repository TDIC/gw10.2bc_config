package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducersMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducersMenuActionsExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducersMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ProducersMenuActionsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=ProducerCurrencyItem) at ProducersMenuActions.pcf: line 20, column 29
    function action_1 () : void {
      NewProducerWizard.go(currency)
    }
    
    // 'action' attribute on MenuItem (id=ProducerCurrencyItem) at ProducersMenuActions.pcf: line 20, column 29
    function action_dest_2 () : pcf.api.Destination {
      return pcf.NewProducerWizard.createDestination(currency)
    }
    
    // 'label' attribute on MenuItem (id=ProducerCurrencyItem) at ProducersMenuActions.pcf: line 20, column 29
    function label_3 () : java.lang.Object {
      return currency
    }
    
    property get currency () : typekey.Currency {
      return getIteratedValue(1) as typekey.Currency
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducersMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducersMenuActionsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=ProducersMenuActions_NewProducer) at ProducersMenuActions.pcf: line 11, column 38
    function action_6 () : void {
      NewProducerWizard.go(gw.api.util.CurrencyUtil.getDefaultCurrency())
    }
    
    // 'action' attribute on MenuItem (id=ProducersMenuActions_NewProducer) at ProducersMenuActions.pcf: line 11, column 38
    function action_dest_7 () : pcf.api.Destination {
      return pcf.NewProducerWizard.createDestination(gw.api.util.CurrencyUtil.getDefaultCurrency())
    }
    
    // 'value' attribute on MenuItemIterator at ProducersMenuActions.pcf: line 16, column 66
    function value_4 () : java.util.List<typekey.Currency> {
      return Currency.getTypeKeys(false)
    }
    
    // 'visible' attribute on MenuItemIterator at ProducersMenuActions.pcf: line 16, column 66
    function visible_0 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on MenuItem (id=ProducersMenuActions_NewProducer) at ProducersMenuActions.pcf: line 11, column 38
    function visible_5 () : java.lang.Boolean {
      return perm.Producer.create
    }
    
    
  }
  
  
}