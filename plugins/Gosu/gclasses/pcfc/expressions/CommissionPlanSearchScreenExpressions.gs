package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/CommissionPlanSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionPlanSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/search/CommissionPlanSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionPlanSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get isClearBundle () : boolean {
      return getRequireValue("isClearBundle", 0) as java.lang.Boolean
    }
    
    property set isClearBundle ($arg :  boolean) {
      setRequireValue("isClearBundle", 0, $arg)
    }
    
    property get tier () : ProducerTier {
      return getRequireValue("tier", 0) as ProducerTier
    }
    
    property set tier ($arg :  ProducerTier) {
      setRequireValue("tier", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/CommissionPlanSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at CommissionPlanSearchScreen.pcf: line 65, column 46
    function action_16 () : void {
      CommissionPlanDetailPopup.push(commissionPlan)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at CommissionPlanSearchScreen.pcf: line 65, column 46
    function action_dest_17 () : pcf.api.Destination {
      return pcf.CommissionPlanDetailPopup.createDestination(commissionPlan)
    }
    
    // 'outputConversion' attribute on TextCell (id=BasePercentage_Cell) at CommissionPlanSearchScreen.pcf: line 46, column 51
    function outputConversion_5 (VALUE :  java.math.BigDecimal) : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(VALUE, 1)
    }
    
    // 'pickValue' attribute on RowIterator at CommissionPlanSearchScreen.pcf: line 38, column 87
    function pickValue_21 () : CommissionPlan {
      return commissionPlan
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at CommissionPlanSearchScreen.pcf: line 55, column 55
    function valueRoot_11 () : java.lang.Object {
      return commissionPlan
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at CommissionPlanSearchScreen.pcf: line 55, column 55
    function value_10 () : java.util.Date {
      return commissionPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at CommissionPlanSearchScreen.pcf: line 59, column 56
    function value_13 () : java.util.Date {
      return commissionPlan.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CommissionPlanSearchScreen.pcf: line 65, column 46
    function value_18 () : java.lang.String {
      return commissionPlan.Name
    }
    
    // 'value' attribute on TextCell (id=BasePercentage_Cell) at CommissionPlanSearchScreen.pcf: line 46, column 51
    function value_6 () : java.math.BigDecimal {
      return commissionPlan.DefaultSubPlan.getBaseRate(TC_PRIMARY)
    }
    
    // 'value' attribute on BooleanRadioCell (id=IncludesIncentives_Cell) at CommissionPlanSearchScreen.pcf: line 51, column 81
    function value_8 () : java.lang.Boolean {
      return commissionPlan.DefaultSubPlan.Incentives.length > 0
    }
    
    property get commissionPlan () : entity.CommissionPlan {
      return getIteratedValue(2) as entity.CommissionPlan
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/CommissionPlanSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends CommissionPlanSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at CommissionPlanSearchScreen.pcf: line 25, column 54
    function def_onEnter_0 (def :  pcf.CommissionPlanSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at CommissionPlanSearchScreen.pcf: line 25, column 54
    function def_refreshVariables_1 (def :  pcf.CommissionPlanSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'maxSearchResults' attribute on SearchPanel at CommissionPlanSearchScreen.pcf: line 23, column 82
    function maxSearchResults_23 () : java.lang.Object {
      return gw.api.system.BCConfigParameters.MaxSearchResults.Value
    }
    
    // 'searchCriteria' attribute on SearchPanel at CommissionPlanSearchScreen.pcf: line 23, column 82
    function searchCriteria_25 () : entity.CommissionPlanSearchCriteria {
      return new CommissionPlanSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at CommissionPlanSearchScreen.pcf: line 23, column 82
    function search_24 () : java.lang.Object {
      return searchCriteria.performSearch(tier, isClearBundle) as gw.api.database.IQueryBeanResult<CommissionPlan>
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at CommissionPlanSearchScreen.pcf: line 55, column 55
    function sortValue_2 (commissionPlan :  entity.CommissionPlan) : java.lang.Object {
      return commissionPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at CommissionPlanSearchScreen.pcf: line 59, column 56
    function sortValue_3 (commissionPlan :  entity.CommissionPlan) : java.lang.Object {
      return commissionPlan.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CommissionPlanSearchScreen.pcf: line 65, column 46
    function sortValue_4 (commissionPlan :  entity.CommissionPlan) : java.lang.Object {
      return commissionPlan.Name
    }
    
    // 'value' attribute on RowIterator at CommissionPlanSearchScreen.pcf: line 38, column 87
    function value_22 () : gw.api.database.IQueryBeanResult<entity.CommissionPlan> {
      return commissionPlans
    }
    
    property get commissionPlans () : gw.api.database.IQueryBeanResult<CommissionPlan> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<CommissionPlan>
    }
    
    property get searchCriteria () : entity.CommissionPlanSearchCriteria {
      return getCriteriaValue(1) as entity.CommissionPlanSearchCriteria
    }
    
    property set searchCriteria ($arg :  entity.CommissionPlanSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}