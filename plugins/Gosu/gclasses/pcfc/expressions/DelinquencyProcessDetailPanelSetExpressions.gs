package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessDetailPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyProcessDetailPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessDetailPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyProcessDetailPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at DelinquencyProcessDetailPanelSet.pcf: line 13, column 24
    function def_onEnter_0 (def :  pcf.DelinquencyProcessDetailsDV) : void {
      def.onEnter(delinquencyProcess)
    }
    
    // 'def' attribute on InputSetRef at DelinquencyProcessDetailPanelSet.pcf: line 19, column 92
    function def_onEnter_2 (def :  pcf.AccountEvaluationInputSet) : void {
      def.onEnter(delinquencyProcess.Account, delinquencyProcess)
    }
    
    // 'def' attribute on ListViewInput at DelinquencyProcessDetailPanelSet.pcf: line 23, column 95
    function def_onEnter_4 (def :  pcf.DelinquencyProcessActivitiesLV) : void {
      def.onEnter(delinquencyProcess)
    }
    
    // 'def' attribute on ListViewInput at DelinquencyProcessDetailPanelSet.pcf: line 29, column 91
    function def_onEnter_6 (def :  pcf.DelinquencyProcessEventsLV) : void {
      def.onEnter(delinquencyProcess)
    }
    
    // 'def' attribute on PanelRef at DelinquencyProcessDetailPanelSet.pcf: line 13, column 24
    function def_refreshVariables_1 (def :  pcf.DelinquencyProcessDetailsDV) : void {
      def.refreshVariables(delinquencyProcess)
    }
    
    // 'def' attribute on InputSetRef at DelinquencyProcessDetailPanelSet.pcf: line 19, column 92
    function def_refreshVariables_3 (def :  pcf.AccountEvaluationInputSet) : void {
      def.refreshVariables(delinquencyProcess.Account, delinquencyProcess)
    }
    
    // 'def' attribute on ListViewInput at DelinquencyProcessDetailPanelSet.pcf: line 23, column 95
    function def_refreshVariables_5 (def :  pcf.DelinquencyProcessActivitiesLV) : void {
      def.refreshVariables(delinquencyProcess)
    }
    
    // 'def' attribute on ListViewInput at DelinquencyProcessDetailPanelSet.pcf: line 29, column 91
    function def_refreshVariables_7 (def :  pcf.DelinquencyProcessEventsLV) : void {
      def.refreshVariables(delinquencyProcess)
    }
    
    property get delinquencyProcess () : DelinquencyProcess {
      return getRequireValue("delinquencyProcess", 0) as DelinquencyProcess
    }
    
    property set delinquencyProcess ($arg :  DelinquencyProcess) {
      setRequireValue("delinquencyProcess", 0, $arg)
    }
    
    
  }
  
  
}