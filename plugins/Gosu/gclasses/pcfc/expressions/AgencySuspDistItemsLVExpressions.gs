package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/AgencySuspDistItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencySuspDistItemsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/AgencySuspDistItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencySuspDistItemsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on DateCell (id=EventDate_Cell) at AgencySuspDistItemsLV.pcf: line 58, column 52
    function label_2 () : java.lang.Object {
      return DisplayKey.get(gw.agencybill.AgencyBillSuspenseItemsHelper.getColumnLabel(suspenseDistItems))
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencySuspDistItemsLV.pcf: line 46, column 25
    function sortValue_0 (item :  entity.BaseSuspDistItem) : java.lang.Object {
      return item.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=MatchingPolicy_Cell) at AgencySuspDistItemsLV.pcf: line 52, column 53
    function sortValue_1 (item :  entity.BaseSuspDistItem) : java.lang.Object {
      return item.MatchingPolicy.PolicyNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossToApply_Cell) at AgencySuspDistItemsLV.pcf: line 63, column 44
    function sortValue_3 (item :  entity.BaseSuspDistItem) : java.lang.Object {
      return item.GrossAmountToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionToApply_Cell) at AgencySuspDistItemsLV.pcf: line 68, column 49
    function sortValue_4 (item :  entity.BaseSuspDistItem) : java.lang.Object {
      return item.CommissionAmountToApply
    }
    
    // 'sortBy' attribute on TextCell (id=Age_Cell) at AgencySuspDistItemsLV.pcf: line 81, column 42
    function sortValue_5 (item :  entity.BaseSuspDistItem) : java.lang.Object {
      return item.CreateTime
    }
    
    // 'value' attribute on RowIterator at AgencySuspDistItemsLV.pcf: line 20, column 83
    function value_43 () : gw.api.database.IQueryBeanResult<entity.BaseSuspDistItem> {
      return suspenseDistItems
    }
    
    property get suspenseDistItems () : gw.api.database.IQueryBeanResult<BaseSuspDistItem> {
      return getRequireValue("suspenseDistItems", 0) as gw.api.database.IQueryBeanResult<BaseSuspDistItem>
    }
    
    property set suspenseDistItems ($arg :  gw.api.database.IQueryBeanResult<BaseSuspDistItem>) {
      setRequireValue("suspenseDistItems", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/AgencySuspDistItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AgencySuspDistItemsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=MatchingPolicy_Cell) at AgencySuspDistItemsLV.pcf: line 52, column 53
    function actionAvailable_17 () : java.lang.Boolean {
      return item.MatchingPolicy != null
    }
    
    // 'action' attribute on TextCell (id=MatchingPolicy_Cell) at AgencySuspDistItemsLV.pcf: line 52, column 53
    function action_15 () : void {
      PolicyOverview.go( item.MatchingPolicy )
    }
    
    // 'action' attribute on DateCell (id=EventDate_Cell) at AgencySuspDistItemsLV.pcf: line 58, column 52
    function action_21 () : void {
      if(agencyCycleDist.BaseMoneyReceived typeis AgencyBillMoneyRcvd) {AgencyBillExecutedPayments.go(producer, (agencyCycleDist.BaseMoneyReceived as AgencyBillMoneyRcvd))} else{AgencyBillExecutedPromises.go(producer, agencyCycleDist as AgencyCyclePromise)}
    }
    
    // 'action' attribute on TextCell (id=Producer_Cell) at AgencySuspDistItemsLV.pcf: line 40, column 25
    function action_8 () : void {
      ProducerDetail.go(producer)
    }
    
    // 'action' attribute on TextCell (id=MatchingPolicy_Cell) at AgencySuspDistItemsLV.pcf: line 52, column 53
    function action_dest_16 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination( item.MatchingPolicy )
    }
    
    // 'action' attribute on TextCell (id=Producer_Cell) at AgencySuspDistItemsLV.pcf: line 40, column 25
    function action_dest_9 () : pcf.api.Destination {
      return pcf.ProducerDetail.createDestination(producer)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=GrossToApply_Cell) at AgencySuspDistItemsLV.pcf: line 63, column 44
    function currency_29 () : typekey.Currency {
      return item.Currency
    }
    
    // 'initialValue' attribute on Variable at AgencySuspDistItemsLV.pcf: line 24, column 33
    function initialValue_6 () : AgencyCycleDist {
      return item.BaseDist as AgencyCycleDist
    }
    
    // 'initialValue' attribute on Variable at AgencySuspDistItemsLV.pcf: line 28, column 26
    function initialValue_7 () : Producer {
      return agencyCycleDist.Producer
    }
    
    // RowIterator at AgencySuspDistItemsLV.pcf: line 20, column 83
    function initializeVariables_42 () : void {
        agencyCycleDist = item.BaseDist as AgencyCycleDist;
  producer = agencyCycleDist.Producer;

    }
    
    // 'label' attribute on DateCell (id=EventDate_Cell) at AgencySuspDistItemsLV.pcf: line 58, column 52
    function label_22 () : java.lang.Object {
      return DisplayKey.get(gw.agencybill.AgencyBillSuspenseItemsHelper.getColumnLabel(suspenseDistItems))
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencySuspDistItemsLV.pcf: line 46, column 25
    function valueRoot_13 () : java.lang.Object {
      return item
    }
    
    // 'value' attribute on TextCell (id=MatchingPolicy_Cell) at AgencySuspDistItemsLV.pcf: line 52, column 53
    function valueRoot_19 () : java.lang.Object {
      return item.MatchingPolicy
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at AgencySuspDistItemsLV.pcf: line 58, column 52
    function valueRoot_24 () : java.lang.Object {
      return agencyCycleDist
    }
    
    // 'value' attribute on TextCell (id=Age_Cell) at AgencySuspDistItemsLV.pcf: line 81, column 42
    function valueRoot_40 () : java.lang.Object {
      return item.CreateTime
    }
    
    // 'value' attribute on TextCell (id=Producer_Cell) at AgencySuspDistItemsLV.pcf: line 40, column 25
    function value_10 () : entity.Producer {
      return producer
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencySuspDistItemsLV.pcf: line 46, column 25
    function value_12 () : java.lang.String {
      return item.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=MatchingPolicy_Cell) at AgencySuspDistItemsLV.pcf: line 52, column 53
    function value_18 () : java.lang.String {
      return item.MatchingPolicy.PolicyNumber
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at AgencySuspDistItemsLV.pcf: line 58, column 52
    function value_23 () : java.util.Date {
      return agencyCycleDist.DistributedDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossToApply_Cell) at AgencySuspDistItemsLV.pcf: line 63, column 44
    function value_27 () : gw.pl.currency.MonetaryAmount {
      return item.GrossAmountToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionToApply_Cell) at AgencySuspDistItemsLV.pcf: line 68, column 49
    function value_31 () : gw.pl.currency.MonetaryAmount {
      return item.CommissionAmountToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetToApply_Cell) at AgencySuspDistItemsLV.pcf: line 75, column 42
    function value_35 () : gw.pl.currency.MonetaryAmount {
      return item.NetAmountToApply
    }
    
    // 'value' attribute on TextCell (id=Age_Cell) at AgencySuspDistItemsLV.pcf: line 81, column 42
    function value_39 () : java.lang.Integer {
      return item.CreateTime.DaysSince
    }
    
    property get agencyCycleDist () : AgencyCycleDist {
      return getVariableValue("agencyCycleDist", 1) as AgencyCycleDist
    }
    
    property set agencyCycleDist ($arg :  AgencyCycleDist) {
      setVariableValue("agencyCycleDist", 1, $arg)
    }
    
    property get item () : entity.BaseSuspDistItem {
      return getIteratedValue(1) as entity.BaseSuspDistItem
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 1) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 1, $arg)
    }
    
    
  }
  
  
}