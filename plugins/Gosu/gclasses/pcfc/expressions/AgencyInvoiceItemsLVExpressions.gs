package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyInvoiceItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyInvoiceItemsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyInvoiceItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyInvoiceItemsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AgencyInvoiceItemsLV.pcf: line 27, column 43
    function sortValue_0 (item :  entity.InvoiceItem) : java.lang.Object {
      return item.Invoice.EventDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Description_Cell) at AgencyInvoiceItemsLV.pcf: line 32, column 48
    function sortValue_1 (item :  entity.InvoiceItem) : java.lang.Object {
      return item.Type
    }
    
    // 'value' attribute on TextCell (id=PolicyPeriod_Cell) at AgencyInvoiceItemsLV.pcf: line 37, column 44
    function sortValue_2 (item :  entity.InvoiceItem) : java.lang.Object {
      return item.PolicyPeriod
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AgencyInvoiceItemsLV.pcf: line 42, column 32
    function sortValue_3 (item :  entity.InvoiceItem) : java.lang.Object {
      return item.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionAmount_Cell) at AgencyInvoiceItemsLV.pcf: line 48, column 49
    function sortValue_4 (item :  entity.InvoiceItem) : java.lang.Object {
      return item.PrimaryCommissionAmount
    }
    
    // 'value' attribute on TextCell (id=InsuredName_Cell) at AgencyInvoiceItemsLV.pcf: line 52, column 67
    function sortValue_5 (item :  entity.InvoiceItem) : java.lang.Object {
      return item.PolicyPeriod.Account.Insured.Contact.Name
    }
    
    // 'value' attribute on RowIterator at AgencyInvoiceItemsLV.pcf: line 19, column 40
    function value_26 () : entity.InvoiceItem[] {
      return invoiceItems
    }
    
    property get invoiceItems () : InvoiceItem[] {
      return getRequireValue("invoiceItems", 0) as InvoiceItem[]
    }
    
    property set invoiceItems ($arg :  InvoiceItem[]) {
      setRequireValue("invoiceItems", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyInvoiceItemsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AgencyInvoiceItemsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at AgencyInvoiceItemsLV.pcf: line 42, column 32
    function currency_17 () : typekey.Currency {
      return item.Currency
    }
    
    // 'value' attribute on TypeKeyCell (id=Description_Cell) at AgencyInvoiceItemsLV.pcf: line 32, column 48
    function valueRoot_10 () : java.lang.Object {
      return item
    }
    
    // 'value' attribute on TextCell (id=InsuredName_Cell) at AgencyInvoiceItemsLV.pcf: line 52, column 67
    function valueRoot_24 () : java.lang.Object {
      return item.PolicyPeriod.Account.Insured.Contact
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AgencyInvoiceItemsLV.pcf: line 27, column 43
    function valueRoot_7 () : java.lang.Object {
      return item.Invoice
    }
    
    // 'value' attribute on TextCell (id=PolicyPeriod_Cell) at AgencyInvoiceItemsLV.pcf: line 37, column 44
    function value_12 () : entity.PolicyPeriod {
      return item.PolicyPeriod
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AgencyInvoiceItemsLV.pcf: line 42, column 32
    function value_15 () : gw.pl.currency.MonetaryAmount {
      return item.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionAmount_Cell) at AgencyInvoiceItemsLV.pcf: line 48, column 49
    function value_19 () : gw.pl.currency.MonetaryAmount {
      return item.PrimaryCommissionAmount
    }
    
    // 'value' attribute on TextCell (id=InsuredName_Cell) at AgencyInvoiceItemsLV.pcf: line 52, column 67
    function value_23 () : java.lang.String {
      return item.PolicyPeriod.Account.Insured.Contact.Name
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AgencyInvoiceItemsLV.pcf: line 27, column 43
    function value_6 () : java.util.Date {
      return item.Invoice.EventDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Description_Cell) at AgencyInvoiceItemsLV.pcf: line 32, column 48
    function value_9 () : typekey.InvoiceItemType {
      return item.Type
    }
    
    property get item () : entity.InvoiceItem {
      return getIteratedValue(1) as entity.InvoiceItem
    }
    
    
  }
  
  
}