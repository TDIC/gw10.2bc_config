package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlanDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionPlanDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlanDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionPlanDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (commissionPlan :  CommissionPlan) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=CommissionPlanDetail) at CommissionPlanDetail.pcf: line 10, column 33
    function canEdit_2 () : java.lang.Boolean {
      return perm.System.commplanedit
    }
    
    // 'def' attribute on ScreenRef at CommissionPlanDetail.pcf: line 17, column 57
    function def_onEnter_0 (def :  pcf.CommissionPlanDetailScreen) : void {
      def.onEnter(commissionPlan)
    }
    
    // 'def' attribute on ScreenRef at CommissionPlanDetail.pcf: line 17, column 57
    function def_refreshVariables_1 (def :  pcf.CommissionPlanDetailScreen) : void {
      def.refreshVariables(commissionPlan)
    }
    
    // 'parent' attribute on Page (id=CommissionPlanDetail) at CommissionPlanDetail.pcf: line 10, column 33
    static function parent_3 (commissionPlan :  CommissionPlan) : pcf.api.Destination {
      return pcf.CommissionPlans.createDestination()
    }
    
    // 'title' attribute on Page (id=CommissionPlanDetail) at CommissionPlanDetail.pcf: line 10, column 33
    static function title_4 (commissionPlan :  CommissionPlan) : java.lang.Object {
      return commissionPlan.Name
    }
    
    override property get CurrentLocation () : pcf.CommissionPlanDetail {
      return super.CurrentLocation as pcf.CommissionPlanDetail
    }
    
    property get commissionPlan () : CommissionPlan {
      return getVariableValue("commissionPlan", 0) as CommissionPlan
    }
    
    property set commissionPlan ($arg :  CommissionPlan) {
      setVariableValue("commissionPlan", 0, $arg)
    }
    
    
  }
  
  
}