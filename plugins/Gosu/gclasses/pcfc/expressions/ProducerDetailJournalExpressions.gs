package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerDetailJournal.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerDetailJournalExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerDetailJournal.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerDetailJournalExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=ProducerDetailJournal) at ProducerDetailJournal.pcf: line 9, column 73
    static function canVisit_2 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodjournalview
    }
    
    // 'def' attribute on ScreenRef at ProducerDetailJournal.pcf: line 16, column 62
    function def_onEnter_0 (def :  pcf.JournalScreen) : void {
      def.onEnter(producer, producer.ProducerCodes)
    }
    
    // 'def' attribute on ScreenRef at ProducerDetailJournal.pcf: line 16, column 62
    function def_refreshVariables_1 (def :  pcf.JournalScreen) : void {
      def.refreshVariables(producer, producer.ProducerCodes)
    }
    
    // Page (id=ProducerDetailJournal) at ProducerDetailJournal.pcf: line 9, column 73
    static function parent_3 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    override property get CurrentLocation () : pcf.ProducerDetailJournal {
      return super.CurrentLocation as pcf.ProducerDetailJournal
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}