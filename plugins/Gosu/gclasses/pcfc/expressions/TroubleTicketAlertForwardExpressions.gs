package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketAlertForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TroubleTicketAlertForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketAlertForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TroubleTicketAlertForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (entity :  HoldRelatedEntity) : int {
      return 0
    }
    
    static function __constructorIndex (entity :  HoldRelatedEntity, policyPeriod :  PolicyPeriod) : int {
      return 1
    }
    
    // 'action' attribute on ForwardCondition at TroubleTicketAlertForward.pcf: line 23, column 47
    function action_1 () : void {
      TroubleTicketDetailsPopup.push(entity.FirstActiveTroubleTicket)
    }
    
    // 'action' attribute on ForwardCondition at TroubleTicketAlertForward.pcf: line 32, column 41
    function action_10 () : void {
      PolicyDetailTroubleTickets.go(policyPeriod, true, false)
    }
    
    // 'action' attribute on ForwardCondition at TroubleTicketAlertForward.pcf: line 35, column 43
    function action_13 () : void {
      ProducerDetailTroubleTickets.go(entity as Producer)
    }
    
    // 'action' attribute on ForwardCondition at TroubleTicketAlertForward.pcf: line 26, column 42
    function action_4 () : void {
      AccountDetailTroubleTickets.go(entity as Account)
    }
    
    // 'action' attribute on ForwardCondition at TroubleTicketAlertForward.pcf: line 29, column 47
    function action_7 () : void {
      PolicyDetailTroubleTickets.go(entity as PolicyPeriod, false, true)
    }
    
    // 'action' attribute on ForwardCondition at TroubleTicketAlertForward.pcf: line 32, column 41
    function action_dest_11 () : pcf.api.Destination {
      return pcf.PolicyDetailTroubleTickets.createDestination(policyPeriod, true, false)
    }
    
    // 'action' attribute on ForwardCondition at TroubleTicketAlertForward.pcf: line 35, column 43
    function action_dest_14 () : pcf.api.Destination {
      return pcf.ProducerDetailTroubleTickets.createDestination(entity as Producer)
    }
    
    // 'action' attribute on ForwardCondition at TroubleTicketAlertForward.pcf: line 23, column 47
    function action_dest_2 () : pcf.api.Destination {
      return pcf.TroubleTicketDetailsPopup.createDestination(entity.FirstActiveTroubleTicket)
    }
    
    // 'action' attribute on ForwardCondition at TroubleTicketAlertForward.pcf: line 26, column 42
    function action_dest_5 () : pcf.api.Destination {
      return pcf.AccountDetailTroubleTickets.createDestination(entity as Account)
    }
    
    // 'action' attribute on ForwardCondition at TroubleTicketAlertForward.pcf: line 29, column 47
    function action_dest_8 () : pcf.api.Destination {
      return pcf.PolicyDetailTroubleTickets.createDestination(entity as PolicyPeriod, false, true)
    }
    
    // 'condition' attribute on ForwardCondition at TroubleTicketAlertForward.pcf: line 32, column 41
    function condition_12 () : java.lang.Boolean {
      return entity typeis Policy
    }
    
    // 'condition' attribute on ForwardCondition at TroubleTicketAlertForward.pcf: line 35, column 43
    function condition_15 () : java.lang.Boolean {
      return entity typeis Producer
    }
    
    // 'condition' attribute on ForwardCondition at TroubleTicketAlertForward.pcf: line 23, column 47
    function condition_3 () : java.lang.Boolean {
      return troubleTickets.length == 1
    }
    
    // 'condition' attribute on ForwardCondition at TroubleTicketAlertForward.pcf: line 26, column 42
    function condition_6 () : java.lang.Boolean {
      return entity typeis Account
    }
    
    // 'condition' attribute on ForwardCondition at TroubleTicketAlertForward.pcf: line 29, column 47
    function condition_9 () : java.lang.Boolean {
      return entity typeis PolicyPeriod
    }
    
    // 'initialValue' attribute on Variable at TroubleTicketAlertForward.pcf: line 17, column 31
    function initialValue_0 () : TroubleTicket[] {
      return entity.ActiveTroubleTickets
    }
    
    override property get CurrentLocation () : pcf.TroubleTicketAlertForward {
      return super.CurrentLocation as pcf.TroubleTicketAlertForward
    }
    
    property get entity () : HoldRelatedEntity {
      return getVariableValue("entity", 0) as HoldRelatedEntity
    }
    
    property set entity ($arg :  HoldRelatedEntity) {
      setVariableValue("entity", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get troubleTickets () : TroubleTicket[] {
      return getVariableValue("troubleTickets", 0) as TroubleTicket[]
    }
    
    property set troubleTickets ($arg :  TroubleTicket[]) {
      setVariableValue("troubleTickets", 0, $arg)
    }
    
    
  }
  
  
}