package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.pl.currency.MonetaryAmount
@javax.annotation.Generated("config/web/pcf/account/AccountDBPaymentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDBPaymentsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDBPaymentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDBPaymentsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at AccountDBPaymentsLV.pcf: line 20, column 83
    function initialValue_0 () : java.util.Map<entity.PaymentMoneyReceived, java.lang.Boolean> {
      return new java.util.HashMap<entity.PaymentMoneyReceived, Boolean>().toAutoMap(\ c -> c.Modified)
    }
    
    // 'value' attribute on TextCell (id=ReversedReason_Cell) at AccountDBPaymentsLV.pcf: line 87, column 41
    function sortValue_1 (moneyReceived :  entity.PaymentMoneyReceived) : java.lang.Object {
      return moneyReceived.ReversalReason.DisplayName
    }
    
    // 'value' attribute on TextCell (id=FundUsedForTransfer_Cell) at AccountDBPaymentsLV.pcf: line 92, column 41
    function sortValue_2 (moneyReceived :  entity.PaymentMoneyReceived) : java.lang.Object {
      return moneyReceived.FundUsedForTransfer_TDIC ? "Yes" : " "
    }
    
    // 'value' attribute on MonetaryAmountCell (id=FeeAmount_Cell) at AccountDBPaymentsLV.pcf: line 120, column 53
    function sortValue_3 (moneyReceived :  entity.PaymentMoneyReceived) : java.lang.Object {
      return validateFeeAmount(moneyReceived)
    }
    
    // 'value' attribute on TextCell (id=ReceivedBy_Cell) at AccountDBPaymentsLV.pcf: line 183, column 36
    function sortValue_7 (moneyReceived :  entity.PaymentMoneyReceived) : java.lang.Object {
      return moneyReceived.CreateUser
    }
    
    // 'value' attribute on RowIterator (id=MoneyReceivedIterator) at AccountDBPaymentsLV.pcf: line 26, column 87
    function value_93 () : gw.api.database.IQueryBeanResult<entity.PaymentMoneyReceived> {
      return moneyReceiveds
    }
    
    // 'visible' attribute on TextCell (id=Policy_Cell) at AccountDBPaymentsLV.pcf: line 148, column 34
    function visible_4 () : java.lang.Boolean {
      return showTargets
    }
    
    // 'visible' attribute on CheckBoxCell (id=Frozen_Cell) at AccountDBPaymentsLV.pcf: line 177, column 76
    function visible_6 () : java.lang.Boolean {
      return gw.api.system.PLConfigParameters.ArchiveEnabled.Value
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get isMoneyModifiedMap () : java.util.Map<entity.PaymentMoneyReceived, java.lang.Boolean> {
      return getVariableValue("isMoneyModifiedMap", 0) as java.util.Map<entity.PaymentMoneyReceived, java.lang.Boolean>
    }
    
    property set isMoneyModifiedMap ($arg :  java.util.Map<entity.PaymentMoneyReceived, java.lang.Boolean>) {
      setVariableValue("isMoneyModifiedMap", 0, $arg)
    }
    
    property get moneyReceiveds () : gw.api.database.IQueryBeanResult<PaymentMoneyReceived> {
      return getRequireValue("moneyReceiveds", 0) as gw.api.database.IQueryBeanResult<PaymentMoneyReceived>
    }
    
    property set moneyReceiveds ($arg :  gw.api.database.IQueryBeanResult<PaymentMoneyReceived>) {
      setRequireValue("moneyReceiveds", 0, $arg)
    }
    
    property get showTargets () : boolean {
      return getRequireValue("showTargets", 0) as java.lang.Boolean
    }
    
    property set showTargets ($arg :  boolean) {
      setRequireValue("showTargets", 0, $arg)
    }
    
    
    function getAmountDistributed(moneyReceived: entity.PaymentMoneyReceived, payment: entity.DirectBillPayment) : gw.pl.currency.MonetaryAmount {
      return moneyReceived.DistributedDenorm ? payment.NetDistributedToInvoiceItems : 0bd.ofCurrency(account.Currency)
    }
    
    function getSuspenseAmount(moneyReceived: entity.PaymentMoneyReceived, payment: entity.DirectBillPayment) : gw.pl.currency.MonetaryAmount {
      return moneyReceived.DistributedDenorm ? payment.SuspDistItemsThatHaveNotBeenReleased.sum(account.Currency, \ b -> b.GrossAmountToApply) : 0bd.ofCurrency(account.Currency)
    }
    
    function getCollateralAmount(moneyReceived: entity.PaymentMoneyReceived, payment: entity.DirectBillPayment) : gw.pl.currency.MonetaryAmount {
      return moneyReceived.DistributedDenorm ? payment.CollateralPaymentItems.where(\ c -> c.ReversedDate == null).sum(account.Currency, \ b -> b.GrossAmountToApply) : 0bd.ofCurrency(account.Currency)
    }
    
    // For performance reasons as this will only get evaluated when the Transaction action is clicked rather then at page render time
    function getMoneyTransactionOnlyWhenGoingToTransactionDetail(moneyReceived: PaymentMoneyReceived) : Transaction {
       return moneyReceived.MoneyReceivedTransaction
    }
    
    function validateFeeAmount(paymentmoneyReceived : PaymentMoneyReceived): MonetaryAmount{
      var ccFeeAmount = ScriptParameters.getParameterValue("TDIC_CreditCardFeeAmount") as double
      var feeAmount = new MonetaryAmount(ccFeeAmount, Currency.TC_USD)
      if(paymentmoneyReceived.PaymentInstrument.PaymentMethod ==  PaymentMethod.TC_CREDITCARD && paymentmoneyReceived.CCFeeWaiverReason_TDIC == null){
        return feeAmount
      }
      return null
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDBPaymentsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountDBPaymentsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=ModifyMoney) at AccountDBPaymentsLV.pcf: line 41, column 84
    function action_10 () : void {
      ModifyDirectBillPaymentPopup.push(account, moneyReceived as DirectBillMoneyRcvd, gw.api.web.payment.WhenModifyingDirectBillMoney.DoNotEditDistribution)
    }
    
    // 'action' attribute on MenuItem (id=ModifyDistribution) at AccountDBPaymentsLV.pcf: line 46, column 91
    function action_13 () : void {
      ModifyDirectBillPaymentPopup.push(account, moneyReceived as DirectBillMoneyRcvd, gw.api.web.payment.WhenModifyingDirectBillMoney.EditDistribution)
    }
    
    // 'action' attribute on MenuItem (id=ReversePayment) at AccountDBPaymentsLV.pcf: line 51, column 87
    function action_16 () : void {
      DBPaymentReversalConfirmationPopup.push(moneyReceived as DirectBillMoneyRcvd)
    }
    
    // 'action' attribute on MenuItem (id=MoveToAccount) at AccountDBPaymentsLV.pcf: line 56, column 86
    function action_19 () : void {
      MoveDirectBillPayment.push(account, moneyReceived as DirectBillMoneyRcvd)
    }
    
    // 'action' attribute on MenuItem (id=MoveToSuspense) at AccountDBPaymentsLV.pcf: line 61, column 87
    function action_22 () : void {
      MovePaymentToSuspense.push(account, moneyReceived as DirectBillMoneyRcvd)
    }
    
    // 'action' attribute on MenuItem (id=Undistribute) at AccountDBPaymentsLV.pcf: line 66, column 85
    function action_25 () : void {
      DBUndistributePaymentConfirmationPopup.push(payment)
    }
    
    // 'action' attribute on MenuItem (id=ViewTransaction) at AccountDBPaymentsLV.pcf: line 71, column 84
    function action_28 () : void {
      AccountTransactionDetail.push(account, getMoneyTransactionOnlyWhenGoingToTransactionDetail(moneyReceived))
    }
    
    // 'action' attribute on TextCell (id=Policy_Cell) at AccountDBPaymentsLV.pcf: line 148, column 34
    function action_70 () : void {
      PolicySummary.go((moneyReceived as DirectBillMoneyRcvd).PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=Invoice_Cell) at AccountDBPaymentsLV.pcf: line 156, column 34
    function action_76 () : void {
      AccountDetailInvoices.go(account, ((moneyReceived as DirectBillMoneyRcvd).Invoice) as AccountInvoice)
    }
    
    // 'action' attribute on MenuItem (id=ModifyMoney) at AccountDBPaymentsLV.pcf: line 41, column 84
    function action_dest_11 () : pcf.api.Destination {
      return pcf.ModifyDirectBillPaymentPopup.createDestination(account, moneyReceived as DirectBillMoneyRcvd, gw.api.web.payment.WhenModifyingDirectBillMoney.DoNotEditDistribution)
    }
    
    // 'action' attribute on MenuItem (id=ModifyDistribution) at AccountDBPaymentsLV.pcf: line 46, column 91
    function action_dest_14 () : pcf.api.Destination {
      return pcf.ModifyDirectBillPaymentPopup.createDestination(account, moneyReceived as DirectBillMoneyRcvd, gw.api.web.payment.WhenModifyingDirectBillMoney.EditDistribution)
    }
    
    // 'action' attribute on MenuItem (id=ReversePayment) at AccountDBPaymentsLV.pcf: line 51, column 87
    function action_dest_17 () : pcf.api.Destination {
      return pcf.DBPaymentReversalConfirmationPopup.createDestination(moneyReceived as DirectBillMoneyRcvd)
    }
    
    // 'action' attribute on MenuItem (id=MoveToAccount) at AccountDBPaymentsLV.pcf: line 56, column 86
    function action_dest_20 () : pcf.api.Destination {
      return pcf.MoveDirectBillPayment.createDestination(account, moneyReceived as DirectBillMoneyRcvd)
    }
    
    // 'action' attribute on MenuItem (id=MoveToSuspense) at AccountDBPaymentsLV.pcf: line 61, column 87
    function action_dest_23 () : pcf.api.Destination {
      return pcf.MovePaymentToSuspense.createDestination(account, moneyReceived as DirectBillMoneyRcvd)
    }
    
    // 'action' attribute on MenuItem (id=Undistribute) at AccountDBPaymentsLV.pcf: line 66, column 85
    function action_dest_26 () : pcf.api.Destination {
      return pcf.DBUndistributePaymentConfirmationPopup.createDestination(payment)
    }
    
    // 'action' attribute on MenuItem (id=ViewTransaction) at AccountDBPaymentsLV.pcf: line 71, column 84
    function action_dest_29 () : pcf.api.Destination {
      return pcf.AccountTransactionDetail.createDestination(account, getMoneyTransactionOnlyWhenGoingToTransactionDetail(moneyReceived))
    }
    
    // 'action' attribute on TextCell (id=Policy_Cell) at AccountDBPaymentsLV.pcf: line 148, column 34
    function action_dest_71 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination((moneyReceived as DirectBillMoneyRcvd).PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=Invoice_Cell) at AccountDBPaymentsLV.pcf: line 156, column 34
    function action_dest_77 () : pcf.api.Destination {
      return pcf.AccountDetailInvoices.createDestination(account, ((moneyReceived as DirectBillMoneyRcvd).Invoice) as AccountInvoice)
    }
    
    // 'available' attribute on MenuItem (id=ModifyDistribution) at AccountDBPaymentsLV.pcf: line 46, column 91
    function available_12 () : java.lang.Boolean {
      return !(moneyReceived typeis ZeroDollarReversal) && !moneyReceived.Frozen
    }
    
    // 'available' attribute on MenuItem (id=ReversePayment) at AccountDBPaymentsLV.pcf: line 51, column 87
    function available_15 () : java.lang.Boolean {
      return !(moneyReceived typeis ZeroDollarReversal) && !moneyReceived.Reversed && !isMoneyModifiedMap[moneyReceived] && !moneyReceived.Frozen && (payment == null || !payment.hasFrozenDistItem())
    }
    
    // 'available' attribute on MenuItem (id=MoveToAccount) at AccountDBPaymentsLV.pcf: line 56, column 86
    function available_18 () : java.lang.Boolean {
      return !(moneyReceived typeis ZeroDollarDMR) && !moneyReceived.Frozen && (payment == null || !payment.hasFrozenDistItem())
    }
    
    // 'available' attribute on MenuItem (id=MoveToSuspense) at AccountDBPaymentsLV.pcf: line 61, column 87
    function available_21 () : java.lang.Boolean {
      return !(moneyReceived typeis ZeroDollarDMR) && !moneyReceived.Frozen and (payment == null || !payment.hasFrozenDistItem())
    }
    
    // 'available' attribute on MenuItem (id=Undistribute) at AccountDBPaymentsLV.pcf: line 66, column 85
    function available_24 () : java.lang.Boolean {
      return !(moneyReceived typeis ZeroDollarDMR) && payment != null && payment.DistributedDate != null && moneyReceived.DistributedDenorm && !moneyReceived.Frozen and !payment.hasFrozenDistItem()
    }
    
    // 'available' attribute on MenuItem (id=ViewTransaction) at AccountDBPaymentsLV.pcf: line 71, column 84
    function available_27 () : java.lang.Boolean {
      return !(moneyReceived typeis ZeroDollarDMR) && moneyReceived.Executed
    }
    
    // 'available' attribute on ButtonCell (id=ActionButton_Cell) at AccountDBPaymentsLV.pcf: line 36, column 290
    function available_30 () : java.lang.Boolean {
      return !moneyReceived.Reversed && !(moneyReceived typeis ZeroDollarReversal) && !isMoneyModifiedMap[moneyReceived] && !moneyReceived.FundUsedForTransfer_TDIC
    }
    
    // 'available' attribute on MenuItem (id=ModifyMoney) at AccountDBPaymentsLV.pcf: line 41, column 84
    function available_9 () : java.lang.Boolean {
      return !(moneyReceived typeis ZeroDollarDMR) && !moneyReceived.Frozen && moneyReceived.BaseDist == null
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=FeeAmount_Cell) at AccountDBPaymentsLV.pcf: line 120, column 53
    function currency_58 () : typekey.Currency {
      return moneyReceived.Currency
    }
    
    // 'initialValue' attribute on Variable at AccountDBPaymentsLV.pcf: line 30, column 35
    function initialValue_8 () : DirectBillPayment {
      return moneyReceived.BaseDist as DirectBillPayment
    }
    
    // RowIterator (id=MoneyReceivedIterator) at AccountDBPaymentsLV.pcf: line 26, column 87
    function initializeVariables_92 () : void {
        payment = moneyReceived.BaseDist as DirectBillPayment;

    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AccountDBPaymentsLV.pcf: line 77, column 47
    function valueRoot_35 () : java.lang.Object {
      return moneyReceived
    }
    
    // 'value' attribute on TextCell (id=ReversedReason_Cell) at AccountDBPaymentsLV.pcf: line 87, column 41
    function valueRoot_41 () : java.lang.Object {
      return moneyReceived.ReversalReason
    }
    
    // 'value' attribute on TextCell (id=feewaiverdescription_Cell) at AccountDBPaymentsLV.pcf: line 110, column 41
    function valueRoot_52 () : java.lang.Object {
      return moneyReceived.CCFeeWaiverReason_TDIC
    }
    
    // 'value' attribute on TextCell (id=UnappliedFund_Cell) at AccountDBPaymentsLV.pcf: line 125, column 98
    function valueRoot_61 () : java.lang.Object {
      return (moneyReceived as DirectBillMoneyRcvd).UnappliedFund
    }
    
    // 'value' attribute on TextCell (id=Policy_Cell) at AccountDBPaymentsLV.pcf: line 148, column 34
    function valueRoot_73 () : java.lang.Object {
      return (moneyReceived as DirectBillMoneyRcvd)
    }
    
    // 'value' attribute on ButtonCell (id=ActionButton_Cell) at AccountDBPaymentsLV.pcf: line 36, column 290
    function value_31 () : java.lang.String {
      return (moneyReceived.Frozen && (moneyReceived typeis ZeroDollarDMR)) ? "" : (moneyReceived.Reversed || isMoneyModifiedMap[moneyReceived]) ? DisplayKey.get("Web.AccountPaymentsLV.Reversed") : DisplayKey.get("Web.AccountPaymentsLV.Actions")
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AccountDBPaymentsLV.pcf: line 77, column 47
    function value_34 () : java.util.Date {
      return moneyReceived.ReceivedDate
    }
    
    // 'value' attribute on DateCell (id=Reversed_Cell) at AccountDBPaymentsLV.pcf: line 82, column 47
    function value_37 () : java.util.Date {
      return moneyReceived.ReversalDate
    }
    
    // 'value' attribute on TextCell (id=ReversedReason_Cell) at AccountDBPaymentsLV.pcf: line 87, column 41
    function value_40 () : java.lang.String {
      return moneyReceived.ReversalReason.DisplayName
    }
    
    // 'value' attribute on TextCell (id=FundUsedForTransfer_Cell) at AccountDBPaymentsLV.pcf: line 92, column 41
    function value_43 () : java.lang.String {
      return moneyReceived.FundUsedForTransfer_TDIC ? "Yes" : " "
    }
    
    // 'value' attribute on TextCell (id=Method_Cell) at AccountDBPaymentsLV.pcf: line 98, column 49
    function value_45 () : entity.PaymentInstrument {
      return moneyReceived.PaymentInstrument
    }
    
    // 'value' attribute on TextCell (id=CheckRef_Cell) at AccountDBPaymentsLV.pcf: line 104, column 44
    function value_48 () : java.lang.String {
      return moneyReceived.RefNumber
    }
    
    // 'value' attribute on TextCell (id=feewaiverdescription_Cell) at AccountDBPaymentsLV.pcf: line 110, column 41
    function value_51 () : java.lang.String {
      return moneyReceived.CCFeeWaiverReason_TDIC.DisplayName
    }
    
    // 'value' attribute on TextCell (id=feewaiverreason_Cell) at AccountDBPaymentsLV.pcf: line 115, column 61
    function value_54 () : java.lang.String {
      return moneyReceived.CCFeeWaiverReasonDesc_TDIC
    }
    
    // 'value' attribute on MonetaryAmountCell (id=FeeAmount_Cell) at AccountDBPaymentsLV.pcf: line 120, column 53
    function value_57 () : gw.pl.currency.MonetaryAmount {
      return validateFeeAmount(moneyReceived)
    }
    
    // 'value' attribute on TextCell (id=UnappliedFund_Cell) at AccountDBPaymentsLV.pcf: line 125, column 98
    function value_60 () : java.lang.String {
      return (moneyReceived as DirectBillMoneyRcvd).UnappliedFund.TAccountShortDisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AccountDBPaymentsLV.pcf: line 132, column 41
    function value_63 () : gw.pl.currency.MonetaryAmount {
      return moneyReceived.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amountDistributed_Cell) at AccountDBPaymentsLV.pcf: line 139, column 65
    function value_67 () : gw.pl.currency.MonetaryAmount {
      return getAmountDistributed(moneyReceived, payment)
    }
    
    // 'value' attribute on TextCell (id=Policy_Cell) at AccountDBPaymentsLV.pcf: line 148, column 34
    function value_72 () : entity.PolicyPeriod {
      return (moneyReceived as DirectBillMoneyRcvd).PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=Invoice_Cell) at AccountDBPaymentsLV.pcf: line 156, column 34
    function value_78 () : entity.Invoice {
      return (moneyReceived as DirectBillMoneyRcvd).Invoice
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amountInSuspense_Cell) at AccountDBPaymentsLV.pcf: line 163, column 62
    function value_82 () : gw.pl.currency.MonetaryAmount {
      return getSuspenseAmount(moneyReceived, payment)
    }
    
    // 'value' attribute on CheckBoxCell (id=Frozen_Cell) at AccountDBPaymentsLV.pcf: line 177, column 76
    function value_85 () : java.lang.Boolean {
      return moneyReceived.Frozen
    }
    
    // 'value' attribute on TextCell (id=ReceivedBy_Cell) at AccountDBPaymentsLV.pcf: line 183, column 36
    function value_89 () : entity.User {
      return moneyReceived.CreateUser
    }
    
    // 'visible' attribute on TextCell (id=Policy_Cell) at AccountDBPaymentsLV.pcf: line 148, column 34
    function visible_74 () : java.lang.Boolean {
      return showTargets
    }
    
    // 'visible' attribute on CheckBoxCell (id=Frozen_Cell) at AccountDBPaymentsLV.pcf: line 177, column 76
    function visible_87 () : java.lang.Boolean {
      return gw.api.system.PLConfigParameters.ArchiveEnabled.Value
    }
    
    property get moneyReceived () : entity.PaymentMoneyReceived {
      return getIteratedValue(1) as entity.PaymentMoneyReceived
    }
    
    property get payment () : DirectBillPayment {
      return getVariableValue("payment", 1) as DirectBillPayment
    }
    
    property set payment ($arg :  DirectBillPayment) {
      setVariableValue("payment", 1, $arg)
    }
    
    
  }
  
  
}