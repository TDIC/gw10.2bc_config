package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultipleAccountsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SelectMultipleAccountsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultipleAccountsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SelectMultipleAccountsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'def' attribute on ScreenRef at SelectMultipleAccountsPopup.pcf: line 12, column 45
    function def_onEnter_0 (def :  pcf.SelectMultipleAccountsScreen) : void {
      def.onEnter()
    }
    
    // 'def' attribute on ScreenRef at SelectMultipleAccountsPopup.pcf: line 12, column 45
    function def_refreshVariables_1 (def :  pcf.SelectMultipleAccountsScreen) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.SelectMultipleAccountsPopup {
      return super.CurrentLocation as pcf.SelectMultipleAccountsPopup
    }
    
    
  }
  
  
}