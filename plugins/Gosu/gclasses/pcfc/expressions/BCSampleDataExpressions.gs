package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/tools/BCSampleData.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BCSampleDataExpressions {
  @javax.annotation.Generated("config/web/pcf/tools/BCSampleData.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BCSampleDataExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=ImportSampleDataAction) at BCSampleData.pcf: line 24, column 27
    function action_1 () : void {
      loadSampleDataSet()
    }
    
    // 'def' attribute on PanelRef at BCSampleData.pcf: line 18, column 46
    function def_onEnter_2 (def :  pcf.UnsupportedToolsDisclaimerDV) : void {
      def.onEnter()
    }
    
    // 'def' attribute on PanelRef at BCSampleData.pcf: line 18, column 46
    function def_refreshVariables_3 (def :  pcf.UnsupportedToolsDisclaimerDV) : void {
      def.refreshVariables()
    }
    
    // 'label' attribute on Verbatim at BCSampleData.pcf: line 16, column 25
    function label_0 () : java.lang.String {
      return result
    }
    
    // Page (id=BCSampleData) at BCSampleData.pcf: line 8, column 70
    static function parent_4 () : pcf.api.Destination {
      return pcf.UnsupportedTools.createDestination()
    }
    
    override property get CurrentLocation () : pcf.BCSampleData {
      return super.CurrentLocation as pcf.BCSampleData
    }
    
    property get result () : java.lang.String {
      return getVariableValue("result", 0) as java.lang.String
    }
    
    property set result ($arg :  java.lang.String) {
      setVariableValue("result", 0, $arg)
    }
    
    function loadSampleDataSet() {
      result = ""
      gw.api.util.SampleDataGenerator.generateDefaultSampleData()
      result = DisplayKey.get("Web.InternalTools.SampleData.SampleDataImported")
    }
    
    
  }
  
  
}