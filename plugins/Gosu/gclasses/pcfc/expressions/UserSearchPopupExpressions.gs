package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/usersearch/UserSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class UserSearchPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/usersearch/UserSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class UserSearchPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'def' attribute on ScreenRef at UserSearchPopup.pcf: line 12, column 33
    function def_onEnter_0 (def :  pcf.UserSearchScreen) : void {
      def.onEnter()
    }
    
    // 'def' attribute on ScreenRef at UserSearchPopup.pcf: line 12, column 33
    function def_refreshVariables_1 (def :  pcf.UserSearchScreen) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.UserSearchPopup {
      return super.CurrentLocation as pcf.UserSearchPopup
    }
    
    
  }
  
  
}