package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadBatchLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DataUploadBatchLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadBatchLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DataUploadBatchLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadBatchLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on DateCell (id=runDate_Cell) at DataUploadBatchLV.pcf: line 29, column 39
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Batch.ExecutionDate")
    }
    
    // 'label' attribute on TextCell (id=batchName_Cell) at DataUploadBatchLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Batch.Name")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadBatchLV.pcf: line 23, column 46
    function sortValue_1 (batch :  tdic.util.dataloader.data.sampledata.BatchData) : java.lang.Object {
      return processor.getLoadStatus(batch)
    }
    
    // 'value' attribute on DateCell (id=runDate_Cell) at DataUploadBatchLV.pcf: line 29, column 39
    function sortValue_4 (batch :  tdic.util.dataloader.data.sampledata.BatchData) : java.lang.Object {
      return batch.EntryDate
    }
    
    // 'value' attribute on TextCell (id=batchName_Cell) at DataUploadBatchLV.pcf: line 35, column 41
    function sortValue_6 (batch :  tdic.util.dataloader.data.sampledata.BatchData) : java.lang.Object {
      return batch.BatchName
    }
    
    // 'value' attribute on RowIterator (id=batchID) at DataUploadBatchLV.pcf: line 15, column 93
    function value_23 () : java.util.ArrayList<tdic.util.dataloader.data.sampledata.BatchData> {
      return processor.BatchArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadBatchLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCSampleDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCSampleDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadBatchLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DataUploadBatchLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at DataUploadBatchLV.pcf: line 17, column 90
    function highlighted_22 () : java.lang.Boolean {
      return (batch.Error or batch.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on DateCell (id=runDate_Cell) at DataUploadBatchLV.pcf: line 29, column 39
    function label_12 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Batch.ExecutionDate")
    }
    
    // 'label' attribute on TextCell (id=batchName_Cell) at DataUploadBatchLV.pcf: line 35, column 41
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Batch.Name")
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadBatchLV.pcf: line 23, column 46
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'value' attribute on DateCell (id=runDate_Cell) at DataUploadBatchLV.pcf: line 29, column 39
    function valueRoot_14 () : java.lang.Object {
      return batch
    }
    
    // 'value' attribute on DateCell (id=runDate_Cell) at DataUploadBatchLV.pcf: line 29, column 39
    function value_13 () : java.util.Date {
      return batch.EntryDate
    }
    
    // 'value' attribute on TextCell (id=batchName_Cell) at DataUploadBatchLV.pcf: line 35, column 41
    function value_18 () : java.lang.String {
      return batch.BatchName
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadBatchLV.pcf: line 23, column 46
    function value_8 () : java.lang.String {
      return processor.getLoadStatus(batch)
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadBatchLV.pcf: line 23, column 46
    function visible_9 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get batch () : tdic.util.dataloader.data.sampledata.BatchData {
      return getIteratedValue(1) as tdic.util.dataloader.data.sampledata.BatchData
    }
    
    
  }
  
  
}