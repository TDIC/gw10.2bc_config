package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountPaymentPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountPaymentPlanLVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountPaymentPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountPaymentPlanLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on RowIterator (id=PaymentPlanRowIterator) at AccountPaymentPlanLV.pcf: line 20, column 60
    function pickLocation_18 () : void {
      AccountAddPaymentPlanPopup.push(account, account.PaymentPlans)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AccountPaymentPlanLV.pcf: line 26, column 37
    function sortValue_0 (paymentPlan :  entity.PaymentPlan) : java.lang.Object {
      return paymentPlan.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AccountPaymentPlanLV.pcf: line 30, column 44
    function sortValue_1 (paymentPlan :  entity.PaymentPlan) : java.lang.Object {
      return paymentPlan.Description
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at AccountPaymentPlanLV.pcf: line 34, column 46
    function sortValue_2 (paymentPlan :  entity.PaymentPlan) : java.lang.Object {
      return paymentPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at AccountPaymentPlanLV.pcf: line 38, column 47
    function sortValue_3 (paymentPlan :  entity.PaymentPlan) : java.lang.Object {
      return paymentPlan.ExpirationDate
    }
    
    // 'toAdd' attribute on RowIterator (id=PaymentPlanRowIterator) at AccountPaymentPlanLV.pcf: line 20, column 60
    function toAdd_19 (paymentPlan :  entity.PaymentPlan) : void {
      account.addToPaymentPlans(paymentPlan)
    }
    
    // 'value' attribute on RowIterator (id=PaymentPlanRowIterator) at AccountPaymentPlanLV.pcf: line 20, column 60
    function value_20 () : java.util.List<entity.PaymentPlan> {
      return account.PaymentPlans
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountPaymentPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountPaymentPlanLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at AccountPaymentPlanLV.pcf: line 26, column 37
    function action_4 () : void {
      PaymentPlanDetail.go(paymentPlan)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at AccountPaymentPlanLV.pcf: line 26, column 37
    function action_dest_5 () : pcf.api.Destination {
      return pcf.PaymentPlanDetail.createDestination(paymentPlan)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AccountPaymentPlanLV.pcf: line 26, column 37
    function valueRoot_7 () : java.lang.Object {
      return paymentPlan
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at AccountPaymentPlanLV.pcf: line 34, column 46
    function value_12 () : java.util.Date {
      return paymentPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at AccountPaymentPlanLV.pcf: line 38, column 47
    function value_15 () : java.util.Date {
      return paymentPlan.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AccountPaymentPlanLV.pcf: line 26, column 37
    function value_6 () : java.lang.String {
      return paymentPlan.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AccountPaymentPlanLV.pcf: line 30, column 44
    function value_9 () : java.lang.String {
      return paymentPlan.Description
    }
    
    property get paymentPlan () : entity.PaymentPlan {
      return getIteratedValue(1) as entity.PaymentPlan
    }
    
    
  }
  
  
}