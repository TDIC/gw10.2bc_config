package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/WriteoffSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class WriteoffSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/WriteoffSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class WriteoffSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at WriteoffSearchDV.pcf: line 89, column 36
    function action_37 () : void {
      AccountSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=PolicyPicker) at WriteoffSearchDV.pcf: line 102, column 41
    function action_48 () : void {
      PolicySearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at WriteoffSearchDV.pcf: line 89, column 36
    function action_dest_38 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=PolicyPicker) at WriteoffSearchDV.pcf: line 102, column 41
    function action_dest_49 () : pcf.api.Destination {
      return pcf.PolicySearchPopup.createDestination()
    }
    
    // 'available' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at WriteoffSearchDV.pcf: line 47, column 43
    function available_15 () : java.lang.Boolean {
      return writeoffSearch.Currency != null || gw.api.util.CurrencyUtil.isSingleCurrencyMode()
    }
    
    // 'available' attribute on TextInput (id=AccountCriterion_Input) at WriteoffSearchDV.pcf: line 89, column 36
    function available_39 () : java.lang.Boolean {
      return accountEditable
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at WriteoffSearchDV.pcf: line 47, column 43
    function currency_19 () : typekey.Currency {
      return gw.search.SearchHelper.getDefaultCurrencyIfNull(writeoffSearch.Currency)
    }
    
    // 'def' attribute on InputSetRef at WriteoffSearchDV.pcf: line 112, column 41
    function def_onEnter_55 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at WriteoffSearchDV.pcf: line 112, column 41
    function def_refreshVariables_56 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at WriteoffSearchDV.pcf: line 37, column 66
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      writeoffSearch.Currency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at WriteoffSearchDV.pcf: line 47, column 43
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      writeoffSearch.MinAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at WriteoffSearchDV.pcf: line 54, column 43
    function defaultSetter_24 (__VALUE_TO_SET :  java.lang.Object) : void {
      writeoffSearch.MaxAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TypeKeyInput (id=ReasonCriterion_Input) at WriteoffSearchDV.pcf: line 30, column 45
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      writeoffSearch.Reason = (__VALUE_TO_SET as typekey.WriteoffReason)
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at WriteoffSearchDV.pcf: line 62, column 46
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      writeoffSearch.EarliestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at WriteoffSearchDV.pcf: line 67, column 44
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      writeoffSearch.LatestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=AccountCriterion_Input) at WriteoffSearchDV.pcf: line 89, column 36
    function defaultSetter_43 (__VALUE_TO_SET :  java.lang.Object) : void {
      writeoffSearch.Account = (__VALUE_TO_SET as entity.Account)
    }
    
    // 'value' attribute on TextInput (id=PolicyPeriodCriterion_Input) at WriteoffSearchDV.pcf: line 102, column 41
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      writeoffSearch.PolicyPeriod = (__VALUE_TO_SET as entity.PolicyPeriod)
    }
    
    // 'editable' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at WriteoffSearchDV.pcf: line 37, column 66
    function editable_7 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode() && accountEditable
    }
    
    // 'initialValue' attribute on Variable at WriteoffSearchDV.pcf: line 16, column 57
    function initialValue_0 () : gw.api.web.account.AccountSearchConverter {
      return new gw.api.web.account.AccountSearchConverter(StringCriterionMode.TC_STARTSWITH)
    }
    
    // 'initialValue' attribute on Variable at WriteoffSearchDV.pcf: line 20, column 55
    function initialValue_1 () : gw.api.web.policy.PolicySearchConverter {
      return new gw.api.web.policy.PolicySearchConverter(StringCriterionMode.TC_STARTSWITH)
    }
    
    // 'inputConversion' attribute on TextInput (id=AccountCriterion_Input) at WriteoffSearchDV.pcf: line 89, column 36
    function inputConversion_41 (VALUE :  java.lang.String) : java.lang.Object {
      return accountSearchConverter.getAccount(VALUE)
    }
    
    // 'inputConversion' attribute on TextInput (id=PolicyPeriodCriterion_Input) at WriteoffSearchDV.pcf: line 102, column 41
    function inputConversion_50 (VALUE :  java.lang.String) : java.lang.Object {
      return policySearchConverter.getPolicyPeriod(VALUE)
    }
    
    // 'onChange' attribute on PostOnChange at WriteoffSearchDV.pcf: line 39, column 69
    function onChange_6 () : void {
      writeoffSearch.blankMinimumAndMaximumFields()
    }
    
    // 'value' attribute on TypeKeyInput (id=ReasonCriterion_Input) at WriteoffSearchDV.pcf: line 30, column 45
    function valueRoot_4 () : java.lang.Object {
      return writeoffSearch
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at WriteoffSearchDV.pcf: line 47, column 43
    function value_16 () : gw.pl.currency.MonetaryAmount {
      return writeoffSearch.MinAmount
    }
    
    // 'value' attribute on TypeKeyInput (id=ReasonCriterion_Input) at WriteoffSearchDV.pcf: line 30, column 45
    function value_2 () : typekey.WriteoffReason {
      return writeoffSearch.Reason
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at WriteoffSearchDV.pcf: line 54, column 43
    function value_23 () : gw.pl.currency.MonetaryAmount {
      return writeoffSearch.MaxAmount
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at WriteoffSearchDV.pcf: line 62, column 46
    function value_29 () : java.util.Date {
      return writeoffSearch.EarliestDate
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at WriteoffSearchDV.pcf: line 67, column 44
    function value_33 () : java.util.Date {
      return writeoffSearch.LatestDate
    }
    
    // 'value' attribute on TextInput (id=AccountCriterion_Input) at WriteoffSearchDV.pcf: line 89, column 36
    function value_42 () : entity.Account {
      return writeoffSearch.Account
    }
    
    // 'value' attribute on TextInput (id=PolicyPeriodCriterion_Input) at WriteoffSearchDV.pcf: line 102, column 41
    function value_51 () : entity.PolicyPeriod {
      return writeoffSearch.PolicyPeriod
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at WriteoffSearchDV.pcf: line 37, column 66
    function value_9 () : typekey.Currency {
      return writeoffSearch.Currency
    }
    
    // 'visible' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at WriteoffSearchDV.pcf: line 37, column 66
    function visible_8 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    property get accountEditable () : Boolean {
      return getRequireValue("accountEditable", 0) as Boolean
    }
    
    property set accountEditable ($arg :  Boolean) {
      setRequireValue("accountEditable", 0, $arg)
    }
    
    property get accountSearchConverter () : gw.api.web.account.AccountSearchConverter {
      return getVariableValue("accountSearchConverter", 0) as gw.api.web.account.AccountSearchConverter
    }
    
    property set accountSearchConverter ($arg :  gw.api.web.account.AccountSearchConverter) {
      setVariableValue("accountSearchConverter", 0, $arg)
    }
    
    property get policySearchConverter () : gw.api.web.policy.PolicySearchConverter {
      return getVariableValue("policySearchConverter", 0) as gw.api.web.policy.PolicySearchConverter
    }
    
    property set policySearchConverter ($arg :  gw.api.web.policy.PolicySearchConverter) {
      setVariableValue("policySearchConverter", 0, $arg)
    }
    
    property get writeoffSearch () : gw.search.WriteoffSearchCriteria {
      return getRequireValue("writeoffSearch", 0) as gw.search.WriteoffSearchCriteria
    }
    
    property set writeoffSearch ($arg :  gw.search.WriteoffSearchCriteria) {
      setRequireValue("writeoffSearch", 0, $arg)
    }
    
    
  }
  
  
}