package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailAcceleratedMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailAcceleratedMenuActionsExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailAcceleratedMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailAcceleratedMenuActionsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get period () : PolicyPeriod {
      return getRequireValue("period", 0) as PolicyPeriod
    }
    
    property set period ($arg :  PolicyPeriod) {
      setRequireValue("period", 0, $arg)
    }
    
    function getCompactLabel(policyPeriod : PolicyPeriod) : String{
      return DisplayKey.get("Web.PolicyDetail.PolicyPeriod.DateRange",
          policyPeriod.TermNumber, policyPeriod.PolicyPerEffDate.AsUIStyle, policyPeriod.EndDate.AsUIStyle)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailAcceleratedMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyGroup_EffectiveDateExpressionsImpl extends PolicyDetailAcceleratedMenuActionsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=PolicyGroup_EffectiveDate) at PolicyDetailAcceleratedMenuActions.pcf: line 17, column 40
    function action_8 () : void {
      PolicyGroup.go(ArgValue)
    }
    
    // 'action' attribute on Link (id=PolicyGroup_EffectiveDate) at PolicyDetailAcceleratedMenuActions.pcf: line 17, column 40
    function action_dest_9 () : pcf.api.Destination {
      return pcf.PolicyGroup.createDestination(ArgValue)
    }
    
    // 'value' attribute on Link (id=PolicyGroup_EffectiveDate) at PolicyDetailAcceleratedMenuActions.pcf: line 17, column 40
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      ArgValue = (__VALUE_TO_SET as entity.PolicyPeriod)
    }
    
    // 'argInitialValue' attribute on Link (id=PolicyGroup_EffectiveDate) at PolicyDetailAcceleratedMenuActions.pcf: line 17, column 40
    function initArg_10 () : void {
      ArgValue = period;
    }
    
    // 'action' attribute on Link (id=PolicyGroup_EffectiveDate) at PolicyDetailAcceleratedMenuActions.pcf: line 17, column 40
    function onChange_0 () : void {
      PolicyGroup.go(ArgValue)
    }
    
    // 'action' attribute on Link (id=PolicyGroup_EffectiveDate) at PolicyDetailAcceleratedMenuActions.pcf: line 17, column 40
    function onChange_dest_1 () : pcf.api.Destination {
      return pcf.PolicyGroup.createDestination(ArgValue)
    }
    
    // 'argOptionLabel' attribute on Link (id=PolicyGroup_EffectiveDate) at PolicyDetailAcceleratedMenuActions.pcf: line 17, column 40
    function optionLabel_4 (VALUE :  entity.PolicyPeriod) : java.lang.String {
      return getCompactLabel(VALUE)
    }
    
    // 'argRange' attribute on Link (id=PolicyGroup_EffectiveDate) at PolicyDetailAcceleratedMenuActions.pcf: line 17, column 40
    function valueRange_5 () : java.lang.Object {
      return gw.api.util.ArrayUtil.reverse(period.Policy.OrderedPolicyPeriods)
    }
    
    // Link (id=PolicyGroup_EffectiveDate) at PolicyDetailAcceleratedMenuActions.pcf: line 17, column 40
    function value_2 () : entity.PolicyPeriod {
      return ArgValue
    }
    
    // 'argRange' attribute on Link (id=PolicyGroup_EffectiveDate) at PolicyDetailAcceleratedMenuActions.pcf: line 17, column 40
    function verifyValueRangeIsAllowedType_6 ($$arg :  entity.PolicyPeriod[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'argRange' attribute on Link (id=PolicyGroup_EffectiveDate) at PolicyDetailAcceleratedMenuActions.pcf: line 17, column 40
    function verifyValueRangeIsAllowedType_6 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyPeriod>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'argRange' attribute on Link (id=PolicyGroup_EffectiveDate) at PolicyDetailAcceleratedMenuActions.pcf: line 17, column 40
    function verifyValueRangeIsAllowedType_6 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'argRange' attribute on Link (id=PolicyGroup_EffectiveDate) at PolicyDetailAcceleratedMenuActions.pcf: line 17, column 40
    function verifyValueRange_7 () : void {
      var __valueRangeArg = gw.api.util.ArrayUtil.reverse(period.Policy.OrderedPolicyPeriods)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_6(__valueRangeArg)
    }
    
    property get ArgValue () : entity.PolicyPeriod {
      return getArgValue(1) as entity.PolicyPeriod
    }
    
    property set ArgValue ($arg :  entity.PolicyPeriod) {
      setArgValue(1, $arg)
    }
    
    
  }
  
  
}