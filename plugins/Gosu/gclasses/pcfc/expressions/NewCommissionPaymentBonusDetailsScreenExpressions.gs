package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentBonusDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewCommissionPaymentBonusDetailsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentBonusDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewCommissionPaymentBonusDetailsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentBonusDetailsScreen.pcf: line 17, column 72
    function def_onEnter_0 (def :  pcf.NewCommissionPaymentBonusDV) : void {
      def.onEnter(bonusPayment, producer, true)
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentBonusDetailsScreen.pcf: line 19, column 61
    function def_onEnter_2 (def :  pcf.NewCommissionPaymentTimeDV) : void {
      def.onEnter(bonusPayment, true)
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentBonusDetailsScreen.pcf: line 17, column 72
    function def_refreshVariables_1 (def :  pcf.NewCommissionPaymentBonusDV) : void {
      def.refreshVariables(bonusPayment, producer, true)
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentBonusDetailsScreen.pcf: line 19, column 61
    function def_refreshVariables_3 (def :  pcf.NewCommissionPaymentTimeDV) : void {
      def.refreshVariables(bonusPayment, true)
    }
    
    property get bonusPayment () : BonusCmsnPayment {
      return getRequireValue("bonusPayment", 0) as BonusCmsnPayment
    }
    
    property set bonusPayment ($arg :  BonusCmsnPayment) {
      setRequireValue("bonusPayment", 0, $arg)
    }
    
    property get producer () : Producer {
      return getRequireValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setRequireValue("producer", 0, $arg)
    }
    
    
  }
  
  
}