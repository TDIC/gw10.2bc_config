package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/ProducersWithExceptionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducersWithExceptionsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/ProducersWithExceptionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ProducersWithExceptionsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=ProducerWithMismatchAgency_Cell) at ProducersWithExceptionsLV.pcf: line 36, column 40
    function action_11 () : void {
      AgencyBillExceptions.go(producerWithExceptions.Producer)
    }
    
    // 'action' attribute on TextCell (id=ProducerWithMismatchAgency_Cell) at ProducersWithExceptionsLV.pcf: line 36, column 40
    function action_dest_12 () : pcf.api.Destination {
      return pcf.AgencyBillExceptions.createDestination(producerWithExceptions.Producer)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=ProducerWithMismatchAmount_Cell) at ProducersWithExceptionsLV.pcf: line 59, column 73
    function currency_27 () : typekey.Currency {
      return producerWithExceptions.Producer.Currency
    }
    
    // 'value' attribute on TextCell (id=ProducerWithMismatchOldestException_Cell) at ProducersWithExceptionsLV.pcf: line 46, column 42
    function valueRoot_20 () : java.lang.Object {
      return producerWithExceptions.OldestMismatchExceptionDate
    }
    
    // 'value' attribute on TextCell (id=ProducerWithMismatchNewestException_Cell) at ProducersWithExceptionsLV.pcf: line 52, column 42
    function valueRoot_23 () : java.lang.Object {
      return producerWithExceptions.NewestMismatchExceptionDate
    }
    
    // 'value' attribute on DateCell (id=ProducerWithMismatchDate_Cell) at ProducersWithExceptionsLV.pcf: line 29, column 71
    function valueRoot_9 () : java.lang.Object {
      return producerWithExceptions
    }
    
    // 'value' attribute on TextCell (id=ProducerWithMismatchAgency_Cell) at ProducersWithExceptionsLV.pcf: line 36, column 40
    function value_13 () : entity.Producer {
      return producerWithExceptions.Producer
    }
    
    // 'value' attribute on TextCell (id=ProducerWithMismatchNumberOfExceptions_Cell) at ProducersWithExceptionsLV.pcf: line 41, column 42
    function value_16 () : java.lang.Integer {
      return producerWithExceptions.NumberOfExceptions
    }
    
    // 'value' attribute on TextCell (id=ProducerWithMismatchOldestException_Cell) at ProducersWithExceptionsLV.pcf: line 46, column 42
    function value_19 () : java.lang.Integer {
      return producerWithExceptions.OldestMismatchExceptionDate.DaysSince
    }
    
    // 'value' attribute on TextCell (id=ProducerWithMismatchNewestException_Cell) at ProducersWithExceptionsLV.pcf: line 52, column 42
    function value_22 () : java.lang.Integer {
      return producerWithExceptions.NewestMismatchExceptionDate.DaysSince
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProducerWithMismatchAmount_Cell) at ProducersWithExceptionsLV.pcf: line 59, column 73
    function value_25 () : gw.pl.currency.MonetaryAmount {
      return producerWithExceptions.NetAmountOfMismatchExceptions
    }
    
    // 'value' attribute on DateCell (id=ProducerWithMismatchDate_Cell) at ProducersWithExceptionsLV.pcf: line 29, column 71
    function value_8 () : java.util.Date {
      return producerWithExceptions.OldestMismatchExceptionDate
    }
    
    property get producerWithExceptions () : gw.api.web.invoice.ProducerWithExceptionsView {
      return getIteratedValue(1) as gw.api.web.invoice.ProducerWithExceptionsView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/ProducersWithExceptionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducersWithExceptionsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=ProducerWithMismatchDate_Cell) at ProducersWithExceptionsLV.pcf: line 29, column 71
    function sortValue_0 (producerWithExceptions :  gw.api.web.invoice.ProducerWithExceptionsView) : java.lang.Object {
      return producerWithExceptions.OldestMismatchExceptionDate
    }
    
    // 'value' attribute on TextCell (id=ProducerWithMismatchAgency_Cell) at ProducersWithExceptionsLV.pcf: line 36, column 40
    function sortValue_1 (producerWithExceptions :  gw.api.web.invoice.ProducerWithExceptionsView) : java.lang.Object {
      return producerWithExceptions.Producer
    }
    
    // 'value' attribute on TextCell (id=ProducerWithMismatchNumberOfExceptions_Cell) at ProducersWithExceptionsLV.pcf: line 41, column 42
    function sortValue_2 (producerWithExceptions :  gw.api.web.invoice.ProducerWithExceptionsView) : java.lang.Object {
      return producerWithExceptions.NumberOfExceptions
    }
    
    // 'value' attribute on TextCell (id=ProducerWithMismatchOldestException_Cell) at ProducersWithExceptionsLV.pcf: line 46, column 42
    function sortValue_3 (producerWithExceptions :  gw.api.web.invoice.ProducerWithExceptionsView) : java.lang.Object {
      return producerWithExceptions.OldestMismatchExceptionDate.DaysSince
    }
    
    // 'value' attribute on TextCell (id=ProducerWithMismatchNewestException_Cell) at ProducersWithExceptionsLV.pcf: line 52, column 42
    function sortValue_4 (producerWithExceptions :  gw.api.web.invoice.ProducerWithExceptionsView) : java.lang.Object {
      return producerWithExceptions.NewestMismatchExceptionDate.DaysSince
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ProducerWithMismatchAmount_Cell) at ProducersWithExceptionsLV.pcf: line 59, column 73
    function sortValue_5 (producerWithExceptions :  gw.api.web.invoice.ProducerWithExceptionsView) : java.lang.Object {
      return producerWithExceptions.NetAmountOfMismatchExceptions
    }
    
    // '$$sumValue' attribute on RowIterator at ProducersWithExceptionsLV.pcf: line 59, column 73
    function sumValueRoot_7 (producerWithExceptions :  gw.api.web.invoice.ProducerWithExceptionsView) : java.lang.Object {
      return producerWithExceptions
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducersWithExceptionsLV.pcf: line 59, column 73
    function sumValue_6 (producerWithExceptions :  gw.api.web.invoice.ProducerWithExceptionsView) : java.lang.Object {
      return producerWithExceptions.NetAmountOfMismatchExceptions
    }
    
    // 'value' attribute on RowIterator at ProducersWithExceptionsLV.pcf: line 22, column 67
    function value_29 () : gw.api.web.invoice.ProducerWithExceptionsView[] {
      return producers
    }
    
    property get producers () : gw.api.web.invoice.ProducerWithExceptionsView[] {
      return getRequireValue("producers", 0) as gw.api.web.invoice.ProducerWithExceptionsView[]
    }
    
    property set producers ($arg :  gw.api.web.invoice.ProducerWithExceptionsView[]) {
      setRequireValue("producers", 0, $arg)
    }
    
    
  }
  
  
}