package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewTransactionAccountPoliciesScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewTransactionAccountPoliciesScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewTransactionAccountPoliciesScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends NewTransactionAccountPoliciesScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=Select) at NewTransactionAccountPoliciesScreen.pcf: line 50, column 44
    function action_8 () : void {
      helper.TAccountOwner = policyPeriod
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at NewTransactionAccountPoliciesScreen.pcf: line 57, column 51
    function valueRoot_10 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on DateCell (id=PolicyPerEffDate_Cell) at NewTransactionAccountPoliciesScreen.pcf: line 61, column 56
    function value_12 () : java.util.Date {
      return policyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=PolicyPerExpirDate_Cell) at NewTransactionAccountPoliciesScreen.pcf: line 65, column 58
    function value_15 () : java.util.Date {
      return policyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at NewTransactionAccountPoliciesScreen.pcf: line 57, column 51
    function value_9 () : java.lang.String {
      return policyPeriod.DisplayName
    }
    
    property get policyPeriod () : entity.PolicyPeriod {
      return getIteratedValue(1) as entity.PolicyPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/NewTransactionAccountPoliciesScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewTransactionAccountPoliciesScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=AccountTAccountOwner) at NewTransactionAccountPoliciesScreen.pcf: line 18, column 110
    function action_0 () : void {
      helper.TAccountOwner = helper.ChargeInitializer.ChargeAccount
    }
    
    // 'sortBy' attribute on TextCell (id=Number_Cell) at NewTransactionAccountPoliciesScreen.pcf: line 57, column 51
    function sortValue_4 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PolicyNumber
    }
    
    // 'sortBy' attribute on TextCell (id=Number_Cell) at NewTransactionAccountPoliciesScreen.pcf: line 57, column 51
    function sortValue_5 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return  policyPeriod.TermNumber
    }
    
    // 'value' attribute on DateCell (id=PolicyPerEffDate_Cell) at NewTransactionAccountPoliciesScreen.pcf: line 61, column 56
    function sortValue_6 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=PolicyPerExpirDate_Cell) at NewTransactionAccountPoliciesScreen.pcf: line 65, column 58
    function sortValue_7 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on RowIterator at NewTransactionAccountPoliciesScreen.pcf: line 41, column 47
    function value_18 () : entity.PolicyPeriod[] {
      return helper.ChargeInitializer.ChargeAccount.OpenPolicyPeriods
    }
    
    // 'value' attribute on TextInput (id=NewChargeTarget_Input) at NewTransactionAccountPoliciesScreen.pcf: line 27, column 44
    function value_2 () : java.lang.String {
      return helper.getDisplayName()
    }
    
    // 'visible' attribute on Toolbar at NewTransactionAccountPoliciesScreen.pcf: line 13, column 58
    function visible_1 () : java.lang.Boolean {
      return helper.TAccountOwner typeis PolicyPeriod
    }
    
    property get helper () : gw.accounting.NewGeneralSingleChargeHelper {
      return getRequireValue("helper", 0) as gw.accounting.NewGeneralSingleChargeHelper
    }
    
    property set helper ($arg :  gw.accounting.NewGeneralSingleChargeHelper) {
      setRequireValue("helper", 0, $arg)
    }
    
    
  }
  
  
}