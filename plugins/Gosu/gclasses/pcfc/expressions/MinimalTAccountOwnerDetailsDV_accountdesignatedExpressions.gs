package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/MinimalTAccountOwnerDetailsDV.accountdesignated.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class MinimalTAccountOwnerDetailsDV_accountdesignatedExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/MinimalTAccountOwnerDetailsDV.accountdesignated.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class MinimalTAccountOwnerDetailsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextInput (id=Account_Input) at MinimalTAccountOwnerDetailsDV.accountdesignated.pcf: line 26, column 38
    function action_1 () : void {
      pcf.AccountSummaryPopup.push(unapplied.Account)
    }
    
    // 'action' attribute on TextInput (id=Account_Input) at MinimalTAccountOwnerDetailsDV.accountdesignated.pcf: line 26, column 38
    function action_dest_2 () : pcf.api.Destination {
      return pcf.AccountSummaryPopup.createDestination(unapplied.Account)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at MinimalTAccountOwnerDetailsDV.accountdesignated.pcf: line 40, column 38
    function currency_15 () : typekey.Currency {
      return unapplied.Currency
    }
    
    // 'value' attribute on TextInput (id=Account_Input) at MinimalTAccountOwnerDetailsDV.accountdesignated.pcf: line 26, column 38
    function valueRoot_4 () : java.lang.Object {
      return unapplied
    }
    
    // 'value' attribute on TextInput (id=Insured_Input) at MinimalTAccountOwnerDetailsDV.accountdesignated.pcf: line 31, column 38
    function valueRoot_9 () : java.lang.Object {
      return unapplied.Account
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at MinimalTAccountOwnerDetailsDV.accountdesignated.pcf: line 40, column 38
    function value_13 () : gw.pl.currency.MonetaryAmount {
      return unapplied.Balance
    }
    
    // 'value' attribute on TextInput (id=Account_Input) at MinimalTAccountOwnerDetailsDV.accountdesignated.pcf: line 26, column 38
    function value_3 () : entity.Account {
      return unapplied.Account
    }
    
    // 'value' attribute on TextInput (id=Insured_Input) at MinimalTAccountOwnerDetailsDV.accountdesignated.pcf: line 31, column 38
    function value_8 () : java.lang.String {
      return unapplied.Account.AccountNameLocalized
    }
    
    // 'visible' attribute on TextInput (id=Account_Input) at MinimalTAccountOwnerDetailsDV.accountdesignated.pcf: line 26, column 38
    function visible_0 () : java.lang.Boolean {
      return unapplied != null
    }
    
    property get tAccountOwner () : TAccountOwner {
      return getRequireValue("tAccountOwner", 0) as TAccountOwner
    }
    
    property set tAccountOwner ($arg :  TAccountOwner) {
      setRequireValue("tAccountOwner", 0, $arg)
    }
    
    property get unapplied () : UnappliedFund {
      return getRequireValue("unapplied", 0) as UnappliedFund
    }
    
    property set unapplied ($arg :  UnappliedFund) {
      setRequireValue("unapplied", 0, $arg)
    }
    
    
  }
  
  
}