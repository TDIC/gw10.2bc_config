package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanThresholdHandlingInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyPlanThresholdHandlingInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanThresholdHandlingInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyPlanThresholdHandlingInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyInput (id=GracePeriodDayUnit_Input) at DelinquencyPlanThresholdHandlingInputSet.default.pcf: line 26, column 36
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      delinquencyPlan.GracePeriodDayUnit = (__VALUE_TO_SET as typekey.DayUnit)
    }
    
    // 'value' attribute on TextInput (id=GracePeriodDays_Input) at DelinquencyPlanThresholdHandlingInputSet.default.pcf: line 20, column 38
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      delinquencyPlan.GracePeriodDays = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'editable' attribute on TextInput (id=GracePeriodDays_Input) at DelinquencyPlanThresholdHandlingInputSet.default.pcf: line 20, column 38
    function editable_0 () : java.lang.Boolean {
      return planNotInUse
    }
    
    // 'validationExpression' attribute on TextInput (id=GracePeriodDays_Input) at DelinquencyPlanThresholdHandlingInputSet.default.pcf: line 20, column 38
    function validationExpression_1 () : java.lang.Object {
      return gw.api.web.delinquency.DelinquencyPlanUtil.validateGracePeriod( delinquencyPlan )
    }
    
    // 'value' attribute on TextInput (id=GracePeriodDays_Input) at DelinquencyPlanThresholdHandlingInputSet.default.pcf: line 20, column 38
    function valueRoot_4 () : java.lang.Object {
      return delinquencyPlan
    }
    
    // 'value' attribute on TextInput (id=GracePeriodDays_Input) at DelinquencyPlanThresholdHandlingInputSet.default.pcf: line 20, column 38
    function value_2 () : java.lang.Integer {
      return delinquencyPlan.GracePeriodDays
    }
    
    // 'value' attribute on TypeKeyInput (id=GracePeriodDayUnit_Input) at DelinquencyPlanThresholdHandlingInputSet.default.pcf: line 26, column 36
    function value_9 () : typekey.DayUnit {
      return delinquencyPlan.GracePeriodDayUnit
    }
    
    property get delinquencyPlan () : DelinquencyPlan {
      return getRequireValue("delinquencyPlan", 0) as DelinquencyPlan
    }
    
    property set delinquencyPlan ($arg :  DelinquencyPlan) {
      setRequireValue("delinquencyPlan", 0, $arg)
    }
    
    property get planNotInUse () : boolean {
      return getRequireValue("planNotInUse", 0) as java.lang.Boolean
    }
    
    property set planNotInUse ($arg :  boolean) {
      setRequireValue("planNotInUse", 0, $arg)
    }
    
    
  }
  
  
}