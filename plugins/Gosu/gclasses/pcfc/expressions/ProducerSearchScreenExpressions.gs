package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ProducerSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/search/ProducerSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get isClearBundle () : boolean {
      return getRequireValue("isClearBundle", 0) as java.lang.Boolean
    }
    
    property set isClearBundle ($arg :  boolean) {
      setRequireValue("isClearBundle", 0, $arg)
    }
    
    property get showHyperlinks () : Boolean {
      return getRequireValue("showHyperlinks", 0) as Boolean
    }
    
    property set showHyperlinks ($arg :  Boolean) {
      setRequireValue("showHyperlinks", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/ProducerSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends ProducerSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at ProducerSearchScreen.pcf: line 25, column 48
    function def_onEnter_0 (def :  pcf.ProducerSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at ProducerSearchScreen.pcf: line 29, column 96
    function def_onEnter_2 (def :  pcf.ProducerSearchResultsLV) : void {
      def.onEnter(producerSearchViews, null, false, showHyperlinks, false)
    }
    
    // 'def' attribute on PanelRef at ProducerSearchScreen.pcf: line 25, column 48
    function def_refreshVariables_1 (def :  pcf.ProducerSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at ProducerSearchScreen.pcf: line 29, column 96
    function def_refreshVariables_3 (def :  pcf.ProducerSearchResultsLV) : void {
      def.refreshVariables(producerSearchViews, null, false, showHyperlinks, false)
    }
    
    // 'maxSearchResults' attribute on SearchPanel at ProducerSearchScreen.pcf: line 23, column 86
    function maxSearchResults_4 () : java.lang.Object {
      return gw.api.system.BCConfigParameters.MaxSearchResults.Value
    }
    
    // 'searchCriteria' attribute on SearchPanel at ProducerSearchScreen.pcf: line 23, column 86
    function searchCriteria_6 () : gw.search.ProducerSearchCriteria {
      return new gw.search.ProducerSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at ProducerSearchScreen.pcf: line 23, column 86
    function search_5 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria, StringCriterionMode.TC_STARTSWITH)
    }
    
    property get producerSearchViews () : gw.api.database.IQueryBeanResult<ProducerSearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<ProducerSearchView>
    }
    
    property get searchCriteria () : gw.search.ProducerSearchCriteria {
      return getCriteriaValue(1) as gw.search.ProducerSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.ProducerSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}