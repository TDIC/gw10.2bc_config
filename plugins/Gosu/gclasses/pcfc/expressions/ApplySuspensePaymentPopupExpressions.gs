package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException
uses tdic.bc.common.batch.banklockbox.helper.TDIC_BankLockboxHelper
@javax.annotation.Generated("config/web/pcf/suspensepayment/ApplySuspensePaymentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ApplySuspensePaymentPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/suspensepayment/ApplySuspensePaymentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ApplySuspensePaymentPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (suspensePayment :  SuspensePayment) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=ProducerUpdate) at ApplySuspensePaymentPopup.pcf: line 69, column 68
    function action_10 () : void {
      CurrentLocation.commit();AgencyBillExecutedPayments.push(targetProducer);
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at ApplySuspensePaymentPopup.pcf: line 89, column 37
    function action_13 () : void {
      AccountSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=PolicyPicker) at ApplySuspensePaymentPopup.pcf: line 108, column 36
    function action_25 () : void {
      PolicySearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at ApplySuspensePaymentPopup.pcf: line 89, column 37
    function action_dest_14 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=PolicyPicker) at ApplySuspensePaymentPopup.pcf: line 108, column 36
    function action_dest_26 () : pcf.api.Destination {
      return pcf.PolicySearchPopup.createDestination()
    }
    
    // 'beforeCommit' attribute on Popup (id=ApplySuspensePaymentPopup) at ApplySuspensePaymentPopup.pcf: line 10, column 77
    function beforeCommit_43 (pickedValue :  java.lang.Object) : void {
      applySuspensePaymentToTarget()
    }
    
    // 'conversionExpression' attribute on TextInput (id=AccountNumber_Input) at ApplySuspensePaymentPopup.pcf: line 89, column 37
    function conversionExpression_16 (PickedValue :  Account) : java.lang.String {
      return (PickedValue as Account).AccountNumber
    }
    
    // 'conversionExpression' attribute on TextInput (id=PolicyNumber_Input) at ApplySuspensePaymentPopup.pcf: line 108, column 36
    function conversionExpression_28 (PickedValue :  PolicyPeriod) : java.lang.String {
      return (PickedValue as PolicyPeriod).PolicyNumber
    }
    
    // 'def' attribute on PanelRef at ApplySuspensePaymentPopup.pcf: line 148, column 63
    function def_onEnter_41 (def :  pcf.SuspensePaymentTransactionsLV) : void {
      def.onEnter(suspensePayment)
    }
    
    // 'def' attribute on PanelRef at ApplySuspensePaymentPopup.pcf: line 148, column 63
    function def_refreshVariables_42 (def :  pcf.SuspensePaymentTransactionsLV) : void {
      def.refreshVariables(suspensePayment)
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at ApplySuspensePaymentPopup.pcf: line 89, column 37
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      accountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on Choice (id=AccountChoice) at ApplySuspensePaymentPopup.pcf: line 82, column 56
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      applyTo = (__VALUE_TO_SET as typekey.SuspensePaymentApplyTo)
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at ApplySuspensePaymentPopup.pcf: line 108, column 36
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at ApplySuspensePaymentPopup.pcf: line 19, column 54
    function initialValue_0 () : gw.api.web.payment.SuspensePaymentUtil {
      return new gw.api.web.payment.SuspensePaymentUtil()
    }
    
    // 'initialValue' attribute on Variable at ApplySuspensePaymentPopup.pcf: line 23, column 50
    function initialValue_1 () : gw.plugin.payment.ISuspensePayment {
      return gw.plugin.Plugins.get(gw.plugin.payment.ISuspensePayment)
    }
    
    // 'initialValue' attribute on Variable at ApplySuspensePaymentPopup.pcf: line 27, column 38
    function initialValue_2 () : SuspensePaymentApplyTo {
      return suspensePayment.CurrentApplyTo
    }
    
    // 'initialValue' attribute on Variable at ApplySuspensePaymentPopup.pcf: line 31, column 32
    function initialValue_3 () : java.lang.String {
      return suspensePayment.AccountNumber 
    }
    
    // 'initialValue' attribute on Variable at ApplySuspensePaymentPopup.pcf: line 35, column 32
    function initialValue_4 () : java.lang.String {
      return suspensePayment.PolicyNumber
    }
    
    // 'initialValue' attribute on Variable at ApplySuspensePaymentPopup.pcf: line 39, column 32
    function initialValue_5 () : java.lang.String {
      return suspensePayment.ProducerName
    }
    
    // 'initialValue' attribute on Variable at ApplySuspensePaymentPopup.pcf: line 52, column 32
    function initialValue_6 () : java.lang.String {
      return suspensePayment.InvoiceNumber
    }
    
    // 'initialValue' attribute on Variable at ApplySuspensePaymentPopup.pcf: line 56, column 32
    function initialValue_7 () : java.lang.String {
      return suspensePayment.RefNumber
    }
    
    // EditButtons at ApplySuspensePaymentPopup.pcf: line 71, column 74
    function label_12 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'option' attribute on Choice (id=AccountChoice) at ApplySuspensePaymentPopup.pcf: line 82, column 56
    function option_21 () : java.lang.Object {
      return SuspensePaymentApplyTo.TC_ACCOUNT.Code
    }
    
    // 'option' attribute on Choice (id=PolicyChoice) at ApplySuspensePaymentPopup.pcf: line 101, column 56
    function option_33 () : java.lang.Object {
      return SuspensePaymentApplyTo.TC_POLICY.Code
    }
    
    // 'validationExpression' attribute on TextInput (id=AccountNumber_Input) at ApplySuspensePaymentPopup.pcf: line 89, column 37
    function validationExpression_15 () : java.lang.Object {
      return updateAndValidateAccount()
    }
    
    // 'validationExpression' attribute on TextInput (id=PolicyNumber_Input) at ApplySuspensePaymentPopup.pcf: line 108, column 36
    function validationExpression_27 () : java.lang.Object {
      return updateAndValidatePolicy()
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at ApplySuspensePaymentPopup.pcf: line 89, column 37
    function value_17 () : java.lang.String {
      return accountNumber
    }
    
    // 'value' attribute on Choice (id=AccountChoice) at ApplySuspensePaymentPopup.pcf: line 82, column 56
    function value_22 () : typekey.SuspensePaymentApplyTo {
      return applyTo
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at ApplySuspensePaymentPopup.pcf: line 108, column 36
    function value_29 () : java.lang.String {
      return policyNumber
    }
    
    // 'value' attribute on TextInput (id=InvoiceNumber_Input) at ApplySuspensePaymentPopup.pcf: line 140, column 36
    function value_37 () : java.lang.String {
      return invoiceNumber
    }
    
    // 'value' attribute on TextInput (id=CheckNumber_Input) at ApplySuspensePaymentPopup.pcf: line 144, column 34
    function value_39 () : java.lang.String {
      return checkNumber
    }
    
    // 'updateVisible' attribute on EditButtons at ApplySuspensePaymentPopup.pcf: line 71, column 74
    function visible_11 () : java.lang.Boolean {
      return applyTo != SuspensePaymentApplyTo.TC_PRODUCER
    }
    
    // 'visible' attribute on ToolbarButton (id=ProducerUpdate) at ApplySuspensePaymentPopup.pcf: line 69, column 68
    function visible_9 () : java.lang.Boolean {
      return applyTo == SuspensePaymentApplyTo.TC_PRODUCER
    }
    
    override property get CurrentLocation () : pcf.ApplySuspensePaymentPopup {
      return super.CurrentLocation as pcf.ApplySuspensePaymentPopup
    }
    
    property get accountNumber () : java.lang.String {
      return getVariableValue("accountNumber", 0) as java.lang.String
    }
    
    property set accountNumber ($arg :  java.lang.String) {
      setVariableValue("accountNumber", 0, $arg)
    }
    
    property get applyTo () : SuspensePaymentApplyTo {
      return getVariableValue("applyTo", 0) as SuspensePaymentApplyTo
    }
    
    property set applyTo ($arg :  SuspensePaymentApplyTo) {
      setVariableValue("applyTo", 0, $arg)
    }
    
    property get checkNumber () : java.lang.String {
      return getVariableValue("checkNumber", 0) as java.lang.String
    }
    
    property set checkNumber ($arg :  java.lang.String) {
      setVariableValue("checkNumber", 0, $arg)
    }
    
    property get invoiceNumber () : java.lang.String {
      return getVariableValue("invoiceNumber", 0) as java.lang.String
    }
    
    property set invoiceNumber ($arg :  java.lang.String) {
      setVariableValue("invoiceNumber", 0, $arg)
    }
    
    property get policyNumber () : java.lang.String {
      return getVariableValue("policyNumber", 0) as java.lang.String
    }
    
    property set policyNumber ($arg :  java.lang.String) {
      setVariableValue("policyNumber", 0, $arg)
    }
    
    property get policyNumberLong () : java.lang.String {
      return getVariableValue("policyNumberLong", 0) as java.lang.String
    }
    
    property set policyNumberLong ($arg :  java.lang.String) {
      setVariableValue("policyNumberLong", 0, $arg)
    }
    
    property get producerName () : java.lang.String {
      return getVariableValue("producerName", 0) as java.lang.String
    }
    
    property set producerName ($arg :  java.lang.String) {
      setVariableValue("producerName", 0, $arg)
    }
    
    property get suspensePayment () : SuspensePayment {
      return getVariableValue("suspensePayment", 0) as SuspensePayment
    }
    
    property set suspensePayment ($arg :  SuspensePayment) {
      setVariableValue("suspensePayment", 0, $arg)
    }
    
    property get suspensePaymentPlugin () : gw.plugin.payment.ISuspensePayment {
      return getVariableValue("suspensePaymentPlugin", 0) as gw.plugin.payment.ISuspensePayment
    }
    
    property set suspensePaymentPlugin ($arg :  gw.plugin.payment.ISuspensePayment) {
      setVariableValue("suspensePaymentPlugin", 0, $arg)
    }
    
    property get suspensePaymentUtil () : gw.api.web.payment.SuspensePaymentUtil {
      return getVariableValue("suspensePaymentUtil", 0) as gw.api.web.payment.SuspensePaymentUtil
    }
    
    property set suspensePaymentUtil ($arg :  gw.api.web.payment.SuspensePaymentUtil) {
      setVariableValue("suspensePaymentUtil", 0, $arg)
    }
    
    property get targetAccount () : Account {
      return getVariableValue("targetAccount", 0) as Account
    }
    
    property set targetAccount ($arg :  Account) {
      setVariableValue("targetAccount", 0, $arg)
    }
    
    property get targetPolicyPeriod () : PolicyPeriod {
      return getVariableValue("targetPolicyPeriod", 0) as PolicyPeriod
    }
    
    property set targetPolicyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("targetPolicyPeriod", 0, $arg)
    }
    
    property get targetProducer () : Producer {
      return getVariableValue("targetProducer", 0) as Producer
    }
    
    property set targetProducer ($arg :  Producer) {
      setVariableValue("targetProducer", 0, $arg)
    }
    
    
    // validationExpression expects an error string or null for success
    function updateAndValidateAccount() : String {
      targetAccount = suspensePaymentPlugin.findAccountToApply(accountNumber)
      if (applyTo == SuspensePaymentApplyTo.TC_ACCOUNT and (targetAccount == null)) {
        return DisplayKey.get("Web.ApplySuspensePaymentPopup.Error.AccountNotFound")
      } else {
        return null
      }
    }
    
    function updateAndValidatePolicy() : String {
      targetPolicyPeriod = suspensePaymentPlugin.findPolicyPeriodToApply(policyNumber)
      if (applyTo == SuspensePaymentApplyTo.TC_POLICY and (targetPolicyPeriod == null)) {
        return DisplayKey.get("Web.ApplySuspensePaymentPopup.Error.PolicyNotFound")
      } else {
        return null
      }
    }
    
    function updateAndValidateProducer() : String {
      targetProducer = suspensePaymentPlugin.findProducerToApply(producerName)
      if (applyTo == SuspensePaymentApplyTo.TC_PRODUCER and (targetProducer == null)) {
        return DisplayKey.get("Web.ApplySuspensePaymentPopup.Error.ProducerNotFound")
      } else {
        return null
      }
    }
    
    function applySuspensePaymentToTarget() {
      switch(applyTo) {
        case SuspensePaymentApplyTo.TC_ACCOUNT:
            if (suspensePayment.Currency != targetAccount.Currency) {
              throw new DisplayableException(DisplayKey.get("Web.ApplySuspensePaymentPopup.Error.AccountCurrencyMismatch"))
            }
            suspensePayment.applyTo(targetAccount)
            break
        case SuspensePaymentApplyTo.TC_POLICY:
            if (suspensePayment.Currency != targetPolicyPeriod.Currency) {
              throw new DisplayableException(DisplayKey.get("Web.ApplySuspensePaymentPopup.Error.PolicyCurrencyMismatch"))
            }
    	try{
              TDIC_BankLockboxHelper.applySuspensePaymentToTargetPolicyPeriod(suspensePayment, targetPolicyPeriod, gw.transaction.Transaction.getCurrent())
            }catch(e: Exception){
              throw new DisplayableException(DisplayKey.get("Web.ApplySuspensePaymentPopup.Error.PaymentTransferFailed", targetPolicyPeriod.PolicyNumberLong))  
            }
            //suspensePayment.applyTo(targetPolicyPeriod)
            break
        case SuspensePaymentApplyTo.TC_PRODUCER:
            if (suspensePayment.Currency != targetProducer.Currency) {
              throw new DisplayableException(DisplayKey.get("Web.ApplySuspensePaymentPopup.Error.ProducerCurrencyMismatch"))
            }
            suspensePayment.applyTo(targetProducer)
            break
        default:
            throw new DisplayableException(DisplayKey.get("Java.Validation.MissingRequired", DisplayKey.get("Web.ApplySuspensePaymentPopup.AccountNumber")
                + ", " + DisplayKey.get("Web.ApplySuspensePaymentPopup.PolicyNumber")
                + ", " + DisplayKey.get("Web.ApplySuspensePaymentPopup.ProducerName")))
      }
    }
    
    
  }
  
  
}