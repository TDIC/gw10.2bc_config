package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/invoice/ReturnPremiumPlanDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ReturnPremiumPlanDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/invoice/ReturnPremiumPlanDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ReturnPremiumPlanDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (returnPremiumPlan :  ReturnPremiumPlan) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=ReturnPremiumPlanDetail) at ReturnPremiumPlanDetail.pcf: line 10, column 99
    function canEdit_2 () : java.lang.Boolean {
      return perm.System.retpremplanedit
    }
    
    // 'def' attribute on ScreenRef at ReturnPremiumPlanDetail.pcf: line 17, column 63
    function def_onEnter_0 (def :  pcf.ReturnPremiumPlanDetailScreen) : void {
      def.onEnter(returnPremiumPlan)
    }
    
    // 'def' attribute on ScreenRef at ReturnPremiumPlanDetail.pcf: line 17, column 63
    function def_refreshVariables_1 (def :  pcf.ReturnPremiumPlanDetailScreen) : void {
      def.refreshVariables(returnPremiumPlan)
    }
    
    // 'parent' attribute on Page (id=ReturnPremiumPlanDetail) at ReturnPremiumPlanDetail.pcf: line 10, column 99
    static function parent_3 (returnPremiumPlan :  ReturnPremiumPlan) : pcf.api.Destination {
      return pcf.ReturnPremiumPlans.createDestination()
    }
    
    // 'title' attribute on Page (id=ReturnPremiumPlanDetail) at ReturnPremiumPlanDetail.pcf: line 10, column 99
    static function title_4 (returnPremiumPlan :  ReturnPremiumPlan) : java.lang.Object {
      return DisplayKey.get("Web.ReturnPremiumPlanDetail.Title", returnPremiumPlan.Name)
    }
    
    override property get CurrentLocation () : pcf.ReturnPremiumPlanDetail {
      return super.CurrentLocation as pcf.ReturnPremiumPlanDetail
    }
    
    property get returnPremiumPlan () : ReturnPremiumPlan {
      return getVariableValue("returnPremiumPlan", 0) as ReturnPremiumPlan
    }
    
    property set returnPremiumPlan ($arg :  ReturnPremiumPlan) {
      setVariableValue("returnPremiumPlan", 0, $arg)
    }
    
    
  }
  
  
}