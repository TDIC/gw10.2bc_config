package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketRelatedEntitiesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TroubleTicketRelatedEntitiesDVExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketRelatedEntitiesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TroubleTicketRelatedEntitiesDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=EntityDisplayName_Cell) at TroubleTicketRelatedEntitiesDV.pcf: line 78, column 118
    function actionAvailable_25 () : java.lang.Boolean {
      return typeof ticketRelatedEntity != Policy || not (ticketRelatedEntity as Policy).PolicyPeriods.IsEmpty
    }
    
    // 'action' attribute on TextCell (id=EntityDisplayName_Cell) at TroubleTicketRelatedEntitiesDV.pcf: line 78, column 118
    function action_24 () : void {
      goToDetailPageForRelatedEntity(ticketRelatedEntity)
    }
    
    // 'value' attribute on TextCell (id=EntityTypeDisplayName_Cell) at TroubleTicketRelatedEntitiesDV.pcf: line 71, column 122
    function value_22 () : java.lang.String {
      return gw.api.web.troubleticket.TroubleTicketUtil.getRelatedEntityTypeDisplayName(ticketRelatedEntity)
    }
    
    // 'value' attribute on TextCell (id=EntityDisplayName_Cell) at TroubleTicketRelatedEntitiesDV.pcf: line 78, column 118
    function value_26 () : java.lang.String {
      return gw.api.web.troubleticket.TroubleTicketUtil.getRelatedEntityDisplayName(ticketRelatedEntity)
    }
    
    property get ticketRelatedEntity () : gw.pl.persistence.core.Bean {
      return getIteratedValue(1) as gw.pl.persistence.core.Bean
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketRelatedEntitiesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TroubleTicketRelatedEntitiesDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=relatedEntities) at TroubleTicketRelatedEntitiesDV.pcf: line 24, column 96
    function action_1 () : void {
      TroubleTicketRelatedEntitiesPopup.push(troubleTicket, createTroubleTicketHelper)
    }
    
    // 'action' attribute on PickerToolbarButton (id=TroubleTicketRelatedEntitiesDV_AddPolicyPeriodsButton) at TroubleTicketRelatedEntitiesDV.pcf: line 45, column 112
    function action_12 () : void {
      SelectMultiplePolicyPeriodsPopup.push(new gw.search.PolicySearchCriteria())
    }
    
    // 'action' attribute on PickerToolbarButton (id=TroubleTicketRelatedEntitiesDV_AddProducersButton) at TroubleTicketRelatedEntitiesDV.pcf: line 52, column 108
    function action_16 () : void {
      SelectMultipleProducersPopup.push()
    }
    
    // 'action' attribute on PickerToolbarButton (id=TroubleTicketRelatedEntitiesDV_AddAccountsButton) at TroubleTicketRelatedEntitiesDV.pcf: line 31, column 107
    function action_4 () : void {
      SelectMultipleAccountsPopup.push()
    }
    
    // 'action' attribute on PickerToolbarButton (id=TroubleTicketRelatedEntitiesDV_AddPoliciesButton) at TroubleTicketRelatedEntitiesDV.pcf: line 38, column 107
    function action_8 () : void {
      SelectMultiplePoliciesPopup.push()
    }
    
    // 'action' attribute on PickerToolbarButton (id=TroubleTicketRelatedEntitiesDV_AddPolicyPeriodsButton) at TroubleTicketRelatedEntitiesDV.pcf: line 45, column 112
    function action_dest_13 () : pcf.api.Destination {
      return pcf.SelectMultiplePolicyPeriodsPopup.createDestination(new gw.search.PolicySearchCriteria())
    }
    
    // 'action' attribute on PickerToolbarButton (id=TroubleTicketRelatedEntitiesDV_AddProducersButton) at TroubleTicketRelatedEntitiesDV.pcf: line 52, column 108
    function action_dest_17 () : pcf.api.Destination {
      return pcf.SelectMultipleProducersPopup.createDestination()
    }
    
    // 'action' attribute on ToolbarButton (id=relatedEntities) at TroubleTicketRelatedEntitiesDV.pcf: line 24, column 96
    function action_dest_2 () : pcf.api.Destination {
      return pcf.TroubleTicketRelatedEntitiesPopup.createDestination(troubleTicket, createTroubleTicketHelper)
    }
    
    // 'action' attribute on PickerToolbarButton (id=TroubleTicketRelatedEntitiesDV_AddAccountsButton) at TroubleTicketRelatedEntitiesDV.pcf: line 31, column 107
    function action_dest_5 () : pcf.api.Destination {
      return pcf.SelectMultipleAccountsPopup.createDestination()
    }
    
    // 'action' attribute on PickerToolbarButton (id=TroubleTicketRelatedEntitiesDV_AddPoliciesButton) at TroubleTicketRelatedEntitiesDV.pcf: line 38, column 107
    function action_dest_9 () : pcf.api.Destination {
      return pcf.SelectMultiplePoliciesPopup.createDestination()
    }
    
    // 'available' attribute on ToolbarButton (id=relatedEntities) at TroubleTicketRelatedEntitiesDV.pcf: line 24, column 96
    function available_0 () : java.lang.Boolean {
      return !troubleTicket.IsClosed
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=TroubleTicketRelatedEntitiesDV_AddPoliciesButton) at TroubleTicketRelatedEntitiesDV.pcf: line 38, column 107
    function onPick_10 (PickedValue :  Policy[]) : void {
      createTroubleTicketHelper.linkTroubleTicketWithPolicies(troubleTicket, PickedValue)
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=TroubleTicketRelatedEntitiesDV_AddPolicyPeriodsButton) at TroubleTicketRelatedEntitiesDV.pcf: line 45, column 112
    function onPick_14 (PickedValue :  PolicyPeriod[]) : void {
      createTroubleTicketHelper.linkTroubleTicketWithPolicyPeriods(troubleTicket, PickedValue)
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=TroubleTicketRelatedEntitiesDV_AddProducersButton) at TroubleTicketRelatedEntitiesDV.pcf: line 52, column 108
    function onPick_18 (PickedValue :  Producer[]) : void {
      createTroubleTicketHelper.linkTroubleTicketWithProducers(troubleTicket, PickedValue)
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=TroubleTicketRelatedEntitiesDV_AddAccountsButton) at TroubleTicketRelatedEntitiesDV.pcf: line 31, column 107
    function onPick_6 (PickedValue :  Account[]) : void {
      createTroubleTicketHelper.linkTroubleTicketWithAccounts(troubleTicket, PickedValue)
    }
    
    // 'value' attribute on TextCell (id=EntityTypeDisplayName_Cell) at TroubleTicketRelatedEntitiesDV.pcf: line 71, column 122
    function sortValue_20 (ticketRelatedEntity :  gw.pl.persistence.core.Bean) : java.lang.Object {
      return gw.api.web.troubleticket.TroubleTicketUtil.getRelatedEntityTypeDisplayName(ticketRelatedEntity)
    }
    
    // 'value' attribute on TextCell (id=EntityDisplayName_Cell) at TroubleTicketRelatedEntitiesDV.pcf: line 78, column 118
    function sortValue_21 (ticketRelatedEntity :  gw.pl.persistence.core.Bean) : java.lang.Object {
      return gw.api.web.troubleticket.TroubleTicketUtil.getRelatedEntityDisplayName(ticketRelatedEntity)
    }
    
    // 'toRemove' attribute on RowIterator at TroubleTicketRelatedEntitiesDV.pcf: line 65, column 55
    function toRemove_28 (ticketRelatedEntity :  gw.pl.persistence.core.Bean) : void {
      troubleTicket.removeFromTicketRelatedEntities(ticketRelatedEntity)
    }
    
    // 'value' attribute on RowIterator at TroubleTicketRelatedEntitiesDV.pcf: line 65, column 55
    function value_29 () : gw.pl.persistence.core.Bean[] {
      return troubleTicket.TicketRelatedEntities
    }
    
    property get createTroubleTicketHelper () : CreateTroubleTicketHelper {
      return getRequireValue("createTroubleTicketHelper", 0) as CreateTroubleTicketHelper
    }
    
    property set createTroubleTicketHelper ($arg :  CreateTroubleTicketHelper) {
      setRequireValue("createTroubleTicketHelper", 0, $arg)
    }
    
    property get troubleTicket () : TroubleTicket {
      return getRequireValue("troubleTicket", 0) as TroubleTicket
    }
    
    property set troubleTicket ($arg :  TroubleTicket) {
      setRequireValue("troubleTicket", 0, $arg)
    }
    
    function goToDetailPageForRelatedEntity(bean : Object) {
      if (bean typeis Account) {
        AccountSummaryPopup.push(bean)
      } else if (bean typeis PolicyPeriod) {
        PolicySummary.push(bean)
      }else if (bean typeis Policy) {
        PolicySummary.push(bean.LatestPolicyPeriod)
      } else if (bean typeis Producer) {
        ProducerDetailPopup.push(bean)
      } else {
        throw DisplayKey.get("Web.TroubleTicketRelatedEntitiesDV.ErrorUnexpectedEntityType")
      }
    }
    
    
  }
  
  
}