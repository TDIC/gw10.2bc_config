package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillSuspenseItems.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillSuspenseItemsExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillSuspenseItems.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillSuspenseItemsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=AgencyBillSuspenseItems) at AgencyBillSuspenseItems.pcf: line 8, column 75
    static function canVisit_4 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodsuspitemsview
    }
    
    // 'def' attribute on PanelRef (id=PaymentItemsPanel) at AgencyBillSuspenseItems.pcf: line 22, column 37
    function def_onEnter_0 (def :  pcf.AgencyBillSuspenseItemPanelSet) : void {
      def.onEnter(producer.findAssociatedSuspensePaymentItems()?.toTypedArray(), Payment)
    }
    
    // 'def' attribute on PanelRef (id=PromiseItemsPanel) at AgencyBillSuspenseItems.pcf: line 29, column 37
    function def_onEnter_2 (def :  pcf.AgencyBillSuspenseItemPanelSet) : void {
      def.onEnter(producer.findAssociatedSuspensePromiseItems()?.toTypedArray(), Promise)
    }
    
    // 'def' attribute on PanelRef (id=PaymentItemsPanel) at AgencyBillSuspenseItems.pcf: line 22, column 37
    function def_refreshVariables_1 (def :  pcf.AgencyBillSuspenseItemPanelSet) : void {
      def.refreshVariables(producer.findAssociatedSuspensePaymentItems()?.toTypedArray(), Payment)
    }
    
    // 'def' attribute on PanelRef (id=PromiseItemsPanel) at AgencyBillSuspenseItems.pcf: line 29, column 37
    function def_refreshVariables_3 (def :  pcf.AgencyBillSuspenseItemPanelSet) : void {
      def.refreshVariables(producer.findAssociatedSuspensePromiseItems()?.toTypedArray(), Promise)
    }
    
    // Page (id=AgencyBillSuspenseItems) at AgencyBillSuspenseItems.pcf: line 8, column 75
    static function parent_5 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    override property get CurrentLocation () : pcf.AgencyBillSuspenseItems {
      return super.CurrentLocation as pcf.AgencyBillSuspenseItems
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}