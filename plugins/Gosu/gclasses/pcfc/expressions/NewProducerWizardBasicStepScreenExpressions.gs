package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewProducerWizardBasicStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewProducerWizardBasicStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/NewProducerWizardBasicStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends NewProducerWizardBasicStepScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=EditLink) at NewProducerWizardBasicStepScreen.pcf: line 179, column 46
    function action_84 () : void {
      ProducerContactDetailPopup.push(producerContact)
    }
    
    // 'action' attribute on Link (id=EditLink) at NewProducerWizardBasicStepScreen.pcf: line 179, column 46
    function action_dest_85 () : pcf.api.Destination {
      return pcf.ProducerContactDetailPopup.createDestination(producerContact)
    }
    
    // 'value' attribute on TextCell (id=ContactName_Cell) at NewProducerWizardBasicStepScreen.pcf: line 184, column 56
    function valueRoot_87 () : java.lang.Object {
      return producerContact
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at NewProducerWizardBasicStepScreen.pcf: line 189, column 47
    function valueRoot_90 () : java.lang.Object {
      return producerContact.Contact
    }
    
    // 'value' attribute on TextCell (id=ContactName_Cell) at NewProducerWizardBasicStepScreen.pcf: line 184, column 56
    function value_86 () : java.lang.String {
      return producerContact.DisplayName
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at NewProducerWizardBasicStepScreen.pcf: line 189, column 47
    function value_89 () : entity.Address {
      return producerContact.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=ContactRoles_Cell) at NewProducerWizardBasicStepScreen.pcf: line 193, column 97
    function value_92 () : java.lang.String {
      return gw.api.web.producer.ProducerUtil.getRolesForDisplay(producerContact)
    }
    
    property get producerContact () : entity.ProducerContact {
      return getIteratedValue(1) as entity.ProducerContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/NewProducerWizardBasicStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewProducerWizardBasicStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at NewProducerWizardBasicStepScreen.pcf: line 80, column 48
    function action_35 () : void {
      NewPaymentInstrumentPopup.push(gw.payment.PaymentInstrumentFilters.producerDetailsPaymentInstrumentOptions,producer,false)
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountRep_Input) at NewProducerWizardBasicStepScreen.pcf: line 111, column 41
    function action_56 () : void {
      UserSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=addNewCompany) at NewProducerWizardBasicStepScreen.pcf: line 145, column 93
    function action_74 () : void {
      NewProducerContactPopup.push(producer, Company)
    }
    
    // 'action' attribute on MenuItem (id=addNewPerson) at NewProducerWizardBasicStepScreen.pcf: line 149, column 92
    function action_76 () : void {
      NewProducerContactPopup.push(producer, Person)
    }
    
    // 'action' attribute on PickerToolbarButton (id=addExistingContact) at NewProducerWizardBasicStepScreen.pcf: line 160, column 102
    function action_78 () : void {
      ContactSearchPopup.push(true)
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at NewProducerWizardBasicStepScreen.pcf: line 80, column 48
    function action_dest_36 () : pcf.api.Destination {
      return pcf.NewPaymentInstrumentPopup.createDestination(gw.payment.PaymentInstrumentFilters.producerDetailsPaymentInstrumentOptions,producer,false)
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountRep_Input) at NewProducerWizardBasicStepScreen.pcf: line 111, column 41
    function action_dest_57 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=addNewCompany) at NewProducerWizardBasicStepScreen.pcf: line 145, column 93
    function action_dest_75 () : pcf.api.Destination {
      return pcf.NewProducerContactPopup.createDestination(producer, Company)
    }
    
    // 'action' attribute on MenuItem (id=addNewPerson) at NewProducerWizardBasicStepScreen.pcf: line 149, column 92
    function action_dest_77 () : pcf.api.Destination {
      return pcf.NewProducerContactPopup.createDestination(producer, Person)
    }
    
    // 'action' attribute on PickerToolbarButton (id=addExistingContact) at NewProducerWizardBasicStepScreen.pcf: line 160, column 102
    function action_dest_79 () : pcf.api.Destination {
      return pcf.ContactSearchPopup.createDestination(true)
    }
    
    // 'conversionExpression' attribute on PickerInput (id=AccountRep_Input) at NewProducerWizardBasicStepScreen.pcf: line 111, column 41
    function conversionExpression_59 (PickedValue :  User) : java.lang.String {
      return PickedValue.DisplayName
    }
    
    // 'def' attribute on InputSetRef at NewProducerWizardBasicStepScreen.pcf: line 71, column 58
    function def_onEnter_33 (def :  pcf.ProducerHoldStatementInputSet) : void {
      def.onEnter(producer)
    }
    
    // 'def' attribute on InputSetRef (id=PrimaryContactPhone) at NewProducerWizardBasicStepScreen.pcf: line 125, column 37
    function def_onEnter_69 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.BusinessPhoneOwner(new gw.api.phone.ContactPhoneDelegate(producer.PrimaryContact.Contact, Contact#WorkPhone), DisplayKey.get("Web.NewProducerDV.PrimaryContact.Phone")))
    }
    
    // 'def' attribute on InputSetRef at NewProducerWizardBasicStepScreen.pcf: line 71, column 58
    function def_refreshVariables_34 (def :  pcf.ProducerHoldStatementInputSet) : void {
      def.refreshVariables(producer)
    }
    
    // 'def' attribute on InputSetRef (id=PrimaryContactPhone) at NewProducerWizardBasicStepScreen.pcf: line 125, column 37
    function def_refreshVariables_70 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.BusinessPhoneOwner(new gw.api.phone.ContactPhoneDelegate(producer.PrimaryContact.Contact, Contact#WorkPhone), DisplayKey.get("Web.NewProducerDV.PrimaryContact.Phone")))
    }
    
    // 'value' attribute on TextInput (id=NameKanji_Input) at NewProducerWizardBasicStepScreen.pcf: line 40, column 86
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      producer.NameKanji = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Tier_Input) at NewProducerWizardBasicStepScreen.pcf: line 46, column 44
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      producer.Tier = (__VALUE_TO_SET as typekey.ProducerTier)
    }
    
    // 'value' attribute on RangeInput (id=SecurityZone_Input) at NewProducerWizardBasicStepScreen.pcf: line 60, column 44
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      producer.SecurityZone = (__VALUE_TO_SET as entity.SecurityZone)
    }
    
    // 'value' attribute on TypeKeyInput (id=Periodicity_Input) at NewProducerWizardBasicStepScreen.pcf: line 68, column 44
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      producer.RecurPeriodicity = (__VALUE_TO_SET as typekey.Periodicity)
    }
    
    // 'value' attribute on RangeInput (id=PaymentInstrument_Input) at NewProducerWizardBasicStepScreen.pcf: line 80, column 48
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      producer.DefaultPaymentInstrument = (__VALUE_TO_SET as entity.PaymentInstrument)
    }
    
    // 'value' attribute on TextInput (id=CommissionDayOfMonth_Input) at NewProducerWizardBasicStepScreen.pcf: line 92, column 42
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      producer.RecurDayOfMonth = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at NewProducerWizardBasicStepScreen.pcf: line 34, column 34
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      producer.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=AgencyBillPlan_Input) at NewProducerWizardBasicStepScreen.pcf: line 102, column 46
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      producer.AgencyBillPlan = (__VALUE_TO_SET as entity.AgencyBillPlan)
    }
    
    // 'value' attribute on PickerInput (id=AccountRep_Input) at NewProducerWizardBasicStepScreen.pcf: line 111, column 41
    function defaultSetter_61 (__VALUE_TO_SET :  java.lang.Object) : void {
      accountRepDisplayName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at NewProducerWizardBasicStepScreen.pcf: line 13, column 22
    function initialValue_0 () : String {
      return producer.AccountRep.DisplayName
    }
    
    // 'initialValue' attribute on Variable at NewProducerWizardBasicStepScreen.pcf: line 17, column 68
    function initialValue_1 () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return gw.api.database.Query.make(SecurityZone).select()
    }
    
    // 'initialValue' attribute on Variable at NewProducerWizardBasicStepScreen.pcf: line 21, column 49
    function initialValue_2 () : gw.payment.PaymentInstrumentRange {
      return new gw.payment.PaymentInstrumentRange(producer.PaymentInstruments)
    }
    
    // 'label' attribute on TextInput (id=Name_Input) at NewProducerWizardBasicStepScreen.pcf: line 34, column 34
    function label_3 () : java.lang.Object {
      return gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP ? DisplayKey.get("Web.NewProducerDV.Basics.NamePhonetic") : DisplayKey.get("Web.NewProducerDV.Basics.Name")
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=addExistingContact) at NewProducerWizardBasicStepScreen.pcf: line 160, column 102
    function onPick_80 (PickedValue :  gw.plugin.contact.ContactResult) : void {
      gw.contact.ContactConnection.connectContactToProducer(PickedValue, producer)
    }
    
    // 'requestValidationExpression' attribute on TextInput (id=CommissionDayOfMonth_Input) at NewProducerWizardBasicStepScreen.pcf: line 92, column 42
    function requestValidationExpression_44 (VALUE :  java.lang.Integer) : java.lang.Object {
      return VALUE != null and VALUE > 0 and VALUE <= 31 ? null : DisplayKey.get("Java.Account.InvoiceDayOfMonth.ValidationError")
    }
    
    // 'value' attribute on TextCell (id=ContactName_Cell) at NewProducerWizardBasicStepScreen.pcf: line 184, column 56
    function sortValue_81 (producerContact :  entity.ProducerContact) : java.lang.Object {
      return producerContact.DisplayName
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at NewProducerWizardBasicStepScreen.pcf: line 189, column 47
    function sortValue_82 (producerContact :  entity.ProducerContact) : java.lang.Object {
      return producerContact.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=ContactRoles_Cell) at NewProducerWizardBasicStepScreen.pcf: line 193, column 97
    function sortValue_83 (producerContact :  entity.ProducerContact) : java.lang.Object {
      return gw.api.web.producer.ProducerUtil.getRolesForDisplay(producerContact)
    }
    
    // 'toRemove' attribute on RowIterator (id=producerContactIterator) at NewProducerWizardBasicStepScreen.pcf: line 170, column 52
    function toRemove_94 (producerContact :  entity.ProducerContact) : void {
      producer.removeFromContacts(producerContact)
    }
    
    // 'validationExpression' attribute on PickerInput (id=AccountRep_Input) at NewProducerWizardBasicStepScreen.pcf: line 111, column 41
    function validationExpression_58 () : java.lang.Object {
      return validateAndSetAccountRep()
    }
    
    // 'validationExpression' attribute on ListViewInput at NewProducerWizardBasicStepScreen.pcf: line 136, column 65
    function validationExpression_96 () : java.lang.Object {
      return producer.checkPrimaryContact()
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at NewProducerWizardBasicStepScreen.pcf: line 60, column 44
    function valueRange_25 () : java.lang.Object {
      return allSecurityZones
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at NewProducerWizardBasicStepScreen.pcf: line 80, column 48
    function valueRange_40 () : java.lang.Object {
      return gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, gw.payment.PaymentInstrumentFilters.producerDetailsPaymentInstrumentFilter)
    }
    
    // 'valueRange' attribute on RangeInput (id=AgencyBillPlan_Input) at NewProducerWizardBasicStepScreen.pcf: line 102, column 46
    function valueRange_52 () : java.lang.Object {
      return Plan.finder.findAllAvailablePlans<AgencyBillPlan>(AgencyBillPlan, producer.Currency)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at NewProducerWizardBasicStepScreen.pcf: line 34, column 34
    function valueRoot_6 () : java.lang.Object {
      return producer
    }
    
    // 'value' attribute on TextInput (id=PrimaryContactName_Input) at NewProducerWizardBasicStepScreen.pcf: line 117, column 56
    function valueRoot_65 () : java.lang.Object {
      return producer.PrimaryContact
    }
    
    // 'value' attribute on TextInput (id=PrimaryContactEmail_Input) at NewProducerWizardBasicStepScreen.pcf: line 129, column 66
    function valueRoot_72 () : java.lang.Object {
      return producer.PrimaryContact.Contact
    }
    
    // 'value' attribute on TextInput (id=NameKanji_Input) at NewProducerWizardBasicStepScreen.pcf: line 40, column 86
    function value_10 () : java.lang.String {
      return producer.NameKanji
    }
    
    // 'value' attribute on TypeKeyInput (id=Tier_Input) at NewProducerWizardBasicStepScreen.pcf: line 46, column 44
    function value_15 () : typekey.ProducerTier {
      return producer.Tier
    }
    
    // 'value' attribute on TextInput (id=Currency_Input) at NewProducerWizardBasicStepScreen.pcf: line 53, column 41
    function value_19 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'value' attribute on RangeInput (id=SecurityZone_Input) at NewProducerWizardBasicStepScreen.pcf: line 60, column 44
    function value_22 () : entity.SecurityZone {
      return producer.SecurityZone
    }
    
    // 'value' attribute on TypeKeyInput (id=Periodicity_Input) at NewProducerWizardBasicStepScreen.pcf: line 68, column 44
    function value_29 () : typekey.Periodicity {
      return producer.RecurPeriodicity
    }
    
    // 'value' attribute on RangeInput (id=PaymentInstrument_Input) at NewProducerWizardBasicStepScreen.pcf: line 80, column 48
    function value_37 () : entity.PaymentInstrument {
      return producer.DefaultPaymentInstrument
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at NewProducerWizardBasicStepScreen.pcf: line 34, column 34
    function value_4 () : java.lang.String {
      return producer.Name
    }
    
    // 'value' attribute on TextInput (id=CommissionDayOfMonth_Input) at NewProducerWizardBasicStepScreen.pcf: line 92, column 42
    function value_45 () : java.lang.Integer {
      return producer.RecurDayOfMonth
    }
    
    // 'value' attribute on RangeInput (id=AgencyBillPlan_Input) at NewProducerWizardBasicStepScreen.pcf: line 102, column 46
    function value_49 () : entity.AgencyBillPlan {
      return producer.AgencyBillPlan
    }
    
    // 'value' attribute on PickerInput (id=AccountRep_Input) at NewProducerWizardBasicStepScreen.pcf: line 111, column 41
    function value_60 () : java.lang.String {
      return accountRepDisplayName
    }
    
    // 'value' attribute on TextInput (id=PrimaryContactName_Input) at NewProducerWizardBasicStepScreen.pcf: line 117, column 56
    function value_64 () : java.lang.String {
      return producer.PrimaryContact.DisplayName
    }
    
    // 'value' attribute on TextInput (id=Address_Input) at NewProducerWizardBasicStepScreen.pcf: line 121, column 129
    function value_67 () : java.lang.String {
      return new gw.api.address.AddressFormatter().format(producer.PrimaryContact.Contact.PrimaryAddress, "\n")
    }
    
    // 'value' attribute on TextInput (id=PrimaryContactEmail_Input) at NewProducerWizardBasicStepScreen.pcf: line 129, column 66
    function value_71 () : java.lang.String {
      return producer.PrimaryContact.Contact.EmailAddress1
    }
    
    // 'value' attribute on RowIterator (id=producerContactIterator) at NewProducerWizardBasicStepScreen.pcf: line 170, column 52
    function value_95 () : entity.ProducerContact[] {
      return producer.Contacts
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at NewProducerWizardBasicStepScreen.pcf: line 60, column 44
    function verifyValueRangeIsAllowedType_26 ($$arg :  entity.SecurityZone[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at NewProducerWizardBasicStepScreen.pcf: line 60, column 44
    function verifyValueRangeIsAllowedType_26 ($$arg :  gw.api.database.IQueryBeanResult<entity.SecurityZone>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at NewProducerWizardBasicStepScreen.pcf: line 60, column 44
    function verifyValueRangeIsAllowedType_26 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at NewProducerWizardBasicStepScreen.pcf: line 80, column 48
    function verifyValueRangeIsAllowedType_41 ($$arg :  entity.PaymentInstrument[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at NewProducerWizardBasicStepScreen.pcf: line 80, column 48
    function verifyValueRangeIsAllowedType_41 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentInstrument>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at NewProducerWizardBasicStepScreen.pcf: line 80, column 48
    function verifyValueRangeIsAllowedType_41 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AgencyBillPlan_Input) at NewProducerWizardBasicStepScreen.pcf: line 102, column 46
    function verifyValueRangeIsAllowedType_53 ($$arg :  entity.AgencyBillPlan[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AgencyBillPlan_Input) at NewProducerWizardBasicStepScreen.pcf: line 102, column 46
    function verifyValueRangeIsAllowedType_53 ($$arg :  gw.api.database.IQueryBeanResult<entity.AgencyBillPlan>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AgencyBillPlan_Input) at NewProducerWizardBasicStepScreen.pcf: line 102, column 46
    function verifyValueRangeIsAllowedType_53 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at NewProducerWizardBasicStepScreen.pcf: line 60, column 44
    function verifyValueRange_27 () : void {
      var __valueRangeArg = allSecurityZones
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_26(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at NewProducerWizardBasicStepScreen.pcf: line 80, column 48
    function verifyValueRange_42 () : void {
      var __valueRangeArg = gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, gw.payment.PaymentInstrumentFilters.producerDetailsPaymentInstrumentFilter)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_41(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=AgencyBillPlan_Input) at NewProducerWizardBasicStepScreen.pcf: line 102, column 46
    function verifyValueRange_54 () : void {
      var __valueRangeArg = Plan.finder.findAllAvailablePlans<AgencyBillPlan>(AgencyBillPlan, producer.Currency)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_53(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=NameKanji_Input) at NewProducerWizardBasicStepScreen.pcf: line 40, column 86
    function visible_9 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP
    }
    
    property get accountRepDisplayName () : String {
      return getVariableValue("accountRepDisplayName", 0) as String
    }
    
    property set accountRepDisplayName ($arg :  String) {
      setVariableValue("accountRepDisplayName", 0, $arg)
    }
    
    property get allSecurityZones () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return getVariableValue("allSecurityZones", 0) as gw.api.database.IQueryBeanResult<SecurityZone>
    }
    
    property set allSecurityZones ($arg :  gw.api.database.IQueryBeanResult<SecurityZone>) {
      setVariableValue("allSecurityZones", 0, $arg)
    }
    
    property get paymentInstrumentRange () : gw.payment.PaymentInstrumentRange {
      return getVariableValue("paymentInstrumentRange", 0) as gw.payment.PaymentInstrumentRange
    }
    
    property set paymentInstrumentRange ($arg :  gw.payment.PaymentInstrumentRange) {
      setVariableValue("paymentInstrumentRange", 0, $arg)
    }
    
    property get producer () : Producer {
      return getRequireValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setRequireValue("producer", 0, $arg)
    }
    
    function validateAndSetAccountRep() : String {
      if (accountRepDisplayName == null) {
        producer.AccountRep = null
        return null
      }
      
      var allUsers = gw.api.database.Query.make(User).select().toList()
      var user = allUsers.firstWhere( \ user -> user.DisplayName == accountRepDisplayName )
      if (user == null) {
        return DisplayKey.get("Web.ProducerDetail.InvalidAccountRep")  
      } else {
        producer.AccountRep = user
        return null
      }
    }
    
    
  }
  
  
}