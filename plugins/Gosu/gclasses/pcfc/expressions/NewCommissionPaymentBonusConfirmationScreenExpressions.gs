package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentBonusConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewCommissionPaymentBonusConfirmationScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentBonusConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewCommissionPaymentBonusConfirmationScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentBonusConfirmationScreen.pcf: line 48, column 51
    function def_onEnter_14 (def :  pcf.ApprovalActivityDV) : void {
      def.onEnter(approvalActivity)
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentBonusConfirmationScreen.pcf: line 30, column 73
    function def_onEnter_3 (def :  pcf.NewCommissionPaymentBonusDV) : void {
      def.onEnter(bonusPayment, producer, false)
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentBonusConfirmationScreen.pcf: line 48, column 51
    function def_refreshVariables_15 (def :  pcf.ApprovalActivityDV) : void {
      def.refreshVariables(approvalActivity)
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentBonusConfirmationScreen.pcf: line 30, column 73
    function def_refreshVariables_4 (def :  pcf.NewCommissionPaymentBonusDV) : void {
      def.refreshVariables(bonusPayment, producer, false)
    }
    
    // 'initialValue' attribute on Variable at NewCommissionPaymentBonusConfirmationScreen.pcf: line 17, column 44
    function initialValue_0 () : entity.BonusCmsnApprActivity {
      return bonusPayment.getOpenApprovalActivity()
    }
    
    // 'initialValue' attribute on Variable at NewCommissionPaymentBonusConfirmationScreen.pcf: line 21, column 22
    function initialValue_1 () : String {
      return (bonusPayment.PaymentTime == CommissionPaymentTime.TC_PAYONDATE) ? bonusPayment.PayOn.toString() : bonusPayment.PaymentTime.DisplayName
    }
    
    // 'label' attribute on TextInput (id=PaymentTime_Input) at NewCommissionPaymentBonusConfirmationScreen.pcf: line 39, column 85
    function label_6 () : java.lang.Object {
      return bonusPayment.PaymentTime == CommissionPaymentTime.TC_IMMEDIATELY ? DisplayKey.get("Web.NewCommissionPaymentTimeDV.Immediately") : DisplayKey.get("Web.NewCommissionPaymentTimeDV.NextPayment")
    }
    
    // 'value' attribute on DateInput (id=PaymentDate_Input) at NewCommissionPaymentBonusConfirmationScreen.pcf: line 44, column 85
    function valueRoot_11 () : java.lang.Object {
      return bonusPayment
    }
    
    // 'value' attribute on DateInput (id=PaymentDate_Input) at NewCommissionPaymentBonusConfirmationScreen.pcf: line 44, column 85
    function value_10 () : java.util.Date {
      return bonusPayment.PayOn
    }
    
    // 'visible' attribute on AlertBar (id=ApprovalActivityAlertBar) at NewCommissionPaymentBonusConfirmationScreen.pcf: line 28, column 43
    function visible_2 () : java.lang.Boolean {
      return approvalActivity != null
    }
    
    // 'visible' attribute on TextInput (id=PaymentTime_Input) at NewCommissionPaymentBonusConfirmationScreen.pcf: line 39, column 85
    function visible_5 () : java.lang.Boolean {
      return bonusPayment.PaymentTime != CommissionPaymentTime.TC_PAYONDATE
    }
    
    // 'visible' attribute on DateInput (id=PaymentDate_Input) at NewCommissionPaymentBonusConfirmationScreen.pcf: line 44, column 85
    function visible_9 () : java.lang.Boolean {
      return bonusPayment.PaymentTime == CommissionPaymentTime.TC_PAYONDATE
    }
    
    property get approvalActivity () : entity.BonusCmsnApprActivity {
      return getVariableValue("approvalActivity", 0) as entity.BonusCmsnApprActivity
    }
    
    property set approvalActivity ($arg :  entity.BonusCmsnApprActivity) {
      setVariableValue("approvalActivity", 0, $arg)
    }
    
    property get bonusPayment () : BonusCmsnPayment {
      return getRequireValue("bonusPayment", 0) as BonusCmsnPayment
    }
    
    property set bonusPayment ($arg :  BonusCmsnPayment) {
      setRequireValue("bonusPayment", 0, $arg)
    }
    
    property get paymentTime () : String {
      return getVariableValue("paymentTime", 0) as String
    }
    
    property set paymentTime ($arg :  String) {
      setVariableValue("paymentTime", 0, $arg)
    }
    
    property get producer () : Producer {
      return getRequireValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setRequireValue("producer", 0, $arg)
    }
    
    
  }
  
  
}