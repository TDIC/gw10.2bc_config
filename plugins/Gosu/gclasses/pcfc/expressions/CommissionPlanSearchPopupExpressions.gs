package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/CommissionPlanSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionPlanSearchPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/search/CommissionPlanSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionPlanSearchPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    static function __constructorIndex (tier :  ProducerTier) : int {
      return 1
    }
    
    // 'def' attribute on ScreenRef at CommissionPlanSearchPopup.pcf: line 17, column 54
    function def_onEnter_0 (def :  pcf.CommissionPlanSearchScreen) : void {
      def.onEnter(tier, false)
    }
    
    // 'def' attribute on ScreenRef at CommissionPlanSearchPopup.pcf: line 17, column 54
    function def_refreshVariables_1 (def :  pcf.CommissionPlanSearchScreen) : void {
      def.refreshVariables(tier, false)
    }
    
    // 'title' attribute on Popup (id=CommissionPlanSearchPopup) at CommissionPlanSearchPopup.pcf: line 8, column 262
    static function title_2 (tier :  ProducerTier) : java.lang.Object {
      return tier == null ?                   DisplayKey.get("Web.Search.CommissionPlan.Title.SearchForActiveCommissionPlans") :                   DisplayKey.get("Web.Search.CommissionPlan.Title.SearchForActiveCommissionPlansWithTier", tier)
    }
    
    override property get CurrentLocation () : pcf.CommissionPlanSearchPopup {
      return super.CurrentLocation as pcf.CommissionPlanSearchPopup
    }
    
    property get tier () : ProducerTier {
      return getVariableValue("tier", 0) as ProducerTier
    }
    
    property set tier ($arg :  ProducerTier) {
      setVariableValue("tier", 0, $arg)
    }
    
    
  }
  
  
}