package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentAdvanceDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewCommissionPaymentAdvanceDVExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentAdvanceDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewCommissionPaymentAdvanceDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at NewCommissionPaymentAdvanceDV.pcf: line 25, column 40
    function currency_3 () : typekey.Currency {
      return advancePayment.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at NewCommissionPaymentAdvanceDV.pcf: line 25, column 40
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      advancePayment.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on DateInput (id=MaintainUntil_Input) at NewCommissionPaymentAdvanceDV.pcf: line 32, column 47
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      advancePayment.MaintainUntil = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'editable' attribute on DetailViewPanel (id=NewCommissionPaymentAdvanceDV) at NewCommissionPaymentAdvanceDV.pcf: line 7, column 40
    function editable_11 () : java.lang.Boolean {
      return canEdit
    }
    
    // 'validationExpression' attribute on DateInput (id=MaintainUntil_Input) at NewCommissionPaymentAdvanceDV.pcf: line 32, column 47
    function validationExpression_5 () : java.lang.Object {
      return advancePayment.MaintainUntil <= gw.api.util.DateUtil.currentDate() ? DisplayKey.get("Web.NewCommissionPaymentAdvanceDV.Error.InvalidMaintainUntil") : null
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at NewCommissionPaymentAdvanceDV.pcf: line 25, column 40
    function valueRoot_2 () : java.lang.Object {
      return advancePayment
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at NewCommissionPaymentAdvanceDV.pcf: line 25, column 40
    function value_0 () : gw.pl.currency.MonetaryAmount {
      return advancePayment.Amount
    }
    
    // 'value' attribute on DateInput (id=MaintainUntil_Input) at NewCommissionPaymentAdvanceDV.pcf: line 32, column 47
    function value_6 () : java.util.Date {
      return advancePayment.MaintainUntil
    }
    
    property get advancePayment () : AdvanceCmsnPayment {
      return getRequireValue("advancePayment", 0) as AdvanceCmsnPayment
    }
    
    property set advancePayment ($arg :  AdvanceCmsnPayment) {
      setRequireValue("advancePayment", 0, $arg)
    }
    
    property get canEdit () : Boolean {
      return getRequireValue("canEdit", 0) as Boolean
    }
    
    property set canEdit ($arg :  Boolean) {
      setRequireValue("canEdit", 0, $arg)
    }
    
    
  }
  
  
}