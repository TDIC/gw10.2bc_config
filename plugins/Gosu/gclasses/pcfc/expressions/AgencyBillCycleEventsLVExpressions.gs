package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillCycleEventsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillCycleEventsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillCycleEventsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillCycleEventsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at AgencyBillCycleEventsLV.pcf: line 15, column 34
    function initialValue_0 () : AgencyCycleProcess {
      return statementInvoice.AgencyCycleProcess
    }
    
    // 'value' attribute on DateCell (id=CycleStartEventDate_Cell) at AgencyBillCycleEventsLV.pcf: line 34, column 45
    function valueRoot_2 () : java.lang.Object {
      return statementInvoice
    }
    
    // 'value' attribute on DateCell (id=SendStatementEventDate_Cell) at AgencyBillCycleEventsLV.pcf: line 47, column 44
    function valueRoot_7 () : java.lang.Object {
      return process
    }
    
    // 'value' attribute on DateCell (id=CycleStartEventDate_Cell) at AgencyBillCycleEventsLV.pcf: line 34, column 45
    function value_1 () : java.util.Date {
      return statementInvoice.EventDate
    }
    
    // 'value' attribute on DateCell (id=SendPromiseReminderEventDate_Cell) at AgencyBillCycleEventsLV.pcf: line 60, column 50
    function value_13 () : java.util.Date {
      return process.SendPromiseReminderDate
    }
    
    // 'value' attribute on TypeKeyCell (id=SendPromiseReminderTriggerStatus_Cell) at AgencyBillCycleEventsLV.pcf: line 64, column 52
    function value_16 () : typekey.AgencyBillEventStatus {
      return process.SendPromiseReminderStatus
    }
    
    // 'value' attribute on DateCell (id=GenPromiseExceptionEventDate_Cell) at AgencyBillCycleEventsLV.pcf: line 73, column 50
    function value_20 () : java.util.Date {
      return process.GenPromiseExceptionDate
    }
    
    // 'value' attribute on TypeKeyCell (id=GenPromiseExceptionTriggerStatus_Cell) at AgencyBillCycleEventsLV.pcf: line 77, column 52
    function value_23 () : typekey.AgencyBillEventStatus {
      return process.GenPromiseExceptionStatus
    }
    
    // 'value' attribute on DateCell (id=DueEventDate_Cell) at AgencyBillCycleEventsLV.pcf: line 85, column 43
    function value_27 () : java.util.Date {
      return statementInvoice.DueDate
    }
    
    // 'value' attribute on TypeKeyCell (id=DueTriggerStatus_Cell) at AgencyBillCycleEventsLV.pcf: line 89, column 52
    function value_30 () : typekey.AgencyBillEventStatus {
      return isExecutedPaymentDue()
    }
    
    // 'value' attribute on DateCell (id=GenPastDueExceptionEventDate_Cell) at AgencyBillCycleEventsLV.pcf: line 98, column 50
    function value_32 () : java.util.Date {
      return process.GenPastDueExceptionDate
    }
    
    // 'value' attribute on TypeKeyCell (id=StatementSentTriggerStatus_Cell) at AgencyBillCycleEventsLV.pcf: line 102, column 52
    function value_35 () : typekey.AgencyBillEventStatus {
      return process.GenPastDueExceptionStatus
    }
    
    // 'value' attribute on DateCell (id=SendDunning1EventDate_Cell) at AgencyBillCycleEventsLV.pcf: line 111, column 43
    function value_39 () : java.util.Date {
      return process.SendDunning1Date
    }
    
    // 'value' attribute on TypeKeyCell (id=CycleStartTriggerStatus_Cell) at AgencyBillCycleEventsLV.pcf: line 38, column 52
    function value_4 () : typekey.AgencyBillEventStatus {
      return isExecuted(statementInvoice.EventDate)
    }
    
    // 'value' attribute on TypeKeyCell (id=SendDunning1TriggerStatus_Cell) at AgencyBillCycleEventsLV.pcf: line 115, column 52
    function value_42 () : typekey.AgencyBillEventStatus {
      return process.SendDunning1Status
    }
    
    // 'value' attribute on DateCell (id=SendDunning2EventDate_Cell) at AgencyBillCycleEventsLV.pcf: line 124, column 43
    function value_46 () : java.util.Date {
      return process.SendDunning2Date
    }
    
    // 'value' attribute on TypeKeyCell (id=SendDunning2TriggerStatus_Cell) at AgencyBillCycleEventsLV.pcf: line 128, column 52
    function value_49 () : typekey.AgencyBillEventStatus {
      return process.SendDunning2Status
    }
    
    // 'value' attribute on DateCell (id=SendStatementEventDate_Cell) at AgencyBillCycleEventsLV.pcf: line 47, column 44
    function value_6 () : java.util.Date {
      return process.SendStatementDate
    }
    
    // 'value' attribute on TypeKeyCell (id=SendStatementTriggerStatus_Cell) at AgencyBillCycleEventsLV.pcf: line 51, column 52
    function value_9 () : typekey.AgencyBillEventStatus {
      return process.SendStatementStatus
    }
    
    // 'visible' attribute on Row at AgencyBillCycleEventsLV.pcf: line 41, column 86
    function visible_12 () : java.lang.Boolean {
      return process.SendStatementStatus != AgencyBillEventStatus.TC_NOTSCHEDULED
    }
    
    // 'visible' attribute on Row at AgencyBillCycleEventsLV.pcf: line 54, column 92
    function visible_19 () : java.lang.Boolean {
      return process.SendPromiseReminderStatus != AgencyBillEventStatus.TC_NOTSCHEDULED
    }
    
    // 'visible' attribute on Row at AgencyBillCycleEventsLV.pcf: line 67, column 92
    function visible_26 () : java.lang.Boolean {
      return process.GenPromiseExceptionStatus != AgencyBillEventStatus.TC_NOTSCHEDULED
    }
    
    // 'visible' attribute on Row at AgencyBillCycleEventsLV.pcf: line 92, column 92
    function visible_38 () : java.lang.Boolean {
      return process.GenPastDueExceptionStatus != AgencyBillEventStatus.TC_NOTSCHEDULED
    }
    
    // 'visible' attribute on Row at AgencyBillCycleEventsLV.pcf: line 105, column 85
    function visible_45 () : java.lang.Boolean {
      return process.SendDunning1Status != AgencyBillEventStatus.TC_NOTSCHEDULED
    }
    
    // 'visible' attribute on Row at AgencyBillCycleEventsLV.pcf: line 118, column 85
    function visible_52 () : java.lang.Boolean {
      return process.SendDunning2Status != AgencyBillEventStatus.TC_NOTSCHEDULED
    }
    
    // 'visible' attribute on ListViewPanel (id=AgencyBillCycleEventsLV) at AgencyBillCycleEventsLV.pcf: line 7, column 31
    function visible_53 () : java.lang.Boolean {
      return process != null
    }
    
    property get process () : AgencyCycleProcess {
      return getVariableValue("process", 0) as AgencyCycleProcess
    }
    
    property set process ($arg :  AgencyCycleProcess) {
      setVariableValue("process", 0, $arg)
    }
    
    property get statementInvoice () : StatementInvoice {
      return getRequireValue("statementInvoice", 0) as StatementInvoice
    }
    
    property set statementInvoice ($arg :  StatementInvoice) {
      setRequireValue("statementInvoice", 0, $arg)
    }
    
    function isExecuted(executionDate : java.util.Date) : AgencyBillEventStatus {
      return java.util.Date.CurrentDate.before(executionDate) ? AgencyBillEventStatus.TC_FUTURE : AgencyBillEventStatus.TC_EXECUTED
    }
    
    function isExecutedPaymentDue() : AgencyBillEventStatus {
      return  statementInvoice.getStatus() == InvoiceStatus.TC_DUE ? AgencyBillEventStatus.TC_EXECUTED : AgencyBillEventStatus.TC_FUTURE
    }
    
    
  }
  
  
}