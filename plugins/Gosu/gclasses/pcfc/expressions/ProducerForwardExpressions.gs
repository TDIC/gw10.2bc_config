package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ForwardCondition at ProducerForward.pcf: line 18, column 126
    function action_0 () : void {
      gw.api.web.producer.ProducerUtil.getMostRecentlyViewedProducerInfo().goToDestination()
    }
    
    // 'action' attribute on ForwardCondition at ProducerForward.pcf: line 20, column 37
    function action_2 () : void {
      ProducersGroup.go()
    }
    
    // 'action' attribute on ForwardCondition at ProducerForward.pcf: line 20, column 37
    function action_dest_3 () : pcf.api.Destination {
      return pcf.ProducersGroup.createDestination()
    }
    
    // 'canVisit' attribute on Forward (id=ProducerForward) at ProducerForward.pcf: line 7, column 26
    static function canVisit_4 () : java.lang.Boolean {
      return perm.System.prodtabview
    }
    
    // 'condition' attribute on ForwardCondition at ProducerForward.pcf: line 18, column 126
    function condition_1 () : java.lang.Boolean {
      return gotoMostRecentProducer and (gw.api.web.producer.ProducerUtil.getMostRecentlyViewedProducerInfo() != null)
    }
    
    override property get CurrentLocation () : pcf.ProducerForward {
      return super.CurrentLocation as pcf.ProducerForward
    }
    
    property get gotoMostRecentProducer () : Boolean {
      return getVariableValue("gotoMostRecentProducer", 0) as Boolean
    }
    
    property set gotoMostRecentProducer ($arg :  Boolean) {
      setVariableValue("gotoMostRecentProducer", 0, $arg)
    }
    
    
  }
  
  
}