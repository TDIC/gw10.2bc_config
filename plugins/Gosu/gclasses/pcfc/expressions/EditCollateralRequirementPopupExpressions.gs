package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/EditCollateralRequirementPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class EditCollateralRequirementPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/collateral/EditCollateralRequirementPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class EditCollateralRequirementPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (requirement :  CollateralRequirement) : int {
      return 0
    }
    
    // 'afterCommit' attribute on Popup (id=EditCollateralRequirementPopup) at EditCollateralRequirementPopup.pcf: line 12, column 77
    function afterCommit_20 (TopLocation :  pcf.api.Location) : void {
      AccountCollateral.go(requirement.Collateral.Account)
    }
    
    // 'beforeCommit' attribute on Popup (id=EditCollateralRequirementPopup) at EditCollateralRequirementPopup.pcf: line 12, column 77
    function beforeCommit_21 (pickedValue :  java.lang.Object) : void {
      collateralUtil.allocateRequirementIfNewEffectiveDateIsToday(requirement)
    }
    
    // 'canVisit' attribute on Popup (id=EditCollateralRequirementPopup) at EditCollateralRequirementPopup.pcf: line 12, column 77
    static function canVisit_22 (requirement :  CollateralRequirement) : java.lang.Boolean {
      return perm.Account.collateralreqedit
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=cash_Input) at EditCollateralRequirementPopup.pcf: line 45, column 50
    function currency_6 () : typekey.Currency {
      return requirement.Currency
    }
    
    // 'def' attribute on PanelRef at EditCollateralRequirementPopup.pcf: line 73, column 80
    function def_onEnter_18 (def :  pcf.CollateralRequirementDV) : void {
      def.onEnter(requirement, isEffectiveDateEditable())
    }
    
    // 'def' attribute on PanelRef at EditCollateralRequirementPopup.pcf: line 73, column 80
    function def_refreshVariables_19 (def :  pcf.CollateralRequirementDV) : void {
      def.refreshVariables(requirement, isEffectiveDateEditable())
    }
    
    // 'initialValue' attribute on Variable at EditCollateralRequirementPopup.pcf: line 21, column 49
    function initialValue_0 () : gw.api.web.account.CollateralUtil {
      return new gw.api.web.account.CollateralUtil()
    }
    
    // EditButtons at EditCollateralRequirementPopup.pcf: line 25, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'value' attribute on MonetaryAmountInput (id=cash_Input) at EditCollateralRequirementPopup.pcf: line 45, column 50
    function valueRoot_5 () : java.lang.Object {
      return requirement
    }
    
    // 'value' attribute on MonetaryAmountInput (id=total_Input) at EditCollateralRequirementPopup.pcf: line 60, column 78
    function value_12 () : gw.pl.currency.MonetaryAmount {
      return collateralUtil.getTotalRequirementHeld( requirement )
    }
    
    // 'value' attribute on MonetaryAmountInput (id=surplus_Input) at EditCollateralRequirementPopup.pcf: line 67, column 65
    function value_15 () : gw.pl.currency.MonetaryAmount {
      return collateralUtil.getSurplus( requirement )
    }
    
    // 'value' attribute on MonetaryAmountInput (id=cash_Input) at EditCollateralRequirementPopup.pcf: line 45, column 50
    function value_4 () : gw.pl.currency.MonetaryAmount {
      return requirement.CashAllocated
    }
    
    // 'value' attribute on MonetaryAmountInput (id=loc_Input) at EditCollateralRequirementPopup.pcf: line 51, column 48
    function value_8 () : gw.pl.currency.MonetaryAmount {
      return requirement.LOCAllocated
    }
    
    // 'visible' attribute on AlertBar (id=amountWarning) at EditCollateralRequirementPopup.pcf: line 30, column 153
    function visible_2 () : java.lang.Boolean {
      return (requirement.OriginalVersion as CollateralRequirement).Required < requirement.Required and requirement.RequirementType == TC_CASH
    }
    
    // 'visible' attribute on AlertBar (id=dateWarning) at EditCollateralRequirementPopup.pcf: line 34, column 208
    function visible_3 () : java.lang.Boolean {
      return gw.api.util.DateUtil.compareIgnoreTime ( (requirement.OriginalVersion as CollateralRequirement).EffectiveDate, requirement.EffectiveDate) > 0 and requirement.RequirementType == TC_CASH
    }
    
    override property get CurrentLocation () : pcf.EditCollateralRequirementPopup {
      return super.CurrentLocation as pcf.EditCollateralRequirementPopup
    }
    
    property get collateralUtil () : gw.api.web.account.CollateralUtil {
      return getVariableValue("collateralUtil", 0) as gw.api.web.account.CollateralUtil
    }
    
    property set collateralUtil ($arg :  gw.api.web.account.CollateralUtil) {
      setVariableValue("collateralUtil", 0, $arg)
    }
    
    property get requirement () : CollateralRequirement {
      return getVariableValue("requirement", 0) as CollateralRequirement
    }
    
    property set requirement ($arg :  CollateralRequirement) {
      setVariableValue("requirement", 0, $arg)
    }
    
    
    function isEffectiveDateEditable() : Boolean{
            return (requirement.Compliance == TC_PENDING)
          }
        
    
    
  }
  
  
}