package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewTransactionWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewTransactionWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewTransactionWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewTransactionWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=NewTransactionWizard) at NewTransactionWizard.pcf: line 12, column 31
    function afterCancel_8 () : void {
      DesktopGroup.go()
    }
    
    // 'afterCancel' attribute on Wizard (id=NewTransactionWizard) at NewTransactionWizard.pcf: line 12, column 31
    function afterCancel_dest_9 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewTransactionWizard) at NewTransactionWizard.pcf: line 12, column 31
    function afterFinish_14 () : void {
      DesktopGroup.go()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewTransactionWizard) at NewTransactionWizard.pcf: line 12, column 31
    function afterFinish_dest_15 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'allowNext' attribute on WizardStep (id=Step1) at NewTransactionWizard.pcf: line 29, column 95
    function allowNext_2 () : java.lang.Boolean {
      return tAccountOwnerReference.TAccountOwner != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=NewTransactionWizard) at NewTransactionWizard.pcf: line 12, column 31
    function beforeCommit_10 (pickedValue :  java.lang.Object) : void {
      helper.BillingInstruction.execute()
    }
    
    // 'canVisit' attribute on Wizard (id=NewTransactionWizard) at NewTransactionWizard.pcf: line 12, column 31
    static function canVisit_11 () : java.lang.Boolean {
      return perm.Transaction.gentxn
    }
    
    // 'initialValue' attribute on Variable at NewTransactionWizard.pcf: line 18, column 60
    function initialValue_0 () : gw.api.web.accounting.TAccountOwnerReference {
      return new gw.api.web.accounting.TAccountOwnerReference()
    }
    
    // 'initialValue' attribute on Variable at NewTransactionWizard.pcf: line 22, column 58
    function initialValue_1 () : gw.accounting.NewGeneralSingleChargeHelper {
      return new gw.accounting.NewGeneralSingleChargeHelper(gw.transaction.UserTransactionType.FEE_OR_GENERAL)
    }
    
    // 'onExit' attribute on WizardStep (id=Step1) at NewTransactionWizard.pcf: line 29, column 95
    function onExit_3 () : void {
      helper.TAccountOwner = tAccountOwnerReference.TAccountOwner
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at NewTransactionWizard.pcf: line 29, column 95
    function screen_onEnter_4 (def :  pcf.WizardsStep1AccountPolicySearchScreen) : void {
      def.onEnter(tAccountOwnerReference, null, false, true)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at NewTransactionWizard.pcf: line 34, column 83
    function screen_onEnter_6 (def :  pcf.ChargeDetailsScreen) : void {
      def.onEnter(helper)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at NewTransactionWizard.pcf: line 29, column 95
    function screen_refreshVariables_5 (def :  pcf.WizardsStep1AccountPolicySearchScreen) : void {
      def.refreshVariables(tAccountOwnerReference, null, false, true)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at NewTransactionWizard.pcf: line 34, column 83
    function screen_refreshVariables_7 (def :  pcf.ChargeDetailsScreen) : void {
      def.refreshVariables(helper)
    }
    
    // 'tabBar' attribute on Wizard (id=NewTransactionWizard) at NewTransactionWizard.pcf: line 12, column 31
    function tabBar_onEnter_12 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=NewTransactionWizard) at NewTransactionWizard.pcf: line 12, column 31
    function tabBar_refreshVariables_13 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.NewTransactionWizard {
      return super.CurrentLocation as pcf.NewTransactionWizard
    }
    
    property get helper () : gw.accounting.NewGeneralSingleChargeHelper {
      return getVariableValue("helper", 0) as gw.accounting.NewGeneralSingleChargeHelper
    }
    
    property set helper ($arg :  gw.accounting.NewGeneralSingleChargeHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get tAccountOwnerReference () : gw.api.web.accounting.TAccountOwnerReference {
      return getVariableValue("tAccountOwnerReference", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set tAccountOwnerReference ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setVariableValue("tAccountOwnerReference", 0, $arg)
    }
    
    
  }
  
  
}