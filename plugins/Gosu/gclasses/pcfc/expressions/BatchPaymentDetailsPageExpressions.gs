package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/BatchPaymentDetailsPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BatchPaymentDetailsPageExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/BatchPaymentDetailsPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BatchPaymentDetailsPageExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (batchPayment :  BatchPayment) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=BatchPaymentDetailsPage) at BatchPaymentDetailsPage.pcf: line 11, column 101
    static function canVisit_3 (batchPayment :  BatchPayment) : java.lang.Boolean {
      return perm.System.viewdesktop and perm.BatchPayment.view
    }
    
    // 'def' attribute on ScreenRef at BatchPaymentDetailsPage.pcf: line 22, column 65
    function def_onEnter_1 (def :  pcf.BatchPaymentDetailsScreen) : void {
      def.onEnter(batchPaymentDetailsView)
    }
    
    // 'def' attribute on ScreenRef at BatchPaymentDetailsPage.pcf: line 22, column 65
    function def_refreshVariables_2 (def :  pcf.BatchPaymentDetailsScreen) : void {
      def.refreshVariables(batchPaymentDetailsView)
    }
    
    // 'initialValue' attribute on Variable at BatchPaymentDetailsPage.pcf: line 20, column 60
    function initialValue_0 () : gw.web.payment.batch.BatchPaymentDetailsView {
      return gw.web.payment.batch.BatchPaymentDetailsView.newModificationInstance(batchPayment)
    }
    
    // 'parent' attribute on Page (id=BatchPaymentDetailsPage) at BatchPaymentDetailsPage.pcf: line 11, column 101
    static function parent_4 (batchPayment :  BatchPayment) : pcf.api.Destination {
      return pcf.DesktopBatchPaymentsSearch.createDestination()
    }
    
    // 'title' attribute on Page (id=BatchPaymentDetailsPage) at BatchPaymentDetailsPage.pcf: line 11, column 101
    static function title_5 (batchPayment :  BatchPayment) : java.lang.Object {
      return DisplayKey.get("Web.BatchPaymentDetailsPage.Title", batchPayment.BatchNumber)
    }
    
    override property get CurrentLocation () : pcf.BatchPaymentDetailsPage {
      return super.CurrentLocation as pcf.BatchPaymentDetailsPage
    }
    
    property get batchPayment () : BatchPayment {
      return getVariableValue("batchPayment", 0) as BatchPayment
    }
    
    property set batchPayment ($arg :  BatchPayment) {
      setVariableValue("batchPayment", 0, $arg)
    }
    
    property get batchPaymentDetailsView () : gw.web.payment.batch.BatchPaymentDetailsView {
      return getVariableValue("batchPaymentDetailsView", 0) as gw.web.payment.batch.BatchPaymentDetailsView
    }
    
    property set batchPaymentDetailsView ($arg :  gw.web.payment.batch.BatchPaymentDetailsView) {
      setVariableValue("batchPaymentDetailsView", 0, $arg)
    }
    
    
  }
  
  
}