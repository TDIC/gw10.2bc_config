package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/BatchPaymentDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BatchPaymentDetailsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/BatchPaymentDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BatchPaymentDetailsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=PostBatchButton) at BatchPaymentDetailsScreen.pcf: line 22, column 100
    function action_5 () : void {
      batchPaymentDetailsView.postAsync()
    }
    
    // 'action' attribute on ToolbarButton (id=ReverseBatchButton) at BatchPaymentDetailsScreen.pcf: line 29, column 108
    function action_8 () : void {
      batchPaymentDetailsView.reverseAsync()
    }
    
    // 'available' attribute on ToolbarButton (id=PostBatchButton) at BatchPaymentDetailsScreen.pcf: line 22, column 100
    function available_3 () : java.lang.Boolean {
      return batchPaymentDetailsView.BatchPayment.canBePosted() and batchPaymentDetailsView.PostPermitted
    }
    
    // 'available' attribute on ToolbarButton (id=ReverseBatchButton) at BatchPaymentDetailsScreen.pcf: line 29, column 108
    function available_6 () : java.lang.Boolean {
      return batchPaymentDetailsView.BatchPayment.canBeReversed() and batchPaymentDetailsView.ReversePermitted
    }
    
    // 'def' attribute on PanelRef (id=BatchPaymentEntriesRef) at BatchPaymentDetailsScreen.pcf: line 51, column 30
    function def_onEnter_16 (def :  pcf.BatchPaymentEntriesLV) : void {
      def.onEnter(batchPaymentDetailsView)
    }
    
    // 'def' attribute on PanelRef (id=BatchInfoRef) at BatchPaymentDetailsScreen.pcf: line 35, column 23
    function def_onEnter_9 (def :  pcf.BatchInfoDV) : void {
      def.onEnter(batchPaymentDetailsView)
    }
    
    // 'def' attribute on PanelRef (id=BatchInfoRef) at BatchPaymentDetailsScreen.pcf: line 35, column 23
    function def_refreshVariables_10 (def :  pcf.BatchInfoDV) : void {
      def.refreshVariables(batchPaymentDetailsView)
    }
    
    // 'def' attribute on PanelRef (id=BatchPaymentEntriesRef) at BatchPaymentDetailsScreen.pcf: line 51, column 30
    function def_refreshVariables_17 (def :  pcf.BatchPaymentEntriesLV) : void {
      def.refreshVariables(batchPaymentDetailsView)
    }
    
    // 'label' attribute on AlertBar (id=SuspensePaymentsDisbursedWarning) at BatchPaymentDetailsScreen.pcf: line 47, column 141
    function label_14 () : java.lang.Object {
      return batchPaymentDetailsView.DisbursedSuspensePaymentsWarning
    }
    
    // 'cancelVisible' attribute on EditButtons at BatchPaymentDetailsScreen.pcf: line 15, column 115
    function visible_0 () : java.lang.Boolean {
      return batchPaymentDetailsView.BatchPaymentEditable and batchPaymentDetailsView.CancelPermitted
    }
    
    // 'editVisible' attribute on EditButtons at BatchPaymentDetailsScreen.pcf: line 15, column 115
    function visible_1 () : java.lang.Boolean {
      return batchPaymentDetailsView.BatchPaymentEditable and batchPaymentDetailsView.EditPermitted
    }
    
    // 'visible' attribute on AlertBar (id=EmptyBatchWarning) at BatchPaymentDetailsScreen.pcf: line 39, column 72
    function visible_11 () : java.lang.Boolean {
      return batchPaymentDetailsView.BatchPayment.Payments.IsEmpty
    }
    
    // 'visible' attribute on AlertBar (id=DoesNotIncludeActionsTakenAfterPostingWarning) at BatchPaymentDetailsScreen.pcf: line 43, column 71
    function visible_12 () : java.lang.Boolean {
      return batchPaymentDetailsView.BatchPayment.hasBeenPosted()
    }
    
    // 'visible' attribute on AlertBar (id=SuspensePaymentsDisbursedWarning) at BatchPaymentDetailsScreen.pcf: line 47, column 141
    function visible_13 () : java.lang.Boolean {
      return batchPaymentDetailsView.BatchPayment.hasBeenPosted() and batchPaymentDetailsView.DisbursedSuspensePaymentsWarning.NotBlank
    }
    
    // 'visible' attribute on Toolbar (id=BatchPaymentEntriesLV_tb) at BatchPaymentDetailsScreen.pcf: line 53, column 95
    function visible_15 () : java.lang.Boolean {
      return CurrentLocation.InEditMode and batchPaymentDetailsView.BatchPaymentEditable
    }
    
    // 'updateVisible' attribute on EditButtons at BatchPaymentDetailsScreen.pcf: line 15, column 115
    function visible_2 () : java.lang.Boolean {
      return batchPaymentDetailsView.BatchPaymentEditable and batchPaymentDetailsView.UpdatePermitted
    }
    
    // 'visible' attribute on ToolbarButton (id=PostBatchButton) at BatchPaymentDetailsScreen.pcf: line 22, column 100
    function visible_4 () : java.lang.Boolean {
      return not CurrentLocation.InEditMode and batchPaymentDetailsView.BatchPaymentEditable
    }
    
    // 'visible' attribute on ToolbarButton (id=ReverseBatchButton) at BatchPaymentDetailsScreen.pcf: line 29, column 108
    function visible_7 () : java.lang.Boolean {
      return not CurrentLocation.InEditMode and batchPaymentDetailsView.BatchPayment.canBeReversed()
    }
    
    property get batchPaymentDetailsView () : gw.web.payment.batch.BatchPaymentDetailsView {
      return getRequireValue("batchPaymentDetailsView", 0) as gw.web.payment.batch.BatchPaymentDetailsView
    }
    
    property set batchPaymentDetailsView ($arg :  gw.web.payment.batch.BatchPaymentDetailsView) {
      setRequireValue("batchPaymentDetailsView", 0, $arg)
    }
    
    
  }
  
  
}