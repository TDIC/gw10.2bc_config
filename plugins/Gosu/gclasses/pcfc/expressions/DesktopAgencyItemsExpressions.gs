package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopAgencyItems.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopAgencyItemsExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopAgencyItems.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopAgencyItemsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=Dismiss) at DesktopAgencyItems.pcf: line 125, column 91
    function allCheckedRowsAction_24 (CheckedValues :  entity.AgencyCycleProcess[], CheckedValuesErrors :  java.util.Map) : void {
      dismissPaymentPastDueExceptions(CheckedValues); numLatePayments = latePayments.getCount()
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=Dismiss) at DesktopAgencyItems.pcf: line 141, column 91
    function allCheckedRowsAction_28 (CheckedValues :  entity.AgencyCycleProcess[], CheckedValuesErrors :  java.util.Map) : void {
      dismissPromisePastDueExceptions(CheckedValues); numLatePromises = latePromises.getCount()
    }
    
    // 'canVisit' attribute on Page (id=DesktopAgencyItems) at DesktopAgencyItems.pcf: line 8, column 64
    static function canVisit_40 () : java.lang.Boolean {
      return perm.System.viewdesktop and perm.System.myagencyitemsview
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=ViewExceptions) at DesktopAgencyItems.pcf: line 75, column 45
    function checkedRowAction_12 (element :  gw.api.web.invoice.ProducerWithExceptionsView, CheckedValue :  gw.api.web.invoice.ProducerWithExceptionsView) : void {
      AgencyBillExceptions.push(CheckedValue.Producer)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=ViewExceptions) at DesktopAgencyItems.pcf: line 93, column 45
    function checkedRowAction_16 (element :  gw.api.web.invoice.ProducerWithExceptionsView, CheckedValue :  gw.api.web.invoice.ProducerWithExceptionsView) : void {
      AgencyBillExceptions.push(CheckedValue.Producer, 1)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=DistributePayment) at DesktopAgencyItems.pcf: line 110, column 45
    function checkedRowAction_20 (element :  entity.AgencyBillMoneyRcvd, CheckedValue :  entity.AgencyBillMoneyRcvd) : void {
      AgencyDistributionWizard.go(CheckedValue.Producer, CheckedValue)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=Release) at DesktopAgencyItems.pcf: line 157, column 93
    function checkedRowAction_32 (element :  entity.BaseSuspDistItem, CheckedValue :  entity.BaseSuspDistItem) : void {
      CheckedValue.goToSuspenseItemPopup()
    }
    
    // 'def' attribute on PanelRef at DesktopAgencyItems.pcf: line 66, column 77
    function def_onEnter_13 (def :  pcf.ProducersWithExceptionsLV) : void {
      def.onEnter(producersWithPaymentExceptions)
    }
    
    // 'def' attribute on PanelRef (id=ProducerWithPromiseException) at DesktopAgencyItems.pcf: line 84, column 47
    function def_onEnter_17 (def :  pcf.ProducersWithExceptionsLV) : void {
      def.onEnter(producersWithPromiseExceptions)
    }
    
    // 'def' attribute on PanelRef at DesktopAgencyItems.pcf: line 101, column 66
    function def_onEnter_21 (def :  pcf.ReceivedAndUnappliedMoneyLV) : void {
      def.onEnter(unappliedPayments)
    }
    
    // 'def' attribute on PanelRef at DesktopAgencyItems.pcf: line 118, column 62
    function def_onEnter_25 (def :  pcf.LateDistributionExceptionsLV) : void {
      def.onEnter(latePayments)
    }
    
    // 'def' attribute on PanelRef (id=LatePromiseExceptionsLV) at DesktopAgencyItems.pcf: line 134, column 42
    function def_onEnter_29 (def :  pcf.LateDistributionExceptionsLV) : void {
      def.onEnter(latePromises)
    }
    
    // 'def' attribute on PanelRef (id=AgencySuspPaymentItemsLV) at DesktopAgencyItems.pcf: line 150, column 43
    function def_onEnter_33 (def :  pcf.AgencySuspDistItemsLV) : void {
      def.onEnter(suspensePaymentItems)
    }
    
    // 'def' attribute on PanelRef (id=AgencySuspPromiseItemsLV) at DesktopAgencyItems.pcf: line 166, column 43
    function def_onEnter_37 (def :  pcf.AgencySuspDistItemsLV) : void {
      def.onEnter(suspensePromiseItems)
    }
    
    // 'def' attribute on PanelRef at DesktopAgencyItems.pcf: line 66, column 77
    function def_refreshVariables_14 (def :  pcf.ProducersWithExceptionsLV) : void {
      def.refreshVariables(producersWithPaymentExceptions)
    }
    
    // 'def' attribute on PanelRef (id=ProducerWithPromiseException) at DesktopAgencyItems.pcf: line 84, column 47
    function def_refreshVariables_18 (def :  pcf.ProducersWithExceptionsLV) : void {
      def.refreshVariables(producersWithPromiseExceptions)
    }
    
    // 'def' attribute on PanelRef at DesktopAgencyItems.pcf: line 101, column 66
    function def_refreshVariables_22 (def :  pcf.ReceivedAndUnappliedMoneyLV) : void {
      def.refreshVariables(unappliedPayments)
    }
    
    // 'def' attribute on PanelRef at DesktopAgencyItems.pcf: line 118, column 62
    function def_refreshVariables_26 (def :  pcf.LateDistributionExceptionsLV) : void {
      def.refreshVariables(latePayments)
    }
    
    // 'def' attribute on PanelRef (id=LatePromiseExceptionsLV) at DesktopAgencyItems.pcf: line 134, column 42
    function def_refreshVariables_30 (def :  pcf.LateDistributionExceptionsLV) : void {
      def.refreshVariables(latePromises)
    }
    
    // 'def' attribute on PanelRef (id=AgencySuspPaymentItemsLV) at DesktopAgencyItems.pcf: line 150, column 43
    function def_refreshVariables_34 (def :  pcf.AgencySuspDistItemsLV) : void {
      def.refreshVariables(suspensePaymentItems)
    }
    
    // 'def' attribute on PanelRef (id=AgencySuspPromiseItemsLV) at DesktopAgencyItems.pcf: line 166, column 43
    function def_refreshVariables_38 (def :  pcf.AgencySuspDistItemsLV) : void {
      def.refreshVariables(suspensePromiseItems)
    }
    
    // 'initialValue' attribute on Variable at DesktopAgencyItems.pcf: line 12, column 19
    function initialValue_0 () : int {
      return (User.util.CurrentUser as User).ReceivedAndUndistributedMoney.getCount()
    }
    
    // 'initialValue' attribute on Variable at DesktopAgencyItems.pcf: line 16, column 19
    function initialValue_1 () : int {
      return (User.util.CurrentUser as User).AgencyCyclesWithPaymentPastDue.getCount()
    }
    
    // 'initialValue' attribute on Variable at DesktopAgencyItems.pcf: line 52, column 74
    function initialValue_10 () : gw.api.database.IQueryBeanResult<AgencySuspPmntItem> {
      return numSuspensePaymentItems > 0 ? (User.util.CurrentUser as User).findAgencySuspensePaymentItemsThatNeedRelease() : null
    }
    
    // 'initialValue' attribute on Variable at DesktopAgencyItems.pcf: line 56, column 77
    function initialValue_11 () : gw.api.database.IQueryBeanResult<AgencySuspPromiseItem> {
      return numSuspensePromiseItems > 0 ? (User.util.CurrentUser as User).findAgencySuspensePromiseItemsThatNeedRelease() : null
    }
    
    // 'initialValue' attribute on Variable at DesktopAgencyItems.pcf: line 20, column 19
    function initialValue_2 () : int {
      return (User.util.CurrentUser as User).AgencyCyclesWithPromisePastDue.getCount()
    }
    
    // 'initialValue' attribute on Variable at DesktopAgencyItems.pcf: line 24, column 19
    function initialValue_3 () : int {
      return (User.util.CurrentUser as User).findAgencySuspensePaymentItemsThatNeedRelease().Count
    }
    
    // 'initialValue' attribute on Variable at DesktopAgencyItems.pcf: line 28, column 19
    function initialValue_4 () : int {
      return (User.util.CurrentUser as User).findAgencySuspensePromiseItemsThatNeedRelease().Count
    }
    
    // 'initialValue' attribute on Variable at DesktopAgencyItems.pcf: line 32, column 63
    function initialValue_5 () : gw.api.web.invoice.ProducerWithExceptionsView[] {
      return (User.util.CurrentUser as User).findProducersWithPaymentExceptions()
    }
    
    // 'initialValue' attribute on Variable at DesktopAgencyItems.pcf: line 36, column 63
    function initialValue_6 () : gw.api.web.invoice.ProducerWithExceptionsView[] {
      return (User.util.CurrentUser as User).findProducersWithPromiseExceptions()
    }
    
    // 'initialValue' attribute on Variable at DesktopAgencyItems.pcf: line 40, column 76
    function initialValue_7 () : gw.api.database.IQueryBeanResult<PaymentMoneyReceived> {
      return numUnappliedPayments > 0 ? (User.util.CurrentUser as User).ReceivedAndUndistributedMoney : null
    }
    
    // 'initialValue' attribute on Variable at DesktopAgencyItems.pcf: line 44, column 74
    function initialValue_8 () : gw.api.database.IQueryBeanResult<AgencyCycleProcess> {
      return numLatePayments > 0 ? (User.util.CurrentUser as User).AgencyCyclesWithPaymentPastDue : null
    }
    
    // 'initialValue' attribute on Variable at DesktopAgencyItems.pcf: line 48, column 74
    function initialValue_9 () : gw.api.database.IQueryBeanResult<AgencyCycleProcess> {
      return numLatePromises > 0 ? (User.util.CurrentUser as User).AgencyCyclesWithPromisePastDue : null
    }
    
    // Page (id=DesktopAgencyItems) at DesktopAgencyItems.pcf: line 8, column 64
    static function parent_41 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'title' attribute on Card (id=ProducersWithMismatchExceptionsCard) at DesktopAgencyItems.pcf: line 64, column 120
    function title_15 () : java.lang.String {
      return DisplayKey.get("Web.DesktopAgencyItems.Exceptions", producersWithPaymentExceptions.length)
    }
    
    // 'title' attribute on Card (id=ProducersWithPromiseExceptionsCard) at DesktopAgencyItems.pcf: line 81, column 127
    function title_19 () : java.lang.String {
      return DisplayKey.get("Web.DesktopAgencyItems.PromiseExceptions", producersWithPromiseExceptions.length)
    }
    
    // 'title' attribute on Card (id=ReceivedAndUnappliedMoneyCard) at DesktopAgencyItems.pcf: line 99, column 110
    function title_23 () : java.lang.String {
      return DisplayKey.get("Web.DesktopAgencyItems.UnappliedPayments", numUnappliedPayments)
    }
    
    // 'title' attribute on Card (id=PaymentPastDueExceptionsCard) at DesktopAgencyItems.pcf: line 116, column 112
    function title_27 () : java.lang.String {
      return DisplayKey.get("Web.DesktopAgencyItems.PaymentPastDueExceptions", numLatePayments)
    }
    
    // 'title' attribute on Card (id=LatePromiseExceptionsCard) at DesktopAgencyItems.pcf: line 131, column 111
    function title_31 () : java.lang.String {
      return DisplayKey.get("Web.DesktopAgencyItems.LatePromiseExceptions",  numLatePromises )
    }
    
    // 'title' attribute on Card (id=AgencySuspensePaymentItemsCard) at DesktopAgencyItems.pcf: line 147, column 122
    function title_35 () : java.lang.String {
      return DisplayKey.get("Web.DesktopAgencyItems.AgencySuspensePaymentItems", numSuspensePaymentItems)
    }
    
    // 'title' attribute on Card (id=AgencySuspensePromiseItemsCard) at DesktopAgencyItems.pcf: line 163, column 122
    function title_39 () : java.lang.String {
      return DisplayKey.get("Web.DesktopAgencyItems.AgencySuspensePromiseItems", numSuspensePromiseItems)
    }
    
    override property get CurrentLocation () : pcf.DesktopAgencyItems {
      return super.CurrentLocation as pcf.DesktopAgencyItems
    }
    
    property get latePayments () : gw.api.database.IQueryBeanResult<AgencyCycleProcess> {
      return getVariableValue("latePayments", 0) as gw.api.database.IQueryBeanResult<AgencyCycleProcess>
    }
    
    property set latePayments ($arg :  gw.api.database.IQueryBeanResult<AgencyCycleProcess>) {
      setVariableValue("latePayments", 0, $arg)
    }
    
    property get latePromises () : gw.api.database.IQueryBeanResult<AgencyCycleProcess> {
      return getVariableValue("latePromises", 0) as gw.api.database.IQueryBeanResult<AgencyCycleProcess>
    }
    
    property set latePromises ($arg :  gw.api.database.IQueryBeanResult<AgencyCycleProcess>) {
      setVariableValue("latePromises", 0, $arg)
    }
    
    property get numLatePayments () : int {
      return getVariableValue("numLatePayments", 0) as java.lang.Integer
    }
    
    property set numLatePayments ($arg :  int) {
      setVariableValue("numLatePayments", 0, $arg)
    }
    
    property get numLatePromises () : int {
      return getVariableValue("numLatePromises", 0) as java.lang.Integer
    }
    
    property set numLatePromises ($arg :  int) {
      setVariableValue("numLatePromises", 0, $arg)
    }
    
    property get numSuspensePaymentItems () : int {
      return getVariableValue("numSuspensePaymentItems", 0) as java.lang.Integer
    }
    
    property set numSuspensePaymentItems ($arg :  int) {
      setVariableValue("numSuspensePaymentItems", 0, $arg)
    }
    
    property get numSuspensePromiseItems () : int {
      return getVariableValue("numSuspensePromiseItems", 0) as java.lang.Integer
    }
    
    property set numSuspensePromiseItems ($arg :  int) {
      setVariableValue("numSuspensePromiseItems", 0, $arg)
    }
    
    property get numUnappliedPayments () : int {
      return getVariableValue("numUnappliedPayments", 0) as java.lang.Integer
    }
    
    property set numUnappliedPayments ($arg :  int) {
      setVariableValue("numUnappliedPayments", 0, $arg)
    }
    
    property get producersWithPaymentExceptions () : gw.api.web.invoice.ProducerWithExceptionsView[] {
      return getVariableValue("producersWithPaymentExceptions", 0) as gw.api.web.invoice.ProducerWithExceptionsView[]
    }
    
    property set producersWithPaymentExceptions ($arg :  gw.api.web.invoice.ProducerWithExceptionsView[]) {
      setVariableValue("producersWithPaymentExceptions", 0, $arg)
    }
    
    property get producersWithPromiseExceptions () : gw.api.web.invoice.ProducerWithExceptionsView[] {
      return getVariableValue("producersWithPromiseExceptions", 0) as gw.api.web.invoice.ProducerWithExceptionsView[]
    }
    
    property set producersWithPromiseExceptions ($arg :  gw.api.web.invoice.ProducerWithExceptionsView[]) {
      setVariableValue("producersWithPromiseExceptions", 0, $arg)
    }
    
    property get suspensePaymentItems () : gw.api.database.IQueryBeanResult<AgencySuspPmntItem> {
      return getVariableValue("suspensePaymentItems", 0) as gw.api.database.IQueryBeanResult<AgencySuspPmntItem>
    }
    
    property set suspensePaymentItems ($arg :  gw.api.database.IQueryBeanResult<AgencySuspPmntItem>) {
      setVariableValue("suspensePaymentItems", 0, $arg)
    }
    
    property get suspensePromiseItems () : gw.api.database.IQueryBeanResult<AgencySuspPromiseItem> {
      return getVariableValue("suspensePromiseItems", 0) as gw.api.database.IQueryBeanResult<AgencySuspPromiseItem>
    }
    
    property set suspensePromiseItems ($arg :  gw.api.database.IQueryBeanResult<AgencySuspPromiseItem>) {
      setVariableValue("suspensePromiseItems", 0, $arg)
    }
    
    property get unappliedPayments () : gw.api.database.IQueryBeanResult<PaymentMoneyReceived> {
      return getVariableValue("unappliedPayments", 0) as gw.api.database.IQueryBeanResult<PaymentMoneyReceived>
    }
    
    property set unappliedPayments ($arg :  gw.api.database.IQueryBeanResult<PaymentMoneyReceived>) {
      setVariableValue("unappliedPayments", 0, $arg)
    }
    
    
                /**
                 * This function dismisses AgencyCycleExceptions.  It was moved here from Java code due to usage of Bundle.commit.
                 * It should not be used as a model for other Gosu function implementation.
                 */
                        function dismissPaymentPastDueExceptions(agencyCycleProcessWithExceptions: AgencyCycleProcess[]) {
                            for (cycleException in agencyCycleProcessWithExceptions) {
                                cycleException.PastDueExceptionDismissed = true
                            }
                            if (agencyCycleProcessWithExceptions.length > 0) {
                                agencyCycleProcessWithExceptions[0].Bundle.commit()
                            }
                        }
    
                        function dismissPromisePastDueExceptions(agencyCycleProcessWithExceptions: AgencyCycleProcess[]) {
                            for (cycleException in agencyCycleProcessWithExceptions) {
                                cycleException.PromiseExceptionDismissed = true
                            }
                            if (agencyCycleProcessWithExceptions.length > 0) {
                                agencyCycleProcessWithExceptions[0].Bundle.commit()
                            }
                        }
                
    
    
  }
  
  
}