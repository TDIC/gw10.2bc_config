package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_DistributionInfoPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyDistributionWizard_DistributionInfoPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_DistributionInfoPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyDistributionWizard_DistributionInfoPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'title' attribute on TitleBar (id=DistributionInfoHeader) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 15, column 59
    function title_1 () : java.lang.String {
      return wizardState.DistributionInfo
    }
    
    // 'visible' attribute on TitleBar (id=DistributionInfoHeader) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 15, column 59
    function visible_0 () : java.lang.Boolean {
      return wizardState.IsNewDistAndNotFromPromise
    }
    
    property get wizardState () : gw.agencybill.AgencyDistributionWizardHelper {
      return getRequireValue("wizardState", 0) as gw.agencybill.AgencyDistributionWizardHelper
    }
    
    property set wizardState ($arg :  gw.agencybill.AgencyDistributionWizardHelper) {
      setRequireValue("wizardState", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_DistributionInfoPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DistributionInfoDVExpressionsImpl extends AgencyDistributionWizard_DistributionInfoPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=MoneyAmount_Input) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 41, column 58
    function currency_10 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 21, column 42
    function initialValue_2 () : entity.AgencyCycleDist {
      return wizardState.AgencyCycleDistView.AgencyCycleDist
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 25, column 44
    function initialValue_3 () : entity.BaseMoneyReceived {
      return agencyCycleDist.BaseMoneyReceived
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 29, column 28
    function initialValue_4 () : Producer {
      return wizardState.MoneySetup.Producer
    }
    
    // 'label' attribute on Label (id=ThisDistributionHeader) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 70, column 191
    function label_32 () : java.lang.String {
      return DisplayKey.get("Web.AgencyDistributionWizard.DistributionInfoPanelSet.DistributionInfoDV.ThisDistributionHeader", wizardState.MoneySetup.DistributionTypeName)
    }
    
    // 'label' attribute on Label (id=DistributionInfoHeader) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 33, column 191
    function label_5 () : java.lang.String {
      return DisplayKey.get("Web.AgencyDistributionWizard.DistributionInfoPanelSet.DistributionInfoDV.DistributionInfoHeader", wizardState.MoneySetup.DistributionTypeName)
    }
    
    // 'label' attribute on MonetaryAmountInput (id=MoneyAmount_Input) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 41, column 58
    function label_7 () : java.lang.Object {
      return DisplayKey.get("Web.AgencyDistributionWizard.DistributionInfoPanelSet.DistributionInfoDV.MoneyAmount", wizardState.MoneySetup.DistributionTypeName)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CreditAmount_Input) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 49, column 57
    function valueRoot_16 () : java.lang.Object {
      return wizardState
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Distributed_Input) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 77, column 73
    function valueRoot_34 () : java.lang.Object {
      return agencyCycleDist
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MoneyAmount_Input) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 41, column 58
    function valueRoot_9 () : java.lang.Object {
      return money
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CreditAmount_Input) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 49, column 57
    function value_15 () : gw.pl.currency.MonetaryAmount {
      return wizardState.CreditAvailableForDistribution
    }
    
    // 'value' attribute on MonetaryAmountInput (id=OtherUnapplied_Input) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 57, column 61
    function value_21 () : gw.pl.currency.MonetaryAmount {
      return wizardState.OtherUnappliedAvailableForDistribution
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalAvailable_Input) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 65, column 61
    function value_27 () : gw.pl.currency.MonetaryAmount {
      return wizardState.TotalAmountAvailableForDistribution
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Distributed_Input) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 77, column 73
    function value_33 () : gw.pl.currency.MonetaryAmount {
      return agencyCycleDist.DistributedAmountForUnexecutedDist
    }
    
    // 'value' attribute on MonetaryAmountInput (id=InSuspense_Input) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 84, column 74
    function value_37 () : gw.pl.currency.MonetaryAmount {
      return agencyCycleDist.NetSuspenseAmountForSavedOrExecuted
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Remaining_Input) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 92, column 58
    function value_42 () : gw.pl.currency.MonetaryAmount {
      return wizardState.Remaining
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MoneyAmount_Input) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 41, column 58
    function value_8 () : gw.pl.currency.MonetaryAmount {
      return money.Amount
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=CreditAmount_Input) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 49, column 57
    function visible_14 () : java.lang.Boolean {
      return wizardState.IsCreditDistribution
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=OtherUnapplied_Input) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 57, column 61
    function visible_20 () : java.lang.Boolean {
      return !wizardState.IsNewCreditDistribution
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=MoneyAmount_Input) at AgencyDistributionWizard_DistributionInfoPanelSet.pcf: line 41, column 58
    function visible_6 () : java.lang.Boolean {
      return !wizardState.IsCreditDistribution
    }
    
    property get agencyCycleDist () : entity.AgencyCycleDist {
      return getVariableValue("agencyCycleDist", 1) as entity.AgencyCycleDist
    }
    
    property set agencyCycleDist ($arg :  entity.AgencyCycleDist) {
      setVariableValue("agencyCycleDist", 1, $arg)
    }
    
    property get money () : entity.BaseMoneyReceived {
      return getVariableValue("money", 1) as entity.BaseMoneyReceived
    }
    
    property set money ($arg :  entity.BaseMoneyReceived) {
      setVariableValue("money", 1, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 1) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 1, $arg)
    }
    
    
  }
  
  
}