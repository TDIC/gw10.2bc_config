package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerDetailDocuments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerDetailDocumentsExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerDetailDocuments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerDetailDocumentsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=ProducerDetailDocuments) at ProducerDetailDocuments.pcf: line 9, column 75
    static function canVisit_23 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.proddocview
    }
    
    // 'initialValue' attribute on Variable at ProducerDetailDocuments.pcf: line 18, column 52
    function initialValue_0 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // Page (id=ProducerDetailDocuments) at ProducerDetailDocuments.pcf: line 9, column 75
    static function parent_24 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    // 'visible' attribute on AlertBar (id=IDCSDisabledAlert) at ProducerDetailDocuments.pcf: line 24, column 68
    function visible_1 () : java.lang.Boolean {
      return not documentsActionsHelper.ContentSourceEnabled
    }
    
    // 'visible' attribute on AlertBar (id=IDCSUnavailableAlert) at ProducerDetailDocuments.pcf: line 28, column 72
    function visible_2 () : java.lang.Boolean {
      return documentsActionsHelper.ShowContentServerDownWarning
    }
    
    // 'visible' attribute on AlertBar (id=IDMSUnavailableAlert) at ProducerDetailDocuments.pcf: line 32, column 73
    function visible_3 () : java.lang.Boolean {
      return documentsActionsHelper.ShowMetadataServerDownWarning
    }
    
    override property get CurrentLocation () : pcf.ProducerDetailDocuments {
      return super.CurrentLocation as pcf.ProducerDetailDocuments
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerDetailDocuments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends ProducerDetailDocumentsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=RefreshAsyncContent) at ProducerDetailDocuments.pcf: line 86, column 110
    function action_18 () : void {
      
    }
    
    // 'available' attribute on ToolbarButton (id=AddDocuments) at ProducerDetailDocuments.pcf: line 68, column 46
    function available_13 () : java.lang.Boolean {
      return documentsActionsHelper.ContentSourceEnabled
    }
    
    // 'available' attribute on ToolbarButton (id=RefreshAsyncContent) at ProducerDetailDocuments.pcf: line 86, column 110
    function available_16 () : java.lang.Boolean {
      return documentsActionsHelper.DocumentContentServerAvailable
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=AccountDocuments_ObsolesceButton) at ProducerDetailDocuments.pcf: line 54, column 29
    function available_6 () : java.lang.Boolean {
      return documentsActionsHelper.DocumentMetadataActionsAvailable
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=AccountDocuments_DeobsolesceButton) at ProducerDetailDocuments.pcf: line 63, column 66
    function checkedRowAction_10 (element :  entity.Document, CheckedValue :  entity.Document) : void {
       CheckedValue.Obsolete = false
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=RemoveDocuments) at ProducerDetailDocuments.pcf: line 79, column 100
    function checkedRowAction_15 (element :  entity.Document, CheckedValue :  entity.Document) : void {
      gw.api.web.document.DocumentsHelper.deleteDocument(CheckedValue)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=AccountDocuments_ObsolesceButton) at ProducerDetailDocuments.pcf: line 54, column 29
    function checkedRowAction_7 (element :  entity.Document, CheckedValue :  entity.Document) : void {
       CheckedValue.Obsolete = true
    }
    
    // 'def' attribute on MenuItemSetRef at ProducerDetailDocuments.pcf: line 70, column 57
    function def_onEnter_11 (def :  pcf.NewDocumentMenuItemSet) : void {
      def.onEnter(producer)
    }
    
    // 'def' attribute on PanelRef at ProducerDetailDocuments.pcf: line 44, column 73
    function def_onEnter_19 (def :  pcf.DocumentsLV) : void {
      def.onEnter(documentList, documentSearchCriteria, true)
    }
    
    // 'def' attribute on PanelRef at ProducerDetailDocuments.pcf: line 42, column 59
    function def_onEnter_4 (def :  pcf.DocumentSearchDV) : void {
      def.onEnter(documentSearchCriteria)
    }
    
    // 'def' attribute on MenuItemSetRef at ProducerDetailDocuments.pcf: line 70, column 57
    function def_refreshVariables_12 (def :  pcf.NewDocumentMenuItemSet) : void {
      def.refreshVariables(producer)
    }
    
    // 'def' attribute on PanelRef at ProducerDetailDocuments.pcf: line 44, column 73
    function def_refreshVariables_20 (def :  pcf.DocumentsLV) : void {
      def.refreshVariables(documentList, documentSearchCriteria, true)
    }
    
    // 'def' attribute on PanelRef at ProducerDetailDocuments.pcf: line 42, column 59
    function def_refreshVariables_5 (def :  pcf.DocumentSearchDV) : void {
      def.refreshVariables(documentSearchCriteria)
    }
    
    // 'searchCriteria' attribute on SearchPanel at ProducerDetailDocuments.pcf: line 40, column 78
    function searchCriteria_22 () : entity.DocumentSearchCriteria {
      var c = new DocumentSearchCriteria(); c.IncludeObsoletes = false; c.Producer = producer; return c;
    }
    
    // 'search' attribute on SearchPanel at ProducerDetailDocuments.pcf: line 40, column 78
    function search_21 () : java.lang.Object {
      return documentSearchCriteria.performSearch(true) as gw.api.database.IQueryBeanResult<Document>
    }
    
    // 'visible' attribute on ToolbarButton (id=AddDocuments) at ProducerDetailDocuments.pcf: line 68, column 46
    function visible_14 () : java.lang.Boolean {
      return perm.Document.create
    }
    
    // 'visible' attribute on ToolbarButton (id=RefreshAsyncContent) at ProducerDetailDocuments.pcf: line 86, column 110
    function visible_17 () : java.lang.Boolean {
      return documentsActionsHelper.isShowAsynchronousRefreshAction(documentList.toTypedArray())
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=AccountDocuments_DeobsolesceButton) at ProducerDetailDocuments.pcf: line 63, column 66
    function visible_9 () : java.lang.Boolean {
      return documentSearchCriteria.IncludeObsoletes
    }
    
    property get documentList () : gw.api.database.IQueryBeanResult<Document> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<Document>
    }
    
    property get documentSearchCriteria () : entity.DocumentSearchCriteria {
      return getCriteriaValue(1) as entity.DocumentSearchCriteria
    }
    
    property set documentSearchCriteria ($arg :  entity.DocumentSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}