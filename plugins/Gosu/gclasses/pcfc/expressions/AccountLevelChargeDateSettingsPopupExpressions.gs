package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountLevelChargeDateSettingsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountLevelChargeDateSettingsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountLevelChargeDateSettingsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountLevelChargeDateSettingsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=BillDateOrDateBilling_Input) at AccountLevelChargeDateSettingsPopup.pcf: line 27, column 59
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.BillDateOrDueDateBilling = (__VALUE_TO_SET as typekey.BillDateOrDueDateBilling)
    }
    
    // 'value' attribute on TextInput (id=DayOfMonth_Input) at AccountLevelChargeDateSettingsPopup.pcf: line 35, column 44
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.InvoiceDayOfMonth = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // EditButtons at AccountLevelChargeDateSettingsPopup.pcf: line 17, column 23
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'validationExpression' attribute on TextInput (id=DayOfMonth_Input) at AccountLevelChargeDateSettingsPopup.pcf: line 35, column 44
    function validationExpression_5 () : java.lang.Object {
      return account.InvoiceDayOfMonth != null and account.InvoiceDayOfMonth > 0 and account.InvoiceDayOfMonth <= 31 ? null : DisplayKey.get("Java.Account.InvoiceDayOfMonth.ValidationError")
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=BillDateOrDateBilling_Input) at AccountLevelChargeDateSettingsPopup.pcf: line 27, column 59
    function valueRoot_3 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=BillDateOrDateBilling_Input) at AccountLevelChargeDateSettingsPopup.pcf: line 27, column 59
    function value_1 () : typekey.BillDateOrDueDateBilling {
      return account.BillDateOrDueDateBilling
    }
    
    // 'value' attribute on TextInput (id=DayOfMonth_Input) at AccountLevelChargeDateSettingsPopup.pcf: line 35, column 44
    function value_6 () : java.lang.Integer {
      return account.InvoiceDayOfMonth
    }
    
    override property get CurrentLocation () : pcf.AccountLevelChargeDateSettingsPopup {
      return super.CurrentLocation as pcf.AccountLevelChargeDateSettingsPopup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  
}