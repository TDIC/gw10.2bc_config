package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadAgencyPayLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DataUploadAgencyPayLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadAgencyPayLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DataUploadAgencyPayLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadAgencyPayLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=amount_Cell) at DataUploadAgencyPayLV.pcf: line 51, column 45
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.Amount")
    }
    
    // 'label' attribute on DateCell (id=receiveDate_Cell) at DataUploadAgencyPayLV.pcf: line 29, column 39
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.ReceivedDate")
    }
    
    // 'label' attribute on TextCell (id=producerName_Cell) at DataUploadAgencyPayLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.ProducerName")
    }
    
    // 'label' attribute on TextCell (id=reference_Cell) at DataUploadAgencyPayLV.pcf: line 40, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.Description")
    }
    
    // 'label' attribute on TypeKeyCell (id=payMethod_Cell) at DataUploadAgencyPayLV.pcf: line 45, column 46
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.PaymentMethod")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadAgencyPayLV.pcf: line 23, column 46
    function sortValue_1 (agencyPayment :  tdic.util.dataloader.data.sampledata.AgencyBillPayData) : java.lang.Object {
      return processor.getLoadStatus(agencyPayment)
    }
    
    // 'value' attribute on TypeKeyCell (id=payMethod_Cell) at DataUploadAgencyPayLV.pcf: line 45, column 46
    function sortValue_10 (agencyPayment :  tdic.util.dataloader.data.sampledata.AgencyBillPayData) : java.lang.Object {
      return agencyPayment.PaymentMethod
    }
    
    // 'value' attribute on TextCell (id=amount_Cell) at DataUploadAgencyPayLV.pcf: line 51, column 45
    function sortValue_12 (agencyPayment :  tdic.util.dataloader.data.sampledata.AgencyBillPayData) : java.lang.Object {
      return agencyPayment.Amount
    }
    
    // 'value' attribute on DateCell (id=receiveDate_Cell) at DataUploadAgencyPayLV.pcf: line 29, column 39
    function sortValue_4 (agencyPayment :  tdic.util.dataloader.data.sampledata.AgencyBillPayData) : java.lang.Object {
      return agencyPayment.EntryDate
    }
    
    // 'value' attribute on TextCell (id=producerName_Cell) at DataUploadAgencyPayLV.pcf: line 35, column 41
    function sortValue_6 (agencyPayment :  tdic.util.dataloader.data.sampledata.AgencyBillPayData) : java.lang.Object {
      return agencyPayment.Producer
    }
    
    // 'value' attribute on TextCell (id=reference_Cell) at DataUploadAgencyPayLV.pcf: line 40, column 41
    function sortValue_8 (agencyPayment :  tdic.util.dataloader.data.sampledata.AgencyBillPayData) : java.lang.Object {
      return agencyPayment.Description
    }
    
    // 'value' attribute on RowIterator (id=agencyPaymentID) at DataUploadAgencyPayLV.pcf: line 15, column 101
    function value_44 () : java.util.ArrayList<tdic.util.dataloader.data.sampledata.AgencyBillPayData> {
      return processor.AgencyBillPaymentArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadAgencyPayLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCSampleDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCSampleDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadAgencyPayLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DataUploadAgencyPayLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at DataUploadAgencyPayLV.pcf: line 17, column 106
    function highlighted_43 () : java.lang.Boolean {
      return (agencyPayment.Error or agencyPayment.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadAgencyPayLV.pcf: line 23, column 46
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on DateCell (id=receiveDate_Cell) at DataUploadAgencyPayLV.pcf: line 29, column 39
    function label_18 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.ReceivedDate")
    }
    
    // 'label' attribute on TextCell (id=producerName_Cell) at DataUploadAgencyPayLV.pcf: line 35, column 41
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.ProducerName")
    }
    
    // 'label' attribute on TextCell (id=reference_Cell) at DataUploadAgencyPayLV.pcf: line 40, column 41
    function label_28 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.Description")
    }
    
    // 'label' attribute on TypeKeyCell (id=payMethod_Cell) at DataUploadAgencyPayLV.pcf: line 45, column 46
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.PaymentMethod")
    }
    
    // 'label' attribute on TextCell (id=amount_Cell) at DataUploadAgencyPayLV.pcf: line 51, column 45
    function label_38 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Payment.Amount")
    }
    
    // 'value' attribute on DateCell (id=receiveDate_Cell) at DataUploadAgencyPayLV.pcf: line 29, column 39
    function valueRoot_20 () : java.lang.Object {
      return agencyPayment
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadAgencyPayLV.pcf: line 23, column 46
    function value_14 () : java.lang.String {
      return processor.getLoadStatus(agencyPayment)
    }
    
    // 'value' attribute on DateCell (id=receiveDate_Cell) at DataUploadAgencyPayLV.pcf: line 29, column 39
    function value_19 () : java.util.Date {
      return agencyPayment.EntryDate
    }
    
    // 'value' attribute on TextCell (id=producerName_Cell) at DataUploadAgencyPayLV.pcf: line 35, column 41
    function value_24 () : java.lang.String {
      return agencyPayment.Producer
    }
    
    // 'value' attribute on TextCell (id=reference_Cell) at DataUploadAgencyPayLV.pcf: line 40, column 41
    function value_29 () : java.lang.String {
      return agencyPayment.Description
    }
    
    // 'value' attribute on TypeKeyCell (id=payMethod_Cell) at DataUploadAgencyPayLV.pcf: line 45, column 46
    function value_34 () : typekey.PaymentMethod {
      return agencyPayment.PaymentMethod
    }
    
    // 'value' attribute on TextCell (id=amount_Cell) at DataUploadAgencyPayLV.pcf: line 51, column 45
    function value_39 () : java.math.BigDecimal {
      return agencyPayment.Amount
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadAgencyPayLV.pcf: line 23, column 46
    function visible_15 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get agencyPayment () : tdic.util.dataloader.data.sampledata.AgencyBillPayData {
      return getIteratedValue(1) as tdic.util.dataloader.data.sampledata.AgencyBillPayData
    }
    
    
  }
  
  
}