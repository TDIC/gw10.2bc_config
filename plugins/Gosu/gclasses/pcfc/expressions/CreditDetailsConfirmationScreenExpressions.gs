package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/CreditDetailsConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreditDetailsConfirmationScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/CreditDetailsConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreditDetailsConfirmationScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at CreditDetailsConfirmationScreen.pcf: line 34, column 51
    function def_onEnter_3 (def :  pcf.ApprovalActivityDV) : void {
      def.onEnter(approvalActivity)
    }
    
    // 'def' attribute on PanelRef at CreditDetailsConfirmationScreen.pcf: line 34, column 51
    function def_refreshVariables_4 (def :  pcf.ApprovalActivityDV) : void {
      def.refreshVariables(approvalActivity)
    }
    
    // 'initialValue' attribute on Variable at CreditDetailsConfirmationScreen.pcf: line 16, column 41
    function initialValue_0 () : entity.CreditApprActivity {
      return credit.getOpenApprovalActivity()
    }
    
    // 'label' attribute on Label at CreditDetailsConfirmationScreen.pcf: line 30, column 138
    function label_2 () : java.lang.String {
      return DisplayKey.get("Web.Credit.Confirmation", gw.api.financials.MonetaryAmounts.render(credit.Amount), account)
    }
    
    // 'visible' attribute on AlertBar (id=ApprovalActivityAlertBar) at CreditDetailsConfirmationScreen.pcf: line 23, column 43
    function visible_1 () : java.lang.Boolean {
      return approvalActivity != null
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get approvalActivity () : entity.CreditApprActivity {
      return getVariableValue("approvalActivity", 0) as entity.CreditApprActivity
    }
    
    property set approvalActivity ($arg :  entity.CreditApprActivity) {
      setVariableValue("approvalActivity", 0, $arg)
    }
    
    property get credit () : Credit {
      return getRequireValue("credit", 0) as Credit
    }
    
    property set credit ($arg :  Credit) {
      setRequireValue("credit", 0, $arg)
    }
    
    
  }
  
  
}