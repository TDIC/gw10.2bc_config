package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/collection/CollectionAgencyAccountsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CollectionAgencyAccountsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/collection/CollectionAgencyAccountsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CollectionAgencyAccountsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=accountnumber_Cell) at CollectionAgencyAccountsLV.pcf: line 24, column 25
    function sortValue_0 (account :  entity.Account) : java.lang.Object {
      return account.AccountNumber
    }
    
    // 'sortBy' attribute on TextCell (id=accountName_Cell) at CollectionAgencyAccountsLV.pcf: line 29, column 49
    function sortValue_1 (account :  entity.Account) : java.lang.Object {
      return account.AccountName
    }
    
    // 'value' attribute on TypeKeyCell (id=accountSegment_Cell) at CollectionAgencyAccountsLV.pcf: line 34, column 47
    function sortValue_2 (account :  entity.Account) : java.lang.Object {
      return account.Segment
    }
    
    // 'value' attribute on DateCell (id=accountCreationDate_Cell) at CollectionAgencyAccountsLV.pcf: line 38, column 39
    function sortValue_3 (account :  entity.Account) : java.lang.Object {
      return account.CreateTime
    }
    
    // 'value' attribute on TypeKeyCell (id=accountDelinquencyStatus_Cell) at CollectionAgencyAccountsLV.pcf: line 43, column 50
    function sortValue_4 (account :  entity.Account) : java.lang.Object {
      return account.DelinquencyStatus
    }
    
    // 'sortBy' attribute on TextCell (id=collecting_Cell) at CollectionAgencyAccountsLV.pcf: line 48, column 74
    function sortValue_5 (account :  entity.Account) : java.lang.Object {
      return account.Collecting
    }
    
    // 'value' attribute on RowIterator at CollectionAgencyAccountsLV.pcf: line 16, column 74
    function value_25 () : gw.api.database.IQueryBeanResult<entity.Account> {
      return gw.api.database.Query.make(Account).compare("CollectionAgency", Equals, collectionAgency).select()
    }
    
    property get collectionAgency () : CollectionAgency {
      return getRequireValue("collectionAgency", 0) as CollectionAgency
    }
    
    property set collectionAgency ($arg :  CollectionAgency) {
      setRequireValue("collectionAgency", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/collection/CollectionAgencyAccountsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CollectionAgencyAccountsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=accountnumber_Cell) at CollectionAgencyAccountsLV.pcf: line 24, column 25
    function action_6 () : void {
      AccountOverview.go(account)
    }
    
    // 'action' attribute on TextCell (id=accountnumber_Cell) at CollectionAgencyAccountsLV.pcf: line 24, column 25
    function action_dest_7 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(account)
    }
    
    // 'value' attribute on TextCell (id=accountnumber_Cell) at CollectionAgencyAccountsLV.pcf: line 24, column 25
    function valueRoot_9 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on TextCell (id=accountName_Cell) at CollectionAgencyAccountsLV.pcf: line 29, column 49
    function value_11 () : java.lang.String {
      return account.AccountNameLocalized
    }
    
    // 'value' attribute on TypeKeyCell (id=accountSegment_Cell) at CollectionAgencyAccountsLV.pcf: line 34, column 47
    function value_14 () : typekey.AccountSegment {
      return account.Segment
    }
    
    // 'value' attribute on DateCell (id=accountCreationDate_Cell) at CollectionAgencyAccountsLV.pcf: line 38, column 39
    function value_17 () : java.util.Date {
      return account.CreateTime
    }
    
    // 'value' attribute on TypeKeyCell (id=accountDelinquencyStatus_Cell) at CollectionAgencyAccountsLV.pcf: line 43, column 50
    function value_20 () : typekey.DelinquencyStatus {
      return account.DelinquencyStatus
    }
    
    // 'value' attribute on TextCell (id=collecting_Cell) at CollectionAgencyAccountsLV.pcf: line 48, column 74
    function value_23 () : java.lang.String {
      return account.Collecting ? "Yes" : "No"
    }
    
    // 'value' attribute on TextCell (id=accountnumber_Cell) at CollectionAgencyAccountsLV.pcf: line 24, column 25
    function value_8 () : java.lang.String {
      return account.AccountNumber
    }
    
    property get account () : entity.Account {
      return getIteratedValue(1) as entity.Account
    }
    
    
  }
  
  
}