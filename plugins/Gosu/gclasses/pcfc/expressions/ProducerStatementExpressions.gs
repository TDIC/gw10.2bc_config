package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.financials.MonetaryAmounts
uses gw.pl.currency.MonetaryAmount
uses java.util.Map
@javax.annotation.Generated("config/web/pcf/producer/ProducerStatement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerStatementExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerStatement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends ProducerStatementScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=policyNumber_Cell) at ProducerStatement.pcf: line 115, column 31
    function action_26 () : void {
      ProducerStatementPopup.push(event.get("PolicyNumber") as String, producerStatement)
    }
    
    // 'action' attribute on TextCell (id=policyNumber_Cell) at ProducerStatement.pcf: line 115, column 31
    function action_dest_27 () : pcf.api.Destination {
      return pcf.ProducerStatementPopup.createDestination(event.get("PolicyNumber") as String, producerStatement)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=commissionAmount_Cell) at ProducerStatement.pcf: line 129, column 53
    function currency_33 () : typekey.Currency {
      return producerStatement.Currency
    }
    
    // 'value' attribute on TextCell (id=policyNumber_Cell) at ProducerStatement.pcf: line 115, column 31
    function value_28 () : java.lang.Object {
      return event.get("PolicyNumber")
    }
    
    // 'value' attribute on TextCell (id=NumberEarned_Cell) at ProducerStatement.pcf: line 121, column 47
    function value_30 () : java.lang.Object {
      return event.get("NumberEarned")
    }
    
    // 'value' attribute on MonetaryAmountCell (id=commissionAmount_Cell) at ProducerStatement.pcf: line 129, column 53
    function value_32 () : gw.pl.currency.MonetaryAmount {
      return getCommissionAmount(event)
    }
    
    property get event () : java.util.Map<java.lang.String, java.lang.Object> {
      return getIteratedValue(2) as java.util.Map<java.lang.String, java.lang.Object>
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerStatement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends ProducerStatementScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=PreUpgradePolicyActivityLV_Amount_Cell) at ProducerStatement.pcf: line 214, column 60
    function currency_72 () : typekey.Currency {
      return producerStatement.Currency
    }
    
    // 'value' attribute on DateCell (id=PreUpgradePolicyActivityLV_Date_Cell) at ProducerStatement.pcf: line 176, column 62
    function valueRoot_50 () : java.lang.Object {
      return producerTransaction
    }
    
    // 'value' attribute on TextCell (id=PreUpgradePolicyActivityLV_PolicyNumber_Cell) at ProducerStatement.pcf: line 181, column 89
    function valueRoot_53 () : java.lang.Object {
      return producerTransaction.PolicyCommission.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=PreUpgradePolicyActivityLV_ProducerCode_Cell) at ProducerStatement.pcf: line 200, column 64
    function valueRoot_65 () : java.lang.Object {
      return producerTransaction.ProducerCode
    }
    
    // 'value' attribute on TextCell (id=PreUpgradePolicyActivityLV_Charge_Cell) at ProducerStatement.pcf: line 206, column 44
    function valueRoot_68 () : java.lang.Object {
      return producerTransaction.ChargeCommission
    }
    
    // 'value' attribute on DateCell (id=PreUpgradePolicyActivityLV_Date_Cell) at ProducerStatement.pcf: line 176, column 62
    function value_49 () : java.util.Date {
      return producerTransaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=PreUpgradePolicyActivityLV_PolicyNumber_Cell) at ProducerStatement.pcf: line 181, column 89
    function value_52 () : java.lang.String {
      return producerTransaction.PolicyCommission.PolicyPeriod.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=PreUpgradePolicyActivityLV_Number_Cell) at ProducerStatement.pcf: line 185, column 64
    function value_55 () : java.lang.String {
      return producerTransaction.TransactionNumber
    }
    
    // 'value' attribute on DateCell (id=PreUpgradePolicyActivityLV_EffectiveDate_Cell) at ProducerStatement.pcf: line 190, column 93
    function value_58 () : java.util.Date {
      return producerTransaction.PolicyCommission.PolicyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on TextCell (id=PreUpgradePolicyActivityLV_Context_Cell) at ProducerStatement.pcf: line 195, column 63
    function value_61 () : java.lang.String {
      return producerTransaction.ShortDisplayName
    }
    
    // 'value' attribute on TextCell (id=PreUpgradePolicyActivityLV_ProducerCode_Cell) at ProducerStatement.pcf: line 200, column 64
    function value_64 () : java.lang.String {
      return producerTransaction.ProducerCode.Code
    }
    
    // 'value' attribute on TextCell (id=PreUpgradePolicyActivityLV_Charge_Cell) at ProducerStatement.pcf: line 206, column 44
    function value_67 () : entity.Charge {
      return producerTransaction.ChargeCommission.Charge
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PreUpgradePolicyActivityLV_Amount_Cell) at ProducerStatement.pcf: line 214, column 60
    function value_70 () : gw.pl.currency.MonetaryAmount {
      return producerTransaction.PayableAmount
    }
    
    property get producerTransaction () : ProducerTransaction {
      return getIteratedValue(2) as ProducerTransaction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerStatement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends ProducerStatementScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=OtherActivityLV_TransactionNumber_Cell) at ProducerStatement.pcf: line 243, column 64
    function action_83 () : void {
      TransactionDetailPopup.push(producerTransaction)
    }
    
    // 'action' attribute on TextCell (id=OtherActivityLV_TransactionNumber_Cell) at ProducerStatement.pcf: line 243, column 64
    function action_dest_84 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(producerTransaction)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=OtherActivityLV_Amount_Cell) at ProducerStatement.pcf: line 256, column 60
    function currency_93 () : typekey.Currency {
      return producerStatement.Currency
    }
    
    // 'value' attribute on DateCell (id=OtherActivityLV_Date_Cell) at ProducerStatement.pcf: line 237, column 62
    function valueRoot_81 () : java.lang.Object {
      return producerTransaction
    }
    
    // 'value' attribute on DateCell (id=OtherActivityLV_Date_Cell) at ProducerStatement.pcf: line 237, column 62
    function value_80 () : java.util.Date {
      return producerTransaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=OtherActivityLV_TransactionNumber_Cell) at ProducerStatement.pcf: line 243, column 64
    function value_85 () : java.lang.String {
      return producerTransaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=OtherActivityLV_Context_Cell) at ProducerStatement.pcf: line 248, column 63
    function value_88 () : java.lang.String {
      return producerTransaction.ShortDisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OtherActivityLV_Amount_Cell) at ProducerStatement.pcf: line 256, column 60
    function value_91 () : gw.pl.currency.MonetaryAmount {
      return producerTransaction.PayableAmount
    }
    
    property get producerTransaction () : ProducerTransaction {
      return getIteratedValue(2) as ProducerTransaction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerStatement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry5ExpressionsImpl extends ProducerStatementScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=PayableTransferActivityLV_TransactionNumber_Cell) at ProducerStatement.pcf: line 285, column 71
    function action_102 () : void {
      TransactionDetailPopup.push(payableTransferTransaction)
    }
    
    // 'action' attribute on TextCell (id=PayableTransferActivityLV_TransactionNumber_Cell) at ProducerStatement.pcf: line 285, column 71
    function action_dest_103 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(payableTransferTransaction)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=PayableTransferActivityLV_Amount_Cell) at ProducerStatement.pcf: line 298, column 99
    function currency_111 () : typekey.Currency {
      return producerStatement.Currency
    }
    
    // 'value' attribute on DateCell (id=PayableTransferActivityLV_Date_Cell) at ProducerStatement.pcf: line 279, column 69
    function valueRoot_100 () : java.lang.Object {
      return payableTransferTransaction
    }
    
    // 'value' attribute on TextCell (id=PayableTransferActivityLV_TransactionNumber_Cell) at ProducerStatement.pcf: line 285, column 71
    function value_104 () : java.lang.String {
      return payableTransferTransaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=PayableTransferActivityLV_Context_Cell) at ProducerStatement.pcf: line 290, column 70
    function value_107 () : java.lang.String {
      return payableTransferTransaction.ShortDisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PayableTransferActivityLV_Amount_Cell) at ProducerStatement.pcf: line 298, column 99
    function value_110 () : gw.pl.currency.MonetaryAmount {
      return payableTransferTransaction.getTransferAmount(producerStatement.Producer)
    }
    
    // 'value' attribute on DateCell (id=PayableTransferActivityLV_Date_Cell) at ProducerStatement.pcf: line 279, column 69
    function value_99 () : java.util.Date {
      return payableTransferTransaction.TransactionDate
    }
    
    property get payableTransferTransaction () : entity.ProducerPayableTransferTxn {
      return getIteratedValue(2) as entity.ProducerPayableTransferTxn
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerStatement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry6ExpressionsImpl extends ProducerStatementScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=PaymentActivityLV_TransactionNumber_Cell) at ProducerStatement.pcf: line 326, column 64
    function action_125 () : void {
      TransactionDetailPopup.push(producerTransaction)
    }
    
    // 'action' attribute on TextCell (id=PaymentActivityLV_PolicyNumber_Cell) at ProducerStatement.pcf: line 332, column 89
    function action_130 () : void {
      PolicyOverview.push(producerTransaction.PolicyCommission.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=PaymentActivityLV_CheckRefNumber_Cell) at ProducerStatement.pcf: line 343, column 80
    function action_138 () : void {
      PaymentDetailForward.push(producerTransaction.PaymentReceipt)
    }
    
    // 'action' attribute on TextCell (id=PaymentActivityLV_TransactionNumber_Cell) at ProducerStatement.pcf: line 326, column 64
    function action_dest_126 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(producerTransaction)
    }
    
    // 'action' attribute on TextCell (id=PaymentActivityLV_PolicyNumber_Cell) at ProducerStatement.pcf: line 332, column 89
    function action_dest_131 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(producerTransaction.PolicyCommission.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=PaymentActivityLV_CheckRefNumber_Cell) at ProducerStatement.pcf: line 343, column 80
    function action_dest_139 () : pcf.api.Destination {
      return pcf.PaymentDetailForward.createDestination(producerTransaction.PaymentReceipt)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=PaymentActivityLV_FromAgent_Cell) at ProducerStatement.pcf: line 358, column 65
    function currency_147 () : typekey.Currency {
      return producerStatement.Currency
    }
    
    // 'value' attribute on DateCell (id=PaymentActivityLV_Date_Cell) at ProducerStatement.pcf: line 320, column 62
    function valueRoot_123 () : java.lang.Object {
      return producerTransaction
    }
    
    // 'value' attribute on TextCell (id=PaymentActivityLV_PolicyNumber_Cell) at ProducerStatement.pcf: line 332, column 89
    function valueRoot_133 () : java.lang.Object {
      return producerTransaction.PolicyCommission.PolicyPeriod
    }
    
    // 'value' attribute on DateCell (id=PaymentActivityLV_Date_Cell) at ProducerStatement.pcf: line 320, column 62
    function value_122 () : java.util.Date {
      return producerTransaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=PaymentActivityLV_TransactionNumber_Cell) at ProducerStatement.pcf: line 326, column 64
    function value_127 () : java.lang.String {
      return producerTransaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=PaymentActivityLV_PolicyNumber_Cell) at ProducerStatement.pcf: line 332, column 89
    function value_132 () : java.lang.String {
      return producerTransaction.PolicyCommission.PolicyPeriod.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=PaymentActivityLV_Context_Cell) at ProducerStatement.pcf: line 337, column 63
    function value_135 () : java.lang.String {
      return producerTransaction.ShortDisplayName
    }
    
    // 'value' attribute on TextCell (id=PaymentActivityLV_CheckRefNumber_Cell) at ProducerStatement.pcf: line 343, column 80
    function value_140 () : java.lang.String {
      return getCheckRefNumber(producerTransaction.PaymentReceipt)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PaymentActivityLV_ToAgent_Cell) at ProducerStatement.pcf: line 350, column 63
    function value_142 () : gw.pl.currency.MonetaryAmount {
      return producerTransaction.ToProducerAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PaymentActivityLV_FromAgent_Cell) at ProducerStatement.pcf: line 358, column 65
    function value_145 () : gw.pl.currency.MonetaryAmount {
      return producerTransaction.FromProducerAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PaymentActivityLV_Payment_Cell) at ProducerStatement.pcf: line 366, column 57
    function value_149 () : gw.pl.currency.MonetaryAmount {
      return producerTransaction.PaidAmount
    }
    
    property get producerTransaction () : entity.ProducerPaymentTxn {
      return getIteratedValue(2) as entity.ProducerPaymentTxn
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerStatement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ProducerStatementScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at ProducerStatement.pcf: line 71, column 52
    function currency_17 () : typekey.Currency {
      return producerStatement.Currency
    }
    
    // 'value' attribute on TextCell (id=Item_Cell) at ProducerStatement.pcf: line 64, column 50
    function valueRoot_13 () : java.lang.Object {
      return summaryEntry
    }
    
    // 'value' attribute on TextCell (id=Item_Cell) at ProducerStatement.pcf: line 64, column 50
    function value_12 () : java.lang.String {
      return summaryEntry.Item
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at ProducerStatement.pcf: line 71, column 52
    function value_15 () : gw.pl.currency.MonetaryAmount {
      return summaryEntry.Amount
    }
    
    property get summaryEntry () : gw.api.domain.producer.ProducerStatementSummaryEntry {
      return getIteratedValue(2) as gw.api.domain.producer.ProducerStatementSummaryEntry
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerStatement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerStatementExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producerStatement :  ProducerStatement) : int {
      return 0
    }
    
    // 'initialValue' attribute on Variable at ProducerStatement.pcf: line 18, column 22
    function initialValue_0 () : String {
      return null
    }
    
    // 'parent' attribute on Page (id=ProducerStatement) at ProducerStatement.pcf: line 9, column 69
    static function parent_154 (producerStatement :  ProducerStatement) : pcf.api.Destination {
      return pcf.ProducerStatementOverview.createDestination(producerStatement.Producer)
    }
    
    override property get CurrentLocation () : pcf.ProducerStatement {
      return super.CurrentLocation as pcf.ProducerStatement
    }
    
    property get policyNumber () : String {
      return getVariableValue("policyNumber", 0) as String
    }
    
    property set policyNumber ($arg :  String) {
      setVariableValue("policyNumber", 0, $arg)
    }
    
    property get producerStatement () : ProducerStatement {
      return getVariableValue("producerStatement", 0) as ProducerStatement
    }
    
    property set producerStatement ($arg :  ProducerStatement) {
      setVariableValue("producerStatement", 0, $arg)
    }
    
    
    function getCheckRefNumber(paymentReceipt : PaymentReceipt) : String {
            if (paymentReceipt == null) {
              return null;
            } else if (paymentReceipt.RefNumber == null) {
              return DisplayKey.get("Web.ProducerTransactions.Payments.Pending");
            } else {
              return paymentReceipt.RefNumber;
            }
          }
     function getCommissionAmount(evt : Map<String, Object>) : MonetaryAmount {
       var commissionAmount = evt.get("CommissionAmount")
       return ((commissionAmount == null ? 0 : commissionAmount) as java.math.BigDecimal).ofCurrency(producerStatement.Currency)
     }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerStatement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerStatementScreenExpressionsImpl extends ProducerStatementExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=PolicyFilterGoButton) at ProducerStatement.pcf: line 94, column 93
    function action_23 () : void {
      evt = (policyNumber != null) ? producerStatement.getItemEvents(policyNumber, true) : evt
    }
    
    // 'action' attribute on ToolbarButton (id=PolicyFilterClearButton) at ProducerStatement.pcf: line 98, column 96
    function action_24 () : void {
      policyNumber = null; evt = producerStatement.getItemEvents(null, false)
    }
    
    // 'action' attribute on ToolbarButton (id=resend) at ProducerStatement.pcf: line 35, column 75
    function action_4 () : void {
      producerStatement.resendStatement()
    }
    
    // 'available' attribute on ToolbarButton (id=resend) at ProducerStatement.pcf: line 35, column 75
    function available_3 () : java.lang.Boolean {
      return perm.Producer.proddbstmtresent and not producerStatement.HasPossiblyArchivedPolicies
    }
    
    // 'def' attribute on PanelRef at ProducerStatement.pcf: line 42, column 54
    function def_onEnter_6 (def :  pcf.ProducerStatementDV) : void {
      def.onEnter(producerStatement)
    }
    
    // 'def' attribute on PanelRef at ProducerStatement.pcf: line 42, column 54
    function def_refreshVariables_7 (def :  pcf.ProducerStatementDV) : void {
      def.refreshVariables(producerStatement)
    }
    
    // 'value' attribute on TextValue (id=PolicyPeriodFilter) at ProducerStatement.pcf: line 89, column 43
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at ProducerStatement.pcf: line 25, column 73
    function initialValue_1 () : java.util.List<entity.ProducerPayableTransferTxn> {
      return producerStatement.findProducerPayableTransferTransactions()
    }
    
    // 'initialValue' attribute on Variable at ProducerStatement.pcf: line 29, column 95
    function initialValue_2 () : java.util.List<java.util.Map<java.lang.String, java.lang.Object>> {
      return producerStatement.getItemEvents(null, false)
    }
    
    // 'value' attribute on DateCell (id=PaymentActivityLV_Date_Cell) at ProducerStatement.pcf: line 320, column 62
    function sortValue_115 (producerTransaction :  entity.ProducerPaymentTxn) : java.lang.Object {
      return producerTransaction.TransactionDate
    }
    
    // 'value' attribute on DateCell (id=PreUpgradePolicyActivityLV_Date_Cell) at ProducerStatement.pcf: line 176, column 62
    function sortValue_45 (producerTransaction :  ProducerTransaction) : java.lang.Object {
      return producerTransaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=PreUpgradePolicyActivityLV_Number_Cell) at ProducerStatement.pcf: line 185, column 64
    function sortValue_46 (producerTransaction :  ProducerTransaction) : java.lang.Object {
      return producerTransaction.TransactionNumber
    }
    
    // 'value' attribute on DateCell (id=OtherActivityLV_Date_Cell) at ProducerStatement.pcf: line 237, column 62
    function sortValue_77 (producerTransaction :  ProducerTransaction) : java.lang.Object {
      return producerTransaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=Item_Cell) at ProducerStatement.pcf: line 64, column 50
    function sortValue_8 (summaryEntry :  gw.api.domain.producer.ProducerStatementSummaryEntry) : java.lang.Object {
      return summaryEntry.Item
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at ProducerStatement.pcf: line 71, column 52
    function sortValue_9 (summaryEntry :  gw.api.domain.producer.ProducerStatementSummaryEntry) : java.lang.Object {
      return summaryEntry.Amount
    }
    
    // 'value' attribute on DateCell (id=PayableTransferActivityLV_Date_Cell) at ProducerStatement.pcf: line 279, column 69
    function sortValue_97 (payableTransferTransaction :  entity.ProducerPayableTransferTxn) : java.lang.Object {
      return payableTransferTransaction.TransactionDate
    }
    
    // '$$sumValue' attribute on RowIterator at ProducerStatement.pcf: line 71, column 52
    function sumValueRoot_11 (summaryEntry :  gw.api.domain.producer.ProducerStatementSummaryEntry) : java.lang.Object {
      return summaryEntry
    }
    
    // '$$sumValue' attribute on RowIterator at ProducerStatement.pcf: line 350, column 63
    function sumValueRoot_117 (producerTransaction :  entity.ProducerPaymentTxn) : java.lang.Object {
      return producerTransaction
    }
    
    // '$$sumValue' attribute on RowIterator at ProducerStatement.pcf: line 214, column 60
    function sumValueRoot_48 (producerTransaction :  ProducerTransaction) : java.lang.Object {
      return producerTransaction
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerStatement.pcf: line 71, column 52
    function sumValue_10 (summaryEntry :  gw.api.domain.producer.ProducerStatementSummaryEntry) : java.lang.Object {
      return summaryEntry.Amount
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerStatement.pcf: line 350, column 63
    function sumValue_116 (producerTransaction :  entity.ProducerPaymentTxn) : java.lang.Object {
      return producerTransaction.ToProducerAmount
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerStatement.pcf: line 358, column 65
    function sumValue_118 (producerTransaction :  entity.ProducerPaymentTxn) : java.lang.Object {
      return producerTransaction.FromProducerAmount
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerStatement.pcf: line 366, column 57
    function sumValue_120 (producerTransaction :  entity.ProducerPaymentTxn) : java.lang.Object {
      return producerTransaction.PaidAmount
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerStatement.pcf: line 129, column 53
    function sumValue_25 (event :  java.util.Map<java.lang.String, java.lang.Object>) : java.lang.Object {
      return getCommissionAmount(event)
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerStatement.pcf: line 214, column 60
    function sumValue_47 (producerTransaction :  ProducerTransaction) : java.lang.Object {
      return producerTransaction.PayableAmount
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerStatement.pcf: line 298, column 99
    function sumValue_98 (payableTransferTransaction :  entity.ProducerPayableTransferTxn) : java.lang.Object {
      return payableTransferTransaction.getTransferAmount(producerStatement.Producer)
    }
    
    // 'value' attribute on TextInput (id=NumberOfReversals_Input) at ProducerStatement.pcf: line 145, column 46
    function valueRoot_38 () : java.lang.Object {
      return producerStatement
    }
    
    // 'value' attribute on RowIterator at ProducerStatement.pcf: line 313, column 91
    function value_153 () : gw.api.database.IQueryBeanResult<entity.ProducerPaymentTxn> {
      return producerStatement.findProducerPaymentTransactions()
    }
    
    // 'value' attribute on RowIterator at ProducerStatement.pcf: line 58, column 86
    function value_19 () : gw.api.domain.producer.ProducerStatementSummaryEntry[] {
      return producerStatement.Summary
    }
    
    // 'value' attribute on TextValue (id=PolicyPeriodFilter) at ProducerStatement.pcf: line 89, column 43
    function value_20 () : java.lang.String {
      return policyNumber
    }
    
    // 'value' attribute on RowIterator at ProducerStatement.pcf: line 106, column 103
    function value_35 () : java.util.List<java.util.Map<java.lang.String, java.lang.Object>> {
      return evt
    }
    
    // 'value' attribute on TextInput (id=NumberOfReversals_Input) at ProducerStatement.pcf: line 145, column 46
    function value_37 () : java.lang.Integer {
      return producerStatement.NumberOfReversalTransactionsWithoutItemEvents
    }
    
    // 'value' attribute on TextInput (id=NumberOfItemEvents_Input) at ProducerStatement.pcf: line 150, column 46
    function value_40 () : java.lang.Integer {
      return producerStatement.NumberOfItemEvents
    }
    
    // 'value' attribute on RowIterator at ProducerStatement.pcf: line 170, column 161
    function value_74 () : gw.api.database.QueryBeanResultWithSummary<entity.ReserveCmsnEarned, java.util.Map<java.lang.String, java.math.BigDecimal>> {
      return producerStatement.TransactionsWithoutItemEvents
    }
    
    // 'value' attribute on RowIterator at ProducerStatement.pcf: line 230, column 89
    function value_95 () : gw.api.database.IQueryBeanResult<entity.CommissionEarned> {
      return producerStatement.findProducerCodeAndProducerLevelPayableTransactions()
    }
    
    // 'type' attribute on RowIterator at ProducerStatement.pcf: line 170, column 161
    function verifyIteratorType_75 () : void {
      var entry : entity.ReserveCmsnEarned = null
      var typedEntry : ProducerTransaction
      // If this cast fails to compile then the type specified by the 'type' attribute on
      // 'RowIterator' is not compatible with the member type of the 'valueType' attribute
      typedEntry = entry as ProducerTransaction
    }
    
    // 'type' attribute on RowIterator at ProducerStatement.pcf: line 230, column 89
    function verifyIteratorType_96 () : void {
      var entry : entity.CommissionEarned = null
      var typedEntry : ProducerTransaction
      // If this cast fails to compile then the type specified by the 'type' attribute on
      // 'RowIterator' is not compatible with the member type of the 'valueType' attribute
      typedEntry = entry as ProducerTransaction
    }
    
    // 'visible' attribute on PanelRef at ProducerStatement.pcf: line 262, column 59
    function visible_114 () : java.lang.Boolean {
      return payableTransferTransactions.HasElements
    }
    
    // 'visible' attribute on PanelRef at ProducerStatement.pcf: line 80, column 81
    function visible_36 () : java.lang.Boolean {
      return producerStatement.Type == ProducerStatementType.TC_PRODUCTION
    }
    
    // 'visible' attribute on PanelRef at ProducerStatement.pcf: line 135, column 78
    function visible_43 () : java.lang.Boolean {
      return producerStatement.Type == ProducerStatementType.TC_UPGRADE
    }
    
    // 'visible' attribute on AlertBar (id=ArchiveWarningAlertBar) at ProducerStatement.pcf: line 40, column 66
    function visible_5 () : java.lang.Boolean {
      return producerStatement.HasPossiblyArchivedPolicies
    }
    
    // 'visible' attribute on PanelRef at ProducerStatement.pcf: line 159, column 81
    function visible_76 () : java.lang.Boolean {
      return producerStatement.Type == ProducerStatementType.TC_PREUPGRADE
    }
    
    property get evt () : java.util.List<java.util.Map<java.lang.String, java.lang.Object>> {
      return getVariableValue("evt", 1) as java.util.List<java.util.Map<java.lang.String, java.lang.Object>>
    }
    
    property set evt ($arg :  java.util.List<java.util.Map<java.lang.String, java.lang.Object>>) {
      setVariableValue("evt", 1, $arg)
    }
    
    property get payableTransferTransactions () : java.util.List<entity.ProducerPayableTransferTxn> {
      return getVariableValue("payableTransferTransactions", 1) as java.util.List<entity.ProducerPayableTransferTxn>
    }
    
    property set payableTransferTransactions ($arg :  java.util.List<entity.ProducerPayableTransferTxn>) {
      setVariableValue("payableTransferTransactions", 1, $arg)
    }
    
    
  }
  
  
}