package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/accounting/LedgerScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class LedgerScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/accounting/LedgerScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends ListDetailPanel2ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Debit_Cell) at LedgerScreen.pcf: line 94, column 116
    function currency_21 () : typekey.Currency {
      return LineItem.Currency
    }
    
    // 'value' attribute on DateCell (id=TransactionDate_Cell) at LedgerScreen.pcf: line 82, column 73
    function valueRoot_15 () : java.lang.Object {
      return LineItem.Transaction
    }
    
    // 'value' attribute on DateCell (id=TransactionDate_Cell) at LedgerScreen.pcf: line 82, column 73
    function value_14 () : java.util.Date {
      return LineItem.Transaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at LedgerScreen.pcf: line 87, column 73
    function value_17 () : java.lang.String {
      return LineItem.Transaction.LongDisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Debit_Cell) at LedgerScreen.pcf: line 94, column 116
    function value_20 () : gw.pl.currency.MonetaryAmount {
      return LineItem.Type == TC_DEBIT ? LineItem.Amount : 0bd.ofCurrency(LineItem.Currency)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Credit_Cell) at LedgerScreen.pcf: line 101, column 117
    function value_23 () : gw.pl.currency.MonetaryAmount {
      return LineItem.Type == TC_CREDIT ? LineItem.Amount : 0bd.ofCurrency(LineItem.Currency)
    }
    
    property get LineItem () : entity.LineItem {
      return getIteratedValue(3) as entity.LineItem
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/accounting/LedgerScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ListDetailPanel2ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Balance_Cell) at LedgerScreen.pcf: line 55, column 49
    function currency_10 () : typekey.Currency {
      return tAccount.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Balance_Cell) at LedgerScreen.pcf: line 55, column 49
    function valueRoot_9 () : java.lang.Object {
      return tAccount
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at LedgerScreen.pcf: line 49, column 52
    function value_6 () : entity.TAccount {
      return tAccount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Balance_Cell) at LedgerScreen.pcf: line 55, column 49
    function value_8 () : gw.pl.currency.MonetaryAmount {
      return tAccount.Balance
    }
    
    property get tAccount () : entity.TAccount {
      return getIteratedValue(3) as entity.TAccount
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/accounting/LedgerScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LedgerScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AlertBar (id=PolicyArchivedAlert) at LedgerScreen.pcf: line 17, column 86
    function label_1 () : java.lang.Object {
      return DisplayKey.get("Web.Archive.PolicyPeriod.ArchivedAlertBar", (tAccountOwner as PolicyPeriod).ArchiveDate.AsUIStyle)
    }
    
    // 'visible' attribute on AlertBar (id=PolicyArchivedAlert) at LedgerScreen.pcf: line 17, column 86
    function visible_0 () : java.lang.Boolean {
      return tAccountOwner typeis PolicyPeriod && tAccountOwner.Archived
    }
    
    property get relatedTAccountOwners () : TAccountOwner[] {
      return getRequireValue("relatedTAccountOwners", 0) as TAccountOwner[]
    }
    
    property set relatedTAccountOwners ($arg :  TAccountOwner[]) {
      setRequireValue("relatedTAccountOwners", 0, $arg)
    }
    
    property get tAccountOwner () : TAccountOwner {
      return getRequireValue("tAccountOwner", 0) as TAccountOwner
    }
    
    property set tAccountOwner ($arg :  TAccountOwner) {
      setRequireValue("tAccountOwner", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/accounting/LedgerScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanel2ExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=TransactionDate_Cell) at LedgerScreen.pcf: line 82, column 73
    function sortValue_13 (LineItem :  entity.LineItem) : java.lang.Object {
      return LineItem.Transaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at LedgerScreen.pcf: line 49, column 52
    function sortValue_4 (tAccount :  entity.TAccount) : java.lang.Object {
      return tAccount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Balance_Cell) at LedgerScreen.pcf: line 55, column 49
    function sortValue_5 (tAccount :  entity.TAccount) : java.lang.Object {
      return tAccount.Balance
    }
    
    // 'value' attribute on RowIterator (id=TAccounts) at LedgerScreen.pcf: line 43, column 49
    function value_12 () : entity.TAccount[] {
      return selectedTAccountOwner.getTAccountsSortedByName()
    }
    
    // 'value' attribute on RowIterator (id=LineItems) at LedgerScreen.pcf: line 74, column 53
    function value_26 () : entity.LineItem[] {
      return selectedTAccount.LineItemsAndPreloadTransactions
    }
    
    property get selectedTAccount () : TAccount {
      return getCurrentSelection(2) as TAccount
    }
    
    property set selectedTAccount ($arg :  TAccount) {
      setCurrentSelection(2, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/accounting/LedgerScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends LedgerScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at LedgerScreen.pcf: line 22, column 70
    function def_onEnter_2 (def :  pcf.TAccountOwnersLV) : void {
      def.onEnter(tAccountOwner, relatedTAccountOwners)
    }
    
    // 'def' attribute on PanelRef at LedgerScreen.pcf: line 22, column 70
    function def_refreshVariables_3 (def :  pcf.TAccountOwnersLV) : void {
      def.refreshVariables(tAccountOwner, relatedTAccountOwners)
    }
    
    property get selectedTAccountOwner () : TAccountOwner {
      return getCurrentSelection(1) as TAccountOwner
    }
    
    property set selectedTAccountOwner ($arg :  TAccountOwner) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  
}