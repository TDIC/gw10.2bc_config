package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/CreateDisbursementDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreateDisbursementDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/disbursement/CreateDisbursementDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreateDisbursementDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at CreateDisbursementDetailScreen.pcf: line 66, column 58
    function currency_21 () : typekey.Currency {
      return disbursementVar.Currency
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=pending_Input) at CreateDisbursementDetailScreen.pcf: line 73, column 76
    function currency_25 () : typekey.Currency {
      return disbursementVar.Account.Currency
    }
    
    // 'def' attribute on PanelRef at CreateDisbursementDetailScreen.pcf: line 77, column 64
    function def_onEnter_27 (def :  pcf.CreateDisbursementDetailDV) : void {
      def.onEnter(disbursementVar, true)
    }
    
    // 'def' attribute on PanelRef at CreateDisbursementDetailScreen.pcf: line 77, column 64
    function def_refreshVariables_28 (def :  pcf.CreateDisbursementDetailDV) : void {
      def.refreshVariables(disbursementVar, true)
    }
    
    // 'value' attribute on RangeInput (id=UnappliedFunds_Input) at CreateDisbursementDetailScreen.pcf: line 52, column 72
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      wizardHelper.TargetUnapplied = (__VALUE_TO_SET as entity.UnappliedFund)
    }
    
    // 'initialValue' attribute on Variable at CreateDisbursementDetailScreen.pcf: line 13, column 30
    function initialValue_0 () : java.util.Date {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'initialValue' attribute on Variable at CreateDisbursementDetailScreen.pcf: line 17, column 23
    function initialValue_1 () : Boolean {
      return perm.Transaction.disbpayeeedit
    }
    
    // 'initialValue' attribute on Variable at CreateDisbursementDetailScreen.pcf: line 21, column 57
    function initialValue_2 () : gw.account.CreateDisbursementWizardHelper {
      return new gw.account.CreateDisbursementWizardHelper(disbursementVar)
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at CreateDisbursementDetailScreen.pcf: line 52, column 72
    function valueRange_13 () : java.lang.Object {
      return disbursementVar.Account.UnappliedFundsSortedByDisplayName
    }
    
    // 'value' attribute on RangeInput (id=UnappliedFunds_Input) at CreateDisbursementDetailScreen.pcf: line 52, column 72
    function valueRoot_12 () : java.lang.Object {
      return wizardHelper
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at CreateDisbursementDetailScreen.pcf: line 66, column 58
    function valueRoot_20 () : java.lang.Object {
      return disbursementVar.UnappliedFund
    }
    
    // 'value' attribute on TextInput (id=account_Input) at CreateDisbursementDetailScreen.pcf: line 33, column 58
    function valueRoot_4 () : java.lang.Object {
      return disbursementVar.Account
    }
    
    // 'value' attribute on RangeInput (id=UnappliedFunds_Input) at CreateDisbursementDetailScreen.pcf: line 52, column 72
    function value_10 () : entity.UnappliedFund {
      return wizardHelper.TargetUnapplied
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at CreateDisbursementDetailScreen.pcf: line 66, column 58
    function value_19 () : gw.pl.currency.MonetaryAmount {
      return disbursementVar.UnappliedFund.Balance
    }
    
    // 'value' attribute on MonetaryAmountInput (id=pending_Input) at CreateDisbursementDetailScreen.pcf: line 73, column 76
    function value_23 () : gw.pl.currency.MonetaryAmount {
      return disbursementVar.UnappliedFund.PendingDisbursementAmount
    }
    
    // 'value' attribute on TextInput (id=account_Input) at CreateDisbursementDetailScreen.pcf: line 33, column 58
    function value_3 () : java.lang.String {
      return disbursementVar.Account.AccountNumber
    }
    
    // 'value' attribute on TextInput (id=insured_Input) at CreateDisbursementDetailScreen.pcf: line 37, column 65
    function value_6 () : java.lang.String {
      return disbursementVar.Account.AccountNameLocalized
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at CreateDisbursementDetailScreen.pcf: line 52, column 72
    function verifyValueRangeIsAllowedType_14 ($$arg :  entity.UnappliedFund[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at CreateDisbursementDetailScreen.pcf: line 52, column 72
    function verifyValueRangeIsAllowedType_14 ($$arg :  gw.api.database.IQueryBeanResult<entity.UnappliedFund>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at CreateDisbursementDetailScreen.pcf: line 52, column 72
    function verifyValueRangeIsAllowedType_14 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at CreateDisbursementDetailScreen.pcf: line 52, column 72
    function verifyValueRange_15 () : void {
      var __valueRangeArg = disbursementVar.Account.UnappliedFundsSortedByDisplayName
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_14(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=UnappliedFunds_Input) at CreateDisbursementDetailScreen.pcf: line 52, column 72
    function visible_9 () : java.lang.Boolean {
      return disbursementVar.Account.HasDesignatedUnappliedFund
    }
    
    property get CanEditDisbursementPayee () : Boolean {
      return getVariableValue("CanEditDisbursementPayee", 0) as Boolean
    }
    
    property set CanEditDisbursementPayee ($arg :  Boolean) {
      setVariableValue("CanEditDisbursementPayee", 0, $arg)
    }
    
    property get disbursementVar () : AccountDisbursement {
      return getRequireValue("disbursementVar", 0) as AccountDisbursement
    }
    
    property set disbursementVar ($arg :  AccountDisbursement) {
      setRequireValue("disbursementVar", 0, $arg)
    }
    
    property get today () : java.util.Date {
      return getVariableValue("today", 0) as java.util.Date
    }
    
    property set today ($arg :  java.util.Date) {
      setVariableValue("today", 0, $arg)
    }
    
    property get wizardHelper () : gw.account.CreateDisbursementWizardHelper {
      return getVariableValue("wizardHelper", 0) as gw.account.CreateDisbursementWizardHelper
    }
    
    property set wizardHelper ($arg :  gw.account.CreateDisbursementWizardHelper) {
      setVariableValue("wizardHelper", 0, $arg)
    }
    
    
  }
  
  
}