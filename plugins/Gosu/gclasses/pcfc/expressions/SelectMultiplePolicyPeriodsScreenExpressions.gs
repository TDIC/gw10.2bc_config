package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultiplePolicyPeriodsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SelectMultiplePolicyPeriodsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultiplePolicyPeriodsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends SelectMultiplePolicyPeriodsScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=CancelButton) at SelectMultiplePolicyPeriodsScreen.pcf: line 28, column 64
    function action_1 () : void {
      CurrentLocation.cancel()
    }
    
    // 'def' attribute on PanelRef at SelectMultiplePolicyPeriodsScreen.pcf: line 18, column 53
    function def_onEnter_2 (def :  pcf.PolicySearchDV) : void {
      def.onEnter(searchCriteria, false)
    }
    
    // 'def' attribute on PanelRef at SelectMultiplePolicyPeriodsScreen.pcf: line 32, column 81
    function def_onEnter_4 (def :  pcf.PolicySearchResultsLV) : void {
      def.onEnter(policySearchViews, null, true, false, true)
    }
    
    // 'def' attribute on PanelRef at SelectMultiplePolicyPeriodsScreen.pcf: line 18, column 53
    function def_refreshVariables_3 (def :  pcf.PolicySearchDV) : void {
      def.refreshVariables(searchCriteria, false)
    }
    
    // 'def' attribute on PanelRef at SelectMultiplePolicyPeriodsScreen.pcf: line 32, column 81
    function def_refreshVariables_5 (def :  pcf.PolicySearchResultsLV) : void {
      def.refreshVariables(policySearchViews, null, true, false, true)
    }
    
    // 'pickValue' attribute on CheckedValuesToolbarButton (id=addbutton) at SelectMultiplePolicyPeriodsScreen.pcf: line 24, column 96
    function pickValue_0 (CheckedValues :  entity.PolicySearchView[]) : PolicyPeriod[] {
      return gw.api.web.search.SearchPopupUtil.getPolicyPeriodArray(CheckedValues)
    }
    
    // 'searchCriteria' attribute on SearchPanel at SelectMultiplePolicyPeriodsScreen.pcf: line 16, column 84
    function searchCriteria_7 () : gw.search.PolicySearchCriteria {
      return policySearchCriteria
    }
    
    // 'search' attribute on SearchPanel at SelectMultiplePolicyPeriodsScreen.pcf: line 16, column 84
    function search_6 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria, StringCriterionMode.TC_STARTSWITH)
    }
    
    property get policySearchViews () : gw.api.database.IQueryBeanResult<PolicySearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<PolicySearchView>
    }
    
    property get searchCriteria () : gw.search.PolicySearchCriteria {
      return getCriteriaValue(1) as gw.search.PolicySearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.PolicySearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultiplePolicyPeriodsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SelectMultiplePolicyPeriodsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get policySearchCriteria () : gw.search.PolicySearchCriteria {
      return getRequireValue("policySearchCriteria", 0) as gw.search.PolicySearchCriteria
    }
    
    property set policySearchCriteria ($arg :  gw.search.PolicySearchCriteria) {
      setRequireValue("policySearchCriteria", 0, $arg)
    }
    
    
  }
  
  
}