package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/OutgoingProducerPaymentSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OutgoingProducerPaymentSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/OutgoingProducerPaymentSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OutgoingProducerPaymentSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerInput (id=PayeeCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 67, column 39
    function action_40 () : void {
      ProducerSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=PayeeCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 67, column 39
    function action_dest_41 () : pcf.api.Destination {
      return pcf.ProducerSearchPopup.createDestination()
    }
    
    // 'available' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 52, column 43
    function available_26 () : java.lang.Boolean {
      return (searchCriteria.Currency != null or gw.api.util.CurrencyUtil.isSingleCurrencyMode())
    }
    
    // 'conversionExpression' attribute on PickerInput (id=PayeeCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 67, column 39
    function conversionExpression_42 (PickedValue :  Producer) : java.lang.String {
      return PickedValue.Name
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 52, column 43
    function currency_30 () : typekey.Currency {
      return gw.search.SearchHelper.getDefaultCurrencyIfNull(searchCriteria.Currency)
    }
    
    // 'def' attribute on InputSetRef at OutgoingProducerPaymentSearchDV.pcf: line 83, column 41
    function def_onEnter_58 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at OutgoingProducerPaymentSearchDV.pcf: line 83, column 41
    function def_refreshVariables_59 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on RangeInput (id=StatusCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 20, column 52
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Status = (__VALUE_TO_SET as typekey.OutgoingPaymentStatus)
    }
    
    // 'value' attribute on DateInput (id=LatestIssueDateCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 30, column 49
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.LatestIssueDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=CheckNumberCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 35, column 45
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.CheckNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 42, column 66
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Currency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 52, column 43
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MinAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 59, column 43
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MaxAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on PickerInput (id=PayeeCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 67, column 39
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Payee = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=MethodCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 74, column 44
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Method = (__VALUE_TO_SET as typekey.PaymentMethod)
    }
    
    // 'value' attribute on TextInput (id=TokenCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 79, column 39
    function defaultSetter_55 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Token = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=EarliestIssueDateCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 25, column 51
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.EarliestIssueDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'onChange' attribute on PostOnChange at OutgoingProducerPaymentSearchDV.pcf: line 44, column 54
    function onChange_19 () : void {
      blankMinimumAndMaximumFields()
    }
    
    // 'valueRange' attribute on RangeInput (id=StatusCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 20, column 52
    function valueRange_3 () : java.lang.Object {
      return StatusRange
    }
    
    // 'valueRange' attribute on RangeInput (id=MethodCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 74, column 44
    function valueRange_50 () : java.lang.Object {
      return PaymentMethodRange
    }
    
    // 'value' attribute on RangeInput (id=StatusCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 20, column 52
    function valueRoot_2 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on RangeInput (id=StatusCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 20, column 52
    function value_0 () : typekey.OutgoingPaymentStatus {
      return searchCriteria.Status
    }
    
    // 'value' attribute on DateInput (id=LatestIssueDateCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 30, column 49
    function value_11 () : java.util.Date {
      return searchCriteria.LatestIssueDate
    }
    
    // 'value' attribute on TextInput (id=CheckNumberCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 35, column 45
    function value_15 () : java.lang.String {
      return searchCriteria.CheckNumber
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 42, column 66
    function value_21 () : typekey.Currency {
      return searchCriteria.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 52, column 43
    function value_27 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MinAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 59, column 43
    function value_34 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MaxAmount
    }
    
    // 'value' attribute on PickerInput (id=PayeeCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 67, column 39
    function value_43 () : java.lang.String {
      return searchCriteria.Payee
    }
    
    // 'value' attribute on RangeInput (id=MethodCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 74, column 44
    function value_47 () : typekey.PaymentMethod {
      return searchCriteria.Method
    }
    
    // 'value' attribute on TextInput (id=TokenCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 79, column 39
    function value_54 () : java.lang.String {
      return searchCriteria.Token
    }
    
    // 'value' attribute on DateInput (id=EarliestIssueDateCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 25, column 51
    function value_7 () : java.util.Date {
      return searchCriteria.EarliestIssueDate
    }
    
    // 'valueRange' attribute on RangeInput (id=StatusCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 20, column 52
    function verifyValueRangeIsAllowedType_4 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=StatusCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 20, column 52
    function verifyValueRangeIsAllowedType_4 ($$arg :  typekey.OutgoingPaymentStatus[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=MethodCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 74, column 44
    function verifyValueRangeIsAllowedType_51 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=MethodCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 74, column 44
    function verifyValueRangeIsAllowedType_51 ($$arg :  typekey.PaymentMethod[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=StatusCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 20, column 52
    function verifyValueRange_5 () : void {
      var __valueRangeArg = StatusRange
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_4(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=MethodCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 74, column 44
    function verifyValueRange_52 () : void {
      var __valueRangeArg = PaymentMethodRange
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_51(__valueRangeArg)
    }
    
    // 'visible' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at OutgoingProducerPaymentSearchDV.pcf: line 42, column 66
    function visible_20 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    property get searchCriteria () : gw.search.OutgoingProducerPmntSearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.search.OutgoingProducerPmntSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.OutgoingProducerPmntSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    property get StatusRange() : java.util.List<OutgoingPaymentStatus> {
      return {TC_ISSUED, TC_VOIDED, TC_CLEARED}
    }
    
    property get PaymentMethodRange() : java.util.List<PaymentMethod> {
      return {TC_RESPONSIVE, TC_ACH, TC_CHECK}
    }
    
    function blankMinimumAndMaximumFields() {
      searchCriteria.MinAmount = null
      searchCriteria.MaxAmount = null
    }
    
    
  }
  
  
}