package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.Percentage
uses gw.api.util.StringUtil
uses gw.pl.currency.MonetaryAmount
uses java.math.BigDecimal
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailSummaryScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailSummaryScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailSummaryScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailSummaryScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on RangeInput (id=DelinquencyPlan_Input) at PolicyDetailSummaryScreen.pcf: line 623, column 47
    function actionAvailable_336 () : java.lang.Boolean {
      return policyPeriod.PolicyPeriodDelinquencyPlan != null
    }
    
    // 'actionAvailable' attribute on TextInput (id=Producer_Input) at PolicyDetailSummaryScreen.pcf: line 641, column 40
    function actionAvailable_352 () : java.lang.Boolean {
      return policyPeriod.PrimaryProducerCode != null
    }
    
    // 'action' attribute on TextInput (id=AccountNumber_Input) at PolicyDetailSummaryScreen.pcf: line 301, column 39
    function action_147 () : void {
      AccountOverview.go(account)
    }
    
    // 'action' attribute on TextInput (id=AccountName_Input) at PolicyDetailSummaryScreen.pcf: line 306, column 49
    function action_151 () : void {
      AccountOverview.go(account)
    }
    
    // 'action' attribute on MenuItem (id=ChangeDirectBillInvoicingOverrides) at PolicyDetailSummaryScreen.pcf: line 320, column 91
    function action_158 () : void {
      ChangeInvoicingOverridesPopup.push(policyPeriod)
    }
    
    // 'action' attribute on MenuItem (id=ChangeListBillInvoicingOverrides) at PolicyDetailSummaryScreen.pcf: line 326, column 89
    function action_162 () : void {
      ListBillInvoicingOverridesWizard.push(policyPeriod)
    }
    
    // 'action' attribute on TextInput (id=OverridingPayerAccount_Input) at PolicyDetailSummaryScreen.pcf: line 334, column 67
    function action_169 () : void {
      AccountOverview.go(policyPeriod.OverridingPayerAccount)
    }
    
    // 'action' attribute on ToolbarButton (id=ArchiveRetrieveButton) at PolicyDetailSummaryScreen.pcf: line 107, column 80
    function action_20 () : void {
      RequestRetrievePopup.push(policyPeriod)
    }
    
    // 'action' attribute on TextInput (id=PaymentPlan_Input) at PolicyDetailSummaryScreen.pcf: line 447, column 50
    function action_225 () : void {
      PaymentPlanDetailPopup.push(policyPeriod.PaymentPlan)
    }
    
    // 'action' attribute on ToolbarButton (id=OpenButton) at PolicyDetailSummaryScreen.pcf: line 113, column 80
    function action_23 () : void {
      reopenPolicy()
    }
    
    // 'action' attribute on ToolbarButton (id=StartDelinquencyButton) at PolicyDetailSummaryScreen.pcf: line 119, column 73
    function action_26 () : void {
      StartDelinquencyProcessPopup.push(account.OpenDelinquencyTargets)
    }
    
    // 'action' attribute on ToolbarButton (id=CloseDelinquencyButton) at PolicyDetailSummaryScreen.pcf: line 125, column 73
    function action_30 () : void {
      TDIC_CloseDelinquencyProcessPopup.push(policyPeriod.Account.OpenDelinquencyTargets)
    }
    
    // 'action' attribute on RangeInput (id=DelinquencyPlan_Input) at PolicyDetailSummaryScreen.pcf: line 623, column 47
    function action_334 () : void {
      DelinquencyPlanDetailPopup.push(policyPeriod.PolicyPeriodDelinquencyPlan)
    }
    
    // 'action' attribute on TextInput (id=ReturnPremiumPlan_Input) at PolicyDetailSummaryScreen.pcf: line 632, column 56
    function action_345 () : void {
      ReturnPremiumPlanDetail.push(policyPeriod.ReturnPremiumPlan)
    }
    
    // 'action' attribute on TextInput (id=Producer_Input) at PolicyDetailSummaryScreen.pcf: line 641, column 40
    function action_350 () : void {
      ProducerDetail.go(policyPeriod.PrimaryProducerCode.Producer)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketAlertAlertBar) at PolicyDetailSummaryScreen.pcf: line 134, column 62
    function action_36 () : void {
      TroubleTicketAlertForward.push(policyPeriod.Policy, policyPeriod)
    }
    
    // 'action' attribute on MenuItem (id=ChangeToAgencyBillAction) at PolicyDetailSummaryScreen.pcf: line 659, column 91
    function action_361 () : void {
      changeBillingMethodToAgencyBill()
    }
    
    // 'action' attribute on MenuItem (id=ChangeToDirectBillAction) at PolicyDetailSummaryScreen.pcf: line 666, column 91
    function action_364 () : void {
      ChangeBillingMethodToDirectBillPopup.push(policyPeriod)
    }
    
    // 'action' attribute on MenuItem (id=changeToListBill) at PolicyDetailSummaryScreen.pcf: line 673, column 89
    function action_369 () : void {
      ListBillInvoicingOverridesWizard.push(policyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketForPolicyPeriod) at PolicyDetailSummaryScreen.pcf: line 140, column 55
    function action_41 () : void {
      TroubleTicketAlertForward.push(policyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_DelinquencyAlertAlertBar) at PolicyDetailSummaryScreen.pcf: line 146, column 72
    function action_46 () : void {
      PolicyDetailDelinquencies.go(policyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_HeldChargesAlertBar) at PolicyDetailSummaryScreen.pcf: line 152, column 48
    function action_51 () : void {
      PolicyDetailCharges.go(policyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_ActivitiesAlertBar) at PolicyDetailSummaryScreen.pcf: line 166, column 121
    function action_57 () : void {
      PolicyPeriodActivitiesPage.go(policyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_PaymentRequestAlertBar) at PolicyDetailSummaryScreen.pcf: line 179, column 179
    function action_62 () : void {
      AccountPaymentRequests.go(policyPeriod.Account)
    }
    
    // 'action' attribute on TextInput (id=AccountNumber_Input) at PolicyDetailSummaryScreen.pcf: line 301, column 39
    function action_dest_148 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(account)
    }
    
    // 'action' attribute on TextInput (id=AccountName_Input) at PolicyDetailSummaryScreen.pcf: line 306, column 49
    function action_dest_152 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(account)
    }
    
    // 'action' attribute on MenuItem (id=ChangeDirectBillInvoicingOverrides) at PolicyDetailSummaryScreen.pcf: line 320, column 91
    function action_dest_159 () : pcf.api.Destination {
      return pcf.ChangeInvoicingOverridesPopup.createDestination(policyPeriod)
    }
    
    // 'action' attribute on MenuItem (id=ChangeListBillInvoicingOverrides) at PolicyDetailSummaryScreen.pcf: line 326, column 89
    function action_dest_163 () : pcf.api.Destination {
      return pcf.ListBillInvoicingOverridesWizard.createDestination(policyPeriod)
    }
    
    // 'action' attribute on TextInput (id=OverridingPayerAccount_Input) at PolicyDetailSummaryScreen.pcf: line 334, column 67
    function action_dest_170 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(policyPeriod.OverridingPayerAccount)
    }
    
    // 'action' attribute on ToolbarButton (id=ArchiveRetrieveButton) at PolicyDetailSummaryScreen.pcf: line 107, column 80
    function action_dest_21 () : pcf.api.Destination {
      return pcf.RequestRetrievePopup.createDestination(policyPeriod)
    }
    
    // 'action' attribute on TextInput (id=PaymentPlan_Input) at PolicyDetailSummaryScreen.pcf: line 447, column 50
    function action_dest_226 () : pcf.api.Destination {
      return pcf.PaymentPlanDetailPopup.createDestination(policyPeriod.PaymentPlan)
    }
    
    // 'action' attribute on ToolbarButton (id=StartDelinquencyButton) at PolicyDetailSummaryScreen.pcf: line 119, column 73
    function action_dest_27 () : pcf.api.Destination {
      return pcf.StartDelinquencyProcessPopup.createDestination(account.OpenDelinquencyTargets)
    }
    
    // 'action' attribute on ToolbarButton (id=CloseDelinquencyButton) at PolicyDetailSummaryScreen.pcf: line 125, column 73
    function action_dest_31 () : pcf.api.Destination {
      return pcf.TDIC_CloseDelinquencyProcessPopup.createDestination(policyPeriod.Account.OpenDelinquencyTargets)
    }
    
    // 'action' attribute on RangeInput (id=DelinquencyPlan_Input) at PolicyDetailSummaryScreen.pcf: line 623, column 47
    function action_dest_335 () : pcf.api.Destination {
      return pcf.DelinquencyPlanDetailPopup.createDestination(policyPeriod.PolicyPeriodDelinquencyPlan)
    }
    
    // 'action' attribute on TextInput (id=ReturnPremiumPlan_Input) at PolicyDetailSummaryScreen.pcf: line 632, column 56
    function action_dest_346 () : pcf.api.Destination {
      return pcf.ReturnPremiumPlanDetail.createDestination(policyPeriod.ReturnPremiumPlan)
    }
    
    // 'action' attribute on TextInput (id=Producer_Input) at PolicyDetailSummaryScreen.pcf: line 641, column 40
    function action_dest_351 () : pcf.api.Destination {
      return pcf.ProducerDetail.createDestination(policyPeriod.PrimaryProducerCode.Producer)
    }
    
    // 'action' attribute on MenuItem (id=ChangeToDirectBillAction) at PolicyDetailSummaryScreen.pcf: line 666, column 91
    function action_dest_365 () : pcf.api.Destination {
      return pcf.ChangeBillingMethodToDirectBillPopup.createDestination(policyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketAlertAlertBar) at PolicyDetailSummaryScreen.pcf: line 134, column 62
    function action_dest_37 () : pcf.api.Destination {
      return pcf.TroubleTicketAlertForward.createDestination(policyPeriod.Policy, policyPeriod)
    }
    
    // 'action' attribute on MenuItem (id=changeToListBill) at PolicyDetailSummaryScreen.pcf: line 673, column 89
    function action_dest_370 () : pcf.api.Destination {
      return pcf.ListBillInvoicingOverridesWizard.createDestination(policyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketForPolicyPeriod) at PolicyDetailSummaryScreen.pcf: line 140, column 55
    function action_dest_42 () : pcf.api.Destination {
      return pcf.TroubleTicketAlertForward.createDestination(policyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_DelinquencyAlertAlertBar) at PolicyDetailSummaryScreen.pcf: line 146, column 72
    function action_dest_47 () : pcf.api.Destination {
      return pcf.PolicyDetailDelinquencies.createDestination(policyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_HeldChargesAlertBar) at PolicyDetailSummaryScreen.pcf: line 152, column 48
    function action_dest_52 () : pcf.api.Destination {
      return pcf.PolicyDetailCharges.createDestination(policyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_ActivitiesAlertBar) at PolicyDetailSummaryScreen.pcf: line 166, column 121
    function action_dest_58 () : pcf.api.Destination {
      return pcf.PolicyPeriodActivitiesPage.createDestination(policyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_PaymentRequestAlertBar) at PolicyDetailSummaryScreen.pcf: line 179, column 179
    function action_dest_63 () : pcf.api.Destination {
      return pcf.AccountPaymentRequests.createDestination(policyPeriod.Account)
    }
    
    // 'available' attribute on MenuItem (id=ChangeDirectBillInvoicingOverrides) at PolicyDetailSummaryScreen.pcf: line 320, column 91
    function available_156 () : java.lang.Boolean {
      return !CurrentLocation.InEditMode && policyPeriod.isDirectBill() && not policyPeriod.Closed
    }
    
    // 'available' attribute on MenuItem (id=ChangeListBillInvoicingOverrides) at PolicyDetailSummaryScreen.pcf: line 326, column 89
    function available_160 () : java.lang.Boolean {
      return not policyPeriod.Closed
    }
    
    // 'available' attribute on ToolbarButton (id=StartDelinquencyButton) at PolicyDetailSummaryScreen.pcf: line 119, column 73
    function available_24 () : java.lang.Boolean {
      return !CurrentLocation.InEditMode && !policyPeriod.isDirectOrImpliedTargetOfHoldType(HoldType.TC_DELINQUENCY) && perm.System.acctdetedit
    }
    
    // 'available' attribute on ToolbarButton (id=CloseDelinquencyButton) at PolicyDetailSummaryScreen.pcf: line 125, column 73
    function available_28 () : java.lang.Boolean {
      return !CurrentLocation.InEditMode && !policyPeriod.isDirectOrImpliedTargetOfHoldType(HoldType.TC_DELINQUENCY) && policyPeriod.hasActiveDelinquencyProcess()
    }
    
    // 'available' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketAlertAlertBar) at PolicyDetailSummaryScreen.pcf: line 134, column 62
    function available_34 () : java.lang.Boolean {
      return perm.System.plcyttktview
    }
    
    // 'available' attribute on AlertBar (id=PolicyDetailSummary_DelinquencyAlertAlertBar) at PolicyDetailSummaryScreen.pcf: line 146, column 72
    function available_44 () : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctdelview
    }
    
    // 'available' attribute on AlertBar (id=PolicyDetailSummary_HeldChargesAlertBar) at PolicyDetailSummaryScreen.pcf: line 152, column 48
    function available_49 () : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcychargesview
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=PolicyCharges_Input) at PolicyDetailSummaryScreen.pcf: line 380, column 28
    function currency_201 () : typekey.Currency {
      return policyPeriod.Currency
    }
    
    // 'def' attribute on PanelRef at PolicyDetailSummaryScreen.pcf: line 679, column 52
    function def_onEnter_376 (def :  pcf.PremiumReportDueDatesLV) : void {
      def.onEnter(policyPeriod)
    }
    
    // 'def' attribute on PanelRef at PolicyDetailSummaryScreen.pcf: line 684, column 48
    function def_onEnter_378 (def :  pcf.TransactionDetailsLV) : void {
      def.onEnter(policyPeriod)
    }
    
    // 'def' attribute on PanelRef at PolicyDetailSummaryScreen.pcf: line 679, column 52
    function def_refreshVariables_377 (def :  pcf.PremiumReportDueDatesLV) : void {
      def.refreshVariables(policyPeriod)
    }
    
    // 'def' attribute on PanelRef at PolicyDetailSummaryScreen.pcf: line 684, column 48
    function def_refreshVariables_379 (def :  pcf.TransactionDetailsLV) : void {
      def.refreshVariables(policyPeriod)
    }
    
    // 'value' attribute on RangeInput (id=SecurityZone_Input) at PolicyDetailSummaryScreen.pcf: line 289, column 44
    function defaultSetter_137 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.SecurityZone = (__VALUE_TO_SET as entity.SecurityZone)
    }
    
    // 'value' attribute on BooleanRadioInput (id=EnableEquityWarnings_Input) at PolicyDetailSummaryScreen.pcf: line 577, column 81
    function defaultSetter_305 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.EquityWarningsEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=EquityBuffer_Input) at PolicyDetailSummaryScreen.pcf: line 587, column 57
    function defaultSetter_313 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.EquityBuffer = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeInput (id=DelinquencyPlan_Input) at PolicyDetailSummaryScreen.pcf: line 623, column 47
    function defaultSetter_338 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriod.PolicyPeriodDelinquencyPlan = (__VALUE_TO_SET as entity.DelinquencyPlan)
    }
    
    // 'editable' attribute on RangeInput (id=SecurityZone_Input) at PolicyDetailSummaryScreen.pcf: line 289, column 44
    function editable_135 () : java.lang.Boolean {
      return perm.System.plcydtlsscrnedit_TDIC
    }
    
    // 'editable' attribute on BooleanRadioInput (id=EnableEquityWarnings_Input) at PolicyDetailSummaryScreen.pcf: line 577, column 81
    function editable_302 () : java.lang.Boolean {
      return perm.System.equitywarningsedit
    }
    
    // 'editable' attribute on RangeInput (id=DelinquencyPlan_Input) at PolicyDetailSummaryScreen.pcf: line 623, column 47
    function editable_333 () : java.lang.Boolean {
      return perm.System.deliqedit_TDIC
    }
    
    // 'encryptionExpression' attribute on PrivacyInput (id=TaxID_Input) at PolicyDetailSummaryScreen.pcf: line 352, column 62
    function encryptionExpression_185 (VALUE :  java.lang.String) : java.lang.String {
      return gw.api.privacy.EncryptionMaskExpressions.maskTaxId(VALUE)
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 14, column 59
    function initialValue_0 () : gw.web.policy.PolicySummaryFinancialsHelper {
      return new gw.web.policy.PolicySummaryFinancialsHelper(policyPeriod)
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 19, column 45
    function initialValue_1 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.getTotalValue(null)
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 64, column 45
    function initialValue_10 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.EarnedAmount
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 69, column 45
    function initialValue_11 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.TotalEquityChargeValue
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 74, column 45
    function initialValue_12 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.PolicyEquity
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 79, column 22
    function initialValue_13 () : Number {
      return policyPeriod.getPolicyEquityPercentage(policyTotalEquityChargeValue, policyEquity)
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 83, column 23
    function initialValue_14 () : Account {
      return policyPeriod.Policy.Account
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 87, column 39
    function initialValue_15 () : DelinquencyProcessEvent {
      return policyPeriod.CancellationProcessEvent
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 91, column 23
    function initialValue_16 () : boolean {
      return policyPeriod.Canceled
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 96, column 68
    function initialValue_17 () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return gw.api.database.Query.make(SecurityZone).select()
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 101, column 22
    function initialValue_18 () : String {
      return policyPeriod.Retrieved ? DisplayKey.get("Web.PolicyDetailDV.ArchiveStatusRetrieved") : policyPeriod.ArchiveState.DisplayName
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 24, column 45
    function initialValue_2 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.getSafeEarnedPremium()
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 29, column 45
    function initialValue_3 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.getEarnedValue(null)
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 34, column 45
    function initialValue_4 () : gw.pl.currency.MonetaryAmount {
      return totalValueToday - policyPeriod.UnbilledAmount
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 39, column 45
    function initialValue_5 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.PaidAmount
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 44, column 45
    function initialValue_6 () : gw.pl.currency.MonetaryAmount {
      return paidAmount
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 49, column 30
    function initialValue_7 () : java.util.Date {
      return policyPeriod.PaidThroughDate
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 54, column 22
    function initialValue_8 () : Number {
      return policyPeriod.getDaysTillPaidThruDate(paidThroughDate)
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailSummaryScreen.pcf: line 59, column 45
    function initialValue_9 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.getTotalValue(TC_PREMIUM)
    }
    
    // EditButtons at PolicyDetailSummaryScreen.pcf: line 127, column 45
    function label_33 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'label' attribute on MenuItem (id=ChangeToDirectBillAction) at PolicyDetailSummaryScreen.pcf: line 666, column 91
    function label_366 () : java.lang.Object {
      return DisplayKey.get("Web.PolicyDetailDV.BillingMethod.ChangeToDirectBillButton") 
    }
    
    // 'label' attribute on MenuItem (id=changeToListBill) at PolicyDetailSummaryScreen.pcf: line 673, column 89
    function label_371 () : java.lang.Object {
      return DisplayKey.get("Web.PolicyDetailDV.BillingMethod.ChangeToListBillButton") 
    }
    
    // 'label' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketAlertAlertBar) at PolicyDetailSummaryScreen.pcf: line 134, column 62
    function label_38 () : java.lang.Object {
      return policyPeriod.Policy.AlertBarDisplayText
    }
    
    // 'label' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketForPolicyPeriod) at PolicyDetailSummaryScreen.pcf: line 140, column 55
    function label_43 () : java.lang.Object {
      return policyPeriod.AlertBarDisplayText
    }
    
    // 'label' attribute on AlertBar (id=PolicyDetailSummary_DelinquencyAlertAlertBar) at PolicyDetailSummaryScreen.pcf: line 146, column 72
    function label_48 () : java.lang.Object {
      return DisplayKey.get("Web.PolicyDetailSummary.DelinquencyAlert", policyPeriod.getDelinquencyReasons())
    }
    
    // 'label' attribute on AlertBar (id=ArchivedAlert) at PolicyDetailSummaryScreen.pcf: line 157, column 40
    function label_54 () : java.lang.Object {
      return DisplayKey.get("Web.Archive.PolicyPeriod.ArchivedAlertBar", policyPeriod.ArchiveDate.AsUIStyle)
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at PolicyDetailSummaryScreen.pcf: line 289, column 44
    function valueRange_139 () : java.lang.Object {
      return allSecurityZones
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at PolicyDetailSummaryScreen.pcf: line 623, column 47
    function valueRange_340 () : java.lang.Object {
      return policyPeriod.getApplicableDelinquencyPlans()
    }
    
    // 'value' attribute on DateInput (id=CancellationType_Input) at PolicyDetailSummaryScreen.pcf: line 273, column 33
    function valueRoot_126 () : java.lang.Object {
      return policyPeriod.Cancellation
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at PolicyDetailSummaryScreen.pcf: line 306, column 49
    function valueRoot_154 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on PrivacyInput (id=TaxID_Input) at PolicyDetailSummaryScreen.pcf: line 352, column 62
    function valueRoot_184 () : java.lang.Object {
      return policyPeriod.PrimaryInsured.Contact
    }
    
    // 'value' attribute on TextInput (id=PaymentPlan_Input) at PolicyDetailSummaryScreen.pcf: line 447, column 50
    function valueRoot_228 () : java.lang.Object {
      return policyPeriod.PaymentPlan
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PastDue_Input) at PolicyDetailSummaryScreen.pcf: line 531, column 58
    function valueRoot_282 () : java.lang.Object {
      return policyPeriodBalances
    }
    
    // 'value' attribute on DateInput (id=CancellationPending_Input) at PolicyDetailSummaryScreen.pcf: line 592, column 76
    function valueRoot_320 () : java.lang.Object {
      return cancellationEvent
    }
    
    // 'value' attribute on TypeKeyInput (id=DelinquencyReason_Input) at PolicyDetailSummaryScreen.pcf: line 598, column 76
    function valueRoot_325 () : java.lang.Object {
      return cancellationEvent.DelinquencyProcess
    }
    
    // 'value' attribute on TextInput (id=ReturnPremiumPlan_Input) at PolicyDetailSummaryScreen.pcf: line 632, column 56
    function valueRoot_348 () : java.lang.Object {
      return policyPeriod.ReturnPremiumPlan
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at PolicyDetailSummaryScreen.pcf: line 641, column 40
    function valueRoot_354 () : java.lang.Object {
      return policyPeriod.PrimaryProducerCode
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at PolicyDetailSummaryScreen.pcf: line 188, column 45
    function valueRoot_65 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on TypeKeyInput (id=Product_Input) at PolicyDetailSummaryScreen.pcf: line 211, column 40
    function valueRoot_80 () : java.lang.Object {
      return policyPeriod.Policy
    }
    
    // 'value' attribute on TypeKeyInput (id=ClosureStatus_Input) at PolicyDetailSummaryScreen.pcf: line 244, column 52
    function value_102 () : typekey.PolicyClosureStatus {
      return policyPeriod.ClosureStatus
    }
    
    // 'value' attribute on BooleanRadioInput (id=RequireFinalAudit_Input) at PolicyDetailSummaryScreen.pcf: line 248, column 51
    function value_105 () : java.lang.Boolean {
      return policyPeriod.RequireFinalAudit
    }
    
    // 'value' attribute on TextInput (id=FinalAuditBilled_Input) at PolicyDetailSummaryScreen.pcf: line 252, column 61
    function value_108 () : java.lang.String {
      return policyPeriod.FinalAuditBilledDisplayName
    }
    
    // 'value' attribute on TypeKeyInput (id=CancellationStatus_Input) at PolicyDetailSummaryScreen.pcf: line 257, column 51
    function value_111 () : typekey.PolicyCancelStatus {
      return policyPeriod.CancelStatus
    }
    
    // 'value' attribute on TextInput (id=CancellationReason_Input) at PolicyDetailSummaryScreen.pcf: line 262, column 33
    function value_115 () : java.lang.String {
      return policyPeriod.CancelReason
    }
    
    // 'value' attribute on TypeKeyInput (id=CancellationDate_Input) at PolicyDetailSummaryScreen.pcf: line 268, column 33
    function value_120 () : typekey.CancellationType {
      return policyPeriod.CancellationType
    }
    
    // 'value' attribute on DateInput (id=CancellationType_Input) at PolicyDetailSummaryScreen.pcf: line 273, column 33
    function value_125 () : java.util.Date {
      return policyPeriod.Cancellation.ModificationDate
    }
    
    // 'value' attribute on TextInput (id=DelinquencyStatus_Input) at PolicyDetailSummaryScreen.pcf: line 277, column 38
    function value_129 () : java.lang.String {
      return DelinquencyStatus
    }
    
    // 'value' attribute on TextInput (id=ArchiveStatus_Input) at PolicyDetailSummaryScreen.pcf: line 282, column 76
    function value_132 () : java.lang.String {
      return archiveState
    }
    
    // 'value' attribute on RangeInput (id=SecurityZone_Input) at PolicyDetailSummaryScreen.pcf: line 289, column 44
    function value_136 () : entity.SecurityZone {
      return policyPeriod.SecurityZone
    }
    
    // 'value' attribute on TextInput (id=Underwriter_Input) at PolicyDetailSummaryScreen.pcf: line 293, column 45
    function value_144 () : java.lang.String {
      return policyPeriod.Underwriter
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at PolicyDetailSummaryScreen.pcf: line 301, column 39
    function value_149 () : entity.Account {
      return account
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at PolicyDetailSummaryScreen.pcf: line 306, column 49
    function value_153 () : java.lang.String {
      return account.AccountNameLocalized
    }
    
    // 'value' attribute on TextInput (id=ChangeInvoicingOverrides_Input) at PolicyDetailSummaryScreen.pcf: line 313, column 50
    function value_165 () : java.lang.Object {
      return null
    }
    
    // 'value' attribute on TextInput (id=OverridingPayerAccount_Input) at PolicyDetailSummaryScreen.pcf: line 334, column 67
    function value_171 () : entity.Account {
      return policyPeriod.OverridingPayerAccount
    }
    
    // 'value' attribute on TextInput (id=OverridingInvoiceStream_Input) at PolicyDetailSummaryScreen.pcf: line 340, column 98
    function value_176 () : entity.InvoiceStream {
      return policyPeriod.OverridingInvoiceStream
    }
    
    // 'value' attribute on TextInput (id=PrimaryInsured_Name_Input) at PolicyDetailSummaryScreen.pcf: line 347, column 51
    function value_180 () : entity.PolicyPeriodContact {
      return policyPeriod.PrimaryInsured
    }
    
    // 'value' attribute on PrivacyInput (id=TaxID_Input) at PolicyDetailSummaryScreen.pcf: line 352, column 62
    function value_183 () : java.lang.String {
      return policyPeriod.PrimaryInsured.Contact.TaxID
    }
    
    // 'value' attribute on TextInput (id=ADANumber_Input) at PolicyDetailSummaryScreen.pcf: line 357, column 91
    function value_188 () : java.lang.String {
      return policyPeriod.PrimaryInsured.Contact.ADANumberOfficialID_TDIC
    }
    
    // 'value' attribute on TextInput (id=Address_Input) at PolicyDetailSummaryScreen.pcf: line 361, column 133
    function value_192 () : java.lang.String {
      return new gw.api.address.AddressFormatter().format(policyPeriod.PrimaryInsured.Contact.PrimaryAddress, "\n")
    }
    
    // 'value' attribute on TextInput (id=PrimaryInsured_Phone_Input) at PolicyDetailSummaryScreen.pcf: line 365, column 71
    function value_194 () : java.lang.String {
      return policyPeriod.PrimaryInsured.Contact.WorkPhoneValue
    }
    
    // 'value' attribute on TextInput (id=PrimaryInsured_Email_Input) at PolicyDetailSummaryScreen.pcf: line 369, column 70
    function value_197 () : java.lang.String {
      return policyPeriod.PrimaryInsured.Contact.EmailAddress1
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PolicyCharges_Input) at PolicyDetailSummaryScreen.pcf: line 380, column 28
    function value_200 () : gw.pl.currency.MonetaryAmount {
      return premium
    }
    
    // 'value' attribute on MonetaryAmountInput (id=OtherCharges_Input) at PolicyDetailSummaryScreen.pcf: line 387, column 46
    function value_203 () : gw.pl.currency.MonetaryAmount {
      return totalValueToday - premium
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalCharges_Input) at PolicyDetailSummaryScreen.pcf: line 395, column 36
    function value_206 () : gw.pl.currency.MonetaryAmount {
      return totalValueToday
    }
    
    // 'value' attribute on TextInput (id=EarnedPremium_Input) at PolicyDetailSummaryScreen.pcf: line 402, column 146
    function value_209 () : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(calculatePointsForDisplay(policyEarnedAmount, policyTotalEquityChargeValue), 0)
    }
    
    // 'value' attribute on TextInput (id=EarnedBalance_Input) at PolicyDetailSummaryScreen.pcf: line 406, column 177
    function value_211 () : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(calculatePointsForDisplay(policyTotalEquityChargeValue - policyEarnedAmount, policyTotalEquityChargeValue), 0)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ChargeProRataEarnedPremium_Input) at PolicyDetailSummaryScreen.pcf: line 418, column 36
    function value_213 () : gw.pl.currency.MonetaryAmount {
      return earnedPremium
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ChargeProRataOtherEarned_Input) at PolicyDetailSummaryScreen.pcf: line 425, column 50
    function value_216 () : gw.pl.currency.MonetaryAmount {
      return totalEarned - earnedPremium
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ChargeProRataTotalEarned_Input) at PolicyDetailSummaryScreen.pcf: line 433, column 34
    function value_219 () : gw.pl.currency.MonetaryAmount {
      return totalEarned
    }
    
    // 'value' attribute on TextInput (id=ChargeProRataPercentEarned_Input) at PolicyDetailSummaryScreen.pcf: line 438, column 132
    function value_222 () : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(policyPeriod.getPercentEarned(totalValueToday, totalEarned), 0)
    }
    
    // 'value' attribute on TextInput (id=PaymentPlan_Input) at PolicyDetailSummaryScreen.pcf: line 447, column 50
    function value_227 () : java.lang.String {
      return policyPeriod.PaymentPlan.Name
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalPaid_Input) at PolicyDetailSummaryScreen.pcf: line 455, column 30
    function value_230 () : gw.pl.currency.MonetaryAmount {
      return totalPaid
    }
    
    // 'value' attribute on TextInput (id=PaidToBilledRatio_Input) at PolicyDetailSummaryScreen.pcf: line 459, column 129
    function value_233 () : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(policyPeriod.getPaidToBilledRatio(totalBilled, paidAmount), 0)
    }
    
    // 'value' attribute on TextInput (id=PaidToValueRatio_Input) at PolicyDetailSummaryScreen.pcf: line 463, column 132
    function value_235 () : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(policyPeriod.getPaidToValueRatio(totalValueToday, paidAmount), 0)
    }
    
    // 'value' attribute on DateInput (id=FullPayDate_Input) at PolicyDetailSummaryScreen.pcf: line 477, column 62
    function value_238 () : java.util.Date {
      return policyPeriod.FullPayDiscountUntil
    }
    
    // 'value' attribute on MonetaryAmountInput (id=DiscountedPayment_Input) at PolicyDetailSummaryScreen.pcf: line 484, column 62
    function value_243 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.DiscountedPaymentThreshold
    }
    
    // 'value' attribute on BooleanRadioInput (id=RiskManagementDiscount_Input) at PolicyDetailSummaryScreen.pcf: line 495, column 81
    function value_252 () : java.lang.Boolean {
      return policyPeriod.RiskManagementDiscount_TDIC
    }
    
    // 'value' attribute on DateInput (id=Expires_Input) at PolicyDetailSummaryScreen.pcf: line 500, column 63
    function value_257 () : java.util.Date {
      return policyPeriod.RiskManagementExpires_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=MultilineAll_Input) at PolicyDetailSummaryScreen.pcf: line 505, column 84
    function value_262 () : java.lang.Boolean {
      return policyPeriod.MultilineAll_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=MultilinePLCP_Input) at PolicyDetailSummaryScreen.pcf: line 510, column 199
    function value_267 () : java.lang.Boolean {
      return policyPeriod.MultilinePLCP_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=MultilineWCPL_Input) at PolicyDetailSummaryScreen.pcf: line 515, column 137
    function value_272 () : java.lang.Boolean {
      return policyPeriod.MultilineWCPL_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=MultilineWC_Input) at PolicyDetailSummaryScreen.pcf: line 520, column 135
    function value_277 () : java.lang.Boolean {
      return policyPeriod.MultilineWC_TDIC
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PastDue_Input) at PolicyDetailSummaryScreen.pcf: line 531, column 58
    function value_281 () : gw.pl.currency.MonetaryAmount {
      return policyPeriodBalances.DelinquentAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalOutstanding_Input) at PolicyDetailSummaryScreen.pcf: line 539, column 59
    function value_285 () : gw.pl.currency.MonetaryAmount {
      return policyPeriodBalances.OutstandingAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=WriteoffExpense_Input) at PolicyDetailSummaryScreen.pcf: line 549, column 58
    function value_289 () : gw.pl.currency.MonetaryAmount {
      return policyPeriodBalances.WrittenOffAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PolicyEquity_Input) at PolicyDetailSummaryScreen.pcf: line 558, column 33
    function value_293 () : gw.pl.currency.MonetaryAmount {
      return policyEquity
    }
    
    // 'value' attribute on TextInput (id=PolicyEquityPercentage_Input) at PolicyDetailSummaryScreen.pcf: line 562, column 90
    function value_296 () : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(policyEquityPercent, 2)
    }
    
    // 'value' attribute on DateInput (id=PaidThruDate_Input) at PolicyDetailSummaryScreen.pcf: line 566, column 36
    function value_298 () : java.util.Date {
      return paidThroughDate
    }
    
    // 'value' attribute on TextInput (id=DaysTillPaidThruDate_Input) at PolicyDetailSummaryScreen.pcf: line 571, column 41
    function value_300 () : java.lang.Double {
      return daysTillPaidThroughDate
    }
    
    // 'value' attribute on BooleanRadioInput (id=EnableEquityWarnings_Input) at PolicyDetailSummaryScreen.pcf: line 577, column 81
    function value_304 () : java.lang.Boolean {
      return policyPeriod.EquityWarningsEnabled
    }
    
    // 'value' attribute on TextInput (id=EquityBuffer_Input) at PolicyDetailSummaryScreen.pcf: line 587, column 57
    function value_312 () : java.lang.Integer {
      return policyPeriod.EquityBuffer
    }
    
    // 'value' attribute on DateInput (id=CancellationPending_Input) at PolicyDetailSummaryScreen.pcf: line 592, column 76
    function value_319 () : java.util.Date {
      return cancellationEvent.TargetDate
    }
    
    // 'value' attribute on TypeKeyInput (id=DelinquencyReason_Input) at PolicyDetailSummaryScreen.pcf: line 598, column 76
    function value_324 () : typekey.DelinquencyReason {
      return cancellationEvent.DelinquencyProcess.Reason
    }
    
    // 'value' attribute on DateInput (id=InitialInception_Input) at PolicyDetailSummaryScreen.pcf: line 605, column 65
    function value_328 () : java.util.Date {
      return policyPeriod.getPolicyInitialInceptionDate()
    }
    
    // 'value' attribute on TextInput (id=TermNumber_Input) at PolicyDetailSummaryScreen.pcf: line 610, column 42
    function value_330 () : java.lang.Integer {
      return policyPeriod.TermNumber
    }
    
    // 'value' attribute on RangeInput (id=DelinquencyPlan_Input) at PolicyDetailSummaryScreen.pcf: line 623, column 47
    function value_337 () : entity.DelinquencyPlan {
      return policyPeriod.PolicyPeriodDelinquencyPlan
    }
    
    // 'value' attribute on TextInput (id=ReturnPremiumPlan_Input) at PolicyDetailSummaryScreen.pcf: line 632, column 56
    function value_347 () : java.lang.String {
      return policyPeriod.ReturnPremiumPlan.Name
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at PolicyDetailSummaryScreen.pcf: line 641, column 40
    function value_353 () : entity.Producer {
      return policyPeriod.PrimaryProducerCode.Producer
    }
    
    // 'value' attribute on TextInput (id=ProducerCode_Input) at PolicyDetailSummaryScreen.pcf: line 645, column 58
    function value_356 () : java.lang.String {
      return policyPeriod.PrimaryProducerCode.Code
    }
    
    // 'value' attribute on TypeKeyInput (id=BillingMethod_Input) at PolicyDetailSummaryScreen.pcf: line 652, column 57
    function value_372 () : typekey.PolicyPeriodBillingMethod {
      return policyPeriod.BillingMethod
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at PolicyDetailSummaryScreen.pcf: line 188, column 45
    function value_64 () : java.lang.String {
      return policyPeriod.DisplayName
    }
    
    // 'value' attribute on TextInput (id=LegacyPolicyNumber_Input) at PolicyDetailSummaryScreen.pcf: line 192, column 57
    function value_67 () : java.lang.String {
      return policyPeriod.LegacyPolicyNumber_TDIC
    }
    
    // 'value' attribute on TextInput (id=LegacyPolicySuffix_Input) at PolicyDetailSummaryScreen.pcf: line 196, column 57
    function value_70 () : java.lang.String {
      return policyPeriod.LegacyPolicySuffix_TDIC
    }
    
    // 'value' attribute on TextInput (id=LegacySourceSystem_Input) at PolicyDetailSummaryScreen.pcf: line 202, column 27
    function value_73 () : java.lang.String {
      return policyPeriod.LegacyPolicySource_TDIC
    }
    
    // 'value' attribute on TextInput (id=DBA_Input) at PolicyDetailSummaryScreen.pcf: line 206, column 37
    function value_76 () : java.lang.String {
      return policyPeriod.DBA
    }
    
    // 'value' attribute on TypeKeyInput (id=Product_Input) at PolicyDetailSummaryScreen.pcf: line 211, column 40
    function value_79 () : typekey.LOBCode {
      return policyPeriod.Policy.LOBCode
    }
    
    // 'value' attribute on TypeKeyInput (id=Offering_Input) at PolicyDetailSummaryScreen.pcf: line 217, column 79
    function value_83 () : typekey.Offering_TDIC {
      return policyPeriod.Offering_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=AssignedRisk_Input) at PolicyDetailSummaryScreen.pcf: line 221, column 46
    function value_87 () : java.lang.Boolean {
      return policyPeriod.AssignedRisk
    }
    
    // 'value' attribute on TypeKeyInput (id=RiskJurisdiction_Input) at PolicyDetailSummaryScreen.pcf: line 226, column 45
    function value_90 () : typekey.Jurisdiction {
      return policyPeriod.RiskJurisdiction
    }
    
    // 'value' attribute on TypeKeyInput (id=UWCompany_Input) at PolicyDetailSummaryScreen.pcf: line 231, column 42
    function value_93 () : typekey.UWCompany {
      return policyPeriod.UWCompany
    }
    
    // 'value' attribute on DateInput (id=PolicyPerEffDate_Input) at PolicyDetailSummaryScreen.pcf: line 235, column 50
    function value_96 () : java.util.Date {
      return policyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateInput (id=PolicyPerExpirDate_Input) at PolicyDetailSummaryScreen.pcf: line 239, column 52
    function value_99 () : java.util.Date {
      return policyPeriod.PolicyPerExpirDate
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at PolicyDetailSummaryScreen.pcf: line 289, column 44
    function verifyValueRangeIsAllowedType_140 ($$arg :  entity.SecurityZone[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at PolicyDetailSummaryScreen.pcf: line 289, column 44
    function verifyValueRangeIsAllowedType_140 ($$arg :  gw.api.database.IQueryBeanResult<entity.SecurityZone>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at PolicyDetailSummaryScreen.pcf: line 289, column 44
    function verifyValueRangeIsAllowedType_140 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at PolicyDetailSummaryScreen.pcf: line 623, column 47
    function verifyValueRangeIsAllowedType_341 ($$arg :  entity.DelinquencyPlan[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at PolicyDetailSummaryScreen.pcf: line 623, column 47
    function verifyValueRangeIsAllowedType_341 ($$arg :  gw.api.database.IQueryBeanResult<entity.DelinquencyPlan>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at PolicyDetailSummaryScreen.pcf: line 623, column 47
    function verifyValueRangeIsAllowedType_341 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at PolicyDetailSummaryScreen.pcf: line 289, column 44
    function verifyValueRange_141 () : void {
      var __valueRangeArg = allSecurityZones
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_140(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at PolicyDetailSummaryScreen.pcf: line 623, column 47
    function verifyValueRange_342 () : void {
      var __valueRangeArg = policyPeriod.getApplicableDelinquencyPlans()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_341(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=CancellationReason_Input) at PolicyDetailSummaryScreen.pcf: line 262, column 33
    function visible_114 () : java.lang.Boolean {
      return isCanceled
    }
    
    // 'visible' attribute on TextInput (id=ArchiveStatus_Input) at PolicyDetailSummaryScreen.pcf: line 282, column 76
    function visible_131 () : java.lang.Boolean {
      return gw.api.system.PLConfigParameters.ArchiveEnabled.Value
    }
    
    // 'visible' attribute on MenuItem (id=ChangeDirectBillInvoicingOverrides) at PolicyDetailSummaryScreen.pcf: line 320, column 91
    function visible_157 () : java.lang.Boolean {
      return !CurrentLocation.InEditMode && policyPeriod.isDirectBill()
    }
    
    // 'visible' attribute on MenuItem (id=ChangeListBillInvoicingOverrides) at PolicyDetailSummaryScreen.pcf: line 326, column 89
    function visible_161 () : java.lang.Boolean {
      return !CurrentLocation.InEditMode && policyPeriod.isListBill()
    }
    
    // 'visible' attribute on TextInput (id=ChangeInvoicingOverrides_Input) at PolicyDetailSummaryScreen.pcf: line 313, column 50
    function visible_164 () : java.lang.Boolean {
      return !policyPeriod.isAgencyBill()
    }
    
    // 'visible' attribute on TextInput (id=OverridingPayerAccount_Input) at PolicyDetailSummaryScreen.pcf: line 334, column 67
    function visible_168 () : java.lang.Boolean {
      return policyPeriod.OverridingPayerAccount != null 
    }
    
    // 'visible' attribute on TextInput (id=OverridingInvoiceStream_Input) at PolicyDetailSummaryScreen.pcf: line 340, column 98
    function visible_175 () : java.lang.Boolean {
      return (!policyPeriod.AgencyBill and policyPeriod.OverridingInvoiceStream != null)
    }
    
    // 'visible' attribute on TextInput (id=ADANumber_Input) at PolicyDetailSummaryScreen.pcf: line 357, column 91
    function visible_187 () : java.lang.Boolean {
      return policyPeriod.PrimaryInsured.Contact.ADANumberOfficialID_TDIC != null
    }
    
    // 'visible' attribute on ToolbarButton (id=ArchiveRetrieveButton) at PolicyDetailSummaryScreen.pcf: line 107, column 80
    function visible_19 () : java.lang.Boolean {
      return policyPeriod.Archived and perm.PolicyPeriod.archiveretrieve
    }
    
    // 'visible' attribute on ToolbarButton (id=OpenButton) at PolicyDetailSummaryScreen.pcf: line 113, column 80
    function visible_22 () : java.lang.Boolean {
      return policyPeriod.Closed && perm.PolicyPeriod.plcyreopen
    }
    
    // 'visible' attribute on InputSet at PolicyDetailSummaryScreen.pcf: line 408, column 90
    function visible_224 () : java.lang.Boolean {
      return gw.api.system.BCConfigParameters.EnableChargeProRataTxCreation.Value
    }
    
    // 'visible' attribute on DateInput (id=FullPayDate_Input) at PolicyDetailSummaryScreen.pcf: line 477, column 62
    function visible_237 () : java.lang.Boolean {
      return policyPeriod.EligibleForFullPayDiscount
    }
    
    // 'visible' attribute on ToolbarButton (id=StartDelinquencyButton) at PolicyDetailSummaryScreen.pcf: line 119, column 73
    function visible_25 () : java.lang.Boolean {
      return !policyPeriod.Closed and perm.System.manualDlnq_TDIC
    }
    
    // 'visible' attribute on BooleanRadioInput (id=RiskManagementDiscount_Input) at PolicyDetailSummaryScreen.pcf: line 495, column 81
    function visible_251 () : java.lang.Boolean {
      return policyPeriod.Policy.LOBCode == LOBCode.TC_GENERALLIABILITY
    }
    
    // 'visible' attribute on BooleanRadioInput (id=MultilineAll_Input) at PolicyDetailSummaryScreen.pcf: line 505, column 84
    function visible_261 () : java.lang.Boolean {
      return policyPeriod.RiskJurisdiction.Code == Jurisdiction.TC_CA.Code
    }
    
    // 'visible' attribute on BooleanRadioInput (id=MultilinePLCP_Input) at PolicyDetailSummaryScreen.pcf: line 510, column 199
    function visible_266 () : java.lang.Boolean {
      return (policyPeriod.Policy.LOBCode == LOBCode.TC_GENERALLIABILITY or policyPeriod.Policy.LOBCode == LOBCode.TC_BUSINESSOWNERS) and policyPeriod.RiskJurisdiction != Jurisdiction.TC_AK
    }
    
    // 'visible' attribute on BooleanRadioInput (id=MultilineWCPL_Input) at PolicyDetailSummaryScreen.pcf: line 515, column 137
    function visible_271 () : java.lang.Boolean {
      return policyPeriod.Policy.LOBCode == LOBCode.TC_GENERALLIABILITY and policyPeriod.RiskJurisdiction == Jurisdiction.TC_CA
    }
    
    // 'visible' attribute on BooleanRadioInput (id=MultilineWC_Input) at PolicyDetailSummaryScreen.pcf: line 520, column 135
    function visible_276 () : java.lang.Boolean {
      return policyPeriod.Policy.LOBCode == LOBCode.TC_WC7WORKERSCOMP and policyPeriod.RiskJurisdiction == Jurisdiction.TC_CA
    }
    
    // 'visible' attribute on BooleanRadioInput (id=EnableEquityWarnings_Input) at PolicyDetailSummaryScreen.pcf: line 577, column 81
    function visible_303 () : java.lang.Boolean {
      return gw.api.system.BCConfigParameters.EnableEquityWarnings.Value
    }
    
    // 'visible' attribute on DateInput (id=CancellationPending_Input) at PolicyDetailSummaryScreen.pcf: line 592, column 76
    function visible_318 () : java.lang.Boolean {
      return not isCanceled && not policyPeriod.AgencyBill
    }
    
    // 'editVisible' attribute on EditButtons at PolicyDetailSummaryScreen.pcf: line 127, column 45
    function visible_32 () : java.lang.Boolean {
      return !policyPeriod.Closed
    }
    
    // 'visible' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketAlertAlertBar) at PolicyDetailSummaryScreen.pcf: line 134, column 62
    function visible_35 () : java.lang.Boolean {
      return policyPeriod.Policy.HasActiveTroubleTickets
    }
    
    // 'visible' attribute on MenuItem (id=ChangeToAgencyBillAction) at PolicyDetailSummaryScreen.pcf: line 659, column 91
    function visible_360 () : java.lang.Boolean {
      return !policyPeriod.AgencyBill and !policyPeriod.hasFrozenInvoiceItems()
    }
    
    // 'visible' attribute on MenuItem (id=ChangeToDirectBillAction) at PolicyDetailSummaryScreen.pcf: line 666, column 91
    function visible_363 () : java.lang.Boolean {
      return !policyPeriod.DirectBill and !policyPeriod.hasFrozenInvoiceItems()
    }
    
    // 'visible' attribute on MenuItem (id=changeToListBill) at PolicyDetailSummaryScreen.pcf: line 673, column 89
    function visible_368 () : java.lang.Boolean {
      return !policyPeriod.ListBill and !policyPeriod.hasFrozenInvoiceItems()
    }
    
    // 'visible' attribute on PanelRef at PolicyDetailSummaryScreen.pcf: line 679, column 52
    function visible_375 () : java.lang.Boolean {
      return policyPeriod.PaymentPlan.Reporting
    }
    
    // 'visible' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketForPolicyPeriod) at PolicyDetailSummaryScreen.pcf: line 140, column 55
    function visible_40 () : java.lang.Boolean {
      return policyPeriod.HasActiveTroubleTickets
    }
    
    // 'visible' attribute on AlertBar (id=PolicyDetailSummary_DelinquencyAlertAlertBar) at PolicyDetailSummaryScreen.pcf: line 146, column 72
    function visible_45 () : java.lang.Boolean {
      return policyPeriod.hasActiveDelinquenciesOutOfGracePeriod()
    }
    
    // 'visible' attribute on AlertBar (id=PolicyDetailSummary_HeldChargesAlertBar) at PolicyDetailSummaryScreen.pcf: line 152, column 48
    function visible_50 () : java.lang.Boolean {
      return policyPeriod.hasHeldCharges()
    }
    
    // 'visible' attribute on AlertBar (id=ArchivedAlert) at PolicyDetailSummaryScreen.pcf: line 157, column 40
    function visible_53 () : java.lang.Boolean {
      return policyPeriod.Archived
    }
    
    // 'visible' attribute on AlertBar (id=ArchivedAndRetrievedAlert) at PolicyDetailSummaryScreen.pcf: line 161, column 41
    function visible_55 () : java.lang.Boolean {
      return policyPeriod.Retrieved
    }
    
    // 'visible' attribute on AlertBar (id=PolicyDetailSummary_ActivitiesAlertBar) at PolicyDetailSummaryScreen.pcf: line 166, column 121
    function visible_56 () : java.lang.Boolean {
      return policyPeriod.Activities_Ext.where(\ a -> a.Status == typekey.ActivityStatus.TC_OPEN ).length > 0
    }
    
    // 'visible' attribute on AlertBar (id=PolicyDetailSummary_NewDentistProgramDiscount) at PolicyDetailSummaryScreen.pcf: line 170, column 54
    function visible_59 () : java.lang.Boolean {
      return policyPeriod.NewDentistProgram_TDIC
    }
    
    // 'visible' attribute on AlertBar (id=PolicyDetailSummary_ServiceMembersCivilReliefAct) at PolicyDetailSummaryScreen.pcf: line 174, column 62
    function visible_60 () : java.lang.Boolean {
      return policyPeriod.ServiceMembCivilReliefAct_TDIC
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailSummary_PaymentRequestAlertBar) at PolicyDetailSummaryScreen.pcf: line 179, column 179
    function visible_61 () : java.lang.Boolean {
      return policyPeriod.Account.PaymentRequests.where(\ a -> a.Status == PaymentRequestStatus.TC_CREATED or a.Status == PaymentRequestStatus.TC_REQUESTED).length > 0
    }
    
    // 'visible' attribute on TypeKeyInput (id=Offering_Input) at PolicyDetailSummaryScreen.pcf: line 217, column 79
    function visible_82 () : java.lang.Boolean {
      return policyPeriod.Policy.LOBCode != LOBCode.TC_WC7WORKERSCOMP
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get allSecurityZones () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return getVariableValue("allSecurityZones", 0) as gw.api.database.IQueryBeanResult<SecurityZone>
    }
    
    property set allSecurityZones ($arg :  gw.api.database.IQueryBeanResult<SecurityZone>) {
      setVariableValue("allSecurityZones", 0, $arg)
    }
    
    property get archiveState () : String {
      return getVariableValue("archiveState", 0) as String
    }
    
    property set archiveState ($arg :  String) {
      setVariableValue("archiveState", 0, $arg)
    }
    
    property get cancellationEvent () : DelinquencyProcessEvent {
      return getVariableValue("cancellationEvent", 0) as DelinquencyProcessEvent
    }
    
    property set cancellationEvent ($arg :  DelinquencyProcessEvent) {
      setVariableValue("cancellationEvent", 0, $arg)
    }
    
    property get daysTillPaidThroughDate () : Number {
      return getVariableValue("daysTillPaidThroughDate", 0) as Number
    }
    
    property set daysTillPaidThroughDate ($arg :  Number) {
      setVariableValue("daysTillPaidThroughDate", 0, $arg)
    }
    
    property get earnedPremium () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("earnedPremium", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set earnedPremium ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("earnedPremium", 0, $arg)
    }
    
    property get isCanceled () : boolean {
      return getVariableValue("isCanceled", 0) as java.lang.Boolean
    }
    
    property set isCanceled ($arg :  boolean) {
      setVariableValue("isCanceled", 0, $arg)
    }
    
    property get paidAmount () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("paidAmount", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set paidAmount ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("paidAmount", 0, $arg)
    }
    
    property get paidThroughDate () : java.util.Date {
      return getVariableValue("paidThroughDate", 0) as java.util.Date
    }
    
    property set paidThroughDate ($arg :  java.util.Date) {
      setVariableValue("paidThroughDate", 0, $arg)
    }
    
    property get policyEarnedAmount () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("policyEarnedAmount", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set policyEarnedAmount ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("policyEarnedAmount", 0, $arg)
    }
    
    property get policyEquity () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("policyEquity", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set policyEquity ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("policyEquity", 0, $arg)
    }
    
    property get policyEquityPercent () : Number {
      return getVariableValue("policyEquityPercent", 0) as Number
    }
    
    property set policyEquityPercent ($arg :  Number) {
      setVariableValue("policyEquityPercent", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get policyPeriodBalances () : gw.web.policy.PolicySummaryFinancialsHelper {
      return getVariableValue("policyPeriodBalances", 0) as gw.web.policy.PolicySummaryFinancialsHelper
    }
    
    property set policyPeriodBalances ($arg :  gw.web.policy.PolicySummaryFinancialsHelper) {
      setVariableValue("policyPeriodBalances", 0, $arg)
    }
    
    property get policyTotalEquityChargeValue () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("policyTotalEquityChargeValue", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set policyTotalEquityChargeValue ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("policyTotalEquityChargeValue", 0, $arg)
    }
    
    property get premium () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("premium", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set premium ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("premium", 0, $arg)
    }
    
    property get totalBilled () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("totalBilled", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set totalBilled ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("totalBilled", 0, $arg)
    }
    
    property get totalEarned () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("totalEarned", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set totalEarned ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("totalEarned", 0, $arg)
    }
    
    property get totalPaid () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("totalPaid", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set totalPaid ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("totalPaid", 0, $arg)
    }
    
    property get totalValueToday () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("totalValueToday", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set totalValueToday ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("totalValueToday", 0, $arg)
    }
    
    
    
    function changeBillingMethodToAgencyBill() {
      policyPeriod.validateCanChangeToAgencyBill()
      ChangeBillingMethodToAgencyBillPopup.push(policyPeriod)
    }
    
    property get DelinquencyStatus() : String {
      var earliestProcess = policyPeriod.EarliestActiveDelinquencyProcess
      var eventApproval = earliestProcess.NextEvent.ApprovalActivity
      if ( eventApproval.Mandatory and
           eventApproval.Approved == null ) {
        var status = eventApproval.ShortSubject
        if ( status == null ) {
          status = eventApproval.Subject
        }
        return status
      }
      return earliestProcess.PreviousEvent.EventName.Description
    }
    
    function EditAvail() :boolean
    {
      //print()
      return User.util.CurrentUser.Credential?.DisplayName != 'su'
    
    }
    
    function reopenPolicy() {
      gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
        var plcyPeriod = bundle.add(policyPeriod)
        plcyPeriod.reopenIfClosed()
      })
    }
    
    function calculatePointsForDisplay(value : MonetaryAmount, totalValue : MonetaryAmount) : BigDecimal {
      return Percentage.fromDisplaySafeRatioBetween(value, totalValue).Points.resolve(4, HALF_UP)
    }
    
    
  }
  
  
}