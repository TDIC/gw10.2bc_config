package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.account.CreateDisbursementWizardHelper
@javax.annotation.Generated("config/web/pcf/disbursement/CreateDisbursementWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreateDisbursementWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/disbursement/CreateDisbursementWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreateDisbursementWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=CreateDisbursementWizard) at CreateDisbursementWizard.pcf: line 12, column 35
    function afterCancel_10 () : void {
      DesktopDisbursements.go()
    }
    
    // 'afterCancel' attribute on Wizard (id=CreateDisbursementWizard) at CreateDisbursementWizard.pcf: line 12, column 35
    function afterCancel_dest_11 () : pcf.api.Destination {
      return pcf.DesktopDisbursements.createDestination()
    }
    
    // 'afterFinish' attribute on Wizard (id=CreateDisbursementWizard) at CreateDisbursementWizard.pcf: line 12, column 35
    function afterFinish_16 () : void {
      DesktopDisbursements.go()
    }
    
    // 'afterFinish' attribute on Wizard (id=CreateDisbursementWizard) at CreateDisbursementWizard.pcf: line 12, column 35
    function afterFinish_dest_17 () : pcf.api.Destination {
      return pcf.DesktopDisbursements.createDestination()
    }
    
    // 'allowNext' attribute on WizardStep (id=Step1) at CreateDisbursementWizard.pcf: line 31, column 90
    function allowNext_1 () : java.lang.Boolean {
      return tAccountOwnerReference.TAccountOwner != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=CreateDisbursementWizard) at CreateDisbursementWizard.pcf: line 12, column 35
    function beforeCommit_12 (pickedValue :  java.lang.Object) : void {
      disbursement.executeDisbursementOrCreateApprovalActivityIfNeeded()
    }
    
    // 'canVisit' attribute on Wizard (id=CreateDisbursementWizard) at CreateDisbursementWizard.pcf: line 12, column 35
    static function canVisit_13 () : java.lang.Boolean {
      return perm.Transaction.disbcreate
    }
    
    // 'initialValue' attribute on Variable at CreateDisbursementWizard.pcf: line 21, column 60
    function initialValue_0 () : gw.api.web.accounting.TAccountOwnerReference {
      return new gw.api.web.accounting.TAccountOwnerReference()
    }
    
    // 'onExit' attribute on WizardStep (id=Step1) at CreateDisbursementWizard.pcf: line 31, column 90
    function onExit_2 () : void {
      setUpDisbursement()
    }
    
    // 'onExit' attribute on WizardStep (id=Step2) at CreateDisbursementWizard.pcf: line 37, column 90
    function onExit_5 () : void {
      disbursement.initiateApprovalActivityIfUserLacksAuthority()
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at CreateDisbursementWizard.pcf: line 31, column 90
    function screen_onEnter_3 (def :  pcf.WizardsStep1AccountPolicySearchScreen) : void {
      def.onEnter(tAccountOwnerReference, TAccountOwnerType.TC_ACCOUNT, false, true)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at CreateDisbursementWizard.pcf: line 37, column 90
    function screen_onEnter_6 (def :  pcf.CreateDisbursementDetailScreen) : void {
      def.onEnter(disbursement)
    }
    
    // 'screen' attribute on WizardStep (id=Step3) at CreateDisbursementWizard.pcf: line 42, column 90
    function screen_onEnter_8 (def :  pcf.CreateDisbursementConfirmScreen) : void {
      def.onEnter(disbursement)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at CreateDisbursementWizard.pcf: line 31, column 90
    function screen_refreshVariables_4 (def :  pcf.WizardsStep1AccountPolicySearchScreen) : void {
      def.refreshVariables(tAccountOwnerReference, TAccountOwnerType.TC_ACCOUNT, false, true)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at CreateDisbursementWizard.pcf: line 37, column 90
    function screen_refreshVariables_7 (def :  pcf.CreateDisbursementDetailScreen) : void {
      def.refreshVariables(disbursement)
    }
    
    // 'screen' attribute on WizardStep (id=Step3) at CreateDisbursementWizard.pcf: line 42, column 90
    function screen_refreshVariables_9 (def :  pcf.CreateDisbursementConfirmScreen) : void {
      def.refreshVariables(disbursement)
    }
    
    // 'tabBar' attribute on Wizard (id=CreateDisbursementWizard) at CreateDisbursementWizard.pcf: line 12, column 35
    function tabBar_onEnter_14 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=CreateDisbursementWizard) at CreateDisbursementWizard.pcf: line 12, column 35
    function tabBar_refreshVariables_15 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.CreateDisbursementWizard {
      return super.CurrentLocation as pcf.CreateDisbursementWizard
    }
    
    property get disbursement () : AccountDisbursement {
      return getVariableValue("disbursement", 0) as AccountDisbursement
    }
    
    property set disbursement ($arg :  AccountDisbursement) {
      setVariableValue("disbursement", 0, $arg)
    }
    
    property get relatedEntity () : KeyableBean {
      return getVariableValue("relatedEntity", 0) as KeyableBean
    }
    
    property set relatedEntity ($arg :  KeyableBean) {
      setVariableValue("relatedEntity", 0, $arg)
    }
    
    property get tAccountOwnerReference () : gw.api.web.accounting.TAccountOwnerReference {
      return getVariableValue("tAccountOwnerReference", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set tAccountOwnerReference ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setVariableValue("tAccountOwnerReference", 0, $arg)
    }
    
    
    function setUpDisbursement() {
      var account = tAccountOwnerReference.TAccountOwner as Account
      if (disbursement == null) {
        disbursement = CreateDisbursementWizardHelper.createDisbursement(account)
      }
      else if (disbursement.Currency != account.Currency)
      {
        disbursement.remove()
        disbursement = CreateDisbursementWizardHelper.createDisbursement(account)
      }
    }
    
    
  }
  
  
}