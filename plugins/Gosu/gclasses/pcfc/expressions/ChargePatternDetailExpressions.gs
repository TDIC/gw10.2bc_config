package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/chargepatterns/ChargePatternDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChargePatternDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/chargepatterns/ChargePatternDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChargePatternDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (chargePattern :  ChargePattern) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=ChargePatternDetail) at ChargePatternDetail.pcf: line 11, column 123
    function canEdit_5 () : java.lang.Boolean {
      return perm.System.chargepatternedit
    }
    
    // 'canVisit' attribute on Page (id=ChargePatternDetail) at ChargePatternDetail.pcf: line 11, column 123
    static function canVisit_6 (chargePattern :  ChargePattern) : java.lang.Boolean {
      return perm.System.admintabview and perm.System.chargepatternview
    }
    
    // 'def' attribute on PanelRef at ChargePatternDetail.pcf: line 23, column 53
    function def_onEnter_1 (def :  pcf.ChargePatternDetailDV) : void {
      def.onEnter(chargePattern)
    }
    
    // 'def' attribute on PanelRef at ChargePatternDetail.pcf: line 25, column 146
    function def_onEnter_3 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(chargePattern, { "ChargeName"}, { DisplayKey.get("Web.ChargePatternDetailDV.ChargeName") })
    }
    
    // 'def' attribute on PanelRef at ChargePatternDetail.pcf: line 23, column 53
    function def_refreshVariables_2 (def :  pcf.ChargePatternDetailDV) : void {
      def.refreshVariables(chargePattern)
    }
    
    // 'def' attribute on PanelRef at ChargePatternDetail.pcf: line 25, column 146
    function def_refreshVariables_4 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(chargePattern, { "ChargeName"}, { DisplayKey.get("Web.ChargePatternDetailDV.ChargeName") })
    }
    
    // EditButtons at ChargePatternDetail.pcf: line 20, column 23
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'parent' attribute on Page (id=ChargePatternDetail) at ChargePatternDetail.pcf: line 11, column 123
    static function parent_7 (chargePattern :  ChargePattern) : pcf.api.Destination {
      return pcf.ChargePatterns.createDestination()
    }
    
    // 'title' attribute on Page (id=ChargePatternDetail) at ChargePatternDetail.pcf: line 11, column 123
    static function title_8 (chargePattern :  ChargePattern) : java.lang.Object {
      return DisplayKey.get("Web.ChargePatternDetail.Title", chargePattern.ChargeName, chargePattern.ChargeCode)
    }
    
    override property get CurrentLocation () : pcf.ChargePatternDetail {
      return super.CurrentLocation as pcf.ChargePatternDetail
    }
    
    property get chargePattern () : ChargePattern {
      return getVariableValue("chargePattern", 0) as ChargePattern
    }
    
    property set chargePattern ($arg :  ChargePattern) {
      setVariableValue("chargePattern", 0, $arg)
    }
    
    
  }
  
  
}