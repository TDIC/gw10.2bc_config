package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/ListBillPaymentPlansPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ListBillPaymentPlansPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/ListBillPaymentPlansPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListBillPaymentPlansPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'def' attribute on ListViewInput (id=PaymentPlansLV) at ListBillPaymentPlansPopup.pcf: line 20, column 31
    function def_onEnter_2 (def :  pcf.AccountPaymentPlanLV) : void {
      def.onEnter(account)
    }
    
    // 'def' attribute on ListViewInput (id=PaymentPlansLV) at ListBillPaymentPlansPopup.pcf: line 20, column 31
    function def_refreshVariables_3 (def :  pcf.AccountPaymentPlanLV) : void {
      def.refreshVariables(account)
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at ListBillPaymentPlansPopup.pcf: line 27, column 84
    function filters_0 () : gw.api.filters.IFilter[] {
      return new gw.api.web.plan.PlanListFilterSet().FilterOptions
    }
    
    // EditButtons at ListBillPaymentPlansPopup.pcf: line 29, column 29
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    override property get CurrentLocation () : pcf.ListBillPaymentPlansPopup {
      return super.CurrentLocation as pcf.ListBillPaymentPlansPopup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  
}