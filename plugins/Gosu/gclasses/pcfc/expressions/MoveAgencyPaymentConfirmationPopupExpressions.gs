package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/MoveAgencyPaymentConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class MoveAgencyPaymentConfirmationPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/MoveAgencyPaymentConfirmationPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class MoveAgencyPaymentConfirmationPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (moneyReceived :  AgencyBillMoneyRcvd) : int {
      return 0
    }
    
    // 'action' attribute on MenuItem (id=SelectProducerPicker) at MoveAgencyPaymentConfirmationPopup.pcf: line 41, column 43
    function action_1 () : void {
      ProducerSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=SelectProducerPicker) at MoveAgencyPaymentConfirmationPopup.pcf: line 41, column 43
    function action_dest_2 () : pcf.api.Destination {
      return pcf.ProducerSearchPopup.createDestination()
    }
    
    // 'beforeCommit' attribute on Popup (id=MoveAgencyPaymentConfirmationPopup) at MoveAgencyPaymentConfirmationPopup.pcf: line 10, column 86
    function beforeCommit_7 (pickedValue :  java.lang.Object) : void {
      moneyReceived.moveToProducer(targetProducer)
    }
    
    // 'value' attribute on TextInput (id=SelectProducer_Input) at MoveAgencyPaymentConfirmationPopup.pcf: line 41, column 43
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      targetProducer = (__VALUE_TO_SET as entity.Producer)
    }
    
    // 'initialValue' attribute on Variable at MoveAgencyPaymentConfirmationPopup.pcf: line 22, column 59
    function initialValue_0 () : gw.api.web.producer.ProducerSearchConverter {
      return new gw.api.web.producer.ProducerSearchConverter()
    }
    
    // 'inputConversion' attribute on TextInput (id=SelectProducer_Input) at MoveAgencyPaymentConfirmationPopup.pcf: line 41, column 43
    function inputConversion_3 (VALUE :  java.lang.String) : java.lang.Object {
      return producerSearchConverter.getProducer(VALUE)
    }
    
    // 'value' attribute on TextInput (id=SelectProducer_Input) at MoveAgencyPaymentConfirmationPopup.pcf: line 41, column 43
    function value_4 () : entity.Producer {
      return targetProducer
    }
    
    override property get CurrentLocation () : pcf.MoveAgencyPaymentConfirmationPopup {
      return super.CurrentLocation as pcf.MoveAgencyPaymentConfirmationPopup
    }
    
    property get moneyReceived () : AgencyBillMoneyRcvd {
      return getVariableValue("moneyReceived", 0) as AgencyBillMoneyRcvd
    }
    
    property set moneyReceived ($arg :  AgencyBillMoneyRcvd) {
      setVariableValue("moneyReceived", 0, $arg)
    }
    
    property get producerSearchConverter () : gw.api.web.producer.ProducerSearchConverter {
      return getVariableValue("producerSearchConverter", 0) as gw.api.web.producer.ProducerSearchConverter
    }
    
    property set producerSearchConverter ($arg :  gw.api.web.producer.ProducerSearchConverter) {
      setVariableValue("producerSearchConverter", 0, $arg)
    }
    
    property get targetProducer () : Producer {
      return getVariableValue("targetProducer", 0) as Producer
    }
    
    property set targetProducer ($arg :  Producer) {
      setVariableValue("targetProducer", 0, $arg)
    }
    
    
  }
  
  
}