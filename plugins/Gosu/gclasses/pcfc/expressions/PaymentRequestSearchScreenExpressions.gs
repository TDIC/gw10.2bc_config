package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/PaymentRequestSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentRequestSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/search/PaymentRequestSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TypeKeyCell (id=Status_Cell) at PaymentRequestSearchScreen.pcf: line 64, column 59
    function action_27 () : void {
      PaymentRequestDetailPage.go(paymentRequestSearchView.PaymentRequest, false)
    }
    
    // 'action' attribute on TypeKeyCell (id=Status_Cell) at PaymentRequestSearchScreen.pcf: line 64, column 59
    function action_dest_28 () : pcf.api.Destination {
      return pcf.PaymentRequestDetailPage.createDestination(paymentRequestSearchView.PaymentRequest, false)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at PaymentRequestSearchScreen.pcf: line 53, column 104
    function currency_22 () : typekey.Currency {
      return paymentRequestSearchView.Currency
    }
    
    // 'value' attribute on DateCell (id=CreateTime_Cell) at PaymentRequestSearchScreen.pcf: line 35, column 62
    function valueRoot_13 () : java.lang.Object {
      return paymentRequestSearchView
    }
    
    // 'value' attribute on TypeKeyCell (id=State_Cell) at PaymentRequestSearchScreen.pcf: line 82, column 51
    function valueRoot_42 () : java.lang.Object {
      return paymentRequestSearchView.PaymentRequest.Invoice.InvoiceStream.Policy.LatestPolicyPeriod
    }
    
    // 'value' attribute on TypeKeyCell (id=ProductLine_Cell) at PaymentRequestSearchScreen.pcf: line 88, column 46
    function valueRoot_45 () : java.lang.Object {
      return paymentRequestSearchView.PaymentRequest.Invoice.InvoiceStream.Policy
    }
    
    // 'value' attribute on DateCell (id=CreateTime_Cell) at PaymentRequestSearchScreen.pcf: line 35, column 62
    function value_12 () : java.util.Date {
      return paymentRequestSearchView.CreateTime
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at PaymentRequestSearchScreen.pcf: line 41, column 31
    function value_15 () : java.lang.String {
      return paymentRequestSearchView.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=AccountName_Cell) at PaymentRequestSearchScreen.pcf: line 47, column 31
    function value_18 () : java.lang.String {
      return paymentRequestSearchView.AccountName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at PaymentRequestSearchScreen.pcf: line 53, column 104
    function value_21 () : gw.pl.currency.MonetaryAmount {
      return paymentRequestSearchView.Amount.ofCurrency(paymentRequestSearchView.Currency)
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentMethod_Cell) at PaymentRequestSearchScreen.pcf: line 58, column 52
    function value_24 () : typekey.PaymentMethod {
      return paymentRequestSearchView.PaymentMethod
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at PaymentRequestSearchScreen.pcf: line 64, column 59
    function value_29 () : typekey.PaymentRequestStatus {
      return paymentRequestSearchView.Status
    }
    
    // 'value' attribute on DateCell (id=DraftDate_Cell) at PaymentRequestSearchScreen.pcf: line 68, column 61
    function value_32 () : java.util.Date {
      return paymentRequestSearchView.DraftDate
    }
    
    // 'value' attribute on DateCell (id=RequestDate_Cell) at PaymentRequestSearchScreen.pcf: line 72, column 63
    function value_35 () : java.util.Date {
      return paymentRequestSearchView.RequestDate
    }
    
    // 'value' attribute on DateCell (id=StatusDate_Cell) at PaymentRequestSearchScreen.pcf: line 76, column 62
    function value_38 () : java.util.Date {
      return paymentRequestSearchView.StatusDate
    }
    
    // 'value' attribute on TypeKeyCell (id=State_Cell) at PaymentRequestSearchScreen.pcf: line 82, column 51
    function value_41 () : typekey.Jurisdiction {
      return paymentRequestSearchView.PaymentRequest.Invoice.InvoiceStream.Policy.LatestPolicyPeriod.RiskJurisdiction
    }
    
    // 'value' attribute on TypeKeyCell (id=ProductLine_Cell) at PaymentRequestSearchScreen.pcf: line 88, column 46
    function value_44 () : typekey.LOBCode {
      return paymentRequestSearchView.PaymentRequest.Invoice.InvoiceStream.Policy.LOBCode
    }
    
    property get paymentRequestSearchView () : entity.PaymentRequestSearchView {
      return getIteratedValue(2) as entity.PaymentRequestSearchView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/PaymentRequestSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentRequestSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/PaymentRequestSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends PaymentRequestSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at PaymentRequestSearchScreen.pcf: line 17, column 54
    function def_onEnter_0 (def :  pcf.PaymentRequestSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at PaymentRequestSearchScreen.pcf: line 17, column 54
    function def_refreshVariables_1 (def :  pcf.PaymentRequestSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'maxSearchResults' attribute on SearchPanel at PaymentRequestSearchScreen.pcf: line 15, column 92
    function maxSearchResults_48 () : java.lang.Object {
      return gw.api.system.BCConfigParameters.MaxSearchResults.Value
    }
    
    // 'searchCriteria' attribute on SearchPanel at PaymentRequestSearchScreen.pcf: line 15, column 92
    function searchCriteria_50 () : gw.search.PaymentRequestSearchCriteria {
      return new gw.search.PaymentRequestSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at PaymentRequestSearchScreen.pcf: line 15, column 92
    function search_49 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria) as gw.api.database.IQueryBeanResult<PaymentRequestSearchView>
    }
    
    // 'value' attribute on DateCell (id=RequestDate_Cell) at PaymentRequestSearchScreen.pcf: line 72, column 63
    function sortValue_10 (paymentRequestSearchView :  entity.PaymentRequestSearchView) : java.lang.Object {
      return paymentRequestSearchView.RequestDate
    }
    
    // 'value' attribute on DateCell (id=StatusDate_Cell) at PaymentRequestSearchScreen.pcf: line 76, column 62
    function sortValue_11 (paymentRequestSearchView :  entity.PaymentRequestSearchView) : java.lang.Object {
      return paymentRequestSearchView.StatusDate
    }
    
    // 'value' attribute on DateCell (id=CreateTime_Cell) at PaymentRequestSearchScreen.pcf: line 35, column 62
    function sortValue_2 (paymentRequestSearchView :  entity.PaymentRequestSearchView) : java.lang.Object {
      return paymentRequestSearchView.CreateTime
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at PaymentRequestSearchScreen.pcf: line 41, column 31
    function sortValue_3 (paymentRequestSearchView :  entity.PaymentRequestSearchView) : java.lang.Object {
      return paymentRequestSearchView.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=AccountName_Cell) at PaymentRequestSearchScreen.pcf: line 47, column 31
    function sortValue_4 (paymentRequestSearchView :  entity.PaymentRequestSearchView) : java.lang.Object {
      return paymentRequestSearchView.AccountName
    }
    
    // 'sortBy' attribute on MonetaryAmountCell (id=Amount_Cell) at PaymentRequestSearchScreen.pcf: line 53, column 104
    function sortValue_5 (paymentRequestSearchView :  entity.PaymentRequestSearchView) : java.lang.Object {
      return paymentRequestSearchView.Currency
    }
    
    // 'sortBy' attribute on MonetaryAmountCell (id=Amount_Cell) at PaymentRequestSearchScreen.pcf: line 53, column 104
    function sortValue_6 (paymentRequestSearchView :  entity.PaymentRequestSearchView) : java.lang.Object {
      return  paymentRequestSearchView.Amount
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentMethod_Cell) at PaymentRequestSearchScreen.pcf: line 58, column 52
    function sortValue_7 (paymentRequestSearchView :  entity.PaymentRequestSearchView) : java.lang.Object {
      return paymentRequestSearchView.PaymentMethod
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at PaymentRequestSearchScreen.pcf: line 64, column 59
    function sortValue_8 (paymentRequestSearchView :  entity.PaymentRequestSearchView) : java.lang.Object {
      return paymentRequestSearchView.Status
    }
    
    // 'value' attribute on DateCell (id=DraftDate_Cell) at PaymentRequestSearchScreen.pcf: line 68, column 61
    function sortValue_9 (paymentRequestSearchView :  entity.PaymentRequestSearchView) : java.lang.Object {
      return paymentRequestSearchView.DraftDate
    }
    
    // 'value' attribute on RowIterator at PaymentRequestSearchScreen.pcf: line 30, column 97
    function value_47 () : gw.api.database.IQueryBeanResult<entity.PaymentRequestSearchView> {
      return paymentRequestSearchViews
    }
    
    property get paymentRequestSearchViews () : gw.api.database.IQueryBeanResult<PaymentRequestSearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<PaymentRequestSearchView>
    }
    
    property get searchCriteria () : gw.search.PaymentRequestSearchCriteria {
      return getCriteriaValue(1) as gw.search.PaymentRequestSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.PaymentRequestSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}