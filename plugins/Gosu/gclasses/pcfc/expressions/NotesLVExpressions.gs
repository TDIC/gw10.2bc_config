package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/note/NotesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NotesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/note/NotesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends NotesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=Author_Input) at NotesLV.pcf: line 46, column 38
    function valueRoot_1 () : java.lang.Object {
      return note
    }
    
    // 'value' attribute on TextInput (id=SecurityType_Input) at NotesLV.pcf: line 58, column 52
    function valueRoot_7 () : java.lang.Object {
      return note.SecurityType
    }
    
    // 'value' attribute on TextInput (id=Author_Input) at NotesLV.pcf: line 46, column 38
    function value_0 () : entity.User {
      return note.Author
    }
    
    // 'value' attribute on TextInput (id=AuthoringDate_Input) at NotesLV.pcf: line 79, column 187
    function value_14 () : java.lang.String {
      return gw.api.util.StringUtil.formatDate(note.AuthoringDate, "medium") + " " + gw.api.util.StringUtil.formatTime(note.AuthoringDate, "short")
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at NotesLV.pcf: line 84, column 45
    function value_17 () : java.lang.String {
      return note.Subject
    }
    
    // 'value' attribute on NoteBodyInput (id=Body_Input) at NotesLV.pcf: line 87, column 32
    function value_21 () : java.lang.String {
      return note.Body
    }
    
    // 'value' attribute on TextInput (id=Topic_Input) at NotesLV.pcf: line 53, column 48
    function value_3 () : typekey.NoteTopicType {
      return note.Topic
    }
    
    // 'value' attribute on TextInput (id=SecurityType_Input) at NotesLV.pcf: line 58, column 52
    function value_6 () : java.lang.String {
      return note.SecurityType.DisplayName
    }
    
    // 'value' attribute on TextInput (id=RelatedTo_Input) at NotesLV.pcf: line 65, column 44
    function value_9 () : typekey.RelatedTo {
      return note.RelatedTo
    }
    
    // 'visible' attribute on TextInput (id=Confidential_Input) at NotesLV.pcf: line 70, column 42
    function visible_12 () : java.lang.Boolean {
      return note.Confidential
    }
    
    // 'visible' attribute on TextInput (id=Subject_Input) at NotesLV.pcf: line 84, column 45
    function visible_16 () : java.lang.Boolean {
      return note.Subject != null
    }
    
    property get note () : entity.Note {
      return getIteratedValue(1) as entity.Note
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/note/NotesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NotesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RowIterator at NotesLV.pcf: line 21, column 71
    function value_24 () : gw.api.database.IQueryBeanResult<entity.Note> {
      return notes
    }
    
    property get docContainer () : DocumentContainer {
      return getRequireValue("docContainer", 0) as DocumentContainer
    }
    
    property set docContainer ($arg :  DocumentContainer) {
      setRequireValue("docContainer", 0, $arg)
    }
    
    property get notes () : gw.api.database.IQueryBeanResult<Note> {
      return getRequireValue("notes", 0) as gw.api.database.IQueryBeanResult<Note>
    }
    
    property set notes ($arg :  gw.api.database.IQueryBeanResult<Note>) {
      setRequireValue("notes", 0, $arg)
    }
    
    
  }
  
  
}