package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.acc.acct360.accountview.AccountBalanceTxnUtil
@javax.annotation.Generated("config/web/pcf/policy/ChangeBillingMethodToAgencyBillPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChangeBillingMethodToAgencyBillPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/ChangeBillingMethodToAgencyBillPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChangeBillingMethodToAgencyBillPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=ChangeBillingMethodToAgencyBillPopup) at ChangeBillingMethodToAgencyBillPopup.pcf: line 10, column 77
    function beforeCommit_10 (pickedValue :  java.lang.Object) : void {
      performAcct360Tracking();policyPeriod.changeBillingMethodToAgencyBill(reversePayments); gw.acc.acct360.accountview.AccountBalanceTxnUtil.processAgencyBillOffsetItems(policyPeriod)
    }
    
    // 'value' attribute on RangeInput (id=reversePayments_Input) at ChangeBillingMethodToAgencyBillPopup.pcf: line 53, column 63
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      reversePayments = (__VALUE_TO_SET as gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem)
    }
    
    // 'initialValue' attribute on Variable at ChangeBillingMethodToAgencyBillPopup.pcf: line 19, column 74
    function initialValue_0 () : gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem {
      return gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem.No
    }
    
    // 'valueRange' attribute on RangeInput (id=reversePayments_Input) at ChangeBillingMethodToAgencyBillPopup.pcf: line 53, column 63
    function valueRange_5 () : java.lang.Object {
      return gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem.values()
    }
    
    // 'value' attribute on RangeInput (id=reversePayments_Input) at ChangeBillingMethodToAgencyBillPopup.pcf: line 53, column 63
    function value_3 () : gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem {
      return reversePayments
    }
    
    // 'valueRange' attribute on RangeInput (id=reversePayments_Input) at ChangeBillingMethodToAgencyBillPopup.pcf: line 53, column 63
    function verifyValueRangeIsAllowedType_6 ($$arg :  gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=reversePayments_Input) at ChangeBillingMethodToAgencyBillPopup.pcf: line 53, column 63
    function verifyValueRangeIsAllowedType_6 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=reversePayments_Input) at ChangeBillingMethodToAgencyBillPopup.pcf: line 53, column 63
    function verifyValueRange_7 () : void {
      var __valueRangeArg = gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem.values()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_6(__valueRangeArg)
    }
    
    // 'visible' attribute on AlertBar (id=ChangeCommissionReservedAlert) at ChangeBillingMethodToAgencyBillPopup.pcf: line 34, column 48
    function visible_1 () : java.lang.Boolean {
      return policyPeriod.isDirectBill()
    }
    
    // 'visible' attribute on RangeInput (id=reversePayments_Input) at ChangeBillingMethodToAgencyBillPopup.pcf: line 53, column 63
    function visible_2 () : java.lang.Boolean {
      return policyPeriod.isPaymentHasBeenApplied()
    }
    
    override property get CurrentLocation () : pcf.ChangeBillingMethodToAgencyBillPopup {
      return super.CurrentLocation as pcf.ChangeBillingMethodToAgencyBillPopup
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get reversePayments () : gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem {
      return getVariableValue("reversePayments", 0) as gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem
    }
    
    property set reversePayments ($arg :  gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem) {
      setVariableValue("reversePayments", 0, $arg)
    }
    
    
        function performAcct360Tracking() {
          AccountBalanceTxnUtil.createMoveItemRecord(policyPeriod.Account, policyPeriod.InvoiceItems.where(\ii -> ii.Invoice.BilledOrDue
            && !ii.AgencyBill
            && ii.Payer == policyPeriod.Account
            && !ii.Reversal
            && !ii.Reversed), AcctBalItemMovedType_Ext.TC_DBTOABMOVE)
    
          // create the transfer record to move the balance
          AccountBalanceTxnUtil.createDBToABTransferRecord(policyPeriod)
        }
    
    
  }
  
  
}