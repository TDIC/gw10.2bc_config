package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralChargePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CollateralChargePopupExpressions {
  @javax.annotation.Generated("config/web/pcf/collateral/CollateralChargePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CollateralChargePopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (chargePatternOwnerPair :  com.google.gdata.util.common.base.Pair<ChargePattern, TAccountOwner>, collateralUtil :  gw.api.web.account.CollateralUtil) : int {
      return 0
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=remainingAmount_Input) at CollateralChargePopup.pcf: line 36, column 80
    function currency_6 () : typekey.Currency {
      return chargePatternOwnerPair.Second.Currency
    }
    
    // 'initialValue' attribute on Variable at CollateralChargePopup.pcf: line 19, column 36
    function initialValue_0 () : entity.ChargePattern {
      return chargePatternOwnerPair.First
    }
    
    // 'initialValue' attribute on Variable at CollateralChargePopup.pcf: line 23, column 36
    function initialValue_1 () : entity.TAccountOwner {
      return chargePatternOwnerPair.Second
    }
    
    // 'value' attribute on TextInput (id=chargePatternType_Input) at CollateralChargePopup.pcf: line 30, column 48
    function valueRoot_3 () : java.lang.Object {
      return chargePattern
    }
    
    // 'value' attribute on TextInput (id=chargePatternType_Input) at CollateralChargePopup.pcf: line 30, column 48
    function value_2 () : java.lang.String {
      return chargePattern.DisplayName
    }
    
    // 'value' attribute on MonetaryAmountInput (id=remainingAmount_Input) at CollateralChargePopup.pcf: line 36, column 80
    function value_5 () : gw.pl.currency.MonetaryAmount {
      return collateralUtil.getRemainingAmount(chargePatternOwnerPair)
    }
    
    override property get CurrentLocation () : pcf.CollateralChargePopup {
      return super.CurrentLocation as pcf.CollateralChargePopup
    }
    
    property get chargePattern () : entity.ChargePattern {
      return getVariableValue("chargePattern", 0) as entity.ChargePattern
    }
    
    property set chargePattern ($arg :  entity.ChargePattern) {
      setVariableValue("chargePattern", 0, $arg)
    }
    
    property get chargePatternOwnerPair () : com.google.gdata.util.common.base.Pair<ChargePattern, TAccountOwner> {
      return getVariableValue("chargePatternOwnerPair", 0) as com.google.gdata.util.common.base.Pair<ChargePattern, TAccountOwner>
    }
    
    property set chargePatternOwnerPair ($arg :  com.google.gdata.util.common.base.Pair<ChargePattern, TAccountOwner>) {
      setVariableValue("chargePatternOwnerPair", 0, $arg)
    }
    
    property get collateralUtil () : gw.api.web.account.CollateralUtil {
      return getVariableValue("collateralUtil", 0) as gw.api.web.account.CollateralUtil
    }
    
    property set collateralUtil ($arg :  gw.api.web.account.CollateralUtil) {
      setVariableValue("collateralUtil", 0, $arg)
    }
    
    property get owner () : entity.TAccountOwner {
      return getVariableValue("owner", 0) as entity.TAccountOwner
    }
    
    property set owner ($arg :  entity.TAccountOwner) {
      setVariableValue("owner", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/collateral/CollateralChargePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CollateralChargesListDetailPanelExpressionsImpl extends CollateralChargePopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=EditInvoiceItems) at CollateralChargePopup.pcf: line 63, column 100
    function action_11 () : void {
      EditInvoiceItemsPopup.push(charge)
    }
    
    // 'action' attribute on ToolbarButton (id=EditInvoiceItems) at CollateralChargePopup.pcf: line 63, column 100
    function action_dest_12 () : pcf.api.Destination {
      return pcf.EditInvoiceItemsPopup.createDestination(charge)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=editHold) at CollateralChargePopup.pcf: line 50, column 87
    function allCheckedRowsAction_8 (CheckedValues :  entity.Charge[], CheckedValuesErrors :  java.util.Map) : void {
      ChargeHoldsPopup.push(CheckedValues)
    }
    
    // 'def' attribute on PanelRef at CollateralChargePopup.pcf: line 58, column 75
    function def_onEnter_13 (def :  pcf.InvoiceItemsLV) : void {
      def.onEnter(charge.AllInvoiceItems, charge, false)
    }
    
    // 'def' attribute on PanelRef at CollateralChargePopup.pcf: line 44, column 142
    function def_onEnter_9 (def :  pcf.ChargesLV) : void {
      def.onEnter(collateralUtil.getChargesOfChargePatternType(chargePatternOwnerPair)?.toTypedArray(), true, 0, false, false, true)
    }
    
    // 'def' attribute on PanelRef at CollateralChargePopup.pcf: line 44, column 142
    function def_refreshVariables_10 (def :  pcf.ChargesLV) : void {
      def.refreshVariables(collateralUtil.getChargesOfChargePatternType(chargePatternOwnerPair)?.toTypedArray(), true, 0, false, false, true)
    }
    
    // 'def' attribute on PanelRef at CollateralChargePopup.pcf: line 58, column 75
    function def_refreshVariables_14 (def :  pcf.InvoiceItemsLV) : void {
      def.refreshVariables(charge.AllInvoiceItems, charge, false)
    }
    
    property get charge () : Charge {
      return getCurrentSelection(1) as Charge
    }
    
    property set charge ($arg :  Charge) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  
}