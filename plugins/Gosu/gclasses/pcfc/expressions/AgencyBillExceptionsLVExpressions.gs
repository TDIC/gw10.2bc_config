package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExceptionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillExceptionsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExceptionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillExceptionsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=statementNumber_Cell) at AgencyBillExceptionsLV.pcf: line 54, column 54
    function sortValue_1 (exceptionItemView :  gw.api.web.invoice.ExceptionItemView) : java.lang.Object {
      return exceptionItemView.StatementNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossDifference_Cell) at AgencyBillExceptionsLV.pcf: line 99, column 68
    function sortValue_10 (exceptionItemView :  gw.api.web.invoice.ExceptionItemView) : java.lang.Object {
      return exceptionItemView.ExceptionItem.GrossDifference
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionDifference_Cell) at AgencyBillExceptionsLV.pcf: line 106, column 73
    function sortValue_11 (exceptionItemView :  gw.api.web.invoice.ExceptionItemView) : java.lang.Object {
      return exceptionItemView.ExceptionItem.CommissionDifference
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AgencyBillExceptionsLV.pcf: line 111, column 47
    function sortValue_12 (exceptionItemView :  gw.api.web.invoice.ExceptionItemView) : java.lang.Object {
      return exceptionItemView.Comments
    }
    
    // 'value' attribute on DateCell (id=statementDate_Cell) at AgencyBillExceptionsLV.pcf: line 59, column 65
    function sortValue_2 (exceptionItemView :  gw.api.web.invoice.ExceptionItemView) : java.lang.Object {
      return exceptionItemView.StatementInvoice.EventDate
    }
    
    // 'value' attribute on DateCell (id=createDate_Cell) at AgencyBillExceptionsLV.pcf: line 63, column 49
    function sortValue_3 (exceptionItemView :  gw.api.web.invoice.ExceptionItemView) : java.lang.Object {
      return exceptionItemView.CreateDate
    }
    
    // 'value' attribute on TextCell (id=event_Cell) at AgencyBillExceptionsLV.pcf: line 67, column 44
    function sortValue_4 (exceptionItemView :  gw.api.web.invoice.ExceptionItemView) : java.lang.Object {
      return exceptionItemView.Issue
    }
    
    // 'value' attribute on TextCell (id=ItemType_Cell) at AgencyBillExceptionsLV.pcf: line 72, column 47
    function sortValue_5 (exceptionItemView :  gw.api.web.invoice.ExceptionItemView) : java.lang.Object {
      return exceptionItemView.ItemType
    }
    
    // 'value' attribute on TextCell (id=TAccountOwner_Cell) at AgencyBillExceptionsLV.pcf: line 78, column 64
    function sortValue_6 (exceptionItemView :  gw.api.web.invoice.ExceptionItemView) : java.lang.Object {
      return exceptionItemView.TAccountOwner.DisplayName
    }
    
    // 'value' attribute on DateCell (id=PolicyEffectiveDate_Cell) at AgencyBillExceptionsLV.pcf: line 82, column 58
    function sortValue_7 (exceptionItemView :  gw.api.web.invoice.ExceptionItemView) : java.lang.Object {
      return exceptionItemView.PolicyEffectiveDate
    }
    
    // 'value' attribute on TextCell (id=Insured_Cell) at AgencyBillExceptionsLV.pcf: line 87, column 52
    function sortValue_8 (exceptionItemView :  gw.api.web.invoice.ExceptionItemView) : java.lang.Object {
      return exceptionItemView.AccountNumber
    }
    
    // 'value' attribute on DateCell (id=Product_Cell) at AgencyBillExceptionsLV.pcf: line 92, column 65
    function sortValue_9 (exceptionItemView :  gw.api.web.invoice.ExceptionItemView) : java.lang.Object {
      return exceptionItemView.InvoiceItem.InvoiceDueDate
    }
    
    // '$$sumValue' attribute on RowIterator at AgencyBillExceptionsLV.pcf: line 99, column 68
    function sumValueRoot_15 (exceptionItemView :  gw.api.web.invoice.ExceptionItemView) : java.lang.Object {
      return exceptionItemView.ExceptionItem
    }
    
    // 'footerSumValue' attribute on RowIterator at AgencyBillExceptionsLV.pcf: line 99, column 68
    function sumValue_14 (exceptionItemView :  gw.api.web.invoice.ExceptionItemView) : java.lang.Object {
      return exceptionItemView.ExceptionItem.GrossDifference
    }
    
    // 'footerSumValue' attribute on RowIterator at AgencyBillExceptionsLV.pcf: line 106, column 73
    function sumValue_16 (exceptionItemView :  gw.api.web.invoice.ExceptionItemView) : java.lang.Object {
      return exceptionItemView.ExceptionItem.CommissionDifference
    }
    
    // 'value' attribute on RowIterator at AgencyBillExceptionsLV.pcf: line 28, column 58
    function value_77 () : gw.api.web.invoice.ExceptionItemView[] {
      return exceptionItemViews
    }
    
    // 'visible' attribute on RangeCell (id=WriteoffReason_Cell) at AgencyBillExceptionsLV.pcf: line 49, column 50
    function visible_0 () : java.lang.Boolean {
      return ShowWriteoffReasonDropdowns
    }
    
    property get ShowCheckboxes () : boolean {
      return getRequireValue("ShowCheckboxes", 0) as java.lang.Boolean
    }
    
    property set ShowCheckboxes ($arg :  boolean) {
      setRequireValue("ShowCheckboxes", 0, $arg)
    }
    
    property get ShowWriteoffReasonDropdowns () : boolean {
      return getRequireValue("ShowWriteoffReasonDropdowns", 0) as java.lang.Boolean
    }
    
    property set ShowWriteoffReasonDropdowns ($arg :  boolean) {
      setRequireValue("ShowWriteoffReasonDropdowns", 0, $arg)
    }
    
    property get exceptionItemViews () : gw.api.web.invoice.ExceptionItemView[] {
      return getRequireValue("exceptionItemViews", 0) as gw.api.web.invoice.ExceptionItemView[]
    }
    
    property set exceptionItemViews ($arg :  gw.api.web.invoice.ExceptionItemView[]) {
      setRequireValue("exceptionItemViews", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExceptionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AgencyBillExceptionsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=TAccountOwner_Cell) at AgencyBillExceptionsLV.pcf: line 78, column 64
    function actionAvailable_49 () : java.lang.Boolean {
      return policyPeriod != null
    }
    
    // 'action' attribute on TextCell (id=statementNumber_Cell) at AgencyBillExceptionsLV.pcf: line 54, column 54
    function action_28 () : void {
      AgencyBillStatementDetail.go(exceptionItemView.StatementInvoice.AgencyBillCycle)
    }
    
    // 'action' attribute on TextCell (id=ItemType_Cell) at AgencyBillExceptionsLV.pcf: line 72, column 47
    function action_42 () : void {
      InvoiceItemHistoryPopup.push(exceptionItemView.InvoiceItem)
    }
    
    // 'action' attribute on TextCell (id=TAccountOwner_Cell) at AgencyBillExceptionsLV.pcf: line 78, column 64
    function action_47 () : void {
      PolicyOverview.go(policyPeriod)
    }
    
    // 'action' attribute on TextCell (id=Insured_Cell) at AgencyBillExceptionsLV.pcf: line 87, column 52
    function action_56 () : void {
      AccountSummary.push(exceptionItemView.InvoiceItem.PolicyPeriod.Account)
    }
    
    // 'action' attribute on TextCell (id=statementNumber_Cell) at AgencyBillExceptionsLV.pcf: line 54, column 54
    function action_dest_29 () : pcf.api.Destination {
      return pcf.AgencyBillStatementDetail.createDestination(exceptionItemView.StatementInvoice.AgencyBillCycle)
    }
    
    // 'action' attribute on TextCell (id=ItemType_Cell) at AgencyBillExceptionsLV.pcf: line 72, column 47
    function action_dest_43 () : pcf.api.Destination {
      return pcf.InvoiceItemHistoryPopup.createDestination(exceptionItemView.InvoiceItem)
    }
    
    // 'action' attribute on TextCell (id=TAccountOwner_Cell) at AgencyBillExceptionsLV.pcf: line 78, column 64
    function action_dest_48 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(policyPeriod)
    }
    
    // 'action' attribute on TextCell (id=Insured_Cell) at AgencyBillExceptionsLV.pcf: line 87, column 52
    function action_dest_57 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination(exceptionItemView.InvoiceItem.PolicyPeriod.Account)
    }
    
    // 'checkBoxVisible' attribute on RowIterator at AgencyBillExceptionsLV.pcf: line 28, column 58
    function checkBoxVisible_75 () : java.lang.Boolean {
      return ShowCheckboxes
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillExceptionsLV.pcf: line 38, column 25
    function condition_19 () : java.lang.Boolean {
      return (exceptionItemView.TAccountOwner typeis PolicyPeriod) and !exceptionItemView.TAccountOwner.isDirectOrImpliedTargetOfHoldType(HoldType.TC_DELINQUENCY)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=GrossDifference_Cell) at AgencyBillExceptionsLV.pcf: line 99, column 68
    function currency_66 () : typekey.Currency {
      return exceptionItemView.ExceptionItem.GrossDifference.Currency
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=CommissionDifference_Cell) at AgencyBillExceptionsLV.pcf: line 106, column 73
    function currency_70 () : typekey.Currency {
      return exceptionItemView.ExceptionItem.CommissionDifference.Currency
    }
    
    // 'value' attribute on RangeCell (id=WriteoffReason_Cell) at AgencyBillExceptionsLV.pcf: line 49, column 50
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      exceptionItemView.WriteoffReason = (__VALUE_TO_SET as typekey.WriteoffReason)
    }
    
    // 'initialValue' attribute on Variable at AgencyBillExceptionsLV.pcf: line 33, column 30
    function initialValue_18 () : PolicyPeriod {
      return exceptionItemView.TAccountOwner typeis PolicyPeriod ? exceptionItemView.TAccountOwner : null
    }
    
    // RowIterator at AgencyBillExceptionsLV.pcf: line 28, column 58
    function initializeVariables_76 () : void {
        policyPeriod = exceptionItemView.TAccountOwner typeis PolicyPeriod ? exceptionItemView.TAccountOwner : null;

    }
    
    // 'valueRange' attribute on RangeCell (id=WriteoffReason_Cell) at AgencyBillExceptionsLV.pcf: line 49, column 50
    function valueRange_23 () : java.lang.Object {
      return WriteoffReason.getTypeKeys(false)
    }
    
    // 'value' attribute on RangeCell (id=WriteoffReason_Cell) at AgencyBillExceptionsLV.pcf: line 49, column 50
    function valueRoot_22 () : java.lang.Object {
      return exceptionItemView
    }
    
    // 'value' attribute on DateCell (id=statementDate_Cell) at AgencyBillExceptionsLV.pcf: line 59, column 65
    function valueRoot_34 () : java.lang.Object {
      return exceptionItemView.StatementInvoice
    }
    
    // 'value' attribute on TextCell (id=TAccountOwner_Cell) at AgencyBillExceptionsLV.pcf: line 78, column 64
    function valueRoot_51 () : java.lang.Object {
      return exceptionItemView.TAccountOwner
    }
    
    // 'value' attribute on DateCell (id=Product_Cell) at AgencyBillExceptionsLV.pcf: line 92, column 65
    function valueRoot_62 () : java.lang.Object {
      return exceptionItemView.InvoiceItem
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossDifference_Cell) at AgencyBillExceptionsLV.pcf: line 99, column 68
    function valueRoot_65 () : java.lang.Object {
      return exceptionItemView.ExceptionItem
    }
    
    // 'value' attribute on RangeCell (id=WriteoffReason_Cell) at AgencyBillExceptionsLV.pcf: line 49, column 50
    function value_20 () : typekey.WriteoffReason {
      return exceptionItemView.WriteoffReason
    }
    
    // 'value' attribute on TextCell (id=statementNumber_Cell) at AgencyBillExceptionsLV.pcf: line 54, column 54
    function value_30 () : java.lang.String {
      return exceptionItemView.StatementNumber
    }
    
    // 'value' attribute on DateCell (id=statementDate_Cell) at AgencyBillExceptionsLV.pcf: line 59, column 65
    function value_33 () : java.util.Date {
      return exceptionItemView.StatementInvoice.EventDate
    }
    
    // 'value' attribute on DateCell (id=createDate_Cell) at AgencyBillExceptionsLV.pcf: line 63, column 49
    function value_36 () : java.util.Date {
      return exceptionItemView.CreateDate
    }
    
    // 'value' attribute on TextCell (id=event_Cell) at AgencyBillExceptionsLV.pcf: line 67, column 44
    function value_39 () : java.lang.String {
      return exceptionItemView.Issue
    }
    
    // 'value' attribute on TextCell (id=ItemType_Cell) at AgencyBillExceptionsLV.pcf: line 72, column 47
    function value_44 () : java.lang.String {
      return exceptionItemView.ItemType
    }
    
    // 'value' attribute on TextCell (id=TAccountOwner_Cell) at AgencyBillExceptionsLV.pcf: line 78, column 64
    function value_50 () : java.lang.String {
      return exceptionItemView.TAccountOwner.DisplayName
    }
    
    // 'value' attribute on DateCell (id=PolicyEffectiveDate_Cell) at AgencyBillExceptionsLV.pcf: line 82, column 58
    function value_53 () : java.util.Date {
      return exceptionItemView.PolicyEffectiveDate
    }
    
    // 'value' attribute on TextCell (id=Insured_Cell) at AgencyBillExceptionsLV.pcf: line 87, column 52
    function value_58 () : java.lang.String {
      return exceptionItemView.AccountNumber
    }
    
    // 'value' attribute on DateCell (id=Product_Cell) at AgencyBillExceptionsLV.pcf: line 92, column 65
    function value_61 () : java.util.Date {
      return exceptionItemView.InvoiceItem.InvoiceDueDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossDifference_Cell) at AgencyBillExceptionsLV.pcf: line 99, column 68
    function value_64 () : gw.pl.currency.MonetaryAmount {
      return exceptionItemView.ExceptionItem.GrossDifference
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionDifference_Cell) at AgencyBillExceptionsLV.pcf: line 106, column 73
    function value_68 () : gw.pl.currency.MonetaryAmount {
      return exceptionItemView.ExceptionItem.CommissionDifference
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AgencyBillExceptionsLV.pcf: line 111, column 47
    function value_72 () : java.lang.String {
      return exceptionItemView.Comments
    }
    
    // 'valueRange' attribute on RangeCell (id=WriteoffReason_Cell) at AgencyBillExceptionsLV.pcf: line 49, column 50
    function verifyValueRangeIsAllowedType_24 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=WriteoffReason_Cell) at AgencyBillExceptionsLV.pcf: line 49, column 50
    function verifyValueRangeIsAllowedType_24 ($$arg :  typekey.WriteoffReason[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=WriteoffReason_Cell) at AgencyBillExceptionsLV.pcf: line 49, column 50
    function verifyValueRange_25 () : void {
      var __valueRangeArg = WriteoffReason.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_24(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeCell (id=WriteoffReason_Cell) at AgencyBillExceptionsLV.pcf: line 49, column 50
    function visible_26 () : java.lang.Boolean {
      return ShowWriteoffReasonDropdowns
    }
    
    property get exceptionItemView () : gw.api.web.invoice.ExceptionItemView {
      return getIteratedValue(1) as gw.api.web.invoice.ExceptionItemView
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 1) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 1, $arg)
    }
    
    
  }
  
  
}