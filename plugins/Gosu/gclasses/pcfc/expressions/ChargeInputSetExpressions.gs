package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/ChargeInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChargeInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/ChargeInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChargeInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=owner_Input) at ChargeInputSet.pcf: line 14, column 35
    function valueRoot_1 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on TextInput (id=Policy_Input) at ChargeInputSet.pcf: line 24, column 51
    function valueRoot_8 () : java.lang.Object {
      return invoiceItem.PolicyPeriod
    }
    
    // 'value' attribute on TextInput (id=owner_Input) at ChargeInputSet.pcf: line 14, column 35
    function value_0 () : entity.Account {
      return invoiceItem.Owner
    }
    
    // 'value' attribute on DateInput (id=ChargeDate_Input) at ChargeInputSet.pcf: line 28, column 38
    function value_11 () : java.util.Date {
      return invoiceItem.EventDate
    }
    
    // 'value' attribute on TextInput (id=invoice_Input) at ChargeInputSet.pcf: line 32, column 272
    function value_14 () : java.lang.String {
      return DisplayKey.get("Web.InvoiceItemDetailPopup.Invoice", (typeof invoiceItem.Invoice).DisplayName, invoiceItem.Invoice.InvoiceNumber, invoiceItem.Invoice.StatusForUI, gw.api.util.StringUtil.formatDate(invoiceItem.Invoice.DueDate, "short"))
    }
    
    // 'value' attribute on TextInput (id=Issured_Input) at ChargeInputSet.pcf: line 19, column 55
    function value_3 () : gw.api.domain.invoice.InvoicePayer {
      return invoiceItem.Payer
    }
    
    // 'value' attribute on TextInput (id=Policy_Input) at ChargeInputSet.pcf: line 24, column 51
    function value_7 () : java.lang.String {
      return invoiceItem.PolicyPeriod.PolicyNumber
    }
    
    // 'visible' attribute on TextInput (id=Policy_Input) at ChargeInputSet.pcf: line 24, column 51
    function visible_6 () : java.lang.Boolean {
      return invoiceItem.PolicyPeriod != null
    }
    
    property get invoiceItem () : InvoiceItem {
      return getRequireValue("invoiceItem", 0) as InvoiceItem
    }
    
    property set invoiceItem ($arg :  InvoiceItem) {
      setRequireValue("invoiceItem", 0, $arg)
    }
    
    
  }
  
  
}