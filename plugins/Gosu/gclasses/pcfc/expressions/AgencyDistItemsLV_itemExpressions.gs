package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistItemsLV.item.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyDistItemsLV_itemExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistItemsLV.item.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyDistItemsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RowIterator (id=PolicyPeriodInvoiceItems) at AgencyDistItemsLV.item.pcf: line 63, column 122
    function value_61 () : java.util.List<gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView> {
      return agencyCycleDistView.ChargeOwners
    }
    
    property get agencyCycleDistView () : gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistView {
      return getRequireValue("agencyCycleDistView", 0) as gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistView
    }
    
    property set agencyCycleDistView ($arg :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistView) {
      setRequireValue("agencyCycleDistView", 0, $arg)
    }
    
    property get wizardState () : gw.agencybill.AgencyDistributionWizardHelper {
      return getRequireValue("wizardState", 0) as gw.agencybill.AgencyDistributionWizardHelper
    }
    
    property set wizardState ($arg :  gw.agencybill.AgencyDistributionWizardHelper) {
      setRequireValue("wizardState", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistItemsLV.item.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistItemsLV.item.pcf: line 136, column 56
    function actionAvailable_21 () : java.lang.Boolean {
      return invoiceItem.Invoice typeis StatementInvoice
    }
    
    // 'action' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistItemsLV.item.pcf: line 136, column 56
    function action_20 () : void {
      invoiceItem.StatementInvoiceDetailViewAction()
    }
    
    // 'action' attribute on TextCell (id=ItemDescription_Cell) at AgencyDistItemsLV.item.pcf: line 148, column 56
    function action_31 () : void {
      InvoiceItemHistoryPopup.push(invoiceItem)
    }
    
    // 'action' attribute on Link (id=FillDefaultsButton) at AgencyDistItemsLV.item.pcf: line 165, column 132
    function action_41 () : void {
      agencyDistItem.fillUnpaidAmounts()
    }
    
    // 'action' attribute on TextCell (id=ItemDescription_Cell) at AgencyDistItemsLV.item.pcf: line 148, column 56
    function action_dest_32 () : pcf.api.Destination {
      return pcf.InvoiceItemHistoryPopup.createDestination(invoiceItem)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=ItemNetOwed_Cell) at AgencyDistItemsLV.item.pcf: line 154, column 51
    function currency_38 () : typekey.Currency {
      return agencyDistItem.NetAmountOwed.Currency
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=NetToApply_Cell) at AgencyDistItemsLV.item.pcf: line 174, column 32
    function currency_47 () : typekey.Currency {
      return agencyDistItem.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetToApply_Cell) at AgencyDistItemsLV.item.pcf: line 174, column 32
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      netToApply = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AgencyDistItemsLV.item.pcf: line 191, column 53
    function defaultSetter_55 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyDistItem.PaymentComments = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on MonetaryAmountCell (id=NetToApply_Cell) at AgencyDistItemsLV.item.pcf: line 174, column 32
    function editable_43 () : java.lang.Boolean {
      return agencyDistItem.CommissionModifiable
    }
    
    // 'editable' attribute on Row at AgencyDistItemsLV.item.pcf: line 123, column 112
    function editable_58 () : java.lang.Boolean {
      return !invoiceItem.Frozen && invoiceItem.Type != InvoiceItemType.TC_COMMISSIONREMAINDER
    }
    
    // 'iconColor' attribute on BooleanRadioCell (id=ExceptionFlag_Cell) at AgencyDistItemsLV.item.pcf: line 131, column 58
    function iconColor_18 () : gw.api.web.color.GWColor {
      return gw.web.util.BCIconHelper.getAgencyBillPaymentExceptionIconColor(invoiceItem)
    }
    
    // 'iconLabel' attribute on BooleanRadioCell (id=ExceptionFlag_Cell) at AgencyDistItemsLV.item.pcf: line 131, column 58
    function iconLabel_14 () : java.lang.String {
      return gw.web.util.BCIconHelper.getAgencyBillPaymentExceptionIconLabel(invoiceItem)
    }
    
    // 'icon' attribute on BooleanRadioCell (id=ExceptionFlag_Cell) at AgencyDistItemsLV.item.pcf: line 131, column 58
    function icon_17 () : java.lang.String {
      return gw.web.util.BCIconHelper.getAgencyBillPaymentExceptionIcon(invoiceItem)
    }
    
    // 'initialValue' attribute on Variable at AgencyDistItemsLV.item.pcf: line 117, column 31
    function initialValue_12 () : InvoiceItem {
      return agencyDistItem.InvoiceItem
    }
    
    // 'initialValue' attribute on Variable at AgencyDistItemsLV.item.pcf: line 121, column 49
    function initialValue_13 () : gw.pl.currency.MonetaryAmount {
      return agencyDistItem.NetAmountToApply
    }
    
    // RowIterator (id=InvoiceItems) at AgencyDistItemsLV.item.pcf: line 113, column 63
    function initializeVariables_59 () : void {
        invoiceItem = agencyDistItem.InvoiceItem;
  netToApply = agencyDistItem.NetAmountToApply;

    }
    
    // 'onChange' attribute on PostOnChange at AgencyDistItemsLV.item.pcf: line 178, column 135
    function onChange_42 () : void {
      gw.agencybill.CommissionCalculator.convertNetToApplyToGrossAndCommissionToApply(netToApply, agencyDistItem)
    }
    
    // 'validationExpression' attribute on MonetaryAmountCell (id=NetToApply_Cell) at AgencyDistItemsLV.item.pcf: line 174, column 32
    function validationExpression_44 () : java.lang.Object {
      return wizardState.isNetAmountToApplyInvalid(agencyDistItem)? DisplayKey.get("Java.Error.AgencyCycleDistChargeOwnerView.InvalidGrossAndCommissionDistributionAmount") : null
    }
    
    // 'value' attribute on BooleanRadioCell (id=ExceptionFlag_Cell) at AgencyDistItemsLV.item.pcf: line 131, column 58
    function valueRoot_16 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistItemsLV.item.pcf: line 136, column 56
    function valueRoot_23 () : java.lang.Object {
      return invoiceItem.Invoice
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ItemNetOwed_Cell) at AgencyDistItemsLV.item.pcf: line 154, column 51
    function valueRoot_37 () : java.lang.Object {
      return agencyDistItem
    }
    
    // 'value' attribute on BooleanRadioCell (id=ExceptionFlag_Cell) at AgencyDistItemsLV.item.pcf: line 131, column 58
    function value_15 () : java.lang.Boolean {
      return invoiceItem.HasBeenPaymentException
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistItemsLV.item.pcf: line 136, column 56
    function value_22 () : java.lang.String {
      return invoiceItem.Invoice.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=LineItemNumber_Cell) at AgencyDistItemsLV.item.pcf: line 141, column 44
    function value_25 () : java.lang.Integer {
      return invoiceItem.LineItemNumber
    }
    
    // 'value' attribute on DateCell (id=Statement_Cell) at AgencyDistItemsLV.item.pcf: line 144, column 52
    function value_28 () : java.util.Date {
      return invoiceItem.Invoice.EventDate
    }
    
    // 'value' attribute on TextCell (id=ItemDescription_Cell) at AgencyDistItemsLV.item.pcf: line 148, column 56
    function value_33 () : java.lang.String {
      return invoiceItem.DisplayNameAsItemType
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ItemNetOwed_Cell) at AgencyDistItemsLV.item.pcf: line 154, column 51
    function value_36 () : gw.pl.currency.MonetaryAmount {
      return agencyDistItem.NetAmountOwed
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetToApply_Cell) at AgencyDistItemsLV.item.pcf: line 174, column 32
    function value_45 () : gw.pl.currency.MonetaryAmount {
      return netToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Difference_Cell) at AgencyDistItemsLV.item.pcf: line 187, column 64
    function value_51 () : gw.pl.currency.MonetaryAmount {
      return agencyDistItem.NetAmountOwed - netToApply
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AgencyDistItemsLV.item.pcf: line 191, column 53
    function value_54 () : java.lang.String {
      return agencyDistItem.PaymentComments
    }
    
    // 'visible' attribute on Link (id=FillDefaultsButton) at AgencyDistItemsLV.item.pcf: line 165, column 132
    function visible_40 () : java.lang.Boolean {
      return invoiceItem.Type != InvoiceItemType.TC_COMMISSIONREMAINDER && agencyDistItem.CommissionModifiable
    }
    
    property get agencyDistItem () : entity.BaseDistItem {
      return getIteratedValue(2) as entity.BaseDistItem
    }
    
    property get invoiceItem () : InvoiceItem {
      return getVariableValue("invoiceItem", 2) as InvoiceItem
    }
    
    property set invoiceItem ($arg :  InvoiceItem) {
      setVariableValue("invoiceItem", 2, $arg)
    }
    
    property get netToApply () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("netToApply", 2) as gw.pl.currency.MonetaryAmount
    }
    
    property set netToApply ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("netToApply", 2, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistItemsLV.item.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AgencyDistItemsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=PolicyDescription_Cell) at AgencyDistItemsLV.item.pcf: line 73, column 49
    function actionAvailable_2 () : java.lang.Boolean {
      return policyPeriodView.ChargeOwner typeis PolicyPeriod or policyPeriodView.ChargeOwner typeis Account
    }
    
    // 'action' attribute on TextCell (id=PolicyDescription_Cell) at AgencyDistItemsLV.item.pcf: line 73, column 49
    function action_0 () : void {
      AgencyChargeOwnerDetailForward.push(policyPeriodView)
    }
    
    // 'action' attribute on Link (id=DetailsButton) at AgencyDistItemsLV.item.pcf: line 99, column 139
    function action_10 () : void {
      AgencyDistributionWizard_DetailsPopup.push(policyPeriodView, policyPeriodView.TotalNetAmountToDistribute, wizardState)
    }
    
    // 'action' attribute on TextCell (id=PolicyDescription_Cell) at AgencyDistItemsLV.item.pcf: line 73, column 49
    function action_dest_1 () : pcf.api.Destination {
      return pcf.AgencyChargeOwnerDetailForward.createDestination(policyPeriodView)
    }
    
    // 'action' attribute on Link (id=DetailsButton) at AgencyDistItemsLV.item.pcf: line 99, column 139
    function action_dest_11 () : pcf.api.Destination {
      return pcf.AgencyDistributionWizard_DetailsPopup.createDestination(policyPeriodView, policyPeriodView.TotalNetAmountToDistribute, wizardState)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=PolicyNetOwed_Cell) at AgencyDistItemsLV.item.pcf: line 85, column 51
    function currency_8 () : typekey.Currency {
      return policyPeriodView.NetAmountOwed.Currency
    }
    
    // 'value' attribute on TextCell (id=PolicyDescription_Cell) at AgencyDistItemsLV.item.pcf: line 73, column 49
    function valueRoot_4 () : java.lang.Object {
      return policyPeriodView
    }
    
    // 'value' attribute on TextCell (id=PolicyDescription_Cell) at AgencyDistItemsLV.item.pcf: line 73, column 49
    function value_3 () : java.lang.String {
      return policyPeriodView.DisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PolicyNetOwed_Cell) at AgencyDistItemsLV.item.pcf: line 85, column 51
    function value_6 () : gw.pl.currency.MonetaryAmount {
      return policyPeriodView.NetAmountOwed
    }
    
    // 'value' attribute on RowIterator (id=InvoiceItems) at AgencyDistItemsLV.item.pcf: line 113, column 63
    function value_60 () : java.util.List<entity.BaseDistItem> {
      return policyPeriodView.AgencyDistItems
    }
    
    property get policyPeriodView () : gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView {
      return getIteratedValue(1) as gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView
    }
    
    
  }
  
  
}