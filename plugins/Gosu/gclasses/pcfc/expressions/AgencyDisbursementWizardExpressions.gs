package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.web.producer.agencybill.AgencyBillMoneySetupFactory
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyDisbursementWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyDisbursementWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyDisbursementWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyDisbursementWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer, fromCreditItems :  boolean) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Wizard (id=AgencyDisbursementWizard) at AgencyDisbursementWizard.pcf: line 10, column 35
    function beforeCommit_12 (pickedValue :  java.lang.Object) : void {
      disburse(payCommissionReference[0])
    }
    
    // 'canVisit' attribute on Wizard (id=AgencyDisbursementWizard) at AgencyDisbursementWizard.pcf: line 10, column 35
    static function canVisit_13 (fromCreditItems :  boolean, producer :  Producer) : java.lang.Boolean {
      return perm.Transaction.disbcreate
    }
    
    // 'initialValue' attribute on Variable at AgencyDisbursementWizard.pcf: line 22, column 34
    function initialValue_0 () : AgencyDisbursement {
      return newDisbursement()
    }
    
    // 'initialValue' attribute on Variable at AgencyDisbursementWizard.pcf: line 26, column 54
    function initialValue_1 () : java.util.ArrayList<InvoiceItem> {
      return new java.util.ArrayList<InvoiceItem>()
    }
    
    // 'initialValue' attribute on Variable at AgencyDisbursementWizard.pcf: line 30, column 25
    function initialValue_2 () : boolean[] {
      return new boolean[]{true}
    }
    
    // 'onExit' attribute on WizardStep (id=SelectCreditItems) at AgencyDisbursementWizard.pcf: line 37, column 91
    function onExit_3 () : void {
      disbursement.Amount = disbursement.computeDisbursementAmount(invoiceItems, payCommissionReference[0])
    }
    
    // 'onExit' attribute on WizardStep (id=Step1) at AgencyDisbursementWizard.pcf: line 43, column 90
    function onExit_7 () : void {
      disbursement.initiateApprovalActivityIfUserLacksAuthority()
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AgencyDisbursementWizard.pcf: line 48, column 90
    function screen_onEnter_10 (def :  pcf.CreateDisbursementConfirmScreen) : void {
      def.onEnter(disbursement)
    }
    
    // 'screen' attribute on WizardStep (id=SelectCreditItems) at AgencyDisbursementWizard.pcf: line 37, column 91
    function screen_onEnter_4 (def :  pcf.SelectCreditItemsScreen) : void {
      def.onEnter(producer, invoiceItems, disbursement, payCommissionReference)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AgencyDisbursementWizard.pcf: line 43, column 90
    function screen_onEnter_8 (def :  pcf.AgencyDisbursementDetailScreen) : void {
      def.onEnter(disbursement, payCommissionReference, invoiceItems, fromCreditItems)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AgencyDisbursementWizard.pcf: line 48, column 90
    function screen_refreshVariables_11 (def :  pcf.CreateDisbursementConfirmScreen) : void {
      def.refreshVariables(disbursement)
    }
    
    // 'screen' attribute on WizardStep (id=SelectCreditItems) at AgencyDisbursementWizard.pcf: line 37, column 91
    function screen_refreshVariables_5 (def :  pcf.SelectCreditItemsScreen) : void {
      def.refreshVariables(producer, invoiceItems, disbursement, payCommissionReference)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AgencyDisbursementWizard.pcf: line 43, column 90
    function screen_refreshVariables_9 (def :  pcf.AgencyDisbursementDetailScreen) : void {
      def.refreshVariables(disbursement, payCommissionReference, invoiceItems, fromCreditItems)
    }
    
    // 'tabBar' attribute on Wizard (id=AgencyDisbursementWizard) at AgencyDisbursementWizard.pcf: line 10, column 35
    function tabBar_onEnter_14 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AgencyDisbursementWizard) at AgencyDisbursementWizard.pcf: line 10, column 35
    function tabBar_refreshVariables_15 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    // '$$wizardStepAvailable' attribute on WizardStep (id=SelectCreditItems) at AgencyDisbursementWizard.pcf: line 37, column 91
    function wizardStepAvailable_6 () : java.lang.Boolean {
      return fromCreditItems
    }
    
    override property get CurrentLocation () : pcf.AgencyDisbursementWizard {
      return super.CurrentLocation as pcf.AgencyDisbursementWizard
    }
    
    property get disbursement () : AgencyDisbursement {
      return getVariableValue("disbursement", 0) as AgencyDisbursement
    }
    
    property set disbursement ($arg :  AgencyDisbursement) {
      setVariableValue("disbursement", 0, $arg)
    }
    
    property get fromCreditItems () : boolean {
      return getVariableValue("fromCreditItems", 0) as java.lang.Boolean
    }
    
    property set fromCreditItems ($arg :  boolean) {
      setVariableValue("fromCreditItems", 0, $arg)
    }
    
    property get invoiceItems () : java.util.ArrayList<InvoiceItem> {
      return getVariableValue("invoiceItems", 0) as java.util.ArrayList<InvoiceItem>
    }
    
    property set invoiceItems ($arg :  java.util.ArrayList<InvoiceItem>) {
      setVariableValue("invoiceItems", 0, $arg)
    }
    
    property get payCommissionReference () : boolean[] {
      return getVariableValue("payCommissionReference", 0) as boolean[]
    }
    
    property set payCommissionReference ($arg :  boolean[]) {
      setVariableValue("payCommissionReference", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
    function newDisbursement() : AgencyDisbursement{
            var disb = new AgencyDisbursement(producer.Currency)
            disb.setProducerAndFields(producer);
            disb.PayTo = producer.PrimaryContact.Contact.Name
            disb.MailTo = producer.PrimaryContact.Contact.Name
            disb.Address = producer.PrimaryContact.Contact.PrimaryAddress as String
            return disb;
          }
          
    function disburse(payCommission : boolean){
      if(fromCreditItems){
        var moneySetup = AgencyBillMoneySetupFactory.createNewZeroDollarMoney(producer, CurrentLocation)
        moneySetup.setPrefill(AgencyCycleDistPrefill.TC_UNPAID)
        var payment = moneySetup.prepareDistribution()
        payment.addInvoiceItems(invoiceItems)
        
        payment.DistItems.each( \ distItem -> {
          distItem.GrossAmountToApply = distItem.GrossAmountOwed
          distItem.CommissionAmountToApply = payCommission ? distItem.CommissionAmountOwed : 0bd.ofCurrency(producer.Currency)
        })
        
        payment.execute()
        disbursement.AgencyCyclePayment = payment
      }
      disbursement.executeDisbursementOrCreateApprovalActivityIfNeeded()
    }
    
    
  }
  
  
}