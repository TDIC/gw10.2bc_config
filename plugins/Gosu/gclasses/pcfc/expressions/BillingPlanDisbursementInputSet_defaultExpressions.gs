package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanDisbursementInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BillingPlanDisbursementInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanDisbursementInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BillingPlanDisbursementInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=ReviewDisbursementOverDefaults) at BillingPlanDisbursementInputSet.default.pcf: line 34, column 44
    function def_onEnter_14 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(billingPlan, BillingPlan#ReviewDisbursementOverDefaults.PropertyInfo, DisplayKey.get("Web.BillingPlanDetailDV.ReviewDisbursement"), true, false, null)
    }
    
    // 'def' attribute on InputSetRef (id=DisbursementOverDefaults) at BillingPlanDisbursementInputSet.default.pcf: line 23, column 38
    function def_onEnter_6 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(billingPlan, BillingPlan#DisbursementOverDefaults.PropertyInfo, DisplayKey.get("Web.BillingPlanDetailDV.DisbursementOver"), true, false, null)
    }
    
    // 'def' attribute on InputSetRef (id=ReviewDisbursementOverDefaults) at BillingPlanDisbursementInputSet.default.pcf: line 34, column 44
    function def_refreshVariables_15 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(billingPlan, BillingPlan#ReviewDisbursementOverDefaults.PropertyInfo, DisplayKey.get("Web.BillingPlanDetailDV.ReviewDisbursement"), true, false, null)
    }
    
    // 'def' attribute on InputSetRef (id=DisbursementOverDefaults) at BillingPlanDisbursementInputSet.default.pcf: line 23, column 38
    function def_refreshVariables_7 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(billingPlan, BillingPlan#DisbursementOverDefaults.PropertyInfo, DisplayKey.get("Web.BillingPlanDetailDV.DisbursementOver"), true, false, null)
    }
    
    // 'value' attribute on BooleanRadioInput (id=CreateApprActForAutoDisb_Input) at BillingPlanDisbursementInputSet.default.pcf: line 31, column 53
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.CreateApprActForAutoDisb = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=DelayDisbursement_Input) at BillingPlanDisbursementInputSet.default.pcf: line 43, column 38
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.DelayDisbursement = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TypeKeyInput (id=AvailableDisbAmtType_Input) at BillingPlanDisbursementInputSet.default.pcf: line 20, column 49
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.AvailableDisbAmtType = (__VALUE_TO_SET as typekey.AvailableDisbAmtType)
    }
    
    // 'editable' attribute on TypeKeyInput (id=AvailableDisbAmtType_Input) at BillingPlanDisbursementInputSet.default.pcf: line 20, column 49
    function editable_0 () : java.lang.Boolean {
      return planNotInUse
    }
    
    // 'value' attribute on TypeKeyInput (id=AvailableDisbAmtType_Input) at BillingPlanDisbursementInputSet.default.pcf: line 20, column 49
    function valueRoot_3 () : java.lang.Object {
      return billingPlan
    }
    
    // 'value' attribute on TypeKeyInput (id=AvailableDisbAmtType_Input) at BillingPlanDisbursementInputSet.default.pcf: line 20, column 49
    function value_1 () : typekey.AvailableDisbAmtType {
      return billingPlan.AvailableDisbAmtType
    }
    
    // 'value' attribute on TextInput (id=DelayDisbursement_Input) at BillingPlanDisbursementInputSet.default.pcf: line 43, column 38
    function value_17 () : java.lang.Integer {
      return billingPlan.DelayDisbursement
    }
    
    // 'value' attribute on BooleanRadioInput (id=CreateApprActForAutoDisb_Input) at BillingPlanDisbursementInputSet.default.pcf: line 31, column 53
    function value_9 () : java.lang.Boolean {
      return billingPlan.CreateApprActForAutoDisb
    }
    
    property get billingPlan () : BillingPlan {
      return getRequireValue("billingPlan", 0) as BillingPlan
    }
    
    property set billingPlan ($arg :  BillingPlan) {
      setRequireValue("billingPlan", 0, $arg)
    }
    
    property get planNotInUse () : boolean {
      return getRequireValue("planNotInUse", 0) as java.lang.Boolean
    }
    
    property set planNotInUse ($arg :  boolean) {
      setRequireValue("planNotInUse", 0, $arg)
    }
    
    
  }
  
  
}