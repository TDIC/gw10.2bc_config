package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/email/SampleEmailWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SampleEmailWorksheetExpressions {
  @javax.annotation.Generated("config/web/pcf/email/SampleEmailWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SampleEmailWorksheetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (srcBean :  KeyableBean) : int {
      return 0
    }
    
    static function __constructorIndex (srcBean :  KeyableBean, docContainer :  DocumentContainer) : int {
      return 1
    }
    
    static function __constructorIndex (srcBean :  KeyableBean, docContainer :  DocumentContainer, emailTemplateStr :  String) : int {
      return 2
    }
    
    // 'def' attribute on ScreenRef at SampleEmailWorksheet.pcf: line 35, column 81
    function def_onEnter_3 (def :  pcf.CreateEmailScreen) : void {
      def.onEnter(symbolTable, docContainer, emailTemplateStr, {})
    }
    
    // 'def' attribute on ScreenRef at SampleEmailWorksheet.pcf: line 35, column 81
    function def_refreshVariables_4 (def :  pcf.CreateEmailScreen) : void {
      def.refreshVariables(symbolTable, docContainer, emailTemplateStr, {})
    }
    
    // 'initialValue' attribute on Variable at SampleEmailWorksheet.pcf: line 25, column 33
    function initialValue_0 () : DocumentContainer {
      return srcBean typeis Account or srcBean typeis Policy or srcBean typeis Producer ? srcBean as DocumentContainer : srcBean typeis PolicyPeriod ? srcBean.Policy : null
    }
    
    // 'initialValue' attribute on Variable at SampleEmailWorksheet.pcf: line 29, column 50
    function initialValue_1 () : java.util.Map<String, Object> {
      return gw.api.util.SymbolTableUtil.populateBeans( srcBean )
    }
    
    // 'initialValue' attribute on Variable at SampleEmailWorksheet.pcf: line 33, column 22
    function initialValue_2 () : String {
      return (srcBean typeis Activity) ? srcBean.EmailTemplate : null
    }
    
    override property get CurrentLocation () : pcf.SampleEmailWorksheet {
      return super.CurrentLocation as pcf.SampleEmailWorksheet
    }
    
    property get docContainer () : DocumentContainer {
      return getVariableValue("docContainer", 0) as DocumentContainer
    }
    
    property set docContainer ($arg :  DocumentContainer) {
      setVariableValue("docContainer", 0, $arg)
    }
    
    property get emailTemplateStr () : String {
      return getVariableValue("emailTemplateStr", 0) as String
    }
    
    property set emailTemplateStr ($arg :  String) {
      setVariableValue("emailTemplateStr", 0, $arg)
    }
    
    property get srcBean () : KeyableBean {
      return getVariableValue("srcBean", 0) as KeyableBean
    }
    
    property set srcBean ($arg :  KeyableBean) {
      setVariableValue("srcBean", 0, $arg)
    }
    
    property get symbolTable () : java.util.Map<String, Object> {
      return getVariableValue("symbolTable", 0) as java.util.Map<String, Object>
    }
    
    property set symbolTable ($arg :  java.util.Map<String, Object>) {
      setVariableValue("symbolTable", 0, $arg)
    }
    
    
  }
  
  
}