package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillStatementDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillStatementDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillStatementDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillStatementDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=PolicyFilterGoButton) at AgencyBillStatementDetailScreen.pcf: line 75, column 120
    function action_30 () : void {
      
    }
    
    // 'action' attribute on ToolbarButton (id=PolicyFilterClearButton) at AgencyBillStatementDetailScreen.pcf: line 79, column 123
    function action_31 () : void {
      agencyBillStatementView.PolicyPeriodFilter = null
    }
    
    // 'def' attribute on PanelRef at AgencyBillStatementDetailScreen.pcf: line 31, column 110
    function def_onEnter_32 (def :  pcf.PolicyActivityLV_itemlevel) : void {
      def.onEnter(agencyBillStatementView, viewOption)
    }
    
    // 'def' attribute on PanelRef at AgencyBillStatementDetailScreen.pcf: line 31, column 110
    function def_onEnter_34 (def :  pcf.PolicyActivityLV_policylevel) : void {
      def.onEnter(agencyBillStatementView, viewOption)
    }
    
    // 'def' attribute on PanelRef at AgencyBillStatementDetailScreen.pcf: line 28, column 67
    function def_onEnter_5 (def :  pcf.AgencyBillStatementTotalsDV) : void {
      def.onEnter(agencyBillStatementView)
    }
    
    // 'def' attribute on PanelRef at AgencyBillStatementDetailScreen.pcf: line 31, column 110
    function def_refreshVariables_33 (def :  pcf.PolicyActivityLV_itemlevel) : void {
      def.refreshVariables(agencyBillStatementView, viewOption)
    }
    
    // 'def' attribute on PanelRef at AgencyBillStatementDetailScreen.pcf: line 31, column 110
    function def_refreshVariables_35 (def :  pcf.PolicyActivityLV_policylevel) : void {
      def.refreshVariables(agencyBillStatementView, viewOption)
    }
    
    // 'def' attribute on PanelRef at AgencyBillStatementDetailScreen.pcf: line 28, column 67
    function def_refreshVariables_6 (def :  pcf.AgencyBillStatementTotalsDV) : void {
      def.refreshVariables(agencyBillStatementView)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=DisplayMode_Input) at AgencyBillStatementDetailScreen.pcf: line 52, column 53
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewOption = (__VALUE_TO_SET as typekey.InvoiceItemViewOption)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=ByItemOrPolicy_Input) at AgencyBillStatementDetailScreen.pcf: line 61, column 51
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      showByPolicy = (__VALUE_TO_SET as typekey.StatementViewOption)
    }
    
    // 'value' attribute on TextValue (id=PolicyPeriodFilter) at AgencyBillStatementDetailScreen.pcf: line 70, column 41
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillStatementView.PolicyPeriodFilter = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=AgencyPaymentItemsFilter_Input) at AgencyBillStatementDetailScreen.pcf: line 42, column 57
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      agencyBillStatementView.InvoiceItemsFilterType = (__VALUE_TO_SET as typekey.AgencyPmntItemsFilterType)
    }
    
    // 'initialValue' attribute on Variable at AgencyBillStatementDetailScreen.pcf: line 13, column 58
    function initialValue_0 () : gw.api.web.invoice.AgencyBillStatementView {
      return new gw.api.web.invoice.AgencyBillStatementView(cycle.StatementInvoice)
    }
    
    // 'initialValue' attribute on Variable at AgencyBillStatementDetailScreen.pcf: line 17, column 37
    function initialValue_1 () : InvoiceItemViewOption {
      return InvoiceItemViewOption.TC_BILLED
    }
    
    // 'initialValue' attribute on Variable at AgencyBillStatementDetailScreen.pcf: line 21, column 43
    function initialValue_2 () : typekey.StatementViewOption {
      return StatementViewOption.TC_BYITEM
    }
    
    // 'label' attribute on AlertBar (id=ArchivedAlert) at AgencyBillStatementDetailScreen.pcf: line 26, column 48
    function label_4 () : java.lang.Object {
      return DisplayKey.get("Web.AgencyBillStatementDetail.ArchivedAlert") 
    }
    
    // 'mode' attribute on PanelRef at AgencyBillStatementDetailScreen.pcf: line 31, column 110
    function mode_36 () : java.lang.Object {
      return showByPolicy == StatementViewOption.TC_BYITEM ? "itemlevel" : "policylevel"
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AgencyPaymentItemsFilter_Input) at AgencyBillStatementDetailScreen.pcf: line 42, column 57
    function valueRange_10 () : java.lang.Object {
      return AgencyPmntItemsFilterType.getTypeKeys( false ).where( \ a -> a != AgencyPmntItemsFilterType.TC_NONZERO )
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=DisplayMode_Input) at AgencyBillStatementDetailScreen.pcf: line 52, column 53
    function valueRange_16 () : java.lang.Object {
      return InvoiceItemViewOption.getTypeKeys( false )
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=ByItemOrPolicy_Input) at AgencyBillStatementDetailScreen.pcf: line 61, column 51
    function valueRange_22 () : java.lang.Object {
      return StatementViewOption.getTypeKeys( false )
    }
    
    // 'value' attribute on ToolbarRangeInput (id=AgencyPaymentItemsFilter_Input) at AgencyBillStatementDetailScreen.pcf: line 42, column 57
    function valueRoot_9 () : java.lang.Object {
      return agencyBillStatementView
    }
    
    // 'value' attribute on ToolbarRangeInput (id=DisplayMode_Input) at AgencyBillStatementDetailScreen.pcf: line 52, column 53
    function value_14 () : typekey.InvoiceItemViewOption {
      return viewOption
    }
    
    // 'value' attribute on ToolbarRangeInput (id=ByItemOrPolicy_Input) at AgencyBillStatementDetailScreen.pcf: line 61, column 51
    function value_20 () : typekey.StatementViewOption {
      return showByPolicy
    }
    
    // 'value' attribute on TextValue (id=PolicyPeriodFilter) at AgencyBillStatementDetailScreen.pcf: line 70, column 41
    function value_26 () : java.lang.String {
      return agencyBillStatementView.PolicyPeriodFilter
    }
    
    // 'value' attribute on ToolbarRangeInput (id=AgencyPaymentItemsFilter_Input) at AgencyBillStatementDetailScreen.pcf: line 42, column 57
    function value_7 () : typekey.AgencyPmntItemsFilterType {
      return agencyBillStatementView.InvoiceItemsFilterType
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AgencyPaymentItemsFilter_Input) at AgencyBillStatementDetailScreen.pcf: line 42, column 57
    function verifyValueRangeIsAllowedType_11 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AgencyPaymentItemsFilter_Input) at AgencyBillStatementDetailScreen.pcf: line 42, column 57
    function verifyValueRangeIsAllowedType_11 ($$arg :  typekey.AgencyPmntItemsFilterType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=DisplayMode_Input) at AgencyBillStatementDetailScreen.pcf: line 52, column 53
    function verifyValueRangeIsAllowedType_17 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=DisplayMode_Input) at AgencyBillStatementDetailScreen.pcf: line 52, column 53
    function verifyValueRangeIsAllowedType_17 ($$arg :  typekey.InvoiceItemViewOption[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=ByItemOrPolicy_Input) at AgencyBillStatementDetailScreen.pcf: line 61, column 51
    function verifyValueRangeIsAllowedType_23 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=ByItemOrPolicy_Input) at AgencyBillStatementDetailScreen.pcf: line 61, column 51
    function verifyValueRangeIsAllowedType_23 ($$arg :  typekey.StatementViewOption[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AgencyPaymentItemsFilter_Input) at AgencyBillStatementDetailScreen.pcf: line 42, column 57
    function verifyValueRange_12 () : void {
      var __valueRangeArg = AgencyPmntItemsFilterType.getTypeKeys( false ).where( \ a -> a != AgencyPmntItemsFilterType.TC_NONZERO )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_11(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=DisplayMode_Input) at AgencyBillStatementDetailScreen.pcf: line 52, column 53
    function verifyValueRange_18 () : void {
      var __valueRangeArg = InvoiceItemViewOption.getTypeKeys( false )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_17(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=ByItemOrPolicy_Input) at AgencyBillStatementDetailScreen.pcf: line 61, column 51
    function verifyValueRange_24 () : void {
      var __valueRangeArg = StatementViewOption.getTypeKeys( false )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_23(__valueRangeArg)
    }
    
    // 'visible' attribute on AlertBar (id=ArchivedAlert) at AgencyBillStatementDetailScreen.pcf: line 26, column 48
    function visible_3 () : java.lang.Boolean {
      return cycle.StatementInvoice.Frozen
    }
    
    property get agencyBillStatementView () : gw.api.web.invoice.AgencyBillStatementView {
      return getVariableValue("agencyBillStatementView", 0) as gw.api.web.invoice.AgencyBillStatementView
    }
    
    property set agencyBillStatementView ($arg :  gw.api.web.invoice.AgencyBillStatementView) {
      setVariableValue("agencyBillStatementView", 0, $arg)
    }
    
    property get cycle () : AgencyBillCycle {
      return getRequireValue("cycle", 0) as AgencyBillCycle
    }
    
    property set cycle ($arg :  AgencyBillCycle) {
      setRequireValue("cycle", 0, $arg)
    }
    
    property get showByPolicy () : typekey.StatementViewOption {
      return getVariableValue("showByPolicy", 0) as typekey.StatementViewOption
    }
    
    property set showByPolicy ($arg :  typekey.StatementViewOption) {
      setVariableValue("showByPolicy", 0, $arg)
    }
    
    property get viewOption () : InvoiceItemViewOption {
      return getVariableValue("viewOption", 0) as InvoiceItemViewOption
    }
    
    property set viewOption ($arg :  InvoiceItemViewOption) {
      setVariableValue("viewOption", 0, $arg)
    }
    
    
  }
  
  
}