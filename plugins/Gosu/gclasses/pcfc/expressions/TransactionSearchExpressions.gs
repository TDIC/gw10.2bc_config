package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/TransactionSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransactionSearchExpressions {
  @javax.annotation.Generated("config/web/pcf/search/TransactionSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransactionSearchExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=TransactionSearch) at TransactionSearch.pcf: line 8, column 71
    static function canVisit_2 () : java.lang.Boolean {
      return perm.System.viewsearch and perm.System.txnsearch
    }
    
    // 'def' attribute on ScreenRef at TransactionSearch.pcf: line 10, column 40
    function def_onEnter_0 (def :  pcf.TransactionSearchScreen) : void {
      def.onEnter()
    }
    
    // 'def' attribute on ScreenRef at TransactionSearch.pcf: line 10, column 40
    function def_refreshVariables_1 (def :  pcf.TransactionSearchScreen) : void {
      def.refreshVariables()
    }
    
    // Page (id=TransactionSearch) at TransactionSearch.pcf: line 8, column 71
    static function parent_3 () : pcf.api.Destination {
      return pcf.SearchGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.TransactionSearch {
      return super.CurrentLocation as pcf.TransactionSearch
    }
    
    
  }
  
  
}