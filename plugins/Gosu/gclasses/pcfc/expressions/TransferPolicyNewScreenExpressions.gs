package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyNewScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransferPolicyNewScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyNewScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransferPolicyNewScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=ProducerPicker) at TransferPolicyNewScreen.pcf: line 37, column 39
    function action_4 () : void {
      ProducerSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=ProducerPicker) at TransferPolicyNewScreen.pcf: line 37, column 39
    function action_dest_5 () : pcf.api.Destination {
      return pcf.ProducerSearchPopup.createDestination()
    }
    
    // 'value' attribute on RangeInput (id=ProducerCode_Input) at TransferPolicyNewScreen.pcf: line 54, column 44
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyTransfer.DestinationProducerCode = (__VALUE_TO_SET as entity.ProducerCode)
    }
    
    // 'value' attribute on RangeRadioInput (id=CommissionTransferOption_Input) at TransferPolicyNewScreen.pcf: line 66, column 57
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyTransfer.CommissionTransferOption = (__VALUE_TO_SET as typekey.CommissionTransferOption)
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at TransferPolicyNewScreen.pcf: line 37, column 39
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      wizardHelper.DestinationProducer = (__VALUE_TO_SET as entity.Producer)
    }
    
    // 'initialValue' attribute on Variable at TransferPolicyNewScreen.pcf: line 13, column 59
    function initialValue_0 () : gw.api.web.producer.ProducerSearchConverter {
      return new gw.api.web.producer.ProducerSearchConverter()
    }
    
    // 'initialValue' attribute on Variable at TransferPolicyNewScreen.pcf: line 17, column 23
    function initialValue_1 () : boolean {
      return policyTransfer.PolicyPeriodsToTransfer.firstWhere( \ p -> p.AgencyBill ) == null
    }
    
    // 'initialValue' attribute on Variable at TransferPolicyNewScreen.pcf: line 21, column 52
    function initialValue_2 () : gw.policy.TransferPolicyWizardHelper {
      return new gw.policy.TransferPolicyWizardHelper(policyTransfer)
    }
    
    // 'inputConversion' attribute on TextInput (id=Producer_Input) at TransferPolicyNewScreen.pcf: line 37, column 39
    function inputConversion_7 (VALUE :  java.lang.String) : java.lang.Object {
      return producerSearchConverter.getProducer(VALUE)
    }
    
    // 'onChange' attribute on PostOnChange at TransferPolicyNewScreen.pcf: line 44, column 67
    function onChange_3 () : void {
      wizardHelper.autoSelectSoleProducerCode()
    }
    
    // 'onPick' attribute on TextInput (id=Producer_Input) at TransferPolicyNewScreen.pcf: line 37, column 39
    function onPick_6 (PickedValue :  Producer) : void {
      wizardHelper.autoSelectSoleProducerCode()
    }
    
    // 'optionLabel' attribute on RangeInput (id=ProducerCode_Input) at TransferPolicyNewScreen.pcf: line 54, column 44
    function optionLabel_16 (VALUE :  entity.ProducerCode) : java.lang.String {
      return VALUE.Code
    }
    
    // 'optionLabel' attribute on RangeRadioInput (id=CommissionTransferOption_Input) at TransferPolicyNewScreen.pcf: line 66, column 57
    function optionLabel_24 (VALUE :  typekey.CommissionTransferOption) : java.lang.String {
      return VALUE.Description
    }
    
    // 'required' attribute on RangeInput (id=ProducerCode_Input) at TransferPolicyNewScreen.pcf: line 54, column 44
    function required_12 () : java.lang.Boolean {
      return not allDirectBillPolicyPeriods
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at TransferPolicyNewScreen.pcf: line 54, column 44
    function valueRange_17 () : java.lang.Object {
      return wizardHelper.DestinationProducer.ActiveProducerCodes
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=CommissionTransferOption_Input) at TransferPolicyNewScreen.pcf: line 66, column 57
    function valueRange_25 () : java.lang.Object {
      return wizardHelper.CommissionTransferOptions
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at TransferPolicyNewScreen.pcf: line 37, column 39
    function valueRoot_10 () : java.lang.Object {
      return wizardHelper
    }
    
    // 'value' attribute on RangeInput (id=ProducerCode_Input) at TransferPolicyNewScreen.pcf: line 54, column 44
    function valueRoot_15 () : java.lang.Object {
      return policyTransfer
    }
    
    // 'value' attribute on RangeInput (id=ProducerCode_Input) at TransferPolicyNewScreen.pcf: line 54, column 44
    function value_13 () : entity.ProducerCode {
      return policyTransfer.DestinationProducerCode
    }
    
    // 'value' attribute on RangeRadioInput (id=CommissionTransferOption_Input) at TransferPolicyNewScreen.pcf: line 66, column 57
    function value_21 () : typekey.CommissionTransferOption {
      return policyTransfer.CommissionTransferOption
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at TransferPolicyNewScreen.pcf: line 37, column 39
    function value_8 () : entity.Producer {
      return wizardHelper.DestinationProducer
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at TransferPolicyNewScreen.pcf: line 54, column 44
    function verifyValueRangeIsAllowedType_18 ($$arg :  entity.ProducerCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at TransferPolicyNewScreen.pcf: line 54, column 44
    function verifyValueRangeIsAllowedType_18 ($$arg :  gw.api.database.IQueryBeanResult<entity.ProducerCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at TransferPolicyNewScreen.pcf: line 54, column 44
    function verifyValueRangeIsAllowedType_18 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=CommissionTransferOption_Input) at TransferPolicyNewScreen.pcf: line 66, column 57
    function verifyValueRangeIsAllowedType_26 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=CommissionTransferOption_Input) at TransferPolicyNewScreen.pcf: line 66, column 57
    function verifyValueRangeIsAllowedType_26 ($$arg :  typekey.CommissionTransferOption[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at TransferPolicyNewScreen.pcf: line 54, column 44
    function verifyValueRange_19 () : void {
      var __valueRangeArg = wizardHelper.DestinationProducer.ActiveProducerCodes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_18(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=CommissionTransferOption_Input) at TransferPolicyNewScreen.pcf: line 66, column 57
    function verifyValueRange_27 () : void {
      var __valueRangeArg = wizardHelper.CommissionTransferOptions
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_26(__valueRangeArg)
    }
    
    property get allDirectBillPolicyPeriods () : boolean {
      return getVariableValue("allDirectBillPolicyPeriods", 0) as java.lang.Boolean
    }
    
    property set allDirectBillPolicyPeriods ($arg :  boolean) {
      setVariableValue("allDirectBillPolicyPeriods", 0, $arg)
    }
    
    property get policyTransfer () : PolicyTransfer {
      return getRequireValue("policyTransfer", 0) as PolicyTransfer
    }
    
    property set policyTransfer ($arg :  PolicyTransfer) {
      setRequireValue("policyTransfer", 0, $arg)
    }
    
    property get producerSearchConverter () : gw.api.web.producer.ProducerSearchConverter {
      return getVariableValue("producerSearchConverter", 0) as gw.api.web.producer.ProducerSearchConverter
    }
    
    property set producerSearchConverter ($arg :  gw.api.web.producer.ProducerSearchConverter) {
      setVariableValue("producerSearchConverter", 0, $arg)
    }
    
    property get wizardHelper () : gw.policy.TransferPolicyWizardHelper {
      return getVariableValue("wizardHelper", 0) as gw.policy.TransferPolicyWizardHelper
    }
    
    property set wizardHelper ($arg :  gw.policy.TransferPolicyWizardHelper) {
      setVariableValue("wizardHelper", 0, $arg)
    }
    
    
  }
  
  
}