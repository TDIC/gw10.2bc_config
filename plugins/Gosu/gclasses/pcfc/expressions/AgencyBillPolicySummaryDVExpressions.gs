package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillPolicySummaryDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillPolicySummaryDVExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillPolicySummaryDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillPolicySummaryDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextInput (id=PolicyNumber_Input) at AgencyBillPolicySummaryDV.pcf: line 21, column 43
    function action_1 () : void {
      PolicyOverview.push( policyPeriod )
    }
    
    // 'action' attribute on TextInput (id=AccountNumber_Input) at AgencyBillPolicySummaryDV.pcf: line 47, column 37
    function action_18 () : void {
      AccountOverview.go(policyPeriod.Account)
    }
    
    // 'action' attribute on TextInput (id=AccountName_Input) at AgencyBillPolicySummaryDV.pcf: line 52, column 60
    function action_23 () : void {
      AccountOverview.go(policyPeriod.Account)
    }
    
    // 'action' attribute on TextInput (id=Producer_Input) at AgencyBillPolicySummaryDV.pcf: line 60, column 38
    function action_28 () : void {
      ProducerDetail.go(policyPeriod.PrimaryPolicyCommission.ProducerCode.Producer)
    }
    
    // 'action' attribute on TextInput (id=AccountNumber_Input) at AgencyBillPolicySummaryDV.pcf: line 47, column 37
    function action_dest_19 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(policyPeriod.Account)
    }
    
    // 'action' attribute on TextInput (id=PolicyNumber_Input) at AgencyBillPolicySummaryDV.pcf: line 21, column 43
    function action_dest_2 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination( policyPeriod )
    }
    
    // 'action' attribute on TextInput (id=AccountName_Input) at AgencyBillPolicySummaryDV.pcf: line 52, column 60
    function action_dest_24 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(policyPeriod.Account)
    }
    
    // 'action' attribute on TextInput (id=Producer_Input) at AgencyBillPolicySummaryDV.pcf: line 60, column 38
    function action_dest_29 () : pcf.api.Destination {
      return pcf.ProducerDetail.createDestination(policyPeriod.PrimaryPolicyCommission.ProducerCode.Producer)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Unbilled_Input) at AgencyBillPolicySummaryDV.pcf: line 81, column 50
    function currency_41 () : typekey.Currency {
      return policyPeriod.Currency
    }
    
    // 'initialValue' attribute on Variable at AgencyBillPolicySummaryDV.pcf: line 13, column 59
    function initialValue_0 () : gw.web.policy.PolicySummaryFinancialsHelper {
      return new gw.web.policy.PolicySummaryFinancialsHelper(policyPeriod)
    }
    
    // 'value' attribute on TypeKeyInput (id=Product_Input) at AgencyBillPolicySummaryDV.pcf: line 31, column 38
    function valueRoot_10 () : java.lang.Object {
      return policyPeriod.Policy
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at AgencyBillPolicySummaryDV.pcf: line 52, column 60
    function valueRoot_26 () : java.lang.Object {
      return policyPeriod.Account
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at AgencyBillPolicySummaryDV.pcf: line 60, column 38
    function valueRoot_31 () : java.lang.Object {
      return policyPeriod.PrimaryPolicyCommission.ProducerCode
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at AgencyBillPolicySummaryDV.pcf: line 21, column 43
    function valueRoot_4 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Unbilled_Input) at AgencyBillPolicySummaryDV.pcf: line 81, column 50
    function valueRoot_40 () : java.lang.Object {
      return financialsHelper
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CommissionReserved_Input) at AgencyBillPolicySummaryDV.pcf: line 125, column 80
    function valueRoot_64 () : java.lang.Object {
      return policyPeriod.PrimaryPolicyCommission
    }
    
    // 'value' attribute on DateInput (id=PolicyPerEffDate_Input) at AgencyBillPolicySummaryDV.pcf: line 35, column 48
    function value_12 () : java.util.Date {
      return policyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateInput (id=PolicyPerExpirDate_Input) at AgencyBillPolicySummaryDV.pcf: line 39, column 50
    function value_15 () : java.util.Date {
      return policyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at AgencyBillPolicySummaryDV.pcf: line 47, column 37
    function value_20 () : entity.Account {
      return policyPeriod.Account
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at AgencyBillPolicySummaryDV.pcf: line 52, column 60
    function value_25 () : java.lang.String {
      return policyPeriod.Account.AccountNameLocalized
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at AgencyBillPolicySummaryDV.pcf: line 21, column 43
    function value_3 () : java.lang.String {
      return policyPeriod.DisplayName
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at AgencyBillPolicySummaryDV.pcf: line 60, column 38
    function value_30 () : entity.Producer {
      return policyPeriod.PrimaryPolicyCommission.ProducerCode.Producer
    }
    
    // 'value' attribute on TextInput (id=ProducerCode_Input) at AgencyBillPolicySummaryDV.pcf: line 64, column 73
    function value_33 () : java.lang.String {
      return policyPeriod.PrimaryPolicyCommission.ProducerCode.Code
    }
    
    // 'value' attribute on TypeKeyInput (id=BillingMethod_Input) at AgencyBillPolicySummaryDV.pcf: line 70, column 56
    function value_36 () : typekey.PolicyPeriodBillingMethod {
      return policyPeriod.BillingMethod
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Unbilled_Input) at AgencyBillPolicySummaryDV.pcf: line 81, column 50
    function value_39 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.UnbilledAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Billed_Input) at AgencyBillPolicySummaryDV.pcf: line 88, column 48
    function value_43 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.BilledAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PastDue_Input) at AgencyBillPolicySummaryDV.pcf: line 95, column 52
    function value_47 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.DelinquentAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Unpaid_Input) at AgencyBillPolicySummaryDV.pcf: line 102, column 48
    function value_51 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.UnpaidAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Paid_Input) at AgencyBillPolicySummaryDV.pcf: line 109, column 46
    function value_55 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.PaidAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalGross_Input) at AgencyBillPolicySummaryDV.pcf: line 116, column 46
    function value_59 () : gw.pl.currency.MonetaryAmount {
      return financialsHelper.TotalValue
    }
    
    // 'value' attribute on TextInput (id=PrimaryInsured_Name_Input) at AgencyBillPolicySummaryDV.pcf: line 26, column 49
    function value_6 () : entity.PolicyPeriodContact {
      return policyPeriod.PrimaryInsured
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CommissionReserved_Input) at AgencyBillPolicySummaryDV.pcf: line 125, column 80
    function value_63 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.PrimaryPolicyCommission.CommissionReserveBalance
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CommissionAllocated_Input) at AgencyBillPolicySummaryDV.pcf: line 132, column 73
    function value_67 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.PrimaryPolicyCommission.CommissionSettled
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalCommission_Input) at AgencyBillPolicySummaryDV.pcf: line 139, column 52
    function value_71 () : gw.pl.currency.MonetaryAmount {
      return policyPeriod.getTotalCommission()
    }
    
    // 'value' attribute on TypeKeyInput (id=Product_Input) at AgencyBillPolicySummaryDV.pcf: line 31, column 38
    function value_9 () : typekey.LOBCode {
      return policyPeriod.Policy.LOBCode
    }
    
    property get financialsHelper () : gw.web.policy.PolicySummaryFinancialsHelper {
      return getVariableValue("financialsHelper", 0) as gw.web.policy.PolicySummaryFinancialsHelper
    }
    
    property set financialsHelper ($arg :  gw.web.policy.PolicySummaryFinancialsHelper) {
      setVariableValue("financialsHelper", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}