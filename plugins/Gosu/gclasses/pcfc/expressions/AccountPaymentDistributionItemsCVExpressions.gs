package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountPaymentDistributionItemsCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountPaymentDistributionItemsCVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountPaymentDistributionItemsCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountPaymentDistributionItemsCVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at AccountPaymentDistributionItemsCV.pcf: line 14, column 33
    function initialValue_0 () : DirectBillPayment {
      return selectedMoney.DirectBillPayment
    }
    
    // 'initialValue' attribute on Variable at AccountPaymentDistributionItemsCV.pcf: line 18, column 23
    function initialValue_1 () : Account {
      return selectedMoney.Account
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at AccountPaymentDistributionItemsCV.pcf: line 54, column 60
    function sortValue_3 (distItem :  DirectBillPaymentItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (distItem.InvoiceItem)
return invoiceItem.Invoice.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=Invoice_Cell) at AccountPaymentDistributionItemsCV.pcf: line 61, column 31
    function sortValue_4 (distItem :  DirectBillPaymentItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (distItem.InvoiceItem)
return invoiceItem.Invoice
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AccountPaymentDistributionItemsCV.pcf: line 131, column 29
    function sortValue_47 (item :  entity.BaseSuspDistItem) : java.lang.Object {
      return item.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AccountPaymentDistributionItemsCV.pcf: line 148, column 45
    function sortValue_48 (item :  entity.BaseSuspDistItem) : java.lang.Object {
      return item.PaymentComments
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at AccountPaymentDistributionItemsCV.pcf: line 65, column 53
    function sortValue_5 (distItem :  DirectBillPaymentItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (distItem.InvoiceItem)
return invoiceItem.InvoiceDueDate
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at AccountPaymentDistributionItemsCV.pcf: line 80, column 31
    function sortValue_6 (distItem :  DirectBillPaymentItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (distItem.InvoiceItem)
return invoiceItem.Type
    }
    
    // 'value' attribute on TextCell (id=Collateral_Cell) at AccountPaymentDistributionItemsCV.pcf: line 187, column 46
    function sortValue_69 (item :  entity.CollateralPaymentItem) : java.lang.Object {
      return account.Collateral
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AccountPaymentDistributionItemsCV.pcf: line 87, column 44
    function sortValue_7 (distItem :  DirectBillPaymentItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (distItem.InvoiceItem)
return invoiceItem.Amount
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AccountPaymentDistributionItemsCV.pcf: line 204, column 45
    function sortValue_70 (item :  entity.CollateralPaymentItem) : java.lang.Object {
      return item.PaymentComments
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnpaidAmount_Cell) at AccountPaymentDistributionItemsCV.pcf: line 96, column 59
    function sortValue_8 (distItem :  DirectBillPaymentItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (distItem.InvoiceItem)
return invoiceItem.GrossUnsettledAmount
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at AccountPaymentDistributionItemsCV.pcf: line 244, column 60
    function sortValue_88 (distItem :  DirectBillPaymentItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (distItem.InvoiceItem)
return invoiceItem.Invoice.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=Invoice_Cell) at AccountPaymentDistributionItemsCV.pcf: line 250, column 45
    function sortValue_89 (distItem :  DirectBillPaymentItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (distItem.InvoiceItem)
return invoiceItem.Invoice
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AccountPaymentDistributionItemsCV.pcf: line 101, column 54
    function sortValue_9 (distItem :  DirectBillPaymentItem) : java.lang.Object {
      return distItem.GrossAmountToApply
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at AccountPaymentDistributionItemsCV.pcf: line 254, column 53
    function sortValue_90 (distItem :  DirectBillPaymentItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (distItem.InvoiceItem)
return invoiceItem.InvoiceDueDate
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at AccountPaymentDistributionItemsCV.pcf: line 267, column 54
    function sortValue_91 (distItem :  DirectBillPaymentItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (distItem.InvoiceItem)
return invoiceItem.Type
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AccountPaymentDistributionItemsCV.pcf: line 275, column 44
    function sortValue_92 (distItem :  DirectBillPaymentItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (distItem.InvoiceItem)
return invoiceItem.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnpaidAmount_Cell) at AccountPaymentDistributionItemsCV.pcf: line 285, column 59
    function sortValue_93 (distItem :  DirectBillPaymentItem) : java.lang.Object {
      var invoiceItem : InvoiceItem = (distItem.InvoiceItem)
return invoiceItem.GrossUnsettledAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AccountPaymentDistributionItemsCV.pcf: line 290, column 54
    function sortValue_94 (distItem :  DirectBillPaymentItem) : java.lang.Object {
      return distItem.GrossAmountToApply
    }
    
    // 'value' attribute on RowIterator at AccountPaymentDistributionItemsCV.pcf: line 234, column 47
    function value_129 () : entity.BaseDistItem[] {
      return selectedPayment.ReversedDistItems
    }
    
    // 'value' attribute on RowIterator at AccountPaymentDistributionItemsCV.pcf: line 319, column 75
    function value_156 () : java.util.List<entity.PaymentMoneyReceived> {
      return selectedMoney.PreviousMonies
    }
    
    // 'value' attribute on RowIterator at AccountPaymentDistributionItemsCV.pcf: line 44, column 67
    function value_44 () : java.util.List<entity.BaseDistItem> {
      return selectedPayment.DistItemsList
    }
    
    // 'value' attribute on RowIterator (id=SuspenseItems) at AccountPaymentDistributionItemsCV.pcf: line 121, column 49
    function value_66 () : entity.BaseSuspDistItem[] {
      return selectedPayment.SuspDistItemsThatHaveNotBeenReleased
    }
    
    // 'value' attribute on RowIterator (id=CollateralItems) at AccountPaymentDistributionItemsCV.pcf: line 178, column 54
    function value_85 () : entity.CollateralPaymentItem[] {
      return  selectedPayment.CollateralPaymentItems.where(\ c -> c.ReversedDate == null)
    }
    
    // 'type' attribute on RowIterator at AccountPaymentDistributionItemsCV.pcf: line 234, column 47
    function verifyIteratorType_130 () : void {
      var entry : entity.BaseDistItem = null
      var typedEntry : DirectBillPaymentItem
      // If this cast fails to compile then the type specified by the 'type' attribute on
      // 'RowIterator' is not compatible with the member type of the 'valueType' attribute
      typedEntry = entry as DirectBillPaymentItem
    }
    
    // 'type' attribute on RowIterator at AccountPaymentDistributionItemsCV.pcf: line 44, column 67
    function verifyIteratorType_45 () : void {
      var entry : entity.BaseDistItem = null
      var typedEntry : DirectBillPaymentItem
      // If this cast fails to compile then the type specified by the 'type' attribute on
      // 'RowIterator' is not compatible with the member type of the 'valueType' attribute
      typedEntry = entry as DirectBillPaymentItem
    }
    
    // 'visible' attribute on Card (id=ReversedItemsDetails) at AccountPaymentDistributionItemsCV.pcf: line 212, column 64
    function visible_132 () : java.lang.Boolean {
      return selectedPayment.ReversedDistItems.Count > 0
    }
    
    // 'visible' attribute on Card (id=reversedMonies) at AccountPaymentDistributionItemsCV.pcf: line 299, column 53
    function visible_157 () : java.lang.Boolean {
      return !selectedMoney.PreviousMonies.Empty
    }
    
    // 'visible' attribute on DetailViewPanel (id=AccountPaymentDistributionFrozenWarningDV) at AccountPaymentDistributionItemsCV.pcf: line 25, column 81
    function visible_2 () : java.lang.Boolean {
      return selectedPayment != null && selectedPayment.isFrozen()
    }
    
    // 'visible' attribute on PanelRef at AccountPaymentDistributionItemsCV.pcf: line 34, column 43
    function visible_46 () : java.lang.Boolean {
      return selectedPayment != null
    }
    
    // 'visible' attribute on Card (id=suspenseItemsCard) at AccountPaymentDistributionItemsCV.pcf: line 110, column 79
    function visible_67 () : java.lang.Boolean {
      return !selectedPayment.SuspDistItemsThatHaveNotBeenReleased.IsEmpty
    }
    
    // 'visible' attribute on Card (id=collateralItemsCard) at AccountPaymentDistributionItemsCV.pcf: line 156, column 163
    function visible_86 () : java.lang.Boolean {
      return selectedPayment.CollateralPaymentItems.HasElements and selectedPayment.CollateralPaymentItems.where(\ c -> c.ReversedDate == null).HasElements
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get selectedMoney () : DirectBillMoneyRcvd {
      return getRequireValue("selectedMoney", 0) as DirectBillMoneyRcvd
    }
    
    property set selectedMoney ($arg :  DirectBillMoneyRcvd) {
      setRequireValue("selectedMoney", 0, $arg)
    }
    
    property get selectedPayment () : DirectBillPayment {
      return getVariableValue("selectedPayment", 0) as DirectBillPayment
    }
    
    property set selectedPayment ($arg :  DirectBillPayment) {
      setVariableValue("selectedPayment", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountPaymentDistributionItemsCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends AccountPaymentDistributionItemsCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AccountPaymentDistributionItemsCV.pcf: line 140, column 47
    function currency_59 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AccountPaymentDistributionItemsCV.pcf: line 131, column 29
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      item.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AccountPaymentDistributionItemsCV.pcf: line 140, column 47
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      item.GrossAmountToApply = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AccountPaymentDistributionItemsCV.pcf: line 148, column 45
    function defaultSetter_63 (__VALUE_TO_SET :  java.lang.Object) : void {
      item.PaymentComments = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextCell (id=PolicyNumber_Cell) at AccountPaymentDistributionItemsCV.pcf: line 131, column 29
    function editable_49 () : java.lang.Boolean {
      return !item.Executed
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AccountPaymentDistributionItemsCV.pcf: line 131, column 29
    function valueRoot_52 () : java.lang.Object {
      return item
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AccountPaymentDistributionItemsCV.pcf: line 131, column 29
    function value_50 () : java.lang.String {
      return item.PolicyNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AccountPaymentDistributionItemsCV.pcf: line 140, column 47
    function value_56 () : gw.pl.currency.MonetaryAmount {
      return item.GrossAmountToApply
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AccountPaymentDistributionItemsCV.pcf: line 148, column 45
    function value_62 () : java.lang.String {
      return item.PaymentComments
    }
    
    property get item () : entity.BaseSuspDistItem {
      return getIteratedValue(1) as entity.BaseSuspDistItem
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountPaymentDistributionItemsCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends AccountPaymentDistributionItemsCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AccountPaymentDistributionItemsCV.pcf: line 196, column 47
    function currency_78 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AccountPaymentDistributionItemsCV.pcf: line 196, column 47
    function defaultSetter_76 (__VALUE_TO_SET :  java.lang.Object) : void {
      item.GrossAmountToApply = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AccountPaymentDistributionItemsCV.pcf: line 204, column 45
    function defaultSetter_82 (__VALUE_TO_SET :  java.lang.Object) : void {
      item.PaymentComments = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AccountPaymentDistributionItemsCV.pcf: line 196, column 47
    function editable_74 () : java.lang.Boolean {
      return !item.Executed
    }
    
    // 'value' attribute on TextCell (id=Collateral_Cell) at AccountPaymentDistributionItemsCV.pcf: line 187, column 46
    function valueRoot_72 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AccountPaymentDistributionItemsCV.pcf: line 196, column 47
    function valueRoot_77 () : java.lang.Object {
      return item
    }
    
    // 'value' attribute on TextCell (id=Collateral_Cell) at AccountPaymentDistributionItemsCV.pcf: line 187, column 46
    function value_71 () : entity.Collateral {
      return account.Collateral
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AccountPaymentDistributionItemsCV.pcf: line 196, column 47
    function value_75 () : gw.pl.currency.MonetaryAmount {
      return item.GrossAmountToApply
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AccountPaymentDistributionItemsCV.pcf: line 204, column 45
    function value_81 () : java.lang.String {
      return item.PaymentComments
    }
    
    property get item () : entity.CollateralPaymentItem {
      return getIteratedValue(1) as entity.CollateralPaymentItem
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountPaymentDistributionItemsCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends AccountPaymentDistributionItemsCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=itemOwner_Cell) at AccountPaymentDistributionItemsCV.pcf: line 261, column 45
    function action_105 () : void {
      AccountSummary.push(invoiceItem.Owner)
    }
    
    // 'action' attribute on TextCell (id=itemOwner_Cell) at AccountPaymentDistributionItemsCV.pcf: line 261, column 45
    function action_dest_106 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination(invoiceItem.Owner)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at AccountPaymentDistributionItemsCV.pcf: line 275, column 44
    function currency_118 () : typekey.Currency {
      return account.Currency
    }
    
    // 'initialValue' attribute on Variable at AccountPaymentDistributionItemsCV.pcf: line 238, column 35
    function initialValue_95 () : InvoiceItem {
      return distItem.InvoiceItem
    }
    
    // RowIterator at AccountPaymentDistributionItemsCV.pcf: line 234, column 47
    function initializeVariables_128 () : void {
        invoiceItem = distItem.InvoiceItem;

    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at AccountPaymentDistributionItemsCV.pcf: line 267, column 54
    function valueRange_112 () : java.lang.Object {
      return gw.api.web.invoice.InvoiceUtil.getUserVisibleInvoiceItemTypes()
    }
    
    // 'value' attribute on TextCell (id=Invoice_Cell) at AccountPaymentDistributionItemsCV.pcf: line 250, column 45
    function valueRoot_100 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AccountPaymentDistributionItemsCV.pcf: line 290, column 54
    function valueRoot_125 () : java.lang.Object {
      return distItem
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at AccountPaymentDistributionItemsCV.pcf: line 244, column 60
    function valueRoot_97 () : java.lang.Object {
      return invoiceItem.Invoice
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at AccountPaymentDistributionItemsCV.pcf: line 254, column 53
    function value_102 () : java.util.Date {
      return invoiceItem.InvoiceDueDate
    }
    
    // 'value' attribute on TextCell (id=itemOwner_Cell) at AccountPaymentDistributionItemsCV.pcf: line 261, column 45
    function value_107 () : entity.Account {
      return invoiceItem.Owner
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at AccountPaymentDistributionItemsCV.pcf: line 267, column 54
    function value_110 () : typekey.InvoiceItemType {
      return invoiceItem.Type
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AccountPaymentDistributionItemsCV.pcf: line 275, column 44
    function value_116 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnpaidAmount_Cell) at AccountPaymentDistributionItemsCV.pcf: line 285, column 59
    function value_120 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.GrossUnsettledAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AccountPaymentDistributionItemsCV.pcf: line 290, column 54
    function value_124 () : gw.pl.currency.MonetaryAmount {
      return distItem.GrossAmountToApply
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at AccountPaymentDistributionItemsCV.pcf: line 244, column 60
    function value_96 () : java.lang.String {
      return invoiceItem.Invoice.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=Invoice_Cell) at AccountPaymentDistributionItemsCV.pcf: line 250, column 45
    function value_99 () : entity.Invoice {
      return invoiceItem.Invoice
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at AccountPaymentDistributionItemsCV.pcf: line 267, column 54
    function verifyValueRangeIsAllowedType_113 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at AccountPaymentDistributionItemsCV.pcf: line 267, column 54
    function verifyValueRangeIsAllowedType_113 ($$arg :  typekey.InvoiceItemType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at AccountPaymentDistributionItemsCV.pcf: line 267, column 54
    function verifyValueRange_114 () : void {
      var __valueRangeArg = gw.api.web.invoice.InvoiceUtil.getUserVisibleInvoiceItemTypes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_113(__valueRangeArg)
    }
    
    property get distItem () : DirectBillPaymentItem {
      return getIteratedValue(1) as DirectBillPaymentItem
    }
    
    property get invoiceItem () : InvoiceItem {
      return getVariableValue("invoiceItem", 1) as InvoiceItem
    }
    
    property set invoiceItem ($arg :  InvoiceItem) {
      setVariableValue("invoiceItem", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountPaymentDistributionItemsCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry5ExpressionsImpl extends AccountPaymentDistributionItemsCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at AccountPaymentDistributionItemsCV.pcf: line 354, column 47
    function currency_154 () : typekey.Currency {
      return account.Currency
    }
    
    // 'fontColor' attribute on TextCell (id=Description_Cell) at AccountPaymentDistributionItemsCV.pcf: line 331, column 52
    function fontColor_137 () : java.lang.Object {
      return moneyReceived.Reversed ? (moneyReceived.Modifying  ? gw.api.web.color.GWColor.THEME_ALERT_INFO : gw.api.web.color.GWColor.THEME_ALERT_ERROR) : null
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AccountPaymentDistributionItemsCV.pcf: line 325, column 53
    function valueRoot_135 () : java.lang.Object {
      return moneyReceived
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AccountPaymentDistributionItemsCV.pcf: line 325, column 53
    function value_134 () : java.util.Date {
      return moneyReceived.ReceivedDate
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AccountPaymentDistributionItemsCV.pcf: line 331, column 52
    function value_138 () : java.lang.String {
      return moneyReceived.Description
    }
    
    // 'value' attribute on DateCell (id=Reversed_Cell) at AccountPaymentDistributionItemsCV.pcf: line 336, column 53
    function value_143 () : java.util.Date {
      return moneyReceived.ReversalDate
    }
    
    // 'value' attribute on TextCell (id=Method_Cell) at AccountPaymentDistributionItemsCV.pcf: line 342, column 55
    function value_146 () : entity.PaymentInstrument {
      return moneyReceived.PaymentInstrument
    }
    
    // 'value' attribute on TextCell (id=CheckRef_Cell) at AccountPaymentDistributionItemsCV.pcf: line 347, column 50
    function value_149 () : java.lang.String {
      return moneyReceived.RefNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AccountPaymentDistributionItemsCV.pcf: line 354, column 47
    function value_152 () : gw.pl.currency.MonetaryAmount {
      return moneyReceived.Amount
    }
    
    // 'fontColor' attribute on TextCell (id=Description_Cell) at AccountPaymentDistributionItemsCV.pcf: line 331, column 52
    function verifyFontColorIsAllowedType_140 ($$arg :  gw.api.web.color.GWColor) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on TextCell (id=Description_Cell) at AccountPaymentDistributionItemsCV.pcf: line 331, column 52
    function verifyFontColorIsAllowedType_140 ($$arg :  java.lang.String) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on TextCell (id=Description_Cell) at AccountPaymentDistributionItemsCV.pcf: line 331, column 52
    function verifyFontColor_141 () : void {
      var __fontColorArg = moneyReceived.Reversed ? (moneyReceived.Modifying  ? gw.api.web.color.GWColor.THEME_ALERT_INFO : gw.api.web.color.GWColor.THEME_ALERT_ERROR) : null
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the fontColor return type is not a valid type for use with a ValueWidget's fontColor
      // It needs to return a String or a GWColor.
      verifyFontColorIsAllowedType_140(__fontColorArg)
    }
    
    property get moneyReceived () : entity.PaymentMoneyReceived {
      return getIteratedValue(1) as entity.PaymentMoneyReceived
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountPaymentDistributionItemsCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountPaymentDistributionItemsCVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=itemOwner_Cell) at AccountPaymentDistributionItemsCV.pcf: line 73, column 45
    function action_20 () : void {
      AccountSummary.push(invoiceItem.Owner)
    }
    
    // 'action' attribute on TextCell (id=itemOwner_Cell) at AccountPaymentDistributionItemsCV.pcf: line 73, column 45
    function action_dest_21 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination(invoiceItem.Owner)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at AccountPaymentDistributionItemsCV.pcf: line 87, column 44
    function currency_33 () : typekey.Currency {
      return account.Currency
    }
    
    // 'initialValue' attribute on Variable at AccountPaymentDistributionItemsCV.pcf: line 48, column 35
    function initialValue_10 () : InvoiceItem {
      return distItem.InvoiceItem
    }
    
    // RowIterator at AccountPaymentDistributionItemsCV.pcf: line 44, column 67
    function initializeVariables_43 () : void {
        invoiceItem = distItem.InvoiceItem;

    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at AccountPaymentDistributionItemsCV.pcf: line 80, column 31
    function valueRange_27 () : java.lang.Object {
      return gw.api.web.invoice.InvoiceUtil.getUserVisibleInvoiceItemTypes()
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at AccountPaymentDistributionItemsCV.pcf: line 54, column 60
    function valueRoot_12 () : java.lang.Object {
      return invoiceItem.Invoice
    }
    
    // 'value' attribute on TextCell (id=Invoice_Cell) at AccountPaymentDistributionItemsCV.pcf: line 61, column 31
    function valueRoot_15 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AccountPaymentDistributionItemsCV.pcf: line 101, column 54
    function valueRoot_40 () : java.lang.Object {
      return distItem
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at AccountPaymentDistributionItemsCV.pcf: line 54, column 60
    function value_11 () : java.lang.String {
      return invoiceItem.Invoice.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=Invoice_Cell) at AccountPaymentDistributionItemsCV.pcf: line 61, column 31
    function value_14 () : entity.Invoice {
      return invoiceItem.Invoice
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at AccountPaymentDistributionItemsCV.pcf: line 65, column 53
    function value_17 () : java.util.Date {
      return invoiceItem.InvoiceDueDate
    }
    
    // 'value' attribute on TextCell (id=itemOwner_Cell) at AccountPaymentDistributionItemsCV.pcf: line 73, column 45
    function value_22 () : entity.Account {
      return invoiceItem.Owner
    }
    
    // 'value' attribute on RangeCell (id=Type_Cell) at AccountPaymentDistributionItemsCV.pcf: line 80, column 31
    function value_25 () : typekey.InvoiceItemType {
      return invoiceItem.Type
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AccountPaymentDistributionItemsCV.pcf: line 87, column 44
    function value_31 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnpaidAmount_Cell) at AccountPaymentDistributionItemsCV.pcf: line 96, column 59
    function value_35 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.GrossUnsettledAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at AccountPaymentDistributionItemsCV.pcf: line 101, column 54
    function value_39 () : gw.pl.currency.MonetaryAmount {
      return distItem.GrossAmountToApply
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at AccountPaymentDistributionItemsCV.pcf: line 80, column 31
    function verifyValueRangeIsAllowedType_28 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at AccountPaymentDistributionItemsCV.pcf: line 80, column 31
    function verifyValueRangeIsAllowedType_28 ($$arg :  typekey.InvoiceItemType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Type_Cell) at AccountPaymentDistributionItemsCV.pcf: line 80, column 31
    function verifyValueRange_29 () : void {
      var __valueRangeArg = gw.api.web.invoice.InvoiceUtil.getUserVisibleInvoiceItemTypes()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_28(__valueRangeArg)
    }
    
    property get distItem () : DirectBillPaymentItem {
      return getIteratedValue(1) as DirectBillPaymentItem
    }
    
    property get invoiceItem () : InvoiceItem {
      return getVariableValue("invoiceItem", 1) as InvoiceItem
    }
    
    property set invoiceItem ($arg :  InvoiceItem) {
      setVariableValue("invoiceItem", 1, $arg)
    }
    
    
  }
  
  
}