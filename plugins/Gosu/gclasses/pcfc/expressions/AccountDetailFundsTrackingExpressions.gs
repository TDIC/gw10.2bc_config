package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailFundsTracking.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailFundsTrackingExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailFundsTracking.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailFundsTrackingExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'afterEnter' attribute on Page (id=AccountDetailFundsTracking) at AccountDetailFundsTracking.pcf: line 10, column 78
    function afterEnter_4 () : void {
      gw.pcf.fundstracking.AccountDetailFundsTrackingHelper.allotFundsOnEnter(account)
    }
    
    // 'afterReturnFromPopup' attribute on Page (id=AccountDetailFundsTracking) at AccountDetailFundsTracking.pcf: line 10, column 78
    function afterReturnFromPopup_5 (popupCommitted :  boolean) : void {
      gw.pcf.fundstracking.AccountDetailFundsTrackingHelper.allotFundsOnEnter(account)
    }
    
    // 'canVisit' attribute on Page (id=AccountDetailFundsTracking) at AccountDetailFundsTracking.pcf: line 10, column 78
    static function canVisit_6 (account :  Account) : java.lang.Boolean {
      return gw.api.domain.fundstracking.FundsTrackingSwitch.isEnabled() and perm.System.acctfundstrackingview
    }
    
    // 'def' attribute on PanelRef (id=FundsSources) at AccountDetailFundsTracking.pcf: line 24, column 31
    function def_onEnter_0 (def :  pcf.FundsTrackerLV) : void {
      def.onEnter(account, entity.FundsSourceTracker.Type)
    }
    
    // 'def' attribute on PanelRef (id=FundsUses) at AccountDetailFundsTracking.pcf: line 33, column 28
    function def_onEnter_2 (def :  pcf.FundsTrackerLV) : void {
      def.onEnter(account, entity.FundsUseTracker.Type)
    }
    
    // 'def' attribute on PanelRef (id=FundsSources) at AccountDetailFundsTracking.pcf: line 24, column 31
    function def_refreshVariables_1 (def :  pcf.FundsTrackerLV) : void {
      def.refreshVariables(account, entity.FundsSourceTracker.Type)
    }
    
    // 'def' attribute on PanelRef (id=FundsUses) at AccountDetailFundsTracking.pcf: line 33, column 28
    function def_refreshVariables_3 (def :  pcf.FundsTrackerLV) : void {
      def.refreshVariables(account, entity.FundsUseTracker.Type)
    }
    
    // Page (id=AccountDetailFundsTracking) at AccountDetailFundsTracking.pcf: line 10, column 78
    static function parent_7 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.AccountDetailFundsTracking {
      return super.CurrentLocation as pcf.AccountDetailFundsTracking
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  
}