package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.domain.fundstracking.FundsTrackingSwitchView
@javax.annotation.Generated("config/web/pcf/tools/FundsTracking.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class FundsTrackingExpressions {
  @javax.annotation.Generated("config/web/pcf/tools/FundsTracking.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FundsTrackingExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on Link (id=FundsTrackingButton) at FundsTracking.pcf: line 22, column 40
    function action_0 () : void {
      gw.api.domain.fundstracking.FundsTrackingSwitchView.toggleFundsTracking()
    }
    
    // 'confirmMessage' attribute on Link (id=FundsTrackingButton) at FundsTracking.pcf: line 22, column 40
    function confirmMessage_1 () : java.lang.String {
      return getConfirmMessage()
    }
    
    // 'label' attribute on Link (id=FundsTrackingButton) at FundsTracking.pcf: line 22, column 40
    function label_2 () : java.lang.Object {
      return getButtonLabel()
    }
    
    // 'label' attribute on ContentInput at FundsTracking.pcf: line 16, column 37
    function label_3 () : java.lang.Object {
      return getInputLabel()
    }
    
    // 'parent' attribute on Page (id=FundsTracking) at FundsTracking.pcf: line 8, column 73
    static function parent_4 () : pcf.api.Destination {
      return pcf.ServerTools.createDestination()
    }
    
    override property get CurrentLocation () : pcf.FundsTracking {
      return super.CurrentLocation as pcf.FundsTracking
    }
    
    
    function getButtonLabel(): String {
      if (FundsTrackingSwitchView.Enabled) {
        return DisplayKey.get("Web.InternalTools.FundsTracking.Button.Disable")
      } else {
        return DisplayKey.get("Web.InternalTools.FundsTracking.Button.Enable")
      }
    }
    
    function getInputLabel(): String {
      if (FundsTrackingSwitchView.Enabled) {
        return DisplayKey.get("Web.InternalTools.FundsTracking.Label.Enabled")
      } else {
        return DisplayKey.get("Web.InternalTools.FundsTracking.Label.Disabled")
      }
    }
    
    function getConfirmMessage(): String {
      if (FundsTrackingSwitchView.Enabled) {
        return DisplayKey.get("Web.InternalTools.FundsTracking.Confirm.Disable")
      } else {
        return DisplayKey.get("Web.InternalTools.FundsTracking.Confirm.Enable")
      }
    }
    
    
  }
  
  
}