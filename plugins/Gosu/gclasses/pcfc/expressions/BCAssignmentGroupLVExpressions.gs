package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/groups/BCAssignmentGroupLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BCAssignmentGroupLVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/groups/BCAssignmentGroupLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BCAssignmentGroupLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on TextCell (id=Group_Cell) at BCAssignmentGroupLV.pcf: line 25, column 38
    function sortValue_0 (group :  entity.Group) : java.lang.Object {
      return group.Name
    }
    
    // 'value' attribute on AltUserCell (id=Supervisor_Cell) at BCAssignmentGroupLV.pcf: line 29, column 37
    function sortValue_1 (group :  entity.Group) : java.lang.Object {
      return group.Supervisor
    }
    
    // 'value' attribute on RowIterator at BCAssignmentGroupLV.pcf: line 18, column 72
    function value_11 () : gw.api.database.IQueryBeanResult<entity.Group> {
      return Groups
    }
    
    property get Groups () : gw.api.database.IQueryBeanResult<Group> {
      return getRequireValue("Groups", 0) as gw.api.database.IQueryBeanResult<Group>
    }
    
    property set Groups ($arg :  gw.api.database.IQueryBeanResult<Group>) {
      setRequireValue("Groups", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/groups/BCAssignmentGroupLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends BCAssignmentGroupLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at BCAssignmentGroupLV.pcf: line 29, column 37
    function action_5 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // MenuItem (id=UserBrowseMenuItem) at BCAssignmentGroupLV.pcf: line 29, column 37
    function action_dest_6 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'pickValue' attribute on RowIterator at BCAssignmentGroupLV.pcf: line 18, column 72
    function pickValue_10 () : gw.api.assignment.Assignee {
      return group
    }
    
    // 'value' attribute on TextCell (id=Group_Cell) at BCAssignmentGroupLV.pcf: line 25, column 38
    function valueRoot_3 () : java.lang.Object {
      return group
    }
    
    // 'value' attribute on TextCell (id=Group_Cell) at BCAssignmentGroupLV.pcf: line 25, column 38
    function value_2 () : java.lang.String {
      return group.DisplayName
    }
    
    // 'value' attribute on AltUserCell (id=Supervisor_Cell) at BCAssignmentGroupLV.pcf: line 29, column 37
    function value_7 () : entity.User {
      return group.Supervisor
    }
    
    property get group () : entity.Group {
      return getIteratedValue(1) as entity.Group
    }
    
    
  }
  
  
}