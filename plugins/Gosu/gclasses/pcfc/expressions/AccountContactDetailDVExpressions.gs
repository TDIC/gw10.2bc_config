package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountContactDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountContactDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountContactDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountContactDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at AccountContactDetailDV.pcf: line 44, column 43
    function def_onEnter_17 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at AccountContactDetailDV.pcf: line 44, column 43
    function def_onEnter_19 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at AccountContactDetailDV.pcf: line 44, column 43
    function def_onEnter_21 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.onEnter(addressOwner)
    }
    
    // 'def' attribute on InputSetRef at AccountContactDetailDV.pcf: line 69, column 27
    function def_onEnter_45 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.BusinessPhoneOwner(new gw.api.phone.ContactPhoneDelegate(accountContact.Contact, Contact#WorkPhone), DisplayKey.get("Web.ContactDetailDV.Phone"), false))
    }
    
    // 'def' attribute on InputSetRef at AccountContactDetailDV.pcf: line 30, column 97
    function def_onEnter_5 (def :  pcf.NameInputSet_Contact) : void {
      def.onEnter(new gw.api.name.BCNameOwner(accountContact.Contact))
    }
    
    // 'def' attribute on InputSetRef at AccountContactDetailDV.pcf: line 30, column 97
    function def_onEnter_7 (def :  pcf.NameInputSet_Person) : void {
      def.onEnter(new gw.api.name.BCNameOwner(accountContact.Contact))
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at AccountContactDetailDV.pcf: line 44, column 43
    function def_refreshVariables_18 (def :  pcf.GlobalAddressInputSet_BigToSmall) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at AccountContactDetailDV.pcf: line 44, column 43
    function def_refreshVariables_20 (def :  pcf.GlobalAddressInputSet_PostCodeBeforeCity) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef (id=globalAddressContainer) at AccountContactDetailDV.pcf: line 44, column 43
    function def_refreshVariables_22 (def :  pcf.GlobalAddressInputSet_default) : void {
      def.refreshVariables(addressOwner)
    }
    
    // 'def' attribute on InputSetRef at AccountContactDetailDV.pcf: line 69, column 27
    function def_refreshVariables_46 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.BusinessPhoneOwner(new gw.api.phone.ContactPhoneDelegate(accountContact.Contact, Contact#WorkPhone), DisplayKey.get("Web.ContactDetailDV.Phone"), false))
    }
    
    // 'def' attribute on InputSetRef at AccountContactDetailDV.pcf: line 30, column 97
    function def_refreshVariables_6 (def :  pcf.NameInputSet_Contact) : void {
      def.refreshVariables(new gw.api.name.BCNameOwner(accountContact.Contact))
    }
    
    // 'def' attribute on InputSetRef at AccountContactDetailDV.pcf: line 30, column 97
    function def_refreshVariables_8 (def :  pcf.NameInputSet_Person) : void {
      def.refreshVariables(new gw.api.name.BCNameOwner(accountContact.Contact))
    }
    
    // 'value' attribute on BooleanRadioInput (id=PrimaryPayer_Input) at AccountContactDetailDV.pcf: line 78, column 46
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      accountContact.PrimaryPayer = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'editable' attribute on InputSetRef at AccountContactDetailDV.pcf: line 30, column 97
    function editable_4 () : java.lang.Boolean {
      return isNew
    }
    
    // 'encryptionExpression' attribute on PrivacyInput (id=SSN_Input) at AccountContactDetailDV.pcf: line 52, column 65
    function encryptionExpression_28 (VALUE :  java.lang.String) : java.lang.String {
      return gw.api.privacy.EncryptionMaskExpressions.maskTaxId(VALUE)
    }
    
    // 'initialValue' attribute on Variable at AccountContactDetailDV.pcf: line 17, column 43
    function initialValue_0 () : gw.api.address.AddressOwner {
      return new gw.api.address.ContactAddressOwner(accountContact.Contact)
    }
    
    // 'mode' attribute on InputSetRef (id=globalAddressContainer) at AccountContactDetailDV.pcf: line 44, column 43
    function mode_23 () : java.lang.Object {
      return gw.api.address.AddressCountrySettings.getSettings(addressOwner.SelectedCountry).PCFMode
    }
    
    // 'mode' attribute on InputSetRef at AccountContactDetailDV.pcf: line 30, column 97
    function mode_9 () : java.lang.Object {
      return accountContact.Contact typeis Person ? "Person" : "Contact"
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at AccountContactDetailDV.pcf: line 103, column 50
    function sortValue_54 (accountContactRole :  entity.AccountContactRole) : java.lang.Object {
      return accountContactRole.Role
    }
    
    // 'toAdd' attribute on RowIterator at AccountContactDetailDV.pcf: line 95, column 53
    function toAdd_59 (accountContactRole :  entity.AccountContactRole) : void {
      accountContact.addToRoles(accountContactRole)
    }
    
    // 'toRemove' attribute on RowIterator at AccountContactDetailDV.pcf: line 95, column 53
    function toRemove_60 (accountContactRole :  entity.AccountContactRole) : void {
      accountContact.removeFromRoles(accountContactRole)
    }
    
    // 'value' attribute on TypeKeyInput (id=Credential_Input) at AccountContactDetailDV.pcf: line 36, column 57
    function valueRoot_12 () : java.lang.Object {
      return (accountContact.Contact as Person)
    }
    
    // 'value' attribute on TextInput (id=Type_Input) at AccountContactDetailDV.pcf: line 24, column 60
    function valueRoot_2 () : java.lang.Object {
      return accountContact.Contact.Subtype
    }
    
    // 'value' attribute on PrivacyInput (id=SSN_Input) at AccountContactDetailDV.pcf: line 52, column 65
    function valueRoot_27 () : java.lang.Object {
      return accountContact.Contact
    }
    
    // 'value' attribute on BooleanRadioInput (id=PrimaryPayer_Input) at AccountContactDetailDV.pcf: line 78, column 46
    function valueRoot_52 () : java.lang.Object {
      return accountContact
    }
    
    // 'value' attribute on TextInput (id=Type_Input) at AccountContactDetailDV.pcf: line 24, column 60
    function value_1 () : java.lang.String {
      return accountContact.Contact.Subtype.DisplayName
    }
    
    // 'value' attribute on TypeKeyInput (id=Credential_Input) at AccountContactDetailDV.pcf: line 36, column 57
    function value_11 () : typekey.Credential_TDIC {
      return (accountContact.Contact as Person).Credential_TDIC
    }
    
    // 'value' attribute on PrivacyInput (id=SSN_Input) at AccountContactDetailDV.pcf: line 52, column 65
    function value_26 () : java.lang.String {
      return accountContact.Contact.SSNOfficialID
    }
    
    // 'value' attribute on TextInput (id=FEIN_Input) at AccountContactDetailDV.pcf: line 57, column 66
    function value_32 () : java.lang.String {
      return accountContact.Contact.FEINOfficialID
    }
    
    // 'value' attribute on TextInput (id=ADANumber_Input) at AccountContactDetailDV.pcf: line 62, column 57
    function value_37 () : java.lang.String {
      return accountContact.Contact.ADANumberOfficialID_TDIC
    }
    
    // 'value' attribute on TextInput (id=VendorNumber_Input) at AccountContactDetailDV.pcf: line 66, column 54
    function value_41 () : java.lang.String {
      return accountContact.Contact.VendorNumber
    }
    
    // 'value' attribute on TextInput (id=Email_Input) at AccountContactDetailDV.pcf: line 73, column 55
    function value_47 () : java.lang.String {
      return accountContact.Contact.EmailAddress1
    }
    
    // 'value' attribute on BooleanRadioInput (id=PrimaryPayer_Input) at AccountContactDetailDV.pcf: line 78, column 46
    function value_50 () : java.lang.Boolean {
      return accountContact.PrimaryPayer
    }
    
    // 'value' attribute on RowIterator at AccountContactDetailDV.pcf: line 95, column 53
    function value_61 () : entity.AccountContactRole[] {
      return accountContact.Roles
    }
    
    // 'visible' attribute on TypeKeyInput (id=Credential_Input) at AccountContactDetailDV.pcf: line 36, column 57
    function visible_10 () : java.lang.Boolean {
      return accountContact.Contact typeis Person
    }
    
    // 'visible' attribute on InputDivider at AccountContactDetailDV.pcf: line 38, column 47
    function visible_15 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'visible' attribute on PrivacyInput (id=SSN_Input) at AccountContactDetailDV.pcf: line 52, column 65
    function visible_25 () : java.lang.Boolean {
      return accountContact.Contact.SSNOfficialID != null
    }
    
    // 'visible' attribute on TextInput (id=FEIN_Input) at AccountContactDetailDV.pcf: line 57, column 66
    function visible_31 () : java.lang.Boolean {
      return accountContact.Contact.FEINOfficialID != null
    }
    
    property get accountContact () : AccountContact {
      return getRequireValue("accountContact", 0) as AccountContact
    }
    
    property set accountContact ($arg :  AccountContact) {
      setRequireValue("accountContact", 0, $arg)
    }
    
    property get addressOwner () : gw.api.address.AddressOwner {
      return getVariableValue("addressOwner", 0) as gw.api.address.AddressOwner
    }
    
    property set addressOwner ($arg :  gw.api.address.AddressOwner) {
      setVariableValue("addressOwner", 0, $arg)
    }
    
    property get isNew () : Boolean {
      return getRequireValue("isNew", 0) as Boolean
    }
    
    property set isNew ($arg :  Boolean) {
      setRequireValue("isNew", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountContactDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountContactDetailDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at AccountContactDetailDV.pcf: line 103, column 50
    function defaultSetter_56 (__VALUE_TO_SET :  java.lang.Object) : void {
      accountContactRole.Role = (__VALUE_TO_SET as typekey.AccountRole)
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at AccountContactDetailDV.pcf: line 103, column 50
    function valueRoot_57 () : java.lang.Object {
      return accountContactRole
    }
    
    // 'value' attribute on TypeKeyCell (id=Role_Cell) at AccountContactDetailDV.pcf: line 103, column 50
    function value_55 () : typekey.AccountRole {
      return accountContactRole.Role
    }
    
    property get accountContactRole () : entity.AccountContactRole {
      return getIteratedValue(1) as entity.AccountContactRole
    }
    
    
  }
  
  
}