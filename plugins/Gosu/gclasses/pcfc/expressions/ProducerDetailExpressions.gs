package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=ProducerDetail) at ProducerDetail.pcf: line 10, column 66
    function canEdit_2 () : java.lang.Boolean {
      return perm.Producer.edit
    }
    
    // 'canVisit' attribute on Page (id=ProducerDetail) at ProducerDetail.pcf: line 10, column 66
    static function canVisit_3 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodsummview
    }
    
    // 'def' attribute on ScreenRef at ProducerDetail.pcf: line 17, column 45
    function def_onEnter_0 (def :  pcf.ProducerDetailScreen) : void {
      def.onEnter(producer)
    }
    
    // 'def' attribute on ScreenRef at ProducerDetail.pcf: line 17, column 45
    function def_refreshVariables_1 (def :  pcf.ProducerDetailScreen) : void {
      def.refreshVariables(producer)
    }
    
    // Page (id=ProducerDetail) at ProducerDetail.pcf: line 10, column 66
    static function parent_4 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    override property get CurrentLocation () : pcf.ProducerDetail {
      return super.CurrentLocation as pcf.ProducerDetail
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}