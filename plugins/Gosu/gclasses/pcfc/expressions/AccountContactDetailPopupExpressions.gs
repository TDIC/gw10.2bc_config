package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountContactDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountContactDetailPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountContactDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountContactDetailPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (accountContact :  AccountContact) : int {
      return 0
    }
    
    // 'canVisit' attribute on Popup (id=AccountContactDetailPopup) at AccountContactDetailPopup.pcf: line 11, column 72
    static function canVisit_2 (accountContact :  AccountContact) : java.lang.Boolean {
      return perm.AccountContact.edit
    }
    
    // 'def' attribute on ScreenRef at AccountContactDetailPopup.pcf: line 18, column 149
    function def_onEnter_0 (def :  pcf.NewAccountContactScreen) : void {
      def.onEnter(accountContact, new gw.pcf.duplicatecontacts.DoNothingDuplicateContactsPopupNavigator(accountContact.Contact))
    }
    
    // 'def' attribute on ScreenRef at AccountContactDetailPopup.pcf: line 18, column 149
    function def_refreshVariables_1 (def :  pcf.NewAccountContactScreen) : void {
      def.refreshVariables(accountContact, new gw.pcf.duplicatecontacts.DoNothingDuplicateContactsPopupNavigator(accountContact.Contact))
    }
    
    override property get CurrentLocation () : pcf.AccountContactDetailPopup {
      return super.CurrentLocation as pcf.AccountContactDetailPopup
    }
    
    property get accountContact () : AccountContact {
      return getVariableValue("accountContact", 0) as AccountContact
    }
    
    property set accountContact ($arg :  AccountContact) {
      setVariableValue("accountContact", 0, $arg)
    }
    
    
  }
  
  
}