package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountContactCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountContactCVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountContactCV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountContactCVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at AccountContactCV.pcf: line 17, column 55
    function def_onEnter_0 (def :  pcf.AccountContactDetailDV) : void {
      def.onEnter(contact, isNew)
    }
    
    // 'def' attribute on PanelRef at AccountContactCV.pcf: line 24, column 56
    function def_onEnter_2 (def :  pcf.AccountContactCorrespondenceDV) : void {
      def.onEnter(contact)
    }
    
    // 'def' attribute on PanelRef at AccountContactCV.pcf: line 17, column 55
    function def_refreshVariables_1 (def :  pcf.AccountContactDetailDV) : void {
      def.refreshVariables(contact, isNew)
    }
    
    // 'def' attribute on PanelRef at AccountContactCV.pcf: line 24, column 56
    function def_refreshVariables_3 (def :  pcf.AccountContactCorrespondenceDV) : void {
      def.refreshVariables(contact)
    }
    
    property get contact () : AccountContact {
      return getRequireValue("contact", 0) as AccountContact
    }
    
    property set contact ($arg :  AccountContact) {
      setRequireValue("contact", 0, $arg)
    }
    
    property get isNew () : Boolean {
      return getRequireValue("isNew", 0) as Boolean
    }
    
    property set isNew ($arg :  Boolean) {
      setRequireValue("isNew", 0, $arg)
    }
    
    
  }
  
  
}