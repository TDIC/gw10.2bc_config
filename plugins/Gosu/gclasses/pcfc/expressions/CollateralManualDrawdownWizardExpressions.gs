package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralManualDrawdownWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CollateralManualDrawdownWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/collateral/CollateralManualDrawdownWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CollateralManualDrawdownWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (collateral :  Collateral) : int {
      return 0
    }
    
    // 'allowNext' attribute on WizardStep (id=amountStep) at CollateralManualDrawdownWizard.pcf: line 24, column 95
    function allowNext_1 () : java.lang.Boolean {
      return collateralDrawdown.getAmount().IsNotZero
    }
    
    // 'allowNext' attribute on WizardStep (id=chargeStep) at CollateralManualDrawdownWizard.pcf: line 32, column 95
    function allowNext_4 () : java.lang.Boolean {
      return collateralDrawdown.isChargeAmountValid()
    }
    
    // 'beforeCommit' attribute on Wizard (id=CollateralManualDrawdownWizard) at CollateralManualDrawdownWizard.pcf: line 8, column 41
    function beforeCommit_10 (pickedValue :  java.lang.Object) : void {
      collateralDrawdown.createDrawdownAndCharge()
    }
    
    // 'initialValue' attribute on Variable at CollateralManualDrawdownWizard.pcf: line 17, column 60
    function initialValue_0 () : gw.api.web.collateral.CollateralDrawdownUtil {
      return new gw.api.web.collateral.CollateralDrawdownUtil(collateral)
    }
    
    // 'screen' attribute on WizardStep (id=amountStep) at CollateralManualDrawdownWizard.pcf: line 24, column 95
    function screen_onEnter_2 (def :  pcf.CollateralManualDrawdownAmountScreen) : void {
      def.onEnter(collateralDrawdown)
    }
    
    // 'screen' attribute on WizardStep (id=chargeStep) at CollateralManualDrawdownWizard.pcf: line 32, column 95
    function screen_onEnter_5 (def :  pcf.CollateralManualDrawdownChargeScreen) : void {
      def.onEnter(collateralDrawdown)
    }
    
    // 'screen' attribute on WizardStep (id=confirmStep) at CollateralManualDrawdownWizard.pcf: line 37, column 96
    function screen_onEnter_8 (def :  pcf.CollateralManualDrawdownConfirmScreen) : void {
      def.onEnter(collateralDrawdown)
    }
    
    // 'screen' attribute on WizardStep (id=amountStep) at CollateralManualDrawdownWizard.pcf: line 24, column 95
    function screen_refreshVariables_3 (def :  pcf.CollateralManualDrawdownAmountScreen) : void {
      def.refreshVariables(collateralDrawdown)
    }
    
    // 'screen' attribute on WizardStep (id=chargeStep) at CollateralManualDrawdownWizard.pcf: line 32, column 95
    function screen_refreshVariables_6 (def :  pcf.CollateralManualDrawdownChargeScreen) : void {
      def.refreshVariables(collateralDrawdown)
    }
    
    // 'screen' attribute on WizardStep (id=confirmStep) at CollateralManualDrawdownWizard.pcf: line 37, column 96
    function screen_refreshVariables_9 (def :  pcf.CollateralManualDrawdownConfirmScreen) : void {
      def.refreshVariables(collateralDrawdown)
    }
    
    // 'tabBar' attribute on Wizard (id=CollateralManualDrawdownWizard) at CollateralManualDrawdownWizard.pcf: line 8, column 41
    function tabBar_onEnter_11 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=CollateralManualDrawdownWizard) at CollateralManualDrawdownWizard.pcf: line 8, column 41
    function tabBar_refreshVariables_12 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    // '$$wizardStepAvailable' attribute on WizardStep (id=chargeStep) at CollateralManualDrawdownWizard.pcf: line 32, column 95
    function wizardStepAvailable_7 () : java.lang.Boolean {
      return collateralDrawdown.isCheckboxVisible()
    }
    
    override property get CurrentLocation () : pcf.CollateralManualDrawdownWizard {
      return super.CurrentLocation as pcf.CollateralManualDrawdownWizard
    }
    
    property get collateral () : Collateral {
      return getVariableValue("collateral", 0) as Collateral
    }
    
    property set collateral ($arg :  Collateral) {
      setVariableValue("collateral", 0, $arg)
    }
    
    property get collateralDrawdown () : gw.api.web.collateral.CollateralDrawdownUtil {
      return getVariableValue("collateralDrawdown", 0) as gw.api.web.collateral.CollateralDrawdownUtil
    }
    
    property set collateralDrawdown ($arg :  gw.api.web.collateral.CollateralDrawdownUtil) {
      setVariableValue("collateralDrawdown", 0, $arg)
    }
    
    
  }
  
  
}