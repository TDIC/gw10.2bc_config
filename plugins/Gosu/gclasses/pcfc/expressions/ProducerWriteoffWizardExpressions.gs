package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/ProducerWriteoffWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerWriteoffWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/ProducerWriteoffWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerWriteoffWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'allowFinish' attribute on WizardStep (id=ConfirmationStep) at ProducerWriteoffWizard.pcf: line 30, column 90
    function allowFinish_4 () : java.lang.Boolean {
      return uiWriteoff != null && uiWriteoff.Amount != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=ProducerWriteoffWizard) at ProducerWriteoffWizard.pcf: line 9, column 33
    function beforeCommit_7 (pickedValue :  java.lang.Object) : void {
      uiWriteoff.doWriteOff()
    }
    
    // 'canVisit' attribute on Wizard (id=ProducerWriteoffWizard) at ProducerWriteoffWizard.pcf: line 9, column 33
    static function canVisit_8 (producer :  Producer) : java.lang.Boolean {
      return perm.System.agencywo
    }
    
    // 'initialValue' attribute on Variable at ProducerWriteoffWizard.pcf: line 18, column 56
    function initialValue_0 () : gw.api.web.accounting.UIWriteOffCreation {
      return createUIWriteoff()
    }
    
    // 'onExit' attribute on WizardStep (id=DetailsStep) at ProducerWriteoffWizard.pcf: line 24, column 85
    function onExit_1 () : void {
      onExitFromDetailsStep()
    }
    
    // 'screen' attribute on WizardStep (id=DetailsStep) at ProducerWriteoffWizard.pcf: line 24, column 85
    function screen_onEnter_2 (def :  pcf.NewWriteoffWizardDetailsStepScreen) : void {
      def.onEnter(uiWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at ProducerWriteoffWizard.pcf: line 30, column 90
    function screen_onEnter_5 (def :  pcf.NewWriteoffWizardConfirmationStepScreen) : void {
      def.onEnter(uiWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=DetailsStep) at ProducerWriteoffWizard.pcf: line 24, column 85
    function screen_refreshVariables_3 (def :  pcf.NewWriteoffWizardDetailsStepScreen) : void {
      def.refreshVariables(uiWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at ProducerWriteoffWizard.pcf: line 30, column 90
    function screen_refreshVariables_6 (def :  pcf.NewWriteoffWizardConfirmationStepScreen) : void {
      def.refreshVariables(uiWriteoff)
    }
    
    // 'tabBar' attribute on Wizard (id=ProducerWriteoffWizard) at ProducerWriteoffWizard.pcf: line 9, column 33
    function tabBar_onEnter_9 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=ProducerWriteoffWizard) at ProducerWriteoffWizard.pcf: line 9, column 33
    function tabBar_refreshVariables_10 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.ProducerWriteoffWizard {
      return super.CurrentLocation as pcf.ProducerWriteoffWizard
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    property get uiWriteoff () : gw.api.web.accounting.UIWriteOffCreation {
      return getVariableValue("uiWriteoff", 0) as gw.api.web.accounting.UIWriteOffCreation
    }
    
    property set uiWriteoff ($arg :  gw.api.web.accounting.UIWriteOffCreation) {
      setVariableValue("uiWriteoff", 0, $arg)
    }
    
    
                  function onExitFromDetailsStep() {
                    uiWriteoff.initiateApprovalActivityIfUserLacksAuthority()
                  }
    
                  function createUIWriteoff(): gw.api.web.accounting.UIWriteOffCreation {
                    var factory = new gw.api.web.accounting.WriteOffFactory(CurrentLocation)
                    var writeOff = factory.createProducerWriteOff(producer)
                    return new gw.api.web.accounting.UIWriteOffCreation(writeOff)
                  }
          
    
    
  }
  
  
}