package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DelinquencyProcessSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyProcessSearchExpressions {
  @javax.annotation.Generated("config/web/pcf/search/DelinquencyProcessSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyProcessSearchExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=DelinquencyProcessSearch) at DelinquencyProcessSearch.pcf: line 8, column 79
    static function canVisit_2 () : java.lang.Boolean {
      return perm.System.viewsearch and perm.System.delsearch
    }
    
    // 'def' attribute on ScreenRef at DelinquencyProcessSearch.pcf: line 10, column 47
    function def_onEnter_0 (def :  pcf.DelinquencyProcessSearchScreen) : void {
      def.onEnter()
    }
    
    // 'def' attribute on ScreenRef at DelinquencyProcessSearch.pcf: line 10, column 47
    function def_refreshVariables_1 (def :  pcf.DelinquencyProcessSearchScreen) : void {
      def.refreshVariables()
    }
    
    // Page (id=DelinquencyProcessSearch) at DelinquencyProcessSearch.pcf: line 8, column 79
    static function parent_3 () : pcf.api.Destination {
      return pcf.SearchGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.DelinquencyProcessSearch {
      return super.CurrentLocation as pcf.DelinquencyProcessSearch
    }
    
    
  }
  
  
}