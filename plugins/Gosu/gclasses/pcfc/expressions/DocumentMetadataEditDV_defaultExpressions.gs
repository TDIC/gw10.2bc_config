package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/DocumentMetadataEditDV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DocumentMetadataEditDV_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/document/DocumentMetadataEditDV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DocumentMetadataEditDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'columns' attribute on Layout at DocumentMetadataEditDV.default.pcf: line 22, column 21
    function columns_1 () : java.lang.Double {
      return documentMetadataBCHelper.AllowFieldsSubset ? 2 : 1
    }
    
    // 'def' attribute on InputSetRef at DocumentMetadataEditDV.default.pcf: line 25, column 91
    function def_onEnter_2 (def :  pcf.DocumentMetadataEditInputSet) : void {
      def.onEnter(documentDetailsApplicationHelper, fromTemplate)
    }
    
    // 'def' attribute on InputSetRef at DocumentMetadataEditDV.default.pcf: line 25, column 91
    function def_refreshVariables_3 (def :  pcf.DocumentMetadataEditInputSet) : void {
      def.refreshVariables(documentDetailsApplicationHelper, fromTemplate)
    }
    
    // 'initialValue' attribute on Variable at DocumentMetadataEditDV.default.pcf: line 18, column 52
    function initialValue_0 () : gw.document.DocumentMetadataBCHelper {
      return documentDetailsApplicationHelper as gw.document.DocumentMetadataBCHelper
    }
    
    property get documentDetailsApplicationHelper () : gw.document.DocumentDetailsApplicationHelper {
      return getRequireValue("documentDetailsApplicationHelper", 0) as gw.document.DocumentDetailsApplicationHelper
    }
    
    property set documentDetailsApplicationHelper ($arg :  gw.document.DocumentDetailsApplicationHelper) {
      setRequireValue("documentDetailsApplicationHelper", 0, $arg)
    }
    
    property get documentMetadataBCHelper () : gw.document.DocumentMetadataBCHelper {
      return getVariableValue("documentMetadataBCHelper", 0) as gw.document.DocumentMetadataBCHelper
    }
    
    property set documentMetadataBCHelper ($arg :  gw.document.DocumentMetadataBCHelper) {
      setVariableValue("documentMetadataBCHelper", 0, $arg)
    }
    
    property get fromTemplate () : boolean {
      return getRequireValue("fromTemplate", 0) as java.lang.Boolean
    }
    
    property set fromTemplate ($arg :  boolean) {
      setRequireValue("fromTemplate", 0, $arg)
    }
    
    
  }
  
  
}