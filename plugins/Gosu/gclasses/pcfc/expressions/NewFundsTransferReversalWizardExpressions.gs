package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewFundsTransferReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewFundsTransferReversalWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewFundsTransferReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewFundsTransferReversalWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=NewFundsTransferReversalWizard) at NewFundsTransferReversalWizard.pcf: line 9, column 41
    function afterCancel_6 () : void {
      DesktopGroup.go()
    }
    
    // 'afterCancel' attribute on Wizard (id=NewFundsTransferReversalWizard) at NewFundsTransferReversalWizard.pcf: line 9, column 41
    function afterCancel_dest_7 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewFundsTransferReversalWizard) at NewFundsTransferReversalWizard.pcf: line 9, column 41
    function afterFinish_11 () : void {
      DesktopGroup.go()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewFundsTransferReversalWizard) at NewFundsTransferReversalWizard.pcf: line 9, column 41
    function afterFinish_dest_12 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'allowNext' attribute on WizardStep (id=transferSearchStep) at NewFundsTransferReversalWizard.pcf: line 19, column 104
    function allowNext_1 () : java.lang.Boolean {
      return transferFundContext.TransferFundTransaction != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=NewFundsTransferReversalWizard) at NewFundsTransferReversalWizard.pcf: line 9, column 41
    function beforeCommit_8 (pickedValue :  java.lang.Object) : void {
      transferFundContext.createReversal()
    }
    
    // 'initialValue' attribute on Variable at NewFundsTransferReversalWizard.pcf: line 13, column 65
    function initialValue_0 () : gw.transaction.TransferFundsReversalWizardContext {
      return new gw.transaction.TransferFundsReversalWizardContext()
    }
    
    // 'screen' attribute on WizardStep (id=transferSearchStep) at NewFundsTransferReversalWizard.pcf: line 19, column 104
    function screen_onEnter_2 (def :  pcf.NewFundsTransferReversalSearchScreen) : void {
      def.onEnter(transferFundContext)
    }
    
    // 'screen' attribute on WizardStep (id=transferConfirmationStep) at NewFundsTransferReversalWizard.pcf: line 24, column 105
    function screen_onEnter_4 (def :  pcf.NewFundsTransferReversalConfirmationScreen) : void {
      def.onEnter(transferFundContext)
    }
    
    // 'screen' attribute on WizardStep (id=transferSearchStep) at NewFundsTransferReversalWizard.pcf: line 19, column 104
    function screen_refreshVariables_3 (def :  pcf.NewFundsTransferReversalSearchScreen) : void {
      def.refreshVariables(transferFundContext)
    }
    
    // 'screen' attribute on WizardStep (id=transferConfirmationStep) at NewFundsTransferReversalWizard.pcf: line 24, column 105
    function screen_refreshVariables_5 (def :  pcf.NewFundsTransferReversalConfirmationScreen) : void {
      def.refreshVariables(transferFundContext)
    }
    
    // 'tabBar' attribute on Wizard (id=NewFundsTransferReversalWizard) at NewFundsTransferReversalWizard.pcf: line 9, column 41
    function tabBar_onEnter_9 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=NewFundsTransferReversalWizard) at NewFundsTransferReversalWizard.pcf: line 9, column 41
    function tabBar_refreshVariables_10 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.NewFundsTransferReversalWizard {
      return super.CurrentLocation as pcf.NewFundsTransferReversalWizard
    }
    
    property get transferFundContext () : gw.transaction.TransferFundsReversalWizardContext {
      return getVariableValue("transferFundContext", 0) as gw.transaction.TransferFundsReversalWizardContext
    }
    
    property set transferFundContext ($arg :  gw.transaction.TransferFundsReversalWizardContext) {
      setVariableValue("transferFundContext", 0, $arg)
    }
    
    
  }
  
  
}