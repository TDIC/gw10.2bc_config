package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/OutgoingProducerPaymentSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OutgoingProducerPaymentSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/search/OutgoingProducerPaymentSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TypeKeyCell (id=Status_Cell) at OutgoingProducerPaymentSearchScreen.pcf: line 67, column 60
    function action_23 () : void {
      OutgoingPaymentDetailPopup.push(outgoingProducerPmntSearchView.OutgoingPayment)
    }
    
    // 'action' attribute on TypeKeyCell (id=Status_Cell) at OutgoingProducerPaymentSearchScreen.pcf: line 67, column 60
    function action_dest_24 () : pcf.api.Destination {
      return pcf.OutgoingPaymentDetailPopup.createDestination(outgoingProducerPmntSearchView.OutgoingPayment)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at OutgoingProducerPaymentSearchScreen.pcf: line 56, column 116
    function currency_18 () : typekey.Currency {
      return outgoingProducerPmntSearchView.Currency
    }
    
    // 'value' attribute on DateCell (id=IssueDate_Cell) at OutgoingProducerPaymentSearchScreen.pcf: line 39, column 67
    function valueRoot_10 () : java.lang.Object {
      return outgoingProducerPmntSearchView
    }
    
    // 'value' attribute on TextCell (id=CheckNumber_Cell) at OutgoingProducerPaymentSearchScreen.pcf: line 45, column 158
    function value_12 () : java.lang.String {
      return outgoingProducerPmntSearchView.PaymentMethod == PaymentMethod.TC_ACH ? "ACH" : outgoingProducerPmntSearchView.CheckNumber
    }
    
    // 'value' attribute on TextCell (id=Payee_Cell) at OutgoingProducerPaymentSearchScreen.pcf: line 50, column 63
    function value_14 () : java.lang.String {
      return outgoingProducerPmntSearchView.PayTo
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at OutgoingProducerPaymentSearchScreen.pcf: line 56, column 116
    function value_17 () : gw.pl.currency.MonetaryAmount {
      return outgoingProducerPmntSearchView.Amount.ofCurrency(outgoingProducerPmntSearchView.Currency)
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentMethod_Cell) at OutgoingProducerPaymentSearchScreen.pcf: line 61, column 52
    function value_20 () : typekey.PaymentMethod {
      return outgoingProducerPmntSearchView.PaymentMethod
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at OutgoingProducerPaymentSearchScreen.pcf: line 67, column 60
    function value_25 () : typekey.OutgoingPaymentStatus {
      return outgoingProducerPmntSearchView.Status
    }
    
    // 'value' attribute on DateCell (id=IssueDate_Cell) at OutgoingProducerPaymentSearchScreen.pcf: line 39, column 67
    function value_9 () : java.util.Date {
      return outgoingProducerPmntSearchView.IssueDate
    }
    
    property get outgoingProducerPmntSearchView () : entity.OutgoingProducerPmntSearchView {
      return getIteratedValue(2) as entity.OutgoingProducerPmntSearchView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/OutgoingProducerPaymentSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OutgoingProducerPaymentSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/OutgoingProducerPaymentSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends OutgoingProducerPaymentSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at OutgoingProducerPaymentSearchScreen.pcf: line 16, column 63
    function def_onEnter_0 (def :  pcf.OutgoingProducerPaymentSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at OutgoingProducerPaymentSearchScreen.pcf: line 16, column 63
    function def_refreshVariables_1 (def :  pcf.OutgoingProducerPaymentSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'maxSearchResults' attribute on SearchPanel at OutgoingProducerPaymentSearchScreen.pcf: line 14, column 98
    function maxSearchResults_29 () : java.lang.Object {
      return gw.api.system.BCConfigParameters.MaxSearchResults.Value
    }
    
    // 'searchCriteria' attribute on SearchPanel at OutgoingProducerPaymentSearchScreen.pcf: line 14, column 98
    function searchCriteria_31 () : gw.search.OutgoingProducerPmntSearchCriteria {
      return new gw.search.OutgoingProducerPmntSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at OutgoingProducerPaymentSearchScreen.pcf: line 14, column 98
    function search_30 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria)
    }
    
    // 'value' attribute on DateCell (id=IssueDate_Cell) at OutgoingProducerPaymentSearchScreen.pcf: line 39, column 67
    function sortValue_2 (outgoingProducerPmntSearchView :  entity.OutgoingProducerPmntSearchView) : java.lang.Object {
      return outgoingProducerPmntSearchView.IssueDate
    }
    
    // 'sortBy' attribute on TextCell (id=CheckNumber_Cell) at OutgoingProducerPaymentSearchScreen.pcf: line 45, column 158
    function sortValue_3 (outgoingProducerPmntSearchView :  entity.OutgoingProducerPmntSearchView) : java.lang.Object {
      return outgoingProducerPmntSearchView.CheckNumber
    }
    
    // 'value' attribute on TextCell (id=Payee_Cell) at OutgoingProducerPaymentSearchScreen.pcf: line 50, column 63
    function sortValue_4 (outgoingProducerPmntSearchView :  entity.OutgoingProducerPmntSearchView) : java.lang.Object {
      return outgoingProducerPmntSearchView.PayTo
    }
    
    // 'sortBy' attribute on MonetaryAmountCell (id=Amount_Cell) at OutgoingProducerPaymentSearchScreen.pcf: line 56, column 116
    function sortValue_5 (outgoingProducerPmntSearchView :  entity.OutgoingProducerPmntSearchView) : java.lang.Object {
      return outgoingProducerPmntSearchView.Currency
    }
    
    // 'sortBy' attribute on MonetaryAmountCell (id=Amount_Cell) at OutgoingProducerPaymentSearchScreen.pcf: line 56, column 116
    function sortValue_6 (outgoingProducerPmntSearchView :  entity.OutgoingProducerPmntSearchView) : java.lang.Object {
      return  outgoingProducerPmntSearchView.Amount
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentMethod_Cell) at OutgoingProducerPaymentSearchScreen.pcf: line 61, column 52
    function sortValue_7 (outgoingProducerPmntSearchView :  entity.OutgoingProducerPmntSearchView) : java.lang.Object {
      return outgoingProducerPmntSearchView.PaymentMethod
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at OutgoingProducerPaymentSearchScreen.pcf: line 67, column 60
    function sortValue_8 (outgoingProducerPmntSearchView :  entity.OutgoingProducerPmntSearchView) : java.lang.Object {
      return outgoingProducerPmntSearchView.Status
    }
    
    // 'value' attribute on RowIterator at OutgoingProducerPaymentSearchScreen.pcf: line 29, column 103
    function value_28 () : gw.api.database.IQueryBeanResult<entity.OutgoingProducerPmntSearchView> {
      return outgoingProducerPmntSearchViews
    }
    
    property get outgoingProducerPmntSearchViews () : gw.api.database.IQueryBeanResult<OutgoingProducerPmntSearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<OutgoingProducerPmntSearchView>
    }
    
    property get searchCriteria () : gw.search.OutgoingProducerPmntSearchCriteria {
      return getCriteriaValue(1) as gw.search.OutgoingProducerPmntSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.OutgoingProducerPmntSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}