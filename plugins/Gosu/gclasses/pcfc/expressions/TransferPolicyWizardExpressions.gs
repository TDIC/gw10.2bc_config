package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransferPolicyWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransferPolicyWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=TransferPolicyWizard) at TransferPolicyWizard.pcf: line 12, column 31
    function afterCancel_7 () : void {
      PolicyDetailCommissions.go(refreshPolicyPeriod(policyPeriod))
    }
    
    // 'afterCancel' attribute on Wizard (id=TransferPolicyWizard) at TransferPolicyWizard.pcf: line 12, column 31
    function afterCancel_dest_8 () : pcf.api.Destination {
      return pcf.PolicyDetailCommissions.createDestination(refreshPolicyPeriod(policyPeriod))
    }
    
    // 'afterFinish' attribute on Wizard (id=TransferPolicyWizard) at TransferPolicyWizard.pcf: line 12, column 31
    function afterFinish_13 () : void {
      PolicyDetailCommissions.go(policyPeriod)
    }
    
    // 'afterFinish' attribute on Wizard (id=TransferPolicyWizard) at TransferPolicyWizard.pcf: line 12, column 31
    function afterFinish_dest_14 () : pcf.api.Destination {
      return pcf.PolicyDetailCommissions.createDestination(policyPeriod)
    }
    
    // 'beforeCommit' attribute on Wizard (id=TransferPolicyWizard) at TransferPolicyWizard.pcf: line 12, column 31
    function beforeCommit_9 (pickedValue :  java.lang.Object) : void {
      policyTransfer.executeTransfer()
    }
    
    // 'canVisit' attribute on Wizard (id=TransferPolicyWizard) at TransferPolicyWizard.pcf: line 12, column 31
    static function canVisit_10 (policyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.PolicyPeriod.plcyprodtx
    }
    
    // 'initialValue' attribute on Variable at TransferPolicyWizard.pcf: line 22, column 40
    function initialValue_0 () : entity.PolTransferByRole {
      return newPolicyTransfer()
    }
    
    // 'screen' attribute on WizardStep (id=SelectRole) at TransferPolicyWizard.pcf: line 27, column 84
    function screen_onEnter_1 (def :  pcf.TransferPolicyRoleScreen) : void {
      def.onEnter(policyTransfer)
    }
    
    // 'screen' attribute on WizardStep (id=SelectNew) at TransferPolicyWizard.pcf: line 32, column 79
    function screen_onEnter_3 (def :  pcf.TransferPolicyNewScreen) : void {
      def.onEnter(policyTransfer)
    }
    
    // 'screen' attribute on WizardStep (id=Confirmation) at TransferPolicyWizard.pcf: line 37, column 88
    function screen_onEnter_5 (def :  pcf.TransferPolicyConfirmationScreen) : void {
      def.onEnter(policyTransfer)
    }
    
    // 'screen' attribute on WizardStep (id=SelectRole) at TransferPolicyWizard.pcf: line 27, column 84
    function screen_refreshVariables_2 (def :  pcf.TransferPolicyRoleScreen) : void {
      def.refreshVariables(policyTransfer)
    }
    
    // 'screen' attribute on WizardStep (id=SelectNew) at TransferPolicyWizard.pcf: line 32, column 79
    function screen_refreshVariables_4 (def :  pcf.TransferPolicyNewScreen) : void {
      def.refreshVariables(policyTransfer)
    }
    
    // 'screen' attribute on WizardStep (id=Confirmation) at TransferPolicyWizard.pcf: line 37, column 88
    function screen_refreshVariables_6 (def :  pcf.TransferPolicyConfirmationScreen) : void {
      def.refreshVariables(policyTransfer)
    }
    
    // 'tabBar' attribute on Wizard (id=TransferPolicyWizard) at TransferPolicyWizard.pcf: line 12, column 31
    function tabBar_onEnter_11 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=TransferPolicyWizard) at TransferPolicyWizard.pcf: line 12, column 31
    function tabBar_refreshVariables_12 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.TransferPolicyWizard {
      return super.CurrentLocation as pcf.TransferPolicyWizard
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get policyTransfer () : entity.PolTransferByRole {
      return getVariableValue("policyTransfer", 0) as entity.PolTransferByRole
    }
    
    property set policyTransfer ($arg :  entity.PolTransferByRole) {
      setVariableValue("policyTransfer", 0, $arg)
    }
    
    function newPolicyTransfer() : PolTransferByRole {
          var tpolicy = new PolTransferByRole(policyPeriod.Currency)
          tpolicy.PolicyPeriod = policyPeriod;
          tpolicy.CommissionTransferOption = policyPeriod.AgencyBill ? 
            CommissionTransferOption.TC_POINTINTIME : CommissionTransferOption.TC_ALLFUTURE;
          tpolicy.RoleToTransfer = PolicyRole.TC_PRIMARY;
          return tpolicy;
        }
        
    /**
     * Force retrieval of the passed-in policy period from the database.  Need this for example so that when we cancel out of 
     * the wizard, we don't get an exception on another page that we go to after the cancel (CC-53983)
     */
    function refreshPolicyPeriod(thePolicyPeriod : PolicyPeriod) : PolicyPeriod {
      var refreshedPolicyPeriod = gw.api.database.Query.make(entity.PolicyPeriod).compare("PublicID", Equals, thePolicyPeriod.PublicID).select().FirstResult
      return refreshedPolicyPeriod
    }
    
    
  }
  
  
}