<% uses pcf.* %>
<% uses entity.* %>
<% uses typekey.* %>
<% uses gw.api.locale.DisplayKey %>
<script type="text/javascript">
  setTokenAndSubmit = function(form) {
  var token = $('#NewPaymentInstrumentPopup-Token').text();
  console.log('Token = ' + token);  
  form.elements["Token"].value = token;
  form.action=getActionUrl();      
  form.submit();
  form.style.display = 'none';         
}

function getActionUrl() {
    return $('#NewPaymentInstrumentPopup-AuthorizeUrl').text();
}
</script>

<form method="post" action="getActionUrl();" id="formAuthorizeNetPopup" name="formAuthorizeNetPopup" target="iframeAuthorizeNet">
    <input type="hidden" name="Token" />
    <input type="hidden" name="PaymentProfileId" value="new" />
    <input type="button" value="Add Credit Card Details" onclick="setTokenAndSubmit(this.form);" />
</form>

<iframe name="iframeAuthorizeNet" id="iframeAuthorizeNet" src="" frameborder="0" scrolling="yes" width="500px" height="550px"></iframe>
 