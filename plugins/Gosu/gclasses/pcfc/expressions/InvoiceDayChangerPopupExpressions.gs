package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/InvoiceDayChangerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceDayChangerPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/InvoiceDayChangerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceDayChangerPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (invoiceStream :  entity.InvoiceStream) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=InvoiceDayChangerPopup) at InvoiceDayChangerPopup.pcf: line 10, column 93
    function beforeCommit_13 (pickedValue :  java.lang.Object) : void {
      invoiceDayChangeHelper.updateInvoiceDay()
    }
    
    // 'def' attribute on InputSetRef at InvoiceDayChangerPopup.pcf: line 28, column 47
    function def_onEnter_10 (def :  pcf.InvoiceStreamAnchorDateInputSet_twicepermonth) : void {
      def.onEnter(invoiceDayChangeHelper, true)
    }
    
    // 'def' attribute on InputSetRef at InvoiceDayChangerPopup.pcf: line 28, column 47
    function def_onEnter_2 (def :  pcf.InvoiceStreamAnchorDateInputSet_default) : void {
      def.onEnter(invoiceDayChangeHelper, true)
    }
    
    // 'def' attribute on InputSetRef at InvoiceDayChangerPopup.pcf: line 28, column 47
    function def_onEnter_4 (def :  pcf.InvoiceStreamAnchorDateInputSet_everyotherweek) : void {
      def.onEnter(invoiceDayChangeHelper, true)
    }
    
    // 'def' attribute on InputSetRef at InvoiceDayChangerPopup.pcf: line 28, column 47
    function def_onEnter_6 (def :  pcf.InvoiceStreamAnchorDateInputSet_everyweek) : void {
      def.onEnter(invoiceDayChangeHelper, true)
    }
    
    // 'def' attribute on InputSetRef at InvoiceDayChangerPopup.pcf: line 28, column 47
    function def_onEnter_8 (def :  pcf.InvoiceStreamAnchorDateInputSet_monthly) : void {
      def.onEnter(invoiceDayChangeHelper, true)
    }
    
    // 'def' attribute on InputSetRef at InvoiceDayChangerPopup.pcf: line 28, column 47
    function def_refreshVariables_11 (def :  pcf.InvoiceStreamAnchorDateInputSet_twicepermonth) : void {
      def.refreshVariables(invoiceDayChangeHelper, true)
    }
    
    // 'def' attribute on InputSetRef at InvoiceDayChangerPopup.pcf: line 28, column 47
    function def_refreshVariables_3 (def :  pcf.InvoiceStreamAnchorDateInputSet_default) : void {
      def.refreshVariables(invoiceDayChangeHelper, true)
    }
    
    // 'def' attribute on InputSetRef at InvoiceDayChangerPopup.pcf: line 28, column 47
    function def_refreshVariables_5 (def :  pcf.InvoiceStreamAnchorDateInputSet_everyotherweek) : void {
      def.refreshVariables(invoiceDayChangeHelper, true)
    }
    
    // 'def' attribute on InputSetRef at InvoiceDayChangerPopup.pcf: line 28, column 47
    function def_refreshVariables_7 (def :  pcf.InvoiceStreamAnchorDateInputSet_everyweek) : void {
      def.refreshVariables(invoiceDayChangeHelper, true)
    }
    
    // 'def' attribute on InputSetRef at InvoiceDayChangerPopup.pcf: line 28, column 47
    function def_refreshVariables_9 (def :  pcf.InvoiceStreamAnchorDateInputSet_monthly) : void {
      def.refreshVariables(invoiceDayChangeHelper, true)
    }
    
    // 'initialValue' attribute on Variable at InvoiceDayChangerPopup.pcf: line 19, column 57
    function initialValue_0 () : gw.api.web.invoice.InvoiceDayChangeHelper {
      return new gw.api.web.invoice.InvoiceDayChangeHelper(invoiceStream)
    }
    
    // EditButtons at InvoiceDayChangerPopup.pcf: line 22, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'mode' attribute on InputSetRef at InvoiceDayChangerPopup.pcf: line 28, column 47
    function mode_12 () : java.lang.Object {
      return invoiceStream.Periodicity
    }
    
    override property get CurrentLocation () : pcf.InvoiceDayChangerPopup {
      return super.CurrentLocation as pcf.InvoiceDayChangerPopup
    }
    
    property get invoiceDayChangeHelper () : gw.api.web.invoice.InvoiceDayChangeHelper {
      return getVariableValue("invoiceDayChangeHelper", 0) as gw.api.web.invoice.InvoiceDayChangeHelper
    }
    
    property set invoiceDayChangeHelper ($arg :  gw.api.web.invoice.InvoiceDayChangeHelper) {
      setVariableValue("invoiceDayChangeHelper", 0, $arg)
    }
    
    property get invoiceStream () : InvoiceStream {
      return getVariableValue("invoiceStream", 0) as InvoiceStream
    }
    
    property set invoiceStream ($arg :  InvoiceStream) {
      setVariableValue("invoiceStream", 0, $arg)
    }
    
    
  }
  
  
}