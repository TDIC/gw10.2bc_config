package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/AccountNewNegativeWriteoffReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountNewNegativeWriteoffReversalWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/AccountNewNegativeWriteoffReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountNewNegativeWriteoffReversalWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'allowFinish' attribute on WizardStep (id=Step2) at AccountNewNegativeWriteoffReversalWizard.pcf: line 32, column 101
    function allowFinish_5 () : java.lang.Boolean {
      return negativeWriteoffToReverse.getNegativeWriteoff() != null
    }
    
    // 'allowNext' attribute on WizardStep (id=Step1) at AccountNewNegativeWriteoffReversalWizard.pcf: line 26, column 103
    function allowNext_1 () : java.lang.Boolean {
      return negativeWriteoffToReverse != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=AccountNewNegativeWriteoffReversalWizard) at AccountNewNegativeWriteoffReversalWizard.pcf: line 10, column 51
    function beforeCommit_8 (pickedValue :  java.lang.Object) : void {
      negativeWriteoffToReverse.reverse()
    }
    
    // 'initialValue' attribute on Variable at AccountNewNegativeWriteoffReversalWizard.pcf: line 16, column 35
    function initialValue_0 () : NegativeWriteoffRev {
      return new NegativeWriteoffRev()
    }
    
    // 'onExit' attribute on WizardStep (id=Step1) at AccountNewNegativeWriteoffReversalWizard.pcf: line 26, column 103
    function onExit_2 () : void {
      negativeWriteoffToReverse.initiateApprovalActivityIfUserLacksAuthority()
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountNewNegativeWriteoffReversalWizard.pcf: line 26, column 103
    function screen_onEnter_3 (def :  pcf.NewNegativeWriteoffReversalSearchScreen) : void {
      def.onEnter(negativeWriteoffToReverse, account)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountNewNegativeWriteoffReversalWizard.pcf: line 32, column 101
    function screen_onEnter_6 (def :  pcf.NewNegativeWriteoffReversalConfirmationScreen) : void {
      def.onEnter(negativeWriteoffToReverse)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountNewNegativeWriteoffReversalWizard.pcf: line 26, column 103
    function screen_refreshVariables_4 (def :  pcf.NewNegativeWriteoffReversalSearchScreen) : void {
      def.refreshVariables(negativeWriteoffToReverse, account)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountNewNegativeWriteoffReversalWizard.pcf: line 32, column 101
    function screen_refreshVariables_7 (def :  pcf.NewNegativeWriteoffReversalConfirmationScreen) : void {
      def.refreshVariables(negativeWriteoffToReverse)
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewNegativeWriteoffReversalWizard) at AccountNewNegativeWriteoffReversalWizard.pcf: line 10, column 51
    function tabBar_onEnter_9 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewNegativeWriteoffReversalWizard) at AccountNewNegativeWriteoffReversalWizard.pcf: line 10, column 51
    function tabBar_refreshVariables_10 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AccountNewNegativeWriteoffReversalWizard {
      return super.CurrentLocation as pcf.AccountNewNegativeWriteoffReversalWizard
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get negativeWriteoffToReverse () : NegativeWriteoffRev {
      return getVariableValue("negativeWriteoffToReverse", 0) as NegativeWriteoffRev
    }
    
    property set negativeWriteoffToReverse ($arg :  NegativeWriteoffRev) {
      setVariableValue("negativeWriteoffToReverse", 0, $arg)
    }
    
    
  }
  
  
}