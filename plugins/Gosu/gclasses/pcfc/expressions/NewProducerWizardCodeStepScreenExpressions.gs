package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewProducerWizardCodeStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewProducerWizardCodeStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/NewProducerWizardCodeStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewProducerWizardCodeStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewProducerWizardCodeStepScreen.pcf: line 17, column 46
    function def_onEnter_0 (def :  pcf.ProducerCodesLV) : void {
      def.onEnter(producer, true)
    }
    
    // 'def' attribute on PanelRef at NewProducerWizardCodeStepScreen.pcf: line 17, column 46
    function def_refreshVariables_1 (def :  pcf.ProducerCodesLV) : void {
      def.refreshVariables(producer, true)
    }
    
    property get producer () : Producer {
      return getRequireValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setRequireValue("producer", 0, $arg)
    }
    
    
  }
  
  
}