package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/email/CreateEmailScreenRecipientInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreateEmailScreenRecipientInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/email/CreateEmailScreenRecipientInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreateEmailScreenRecipientInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at CreateEmailScreenRecipientInputSet.pcf: line 66, column 39
    function sortValue_0 (recipient :  gw.api.email.EmailContact) : java.lang.Object {
      return recipient.Name
    }
    
    // 'value' attribute on TextCell (id=email_Cell) at CreateEmailScreenRecipientInputSet.pcf: line 73, column 47
    function sortValue_1 (recipient :  gw.api.email.EmailContact) : java.lang.Object {
      return recipient.EmailAddress
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at CreateEmailScreenRecipientInputSet.pcf: line 41, column 51
    function toCreateAndAdd_14 () : gw.api.email.EmailContact {
      var newEmailContact = new gw.api.email.EmailContact(); contacts.add(newEmailContact); return newEmailContact;
    }
    
    // 'toRemove' attribute on RowIterator at CreateEmailScreenRecipientInputSet.pcf: line 41, column 51
    function toRemove_15 (recipient :  gw.api.email.EmailContact) : void {
      contacts.remove( recipient )
    }
    
    // 'validationLabel' attribute on RowIterator at CreateEmailScreenRecipientInputSet.pcf: line 41, column 51
    function validationLabel_16 () : java.lang.String {
      return title
    }
    
    // 'value' attribute on RowIterator at CreateEmailScreenRecipientInputSet.pcf: line 41, column 51
    function value_17 () : gw.api.email.EmailContact[] {
      return contacts?.toTypedArray()
    }
    
    property get contacts () : List<gw.api.email.EmailContact> {
      return getRequireValue("contacts", 0) as List<gw.api.email.EmailContact>
    }
    
    property set contacts ($arg :  List<gw.api.email.EmailContact>) {
      setRequireValue("contacts", 0, $arg)
    }
    
    property get title () : String {
      return getRequireValue("title", 0) as String
    }
    
    property set title ($arg :  String) {
      setRequireValue("title", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/email/CreateEmailScreenRecipientInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CreateEmailScreenRecipientInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=email_Cell) at CreateEmailScreenRecipientInputSet.pcf: line 73, column 47
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      recipient.EmailAddress = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at CreateEmailScreenRecipientInputSet.pcf: line 66, column 39
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      recipient.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at CreateEmailScreenRecipientInputSet.pcf: line 45, column 43
    function initialValue_2 () : gw.entity.IEntityType {
      return null
    }
    
    // 'initialValue' attribute on Variable at CreateEmailScreenRecipientInputSet.pcf: line 49, column 28
    function initialValue_3 () : String {
      return null
    }
    
    // RowIterator at CreateEmailScreenRecipientInputSet.pcf: line 41, column 51
    function initializeVariables_13 () : void {
        entityType = null;
  roleStr = null;

    }
    
    // 'requestValidationExpression' attribute on TextCell (id=email_Cell) at CreateEmailScreenRecipientInputSet.pcf: line 73, column 47
    function requestValidationExpression_8 (VALUE :  java.lang.String) : java.lang.Object {
      return VALUE == null ? DisplayKey.get("Web.Email.Error.AddressForRecipientRequired") : null
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at CreateEmailScreenRecipientInputSet.pcf: line 66, column 39
    function valueRoot_6 () : java.lang.Object {
      return recipient
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at CreateEmailScreenRecipientInputSet.pcf: line 66, column 39
    function value_4 () : java.lang.String {
      return recipient.Name
    }
    
    // 'value' attribute on TextCell (id=email_Cell) at CreateEmailScreenRecipientInputSet.pcf: line 73, column 47
    function value_9 () : java.lang.String {
      return recipient.EmailAddress
    }
    
    property get entityType () : gw.entity.IEntityType {
      return getVariableValue("entityType", 1) as gw.entity.IEntityType
    }
    
    property set entityType ($arg :  gw.entity.IEntityType) {
      setVariableValue("entityType", 1, $arg)
    }
    
    property get recipient () : gw.api.email.EmailContact {
      return getIteratedValue(1) as gw.api.email.EmailContact
    }
    
    property get roleStr () : String {
      return getVariableValue("roleStr", 1) as String
    }
    
    property set roleStr ($arg :  String) {
      setVariableValue("roleStr", 1, $arg)
    }
    
    
  }
  
  
}