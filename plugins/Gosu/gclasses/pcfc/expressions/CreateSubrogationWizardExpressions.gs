package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/CreateSubrogationWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreateSubrogationWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/account/CreateSubrogationWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreateSubrogationWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=CreateSubrogationWizard) at CreateSubrogationWizard.pcf: line 13, column 34
    function afterCancel_9 () : void {
      DesktopGroup.go()
    }
    
    // 'afterCancel' attribute on Wizard (id=CreateSubrogationWizard) at CreateSubrogationWizard.pcf: line 13, column 34
    function afterCancel_dest_10 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'afterFinish' attribute on Wizard (id=CreateSubrogationWizard) at CreateSubrogationWizard.pcf: line 13, column 34
    function afterFinish_15 () : void {
      DesktopGroup.go()
    }
    
    // 'afterFinish' attribute on Wizard (id=CreateSubrogationWizard) at CreateSubrogationWizard.pcf: line 13, column 34
    function afterFinish_dest_16 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'allowNext' attribute on WizardStep (id=Step1) at CreateSubrogationWizard.pcf: line 29, column 88
    function allowNext_1 () : java.lang.Boolean {
      return tAccountOwnerReference.TAccountOwner != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=CreateSubrogationWizard) at CreateSubrogationWizard.pcf: line 13, column 34
    function beforeCommit_11 (pickedValue :  java.lang.Object) : void {
      subrogation.doSubrogation()
    }
    
    // 'canVisit' attribute on Wizard (id=CreateSubrogationWizard) at CreateSubrogationWizard.pcf: line 13, column 34
    static function canVisit_12 () : java.lang.Boolean {
      return perm.Transaction.subrtxn
    }
    
    // 'initialValue' attribute on Variable at CreateSubrogationWizard.pcf: line 22, column 60
    function initialValue_0 () : gw.api.web.accounting.TAccountOwnerReference {
      return new gw.api.web.accounting.TAccountOwnerReference()
    }
    
    // 'onExit' attribute on WizardStep (id=Step1) at CreateSubrogationWizard.pcf: line 29, column 88
    function onExit_2 () : void {
      setUpSubrogation()
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at CreateSubrogationWizard.pcf: line 29, column 88
    function screen_onEnter_3 (def :  pcf.CreateSubrogationTargetsScreen) : void {
      def.onEnter(subrogation, tAccountOwnerReference)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at CreateSubrogationWizard.pcf: line 34, column 86
    function screen_onEnter_5 (def :  pcf.CreateSubrogationInfoScreen) : void {
      def.onEnter(subrogation)
    }
    
    // 'screen' attribute on WizardStep (id=Step3) at CreateSubrogationWizard.pcf: line 39, column 89
    function screen_onEnter_7 (def :  pcf.CreateSubrogationConfirmScreen) : void {
      def.onEnter(subrogation)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at CreateSubrogationWizard.pcf: line 29, column 88
    function screen_refreshVariables_4 (def :  pcf.CreateSubrogationTargetsScreen) : void {
      def.refreshVariables(subrogation, tAccountOwnerReference)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at CreateSubrogationWizard.pcf: line 34, column 86
    function screen_refreshVariables_6 (def :  pcf.CreateSubrogationInfoScreen) : void {
      def.refreshVariables(subrogation)
    }
    
    // 'screen' attribute on WizardStep (id=Step3) at CreateSubrogationWizard.pcf: line 39, column 89
    function screen_refreshVariables_8 (def :  pcf.CreateSubrogationConfirmScreen) : void {
      def.refreshVariables(subrogation)
    }
    
    // 'tabBar' attribute on Wizard (id=CreateSubrogationWizard) at CreateSubrogationWizard.pcf: line 13, column 34
    function tabBar_onEnter_13 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=CreateSubrogationWizard) at CreateSubrogationWizard.pcf: line 13, column 34
    function tabBar_refreshVariables_14 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.CreateSubrogationWizard {
      return super.CurrentLocation as pcf.CreateSubrogationWizard
    }
    
    property get subrogation () : Subrogation {
      return getVariableValue("subrogation", 0) as Subrogation
    }
    
    property set subrogation ($arg :  Subrogation) {
      setVariableValue("subrogation", 0, $arg)
    }
    
    property get tAccountOwnerReference () : gw.api.web.accounting.TAccountOwnerReference {
      return getVariableValue("tAccountOwnerReference", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set tAccountOwnerReference ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setVariableValue("tAccountOwnerReference", 0, $arg)
    }
    
    function setUpSubrogation() {
      if (subrogation == null) {
        subrogation = createSubrogationWithAccountCurrency()
      }
      else if (subrogation.Currency != tAccountOwnerReference.TAccountOwner.Currency)
      {
        subrogation.remove()
        subrogation = createSubrogationWithAccountCurrency()
      }
      
      subrogation.SourceAccount = (tAccountOwnerReference.TAccountOwner as Account)
    }
    
    function createSubrogationWithAccountCurrency() : Subrogation {
      return new Subrogation(CurrentLocation, tAccountOwnerReference.TAccountOwner.Currency)
    }
    
    
  }
  
  
}