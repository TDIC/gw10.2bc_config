package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.web.account.AccountTransactionsCollector
@javax.annotation.Generated("config/web/pcf/account/AccountDetailTransactions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailTransactionsExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailTransactions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailTransactionsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'afterEnter' attribute on Page (id=AccountDetailTransactions) at AccountDetailTransactions.pcf: line 11, column 77
    function afterEnter_11 () : void {
      populateTransactions()
    }
    
    // 'afterReturnFromPopup' attribute on Page (id=AccountDetailTransactions) at AccountDetailTransactions.pcf: line 11, column 77
    function afterReturnFromPopup_12 (popupCommitted :  boolean) : void {
      populateTransactions()
    }
    
    // 'canVisit' attribute on Page (id=AccountDetailTransactions) at AccountDetailTransactions.pcf: line 11, column 77
    static function canVisit_13 (account :  Account) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.accttxnview
    }
    
    // 'def' attribute on PanelRef at AccountDetailTransactions.pcf: line 34, column 59
    function def_onEnter_9 (def :  pcf.AccountDetailTransactionsLV) : void {
      def.onEnter( transactions )
    }
    
    // 'def' attribute on PanelRef at AccountDetailTransactions.pcf: line 34, column 59
    function def_refreshVariables_10 (def :  pcf.AccountDetailTransactionsLV) : void {
      def.refreshVariables( transactions )
    }
    
    // 'value' attribute on ToolbarRangeInput (id=TransactionsFilter_Input) at AccountDetailTransactions.pcf: line 45, column 60
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectedFilterOption = (__VALUE_TO_SET as gw.api.filters.StandardQueryFilter)
    }
    
    // 'initialValue' attribute on Variable at AccountDetailTransactions.pcf: line 20, column 50
    function initialValue_0 () : gw.api.filters.StandardQueryFilter {
      return gw.api.web.transaction.TransactionFilterSet.FilterOptions[0]
    }
    
    // 'onChange' attribute on PostOnChange at AccountDetailTransactions.pcf: line 47, column 50
    function onChange_2 () : void {
      populateTransactions()
    }
    
    // Page (id=AccountDetailTransactions) at AccountDetailTransactions.pcf: line 11, column 77
    static function parent_14 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=TransactionsFilter_Input) at AccountDetailTransactions.pcf: line 45, column 60
    function valueRange_5 () : java.lang.Object {
      return gw.api.web.transaction.TransactionFilterSet.FilterOptions
    }
    
    // 'value' attribute on ToolbarRangeInput (id=TransactionsFilter_Input) at AccountDetailTransactions.pcf: line 45, column 60
    function value_3 () : gw.api.filters.StandardQueryFilter {
      return selectedFilterOption
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=TransactionsFilter_Input) at AccountDetailTransactions.pcf: line 45, column 60
    function verifyValueRangeIsAllowedType_6 ($$arg :  gw.api.filters.StandardQueryFilter[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=TransactionsFilter_Input) at AccountDetailTransactions.pcf: line 45, column 60
    function verifyValueRangeIsAllowedType_6 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=TransactionsFilter_Input) at AccountDetailTransactions.pcf: line 45, column 60
    function verifyValueRange_7 () : void {
      var __valueRangeArg = gw.api.web.transaction.TransactionFilterSet.FilterOptions
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_6(__valueRangeArg)
    }
    
    // 'visible' attribute on AlertBar (id=MissingArchivedTransactions) at AccountDetailTransactions.pcf: line 32, column 156
    function visible_1 () : java.lang.Boolean {
      return account.PolicyPeriods.hasMatch(\ policyPeriod -> policyPeriod.Archived) or account.Invoices.hasMatch(\ invoice -> invoice.Frozen)
    }
    
    override property get CurrentLocation () : pcf.AccountDetailTransactions {
      return super.CurrentLocation as pcf.AccountDetailTransactions
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get selectedFilterOption () : gw.api.filters.StandardQueryFilter {
      return getVariableValue("selectedFilterOption", 0) as gw.api.filters.StandardQueryFilter
    }
    
    property set selectedFilterOption ($arg :  gw.api.filters.StandardQueryFilter) {
      setVariableValue("selectedFilterOption", 0, $arg)
    }
    
    property get transactions () : Transaction[] {
      return getVariableValue("transactions", 0) as Transaction[]
    }
    
    property set transactions ($arg :  Transaction[]) {
      setVariableValue("transactions", 0, $arg)
    }
    
    
          function populateTransactions() {
            transactions = AccountTransactionsCollector.getAllTransactions(account, selectedFilterOption)
          }
    
    
  }
  
  
}