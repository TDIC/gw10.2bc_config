package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffWizardConfirmationStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewWriteoffWizardConfirmationStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffWizardConfirmationStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewWriteoffWizardConfirmationStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffWizardConfirmationStepScreen.pcf: line 37, column 30
    function def_onEnter_6 (def :  pcf.ApprovalActivityDV) : void {
      def.onEnter(approvalActivity)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffWizardConfirmationStepScreen.pcf: line 37, column 30
    function def_refreshVariables_7 (def :  pcf.ApprovalActivityDV) : void {
      def.refreshVariables(approvalActivity)
    }
    
    // 'initialValue' attribute on Variable at NewWriteoffWizardConfirmationStepScreen.pcf: line 13, column 44
    function initialValue_0 () : entity.ActivityCreatedByAppr {
      return uiWriteoff.WriteOff.getOpenApprovalActivity()
    }
    
    // 'label' attribute on Label at NewWriteoffWizardConfirmationStepScreen.pcf: line 32, column 146
    function label_3 () : java.lang.String {
      return DisplayKey.get("Web.NewWriteoffWizard.Confirmation", uiWriteoff.Amount.render(), uiWriteoff.WriteOff.TAccountOwner)
    }
    
    // 'visible' attribute on AlertBar (id=NullAmountAlertBar) at NewWriteoffWizardConfirmationStepScreen.pcf: line 20, column 31
    function visible_1 () : java.lang.Boolean {
      return !hasAmount()
    }
    
    // 'visible' attribute on AlertBar (id=ApprovalActivityAlertBar) at NewWriteoffWizardConfirmationStepScreen.pcf: line 24, column 43
    function visible_2 () : java.lang.Boolean {
      return approvalActivity != null
    }
    
    // 'visible' attribute on DetailViewPanel (id=ConfirmDV) at NewWriteoffWizardConfirmationStepScreen.pcf: line 27, column 29
    function visible_4 () : java.lang.Boolean {
      return hasAmount()
    }
    
    property get approvalActivity () : entity.ActivityCreatedByAppr {
      return getVariableValue("approvalActivity", 0) as entity.ActivityCreatedByAppr
    }
    
    property set approvalActivity ($arg :  entity.ActivityCreatedByAppr) {
      setVariableValue("approvalActivity", 0, $arg)
    }
    
    property get uiWriteoff () : gw.api.web.accounting.UIWriteOffCreation {
      return getRequireValue("uiWriteoff", 0) as gw.api.web.accounting.UIWriteOffCreation
    }
    
    property set uiWriteoff ($arg :  gw.api.web.accounting.UIWriteOffCreation) {
      setRequireValue("uiWriteoff", 0, $arg)
    }
    
    function hasAmount() : boolean {
      return uiWriteoff != null && uiWriteoff.Amount != null
    }
    
    
  }
  
  
}