package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/payment/NewPaymentPlan_SecondStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewPaymentPlan_SecondStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/payment/NewPaymentPlan_SecondStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewPaymentPlan_SecondStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at NewPaymentPlan_SecondStepScreen.pcf: line 39, column 57
    function def_onEnter_11 (def :  pcf.InstallmentTreatmentInputSet_FirstTermOnly) : void {
      def.onEnter(paymentPlanHelper)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at NewPaymentPlan_SecondStepScreen.pcf: line 39, column 57
    function def_onEnter_13 (def :  pcf.InstallmentTreatmentInputSet_None) : void {
      def.onEnter(paymentPlanHelper)
    }
    
    // 'def' attribute on PanelRef at NewPaymentPlan_SecondStepScreen.pcf: line 45, column 62
    function def_onEnter_16 (def :  pcf.NewPaymentPlan_SummaryDV) : void {
      def.onEnter(paymentPlanHelper)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at NewPaymentPlan_SecondStepScreen.pcf: line 39, column 57
    function def_onEnter_9 (def :  pcf.InstallmentTreatmentInputSet_EveryTerm) : void {
      def.onEnter(paymentPlanHelper)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at NewPaymentPlan_SecondStepScreen.pcf: line 39, column 57
    function def_refreshVariables_10 (def :  pcf.InstallmentTreatmentInputSet_EveryTerm) : void {
      def.refreshVariables(paymentPlanHelper)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at NewPaymentPlan_SecondStepScreen.pcf: line 39, column 57
    function def_refreshVariables_12 (def :  pcf.InstallmentTreatmentInputSet_FirstTermOnly) : void {
      def.refreshVariables(paymentPlanHelper)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at NewPaymentPlan_SecondStepScreen.pcf: line 39, column 57
    function def_refreshVariables_14 (def :  pcf.InstallmentTreatmentInputSet_None) : void {
      def.refreshVariables(paymentPlanHelper)
    }
    
    // 'def' attribute on PanelRef at NewPaymentPlan_SecondStepScreen.pcf: line 45, column 62
    function def_refreshVariables_17 (def :  pcf.NewPaymentPlan_SummaryDV) : void {
      def.refreshVariables(paymentPlanHelper)
    }
    
    // 'value' attribute on RangeRadioInput (id=DownPaymentFrequency_Input) at NewPaymentPlan_SecondStepScreen.pcf: line 32, column 69
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlanHelper.FrequencyOfDownPayment = (__VALUE_TO_SET as gw.admin.paymentplan.DownPaymentFrequency)
    }
    
    // 'initialValue' attribute on Variable at NewPaymentPlan_SecondStepScreen.pcf: line 13, column 27
    function initialValue_0 () : PaymentPlan {
      return paymentPlanHelper.PaymentPlan //not used on screen, but needs to be present for bean to be committed
    }
    
    // 'mode' attribute on InputSetRef (id=InstallmentTreatment) at NewPaymentPlan_SecondStepScreen.pcf: line 39, column 57
    function mode_15 () : java.lang.Object {
      return paymentPlanHelper.DownPaymentMode
    }
    
    // 'onChange' attribute on PostOnChange at NewPaymentPlan_SecondStepScreen.pcf: line 34, column 78
    function onChange_1 () : void {
      paymentPlanHelper.onDownPaymentFrequencyChange()
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DownPaymentFrequency_Input) at NewPaymentPlan_SecondStepScreen.pcf: line 32, column 69
    function valueRange_5 () : java.lang.Object {
      return gw.admin.paymentplan.DownPaymentFrequency.getValuesForWizardScreen()
    }
    
    // 'value' attribute on RangeRadioInput (id=DownPaymentFrequency_Input) at NewPaymentPlan_SecondStepScreen.pcf: line 32, column 69
    function valueRoot_4 () : java.lang.Object {
      return paymentPlanHelper
    }
    
    // 'value' attribute on RangeRadioInput (id=DownPaymentFrequency_Input) at NewPaymentPlan_SecondStepScreen.pcf: line 32, column 69
    function value_2 () : gw.admin.paymentplan.DownPaymentFrequency {
      return paymentPlanHelper.FrequencyOfDownPayment
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DownPaymentFrequency_Input) at NewPaymentPlan_SecondStepScreen.pcf: line 32, column 69
    function verifyValueRangeIsAllowedType_6 ($$arg :  gw.admin.paymentplan.DownPaymentFrequency[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DownPaymentFrequency_Input) at NewPaymentPlan_SecondStepScreen.pcf: line 32, column 69
    function verifyValueRangeIsAllowedType_6 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DownPaymentFrequency_Input) at NewPaymentPlan_SecondStepScreen.pcf: line 32, column 69
    function verifyValueRange_7 () : void {
      var __valueRangeArg = gw.admin.paymentplan.DownPaymentFrequency.getValuesForWizardScreen()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_6(__valueRangeArg)
    }
    
    property get paymentPlan () : PaymentPlan {
      return getVariableValue("paymentPlan", 0) as PaymentPlan
    }
    
    property set paymentPlan ($arg :  PaymentPlan) {
      setVariableValue("paymentPlan", 0, $arg)
    }
    
    property get paymentPlanHelper () : gw.admin.paymentplan.PaymentPlanViewHelper {
      return getRequireValue("paymentPlanHelper", 0) as gw.admin.paymentplan.PaymentPlanViewHelper
    }
    
    property set paymentPlanHelper ($arg :  gw.admin.paymentplan.PaymentPlanViewHelper) {
      setRequireValue("paymentPlanHelper", 0, $arg)
    }
    
    
  }
  
  
}