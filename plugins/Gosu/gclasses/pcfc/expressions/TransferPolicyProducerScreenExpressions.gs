package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyProducerScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransferPolicyProducerScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyProducerScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransferPolicyProducerScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RangeInput (id=producerCode_Input) at TransferPolicyProducerScreen.pcf: line 34, column 44
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyTransfer.SourceProducerCode = (__VALUE_TO_SET as entity.ProducerCode)
    }
    
    // 'optionLabel' attribute on RangeInput (id=producerCode_Input) at TransferPolicyProducerScreen.pcf: line 34, column 44
    function optionLabel_5 (VALUE :  entity.ProducerCode) : java.lang.String {
      return VALUE.Code
    }
    
    // 'valueRange' attribute on RangeInput (id=producerCode_Input) at TransferPolicyProducerScreen.pcf: line 34, column 44
    function valueRange_6 () : java.lang.Object {
      return producer.ProducerCodes
    }
    
    // 'value' attribute on RangeInput (id=producerCode_Input) at TransferPolicyProducerScreen.pcf: line 34, column 44
    function valueRoot_4 () : java.lang.Object {
      return policyTransfer
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at TransferPolicyProducerScreen.pcf: line 25, column 40
    function value_0 () : entity.Producer {
      return producer
    }
    
    // 'value' attribute on RangeInput (id=producerCode_Input) at TransferPolicyProducerScreen.pcf: line 34, column 44
    function value_2 () : entity.ProducerCode {
      return policyTransfer.SourceProducerCode
    }
    
    // 'valueRange' attribute on RangeInput (id=producerCode_Input) at TransferPolicyProducerScreen.pcf: line 34, column 44
    function verifyValueRangeIsAllowedType_7 ($$arg :  entity.ProducerCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=producerCode_Input) at TransferPolicyProducerScreen.pcf: line 34, column 44
    function verifyValueRangeIsAllowedType_7 ($$arg :  gw.api.database.IQueryBeanResult<entity.ProducerCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=producerCode_Input) at TransferPolicyProducerScreen.pcf: line 34, column 44
    function verifyValueRangeIsAllowedType_7 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=producerCode_Input) at TransferPolicyProducerScreen.pcf: line 34, column 44
    function verifyValueRange_8 () : void {
      var __valueRangeArg = producer.ProducerCodes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_7(__valueRangeArg)
    }
    
    property get policyTransfer () : PolTransferByProdCode {
      return getRequireValue("policyTransfer", 0) as PolTransferByProdCode
    }
    
    property set policyTransfer ($arg :  PolTransferByProdCode) {
      setRequireValue("policyTransfer", 0, $arg)
    }
    
    property get producer () : Producer {
      return getRequireValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setRequireValue("producer", 0, $arg)
    }
    
    
  }
  
  
}