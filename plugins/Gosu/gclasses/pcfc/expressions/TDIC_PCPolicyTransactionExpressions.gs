package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/exitpoints/TDIC_PCPolicyTransaction.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_PCPolicyTransactionExpressions {
  @javax.annotation.Generated("config/web/pcf/exitpoints/TDIC_PCPolicyTransaction.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_PCPolicyTransactionExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyCenterSystemURL :  String, offerNumber :  String, jobIndex :  int) : int {
      return 0
    }
    
    override property get CurrentLocation () : pcf.TDIC_PCPolicyTransaction {
      return super.CurrentLocation as pcf.TDIC_PCPolicyTransaction
    }
    
    property get jobIndex () : int {
      return getVariableValue("jobIndex", 0) as java.lang.Integer
    }
    
    property set jobIndex ($arg :  int) {
      setVariableValue("jobIndex", 0, $arg)
    }
    
    property get offerNumber () : String {
      return getVariableValue("offerNumber", 0) as String
    }
    
    property set offerNumber ($arg :  String) {
      setVariableValue("offerNumber", 0, $arg)
    }
    
    property get policyCenterSystemURL () : String {
      return getVariableValue("policyCenterSystemURL", 0) as String
    }
    
    property set policyCenterSystemURL ($arg :  String) {
      setVariableValue("policyCenterSystemURL", 0, $arg)
    }
    
    
  }
  
  
}