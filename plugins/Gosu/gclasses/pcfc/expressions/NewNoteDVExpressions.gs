package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/note/NewNoteDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewNoteDVExpressions {
  @javax.annotation.Generated("config/web/pcf/note/NewNoteDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewNoteDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyInput (id=Topic_Input) at NewNoteDV.pcf: line 33, column 43
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      Note.Topic = (__VALUE_TO_SET as typekey.NoteTopicType)
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at NewNoteDV.pcf: line 41, column 31
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      Note.Subject = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=RelatedTo_Input) at NewNoteDV.pcf: line 48, column 39
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      Note.RelatedTo = (__VALUE_TO_SET as typekey.RelatedTo)
    }
    
    // 'value' attribute on TypeKeyInput (id=Confidential_Input) at NewNoteDV.pcf: line 57, column 46
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      Note.SecurityType = (__VALUE_TO_SET as typekey.NoteSecurityType)
    }
    
    // 'value' attribute on TextAreaInput (id=Text_Input) at NewNoteDV.pcf: line 66, column 28
    function defaultSetter_29 (__VALUE_TO_SET :  java.lang.Object) : void {
      Note.Body = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at NewNoteDV.pcf: line 26, column 43
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      Note.Language = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'editable' attribute on TypeKeyInput (id=Language_Input) at NewNoteDV.pcf: line 26, column 43
    function editable_6 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count > 1
    }
    
    // 'value' attribute on DateInput (id=AuthoringDate_Input) at NewNoteDV.pcf: line 14, column 37
    function valueRoot_1 () : java.lang.Object {
      return Note
    }
    
    // 'value' attribute on DateInput (id=AuthoringDate_Input) at NewNoteDV.pcf: line 14, column 37
    function value_0 () : java.util.Date {
      return Note.AuthoringDate
    }
    
    // 'value' attribute on TypeKeyInput (id=Topic_Input) at NewNoteDV.pcf: line 33, column 43
    function value_12 () : typekey.NoteTopicType {
      return Note.Topic
    }
    
    // 'value' attribute on TextInput (id=Subject_Input) at NewNoteDV.pcf: line 41, column 31
    function value_16 () : java.lang.String {
      return Note.Subject
    }
    
    // 'value' attribute on TypeKeyInput (id=RelatedTo_Input) at NewNoteDV.pcf: line 48, column 39
    function value_20 () : typekey.RelatedTo {
      return Note.RelatedTo
    }
    
    // 'value' attribute on TypeKeyInput (id=Confidential_Input) at NewNoteDV.pcf: line 57, column 46
    function value_24 () : typekey.NoteSecurityType {
      return Note.SecurityType
    }
    
    // 'value' attribute on TextAreaInput (id=Text_Input) at NewNoteDV.pcf: line 66, column 28
    function value_28 () : java.lang.String {
      return Note.Body
    }
    
    // 'value' attribute on TextInput (id=Author_Input) at NewNoteDV.pcf: line 19, column 34
    function value_3 () : entity.User {
      return Note.Author
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at NewNoteDV.pcf: line 26, column 43
    function value_7 () : typekey.LanguageType {
      return Note.Language
    }
    
    property get Note () : Note {
      return getRequireValue("Note", 0) as Note
    }
    
    property set Note ($arg :  Note) {
      setRequireValue("Note", 0, $arg)
    }
    
    
  }
  
  
}