package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadProducerLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DataUploadProducerLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadProducerLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DataUploadProducerLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadProducerLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=city_Cell) at DataUploadProducerLV.pcf: line 55, column 41
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.City")
    }
    
    // 'label' attribute on TextCell (id=state_Cell) at DataUploadProducerLV.pcf: line 60, column 41
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.State")
    }
    
    // 'label' attribute on TextCell (id=postalCode_Cell) at DataUploadProducerLV.pcf: line 65, column 41
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.PostalCode")
    }
    
    // 'label' attribute on TypeKeyCell (id=country_Cell) at DataUploadProducerLV.pcf: line 70, column 40
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Country")
    }
    
    // 'label' attribute on BooleanRadioCell (id=active_Cell) at DataUploadProducerLV.pcf: line 75, column 42
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.Active")
    }
    
    // 'label' attribute on TextCell (id=firstName_Cell) at DataUploadProducerLV.pcf: line 80, column 41
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.FirstName")
    }
    
    // 'label' attribute on TextCell (id=lastName_Cell) at DataUploadProducerLV.pcf: line 85, column 41
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.LastName")
    }
    
    // 'label' attribute on TextCell (id=workPhone_Cell) at DataUploadProducerLV.pcf: line 90, column 41
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.WorkPhone")
    }
    
    // 'label' attribute on TextCell (id=fax_Cell) at DataUploadProducerLV.pcf: line 95, column 41
    function label_29 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.Fax")
    }
    
    // 'label' attribute on DateCell (id=createDate_Cell) at DataUploadProducerLV.pcf: line 29, column 39
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.CreateDate")
    }
    
    // 'label' attribute on TextCell (id=email_Cell) at DataUploadProducerLV.pcf: line 100, column 41
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.Email")
    }
    
    // 'label' attribute on TextCell (id=contactRole_Cell) at DataUploadProducerLV.pcf: line 105, column 41
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.ContactRole")
    }
    
    // 'label' attribute on TextCell (id=accountRep_Cell) at DataUploadProducerLV.pcf: line 110, column 41
    function label_35 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.AccountRep")
    }
    
    // 'label' attribute on TextCell (id=agencyBillPlan_Cell) at DataUploadProducerLV.pcf: line 115, column 41
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.AgencyBillPlan")
    }
    
    // 'label' attribute on TextCell (id=commissionPlan_Cell) at DataUploadProducerLV.pcf: line 120, column 41
    function label_39 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.CommissionPlan")
    }
    
    // 'label' attribute on TextCell (id=tier_Cell) at DataUploadProducerLV.pcf: line 125, column 41
    function label_41 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.Tier")
    }
    
    // 'label' attribute on TextCell (id=periodicity_Cell) at DataUploadProducerLV.pcf: line 130, column 41
    function label_43 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.Periodicity")
    }
    
    // 'label' attribute on TextCell (id=commisionDOM_Cell) at DataUploadProducerLV.pcf: line 135, column 42
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.CommissionDayOfMonth")
    }
    
    // 'label' attribute on TextCell (id=policyNumber_Cell) at DataUploadProducerLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.ProducerName")
    }
    
    // 'label' attribute on TextCell (id=code_Cell) at DataUploadProducerLV.pcf: line 40, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.Code")
    }
    
    // 'label' attribute on TextCell (id=addressLine1_Cell) at DataUploadProducerLV.pcf: line 45, column 41
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Line1")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadProducerLV.pcf: line 23, column 46
    function sortValue_1 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return processor.getLoadStatus(producer)
    }
    
    // 'value' attribute on TextCell (id=addressLine1_Cell) at DataUploadProducerLV.pcf: line 45, column 41
    function sortValue_10 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.Contact.PrimaryAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=addressLine2_Cell) at DataUploadProducerLV.pcf: line 50, column 41
    function sortValue_12 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.Contact.PrimaryAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at DataUploadProducerLV.pcf: line 55, column 41
    function sortValue_14 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.Contact.PrimaryAddress.City
    }
    
    // 'value' attribute on TextCell (id=state_Cell) at DataUploadProducerLV.pcf: line 60, column 41
    function sortValue_16 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.Contact.PrimaryAddress.State
    }
    
    // 'value' attribute on TextCell (id=postalCode_Cell) at DataUploadProducerLV.pcf: line 65, column 41
    function sortValue_18 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.Contact.PrimaryAddress.PostalCode
    }
    
    // 'value' attribute on TypeKeyCell (id=country_Cell) at DataUploadProducerLV.pcf: line 70, column 40
    function sortValue_20 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.Contact.PrimaryAddress.Country
    }
    
    // 'value' attribute on BooleanRadioCell (id=active_Cell) at DataUploadProducerLV.pcf: line 75, column 42
    function sortValue_22 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.Active
    }
    
    // 'value' attribute on TextCell (id=firstName_Cell) at DataUploadProducerLV.pcf: line 80, column 41
    function sortValue_24 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.Contact.FirstName
    }
    
    // 'value' attribute on TextCell (id=lastName_Cell) at DataUploadProducerLV.pcf: line 85, column 41
    function sortValue_26 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.Contact.LastName
    }
    
    // 'value' attribute on TextCell (id=workPhone_Cell) at DataUploadProducerLV.pcf: line 90, column 41
    function sortValue_28 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.Contact.WorkPhone
    }
    
    // 'value' attribute on TextCell (id=fax_Cell) at DataUploadProducerLV.pcf: line 95, column 41
    function sortValue_30 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.Contact.FaxPhone
    }
    
    // 'value' attribute on TextCell (id=email_Cell) at DataUploadProducerLV.pcf: line 100, column 41
    function sortValue_32 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.Contact.Email
    }
    
    // 'value' attribute on TextCell (id=contactRole_Cell) at DataUploadProducerLV.pcf: line 105, column 41
    function sortValue_34 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.Role.Code
    }
    
    // 'value' attribute on TextCell (id=accountRep_Cell) at DataUploadProducerLV.pcf: line 110, column 41
    function sortValue_36 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.AccountRep
    }
    
    // 'value' attribute on TextCell (id=agencyBillPlan_Cell) at DataUploadProducerLV.pcf: line 115, column 41
    function sortValue_38 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.AgencyBillPlan
    }
    
    // 'value' attribute on DateCell (id=createDate_Cell) at DataUploadProducerLV.pcf: line 29, column 39
    function sortValue_4 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.EntryDate
    }
    
    // 'value' attribute on TextCell (id=commissionPlan_Cell) at DataUploadProducerLV.pcf: line 120, column 41
    function sortValue_40 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.CommissionPlan
    }
    
    // 'value' attribute on TextCell (id=tier_Cell) at DataUploadProducerLV.pcf: line 125, column 41
    function sortValue_42 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.Tier
    }
    
    // 'value' attribute on TextCell (id=periodicity_Cell) at DataUploadProducerLV.pcf: line 130, column 41
    function sortValue_44 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.Periodicity
    }
    
    // 'value' attribute on TextCell (id=commisionDOM_Cell) at DataUploadProducerLV.pcf: line 135, column 42
    function sortValue_46 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.CommissionDOM
    }
    
    // 'value' attribute on TextCell (id=policyNumber_Cell) at DataUploadProducerLV.pcf: line 35, column 41
    function sortValue_6 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.ProducerName
    }
    
    // 'value' attribute on TextCell (id=code_Cell) at DataUploadProducerLV.pcf: line 40, column 41
    function sortValue_8 (producer :  tdic.util.dataloader.data.sampledata.ProducerData) : java.lang.Object {
      return producer.ProducerCode
    }
    
    // 'value' attribute on RowIterator (id=producerID) at DataUploadProducerLV.pcf: line 15, column 96
    function value_163 () : java.util.ArrayList<tdic.util.dataloader.data.sampledata.ProducerData> {
      return processor.ProducerArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadProducerLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCSampleDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCSampleDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadProducerLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DataUploadProducerLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at DataUploadProducerLV.pcf: line 17, column 96
    function highlighted_162 () : java.lang.Boolean {
      return (producer.Error or producer.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=firstName_Cell) at DataUploadProducerLV.pcf: line 80, column 41
    function label_102 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.FirstName")
    }
    
    // 'label' attribute on TextCell (id=lastName_Cell) at DataUploadProducerLV.pcf: line 85, column 41
    function label_107 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.LastName")
    }
    
    // 'label' attribute on TextCell (id=workPhone_Cell) at DataUploadProducerLV.pcf: line 90, column 41
    function label_112 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.WorkPhone")
    }
    
    // 'label' attribute on TextCell (id=fax_Cell) at DataUploadProducerLV.pcf: line 95, column 41
    function label_117 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.Fax")
    }
    
    // 'label' attribute on TextCell (id=email_Cell) at DataUploadProducerLV.pcf: line 100, column 41
    function label_122 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.Email")
    }
    
    // 'label' attribute on TextCell (id=contactRole_Cell) at DataUploadProducerLV.pcf: line 105, column 41
    function label_127 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.ContactRole")
    }
    
    // 'label' attribute on TextCell (id=accountRep_Cell) at DataUploadProducerLV.pcf: line 110, column 41
    function label_132 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.AccountRep")
    }
    
    // 'label' attribute on TextCell (id=agencyBillPlan_Cell) at DataUploadProducerLV.pcf: line 115, column 41
    function label_137 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.AgencyBillPlan")
    }
    
    // 'label' attribute on TextCell (id=commissionPlan_Cell) at DataUploadProducerLV.pcf: line 120, column 41
    function label_142 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.CommissionPlan")
    }
    
    // 'label' attribute on TextCell (id=tier_Cell) at DataUploadProducerLV.pcf: line 125, column 41
    function label_147 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.Tier")
    }
    
    // 'label' attribute on TextCell (id=periodicity_Cell) at DataUploadProducerLV.pcf: line 130, column 41
    function label_152 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.Periodicity")
    }
    
    // 'label' attribute on TextCell (id=commisionDOM_Cell) at DataUploadProducerLV.pcf: line 135, column 42
    function label_157 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.CommissionDayOfMonth")
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadProducerLV.pcf: line 23, column 46
    function label_47 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on DateCell (id=createDate_Cell) at DataUploadProducerLV.pcf: line 29, column 39
    function label_52 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.CreateDate")
    }
    
    // 'label' attribute on TextCell (id=policyNumber_Cell) at DataUploadProducerLV.pcf: line 35, column 41
    function label_57 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.ProducerName")
    }
    
    // 'label' attribute on TextCell (id=code_Cell) at DataUploadProducerLV.pcf: line 40, column 41
    function label_62 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.Code")
    }
    
    // 'label' attribute on TextCell (id=addressLine1_Cell) at DataUploadProducerLV.pcf: line 45, column 41
    function label_67 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Line1")
    }
    
    // 'label' attribute on TextCell (id=city_Cell) at DataUploadProducerLV.pcf: line 55, column 41
    function label_77 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.City")
    }
    
    // 'label' attribute on TextCell (id=state_Cell) at DataUploadProducerLV.pcf: line 60, column 41
    function label_82 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.State")
    }
    
    // 'label' attribute on TextCell (id=postalCode_Cell) at DataUploadProducerLV.pcf: line 65, column 41
    function label_87 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.PostalCode")
    }
    
    // 'label' attribute on TypeKeyCell (id=country_Cell) at DataUploadProducerLV.pcf: line 70, column 40
    function label_92 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Address.Country")
    }
    
    // 'label' attribute on BooleanRadioCell (id=active_Cell) at DataUploadProducerLV.pcf: line 75, column 42
    function label_97 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producer.Active")
    }
    
    // 'value' attribute on TextCell (id=firstName_Cell) at DataUploadProducerLV.pcf: line 80, column 41
    function valueRoot_104 () : java.lang.Object {
      return producer.Contact
    }
    
    // 'value' attribute on TextCell (id=contactRole_Cell) at DataUploadProducerLV.pcf: line 105, column 41
    function valueRoot_129 () : java.lang.Object {
      return producer.Role
    }
    
    // 'value' attribute on DateCell (id=createDate_Cell) at DataUploadProducerLV.pcf: line 29, column 39
    function valueRoot_54 () : java.lang.Object {
      return producer
    }
    
    // 'value' attribute on TextCell (id=addressLine1_Cell) at DataUploadProducerLV.pcf: line 45, column 41
    function valueRoot_69 () : java.lang.Object {
      return producer.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=firstName_Cell) at DataUploadProducerLV.pcf: line 80, column 41
    function value_103 () : java.lang.String {
      return producer.Contact.FirstName
    }
    
    // 'value' attribute on TextCell (id=lastName_Cell) at DataUploadProducerLV.pcf: line 85, column 41
    function value_108 () : java.lang.String {
      return producer.Contact.LastName
    }
    
    // 'value' attribute on TextCell (id=workPhone_Cell) at DataUploadProducerLV.pcf: line 90, column 41
    function value_113 () : java.lang.String {
      return producer.Contact.WorkPhone
    }
    
    // 'value' attribute on TextCell (id=fax_Cell) at DataUploadProducerLV.pcf: line 95, column 41
    function value_118 () : java.lang.String {
      return producer.Contact.FaxPhone
    }
    
    // 'value' attribute on TextCell (id=email_Cell) at DataUploadProducerLV.pcf: line 100, column 41
    function value_123 () : java.lang.String {
      return producer.Contact.Email
    }
    
    // 'value' attribute on TextCell (id=contactRole_Cell) at DataUploadProducerLV.pcf: line 105, column 41
    function value_128 () : java.lang.String {
      return producer.Role.Code
    }
    
    // 'value' attribute on TextCell (id=accountRep_Cell) at DataUploadProducerLV.pcf: line 110, column 41
    function value_133 () : java.lang.String {
      return producer.AccountRep
    }
    
    // 'value' attribute on TextCell (id=agencyBillPlan_Cell) at DataUploadProducerLV.pcf: line 115, column 41
    function value_138 () : java.lang.String {
      return producer.AgencyBillPlan
    }
    
    // 'value' attribute on TextCell (id=commissionPlan_Cell) at DataUploadProducerLV.pcf: line 120, column 41
    function value_143 () : java.lang.String {
      return producer.CommissionPlan
    }
    
    // 'value' attribute on TextCell (id=tier_Cell) at DataUploadProducerLV.pcf: line 125, column 41
    function value_148 () : java.lang.String {
      return producer.Tier
    }
    
    // 'value' attribute on TextCell (id=periodicity_Cell) at DataUploadProducerLV.pcf: line 130, column 41
    function value_153 () : java.lang.String {
      return producer.Periodicity
    }
    
    // 'value' attribute on TextCell (id=commisionDOM_Cell) at DataUploadProducerLV.pcf: line 135, column 42
    function value_158 () : java.lang.Integer {
      return producer.CommissionDOM
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadProducerLV.pcf: line 23, column 46
    function value_48 () : java.lang.String {
      return processor.getLoadStatus(producer)
    }
    
    // 'value' attribute on DateCell (id=createDate_Cell) at DataUploadProducerLV.pcf: line 29, column 39
    function value_53 () : java.util.Date {
      return producer.EntryDate
    }
    
    // 'value' attribute on TextCell (id=policyNumber_Cell) at DataUploadProducerLV.pcf: line 35, column 41
    function value_58 () : java.lang.String {
      return producer.ProducerName
    }
    
    // 'value' attribute on TextCell (id=code_Cell) at DataUploadProducerLV.pcf: line 40, column 41
    function value_63 () : java.lang.String {
      return producer.ProducerCode
    }
    
    // 'value' attribute on TextCell (id=addressLine1_Cell) at DataUploadProducerLV.pcf: line 45, column 41
    function value_68 () : java.lang.String {
      return producer.Contact.PrimaryAddress.AddressLine1
    }
    
    // 'value' attribute on TextCell (id=addressLine2_Cell) at DataUploadProducerLV.pcf: line 50, column 41
    function value_73 () : java.lang.String {
      return producer.Contact.PrimaryAddress.AddressLine2
    }
    
    // 'value' attribute on TextCell (id=city_Cell) at DataUploadProducerLV.pcf: line 55, column 41
    function value_78 () : java.lang.String {
      return producer.Contact.PrimaryAddress.City
    }
    
    // 'value' attribute on TextCell (id=state_Cell) at DataUploadProducerLV.pcf: line 60, column 41
    function value_83 () : java.lang.String {
      return producer.Contact.PrimaryAddress.State
    }
    
    // 'value' attribute on TextCell (id=postalCode_Cell) at DataUploadProducerLV.pcf: line 65, column 41
    function value_88 () : java.lang.String {
      return producer.Contact.PrimaryAddress.PostalCode
    }
    
    // 'value' attribute on TypeKeyCell (id=country_Cell) at DataUploadProducerLV.pcf: line 70, column 40
    function value_93 () : typekey.Country {
      return producer.Contact.PrimaryAddress.Country
    }
    
    // 'value' attribute on BooleanRadioCell (id=active_Cell) at DataUploadProducerLV.pcf: line 75, column 42
    function value_98 () : java.lang.Boolean {
      return producer.Active
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadProducerLV.pcf: line 23, column 46
    function visible_49 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get producer () : tdic.util.dataloader.data.sampledata.ProducerData {
      return getIteratedValue(1) as tdic.util.dataloader.data.sampledata.ProducerData
    }
    
    
  }
  
  
}