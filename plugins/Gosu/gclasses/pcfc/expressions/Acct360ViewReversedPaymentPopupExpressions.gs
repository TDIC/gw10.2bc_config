package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/Acct360ViewReversedPaymentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class Acct360ViewReversedPaymentPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/acct360/Acct360ViewReversedPaymentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Acct360ViewReversedPaymentPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (payment :  DirectBillMoneyRcvd) : int {
      return 0
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at Acct360ViewReversedPaymentPopup.pcf: line 26, column 40
    function action_0 () : void {
      AccountSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at Acct360ViewReversedPaymentPopup.pcf: line 26, column 40
    function action_dest_1 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at Acct360ViewReversedPaymentPopup.pcf: line 50, column 56
    function currency_15 () : typekey.Currency {
      return payment.Account.Currency
    }
    
    // 'def' attribute on InputSetRef at Acct360ViewReversedPaymentPopup.pcf: line 84, column 61
    function def_onEnter_40 (def :  pcf.PaymentReceiptInputSet_ach) : void {
      def.onEnter(payment)
    }
    
    // 'def' attribute on InputSetRef at Acct360ViewReversedPaymentPopup.pcf: line 84, column 61
    function def_onEnter_42 (def :  pcf.PaymentReceiptInputSet_check) : void {
      def.onEnter(payment)
    }
    
    // 'def' attribute on InputSetRef at Acct360ViewReversedPaymentPopup.pcf: line 84, column 61
    function def_onEnter_44 (def :  pcf.PaymentReceiptInputSet_creditcard) : void {
      def.onEnter(payment)
    }
    
    // 'def' attribute on InputSetRef at Acct360ViewReversedPaymentPopup.pcf: line 84, column 61
    function def_onEnter_46 (def :  pcf.PaymentReceiptInputSet_default) : void {
      def.onEnter(payment)
    }
    
    // 'def' attribute on InputSetRef at Acct360ViewReversedPaymentPopup.pcf: line 84, column 61
    function def_onEnter_48 (def :  pcf.PaymentReceiptInputSet_wire) : void {
      def.onEnter(payment)
    }
    
    // 'def' attribute on InputSetRef at Acct360ViewReversedPaymentPopup.pcf: line 84, column 61
    function def_refreshVariables_41 (def :  pcf.PaymentReceiptInputSet_ach) : void {
      def.refreshVariables(payment)
    }
    
    // 'def' attribute on InputSetRef at Acct360ViewReversedPaymentPopup.pcf: line 84, column 61
    function def_refreshVariables_43 (def :  pcf.PaymentReceiptInputSet_check) : void {
      def.refreshVariables(payment)
    }
    
    // 'def' attribute on InputSetRef at Acct360ViewReversedPaymentPopup.pcf: line 84, column 61
    function def_refreshVariables_45 (def :  pcf.PaymentReceiptInputSet_creditcard) : void {
      def.refreshVariables(payment)
    }
    
    // 'def' attribute on InputSetRef at Acct360ViewReversedPaymentPopup.pcf: line 84, column 61
    function def_refreshVariables_47 (def :  pcf.PaymentReceiptInputSet_default) : void {
      def.refreshVariables(payment)
    }
    
    // 'def' attribute on InputSetRef at Acct360ViewReversedPaymentPopup.pcf: line 84, column 61
    function def_refreshVariables_49 (def :  pcf.PaymentReceiptInputSet_wire) : void {
      def.refreshVariables(payment)
    }
    
    // 'value' attribute on TextInput (id=CreditDistDescription_Input) at Acct360ViewReversedPaymentPopup.pcf: line 62, column 55
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      payment.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'label' attribute on Label at Acct360ViewReversedPaymentPopup.pcf: line 37, column 210
    function label_8 () : java.lang.String {
      return (payment typeis ZeroDollarDMR) ? DisplayKey.get("Web.NewDirectBillPayment.CreditDistributionData.Label") : DisplayKey.get("Web.NewDirectBillPayment.PaymentData.Label")
    }
    
    // 'mode' attribute on InputSetRef at Acct360ViewReversedPaymentPopup.pcf: line 84, column 61
    function mode_50 () : java.lang.Object {
      return payment.PaymentInstrument.PaymentMethod
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at Acct360ViewReversedPaymentPopup.pcf: line 26, column 40
    function valueRoot_3 () : java.lang.Object {
      return payment
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at Acct360ViewReversedPaymentPopup.pcf: line 35, column 59
    function valueRoot_6 () : java.lang.Object {
      return payment.Account
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at Acct360ViewReversedPaymentPopup.pcf: line 50, column 56
    function value_13 () : gw.pl.currency.MonetaryAmount {
      return payment.Amount
    }
    
    // 'value' attribute on TextInput (id=UnappliedFunds_Input) at Acct360ViewReversedPaymentPopup.pcf: line 56, column 40
    function value_18 () : UnappliedFund {
      return payment.UnappliedFund
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at Acct360ViewReversedPaymentPopup.pcf: line 26, column 40
    function value_2 () : entity.Account {
      return payment.Account
    }
    
    // 'value' attribute on TextInput (id=CreditDistDescription_Input) at Acct360ViewReversedPaymentPopup.pcf: line 62, column 55
    function value_22 () : java.lang.String {
      return payment.Description
    }
    
    // 'value' attribute on TextInput (id=PaymentInstrument_Input) at Acct360ViewReversedPaymentPopup.pcf: line 74, column 56
    function value_29 () : entity.PaymentInstrument {
      return payment.PaymentInstrument
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at Acct360ViewReversedPaymentPopup.pcf: line 35, column 59
    function value_5 () : java.lang.String {
      return payment.Account.AccountNameLocalized
    }
    
    // 'value' attribute on TextInput (id=reversalReason_Input) at Acct360ViewReversedPaymentPopup.pcf: line 89, column 48
    function value_51 () : PaymentReversalReason {
      return payment.ReversalReason
    }
    
    // 'value' attribute on DateInput (id=reversaldate_Input) at Acct360ViewReversedPaymentPopup.pcf: line 93, column 43
    function value_54 () : java.util.Date {
      return payment.ReversalDate
    }
    
    // 'value' attribute on DateInput (id=PaymentDate_Input) at Acct360ViewReversedPaymentPopup.pcf: line 42, column 43
    function value_9 () : java.util.Date {
      return payment.ReceivedDate
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=Amount_Input) at Acct360ViewReversedPaymentPopup.pcf: line 50, column 56
    function visible_12 () : java.lang.Boolean {
      return !(payment typeis ZeroDollarDMR)
    }
    
    // 'visible' attribute on TextInput (id=CreditDistDescription_Input) at Acct360ViewReversedPaymentPopup.pcf: line 62, column 55
    function visible_21 () : java.lang.Boolean {
      return (payment typeis ZeroDollarDMR)
    }
    
    override property get CurrentLocation () : pcf.Acct360ViewReversedPaymentPopup {
      return super.CurrentLocation as pcf.Acct360ViewReversedPaymentPopup
    }
    
    property get payment () : DirectBillMoneyRcvd {
      return getVariableValue("payment", 0) as DirectBillMoneyRcvd
    }
    
    property set payment ($arg :  DirectBillMoneyRcvd) {
      setVariableValue("payment", 0, $arg)
    }
    
    
  }
  
  
}