package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailSummaryPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailSummaryPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailSummaryPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailSummaryPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'canVisit' attribute on Popup (id=PolicyDetailSummaryPopup) at PolicyDetailSummaryPopup.pcf: line 9, column 71
    static function canVisit_2 (policyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcysummview
    }
    
    // 'def' attribute on ScreenRef at PolicyDetailSummaryPopup.pcf: line 16, column 54
    function def_onEnter_0 (def :  pcf.PolicyDetailSummaryScreen) : void {
      def.onEnter(policyPeriod)
    }
    
    // 'def' attribute on ScreenRef at PolicyDetailSummaryPopup.pcf: line 16, column 54
    function def_refreshVariables_1 (def :  pcf.PolicyDetailSummaryScreen) : void {
      def.refreshVariables(policyPeriod)
    }
    
    override property get CurrentLocation () : pcf.PolicyDetailSummaryPopup {
      return super.CurrentLocation as pcf.PolicyDetailSummaryPopup
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}