package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/OutgoingPaymentDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OutgoingPaymentDetailPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/OutgoingPaymentDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OutgoingPaymentDetailPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (outgoingPayment :  OutgoingPayment) : int {
      return 0
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at OutgoingPaymentDetailPopup.pcf: line 44, column 45
    function currency_20 () : typekey.Currency {
      return outgoingPayment.Currency
    }
    
    // 'def' attribute on InputSetRef at OutgoingPaymentDetailPopup.pcf: line 75, column 69
    function def_onEnter_44 (def :  pcf.PaymentReceiptInputSet_ach) : void {
      def.onEnter(outgoingPayment)
    }
    
    // 'def' attribute on InputSetRef at OutgoingPaymentDetailPopup.pcf: line 75, column 69
    function def_onEnter_46 (def :  pcf.PaymentReceiptInputSet_check) : void {
      def.onEnter(outgoingPayment)
    }
    
    // 'def' attribute on InputSetRef at OutgoingPaymentDetailPopup.pcf: line 75, column 69
    function def_onEnter_48 (def :  pcf.PaymentReceiptInputSet_creditcard) : void {
      def.onEnter(outgoingPayment)
    }
    
    // 'def' attribute on InputSetRef at OutgoingPaymentDetailPopup.pcf: line 75, column 69
    function def_onEnter_50 (def :  pcf.PaymentReceiptInputSet_default) : void {
      def.onEnter(outgoingPayment)
    }
    
    // 'def' attribute on InputSetRef at OutgoingPaymentDetailPopup.pcf: line 75, column 69
    function def_onEnter_52 (def :  pcf.PaymentReceiptInputSet_wire) : void {
      def.onEnter(outgoingPayment)
    }
    
    // 'def' attribute on InputSetRef at OutgoingPaymentDetailPopup.pcf: line 75, column 69
    function def_refreshVariables_45 (def :  pcf.PaymentReceiptInputSet_ach) : void {
      def.refreshVariables(outgoingPayment)
    }
    
    // 'def' attribute on InputSetRef at OutgoingPaymentDetailPopup.pcf: line 75, column 69
    function def_refreshVariables_47 (def :  pcf.PaymentReceiptInputSet_check) : void {
      def.refreshVariables(outgoingPayment)
    }
    
    // 'def' attribute on InputSetRef at OutgoingPaymentDetailPopup.pcf: line 75, column 69
    function def_refreshVariables_49 (def :  pcf.PaymentReceiptInputSet_creditcard) : void {
      def.refreshVariables(outgoingPayment)
    }
    
    // 'def' attribute on InputSetRef at OutgoingPaymentDetailPopup.pcf: line 75, column 69
    function def_refreshVariables_51 (def :  pcf.PaymentReceiptInputSet_default) : void {
      def.refreshVariables(outgoingPayment)
    }
    
    // 'def' attribute on InputSetRef at OutgoingPaymentDetailPopup.pcf: line 75, column 69
    function def_refreshVariables_53 (def :  pcf.PaymentReceiptInputSet_wire) : void {
      def.refreshVariables(outgoingPayment)
    }
    
    // 'value' attribute on DateInput (id=IssueDate_Input) at OutgoingPaymentDetailPopup.pcf: line 21, column 48
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      outgoingPayment.IssueDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=RejectedDate_Input) at OutgoingPaymentDetailPopup.pcf: line 32, column 61
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      outgoingPayment.RejectedDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=RefNumber_Input) at OutgoingPaymentDetailPopup.pcf: line 37, column 48
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      outgoingPayment.RefNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=Status_Input) at OutgoingPaymentDetailPopup.pcf: line 51, column 56
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      outgoingPayment.Status = (__VALUE_TO_SET as typekey.OutgoingPaymentStatus)
    }
    
    // 'value' attribute on DateInput (id=PaidDate_Input) at OutgoingPaymentDetailPopup.pcf: line 26, column 47
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      outgoingPayment.PaidDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'mode' attribute on InputSetRef at OutgoingPaymentDetailPopup.pcf: line 75, column 69
    function mode_54 () : java.lang.Object {
      return outgoingPayment.PaymentInstrument.PaymentMethod
    }
    
    // 'valueRange' attribute on RangeInput (id=Status_Input) at OutgoingPaymentDetailPopup.pcf: line 51, column 56
    function valueRange_25 () : java.lang.Object {
      return typekey.OutgoingPaymentStatus.getTypeKeys(false)
    }
    
    // 'value' attribute on DateInput (id=IssueDate_Input) at OutgoingPaymentDetailPopup.pcf: line 21, column 48
    function valueRoot_2 () : java.lang.Object {
      return outgoingPayment
    }
    
    // 'value' attribute on DateInput (id=IssueDate_Input) at OutgoingPaymentDetailPopup.pcf: line 21, column 48
    function value_0 () : java.util.Date {
      return outgoingPayment.IssueDate
    }
    
    // 'value' attribute on TextInput (id=RefNumber_Input) at OutgoingPaymentDetailPopup.pcf: line 37, column 48
    function value_14 () : java.lang.String {
      return outgoingPayment.RefNumber
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at OutgoingPaymentDetailPopup.pcf: line 44, column 45
    function value_18 () : gw.pl.currency.MonetaryAmount {
      return outgoingPayment.Amount
    }
    
    // 'value' attribute on RangeInput (id=Status_Input) at OutgoingPaymentDetailPopup.pcf: line 51, column 56
    function value_22 () : typekey.OutgoingPaymentStatus {
      return outgoingPayment.Status
    }
    
    // 'value' attribute on TextInput (id=PayTo_Input) at OutgoingPaymentDetailPopup.pcf: line 55, column 44
    function value_29 () : java.lang.String {
      return outgoingPayment.PayTo
    }
    
    // 'value' attribute on TextInput (id=MailTo_Input) at OutgoingPaymentDetailPopup.pcf: line 59, column 45
    function value_32 () : java.lang.String {
      return outgoingPayment.MailTo
    }
    
    // 'value' attribute on TextInput (id=MailToAddress_Input) at OutgoingPaymentDetailPopup.pcf: line 63, column 52
    function value_35 () : java.lang.String {
      return outgoingPayment.MailToAddress
    }
    
    // 'value' attribute on TextInput (id=Memo_Input) at OutgoingPaymentDetailPopup.pcf: line 67, column 43
    function value_38 () : java.lang.String {
      return outgoingPayment.Memo
    }
    
    // 'value' attribute on DateInput (id=PaidDate_Input) at OutgoingPaymentDetailPopup.pcf: line 26, column 47
    function value_4 () : java.util.Date {
      return outgoingPayment.PaidDate
    }
    
    // 'value' attribute on TextInput (id=PaymentInstrument_Input) at OutgoingPaymentDetailPopup.pcf: line 72, column 51
    function value_41 () : entity.PaymentInstrument {
      return outgoingPayment.PaymentInstrument
    }
    
    // 'value' attribute on DateInput (id=RejectedDate_Input) at OutgoingPaymentDetailPopup.pcf: line 32, column 61
    function value_9 () : java.util.Date {
      return outgoingPayment.RejectedDate
    }
    
    // 'valueRange' attribute on RangeInput (id=Status_Input) at OutgoingPaymentDetailPopup.pcf: line 51, column 56
    function verifyValueRangeIsAllowedType_26 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Status_Input) at OutgoingPaymentDetailPopup.pcf: line 51, column 56
    function verifyValueRangeIsAllowedType_26 ($$arg :  typekey.OutgoingPaymentStatus[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Status_Input) at OutgoingPaymentDetailPopup.pcf: line 51, column 56
    function verifyValueRange_27 () : void {
      var __valueRangeArg = typekey.OutgoingPaymentStatus.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_26(__valueRangeArg)
    }
    
    // 'visible' attribute on DateInput (id=RejectedDate_Input) at OutgoingPaymentDetailPopup.pcf: line 32, column 61
    function visible_8 () : java.lang.Boolean {
      return outgoingPayment.RejectedDate != null
    }
    
    override property get CurrentLocation () : pcf.OutgoingPaymentDetailPopup {
      return super.CurrentLocation as pcf.OutgoingPaymentDetailPopup
    }
    
    property get outgoingPayment () : OutgoingPayment {
      return getVariableValue("outgoingPayment", 0) as OutgoingPayment
    }
    
    property set outgoingPayment ($arg :  OutgoingPayment) {
      setVariableValue("outgoingPayment", 0, $arg)
    }
    
    
  }
  
  
}