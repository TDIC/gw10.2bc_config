package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.pl.currency.MonetaryAmount
@javax.annotation.Generated("config/web/pcf/policy/PolicySummaryScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicySummaryScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicySummaryScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanelExpressionsImpl extends PolicySummaryScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=NotesAddIcon) at PolicySummaryScreen.pcf: line 222, column 45
    function action_90 () : void {
      PolicyNewNoteWorksheet.goInWorkspace(plcyPeriod)
    }
    
    // 'action' attribute on Link (id=NotesEditIcon) at PolicySummaryScreen.pcf: line 233, column 41
    function action_93 () : void {
      PolicyNewNoteWorksheet.goInWorkspace(plcyPeriod, note)
    }
    
    // 'action' attribute on TextInput (id=NoteLink_Input) at PolicySummaryScreen.pcf: line 240, column 39
    function action_96 () : void {
      PolicyDetailNotes.push(plcyPeriod, false, note)
    }
    
    // 'action' attribute on Link (id=NotesAddIcon) at PolicySummaryScreen.pcf: line 222, column 45
    function action_dest_91 () : pcf.api.Destination {
      return pcf.PolicyNewNoteWorksheet.createDestination(plcyPeriod)
    }
    
    // 'action' attribute on Link (id=NotesEditIcon) at PolicySummaryScreen.pcf: line 233, column 41
    function action_dest_94 () : pcf.api.Destination {
      return pcf.PolicyNewNoteWorksheet.createDestination(plcyPeriod, note)
    }
    
    // 'action' attribute on TextInput (id=NoteLink_Input) at PolicySummaryScreen.pcf: line 240, column 39
    function action_dest_97 () : pcf.api.Destination {
      return pcf.PolicyDetailNotes.createDestination(plcyPeriod, false, note)
    }
    
    // 'icon' attribute on Link (id=NotesSpacer) at PolicySummaryScreen.pcf: line 252, column 35
    function icon_101 () : java.lang.String {
      return 'blank_16.png'
    }
    
    // 'initialValue' attribute on Variable at PolicySummaryScreen.pcf: line 197, column 66
    function initialValue_88 () : gw.api.database.IQueryBeanResult<Note> {
      return new NoteSearchCriteria() {:PolicyPeriod = plcyPeriod, :SortBy = TC_DATE}.performSearch(true)
    }
    
    // 'initialValue' attribute on Variable at PolicySummaryScreen.pcf: line 202, column 26
    function initialValue_89 () : Note {
      return noteQuery.FirstResult
    }
    
    // 'value' attribute on DateInput (id=NoteDate_Input) at PolicySummaryScreen.pcf: line 262, column 39
    function valueRoot_105 () : java.lang.Object {
      return note
    }
    
    // 'value' attribute on DateInput (id=NoteDate_Input) at PolicySummaryScreen.pcf: line 262, column 39
    function value_104 () : java.util.Date {
      return note.AuthoringDate
    }
    
    // 'value' attribute on TextInput (id=NoteTopic_Input) at PolicySummaryScreen.pcf: line 268, column 39
    function value_109 () : typekey.NoteTopicType {
      return note.Topic
    }
    
    // 'value' attribute on TextInput (id=NoteSubject_Input) at PolicySummaryScreen.pcf: line 273, column 39
    function value_114 () : java.lang.String {
      return note.Subject
    }
    
    // 'value' attribute on TextAreaInput (id=NoteText_Input) at PolicySummaryScreen.pcf: line 279, column 39
    function value_119 () : java.lang.String {
      return note.Body
    }
    
    // 'value' attribute on TextInput (id=NoteLink_Input) at PolicySummaryScreen.pcf: line 240, column 39
    function value_98 () : java.lang.String {
      return noteQuery.Count == 1 ? DisplayKey.get("Web.Summary.Overview.Notes.LinkSingle") : DisplayKey.get("Web.Summary.Overview.Notes.Link", noteQuery.Count)
    }
    
    // 'visible' attribute on ContentInput at PolicySummaryScreen.pcf: line 244, column 38
    function visible_102 () : java.lang.Boolean {
      return note == null
    }
    
    // 'visible' attribute on Link (id=NotesEditIcon) at PolicySummaryScreen.pcf: line 233, column 41
    function visible_92 () : java.lang.Boolean {
      return note != null
    }
    
    property get note () : Note {
      return getVariableValue("note", 1) as Note
    }
    
    property set note ($arg :  Note) {
      setVariableValue("note", 1, $arg)
    }
    
    property get noteQuery () : gw.api.database.IQueryBeanResult<Note> {
      return getVariableValue("noteQuery", 1) as gw.api.database.IQueryBeanResult<Note>
    }
    
    property set noteQuery ($arg :  gw.api.database.IQueryBeanResult<Note>) {
      setVariableValue("noteQuery", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PolicySummaryScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicySummaryScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=CloseDelinquencyButton) at PolicySummaryScreen.pcf: line 41, column 71
    function action_12 () : void {
      TDIC_CloseDelinquencyProcessPopup.push(plcyPeriod.Account.OpenDelinquencyTargets)
    }
    
    // 'action' attribute on ToolbarButton (id=DetailsButton) at PolicySummaryScreen.pcf: line 46, column 41
    function action_15 () : void {
      PolicyDetailSummary.go(plcyPeriod)
    }
    
    // 'action' attribute on MonetaryAmountInput (id=ArchiveTotalUnapplied_Input) at PolicySummaryScreen.pcf: line 357, column 45
    function action_150 () : void {
      UnappliedFundsDetailPopup.push(plcyPeriod.Account)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketAlertAlertBar) at PolicySummaryScreen.pcf: line 53, column 60
    function action_19 () : void {
      TroubleTicketAlertForward.push(plcyPeriod.Policy, plcyPeriod)
    }
    
    // 'action' attribute on MonetaryAmountInput (id=TotalUnapplied_Input) at PolicySummaryScreen.pcf: line 436, column 47
    function action_190 () : void {
      UnappliedFundsDetailPopup.push(plcyPeriod.Account)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketForPolicyPeriod) at PolicySummaryScreen.pcf: line 59, column 53
    function action_24 () : void {
      TroubleTicketAlertForward.push(plcyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_DelinquencyAlertAlertBar) at PolicySummaryScreen.pcf: line 65, column 70
    function action_29 () : void {
      PolicyDetailDelinquencies.go(plcyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_ActivitiesAlertBar) at PolicySummaryScreen.pcf: line 85, column 119
    function action_39 () : void {
      PolicyPeriodActivitiesPage.go(plcyPeriod)
    }
    
    // 'action' attribute on ToolbarButton (id=ArchiveRetrieveButton) at PolicySummaryScreen.pcf: line 29, column 78
    function action_4 () : void {
      RequestRetrievePopup.push(plcyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_PaymentRequestAlertBar) at PolicySummaryScreen.pcf: line 98, column 177
    function action_44 () : void {
      AccountPaymentRequests.go(plcyPeriod.Account)
    }
    
    // 'action' attribute on TextInput (id=Insured_Input) at PolicySummaryScreen.pcf: line 114, column 55
    function action_46 () : void {
      if (plcyPeriod.PrimaryInsured != null) PolicyDetailContacts.go(plcyPeriod, plcyPeriod.PrimaryInsured)
    }
    
    // 'action' attribute on Link (id=DelinquencyFlag) at PolicySummaryScreen.pcf: line 146, column 62
    function action_58 () : void {
      PolicyDetailDelinquencies.go(plcyPeriod)
    }
    
    // 'action' attribute on ToolbarButton (id=StartDelinquencyButton) at PolicySummaryScreen.pcf: line 35, column 71
    function action_8 () : void {
      StartDelinquencyProcessPopup.push(plcyPeriod.Account.OpenDelinquencyTargets)
    }
    
    // 'action' attribute on TextInput (id=PaymentPlan_Input) at PolicySummaryScreen.pcf: line 187, column 47
    function action_83 () : void {
      PaymentPlanDetailPopup.push(plcyPeriod.PaymentPlan)
    }
    
    // 'action' attribute on ToolbarButton (id=CloseDelinquencyButton) at PolicySummaryScreen.pcf: line 41, column 71
    function action_dest_13 () : pcf.api.Destination {
      return pcf.TDIC_CloseDelinquencyProcessPopup.createDestination(plcyPeriod.Account.OpenDelinquencyTargets)
    }
    
    // 'action' attribute on MonetaryAmountInput (id=ArchiveTotalUnapplied_Input) at PolicySummaryScreen.pcf: line 357, column 45
    function action_dest_151 () : pcf.api.Destination {
      return pcf.UnappliedFundsDetailPopup.createDestination(plcyPeriod.Account)
    }
    
    // 'action' attribute on ToolbarButton (id=DetailsButton) at PolicySummaryScreen.pcf: line 46, column 41
    function action_dest_16 () : pcf.api.Destination {
      return pcf.PolicyDetailSummary.createDestination(plcyPeriod)
    }
    
    // 'action' attribute on MonetaryAmountInput (id=TotalUnapplied_Input) at PolicySummaryScreen.pcf: line 436, column 47
    function action_dest_191 () : pcf.api.Destination {
      return pcf.UnappliedFundsDetailPopup.createDestination(plcyPeriod.Account)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketAlertAlertBar) at PolicySummaryScreen.pcf: line 53, column 60
    function action_dest_20 () : pcf.api.Destination {
      return pcf.TroubleTicketAlertForward.createDestination(plcyPeriod.Policy, plcyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketForPolicyPeriod) at PolicySummaryScreen.pcf: line 59, column 53
    function action_dest_25 () : pcf.api.Destination {
      return pcf.TroubleTicketAlertForward.createDestination(plcyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_DelinquencyAlertAlertBar) at PolicySummaryScreen.pcf: line 65, column 70
    function action_dest_30 () : pcf.api.Destination {
      return pcf.PolicyDetailDelinquencies.createDestination(plcyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=PolicyDetailSummary_ActivitiesAlertBar) at PolicySummaryScreen.pcf: line 85, column 119
    function action_dest_40 () : pcf.api.Destination {
      return pcf.PolicyPeriodActivitiesPage.createDestination(plcyPeriod)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_PaymentRequestAlertBar) at PolicySummaryScreen.pcf: line 98, column 177
    function action_dest_45 () : pcf.api.Destination {
      return pcf.AccountPaymentRequests.createDestination(plcyPeriod.Account)
    }
    
    // 'action' attribute on ToolbarButton (id=ArchiveRetrieveButton) at PolicySummaryScreen.pcf: line 29, column 78
    function action_dest_5 () : pcf.api.Destination {
      return pcf.RequestRetrievePopup.createDestination(plcyPeriod)
    }
    
    // 'action' attribute on Link (id=DelinquencyFlag) at PolicySummaryScreen.pcf: line 146, column 62
    function action_dest_59 () : pcf.api.Destination {
      return pcf.PolicyDetailDelinquencies.createDestination(plcyPeriod)
    }
    
    // 'action' attribute on TextInput (id=PaymentPlan_Input) at PolicySummaryScreen.pcf: line 187, column 47
    function action_dest_84 () : pcf.api.Destination {
      return pcf.PaymentPlanDetailPopup.createDestination(plcyPeriod.PaymentPlan)
    }
    
    // 'action' attribute on ToolbarButton (id=StartDelinquencyButton) at PolicySummaryScreen.pcf: line 35, column 71
    function action_dest_9 () : pcf.api.Destination {
      return pcf.StartDelinquencyProcessPopup.createDestination(plcyPeriod.Account.OpenDelinquencyTargets)
    }
    
    // 'available' attribute on ToolbarButton (id=CloseDelinquencyButton) at PolicySummaryScreen.pcf: line 41, column 71
    function available_10 () : java.lang.Boolean {
      return !CurrentLocation.InEditMode && !plcyPeriod.isDirectOrImpliedTargetOfHoldType(HoldType.TC_DELINQUENCY) && plcyPeriod.hasActiveDelinquencyProcess()
    }
    
    // 'available' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketAlertAlertBar) at PolicySummaryScreen.pcf: line 53, column 60
    function available_17 () : java.lang.Boolean {
      return perm.System.plcyttktview
    }
    
    // 'available' attribute on AlertBar (id=PolicyDetailSummary_DelinquencyAlertAlertBar) at PolicySummaryScreen.pcf: line 65, column 70
    function available_27 () : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctdelview
    }
    
    // 'available' attribute on ToolbarButton (id=StartDelinquencyButton) at PolicySummaryScreen.pcf: line 35, column 71
    function available_6 () : java.lang.Boolean {
      return !CurrentLocation.InEditMode && !plcyPeriod.isDirectOrImpliedTargetOfHoldType(HoldType.TC_DELINQUENCY)
    }
    
    // 'def' attribute on PanelRef at PolicySummaryScreen.pcf: line 374, column 36
    function def_onEnter_157 (def :  pcf.PolicySummaryFinancialsDV_Account) : void {
      def.onEnter(plcyPeriod, summaryHelper)
    }
    
    // 'def' attribute on PanelRef at PolicySummaryScreen.pcf: line 374, column 36
    function def_onEnter_159 (def :  pcf.PolicySummaryFinancialsDV_AgencyBill) : void {
      def.onEnter(plcyPeriod, summaryHelper)
    }
    
    // 'def' attribute on PanelRef at PolicySummaryScreen.pcf: line 374, column 36
    function def_onEnter_161 (def :  pcf.PolicySummaryFinancialsDV_Policy) : void {
      def.onEnter(plcyPeriod, summaryHelper)
    }
    
    // 'def' attribute on PanelRef (id=PolicyValuesChart) at PolicySummaryScreen.pcf: line 379, column 37
    function def_onEnter_164 (def :  pcf.SummaryChartPanelSet) : void {
      def.onEnter(summaryHelper.Financials.PolicyValuesChartEntries)
    }
    
    // 'def' attribute on PanelRef at PolicySummaryScreen.pcf: line 374, column 36
    function def_refreshVariables_158 (def :  pcf.PolicySummaryFinancialsDV_Account) : void {
      def.refreshVariables(plcyPeriod, summaryHelper)
    }
    
    // 'def' attribute on PanelRef at PolicySummaryScreen.pcf: line 374, column 36
    function def_refreshVariables_160 (def :  pcf.PolicySummaryFinancialsDV_AgencyBill) : void {
      def.refreshVariables(plcyPeriod, summaryHelper)
    }
    
    // 'def' attribute on PanelRef at PolicySummaryScreen.pcf: line 374, column 36
    function def_refreshVariables_162 (def :  pcf.PolicySummaryFinancialsDV_Policy) : void {
      def.refreshVariables(plcyPeriod, summaryHelper)
    }
    
    // 'def' attribute on PanelRef (id=PolicyValuesChart) at PolicySummaryScreen.pcf: line 379, column 37
    function def_refreshVariables_165 (def :  pcf.SummaryChartPanelSet) : void {
      def.refreshVariables(summaryHelper.Financials.PolicyValuesChartEntries)
    }
    
    // 'iconColor' attribute on Link (id=DelinquencyFlag) at PolicySummaryScreen.pcf: line 146, column 62
    function iconColor_62 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_PROGRESS_OVERDUE
    }
    
    // 'icon' attribute on Link (id=InfoSpacer) at PolicySummaryScreen.pcf: line 303, column 34
    function icon_123 () : java.lang.String {
      return 'blank_16.png'
    }
    
    // 'icon' attribute on Link (id=PolicyLifetimeIcon) at PolicySummaryScreen.pcf: line 122, column 69
    function icon_51 () : java.lang.String {
      return summaryHelper.TimeSinceInceptionIcon
    }
    
    // 'icon' attribute on Link (id=DelinquencyFlag) at PolicySummaryScreen.pcf: line 146, column 62
    function icon_61 () : java.lang.String {
      return summaryHelper.DelinquencyIcon
    }
    
    // 'initialValue' attribute on Variable at PolicySummaryScreen.pcf: line 14, column 49
    function initialValue_0 () : gw.web.policy.PolicySummaryHelper {
      return new gw.web.policy.PolicySummaryHelper(plcyPeriod)
    }
    
    // 'initialValue' attribute on Variable at PolicySummaryScreen.pcf: line 19, column 22
    function initialValue_1 () : String {
      return summaryHelper.Financials.FinancialsMode
    }
    
    // 'initialValue' attribute on Variable at PolicySummaryScreen.pcf: line 23, column 23
    function initialValue_2 () : boolean {
      return plcyPeriod.Account.UnappliedFunds.length > 1
    }
    
    // 'label' attribute on Link (id=AccountInfoText) at PolicySummaryScreen.pcf: line 306, column 44
    function label_124 () : java.lang.Object {
      return BillingInfoString
    }
    
    // 'label' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketAlertAlertBar) at PolicySummaryScreen.pcf: line 53, column 60
    function label_21 () : java.lang.Object {
      return plcyPeriod.Policy.AlertBarDisplayText
    }
    
    // 'label' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketForPolicyPeriod) at PolicySummaryScreen.pcf: line 59, column 53
    function label_26 () : java.lang.Object {
      return plcyPeriod.AlertBarDisplayText
    }
    
    // 'label' attribute on AlertBar (id=PolicyDetailSummary_DelinquencyAlertAlertBar) at PolicySummaryScreen.pcf: line 65, column 70
    function label_31 () : java.lang.Object {
      return DisplayKey.get("Web.PolicyDetailSummary.DelinquencyAlert", plcyPeriod.getDelinquencyReasons())
    }
    
    // 'label' attribute on AlertBar (id=PolicyDetailSummary_PreDueAlertBar) at PolicySummaryScreen.pcf: line 70, column 212
    function label_34 () : java.lang.Object {
      return DisplayKey.get("Web.PolicyDetailSummary.PreDueAlert", plcyPeriod.getPaymentDueDateToAvoidDelinquencyOn(Date.Today).AsUIStyle)
    }
    
    // 'label' attribute on AlertBar (id=ArchivedAlert) at PolicySummaryScreen.pcf: line 75, column 38
    function label_36 () : java.lang.Object {
      return DisplayKey.get("Web.Archive.PolicyPeriod.ArchivedAlertBar", plcyPeriod.ArchiveDate.AsUIStyle)
    }
    
    // 'label' attribute on Link (id=PolicySinceValue) at PolicySummaryScreen.pcf: line 125, column 66
    function label_52 () : java.lang.Object {
      return summaryHelper.TimeSinceInceptionDisplay
    }
    
    // 'label' attribute on Link (id=DelinquencyDescription) at PolicySummaryScreen.pcf: line 150, column 45
    function label_63 () : java.lang.Object {
      return summaryHelper.HasActiveDelinquencyTarget ? DisplayKey.get("Web.PolicySummary.Overview.Delinquencies.HowManyRecent", summaryHelper.RecentDelinquenciesCount) : DisplayKey.get("Web.PolicySummary.Overview.Delinquencies.HowMany", summaryHelper.DelinquenciesCount)
    }
    
    // 'mode' attribute on PanelRef at PolicySummaryScreen.pcf: line 374, column 36
    function mode_163 () : java.lang.Object {
      return financialsMode
    }
    
    // 'tooltip' attribute on Link (id=DelinquencyFlag) at PolicySummaryScreen.pcf: line 146, column 62
    function tooltip_60 () : java.lang.String {
      return summaryHelper.RecentDelinquenciesTooltip
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ArchivePremium_Input) at PolicySummaryScreen.pcf: line 314, column 77
    function valueRoot_127 () : java.lang.Object {
      return plcyPeriod.PolicyPeriodArchiveSummary
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ArchiveDefaultUnapplied_Input) at PolicySummaryScreen.pcf: line 350, column 46
    function valueRoot_146 () : java.lang.Object {
      return plcyPeriod.Account
    }
    
    // 'value' attribute on TextInput (id=section_spacer_Input) at PolicySummaryScreen.pcf: line 415, column 37
    function valueRoot_177 () : java.lang.Object {
      return gw.util.GosuStringUtil
    }
    
    // 'value' attribute on MonetaryAmountInput (id=DesignatedUnapplied_Input) at PolicySummaryScreen.pcf: line 423, column 65
    function valueRoot_181 () : java.lang.Object {
      return plcyPeriod.Policy.getDesignatedUnappliedFund(plcyPeriod.Account)
    }
    
    // 'value' attribute on TextInput (id=Insured_Input) at PolicySummaryScreen.pcf: line 114, column 55
    function valueRoot_48 () : java.lang.Object {
      return plcyPeriod
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ArchivePremium_Input) at PolicySummaryScreen.pcf: line 314, column 77
    function value_126 () : gw.pl.currency.MonetaryAmount {
      return plcyPeriod.PolicyPeriodArchiveSummary.PremiumCharges
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ArchiveOther_Input) at PolicySummaryScreen.pcf: line 319, column 75
    function value_129 () : gw.pl.currency.MonetaryAmount {
      return plcyPeriod.PolicyPeriodArchiveSummary.OtherCharges
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ArchiveTotal_Input) at PolicySummaryScreen.pcf: line 325, column 133
    function value_132 () : gw.pl.currency.MonetaryAmount {
      return plcyPeriod.PolicyPeriodArchiveSummary.PremiumCharges.add(plcyPeriod.PolicyPeriodArchiveSummary.OtherCharges)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ArchivePaid_Input) at PolicySummaryScreen.pcf: line 330, column 67
    function value_134 () : gw.pl.currency.MonetaryAmount {
      return plcyPeriod.PolicyPeriodArchiveSummary.Paid
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ArchiveWrittenOff_Input) at PolicySummaryScreen.pcf: line 335, column 73
    function value_137 () : gw.pl.currency.MonetaryAmount {
      return plcyPeriod.PolicyPeriodArchiveSummary.WrittenOff
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ArchiveDesignatedUnapplied_Input) at PolicySummaryScreen.pcf: line 344, column 63
    function value_141 () : gw.pl.currency.MonetaryAmount {
      return PolicyDesignatedUnapplied
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ArchiveDefaultUnapplied_Input) at PolicySummaryScreen.pcf: line 350, column 46
    function value_145 () : gw.pl.currency.MonetaryAmount {
      return plcyPeriod.Account.UnappliedAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ArchiveTotalUnapplied_Input) at PolicySummaryScreen.pcf: line 357, column 45
    function value_152 () : gw.pl.currency.MonetaryAmount {
      return plcyPeriod.Account.TotalUnappliedAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PolicyValue_Input) at PolicySummaryScreen.pcf: line 390, column 57
    function value_166 () : gw.pl.currency.MonetaryAmount {
      return plcyPeriod.getTotalValue(null)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PolicyEquity_Input) at PolicySummaryScreen.pcf: line 403, column 50
    function value_168 () : gw.pl.currency.MonetaryAmount {
      return plcyPeriod.PolicyEquity
    }
    
    // 'value' attribute on TextInput (id=PolicyEquityPercentage_Input) at PolicySummaryScreen.pcf: line 407, column 115
    function value_171 () : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(plcyPeriod.getPolicyEquityPercentage(), 2)
    }
    
    // 'value' attribute on DateInput (id=PaidThruDate_Input) at PolicySummaryScreen.pcf: line 411, column 53
    function value_173 () : java.util.Date {
      return plcyPeriod.PaidThroughDate
    }
    
    // 'value' attribute on TextInput (id=section_spacer_Input) at PolicySummaryScreen.pcf: line 415, column 37
    function value_176 () : String {
      return gw.util.GosuStringUtil.EMPTY
    }
    
    // 'value' attribute on MonetaryAmountInput (id=DesignatedUnapplied_Input) at PolicySummaryScreen.pcf: line 423, column 65
    function value_180 () : gw.pl.currency.MonetaryAmount {
      return plcyPeriod.Policy.getDesignatedUnappliedFund(plcyPeriod.Account).Balance
    }
    
    // 'value' attribute on TextInput (id=Insured_Input) at PolicySummaryScreen.pcf: line 114, column 55
    function value_47 () : entity.PolicyPeriodContact {
      return plcyPeriod.PrimaryInsured
    }
    
    // 'value' attribute on TextInput (id=EffectiveDate_Input) at PolicySummaryScreen.pcf: line 130, column 122
    function value_53 () : java.lang.String {
      return plcyPeriod.PolicyPerEffDate.AsUIStyle + " - " + plcyPeriod.PolicyPerExpirDate.AsUIStyle
    }
    
    // 'value' attribute on TextInput (id=CancellationStatus_Input) at PolicySummaryScreen.pcf: line 134, column 233
    function value_55 () : java.lang.String {
      return plcyPeriod.CancelStatus == PolicyCancelStatus.TC_CANCELED ? (plcyPeriod.CancelStatus.DisplayName + " (" + plcyPeriod.CancellationDate.AsUIStyle + ")") : plcyPeriod.CancelStatus.DisplayName
    }
    
    // 'value' attribute on TextInput (id=UWCompany_Input) at PolicySummaryScreen.pcf: line 156, column 46
    function value_64 () : typekey.UWCompany {
      return plcyPeriod.UWCompany
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at PolicySummaryScreen.pcf: line 164, column 45
    function value_67 () : typekey.Currency {
      return plcyPeriod.Currency
    }
    
    // 'value' attribute on TypeKeyInput (id=BillingMethod_Input) at PolicySummaryScreen.pcf: line 169, column 62
    function value_70 () : typekey.PolicyPeriodBillingMethod {
      return plcyPeriod.BillingMethod
    }
    
    // 'value' attribute on TypeKeyInput (id=SendInvoicesBy_Input) at PolicySummaryScreen.pcf: line 175, column 100
    function value_74 () : typekey.InvoiceDeliveryMethod {
      return plcyPeriod.InvoiceDeliveryType
    }
    
    // 'value' attribute on TextInput (id=PaymentMethod_Input) at PolicySummaryScreen.pcf: line 181, column 47
    function value_79 () : entity.PaymentInstrument {
      return plcyPeriod.PaymentInstrument
    }
    
    // 'value' attribute on TextInput (id=PaymentPlan_Input) at PolicySummaryScreen.pcf: line 187, column 47
    function value_85 () : entity.PaymentPlan {
      return plcyPeriod.PaymentPlan
    }
    
    // 'visible' attribute on ContentInput at PolicySummaryScreen.pcf: line 296, column 62
    function visible_125 () : java.lang.Boolean {
      return financialsMode != "Policy"
    }
    
    // 'visible' attribute on ToolbarButton (id=DetailsButton) at PolicySummaryScreen.pcf: line 46, column 41
    function visible_14 () : java.lang.Boolean {
      return !plcyPeriod.Archived
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=ArchiveDesignatedUnapplied_Input) at PolicySummaryScreen.pcf: line 344, column 63
    function visible_140 () : java.lang.Boolean {
      return financialsMode == "Policy"
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=ArchiveDefaultUnapplied_Input) at PolicySummaryScreen.pcf: line 350, column 46
    function visible_144 () : java.lang.Boolean {
      return !showTotalUnapplied
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=ArchiveTotalUnapplied_Input) at PolicySummaryScreen.pcf: line 357, column 45
    function visible_149 () : java.lang.Boolean {
      return showTotalUnapplied
    }
    
    // 'visible' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketAlertAlertBar) at PolicySummaryScreen.pcf: line 53, column 60
    function visible_18 () : java.lang.Boolean {
      return plcyPeriod.Policy.HasActiveTroubleTickets
    }
    
    // 'visible' attribute on AlertBar (id=PolicyDetailSummary_TroubleTicketForPolicyPeriod) at PolicySummaryScreen.pcf: line 59, column 53
    function visible_23 () : java.lang.Boolean {
      return plcyPeriod.HasActiveTroubleTickets
    }
    
    // 'visible' attribute on AlertBar (id=PolicyDetailSummary_DelinquencyAlertAlertBar) at PolicySummaryScreen.pcf: line 65, column 70
    function visible_28 () : java.lang.Boolean {
      return plcyPeriod.hasActiveDelinquenciesOutOfGracePeriod()
    }
    
    // 'visible' attribute on ToolbarButton (id=ArchiveRetrieveButton) at PolicySummaryScreen.pcf: line 29, column 78
    function visible_3 () : java.lang.Boolean {
      return plcyPeriod.Archived and perm.PolicyPeriod.archiveretrieve
    }
    
    // 'visible' attribute on AlertBar (id=PolicyDetailSummary_PreDueAlertBar) at PolicySummaryScreen.pcf: line 70, column 212
    function visible_33 () : java.lang.Boolean {
      return plcyPeriod.PreDueUnsettledAmount.IsPositive && !plcyPeriod.hasActiveDelinquenciesOutOfGracePeriod() && plcyPeriod.getPaymentDueDateToAvoidDelinquencyOn(Date.Today) == Date.Today
    }
    
    // 'visible' attribute on AlertBar (id=ArchivedAlert) at PolicySummaryScreen.pcf: line 75, column 38
    function visible_35 () : java.lang.Boolean {
      return plcyPeriod.Archived
    }
    
    // 'visible' attribute on AlertBar (id=ArchivedAndRetrievedAlert) at PolicySummaryScreen.pcf: line 80, column 39
    function visible_37 () : java.lang.Boolean {
      return plcyPeriod.Retrieved
    }
    
    // 'visible' attribute on AlertBar (id=PolicyDetailSummary_ActivitiesAlertBar) at PolicySummaryScreen.pcf: line 85, column 119
    function visible_38 () : java.lang.Boolean {
      return plcyPeriod.Activities_Ext.where(\ a -> a.Status == typekey.ActivityStatus.TC_OPEN ).length > 0
    }
    
    // 'visible' attribute on AlertBar (id=PolicyDetailSummary_NewDentistProgramDiscount) at PolicySummaryScreen.pcf: line 89, column 52
    function visible_41 () : java.lang.Boolean {
      return plcyPeriod.NewDentistProgram_TDIC
    }
    
    // 'visible' attribute on AlertBar (id=PolicyDetailSummary_ServiceMembersCivilReliefAct) at PolicySummaryScreen.pcf: line 93, column 60
    function visible_42 () : java.lang.Boolean {
      return plcyPeriod.ServiceMembCivilReliefAct_TDIC
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailSummary_PaymentRequestAlertBar) at PolicySummaryScreen.pcf: line 98, column 177
    function visible_43 () : java.lang.Boolean {
      return plcyPeriod.Account.PaymentRequests.where(\ a -> a.Status == PaymentRequestStatus.TC_CREATED or a.Status == PaymentRequestStatus.TC_REQUESTED).length > 0
    }
    
    // 'visible' attribute on Link (id=PolicyLifetimeIcon) at PolicySummaryScreen.pcf: line 122, column 69
    function visible_50 () : java.lang.Boolean {
      return summaryHelper.ShowTimeSinceInceptionIcon
    }
    
    // 'visible' attribute on Link (id=DelinquencyFlag) at PolicySummaryScreen.pcf: line 146, column 62
    function visible_57 () : java.lang.Boolean {
      return summaryHelper.ShowDelinquencyIcon
    }
    
    // 'visible' attribute on ToolbarButton (id=StartDelinquencyButton) at PolicySummaryScreen.pcf: line 35, column 71
    function visible_7 () : java.lang.Boolean {
      return !plcyPeriod.Closed and perm.System.manualDlnq_TDIC
    }
    
    // 'visible' attribute on TypeKeyInput (id=SendInvoicesBy_Input) at PolicySummaryScreen.pcf: line 175, column 100
    function visible_73 () : java.lang.Boolean {
      return plcyPeriod.BillingMethod == TC_DIRECTBILL && !plcyPeriod.Archived
    }
    
    property get financialsMode () : String {
      return getVariableValue("financialsMode", 0) as String
    }
    
    property set financialsMode ($arg :  String) {
      setVariableValue("financialsMode", 0, $arg)
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getRequireValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("plcyPeriod", 0, $arg)
    }
    
    property get showTotalUnapplied () : boolean {
      return getVariableValue("showTotalUnapplied", 0) as java.lang.Boolean
    }
    
    property set showTotalUnapplied ($arg :  boolean) {
      setVariableValue("showTotalUnapplied", 0, $arg)
    }
    
    property get summaryHelper () : gw.web.policy.PolicySummaryHelper {
      return getVariableValue("summaryHelper", 0) as gw.web.policy.PolicySummaryHelper
    }
    
    property set summaryHelper ($arg :  gw.web.policy.PolicySummaryHelper) {
      setVariableValue("summaryHelper", 0, $arg)
    }
    
    
    property get PolicyDesignatedUnapplied() : MonetaryAmount {
      var unappliedFund = plcyPeriod.Policy.getDesignatedUnappliedFund(plcyPeriod.Account)
      if (unappliedFund != null) {
        return unappliedFund.Balance
      } else {
        return new MonetaryAmount(0, plcyPeriod.Currency)
      }
    }
    
    property get BillingInfoString() : String {
      if (financialsMode == "Account") {
        return DisplayKey.get("Web.PolicySummary.Financials.Account")
      } else if (financialsMode == "AgencyBill") {
        return DisplayKey.get("Web.PolicySummary.Financials.Producer")
      } else {
        return null
      }
    }
    
    
  }
  
  
}