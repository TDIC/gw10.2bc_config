package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/note/NoteEditPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NoteEditPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/note/NoteEditPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NoteEditPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (Note :  Note) : int {
      return 0
    }
    
    static function __constructorIndex (Note :  Note, docContainer :  DocumentContainer) : int {
      return 1
    }
    
    // 'action' attribute on PickerToolbarButton (id=linkDocsPickerButton) at NoteEditPopup.pcf: line 29, column 85
    function action_1 () : void {
      PickExistingDocumentPopup.push(docContainer)
    }
    
    // 'action' attribute on PickerToolbarButton (id=linkDocsPickerButton) at NoteEditPopup.pcf: line 29, column 85
    function action_dest_2 () : pcf.api.Destination {
      return pcf.PickExistingDocumentPopup.createDestination(docContainer)
    }
    
    // 'canVisit' attribute on Popup (id=NoteEditPopup) at NoteEditPopup.pcf: line 10, column 60
    static function canVisit_6 (Note :  Note, docContainer :  DocumentContainer) : java.lang.Boolean {
      return perm.Note.edit(Note)
    }
    
    // 'def' attribute on PanelRef at NoteEditPopup.pcf: line 32, column 32
    function def_onEnter_4 (def :  pcf.NewNoteDV) : void {
      def.onEnter(Note)
    }
    
    // 'def' attribute on PanelRef at NoteEditPopup.pcf: line 32, column 32
    function def_refreshVariables_5 (def :  pcf.NewNoteDV) : void {
      def.refreshVariables(Note)
    }
    
    // EditButtons at NoteEditPopup.pcf: line 24, column 23
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=linkDocsPickerButton) at NoteEditPopup.pcf: line 29, column 85
    function onPick_3 (PickedValue :  Document) : void {
      acc.onbase.util.NotesUtil.linkDocumentToNote(PickedValue, Note)
    }
    
    override property get CurrentLocation () : pcf.NoteEditPopup {
      return super.CurrentLocation as pcf.NoteEditPopup
    }
    
    property get Note () : Note {
      return getVariableValue("Note", 0) as Note
    }
    
    property set Note ($arg :  Note) {
      setVariableValue("Note", 0, $arg)
    }
    
    property get docContainer () : DocumentContainer {
      return getVariableValue("docContainer", 0) as DocumentContainer
    }
    
    property set docContainer ($arg :  DocumentContainer) {
      setVariableValue("docContainer", 0, $arg)
    }
    
    
  }
  
  
}