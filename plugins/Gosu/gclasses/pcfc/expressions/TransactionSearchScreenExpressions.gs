package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/TransactionSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransactionSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/search/TransactionSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Debit_Cell) at TransactionSearchScreen.pcf: line 104, column 44
    function currency_43 () : typekey.Currency {
      return LineItem.Currency
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at TransactionSearchScreen.pcf: line 99, column 80
    function valueRoot_39 () : java.lang.Object {
      return LineItem.TAccount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Debit_Cell) at TransactionSearchScreen.pcf: line 104, column 44
    function valueRoot_42 () : java.lang.Object {
      return LineItem
    }
    
    // 'value' attribute on TextCell (id=Date_Cell) at TransactionSearchScreen.pcf: line 84, column 49
    function value_30 () : java.lang.Object {
      return null
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at TransactionSearchScreen.pcf: line 99, column 80
    function value_38 () : java.lang.String {
      return LineItem.TAccount.TAccountOwnerTypeNameTAccountName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Debit_Cell) at TransactionSearchScreen.pcf: line 104, column 44
    function value_41 () : gw.pl.currency.MonetaryAmount {
      return LineItem.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Credit_Cell) at TransactionSearchScreen.pcf: line 109, column 62
    function value_45 () : gw.pl.currency.MonetaryAmount {
      return 0bd.ofCurrency(LineItem.Currency)
    }
    
    property get LineItem () : entity.LineItem {
      return getIteratedValue(3) as entity.LineItem
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/TransactionSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Debit_Cell) at TransactionSearchScreen.pcf: line 143, column 65
    function currency_61 () : typekey.Currency {
      return LineItem.Currency
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at TransactionSearchScreen.pcf: line 138, column 80
    function valueRoot_58 () : java.lang.Object {
      return LineItem.TAccount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Credit_Cell) at TransactionSearchScreen.pcf: line 148, column 44
    function valueRoot_64 () : java.lang.Object {
      return LineItem
    }
    
    // 'value' attribute on TextCell (id=Date_Cell) at TransactionSearchScreen.pcf: line 123, column 49
    function value_49 () : java.lang.Object {
      return null
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at TransactionSearchScreen.pcf: line 138, column 80
    function value_57 () : java.lang.String {
      return LineItem.TAccount.TAccountOwnerTypeNameTAccountName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Debit_Cell) at TransactionSearchScreen.pcf: line 143, column 65
    function value_60 () : gw.pl.currency.MonetaryAmount {
      return 0bd.ofCurrency(transaction.Currency)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Credit_Cell) at TransactionSearchScreen.pcf: line 148, column 44
    function value_63 () : gw.pl.currency.MonetaryAmount {
      return LineItem.Amount
    }
    
    property get LineItem () : entity.LineItem {
      return getIteratedValue(3) as entity.LineItem
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/TransactionSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at TransactionSearchScreen.pcf: line 41, column 31
    function action_9 () : void {
      TransactionDetailPopup.push(transaction)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at TransactionSearchScreen.pcf: line 41, column 31
    function action_dest_10 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(transaction)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at TransactionSearchScreen.pcf: line 51, column 45
    function currency_19 () : typekey.Currency {
      return transaction.Currency
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at TransactionSearchScreen.pcf: line 57, column 95
    function valueRoot_22 () : java.lang.Object {
      return transaction.FirstLineItem.TAccount
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at TransactionSearchScreen.pcf: line 35, column 54
    function valueRoot_7 () : java.lang.Object {
      return transaction
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at TransactionSearchScreen.pcf: line 41, column 31
    function value_11 () : java.lang.String {
      return transaction.TransactionNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at TransactionSearchScreen.pcf: line 46, column 50
    function value_14 () : typekey.Transaction {
      return transaction.Subtype
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at TransactionSearchScreen.pcf: line 51, column 45
    function value_17 () : gw.pl.currency.MonetaryAmount {
      return transaction.Amount
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at TransactionSearchScreen.pcf: line 57, column 95
    function value_21 () : java.lang.String {
      return transaction.FirstLineItem.TAccount.TAccountOwnerTypeNameTAccountName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Debit_Cell) at TransactionSearchScreen.pcf: line 64, column 111
    function value_24 () : gw.pl.currency.MonetaryAmount {
      return transaction.FirstLineItem.Type == TC_DEBIT ? transaction.FirstLineItem.Amount : null
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Credit_Cell) at TransactionSearchScreen.pcf: line 71, column 112
    function value_27 () : gw.pl.currency.MonetaryAmount {
      return transaction.FirstLineItem.Type == TC_CREDIT ? transaction.FirstLineItem.Amount : null
    }
    
    // 'value' attribute on RowIterator (id=OtherDebits) at TransactionSearchScreen.pcf: line 79, column 45
    function value_48 () : entity.LineItem[] {
      return org.apache.commons.lang.ArrayUtils.remove(transaction.DebitLineItems, 0) as LineItem[]
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at TransactionSearchScreen.pcf: line 35, column 54
    function value_6 () : java.util.Date {
      return transaction.TransactionDate
    }
    
    // 'value' attribute on RowIterator (id=OtherCredits) at TransactionSearchScreen.pcf: line 118, column 45
    function value_67 () : entity.LineItem[] {
      return transaction.CreditLineItems
    }
    
    property get transaction () : entity.Transaction {
      return getIteratedValue(2) as entity.Transaction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/TransactionSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends TransactionSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at TransactionSearchScreen.pcf: line 16, column 51
    function def_onEnter_0 (def :  pcf.TransactionSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at TransactionSearchScreen.pcf: line 16, column 51
    function def_refreshVariables_1 (def :  pcf.TransactionSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'maxSearchResults' attribute on SearchPanel at TransactionSearchScreen.pcf: line 14, column 79
    function maxSearchResults_69 () : java.lang.Object {
      return gw.api.system.BCConfigParameters.MaxSearchResults.Value
    }
    
    // 'searchCriteria' attribute on SearchPanel at TransactionSearchScreen.pcf: line 14, column 79
    function searchCriteria_71 () : gw.search.TransactionSearchCriteria {
      return new gw.search.TransactionSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at TransactionSearchScreen.pcf: line 14, column 79
    function search_70 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria)
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at TransactionSearchScreen.pcf: line 35, column 54
    function sortValue_2 (transaction :  entity.Transaction) : java.lang.Object {
      return transaction.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at TransactionSearchScreen.pcf: line 41, column 31
    function sortValue_3 (transaction :  entity.Transaction) : java.lang.Object {
      return transaction.TransactionNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at TransactionSearchScreen.pcf: line 46, column 50
    function sortValue_4 (transaction :  entity.Transaction) : java.lang.Object {
      return transaction.Subtype
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at TransactionSearchScreen.pcf: line 51, column 45
    function sortValue_5 (transaction :  entity.Transaction) : java.lang.Object {
      return transaction.Amount
    }
    
    // 'value' attribute on RowIterator at TransactionSearchScreen.pcf: line 28, column 84
    function value_68 () : gw.api.database.IQueryBeanResult<entity.Transaction> {
      return transactions
    }
    
    property get searchCriteria () : gw.search.TransactionSearchCriteria {
      return getCriteriaValue(1) as gw.search.TransactionSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.TransactionSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    property get transactions () : gw.api.database.IQueryBeanResult<Transaction> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<Transaction>
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/TransactionSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransactionSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    
  }
  
  
}