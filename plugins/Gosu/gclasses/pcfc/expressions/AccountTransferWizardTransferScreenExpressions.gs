package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transfer/AccountTransferWizardTransferScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountTransferWizardTransferScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transfer/AccountTransferWizardTransferScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountTransferWizardTransferScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on AlertBar (id=CannotTransferToAListBillAccount) at AccountTransferWizardTransferScreen.pcf: line 17, column 54
    function available_0 () : java.lang.Boolean {
      return accountTransfer.ToAccount.ListBill 
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at AccountTransferWizardTransferScreen.pcf: line 62, column 75
    function sortValue_10 (PolicyToTransfer :  gw.api.domain.account.AccountTransferPolicyPeriod) : java.lang.Object {
      return PolicyToTransfer.PolicyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at AccountTransferWizardTransferScreen.pcf: line 66, column 77
    function sortValue_11 (PolicyToTransfer :  gw.api.domain.account.AccountTransferPolicyPeriod) : java.lang.Object {
      return PolicyToTransfer.PolicyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AccountTransferWizardTransferScreen.pcf: line 58, column 70
    function sortValue_9 (PolicyToTransfer :  gw.api.domain.account.AccountTransferPolicyPeriod) : java.lang.Object {
      return PolicyToTransfer.PolicyPeriod.DisplayName
    }
    
    // 'value' attribute on TextInput (id=FromAccountNumber_Input) at AccountTransferWizardTransferScreen.pcf: line 30, column 62
    function valueRoot_4 () : java.lang.Object {
      return accountTransfer.FromAccount
    }
    
    // 'value' attribute on TextInput (id=ToAccountNumber_Input) at AccountTransferWizardTransferScreen.pcf: line 36, column 60
    function valueRoot_7 () : java.lang.Object {
      return accountTransfer.ToAccount
    }
    
    // 'value' attribute on RowIterator at AccountTransferWizardTransferScreen.pcf: line 48, column 79
    function value_27 () : gw.api.domain.account.AccountTransferPolicyPeriod[] {
      return accountTransfer.PolicyPeriods
    }
    
    // 'value' attribute on TextInput (id=FromAccountNumber_Input) at AccountTransferWizardTransferScreen.pcf: line 30, column 62
    function value_3 () : java.lang.String {
      return accountTransfer.FromAccount.AccountNumber
    }
    
    // 'value' attribute on TextInput (id=ToAccountNumber_Input) at AccountTransferWizardTransferScreen.pcf: line 36, column 60
    function value_6 () : java.lang.String {
      return accountTransfer.ToAccount.AccountNumber
    }
    
    // 'visible' attribute on AlertBar (id=ToAccountMissing) at AccountTransferWizardTransferScreen.pcf: line 21, column 50
    function visible_2 () : java.lang.Boolean {
      return accountTransfer.ToAccount==null
    }
    
    property get accountTransfer () : gw.api.domain.account.AccountTransfer {
      return getRequireValue("accountTransfer", 0) as gw.api.domain.account.AccountTransfer
    }
    
    property set accountTransfer ($arg :  gw.api.domain.account.AccountTransfer) {
      setRequireValue("accountTransfer", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transfer/AccountTransferWizardTransferScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountTransferWizardTransferScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on CheckBoxCell (id=TransferPolicy_Cell) at AccountTransferWizardTransferScreen.pcf: line 54, column 54
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      PolicyToTransfer.Transfer = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'validationExpression' attribute on CheckBoxCell (id=TransferPolicy_Cell) at AccountTransferWizardTransferScreen.pcf: line 54, column 54
    function validationExpression_12 () : java.lang.Object {
      var msg = accountTransfer.validatePolicySelected(); return msg != null ? msg : accountTransfer.validatePolicyHasNoSelectedFrozenItems()
    }
    
    // 'value' attribute on CheckBoxCell (id=TransferPolicy_Cell) at AccountTransferWizardTransferScreen.pcf: line 54, column 54
    function valueRoot_15 () : java.lang.Object {
      return PolicyToTransfer
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AccountTransferWizardTransferScreen.pcf: line 58, column 70
    function valueRoot_19 () : java.lang.Object {
      return PolicyToTransfer.PolicyPeriod
    }
    
    // 'value' attribute on CheckBoxCell (id=TransferPolicy_Cell) at AccountTransferWizardTransferScreen.pcf: line 54, column 54
    function value_13 () : java.lang.Boolean {
      return PolicyToTransfer.Transfer
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AccountTransferWizardTransferScreen.pcf: line 58, column 70
    function value_18 () : java.lang.String {
      return PolicyToTransfer.PolicyPeriod.DisplayName
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at AccountTransferWizardTransferScreen.pcf: line 62, column 75
    function value_21 () : java.util.Date {
      return PolicyToTransfer.PolicyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at AccountTransferWizardTransferScreen.pcf: line 66, column 77
    function value_24 () : java.util.Date {
      return PolicyToTransfer.PolicyPeriod.PolicyPerExpirDate
    }
    
    property get PolicyToTransfer () : gw.api.domain.account.AccountTransferPolicyPeriod {
      return getIteratedValue(1) as gw.api.domain.account.AccountTransferPolicyPeriod
    }
    
    
  }
  
  
}