package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/businesssettings/PaymentPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentPlansExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/PaymentPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PaymentPlansExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at PaymentPlans.pcf: line 44, column 41
    function action_10 () : void {
      PaymentPlanDetail.go(paymentPlan)
    }
    
    // 'action' attribute on Link (id=MoveUp) at PaymentPlans.pcf: line 68, column 57
    function action_26 () : void {
      PlanHelper.moveUp(paymentPlan)
    }
    
    // 'action' attribute on Link (id=MoveDown) at PaymentPlans.pcf: line 76, column 63
    function action_29 () : void {
      PlanHelper.moveDown(paymentPlan)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at PaymentPlans.pcf: line 44, column 41
    function action_dest_11 () : pcf.api.Destination {
      return pcf.PaymentPlanDetail.createDestination(paymentPlan)
    }
    
    // 'available' attribute on Link (id=MoveUp) at PaymentPlans.pcf: line 68, column 57
    function available_24 () : java.lang.Boolean {
      return perm.System.pmntplanedit
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at PaymentPlans.pcf: line 38, column 46
    function valueRoot_8 () : java.lang.Object {
      return paymentPlan
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at PaymentPlans.pcf: line 44, column 41
    function value_12 () : java.lang.String {
      return paymentPlan.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at PaymentPlans.pcf: line 49, column 48
    function value_15 () : java.lang.String {
      return paymentPlan.Description
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at PaymentPlans.pcf: line 53, column 50
    function value_18 () : java.util.Date {
      return paymentPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at PaymentPlans.pcf: line 57, column 51
    function value_21 () : java.util.Date {
      return paymentPlan.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at PaymentPlans.pcf: line 38, column 46
    function value_7 () : java.lang.Integer {
      return paymentPlan.PlanOrder
    }
    
    // 'visible' attribute on Link (id=MoveUp) at PaymentPlans.pcf: line 68, column 57
    function visible_25 () : java.lang.Boolean {
      return paymentPlan.PlanOrder > 1
    }
    
    // 'visible' attribute on Link (id=MoveDown) at PaymentPlans.pcf: line 76, column 63
    function visible_28 () : java.lang.Boolean {
      return !paymentPlan.hasHighestPlanOrder()
    }
    
    property get paymentPlan () : entity.PaymentPlan {
      return getIteratedValue(1) as entity.PaymentPlan
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/PaymentPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentPlansExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=PaymentPlans) at PaymentPlans.pcf: line 9, column 64
    static function canVisit_31 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.pmntplanview
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at PaymentPlans.pcf: line 29, column 80
    function filters_1 () : gw.api.filters.IFilter[] {
      return new gw.api.web.plan.PlanListFilterSet().FilterOptions
    }
    
    // 'initialValue' attribute on Variable at PaymentPlans.pcf: line 13, column 42
    function initialValue_0 () : gw.api.web.plan.PlanHelper {
      return new gw.api.web.plan.PlanHelper()
    }
    
    // Page (id=PaymentPlans) at PaymentPlans.pcf: line 9, column 64
    static function parent_32 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at PaymentPlans.pcf: line 38, column 46
    function sortValue_2 (paymentPlan :  entity.PaymentPlan) : java.lang.Object {
      return paymentPlan.PlanOrder
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at PaymentPlans.pcf: line 44, column 41
    function sortValue_3 (paymentPlan :  entity.PaymentPlan) : java.lang.Object {
      return paymentPlan.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at PaymentPlans.pcf: line 49, column 48
    function sortValue_4 (paymentPlan :  entity.PaymentPlan) : java.lang.Object {
      return paymentPlan.Description
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at PaymentPlans.pcf: line 53, column 50
    function sortValue_5 (paymentPlan :  entity.PaymentPlan) : java.lang.Object {
      return paymentPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at PaymentPlans.pcf: line 57, column 51
    function sortValue_6 (paymentPlan :  entity.PaymentPlan) : java.lang.Object {
      return paymentPlan.ExpirationDate
    }
    
    // 'value' attribute on RowIterator at PaymentPlans.pcf: line 24, column 82
    function value_30 () : gw.api.database.IQueryBeanResult<entity.PaymentPlan> {
      return gw.api.web.plan.PaymentPlans.findVisiblePaymentPlans()
    }
    
    override property get CurrentLocation () : pcf.PaymentPlans {
      return super.CurrentLocation as pcf.PaymentPlans
    }
    
    property get PlanHelper () : gw.api.web.plan.PlanHelper {
      return getVariableValue("PlanHelper", 0) as gw.api.web.plan.PlanHelper
    }
    
    property set PlanHelper ($arg :  gw.api.web.plan.PlanHelper) {
      setVariableValue("PlanHelper", 0, $arg)
    }
    
    
  }
  
  
}