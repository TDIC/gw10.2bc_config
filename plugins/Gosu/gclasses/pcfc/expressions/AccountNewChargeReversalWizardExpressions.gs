package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/AccountNewChargeReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountNewChargeReversalWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/AccountNewChargeReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountNewChargeReversalWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (accountID :  gw.pl.persistence.core.Key) : int {
      return 0
    }
    
    // 'allowNext' attribute on WizardStep (id=Step1) at AccountNewChargeReversalWizard.pcf: line 28, column 91
    function allowNext_1 () : java.lang.Boolean {
      return reversal.Charge != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=AccountNewChargeReversalWizard) at AccountNewChargeReversalWizard.pcf: line 11, column 41
    function beforeCommit_7 (pickedValue :  java.lang.Object) : void {
      reversal.reverse()
    }
    
    // 'canVisit' attribute on Wizard (id=AccountNewChargeReversalWizard) at AccountNewChargeReversalWizard.pcf: line 11, column 41
    static function canVisit_8 (accountID :  gw.pl.persistence.core.Key) : java.lang.Boolean {
      return perm.Transaction.revtxn
    }
    
    // 'initialValue' attribute on Variable at AccountNewChargeReversalWizard.pcf: line 20, column 30
    function initialValue_0 () : ChargeReversal {
      return new ChargeReversal()
    }
    
    // 'onExit' attribute on WizardStep (id=Step1) at AccountNewChargeReversalWizard.pcf: line 28, column 91
    function onExit_2 () : void {
      reversal.initiateApprovalActivityIfUserLacksAuthority()
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountNewChargeReversalWizard.pcf: line 28, column 91
    function screen_onEnter_3 (def :  pcf.NewChargeReversalAccountChargesScreen) : void {
      def.onEnter(reversal, accountID)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountNewChargeReversalWizard.pcf: line 33, column 91
    function screen_onEnter_5 (def :  pcf.NewChargeReversalConfirmationScreen) : void {
      def.onEnter(reversal)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountNewChargeReversalWizard.pcf: line 28, column 91
    function screen_refreshVariables_4 (def :  pcf.NewChargeReversalAccountChargesScreen) : void {
      def.refreshVariables(reversal, accountID)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountNewChargeReversalWizard.pcf: line 33, column 91
    function screen_refreshVariables_6 (def :  pcf.NewChargeReversalConfirmationScreen) : void {
      def.refreshVariables(reversal)
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewChargeReversalWizard) at AccountNewChargeReversalWizard.pcf: line 11, column 41
    function tabBar_onEnter_9 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewChargeReversalWizard) at AccountNewChargeReversalWizard.pcf: line 11, column 41
    function tabBar_refreshVariables_10 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AccountNewChargeReversalWizard {
      return super.CurrentLocation as pcf.AccountNewChargeReversalWizard
    }
    
    property get accountID () : gw.pl.persistence.core.Key {
      return getVariableValue("accountID", 0) as gw.pl.persistence.core.Key
    }
    
    property set accountID ($arg :  gw.pl.persistence.core.Key) {
      setVariableValue("accountID", 0, $arg)
    }
    
    property get reversal () : ChargeReversal {
      return getVariableValue("reversal", 0) as ChargeReversal
    }
    
    property set reversal ($arg :  ChargeReversal) {
      setVariableValue("reversal", 0, $arg)
    }
    
    
  }
  
  
}