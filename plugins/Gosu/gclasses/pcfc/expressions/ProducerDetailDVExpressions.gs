package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on RangeInput (id=SecurityZone_Input) at ProducerDetailDV.pcf: line 71, column 42
    function action_18 () : void {
      SecurityZoneDetail.go(producer.SecurityZone)
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at ProducerDetailDV.pcf: line 181, column 46
    function action_79 () : void {
      NewPaymentInstrumentPopup.push(gw.payment.PaymentInstrumentFilters.producerDetailsPaymentInstrumentOptions,producer, false)
    }
    
    // 'action' attribute on RangeInput (id=AgencyBillPlan_Input) at ProducerDetailDV.pcf: line 197, column 44
    function action_89 () : void {
      AgencyBillPlanDetailPopup.push(producer.AgencyBillPlan)
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountRep_Input) at ProducerDetailDV.pcf: line 206, column 39
    function action_98 () : void {
      UserSearchPopup.push()
    }
    
    // 'action' attribute on RangeInput (id=SecurityZone_Input) at ProducerDetailDV.pcf: line 71, column 42
    function action_dest_19 () : pcf.api.Destination {
      return pcf.SecurityZoneDetail.createDestination(producer.SecurityZone)
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at ProducerDetailDV.pcf: line 181, column 46
    function action_dest_80 () : pcf.api.Destination {
      return pcf.NewPaymentInstrumentPopup.createDestination(gw.payment.PaymentInstrumentFilters.producerDetailsPaymentInstrumentOptions,producer, false)
    }
    
    // 'action' attribute on RangeInput (id=AgencyBillPlan_Input) at ProducerDetailDV.pcf: line 197, column 44
    function action_dest_90 () : pcf.api.Destination {
      return pcf.AgencyBillPlanDetailPopup.createDestination(producer.AgencyBillPlan)
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountRep_Input) at ProducerDetailDV.pcf: line 206, column 39
    function action_dest_99 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'conversionExpression' attribute on PickerInput (id=AccountRep_Input) at ProducerDetailDV.pcf: line 206, column 39
    function conversionExpression_101 (PickedValue :  User) : java.lang.String {
      return PickedValue.DisplayName
    }
    
    // 'def' attribute on InputSetRef (id=HoldStatementInputSet) at ProducerDetailDV.pcf: line 171, column 37
    function def_onEnter_77 (def :  pcf.ProducerHoldStatementInputSet) : void {
      def.onEnter(producer)
    }
    
    // 'def' attribute on InputSetRef (id=HoldStatementInputSet) at ProducerDetailDV.pcf: line 171, column 37
    function def_refreshVariables_78 (def :  pcf.ProducerHoldStatementInputSet) : void {
      def.refreshVariables(producer)
    }
    
    // 'value' attribute on PickerInput (id=AccountRep_Input) at ProducerDetailDV.pcf: line 206, column 39
    function defaultSetter_103 (__VALUE_TO_SET :  java.lang.Object) : void {
      accountRepDisplayName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Tier_Input) at ProducerDetailDV.pcf: line 55, column 42
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      producer.Tier = (__VALUE_TO_SET as typekey.ProducerTier)
    }
    
    // 'value' attribute on RangeInput (id=SecurityZone_Input) at ProducerDetailDV.pcf: line 71, column 42
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      producer.SecurityZone = (__VALUE_TO_SET as entity.SecurityZone)
    }
    
    // 'value' attribute on TextInput (id=CommissionDayOfMonth_Input) at ProducerDetailDV.pcf: line 161, column 40
    function defaultSetter_70 (__VALUE_TO_SET :  java.lang.Object) : void {
      producer.RecurDayOfMonth = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TypeKeyInput (id=Periodicity_Input) at ProducerDetailDV.pcf: line 167, column 42
    function defaultSetter_74 (__VALUE_TO_SET :  java.lang.Object) : void {
      producer.RecurPeriodicity = (__VALUE_TO_SET as typekey.Periodicity)
    }
    
    // 'value' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at ProducerDetailDV.pcf: line 181, column 46
    function defaultSetter_83 (__VALUE_TO_SET :  java.lang.Object) : void {
      producer.DefaultPaymentInstrument = (__VALUE_TO_SET as entity.PaymentInstrument)
    }
    
    // 'value' attribute on RangeInput (id=AgencyBillPlan_Input) at ProducerDetailDV.pcf: line 197, column 44
    function defaultSetter_92 (__VALUE_TO_SET :  java.lang.Object) : void {
      producer.AgencyBillPlan = (__VALUE_TO_SET as entity.AgencyBillPlan)
    }
    
    // 'editable' attribute on TypeKeyInput (id=Tier_Input) at ProducerDetailDV.pcf: line 55, column 42
    function editable_9 () : java.lang.Boolean {
      return perm.Producer.prodtieredit
    }
    
    // 'initialValue' attribute on Variable at ProducerDetailDV.pcf: line 14, column 38
    function initialValue_0 () : entity.ProducerContact {
      return producer.PrimaryContact
    }
    
    // 'initialValue' attribute on Variable at ProducerDetailDV.pcf: line 19, column 30
    function initialValue_1 () : entity.Address {
      return primaryContact.Contact.PrimaryAddress
    }
    
    // 'initialValue' attribute on Variable at ProducerDetailDV.pcf: line 24, column 30
    function initialValue_2 () : java.util.Date {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'initialValue' attribute on Variable at ProducerDetailDV.pcf: line 28, column 22
    function initialValue_3 () : String {
      return producer.AccountRep.DisplayName
    }
    
    // 'initialValue' attribute on Variable at ProducerDetailDV.pcf: line 32, column 68
    function initialValue_4 () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return gw.api.database.Query.make(SecurityZone).select()
    }
    
    // 'initialValue' attribute on Variable at ProducerDetailDV.pcf: line 37, column 45
    function initialValue_5 () : gw.pl.currency.MonetaryAmount {
      return producer.RawCommissionPayable
    }
    
    // 'initialValue' attribute on Variable at ProducerDetailDV.pcf: line 41, column 49
    function initialValue_6 () : gw.payment.PaymentInstrumentRange {
      return new gw.payment.PaymentInstrumentRange(producer.PaymentInstruments)
    }
    
    // 'onPick' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at ProducerDetailDV.pcf: line 181, column 46
    function onPick_81 (PickedValue :  PaymentInstrument) : void {
      paymentInstrumentRange.addPaymentInstrument(producer.DefaultPaymentInstrument)
    }
    
    // 'requestValidationExpression' attribute on TextInput (id=CommissionDayOfMonth_Input) at ProducerDetailDV.pcf: line 161, column 40
    function requestValidationExpression_68 (VALUE :  java.lang.Integer) : java.lang.Object {
      return VALUE != null and VALUE > 0 and VALUE <= 31 ? null : DisplayKey.get("Java.Account.InvoiceDayOfMonth.ValidationError")
    }
    
    // 'validationExpression' attribute on PickerInput (id=AccountRep_Input) at ProducerDetailDV.pcf: line 206, column 39
    function validationExpression_100 () : java.lang.Object {
      return validateAndSetAccountRep()
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at ProducerDetailDV.pcf: line 71, column 42
    function valueRange_23 () : java.lang.Object {
      return allSecurityZones
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at ProducerDetailDV.pcf: line 181, column 46
    function valueRange_85 () : java.lang.Object {
      return gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange,gw.payment.PaymentInstrumentFilters.producerDetailsPaymentInstrumentFilter)
    }
    
    // 'valueRange' attribute on RangeInput (id=AgencyBillPlan_Input) at ProducerDetailDV.pcf: line 197, column 44
    function valueRange_94 () : java.lang.Object {
      return Plan.finder.findAllAvailablePlans<AgencyBillPlan>(AgencyBillPlan, producer.Currency)
    }
    
    // 'value' attribute on TypeKeyInput (id=Tier_Input) at ProducerDetailDV.pcf: line 55, column 42
    function valueRoot_12 () : java.lang.Object {
      return producer
    }
    
    // 'value' attribute on TextInput (id=PrimaryContact_Input) at ProducerDetailDV.pcf: line 78, column 37
    function valueRoot_28 () : java.lang.Object {
      return primaryContact
    }
    
    // 'value' attribute on TextInput (id=Email_Input) at ProducerDetailDV.pcf: line 86, column 55
    function valueRoot_33 () : java.lang.Object {
      return primaryContact.Contact
    }
    
    // 'value' attribute on TypeKeyInput (id=Tier_Input) at ProducerDetailDV.pcf: line 55, column 42
    function value_10 () : typekey.ProducerTier {
      return producer.Tier
    }
    
    // 'value' attribute on PickerInput (id=AccountRep_Input) at ProducerDetailDV.pcf: line 206, column 39
    function value_102 () : java.lang.String {
      return accountRepDisplayName
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at ProducerDetailDV.pcf: line 63, column 39
    function value_15 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'value' attribute on RangeInput (id=SecurityZone_Input) at ProducerDetailDV.pcf: line 71, column 42
    function value_20 () : entity.SecurityZone {
      return producer.SecurityZone
    }
    
    // 'value' attribute on TextInput (id=PrimaryContact_Input) at ProducerDetailDV.pcf: line 78, column 37
    function value_27 () : entity.Contact {
      return primaryContact.Contact
    }
    
    // 'value' attribute on TextInput (id=Address_Input) at ProducerDetailDV.pcf: line 82, column 95
    function value_30 () : java.lang.String {
      return new gw.api.address.AddressFormatter().format(primaryAddress, "\n")
    }
    
    // 'value' attribute on TextInput (id=Email_Input) at ProducerDetailDV.pcf: line 86, column 55
    function value_32 () : java.lang.String {
      return primaryContact.Contact.EmailAddress1
    }
    
    // 'value' attribute on TextInput (id=Phone_Input) at ProducerDetailDV.pcf: line 90, column 56
    function value_35 () : java.lang.String {
      return primaryContact.Contact.WorkPhoneValue
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CommissionPaidYearToDate_Input) at ProducerDetailDV.pcf: line 101, column 149
    function value_38 () : gw.pl.currency.MonetaryAmount {
      return producer.getCommissionPaid(gw.api.upgrade.Coercions.makeDateFrom("01/01/" + gw.api.util.DateUtil.getYear(today)), today)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CommissionPayable_Input) at ProducerDetailDV.pcf: line 108, column 39
    function value_41 () : gw.pl.currency.MonetaryAmount {
      return rawCommissionPayable
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CommissionReserve_Input) at ProducerDetailDV.pcf: line 115, column 50
    function value_44 () : gw.pl.currency.MonetaryAmount {
      return producer.TotalCommissionReserve
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CommissionExpense_Input) at ProducerDetailDV.pcf: line 122, column 50
    function value_48 () : gw.pl.currency.MonetaryAmount {
      return producer.TotalCommissionExpense
    }
    
    // 'value' attribute on MonetaryAmountInput (id=AdvanceBalance_Input) at ProducerDetailDV.pcf: line 129, column 42
    function value_52 () : gw.pl.currency.MonetaryAmount {
      return producer.AdvanceBalance
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnappliedAmount_Input) at ProducerDetailDV.pcf: line 136, column 43
    function value_56 () : gw.pl.currency.MonetaryAmount {
      return producer.UnappliedAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=SuspenseAmount_Input) at ProducerDetailDV.pcf: line 143, column 42
    function value_60 () : gw.pl.currency.MonetaryAmount {
      return producer.SuspenseAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=WriteoffExpense_Input) at ProducerDetailDV.pcf: line 150, column 50
    function value_64 () : gw.pl.currency.MonetaryAmount {
      return producer.WriteoffExpenseBalance
    }
    
    // 'value' attribute on TextInput (id=CommissionDayOfMonth_Input) at ProducerDetailDV.pcf: line 161, column 40
    function value_69 () : java.lang.Integer {
      return producer.RecurDayOfMonth
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at ProducerDetailDV.pcf: line 49, column 38
    function value_7 () : entity.Producer {
      return producer
    }
    
    // 'value' attribute on TypeKeyInput (id=Periodicity_Input) at ProducerDetailDV.pcf: line 167, column 42
    function value_73 () : typekey.Periodicity {
      return producer.RecurPeriodicity
    }
    
    // 'value' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at ProducerDetailDV.pcf: line 181, column 46
    function value_82 () : entity.PaymentInstrument {
      return producer.DefaultPaymentInstrument
    }
    
    // 'value' attribute on RangeInput (id=AgencyBillPlan_Input) at ProducerDetailDV.pcf: line 197, column 44
    function value_91 () : entity.AgencyBillPlan {
      return producer.AgencyBillPlan
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at ProducerDetailDV.pcf: line 71, column 42
    function verifyValueRangeIsAllowedType_24 ($$arg :  entity.SecurityZone[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at ProducerDetailDV.pcf: line 71, column 42
    function verifyValueRangeIsAllowedType_24 ($$arg :  gw.api.database.IQueryBeanResult<entity.SecurityZone>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at ProducerDetailDV.pcf: line 71, column 42
    function verifyValueRangeIsAllowedType_24 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at ProducerDetailDV.pcf: line 181, column 46
    function verifyValueRangeIsAllowedType_86 ($$arg :  entity.PaymentInstrument[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at ProducerDetailDV.pcf: line 181, column 46
    function verifyValueRangeIsAllowedType_86 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentInstrument>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at ProducerDetailDV.pcf: line 181, column 46
    function verifyValueRangeIsAllowedType_86 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AgencyBillPlan_Input) at ProducerDetailDV.pcf: line 197, column 44
    function verifyValueRangeIsAllowedType_95 ($$arg :  entity.AgencyBillPlan[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AgencyBillPlan_Input) at ProducerDetailDV.pcf: line 197, column 44
    function verifyValueRangeIsAllowedType_95 ($$arg :  gw.api.database.IQueryBeanResult<entity.AgencyBillPlan>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AgencyBillPlan_Input) at ProducerDetailDV.pcf: line 197, column 44
    function verifyValueRangeIsAllowedType_95 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at ProducerDetailDV.pcf: line 71, column 42
    function verifyValueRange_25 () : void {
      var __valueRangeArg = allSecurityZones
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_24(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=DefaultPaymentInstrument_Input) at ProducerDetailDV.pcf: line 181, column 46
    function verifyValueRange_87 () : void {
      var __valueRangeArg = gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange,gw.payment.PaymentInstrumentFilters.producerDetailsPaymentInstrumentFilter)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_86(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=AgencyBillPlan_Input) at ProducerDetailDV.pcf: line 197, column 44
    function verifyValueRange_96 () : void {
      var __valueRangeArg = Plan.finder.findAllAvailablePlans<AgencyBillPlan>(AgencyBillPlan, producer.Currency)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_95(__valueRangeArg)
    }
    
    property get accountRepDisplayName () : String {
      return getVariableValue("accountRepDisplayName", 0) as String
    }
    
    property set accountRepDisplayName ($arg :  String) {
      setVariableValue("accountRepDisplayName", 0, $arg)
    }
    
    property get allSecurityZones () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return getVariableValue("allSecurityZones", 0) as gw.api.database.IQueryBeanResult<SecurityZone>
    }
    
    property set allSecurityZones ($arg :  gw.api.database.IQueryBeanResult<SecurityZone>) {
      setVariableValue("allSecurityZones", 0, $arg)
    }
    
    property get paymentInstrumentRange () : gw.payment.PaymentInstrumentRange {
      return getVariableValue("paymentInstrumentRange", 0) as gw.payment.PaymentInstrumentRange
    }
    
    property set paymentInstrumentRange ($arg :  gw.payment.PaymentInstrumentRange) {
      setVariableValue("paymentInstrumentRange", 0, $arg)
    }
    
    property get primaryAddress () : entity.Address {
      return getVariableValue("primaryAddress", 0) as entity.Address
    }
    
    property set primaryAddress ($arg :  entity.Address) {
      setVariableValue("primaryAddress", 0, $arg)
    }
    
    property get primaryContact () : entity.ProducerContact {
      return getVariableValue("primaryContact", 0) as entity.ProducerContact
    }
    
    property set primaryContact ($arg :  entity.ProducerContact) {
      setVariableValue("primaryContact", 0, $arg)
    }
    
    property get producer () : Producer {
      return getRequireValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setRequireValue("producer", 0, $arg)
    }
    
    property get rawCommissionPayable () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("rawCommissionPayable", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set rawCommissionPayable ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("rawCommissionPayable", 0, $arg)
    }
    
    property get today () : java.util.Date {
      return getVariableValue("today", 0) as java.util.Date
    }
    
    property set today ($arg :  java.util.Date) {
      setVariableValue("today", 0, $arg)
    }
    
    function validateAndSetAccountRep() : String {
      if (accountRepDisplayName == null) {
        producer.AccountRep = null
        return null
      }
      
      var allUsers = gw.api.database.Query.make(User).select().toList()
      var user = allUsers.firstWhere( \ user -> user.DisplayName == accountRepDisplayName )
      if (user == null) {
        return DisplayKey.get("Web.ProducerDetail.InvalidAccountRep")  
      } else {
        producer.AccountRep = user
        return null
      }
    }
    
    
  }
  
  
}