package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountsMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountsMenuActionsExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountsMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountsMenuActionsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=AccountsMenuActions_NewAccount) at AccountsMenuActions.pcf: line 12, column 37
    function action_6 () : void {
      NewAccount.go(gw.api.util.CurrencyUtil.getDefaultCurrency())
    }
    
    // 'action' attribute on MenuItem (id=AccountsMenuActions_NewAccount) at AccountsMenuActions.pcf: line 12, column 37
    function action_dest_7 () : pcf.api.Destination {
      return pcf.NewAccount.createDestination(gw.api.util.CurrencyUtil.getDefaultCurrency())
    }
    
    // 'value' attribute on MenuItemIterator at AccountsMenuActions.pcf: line 17, column 66
    function value_4 () : java.util.List<typekey.Currency> {
      return Currency.getTypeKeys(false)
    }
    
    // 'visible' attribute on MenuItemIterator at AccountsMenuActions.pcf: line 17, column 66
    function visible_0 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on MenuItem (id=AccountsMenuActions_NewAccount) at AccountsMenuActions.pcf: line 12, column 37
    function visible_5 () : java.lang.Boolean {
      return perm.Account.create
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountsMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountsMenuActionsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=CurrencyItem) at AccountsMenuActions.pcf: line 21, column 29
    function action_1 () : void {
      NewAccount.go(currency)
    }
    
    // 'action' attribute on MenuItem (id=CurrencyItem) at AccountsMenuActions.pcf: line 21, column 29
    function action_dest_2 () : pcf.api.Destination {
      return pcf.NewAccount.createDestination(currency)
    }
    
    // 'label' attribute on MenuItem (id=CurrencyItem) at AccountsMenuActions.pcf: line 21, column 29
    function label_3 () : java.lang.Object {
      return currency
    }
    
    property get currency () : typekey.Currency {
      return getIteratedValue(1) as typekey.Currency
    }
    
    
  }
  
  
}