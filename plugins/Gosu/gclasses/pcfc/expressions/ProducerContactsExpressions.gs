package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerContacts.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerContactsExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerContacts.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailPanelExpressionsImpl extends ProducerContactsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at ProducerContacts.pcf: line 90, column 33
    function def_onEnter_12 (def :  pcf.ProducerContactDetailDV) : void {
      def.onEnter(contact, false)
    }
    
    // 'def' attribute on PanelRef at ProducerContacts.pcf: line 90, column 33
    function def_refreshVariables_13 (def :  pcf.ProducerContactDetailDV) : void {
      def.refreshVariables(contact, false)
    }
    
    // 'value' attribute on TextCell (id=ContactName_Cell) at ProducerContacts.pcf: line 70, column 53
    function sortValue_0 (producerContact :  entity.ProducerContact) : java.lang.Object {
      return producerContact
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at ProducerContacts.pcf: line 75, column 45
    function sortValue_1 (producerContact :  entity.ProducerContact) : java.lang.Object {
      return producerContact.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=ContactRoles_Cell) at ProducerContacts.pcf: line 80, column 31
    function sortValue_2 (producerContact :  entity.ProducerContact) : java.lang.Object {
      return gw.api.web.producer.ProducerUtil.getRolesForDisplay(producerContact)
    }
    
    // 'title' attribute on Card (id=ContactDetailCard) at ProducerContacts.pcf: line 87, column 199
    function title_14 () : java.lang.String {
      return DisplayKey.get("Web.ProducerContacts.ContactDetail", contact.Contact.Name != null ? contact.Contact.Name : DisplayKey.get("Web.ProducerContacts.NewContact"))
    }
    
    // 'toRemove' attribute on RowIterator (id=producerContactIterator) at ProducerContacts.pcf: line 63, column 50
    function toRemove_10 (producerContact :  entity.ProducerContact) : void {
      producer.removeFromContacts(producerContact)
    }
    
    // 'value' attribute on RowIterator (id=producerContactIterator) at ProducerContacts.pcf: line 63, column 50
    function value_11 () : entity.ProducerContact[] {
      return producer.Contacts
    }
    
    property get contact () : ProducerContact {
      return getCurrentSelection(1) as ProducerContact
    }
    
    property set contact ($arg :  ProducerContact) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerContacts.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at ProducerContacts.pcf: line 75, column 45
    function valueRoot_6 () : java.lang.Object {
      return producerContact.Contact
    }
    
    // 'value' attribute on TextCell (id=ContactName_Cell) at ProducerContacts.pcf: line 70, column 53
    function value_3 () : entity.ProducerContact {
      return producerContact
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at ProducerContacts.pcf: line 75, column 45
    function value_5 () : entity.Address {
      return producerContact.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=ContactRoles_Cell) at ProducerContacts.pcf: line 80, column 31
    function value_8 () : java.lang.String {
      return gw.api.web.producer.ProducerUtil.getRolesForDisplay(producerContact)
    }
    
    property get producerContact () : entity.ProducerContact {
      return getIteratedValue(2) as entity.ProducerContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerContacts.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerContactsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=ProducerContacts) at ProducerContacts.pcf: line 10, column 68
    function canEdit_15 () : java.lang.Boolean {
      return perm.ProducerContact.edit
    }
    
    // 'canVisit' attribute on Page (id=ProducerContacts) at ProducerContacts.pcf: line 10, column 68
    static function canVisit_16 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodcontview
    }
    
    // Page (id=ProducerContacts) at ProducerContacts.pcf: line 10, column 68
    static function parent_17 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    override property get CurrentLocation () : pcf.ProducerContacts {
      return super.CurrentLocation as pcf.ProducerContacts
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}