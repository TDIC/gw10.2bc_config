package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/TroubleTicketSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TroubleTicketSearchExpressions {
  @javax.annotation.Generated("config/web/pcf/search/TroubleTicketSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TroubleTicketSearchExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=TroubleTicketSearch) at TroubleTicketSearch.pcf: line 8, column 73
    static function canVisit_2 () : java.lang.Boolean {
      return perm.System.viewsearch and perm.System.ttktsearch
    }
    
    // 'def' attribute on ScreenRef at TroubleTicketSearch.pcf: line 10, column 42
    function def_onEnter_0 (def :  pcf.TroubleTicketSearchScreen) : void {
      def.onEnter()
    }
    
    // 'def' attribute on ScreenRef at TroubleTicketSearch.pcf: line 10, column 42
    function def_refreshVariables_1 (def :  pcf.TroubleTicketSearchScreen) : void {
      def.refreshVariables()
    }
    
    // Page (id=TroubleTicketSearch) at TroubleTicketSearch.pcf: line 8, column 73
    static function parent_3 () : pcf.api.Destination {
      return pcf.SearchGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.TroubleTicketSearch {
      return super.CurrentLocation as pcf.TroubleTicketSearch
    }
    
    
  }
  
  
}