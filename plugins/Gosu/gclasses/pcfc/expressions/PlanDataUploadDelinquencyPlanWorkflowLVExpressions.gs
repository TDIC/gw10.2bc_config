package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadDelinquencyPlanWorkflowLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanDataUploadDelinquencyPlanWorkflowLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadDelinquencyPlanWorkflowLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PlanDataUploadDelinquencyPlanWorkflowLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 17, column 126
    function highlighted_64 () : java.lang.Boolean {
      return (delinquencyPlanWorkflow.Error or delinquencyPlanWorkflow.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 23, column 46
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 29, column 41
    function label_24 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflow.Name")
    }
    
    // 'label' attribute on TypeKeyCell (id=delinquencyReason_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 34, column 50
    function label_29 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflow.DelinquencyReason")
    }
    
    // 'label' attribute on TextCell (id=workflowType_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 39, column 41
    function label_34 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflow.WorkflowType")
    }
    
    // 'label' attribute on TypeKeyCell (id=eventName_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 44, column 53
    function label_39 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflow.EventName")
    }
    
    // 'label' attribute on TypeKeyCell (id=eventTrigger_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 49, column 56
    function label_44 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflow.EventTrigger")
    }
    
    // 'label' attribute on TextCell (id=eventOffset_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 54, column 42
    function label_49 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflow.EventOffset")
    }
    
    // 'label' attribute on TextCell (id=eventRelativeOrder_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 59, column 42
    function label_54 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflow.EventRelativeOrder")
    }
    
    // 'label' attribute on BooleanRadioCell (id=eventAutomatic_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 64, column 42
    function label_59 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflow.EventAutomatic")
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 29, column 41
    function valueRoot_26 () : java.lang.Object {
      return delinquencyPlanWorkflow
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 23, column 46
    function value_20 () : java.lang.String {
      return processor.getLoadStatus(delinquencyPlanWorkflow)
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 29, column 41
    function value_25 () : java.lang.String {
      return delinquencyPlanWorkflow.DelinquencyPlanName
    }
    
    // 'value' attribute on TypeKeyCell (id=delinquencyReason_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 34, column 50
    function value_30 () : typekey.DelinquencyReason {
      return delinquencyPlanWorkflow.DelinquencyReason
    }
    
    // 'value' attribute on TextCell (id=workflowType_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 39, column 41
    function value_35 () : java.lang.String {
      return delinquencyPlanWorkflow.WorkflowType
    }
    
    // 'value' attribute on TypeKeyCell (id=eventName_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 44, column 53
    function value_40 () : typekey.DelinquencyEventName {
      return delinquencyPlanWorkflow.EventName
    }
    
    // 'value' attribute on TypeKeyCell (id=eventTrigger_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 49, column 56
    function value_45 () : typekey.DelinquencyTriggerBasis {
      return delinquencyPlanWorkflow.EventTrigger
    }
    
    // 'value' attribute on TextCell (id=eventOffset_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 54, column 42
    function value_50 () : java.lang.Integer {
      return delinquencyPlanWorkflow.EventOffset
    }
    
    // 'value' attribute on TextCell (id=eventRelativeOrder_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 59, column 42
    function value_55 () : java.lang.Integer {
      return delinquencyPlanWorkflow.EventRelativeOrder
    }
    
    // 'value' attribute on BooleanRadioCell (id=eventAutomatic_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 64, column 42
    function value_60 () : java.lang.Boolean {
      return delinquencyPlanWorkflow.EventAutomatic
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 23, column 46
    function visible_21 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get delinquencyPlanWorkflow () : tdic.util.dataloader.data.plandata.DelinquencyPlanWorkflowData {
      return getIteratedValue(1) as tdic.util.dataloader.data.plandata.DelinquencyPlanWorkflowData
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadDelinquencyPlanWorkflowLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanDataUploadDelinquencyPlanWorkflowLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TypeKeyCell (id=eventTrigger_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 49, column 56
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflow.EventTrigger")
    }
    
    // 'label' attribute on TextCell (id=eventOffset_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 54, column 42
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflow.EventOffset")
    }
    
    // 'label' attribute on TextCell (id=eventRelativeOrder_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 59, column 42
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflow.EventRelativeOrder")
    }
    
    // 'label' attribute on BooleanRadioCell (id=eventAutomatic_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 64, column 42
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflow.EventAutomatic")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflow.Name")
    }
    
    // 'label' attribute on TypeKeyCell (id=delinquencyReason_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 34, column 50
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflow.DelinquencyReason")
    }
    
    // 'label' attribute on TextCell (id=workflowType_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 39, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflow.WorkflowType")
    }
    
    // 'label' attribute on TypeKeyCell (id=eventName_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 44, column 53
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflow.EventName")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 23, column 46
    function sortValue_1 (delinquencyPlanWorkflow :  tdic.util.dataloader.data.plandata.DelinquencyPlanWorkflowData) : java.lang.Object {
      return processor.getLoadStatus(delinquencyPlanWorkflow)
    }
    
    // 'value' attribute on TypeKeyCell (id=eventName_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 44, column 53
    function sortValue_10 (delinquencyPlanWorkflow :  tdic.util.dataloader.data.plandata.DelinquencyPlanWorkflowData) : java.lang.Object {
      return delinquencyPlanWorkflow.EventName
    }
    
    // 'value' attribute on TypeKeyCell (id=eventTrigger_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 49, column 56
    function sortValue_12 (delinquencyPlanWorkflow :  tdic.util.dataloader.data.plandata.DelinquencyPlanWorkflowData) : java.lang.Object {
      return delinquencyPlanWorkflow.EventTrigger
    }
    
    // 'value' attribute on TextCell (id=eventOffset_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 54, column 42
    function sortValue_14 (delinquencyPlanWorkflow :  tdic.util.dataloader.data.plandata.DelinquencyPlanWorkflowData) : java.lang.Object {
      return delinquencyPlanWorkflow.EventOffset
    }
    
    // 'value' attribute on TextCell (id=eventRelativeOrder_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 59, column 42
    function sortValue_16 (delinquencyPlanWorkflow :  tdic.util.dataloader.data.plandata.DelinquencyPlanWorkflowData) : java.lang.Object {
      return delinquencyPlanWorkflow.EventRelativeOrder
    }
    
    // 'value' attribute on BooleanRadioCell (id=eventAutomatic_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 64, column 42
    function sortValue_18 (delinquencyPlanWorkflow :  tdic.util.dataloader.data.plandata.DelinquencyPlanWorkflowData) : java.lang.Object {
      return delinquencyPlanWorkflow.EventAutomatic
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 29, column 41
    function sortValue_4 (delinquencyPlanWorkflow :  tdic.util.dataloader.data.plandata.DelinquencyPlanWorkflowData) : java.lang.Object {
      return delinquencyPlanWorkflow.DelinquencyPlanName
    }
    
    // 'value' attribute on TypeKeyCell (id=delinquencyReason_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 34, column 50
    function sortValue_6 (delinquencyPlanWorkflow :  tdic.util.dataloader.data.plandata.DelinquencyPlanWorkflowData) : java.lang.Object {
      return delinquencyPlanWorkflow.DelinquencyReason
    }
    
    // 'value' attribute on TextCell (id=workflowType_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 39, column 41
    function sortValue_8 (delinquencyPlanWorkflow :  tdic.util.dataloader.data.plandata.DelinquencyPlanWorkflowData) : java.lang.Object {
      return delinquencyPlanWorkflow.WorkflowType
    }
    
    // 'value' attribute on RowIterator (id=DelinquencyPlanWorkflow) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 15, column 109
    function value_65 () : java.util.ArrayList<tdic.util.dataloader.data.plandata.DelinquencyPlanWorkflowData> {
      return processor.DelinquencyPlanWorkflowArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadDelinquencyPlanWorkflowLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}