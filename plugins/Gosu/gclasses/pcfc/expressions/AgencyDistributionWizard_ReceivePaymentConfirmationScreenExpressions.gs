package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_ReceivePaymentConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyDistributionWizard_ReceivePaymentConfirmationScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_ReceivePaymentConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyDistributionWizard_ReceivePaymentConfirmationScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyDistributionWizard_ReceivePaymentConfirmationScreen.pcf: line 32, column 33
    function currency_8 () : typekey.Currency {
      return money.Currency
    }
    
    // 'value' attribute on TextInput (id=PaymentInstrument_Input) at AgencyDistributionWizard_ReceivePaymentConfirmationScreen.pcf: line 22, column 49
    function valueRoot_1 () : java.lang.Object {
      return money
    }
    
    // 'value' attribute on TextInput (id=PaymentInstrument_Input) at AgencyDistributionWizard_ReceivePaymentConfirmationScreen.pcf: line 22, column 49
    function value_0 () : entity.PaymentInstrument {
      return money.PaymentInstrument
    }
    
    // 'value' attribute on TextInput (id=RefNumber_Input) at AgencyDistributionWizard_ReceivePaymentConfirmationScreen.pcf: line 26, column 36
    function value_3 () : java.lang.String {
      return money.RefNumber
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyDistributionWizard_ReceivePaymentConfirmationScreen.pcf: line 32, column 33
    function value_6 () : gw.pl.currency.MonetaryAmount {
      return money.Amount
    }
    
    property get money () : AgencyBillMoneyRcvd {
      return getRequireValue("money", 0) as AgencyBillMoneyRcvd
    }
    
    property set money ($arg :  AgencyBillMoneyRcvd) {
      setRequireValue("money", 0, $arg)
    }
    
    
  }
  
  
}