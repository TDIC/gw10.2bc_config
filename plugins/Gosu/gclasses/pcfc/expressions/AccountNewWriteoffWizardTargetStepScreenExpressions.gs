package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/AccountNewWriteoffWizardTargetStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountNewWriteoffWizardTargetStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/AccountNewWriteoffWizardTargetStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountNewWriteoffWizardTargetStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on TextCell (id=Number_Cell) at AccountNewWriteoffWizardTargetStepScreen.pcf: line 66, column 53
    function sortValue_4 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PolicyNumber
    }
    
    // 'sortBy' attribute on TextCell (id=Number_Cell) at AccountNewWriteoffWizardTargetStepScreen.pcf: line 66, column 53
    function sortValue_5 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return  policyPeriod.TermNumber
    }
    
    // 'value' attribute on DateCell (id=PolicyPerEffDate_Cell) at AccountNewWriteoffWizardTargetStepScreen.pcf: line 70, column 58
    function sortValue_6 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=PolicyPerExpirDate_Cell) at AccountNewWriteoffWizardTargetStepScreen.pcf: line 74, column 60
    function sortValue_7 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on TextInput (id=SourceAccount_Input) at AccountNewWriteoffWizardTargetStepScreen.pcf: line 24, column 69
    function value_0 () : gw.api.web.accounting.TAccountOwnerReference {
      return targetOfWriteoff.TAccountOwner typeis Account ? targetOfWriteoff : null
    }
    
    // 'value' attribute on RowIterator at AccountNewWriteoffWizardTargetStepScreen.pcf: line 49, column 49
    function value_19 () : entity.PolicyPeriod[] {
      return account.OpenPolicyPeriods
    }
    
    // 'value' attribute on TextInput (id=SourcePolicy_Input) at AccountNewWriteoffWizardTargetStepScreen.pcf: line 30, column 69
    function value_2 () : gw.api.web.accounting.TAccountOwnerReference {
      return targetOfWriteoff.TAccountOwner typeis PolicyPeriod ? targetOfWriteoff : null
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get targetOfWriteoff () : gw.api.web.accounting.TAccountOwnerReference {
      return getRequireValue("targetOfWriteoff", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set targetOfWriteoff ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setRequireValue("targetOfWriteoff", 0, $arg)
    }
    
    function setAccountAsTargetOfWriteoff() {
      targetOfWriteoff.TAccountOwner = account
    }
    
    function setPolicyAsTargetOfWriteoff(policyPeriod : PolicyPeriod) {
      targetOfWriteoff.TAccountOwner = policyPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/AccountNewWriteoffWizardTargetStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountNewWriteoffWizardTargetStepScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=Select) at AccountNewWriteoffWizardTargetStepScreen.pcf: line 59, column 46
    function action_9 () : void {
      setPolicyAsTargetOfWriteoff( policyPeriod )
    }
    
    // 'available' attribute on Link (id=Select) at AccountNewWriteoffWizardTargetStepScreen.pcf: line 59, column 46
    function available_8 () : java.lang.Boolean {
      return perm.Transaction.plcywo
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at AccountNewWriteoffWizardTargetStepScreen.pcf: line 66, column 53
    function valueRoot_11 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at AccountNewWriteoffWizardTargetStepScreen.pcf: line 66, column 53
    function value_10 () : java.lang.String {
      return policyPeriod.DisplayName
    }
    
    // 'value' attribute on DateCell (id=PolicyPerEffDate_Cell) at AccountNewWriteoffWizardTargetStepScreen.pcf: line 70, column 58
    function value_13 () : java.util.Date {
      return policyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=PolicyPerExpirDate_Cell) at AccountNewWriteoffWizardTargetStepScreen.pcf: line 74, column 60
    function value_16 () : java.util.Date {
      return policyPeriod.PolicyPerExpirDate
    }
    
    property get policyPeriod () : entity.PolicyPeriod {
      return getIteratedValue(1) as entity.PolicyPeriod
    }
    
    
  }
  
  
}