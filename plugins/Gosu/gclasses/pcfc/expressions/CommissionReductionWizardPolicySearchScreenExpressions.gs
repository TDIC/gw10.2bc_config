package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/CommissionReductionWizardPolicySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionReductionWizardPolicySearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/CommissionReductionWizardPolicySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionReductionWizardPolicySearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get tAccountOwnerReference () : gw.api.web.accounting.TAccountOwnerReference {
      return getRequireValue("tAccountOwnerReference", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set tAccountOwnerReference ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setRequireValue("tAccountOwnerReference", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/CommissionReductionWizardPolicySearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends CommissionReductionWizardPolicySearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at CommissionReductionWizardPolicySearchScreen.pcf: line 25, column 57
    function def_onEnter_0 (def :  pcf.PolicySearchDV) : void {
      def.onEnter(searchCriteria, false)
    }
    
    // 'def' attribute on PanelRef at CommissionReductionWizardPolicySearchScreen.pcf: line 31, column 104
    function def_onEnter_2 (def :  pcf.PolicySearchResultsLV) : void {
      def.onEnter(policySearchViews, tAccountOwnerReference, true, false, false)
    }
    
    // 'def' attribute on PanelRef at CommissionReductionWizardPolicySearchScreen.pcf: line 25, column 57
    function def_refreshVariables_1 (def :  pcf.PolicySearchDV) : void {
      def.refreshVariables(searchCriteria, false)
    }
    
    // 'def' attribute on PanelRef at CommissionReductionWizardPolicySearchScreen.pcf: line 31, column 104
    function def_refreshVariables_3 (def :  pcf.PolicySearchResultsLV) : void {
      def.refreshVariables(policySearchViews, tAccountOwnerReference, true, false, false)
    }
    
    // 'searchCriteria' attribute on SearchPanel at CommissionReductionWizardPolicySearchScreen.pcf: line 23, column 88
    function searchCriteria_5 () : gw.search.PolicySearchCriteria {
      return new gw.search.PolicySearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at CommissionReductionWizardPolicySearchScreen.pcf: line 23, column 88
    function search_4 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria, StringCriterionMode.TC_STARTSWITH)
    }
    
    property get policySearchViews () : gw.api.database.IQueryBeanResult<PolicySearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<PolicySearchView>
    }
    
    property get searchCriteria () : gw.search.PolicySearchCriteria {
      return getCriteriaValue(1) as gw.search.PolicySearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.PolicySearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}