package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseDocumentListPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OnBaseDocumentListPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseDocumentListPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends OnBaseDocumentListPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=UnityLink) at OnBaseDocumentListPopup.pcf: line 102, column 154
    function action_26 () : void {
      Document.viewOnBaseDocument()
    }
    
    // 'action' attribute on Link (id=WebLink) at OnBaseDocumentListPopup.pcf: line 111, column 152
    function action_31 () : void {
      Document.downloadContent()
    }
    
    // 'available' attribute on Link (id=UnityLink) at OnBaseDocumentListPopup.pcf: line 102, column 154
    function available_24 () : java.lang.Boolean {
      return documentsActionsHelper.isViewDocumentContentAvailable(Document as Document)
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Icon_Cell) at OnBaseDocumentListPopup.pcf: line 81, column 38
    function icon_20 () : java.lang.String {
      return Document.Icon
    }
    
    // 'label' attribute on Link (id=UnityLink) at OnBaseDocumentListPopup.pcf: line 102, column 154
    function label_27 () : java.lang.Object {
      return Document.Name
    }
    
    // 'tooltip' attribute on Link (id=UnityLink) at OnBaseDocumentListPopup.pcf: line 102, column 154
    function tooltip_28 () : java.lang.String {
      return documentsActionsHelper.getViewDocumentContentTooltip(Document as Document)
    }
    
    // 'value' attribute on TextCell (id=docId_Cell) at OnBaseDocumentListPopup.pcf: line 86, column 42
    function valueRoot_22 () : java.lang.Object {
      return Document
    }
    
    // 'value' attribute on TextCell (id=docId_Cell) at OnBaseDocumentListPopup.pcf: line 86, column 42
    function value_21 () : java.lang.String {
      return Document.DocUID
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at OnBaseDocumentListPopup.pcf: line 117, column 51
    function value_34 () : typekey.DocumentType {
      return Document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=Subtype_Cell) at OnBaseDocumentListPopup.pcf: line 122, column 64
    function value_37 () : typekey.OnBaseDocumentSubtype_Ext {
      return Document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at OnBaseDocumentListPopup.pcf: line 127, column 57
    function value_40 () : typekey.DocumentStatusType {
      return Document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at OnBaseDocumentListPopup.pcf: line 132, column 42
    function value_43 () : java.lang.String {
      return Document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at OnBaseDocumentListPopup.pcf: line 140, column 48
    function value_46 () : java.util.Date {
      return Document.DateModified
    }
    
    // 'visible' attribute on Link (id=UnityLink) at OnBaseDocumentListPopup.pcf: line 102, column 154
    function visible_25 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType  == acc.onbase.configuration.OnBaseClientType.Unity
    }
    
    // 'visible' attribute on Link (id=WebLink) at OnBaseDocumentListPopup.pcf: line 111, column 152
    function visible_30 () : java.lang.Boolean {
      return acc.onbase.configuration.OnBaseConfigurationFactory.Instance.ClientType  == acc.onbase.configuration.OnBaseClientType.Web
    }
    
    property get Document () : Document {
      return getIteratedValue(1) as Document
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/onbase/OnBaseDocumentListPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OnBaseDocumentListPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (Entity :  KeyableBean, LinkType :  acc.onbase.configuration.DocumentLinkType, EntityDescription :  java.lang.String, Beans :  KeyableBean[]) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=OnBaseDocumentListPopup_LinkDocumentButton) at OnBaseDocumentListPopup.pcf: line 59, column 108
    function action_10 () : void {
      OnBasePickExistingDocumentPopup.push(Entity, LinkType, Beans)
    }
    
    // 'action' attribute on ToolbarButton (id=OnBaseDocumentListPopup_LinkDocumentButton) at OnBaseDocumentListPopup.pcf: line 59, column 108
    function action_dest_11 () : pcf.api.Destination {
      return pcf.OnBasePickExistingDocumentPopup.createDestination(Entity, LinkType, Beans)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=OnBaseDocumentListPopup_UnlinkDocumentButton) at OnBaseDocumentListPopup.pcf: line 64, column 110
    function allCheckedRowsAction_12 (CheckedValues :  Document[], CheckedValuesErrors :  java.util.Map) : void {
      DocumentLinking.unlinkDocumentsFromEntity(Entity, CheckedValues , LinkType);
    }
    
    // 'initialValue' attribute on Variable at OnBaseDocumentListPopup.pcf: line 19, column 52
    function initialValue_0 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // 'initialValue' attribute on Variable at OnBaseDocumentListPopup.pcf: line 23, column 58
    function initialValue_1 () : acc.onbase.api.application.DocumentLinking {
      if(Entity.isNew()){return new acc.onbase.api.application.DocumentLinkingNewEntity()} else{return new acc.onbase.api.application.DocumentLinking()}
    }
    
    // 'initialValue' attribute on Variable at OnBaseDocumentListPopup.pcf: line 31, column 43
    function initialValue_2 () : List<entity.Document> {
      return DocumentLinking.getDocumentsLinkedToEntity(Entity, LinkType)
    }
    
    // 'value' attribute on TextCell (id=docId_Cell) at OnBaseDocumentListPopup.pcf: line 86, column 42
    function sortValue_13 (Document :  Document) : java.lang.Object {
      return Document.DocUID
    }
    
    // 'sortBy' attribute on LinkCell (id=Name) at OnBaseDocumentListPopup.pcf: line 93, column 29
    function sortValue_14 (Document :  Document) : java.lang.Object {
      return Document.Name
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at OnBaseDocumentListPopup.pcf: line 117, column 51
    function sortValue_15 (Document :  Document) : java.lang.Object {
      return Document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=Subtype_Cell) at OnBaseDocumentListPopup.pcf: line 122, column 64
    function sortValue_16 (Document :  Document) : java.lang.Object {
      return Document.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at OnBaseDocumentListPopup.pcf: line 127, column 57
    function sortValue_17 (Document :  Document) : java.lang.Object {
      return Document.Status
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at OnBaseDocumentListPopup.pcf: line 132, column 42
    function sortValue_18 (Document :  Document) : java.lang.Object {
      return Document.Author
    }
    
    // 'value' attribute on DateCell (id=DateModified_Cell) at OnBaseDocumentListPopup.pcf: line 140, column 48
    function sortValue_19 (Document :  Document) : java.lang.Object {
      return Document.DateModified
    }
    
    // 'value' attribute on TextInput (id=EntityId_Input) at OnBaseDocumentListPopup.pcf: line 41, column 38
    function valueRoot_4 () : java.lang.Object {
      return Entity
    }
    
    // 'value' attribute on TextInput (id=EntityId_Input) at OnBaseDocumentListPopup.pcf: line 41, column 38
    function value_3 () : java.lang.String {
      return Entity.PublicID
    }
    
    // 'value' attribute on RowIterator at OnBaseDocumentListPopup.pcf: line 74, column 53
    function value_49 () : List<entity.Document> {
      return LinkedDocuments
    }
    
    // 'value' attribute on TextInput (id=EntityType_Input) at OnBaseDocumentListPopup.pcf: line 46, column 68
    function value_6 () : acc.onbase.configuration.DocumentLinkType {
      return LinkType
    }
    
    // 'value' attribute on TextAreaInput (id=EntityDesc_Input) at OnBaseDocumentListPopup.pcf: line 51, column 40
    function value_8 () : java.lang.String {
      return EntityDescription
    }
    
    // 'valueType' attribute on RowIterator at OnBaseDocumentListPopup.pcf: line 74, column 53
    function verifyValueTypeIsAllowedType_50 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at OnBaseDocumentListPopup.pcf: line 74, column 53
    function verifyValueTypeIsAllowedType_50 ($$arg :  gw.api.iterator.IteratorBackingData) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at OnBaseDocumentListPopup.pcf: line 74, column 53
    function verifyValueTypeIsAllowedType_50 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on RowIterator at OnBaseDocumentListPopup.pcf: line 74, column 53
    function verifyValueType_51 () : void {
      var __valueTypeArg : List<entity.Document>
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the valueType is not a valid type for use with an iterator
      // The valueType for an iterator must be an array or extend from List or IQueryBeanResult
      verifyValueTypeIsAllowedType_50(__valueTypeArg)
    }
    
    property get Beans () : KeyableBean[] {
      return getVariableValue("Beans", 0) as KeyableBean[]
    }
    
    property set Beans ($arg :  KeyableBean[]) {
      setVariableValue("Beans", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.OnBaseDocumentListPopup {
      return super.CurrentLocation as pcf.OnBaseDocumentListPopup
    }
    
    property get DocumentLinking () : acc.onbase.api.application.DocumentLinking {
      return getVariableValue("DocumentLinking", 0) as acc.onbase.api.application.DocumentLinking
    }
    
    property set DocumentLinking ($arg :  acc.onbase.api.application.DocumentLinking) {
      setVariableValue("DocumentLinking", 0, $arg)
    }
    
    property get Entity () : KeyableBean {
      return getVariableValue("Entity", 0) as KeyableBean
    }
    
    property set Entity ($arg :  KeyableBean) {
      setVariableValue("Entity", 0, $arg)
    }
    
    property get EntityDescription () : String {
      return getVariableValue("EntityDescription", 0) as String
    }
    
    property set EntityDescription ($arg :  String) {
      setVariableValue("EntityDescription", 0, $arg)
    }
    
    property get LinkType () : acc.onbase.configuration.DocumentLinkType {
      return getVariableValue("LinkType", 0) as acc.onbase.configuration.DocumentLinkType
    }
    
    property set LinkType ($arg :  acc.onbase.configuration.DocumentLinkType) {
      setVariableValue("LinkType", 0, $arg)
    }
    
    property get LinkedDocuments () : List<entity.Document> {
      return getVariableValue("LinkedDocuments", 0) as List<entity.Document>
    }
    
    property set LinkedDocuments ($arg :  List<entity.Document>) {
      setVariableValue("LinkedDocuments", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    
  }
  
  
}