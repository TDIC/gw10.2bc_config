package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailSummaryScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailSummaryScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailSummaryScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailSummaryScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=CloseDelinquencyButton) at AccountDetailSummaryScreen.pcf: line 46, column 48
    function action_12 () : void {
      TDIC_CloseDelinquencyProcessPopup.push(account.OpenDelinquencyTargets)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_TroubleTicketAlertAlertBar) at AccountDetailSummaryScreen.pcf: line 53, column 50
    function action_16 () : void {
      TroubleTicketAlertForward.push(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_DelinquencyAlertAlertBar) at AccountDetailSummaryScreen.pcf: line 59, column 67
    function action_21 () : void {
      AccountDetailDelinquencies.go(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_HeldChargesAlertBar) at AccountDetailSummaryScreen.pcf: line 69, column 33
    function action_27 () : void {
      AccountDetailCharges.go(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_ActivitiesAlertBar) at AccountDetailSummaryScreen.pcf: line 74, column 115
    function action_30 () : void {
      AccountActivitiesPage.go(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_PaymentRequestAlertBar) at AccountDetailSummaryScreen.pcf: line 79, column 166
    function action_33 () : void {
      AccountPaymentRequests.go(account)
    }
    
    // 'action' attribute on ToolbarButton (id=StartDelinquencyButton) at AccountDetailSummaryScreen.pcf: line 40, column 48
    function action_8 () : void {
      StartDelinquencyProcessPopup.push(account.OpenDelinquencyTargets)
    }
    
    // 'action' attribute on ToolbarButton (id=CloseDelinquencyButton) at AccountDetailSummaryScreen.pcf: line 46, column 48
    function action_dest_13 () : pcf.api.Destination {
      return pcf.TDIC_CloseDelinquencyProcessPopup.createDestination(account.OpenDelinquencyTargets)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_TroubleTicketAlertAlertBar) at AccountDetailSummaryScreen.pcf: line 53, column 50
    function action_dest_17 () : pcf.api.Destination {
      return pcf.TroubleTicketAlertForward.createDestination(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_DelinquencyAlertAlertBar) at AccountDetailSummaryScreen.pcf: line 59, column 67
    function action_dest_22 () : pcf.api.Destination {
      return pcf.AccountDetailDelinquencies.createDestination(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_HeldChargesAlertBar) at AccountDetailSummaryScreen.pcf: line 69, column 33
    function action_dest_28 () : pcf.api.Destination {
      return pcf.AccountDetailCharges.createDestination(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_ActivitiesAlertBar) at AccountDetailSummaryScreen.pcf: line 74, column 115
    function action_dest_31 () : pcf.api.Destination {
      return pcf.AccountActivitiesPage.createDestination(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_PaymentRequestAlertBar) at AccountDetailSummaryScreen.pcf: line 79, column 166
    function action_dest_34 () : pcf.api.Destination {
      return pcf.AccountPaymentRequests.createDestination(account)
    }
    
    // 'action' attribute on ToolbarButton (id=StartDelinquencyButton) at AccountDetailSummaryScreen.pcf: line 40, column 48
    function action_dest_9 () : pcf.api.Destination {
      return pcf.StartDelinquencyProcessPopup.createDestination(account.OpenDelinquencyTargets)
    }
    
    // 'available' attribute on ToolbarButton (id=CloseDelinquencyButton) at AccountDetailSummaryScreen.pcf: line 46, column 48
    function available_10 () : java.lang.Boolean {
      return !CurrentLocation.InEditMode && !account.isTargetOfHoldType(HoldType.TC_DELINQUENCY) && account.hasActiveDelinquencyProcess()
    }
    
    // 'available' attribute on AlertBar (id=AccountDetailSummary_TroubleTicketAlertAlertBar) at AccountDetailSummaryScreen.pcf: line 53, column 50
    function available_14 () : java.lang.Boolean {
      return perm.System.acctttktview
    }
    
    // 'available' attribute on AlertBar (id=AccountDetailSummary_DelinquencyAlertAlertBar) at AccountDetailSummaryScreen.pcf: line 59, column 67
    function available_19 () : java.lang.Boolean {
      return perm.System.acctdelview
    }
    
    // 'available' attribute on AlertBar (id=AccountDetailSummary_HeldChargesAlertBar) at AccountDetailSummaryScreen.pcf: line 69, column 33
    function available_25 () : java.lang.Boolean {
      return perm.System.acctchargesview
    }
    
    // 'available' attribute on ToolbarButton (id=StartDelinquencyButton) at AccountDetailSummaryScreen.pcf: line 40, column 48
    function available_6 () : java.lang.Boolean {
      return !CurrentLocation.InEditMode && !account.isTargetOfHoldType(HoldType.TC_DELINQUENCY)
    }
    
    // 'updateConfirmMessage' attribute on EditButtons at AccountDetailSummaryScreen.pcf: line 28, column 110
    function confirmMessage_4 () : java.lang.String {
      return gw.api.util.EquityValidationHelper.isEquityViolatedCheckAllPolicies(account)
    }
    
    // 'def' attribute on PanelRef at AccountDetailSummaryScreen.pcf: line 248, column 115
    function def_onEnter_121 (def :  pcf.AccountDBPaymentsLV) : void {
      def.onEnter(account, account.findReceivedPaymentMoneysSortedByReceivedDateDescending(), true)
    }
    
    // 'def' attribute on PanelRef at AccountDetailSummaryScreen.pcf: line 81, column 64
    function def_onEnter_35 (def :  pcf.AccountDetailDV) : void {
      def.onEnter(account, summaryFinancialsHelper)
    }
    
    // 'def' attribute on PanelRef at AccountDetailSummaryScreen.pcf: line 248, column 115
    function def_refreshVariables_122 (def :  pcf.AccountDBPaymentsLV) : void {
      def.refreshVariables(account, account.findReceivedPaymentMoneysSortedByReceivedDateDescending(), true)
    }
    
    // 'def' attribute on PanelRef at AccountDetailSummaryScreen.pcf: line 81, column 64
    function def_refreshVariables_36 (def :  pcf.AccountDetailDV) : void {
      def.refreshVariables(account, summaryFinancialsHelper)
    }
    
    // 'initialValue' attribute on Variable at AccountDetailSummaryScreen.pcf: line 13, column 30
    function initialValue_0 () : java.util.Date {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'initialValue' attribute on Variable at AccountDetailSummaryScreen.pcf: line 17, column 23
    function initialValue_1 () : boolean {
      return account.hasHeldCharges()
    }
    
    // 'initialValue' attribute on Variable at AccountDetailSummaryScreen.pcf: line 21, column 45
    function initialValue_2 () : gw.pl.currency.MonetaryAmount {
      return account.TotalValue
    }
    
    // 'initialValue' attribute on Variable at AccountDetailSummaryScreen.pcf: line 25, column 61
    function initialValue_3 () : gw.web.account.AccountSummaryFinancialsHelper {
      return new gw.web.account.AccountSummaryFinancialsHelper(account)
    }
    
    // 'label' attribute on AlertBar (id=AccountDetailSummary_TroubleTicketAlertAlertBar) at AccountDetailSummaryScreen.pcf: line 53, column 50
    function label_18 () : java.lang.Object {
      return account.AlertBarDisplayText
    }
    
    // 'label' attribute on AlertBar (id=AccountDetailSummary_DelinquencyAlertAlertBar) at AccountDetailSummaryScreen.pcf: line 59, column 67
    function label_23 () : java.lang.Object {
      return DisplayKey.get("Web.AccountDetailSummary.DelinquencyAlert", account.DelinquencyReasons)
    }
    
    // EditButtons at AccountDetailSummaryScreen.pcf: line 28, column 110
    function label_5 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at AccountDetailSummaryScreen.pcf: line 269, column 61
    function sortValue_123 (relatedAccount :  tdic.bc.config.account.RelatedAccount) : java.lang.Object {
      return relatedAccount.Account.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=AccountName_Cell) at AccountDetailSummaryScreen.pcf: line 274, column 68
    function sortValue_124 (relatedAccount :  tdic.bc.config.account.RelatedAccount) : java.lang.Object {
      return relatedAccount.Account.AccountNameLocalized
    }
    
    // 'value' attribute on TextCell (id=InsuredContatName_Cell) at AccountDetailSummaryScreen.pcf: line 278, column 59
    function sortValue_125 (relatedAccount :  tdic.bc.config.account.RelatedAccount) : java.lang.Object {
      return relatedAccount.Contact.DisplayName
    }
    
    // 'value' attribute on TextCell (id=AccountAddress_Cell) at AccountDetailSummaryScreen.pcf: line 282, column 72
    function sortValue_126 (relatedAccount :  tdic.bc.config.account.RelatedAccount) : java.lang.Object {
      return relatedAccount.Contact.ADANumberOfficialID_TDIC
    }
    
    // 'value' attribute on RowIterator at AccountDetailSummaryScreen.pcf: line 263, column 83
    function value_143 () : java.util.List<tdic.bc.config.account.RelatedAccount> {
      return account.AssociatedAccounts_TDIC
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailSummary_TroubleTicketAlertAlertBar) at AccountDetailSummaryScreen.pcf: line 53, column 50
    function visible_15 () : java.lang.Boolean {
      return account.HasActiveTroubleTickets
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailSummary_DelinquencyAlertAlertBar) at AccountDetailSummaryScreen.pcf: line 59, column 67
    function visible_20 () : java.lang.Boolean {
      return account.hasActiveDelinquenciesOutOfGracePeriod()
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailSummary_WriteoffAlertAlertBar) at AccountDetailSummaryScreen.pcf: line 63, column 120
    function visible_24 () : java.lang.Boolean {
      return account.WriteoffExpense.IsPositive && account.WriteoffExpense > (0.1 * account.TotalValue)
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailSummary_HeldChargesAlertBar) at AccountDetailSummaryScreen.pcf: line 69, column 33
    function visible_26 () : java.lang.Boolean {
      return hasHeldCharges
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailSummary_ActivitiesAlertBar) at AccountDetailSummaryScreen.pcf: line 74, column 115
    function visible_29 () : java.lang.Boolean {
      return account.Activities_Ext.where(\ a -> a.Status == typekey.ActivityStatus.TC_OPEN).length > 0
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailSummary_PaymentRequestAlertBar) at AccountDetailSummaryScreen.pcf: line 79, column 166
    function visible_32 () : java.lang.Boolean {
      return account.PaymentRequests.where(\ a -> a.Status == PaymentRequestStatus.TC_CREATED or a.Status == PaymentRequestStatus.TC_REQUESTED).length > 0
    }
    
    // 'visible' attribute on ToolbarButton (id=StartDelinquencyButton) at AccountDetailSummaryScreen.pcf: line 40, column 48
    function visible_7 () : java.lang.Boolean {
      return perm.System.manualDlnq_TDIC
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get hasHeldCharges () : boolean {
      return getVariableValue("hasHeldCharges", 0) as java.lang.Boolean
    }
    
    property set hasHeldCharges ($arg :  boolean) {
      setVariableValue("hasHeldCharges", 0, $arg)
    }
    
    property get summaryFinancialsHelper () : gw.web.account.AccountSummaryFinancialsHelper {
      return getVariableValue("summaryFinancialsHelper", 0) as gw.web.account.AccountSummaryFinancialsHelper
    }
    
    property set summaryFinancialsHelper ($arg :  gw.web.account.AccountSummaryFinancialsHelper) {
      setVariableValue("summaryFinancialsHelper", 0, $arg)
    }
    
    property get today () : java.util.Date {
      return getVariableValue("today", 0) as java.util.Date
    }
    
    property set today ($arg :  java.util.Date) {
      setVariableValue("today", 0, $arg)
    }
    
    property get totalValue () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("totalValue", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set totalValue ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("totalValue", 0, $arg)
    }
    
    function getPolicyDelinquencyStatusDescription(policyPeriod : PolicyPeriod) : String {
          return policyPeriod.Delinquent ? DisplayKey.get("Web.AccountPoliciesLV.PastDue") : DisplayKey.get("Web.AccountPoliciesLV.GoodStanding")
        }
    
        function getPolicyPeriodsPayedByListBillAccount(): gw.api.database.IQueryBeanResult<PolicyPeriod>{
          return gw.api.web.account.Policies.findAllOpenPolicyPeriodsWhereAccountIsOverridingPayer(account, null)
        }
    
        function getAllOpenPolicyPeriods() : gw.api.database.IQueryBeanResult<PolicyPeriod>{
          return gw.api.web.account.Policies.findAllOpenPolicyPeriods(account)
        }
    
        function getPolicyPeriodsForLV() : gw.api.database.IQueryBeanResult<PolicyPeriod> {
          return account.ListBill ? getPolicyPeriodsPayedByListBillAccount()  : getAllOpenPolicyPeriods()
        }
    function getTotalValue(policyPeriod: PolicyPeriod): gw.pl.currency.MonetaryAmount {
      return policyPeriod.TotalValue
    }
    
    function getPaidAmount(policyPeriod: PolicyPeriod): gw.pl.currency.MonetaryAmount {
      return policyPeriod.PaidAmount
    }
    
    function getDelinquentAmount(policyPeriod: PolicyPeriod): gw.pl.currency.MonetaryAmount {
      return policyPeriod.DelinquentAmount
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailSummaryScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountPoliciesLVExpressionsImpl extends AccountDetailSummaryScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=TotalUnbilledAmount_Cell) at AccountDetailSummaryScreen.pcf: line 213, column 43
    function currency_104 () : typekey.Currency {
      return account.Currency
    }
    
    // 'initialValue' attribute on Variable at AccountDetailSummaryScreen.pcf: line 93, column 49
    function initialValue_37 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.PoliciesUnbilledAmount
    }
    
    // 'initialValue' attribute on Variable at AccountDetailSummaryScreen.pcf: line 98, column 49
    function initialValue_38 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.PoliciesBilledAmount
    }
    
    // 'initialValue' attribute on Variable at AccountDetailSummaryScreen.pcf: line 103, column 49
    function initialValue_39 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.PoliciesOutstandingAmount
    }
    
    // 'initialValue' attribute on Variable at AccountDetailSummaryScreen.pcf: line 108, column 49
    function initialValue_40 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.PoliciesDelinquentAmount
    }
    
    // 'initialValue' attribute on Variable at AccountDetailSummaryScreen.pcf: line 113, column 49
    function initialValue_41 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.PoliciesPaidAmount
    }
    
    // 'initialValue' attribute on Variable at AccountDetailSummaryScreen.pcf: line 118, column 49
    function initialValue_42 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.PoliciesTotalValue
    }
    
    // 'sortBy' attribute on TextCell (id=PolicyNumber_Cell) at AccountDetailSummaryScreen.pcf: line 136, column 29
    function sortValue_43 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PolicyNumber
    }
    
    // 'sortBy' attribute on TextCell (id=PolicyNumber_Cell) at AccountDetailSummaryScreen.pcf: line 136, column 29
    function sortValue_44 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return  policyPeriod.TermNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at AccountDetailSummaryScreen.pcf: line 143, column 29
    function sortValue_45 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.Policy.LOBCode
    }
    
    // 'value' attribute on TextCell (id=PolicyStatus_Cell) at AccountDetailSummaryScreen.pcf: line 148, column 76
    function sortValue_46 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return getPolicyDelinquencyStatusDescription(policyPeriod)
    }
    
    // 'value' attribute on DateCell (id=PolicyPerEffDate_Cell) at AccountDetailSummaryScreen.pcf: line 152, column 54
    function sortValue_47 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=PolicyPerExpirDate_Cell) at AccountDetailSummaryScreen.pcf: line 156, column 56
    function sortValue_48 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      return policyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnbilledAmount_Cell) at AccountDetailSummaryScreen.pcf: line 162, column 60
    function sortValue_49 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      var policyPeriodBalances : gw.web.policy.PolicySummaryFinancialsHelper = (new gw.web.policy.PolicySummaryFinancialsHelper(policyPeriod))
return policyPeriodBalances.UnbilledAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CurrentAmountDue_Cell) at AccountDetailSummaryScreen.pcf: line 168, column 58
    function sortValue_50 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      var policyPeriodBalances : gw.web.policy.PolicySummaryFinancialsHelper = (new gw.web.policy.PolicySummaryFinancialsHelper(policyPeriod))
return policyPeriodBalances.BilledAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PastDueAmount_Cell) at AccountDetailSummaryScreen.pcf: line 174, column 62
    function sortValue_51 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      var policyPeriodBalances : gw.web.policy.PolicySummaryFinancialsHelper = (new gw.web.policy.PolicySummaryFinancialsHelper(policyPeriod))
return policyPeriodBalances.DelinquentAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalOutstanding_Cell) at AccountDetailSummaryScreen.pcf: line 180, column 63
    function sortValue_52 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      var policyPeriodBalances : gw.web.policy.PolicySummaryFinancialsHelper = (new gw.web.policy.PolicySummaryFinancialsHelper(policyPeriod))
return policyPeriodBalances.OutstandingAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PaidAmount_Cell) at AccountDetailSummaryScreen.pcf: line 186, column 56
    function sortValue_53 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      var policyPeriodBalances : gw.web.policy.PolicySummaryFinancialsHelper = (new gw.web.policy.PolicySummaryFinancialsHelper(policyPeriod))
return policyPeriodBalances.PaidAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalValue_Cell) at AccountDetailSummaryScreen.pcf: line 192, column 56
    function sortValue_54 (policyPeriod :  entity.PolicyPeriod) : java.lang.Object {
      var policyPeriodBalances : gw.web.policy.PolicySummaryFinancialsHelper = (new gw.web.policy.PolicySummaryFinancialsHelper(policyPeriod))
return policyPeriodBalances.TotalValue
    }
    
    // 'value' attribute on RowIterator at AccountDetailSummaryScreen.pcf: line 123, column 83
    function value_102 () : gw.api.database.IQueryBeanResult<entity.PolicyPeriod> {
      return getPolicyPeriodsForLV()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalUnbilledAmount_Cell) at AccountDetailSummaryScreen.pcf: line 213, column 43
    function value_103 () : gw.pl.currency.MonetaryAmount {
      return sumAdjUnbilledAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalCurrentAmountDue_Cell) at AccountDetailSummaryScreen.pcf: line 219, column 41
    function value_106 () : gw.pl.currency.MonetaryAmount {
      return sumAdjBilledAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalPastDueAmount_Cell) at AccountDetailSummaryScreen.pcf: line 225, column 39
    function value_109 () : gw.pl.currency.MonetaryAmount {
      return sumPastDueAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalTotalOutstanding_Cell) at AccountDetailSummaryScreen.pcf: line 231, column 46
    function value_112 () : gw.pl.currency.MonetaryAmount {
      return sumAdjOutstandingAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalPaidAmount_Cell) at AccountDetailSummaryScreen.pcf: line 237, column 36
    function value_115 () : gw.pl.currency.MonetaryAmount {
      return sumPaidAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalTotalValue_Cell) at AccountDetailSummaryScreen.pcf: line 243, column 36
    function value_118 () : gw.pl.currency.MonetaryAmount {
      return sumTotalValue
    }
    
    property get sumAdjBilledAmount () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("sumAdjBilledAmount", 1) as gw.pl.currency.MonetaryAmount
    }
    
    property set sumAdjBilledAmount ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("sumAdjBilledAmount", 1, $arg)
    }
    
    property get sumAdjOutstandingAmount () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("sumAdjOutstandingAmount", 1) as gw.pl.currency.MonetaryAmount
    }
    
    property set sumAdjOutstandingAmount ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("sumAdjOutstandingAmount", 1, $arg)
    }
    
    property get sumAdjUnbilledAmount () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("sumAdjUnbilledAmount", 1) as gw.pl.currency.MonetaryAmount
    }
    
    property set sumAdjUnbilledAmount ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("sumAdjUnbilledAmount", 1, $arg)
    }
    
    property get sumPaidAmount () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("sumPaidAmount", 1) as gw.pl.currency.MonetaryAmount
    }
    
    property set sumPaidAmount ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("sumPaidAmount", 1, $arg)
    }
    
    property get sumPastDueAmount () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("sumPastDueAmount", 1) as gw.pl.currency.MonetaryAmount
    }
    
    property set sumPastDueAmount ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("sumPastDueAmount", 1, $arg)
    }
    
    property get sumTotalValue () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("sumTotalValue", 1) as gw.pl.currency.MonetaryAmount
    }
    
    property set sumTotalValue ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("sumTotalValue", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailSummaryScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends AccountDetailSummaryScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at AccountDetailSummaryScreen.pcf: line 269, column 61
    function action_127 () : void {
      AccountDetailSummary.go(relatedAccount.Account)
    }
    
    // 'action' attribute on TextCell (id=AccountName_Cell) at AccountDetailSummaryScreen.pcf: line 274, column 68
    function action_132 () : void {
      AccountDetailSummary.go(relatedAccount.Account)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at AccountDetailSummaryScreen.pcf: line 269, column 61
    function action_dest_128 () : pcf.api.Destination {
      return pcf.AccountDetailSummary.createDestination(relatedAccount.Account)
    }
    
    // 'action' attribute on TextCell (id=AccountName_Cell) at AccountDetailSummaryScreen.pcf: line 274, column 68
    function action_dest_133 () : pcf.api.Destination {
      return pcf.AccountDetailSummary.createDestination(relatedAccount.Account)
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at AccountDetailSummaryScreen.pcf: line 269, column 61
    function valueRoot_130 () : java.lang.Object {
      return relatedAccount.Account
    }
    
    // 'value' attribute on TextCell (id=InsuredContatName_Cell) at AccountDetailSummaryScreen.pcf: line 278, column 59
    function valueRoot_138 () : java.lang.Object {
      return relatedAccount.Contact
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at AccountDetailSummaryScreen.pcf: line 269, column 61
    function value_129 () : java.lang.String {
      return relatedAccount.Account.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=AccountName_Cell) at AccountDetailSummaryScreen.pcf: line 274, column 68
    function value_134 () : java.lang.String {
      return relatedAccount.Account.AccountNameLocalized
    }
    
    // 'value' attribute on TextCell (id=InsuredContatName_Cell) at AccountDetailSummaryScreen.pcf: line 278, column 59
    function value_137 () : java.lang.String {
      return relatedAccount.Contact.DisplayName
    }
    
    // 'value' attribute on TextCell (id=AccountAddress_Cell) at AccountDetailSummaryScreen.pcf: line 282, column 72
    function value_140 () : java.lang.String {
      return relatedAccount.Contact.ADANumberOfficialID_TDIC
    }
    
    property get relatedAccount () : tdic.bc.config.account.RelatedAccount {
      return getIteratedValue(1) as tdic.bc.config.account.RelatedAccount
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailSummaryScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountPoliciesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at AccountDetailSummaryScreen.pcf: line 136, column 29
    function action_56 () : void {
      PolicyOverview.go(policyPeriod)
    }
    
    // 'action' attribute on TypeKeyCell (id=Product_Cell) at AccountDetailSummaryScreen.pcf: line 143, column 29
    function action_61 () : void {
      PolicyOverview.go(policyPeriod)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at AccountDetailSummaryScreen.pcf: line 136, column 29
    function action_dest_57 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(policyPeriod)
    }
    
    // 'action' attribute on TypeKeyCell (id=Product_Cell) at AccountDetailSummaryScreen.pcf: line 143, column 29
    function action_dest_62 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(policyPeriod)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=UnbilledAmount_Cell) at AccountDetailSummaryScreen.pcf: line 162, column 60
    function currency_79 () : typekey.Currency {
      return account.Currency
    }
    
    // 'fontColor' attribute on TextCell (id=PolicyStatus_Cell) at AccountDetailSummaryScreen.pcf: line 148, column 76
    function fontColor_66 () : java.lang.Object {
      return policyPeriod.Delinquent ? gw.api.web.color.GWColor.THEME_PROGRESS_OVERDUE : null
    }
    
    // 'initialValue' attribute on Variable at AccountDetailSummaryScreen.pcf: line 127, column 65
    function initialValue_55 () : gw.web.policy.PolicySummaryFinancialsHelper {
      return new gw.web.policy.PolicySummaryFinancialsHelper(policyPeriod)
    }
    
    // RowIterator at AccountDetailSummaryScreen.pcf: line 123, column 83
    function initializeVariables_101 () : void {
        policyPeriodBalances = new gw.web.policy.PolicySummaryFinancialsHelper(policyPeriod);

    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AccountDetailSummaryScreen.pcf: line 136, column 29
    function valueRoot_59 () : java.lang.Object {
      return policyPeriod
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at AccountDetailSummaryScreen.pcf: line 143, column 29
    function valueRoot_64 () : java.lang.Object {
      return policyPeriod.Policy
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnbilledAmount_Cell) at AccountDetailSummaryScreen.pcf: line 162, column 60
    function valueRoot_78 () : java.lang.Object {
      return policyPeriodBalances
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AccountDetailSummaryScreen.pcf: line 136, column 29
    function value_58 () : java.lang.String {
      return policyPeriod.DisplayName
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at AccountDetailSummaryScreen.pcf: line 143, column 29
    function value_63 () : typekey.LOBCode {
      return policyPeriod.Policy.LOBCode
    }
    
    // 'value' attribute on TextCell (id=PolicyStatus_Cell) at AccountDetailSummaryScreen.pcf: line 148, column 76
    function value_67 () : java.lang.String {
      return getPolicyDelinquencyStatusDescription(policyPeriod)
    }
    
    // 'value' attribute on DateCell (id=PolicyPerEffDate_Cell) at AccountDetailSummaryScreen.pcf: line 152, column 54
    function value_71 () : java.util.Date {
      return policyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=PolicyPerExpirDate_Cell) at AccountDetailSummaryScreen.pcf: line 156, column 56
    function value_74 () : java.util.Date {
      return policyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnbilledAmount_Cell) at AccountDetailSummaryScreen.pcf: line 162, column 60
    function value_77 () : gw.pl.currency.MonetaryAmount {
      return policyPeriodBalances.UnbilledAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CurrentAmountDue_Cell) at AccountDetailSummaryScreen.pcf: line 168, column 58
    function value_81 () : gw.pl.currency.MonetaryAmount {
      return policyPeriodBalances.BilledAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PastDueAmount_Cell) at AccountDetailSummaryScreen.pcf: line 174, column 62
    function value_85 () : gw.pl.currency.MonetaryAmount {
      return policyPeriodBalances.DelinquentAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalOutstanding_Cell) at AccountDetailSummaryScreen.pcf: line 180, column 63
    function value_89 () : gw.pl.currency.MonetaryAmount {
      return policyPeriodBalances.OutstandingAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PaidAmount_Cell) at AccountDetailSummaryScreen.pcf: line 186, column 56
    function value_93 () : gw.pl.currency.MonetaryAmount {
      return policyPeriodBalances.PaidAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalValue_Cell) at AccountDetailSummaryScreen.pcf: line 192, column 56
    function value_97 () : gw.pl.currency.MonetaryAmount {
      return policyPeriodBalances.TotalValue
    }
    
    // 'fontColor' attribute on TextCell (id=PolicyStatus_Cell) at AccountDetailSummaryScreen.pcf: line 148, column 76
    function verifyFontColorIsAllowedType_68 ($$arg :  gw.api.web.color.GWColor) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on TextCell (id=PolicyStatus_Cell) at AccountDetailSummaryScreen.pcf: line 148, column 76
    function verifyFontColorIsAllowedType_68 ($$arg :  java.lang.String) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on TextCell (id=PolicyStatus_Cell) at AccountDetailSummaryScreen.pcf: line 148, column 76
    function verifyFontColor_69 () : void {
      var __fontColorArg = policyPeriod.Delinquent ? gw.api.web.color.GWColor.THEME_PROGRESS_OVERDUE : null
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the fontColor return type is not a valid type for use with a ValueWidget's fontColor
      // It needs to return a String or a GWColor.
      verifyFontColorIsAllowedType_68(__fontColorArg)
    }
    
    property get policyPeriod () : entity.PolicyPeriod {
      return getIteratedValue(2) as entity.PolicyPeriod
    }
    
    property get policyPeriodBalances () : gw.web.policy.PolicySummaryFinancialsHelper {
      return getVariableValue("policyPeriodBalances", 2) as gw.web.policy.PolicySummaryFinancialsHelper
    }
    
    property set policyPeriodBalances ($arg :  gw.web.policy.PolicySummaryFinancialsHelper) {
      setVariableValue("policyPeriodBalances", 2, $arg)
    }
    
    
  }
  
  
}