package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailDocuments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailDocumentsExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailDocuments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailDocumentsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policy :  Policy, plcyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=webCustomQueryButton) at PolicyDetailDocuments.pcf: line 27, column 49
    function action_1 () : void {
      OnBaseUrl.push(org.apache.commons.lang.StringUtils.isBlank(plcyPeriod.LegacyPolicyNumber_TDIC) ?  tdic.bc.integ.plugins.onbase.util.OnBaseWebUtils_TDIC.renderPopUrl((ScriptParameters.getParameterValue("acc.onbase.WebCustomQuery") as String)?.trim() , {plcyPeriod.PolicyNumber, ((acc.onbase.configuration.OnBaseConfigurationFactory.Instance.WebClientType) as String)?.trim()}) : tdic.bc.integ.plugins.onbase.util.OnBaseWebUtils_TDIC.renderPopUrl((ScriptParameters.getParameterValue("acc.onbase.WebCustomQueryWithLegacyPolicy") as String)?.trim() , {tdic.bc.integ.plugins.onbase.util.OnBaseWebUtils_TDIC.getHistoricalPolicyNumber(plcyPeriod.LegacyPolicyNumber_TDIC), plcyPeriod.PolicyNumber, ((acc.onbase.configuration.OnBaseConfigurationFactory.Instance.WebClientType) as String)?.trim()}))
    }
    
    // 'action' attribute on ToolbarButton (id=webCustomQueryButton) at PolicyDetailDocuments.pcf: line 27, column 49
    function action_dest_2 () : pcf.api.Destination {
      return pcf.OnBaseUrl.createDestination(org.apache.commons.lang.StringUtils.isBlank(plcyPeriod.LegacyPolicyNumber_TDIC) ?  tdic.bc.integ.plugins.onbase.util.OnBaseWebUtils_TDIC.renderPopUrl((ScriptParameters.getParameterValue("acc.onbase.WebCustomQuery") as String)?.trim() , {plcyPeriod.PolicyNumber, ((acc.onbase.configuration.OnBaseConfigurationFactory.Instance.WebClientType) as String)?.trim()}) : tdic.bc.integ.plugins.onbase.util.OnBaseWebUtils_TDIC.renderPopUrl((ScriptParameters.getParameterValue("acc.onbase.WebCustomQueryWithLegacyPolicy") as String)?.trim() , {tdic.bc.integ.plugins.onbase.util.OnBaseWebUtils_TDIC.getHistoricalPolicyNumber(plcyPeriod.LegacyPolicyNumber_TDIC), plcyPeriod.PolicyNumber, ((acc.onbase.configuration.OnBaseConfigurationFactory.Instance.WebClientType) as String)?.trim()}))
    }
    
    // 'canVisit' attribute on Page (id=PolicyDetailDocuments) at PolicyDetailDocuments.pcf: line 8, column 75
    static function canVisit_26 (plcyPeriod :  PolicyPeriod, policy :  Policy) : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcydocview and policy.hasNonArchivedPolicyPeriods()
    }
    
    // 'initialValue' attribute on Variable at PolicyDetailDocuments.pcf: line 20, column 52
    function initialValue_0 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    // Page (id=PolicyDetailDocuments) at PolicyDetailDocuments.pcf: line 8, column 75
    static function parent_27 (plcyPeriod :  PolicyPeriod, policy :  Policy) : pcf.api.Destination {
      return pcf.PolicyGroup.createDestination(plcyPeriod)
    }
    
    // 'visible' attribute on AlertBar (id=DocumentStoreSuspendedWarning) at PolicyDetailDocuments.pcf: line 32, column 76
    function visible_3 () : java.lang.Boolean {
      return documentsActionHelper.ShowDocumentStoreSuspendedWarning
    }
    
    // 'visible' attribute on AlertBar (id=IDCSDisabledAlert) at PolicyDetailDocuments.pcf: line 36, column 67
    function visible_4 () : java.lang.Boolean {
      return not documentsActionHelper.ContentSourceEnabled
    }
    
    // 'visible' attribute on AlertBar (id=IDCSUnavailableAlert) at PolicyDetailDocuments.pcf: line 40, column 71
    function visible_5 () : java.lang.Boolean {
      return documentsActionHelper.ShowContentServerDownWarning
    }
    
    // 'visible' attribute on AlertBar (id=IDMSUnavailableAlert) at PolicyDetailDocuments.pcf: line 44, column 72
    function visible_6 () : java.lang.Boolean {
      return documentsActionHelper.ShowMetadataServerDownWarning
    }
    
    override property get CurrentLocation () : pcf.PolicyDetailDocuments {
      return super.CurrentLocation as pcf.PolicyDetailDocuments
    }
    
    property get documentsActionHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionHelper", 0, $arg)
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 0, $arg)
    }
    
    property get policy () : Policy {
      return getVariableValue("policy", 0) as Policy
    }
    
    property set policy ($arg :  Policy) {
      setVariableValue("policy", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailDocuments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends PolicyDetailDocumentsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=RefreshAsyncContent) at PolicyDetailDocuments.pcf: line 98, column 109
    function action_21 () : void {
      
    }
    
    // 'available' attribute on ToolbarButton (id=AddDocuments) at PolicyDetailDocuments.pcf: line 80, column 46
    function available_16 () : java.lang.Boolean {
      return documentsActionHelper.ContentSourceEnabled
    }
    
    // 'available' attribute on ToolbarButton (id=RefreshAsyncContent) at PolicyDetailDocuments.pcf: line 98, column 109
    function available_19 () : java.lang.Boolean {
      return documentsActionHelper.DocumentContentServerAvailable
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=AccountDocuments_ObsolesceButton) at PolicyDetailDocuments.pcf: line 66, column 29
    function available_9 () : java.lang.Boolean {
      return documentsActionHelper.DocumentMetadataActionsAvailable
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=AccountDocuments_ObsolesceButton) at PolicyDetailDocuments.pcf: line 66, column 29
    function checkedRowAction_10 (element :  entity.Document, CheckedValue :  entity.Document) : void {
       CheckedValue.Obsolete = true
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=AccountDocuments_DeobsolesceButton) at PolicyDetailDocuments.pcf: line 75, column 66
    function checkedRowAction_13 (element :  entity.Document, CheckedValue :  entity.Document) : void {
       CheckedValue.Obsolete = false
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=RemoveDocuments) at PolicyDetailDocuments.pcf: line 91, column 100
    function checkedRowAction_18 (element :  entity.Document, CheckedValue :  entity.Document) : void {
      gw.api.web.document.DocumentsHelper.deleteDocument(CheckedValue)
    }
    
    // 'def' attribute on MenuItemSetRef at PolicyDetailDocuments.pcf: line 82, column 55
    function def_onEnter_14 (def :  pcf.NewDocumentMenuItemSet) : void {
      def.onEnter(policy)
    }
    
    // 'def' attribute on PanelRef at PolicyDetailDocuments.pcf: line 56, column 73
    function def_onEnter_22 (def :  pcf.DocumentsLV) : void {
      def.onEnter(documentList, documentSearchCriteria, true)
    }
    
    // 'def' attribute on PanelRef at PolicyDetailDocuments.pcf: line 54, column 59
    function def_onEnter_7 (def :  pcf.DocumentSearchDV) : void {
      def.onEnter(documentSearchCriteria)
    }
    
    // 'def' attribute on MenuItemSetRef at PolicyDetailDocuments.pcf: line 82, column 55
    function def_refreshVariables_15 (def :  pcf.NewDocumentMenuItemSet) : void {
      def.refreshVariables(policy)
    }
    
    // 'def' attribute on PanelRef at PolicyDetailDocuments.pcf: line 56, column 73
    function def_refreshVariables_23 (def :  pcf.DocumentsLV) : void {
      def.refreshVariables(documentList, documentSearchCriteria, true)
    }
    
    // 'def' attribute on PanelRef at PolicyDetailDocuments.pcf: line 54, column 59
    function def_refreshVariables_8 (def :  pcf.DocumentSearchDV) : void {
      def.refreshVariables(documentSearchCriteria)
    }
    
    // 'searchCriteria' attribute on SearchPanel at PolicyDetailDocuments.pcf: line 52, column 78
    function searchCriteria_25 () : entity.DocumentSearchCriteria {
      var c = new DocumentSearchCriteria(); c.IncludeObsoletes = false; c.Policy = policy; return c;
    }
    
    // 'search' attribute on SearchPanel at PolicyDetailDocuments.pcf: line 52, column 78
    function search_24 () : java.lang.Object {
      return documentSearchCriteria.performSearch(true) as gw.api.database.IQueryBeanResult<Document>
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=AccountDocuments_DeobsolesceButton) at PolicyDetailDocuments.pcf: line 75, column 66
    function visible_12 () : java.lang.Boolean {
      return documentSearchCriteria.IncludeObsoletes
    }
    
    // 'visible' attribute on ToolbarButton (id=AddDocuments) at PolicyDetailDocuments.pcf: line 80, column 46
    function visible_17 () : java.lang.Boolean {
      return perm.Document.create
    }
    
    // 'visible' attribute on ToolbarButton (id=RefreshAsyncContent) at PolicyDetailDocuments.pcf: line 98, column 109
    function visible_20 () : java.lang.Boolean {
      return documentsActionHelper.isShowAsynchronousRefreshAction(documentList.toTypedArray())
    }
    
    property get documentList () : gw.api.database.IQueryBeanResult<Document> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<Document>
    }
    
    property get documentSearchCriteria () : entity.DocumentSearchCriteria {
      return getCriteriaValue(1) as entity.DocumentSearchCriteria
    }
    
    property set documentSearchCriteria ($arg :  entity.DocumentSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}