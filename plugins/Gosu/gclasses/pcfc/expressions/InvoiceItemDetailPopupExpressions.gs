package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/InvoiceItemDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceItemDetailPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/invoice/InvoiceItemDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceItemDetailPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (invoiceItem :  InvoiceItem) : int {
      return 0
    }
    
    // 'action' attribute on TextInput (id=payer_Input) at InvoiceItemDetailPopup.pcf: line 43, column 61
    function action_14 () : void {
      goToPayer()
    }
    
    // 'action' attribute on TextInput (id=owner_Input) at InvoiceItemDetailPopup.pcf: line 37, column 41
    function action_9 () : void {
      AccountSummary.push(invoiceItem.Owner)
    }
    
    // 'action' attribute on TextInput (id=owner_Input) at InvoiceItemDetailPopup.pcf: line 37, column 41
    function action_dest_10 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination(invoiceItem.Owner)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=amount_Input) at InvoiceItemDetailPopup.pcf: line 27, column 41
    function currency_5 () : typekey.Currency {
      return invoiceItem.Currency
    }
    
    // 'def' attribute on PanelRef at InvoiceItemDetailPopup.pcf: line 47, column 58
    function def_onEnter_18 (def :  pcf.InvoiceItemBreakdownPanelSet) : void {
      def.onEnter(invoiceItem)
    }
    
    // 'def' attribute on PanelRef (id=itemEvents) at InvoiceItemDetailPopup.pcf: line 50, column 26
    function def_onEnter_20 (def :  pcf.ItemEventPanelSet) : void {
      def.onEnter(invoiceItem)
    }
    
    // 'def' attribute on PanelRef at InvoiceItemDetailPopup.pcf: line 47, column 58
    function def_refreshVariables_19 (def :  pcf.InvoiceItemBreakdownPanelSet) : void {
      def.refreshVariables(invoiceItem)
    }
    
    // 'def' attribute on PanelRef (id=itemEvents) at InvoiceItemDetailPopup.pcf: line 50, column 26
    function def_refreshVariables_21 (def :  pcf.ItemEventPanelSet) : void {
      def.refreshVariables(invoiceItem)
    }
    
    // 'value' attribute on DateInput (id=eventDate_Input) at InvoiceItemDetailPopup.pcf: line 21, column 44
    function valueRoot_1 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on DateInput (id=eventDate_Input) at InvoiceItemDetailPopup.pcf: line 21, column 44
    function value_0 () : java.util.Date {
      return invoiceItem.EventDate
    }
    
    // 'value' attribute on TextInput (id=owner_Input) at InvoiceItemDetailPopup.pcf: line 37, column 41
    function value_11 () : entity.Account {
      return invoiceItem.Owner
    }
    
    // 'value' attribute on TextInput (id=payer_Input) at InvoiceItemDetailPopup.pcf: line 43, column 61
    function value_15 () : gw.api.domain.invoice.InvoicePayer {
      return invoiceItem.Payer
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amount_Input) at InvoiceItemDetailPopup.pcf: line 27, column 41
    function value_3 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.Amount
    }
    
    // 'value' attribute on TextInput (id=invoice_Input) at InvoiceItemDetailPopup.pcf: line 31, column 266
    function value_7 () : java.lang.String {
      return DisplayKey.get("Web.InvoiceItemDetailPopup.Invoice", invoiceItem.Invoice.Subtype, invoiceItem.Invoice.InvoiceNumber,  invoiceItem.Invoice.StatusForUI, gw.api.util.StringUtil.formatDate(invoiceItem.Invoice.DueDate, "short"))
    }
    
    override property get CurrentLocation () : pcf.InvoiceItemDetailPopup {
      return super.CurrentLocation as pcf.InvoiceItemDetailPopup
    }
    
    property get invoiceItem () : InvoiceItem {
      return getVariableValue("invoiceItem", 0) as InvoiceItem
    }
    
    property set invoiceItem ($arg :  InvoiceItem) {
      setVariableValue("invoiceItem", 0, $arg)
    }
    
    function goToPayer( ) {
      var payer = invoiceItem.getPayer() 
      if ( payer typeis Producer ) {
        ProducerDetail.push(payer)
      } else {
        AccountSummary.push(payer as Account)
      }
    }
    
    
  }
  
  
}