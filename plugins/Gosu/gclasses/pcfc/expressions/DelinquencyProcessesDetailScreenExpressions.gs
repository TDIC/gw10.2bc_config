package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessesDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyProcessesDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessesDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyProcessesDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get initialDelinquencyProcess () : DelinquencyProcess {
      return getRequireValue("initialDelinquencyProcess", 0) as DelinquencyProcess
    }
    
    property set initialDelinquencyProcess ($arg :  DelinquencyProcess) {
      setRequireValue("initialDelinquencyProcess", 0, $arg)
    }
    
    property get target () : gw.api.domain.delinquency.DelinquencyTarget {
      return getRequireValue("target", 0) as gw.api.domain.delinquency.DelinquencyTarget
    }
    
    property set target ($arg :  gw.api.domain.delinquency.DelinquencyTarget) {
      setRequireValue("target", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessesDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailPanelExpressionsImpl extends DelinquencyProcessesDetailScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at DelinquencyProcessesDetailScreen.pcf: line 79, column 80
    function def_onEnter_30 (def :  pcf.DelinquencyProcessDetailPanelSet) : void {
      def.onEnter(selectedDelinquencyProcess)
    }
    
    // 'def' attribute on PanelRef at DelinquencyProcessesDetailScreen.pcf: line 79, column 80
    function def_refreshVariables_31 (def :  pcf.DelinquencyProcessDetailPanelSet) : void {
      def.refreshVariables(selectedDelinquencyProcess)
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at DelinquencyProcessesDetailScreen.pcf: line 33, column 95
    function filters_0 () : gw.api.filters.IFilter[] {
      return new gw.api.web.delinquency.DelinquencyProcessFilters().FilterOptions
    }
    
    // 'selectionOnEnter' attribute on ListDetailPanel (id=DetailPanel) at DelinquencyProcessesDetailScreen.pcf: line 20, column 42
    function selectionOnEnter_32 () : java.lang.Object {
      return selectedDelinquencyProcess
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DelinquencyProcessesDetailScreen.pcf: line 40, column 61
    function sortValue_1 (DelinquencyProcess :  entity.DelinquencyProcess) : java.lang.Object {
      return DelinquencyProcess.Status
    }
    
    // 'value' attribute on TypeKeyCell (id=Reason_Cell) at DelinquencyProcessesDetailScreen.pcf: line 45, column 54
    function sortValue_2 (DelinquencyProcess :  entity.DelinquencyProcess) : java.lang.Object {
      return DelinquencyProcess.Reason
    }
    
    // 'value' attribute on TextCell (id=DelinquencyTarget_Cell) at DelinquencyProcessesDetailScreen.pcf: line 51, column 72
    function sortValue_3 (DelinquencyProcess :  entity.DelinquencyProcess) : java.lang.Object {
      return DelinquencyProcess.Target
    }
    
    // 'value' attribute on TextCell (id=PolicyState_Cell) at DelinquencyProcessesDetailScreen.pcf: line 56, column 49
    function sortValue_4 (DelinquencyProcess :  entity.DelinquencyProcess) : java.lang.Object {
      return DelinquencyProcess.Target typeis PolicyPeriod ? DelinquencyProcess.Target.RiskJurisdiction : null
    }
    
    // 'value' attribute on TextCell (id=ProductLOB_Cell) at DelinquencyProcessesDetailScreen.pcf: line 61, column 44
    function sortValue_5 (DelinquencyProcess :  entity.DelinquencyProcess) : java.lang.Object {
      return DelinquencyProcess.Target typeis PolicyPeriod ? DelinquencyProcess.Target.Policy.LOBCode : null
    }
    
    // 'value' attribute on DateCell (id=StartDate_Cell) at DelinquencyProcessesDetailScreen.pcf: line 65, column 53
    function sortValue_6 (DelinquencyProcess :  entity.DelinquencyProcess) : java.lang.Object {
      return DelinquencyProcess.StartDate
    }
    
    // 'value' attribute on TextCell (id=AssignedUser_Cell) at DelinquencyProcessesDetailScreen.pcf: line 70, column 40
    function sortValue_7 (DelinquencyProcess :  entity.DelinquencyProcess) : java.lang.Object {
      return DelinquencyProcess.AssignedUser
    }
    
    // 'value' attribute on RowIterator at DelinquencyProcessesDetailScreen.pcf: line 29, column 51
    function value_29 () : entity.DelinquencyProcess[] {
      return target.getDelinquencyProcesses()
    }
    
    property get selectedDelinquencyProcess () : DelinquencyProcess {
      return getCurrentSelection(1) as DelinquencyProcess
    }
    
    property set selectedDelinquencyProcess ($arg :  DelinquencyProcess) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessesDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=DelinquencyTarget_Cell) at DelinquencyProcessesDetailScreen.pcf: line 51, column 72
    function action_14 () : void {
      DelinquencyTargetDetailsForward.go(DelinquencyProcess.Target)
    }
    
    // 'action' attribute on TextCell (id=DelinquencyTarget_Cell) at DelinquencyProcessesDetailScreen.pcf: line 51, column 72
    function action_dest_15 () : pcf.api.Destination {
      return pcf.DelinquencyTargetDetailsForward.createDestination(DelinquencyProcess.Target)
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DelinquencyProcessesDetailScreen.pcf: line 40, column 61
    function valueRoot_9 () : java.lang.Object {
      return DelinquencyProcess
    }
    
    // 'value' attribute on TypeKeyCell (id=Reason_Cell) at DelinquencyProcessesDetailScreen.pcf: line 45, column 54
    function value_11 () : typekey.DelinquencyReason {
      return DelinquencyProcess.Reason
    }
    
    // 'value' attribute on TextCell (id=DelinquencyTarget_Cell) at DelinquencyProcessesDetailScreen.pcf: line 51, column 72
    function value_16 () : gw.api.domain.delinquency.DelinquencyTarget {
      return DelinquencyProcess.Target
    }
    
    // 'value' attribute on TextCell (id=PolicyState_Cell) at DelinquencyProcessesDetailScreen.pcf: line 56, column 49
    function value_19 () : typekey.Jurisdiction {
      return DelinquencyProcess.Target typeis PolicyPeriod ? DelinquencyProcess.Target.RiskJurisdiction : null
    }
    
    // 'value' attribute on TextCell (id=ProductLOB_Cell) at DelinquencyProcessesDetailScreen.pcf: line 61, column 44
    function value_21 () : typekey.LOBCode {
      return DelinquencyProcess.Target typeis PolicyPeriod ? DelinquencyProcess.Target.Policy.LOBCode : null
    }
    
    // 'value' attribute on DateCell (id=StartDate_Cell) at DelinquencyProcessesDetailScreen.pcf: line 65, column 53
    function value_23 () : java.util.Date {
      return DelinquencyProcess.StartDate
    }
    
    // 'value' attribute on TextCell (id=AssignedUser_Cell) at DelinquencyProcessesDetailScreen.pcf: line 70, column 40
    function value_26 () : entity.User {
      return DelinquencyProcess.AssignedUser
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DelinquencyProcessesDetailScreen.pcf: line 40, column 61
    function value_8 () : typekey.DelinquencyProcessStatus {
      return DelinquencyProcess.Status
    }
    
    property get DelinquencyProcess () : entity.DelinquencyProcess {
      return getIteratedValue(2) as entity.DelinquencyProcess
    }
    
    
  }
  
  
}