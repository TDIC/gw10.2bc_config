package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/Producers.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducersExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/Producers.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducersExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=Producers) at Producers.pcf: line 9, column 61
    static function canVisit_2 () : java.lang.Boolean {
      return perm.System.prodtabview
    }
    
    // 'def' attribute on ScreenRef at Producers.pcf: line 11, column 47
    function def_onEnter_0 (def :  pcf.ProducerSearchScreen) : void {
      def.onEnter(true, true)
    }
    
    // 'def' attribute on ScreenRef at Producers.pcf: line 11, column 47
    function def_refreshVariables_1 (def :  pcf.ProducerSearchScreen) : void {
      def.refreshVariables(true, true)
    }
    
    // Page (id=Producers) at Producers.pcf: line 9, column 61
    static function parent_3 () : pcf.api.Destination {
      return pcf.ProducersGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.Producers {
      return super.CurrentLocation as pcf.Producers
    }
    
    
  }
  
  
}