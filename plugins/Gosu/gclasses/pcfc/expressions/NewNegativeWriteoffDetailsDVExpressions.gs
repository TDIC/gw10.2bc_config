package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewNegativeWriteoffDetailsDVExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewNegativeWriteoffDetailsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at NewNegativeWriteoffDetailsDV.pcf: line 29, column 36
    function currency_13 () : typekey.Currency {
      return uiWriteoff.WriteOff.TAccountOwner.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at NewNegativeWriteoffDetailsDV.pcf: line 29, column 36
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      uiWriteoff.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on CheckBoxInput (id=UseFullAmount_Input) at NewNegativeWriteoffDetailsDV.pcf: line 18, column 25
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      uiWriteoff.UseFullAmount = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'editable' attribute on CheckBoxInput (id=UseFullAmount_Input) at NewNegativeWriteoffDetailsDV.pcf: line 18, column 25
    function editable_0 () : java.lang.Boolean {
      return uiWriteoff.FullAmount.IsPositive
    }
    
    // 'label' attribute on CheckBoxInput (id=UseFullAmount_Input) at NewNegativeWriteoffDetailsDV.pcf: line 18, column 25
    function label_1 () : java.lang.Object {
      return DisplayKey.get("Web.NewNegativeWriteoffWizardDetailsStepScreen.FullAmount", uiWriteoff.FullAmount.render())
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=Amount_Input) at NewNegativeWriteoffDetailsDV.pcf: line 29, column 36
    function validationExpression_9 () : java.lang.Object {
      return validateAmount()
    }
    
    // 'value' attribute on CheckBoxInput (id=UseFullAmount_Input) at NewNegativeWriteoffDetailsDV.pcf: line 18, column 25
    function valueRoot_4 () : java.lang.Object {
      return uiWriteoff
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at NewNegativeWriteoffDetailsDV.pcf: line 29, column 36
    function value_10 () : gw.pl.currency.MonetaryAmount {
      return uiWriteoff.Amount
    }
    
    // 'value' attribute on CheckBoxInput (id=UseFullAmount_Input) at NewNegativeWriteoffDetailsDV.pcf: line 18, column 25
    function value_2 () : java.lang.Boolean {
      return uiWriteoff.UseFullAmount
    }
    
    property get uiWriteoff () : gw.api.web.accounting.UIWriteOffCreation {
      return getRequireValue("uiWriteoff", 0) as gw.api.web.accounting.UIWriteOffCreation
    }
    
    property set uiWriteoff ($arg :  gw.api.web.accounting.UIWriteOffCreation) {
      setRequireValue("uiWriteoff", 0, $arg)
    }
    
    // todo EDF unify this with validation in new write-off wizard
          function validateAmount() : String {
            var amount = uiWriteoff.Amount;
    
            // ensure that it is positive and no more than the max allowed
            if (!amount.IsPositive) {
              return DisplayKey.get("Web.NewNegativeWriteoffWizard.NonPositiveNegativeWriteoffAmount");
            }
            if (amount > uiWriteoff.FullAmount) {
              return DisplayKey.get("Web.NewNegativeWriteoffWizard.OverFullNegativeWriteoffAmount", uiWriteoff.FullAmount.render());
            }
            return null;
          }
    
    
  }
  
  
}