package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/TDIC_PlanDataUploadGLTransactionMappingLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_PlanDataUploadGLTransactionMappingLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/TDIC_PlanDataUploadGLTransactionMappingLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TDIC_PlanDataUploadGLTransactionMappingLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at TDIC_PlanDataUploadGLTransactionMappingLV.pcf: line 17, column 98
    function highlighted_13 () : java.lang.Boolean {
      return (glMapping.Error or glMapping.Skipped)  && processor.LoadCompleted
    }
    
    // 'value' attribute on TypeKeyCell (id=transactionType_Cell) at TDIC_PlanDataUploadGLTransactionMappingLV.pcf: line 28, column 44
    function valueRoot_8 () : java.lang.Object {
      return glMapping
    }
    
    // 'value' attribute on TextCell (id=mappedName_Cell) at TDIC_PlanDataUploadGLTransactionMappingLV.pcf: line 33, column 41
    function value_10 () : java.lang.String {
      return glMapping.MappedTransactionName
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at TDIC_PlanDataUploadGLTransactionMappingLV.pcf: line 23, column 46
    function value_4 () : java.lang.String {
      return processor.getLoadStatus(glMapping)
    }
    
    // 'value' attribute on TypeKeyCell (id=transactionType_Cell) at TDIC_PlanDataUploadGLTransactionMappingLV.pcf: line 28, column 44
    function value_7 () : typekey.Transaction {
      return glMapping.OriginalTransactionName
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at TDIC_PlanDataUploadGLTransactionMappingLV.pcf: line 23, column 46
    function visible_5 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get glMapping () : tdic.util.dataloader.data.plandata.GLTransactionNameMappingData {
      return getIteratedValue(1) as tdic.util.dataloader.data.plandata.GLTransactionNameMappingData
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/TDIC_PlanDataUploadGLTransactionMappingLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_PlanDataUploadGLTransactionMappingLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at TDIC_PlanDataUploadGLTransactionMappingLV.pcf: line 23, column 46
    function sortValue_0 (glMapping :  tdic.util.dataloader.data.plandata.GLTransactionNameMappingData) : java.lang.Object {
      return processor.getLoadStatus(glMapping)
    }
    
    // 'value' attribute on TypeKeyCell (id=transactionType_Cell) at TDIC_PlanDataUploadGLTransactionMappingLV.pcf: line 28, column 44
    function sortValue_2 (glMapping :  tdic.util.dataloader.data.plandata.GLTransactionNameMappingData) : java.lang.Object {
      return glMapping.OriginalTransactionName
    }
    
    // 'value' attribute on TextCell (id=mappedName_Cell) at TDIC_PlanDataUploadGLTransactionMappingLV.pcf: line 33, column 41
    function sortValue_3 (glMapping :  tdic.util.dataloader.data.plandata.GLTransactionNameMappingData) : java.lang.Object {
      return glMapping.MappedTransactionName
    }
    
    // 'value' attribute on RowIterator (id=GLMapping) at TDIC_PlanDataUploadGLTransactionMappingLV.pcf: line 15, column 110
    function value_14 () : java.util.ArrayList<tdic.util.dataloader.data.plandata.GLTransactionNameMappingData> {
      return processor.GLTransactionNameMappingArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at TDIC_PlanDataUploadGLTransactionMappingLV.pcf: line 23, column 46
    function visible_1 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}