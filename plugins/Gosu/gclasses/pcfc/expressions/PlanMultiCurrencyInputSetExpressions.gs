package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/multicurrencyplans/PlanMultiCurrencyInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanMultiCurrencyInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/multicurrencyplans/PlanMultiCurrencyInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends PlanMultiCurrencyInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'checkBoxVisible' attribute on RowIterator (id=PlanCurrencies) at PlanMultiCurrencyInputSet.pcf: line 58, column 45
    function checkBoxVisible_14 () : java.lang.Boolean {
      return pc.New
    }
    
    // 'outputConversion' attribute on TypeKeyCell (id=Currency_Cell) at PlanMultiCurrencyInputSet.pcf: line 67, column 44
    function outputConversion_10 (VALUE :  typekey.Currency) : java.lang.String {
      return pc.getDisplayName("PlanUI")
    }
    
    // 'value' attribute on TypeKeyCell (id=Currency_Cell) at PlanMultiCurrencyInputSet.pcf: line 67, column 44
    function valueRoot_12 () : java.lang.Object {
      return pc
    }
    
    // 'value' attribute on TypeKeyCell (id=Currency_Cell) at PlanMultiCurrencyInputSet.pcf: line 67, column 44
    function value_11 () : typekey.Currency {
      return pc.Currency
    }
    
    property get pc () : entity.PlanCurrency {
      return getIteratedValue(1) as entity.PlanCurrency
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/multicurrencyplans/PlanMultiCurrencyInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PlanMultiCurrencyInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=currencySelection) at PlanMultiCurrencyInputSet.pcf: line 38, column 77
    function label_5 () : java.lang.Object {
      return (newCurrency as Currency).DisplayName
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=currencySelection) at PlanMultiCurrencyInputSet.pcf: line 38, column 77
    function toCreateAndAdd_6 (CheckedValues :  Object[]) : java.lang.Object {
      return addFeesForCurrency(newCurrency as Currency)
    }
    
    property get newCurrency () : java.lang.Object {
      return getIteratedValue(1) as java.lang.Object
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/multicurrencyplans/PlanMultiCurrencyInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanMultiCurrencyInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at PlanMultiCurrencyInputSet.pcf: line 13, column 23
    function initialValue_0 () : Boolean {
      return isMultiCurrencyMode()
    }
    
    // 'toRemove' attribute on RowIterator (id=PlanCurrencies) at PlanMultiCurrencyInputSet.pcf: line 58, column 45
    function toRemove_15 (pc :  entity.PlanCurrency) : void {
      plan.removeFromCurrencies(pc.Currency)
    }
    
    // 'validationExpression' attribute on ListViewInput (id=CurrencyListInput) at PlanMultiCurrencyInputSet.pcf: line 24, column 35
    function validationExpression_17 () : java.lang.Object {
      return plan.Currencies.IsEmpty ? DisplayKey.get("Web.PlanMultiCurrencyInputSet.Currencies.Error") : null
    }
    
    // 'value' attribute on RowIterator (id=PlanCurrencies) at PlanMultiCurrencyInputSet.pcf: line 58, column 45
    function value_16 () : entity.PlanCurrency[] {
      return plan.PlanCurrencies //Changing this to be backed by a query will almost certainly cause this page to break because of the use of forceRefresh property on the iterator
    }
    
    // 'value' attribute on TextInput (id=Currency_Input) at PlanMultiCurrencyInputSet.pcf: line 19, column 37
    function value_2 () : typekey.Currency {
      return plan.getCurrencies()[0]
    }
    
    // 'value' attribute on AddMenuItemIterator at PlanMultiCurrencyInputSet.pcf: line 33, column 46
    function value_7 () : List<Currency> {
      return typekey.Currency.AllTypeKeys.subtract(Arrays.asList(plan.Currencies)).toList()
    }
    
    // 'valueType' attribute on AddMenuItemIterator at PlanMultiCurrencyInputSet.pcf: line 33, column 46
    function verifyValueTypeIsAllowedType_8 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on AddMenuItemIterator at PlanMultiCurrencyInputSet.pcf: line 33, column 46
    function verifyValueTypeIsAllowedType_8 ($$arg :  gw.api.iterator.IteratorBackingData) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on AddMenuItemIterator at PlanMultiCurrencyInputSet.pcf: line 33, column 46
    function verifyValueTypeIsAllowedType_8 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on AddMenuItemIterator at PlanMultiCurrencyInputSet.pcf: line 33, column 46
    function verifyValueType_9 () : void {
      var __valueTypeArg : List<Currency>
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the valueType is not a valid type for use with an iterator
      // The valueType for an iterator must be an array or extend from List or IQueryBeanResult
      verifyValueTypeIsAllowedType_8(__valueTypeArg)
    }
    
    // 'visible' attribute on TextInput (id=Currency_Input) at PlanMultiCurrencyInputSet.pcf: line 19, column 37
    function visible_1 () : java.lang.Boolean {
      return !multiCurrencyMode
    }
    
    // 'visible' attribute on ListViewInput (id=CurrencyListInput) at PlanMultiCurrencyInputSet.pcf: line 24, column 35
    function visible_18 () : java.lang.Boolean {
      return multiCurrencyMode
    }
    
    property get multiCurrencyMode () : Boolean {
      return getVariableValue("multiCurrencyMode", 0) as Boolean
    }
    
    property set multiCurrencyMode ($arg :  Boolean) {
      setVariableValue("multiCurrencyMode", 0, $arg)
    }
    
    property get plan () : MultiCurrencyPlan {
      return getRequireValue("plan", 0) as MultiCurrencyPlan
    }
    
    property set plan ($arg :  MultiCurrencyPlan) {
      setRequireValue("plan", 0, $arg)
    }
    
    function isMultiCurrencyMode(): boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode() and Currency.getTypeKeys(true).Count > 1
    }
    
    function addFeesForCurrency(currency: Currency): PlanCurrency {
      plan.addToCurrencies(currency)
      return plan.getPlanCurrencies().singleWhere(\elt -> elt.Currency == currency)
    }
    
    
  }
  
  
}