package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DisbursementSearchResultsLV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DisbursementSearchResultsLV_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/search/DisbursementSearchResultsLV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DisbursementSearchResultsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateCell (id=CloseDate_Cell) at DisbursementSearchResultsLV.default.pcf: line 22, column 53
    function sortValue_0 (disbursementSearchView :  entity.DisbursementSearchView) : java.lang.Object {
      return disbursementSearchView.CloseDate
    }
    
    // 'value' attribute on TextCell (id=CheckNumber_Cell) at DisbursementSearchResultsLV.default.pcf: line 26, column 55
    function sortValue_1 (disbursementSearchView :  entity.DisbursementSearchView) : java.lang.Object {
      return disbursementSearchView.CheckNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Reason_Cell) at DisbursementSearchResultsLV.default.pcf: line 32, column 39
    function sortValue_2 (disbursementSearchView :  entity.DisbursementSearchView) : java.lang.Object {
      return disbursementSearchView.Reason
    }
    
    // 'value' attribute on TextCell (id=Payee_Cell) at DisbursementSearchResultsLV.default.pcf: line 36, column 49
    function sortValue_3 (disbursementSearchView :  entity.DisbursementSearchView) : java.lang.Object {
      return disbursementSearchView.PayTo
    }
    
    // 'sortBy' attribute on MonetaryAmountCell (id=Amount_Cell) at DisbursementSearchResultsLV.default.pcf: line 42, column 94
    function sortValue_4 (disbursementSearchView :  entity.DisbursementSearchView) : java.lang.Object {
      return disbursementSearchView.Currency
    }
    
    // 'sortBy' attribute on MonetaryAmountCell (id=Amount_Cell) at DisbursementSearchResultsLV.default.pcf: line 42, column 94
    function sortValue_5 (disbursementSearchView :  entity.DisbursementSearchView) : java.lang.Object {
      return  disbursementSearchView.Amount
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentMethod_Cell) at DisbursementSearchResultsLV.default.pcf: line 47, column 46
    function sortValue_6 (disbursementSearchView :  entity.DisbursementSearchView) : java.lang.Object {
      return disbursementSearchView.PaymentMethod
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DisbursementSearchResultsLV.default.pcf: line 52, column 51
    function sortValue_7 (disbursementSearchView :  entity.DisbursementSearchView) : java.lang.Object {
      return disbursementSearchView.Status
    }
    
    // 'value' attribute on RowIterator at DisbursementSearchResultsLV.default.pcf: line 16, column 89
    function value_37 () : gw.api.database.IQueryBeanResult<entity.DisbursementSearchView> {
      return disbursementSearchViews
    }
    
    property get disbursementSearchViews () : gw.api.database.IQueryBeanResult<DisbursementSearchView> {
      return getRequireValue("disbursementSearchViews", 0) as gw.api.database.IQueryBeanResult<DisbursementSearchView>
    }
    
    property set disbursementSearchViews ($arg :  gw.api.database.IQueryBeanResult<DisbursementSearchView>) {
      setRequireValue("disbursementSearchViews", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/DisbursementSearchResultsLV.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DisbursementSearchResultsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TypeKeyCell (id=Reason_Cell) at DisbursementSearchResultsLV.default.pcf: line 32, column 39
    function action_14 () : void {
      DisbursementDetail.go(disbursementSearchView.Disbursement, false)
    }
    
    // 'action' attribute on TypeKeyCell (id=Reason_Cell) at DisbursementSearchResultsLV.default.pcf: line 32, column 39
    function action_dest_15 () : pcf.api.Destination {
      return pcf.DisbursementDetail.createDestination(disbursementSearchView.Disbursement, false)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at DisbursementSearchResultsLV.default.pcf: line 42, column 94
    function currency_23 () : typekey.Currency {
      return disbursementSearchView.Currency
    }
    
    // 'value' attribute on DateCell (id=CloseDate_Cell) at DisbursementSearchResultsLV.default.pcf: line 22, column 53
    function valueRoot_9 () : java.lang.Object {
      return disbursementSearchView
    }
    
    // 'value' attribute on TextCell (id=CheckNumber_Cell) at DisbursementSearchResultsLV.default.pcf: line 26, column 55
    function value_11 () : java.lang.String {
      return disbursementSearchView.CheckNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Reason_Cell) at DisbursementSearchResultsLV.default.pcf: line 32, column 39
    function value_16 () : typekey.Reason {
      return disbursementSearchView.Reason
    }
    
    // 'value' attribute on TextCell (id=Payee_Cell) at DisbursementSearchResultsLV.default.pcf: line 36, column 49
    function value_19 () : java.lang.String {
      return disbursementSearchView.PayTo
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at DisbursementSearchResultsLV.default.pcf: line 42, column 94
    function value_22 () : gw.pl.currency.MonetaryAmount {
      return disbursementSearchView.Amount.ofCurrency(disbursementSearchView.Currency)
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentMethod_Cell) at DisbursementSearchResultsLV.default.pcf: line 47, column 46
    function value_25 () : typekey.PaymentMethod {
      return disbursementSearchView.PaymentMethod
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DisbursementSearchResultsLV.default.pcf: line 52, column 51
    function value_28 () : typekey.DisbursementStatus {
      return disbursementSearchView.Status
    }
    
    // 'value' attribute on TextCell (id=Suspense_Cell) at DisbursementSearchResultsLV.default.pcf: line 58, column 47
    function value_31 () : entity.SuspensePayment {
      return disbursementSearchView.Disbursement typeis SuspenseDisbursement ? disbursementSearchView.Disbursement.SuspensePayment : null
    }
    
    // 'value' attribute on TextCell (id=Collateral_Cell) at DisbursementSearchResultsLV.default.pcf: line 64, column 25
    function value_33 () : java.lang.String {
      return disbursementSearchView.Disbursement typeis CollateralDisbursement ? disbursementSearchView.Disbursement.Collateral.Account.AccountNumber : null
    }
    
    // 'value' attribute on TextCell (id=Producer_Cell) at DisbursementSearchResultsLV.default.pcf: line 70, column 40
    function value_35 () : entity.Producer {
      return disbursementSearchView.Disbursement typeis AgencyDisbursement ? disbursementSearchView.Disbursement.Producer : null
    }
    
    // 'value' attribute on DateCell (id=CloseDate_Cell) at DisbursementSearchResultsLV.default.pcf: line 22, column 53
    function value_8 () : java.util.Date {
      return disbursementSearchView.CloseDate
    }
    
    property get disbursementSearchView () : entity.DisbursementSearchView {
      return getIteratedValue(1) as entity.DisbursementSearchView
    }
    
    
  }
  
  
}