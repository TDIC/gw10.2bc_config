package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailLedger.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailLedgerExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailLedger.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailLedgerExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=PolicyDetailLedger) at PolicyDetailLedger.pcf: line 9, column 70
    static function canVisit_2 (plcyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcyledgerview
    }
    
    // 'def' attribute on ScreenRef at PolicyDetailLedger.pcf: line 16, column 129
    function def_onEnter_0 (def :  pcf.LedgerScreen) : void {
      def.onEnter(plcyPeriod, gw.api.web.policy.PolicyPeriodUtil.findAllPolicyCommissionsWithChargeDetails(plcyPeriod))
    }
    
    // 'def' attribute on ScreenRef at PolicyDetailLedger.pcf: line 16, column 129
    function def_refreshVariables_1 (def :  pcf.LedgerScreen) : void {
      def.refreshVariables(plcyPeriod, gw.api.web.policy.PolicyPeriodUtil.findAllPolicyCommissionsWithChargeDetails(plcyPeriod))
    }
    
    // Page (id=PolicyDetailLedger) at PolicyDetailLedger.pcf: line 9, column 70
    static function parent_3 (plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyGroup.createDestination(plcyPeriod)
    }
    
    override property get CurrentLocation () : pcf.PolicyDetailLedger {
      return super.CurrentLocation as pcf.PolicyDetailLedger
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 0, $arg)
    }
    
    
  }
  
  
}