package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/invoice/CloneReturnPremiumPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CloneReturnPremiumPlanExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/invoice/CloneReturnPremiumPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CloneReturnPremiumPlanExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (returnPremiumPlan :  ReturnPremiumPlan) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=CloneReturnPremiumPlan) at CloneReturnPremiumPlan.pcf: line 13, column 93
    function afterCancel_3 () : void {
      ReturnPremiumPlanDetail.go(returnPremiumPlan)
    }
    
    // 'afterCancel' attribute on Page (id=CloneReturnPremiumPlan) at CloneReturnPremiumPlan.pcf: line 13, column 93
    function afterCancel_dest_4 () : pcf.api.Destination {
      return pcf.ReturnPremiumPlanDetail.createDestination(returnPremiumPlan)
    }
    
    // 'afterCommit' attribute on Page (id=CloneReturnPremiumPlan) at CloneReturnPremiumPlan.pcf: line 13, column 93
    function afterCommit_5 (TopLocation :  pcf.api.Location) : void {
      ReturnPremiumPlanDetail.go(clonedReturnPremiumPlan)
    }
    
    // 'def' attribute on ScreenRef at CloneReturnPremiumPlan.pcf: line 24, column 69
    function def_onEnter_1 (def :  pcf.ReturnPremiumPlanDetailScreen) : void {
      def.onEnter(clonedReturnPremiumPlan)
    }
    
    // 'def' attribute on ScreenRef at CloneReturnPremiumPlan.pcf: line 24, column 69
    function def_refreshVariables_2 (def :  pcf.ReturnPremiumPlanDetailScreen) : void {
      def.refreshVariables(clonedReturnPremiumPlan)
    }
    
    // 'initialValue' attribute on Variable at CloneReturnPremiumPlan.pcf: line 22, column 40
    function initialValue_0 () : entity.ReturnPremiumPlan {
      return returnPremiumPlan.makeClone() as ReturnPremiumPlan
    }
    
    // 'parent' attribute on Page (id=CloneReturnPremiumPlan) at CloneReturnPremiumPlan.pcf: line 13, column 93
    static function parent_6 (returnPremiumPlan :  ReturnPremiumPlan) : pcf.api.Destination {
      return pcf.ReturnPremiumPlans.createDestination()
    }
    
    // 'title' attribute on Page (id=CloneReturnPremiumPlan) at CloneReturnPremiumPlan.pcf: line 13, column 93
    static function title_7 (returnPremiumPlan :  ReturnPremiumPlan) : java.lang.Object {
      return DisplayKey.get("Web.CloneReturnPremiumPlan.Title", returnPremiumPlan)
    }
    
    override property get CurrentLocation () : pcf.CloneReturnPremiumPlan {
      return super.CurrentLocation as pcf.CloneReturnPremiumPlan
    }
    
    property get clonedReturnPremiumPlan () : entity.ReturnPremiumPlan {
      return getVariableValue("clonedReturnPremiumPlan", 0) as entity.ReturnPremiumPlan
    }
    
    property set clonedReturnPremiumPlan ($arg :  entity.ReturnPremiumPlan) {
      setVariableValue("clonedReturnPremiumPlan", 0, $arg)
    }
    
    property get returnPremiumPlan () : ReturnPremiumPlan {
      return getVariableValue("returnPremiumPlan", 0) as ReturnPremiumPlan
    }
    
    property set returnPremiumPlan ($arg :  ReturnPremiumPlan) {
      setVariableValue("returnPremiumPlan", 0, $arg)
    }
    
    
  }
  
  
}