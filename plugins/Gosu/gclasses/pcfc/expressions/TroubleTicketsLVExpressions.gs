package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TroubleTicketsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TroubleTicketsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Number_Cell) at TroubleTicketsLV.pcf: line 49, column 54
    function action_20 () : void {
      TroubleTicketDetailsPopup.push(troubleTicket)
    }
    
    // 'action' attribute on TextCell (id=Number_Cell) at TroubleTicketsLV.pcf: line 49, column 54
    function action_dest_21 () : pcf.api.Destination {
      return pcf.TroubleTicketDetailsPopup.createDestination(troubleTicket)
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at TroubleTicketsLV.pcf: line 54, column 45
    function fontColor_25 () : java.lang.Object {
      return troubleTicket.Overdue == true ? gw.api.web.color.GWColor.THEME_URGENCY_HIGH : null
    }
    
    // 'iconColor' attribute on BooleanRadioCell (id=Escalated_Cell) at TroubleTicketsLV.pcf: line 43, column 44
    function iconColor_18 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_URGENCY_HIGH
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at TroubleTicketsLV.pcf: line 43, column 44
    function valueRoot_17 () : java.lang.Object {
      return troubleTicket
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at TroubleTicketsLV.pcf: line 43, column 44
    function value_16 () : java.lang.Boolean {
      return troubleTicket.Escalated
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at TroubleTicketsLV.pcf: line 49, column 54
    function value_22 () : java.lang.String {
      return troubleTicket.TroubleTicketNumber
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at TroubleTicketsLV.pcf: line 54, column 45
    function value_26 () : java.util.Date {
      return troubleTicket.TargetDate
    }
    
    // 'value' attribute on DateCell (id=CreateTime_Cell) at TroubleTicketsLV.pcf: line 58, column 45
    function value_31 () : java.util.Date {
      return troubleTicket.CreateTime
    }
    
    // 'value' attribute on TextCell (id=CreateUser_Cell) at TroubleTicketsLV.pcf: line 64, column 41
    function value_34 () : java.lang.Object {
      return troubleTicket.AssignedTo
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at TroubleTicketsLV.pcf: line 70, column 25
    function value_37 () : typekey.Priority {
      return troubleTicket.Priority
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at TroubleTicketsLV.pcf: line 75, column 25
    function value_40 () : java.lang.String {
      return troubleTicket.TicketStatus
    }
    
    // 'value' attribute on TextCell (id=Title_Cell) at TroubleTicketsLV.pcf: line 80, column 40
    function value_43 () : java.lang.String {
      return troubleTicket.Title
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at TroubleTicketsLV.pcf: line 54, column 45
    function verifyFontColorIsAllowedType_28 ($$arg :  gw.api.web.color.GWColor) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at TroubleTicketsLV.pcf: line 54, column 45
    function verifyFontColorIsAllowedType_28 ($$arg :  java.lang.String) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at TroubleTicketsLV.pcf: line 54, column 45
    function verifyFontColor_29 () : void {
      var __fontColorArg = troubleTicket.Overdue == true ? gw.api.web.color.GWColor.THEME_URGENCY_HIGH : null
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the fontColor return type is not a valid type for use with a ValueWidget's fontColor
      // It needs to return a String or a GWColor.
      verifyFontColorIsAllowedType_28(__fontColorArg)
    }
    
    property get troubleTicket () : entity.TroubleTicket {
      return getIteratedValue(1) as entity.TroubleTicket
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TroubleTicketsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'filter' attribute on ToolbarFilterOption at TroubleTicketsLV.pcf: line 19, column 61
    function filter_0 () : gw.api.filters.IFilter {
      return new gw.api.util.CoreFilters.AllFilter()
    }
    
    // 'filter' attribute on ToolbarFilterOption at TroubleTicketsLV.pcf: line 22, column 33
    function filter_1 () : gw.api.filters.IFilter {
      return new gw.api.web.troubleticket.TroubleTicketListFilterSet.AllOpen()
    }
    
    // 'filter' attribute on ToolbarFilterOption at TroubleTicketsLV.pcf: line 24, column 97
    function filter_2 () : gw.api.filters.IFilter {
      return new gw.api.web.troubleticket.TroubleTicketListFilterSet.AllOpenedThisWeek()
    }
    
    // 'filter' attribute on ToolbarFilterOption at TroubleTicketsLV.pcf: line 26, column 93
    function filter_3 () : gw.api.filters.IFilter {
      return new gw.api.web.troubleticket.TroubleTicketListFilterSet.AllOpenUrgent()
    }
    
    // 'filter' attribute on ToolbarFilterOption at TroubleTicketsLV.pcf: line 28, column 95
    function filter_4 () : gw.api.filters.IFilter {
      return new gw.api.web.troubleticket.TroubleTicketListFilterSet.AllOpenLastYear()
    }
    
    // 'filter' attribute on ToolbarFilterOption at TroubleTicketsLV.pcf: line 30, column 101
    function filter_5 () : gw.api.filters.IFilter {
      return new gw.api.web.troubleticket.TroubleTicketListFilterSet.AllClosedInLast30Days()
    }
    
    // 'filter' attribute on ToolbarFilterOption at TroubleTicketsLV.pcf: line 32, column 97
    function filter_6 () : gw.api.filters.IFilter {
      return new gw.api.web.troubleticket.TroubleTicketListFilterSet.AllClosedLastYear()
    }
    
    // 'iconColor' attribute on BooleanRadioCell (id=Escalated_Cell) at TroubleTicketsLV.pcf: line 43, column 44
    function iconColor_7 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_URGENCY_HIGH
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at TroubleTicketsLV.pcf: line 54, column 45
    function sortValue_10 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.TargetDate
    }
    
    // 'value' attribute on DateCell (id=CreateTime_Cell) at TroubleTicketsLV.pcf: line 58, column 45
    function sortValue_11 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.CreateTime
    }
    
    // 'value' attribute on TextCell (id=CreateUser_Cell) at TroubleTicketsLV.pcf: line 64, column 41
    function sortValue_12 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.AssignedTo
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at TroubleTicketsLV.pcf: line 70, column 25
    function sortValue_13 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.Priority
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at TroubleTicketsLV.pcf: line 75, column 25
    function sortValue_14 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.TicketStatus
    }
    
    // 'value' attribute on TextCell (id=Title_Cell) at TroubleTicketsLV.pcf: line 80, column 40
    function sortValue_15 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.Title
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at TroubleTicketsLV.pcf: line 43, column 44
    function sortValue_8 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.Escalated
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at TroubleTicketsLV.pcf: line 49, column 54
    function sortValue_9 (troubleTicket :  entity.TroubleTicket) : java.lang.Object {
      return troubleTicket.TroubleTicketNumber
    }
    
    // 'value' attribute on RowIterator at TroubleTicketsLV.pcf: line 15, column 42
    function value_46 () : entity.TroubleTicket[] {
      return troubleTickets
    }
    
    property get troubleTickets () : TroubleTicket[] {
      return getRequireValue("troubleTickets", 0) as TroubleTicket[]
    }
    
    property set troubleTickets ($arg :  TroubleTicket[]) {
      setRequireValue("troubleTickets", 0, $arg)
    }
    
    
  }
  
  
}