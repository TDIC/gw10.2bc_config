package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentAdvanceConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewCommissionPaymentAdvanceConfirmationScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentAdvanceConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewCommissionPaymentAdvanceConfirmationScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentAdvanceConfirmationScreen.pcf: line 41, column 51
    function def_onEnter_13 (def :  pcf.ApprovalActivityDV) : void {
      def.onEnter(approvalActivity)
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentAdvanceConfirmationScreen.pcf: line 23, column 67
    function def_onEnter_2 (def :  pcf.NewCommissionPaymentAdvanceDV) : void {
      def.onEnter(advancePayment, false)
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentAdvanceConfirmationScreen.pcf: line 41, column 51
    function def_refreshVariables_14 (def :  pcf.ApprovalActivityDV) : void {
      def.refreshVariables(approvalActivity)
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentAdvanceConfirmationScreen.pcf: line 23, column 67
    function def_refreshVariables_3 (def :  pcf.NewCommissionPaymentAdvanceDV) : void {
      def.refreshVariables(advancePayment, false)
    }
    
    // 'initialValue' attribute on Variable at NewCommissionPaymentAdvanceConfirmationScreen.pcf: line 14, column 43
    function initialValue_0 () : entity.AdvcCmsnApprActivity {
      return advancePayment.getOpenApprovalActivity()
    }
    
    // 'label' attribute on TextInput (id=PaymentTime_Input) at NewCommissionPaymentAdvanceConfirmationScreen.pcf: line 32, column 87
    function label_5 () : java.lang.Object {
      return advancePayment.PaymentTime == CommissionPaymentTime.TC_IMMEDIATELY ? DisplayKey.get("Web.NewCommissionPaymentTimeDV.Immediately") : DisplayKey.get("Web.NewCommissionPaymentTimeDV.NextPayment")
    }
    
    // 'value' attribute on DateInput (id=PaymentDate_Input) at NewCommissionPaymentAdvanceConfirmationScreen.pcf: line 37, column 87
    function valueRoot_10 () : java.lang.Object {
      return advancePayment
    }
    
    // 'value' attribute on DateInput (id=PaymentDate_Input) at NewCommissionPaymentAdvanceConfirmationScreen.pcf: line 37, column 87
    function value_9 () : java.util.Date {
      return advancePayment.PayOn
    }
    
    // 'visible' attribute on AlertBar (id=ApprovalActivityAlertBar) at NewCommissionPaymentAdvanceConfirmationScreen.pcf: line 21, column 43
    function visible_1 () : java.lang.Boolean {
      return approvalActivity != null
    }
    
    // 'visible' attribute on TextInput (id=PaymentTime_Input) at NewCommissionPaymentAdvanceConfirmationScreen.pcf: line 32, column 87
    function visible_4 () : java.lang.Boolean {
      return advancePayment.PaymentTime != CommissionPaymentTime.TC_PAYONDATE
    }
    
    // 'visible' attribute on DateInput (id=PaymentDate_Input) at NewCommissionPaymentAdvanceConfirmationScreen.pcf: line 37, column 87
    function visible_8 () : java.lang.Boolean {
      return advancePayment.PaymentTime == CommissionPaymentTime.TC_PAYONDATE
    }
    
    property get advancePayment () : AdvanceCmsnPayment {
      return getRequireValue("advancePayment", 0) as AdvanceCmsnPayment
    }
    
    property set advancePayment ($arg :  AdvanceCmsnPayment) {
      setRequireValue("advancePayment", 0, $arg)
    }
    
    property get approvalActivity () : entity.AdvcCmsnApprActivity {
      return getVariableValue("approvalActivity", 0) as entity.AdvcCmsnApprActivity
    }
    
    property set approvalActivity ($arg :  entity.AdvcCmsnApprActivity) {
      setVariableValue("approvalActivity", 0, $arg)
    }
    
    
  }
  
  
}