package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffWizardConfirmationStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewNegativeWriteoffWizardConfirmationStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewNegativeWriteoffWizardConfirmationStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewNegativeWriteoffWizardConfirmationStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffWizardConfirmationStepScreen.pcf: line 45, column 28
    function def_onEnter_8 (def :  pcf.ApprovalActivityDV) : void {
      def.onEnter(approvalActivity)
    }
    
    // 'def' attribute on PanelRef at NewNegativeWriteoffWizardConfirmationStepScreen.pcf: line 45, column 28
    function def_refreshVariables_9 (def :  pcf.ApprovalActivityDV) : void {
      def.refreshVariables(approvalActivity)
    }
    
    // 'initialValue' attribute on Variable at NewNegativeWriteoffWizardConfirmationStepScreen.pcf: line 15, column 44
    function initialValue_0 () : entity.ActivityCreatedByAppr {
      return uiWriteoff.WriteOff.OpenApprovalActivity
    }
    
    // 'initialValue' attribute on Variable at NewNegativeWriteoffWizardConfirmationStepScreen.pcf: line 19, column 22
    function initialValue_1 () : String {
      return (uiWriteoff.WriteOff typeis AcctNegativeWriteoff) ? uiWriteoff.WriteOff.UnappliedFund.DisplayName : uiWriteoff.WriteOff.TAccountOwner.DisplayName
    }
    
    // 'initialValue' attribute on Variable at NewNegativeWriteoffWizardConfirmationStepScreen.pcf: line 23, column 23
    function initialValue_2 () : boolean {
      return uiWriteoff != null && uiWriteoff.Amount != null
    }
    
    // 'label' attribute on Label at NewNegativeWriteoffWizardConfirmationStepScreen.pcf: line 40, column 138
    function label_5 () : java.lang.String {
      return DisplayKey.get("Web.NewNegativeWriteoffWizard.Confirmation", uiWriteoff.Amount.render(), targetForWriteoff)
    }
    
    // 'visible' attribute on AlertBar (id=NullAmountAlertBar) at NewNegativeWriteoffWizardConfirmationStepScreen.pcf: line 30, column 29
    function visible_3 () : java.lang.Boolean {
      return !hasAmount
    }
    
    // 'visible' attribute on AlertBar (id=ApprovalActivityAlertBar) at NewNegativeWriteoffWizardConfirmationStepScreen.pcf: line 34, column 43
    function visible_4 () : java.lang.Boolean {
      return approvalActivity != null
    }
    
    // 'visible' attribute on DetailViewPanel (id=ConfirmDV) at NewNegativeWriteoffWizardConfirmationStepScreen.pcf: line 37, column 27
    function visible_6 () : java.lang.Boolean {
      return hasAmount
    }
    
    property get approvalActivity () : entity.ActivityCreatedByAppr {
      return getVariableValue("approvalActivity", 0) as entity.ActivityCreatedByAppr
    }
    
    property set approvalActivity ($arg :  entity.ActivityCreatedByAppr) {
      setVariableValue("approvalActivity", 0, $arg)
    }
    
    property get hasAmount () : boolean {
      return getVariableValue("hasAmount", 0) as java.lang.Boolean
    }
    
    property set hasAmount ($arg :  boolean) {
      setVariableValue("hasAmount", 0, $arg)
    }
    
    property get targetForWriteoff () : String {
      return getVariableValue("targetForWriteoff", 0) as String
    }
    
    property set targetForWriteoff ($arg :  String) {
      setVariableValue("targetForWriteoff", 0, $arg)
    }
    
    property get uiWriteoff () : gw.api.web.accounting.UIWriteOffCreation {
      return getRequireValue("uiWriteoff", 0) as gw.api.web.accounting.UIWriteOffCreation
    }
    
    property set uiWriteoff ($arg :  gw.api.web.accounting.UIWriteOffCreation) {
      setRequireValue("uiWriteoff", 0, $arg)
    }
    
    
  }
  
  
}