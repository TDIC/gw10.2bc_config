package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/PoliciesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PoliciesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/PoliciesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PoliciesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at PoliciesLV.pcf: line 21, column 25
    function valueRoot_1 () : java.lang.Object {
      return policy
    }
    
    // 'value' attribute on TextCell (id=Insured_Cell) at PoliciesLV.pcf: line 27, column 51
    function valueRoot_4 () : java.lang.Object {
      return policy.LatestPolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at PoliciesLV.pcf: line 21, column 25
    function value_0 () : java.lang.String {
      return policy.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Insured_Cell) at PoliciesLV.pcf: line 27, column 51
    function value_3 () : entity.PolicyPeriodContact {
      return policy.LatestPolicyPeriod.PrimaryInsured
    }
    
    property get policy () : entity.Policy {
      return getIteratedValue(1) as entity.Policy
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/PoliciesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PoliciesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on RowIterator at PoliciesLV.pcf: line 14, column 73
    function value_6 () : gw.api.database.IQueryBeanResult<entity.Policy> {
      return policies
    }
    
    property get policies () : gw.api.database.IQueryBeanResult<Policy> {
      return getRequireValue("policies", 0) as gw.api.database.IQueryBeanResult<Policy>
    }
    
    property set policies ($arg :  gw.api.database.IQueryBeanResult<Policy>) {
      setRequireValue("policies", 0, $arg)
    }
    
    
  }
  
  
}