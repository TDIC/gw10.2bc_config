<% uses pcf.* %>
<% uses entity.* %>
<% uses typekey.* %>
<% uses gw.api.locale.DisplayKey %>
<%@ params(final scrapeXml : String[]) %>

    <!-- Clear scrapeXml after setting the iframe source so that Unity only
         pops once. Otherwise the frame would pop again on page refresh. -->
		<iframe src="<%=scrapeXml[0]%>" style="display:none"></iframe>
		<%scrapeXml[0] = ""%>
	
      