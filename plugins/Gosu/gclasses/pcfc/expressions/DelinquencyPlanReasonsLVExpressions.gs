package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanReasonsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyPlanReasonsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanReasonsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyPlanReasonsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'autoAdd' attribute on RowIterator at DelinquencyPlanReasonsLV.pcf: line 30, column 50
    function autoAdd_3 () : java.lang.Boolean {
      return !planNotInUse and (dlnqPlan.DelinquencyPlanReasons.length == 0)
    }
    
    // 'value' attribute on TypeKeyCell (id=DelinquencyReason_Cell) at DelinquencyPlanReasonsLV.pcf: line 43, column 25
    function sortValue_0 (planReason :  entity.DelinquencyPlanReason) : java.lang.Object {
      return planReason.DelinquencyReason
    }
    
    // 'value' attribute on RangeCell (id=WorkflowType_Cell) at DelinquencyPlanReasonsLV.pcf: line 51, column 25
    function sortValue_1 (planReason :  entity.DelinquencyPlanReason) : java.lang.Object {
      return planReason.WorkflowType
    }
    
    // 'value' attribute on TextCell (id=DelinquencyPlanEvents_Cell) at DelinquencyPlanReasonsLV.pcf: line 56, column 56
    function sortValue_2 (planReason :  entity.DelinquencyPlanReason) : java.lang.Object {
      return listPlanEventsForReason(planReason)
    }
    
    // 'toAdd' attribute on RowIterator at DelinquencyPlanReasonsLV.pcf: line 30, column 50
    function toAdd_16 (planReason :  entity.DelinquencyPlanReason) : void {
      dlnqPlan.addToDelinquencyPlanReasons(planReason)
    }
    
    // 'toRemove' attribute on RowIterator at DelinquencyPlanReasonsLV.pcf: line 30, column 50
    function toRemove_17 (planReason :  entity.DelinquencyPlanReason) : void {
      dlnqPlan.removeFromDelinquencyPlanReasons(planReason)
    }
    
    // 'value' attribute on RowIterator at DelinquencyPlanReasonsLV.pcf: line 30, column 50
    function value_18 () : entity.DelinquencyPlanReason[] {
      return dlnqPlan.DelinquencyPlanReasons
    }
    
    property get dlnqPlan () : DelinquencyPlan {
      return getRequireValue("dlnqPlan", 0) as DelinquencyPlan
    }
    
    property set dlnqPlan ($arg :  DelinquencyPlan) {
      setRequireValue("dlnqPlan", 0, $arg)
    }
    
    property get planNotInUse () : boolean {
      return getRequireValue("planNotInUse", 0) as java.lang.Boolean
    }
    
    property set planNotInUse ($arg :  boolean) {
      setRequireValue("planNotInUse", 0, $arg)
    }
    
    function isFieldEditable(planReason: DelinquencyPlanReason): Boolean {
      if (planReason.New) {
        return true;
      }
      return planNotInUse;
    }
    
    function listPlanEventsForReason(planReason: DelinquencyPlanReason): String {
      var delinquencyPlanEvents = planReason.OrderedEvents
      return delinquencyPlanEvents == null ? "" : delinquencyPlanEvents.reduce(null as String,
          \q, d -> ( q == null ? "" : q + ", " ) + d.EventName.DisplayName)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanReasonsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DelinquencyPlanReasonsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'checkBoxVisible' attribute on RowIterator at DelinquencyPlanReasonsLV.pcf: line 30, column 50
    function checkBoxVisible_15 () : java.lang.Boolean {
      return isFieldEditable(planReason)
    }
    
    // 'valueRange' attribute on RangeCell (id=WorkflowType_Cell) at DelinquencyPlanReasonsLV.pcf: line 51, column 25
    function valueRange_9 () : java.lang.Object {
      return gw.api.web.delinquency.DelinquencyPlanUtil.getDelinquencyWorkflows()
    }
    
    // 'value' attribute on TypeKeyCell (id=DelinquencyReason_Cell) at DelinquencyPlanReasonsLV.pcf: line 43, column 25
    function valueRoot_5 () : java.lang.Object {
      return planReason
    }
    
    // 'value' attribute on TextCell (id=DelinquencyPlanEvents_Cell) at DelinquencyPlanReasonsLV.pcf: line 56, column 56
    function value_13 () : java.lang.String {
      return listPlanEventsForReason(planReason)
    }
    
    // 'value' attribute on TypeKeyCell (id=DelinquencyReason_Cell) at DelinquencyPlanReasonsLV.pcf: line 43, column 25
    function value_4 () : typekey.DelinquencyReason {
      return planReason.DelinquencyReason
    }
    
    // 'value' attribute on RangeCell (id=WorkflowType_Cell) at DelinquencyPlanReasonsLV.pcf: line 51, column 25
    function value_7 () : typekey.Workflow {
      return planReason.WorkflowType
    }
    
    // 'valueRange' attribute on RangeCell (id=WorkflowType_Cell) at DelinquencyPlanReasonsLV.pcf: line 51, column 25
    function verifyValueRangeIsAllowedType_10 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=WorkflowType_Cell) at DelinquencyPlanReasonsLV.pcf: line 51, column 25
    function verifyValueRangeIsAllowedType_10 ($$arg :  typekey.Workflow[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=WorkflowType_Cell) at DelinquencyPlanReasonsLV.pcf: line 51, column 25
    function verifyValueRange_11 () : void {
      var __valueRangeArg = gw.api.web.delinquency.DelinquencyPlanUtil.getDelinquencyWorkflows()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_10(__valueRangeArg)
    }
    
    property get planReason () : entity.DelinquencyPlanReason {
      return getIteratedValue(1) as entity.DelinquencyPlanReason
    }
    
    
  }
  
  
}