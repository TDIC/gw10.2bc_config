package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/BillingDataLoaderWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BillingDataLoaderWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/BillingDataLoaderWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BillingDataLoaderWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterFinish' attribute on Wizard (id=BillingDataLoaderWizard) at BillingDataLoaderWizard.pcf: line 8, column 36
    function afterFinish_10 () : void {
      DataUploadConfirmation.go(processor)
    }
    
    // 'afterFinish' attribute on Wizard (id=BillingDataLoaderWizard) at BillingDataLoaderWizard.pcf: line 8, column 36
    function afterFinish_dest_11 () : pcf.api.Destination {
      return pcf.DataUploadConfirmation.createDestination(processor)
    }
    
    // 'initialValue' attribute on Variable at BillingDataLoaderWizard.pcf: line 12, column 74
    function initialValue_0 () : tdic.util.dataloader.processor.BCSampleDataLoaderProcessor {
      return new tdic.util.dataloader.processor.BCSampleDataLoaderProcessor()
    }
    
    // 'onExit' attribute on WizardStep (id=upload) at BillingDataLoaderWizard.pcf: line 17, column 83
    function onExit_1 () : void {
      processor.readAndImportFile()
    }
    
    // 'onExit' attribute on WizardStep (id=review) at BillingDataLoaderWizard.pcf: line 23, column 83
    function onExit_4 () : void {
      processor.loadData()
    }
    
    // 'screen' attribute on WizardStep (id=upload) at BillingDataLoaderWizard.pcf: line 17, column 83
    function screen_onEnter_2 (def :  pcf.DataUploadImportScreen) : void {
      def.onEnter(processor)
    }
    
    // 'screen' attribute on WizardStep (id=review) at BillingDataLoaderWizard.pcf: line 23, column 83
    function screen_onEnter_5 (def :  pcf.DataUploadReviewScreen) : void {
      def.onEnter(processor)
    }
    
    // 'screen' attribute on WizardStep (id=upload) at BillingDataLoaderWizard.pcf: line 17, column 83
    function screen_refreshVariables_3 (def :  pcf.DataUploadImportScreen) : void {
      def.refreshVariables(processor)
    }
    
    // 'screen' attribute on WizardStep (id=review) at BillingDataLoaderWizard.pcf: line 23, column 83
    function screen_refreshVariables_6 (def :  pcf.DataUploadReviewScreen) : void {
      def.refreshVariables(processor)
    }
    
    // 'tabBar' attribute on Wizard (id=BillingDataLoaderWizard) at BillingDataLoaderWizard.pcf: line 8, column 36
    function tabBar_onEnter_8 (def :  pcf.InternalToolsTabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=BillingDataLoaderWizard) at BillingDataLoaderWizard.pcf: line 8, column 36
    function tabBar_refreshVariables_9 (def :  pcf.InternalToolsTabBar) : void {
      def.refreshVariables()
    }
    
    // '$$wizardStepAvailable' attribute on WizardStep (id=review) at BillingDataLoaderWizard.pcf: line 23, column 83
    function wizardStepAvailable_7 () : java.lang.Boolean {
      return processor != null
    }
    
    override property get CurrentLocation () : pcf.BillingDataLoaderWizard {
      return super.CurrentLocation as pcf.BillingDataLoaderWizard
    }
    
    property get processor () : tdic.util.dataloader.processor.BCSampleDataLoaderProcessor {
      return getVariableValue("processor", 0) as tdic.util.dataloader.processor.BCSampleDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) {
      setVariableValue("processor", 0, $arg)
    }
    
    
  }
  
  
}