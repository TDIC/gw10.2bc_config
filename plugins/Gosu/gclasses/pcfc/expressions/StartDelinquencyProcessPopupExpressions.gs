package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/StartDelinquencyProcessPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class StartDelinquencyProcessPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/delinquency/StartDelinquencyProcessPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends StartDelinquencyProcessPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=DelinquencyTarget_Cell) at StartDelinquencyProcessPopup.pcf: line 74, column 55
    function action_15 () : void {
      DelinquencyTargetDetailsForward.go(target)
    }
    
    // 'action' attribute on TextCell (id=DelinquencyTarget_Cell) at StartDelinquencyProcessPopup.pcf: line 74, column 55
    function action_dest_16 () : pcf.api.Destination {
      return pcf.DelinquencyTargetDetailsForward.createDestination(target)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=PastDueAmt_Cell) at StartDelinquencyProcessPopup.pcf: line 89, column 54
    function currency_28 () : typekey.Currency {
      return target.getCurrency()
    }
    
    // 'value' attribute on TextCell (id=DelinquencyTarget_Cell) at StartDelinquencyProcessPopup.pcf: line 74, column 55
    function valueRoot_18 () : java.lang.Object {
      return target
    }
    
    // 'value' attribute on TextCell (id=DelinquencyTarget_Cell) at StartDelinquencyProcessPopup.pcf: line 74, column 55
    function value_17 () : java.lang.String {
      return target.TargetDisplayName
    }
    
    // 'value' attribute on DateCell (id=EffDate_Cell) at StartDelinquencyProcessPopup.pcf: line 78, column 51
    function value_20 () : java.util.Date {
      return target.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpDate_Cell) at StartDelinquencyProcessPopup.pcf: line 82, column 52
    function value_23 () : java.util.Date {
      return target.ExpirationDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PastDueAmt_Cell) at StartDelinquencyProcessPopup.pcf: line 89, column 54
    function value_26 () : gw.pl.currency.MonetaryAmount {
      return target.DelinquentAmount
    }
    
    property get target () : gw.api.domain.delinquency.DelinquencyTarget {
      return getIteratedValue(1) as gw.api.domain.delinquency.DelinquencyTarget
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/delinquency/StartDelinquencyProcessPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class StartDelinquencyProcessPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (dlnqTargets :  gw.api.domain.delinquency.DelinquencyTarget[]) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=Cancel) at StartDelinquencyProcessPopup.pcf: line 35, column 83
    function action_3 () : void {
      CurrentLocation.cancel()
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=Execute) at StartDelinquencyProcessPopup.pcf: line 31, column 84
    function allCheckedRowsAction_2 (CheckedValues :  gw.api.domain.delinquency.DelinquencyTarget[], CheckedValuesErrors :  java.util.Map) : void {
      performAction(CheckedValues)
    }
    
    // 'value' attribute on RangeInput (id=Reason_Input) at StartDelinquencyProcessPopup.pcf: line 47, column 51
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      reason = (__VALUE_TO_SET as typekey.DelinquencyReason)
    }
    
    // 'initialValue' attribute on Variable at StartDelinquencyProcessPopup.pcf: line 18, column 33
    function initialValue_0 () : DelinquencyReason {
      return DelinquencyReason.TC_PASTDUE
    }
    
    // 'initialValue' attribute on Variable at StartDelinquencyProcessPopup.pcf: line 22, column 35
    function initialValue_1 () : DelinquencyReason[] {
      return filterReasons()
    }
    
    // 'value' attribute on TextCell (id=DelinquencyTarget_Cell) at StartDelinquencyProcessPopup.pcf: line 74, column 55
    function sortValue_10 (target :  gw.api.domain.delinquency.DelinquencyTarget) : java.lang.Object {
      return target.TargetDisplayName
    }
    
    // 'value' attribute on DateCell (id=EffDate_Cell) at StartDelinquencyProcessPopup.pcf: line 78, column 51
    function sortValue_11 (target :  gw.api.domain.delinquency.DelinquencyTarget) : java.lang.Object {
      return target.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpDate_Cell) at StartDelinquencyProcessPopup.pcf: line 82, column 52
    function sortValue_12 (target :  gw.api.domain.delinquency.DelinquencyTarget) : java.lang.Object {
      return target.ExpirationDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PastDueAmt_Cell) at StartDelinquencyProcessPopup.pcf: line 89, column 54
    function sortValue_13 (target :  gw.api.domain.delinquency.DelinquencyTarget) : java.lang.Object {
      return target.DelinquentAmount
    }
    
    // 'footerSumValue' attribute on RowIterator at StartDelinquencyProcessPopup.pcf: line 89, column 54
    function sumValue_14 (target :  gw.api.domain.delinquency.DelinquencyTarget) : java.lang.Object {
      return ( target typeis PolicyPeriod ? target.DelinquentAmount : 0bd.ofCurrency(target.DelinquentAmount.getCurrency()) )
    }
    
    // 'valueRange' attribute on RangeInput (id=Reason_Input) at StartDelinquencyProcessPopup.pcf: line 47, column 51
    function valueRange_6 () : java.lang.Object {
      return filteredReasons
    }
    
    // 'value' attribute on RowIterator at StartDelinquencyProcessPopup.pcf: line 62, column 75
    function value_30 () : gw.api.domain.delinquency.DelinquencyTarget[] {
      return dlnqTargets.where(\ target -> !(target typeis Account))
    }
    
    // 'value' attribute on RangeInput (id=Reason_Input) at StartDelinquencyProcessPopup.pcf: line 47, column 51
    function value_4 () : typekey.DelinquencyReason {
      return reason
    }
    
    // 'valueRange' attribute on RangeInput (id=Reason_Input) at StartDelinquencyProcessPopup.pcf: line 47, column 51
    function verifyValueRangeIsAllowedType_7 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Reason_Input) at StartDelinquencyProcessPopup.pcf: line 47, column 51
    function verifyValueRangeIsAllowedType_7 ($$arg :  typekey.DelinquencyReason[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Reason_Input) at StartDelinquencyProcessPopup.pcf: line 47, column 51
    function verifyValueRange_8 () : void {
      var __valueRangeArg = filteredReasons
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_7(__valueRangeArg)
    }
    
    override property get CurrentLocation () : pcf.StartDelinquencyProcessPopup {
      return super.CurrentLocation as pcf.StartDelinquencyProcessPopup
    }
    
    property get dlnqTargets () : gw.api.domain.delinquency.DelinquencyTarget[] {
      return getVariableValue("dlnqTargets", 0) as gw.api.domain.delinquency.DelinquencyTarget[]
    }
    
    property set dlnqTargets ($arg :  gw.api.domain.delinquency.DelinquencyTarget[]) {
      setVariableValue("dlnqTargets", 0, $arg)
    }
    
    property get filteredReasons () : DelinquencyReason[] {
      return getVariableValue("filteredReasons", 0) as DelinquencyReason[]
    }
    
    property set filteredReasons ($arg :  DelinquencyReason[]) {
      setVariableValue("filteredReasons", 0, $arg)
    }
    
    property get reason () : DelinquencyReason {
      return getVariableValue("reason", 0) as DelinquencyReason
    }
    
    property set reason ($arg :  DelinquencyReason) {
      setVariableValue("reason", 0, $arg)
    }
    
    /**
           * Filter available reasons to those on referenced Delinquency Plans.  The filtering is
           * more difficult due to AgencyBill; we must find all Delinquency Plans, and then only
           * display the Reasons defined across all of the Plans.
           * @return an array of DelinquencyReason typekeys, as defined in the DelinquencyPlans.
           */
          function filterReasons() : DelinquencyReason[] {
            var plans = dlnqTargets.map( \ dt -> dt.DelinquencyPlan )
            var reasonSets = plans.map( \ pl -> pl.DelinquencyPlanReasons.map( \ pr -> pr.DelinquencyReason ).toSet() )
            /**
             * US135 - Remove "Not Taken" from Delinquency Reasons 
             * 12/4/2014 Alvin Lee
             */  
            return reasonSets.fold( \ set1, set2 -> set1.intersect( set2 ) ).toTypedArray()
          }
    
          /**
           * Perform the popup action.
           */
          function performAction( checkedValues : Object[] ) {
            gw.api.web.delinquency.DelinquencyUtil.startDelinquencies(
                checkedValues as gw.api.domain.delinquency.DelinquencyTarget[],
                reason );
            CurrentLocation.commit();
          }
           
           /**
            * The target is not available if any of the delinquency processes on it have the same
            * reason as the selected reason. Also NOTTAKEN reason is only appliable to policy periods
            */
            /*function isTargetAvailable(target : gw.api.domain.delinquency.DelinquencyTarget) : boolean {
             for (delinquencyProcess in target.DelinquencyProcesses index i) {
               if (delinquencyProcess.Reason == reason ) {
                 return false
               }
             }
             if(reason == DelinquencyReason.TC_NOTTAKEN and target typeis Account){
               return false
             }
             return true
           }*/
    
    
  }
  
  
}