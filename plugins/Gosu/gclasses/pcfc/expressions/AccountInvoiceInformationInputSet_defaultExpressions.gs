package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/AccountInvoiceInformationInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountInvoiceInformationInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/invoice/AccountInvoiceInformationInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountInvoiceInformationInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateInput (id=PaymentDueDate_Input) at AccountInvoiceInformationInputSet.default.pcf: line 26, column 38
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoice.PaymentDueDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=InvoiceDate_Input) at AccountInvoiceInformationInputSet.default.pcf: line 17, column 33
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoice.EventDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'editable' attribute on DateInput (id=InvoiceDate_Input) at AccountInvoiceInformationInputSet.default.pcf: line 17, column 33
    function editable_0 () : java.lang.Boolean {
      return invoice.Status == TC_PLANNED and perm.Account.invcdateedit
    }
    
    // 'validationExpression' attribute on DateInput (id=InvoiceDate_Input) at AccountInvoiceInformationInputSet.default.pcf: line 17, column 33
    function validationExpression_1 () : java.lang.Object {
      return gw.api.web.invoice.InvoiceUtil.validateInvoiceDate(invoice)
    }
    
    // 'validationExpression' attribute on DateInput (id=PaymentDueDate_Input) at AccountInvoiceInformationInputSet.default.pcf: line 26, column 38
    function validationExpression_9 () : java.lang.Object {
      return gw.api.web.invoice.InvoiceUtil.validatePaymentDueDate(invoice)
    }
    
    // 'value' attribute on DateInput (id=InvoiceDate_Input) at AccountInvoiceInformationInputSet.default.pcf: line 17, column 33
    function valueRoot_4 () : java.lang.Object {
      return invoice
    }
    
    // 'value' attribute on DateInput (id=PaymentDueDate_Input) at AccountInvoiceInformationInputSet.default.pcf: line 26, column 38
    function value_10 () : java.util.Date {
      return invoice.PaymentDueDate
    }
    
    // 'value' attribute on TextInput (id=Status_Input) at AccountInvoiceInformationInputSet.default.pcf: line 32, column 36
    function value_16 () : java.lang.String {
      return invoice.StatusForUI
    }
    
    // 'value' attribute on BooleanRadioInput (id=AdHoc_Input) at AccountInvoiceInformationInputSet.default.pcf: line 36, column 34
    function value_19 () : java.lang.Boolean {
      return invoice.isAdHoc()
    }
    
    // 'value' attribute on DateInput (id=InvoiceDate_Input) at AccountInvoiceInformationInputSet.default.pcf: line 17, column 33
    function value_2 () : java.util.Date {
      return invoice.EventDate
    }
    
    property get invoice () : AccountInvoice {
      return getRequireValue("invoice", 0) as AccountInvoice
    }
    
    property set invoice ($arg :  AccountInvoice) {
      setRequireValue("invoice", 0, $arg)
    }
    
    
  }
  
  
}