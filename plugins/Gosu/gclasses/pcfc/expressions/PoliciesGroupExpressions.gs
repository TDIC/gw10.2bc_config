package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PoliciesGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PoliciesGroupExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PoliciesGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PoliciesGroupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'location' attribute on LocationGroup (id=PoliciesGroup) at PoliciesGroup.pcf: line 15, column 30
    function action_0 () : void {
      pcf.Policies.go()
    }
    
    // 'location' attribute on LocationGroup (id=PoliciesGroup) at PoliciesGroup.pcf: line 15, column 30
    function action_dest_1 () : pcf.api.Destination {
      return pcf.Policies.createDestination()
    }
    
    // LocationGroup (id=PoliciesGroup) at PoliciesGroup.pcf: line 11, column 26
    static function firstVisitableChildDestinationMethod_9 () : pcf.api.Destination {
      var dest : pcf.api.Destination
      dest = pcf.Policies.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      return null
    }
    
    // 'infoBar' attribute on LocationGroup (id=PoliciesGroup) at PoliciesGroup.pcf: line 11, column 26
    function infoBar_onEnter_2 (def :  pcf.CurrentDateInfoBar) : void {
      def.onEnter()
    }
    
    // 'infoBar' attribute on LocationGroup (id=PoliciesGroup) at PoliciesGroup.pcf: line 11, column 26
    function infoBar_refreshVariables_3 (def :  pcf.CurrentDateInfoBar) : void {
      def.refreshVariables()
    }
    
    // 'menuActions' attribute on LocationGroup (id=PoliciesGroup) at PoliciesGroup.pcf: line 11, column 26
    function menuActions_onEnter_4 (def :  pcf.PoliciesMenuActions) : void {
      def.onEnter()
    }
    
    // 'menuActions' attribute on LocationGroup (id=PoliciesGroup) at PoliciesGroup.pcf: line 11, column 26
    function menuActions_refreshVariables_5 (def :  pcf.PoliciesMenuActions) : void {
      def.refreshVariables()
    }
    
    // 'parent' attribute on LocationGroup (id=PoliciesGroup) at PoliciesGroup.pcf: line 11, column 26
    static function parent_6 () : pcf.api.Destination {
      return pcf.PolicyForward.createDestination()
    }
    
    // 'tabBar' attribute on LocationGroup (id=PoliciesGroup) at PoliciesGroup.pcf: line 11, column 26
    function tabBar_onEnter_7 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on LocationGroup (id=PoliciesGroup) at PoliciesGroup.pcf: line 11, column 26
    function tabBar_refreshVariables_8 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.PoliciesGroup {
      return super.CurrentLocation as pcf.PoliciesGroup
    }
    
    
  }
  
  
}