package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/workspace/preferences/UserPreferencesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class UserPreferencesDVExpressions {
  @javax.annotation.Generated("config/web/pcf/workspace/preferences/UserPreferencesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class UserPreferencesDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on ConfirmPasswordInput (id=Password_Input) at UserPreferencesDV.pcf: line 18, column 43
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      user.Credential.Password = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=StartupPage_Input) at UserPreferencesDV.pcf: line 28, column 42
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      user.UserSettings.StartupPage = (__VALUE_TO_SET as typekey.StartupPage)
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at UserPreferencesDV.pcf: line 37, column 65
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      user.UserLanguage = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'value' attribute on TypeKeyInput (id=RegionalFormats_Input) at UserPreferencesDV.pcf: line 44, column 63
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      user.UserLocale = (__VALUE_TO_SET as typekey.LocaleType)
    }
    
    // 'value' attribute on TypeKeyInput (id=DefaultCountry_Input) at UserPreferencesDV.pcf: line 50, column 40
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      user.UserDefaultCountry = (__VALUE_TO_SET as typekey.Country)
    }
    
    // 'value' attribute on TypeKeyInput (id=DefaultPhoneCountry_Input) at UserPreferencesDV.pcf: line 56, column 49
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      user.UserDefaultPhoneCountry = (__VALUE_TO_SET as typekey.PhoneCountryCode)
    }
    
    // 'valueRange' attribute on RangeInput (id=StartupPage_Input) at UserPreferencesDV.pcf: line 28, column 42
    function valueRange_16 () : java.lang.Object {
      return user.getVisibleStartupPages()
    }
    
    // 'value' attribute on RangeInput (id=StartupPage_Input) at UserPreferencesDV.pcf: line 28, column 42
    function valueRoot_15 () : java.lang.Object {
      return user.UserSettings
    }
    
    // 'value' attribute on ConfirmPasswordInput (id=Password_Input) at UserPreferencesDV.pcf: line 18, column 43
    function valueRoot_2 () : java.lang.Object {
      return user.Credential
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at UserPreferencesDV.pcf: line 37, column 65
    function valueRoot_23 () : java.lang.Object {
      return user
    }
    
    // 'value' attribute on ConfirmPasswordInput (id=Password_Input) at UserPreferencesDV.pcf: line 18, column 43
    function value_0 () : java.lang.String {
      return user.Credential.Password
    }
    
    // 'value' attribute on RangeInput (id=StartupPage_Input) at UserPreferencesDV.pcf: line 28, column 42
    function value_13 () : typekey.StartupPage {
      return user.UserSettings.StartupPage
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at UserPreferencesDV.pcf: line 37, column 65
    function value_21 () : typekey.LanguageType {
      return user.UserLanguage
    }
    
    // 'value' attribute on TypeKeyInput (id=RegionalFormats_Input) at UserPreferencesDV.pcf: line 44, column 63
    function value_27 () : typekey.LocaleType {
      return user.UserLocale
    }
    
    // 'value' attribute on ConfirmPasswordInput (id=Password_Input) at UserPreferencesDV.pcf: line 18, column 43
    function value_3 () : java.lang.Object {
      return user.Credential.Password
    }
    
    // 'value' attribute on TypeKeyInput (id=DefaultCountry_Input) at UserPreferencesDV.pcf: line 50, column 40
    function value_32 () : typekey.Country {
      return user.UserDefaultCountry
    }
    
    // 'value' attribute on TypeKeyInput (id=DefaultPhoneCountry_Input) at UserPreferencesDV.pcf: line 56, column 49
    function value_36 () : typekey.PhoneCountryCode {
      return user.UserDefaultPhoneCountry
    }
    
    // 'valueRange' attribute on RangeInput (id=StartupPage_Input) at UserPreferencesDV.pcf: line 28, column 42
    function verifyValueRangeIsAllowedType_17 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=StartupPage_Input) at UserPreferencesDV.pcf: line 28, column 42
    function verifyValueRangeIsAllowedType_17 ($$arg :  typekey.StartupPage[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=StartupPage_Input) at UserPreferencesDV.pcf: line 28, column 42
    function verifyValueRange_18 () : void {
      var __valueRangeArg = user.getVisibleStartupPages()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_17(__valueRangeArg)
    }
    
    // 'visible' attribute on TypeKeyInput (id=Language_Input) at UserPreferencesDV.pcf: line 37, column 65
    function visible_20 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.canSwitchLanguage()
    }
    
    // 'visible' attribute on TypeKeyInput (id=RegionalFormats_Input) at UserPreferencesDV.pcf: line 44, column 63
    function visible_26 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.canSwitchLocale()
    }
    
    property get user () : User {
      return getRequireValue("user", 0) as User
    }
    
    property set user ($arg :  User) {
      setRequireValue("user", 0, $arg)
    }
    
    
  }
  
  
}