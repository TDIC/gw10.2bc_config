package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillSavedPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillSavedPaymentsExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillSavedPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillSavedPaymentsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=AgencyBillSavedPayments) at AgencyBillSavedPayments.pcf: line 9, column 84
    function canEdit_106 () : java.lang.Boolean {
      return perm.System.prodpmntedit
    }
    
    // 'canVisit' attribute on Page (id=AgencyBillSavedPayments) at AgencyBillSavedPayments.pcf: line 9, column 84
    static function canVisit_107 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodpmntview
    }
    
    // Page (id=AgencyBillSavedPayments) at AgencyBillSavedPayments.pcf: line 9, column 84
    static function parent_108 (producer :  Producer) : pcf.api.Destination {
      return pcf.AgencyBillPayments.createDestination(producer)
    }
    
    override property get CurrentLocation () : pcf.AgencyBillSavedPayments {
      return super.CurrentLocation as pcf.AgencyBillSavedPayments
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    function discardPayments(payments : AgencyCyclePayment[]) {
      gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
        payments.each( \ payment ->  {
          if (payment.Executed) {
            throw new gw.api.util.DisplayableException(DisplayKey.get("Web.BaseDistEnhancement.Error.CalledOnExecutedDist.Discard"))
          }
          payment = bundle.add(payment)
          payment.remove()
        })
      })
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillSavedPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at AgencyBillSavedPayments.pcf: line 113, column 47
    function action_21 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // MenuItem (id=UserBrowseMenuItem) at AgencyBillSavedPayments.pcf: line 113, column 47
    function action_dest_22 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillSavedPayments.pcf: line 88, column 43
    function condition_12 () : java.lang.Boolean {
      return payment.hasFrozenDistItem()
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillSavedPayments.pcf: line 91, column 32
    function condition_13 () : java.lang.Boolean {
      return payment.Frozen
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillSavedPayments.pcf: line 94, column 39
    function condition_14 () : java.lang.Boolean {
      return payment.SuspDistItemsThatHaveNotBeenReleased.HasElements
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at AgencyBillSavedPayments.pcf: line 125, column 66
    function currency_31 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AgencyBillSavedPayments.pcf: line 101, column 67
    function valueRoot_16 () : java.lang.Object {
      return payment.BaseMoneyReceived
    }
    
    // 'value' attribute on AltUserCell (id=SavedBy_Cell) at AgencyBillSavedPayments.pcf: line 113, column 47
    function valueRoot_24 () : java.lang.Object {
      return payment
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AgencyBillSavedPayments.pcf: line 101, column 67
    function value_15 () : java.util.Date {
      return payment.BaseMoneyReceived.ReceivedDate
    }
    
    // 'value' attribute on DateCell (id=SavedDate_Cell) at AgencyBillSavedPayments.pcf: line 107, column 65
    function value_18 () : java.util.Date {
      return payment.BaseMoneyReceived.UpdateTime
    }
    
    // 'value' attribute on AltUserCell (id=SavedBy_Cell) at AgencyBillSavedPayments.pcf: line 113, column 47
    function value_23 () : entity.User {
      return payment.UpdateUser
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AgencyBillSavedPayments.pcf: line 118, column 59
    function value_26 () : java.lang.String {
      return payment.BaseMoneyReceived.Name
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AgencyBillSavedPayments.pcf: line 125, column 66
    function value_29 () : gw.pl.currency.MonetaryAmount {
      return payment.BaseMoneyReceived.TotalAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Distributed_Cell) at AgencyBillSavedPayments.pcf: line 132, column 71
    function value_33 () : gw.pl.currency.MonetaryAmount {
      return payment.DistributedAmountForUnexecutedDist
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Suspense_Cell) at AgencyBillSavedPayments.pcf: line 139, column 68
    function value_37 () : gw.pl.currency.MonetaryAmount {
      return payment.SuspenseAmountForUnexecutedDist
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Remaining_Cell) at AgencyBillSavedPayments.pcf: line 146, column 52
    function value_41 () : gw.pl.currency.MonetaryAmount {
      return payment.RemainingAmount
    }
    
    property get payment () : entity.AgencyCyclePayment {
      return getIteratedValue(2) as entity.AgencyCyclePayment
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillSavedPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends AgencyBillSavedPaymentsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at AgencyBillSavedPayments.pcf: line 174, column 55
    function action_56 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // MenuItem (id=UserBrowseMenuItem) at AgencyBillSavedPayments.pcf: line 174, column 55
    function action_dest_57 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=Discard) at AgencyBillSavedPayments.pcf: line 71, column 51
    function allCheckedRowsAction_8 (CheckedValues :  entity.AgencyCyclePayment[], CheckedValuesErrors :  java.util.Map) : void {
      discardPayments(CheckedValues)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=EditSaved) at AgencyBillSavedPayments.pcf: line 53, column 29
    function checkedRowAction_5 (payment :  entity.AgencyCyclePayment, CheckedValue :  entity.AgencyCyclePayment) : void {
      AgencyDistributionWizard.go(producer, CheckedValue.BaseMoneyReceived)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=ViewSuspenseItems) at AgencyBillSavedPayments.pcf: line 61, column 29
    function checkedRowAction_6 (payment :  entity.AgencyCyclePayment, CheckedValue :  entity.AgencyCyclePayment) : void {
      AgencySuspenseItemsPopup.push(CheckedValue, false)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyBillSavedPayments.pcf: line 193, column 70
    function currency_70 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'def' attribute on PanelRef at AgencyBillSavedPayments.pcf: line 237, column 64
    function def_onEnter_103 (def :  pcf.AgencyDistItemsReadOnlyCV) : void {
      def.onEnter(selectedPayment)
    }
    
    // 'def' attribute on PanelRef at AgencyBillSavedPayments.pcf: line 156, column 76
    function def_onEnter_46 (def :  pcf.AgencyBillDistArchiveWarningPanelSet) : void {
      def.onEnter(selectedPayment)
    }
    
    // 'def' attribute on PanelRef at AgencyBillSavedPayments.pcf: line 237, column 64
    function def_refreshVariables_104 (def :  pcf.AgencyDistItemsReadOnlyCV) : void {
      def.refreshVariables(selectedPayment)
    }
    
    // 'def' attribute on PanelRef at AgencyBillSavedPayments.pcf: line 156, column 76
    function def_refreshVariables_47 (def :  pcf.AgencyBillDistArchiveWarningPanelSet) : void {
      def.refreshVariables(selectedPayment)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillSavedPayments.pcf: line 35, column 136
    function filter_1 () : gw.api.filters.IFilter {
      return new gw.payment.BaseDistReceivedDateFilter(30)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillSavedPayments.pcf: line 38, column 136
    function filter_2 () : gw.api.filters.IFilter {
      return new gw.payment.BaseDistReceivedDateFilter(60)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillSavedPayments.pcf: line 41, column 136
    function filter_3 () : gw.api.filters.IFilter {
      return new gw.payment.BaseDistReceivedDateFilter(90)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillSavedPayments.pcf: line 44, column 129
    function filter_4 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("All", \ qf -> {})
    }
    
    // 'initialValue' attribute on Variable at AgencyBillSavedPayments.pcf: line 23, column 85
    function initialValue_0 () : gw.api.database.IQueryBeanResult<entity.AgencyCyclePayment> {
      return producer.SavedPayments
    }
    
    // 'value' attribute on DateCell (id=SavedDate_Cell) at AgencyBillSavedPayments.pcf: line 107, column 65
    function sortValue_10 (payment :  entity.AgencyCyclePayment) : java.lang.Object {
      return payment.BaseMoneyReceived.UpdateTime
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AgencyBillSavedPayments.pcf: line 118, column 59
    function sortValue_11 (payment :  entity.AgencyCyclePayment) : java.lang.Object {
      return payment.BaseMoneyReceived.Name
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AgencyBillSavedPayments.pcf: line 101, column 67
    function sortValue_9 (payment :  entity.AgencyCyclePayment) : java.lang.Object {
      return payment.BaseMoneyReceived.ReceivedDate
    }
    
    // 'title' attribute on Card (id=PaymentDetailCard) at AgencyBillSavedPayments.pcf: line 154, column 158
    function title_105 () : java.lang.String {
      return DisplayKey.get("Web.AgencyBillPayments.SavedPayments.PaymentDetail", selectedPayment.BaseMoneyReceived.ReceivedDate.AsUIStyle)
    }
    
    // 'value' attribute on TextInput (id=PaymentInstrumentToken_Input) at AgencyBillSavedPayments.pcf: line 233, column 70
    function valueRoot_100 () : java.lang.Object {
      return selectedPayment.AgencyMoneyReceived.PaymentInstrument
    }
    
    // 'value' attribute on DateInput (id=ReceivedDate_Input) at AgencyBillSavedPayments.pcf: line 166, column 70
    function valueRoot_50 () : java.lang.Object {
      return selectedPayment.BaseMoneyReceived
    }
    
    // 'value' attribute on DateInput (id=SavedDate_Input) at AgencyBillSavedPayments.pcf: line 170, column 55
    function valueRoot_54 () : java.lang.Object {
      return selectedPayment
    }
    
    // 'value' attribute on TextInput (id=PaymentDescription_Input) at AgencyBillSavedPayments.pcf: line 182, column 76
    function valueRoot_65 () : java.lang.Object {
      return selectedPayment.AgencyMoneyReceived
    }
    
    // 'value' attribute on RowIterator (id=PaymentsIterator) at AgencyBillSavedPayments.pcf: line 83, column 93
    function value_45 () : gw.api.database.IQueryBeanResult<entity.AgencyCyclePayment> {
      return payments
    }
    
    // 'value' attribute on DateInput (id=ReceivedDate_Input) at AgencyBillSavedPayments.pcf: line 166, column 70
    function value_49 () : java.util.Date {
      return selectedPayment.BaseMoneyReceived.ReceivedDate
    }
    
    // 'value' attribute on DateInput (id=SavedDate_Input) at AgencyBillSavedPayments.pcf: line 170, column 55
    function value_53 () : java.util.Date {
      return selectedPayment.UpdateTime
    }
    
    // 'value' attribute on AltUserInput (id=SavedBy_Input) at AgencyBillSavedPayments.pcf: line 174, column 55
    function value_58 () : entity.User {
      return selectedPayment.UpdateUser
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AgencyBillSavedPayments.pcf: line 178, column 67
    function value_61 () : java.lang.String {
      return selectedPayment.BaseMoneyReceived.Name
    }
    
    // 'value' attribute on TextInput (id=PaymentDescription_Input) at AgencyBillSavedPayments.pcf: line 182, column 76
    function value_64 () : java.lang.String {
      return selectedPayment.AgencyMoneyReceived.Description
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyBillSavedPayments.pcf: line 193, column 70
    function value_68 () : gw.pl.currency.MonetaryAmount {
      return selectedPayment.BaseMoneyReceived.Amount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=DistributedAmount_Input) at AgencyBillSavedPayments.pcf: line 199, column 79
    function value_73 () : gw.pl.currency.MonetaryAmount {
      return selectedPayment.DistributedAmountForUnexecutedDist
    }
    
    // 'value' attribute on MonetaryAmountInput (id=SuspenseAmount_Input) at AgencyBillSavedPayments.pcf: line 205, column 76
    function value_77 () : gw.pl.currency.MonetaryAmount {
      return selectedPayment.SuspenseAmountForUnexecutedDist
    }
    
    // 'value' attribute on MonetaryAmountInput (id=RemainingAmount_Input) at AgencyBillSavedPayments.pcf: line 212, column 70
    function value_82 () : gw.pl.currency.MonetaryAmount {
      return selectedPayment.RemainingAmount
    }
    
    // 'value' attribute on TextInput (id=PaymentInstrument_Input) at AgencyBillSavedPayments.pcf: line 223, column 70
    function value_89 () : entity.PaymentInstrument {
      return selectedPayment.AgencyMoneyReceived.PaymentInstrument
    }
    
    // 'value' attribute on TextInput (id=RefNumber_Input) at AgencyBillSavedPayments.pcf: line 228, column 70
    function value_94 () : java.lang.String {
      return selectedPayment.AgencyMoneyReceived.RefNumber
    }
    
    // 'value' attribute on TextInput (id=PaymentInstrumentToken_Input) at AgencyBillSavedPayments.pcf: line 233, column 70
    function value_99 () : java.lang.String {
      return selectedPayment.AgencyMoneyReceived.PaymentInstrument.Token
    }
    
    // 'visible' attribute on DateInput (id=ReceivedDate_Input) at AgencyBillSavedPayments.pcf: line 166, column 70
    function visible_48 () : java.lang.Boolean {
      return !selectedPayment.isCreditDistribution()
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=Discard) at AgencyBillSavedPayments.pcf: line 71, column 51
    function visible_7 () : java.lang.Boolean {
      return perm.System.prodpromedit
    }
    
    property get initialPayment () : AgencyCyclePayment {
      return getVariableValue("initialPayment", 1) as AgencyCyclePayment
    }
    
    property set initialPayment ($arg :  AgencyCyclePayment) {
      setVariableValue("initialPayment", 1, $arg)
    }
    
    property get payments () : gw.api.database.IQueryBeanResult<entity.AgencyCyclePayment> {
      return getVariableValue("payments", 1) as gw.api.database.IQueryBeanResult<entity.AgencyCyclePayment>
    }
    
    property set payments ($arg :  gw.api.database.IQueryBeanResult<entity.AgencyCyclePayment>) {
      setVariableValue("payments", 1, $arg)
    }
    
    property get selectedPayment () : AgencyCyclePayment {
      return getCurrentSelection(1) as AgencyCyclePayment
    }
    
    property set selectedPayment ($arg :  AgencyCyclePayment) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  
}