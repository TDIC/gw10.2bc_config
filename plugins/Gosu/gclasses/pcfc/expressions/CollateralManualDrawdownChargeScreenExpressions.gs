package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/CollateralManualDrawdownChargeScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CollateralManualDrawdownChargeScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/collateral/CollateralManualDrawdownChargeScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CollateralManualDrawdownChargeScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=shortfall_Input) at CollateralManualDrawdownChargeScreen.pcf: line 21, column 54
    function currency_1 () : typekey.Currency {
      return collateralDrawdown.Collateral.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=chargeAmount_Input) at CollateralManualDrawdownChargeScreen.pcf: line 43, column 54
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      collateralDrawdown.ChargeAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on CheckBoxInput (id=createCharge_Input) at CollateralManualDrawdownChargeScreen.pcf: line 32, column 51
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      collateralDrawdown.CreateCharge = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=chargeAmount_Input) at CollateralManualDrawdownChargeScreen.pcf: line 43, column 54
    function validationExpression_10 () : java.lang.Object {
      return collateralDrawdown.isChargeAmountValid() ? null : DisplayKey.get("Web.CollateralManualDrawdownChargeScreen.ChargeAmountWarning")
    }
    
    // 'value' attribute on CheckBoxInput (id=createCharge_Input) at CollateralManualDrawdownChargeScreen.pcf: line 32, column 51
    function valueRoot_8 () : java.lang.Object {
      return collateralDrawdown
    }
    
    // 'value' attribute on MonetaryAmountInput (id=shortfall_Input) at CollateralManualDrawdownChargeScreen.pcf: line 21, column 54
    function value_0 () : gw.pl.currency.MonetaryAmount {
      return collateralDrawdown.getShortfall()
    }
    
    // 'value' attribute on MonetaryAmountInput (id=chargeAmount_Input) at CollateralManualDrawdownChargeScreen.pcf: line 43, column 54
    function value_12 () : gw.pl.currency.MonetaryAmount {
      return collateralDrawdown.ChargeAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=shortfallCovered_Input) at CollateralManualDrawdownChargeScreen.pcf: line 27, column 66
    function value_3 () : gw.pl.currency.MonetaryAmount {
      return collateralDrawdown.getShortfallCoveredByLOC()
    }
    
    // 'value' attribute on CheckBoxInput (id=createCharge_Input) at CollateralManualDrawdownChargeScreen.pcf: line 32, column 51
    function value_6 () : java.lang.Boolean {
      return collateralDrawdown.CreateCharge
    }
    
    property get collateralDrawdown () : gw.api.web.collateral.CollateralDrawdownUtil {
      return getRequireValue("collateralDrawdown", 0) as gw.api.web.collateral.CollateralDrawdownUtil
    }
    
    property set collateralDrawdown ($arg :  gw.api.web.collateral.CollateralDrawdownUtil) {
      setRequireValue("collateralDrawdown", 0, $arg)
    }
    
    
  }
  
  
}