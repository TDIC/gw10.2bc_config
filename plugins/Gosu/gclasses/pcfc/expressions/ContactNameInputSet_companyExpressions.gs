package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contact/ContactNameInputSet.company.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ContactNameInputSet_companyExpressions {
  @javax.annotation.Generated("config/web/pcf/contact/ContactNameInputSet.company.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ContactNameInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at ContactNameInputSet.company.pcf: line 15, column 54
    function def_onEnter_1 (def :  pcf.GlobalContactNameInputSet_Japan) : void {
      def.onEnter(new gw.api.name.BCNameOwner(new gw.api.name.ContactNameDelegate(contact)))
    }
    
    // 'def' attribute on InputSetRef at ContactNameInputSet.company.pcf: line 15, column 54
    function def_onEnter_3 (def :  pcf.GlobalContactNameInputSet_default) : void {
      def.onEnter(new gw.api.name.BCNameOwner(new gw.api.name.ContactNameDelegate(contact)))
    }
    
    // 'def' attribute on InputSetRef (id=WorkPhone) at ContactNameInputSet.company.pcf: line 18, column 23
    function def_onEnter_6 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.BusinessPhoneOwner(new gw.api.phone.ContactPhoneDelegate(contact, Contact#WorkPhone), DisplayKey.get("Web.ContactDetailDV.Phone")))
    }
    
    // 'def' attribute on InputSetRef at ContactNameInputSet.company.pcf: line 15, column 54
    function def_refreshVariables_2 (def :  pcf.GlobalContactNameInputSet_Japan) : void {
      def.refreshVariables(new gw.api.name.BCNameOwner(new gw.api.name.ContactNameDelegate(contact)))
    }
    
    // 'def' attribute on InputSetRef at ContactNameInputSet.company.pcf: line 15, column 54
    function def_refreshVariables_4 (def :  pcf.GlobalContactNameInputSet_default) : void {
      def.refreshVariables(new gw.api.name.BCNameOwner(new gw.api.name.ContactNameDelegate(contact)))
    }
    
    // 'def' attribute on InputSetRef (id=WorkPhone) at ContactNameInputSet.company.pcf: line 18, column 23
    function def_refreshVariables_7 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.BusinessPhoneOwner(new gw.api.phone.ContactPhoneDelegate(contact, Contact#WorkPhone), DisplayKey.get("Web.ContactDetailDV.Phone")))
    }
    
    // 'value' attribute on TextInput (id=EmailAddress1_Input) at ContactNameInputSet.company.pcf: line 23, column 38
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      contact.EmailAddress1 = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'label' attribute on Label at ContactNameInputSet.company.pcf: line 12, column 44
    function label_0 () : java.lang.String {
      return contact.Subtype.DisplayName
    }
    
    // 'mode' attribute on InputSetRef at ContactNameInputSet.company.pcf: line 15, column 54
    function mode_5 () : java.lang.Object {
      return gw.api.name.NameLocaleSettings.PCFMode
    }
    
    // 'value' attribute on TextInput (id=EmailAddress1_Input) at ContactNameInputSet.company.pcf: line 23, column 38
    function valueRoot_10 () : java.lang.Object {
      return contact
    }
    
    // 'value' attribute on TextInput (id=EmailAddress1_Input) at ContactNameInputSet.company.pcf: line 23, column 38
    function value_8 () : java.lang.String {
      return contact.EmailAddress1
    }
    
    property get contact () : Contact {
      return getRequireValue("contact", 0) as Contact
    }
    
    property set contact ($arg :  Contact) {
      setRequireValue("contact", 0, $arg)
    }
    
    
  }
  
  
}