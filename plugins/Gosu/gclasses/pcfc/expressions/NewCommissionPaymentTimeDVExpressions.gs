package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentTimeDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewCommissionPaymentTimeDVExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentTimeDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewCommissionPaymentTimeDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateInput (id=PayDate_Input) at NewCommissionPaymentTimeDV.pcf: line 51, column 41
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      delayedPayment.PayOn = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on Choice (id=ImmediatelyChoice) at NewCommissionPaymentTimeDV.pcf: line 22, column 51
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      delayedPayment.PaymentTime = (__VALUE_TO_SET as typekey.CommissionPaymentTime)
    }
    
    // 'editable' attribute on DetailViewPanel (id=NewCommissionPaymentTimeDV) at NewCommissionPaymentTimeDV.pcf: line 7, column 37
    function editable_24 () : java.lang.Boolean {
      return canEdit
    }
    
    // 'option' attribute on Choice (id=PayDateChoice) at NewCommissionPaymentTimeDV.pcf: line 44, column 51
    function option_19 () : java.lang.Object {
      return CommissionPaymentTime.TC_PAYONDATE.Code
    }
    
    // 'option' attribute on Choice (id=ImmediatelyChoice) at NewCommissionPaymentTimeDV.pcf: line 22, column 51
    function option_2 () : java.lang.Object {
      return CommissionPaymentTime.TC_IMMEDIATELY.Code
    }
    
    // 'option' attribute on Choice (id=NextPaymentChoice) at NewCommissionPaymentTimeDV.pcf: line 33, column 51
    function option_9 () : java.lang.Object {
      return CommissionPaymentTime.TC_NEXTPAYMENT.Code
    }
    
    // 'requestValidationExpression' attribute on DateInput (id=PayDate_Input) at NewCommissionPaymentTimeDV.pcf: line 51, column 41
    function requestValidationExpression_14 (VALUE :  java.util.Date) : java.lang.Object {
      return gw.api.util.DateUtil.compareIgnoreTime(VALUE, gw.api.util.DateUtil.currentDate())>=0 ? null : DisplayKey.get("Java.CommissionPayment.Exception.InvalidPayOnDate")
    }
    
    // 'value' attribute on Choice (id=ImmediatelyChoice) at NewCommissionPaymentTimeDV.pcf: line 22, column 51
    function valueRoot_5 () : java.lang.Object {
      return delayedPayment
    }
    
    // 'value' attribute on TextInput (id=Immediately_Input) at NewCommissionPaymentTimeDV.pcf: line 27, column 41
    function value_0 () : java.lang.Object {
      return null
    }
    
    // 'value' attribute on DateInput (id=PayDate_Input) at NewCommissionPaymentTimeDV.pcf: line 51, column 41
    function value_15 () : java.util.Date {
      return delayedPayment.PayOn
    }
    
    // 'value' attribute on Choice (id=ImmediatelyChoice) at NewCommissionPaymentTimeDV.pcf: line 22, column 51
    function value_3 () : typekey.CommissionPaymentTime {
      return delayedPayment.PaymentTime
    }
    
    property get canEdit () : Boolean {
      return getRequireValue("canEdit", 0) as Boolean
    }
    
    property set canEdit ($arg :  Boolean) {
      setRequireValue("canEdit", 0, $arg)
    }
    
    property get delayedPayment () : DelayedCmsnPayment {
      return getRequireValue("delayedPayment", 0) as DelayedCmsnPayment
    }
    
    property set delayedPayment ($arg :  DelayedCmsnPayment) {
      setRequireValue("delayedPayment", 0, $arg)
    }
    
    
  }
  
  
}