package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/CreditReversalConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreditReversalConfirmationScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/CreditReversalConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreditReversalConfirmationScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at CreditReversalConfirmationScreen.pcf: line 36, column 51
    function def_onEnter_3 (def :  pcf.ApprovalActivityDV) : void {
      def.onEnter(approvalActivity)
    }
    
    // 'def' attribute on PanelRef at CreditReversalConfirmationScreen.pcf: line 36, column 51
    function def_refreshVariables_4 (def :  pcf.ApprovalActivityDV) : void {
      def.refreshVariables(approvalActivity)
    }
    
    // 'initialValue' attribute on Variable at CreditReversalConfirmationScreen.pcf: line 18, column 49
    function initialValue_0 () : entity.CreditReversalApprActivity {
      return creditReversal.getOpenApprovalActivity()
    }
    
    // 'label' attribute on Label at CreditReversalConfirmationScreen.pcf: line 32, column 128
    function label_2 () : java.lang.String {
      return DisplayKey.get("Web.CreditReversal.Confirmation", creditReversal.Credit.Amount.render(), account)
    }
    
    // 'visible' attribute on AlertBar (id=ApprovalActivityAlertBar) at CreditReversalConfirmationScreen.pcf: line 25, column 43
    function visible_1 () : java.lang.Boolean {
      return approvalActivity != null
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get approvalActivity () : entity.CreditReversalApprActivity {
      return getVariableValue("approvalActivity", 0) as entity.CreditReversalApprActivity
    }
    
    property set approvalActivity ($arg :  entity.CreditReversalApprActivity) {
      setVariableValue("approvalActivity", 0, $arg)
    }
    
    property get creditReversal () : CreditReversal {
      return getRequireValue("creditReversal", 0) as CreditReversal
    }
    
    property set creditReversal ($arg :  CreditReversal) {
      setRequireValue("creditReversal", 0, $arg)
    }
    
    
  }
  
  
}