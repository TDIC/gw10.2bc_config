package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/payment/NewPaymentPlan_FirstStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewPaymentPlan_FirstStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/payment/NewPaymentPlan_FirstStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewPaymentPlan_FirstStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at NewPaymentPlan_FirstStepScreen.pcf: line 55, column 57
    function def_onEnter_23 (def :  pcf.PlanMultiCurrencyInputSet) : void {
      def.onEnter(paymentPlan)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentFeesDefaults) at NewPaymentPlan_FirstStepScreen.pcf: line 121, column 41
    function def_onEnter_45 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(paymentPlan, PaymentPlan#InstallmentFeeDefaults.PropertyInfo, DisplayKey.get("Web.NewPaymentPlanWizard.FeeAmount"), false, true, null)
    }
    
    // 'def' attribute on PanelRef at NewPaymentPlan_FirstStepScreen.pcf: line 154, column 41
    function def_onEnter_65 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(paymentPlan, { 'Name', 'Description' }, { DisplayKey.get("Web.PaymentPlanDetailDV.Name"), DisplayKey.get("Web.PaymentPlanDetailDV.Description") })
    }
    
    // 'def' attribute on InputSetRef at NewPaymentPlan_FirstStepScreen.pcf: line 55, column 57
    function def_refreshVariables_24 (def :  pcf.PlanMultiCurrencyInputSet) : void {
      def.refreshVariables(paymentPlan)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentFeesDefaults) at NewPaymentPlan_FirstStepScreen.pcf: line 121, column 41
    function def_refreshVariables_46 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(paymentPlan, PaymentPlan#InstallmentFeeDefaults.PropertyInfo, DisplayKey.get("Web.NewPaymentPlanWizard.FeeAmount"), false, true, null)
    }
    
    // 'def' attribute on PanelRef at NewPaymentPlan_FirstStepScreen.pcf: line 154, column 41
    function def_refreshVariables_66 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(paymentPlan, { 'Name', 'Description' }, { DisplayKey.get("Web.PaymentPlanDetailDV.Name"), DisplayKey.get("Web.PaymentPlanDetailDV.Description") })
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 23, column 36
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 44, column 47
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.ExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Reporting_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 50, column 41
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.Reporting = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=PaymentInterval_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 62, column 44
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.Periodicity = (__VALUE_TO_SET as typekey.Periodicity)
    }
    
    // 'value' attribute on TypeKeyInput (id=BillDateOrDueDateBilling_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 78, column 59
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.PolicyLevelBillingBillDateOrDueDateBilling = (__VALUE_TO_SET as typekey.BillDateOrDueDateBilling)
    }
    
    // 'value' attribute on TypeKeyInput (id=InvoiceItemPlacementCutoffType_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 91, column 65
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.AccountLevelBillingInvoiceItemPlacementCutoffType = (__VALUE_TO_SET as typekey.InvoiceItemPlacementCutoffType)
    }
    
    // 'value' attribute on TypeKeyInput (id=InvoicingBlackoutType_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 108, column 54
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.InvoicingBlackoutType = (__VALUE_TO_SET as typekey.InvoicingBlackoutType)
    }
    
    // 'value' attribute on TextInput (id=LastInvoiceByDaysBeforePolicyExpiration_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 115, column 42
    function defaultSetter_42 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.DaysBeforePolicyExpirationForInvoicingBlackout = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SkipFeeForDownPayment_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 127, column 54
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.SkipFeeForDownPayment = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 30, column 43
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioInput (id=EnableEquityWarnings_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 138, column 115
    function defaultSetter_54 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.EquityWarningsEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=EquityBuffer_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 148, column 127
    function defaultSetter_60 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.EquityBuffer = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 38, column 46
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.EffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'editable' attribute on PanelRef at NewPaymentPlan_FirstStepScreen.pcf: line 154, column 41
    function editable_64 () : java.lang.Boolean {
      return not paymentPlan.InUse
    }
    
    // 'onChange' attribute on PostOnChange at NewPaymentPlan_FirstStepScreen.pcf: line 52, column 69
    function onChange_18 () : void {
      setVisibilityForEnableEquityWarningsField()
    }
    
    // 'validationExpression' attribute on DateInput (id=ExpirationDate_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 44, column 47
    function validationExpression_12 () : java.lang.Object {
      return paymentPlan.hasValidExpirationDate() ? null : DisplayKey.get("Web.Plan.ExpirationDate.ValidationError")
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 23, column 36
    function valueRoot_2 () : java.lang.Object {
      return paymentPlan
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 23, column 36
    function value_0 () : java.lang.String {
      return paymentPlan.Name
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 44, column 47
    function value_13 () : java.util.Date {
      return paymentPlan.ExpirationDate
    }
    
    // 'value' attribute on BooleanRadioInput (id=Reporting_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 50, column 41
    function value_19 () : java.lang.Boolean {
      return paymentPlan.Reporting
    }
    
    // 'value' attribute on TypeKeyInput (id=PaymentInterval_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 62, column 44
    function value_25 () : typekey.Periodicity {
      return paymentPlan.Periodicity
    }
    
    // 'value' attribute on TypeKeyInput (id=BillDateOrDueDateBilling_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 78, column 59
    function value_29 () : typekey.BillDateOrDueDateBilling {
      return paymentPlan.PolicyLevelBillingBillDateOrDueDateBilling
    }
    
    // 'value' attribute on TypeKeyInput (id=InvoiceItemPlacementCutoffType_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 91, column 65
    function value_33 () : typekey.InvoiceItemPlacementCutoffType {
      return paymentPlan.AccountLevelBillingInvoiceItemPlacementCutoffType
    }
    
    // 'value' attribute on TypeKeyInput (id=InvoicingBlackoutType_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 108, column 54
    function value_37 () : typekey.InvoicingBlackoutType {
      return paymentPlan.InvoicingBlackoutType
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 30, column 43
    function value_4 () : java.lang.String {
      return paymentPlan.Description
    }
    
    // 'value' attribute on TextInput (id=LastInvoiceByDaysBeforePolicyExpiration_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 115, column 42
    function value_41 () : java.lang.Integer {
      return paymentPlan.DaysBeforePolicyExpirationForInvoicingBlackout
    }
    
    // 'value' attribute on BooleanRadioInput (id=SkipFeeForDownPayment_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 127, column 54
    function value_47 () : java.lang.Boolean {
      return paymentPlan.SkipFeeForDownPayment
    }
    
    // 'value' attribute on BooleanRadioInput (id=EnableEquityWarnings_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 138, column 115
    function value_53 () : java.lang.Boolean {
      return paymentPlan.EquityWarningsEnabled
    }
    
    // 'value' attribute on TextInput (id=EquityBuffer_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 148, column 127
    function value_59 () : java.lang.Integer {
      return paymentPlan.EquityBuffer
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 38, column 46
    function value_8 () : java.util.Date {
      return paymentPlan.EffectiveDate
    }
    
    // 'visible' attribute on Label at NewPaymentPlan_FirstStepScreen.pcf: line 131, column 116
    function visible_51 () : java.lang.Boolean {
      return gw.api.system.BCConfigParameters.EnableEquityWarnings.Value && !paymentPlan.Reporting
    }
    
    // 'visible' attribute on TextInput (id=EquityBuffer_Input) at NewPaymentPlan_FirstStepScreen.pcf: line 148, column 127
    function visible_58 () : java.lang.Boolean {
      return gw.api.system.BCConfigParameters.EnableEquityWarnings.Value && paymentPlan.EquityWarningsEnabled
    }
    
    property get paymentPlan () : PaymentPlan {
      return getRequireValue("paymentPlan", 0) as PaymentPlan
    }
    
    property set paymentPlan ($arg :  PaymentPlan) {
      setRequireValue("paymentPlan", 0, $arg)
    }
    
    function setVisibilityForEnableEquityWarningsField() {
      if (paymentPlan.Reporting) {
        paymentPlan.EquityWarningsEnabled = false;
      }
    }
    
    
  }
  
  
}