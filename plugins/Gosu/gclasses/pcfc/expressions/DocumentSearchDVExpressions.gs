package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/DocumentSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DocumentSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/document/DocumentSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DocumentSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function available_23 () : java.lang.Boolean {
      return documentSearchCriteria.DateCriterionChoice.DateSearchType == DateSearchType.TC_FROMLIST
    }
    
    // 'available' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function available_32 () : java.lang.Boolean {
      return documentSearchCriteria.DateCriterionChoice.DateSearchType == DateSearchType.TC_ENTEREDRANGE
    }
    
    // 'def' attribute on InputSetRef at DocumentSearchDV.pcf: line 56, column 41
    function def_onEnter_61 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at DocumentSearchDV.pcf: line 56, column 41
    function def_refreshVariables_62 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TextInput (id=AuthorCriterion_Input) at DocumentSearchDV.pcf: line 15, column 48
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentSearchCriteria.Author = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=DocumentName_Input) at DocumentSearchDV.pcf: line 32, column 50
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentSearchCriteria.NameOrID = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentSearchCriteria.DateCriterionChoice.ChosenOption = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentSearchCriteria.DateCriterionChoice.DateRangeChoice = (__VALUE_TO_SET as typekey.DateRangeChoiceType)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentSearchCriteria.DateCriterionChoice.DateSearchType = (__VALUE_TO_SET as typekey.DateSearchType)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentSearchCriteria.DateCriterionChoice.StartDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentSearchCriteria.DateCriterionChoice.EndDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentSearchCriteria.DateCriterionChoice = (__VALUE_TO_SET as entity.DateCriterionChoice)
    }
    
    // 'value' attribute on TypeKeyInput (id=StatusCriterion_Input) at DocumentSearchDV.pcf: line 21, column 49
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentSearchCriteria.Status = (__VALUE_TO_SET as typekey.DocumentStatusType)
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at DocumentSearchDV.pcf: line 47, column 67
    function defaultSetter_53 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentSearchCriteria.Language = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'value' attribute on BooleanRadioInput (id=IncludeObsoletes_Input) at DocumentSearchDV.pcf: line 52, column 58
    function defaultSetter_58 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentSearchCriteria.IncludeObsoletes = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=TypeCriterion_Input) at DocumentSearchDV.pcf: line 27, column 43
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentSearchCriteria.Type = (__VALUE_TO_SET as typekey.DocumentType)
    }
    
    // DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function optionLabel_19 (VALUE :  java.lang.String) : java.lang.String {
      return gw.api.locale.DisplayKey.get(VALUE)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function valueRange_20 () : java.lang.Object {
      return documentSearchCriteria.DateCriterionChoice.Options
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function valueRoot_18 () : java.lang.Object {
      return documentSearchCriteria.DateCriterionChoice
    }
    
    // 'value' attribute on TextInput (id=AuthorCriterion_Input) at DocumentSearchDV.pcf: line 15, column 48
    function valueRoot_2 () : java.lang.Object {
      return documentSearchCriteria
    }
    
    // 'value' attribute on TextInput (id=AuthorCriterion_Input) at DocumentSearchDV.pcf: line 15, column 48
    function value_0 () : java.lang.String {
      return documentSearchCriteria.Author
    }
    
    // 'value' attribute on TextInput (id=DocumentName_Input) at DocumentSearchDV.pcf: line 32, column 50
    function value_12 () : java.lang.String {
      return documentSearchCriteria.NameOrID
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function value_16 () : java.lang.String {
      return documentSearchCriteria.DateCriterionChoice.ChosenOption
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function value_24 () : typekey.DateRangeChoiceType {
      return documentSearchCriteria.DateCriterionChoice.DateRangeChoice
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function value_28 () : java.lang.Object {
      return documentSearchCriteria.DateCriterionChoice.DateRangeChoice
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function value_29 () : typekey.DateSearchType {
      return documentSearchCriteria.DateCriterionChoice.DateSearchType
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function value_33 () : java.util.Date {
      return documentSearchCriteria.DateCriterionChoice.StartDate
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function value_39 () : java.util.Date {
      return documentSearchCriteria.DateCriterionChoice.EndDate
    }
    
    // 'value' attribute on TypeKeyInput (id=StatusCriterion_Input) at DocumentSearchDV.pcf: line 21, column 49
    function value_4 () : typekey.DocumentStatusType {
      return documentSearchCriteria.Status
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function value_47 () : entity.DateCriterionChoice {
      return documentSearchCriteria.DateCriterionChoice
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at DocumentSearchDV.pcf: line 47, column 67
    function value_52 () : typekey.LanguageType {
      return documentSearchCriteria.Language
    }
    
    // 'value' attribute on BooleanRadioInput (id=IncludeObsoletes_Input) at DocumentSearchDV.pcf: line 52, column 58
    function value_57 () : java.lang.Boolean {
      return documentSearchCriteria.IncludeObsoletes
    }
    
    // 'value' attribute on TypeKeyInput (id=TypeCriterion_Input) at DocumentSearchDV.pcf: line 27, column 43
    function value_8 () : typekey.DocumentType {
      return documentSearchCriteria.Type
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function verifyValueRangeIsAllowedType_21 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function verifyValueRangeIsAllowedType_21 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearchCriteria_Input) at DocumentSearchDV.pcf: line 40, column 61
    function verifyValueRange_22 () : void {
      var __valueRangeArg = documentSearchCriteria.DateCriterionChoice.Options
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_21(__valueRangeArg)
    }
    
    // 'visible' attribute on TypeKeyInput (id=Language_Input) at DocumentSearchDV.pcf: line 47, column 67
    function visible_51 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count > 1
    }
    
    property get documentSearchCriteria () : DocumentSearchCriteria {
      return getRequireValue("documentSearchCriteria", 0) as DocumentSearchCriteria
    }
    
    property set documentSearchCriteria ($arg :  DocumentSearchCriteria) {
      setRequireValue("documentSearchCriteria", 0, $arg)
    }
    
    
  }
  
  
}