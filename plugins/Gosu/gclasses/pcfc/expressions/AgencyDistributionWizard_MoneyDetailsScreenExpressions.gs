package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_MoneyDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyDistributionWizard_MoneyDetailsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_MoneyDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyDistributionWizard_MoneyDetailsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on PickerToolbarButton (id=AddSpecificPolicies) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 266, column 33
    function action_134 () : void {
      AgencyDistributionWizard_AddPoliciesPopup.push(wizardState)
    }
    
    // 'action' attribute on PickerToolbarButton (id=AddStatements) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 187, column 33
    function action_95 () : void {
      AgencyDistributionWizard_AddStatementsPopup.push(wizardState)
    }
    
    // 'action' attribute on PickerToolbarButton (id=AddSpecificPolicies) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 266, column 33
    function action_dest_135 () : pcf.api.Destination {
      return pcf.AgencyDistributionWizard_AddPoliciesPopup.createDestination(wizardState)
    }
    
    // 'action' attribute on PickerToolbarButton (id=AddStatements) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 187, column 33
    function action_dest_96 () : pcf.api.Destination {
      return pcf.AgencyDistributionWizard_AddStatementsPopup.createDestination(wizardState)
    }
    
    // 'value' attribute on RangeInput (id=PolicyFilter_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 252, column 106
    function defaultSetter_128 (__VALUE_TO_SET :  java.lang.Object) : void {
      wizardState.PolicyFilter = (__VALUE_TO_SET as gw.api.web.producer.agencybill.AgencyBillMoneyReceivedSetup.PolicyFilterOption)
    }
    
    // 'value' attribute on RangeInput (id=SelectPromise_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 348, column 63
    function defaultSetter_178 (__VALUE_TO_SET :  java.lang.Object) : void {
      wizardState.PaymentMoneySetup.AppliedFromPromise = (__VALUE_TO_SET as entity.AgencyCyclePromise)
    }
    
    // 'value' attribute on RangeRadioInput (id=DistributeTo_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 130, column 28
    function defaultSetter_64 (__VALUE_TO_SET :  java.lang.Object) : void {
      wizardState.DistributeTo = (__VALUE_TO_SET as gw.agencybill.AgencyDistributionWizardHelper.DistributeToEnum)
    }
    
    // 'value' attribute on RangeRadioInput (id=DistributeAmounts_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 147, column 108
    function defaultSetter_72 (__VALUE_TO_SET :  java.lang.Object) : void {
      wizardState.DistributeAmounts = (__VALUE_TO_SET as gw.agencybill.AgencyDistributionWizardHelper.DistributeAmountsEnum)
    }
    
    // 'value' attribute on RangeRadioInput (id=DistributeBy_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 159, column 151
    function defaultSetter_81 (__VALUE_TO_SET :  java.lang.Object) : void {
      wizardState.DistributeBy = (__VALUE_TO_SET as gw.agencybill.AgencyDistributionWizardHelper.DistributeByEnum)
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=Prefill_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 168, column 113
    function defaultSetter_90 (__VALUE_TO_SET :  java.lang.Object) : void {
      moneySetup.Prefill = (__VALUE_TO_SET as typekey.AgencyCycleDistPrefill)
    }
    
    // 'editable' attribute on DetailViewPanel (id=DistributionDetailsDV) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 119, column 36
    function editable_198 () : java.lang.Boolean {
      return wizardState.AllowChangeOfDistribution
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 13, column 75
    function initialValue_0 () : gw.api.web.producer.agencybill.AgencyBillMoneyReceivedSetup {
      return wizardState.MoneySetup
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 17, column 33
    function initialValue_1 () : BaseMoneyReceived {
      return moneySetup.Money
    }
    
    // 'label' attribute on TextInput (id=SelectSavedDistribution_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 361, column 51
    function label_186 () : java.lang.Object {
      return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.SavedDistribution", moneySetup.DistributionTypeName)
    }
    
    // 'label' attribute on TextInput (id=ModifyingDistribution_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 371, column 51
    function label_192 () : java.lang.Object {
      return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.ModifyingDistribution", moneySetup.DistributionTypeName)
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=AddSpecificPolicies) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 266, column 33
    function onPick_136 (PickedValue :  PolicyPeriod[]) : void {
      moneySetup.addPolicies(PickedValue)
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=AddStatements) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 187, column 33
    function onPick_97 (PickedValue :  StatementInvoice[]) : void {
      moneySetup.addDistributeToStatements(PickedValue)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetOwed_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 225, column 55
    function sortValue_100 (statement :  entity.StatementInvoice) : java.lang.Object {
      return statement.NetAmountDue
    }
    
    // 'value' attribute on DateCell (id=BillDate_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 230, column 52
    function sortValue_101 (statement :  entity.StatementInvoice) : java.lang.Object {
      return statement.EventDate
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 234, column 50
    function sortValue_102 (statement :  entity.StatementInvoice) : java.lang.Object {
      return statement.DueDate
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 295, column 56
    function sortValue_137 (policy :  entity.PolicyPeriod) : java.lang.Object {
      return policy.PolicyNumberLong
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 300, column 68
    function sortValue_138 (policy :  entity.PolicyPeriod) : java.lang.Object {
      return policy.Policy.Account.AccountNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 305, column 52
    function sortValue_139 (policy :  entity.PolicyPeriod) : java.lang.Object {
      return policy.Policy.LOBCode
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentStatus_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 310, column 62
    function sortValue_140 (policy :  entity.PolicyPeriod) : java.lang.Object {
      return policy.Policy.Account.DelinquencyStatus
    }
    
    // 'value' attribute on TextCell (id=Producer_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 327, column 52
    function sortValue_141 (policy :  entity.PolicyPeriod) : java.lang.Object {
      return policy.PrimaryPolicyCommission.ProducerCode.Producer
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 331, column 81
    function sortValue_142 (policy :  entity.PolicyPeriod) : java.lang.Object {
      return policy.PrimaryPolicyCommission.ProducerCode.Code
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 214, column 56
    function sortValue_98 (statement :  entity.StatementInvoice) : java.lang.Object {
      return statement.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 218, column 56
    function sortValue_99 (statement :  entity.StatementInvoice) : java.lang.Object {
      return statement.DisplayStatus
    }
    
    // '$$sumValue' attribute on RowIterator (id=StatementsIterator) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 225, column 55
    function sumValueRoot_104 (statement :  entity.StatementInvoice) : java.lang.Object {
      return statement
    }
    
    // 'footerSumValue' attribute on RowIterator (id=StatementsIterator) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 225, column 55
    function sumValue_103 (statement :  entity.StatementInvoice) : java.lang.Object {
      return statement.NetAmountDue
    }
    
    // 'toRemove' attribute on RowIterator (id=StatementsIterator) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 205, column 57
    function toRemove_124 (statement :  entity.StatementInvoice) : void {
      moneySetup.removeDistributeToStatements({statement})
    }
    
    // 'toRemove' attribute on RowIterator (id=PoliciesIterator) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 286, column 53
    function toRemove_172 (policy :  entity.PolicyPeriod) : void {
      moneySetup.removePolicies(new PolicyPeriod[]{policy})
    }
    
    // 'valueRange' attribute on RangeInput (id=PolicyFilter_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 252, column 106
    function valueRange_130 () : java.lang.Object {
      return wizardState.PolicyFilterValues
    }
    
    // 'valueRange' attribute on RangeInput (id=SelectPromise_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 348, column 63
    function valueRange_180 () : java.lang.Object {
      return moneySetup.Producer.findActivePromises()
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DistributeTo_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 130, column 28
    function valueRange_66 () : java.lang.Object {
      return wizardState.DistributeToValues
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DistributeAmounts_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 147, column 108
    function valueRange_74 () : java.lang.Object {
      return wizardState.DistributeAmountsValues
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DistributeBy_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 159, column 151
    function valueRange_83 () : java.lang.Object {
      return wizardState.DistributeByValues
    }
    
    // 'value' attribute on RangeInput (id=SelectPromise_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 348, column 63
    function valueRoot_179 () : java.lang.Object {
      return wizardState.PaymentMoneySetup
    }
    
    // 'value' attribute on RangeRadioInput (id=DistributeTo_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 130, column 28
    function valueRoot_65 () : java.lang.Object {
      return wizardState
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=Prefill_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 168, column 113
    function valueRoot_91 () : java.lang.Object {
      return moneySetup
    }
    
    // 'value' attribute on RowIterator (id=StatementsIterator) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 205, column 57
    function value_125 () : entity.StatementInvoice[] {
      return moneySetup.DistributeToStatements
    }
    
    // 'value' attribute on RangeInput (id=PolicyFilter_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 252, column 106
    function value_127 () : gw.api.web.producer.agencybill.AgencyBillMoneyReceivedSetup.PolicyFilterOption {
      return wizardState.PolicyFilter
    }
    
    // 'value' attribute on RowIterator (id=PoliciesIterator) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 286, column 53
    function value_173 () : entity.PolicyPeriod[] {
      return moneySetup.PoliciesToInclude
    }
    
    // 'value' attribute on RangeInput (id=SelectPromise_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 348, column 63
    function value_177 () : entity.AgencyCyclePromise {
      return wizardState.PaymentMoneySetup.AppliedFromPromise
    }
    
    // 'value' attribute on TextInput (id=SelectSavedDistribution_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 361, column 51
    function value_187 () : entity.AgencyCycleDist {
      return moneySetup.SavedDistribution
    }
    
    // 'value' attribute on TextInput (id=ModifyingDistribution_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 371, column 51
    function value_193 () : entity.AgencyCycleDist {
      return moneySetup.PreviouslyExecutedDistribution
    }
    
    // 'value' attribute on RangeRadioInput (id=DistributeTo_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 130, column 28
    function value_63 () : gw.agencybill.AgencyDistributionWizardHelper.DistributeToEnum {
      return wizardState.DistributeTo
    }
    
    // 'value' attribute on RangeRadioInput (id=DistributeAmounts_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 147, column 108
    function value_71 () : gw.agencybill.AgencyDistributionWizardHelper.DistributeAmountsEnum {
      return wizardState.DistributeAmounts
    }
    
    // 'value' attribute on RangeRadioInput (id=DistributeBy_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 159, column 151
    function value_80 () : gw.agencybill.AgencyDistributionWizardHelper.DistributeByEnum {
      return wizardState.DistributeBy
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=Prefill_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 168, column 113
    function value_89 () : typekey.AgencyCycleDistPrefill {
      return moneySetup.Prefill
    }
    
    // 'valueRange' attribute on RangeInput (id=PolicyFilter_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 252, column 106
    function verifyValueRangeIsAllowedType_131 ($$arg :  gw.api.web.producer.agencybill.AgencyBillMoneyReceivedSetup.PolicyFilterOption[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PolicyFilter_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 252, column 106
    function verifyValueRangeIsAllowedType_131 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SelectPromise_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 348, column 63
    function verifyValueRangeIsAllowedType_181 ($$arg :  entity.AgencyCyclePromise[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SelectPromise_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 348, column 63
    function verifyValueRangeIsAllowedType_181 ($$arg :  gw.api.database.IQueryBeanResult<entity.AgencyCyclePromise>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SelectPromise_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 348, column 63
    function verifyValueRangeIsAllowedType_181 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DistributeTo_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 130, column 28
    function verifyValueRangeIsAllowedType_67 ($$arg :  gw.agencybill.AgencyDistributionWizardHelper.DistributeToEnum[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DistributeTo_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 130, column 28
    function verifyValueRangeIsAllowedType_67 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DistributeAmounts_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 147, column 108
    function verifyValueRangeIsAllowedType_75 ($$arg :  gw.agencybill.AgencyDistributionWizardHelper.DistributeAmountsEnum[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DistributeAmounts_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 147, column 108
    function verifyValueRangeIsAllowedType_75 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DistributeBy_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 159, column 151
    function verifyValueRangeIsAllowedType_84 ($$arg :  gw.agencybill.AgencyDistributionWizardHelper.DistributeByEnum[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DistributeBy_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 159, column 151
    function verifyValueRangeIsAllowedType_84 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PolicyFilter_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 252, column 106
    function verifyValueRange_132 () : void {
      var __valueRangeArg = wizardState.PolicyFilterValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_131(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=SelectPromise_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 348, column 63
    function verifyValueRange_182 () : void {
      var __valueRangeArg = moneySetup.Producer.findActivePromises()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_181(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DistributeTo_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 130, column 28
    function verifyValueRange_68 () : void {
      var __valueRangeArg = wizardState.DistributeToValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_67(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DistributeAmounts_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 147, column 108
    function verifyValueRange_76 () : void {
      var __valueRangeArg = wizardState.DistributeAmountsValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_75(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DistributeBy_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 159, column 151
    function verifyValueRange_85 () : void {
      var __valueRangeArg = wizardState.DistributeByValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_84(__valueRangeArg)
    }
    
    // 'visible' attribute on InputSet (id=Statements) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 174, column 69
    function visible_126 () : java.lang.Boolean {
      return wizardState.DistributeToStatementsAndPolicies
    }
    
    // 'visible' attribute on ListViewInput (id=SpecificPoliciesLV) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 258, column 58
    function visible_174 () : java.lang.Boolean {
      return wizardState.ShowSpecificPolicies
    }
    
    // 'visible' attribute on InputSet (id=Policies) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 242, column 123
    function visible_175 () : java.lang.Boolean {
      return wizardState.DistributeToStatementsAndPolicies && !moneySetup.DistributeToStatements.IsEmpty
    }
    
    // 'visible' attribute on RangeInput (id=SelectPromise_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 348, column 63
    function visible_176 () : java.lang.Boolean {
      return wizardState.PaymentMoneySetup != null
    }
    
    // 'visible' attribute on InputSet (id=Promises) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 339, column 55
    function visible_185 () : java.lang.Boolean {
      return wizardState.DistributeToPromise
    }
    
    // 'visible' attribute on InputSet (id=SavedDistribution) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 355, column 53
    function visible_191 () : java.lang.Boolean {
      return wizardState.DistributeToSaved
    }
    
    // 'visible' attribute on InputSet (id=ModifyingDistribution) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 365, column 62
    function visible_197 () : java.lang.Boolean {
      return moneySetup.EditingExecutedDistribution
    }
    
    // 'visible' attribute on AlertBar (id=CannotChangeDistributionAlert) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 112, column 57
    function visible_62 () : java.lang.Boolean {
      return !wizardState.AllowChangeOfDistribution
    }
    
    // 'visible' attribute on RangeRadioInput (id=DistributeAmounts_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 147, column 108
    function visible_70 () : java.lang.Boolean {
      return (wizardState.DistributeToStatementsAndPolicies || wizardState.DistributeToPromise)
    }
    
    // 'visible' attribute on RangeRadioInput (id=DistributeBy_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 159, column 151
    function visible_79 () : java.lang.Boolean {
      return (wizardState.DistributeToStatementsAndPolicies or wizardState.DistributeToPromise) and wizardState.SelectedEditDistribution 
    }
    
    // 'visible' attribute on TypeKeyRadioInput (id=Prefill_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 168, column 113
    function visible_88 () : java.lang.Boolean {
      return wizardState.DistributeToStatementsAndPolicies and wizardState.SelectedEditDistribution
    }
    
    // 'visible' attribute on InputSet at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 137, column 104
    function visible_94 () : java.lang.Boolean {
      return wizardState.DistributeToStatementsAndPolicies || wizardState.DistributeToPromise
    }
    
    property get money () : BaseMoneyReceived {
      return getVariableValue("money", 0) as BaseMoneyReceived
    }
    
    property set money ($arg :  BaseMoneyReceived) {
      setVariableValue("money", 0, $arg)
    }
    
    property get moneySetup () : gw.api.web.producer.agencybill.AgencyBillMoneyReceivedSetup {
      return getVariableValue("moneySetup", 0) as gw.api.web.producer.agencybill.AgencyBillMoneyReceivedSetup
    }
    
    property set moneySetup ($arg :  gw.api.web.producer.agencybill.AgencyBillMoneyReceivedSetup) {
      setVariableValue("moneySetup", 0, $arg)
    }
    
    property get wizardState () : gw.agencybill.AgencyDistributionWizardHelper {
      return getRequireValue("wizardState", 0) as gw.agencybill.AgencyDistributionWizardHelper
    }
    
    property set wizardState ($arg :  gw.agencybill.AgencyDistributionWizardHelper) {
      setRequireValue("wizardState", 0, $arg)
    }
    
    property get Header() : String {
      if (wizardState.IsCreditDistribution) {
        return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.DistributionDetailsHeader.CreditDistribution")
      } else if (wizardState.IsPayment) {
        return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.DistributionDetailsHeader.Payment")
      } else {
        return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.DistributionDetailsHeader.Promise")
      }
    }
    
    property get DistributionDescription() : String {
      if (wizardState.IsCreditDistribution) {
        return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.DistributionDescription.CreditDistribution")
      } else if (wizardState.IsPayment) {
        return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.DistributionDescription.Payment")
      } else {
        return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.DistributionDescription.Promise")
      }
    }
    
    property get AmountLabel() : String {
    if (wizardState.IsPayment) {
        return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.Amount.Payment")
      } else if (wizardState.IsPromise) {
        return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.Amount.Promise")
      } else {
        return ""
      }
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_MoneyDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanelExpressionsImpl extends AgencyDistributionWizard_MoneyDetailsScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 82, column 57
    function action_31 () : void {
      NewPaymentInstrumentPopup.push(gw.payment.PaymentInstrumentFilters.agencyPaymentInstrumentOptions,moneySetup.Producer,true)
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 82, column 57
    function action_dest_32 () : pcf.api.Destination {
      return pcf.NewPaymentInstrumentPopup.createDestination(gw.payment.PaymentInstrumentFilters.agencyPaymentInstrumentOptions,moneySetup.Producer,true)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=UnappliedBalance_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 47, column 56
    function currency_10 () : typekey.Currency {
      return moneySetup.Producer.Currency
    }
    
    // 'value' attribute on DateInput (id=ReceivedDate_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 56, column 39
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      money.ReceivedDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 66, column 56
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      money.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on RangeInput (id=PaymentInstrument_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 82, column 57
    function defaultSetter_36 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentMoney.PaymentInstrument = (__VALUE_TO_SET as entity.PaymentInstrument)
    }
    
    // 'value' attribute on TextInput (id=RefNumber_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 93, column 58
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentMoney.RefNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 100, column 27
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      money.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 106, column 43
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentMoney.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on DateInput (id=ReceivedDate_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 56, column 39
    function editable_13 () : java.lang.Boolean {
      return !wizardState.IsCreditDistribution
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 25, column 51
    function initialValue_2 () : gw.payment.PaymentInstrumentRange {
      return new gw.payment.PaymentInstrumentRange(moneySetup.Producer.PaymentInstruments) 
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 29, column 86
    function initialValue_3 () : com.google.common.collect.ImmutableList<typekey.PaymentMethod> {
      return gw.payment.PaymentInstrumentFilters.agencyPaymentInstrumentFilter
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 33, column 44
    function initialValue_4 () : entity.AgencyBillMoneyRcvd {
      return wizardState.PaymentMoneySetup.Money
    }
    
    // 'label' attribute on Label at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 49, column 147
    function label_12 () : java.lang.String {
      return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.DistributionHeader", moneySetup.DistributionTypeName)
    }
    
    // 'label' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 66, column 56
    function label_21 () : java.lang.Object {
      return AmountLabel
    }
    
    // 'label' attribute on Label at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 70, column 27
    function label_30 () : java.lang.String {
      return Header
    }
    
    // 'label' attribute on TextInput (id=Description_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 106, column 43
    function label_55 () : java.lang.Object {
      return DistributionDescription
    }
    
    // 'onPick' attribute on RangeInput (id=PaymentInstrument_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 82, column 57
    function onPick_34 (PickedValue :  PaymentInstrument) : void {
      paymentInstrumentRange.addPaymentInstrument(paymentMoney.PaymentInstrument)
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 66, column 56
    function validationExpression_19 () : java.lang.Object {
      return !money.Amount.IsPositive ? DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.Error.InvalidAmount")  : null
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 82, column 57
    function valueRange_38 () : java.lang.Object {
      return gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, paymentInstrumentFilter)  
    }
    
    // 'value' attribute on DateInput (id=ReceivedDate_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 56, column 39
    function valueRoot_16 () : java.lang.Object {
      return money
    }
    
    // 'value' attribute on RangeInput (id=PaymentInstrument_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 82, column 57
    function valueRoot_37 () : java.lang.Object {
      return paymentMoney
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 41, column 40
    function valueRoot_6 () : java.lang.Object {
      return moneySetup
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnappliedBalance_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 47, column 56
    function valueRoot_9 () : java.lang.Object {
      return moneySetup.Producer
    }
    
    // 'value' attribute on DateInput (id=ReceivedDate_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 56, column 39
    function value_14 () : java.util.Date {
      return money.ReceivedDate
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 66, column 56
    function value_22 () : gw.pl.currency.MonetaryAmount {
      return money.Amount
    }
    
    // 'value' attribute on RangeInput (id=PaymentInstrument_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 82, column 57
    function value_35 () : entity.PaymentInstrument {
      return paymentMoney.PaymentInstrument
    }
    
    // 'value' attribute on TextInput (id=RefNumber_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 93, column 58
    function value_44 () : java.lang.String {
      return paymentMoney.RefNumber
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 41, column 40
    function value_5 () : entity.Producer {
      return moneySetup.Producer
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 100, column 27
    function value_50 () : java.lang.String {
      return money.Name
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 106, column 43
    function value_56 () : java.lang.String {
      return paymentMoney.Description
    }
    
    // 'value' attribute on MonetaryAmountInput (id=UnappliedBalance_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 47, column 56
    function value_8 () : gw.pl.currency.MonetaryAmount {
      return moneySetup.Producer.UnappliedAmount
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 82, column 57
    function verifyValueRangeIsAllowedType_39 ($$arg :  entity.PaymentInstrument[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 82, column 57
    function verifyValueRangeIsAllowedType_39 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentInstrument>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 82, column 57
    function verifyValueRangeIsAllowedType_39 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 82, column 57
    function verifyValueRange_40 () : void {
      var __valueRangeArg = gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, paymentInstrumentFilter)  
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_39(__valueRangeArg)
    }
    
    // 'visible' attribute on InputSet at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 72, column 42
    function visible_49 () : java.lang.Boolean {
      return paymentMoney != null
    }
    
    property get paymentInstrumentFilter () : com.google.common.collect.ImmutableList<typekey.PaymentMethod> {
      return getVariableValue("paymentInstrumentFilter", 1) as com.google.common.collect.ImmutableList<typekey.PaymentMethod>
    }
    
    property set paymentInstrumentFilter ($arg :  com.google.common.collect.ImmutableList<typekey.PaymentMethod>) {
      setVariableValue("paymentInstrumentFilter", 1, $arg)
    }
    
    property get paymentInstrumentRange () : gw.payment.PaymentInstrumentRange {
      return getVariableValue("paymentInstrumentRange", 1) as gw.payment.PaymentInstrumentRange
    }
    
    property set paymentInstrumentRange ($arg :  gw.payment.PaymentInstrumentRange) {
      setVariableValue("paymentInstrumentRange", 1, $arg)
    }
    
    property get paymentMoney () : entity.AgencyBillMoneyRcvd {
      return getVariableValue("paymentMoney", 1) as entity.AgencyBillMoneyRcvd
    }
    
    property set paymentMoney ($arg :  entity.AgencyBillMoneyRcvd) {
      setVariableValue("paymentMoney", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_MoneyDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends AgencyDistributionWizard_MoneyDetailsScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 295, column 56
    function action_143 () : void {
      PolicySummary.push(policy)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 300, column 68
    function action_148 () : void {
      AccountSummaryPopup.push(policy.Policy.Account)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 295, column 56
    function action_dest_144 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(policy)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 300, column 68
    function action_dest_149 () : pcf.api.Destination {
      return pcf.AccountSummaryPopup.createDestination(policy.Policy.Account)
    }
    
    // 'checkBoxVisible' attribute on RowIterator (id=PoliciesIterator) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 286, column 53
    function checkBoxVisible_171 () : java.lang.Boolean {
      return wizardState.AllowChangeOfDistribution
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 295, column 56
    function valueRoot_146 () : java.lang.Object {
      return policy
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 300, column 68
    function valueRoot_151 () : java.lang.Object {
      return policy.Policy.Account
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 305, column 52
    function valueRoot_154 () : java.lang.Object {
      return policy.Policy
    }
    
    // 'value' attribute on TextCell (id=Producer_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 327, column 52
    function valueRoot_166 () : java.lang.Object {
      return policy.PrimaryPolicyCommission.ProducerCode
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 295, column 56
    function value_145 () : java.lang.String {
      return policy.PolicyNumberLong
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 300, column 68
    function value_150 () : java.lang.String {
      return policy.Policy.Account.AccountNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 305, column 52
    function value_153 () : typekey.LOBCode {
      return policy.Policy.LOBCode
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentStatus_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 310, column 62
    function value_156 () : typekey.DelinquencyStatus {
      return policy.Policy.Account.DelinquencyStatus
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 315, column 53
    function value_159 () : java.util.Date {
      return policy.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 320, column 53
    function value_162 () : java.util.Date {
      return policy.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=Producer_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 327, column 52
    function value_165 () : entity.Producer {
      return policy.PrimaryPolicyCommission.ProducerCode.Producer
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 331, column 81
    function value_168 () : java.lang.String {
      return policy.PrimaryPolicyCommission.ProducerCode.Code
    }
    
    property get policy () : entity.PolicyPeriod {
      return getIteratedValue(1) as entity.PolicyPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_MoneyDetailsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AgencyDistributionWizard_MoneyDetailsScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 214, column 56
    function action_105 () : void {
      AgencyBillStatementDetailPopup.push(statement)
    }
    
    // 'action' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 214, column 56
    function action_dest_106 () : pcf.api.Destination {
      return pcf.AgencyBillStatementDetailPopup.createDestination(statement)
    }
    
    // 'checkBoxVisible' attribute on RowIterator (id=StatementsIterator) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 205, column 57
    function checkBoxVisible_123 () : java.lang.Boolean {
      return wizardState.AllowChangeOfDistribution
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=NetOwed_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 225, column 55
    function currency_115 () : typekey.Currency {
      return moneySetup.Producer.Currency
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 214, column 56
    function valueRoot_108 () : java.lang.Object {
      return statement
    }
    
    // 'value' attribute on TextCell (id=StatementNumber_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 214, column 56
    function value_107 () : java.lang.String {
      return statement.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 218, column 56
    function value_110 () : java.lang.String {
      return statement.DisplayStatus
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetOwed_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 225, column 55
    function value_113 () : gw.pl.currency.MonetaryAmount {
      return statement.NetAmountDue
    }
    
    // 'value' attribute on DateCell (id=BillDate_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 230, column 52
    function value_117 () : java.util.Date {
      return statement.EventDate
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at AgencyDistributionWizard_MoneyDetailsScreen.pcf: line 234, column 50
    function value_120 () : java.util.Date {
      return statement.DueDate
    }
    
    property get statement () : entity.StatementInvoice {
      return getIteratedValue(1) as entity.StatementInvoice
    }
    
    
  }
  
  
}