package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/SourceOfFundsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SourceOfFundsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/SourceOfFundsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SourceOfFundsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (fundsSourceTracker :  FundsSourceTracker) : int {
      return 0
    }
    
    // 'actionAvailable' attribute on TextInput (id=source_Input) at SourceOfFundsPopup.pcf: line 21, column 211
    function actionAvailable_1 () : java.lang.Boolean {
      return fundsSourceTracker.TypeBeingTracked != FundsSourceType.TC_UNAPPLIEDFUND
    }
    
    // 'action' attribute on TextInput (id=source_Input) at SourceOfFundsPopup.pcf: line 21, column 211
    function action_0 () : void {
      fundsSourceTracker.openTrackableDetails()
    }
    
    // 'def' attribute on PanelRef at SourceOfFundsPopup.pcf: line 25, column 52
    function def_onEnter_4 (def :  pcf.FundsAllotmentLV) : void {
      def.onEnter(fundsSourceTracker)
    }
    
    // 'def' attribute on PanelRef at SourceOfFundsPopup.pcf: line 25, column 52
    function def_refreshVariables_5 (def :  pcf.FundsAllotmentLV) : void {
      def.refreshVariables(fundsSourceTracker)
    }
    
    // 'value' attribute on TextInput (id=source_Input) at SourceOfFundsPopup.pcf: line 21, column 211
    function value_2 () : java.lang.String {
      return DisplayKey.get("Web.FundsSource.LongDescription", fundsSourceTracker.Description, fundsSourceTracker.EventDate.format("short"), fundsSourceTracker.TotalAmount.render())
    }
    
    override property get CurrentLocation () : pcf.SourceOfFundsPopup {
      return super.CurrentLocation as pcf.SourceOfFundsPopup
    }
    
    property get fundsSourceTracker () : FundsSourceTracker {
      return getVariableValue("fundsSourceTracker", 0) as FundsSourceTracker
    }
    
    property set fundsSourceTracker ($arg :  FundsSourceTracker) {
      setVariableValue("fundsSourceTracker", 0, $arg)
    }
    
    
  }
  
  
}