package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/FundsTrackerLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class FundsTrackerLVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/FundsTrackerLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FundsTrackerLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at FundsTrackerLV.pcf: line 23, column 105
    function filters_0 () : gw.api.filters.IFilter[] {
      return gw.pcf.fundstracking.AccountDetailFundsTrackingHelper.getUnappliedFilters(account)
    }
    
    // 'label' attribute on TextCell (id=Description_Cell) at FundsTrackerLV.pcf: line 31, column 45
    function label_1 () : java.lang.Object {
      return TypeName
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at FundsTrackerLV.pcf: line 31, column 45
    function sortValue_2 (fundsTracker :  entity.FundsTracker) : java.lang.Object {
      return fundsTracker.Description
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at FundsTrackerLV.pcf: line 37, column 43
    function sortValue_3 (fundsTracker :  entity.FundsTracker) : java.lang.Object {
      return fundsTracker.EventDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnallottedAmount_Cell) at FundsTrackerLV.pcf: line 44, column 50
    function sortValue_4 (fundsTracker :  entity.FundsTracker) : java.lang.Object {
      return fundsTracker.UnallottedAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalAmount_Cell) at FundsTrackerLV.pcf: line 51, column 45
    function sortValue_5 (fundsTracker :  entity.FundsTracker) : java.lang.Object {
      return fundsTracker.TotalAmount
    }
    
    // 'value' attribute on TextCell (id=Reason_Cell) at FundsTrackerLV.pcf: line 56, column 55
    function sortValue_6 (fundsTracker :  entity.FundsTracker) : java.lang.Object {
      return fundsTracker.PaymentReversalReason
    }
    
    // 'value' attribute on RowIterator (id=FundsIterator) at FundsTrackerLV.pcf: line 18, column 41
    function value_27 () : entity.FundsTracker[] {
      return gw.pcf.fundstracking.AccountDetailFundsTrackingHelper.getFundsTrackers(account, type)
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get type () : gw.entity.IEntityType {
      return getRequireValue("type", 0) as gw.entity.IEntityType
    }
    
    property set type ($arg :  gw.entity.IEntityType) {
      setRequireValue("type", 0, $arg)
    }
    
    function goToPopup(fundsTracker : FundsTracker) : pcf.api.Location{
      if (fundsTracker typeis FundsSourceTracker){
         return SourceOfFundsPopup.push(fundsTracker)
      } else if (fundsTracker typeis FundsUseTracker){
        return UseOfFundsPopup.push(fundsTracker)
      }
      return null //unreachable  
    }
    
    property get TypeName() : String {
      return (type == FundsSourceTracker) ? DisplayKey.get("Web.AccountDetailFundsTracking.FundsSource") : DisplayKey.get("Web.AccountDetailFundsTracking.FundsUse")
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/FundsTrackerLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends FundsTrackerLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Description_Cell) at FundsTrackerLV.pcf: line 31, column 45
    function action_7 () : void {
      goToPopup(fundsTracker)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=UnallottedAmount_Cell) at FundsTrackerLV.pcf: line 44, column 50
    function currency_18 () : typekey.Currency {
      return fundsTracker.Currency
    }
    
    // 'label' attribute on TextCell (id=Description_Cell) at FundsTrackerLV.pcf: line 31, column 45
    function label_8 () : java.lang.Object {
      return TypeName
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at FundsTrackerLV.pcf: line 31, column 45
    function valueRoot_10 () : java.lang.Object {
      return fundsTracker
    }
    
    // 'value' attribute on DateCell (id=EventDate_Cell) at FundsTrackerLV.pcf: line 37, column 43
    function value_13 () : java.util.Date {
      return fundsTracker.EventDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnallottedAmount_Cell) at FundsTrackerLV.pcf: line 44, column 50
    function value_16 () : gw.pl.currency.MonetaryAmount {
      return fundsTracker.UnallottedAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalAmount_Cell) at FundsTrackerLV.pcf: line 51, column 45
    function value_20 () : gw.pl.currency.MonetaryAmount {
      return fundsTracker.TotalAmount
    }
    
    // 'value' attribute on TextCell (id=Reason_Cell) at FundsTrackerLV.pcf: line 56, column 55
    function value_24 () : java.lang.String {
      return fundsTracker.PaymentReversalReason
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at FundsTrackerLV.pcf: line 31, column 45
    function value_9 () : java.lang.String {
      return fundsTracker.Description
    }
    
    property get fundsTracker () : entity.FundsTracker {
      return getIteratedValue(1) as entity.FundsTracker
    }
    
    
  }
  
  
}