package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/AccountNewTransferWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountNewTransferWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/AccountNewTransferWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountNewTransferWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'allowFinish' attribute on WizardStep (id=Step2) at AccountNewTransferWizard.pcf: line 31, column 93
    function allowFinish_3 () : java.lang.Boolean {
      return fundsTransferUtil.getTransferTargets().length > 0
    }
    
    // 'beforeCommit' attribute on Wizard (id=AccountNewTransferWizard) at AccountNewTransferWizard.pcf: line 10, column 35
    function beforeCommit_7 (pickedValue :  java.lang.Object) : void {
      initiateApvalActvtyIfUserLacksTransferAuthority_TDIC()
    }
    
    // 'canVisit' attribute on Wizard (id=AccountNewTransferWizard) at AccountNewTransferWizard.pcf: line 10, column 35
    static function canVisit_8 (account :  Account) : java.lang.Boolean {
      return perm.Transaction.cashtx
    }
    
    // 'initialValue' attribute on Variable at AccountNewTransferWizard.pcf: line 19, column 56
    function initialValue_0 () : gw.api.web.transaction.FundsTransferUtil {
      return createNewFundsTransferUtil()
    }
    
    // 'onExit' attribute on WizardStep (id=Step2) at AccountNewTransferWizard.pcf: line 31, column 93
    function onExit_4 () : void {
      fundsTransferUtil.setSources(); validateTransferAmount(); fundsTransferUtil.createTransfers();
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountNewTransferWizard.pcf: line 24, column 88
    function screen_onEnter_1 (def :  pcf.TransferDetailsScreen) : void {
      def.onEnter(fundsTransferUtil)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountNewTransferWizard.pcf: line 31, column 93
    function screen_onEnter_5 (def :  pcf.TransferConfirmationScreen) : void {
      def.onEnter(fundsTransferUtil)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountNewTransferWizard.pcf: line 24, column 88
    function screen_refreshVariables_2 (def :  pcf.TransferDetailsScreen) : void {
      def.refreshVariables(fundsTransferUtil)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountNewTransferWizard.pcf: line 31, column 93
    function screen_refreshVariables_6 (def :  pcf.TransferConfirmationScreen) : void {
      def.refreshVariables(fundsTransferUtil)
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewTransferWizard) at AccountNewTransferWizard.pcf: line 10, column 35
    function tabBar_onEnter_9 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewTransferWizard) at AccountNewTransferWizard.pcf: line 10, column 35
    function tabBar_refreshVariables_10 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AccountNewTransferWizard {
      return super.CurrentLocation as pcf.AccountNewTransferWizard
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get fundsTransferUtil () : gw.api.web.transaction.FundsTransferUtil {
      return getVariableValue("fundsTransferUtil", 0) as gw.api.web.transaction.FundsTransferUtil
    }
    
    property set fundsTransferUtil ($arg :  gw.api.web.transaction.FundsTransferUtil) {
      setVariableValue("fundsTransferUtil", 0, $arg)
    }
    
    function createNewFundsTransferUtil() : gw.api.web.transaction.FundsTransferUtil {
      fundsTransferUtil = new gw.api.web.transaction.FundsTransferUtil();
      fundsTransferUtil.addToTransfers(new FundsTransfer(account.Currency))
      fundsTransferUtil.SourceOwner = account;
      fundsTransferUtil.TargetType = TAccountOwnerType.TC_ACCOUNT
      return fundsTransferUtil;
    }
    
    function validateTransferAmount() {
      if (!fundsTransferUtil.validateAmounts()) {
        throw new gw.api.util.DisplayableException(DisplayKey.get("Web.NewTransferWizard.InsufficientUnappliedFundsForTransfer"))
      }
    }
    
    function initiateApvalActvtyIfUserLacksTransferAuthority_TDIC() {
      //VIJAY B - GBC-2856 - Manual Transfers are assigning to the Approval Transaction Queue
      var fundTransfer = fundsTransferUtil.getTransferTargets().first()
      var activity = fundTransfer.getOpenApprovalActivity()
      var groupName = gw.command.demo.GeneralUtil.findGroupByUserName("Approve Transactions")
      if (activity?.assignGroup(groupName)) {
        var queueName = activity.AssignedGroup.getQueue("Approve Transactions")
        if (queueName != null) {
          activity.assignActivityToQueue(queueName, groupName)
        }
      }
    }
    
    
  }
  
  
}