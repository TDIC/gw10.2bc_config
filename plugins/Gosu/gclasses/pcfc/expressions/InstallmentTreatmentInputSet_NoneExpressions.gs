package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/payment/InstallmentTreatmentInputSet.None.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InstallmentTreatmentInputSet_NoneExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/payment/InstallmentTreatmentInputSet.None.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InstallmentTreatmentInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToFirstInstallment_Input) at InstallmentTreatmentInputSet.None.pcf: line 42, column 39
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.DaysFromReferenceDateToFirstInstallment = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=MaximumNumberOfInstallments_Input) at InstallmentTreatmentInputSet.None.pcf: line 17, column 38
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.MaximumNumberOfInstallments = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeInput (id=FirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 53, column 47
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.BeforeAfterFirstInstallment = (__VALUE_TO_SET as gw.admin.paymentplan.When)
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleFirstInstallmentAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 63, column 52
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.FirstInstallmentAfter = (__VALUE_TO_SET as typekey.PaymentScheduledAfter)
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToSecondInstallment_Input) at InstallmentTreatmentInputSet.None.pcf: line 76, column 39
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.DaysFromReferenceDateToSecondInstallment = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeInput (id=SecondInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 87, column 47
    function defaultSetter_41 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.BeforeAfterSecondInstallment = (__VALUE_TO_SET as gw.admin.paymentplan.When)
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleSecondInstallmentAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 97, column 52
    function defaultSetter_49 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.SecondInstallmentAfter = (__VALUE_TO_SET as typekey.PaymentScheduledAfter)
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToOneTimeCharge_Input) at InstallmentTreatmentInputSet.None.pcf: line 110, column 39
    function defaultSetter_56 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.DaysFromReferenceDateToOneTimeCharge = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeInput (id=OneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 121, column 47
    function defaultSetter_60 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.BeforeAfterOneTimeCharge = (__VALUE_TO_SET as gw.admin.paymentplan.When)
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleOneTimeChargeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 131, column 52
    function defaultSetter_68 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.OneTimeChargeAfter = (__VALUE_TO_SET as typekey.PaymentScheduledAfter)
    }
    
    // 'value' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.None.pcf: line 28, column 64
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.SecondInstallmentChoice = (__VALUE_TO_SET as gw.admin.paymentplan.SecondInstallmentChoice)
    }
    
    // 'noneSelectedLabel' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.None.pcf: line 28, column 64
    function noneSelectedLabel_11 () : java.lang.String {
      return viewHelper.NoneSelectedLabel
    }
    
    // 'onChange' attribute on PostOnChange at InstallmentTreatmentInputSet.None.pcf: line 30, column 63
    function onChange_5 () : void {
      viewHelper.onHasSecondInstallmentChange()
    }
    
    // 'required' attribute on TextInput (id=MaximumNumberOfInstallments_Input) at InstallmentTreatmentInputSet.None.pcf: line 17, column 38
    function required_0 () : java.lang.Boolean {
      return viewHelper.RequiredFieldCalculator.MaximumNumberOfInstallmentsRequired
    }
    
    // 'required' attribute on TextInput (id=DaysFromEventToFirstInstallment_Input) at InstallmentTreatmentInputSet.None.pcf: line 42, column 39
    function required_17 () : java.lang.Boolean {
      return viewHelper.RequiredFieldCalculator.FirstInstallmentFieldsRequired
    }
    
    // 'required' attribute on TextInput (id=DaysFromEventToSecondInstallment_Input) at InstallmentTreatmentInputSet.None.pcf: line 76, column 39
    function required_35 () : java.lang.Boolean {
      return viewHelper.RequiredFieldCalculator.SecondInstallmentFieldsRequired
    }
    
    // 'required' attribute on TextInput (id=DaysFromEventToOneTimeCharge_Input) at InstallmentTreatmentInputSet.None.pcf: line 110, column 39
    function required_54 () : java.lang.Boolean {
      return viewHelper.RequiredFieldCalculator.OneTimeChargesFieldsRequired
    }
    
    // 'required' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.None.pcf: line 28, column 64
    function required_7 () : java.lang.Boolean {
      return viewHelper.RequiredFieldCalculator.HasSecondInstallmentRequired
    }
    
    // 'validationExpression' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.None.pcf: line 28, column 64
    function validationExpression_6 () : java.lang.Object {
      return viewHelper.validateSecondInstallmentFields()
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.None.pcf: line 28, column 64
    function valueRange_12 () : java.lang.Object {
      return gw.admin.paymentplan.SecondInstallmentChoice.getValues(viewHelper.RequiredFieldCalculator.CanShowSecondInstallmentNotOverridden)
    }
    
    // 'valueRange' attribute on RangeInput (id=FirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 53, column 47
    function valueRange_25 () : java.lang.Object {
      return gw.admin.paymentplan.When.AllValues
    }
    
    // 'value' attribute on TextInput (id=MaximumNumberOfInstallments_Input) at InstallmentTreatmentInputSet.None.pcf: line 17, column 38
    function valueRoot_3 () : java.lang.Object {
      return viewHelper
    }
    
    // 'value' attribute on TextInput (id=MaximumNumberOfInstallments_Input) at InstallmentTreatmentInputSet.None.pcf: line 17, column 38
    function value_1 () : java.lang.Integer {
      return viewHelper.MaximumNumberOfInstallments
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToFirstInstallment_Input) at InstallmentTreatmentInputSet.None.pcf: line 42, column 39
    function value_18 () : java.lang.Integer {
      return viewHelper.DaysFromReferenceDateToFirstInstallment
    }
    
    // 'value' attribute on RangeInput (id=FirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 53, column 47
    function value_22 () : gw.admin.paymentplan.When {
      return viewHelper.BeforeAfterFirstInstallment
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleFirstInstallmentAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 63, column 52
    function value_30 () : typekey.PaymentScheduledAfter {
      return viewHelper.FirstInstallmentAfter
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToSecondInstallment_Input) at InstallmentTreatmentInputSet.None.pcf: line 76, column 39
    function value_36 () : java.lang.Integer {
      return viewHelper.DaysFromReferenceDateToSecondInstallment
    }
    
    // 'value' attribute on RangeInput (id=SecondInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 87, column 47
    function value_40 () : gw.admin.paymentplan.When {
      return viewHelper.BeforeAfterSecondInstallment
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleSecondInstallmentAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 97, column 52
    function value_48 () : typekey.PaymentScheduledAfter {
      return viewHelper.SecondInstallmentAfter
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToOneTimeCharge_Input) at InstallmentTreatmentInputSet.None.pcf: line 110, column 39
    function value_55 () : java.lang.Integer {
      return viewHelper.DaysFromReferenceDateToOneTimeCharge
    }
    
    // 'value' attribute on RangeInput (id=OneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 121, column 47
    function value_59 () : gw.admin.paymentplan.When {
      return viewHelper.BeforeAfterOneTimeCharge
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleOneTimeChargeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 131, column 52
    function value_67 () : typekey.PaymentScheduledAfter {
      return viewHelper.OneTimeChargeAfter
    }
    
    // 'value' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.None.pcf: line 28, column 64
    function value_8 () : gw.admin.paymentplan.SecondInstallmentChoice {
      return viewHelper.SecondInstallmentChoice
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.None.pcf: line 28, column 64
    function verifyValueRangeIsAllowedType_13 ($$arg :  gw.admin.paymentplan.SecondInstallmentChoice[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.None.pcf: line 28, column 64
    function verifyValueRangeIsAllowedType_13 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=FirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 53, column 47
    function verifyValueRangeIsAllowedType_26 ($$arg :  gw.admin.paymentplan.When[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=FirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 53, column 47
    function verifyValueRangeIsAllowedType_26 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecondInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 87, column 47
    function verifyValueRangeIsAllowedType_44 ($$arg :  gw.admin.paymentplan.When[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecondInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 87, column 47
    function verifyValueRangeIsAllowedType_44 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 121, column 47
    function verifyValueRangeIsAllowedType_63 ($$arg :  gw.admin.paymentplan.When[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 121, column 47
    function verifyValueRangeIsAllowedType_63 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.None.pcf: line 28, column 64
    function verifyValueRange_14 () : void {
      var __valueRangeArg = gw.admin.paymentplan.SecondInstallmentChoice.getValues(viewHelper.RequiredFieldCalculator.CanShowSecondInstallmentNotOverridden)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_13(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=FirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 53, column 47
    function verifyValueRange_27 () : void {
      var __valueRangeArg = gw.admin.paymentplan.When.AllValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_26(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=SecondInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 87, column 47
    function verifyValueRange_45 () : void {
      var __valueRangeArg = gw.admin.paymentplan.When.AllValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_44(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=OneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.None.pcf: line 121, column 47
    function verifyValueRange_64 () : void {
      var __valueRangeArg = gw.admin.paymentplan.When.AllValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_63(__valueRangeArg)
    }
    
    // 'visible' attribute on InputSet at InstallmentTreatmentInputSet.None.pcf: line 66, column 83
    function visible_53 () : java.lang.Boolean {
      return viewHelper.RequiredFieldCalculator.CanShowSecondInstallmentFields
    }
    
    property get viewHelper () : gw.admin.paymentplan.InstallmentViewHelper {
      return getRequireValue("viewHelper", 0) as gw.admin.paymentplan.InstallmentViewHelper
    }
    
    property set viewHelper ($arg :  gw.admin.paymentplan.InstallmentViewHelper) {
      setRequireValue("viewHelper", 0, $arg)
    }
    
    
  }
  
  
}