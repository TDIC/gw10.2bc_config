package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/UserSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class UserSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/UserSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class UserSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at UserSearchDV.pcf: line 45, column 41
    function def_onEnter_32 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at UserSearchDV.pcf: line 18, column 56
    function def_onEnter_4 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.onEnter( new gw.api.name.SearchNameOwner (SearchCriteria.Contact)) 
    }
    
    // 'def' attribute on InputSetRef at UserSearchDV.pcf: line 18, column 56
    function def_onEnter_6 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.onEnter( new gw.api.name.SearchNameOwner (SearchCriteria.Contact)) 
    }
    
    // 'def' attribute on InputSetRef at UserSearchDV.pcf: line 45, column 41
    function def_refreshVariables_33 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'def' attribute on InputSetRef at UserSearchDV.pcf: line 18, column 56
    function def_refreshVariables_5 (def :  pcf.GlobalPersonNameInputSet_Japan) : void {
      def.refreshVariables( new gw.api.name.SearchNameOwner (SearchCriteria.Contact)) 
    }
    
    // 'def' attribute on InputSetRef at UserSearchDV.pcf: line 18, column 56
    function def_refreshVariables_7 (def :  pcf.GlobalPersonNameInputSet_default) : void {
      def.refreshVariables( new gw.api.name.SearchNameOwner (SearchCriteria.Contact)) 
    }
    
    // 'value' attribute on TextInput (id=UsernameCriterion_Input) at UserSearchDV.pcf: line 15, column 42
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.Username = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=GroupNameCriterion_Input) at UserSearchDV.pcf: line 23, column 43
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.GroupName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=GroupNameCriterionKanji_Input) at UserSearchDV.pcf: line 29, column 84
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.GroupNameKanji = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioInput (id=NotInAnyGroupCriterion_Input) at UserSearchDV.pcf: line 34, column 47
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.NotInAnyGroup = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=Role_Input) at UserSearchDV.pcf: line 41, column 34
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      SearchCriteria.Role = (__VALUE_TO_SET as entity.Role)
    }
    
    // 'label' attribute on TextInput (id=GroupNameCriterion_Input) at UserSearchDV.pcf: line 23, column 43
    function label_9 () : java.lang.Object {
      return (gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP) ? DisplayKey.get("Web.UserSearch.GroupNamePhonetic") : DisplayKey.get("Web.UserSearch.GroupName")
    }
    
    // 'mode' attribute on InputSetRef at UserSearchDV.pcf: line 18, column 56
    function mode_8 () : java.lang.Object {
      return gw.api.name.NameLocaleSettings.PCFMode
    }
    
    // 'valueRange' attribute on RangeInput (id=Role_Input) at UserSearchDV.pcf: line 41, column 34
    function valueRange_28 () : java.lang.Object {
      return SearchCriteria.getAllRoles()
    }
    
    // 'value' attribute on TextInput (id=UsernameCriterion_Input) at UserSearchDV.pcf: line 15, column 42
    function valueRoot_2 () : java.lang.Object {
      return SearchCriteria
    }
    
    // 'value' attribute on TextInput (id=UsernameCriterion_Input) at UserSearchDV.pcf: line 15, column 42
    function value_0 () : java.lang.String {
      return SearchCriteria.Username
    }
    
    // 'value' attribute on TextInput (id=GroupNameCriterion_Input) at UserSearchDV.pcf: line 23, column 43
    function value_10 () : java.lang.String {
      return SearchCriteria.GroupName
    }
    
    // 'value' attribute on TextInput (id=GroupNameCriterionKanji_Input) at UserSearchDV.pcf: line 29, column 84
    function value_16 () : java.lang.String {
      return SearchCriteria.GroupNameKanji
    }
    
    // 'value' attribute on BooleanRadioInput (id=NotInAnyGroupCriterion_Input) at UserSearchDV.pcf: line 34, column 47
    function value_21 () : java.lang.Boolean {
      return SearchCriteria.NotInAnyGroup
    }
    
    // 'value' attribute on RangeInput (id=Role_Input) at UserSearchDV.pcf: line 41, column 34
    function value_25 () : entity.Role {
      return SearchCriteria.Role
    }
    
    // 'valueRange' attribute on RangeInput (id=Role_Input) at UserSearchDV.pcf: line 41, column 34
    function verifyValueRangeIsAllowedType_29 ($$arg :  entity.Role[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Role_Input) at UserSearchDV.pcf: line 41, column 34
    function verifyValueRangeIsAllowedType_29 ($$arg :  gw.api.database.IQueryBeanResult<entity.Role>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Role_Input) at UserSearchDV.pcf: line 41, column 34
    function verifyValueRangeIsAllowedType_29 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Role_Input) at UserSearchDV.pcf: line 41, column 34
    function verifyValueRange_30 () : void {
      var __valueRangeArg = SearchCriteria.getAllRoles()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_29(__valueRangeArg)
    }
    
    // 'visible' attribute on TextInput (id=GroupNameCriterionKanji_Input) at UserSearchDV.pcf: line 29, column 84
    function visible_15 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP
    }
    
    property get SearchCriteria () : UserSearchCriteria {
      return getRequireValue("SearchCriteria", 0) as UserSearchCriteria
    }
    
    property set SearchCriteria ($arg :  UserSearchCriteria) {
      setRequireValue("SearchCriteria", 0, $arg)
    }
    
    
  }
  
  
}