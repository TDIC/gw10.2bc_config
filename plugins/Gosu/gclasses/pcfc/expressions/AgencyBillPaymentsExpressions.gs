package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillPaymentsExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillPaymentsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'location' attribute on LocationGroup (id=AgencyBillPayments) at AgencyBillPayments.pcf: line 14, column 56
    function action_0 () : void {
      pcf.AgencyBillExecutedPayments.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=AgencyBillPayments) at AgencyBillPayments.pcf: line 16, column 53
    function action_2 () : void {
      pcf.AgencyBillSavedPayments.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=AgencyBillPayments) at AgencyBillPayments.pcf: line 18, column 67
    function action_4 () : void {
      pcf.AgencyBillExecutedCreditDistributions.go(producer)
    }
    
    // 'location' attribute on LocationGroup (id=AgencyBillPayments) at AgencyBillPayments.pcf: line 14, column 56
    function action_dest_1 () : pcf.api.Destination {
      return pcf.AgencyBillExecutedPayments.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=AgencyBillPayments) at AgencyBillPayments.pcf: line 16, column 53
    function action_dest_3 () : pcf.api.Destination {
      return pcf.AgencyBillSavedPayments.createDestination(producer)
    }
    
    // 'location' attribute on LocationGroup (id=AgencyBillPayments) at AgencyBillPayments.pcf: line 18, column 67
    function action_dest_5 () : pcf.api.Destination {
      return pcf.AgencyBillExecutedCreditDistributions.createDestination(producer)
    }
    
    // LocationGroup (id=AgencyBillPayments) at AgencyBillPayments.pcf: line 7, column 31
    static function firstVisitableChildDestinationMethod_9 (producer :  Producer) : pcf.api.Destination {
      var dest : pcf.api.Destination
      dest = pcf.AgencyBillExecutedPayments.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AgencyBillSavedPayments.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AgencyBillExecutedCreditDistributions.createDestination(producer)
      if (dest.canVisitSelf()) {
        return dest
      }
      return null
    }
    
    // LocationGroup (id=AgencyBillPayments) at AgencyBillPayments.pcf: line 7, column 31
    static function parent_6 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    // 'tabBar' attribute on LocationGroup (id=AgencyBillPayments) at AgencyBillPayments.pcf: line 7, column 31
    function tabBar_onEnter_7 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on LocationGroup (id=AgencyBillPayments) at AgencyBillPayments.pcf: line 7, column 31
    function tabBar_refreshVariables_8 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AgencyBillPayments {
      return super.CurrentLocation as pcf.AgencyBillPayments
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}