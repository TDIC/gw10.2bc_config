package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/NewPaymentRequestWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewPaymentRequestWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/NewPaymentRequestWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewPaymentRequestWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=NewPaymentRequestWizard) at NewPaymentRequestWizard.pcf: line 9, column 34
    function afterCancel_10 () : void {
      DesktopGroup.go()
    }
    
    // 'afterCancel' attribute on Wizard (id=NewPaymentRequestWizard) at NewPaymentRequestWizard.pcf: line 9, column 34
    function afterCancel_dest_11 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewPaymentRequestWizard) at NewPaymentRequestWizard.pcf: line 9, column 34
    function afterFinish_15 () : void {
      DesktopGroup.go()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewPaymentRequestWizard) at NewPaymentRequestWizard.pcf: line 9, column 34
    function afterFinish_dest_16 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'allowFinish' attribute on WizardStep (id=newPaymentRequestConfirmation) at NewPaymentRequestWizard.pcf: line 38, column 91
    function allowFinish_7 () : java.lang.Boolean {
      return paymentRequest.Account != null
    }
    
    // 'allowNext' attribute on WizardStep (id=newPaymentRequestAccount) at NewPaymentRequestWizard.pcf: line 26, column 92
    function allowNext_1 () : java.lang.Boolean {
      return tAccountOwnerReference.TAccountOwner != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=NewPaymentRequestWizard) at NewPaymentRequestWizard.pcf: line 9, column 34
    function beforeCommit_12 (pickedValue :  java.lang.Object) : void {
      finalDataSet()
    }
    
    // 'initialValue' attribute on Variable at NewPaymentRequestWizard.pcf: line 18, column 60
    function initialValue_0 () : gw.api.web.accounting.TAccountOwnerReference {
      return new gw.api.web.accounting.TAccountOwnerReference()
    }
    
    // 'onExit' attribute on WizardStep (id=newPaymentRequestAccount) at NewPaymentRequestWizard.pcf: line 26, column 92
    function onExit_2 () : void {
      setUpPaymentRequest()
    }
    
    // 'screen' attribute on WizardStep (id=newPaymentRequestAccount) at NewPaymentRequestWizard.pcf: line 26, column 92
    function screen_onEnter_3 (def :  pcf.NewPaymentRequestAccountSearchScreen) : void {
      def.onEnter(tAccountOwnerReference)
    }
    
    // 'screen' attribute on WizardStep (id=newPaymentRequest) at NewPaymentRequestWizard.pcf: line 32, column 96
    function screen_onEnter_5 (def :  pcf.NewPaymentRequestScreen) : void {
      def.onEnter(paymentRequest)
    }
    
    // 'screen' attribute on WizardStep (id=newPaymentRequestConfirmation) at NewPaymentRequestWizard.pcf: line 38, column 91
    function screen_onEnter_8 (def :  pcf.NewPaymentRequestConfirmationScreen) : void {
      def.onEnter(paymentRequest)
    }
    
    // 'screen' attribute on WizardStep (id=newPaymentRequestAccount) at NewPaymentRequestWizard.pcf: line 26, column 92
    function screen_refreshVariables_4 (def :  pcf.NewPaymentRequestAccountSearchScreen) : void {
      def.refreshVariables(tAccountOwnerReference)
    }
    
    // 'screen' attribute on WizardStep (id=newPaymentRequest) at NewPaymentRequestWizard.pcf: line 32, column 96
    function screen_refreshVariables_6 (def :  pcf.NewPaymentRequestScreen) : void {
      def.refreshVariables(paymentRequest)
    }
    
    // 'screen' attribute on WizardStep (id=newPaymentRequestConfirmation) at NewPaymentRequestWizard.pcf: line 38, column 91
    function screen_refreshVariables_9 (def :  pcf.NewPaymentRequestConfirmationScreen) : void {
      def.refreshVariables(paymentRequest)
    }
    
    // 'tabBar' attribute on Wizard (id=NewPaymentRequestWizard) at NewPaymentRequestWizard.pcf: line 9, column 34
    function tabBar_onEnter_13 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=NewPaymentRequestWizard) at NewPaymentRequestWizard.pcf: line 9, column 34
    function tabBar_refreshVariables_14 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.NewPaymentRequestWizard {
      return super.CurrentLocation as pcf.NewPaymentRequestWizard
    }
    
    property get paymentRequest () : PaymentRequest {
      return getVariableValue("paymentRequest", 0) as PaymentRequest
    }
    
    property set paymentRequest ($arg :  PaymentRequest) {
      setVariableValue("paymentRequest", 0, $arg)
    }
    
    property get tAccountOwnerReference () : gw.api.web.accounting.TAccountOwnerReference {
      return getVariableValue("tAccountOwnerReference", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set tAccountOwnerReference ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setVariableValue("tAccountOwnerReference", 0, $arg)
    }
    
    function finalDataSet() {
      paymentRequest.StatusDate = paymentRequest.ChangeDeadlineDate
    }
    
    function setUpPaymentRequest() {
      if (paymentRequest == null) {
        paymentRequest = createRequestWithAccountCurrency()
      }
      else if (paymentRequest.Currency != tAccountOwnerReference.TAccountOwner.Currency)
      {
        paymentRequest.remove()
        paymentRequest = createRequestWithAccountCurrency()
      }
    
      paymentRequest.Account = (tAccountOwnerReference.TAccountOwner as Account)
    
    /**
       * US1158 - APW Payment Reversals
       * 3/11/2015 Alvin Lee
       *
       * Set the draft date based on the next available APW withdrawal date.
       * OOTB: request.DraftDate = util.DateUtil.addDays(request.RequestDate, request.Account.BillingPlan.DraftInterval)
       */
      paymentRequest.DraftDate = tdic.bc.config.payment.PaymentRequestUtil.getNextAvailableDraftDate(paymentRequest.RequestDate, paymentRequest.Account)
      if (gw.payment.PaymentInstrumentFilters.paymentRequestPaymentInstrumentFilter.contains(paymentRequest.Account.DefaultPaymentInstrument.PaymentMethod)) {
        paymentRequest.PaymentInstrument = paymentRequest.Account.DefaultPaymentInstrument
      }
      //paymentRequest.DraftDate = paymentRequest.Account.BillingPlan.DraftInterval.addTo(paymentRequest.RequestDate)
      paymentRequest.DueDate = paymentRequest.Account.BillingPlan.DraftInterval.addTo(paymentRequest.DraftDate)
    }
    
    function createRequestWithAccountCurrency() : PaymentRequest {
      var newPaymentRequest = new PaymentRequest(CurrentLocation, tAccountOwnerReference.TAccountOwner.Currency)
      newPaymentRequest.RequestDate = gw.api.util.DateUtil.currentDate();
      newPaymentRequest.Status = TC_REQUESTED;
      return newPaymentRequest
    }
    
    
  }
  
  
}