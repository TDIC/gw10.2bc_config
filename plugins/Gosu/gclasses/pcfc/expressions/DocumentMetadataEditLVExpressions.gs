package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/DocumentMetadataEditLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DocumentMetadataEditLVExpressions {
  @javax.annotation.Generated("config/web/pcf/document/DocumentMetadataEditLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DocumentMetadataEditLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at DocumentMetadataEditLV.pcf: line 25, column 45
    function initialValue_0 () : gw.document.DocumentBCContext {
      return DocumentApplicationContext as gw.document.DocumentBCContext
    }
    
    // 'value' attribute on TextCell (id=DocumentName_Cell) at DocumentMetadataEditLV.pcf: line 46, column 34
    function sortValue_1 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Name
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at DocumentMetadataEditLV.pcf: line 101, column 44
    function sortValue_10 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=Subtype_Cell) at DocumentMetadataEditLV.pcf: line 111, column 58
    function sortValue_11 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Subtype
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DocumentMetadataEditLV.pcf: line 51, column 41
    function sortValue_2 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Description
    }
    
    // 'value' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.pcf: line 60, column 41
    function sortValue_3 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.MimeType
    }
    
    // 'value' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.pcf: line 68, column 69
    function sortValue_4 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Language
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at DocumentMetadataEditLV.pcf: line 73, column 36
    function sortValue_6 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Author
    }
    
    // 'value' attribute on TextCell (id=Recipient_Cell) at DocumentMetadataEditLV.pcf: line 78, column 39
    function sortValue_7 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Recipient
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DocumentMetadataEditLV.pcf: line 85, column 51
    function sortValue_8 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.Status
    }
    
    // 'value' attribute on TypeKeyCell (id=SecurityType_Cell) at DocumentMetadataEditLV.pcf: line 92, column 52
    function sortValue_9 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : java.lang.Object {
      var document : entity.Document = (DocumentCreationInfo.Document)
return document.SecurityType
    }
    
    // 'toRemove' attribute on RowIterator at DocumentMetadataEditLV.pcf: line 35, column 54
    function toRemove_63 (DocumentCreationInfo :  gw.document.DocumentCreationInfo) : void {
      DocumentCreationInfos.remove(DocumentCreationInfo); DocumentCreationInfo.Document.remove()
    }
    
    // 'value' attribute on RowIterator at DocumentMetadataEditLV.pcf: line 35, column 54
    function value_64 () : gw.document.DocumentCreationInfo[] {
      return DocumentCreationInfos.toTypedArray()
    }
    
    // 'visible' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.pcf: line 68, column 69
    function visible_5 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count > 1
    }
    
    property get DocumentApplicationContext () : gw.document.DocumentApplicationContext {
      return getRequireValue("DocumentApplicationContext", 0) as gw.document.DocumentApplicationContext
    }
    
    property set DocumentApplicationContext ($arg :  gw.document.DocumentApplicationContext) {
      setRequireValue("DocumentApplicationContext", 0, $arg)
    }
    
    property get DocumentBCContext () : gw.document.DocumentBCContext {
      return getVariableValue("DocumentBCContext", 0) as gw.document.DocumentBCContext
    }
    
    property set DocumentBCContext ($arg :  gw.document.DocumentBCContext) {
      setVariableValue("DocumentBCContext", 0, $arg)
    }
    
    property get DocumentCreationInfos () : java.util.Collection<gw.document.DocumentCreationInfo> {
      return getRequireValue("DocumentCreationInfos", 0) as java.util.Collection<gw.document.DocumentCreationInfo>
    }
    
    property set DocumentCreationInfos ($arg :  java.util.Collection<gw.document.DocumentCreationInfo>) {
      setRequireValue("DocumentCreationInfos", 0, $arg)
    }
    
    function defaultSubType(document : Document){
        if (document.Type == null) {
        document.Subtype = null
      }
        var onlySubType : OnBaseDocumentSubtype_Ext = null
      if(document.Type == DocumentType.TC_BIL_CORRESPONDENCE_TDIC){
        document.Subtype = OnBaseDocumentSubtype_Ext.TC_LETTERS_TDIC
      }
      else {
        foreach (subtype in OnBaseDocumentSubtype_Ext.AllTypeKeys) {
          if (subtype.hasCategory(document.Type)) {
            if (onlySubType == null) {
              onlySubType = subtype
            } else {
              document.Subtype = null
            }
          }
        }
      }
     }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/document/DocumentMetadataEditLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DocumentMetadataEditLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=DocumentName_Cell) at DocumentMetadataEditLV.pcf: line 46, column 34
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DocumentMetadataEditLV.pcf: line 51, column 41
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.pcf: line 60, column 41
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.MimeType = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.pcf: line 68, column 69
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Language = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at DocumentMetadataEditLV.pcf: line 73, column 36
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Author = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=Recipient_Cell) at DocumentMetadataEditLV.pcf: line 78, column 39
    function defaultSetter_42 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Recipient = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DocumentMetadataEditLV.pcf: line 85, column 51
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Status = (__VALUE_TO_SET as typekey.DocumentStatusType)
    }
    
    // 'value' attribute on TypeKeyCell (id=SecurityType_Cell) at DocumentMetadataEditLV.pcf: line 92, column 52
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.SecurityType = (__VALUE_TO_SET as typekey.DocumentSecurityType)
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at DocumentMetadataEditLV.pcf: line 101, column 44
    function defaultSetter_55 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Type = (__VALUE_TO_SET as typekey.DocumentType)
    }
    
    // 'value' attribute on TypeKeyCell (id=Subtype_Cell) at DocumentMetadataEditLV.pcf: line 111, column 58
    function defaultSetter_59 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Subtype = (__VALUE_TO_SET as typekey.OnBaseDocumentSubtype_Ext)
    }
    
    // 'initialValue' attribute on Variable at DocumentMetadataEditLV.pcf: line 39, column 33
    function initialValue_12 () : entity.Document {
      return DocumentCreationInfo.Document
    }
    
    // RowIterator at DocumentMetadataEditLV.pcf: line 35, column 54
    function initializeVariables_62 () : void {
        document = DocumentCreationInfo.Document;

    }
    
    // 'onChange' attribute on PostOnChange at DocumentMetadataEditLV.pcf: line 103, column 50
    function onChange_53 () : void {
      defaultSubType(document)
    }
    
    // 'optionLabel' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.pcf: line 60, column 41
    function optionLabel_24 (VALUE :  java.lang.String) : java.lang.String {
      return document.getMimeTypeLabel(VALUE)
    }
    
    // 'valueRange' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.pcf: line 60, column 41
    function valueRange_25 () : java.lang.Object {
      return document.MimeTypeList
    }
    
    // 'valueRange' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.pcf: line 68, column 69
    function valueRange_32 () : java.lang.Object {
      return LanguageType.getTypeKeys( false )
    }
    
    // 'value' attribute on TextCell (id=DocumentName_Cell) at DocumentMetadataEditLV.pcf: line 46, column 34
    function valueRoot_15 () : java.lang.Object {
      return document
    }
    
    // 'value' attribute on TextCell (id=DocumentName_Cell) at DocumentMetadataEditLV.pcf: line 46, column 34
    function value_13 () : java.lang.String {
      return document.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at DocumentMetadataEditLV.pcf: line 51, column 41
    function value_17 () : java.lang.String {
      return document.Description
    }
    
    // 'value' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.pcf: line 60, column 41
    function value_21 () : java.lang.String {
      return document.MimeType
    }
    
    // 'value' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.pcf: line 68, column 69
    function value_29 () : typekey.LanguageType {
      return document.Language
    }
    
    // 'value' attribute on TextCell (id=Author_Cell) at DocumentMetadataEditLV.pcf: line 73, column 36
    function value_37 () : java.lang.String {
      return document.Author
    }
    
    // 'value' attribute on TextCell (id=Recipient_Cell) at DocumentMetadataEditLV.pcf: line 78, column 39
    function value_41 () : java.lang.String {
      return document.Recipient
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DocumentMetadataEditLV.pcf: line 85, column 51
    function value_45 () : typekey.DocumentStatusType {
      return document.Status
    }
    
    // 'value' attribute on TypeKeyCell (id=SecurityType_Cell) at DocumentMetadataEditLV.pcf: line 92, column 52
    function value_49 () : typekey.DocumentSecurityType {
      return document.SecurityType
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at DocumentMetadataEditLV.pcf: line 101, column 44
    function value_54 () : typekey.DocumentType {
      return document.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=Subtype_Cell) at DocumentMetadataEditLV.pcf: line 111, column 58
    function value_58 () : typekey.OnBaseDocumentSubtype_Ext {
      return document.Subtype
    }
    
    // 'valueRange' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.pcf: line 60, column 41
    function verifyValueRangeIsAllowedType_26 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.pcf: line 60, column 41
    function verifyValueRangeIsAllowedType_26 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.pcf: line 68, column 69
    function verifyValueRangeIsAllowedType_33 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.pcf: line 68, column 69
    function verifyValueRangeIsAllowedType_33 ($$arg :  typekey.LanguageType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=MimeType_Cell) at DocumentMetadataEditLV.pcf: line 60, column 41
    function verifyValueRange_27 () : void {
      var __valueRangeArg = document.MimeTypeList
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_26(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.pcf: line 68, column 69
    function verifyValueRange_34 () : void {
      var __valueRangeArg = LanguageType.getTypeKeys( false )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_33(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeCell (id=Language_Cell) at DocumentMetadataEditLV.pcf: line 68, column 69
    function visible_35 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count > 1
    }
    
    property get DocumentCreationInfo () : gw.document.DocumentCreationInfo {
      return getIteratedValue(1) as gw.document.DocumentCreationInfo
    }
    
    property get document () : entity.Document {
      return getVariableValue("document", 1) as entity.Document
    }
    
    property set document ($arg :  entity.Document) {
      setVariableValue("document", 1, $arg)
    }
    
    
  }
  
  
}