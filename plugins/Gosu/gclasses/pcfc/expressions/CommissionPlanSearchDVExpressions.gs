package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/CommissionPlanSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionPlanSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/CommissionPlanSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionPlanSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at CommissionPlanSearchDV.pcf: line 22, column 41
    function def_onEnter_4 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at CommissionPlanSearchDV.pcf: line 22, column 41
    function def_refreshVariables_5 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TextInput (id=NameCriterion_Input) at CommissionPlanSearchDV.pcf: line 18, column 38
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=NameCriterion_Input) at CommissionPlanSearchDV.pcf: line 18, column 38
    function valueRoot_2 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TextInput (id=NameCriterion_Input) at CommissionPlanSearchDV.pcf: line 18, column 38
    function value_0 () : java.lang.String {
      return searchCriteria.Name
    }
    
    property get searchCriteria () : CommissionPlanSearchCriteria {
      return getRequireValue("searchCriteria", 0) as CommissionPlanSearchCriteria
    }
    
    property set searchCriteria ($arg :  CommissionPlanSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    
  }
  
  
}