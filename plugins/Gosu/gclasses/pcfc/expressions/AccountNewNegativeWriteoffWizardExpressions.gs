package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/AccountNewNegativeWriteoffWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountNewNegativeWriteoffWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/AccountNewNegativeWriteoffWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountNewNegativeWriteoffWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'allowFinish' attribute on WizardStep (id=ConfirmationStep) at AccountNewNegativeWriteoffWizard.pcf: line 35, column 93
    function allowFinish_7 () : java.lang.Boolean {
      return uiWriteoff != null && uiWriteoff.Amount != null
    }
    
    // 'allowNext' attribute on WizardStep (id=TargetStep) at AccountNewNegativeWriteoffWizard.pcf: line 23, column 87
    function allowNext_1 () : java.lang.Boolean {
      return uiWriteoff != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=AccountNewNegativeWriteoffWizard) at AccountNewNegativeWriteoffWizard.pcf: line 8, column 43
    function beforeCommit_10 (pickedValue :  java.lang.Object) : void {
      uiWriteoff.doWriteOff()
    }
    
    // 'initialValue' attribute on Variable at AccountNewNegativeWriteoffWizard.pcf: line 17, column 56
    function initialValue_0 () : gw.api.web.accounting.UIWriteOffCreation {
      return createUIWriteoff()
    }
    
    // 'onExit' attribute on WizardStep (id=DetailsStep) at AccountNewNegativeWriteoffWizard.pcf: line 29, column 88
    function onExit_4 () : void {
      onExitFromDetailsStep()
    }
    
    // 'screen' attribute on WizardStep (id=TargetStep) at AccountNewNegativeWriteoffWizard.pcf: line 23, column 87
    function screen_onEnter_2 (def :  pcf.AccountNewNegativeWriteoffWizardTargetStepScreen) : void {
      def.onEnter(account, uiWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=DetailsStep) at AccountNewNegativeWriteoffWizard.pcf: line 29, column 88
    function screen_onEnter_5 (def :  pcf.NewNegativeWriteoffWizardDetailsStepScreen) : void {
      def.onEnter(uiWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at AccountNewNegativeWriteoffWizard.pcf: line 35, column 93
    function screen_onEnter_8 (def :  pcf.NewNegativeWriteoffWizardConfirmationStepScreen) : void {
      def.onEnter(uiWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=TargetStep) at AccountNewNegativeWriteoffWizard.pcf: line 23, column 87
    function screen_refreshVariables_3 (def :  pcf.AccountNewNegativeWriteoffWizardTargetStepScreen) : void {
      def.refreshVariables(account, uiWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=DetailsStep) at AccountNewNegativeWriteoffWizard.pcf: line 29, column 88
    function screen_refreshVariables_6 (def :  pcf.NewNegativeWriteoffWizardDetailsStepScreen) : void {
      def.refreshVariables(uiWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at AccountNewNegativeWriteoffWizard.pcf: line 35, column 93
    function screen_refreshVariables_9 (def :  pcf.NewNegativeWriteoffWizardConfirmationStepScreen) : void {
      def.refreshVariables(uiWriteoff)
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewNegativeWriteoffWizard) at AccountNewNegativeWriteoffWizard.pcf: line 8, column 43
    function tabBar_onEnter_11 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewNegativeWriteoffWizard) at AccountNewNegativeWriteoffWizard.pcf: line 8, column 43
    function tabBar_refreshVariables_12 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AccountNewNegativeWriteoffWizard {
      return super.CurrentLocation as pcf.AccountNewNegativeWriteoffWizard
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get uiWriteoff () : gw.api.web.accounting.UIWriteOffCreation {
      return getVariableValue("uiWriteoff", 0) as gw.api.web.accounting.UIWriteOffCreation
    }
    
    property set uiWriteoff ($arg :  gw.api.web.accounting.UIWriteOffCreation) {
      setVariableValue("uiWriteoff", 0, $arg)
    }
    
    
                  function onExitFromDetailsStep() {
                    uiWriteoff.initiateApprovalActivityIfUserLacksAuthority()
                  }
    
                  function createUIWriteoff(): gw.api.web.accounting.UIWriteOffCreation {
                    var factory = new gw.api.web.accounting.WriteOffFactory(CurrentLocation)
                    var writeOff = factory.createAccountNegativeWriteOff(account)
                    return new gw.api.web.accounting.UIWriteOffCreation(writeOff)
                  }
          
    
    
  }
  
  
}