package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/collection/CollectionAgencyDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CollectionAgencyDetailPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/collection/CollectionAgencyDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CollectionAgencyDetailPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (collectionAgency :  CollectionAgency) : int {
      return 0
    }
    
    // 'canEdit' attribute on Popup (id=CollectionAgencyDetailPopup) at CollectionAgencyDetailPopup.pcf: line 8, column 92
    function canEdit_2 () : java.lang.Boolean {
      return perm.System.colagencyedit
    }
    
    // 'def' attribute on ScreenRef at CollectionAgencyDetailPopup.pcf: line 15, column 61
    function def_onEnter_0 (def :  pcf.CollectionAgencyDetailScreen) : void {
      def.onEnter(collectionAgency)
    }
    
    // 'def' attribute on ScreenRef at CollectionAgencyDetailPopup.pcf: line 15, column 61
    function def_refreshVariables_1 (def :  pcf.CollectionAgencyDetailScreen) : void {
      def.refreshVariables(collectionAgency)
    }
    
    // 'title' attribute on Popup (id=CollectionAgencyDetailPopup) at CollectionAgencyDetailPopup.pcf: line 8, column 92
    static function title_3 (collectionAgency :  CollectionAgency) : java.lang.Object {
      return DisplayKey.get("Web.CollectionAgencyDetail.Title", collectionAgency)
    }
    
    override property get CurrentLocation () : pcf.CollectionAgencyDetailPopup {
      return super.CurrentLocation as pcf.CollectionAgencyDetailPopup
    }
    
    property get collectionAgency () : CollectionAgency {
      return getVariableValue("collectionAgency", 0) as CollectionAgency
    }
    
    property set collectionAgency ($arg :  CollectionAgency) {
      setVariableValue("collectionAgency", 0, $arg)
    }
    
    
  }
  
  
}