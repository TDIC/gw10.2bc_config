package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/NewPolicyWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewPolicyWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/NewPolicyWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewPolicyWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=NewPolicyWizard) at NewPolicyWizard.pcf: line 10, column 26
    function afterCancel_10 () : void {
      AccountOverview.go(account)
    }
    
    // 'afterCancel' attribute on Wizard (id=NewPolicyWizard) at NewPolicyWizard.pcf: line 10, column 26
    function afterCancel_dest_11 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(account)
    }
    
    // 'afterFinish' attribute on Wizard (id=NewPolicyWizard) at NewPolicyWizard.pcf: line 10, column 26
    function afterFinish_16 () : void {
      AccountOverview.go(account)
    }
    
    // 'afterFinish' attribute on Wizard (id=NewPolicyWizard) at NewPolicyWizard.pcf: line 10, column 26
    function afterFinish_dest_17 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(account)
    }
    
    // 'beforeCommit' attribute on Wizard (id=NewPolicyWizard) at NewPolicyWizard.pcf: line 10, column 26
    function beforeCommit_12 (pickedValue :  java.lang.Object) : void {
      chargesStepView.beforeCommit(); gw.api.web.policy.PolicyPeriodUtil.validatePolicyPeriodCreatedFromUI(issuance); issuance.execute()
    }
    
    // 'canVisit' attribute on Wizard (id=NewPolicyWizard) at NewPolicyWizard.pcf: line 10, column 26
    static function canVisit_13 (account :  Account) : java.lang.Boolean {
      return perm.PolicyPeriod.create
    }
    
    // 'initialValue' attribute on Variable at NewPolicyWizard.pcf: line 19, column 28
    function initialValue_0 () : PolicyPeriod {
      return gw.api.web.policy.NewPolicyUtil.createNewPolicyPeriod(account)
    }
    
    // 'initialValue' attribute on Variable at NewPolicyWizard.pcf: line 23, column 24
    function initialValue_1 () : Issuance {
      return gw.api.web.policy.NewPolicyUtil.createIssuance(account, policyPeriod)
    }
    
    // 'initialValue' attribute on Variable at NewPolicyWizard.pcf: line 27, column 49
    function initialValue_2 () : gw.invoice.InvoicingOverridesView {
      return new gw.invoice.InvoicingOverridesView(policyPeriod)
    }
    
    // 'initialValue' attribute on Variable at NewPolicyWizard.pcf: line 31, column 114
    function initialValue_3 () : java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView> {
      return com.google.common.collect.Maps.newHashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>()
    }
    
    // 'initialValue' attribute on Variable at NewPolicyWizard.pcf: line 35, column 62
    function initialValue_4 () : gw.web.policy.PolicyWizardChargeStepScreenView {
      return new gw.web.policy.PolicyWizardChargeStepScreenView(policyPeriod, issuance)
    }
    
    // 'onExit' attribute on WizardStep (id=BasicsStep) at NewPolicyWizard.pcf: line 41, column 77
    function onExit_5 () : void {
      invoicingOverridesView.update()
    }
    
    // 'screen' attribute on WizardStep (id=BasicsStep) at NewPolicyWizard.pcf: line 41, column 77
    function screen_onEnter_6 (def :  pcf.NewPolicyWizardSummaryStepScreen) : void {
      def.onEnter(account, policyPeriod, issuance.ProducerCodes, invoicingOverridesView, issuance)
    }
    
    // 'screen' attribute on WizardStep (id=ChargeStep) at NewPolicyWizard.pcf: line 47, column 78
    function screen_onEnter_8 (def :  pcf.NewPolicyWizardChargeStepScreen) : void {
      def.onEnter(account, policyPeriod, issuance, chargeToInvoicingOverridesViewMap, chargesStepView)
    }
    
    // 'screen' attribute on WizardStep (id=BasicsStep) at NewPolicyWizard.pcf: line 41, column 77
    function screen_refreshVariables_7 (def :  pcf.NewPolicyWizardSummaryStepScreen) : void {
      def.refreshVariables(account, policyPeriod, issuance.ProducerCodes, invoicingOverridesView, issuance)
    }
    
    // 'screen' attribute on WizardStep (id=ChargeStep) at NewPolicyWizard.pcf: line 47, column 78
    function screen_refreshVariables_9 (def :  pcf.NewPolicyWizardChargeStepScreen) : void {
      def.refreshVariables(account, policyPeriod, issuance, chargeToInvoicingOverridesViewMap, chargesStepView)
    }
    
    // 'tabBar' attribute on Wizard (id=NewPolicyWizard) at NewPolicyWizard.pcf: line 10, column 26
    function tabBar_onEnter_14 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=NewPolicyWizard) at NewPolicyWizard.pcf: line 10, column 26
    function tabBar_refreshVariables_15 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.NewPolicyWizard {
      return super.CurrentLocation as pcf.NewPolicyWizard
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get chargeToInvoicingOverridesViewMap () : java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView> {
      return getVariableValue("chargeToInvoicingOverridesViewMap", 0) as java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>
    }
    
    property set chargeToInvoicingOverridesViewMap ($arg :  java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>) {
      setVariableValue("chargeToInvoicingOverridesViewMap", 0, $arg)
    }
    
    property get chargesStepView () : gw.web.policy.PolicyWizardChargeStepScreenView {
      return getVariableValue("chargesStepView", 0) as gw.web.policy.PolicyWizardChargeStepScreenView
    }
    
    property set chargesStepView ($arg :  gw.web.policy.PolicyWizardChargeStepScreenView) {
      setVariableValue("chargesStepView", 0, $arg)
    }
    
    property get invoicingOverridesView () : gw.invoice.InvoicingOverridesView {
      return getVariableValue("invoicingOverridesView", 0) as gw.invoice.InvoicingOverridesView
    }
    
    property set invoicingOverridesView ($arg :  gw.invoice.InvoicingOverridesView) {
      setVariableValue("invoicingOverridesView", 0, $arg)
    }
    
    property get issuance () : Issuance {
      return getVariableValue("issuance", 0) as Issuance
    }
    
    property set issuance ($arg :  Issuance) {
      setVariableValue("issuance", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}