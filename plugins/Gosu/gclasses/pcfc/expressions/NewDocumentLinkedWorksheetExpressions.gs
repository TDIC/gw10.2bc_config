package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/NewDocumentLinkedWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewDocumentLinkedWorksheetExpressions {
  @javax.annotation.Generated("config/web/pcf/document/NewDocumentLinkedWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewDocumentLinkedWorksheetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (documentContainer :  DocumentContainer) : int {
      return 0
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod) : int {
      return 1
    }
    
    // 'beforeCommit' attribute on Worksheet (id=NewDocumentLinkedWorksheet) at NewDocumentLinkedWorksheet.pcf: line 13, column 79
    function beforeCommit_4 (pickedValue :  java.lang.Object) : void {
      gw.document.DocumentContainerMethods.addHistoryIfDCIsAccount(documentContainer)
    }
    
    // 'canVisit' attribute on Worksheet (id=NewDocumentLinkedWorksheet) at NewDocumentLinkedWorksheet.pcf: line 13, column 79
    static function canVisit_5 (documentContainer :  DocumentContainer, policyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.Document.create
    }
    
    // 'def' attribute on ScreenRef at NewDocumentLinkedWorksheet.pcf: line 32, column 63
    function def_onEnter_2 (def :  pcf.UploadDocumentScreen) : void {
      def.onEnter(DocumentApplicationContext)
    }
    
    // 'def' attribute on ScreenRef at NewDocumentLinkedWorksheet.pcf: line 32, column 63
    function def_refreshVariables_3 (def :  pcf.UploadDocumentScreen) : void {
      def.refreshVariables(DocumentApplicationContext)
    }
    
    // 'initialValue' attribute on Variable at NewDocumentLinkedWorksheet.pcf: line 26, column 33
    function initialValue_0 () : DocumentContainer {
      return policyPeriod != null ? policyPeriod.Policy : documentContainer
    }
    
    // 'initialValue' attribute on Variable at NewDocumentLinkedWorksheet.pcf: line 30, column 54
    function initialValue_1 () : gw.document.DocumentApplicationContext {
      return new gw.document.DocumentBCContext(documentContainer)
    }
    
    override property get CurrentLocation () : pcf.NewDocumentLinkedWorksheet {
      return super.CurrentLocation as pcf.NewDocumentLinkedWorksheet
    }
    
    property get DocumentApplicationContext () : gw.document.DocumentApplicationContext {
      return getVariableValue("DocumentApplicationContext", 0) as gw.document.DocumentApplicationContext
    }
    
    property set DocumentApplicationContext ($arg :  gw.document.DocumentApplicationContext) {
      setVariableValue("DocumentApplicationContext", 0, $arg)
    }
    
    property get documentContainer () : DocumentContainer {
      return getVariableValue("documentContainer", 0) as DocumentContainer
    }
    
    property set documentContainer ($arg :  DocumentContainer) {
      setVariableValue("documentContainer", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}