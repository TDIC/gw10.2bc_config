package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/payment/ClonePaymentPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ClonePaymentPlanExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/payment/ClonePaymentPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ClonePaymentPlanExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (paymentPlan :  PaymentPlan) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=ClonePaymentPlan) at ClonePaymentPlan.pcf: line 13, column 81
    function afterCancel_3 () : void {
      PaymentPlanDetail.go(paymentPlan)
    }
    
    // 'afterCancel' attribute on Page (id=ClonePaymentPlan) at ClonePaymentPlan.pcf: line 13, column 81
    function afterCancel_dest_4 () : pcf.api.Destination {
      return pcf.PaymentPlanDetail.createDestination(paymentPlan)
    }
    
    // 'afterCommit' attribute on Page (id=ClonePaymentPlan) at ClonePaymentPlan.pcf: line 13, column 81
    function afterCommit_5 (TopLocation :  pcf.api.Location) : void {
      PaymentPlanDetail.go(clonedPaymentPlan)
    }
    
    // 'def' attribute on ScreenRef at ClonePaymentPlan.pcf: line 24, column 63
    function def_onEnter_1 (def :  pcf.PaymentPlanDetailScreen) : void {
      def.onEnter(clonedPaymentPlan, true)
    }
    
    // 'def' attribute on ScreenRef at ClonePaymentPlan.pcf: line 24, column 63
    function def_refreshVariables_2 (def :  pcf.PaymentPlanDetailScreen) : void {
      def.refreshVariables(clonedPaymentPlan, true)
    }
    
    // 'initialValue' attribute on Variable at ClonePaymentPlan.pcf: line 22, column 34
    function initialValue_0 () : entity.PaymentPlan {
      return paymentPlan.makeClone() as PaymentPlan
    }
    
    // 'parent' attribute on Page (id=ClonePaymentPlan) at ClonePaymentPlan.pcf: line 13, column 81
    static function parent_6 (paymentPlan :  PaymentPlan) : pcf.api.Destination {
      return pcf.PaymentPlans.createDestination()
    }
    
    // 'title' attribute on Page (id=ClonePaymentPlan) at ClonePaymentPlan.pcf: line 13, column 81
    static function title_7 (paymentPlan :  PaymentPlan) : java.lang.Object {
      return DisplayKey.get("Web.ClonePaymentPlan.Title", paymentPlan)
    }
    
    override property get CurrentLocation () : pcf.ClonePaymentPlan {
      return super.CurrentLocation as pcf.ClonePaymentPlan
    }
    
    property get clonedPaymentPlan () : entity.PaymentPlan {
      return getVariableValue("clonedPaymentPlan", 0) as entity.PaymentPlan
    }
    
    property set clonedPaymentPlan ($arg :  entity.PaymentPlan) {
      setVariableValue("clonedPaymentPlan", 0, $arg)
    }
    
    property get paymentPlan () : PaymentPlan {
      return getVariableValue("paymentPlan", 0) as PaymentPlan
    }
    
    property set paymentPlan ($arg :  PaymentPlan) {
      setVariableValue("paymentPlan", 0, $arg)
    }
    
    
  }
  
  
}