package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffWizardDetailsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewWriteoffWizardDetailsStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffWizardDetailsStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewWriteoffWizardDetailsStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 56, column 37
    function currency_35 () : typekey.Currency {
      return uiWriteoff.WriteOff.TAccountOwner.Currency
    }
    
    // 'def' attribute on PanelRef at NewWriteoffWizardDetailsStepScreen.pcf: line 23, column 40
    function def_onEnter_2 (def :  pcf.MinimalTAccountOwnerDetailsDV_accountdefault) : void {
      def.onEnter(uiWriteoff.WriteOff.TAccountOwner, null)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffWizardDetailsStepScreen.pcf: line 23, column 40
    function def_onEnter_4 (def :  pcf.MinimalTAccountOwnerDetailsDV_accountdesignated) : void {
      def.onEnter(uiWriteoff.WriteOff.TAccountOwner, null)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffWizardDetailsStepScreen.pcf: line 23, column 40
    function def_onEnter_6 (def :  pcf.MinimalTAccountOwnerDetailsDV_policy) : void {
      def.onEnter(uiWriteoff.WriteOff.TAccountOwner, null)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffWizardDetailsStepScreen.pcf: line 23, column 40
    function def_onEnter_8 (def :  pcf.MinimalTAccountOwnerDetailsDV_producer) : void {
      def.onEnter(uiWriteoff.WriteOff.TAccountOwner, null)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffWizardDetailsStepScreen.pcf: line 23, column 40
    function def_refreshVariables_3 (def :  pcf.MinimalTAccountOwnerDetailsDV_accountdefault) : void {
      def.refreshVariables(uiWriteoff.WriteOff.TAccountOwner, null)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffWizardDetailsStepScreen.pcf: line 23, column 40
    function def_refreshVariables_5 (def :  pcf.MinimalTAccountOwnerDetailsDV_accountdesignated) : void {
      def.refreshVariables(uiWriteoff.WriteOff.TAccountOwner, null)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffWizardDetailsStepScreen.pcf: line 23, column 40
    function def_refreshVariables_7 (def :  pcf.MinimalTAccountOwnerDetailsDV_policy) : void {
      def.refreshVariables(uiWriteoff.WriteOff.TAccountOwner, null)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffWizardDetailsStepScreen.pcf: line 23, column 40
    function def_refreshVariables_9 (def :  pcf.MinimalTAccountOwnerDetailsDV_producer) : void {
      def.refreshVariables(uiWriteoff.WriteOff.TAccountOwner, null)
    }
    
    // 'value' attribute on RangeInput (id=ChargePattern_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 36, column 70
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      uiWriteoff.ChargePattern = (__VALUE_TO_SET as entity.ChargePattern)
    }
    
    // 'value' attribute on CheckBoxInput (id=UseFullAmount_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 46, column 27
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      uiWriteoff.UseFullAmount = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 56, column 37
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      uiWriteoff.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TypeKeyInput (id=Reason_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 65, column 47
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      uiWriteoff.Reason = (__VALUE_TO_SET as typekey.WriteoffReason)
    }
    
    // 'editable' attribute on CheckBoxInput (id=UseFullAmount_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 46, column 27
    function editable_21 () : java.lang.Boolean {
      return maximumAmount.IsPositive
    }
    
    // 'initialValue' attribute on Variable at NewWriteoffWizardDetailsStepScreen.pcf: line 13, column 30
    function initialValue_0 () : java.util.Date {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'initialValue' attribute on Variable at NewWriteoffWizardDetailsStepScreen.pcf: line 17, column 45
    function initialValue_1 () : gw.pl.currency.MonetaryAmount {
      return uiWriteoff.WriteOff.MaxPotentialWriteOffAmount
    }
    
    // 'label' attribute on CheckBoxInput (id=UseFullAmount_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 46, column 27
    function label_23 () : java.lang.Object {
      return DisplayKey.get("Web.NewWriteoffWizardDetailsStepScreen.FullAmount", uiWriteoff.WriteOff.MaxPotentialWriteOffAmount.render())
    }
    
    // 'mode' attribute on PanelRef at NewWriteoffWizardDetailsStepScreen.pcf: line 23, column 40
    function mode_10 () : java.lang.Object {
      return TAccountOwnerDetailsMode
    }
    
    // 'onChange' attribute on PostOnChange at NewWriteoffWizardDetailsStepScreen.pcf: line 38, column 99
    function onChange_11 () : void {
      if (uiWriteoff.UseFullAmount) {uiWriteoff.Amount = uiWriteoff.FullAmount}
    }
    
    // 'validationExpression' attribute on CheckBoxInput (id=UseFullAmount_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 46, column 27
    function validationExpression_22 () : java.lang.Object {
      return uiWriteoff.UseFullAmount ? validateWriteoffAmount() : null
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=Amount_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 56, column 37
    function validationExpression_31 () : java.lang.Object {
      return validateWriteoffAmount()
    }
    
    // 'valueRange' attribute on RangeInput (id=ChargePattern_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 36, column 70
    function valueRange_16 () : java.lang.Object {
      return uiWriteoff.WriteOff.TAccountOwner.ChargePatterns.where( \ pattern -> pattern.ChargeName == "Premium")
    }
    
    // 'value' attribute on RangeInput (id=ChargePattern_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 36, column 70
    function valueRoot_15 () : java.lang.Object {
      return uiWriteoff
    }
    
    // 'value' attribute on RangeInput (id=ChargePattern_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 36, column 70
    function value_13 () : entity.ChargePattern {
      return uiWriteoff.ChargePattern
    }
    
    // 'value' attribute on CheckBoxInput (id=UseFullAmount_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 46, column 27
    function value_24 () : java.lang.Boolean {
      return uiWriteoff.UseFullAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 56, column 37
    function value_32 () : gw.pl.currency.MonetaryAmount {
      return uiWriteoff.Amount
    }
    
    // 'value' attribute on TypeKeyInput (id=Reason_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 65, column 47
    function value_38 () : typekey.WriteoffReason {
      return uiWriteoff.Reason
    }
    
    // 'valueRange' attribute on RangeInput (id=ChargePattern_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 36, column 70
    function verifyValueRangeIsAllowedType_17 ($$arg :  entity.ChargePattern[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ChargePattern_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 36, column 70
    function verifyValueRangeIsAllowedType_17 ($$arg :  gw.api.database.IQueryBeanResult<entity.ChargePattern>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ChargePattern_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 36, column 70
    function verifyValueRangeIsAllowedType_17 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ChargePattern_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 36, column 70
    function verifyValueRange_18 () : void {
      var __valueRangeArg = uiWriteoff.WriteOff.TAccountOwner.ChargePatterns.where( \ pattern -> pattern.ChargeName == "Premium")
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_17(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=ChargePattern_Input) at NewWriteoffWizardDetailsStepScreen.pcf: line 36, column 70
    function visible_12 () : java.lang.Boolean {
      return TAccountOwnerDetailsMode != "producer"
    }
    
    property get maximumAmount () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("maximumAmount", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set maximumAmount ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("maximumAmount", 0, $arg)
    }
    
    property get today () : java.util.Date {
      return getVariableValue("today", 0) as java.util.Date
    }
    
    property set today ($arg :  java.util.Date) {
      setVariableValue("today", 0, $arg)
    }
    
    property get uiWriteoff () : gw.api.web.accounting.UIWriteOffCreation {
      return getRequireValue("uiWriteoff", 0) as gw.api.web.accounting.UIWriteOffCreation
    }
    
    property set uiWriteoff ($arg :  gw.api.web.accounting.UIWriteOffCreation) {
      setRequireValue("uiWriteoff", 0, $arg)
    }
    
    // todo EDF unify this with validation in NegativeWriteoff wizard
    function  validateWriteoffAmount() : String {     
      var amount = uiWriteoff.Amount;
      if (!amount.IsPositive) {
        return DisplayKey.get("Web.NewWriteoffWizard.NonPositiveWriteoffAmount")
      }
      if (amount > uiWriteoff.WriteOff.MaxPotentialWriteOffAmount) {
        return DisplayKey.get("Web.NewWriteoffWizard.OverFullWriteoffAmount", uiWriteoff.WriteOff.MaxPotentialWriteOffAmount.render());
      }
      return null;
    }
    
    
    property get TAccountOwnerDetailsMode() : String {
      if (uiWriteoff.WriteOff.TAccountOwner typeis Account) {
      return "accountdefault";
      } 
      else if (uiWriteoff.WriteOff.TAccountOwner typeis PolicyPeriod) {
        return "policy";
      }
      
      else if (uiWriteoff.WriteOff.TAccountOwner typeis Producer) {
        return "producer";
      }
      else {
        throw new java.lang.UnsupportedOperationException("This t-account owner is unsupported for write-offs.");
      }  
    }
    
    
  }
  
  
}