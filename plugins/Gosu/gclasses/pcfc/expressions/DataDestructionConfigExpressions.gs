package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/tools/DataDestructionConfig.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DataDestructionConfigExpressions {
  @javax.annotation.Generated("config/web/pcf/tools/DataDestructionConfig.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DataDestructionConfigExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=DataDestructionConfig) at DataDestructionConfig.pcf: line 9, column 81
    static function canVisit_29 () : java.lang.Boolean {
      return gw.api.system.PLConfigParameters.PersonalDataDestructionEnabled.getValue()
    }
    
    // Page (id=DataDestructionConfig) at DataDestructionConfig.pcf: line 9, column 81
    static function parent_30 () : pcf.api.Destination {
      return pcf.UnsupportedTools.createDestination()
    }
    
    override property get CurrentLocation () : pcf.DataDestructionConfig {
      return super.CurrentLocation as pcf.DataDestructionConfig
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/tools/DataDestructionConfig.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=propertyName_Cell) at DataDestructionConfig.pcf: line 67, column 59
    function valueRoot_9 () : java.lang.Object {
      return entityProperty
    }
    
    // 'value' attribute on TextCell (id=propertyName_Cell) at DataDestructionConfig.pcf: line 67, column 59
    function value_8 () : java.lang.String {
      return entityProperty.DisplayName
    }
    
    property get entityProperty () : gw.entity.IEntityPropertyInfo {
      return getIteratedValue(3) as gw.entity.IEntityPropertyInfo
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/tools/DataDestructionConfig.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends ScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=EntityType_Cell) at DataDestructionConfig.pcf: line 103, column 35
    function valueRoot_17 () : java.lang.Object {
      return entityType
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at DataDestructionConfig.pcf: line 109, column 108
    function valueRoot_20 () : java.lang.Object {
      return dataDestructionInfoView.getFirstWhitelistedPropertyOf(entityType)
    }
    
    // 'value' attribute on TextCell (id=EntityType_Cell) at DataDestructionConfig.pcf: line 103, column 35
    function value_16 () : java.lang.String {
      return entityType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at DataDestructionConfig.pcf: line 109, column 108
    function value_19 () : java.lang.String {
      return dataDestructionInfoView.getFirstWhitelistedPropertyOf(entityType).DisplayName
    }
    
    // 'value' attribute on RowIterator (id=otherEntityProperties) at DataDestructionConfig.pcf: line 118, column 63
    function value_25 () : gw.entity.IEntityPropertyInfo[] {
      return dataDestructionInfoView.getRemainingWhitelistedProperiesOf(entityType)
    }
    
    property get entityType () : gw.entity.IEntityType {
      return getIteratedValue(2) as gw.entity.IEntityType
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/tools/DataDestructionConfig.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends IteratorEntry3ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=propertyName_Cell) at DataDestructionConfig.pcf: line 125, column 59
    function valueRoot_23 () : java.lang.Object {
      return entityProperty
    }
    
    // 'value' attribute on TextCell (id=propertyName_Cell) at DataDestructionConfig.pcf: line 125, column 59
    function value_22 () : java.lang.String {
      return entityProperty.DisplayName
    }
    
    property get entityProperty () : gw.entity.IEntityPropertyInfo {
      return getIteratedValue(3) as gw.entity.IEntityPropertyInfo
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/tools/DataDestructionConfig.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=EntityType_Cell) at DataDestructionConfig.pcf: line 45, column 35
    function valueRoot_3 () : java.lang.Object {
      return entityType
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at DataDestructionConfig.pcf: line 51, column 103
    function valueRoot_6 () : java.lang.Object {
      return dataDestructionInfoView.getFirstTaggedPropertyOf(entityType)
    }
    
    // 'value' attribute on RowIterator (id=otherEntityProperties) at DataDestructionConfig.pcf: line 60, column 63
    function value_11 () : gw.entity.IEntityPropertyInfo[] {
      return dataDestructionInfoView.getRemainingTaggedProperiesOf(entityType)
    }
    
    // 'value' attribute on TextCell (id=EntityType_Cell) at DataDestructionConfig.pcf: line 45, column 35
    function value_2 () : java.lang.String {
      return entityType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at DataDestructionConfig.pcf: line 51, column 103
    function value_5 () : java.lang.String {
      return dataDestructionInfoView.getFirstTaggedPropertyOf(entityType).DisplayName
    }
    
    property get entityType () : gw.entity.IEntityType {
      return getIteratedValue(2) as gw.entity.IEntityType
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/tools/DataDestructionConfig.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ScreenExpressionsImpl extends DataDestructionConfigExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at DataDestructionConfig.pcf: line 134, column 47
    function def_onEnter_27 (def :  pcf.UnsupportedToolsDisclaimerDV) : void {
      def.onEnter()
    }
    
    // 'def' attribute on PanelRef at DataDestructionConfig.pcf: line 134, column 47
    function def_refreshVariables_28 (def :  pcf.UnsupportedToolsDisclaimerDV) : void {
      def.refreshVariables()
    }
    
    // 'initialValue' attribute on Variable at DataDestructionConfig.pcf: line 14, column 58
    function initialValue_0 () : gw.api.web.admin.DataDestructionInfoView {
      return new gw.api.web.admin.DataDestructionInfoView()
    }
    
    // 'value' attribute on TextCell (id=EntityType_Cell) at DataDestructionConfig.pcf: line 45, column 35
    function sortValue_1 (entityType :  gw.entity.IEntityType) : java.lang.Object {
      return entityType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=EntityType_Cell) at DataDestructionConfig.pcf: line 103, column 35
    function sortValue_15 (entityType :  gw.entity.IEntityType) : java.lang.Object {
      return entityType.DisplayName
    }
    
    // 'value' attribute on RowIterator (id=TaggedFieldsEntityTypeIterator) at DataDestructionConfig.pcf: line 38, column 53
    function value_12 () : gw.entity.IEntityType[] {
      return dataDestructionInfoView.getTaggedEntityTypes()
    }
    
    // 'value' attribute on TextInput (id=TaggingWhitelistDetail_Input) at DataDestructionConfig.pcf: line 84, column 189
    function value_13 () : java.lang.String {
      return DisplayKey.get("Web.InternalTools.DataDestructionConfig.Details") + DisplayKey.get("Web.InternalTools.DataDestructionConfig.ExtensionsDetail")
    }
    
    // 'value' attribute on RowIterator (id=WhitelistEntityTypeIterator) at DataDestructionConfig.pcf: line 96, column 53
    function value_26 () : gw.entity.IEntityType[] {
      return dataDestructionInfoView.getWhitelistedEntityTypes()
    }
    
    property get dataDestructionInfoView () : gw.api.web.admin.DataDestructionInfoView {
      return getVariableValue("dataDestructionInfoView", 1) as gw.api.web.admin.DataDestructionInfoView
    }
    
    property set dataDestructionInfoView ($arg :  gw.api.web.admin.DataDestructionInfoView) {
      setVariableValue("dataDestructionInfoView", 1, $arg)
    }
    
    
  }
  
  
}