package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/RenewPolicyWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class RenewPolicyWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/RenewPolicyWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RenewPolicyWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (prevPolicyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=RenewPolicyWizard) at RenewPolicyWizard.pcf: line 10, column 28
    function afterCancel_8 () : void {
      PolicyOverview.go(prevPolicyPeriod)
    }
    
    // 'afterCancel' attribute on Wizard (id=RenewPolicyWizard) at RenewPolicyWizard.pcf: line 10, column 28
    function afterCancel_dest_9 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(prevPolicyPeriod)
    }
    
    // 'afterFinish' attribute on Wizard (id=RenewPolicyWizard) at RenewPolicyWizard.pcf: line 10, column 28
    function afterFinish_14 () : void {
      PolicySummary.go(renewal.NewPolicyPeriod)
    }
    
    // 'afterFinish' attribute on Wizard (id=RenewPolicyWizard) at RenewPolicyWizard.pcf: line 10, column 28
    function afterFinish_dest_15 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(renewal.NewPolicyPeriod)
    }
    
    // 'beforeCommit' attribute on Wizard (id=RenewPolicyWizard) at RenewPolicyWizard.pcf: line 10, column 28
    function beforeCommit_10 (pickedValue :  java.lang.Object) : void {
      chargesStepView.beforeCommit(); gw.api.web.policy.PolicyPeriodUtil.validatePolicyPeriodCreatedFromUI(renewal); renewal.execute()
    }
    
    // 'canVisit' attribute on Wizard (id=RenewPolicyWizard) at RenewPolicyWizard.pcf: line 10, column 28
    static function canVisit_11 (prevPolicyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.PolicyPeriod.plcyrenew
    }
    
    // 'initialValue' attribute on Variable at RenewPolicyWizard.pcf: line 19, column 23
    function initialValue_0 () : Renewal {
      return createNewRenewal()
    }
    
    // 'initialValue' attribute on Variable at RenewPolicyWizard.pcf: line 23, column 49
    function initialValue_1 () : gw.invoice.InvoicingOverridesView {
      return new gw.invoice.InvoicingOverridesView(renewal.NewPolicyPeriod)
    }
    
    // 'initialValue' attribute on Variable at RenewPolicyWizard.pcf: line 27, column 62
    function initialValue_2 () : gw.web.policy.PolicyWizardChargeStepScreenView {
      return new gw.web.policy.PolicyWizardChargeStepScreenView(renewal.NewPolicyPeriod, renewal)
    }
    
    // 'onExit' attribute on WizardStep (id=BasicsStep) at RenewPolicyWizard.pcf: line 33, column 92
    function onExit_3 () : void {
      invoicingOverridesView.update()
    }
    
    // 'screen' attribute on WizardStep (id=BasicsStep) at RenewPolicyWizard.pcf: line 33, column 92
    function screen_onEnter_4 (def :  pcf.RenewPolicyWizardBasicsStepScreen) : void {
      def.onEnter(prevPolicyPeriod, renewal.NewPolicyPeriod, renewal.ProducerCodes, invoicingOverridesView, renewal)
    }
    
    // 'screen' attribute on WizardStep (id=ChargeStep) at RenewPolicyWizard.pcf: line 39, column 93
    function screen_onEnter_6 (def :  pcf.RenewPolicyWizardChargeStepScreen) : void {
      def.onEnter(renewal, chargesStepView)
    }
    
    // 'screen' attribute on WizardStep (id=BasicsStep) at RenewPolicyWizard.pcf: line 33, column 92
    function screen_refreshVariables_5 (def :  pcf.RenewPolicyWizardBasicsStepScreen) : void {
      def.refreshVariables(prevPolicyPeriod, renewal.NewPolicyPeriod, renewal.ProducerCodes, invoicingOverridesView, renewal)
    }
    
    // 'screen' attribute on WizardStep (id=ChargeStep) at RenewPolicyWizard.pcf: line 39, column 93
    function screen_refreshVariables_7 (def :  pcf.RenewPolicyWizardChargeStepScreen) : void {
      def.refreshVariables(renewal, chargesStepView)
    }
    
    // 'tabBar' attribute on Wizard (id=RenewPolicyWizard) at RenewPolicyWizard.pcf: line 10, column 28
    function tabBar_onEnter_12 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=RenewPolicyWizard) at RenewPolicyWizard.pcf: line 10, column 28
    function tabBar_refreshVariables_13 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.RenewPolicyWizard {
      return super.CurrentLocation as pcf.RenewPolicyWizard
    }
    
    property get chargesStepView () : gw.web.policy.PolicyWizardChargeStepScreenView {
      return getVariableValue("chargesStepView", 0) as gw.web.policy.PolicyWizardChargeStepScreenView
    }
    
    property set chargesStepView ($arg :  gw.web.policy.PolicyWizardChargeStepScreenView) {
      setVariableValue("chargesStepView", 0, $arg)
    }
    
    property get invoicingOverridesView () : gw.invoice.InvoicingOverridesView {
      return getVariableValue("invoicingOverridesView", 0) as gw.invoice.InvoicingOverridesView
    }
    
    property set invoicingOverridesView ($arg :  gw.invoice.InvoicingOverridesView) {
      setVariableValue("invoicingOverridesView", 0, $arg)
    }
    
    property get prevPolicyPeriod () : PolicyPeriod {
      return getVariableValue("prevPolicyPeriod", 0) as PolicyPeriod
    }
    
    property set prevPolicyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("prevPolicyPeriod", 0, $arg)
    }
    
    property get renewal () : Renewal {
      return getVariableValue("renewal", 0) as Renewal
    }
    
    property set renewal ($arg :  Renewal) {
      setVariableValue("renewal", 0, $arg)
    }
    
    
    function createNewRenewal() : Renewal {
      return gw.api.web.policy.NewPolicyUtil.createRenewal(prevPolicyPeriod)
    }
    
    
  }
  
  
}