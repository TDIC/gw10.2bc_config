package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadReviewScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DataUploadReviewScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadReviewScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DataUploadReviewScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on DateInput (id=overrideDate_Input) at DataUploadReviewScreen.pcf: line 174, column 39
    function available_100 () : java.lang.Boolean {
      return processor.DateHandling == processor.dateHandlingText[2]
    }
    
    // 'available' attribute on DateInput (id=startDate_Input) at DataUploadReviewScreen.pcf: line 183, column 39
    function available_109 () : java.lang.Boolean {
      return processor.DateHandling == processor.dateHandlingText[0]
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 212, column 48
    function def_onEnter_137 (def :  pcf.DataUploadAccountLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 220, column 49
    function def_onEnter_140 (def :  pcf.DataUploadProducerLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 228, column 47
    function def_onEnter_143 (def :  pcf.DataUploadPolicyLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 236, column 54
    function def_onEnter_146 (def :  pcf.DataUploadInvoiceStreamLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 244, column 49
    function def_onEnter_149 (def :  pcf.DataUploadIssuanceLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 252, column 51
    function def_onEnter_152 (def :  pcf.DataUploadNewRenewalLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 260, column 47
    function def_onEnter_155 (def :  pcf.DataUploadChangeLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 268, column 46
    function def_onEnter_158 (def :  pcf.DataUploadRenewLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 276, column 47
    function def_onEnter_161 (def :  pcf.DataUploadCancelLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 284, column 50
    function def_onEnter_164 (def :  pcf.DataUploadDirectPayLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 292, column 48
    function def_onEnter_167 (def :  pcf.DataUploadRewriteLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 300, column 50
    function def_onEnter_170 (def :  pcf.DataUploadReinstateLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 308, column 46
    function def_onEnter_173 (def :  pcf.DataUploadAuditLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 316, column 50
    function def_onEnter_176 (def :  pcf.DataUploadAgencyPayLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 324, column 46
    function def_onEnter_179 (def :  pcf.DataUploadBatchLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 332, column 56
    function def_onEnter_182 (def :  pcf.DataUploadPaymentReversalLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 212, column 48
    function def_refreshVariables_138 (def :  pcf.DataUploadAccountLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 220, column 49
    function def_refreshVariables_141 (def :  pcf.DataUploadProducerLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 228, column 47
    function def_refreshVariables_144 (def :  pcf.DataUploadPolicyLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 236, column 54
    function def_refreshVariables_147 (def :  pcf.DataUploadInvoiceStreamLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 244, column 49
    function def_refreshVariables_150 (def :  pcf.DataUploadIssuanceLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 252, column 51
    function def_refreshVariables_153 (def :  pcf.DataUploadNewRenewalLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 260, column 47
    function def_refreshVariables_156 (def :  pcf.DataUploadChangeLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 268, column 46
    function def_refreshVariables_159 (def :  pcf.DataUploadRenewLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 276, column 47
    function def_refreshVariables_162 (def :  pcf.DataUploadCancelLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 284, column 50
    function def_refreshVariables_165 (def :  pcf.DataUploadDirectPayLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 292, column 48
    function def_refreshVariables_168 (def :  pcf.DataUploadRewriteLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 300, column 50
    function def_refreshVariables_171 (def :  pcf.DataUploadReinstateLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 308, column 46
    function def_refreshVariables_174 (def :  pcf.DataUploadAuditLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 316, column 50
    function def_refreshVariables_177 (def :  pcf.DataUploadAgencyPayLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 324, column 46
    function def_refreshVariables_180 (def :  pcf.DataUploadBatchLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at DataUploadReviewScreen.pcf: line 332, column 56
    function def_refreshVariables_183 (def :  pcf.DataUploadPaymentReversalLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'value' attribute on DateInput (id=overrideDate_Input) at DataUploadReviewScreen.pcf: line 174, column 39
    function defaultSetter_103 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.DateHandlingOverride = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=startDate_Input) at DataUploadReviewScreen.pcf: line 183, column 39
    function defaultSetter_112 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ProcessingStartDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=endDate_Input) at DataUploadReviewScreen.pcf: line 190, column 39
    function defaultSetter_120 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ProcessingEndDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on CheckBoxInput (id=processPayment_Input) at DataUploadReviewScreen.pcf: line 197, column 42
    function defaultSetter_127 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.LoadAllPayments = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=processAgencyBillPayment_Input) at DataUploadReviewScreen.pcf: line 204, column 42
    function defaultSetter_133 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.LoadAllAgencyBillPayments = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkIssuance_Input) at DataUploadReviewScreen.pcf: line 52, column 42
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkIssuance = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkNewRenewal_Input) at DataUploadReviewScreen.pcf: line 59, column 42
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkNewRenewal = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkAccounts_Input) at DataUploadReviewScreen.pcf: line 24, column 42
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkAccount = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkChange_Input) at DataUploadReviewScreen.pcf: line 66, column 42
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkChange = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkRenew_Input) at DataUploadReviewScreen.pcf: line 73, column 42
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkRenew = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkCancel_Input) at DataUploadReviewScreen.pcf: line 80, column 42
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkCancel = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkDirectPayment_Input) at DataUploadReviewScreen.pcf: line 87, column 42
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkDirectPayment = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkRewrite_Input) at DataUploadReviewScreen.pcf: line 94, column 42
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkRewrite = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkReinstatement_Input) at DataUploadReviewScreen.pcf: line 101, column 42
    function defaultSetter_63 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkReinstatement = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkAudit_Input) at DataUploadReviewScreen.pcf: line 108, column 42
    function defaultSetter_69 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkAudit = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkAgencyBillPayment_Input) at DataUploadReviewScreen.pcf: line 115, column 42
    function defaultSetter_75 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkAgencyBillPayment = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkPaymentReversal_Input) at DataUploadReviewScreen.pcf: line 122, column 42
    function defaultSetter_81 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkPaymentReversal = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkBatch_Input) at DataUploadReviewScreen.pcf: line 129, column 42
    function defaultSetter_87 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkBatch = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkProducer_Input) at DataUploadReviewScreen.pcf: line 31, column 42
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkProducer = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=dateHandlingTest_Input) at DataUploadReviewScreen.pcf: line 165, column 40
    function defaultSetter_93 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.DateHandling = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'label' attribute on Verbatim at DataUploadReviewScreen.pcf: line 14, column 98
    function label_0 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewLabel")
    }
    
    // 'label' attribute on CheckBoxInput (id=chkAccounts_Input) at DataUploadReviewScreen.pcf: line 24, column 42
    function label_1 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Accounts"), processor.AccountArray.Count)
    }
    
    // 'label' attribute on DateInput (id=overrideDate_Input) at DataUploadReviewScreen.pcf: line 174, column 39
    function label_101 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.OverrideDate")
    }
    
    // 'label' attribute on Label at DataUploadReviewScreen.pcf: line 176, column 118
    function label_108 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.OptionalProcessingDates")
    }
    
    // 'label' attribute on DateInput (id=startDate_Input) at DataUploadReviewScreen.pcf: line 183, column 39
    function label_110 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.ProcessingStartDate")
    }
    
    // 'label' attribute on DateInput (id=endDate_Input) at DataUploadReviewScreen.pcf: line 190, column 39
    function label_118 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.ProcessingEndDate")
    }
    
    // 'label' attribute on CheckBoxInput (id=processPayment_Input) at DataUploadReviewScreen.pcf: line 197, column 42
    function label_125 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.ProcessAllDirectBillPayments")
    }
    
    // 'label' attribute on CheckBoxInput (id=chkPolicy_Input) at DataUploadReviewScreen.pcf: line 38, column 42
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policies"), processor.PolicyArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=processAgencyBillPayment_Input) at DataUploadReviewScreen.pcf: line 204, column 42
    function label_131 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.ProcessAllAgencyBillPayments")
    }
    
    // 'label' attribute on CheckBoxInput (id=chkInvoiceStreams_Input) at DataUploadReviewScreen.pcf: line 45, column 42
    function label_16 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStreams"), processor.InvoiceStreamArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkIssuance_Input) at DataUploadReviewScreen.pcf: line 52, column 42
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Issuances"), processor.IssuanceArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkNewRenewal_Input) at DataUploadReviewScreen.pcf: line 59, column 42
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.NewRenewals"), processor.NewRenewalArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkChange_Input) at DataUploadReviewScreen.pcf: line 66, column 42
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Changes"), processor.PolicyChangeArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkRenew_Input) at DataUploadReviewScreen.pcf: line 73, column 42
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Renewals"), processor.RenewalArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkCancel_Input) at DataUploadReviewScreen.pcf: line 80, column 42
    function label_43 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Cancellations"), processor.CancellationArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkDirectPayment_Input) at DataUploadReviewScreen.pcf: line 87, column 42
    function label_49 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.DirectBillPayments"), processor.DirectPaymentArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkRewrite_Input) at DataUploadReviewScreen.pcf: line 94, column 42
    function label_55 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Rewrites"), processor.RewriteArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkReinstatement_Input) at DataUploadReviewScreen.pcf: line 101, column 42
    function label_61 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Reinstatements"), processor.ReinstatementArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkAudit_Input) at DataUploadReviewScreen.pcf: line 108, column 42
    function label_67 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Audits"), processor.AuditArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkProducer_Input) at DataUploadReviewScreen.pcf: line 31, column 42
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producers"), processor.ProducerArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkAgencyBillPayment_Input) at DataUploadReviewScreen.pcf: line 115, column 42
    function label_73 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.AgencyBillPayments"), processor.AgencyBillPaymentArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkPaymentReversal_Input) at DataUploadReviewScreen.pcf: line 122, column 42
    function label_79 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.PaymentReversals"), processor.PaymentReversalArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkBatch_Input) at DataUploadReviewScreen.pcf: line 129, column 42
    function label_85 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Batches"), processor.BatchArray.Count)
    }
    
    // 'label' attribute on RangeInput (id=dateHandlingTest_Input) at DataUploadReviewScreen.pcf: line 165, column 40
    function label_91 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.DateHandlingMethod")
    }
    
    // 'title' attribute on Card (id=account) at DataUploadReviewScreen.pcf: line 210, column 100
    function title_139 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Accounts")
    }
    
    // 'title' attribute on Card (id=producer) at DataUploadReviewScreen.pcf: line 218, column 101
    function title_142 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Producers")
    }
    
    // 'title' attribute on Card (id=policy) at DataUploadReviewScreen.pcf: line 226, column 100
    function title_145 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Policies")
    }
    
    // 'title' attribute on Card (id=invoiceStream) at DataUploadReviewScreen.pcf: line 234, column 106
    function title_148 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStreams")
    }
    
    // 'title' attribute on Card (id=issuance) at DataUploadReviewScreen.pcf: line 242, column 101
    function title_151 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Issuances")
    }
    
    // 'title' attribute on Card (id=newrenewal) at DataUploadReviewScreen.pcf: line 250, column 103
    function title_154 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.NewRenewals")
    }
    
    // 'title' attribute on Card (id=change) at DataUploadReviewScreen.pcf: line 258, column 99
    function title_157 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Changes")
    }
    
    // 'title' attribute on Card (id=renew) at DataUploadReviewScreen.pcf: line 266, column 100
    function title_160 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Renewals")
    }
    
    // 'title' attribute on Card (id=cancel) at DataUploadReviewScreen.pcf: line 274, column 105
    function title_163 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Cancellations")
    }
    
    // 'title' attribute on Card (id=directPayment) at DataUploadReviewScreen.pcf: line 282, column 110
    function title_166 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.DirectBillPayments")
    }
    
    // 'title' attribute on Card (id=rewrite) at DataUploadReviewScreen.pcf: line 290, column 100
    function title_169 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Rewrites")
    }
    
    // 'title' attribute on Card (id=reinstatements) at DataUploadReviewScreen.pcf: line 298, column 106
    function title_172 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Reinstatements")
    }
    
    // 'title' attribute on Card (id=audit) at DataUploadReviewScreen.pcf: line 306, column 98
    function title_175 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Audits")
    }
    
    // 'title' attribute on Card (id=agencyBillPayment) at DataUploadReviewScreen.pcf: line 314, column 110
    function title_178 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.AgencyBillPayments")
    }
    
    // 'title' attribute on Card (id=batch) at DataUploadReviewScreen.pcf: line 322, column 99
    function title_181 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.Batches")
    }
    
    // 'title' attribute on Card (id=paymentReversal) at DataUploadReviewScreen.pcf: line 330, column 108
    function title_184 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.PaymentReversals")
    }
    
    // 'valueLabel' attribute on CheckBoxInput (id=chkPolicy_Input) at DataUploadReviewScreen.pcf: line 38, column 42
    function valueLabel_14 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.ReviewPolicyIncludeNote")
    }
    
    // 'valueLabel' attribute on CheckBoxInput (id=chkInvoiceStreams_Input) at DataUploadReviewScreen.pcf: line 45, column 42
    function valueLabel_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.ReviewInvoiceStreamIncludeNote")
    }
    
    // 'valueRange' attribute on RangeInput (id=dateHandlingTest_Input) at DataUploadReviewScreen.pcf: line 165, column 40
    function valueRange_95 () : java.lang.Object {
      return processor.dateHandlingText
    }
    
    // 'value' attribute on CheckBoxInput (id=chkAccounts_Input) at DataUploadReviewScreen.pcf: line 24, column 42
    function valueRoot_4 () : java.lang.Object {
      return processor
    }
    
    // 'value' attribute on DateInput (id=overrideDate_Input) at DataUploadReviewScreen.pcf: line 174, column 39
    function value_102 () : java.util.Date {
      return processor.DateHandlingOverride
    }
    
    // 'value' attribute on DateInput (id=startDate_Input) at DataUploadReviewScreen.pcf: line 183, column 39
    function value_111 () : java.util.Date {
      return processor.ProcessingStartDate
    }
    
    // 'value' attribute on DateInput (id=endDate_Input) at DataUploadReviewScreen.pcf: line 190, column 39
    function value_119 () : java.util.Date {
      return processor.ProcessingEndDate
    }
    
    // 'value' attribute on CheckBoxInput (id=processPayment_Input) at DataUploadReviewScreen.pcf: line 197, column 42
    function value_126 () : java.lang.Boolean {
      return processor.LoadAllPayments
    }
    
    // 'value' attribute on CheckBoxInput (id=processAgencyBillPayment_Input) at DataUploadReviewScreen.pcf: line 204, column 42
    function value_132 () : java.lang.Boolean {
      return processor.LoadAllAgencyBillPayments
    }
    
    // 'value' attribute on CheckBoxInput (id=chkAccounts_Input) at DataUploadReviewScreen.pcf: line 24, column 42
    function value_2 () : java.lang.Boolean {
      return processor.ChkAccount
    }
    
    // 'value' attribute on CheckBoxInput (id=chkIssuance_Input) at DataUploadReviewScreen.pcf: line 52, column 42
    function value_20 () : java.lang.Boolean {
      return processor.ChkIssuance
    }
    
    // 'value' attribute on CheckBoxInput (id=chkNewRenewal_Input) at DataUploadReviewScreen.pcf: line 59, column 42
    function value_26 () : java.lang.Boolean {
      return processor.ChkNewRenewal
    }
    
    // 'value' attribute on CheckBoxInput (id=chkChange_Input) at DataUploadReviewScreen.pcf: line 66, column 42
    function value_32 () : java.lang.Boolean {
      return processor.ChkChange
    }
    
    // 'value' attribute on CheckBoxInput (id=chkRenew_Input) at DataUploadReviewScreen.pcf: line 73, column 42
    function value_38 () : java.lang.Boolean {
      return processor.ChkRenew
    }
    
    // 'value' attribute on CheckBoxInput (id=chkCancel_Input) at DataUploadReviewScreen.pcf: line 80, column 42
    function value_44 () : java.lang.Boolean {
      return processor.ChkCancel
    }
    
    // 'value' attribute on CheckBoxInput (id=chkDirectPayment_Input) at DataUploadReviewScreen.pcf: line 87, column 42
    function value_50 () : java.lang.Boolean {
      return processor.ChkDirectPayment
    }
    
    // 'value' attribute on CheckBoxInput (id=chkRewrite_Input) at DataUploadReviewScreen.pcf: line 94, column 42
    function value_56 () : java.lang.Boolean {
      return processor.ChkRewrite
    }
    
    // 'value' attribute on CheckBoxInput (id=chkReinstatement_Input) at DataUploadReviewScreen.pcf: line 101, column 42
    function value_62 () : java.lang.Boolean {
      return processor.ChkReinstatement
    }
    
    // 'value' attribute on CheckBoxInput (id=chkAudit_Input) at DataUploadReviewScreen.pcf: line 108, column 42
    function value_68 () : java.lang.Boolean {
      return processor.ChkAudit
    }
    
    // 'value' attribute on CheckBoxInput (id=chkAgencyBillPayment_Input) at DataUploadReviewScreen.pcf: line 115, column 42
    function value_74 () : java.lang.Boolean {
      return processor.ChkAgencyBillPayment
    }
    
    // 'value' attribute on CheckBoxInput (id=chkProducer_Input) at DataUploadReviewScreen.pcf: line 31, column 42
    function value_8 () : java.lang.Boolean {
      return processor.ChkProducer
    }
    
    // 'value' attribute on CheckBoxInput (id=chkPaymentReversal_Input) at DataUploadReviewScreen.pcf: line 122, column 42
    function value_80 () : java.lang.Boolean {
      return processor.ChkPaymentReversal
    }
    
    // 'value' attribute on CheckBoxInput (id=chkBatch_Input) at DataUploadReviewScreen.pcf: line 129, column 42
    function value_86 () : java.lang.Boolean {
      return processor.ChkBatch
    }
    
    // 'value' attribute on RangeInput (id=dateHandlingTest_Input) at DataUploadReviewScreen.pcf: line 165, column 40
    function value_92 () : java.lang.String {
      return processor.DateHandling
    }
    
    // 'valueRange' attribute on RangeInput (id=dateHandlingTest_Input) at DataUploadReviewScreen.pcf: line 165, column 40
    function verifyValueRangeIsAllowedType_96 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=dateHandlingTest_Input) at DataUploadReviewScreen.pcf: line 165, column 40
    function verifyValueRangeIsAllowedType_96 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=dateHandlingTest_Input) at DataUploadReviewScreen.pcf: line 165, column 40
    function verifyValueRange_97 () : void {
      var __valueRangeArg = processor.dateHandlingText
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_96(__valueRangeArg)
    }
    
    property get processor () : tdic.util.dataloader.processor.BCSampleDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCSampleDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}