package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/Policies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PoliciesExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/Policies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PoliciesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=Policies) at Policies.pcf: line 8, column 60
    static function canVisit_2 () : java.lang.Boolean {
      return perm.System.plcytabview
    }
    
    // 'def' attribute on ScreenRef at Policies.pcf: line 10, column 51
    function def_onEnter_0 (def :  pcf.PolicySearchScreen) : void {
      def.onEnter(true, true, true)
    }
    
    // 'def' attribute on ScreenRef at Policies.pcf: line 10, column 51
    function def_refreshVariables_1 (def :  pcf.PolicySearchScreen) : void {
      def.refreshVariables(true, true, true)
    }
    
    // Page (id=Policies) at Policies.pcf: line 8, column 60
    static function parent_3 () : pcf.api.Destination {
      return pcf.PoliciesGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.Policies {
      return super.CurrentLocation as pcf.Policies
    }
    
    
  }
  
  
}