package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadInvoiceStreamLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DataUploadInvoiceStreamLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadInvoiceStreamLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DataUploadInvoiceStreamLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadInvoiceStreamLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=twiceAMonthFirstDay_Cell) at DataUploadInvoiceStreamLV.pcf: line 50, column 42
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.TwiceAMonthFirstDay")
    }
    
    // 'label' attribute on TextCell (id=twiceAMonthSecondDay_Cell) at DataUploadInvoiceStreamLV.pcf: line 55, column 42
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.TwiceAMonthSecondDay")
    }
    
    // 'label' attribute on TypeKeyCell (id=weeklyDayOfWeek_Cell) at DataUploadInvoiceStreamLV.pcf: line 60, column 42
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.WeeklyDayOfWeek")
    }
    
    // 'label' attribute on DateCell (id=everyOtherWeekAnchorDate_Cell) at DataUploadInvoiceStreamLV.pcf: line 65, column 39
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.EveryOtherWeekAnchorDate")
    }
    
    // 'label' attribute on TypeKeyCell (id=paymentMethod_Cell) at DataUploadInvoiceStreamLV.pcf: line 70, column 46
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.PaymentMethod")
    }
    
    // 'label' attribute on TextCell (id=token_Cell) at DataUploadInvoiceStreamLV.pcf: line 75, column 41
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.Token")
    }
    
    // 'label' attribute on TextCell (id=leadTime_Cell) at DataUploadInvoiceStreamLV.pcf: line 80, column 42
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.LeadTime")
    }
    
    // 'label' attribute on TextCell (id=invoiceStreamName_Cell) at DataUploadInvoiceStreamLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.Name")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at DataUploadInvoiceStreamLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.Description")
    }
    
    // 'label' attribute on TypeKeyCell (id=dueOrInvoiceDay_Cell) at DataUploadInvoiceStreamLV.pcf: line 40, column 57
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.DueOrInvoiceDay")
    }
    
    // 'label' attribute on TextCell (id=monthlyDayOfMonth_Cell) at DataUploadInvoiceStreamLV.pcf: line 45, column 42
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.MonthlyDayOfMonth")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadInvoiceStreamLV.pcf: line 23, column 46
    function sortValue_1 (invoiceStream :  tdic.util.dataloader.data.sampledata.InvoiceStreamData) : java.lang.Object {
      return processor.getLoadStatus(invoiceStream)
    }
    
    // 'value' attribute on TextCell (id=monthlyDayOfMonth_Cell) at DataUploadInvoiceStreamLV.pcf: line 45, column 42
    function sortValue_10 (invoiceStream :  tdic.util.dataloader.data.sampledata.InvoiceStreamData) : java.lang.Object {
      return invoiceStream.MonthlyDayOfMonth
    }
    
    // 'value' attribute on TextCell (id=twiceAMonthFirstDay_Cell) at DataUploadInvoiceStreamLV.pcf: line 50, column 42
    function sortValue_12 (invoiceStream :  tdic.util.dataloader.data.sampledata.InvoiceStreamData) : java.lang.Object {
      return invoiceStream.TwiceAMonthFirstDay
    }
    
    // 'value' attribute on TextCell (id=twiceAMonthSecondDay_Cell) at DataUploadInvoiceStreamLV.pcf: line 55, column 42
    function sortValue_14 (invoiceStream :  tdic.util.dataloader.data.sampledata.InvoiceStreamData) : java.lang.Object {
      return invoiceStream.TwiceAMonthSecondDay
    }
    
    // 'value' attribute on TypeKeyCell (id=weeklyDayOfWeek_Cell) at DataUploadInvoiceStreamLV.pcf: line 60, column 42
    function sortValue_16 (invoiceStream :  tdic.util.dataloader.data.sampledata.InvoiceStreamData) : java.lang.Object {
      return invoiceStream.WeeklyDayOfWeek
    }
    
    // 'value' attribute on DateCell (id=everyOtherWeekAnchorDate_Cell) at DataUploadInvoiceStreamLV.pcf: line 65, column 39
    function sortValue_18 (invoiceStream :  tdic.util.dataloader.data.sampledata.InvoiceStreamData) : java.lang.Object {
      return invoiceStream.EveryOtherWeekAnchorDate
    }
    
    // 'value' attribute on TypeKeyCell (id=paymentMethod_Cell) at DataUploadInvoiceStreamLV.pcf: line 70, column 46
    function sortValue_20 (invoiceStream :  tdic.util.dataloader.data.sampledata.InvoiceStreamData) : java.lang.Object {
      return invoiceStream.PaymentMethod
    }
    
    // 'value' attribute on TextCell (id=token_Cell) at DataUploadInvoiceStreamLV.pcf: line 75, column 41
    function sortValue_22 (invoiceStream :  tdic.util.dataloader.data.sampledata.InvoiceStreamData) : java.lang.Object {
      return invoiceStream.Token
    }
    
    // 'value' attribute on TextCell (id=leadTime_Cell) at DataUploadInvoiceStreamLV.pcf: line 80, column 42
    function sortValue_24 (invoiceStream :  tdic.util.dataloader.data.sampledata.InvoiceStreamData) : java.lang.Object {
      return invoiceStream.LeadTime
    }
    
    // 'value' attribute on TextCell (id=invoiceStreamName_Cell) at DataUploadInvoiceStreamLV.pcf: line 29, column 41
    function sortValue_4 (invoiceStream :  tdic.util.dataloader.data.sampledata.InvoiceStreamData) : java.lang.Object {
      return invoiceStream.InvoiceStreamName
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at DataUploadInvoiceStreamLV.pcf: line 35, column 41
    function sortValue_6 (invoiceStream :  tdic.util.dataloader.data.sampledata.InvoiceStreamData) : java.lang.Object {
      return invoiceStream.Description
    }
    
    // 'value' attribute on TypeKeyCell (id=dueOrInvoiceDay_Cell) at DataUploadInvoiceStreamLV.pcf: line 40, column 57
    function sortValue_8 (invoiceStream :  tdic.util.dataloader.data.sampledata.InvoiceStreamData) : java.lang.Object {
      return invoiceStream.DueOrInvoiceDay
    }
    
    // 'value' attribute on RowIterator (id=invoiceStreamID) at DataUploadInvoiceStreamLV.pcf: line 15, column 101
    function value_86 () : java.util.ArrayList<tdic.util.dataloader.data.sampledata.InvoiceStreamData> {
      return processor.InvoiceStreamArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadInvoiceStreamLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCSampleDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCSampleDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadInvoiceStreamLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DataUploadInvoiceStreamLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at DataUploadInvoiceStreamLV.pcf: line 17, column 105
    function highlighted_85 () : java.lang.Boolean {
      return (invoiceStream.Skipped or invoiceStream.Error) && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadInvoiceStreamLV.pcf: line 23, column 46
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=invoiceStreamName_Cell) at DataUploadInvoiceStreamLV.pcf: line 29, column 41
    function label_30 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.Name")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at DataUploadInvoiceStreamLV.pcf: line 35, column 41
    function label_35 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.Description")
    }
    
    // 'label' attribute on TypeKeyCell (id=dueOrInvoiceDay_Cell) at DataUploadInvoiceStreamLV.pcf: line 40, column 57
    function label_40 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.DueOrInvoiceDay")
    }
    
    // 'label' attribute on TextCell (id=monthlyDayOfMonth_Cell) at DataUploadInvoiceStreamLV.pcf: line 45, column 42
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.MonthlyDayOfMonth")
    }
    
    // 'label' attribute on TextCell (id=twiceAMonthFirstDay_Cell) at DataUploadInvoiceStreamLV.pcf: line 50, column 42
    function label_50 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.TwiceAMonthFirstDay")
    }
    
    // 'label' attribute on TextCell (id=twiceAMonthSecondDay_Cell) at DataUploadInvoiceStreamLV.pcf: line 55, column 42
    function label_55 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.TwiceAMonthSecondDay")
    }
    
    // 'label' attribute on TypeKeyCell (id=weeklyDayOfWeek_Cell) at DataUploadInvoiceStreamLV.pcf: line 60, column 42
    function label_60 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.WeeklyDayOfWeek")
    }
    
    // 'label' attribute on DateCell (id=everyOtherWeekAnchorDate_Cell) at DataUploadInvoiceStreamLV.pcf: line 65, column 39
    function label_65 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.EveryOtherWeekAnchorDate")
    }
    
    // 'label' attribute on TypeKeyCell (id=paymentMethod_Cell) at DataUploadInvoiceStreamLV.pcf: line 70, column 46
    function label_70 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.PaymentMethod")
    }
    
    // 'label' attribute on TextCell (id=token_Cell) at DataUploadInvoiceStreamLV.pcf: line 75, column 41
    function label_75 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.Token")
    }
    
    // 'label' attribute on TextCell (id=leadTime_Cell) at DataUploadInvoiceStreamLV.pcf: line 80, column 42
    function label_80 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.InvoiceStream.LeadTime")
    }
    
    // 'value' attribute on TextCell (id=invoiceStreamName_Cell) at DataUploadInvoiceStreamLV.pcf: line 29, column 41
    function valueRoot_32 () : java.lang.Object {
      return invoiceStream
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadInvoiceStreamLV.pcf: line 23, column 46
    function value_26 () : java.lang.String {
      return processor.getLoadStatus(invoiceStream)
    }
    
    // 'value' attribute on TextCell (id=invoiceStreamName_Cell) at DataUploadInvoiceStreamLV.pcf: line 29, column 41
    function value_31 () : java.lang.String {
      return invoiceStream.InvoiceStreamName
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at DataUploadInvoiceStreamLV.pcf: line 35, column 41
    function value_36 () : java.lang.String {
      return invoiceStream.Description
    }
    
    // 'value' attribute on TypeKeyCell (id=dueOrInvoiceDay_Cell) at DataUploadInvoiceStreamLV.pcf: line 40, column 57
    function value_41 () : typekey.BillDateOrDueDateBilling {
      return invoiceStream.DueOrInvoiceDay
    }
    
    // 'value' attribute on TextCell (id=monthlyDayOfMonth_Cell) at DataUploadInvoiceStreamLV.pcf: line 45, column 42
    function value_46 () : java.lang.Integer {
      return invoiceStream.MonthlyDayOfMonth
    }
    
    // 'value' attribute on TextCell (id=twiceAMonthFirstDay_Cell) at DataUploadInvoiceStreamLV.pcf: line 50, column 42
    function value_51 () : java.lang.Integer {
      return invoiceStream.TwiceAMonthFirstDay
    }
    
    // 'value' attribute on TextCell (id=twiceAMonthSecondDay_Cell) at DataUploadInvoiceStreamLV.pcf: line 55, column 42
    function value_56 () : java.lang.Integer {
      return invoiceStream.TwiceAMonthSecondDay
    }
    
    // 'value' attribute on TypeKeyCell (id=weeklyDayOfWeek_Cell) at DataUploadInvoiceStreamLV.pcf: line 60, column 42
    function value_61 () : typekey.DayOfWeek {
      return invoiceStream.WeeklyDayOfWeek
    }
    
    // 'value' attribute on DateCell (id=everyOtherWeekAnchorDate_Cell) at DataUploadInvoiceStreamLV.pcf: line 65, column 39
    function value_66 () : java.util.Date {
      return invoiceStream.EveryOtherWeekAnchorDate
    }
    
    // 'value' attribute on TypeKeyCell (id=paymentMethod_Cell) at DataUploadInvoiceStreamLV.pcf: line 70, column 46
    function value_71 () : typekey.PaymentMethod {
      return invoiceStream.PaymentMethod
    }
    
    // 'value' attribute on TextCell (id=token_Cell) at DataUploadInvoiceStreamLV.pcf: line 75, column 41
    function value_76 () : java.lang.String {
      return invoiceStream.Token
    }
    
    // 'value' attribute on TextCell (id=leadTime_Cell) at DataUploadInvoiceStreamLV.pcf: line 80, column 42
    function value_81 () : java.lang.Integer {
      return invoiceStream.LeadTime
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadInvoiceStreamLV.pcf: line 23, column 46
    function visible_27 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get invoiceStream () : tdic.util.dataloader.data.sampledata.InvoiceStreamData {
      return getIteratedValue(1) as tdic.util.dataloader.data.sampledata.InvoiceStreamData
    }
    
    
  }
  
  
}