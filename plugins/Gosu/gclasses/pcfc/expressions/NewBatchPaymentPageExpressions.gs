package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/NewBatchPaymentPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewBatchPaymentPageExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/NewBatchPaymentPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewBatchPaymentPageExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (currency :  Currency) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=NewBatchPaymentPage) at NewBatchPaymentPage.pcf: line 14, column 71
    function afterCancel_3 () : void {
      DesktopBatchPaymentsSearch.go()
    }
    
    // 'afterCancel' attribute on Page (id=NewBatchPaymentPage) at NewBatchPaymentPage.pcf: line 14, column 71
    function afterCancel_dest_4 () : pcf.api.Destination {
      return pcf.DesktopBatchPaymentsSearch.createDestination()
    }
    
    // 'afterCommit' attribute on Page (id=NewBatchPaymentPage) at NewBatchPaymentPage.pcf: line 14, column 71
    function afterCommit_5 (TopLocation :  pcf.api.Location) : void {
      BatchPaymentDetailsPage.go(batchPaymentDetailsView.BatchPayment)
    }
    
    // 'canVisit' attribute on Page (id=NewBatchPaymentPage) at NewBatchPaymentPage.pcf: line 14, column 71
    static function canVisit_6 (currency :  Currency) : java.lang.Boolean {
      return perm.System.viewdesktop and perm.BatchPayment.process
    }
    
    // 'def' attribute on ScreenRef at NewBatchPaymentPage.pcf: line 25, column 65
    function def_onEnter_1 (def :  pcf.BatchPaymentDetailsScreen) : void {
      def.onEnter(batchPaymentDetailsView)
    }
    
    // 'def' attribute on ScreenRef at NewBatchPaymentPage.pcf: line 25, column 65
    function def_refreshVariables_2 (def :  pcf.BatchPaymentDetailsScreen) : void {
      def.refreshVariables(batchPaymentDetailsView)
    }
    
    // 'initialValue' attribute on Variable at NewBatchPaymentPage.pcf: line 23, column 60
    function initialValue_0 () : gw.web.payment.batch.BatchPaymentDetailsView {
      return gw.web.payment.batch.BatchPaymentDetailsView.newCreationInstance(CurrentLocation,currency)
    }
    
    // 'parent' attribute on Page (id=NewBatchPaymentPage) at NewBatchPaymentPage.pcf: line 14, column 71
    static function parent_7 (currency :  Currency) : pcf.api.Destination {
      return pcf.DesktopBatchPaymentsSearch.createDestination()
    }
    
    override property get CurrentLocation () : pcf.NewBatchPaymentPage {
      return super.CurrentLocation as pcf.NewBatchPaymentPage
    }
    
    property get batchPaymentDetailsView () : gw.web.payment.batch.BatchPaymentDetailsView {
      return getVariableValue("batchPaymentDetailsView", 0) as gw.web.payment.batch.BatchPaymentDetailsView
    }
    
    property set batchPaymentDetailsView ($arg :  gw.web.payment.batch.BatchPaymentDetailsView) {
      setVariableValue("batchPaymentDetailsView", 0, $arg)
    }
    
    property get currency () : Currency {
      return getVariableValue("currency", 0) as Currency
    }
    
    property set currency ($arg :  Currency) {
      setVariableValue("currency", 0, $arg)
    }
    
    
  }
  
  
}