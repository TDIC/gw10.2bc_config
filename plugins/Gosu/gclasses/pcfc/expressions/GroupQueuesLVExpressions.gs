package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/activityenhancement/groups/GroupQueuesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class GroupQueuesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/activityenhancement/groups/GroupQueuesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class GroupQueuesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at GroupQueuesLV.pcf: line 32, column 41
    function sortValue_0 (AssignableQueue :  AssignableQueue) : java.lang.Object {
      return AssignableQueue.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at GroupQueuesLV.pcf: line 37, column 48
    function sortValue_1 (AssignableQueue :  AssignableQueue) : java.lang.Object {
      return AssignableQueue.Description
    }
    
    // 'value' attribute on BooleanRadioCell (id=SubGroupVisible_Cell) at GroupQueuesLV.pcf: line 44, column 42
    function sortValue_2 (AssignableQueue :  AssignableQueue) : java.lang.Object {
      return AssignableQueue.SubGroupVisible
    }
    
    // 'toAdd' attribute on RowIterator at GroupQueuesLV.pcf: line 21, column 37
    function toAdd_17 (AssignableQueue :  AssignableQueue) : void {
      Group.addToAssignableQueues(AssignableQueue)
    }
    
    // 'toRemove' attribute on RowIterator at GroupQueuesLV.pcf: line 21, column 37
    function toRemove_18 (AssignableQueue :  AssignableQueue) : void {
      Group.removeFromAssignableQueues(AssignableQueue)
    }
    
    // 'value' attribute on RowIterator at GroupQueuesLV.pcf: line 21, column 37
    function value_19 () : AssignableQueue[] {
      return Group.AssignableQueues
    }
    
    property get Group () : Group {
      return getRequireValue("Group", 0) as Group
    }
    
    property set Group ($arg :  Group) {
      setRequireValue("Group", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/activityenhancement/groups/GroupQueuesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends GroupQueuesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at GroupQueuesLV.pcf: line 32, column 41
    function action_3 () : void {
      GroupDetail_QueueDetailPage.go(Group, AssignableQueue)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at GroupQueuesLV.pcf: line 32, column 41
    function action_dest_4 () : pcf.api.Destination {
      return pcf.GroupDetail_QueueDetailPage.createDestination(Group, AssignableQueue)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at GroupQueuesLV.pcf: line 37, column 48
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      AssignableQueue.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on BooleanRadioCell (id=SubGroupVisible_Cell) at GroupQueuesLV.pcf: line 44, column 42
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      AssignableQueue.SubGroupVisible = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at GroupQueuesLV.pcf: line 32, column 41
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      AssignableQueue.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at GroupQueuesLV.pcf: line 32, column 41
    function valueRoot_7 () : java.lang.Object {
      return AssignableQueue
    }
    
    // 'value' attribute on BooleanRadioCell (id=SubGroupVisible_Cell) at GroupQueuesLV.pcf: line 44, column 42
    function value_13 () : java.lang.Boolean {
      return AssignableQueue.SubGroupVisible
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at GroupQueuesLV.pcf: line 32, column 41
    function value_5 () : java.lang.String {
      return AssignableQueue.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at GroupQueuesLV.pcf: line 37, column 48
    function value_9 () : java.lang.String {
      return AssignableQueue.Description
    }
    
    property get AssignableQueue () : AssignableQueue {
      return getIteratedValue(1) as AssignableQueue
    }
    
    
  }
  
  
}