package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerInfoBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerInfoBarExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerInfoBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerInfoBarExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on InfoBarElement (id=Name) at ProducerInfoBar.pcf: line 12, column 79
    function value_0 () : java.lang.Object {
      return DisplayKey.get("Web.ProducerInfoBar.Name", Producer)
    }
    
    // 'value' attribute on InfoBarElement (id=CurrentDate) at ProducerInfoBar.pcf: line 16, column 51
    function value_1 () : java.lang.Object {
      return gw.api.util.DateUtil.currentDate()
    }
    
    property get Producer () : Producer {
      return getRequireValue("Producer", 0) as Producer
    }
    
    property set Producer ($arg :  Producer) {
      setRequireValue("Producer", 0, $arg)
    }
    
    
  }
  
  
}