package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/login/TDIC_SSOLoginPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TDIC_SSOLoginPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/login/TDIC_SSOLoginPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TDIC_SSOLoginPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=RedirectURL) at TDIC_SSOLoginPanelSet.pcf: line 24, column 104
    function action_2 () : void {
      SSOLogin_Tdic_ExitPoint.push(url)
    }
    
    // 'action' attribute on Link (id=RedirectURL) at TDIC_SSOLoginPanelSet.pcf: line 24, column 104
    function action_dest_3 () : pcf.api.Destination {
      return pcf.SSOLogin_Tdic_ExitPoint.createDestination(url)
    }
    
    // 'visible' attribute on Verbatim at TDIC_SSOLoginPanelSet.pcf: line 15, column 98
    function visible_0 () : java.lang.Boolean {
      return com.tdic.util.properties.PropertyUtil.isSsoEnabled() and entryException == null
    }
    
    property get entryException () : java.lang.Exception {
      return getRequireValue("entryException", 0) as java.lang.Exception
    }
    
    property set entryException ($arg :  java.lang.Exception) {
      setRequireValue("entryException", 0, $arg)
    }
    
    property get url () : String {
      return getRequireValue("url", 0) as String
    }
    
    property set url ($arg :  String) {
      setRequireValue("url", 0, $arg)
    }
    
    
  }
  
  
}