package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/NewAccount.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewAccountExpressions {
  @javax.annotation.Generated("config/web/pcf/account/NewAccount.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends NewAccountScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at NewAccount.pcf: line 412, column 54
    function action_195 () : void {
      PaymentPlanDetail.go(paymentPlan)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at NewAccount.pcf: line 412, column 54
    function action_dest_196 () : pcf.api.Destination {
      return pcf.PaymentPlanDetail.createDestination(paymentPlan)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at NewAccount.pcf: line 412, column 54
    function valueRoot_198 () : java.lang.Object {
      return paymentPlan
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at NewAccount.pcf: line 412, column 54
    function value_197 () : java.lang.String {
      return paymentPlan.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at NewAccount.pcf: line 416, column 54
    function value_200 () : java.lang.String {
      return paymentPlan.Description
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at NewAccount.pcf: line 420, column 56
    function value_203 () : java.util.Date {
      return paymentPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at NewAccount.pcf: line 424, column 57
    function value_206 () : java.util.Date {
      return paymentPlan.ExpirationDate
    }
    
    property get paymentPlan () : entity.PaymentPlan {
      return getIteratedValue(2) as entity.PaymentPlan
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/NewAccount.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends NewAccountScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at NewAccount.pcf: line 463, column 56
    function valueRoot_220 () : java.lang.Object {
      return invoiceStream
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at NewAccount.pcf: line 463, column 56
    function value_219 () : java.lang.String {
      return invoiceStream.DisplayName
    }
    
    // 'value' attribute on TypeKeyCell (id=Description_Cell) at NewAccount.pcf: line 468, column 54
    function value_222 () : typekey.Periodicity {
      return invoiceStream.Periodicity
    }
    
    // 'value' attribute on TypeKeyCell (id=EffectiveDate_Cell) at NewAccount.pcf: line 473, column 67
    function value_225 () : typekey.BillDateOrDueDateBilling {
      return invoiceStream.BillDateOrDueDateBilling
    }
    
    property get invoiceStream () : entity.InvoiceStream {
      return getIteratedValue(2) as entity.InvoiceStream
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/NewAccount.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends NewAccountScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=EditLink) at NewAccount.pcf: line 355, column 48
    function action_176 () : void {
      AccountContactDetailPopup.push(accountContact)
    }
    
    // 'action' attribute on Link (id=EditLink) at NewAccount.pcf: line 355, column 48
    function action_dest_177 () : pcf.api.Destination {
      return pcf.AccountContactDetailPopup.createDestination(accountContact)
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at NewAccount.pcf: line 366, column 49
    function valueRoot_181 () : java.lang.Object {
      return accountContact.Contact
    }
    
    // 'value' attribute on BooleanRadioCell (id=IsPrimaryPayer_Cell) at NewAccount.pcf: line 374, column 58
    function valueRoot_186 () : java.lang.Object {
      return accountContact
    }
    
    // 'value' attribute on TextCell (id=ContactName_Cell) at NewAccount.pcf: line 361, column 56
    function value_178 () : entity.AccountContact {
      return accountContact
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at NewAccount.pcf: line 366, column 49
    function value_180 () : entity.Address {
      return accountContact.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=ContactRoles_Cell) at NewAccount.pcf: line 370, column 96
    function value_183 () : java.lang.String {
      return gw.api.web.account.AccountUtil.getRolesForDisplay(accountContact)
    }
    
    // 'value' attribute on BooleanRadioCell (id=IsPrimaryPayer_Cell) at NewAccount.pcf: line 374, column 58
    function value_185 () : java.lang.Boolean {
      return accountContact.PrimaryPayer
    }
    
    property get accountContact () : entity.AccountContact {
      return getIteratedValue(2) as entity.AccountContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/NewAccount.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewAccountExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (currency :  Currency) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=NewAccount) at NewAccount.pcf: line 16, column 62
    function afterCancel_231 () : void {
      Accounts.go()
    }
    
    // 'afterCancel' attribute on Page (id=NewAccount) at NewAccount.pcf: line 16, column 62
    function afterCancel_dest_232 () : pcf.api.Destination {
      return pcf.Accounts.createDestination()
    }
    
    // 'afterCommit' attribute on Page (id=NewAccount) at NewAccount.pcf: line 16, column 62
    function afterCommit_233 (TopLocation :  pcf.api.Location) : void {
      AccountOverview.go(account)
    }
    
    // 'beforeCommit' attribute on Page (id=NewAccount) at NewAccount.pcf: line 16, column 62
    function beforeCommit_234 (pickedValue :  java.lang.Object) : void {
      gw.api.web.account.AccountUtil.validateAccountContacts(account)
    }
    
    // 'beforeValidate' attribute on Page (id=NewAccount) at NewAccount.pcf: line 16, column 62
    function beforeValidate_235 (pickedValue :  java.lang.Object) : void {
      processAccountBeforeValidate()
    }
    
    // 'canVisit' attribute on Page (id=NewAccount) at NewAccount.pcf: line 16, column 62
    static function canVisit_236 (currency :  Currency) : java.lang.Boolean {
      return perm.Account.create
    }
    
    // 'initialValue' attribute on Variable at NewAccount.pcf: line 22, column 23
    function initialValue_0 () : Account {
      return createNewAccount()
    }
    
    // 'initialValue' attribute on Variable at NewAccount.pcf: line 26, column 68
    function initialValue_1 () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return gw.api.database.Query.make(SecurityZone).select()
    }
    
    // 'initialValue' attribute on Variable at NewAccount.pcf: line 34, column 39
    function initialValue_2 () : List<PaymentPlan> {
      return com.google.common.collect.Lists.newArrayList<PaymentPlan>()
    }
    
    // 'initialValue' attribute on Variable at NewAccount.pcf: line 38, column 41
    function initialValue_3 () : List<InvoiceStream> {
      return com.google.common.collect.Lists.newArrayList<InvoiceStream>()
    }
    
    // 'initialValue' attribute on Variable at NewAccount.pcf: line 42, column 49
    function initialValue_4 () : gw.payment.PaymentInstrumentRange {
      return new gw.payment.PaymentInstrumentRange(account.PaymentInstruments)
    }
    
    // 'initialValue' attribute on Variable at NewAccount.pcf: line 49, column 23
    function initialValue_5 () : boolean {
      return account.BillingLevel == BillingLevel.TC_ACCOUNT
    }
    
    // 'initialValue' attribute on Variable at NewAccount.pcf: line 53, column 23
    function initialValue_6 () : boolean {
      return account.BillingLevel != BillingLevel.TC_POLICYDESIGNATEDUNAPPLIED
    }
    
    // 'initialValue' attribute on Variable at NewAccount.pcf: line 57, column 84
    function initialValue_7 () : gw.api.database.IQueryBeanResult<entity.PaymentAllocationPlan> {
      return PaymentAllocationPlan.finder.findAllAvailablePlans<PaymentAllocationPlan>(PaymentAllocationPlan)
    }
    
    // 'parent' attribute on Page (id=NewAccount) at NewAccount.pcf: line 16, column 62
    static function parent_237 (currency :  Currency) : pcf.api.Destination {
      return pcf.AccountsGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.NewAccount {
      return super.CurrentLocation as pcf.NewAccount
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get allSecurityZones () : gw.api.database.IQueryBeanResult<SecurityZone> {
      return getVariableValue("allSecurityZones", 0) as gw.api.database.IQueryBeanResult<SecurityZone>
    }
    
    property set allSecurityZones ($arg :  gw.api.database.IQueryBeanResult<SecurityZone>) {
      setVariableValue("allSecurityZones", 0, $arg)
    }
    
    property get availablePaymentAllocationPlans () : gw.api.database.IQueryBeanResult<entity.PaymentAllocationPlan> {
      return getVariableValue("availablePaymentAllocationPlans", 0) as gw.api.database.IQueryBeanResult<entity.PaymentAllocationPlan>
    }
    
    property set availablePaymentAllocationPlans ($arg :  gw.api.database.IQueryBeanResult<entity.PaymentAllocationPlan>) {
      setVariableValue("availablePaymentAllocationPlans", 0, $arg)
    }
    
    property get currency () : Currency {
      return getVariableValue("currency", 0) as Currency
    }
    
    property set currency ($arg :  Currency) {
      setVariableValue("currency", 0, $arg)
    }
    
    property get inputGroup () : boolean {
      return getVariableValue("inputGroup", 0) as java.lang.Boolean
    }
    
    property set inputGroup ($arg :  boolean) {
      setVariableValue("inputGroup", 0, $arg)
    }
    
    property get invoiceBy () : boolean {
      return getVariableValue("invoiceBy", 0) as java.lang.Boolean
    }
    
    property set invoiceBy ($arg :  boolean) {
      setVariableValue("invoiceBy", 0, $arg)
    }
    
    property get listBillInvoiceStreams () : List<InvoiceStream> {
      return getVariableValue("listBillInvoiceStreams", 0) as List<InvoiceStream>
    }
    
    property set listBillInvoiceStreams ($arg :  List<InvoiceStream>) {
      setVariableValue("listBillInvoiceStreams", 0, $arg)
    }
    
    property get listBillPaymentPlans () : List<PaymentPlan> {
      return getVariableValue("listBillPaymentPlans", 0) as List<PaymentPlan>
    }
    
    property set listBillPaymentPlans ($arg :  List<PaymentPlan>) {
      setVariableValue("listBillPaymentPlans", 0, $arg)
    }
    
    property get paymentInstrumentRange () : gw.payment.PaymentInstrumentRange {
      return getVariableValue("paymentInstrumentRange", 0) as gw.payment.PaymentInstrumentRange
    }
    
    property set paymentInstrumentRange ($arg :  gw.payment.PaymentInstrumentRange) {
      setVariableValue("paymentInstrumentRange", 0, $arg)
    }
    
    property get separateIncomingFundsBy () : boolean {
      return getVariableValue("separateIncomingFundsBy", 0) as java.lang.Boolean
    }
    
    property set separateIncomingFundsBy ($arg :  boolean) {
      setVariableValue("separateIncomingFundsBy", 0, $arg)
    }
    
    function createNewAccount() : Account {
          return new Account(CurrentLocation, currency)
        }
    
        function processAccountBeforeValidate(){
          addPaymentPlansToListBillAccount()
          removeInvoiceStreamsFromNonListBillAccount()
        }
    
        function addPaymentPlansToListBillAccount() {
          if(account.isListBill()){
            account.resyncPaymentPlans(listBillPaymentPlans)
          }
        }
    
        function removeInvoiceStreamsFromNonListBillAccount() {
          if(not account.isListBill()){
            for(var invoiceStream in account.InvoiceStreams){
              account.removeEmptyInvoiceStream(invoiceStream)
            }
          }
        }
    
        function updateBillingLevel() {
          if (invoiceBy) {
            account.BillingLevel = BillingLevel.TC_ACCOUNT
            separateIncomingFundsBy = true
          } else if (separateIncomingFundsBy) {
            account.BillingLevel = BillingLevel.TC_POLICYDEFAULTUNAPPLIED
          } else {
            account.BillingLevel = BillingLevel.TC_POLICYDESIGNATEDUNAPPLIED
          }
        }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/NewAccount.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewAccountScreenExpressionsImpl extends NewAccountExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at NewAccount.pcf: line 268, column 50
    function action_137 () : void {
      NewPaymentInstrumentPopup.push(tdic.bc.config.payment.PaymentInstrumentFilters.newPaymentInstrumentFilterACHOnly,account,false)
    }
    
    // 'action' attribute on MenuItem (id=addNewCompany) at NewAccount.pcf: line 321, column 95
    function action_165 () : void {
      NewAccountContactPopup.push(account, Company)
    }
    
    // 'action' attribute on MenuItem (id=addNewPerson) at NewAccount.pcf: line 325, column 94
    function action_167 () : void {
      NewAccountContactPopup.push(account, Person)
    }
    
    // 'action' attribute on PickerToolbarButton (id=addExistingContact) at NewAccount.pcf: line 336, column 102
    function action_169 () : void {
      ContactSearchPopup.push(false)
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at NewAccount.pcf: line 100, column 40
    function action_25 () : void {
      AccountSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at NewAccount.pcf: line 268, column 50
    function action_dest_138 () : pcf.api.Destination {
      return pcf.NewPaymentInstrumentPopup.createDestination(tdic.bc.config.payment.PaymentInstrumentFilters.newPaymentInstrumentFilterACHOnly,account,false)
    }
    
    // 'action' attribute on MenuItem (id=addNewCompany) at NewAccount.pcf: line 321, column 95
    function action_dest_166 () : pcf.api.Destination {
      return pcf.NewAccountContactPopup.createDestination(account, Company)
    }
    
    // 'action' attribute on MenuItem (id=addNewPerson) at NewAccount.pcf: line 325, column 94
    function action_dest_168 () : pcf.api.Destination {
      return pcf.NewAccountContactPopup.createDestination(account, Person)
    }
    
    // 'action' attribute on PickerToolbarButton (id=addExistingContact) at NewAccount.pcf: line 336, column 102
    function action_dest_170 () : pcf.api.Destination {
      return pcf.ContactSearchPopup.createDestination(false)
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at NewAccount.pcf: line 100, column 40
    function action_dest_26 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'def' attribute on InputSetRef (id=PrimaryContactPhone) at NewAccount.pcf: line 290, column 39
    function def_onEnter_152 (def :  pcf.GlobalPhoneInputSet) : void {
      def.onEnter(new gw.api.phone.BusinessPhoneOwner(new gw.api.phone.ContactPhoneDelegate(account.Insured.Contact, Contact#WorkPhone), DisplayKey.get("Web.NewAccountDV.PrimaryContact.Phone")))
    }
    
    // 'def' attribute on InputSetRef (id=PrimaryContactPhone) at NewAccount.pcf: line 290, column 39
    function def_refreshVariables_153 (def :  pcf.GlobalPhoneInputSet) : void {
      def.refreshVariables(new gw.api.phone.BusinessPhoneOwner(new gw.api.phone.ContactPhoneDelegate(account.Insured.Contact, Contact#WorkPhone), DisplayKey.get("Web.NewAccountDV.PrimaryContact.Phone")))
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at NewAccount.pcf: line 78, column 44
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.AccountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=EveryOtherWeekInvoiceAnchorDate_Input) at NewAccount.pcf: line 209, column 64
    function defaultSetter_101 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.EveryOtherWeekInvoiceAnchorDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=FirstTwicePerMonthInvoiceDayOfMonth_Input) at NewAccount.pcf: line 219, column 46
    function defaultSetter_106 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.FirstTwicePerMonthInvoiceDayOfMonth = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=SecondTwicePerMonthInvoiceDayOfMonth_Input) at NewAccount.pcf: line 227, column 46
    function defaultSetter_112 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.SecondTwicePerMonthInvoiceDayOfMonth = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on InputGroup (id=InvoicesFixedOnInputGroup) at NewAccount.pcf: line 183, column 85
    function defaultSetter_118 (__VALUE_TO_SET :  java.lang.Object) : void {
      inputGroup = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=SendInvoicesBy_Input) at NewAccount.pcf: line 235, column 56
    function defaultSetter_120 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.InvoiceDeliveryType = (__VALUE_TO_SET as typekey.InvoiceDeliveryMethod)
    }
    
    // 'value' attribute on BooleanRadioInput (id=BillingLevelInvoiceByAccount_Input) at NewAccount.pcf: line 244, column 45
    function defaultSetter_126 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceBy = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=BillingLevelSeparateIncomingFundsByAccount_Input) at NewAccount.pcf: line 256, column 45
    function defaultSetter_133 (__VALUE_TO_SET :  java.lang.Object) : void {
      separateIncomingFundsBy = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on RangeInput (id=PaymentInstrument_Input) at NewAccount.pcf: line 268, column 50
    function defaultSetter_141 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.DefaultPaymentInstrument = (__VALUE_TO_SET as entity.PaymentInstrument)
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at NewAccount.pcf: line 84, column 42
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.AccountName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=OrganizationType_Input) at NewAccount.pcf: line 301, column 47
    function defaultSetter_158 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.OrganizationType = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=FEIN_Input) at NewAccount.pcf: line 306, column 35
    function defaultSetter_162 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.FEIN = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=AccountNameKanji_Input) at NewAccount.pcf: line 91, column 88
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.AccountNameKanji = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=ParentAccount_Input) at NewAccount.pcf: line 100, column 40
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.ParentAccount = (__VALUE_TO_SET as entity.Account)
    }
    
    // 'value' attribute on TypeKeyInput (id=AccountTypeInput_Input) at NewAccount.pcf: line 119, column 45
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.AccountType = (__VALUE_TO_SET as typekey.AccountType)
    }
    
    // 'value' attribute on TypeKeyInput (id=CustomerServiceTier_Input) at NewAccount.pcf: line 128, column 54
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.ServiceTier = (__VALUE_TO_SET as typekey.CustomerServiceTier)
    }
    
    // 'value' attribute on RangeInput (id=SecurityZone_Input) at NewAccount.pcf: line 135, column 46
    function defaultSetter_49 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.SecurityZone = (__VALUE_TO_SET as entity.SecurityZone)
    }
    
    // 'value' attribute on TextInput (id=DBA_Input) at NewAccount.pcf: line 140, column 34
    function defaultSetter_56 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.DBA = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Segment_Input) at NewAccount.pcf: line 146, column 49
    function defaultSetter_60 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.Segment = (__VALUE_TO_SET as typekey.AccountSegment)
    }
    
    // 'value' attribute on RangeInput (id=BillingPlan_Input) at NewAccount.pcf: line 154, column 44
    function defaultSetter_64 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.BillingPlan = (__VALUE_TO_SET as entity.BillingPlan)
    }
    
    // 'value' attribute on RangeInput (id=DelinquencyPlan_Input) at NewAccount.pcf: line 164, column 49
    function defaultSetter_71 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.DelinquencyPlan = (__VALUE_TO_SET as entity.DelinquencyPlan)
    }
    
    // 'value' attribute on RangeInput (id=PaymentAllocationPlan_Input) at NewAccount.pcf: line 172, column 55
    function defaultSetter_79 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.PaymentAllocationPlan = (__VALUE_TO_SET as entity.PaymentAllocationPlan)
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=FixDueDate_Input) at NewAccount.pcf: line 179, column 59
    function defaultSetter_87 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.BillDateOrDueDateBilling = (__VALUE_TO_SET as typekey.BillDateOrDueDateBilling)
    }
    
    // 'value' attribute on TextInput (id=InvoiceDayOfMonth_Input) at NewAccount.pcf: line 193, column 46
    function defaultSetter_92 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.InvoiceDayOfMonth = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TypeKeyInput (id=InvoiceDayOfWeek_Input) at NewAccount.pcf: line 201, column 46
    function defaultSetter_97 (__VALUE_TO_SET :  java.lang.Object) : void {
      account.InvoiceDayOfWeek = (__VALUE_TO_SET as typekey.DayOfWeek)
    }
    
    // 'editable' attribute on BooleanRadioInput (id=BillingLevelSeparateIncomingFundsByAccount_Input) at NewAccount.pcf: line 256, column 45
    function editable_130 () : java.lang.Boolean {
      return not invoiceBy
    }
    
    // 'editable' attribute on RangeInput (id=PaymentAllocationPlan_Input) at NewAccount.pcf: line 172, column 55
    function editable_77 () : java.lang.Boolean {
      return availablePaymentAllocationPlans.Count > 1
    }
    
    // 'initialValue' attribute on Variable at NewAccount.pcf: line 63, column 59
    function initialValue_8 () : gw.api.web.account.AccountSearchConverter {
      return new gw.api.web.account.AccountSearchConverter()
    }
    
    // 'inputConversion' attribute on TextInput (id=ParentAccount_Input) at NewAccount.pcf: line 100, column 40
    function inputConversion_28 (VALUE :  java.lang.String) : java.lang.Object {
      return accountSearchConverter.getAccount(VALUE)
    }
    
    // 'label' attribute on TextInput (id=AccountName_Input) at NewAccount.pcf: line 84, column 42
    function label_13 () : java.lang.Object {
      return (gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP) ? DisplayKey.get("Web.NewAccountDV.AccountNamePhonetic") : DisplayKey.get("Web.NewAccountDV.AccountName")
    }
    
    // 'onChange' attribute on PostOnChange at NewAccount.pcf: line 246, column 48
    function onChange_123 () : void {
      updateBillingLevel()
    }
    
    // 'onChange' attribute on PostOnChange at NewAccount.pcf: line 258, column 48
    function onChange_129 () : void {
      updateBillingLevel()
    }
    
    // 'onChange' attribute on PostOnChange at NewAccount.pcf: line 121, column 100
    function onChange_37 () : void {
      if(account.isListBill()){account.BillingLevel = BillingLevel.TC_ACCOUNT}
    }
    
    // 'onPick' attribute on RangeInput (id=PaymentInstrument_Input) at NewAccount.pcf: line 268, column 50
    function onPick_139 (PickedValue :  PaymentInstrument) : void {
      paymentInstrumentRange.addPaymentInstrument(account.DefaultPaymentInstrument)
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=addExistingContact) at NewAccount.pcf: line 336, column 102
    function onPick_171 (PickedValue :  gw.plugin.contact.ContactResult) : void {
      gw.contact.ContactConnection.connectContactToAccount(PickedValue, account)
    }
    
    // 'pickLocation' attribute on RowIterator (id=PaymentPlanRowIterator) at NewAccount.pcf: line 406, column 70
    function pickLocation_209 () : void {
      AccountAddPaymentPlanPopup.push(account, listBillPaymentPlans)
    }
    
    // 'pickLocation' attribute on AddButton (id=addInvoiceStreamButton) at NewAccount.pcf: line 445, column 76
    function pickLocation_214 () : void {
      AccountAddInvoiceStreamPopup.push(account)
    }
    
    // 'value' attribute on TextCell (id=ContactName_Cell) at NewAccount.pcf: line 361, column 56
    function sortValue_172 (accountContact :  entity.AccountContact) : java.lang.Object {
      return accountContact
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at NewAccount.pcf: line 366, column 49
    function sortValue_173 (accountContact :  entity.AccountContact) : java.lang.Object {
      return accountContact.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=ContactRoles_Cell) at NewAccount.pcf: line 370, column 96
    function sortValue_174 (accountContact :  entity.AccountContact) : java.lang.Object {
      return gw.api.web.account.AccountUtil.getRolesForDisplay(accountContact)
    }
    
    // 'value' attribute on BooleanRadioCell (id=IsPrimaryPayer_Cell) at NewAccount.pcf: line 374, column 58
    function sortValue_175 (accountContact :  entity.AccountContact) : java.lang.Object {
      return accountContact.PrimaryPayer
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at NewAccount.pcf: line 412, column 54
    function sortValue_191 (paymentPlan :  entity.PaymentPlan) : java.lang.Object {
      return paymentPlan.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at NewAccount.pcf: line 416, column 54
    function sortValue_192 (paymentPlan :  entity.PaymentPlan) : java.lang.Object {
      return paymentPlan.Description
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at NewAccount.pcf: line 420, column 56
    function sortValue_193 (paymentPlan :  entity.PaymentPlan) : java.lang.Object {
      return paymentPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at NewAccount.pcf: line 424, column 57
    function sortValue_194 (paymentPlan :  entity.PaymentPlan) : java.lang.Object {
      return paymentPlan.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at NewAccount.pcf: line 463, column 56
    function sortValue_216 (invoiceStream :  entity.InvoiceStream) : java.lang.Object {
      return invoiceStream.DisplayName
    }
    
    // 'value' attribute on TypeKeyCell (id=Description_Cell) at NewAccount.pcf: line 468, column 54
    function sortValue_217 (invoiceStream :  entity.InvoiceStream) : java.lang.Object {
      return invoiceStream.Periodicity
    }
    
    // 'value' attribute on TypeKeyCell (id=EffectiveDate_Cell) at NewAccount.pcf: line 473, column 67
    function sortValue_218 (invoiceStream :  entity.InvoiceStream) : java.lang.Object {
      return invoiceStream.BillDateOrDueDateBilling
    }
    
    // 'toAdd' attribute on RowIterator (id=PaymentPlanRowIterator) at NewAccount.pcf: line 406, column 70
    function toAdd_210 (paymentPlan :  entity.PaymentPlan) : void {
      listBillPaymentPlans.add(paymentPlan)
    }
    
    // 'toRemove' attribute on RowIterator (id=accountContactIterator) at NewAccount.pcf: line 346, column 53
    function toRemove_188 (accountContact :  entity.AccountContact) : void {
      account.removeFromContacts(accountContact)
    }
    
    // 'toRemove' attribute on RowIterator (id=PaymentPlanRowIterator) at NewAccount.pcf: line 406, column 70
    function toRemove_211 (paymentPlan :  entity.PaymentPlan) : void {
      listBillPaymentPlans.remove(paymentPlan)
    }
    
    // 'toRemove' attribute on RowIterator (id=InvoiceStreamRowIterator) at NewAccount.pcf: line 458, column 52
    function toRemove_228 (invoiceStream :  entity.InvoiceStream) : void {
      account.removeEmptyInvoiceStream(invoiceStream)
    }
    
    // 'validationExpression' attribute on TextInput (id=FirstTwicePerMonthInvoiceDayOfMonth_Input) at NewAccount.pcf: line 219, column 46
    function validationExpression_104 () : java.lang.Object {
      return account.FirstTwicePerMonthInvoiceDayOfMonth != null and account.FirstTwicePerMonthInvoiceDayOfMonth > 0 and account.FirstTwicePerMonthInvoiceDayOfMonth <= 31 ? null : DisplayKey.get("Java.Account.InvoiceDayOfMonth.ValidationError")
    }
    
    // 'validationExpression' attribute on TextInput (id=SecondTwicePerMonthInvoiceDayOfMonth_Input) at NewAccount.pcf: line 227, column 46
    function validationExpression_110 () : java.lang.Object {
      return account.SecondTwicePerMonthInvoiceDayOfMonth != null and account.SecondTwicePerMonthInvoiceDayOfMonth > 0 and account.SecondTwicePerMonthInvoiceDayOfMonth <= 31 ? null : DisplayKey.get("Java.Account.InvoiceDayOfMonth.ValidationError")
    }
    
    // 'validationExpression' attribute on TextInput (id=ParentAccount_Input) at NewAccount.pcf: line 100, column 40
    function validationExpression_27 () : java.lang.Object {
      return account.hasValidParentAccount() ? null : DisplayKey.get("Web.AccountDetailDV.ParentNotValid")
    }
    
    // 'validationExpression' attribute on TypeKeyInput (id=AccountTypeInput_Input) at NewAccount.pcf: line 119, column 45
    function validationExpression_38 () : java.lang.Object {
      return account.checkForListBillErrors()
    }
    
    // 'validationExpression' attribute on TextInput (id=InvoiceDayOfMonth_Input) at NewAccount.pcf: line 193, column 46
    function validationExpression_90 () : java.lang.Object {
      return account.InvoiceDayOfMonth != null and account.InvoiceDayOfMonth > 0 and account.InvoiceDayOfMonth <= 31 ? null : DisplayKey.get("Java.Account.InvoiceDayOfMonth.ValidationError")
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at NewAccount.pcf: line 268, column 50
    function valueRange_143 () : java.lang.Object {
      return gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, gw.payment.PaymentInstrumentFilters.accountDetailsPaymentInstrumentFilter)
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at NewAccount.pcf: line 135, column 46
    function valueRange_51 () : java.lang.Object {
      return allSecurityZones
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingPlan_Input) at NewAccount.pcf: line 154, column 44
    function valueRange_66 () : java.lang.Object {
      return Plan.finder.findAllAvailablePlans<BillingPlan>(BillingPlan, account.Currency)
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at NewAccount.pcf: line 164, column 49
    function valueRange_73 () : java.lang.Object {
      return account.getApplicableDelinquencyPlans()
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentAllocationPlan_Input) at NewAccount.pcf: line 172, column 55
    function valueRange_81 () : java.lang.Object {
      return availablePaymentAllocationPlans
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at NewAccount.pcf: line 78, column 44
    function valueRoot_11 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on TextInput (id=PrimaryContactEmail_Input) at NewAccount.pcf: line 294, column 60
    function valueRoot_155 () : java.lang.Object {
      return account.Insured.Contact
    }
    
    // 'value' attribute on DateInput (id=EveryOtherWeekInvoiceAnchorDate_Input) at NewAccount.pcf: line 209, column 64
    function value_100 () : java.util.Date {
      return account.EveryOtherWeekInvoiceAnchorDate
    }
    
    // 'value' attribute on TextInput (id=FirstTwicePerMonthInvoiceDayOfMonth_Input) at NewAccount.pcf: line 219, column 46
    function value_105 () : java.lang.Integer {
      return account.FirstTwicePerMonthInvoiceDayOfMonth
    }
    
    // 'value' attribute on TextInput (id=SecondTwicePerMonthInvoiceDayOfMonth_Input) at NewAccount.pcf: line 227, column 46
    function value_111 () : java.lang.Integer {
      return account.SecondTwicePerMonthInvoiceDayOfMonth
    }
    
    // 'value' attribute on TypeKeyInput (id=SendInvoicesBy_Input) at NewAccount.pcf: line 235, column 56
    function value_119 () : typekey.InvoiceDeliveryMethod {
      return account.InvoiceDeliveryType
    }
    
    // 'value' attribute on BooleanRadioInput (id=BillingLevelInvoiceByAccount_Input) at NewAccount.pcf: line 244, column 45
    function value_125 () : java.lang.Boolean {
      return invoiceBy
    }
    
    // 'value' attribute on BooleanRadioInput (id=BillingLevelSeparateIncomingFundsByAccount_Input) at NewAccount.pcf: line 256, column 45
    function value_132 () : java.lang.Boolean {
      return separateIncomingFundsBy
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at NewAccount.pcf: line 84, column 42
    function value_14 () : java.lang.String {
      return account.AccountName
    }
    
    // 'value' attribute on RangeInput (id=PaymentInstrument_Input) at NewAccount.pcf: line 268, column 50
    function value_140 () : entity.PaymentInstrument {
      return account.DefaultPaymentInstrument
    }
    
    // 'value' attribute on TextInput (id=PrimaryContactName_Input) at NewAccount.pcf: line 282, column 48
    function value_147 () : entity.AccountContact {
      return account.Insured
    }
    
    // 'value' attribute on TextInput (id=Address_Input) at NewAccount.pcf: line 286, column 123
    function value_150 () : java.lang.String {
      return new gw.api.address.AddressFormatter().format(account.Insured.Contact.PrimaryAddress, "\n")
    }
    
    // 'value' attribute on TextInput (id=PrimaryContactEmail_Input) at NewAccount.pcf: line 294, column 60
    function value_154 () : java.lang.String {
      return account.Insured.Contact.EmailAddress1
    }
    
    // 'value' attribute on TextInput (id=OrganizationType_Input) at NewAccount.pcf: line 301, column 47
    function value_157 () : java.lang.String {
      return account.OrganizationType
    }
    
    // 'value' attribute on TextInput (id=FEIN_Input) at NewAccount.pcf: line 306, column 35
    function value_161 () : java.lang.String {
      return account.FEIN
    }
    
    // 'value' attribute on RowIterator (id=accountContactIterator) at NewAccount.pcf: line 346, column 53
    function value_189 () : entity.AccountContact[] {
      return account.Contacts
    }
    
    // 'value' attribute on TextInput (id=AccountNameKanji_Input) at NewAccount.pcf: line 91, column 88
    function value_20 () : java.lang.String {
      return account.AccountNameKanji
    }
    
    // 'value' attribute on RowIterator (id=PaymentPlanRowIterator) at NewAccount.pcf: line 406, column 70
    function value_212 () : java.util.List<entity.PaymentPlan> {
      return listBillPaymentPlans
    }
    
    // 'value' attribute on RowIterator (id=InvoiceStreamRowIterator) at NewAccount.pcf: line 458, column 52
    function value_229 () : entity.InvoiceStream[] {
      return account.InvoiceStreams
    }
    
    // 'value' attribute on TextInput (id=ParentAccount_Input) at NewAccount.pcf: line 100, column 40
    function value_29 () : entity.Account {
      return account.ParentAccount
    }
    
    // 'value' attribute on TextInput (id=Currency_Input) at NewAccount.pcf: line 111, column 43
    function value_34 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on TypeKeyInput (id=AccountTypeInput_Input) at NewAccount.pcf: line 119, column 45
    function value_39 () : typekey.AccountType {
      return account.AccountType
    }
    
    // 'value' attribute on TypeKeyInput (id=CustomerServiceTier_Input) at NewAccount.pcf: line 128, column 54
    function value_44 () : typekey.CustomerServiceTier {
      return account.ServiceTier
    }
    
    // 'value' attribute on RangeInput (id=SecurityZone_Input) at NewAccount.pcf: line 135, column 46
    function value_48 () : entity.SecurityZone {
      return account.SecurityZone
    }
    
    // 'value' attribute on TextInput (id=DBA_Input) at NewAccount.pcf: line 140, column 34
    function value_55 () : java.lang.String {
      return account.DBA
    }
    
    // 'value' attribute on TypeKeyInput (id=Segment_Input) at NewAccount.pcf: line 146, column 49
    function value_59 () : typekey.AccountSegment {
      return account.Segment
    }
    
    // 'value' attribute on RangeInput (id=BillingPlan_Input) at NewAccount.pcf: line 154, column 44
    function value_63 () : entity.BillingPlan {
      return account.BillingPlan
    }
    
    // 'value' attribute on RangeInput (id=DelinquencyPlan_Input) at NewAccount.pcf: line 164, column 49
    function value_70 () : entity.DelinquencyPlan {
      return account.DelinquencyPlan
    }
    
    // 'value' attribute on RangeInput (id=PaymentAllocationPlan_Input) at NewAccount.pcf: line 172, column 55
    function value_78 () : entity.PaymentAllocationPlan {
      return account.PaymentAllocationPlan
    }
    
    // 'value' attribute on TypeKeyRadioInput (id=FixDueDate_Input) at NewAccount.pcf: line 179, column 59
    function value_86 () : typekey.BillDateOrDueDateBilling {
      return account.BillDateOrDueDateBilling
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at NewAccount.pcf: line 78, column 44
    function value_9 () : java.lang.String {
      return account.AccountNumber
    }
    
    // 'value' attribute on TextInput (id=InvoiceDayOfMonth_Input) at NewAccount.pcf: line 193, column 46
    function value_91 () : java.lang.Integer {
      return account.InvoiceDayOfMonth
    }
    
    // 'value' attribute on TypeKeyInput (id=InvoiceDayOfWeek_Input) at NewAccount.pcf: line 201, column 46
    function value_96 () : typekey.DayOfWeek {
      return account.InvoiceDayOfWeek
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at NewAccount.pcf: line 268, column 50
    function verifyValueRangeIsAllowedType_144 ($$arg :  entity.PaymentInstrument[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at NewAccount.pcf: line 268, column 50
    function verifyValueRangeIsAllowedType_144 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentInstrument>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at NewAccount.pcf: line 268, column 50
    function verifyValueRangeIsAllowedType_144 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at NewAccount.pcf: line 135, column 46
    function verifyValueRangeIsAllowedType_52 ($$arg :  entity.SecurityZone[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at NewAccount.pcf: line 135, column 46
    function verifyValueRangeIsAllowedType_52 ($$arg :  gw.api.database.IQueryBeanResult<entity.SecurityZone>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at NewAccount.pcf: line 135, column 46
    function verifyValueRangeIsAllowedType_52 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingPlan_Input) at NewAccount.pcf: line 154, column 44
    function verifyValueRangeIsAllowedType_67 ($$arg :  entity.BillingPlan[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingPlan_Input) at NewAccount.pcf: line 154, column 44
    function verifyValueRangeIsAllowedType_67 ($$arg :  gw.api.database.IQueryBeanResult<entity.BillingPlan>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingPlan_Input) at NewAccount.pcf: line 154, column 44
    function verifyValueRangeIsAllowedType_67 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at NewAccount.pcf: line 164, column 49
    function verifyValueRangeIsAllowedType_74 ($$arg :  entity.DelinquencyPlan[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at NewAccount.pcf: line 164, column 49
    function verifyValueRangeIsAllowedType_74 ($$arg :  gw.api.database.IQueryBeanResult<entity.DelinquencyPlan>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at NewAccount.pcf: line 164, column 49
    function verifyValueRangeIsAllowedType_74 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentAllocationPlan_Input) at NewAccount.pcf: line 172, column 55
    function verifyValueRangeIsAllowedType_82 ($$arg :  entity.PaymentAllocationPlan[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentAllocationPlan_Input) at NewAccount.pcf: line 172, column 55
    function verifyValueRangeIsAllowedType_82 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentAllocationPlan>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentAllocationPlan_Input) at NewAccount.pcf: line 172, column 55
    function verifyValueRangeIsAllowedType_82 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at NewAccount.pcf: line 268, column 50
    function verifyValueRange_145 () : void {
      var __valueRangeArg = gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, gw.payment.PaymentInstrumentFilters.accountDetailsPaymentInstrumentFilter)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_144(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=SecurityZone_Input) at NewAccount.pcf: line 135, column 46
    function verifyValueRange_53 () : void {
      var __valueRangeArg = allSecurityZones
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_52(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingPlan_Input) at NewAccount.pcf: line 154, column 44
    function verifyValueRange_68 () : void {
      var __valueRangeArg = Plan.finder.findAllAvailablePlans<BillingPlan>(BillingPlan, account.Currency)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_67(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=DelinquencyPlan_Input) at NewAccount.pcf: line 164, column 49
    function verifyValueRange_75 () : void {
      var __valueRangeArg = account.getApplicableDelinquencyPlans()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_74(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentAllocationPlan_Input) at NewAccount.pcf: line 172, column 55
    function verifyValueRange_83 () : void {
      var __valueRangeArg = availablePaymentAllocationPlans
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_82(__valueRangeArg)
    }
    
    // 'childrenVisible' attribute on InputGroup (id=InvoicesFixedOnInputGroup) at NewAccount.pcf: line 183, column 85
    function visible_116 () : java.lang.Boolean {
      return inputGroup
    }
    
    // 'visible' attribute on BooleanRadioInput (id=BillingLevelInvoiceByAccount_Input) at NewAccount.pcf: line 244, column 45
    function visible_124 () : java.lang.Boolean {
      return !account.isListBill()
    }
    
    // 'visible' attribute on TextInput (id=AccountNameKanji_Input) at NewAccount.pcf: line 91, column 88
    function visible_19 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP
    }
    
    // 'visible' attribute on PanelDivider at NewAccount.pcf: line 382, column 41
    function visible_190 () : java.lang.Boolean {
      return account.isListBill()
    }
    
    // 'visible' attribute on Toolbar (id=NewAccountInvoiceStreamsLV_tb) at NewAccount.pcf: line 440, column 53
    function visible_215 () : java.lang.Boolean {
      return account.BillingPlan != null
    }
    
    property get accountSearchConverter () : gw.api.web.account.AccountSearchConverter {
      return getVariableValue("accountSearchConverter", 1) as gw.api.web.account.AccountSearchConverter
    }
    
    property set accountSearchConverter ($arg :  gw.api.web.account.AccountSearchConverter) {
      setVariableValue("accountSearchConverter", 1, $arg)
    }
    
    
  }
  
  
}