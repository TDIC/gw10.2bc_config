package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.document.SymbolProvider
uses gw.document.SimpleSymbol
uses gw.document.GosuCaseInsensitiveBackwardsCompatibleSymbolProvider
@javax.annotation.Generated("config/web/pcf/activity/ActivityDetailDV.disbappractivity.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ActivityDetailDV_disbappractivityExpressions {
  @javax.annotation.Generated("config/web/pcf/activity/ActivityDetailDV.disbappractivity.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ActivityDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextInput (id=ActivityDetailDV_Disbursement_Input) at ActivityDetailDV.disbappractivity.pcf: line 26, column 42
    function action_1 () : void {
      gotoDisbursementsScreenBasedOnDisbursementSubtype(activity as DisbApprActivity)
    }
    
    // 'action' attribute on MenuItem (id=ActivityDetailDV_AssignActivity_PickerButton) at AssigneeWidget.xml: line 7, column 25
    function action_31 () : void {
      AssigneePickerPopup.push(new gw.api.assignment.AssigneePicker(entity.Activity.Type.isAssignableFrom(typeof (activity))))
    }
    
    // 'pickLocation' attribute on DocumentTemplateInput (id=DocumentTemplate_Input) at ActivityDetailDV.disbappractivity.pcf: line 83, column 39
    function action_52 () : void {
      DocumentTemplateSearchPopup.push( symbolProvider)
    }
    
    // 'pickLocation' attribute on PickerInput (id=EmailTemplate_Input) at ActivityDetailDV.disbappractivity.pcf: line 92, column 39
    function action_58 () : void {
      PickEmailTemplatePopup.push(new gw.api.email.EmailTemplateSearchCriteria({"activity"}))
    }
    
    // 'action' attribute on MenuItem (id=ActivityDetailDV_AssignActivity_PickerButton) at AssigneeWidget.xml: line 7, column 25
    function action_dest_32 () : pcf.api.Destination {
      return pcf.AssigneePickerPopup.createDestination(new gw.api.assignment.AssigneePicker(entity.Activity.Type.isAssignableFrom(typeof (activity))))
    }
    
    // 'pickLocation' attribute on DocumentTemplateInput (id=DocumentTemplate_Input) at ActivityDetailDV.disbappractivity.pcf: line 83, column 39
    function action_dest_53 () : pcf.api.Destination {
      return pcf.DocumentTemplateSearchPopup.createDestination( symbolProvider)
    }
    
    // 'pickLocation' attribute on PickerInput (id=EmailTemplate_Input) at ActivityDetailDV.disbappractivity.pcf: line 92, column 39
    function action_dest_59 () : pcf.api.Destination {
      return pcf.PickEmailTemplatePopup.createDestination(new gw.api.email.EmailTemplateSearchCriteria({"activity"}))
    }
    
    // 'conversionExpression' attribute on DocumentTemplateInput (id=DocumentTemplate_Input) at ActivityDetailDV.disbappractivity.pcf: line 83, column 39
    function conversionExpression_54 (PickedValue :  gw.plugin.document.IDocumentTemplateDescriptor) : java.lang.String {
      return PickedValue.TemplateId
    }
    
    // 'conversionExpression' attribute on PickerInput (id=EmailTemplate_Input) at ActivityDetailDV.disbappractivity.pcf: line 92, column 39
    function conversionExpression_60 (PickedValue :  gw.plugin.email.IEmailTemplateDescriptor) : java.lang.String {
      return PickedValue.Filename
    }
    
    // 'def' attribute on InputSetRef at ActivityDetailDV.disbappractivity.pcf: line 58, column 47
    function def_onEnter_29 (def :  pcf.EdgeEntitiesInputSet) : void {
      def.onEnter(activity)
    }
    
    // 'def' attribute on InputSetRef at ActivityDetailDV.disbappractivity.pcf: line 58, column 47
    function def_refreshVariables_30 (def :  pcf.EdgeEntitiesInputSet) : void {
      def.refreshVariables(activity)
    }
    
    // 'value' attribute on TextAreaInput (id=ActivityDetailDV_Description_Input) at ActivityDetailDV.disbappractivity.pcf: line 38, column 39
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      activity.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=ActivityDetailDV_Priority_Input) at ActivityDetailDV.disbappractivity.pcf: line 44, column 39
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      activity.Priority = (__VALUE_TO_SET as typekey.Priority)
    }
    
    // 'value' attribute on DateInput (id=ActivityDetailDV_TargetDate_Input) at ActivityDetailDV.disbappractivity.pcf: line 50, column 38
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      activity.TargetDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=ActivityDetailDV_EscalationDate_Input) at ActivityDetailDV.disbappractivity.pcf: line 56, column 42
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      activity.EscalationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on AssigneeInput (id=ActivityDetailDV_AssignActivity_Input) at AssigneeWidget.xml: line 7, column 25
    function defaultSetter_36 (__VALUE_TO_SET :  java.lang.Object) : void {
      AssigneeHolder[0] = (__VALUE_TO_SET as gw.api.assignment.Assignee)
    }
    
    // 'value' attribute on TextInput (id=ActivityDetailDV_Subject_Input) at ActivityDetailDV.disbappractivity.pcf: line 32, column 35
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      activity.Subject = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on AssigneeInput (id=ActivityDetailDV_AssignActivity_Input) at AssigneeWidget.xml: line 7, column 25
    function editable_33 () : java.lang.Boolean {
      return activity.canAssign()
    }
    
    // 'initialValue' attribute on Variable at ActivityDetailDV.disbappractivity.pcf: line 17, column 42
    function initialValue_0 () : gw.document.SymbolProvider {
      return createSymbolProvider()
    }
    
    // 'outputConversion' attribute on PickerInput (id=EmailTemplate_Input) at ActivityDetailDV.disbappractivity.pcf: line 92, column 39
    function outputConversion_61 (VALUE :  java.lang.String) : java.lang.String {
      return getDisplayName(VALUE)
    }
    
    // 'validationExpression' attribute on DateInput (id=ActivityDetailDV_TargetDate_Input) at ActivityDetailDV.disbappractivity.pcf: line 50, column 38
    function validationExpression_17 () : java.lang.Object {
      return gw.activity.ActivityMethods.validateTargetDate(activity)
    }
    
    // 'validationExpression' attribute on DateInput (id=ActivityDetailDV_EscalationDate_Input) at ActivityDetailDV.disbappractivity.pcf: line 56, column 42
    function validationExpression_23 () : java.lang.Object {
      return gw.activity.ActivityMethods.validateEscalationDate(activity)
    }
    
    // 'valueRange' attribute on AssigneeInput (id=ActivityDetailDV_AssignActivity_Input) at AssigneeWidget.xml: line 7, column 25
    function valueRange_37 () : java.lang.Object {
      return activity.SuggestedAssignees
    }
    
    // 'valueRange' attribute on RangeInput (id=ActivityDetailDV_Status_Input) at ActivityDetailDV.disbappractivity.pcf: line 75, column 45
    function valueRange_48 () : java.lang.Object {
      return activity.BCStatusRange
    }
    
    // 'value' attribute on TextInput (id=ActivityDetailDV_Disbursement_Input) at ActivityDetailDV.disbappractivity.pcf: line 26, column 42
    function valueRoot_3 () : java.lang.Object {
      return (activity as DisbApprActivity)
    }
    
    // 'value' attribute on TextInput (id=ActivityDetailDV_Subject_Input) at ActivityDetailDV.disbappractivity.pcf: line 32, column 35
    function valueRoot_7 () : java.lang.Object {
      return activity
    }
    
    // 'value' attribute on TypeKeyInput (id=ActivityDetailDV_Priority_Input) at ActivityDetailDV.disbappractivity.pcf: line 44, column 39
    function value_13 () : typekey.Priority {
      return activity.Priority
    }
    
    // 'value' attribute on DateInput (id=ActivityDetailDV_TargetDate_Input) at ActivityDetailDV.disbappractivity.pcf: line 50, column 38
    function value_18 () : java.util.Date {
      return activity.TargetDate
    }
    
    // 'value' attribute on TextInput (id=ActivityDetailDV_Disbursement_Input) at ActivityDetailDV.disbappractivity.pcf: line 26, column 42
    function value_2 () : entity.Disbursement {
      return (activity as DisbApprActivity).Disbursement
    }
    
    // 'value' attribute on DateInput (id=ActivityDetailDV_EscalationDate_Input) at ActivityDetailDV.disbappractivity.pcf: line 56, column 42
    function value_24 () : java.util.Date {
      return activity.EscalationDate
    }
    
    // 'value' attribute on AssigneeInput (id=ActivityDetailDV_AssignActivity_Input) at AssigneeWidget.xml: line 7, column 25
    function value_35 () : gw.api.assignment.Assignee {
      return AssigneeHolder[0]
    }
    
    // 'value' attribute on RangeInput (id=ActivityDetailDV_Status_Input) at ActivityDetailDV.disbappractivity.pcf: line 75, column 45
    function value_46 () : typekey.ActivityStatus {
      return activity.Status
    }
    
    // 'value' attribute on TextInput (id=ActivityDetailDV_Subject_Input) at ActivityDetailDV.disbappractivity.pcf: line 32, column 35
    function value_5 () : java.lang.String {
      return activity.Subject
    }
    
    // 'value' attribute on DocumentTemplateInput (id=DocumentTemplate_Input) at ActivityDetailDV.disbappractivity.pcf: line 83, column 39
    function value_55 () : java.lang.String {
      return activity.DocumentTemplate
    }
    
    // 'value' attribute on PickerInput (id=EmailTemplate_Input) at ActivityDetailDV.disbappractivity.pcf: line 92, column 39
    function value_62 () : java.lang.String {
      return activity.EmailTemplate
    }
    
    // 'value' attribute on TextAreaInput (id=ActivityDetailDV_Description_Input) at ActivityDetailDV.disbappractivity.pcf: line 38, column 39
    function value_9 () : java.lang.String {
      return activity.Description
    }
    
    // 'valueRange' attribute on AssigneeInput (id=ActivityDetailDV_AssignActivity_Input) at AssigneeWidget.xml: line 7, column 25
    function verifyValueRangeIsAllowedType_38 ($$arg :  gw.api.assignment.Assignee[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on AssigneeInput (id=ActivityDetailDV_AssignActivity_Input) at AssigneeWidget.xml: line 7, column 25
    function verifyValueRangeIsAllowedType_38 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ActivityDetailDV_Status_Input) at ActivityDetailDV.disbappractivity.pcf: line 75, column 45
    function verifyValueRangeIsAllowedType_49 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ActivityDetailDV_Status_Input) at ActivityDetailDV.disbappractivity.pcf: line 75, column 45
    function verifyValueRangeIsAllowedType_49 ($$arg :  typekey.ActivityStatus[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on AssigneeInput (id=ActivityDetailDV_AssignActivity_Input) at AssigneeWidget.xml: line 7, column 25
    function verifyValueRange_39 () : void {
      var __valueRangeArg = activity.SuggestedAssignees
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_38(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=ActivityDetailDV_Status_Input) at ActivityDetailDV.disbappractivity.pcf: line 75, column 45
    function verifyValueRange_50 () : void {
      var __valueRangeArg = activity.BCStatusRange
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_49(__valueRangeArg)
    }
    
    // 'visible' attribute on AssigneeInput (id=ActivityDetailDV_AssignActivity_Input) at AssigneeWidget.xml: line 7, column 25
    function visible_34 () : java.lang.Boolean {
      return !gw.api.web.activity.ActivityUtil.isShared(activity)
    }
    
    property get AssigneeHolder () : gw.api.assignment.Assignee[] {
      return getRequireValue("AssigneeHolder", 0) as gw.api.assignment.Assignee[]
    }
    
    property set AssigneeHolder ($arg :  gw.api.assignment.Assignee[]) {
      setRequireValue("AssigneeHolder", 0, $arg)
    }
    
    property get activity () : Activity {
      return getRequireValue("activity", 0) as Activity
    }
    
    property set activity ($arg :  Activity) {
      setRequireValue("activity", 0, $arg)
    }
    
    property get symbolProvider () : gw.document.SymbolProvider {
      return getVariableValue("symbolProvider", 0) as gw.document.SymbolProvider
    }
    
    property set symbolProvider ($arg :  gw.document.SymbolProvider) {
      setVariableValue("symbolProvider", 0, $arg)
    }
    
    
    
    function createSymbolProvider(): SymbolProvider {
      return new GosuCaseInsensitiveBackwardsCompatibleSymbolProvider({
        "Activity"->activity,
        "AssigneeHolder"->AssigneeHolder
      })
    }
    
    function getDisplayName(templateFilename : String) : String {
      if (templateFilename == null) {
        return null
      }
      var ets = gw.plugin.Plugins.get(gw.plugin.email.IEmailTemplateSource)
      return ets.getEmailTemplate(templateFilename).getName();
    }
    function gotoDisbursementsScreenBasedOnDisbursementSubtype(disbursementActivity : DisbApprActivity) {
      if (disbursementActivity.Disbursement typeis AccountDisbursement) {
        pcf.AccountDetailDisbursements.goInMain(disbursementActivity.Disbursement.Account)
      } else if (disbursementActivity.Disbursement typeis CollateralDisbursement) {
        pcf.AccountDetailDisbursements.goInMain(disbursementActivity.Disbursement.Collateral.Account)
      } else if (disbursementActivity.Disbursement typeis SuspenseDisbursement) {
        pcf.DesktopDisbursements.goInMain()
      } else if (disbursementActivity.Disbursement typeis AgencyDisbursement) {
        pcf.ProducerDisbursements.goInMain(disbursementActivity.Disbursement.Producer)
      }
    }
          
        
    
    
  }
  
  
}