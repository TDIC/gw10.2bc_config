package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/businesssettings/AgencyBillPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillPlansExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/AgencyBillPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillPlansExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=AgencyBillPlans) at AgencyBillPlans.pcf: line 8, column 67
    static function canVisit_31 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.agencybillplanview
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at AgencyBillPlans.pcf: line 28, column 80
    function filters_1 () : gw.api.filters.IFilter[] {
      return new gw.api.web.plan.PlanListFilterSet().FilterOptions
    }
    
    // 'initialValue' attribute on Variable at AgencyBillPlans.pcf: line 12, column 42
    function initialValue_0 () : gw.api.web.plan.PlanHelper {
      return new gw.api.web.plan.PlanHelper()
    }
    
    // Page (id=AgencyBillPlans) at AgencyBillPlans.pcf: line 8, column 67
    static function parent_32 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at AgencyBillPlans.pcf: line 37, column 46
    function sortValue_2 (agencyBillPlan :  entity.AgencyBillPlan) : java.lang.Object {
      return agencyBillPlan.PlanOrder
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AgencyBillPlans.pcf: line 43, column 44
    function sortValue_3 (agencyBillPlan :  entity.AgencyBillPlan) : java.lang.Object {
      return agencyBillPlan.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AgencyBillPlans.pcf: line 48, column 51
    function sortValue_4 (agencyBillPlan :  entity.AgencyBillPlan) : java.lang.Object {
      return agencyBillPlan.Description
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at AgencyBillPlans.pcf: line 52, column 53
    function sortValue_5 (agencyBillPlan :  entity.AgencyBillPlan) : java.lang.Object {
      return agencyBillPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at AgencyBillPlans.pcf: line 56, column 54
    function sortValue_6 (agencyBillPlan :  entity.AgencyBillPlan) : java.lang.Object {
      return agencyBillPlan.ExpirationDate
    }
    
    // 'value' attribute on RowIterator at AgencyBillPlans.pcf: line 23, column 85
    function value_30 () : gw.api.database.IQueryBeanResult<entity.AgencyBillPlan> {
      return gw.api.database.Query.make(AgencyBillPlan).select()
    }
    
    override property get CurrentLocation () : pcf.AgencyBillPlans {
      return super.CurrentLocation as pcf.AgencyBillPlans
    }
    
    property get PlanHelper () : gw.api.web.plan.PlanHelper {
      return getVariableValue("PlanHelper", 0) as gw.api.web.plan.PlanHelper
    }
    
    property set PlanHelper ($arg :  gw.api.web.plan.PlanHelper) {
      setVariableValue("PlanHelper", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/AgencyBillPlans.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AgencyBillPlansExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at AgencyBillPlans.pcf: line 43, column 44
    function action_10 () : void {
      AgencyBillPlanDetail.go(agencyBillPlan)
    }
    
    // 'action' attribute on Link (id=MoveUp) at AgencyBillPlans.pcf: line 67, column 60
    function action_26 () : void {
      PlanHelper.moveUp(agencyBillPlan);
    }
    
    // 'action' attribute on Link (id=MoveDown) at AgencyBillPlans.pcf: line 75, column 66
    function action_29 () : void {
      PlanHelper.moveDown(agencyBillPlan);
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at AgencyBillPlans.pcf: line 43, column 44
    function action_dest_11 () : pcf.api.Destination {
      return pcf.AgencyBillPlanDetail.createDestination(agencyBillPlan)
    }
    
    // 'available' attribute on Link (id=MoveUp) at AgencyBillPlans.pcf: line 67, column 60
    function available_24 () : java.lang.Boolean {
      return perm.System.agencybillplanedit
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at AgencyBillPlans.pcf: line 37, column 46
    function valueRoot_8 () : java.lang.Object {
      return agencyBillPlan
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AgencyBillPlans.pcf: line 43, column 44
    function value_12 () : java.lang.String {
      return agencyBillPlan.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AgencyBillPlans.pcf: line 48, column 51
    function value_15 () : java.lang.String {
      return agencyBillPlan.Description
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at AgencyBillPlans.pcf: line 52, column 53
    function value_18 () : java.util.Date {
      return agencyBillPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at AgencyBillPlans.pcf: line 56, column 54
    function value_21 () : java.util.Date {
      return agencyBillPlan.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at AgencyBillPlans.pcf: line 37, column 46
    function value_7 () : java.lang.Integer {
      return agencyBillPlan.PlanOrder
    }
    
    // 'visible' attribute on Link (id=MoveUp) at AgencyBillPlans.pcf: line 67, column 60
    function visible_25 () : java.lang.Boolean {
      return agencyBillPlan.PlanOrder > 1
    }
    
    // 'visible' attribute on Link (id=MoveDown) at AgencyBillPlans.pcf: line 75, column 66
    function visible_28 () : java.lang.Boolean {
      return !agencyBillPlan.hasHighestPlanOrder()
    }
    
    property get agencyBillPlan () : entity.AgencyBillPlan {
      return getIteratedValue(1) as entity.AgencyBillPlan
    }
    
    
  }
  
  
}