package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentStandardPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewCommissionPaymentStandardPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentStandardPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewCommissionPaymentStandardPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=NewCommissionPaymentStandardPopup) at NewCommissionPaymentStandardPopup.pcf: line 10, column 80
    function beforeCommit_10 (pickedValue :  java.lang.Object) : void {
      standardPayment.makePayment()
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentStandardPopup.pcf: line 36, column 56
    function def_onEnter_8 (def :  pcf.NewCommissionPaymentDV) : void {
      def.onEnter(standardPayment)
    }
    
    // 'def' attribute on PanelRef at NewCommissionPaymentStandardPopup.pcf: line 36, column 56
    function def_refreshVariables_9 (def :  pcf.NewCommissionPaymentDV) : void {
      def.refreshVariables(standardPayment)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=producercode_Input) at NewCommissionPaymentStandardPopup.pcf: line 29, column 43
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      standardPayment.StandardProducerCode = (__VALUE_TO_SET as entity.ProducerCode)
    }
    
    // 'initialValue' attribute on Variable at NewCommissionPaymentStandardPopup.pcf: line 19, column 42
    function initialValue_0 () : entity.StandardCmsnPayment {
      return newStandardPayment()
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=producercode_Input) at NewCommissionPaymentStandardPopup.pcf: line 29, column 43
    function valueRange_4 () : java.lang.Object {
      return producer.ProducerCodes
    }
    
    // 'value' attribute on ToolbarRangeInput (id=producercode_Input) at NewCommissionPaymentStandardPopup.pcf: line 29, column 43
    function valueRoot_3 () : java.lang.Object {
      return standardPayment
    }
    
    // 'value' attribute on ToolbarRangeInput (id=producercode_Input) at NewCommissionPaymentStandardPopup.pcf: line 29, column 43
    function value_1 () : entity.ProducerCode {
      return standardPayment.StandardProducerCode
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=producercode_Input) at NewCommissionPaymentStandardPopup.pcf: line 29, column 43
    function verifyValueRangeIsAllowedType_5 ($$arg :  entity.ProducerCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=producercode_Input) at NewCommissionPaymentStandardPopup.pcf: line 29, column 43
    function verifyValueRangeIsAllowedType_5 ($$arg :  gw.api.database.IQueryBeanResult<entity.ProducerCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=producercode_Input) at NewCommissionPaymentStandardPopup.pcf: line 29, column 43
    function verifyValueRangeIsAllowedType_5 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=producercode_Input) at NewCommissionPaymentStandardPopup.pcf: line 29, column 43
    function verifyValueRange_6 () : void {
      var __valueRangeArg = producer.ProducerCodes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_5(__valueRangeArg)
    }
    
    override property get CurrentLocation () : pcf.NewCommissionPaymentStandardPopup {
      return super.CurrentLocation as pcf.NewCommissionPaymentStandardPopup
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    property get standardPayment () : entity.StandardCmsnPayment {
      return getVariableValue("standardPayment", 0) as entity.StandardCmsnPayment
    }
    
    property set standardPayment ($arg :  entity.StandardCmsnPayment) {
      setVariableValue("standardPayment", 0, $arg)
    }
    
    function newStandardPayment() : StandardCmsnPayment {
            var scp = new StandardCmsnPayment(producer.Currency)
            scp.StandardProducerCode = producer.ActiveProducerCodes[0];
            scp.PaymentType = TC_ALLCURRENT;
            return scp;
          }
    
    
  }
  
  
}