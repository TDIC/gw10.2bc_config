package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/NewDocumentMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewDocumentMenuItemSetExpressions {
  @javax.annotation.Generated("config/web/pcf/document/NewDocumentMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewDocumentMenuItemSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=NewDocumentFromTemplate) at NewDocumentMenuItemSet.pcf: line 18, column 85
    function action_2 () : void {
      NewDocumentFromTemplateWorksheet.goInWorkspace(documentContainer)
    }
    
    // 'action' attribute on MenuItem (id=NewDocumentByUpload) at NewDocumentMenuItemSet.pcf: line 23, column 83
    function action_5 () : void {
      NewDocumentLinkedWorksheet.goInWorkspace(documentContainer)
    }
    
    // 'action' attribute on MenuItem (id=NewDocumentFromTemplate) at NewDocumentMenuItemSet.pcf: line 18, column 85
    function action_dest_3 () : pcf.api.Destination {
      return pcf.NewDocumentFromTemplateWorksheet.createDestination(documentContainer)
    }
    
    // 'action' attribute on MenuItem (id=NewDocumentByUpload) at NewDocumentMenuItemSet.pcf: line 23, column 83
    function action_dest_6 () : pcf.api.Destination {
      return pcf.NewDocumentLinkedWorksheet.createDestination(documentContainer)
    }
    
    // 'available' attribute on MenuItem (id=NewDocumentFromTemplate) at NewDocumentMenuItemSet.pcf: line 18, column 85
    function available_1 () : java.lang.Boolean {
      return documentsActionsHelper.DocumentTemplateActionsAvailable
    }
    
    // 'available' attribute on MenuItem (id=NewDocumentByUpload) at NewDocumentMenuItemSet.pcf: line 23, column 83
    function available_4 () : java.lang.Boolean {
      return documentsActionsHelper.DocumentLinkActionsAvailable
    }
    
    // 'initialValue' attribute on Variable at NewDocumentMenuItemSet.pcf: line 13, column 52
    function initialValue_0 () : gw.document.DocumentsActionsUIHelper {
      return new gw.document.DocumentsActionsUIHelper()
    }
    
    property get documentContainer () : DocumentContainer {
      return getRequireValue("documentContainer", 0) as DocumentContainer
    }
    
    property set documentContainer ($arg :  DocumentContainer) {
      setRequireValue("documentContainer", 0, $arg)
    }
    
    property get documentsActionsHelper () : gw.document.DocumentsActionsUIHelper {
      return getVariableValue("documentsActionsHelper", 0) as gw.document.DocumentsActionsUIHelper
    }
    
    property set documentsActionsHelper ($arg :  gw.document.DocumentsActionsUIHelper) {
      setVariableValue("documentsActionsHelper", 0, $arg)
    }
    
    
  }
  
  
}