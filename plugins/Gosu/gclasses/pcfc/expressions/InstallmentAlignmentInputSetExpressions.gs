package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/payment/InstallmentAlignmentInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InstallmentAlignmentInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/payment/InstallmentAlignmentInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InstallmentAlignmentInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on BooleanRadioInput (id=AlignInstallmentsToInvoicesInput_Input) at InstallmentAlignmentInputSet.pcf: line 18, column 56
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.AlignInstallmentsToInvoices = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=AlignInstallmentsToInvoicesInput_Input) at InstallmentAlignmentInputSet.pcf: line 18, column 56
    function valueRoot_2 () : java.lang.Object {
      return paymentPlan
    }
    
    // 'value' attribute on BooleanRadioInput (id=AlignInstallmentsToInvoicesInput_Input) at InstallmentAlignmentInputSet.pcf: line 18, column 56
    function value_0 () : java.lang.Boolean {
      return paymentPlan.AlignInstallmentsToInvoices
    }
    
    property get paymentPlan () : PaymentPlan {
      return getRequireValue("paymentPlan", 0) as PaymentPlan
    }
    
    property set paymentPlan ($arg :  PaymentPlan) {
      setRequireValue("paymentPlan", 0, $arg)
    }
    
    
  }
  
  
}