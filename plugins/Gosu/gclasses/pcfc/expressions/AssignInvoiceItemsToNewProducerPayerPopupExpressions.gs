package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.acc.acct360.accountview.AccountBalanceTxnUtil
@javax.annotation.Generated("config/web/pcf/account/AssignInvoiceItemsToNewProducerPayerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AssignInvoiceItemsToNewProducerPayerPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AssignInvoiceItemsToNewProducerPayerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AssignInvoiceItemsToNewProducerPayerPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (invoiceItems :  InvoiceItem[], chargesToAssign :  Charge[], currentAccount :  Account) : int {
      return 0
    }
    
    // 'pickLocation' attribute on PickerInput (id=Producer_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 77, column 41
    function action_9 () : void {
      ProducerSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=Producer_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 77, column 41
    function action_dest_10 () : pcf.api.Destination {
      return pcf.ProducerSearchPopup.createDestination()
    }
    
    // 'beforeCommit' attribute on Popup (id=AssignInvoiceItemsToNewProducerPayerPopup) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 10, column 85
    function beforeCommit_32 (pickedValue :  java.lang.Object) : void {
      assignItemsToPayer()
    }
    
    // 'def' attribute on PanelRef (id=invoiceItemsLV) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 118, column 34
    function def_onEnter_30 (def :  pcf.InvoiceItemsLV) : void {
      def.onEnter(itemsToAssign, null, false)
    }
    
    // 'def' attribute on PanelRef (id=invoiceItemsLV) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 118, column 34
    function def_refreshVariables_31 (def :  pcf.InvoiceItemsLV) : void {
      def.refreshVariables(itemsToAssign, null, false)
    }
    
    // 'value' attribute on PickerInput (id=Producer_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 77, column 41
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPayerProducer = (__VALUE_TO_SET as entity.Producer)
    }
    
    // 'value' attribute on RangeInput (id=ProducerCode_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 89, column 45
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPayerProducerCode = (__VALUE_TO_SET as entity.ProducerCode)
    }
    
    // 'value' attribute on BooleanRadioInput (id=WhatItemsToAssign_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 98, column 46
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      isAssignAllItemsSelected = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=ReversePayments_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 108, column 48
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      isReversePaymentsSelected = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'initialValue' attribute on Variable at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 32, column 29
    function initialValue_0 () : InvoiceItem[] {
      return computeItemsToAssign()
    }
    
    // 'initialValue' attribute on Variable at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 36, column 59
    function initialValue_1 () : gw.api.web.producer.ProducerSearchConverter {
      return new gw.api.web.producer.ProducerSearchConverter()
    }
    
    // 'inputConversion' attribute on PickerInput (id=Producer_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 77, column 41
    function inputConversion_12 (VALUE :  java.lang.String) : java.lang.Object {
      return producerSearchConverter.getProducer( VALUE )
    }
    
    // 'label' attribute on AlertBar (id=notOwnerOrPayerWarning) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 58, column 44
    function label_4 () : java.lang.Object {
      return DisplayKey.get("Web.AssignInvoiceItemsToNewPayerPopup.ProducerNotOwnerOrPayerWarning", newPayerProducer.DisplayName)
    }
    
    // 'label' attribute on AlertBar (id=producerNotAgblWarning) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 62, column 44
    function label_6 () : java.lang.Object {
      return DisplayKey.get("Web.AssignInvoiceItemsToNewPayerPopup.ProducerNotAgblWarning", newPayerProducer.DisplayName)
    }
    
    // 'onChange' attribute on PostOnChange at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 100, column 66
    function onChange_23 () : void {
      itemsToAssign = computeItemsToAssign()
    }
    
    // 'onChange' attribute on PostOnChange at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 79, column 88
    function onChange_8 () : void {
      autoSelectSoleProducerCode(); overridingInvoiceStream = null
    }
    
    // 'onPick' attribute on PickerInput (id=Producer_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 77, column 41
    function onPick_11 (PickedValue :  Producer) : void {
      autoSelectSoleProducerCode(); overridingInvoiceStream = null
    }
    
    // 'optionLabel' attribute on RangeInput (id=ProducerCode_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 89, column 45
    function optionLabel_18 (VALUE :  entity.ProducerCode) : java.lang.String {
      return VALUE.Code
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 89, column 45
    function valueRange_19 () : java.lang.Object {
      return newPayerProducer.ActiveProducerCodes
    }
    
    // 'value' attribute on PickerInput (id=Producer_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 77, column 41
    function value_13 () : entity.Producer {
      return newPayerProducer
    }
    
    // 'value' attribute on RangeInput (id=ProducerCode_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 89, column 45
    function value_16 () : entity.ProducerCode {
      return newPayerProducerCode
    }
    
    // 'value' attribute on BooleanRadioInput (id=WhatItemsToAssign_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 98, column 46
    function value_24 () : java.lang.Boolean {
      return isAssignAllItemsSelected
    }
    
    // 'value' attribute on BooleanRadioInput (id=ReversePayments_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 108, column 48
    function value_27 () : java.lang.Boolean {
      return isReversePaymentsSelected
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 89, column 45
    function verifyValueRangeIsAllowedType_20 ($$arg :  entity.ProducerCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 89, column 45
    function verifyValueRangeIsAllowedType_20 ($$arg :  gw.api.database.IQueryBeanResult<entity.ProducerCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 89, column 45
    function verifyValueRangeIsAllowedType_20 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 89, column 45
    function verifyValueRange_21 () : void {
      var __valueRangeArg = newPayerProducer.ActiveProducerCodes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_20(__valueRangeArg)
    }
    
    // 'updateVisible' attribute on EditButtons at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 53, column 129
    function visible_2 () : java.lang.Boolean {
      return newPayerProducerCode != null && newPayerProducer != null && !producerHasNoAgblPlan()
    }
    
    // 'visible' attribute on AlertBar (id=notOwnerOrPayerWarning) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 58, column 44
    function visible_3 () : java.lang.Boolean {
      return hasNonOwnedOrPaidItem()
    }
    
    // 'visible' attribute on AlertBar (id=producerNotAgblWarning) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 62, column 44
    function visible_5 () : java.lang.Boolean {
      return producerHasNoAgblPlan()
    }
    
    // 'visible' attribute on AlertBar (id=filteringFrozenItemsWarning) at AssignInvoiceItemsToNewProducerPayerPopup.pcf: line 66, column 54
    function visible_7 () : java.lang.Boolean {
      return showFilteringFrozenItemsWarning()
    }
    
    override property get CurrentLocation () : pcf.AssignInvoiceItemsToNewProducerPayerPopup {
      return super.CurrentLocation as pcf.AssignInvoiceItemsToNewProducerPayerPopup
    }
    
    property get chargesToAssign () : Charge[] {
      return getVariableValue("chargesToAssign", 0) as Charge[]
    }
    
    property set chargesToAssign ($arg :  Charge[]) {
      setVariableValue("chargesToAssign", 0, $arg)
    }
    
    property get currentAccount () : Account {
      return getVariableValue("currentAccount", 0) as Account
    }
    
    property set currentAccount ($arg :  Account) {
      setVariableValue("currentAccount", 0, $arg)
    }
    
    property get invoiceItems () : InvoiceItem[] {
      return getVariableValue("invoiceItems", 0) as InvoiceItem[]
    }
    
    property set invoiceItems ($arg :  InvoiceItem[]) {
      setVariableValue("invoiceItems", 0, $arg)
    }
    
    property get isAssignAllItemsSelected () : boolean {
      return getVariableValue("isAssignAllItemsSelected", 0) as java.lang.Boolean
    }
    
    property set isAssignAllItemsSelected ($arg :  boolean) {
      setVariableValue("isAssignAllItemsSelected", 0, $arg)
    }
    
    property get isReversePaymentsSelected () : boolean {
      return getVariableValue("isReversePaymentsSelected", 0) as java.lang.Boolean
    }
    
    property set isReversePaymentsSelected ($arg :  boolean) {
      setVariableValue("isReversePaymentsSelected", 0, $arg)
    }
    
    property get itemsToAssign () : InvoiceItem[] {
      return getVariableValue("itemsToAssign", 0) as InvoiceItem[]
    }
    
    property set itemsToAssign ($arg :  InvoiceItem[]) {
      setVariableValue("itemsToAssign", 0, $arg)
    }
    
    property get newPayerProducer () : Producer {
      return getVariableValue("newPayerProducer", 0) as Producer
    }
    
    property set newPayerProducer ($arg :  Producer) {
      setVariableValue("newPayerProducer", 0, $arg)
    }
    
    property get newPayerProducerCode () : ProducerCode {
      return getVariableValue("newPayerProducerCode", 0) as ProducerCode
    }
    
    property set newPayerProducerCode ($arg :  ProducerCode) {
      setVariableValue("newPayerProducerCode", 0, $arg)
    }
    
    property get overridingInvoiceStream () : InvoiceStream {
      return getVariableValue("overridingInvoiceStream", 0) as InvoiceStream
    }
    
    property set overridingInvoiceStream ($arg :  InvoiceStream) {
      setVariableValue("overridingInvoiceStream", 0, $arg)
    }
    
    property get producerSearchConverter () : gw.api.web.producer.ProducerSearchConverter {
      return getVariableValue("producerSearchConverter", 0) as gw.api.web.producer.ProducerSearchConverter
    }
    
    property set producerSearchConverter ($arg :  gw.api.web.producer.ProducerSearchConverter) {
      setVariableValue("producerSearchConverter", 0, $arg)
    }
    
    
        function hasNonOwnedOrPaidItem() : boolean {
          return invoiceItems.hasMatch(\invoiceItem -> (invoiceItem.getOwner() != currentAccount) and (invoiceItem.getPayer() != currentAccount))
        }
    
        function producerHasNoAgblPlan() : boolean {
          if (newPayerProducer == null) {
            return false
          } else {
            return (newPayerProducer.AgencyBillPlan == null)
          }
        }
    
        function getNotFullyPaidInvoiceItems() : InvoiceItem[] {
          return invoiceItems.where(\invoiceItem -> !invoiceItem.FullyConsumed)
        }
    
        function computeItemsBasedOnSelection() : InvoiceItem[] {
          return isAssignAllItemsSelected ? invoiceItems : getNotFullyPaidInvoiceItems()
        }
    
        function computeItemsToAssign() : InvoiceItem[] {
          return computeItemsBasedOnSelection().where(\invoiceItem -> !invoiceItem.Frozen)
        }
    
        function showFilteringFrozenItemsWarning() : boolean {
          return computeItemsBasedOnSelection().hasMatch(\item -> item.Frozen)
        }
    
        function assignItemsToPayer() {
          newPayerProducer.becomePayerOfInvoiceItems(newPayerProducerCode,
            isReversePaymentsSelected
              ? gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem.Yes
              : gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem.No,
            itemsToAssign)
    
          AccountBalanceTxnUtil.createMoveItemRecord(currentAccount,
            itemsToAssign,
            AcctBalItemMovedType_Ext.TC_PAYERCHANGED)
          makeOverridesDefaultForAllPassedInCharges()
        }
    
        function makeOverridesDefaultForAllPassedInCharges() {
          for (chargeToAssign in chargesToAssign) {
            chargeToAssign.setOverridingProducerPayer(newPayerProducer, newPayerProducerCode, overridingInvoiceStream)
          }
        }
    
        function autoSelectSoleProducerCode() {
          var activeProducerCodes = newPayerProducer.ActiveProducerCodes;
          if (activeProducerCodes.length == 1) {
            newPayerProducerCode = activeProducerCodes[0];
          }
        }
    
    
  }
  
  
}