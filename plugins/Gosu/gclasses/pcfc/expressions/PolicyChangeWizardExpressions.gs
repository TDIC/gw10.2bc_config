package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyChangeWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyChangeWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyChangeWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyChangeWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Wizard (id=PolicyChangeWizard) at PolicyChangeWizard.pcf: line 7, column 29
    function beforeCommit_7 (pickedValue :  java.lang.Object) : void {
      chargesStepView.beforeCommit(); executePolicyChange()
    }
    
    // 'initialValue' attribute on Variable at PolicyChangeWizard.pcf: line 16, column 28
    function initialValue_0 () : PolicyChange {
      return initializePolicyChange()
    }
    
    // 'initialValue' attribute on Variable at PolicyChangeWizard.pcf: line 20, column 114
    function initialValue_1 () : java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView> {
      return com.google.common.collect.Maps.newHashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>()
    }
    
    // 'initialValue' attribute on Variable at PolicyChangeWizard.pcf: line 24, column 62
    function initialValue_2 () : gw.web.policy.PolicyWizardChargeStepScreenView {
      return new gw.web.policy.PolicyWizardChargeStepScreenView(policyPeriod, policyChange)
    }
    
    // 'screen' attribute on WizardStep (id=BasicsStep) at PolicyChangeWizard.pcf: line 29, column 93
    function screen_onEnter_3 (def :  pcf.PolicyChangeWizardBasicsStepScreen) : void {
      def.onEnter(policyChange)
    }
    
    // 'screen' attribute on WizardStep (id=ChargeStep) at PolicyChangeWizard.pcf: line 35, column 94
    function screen_onEnter_5 (def :  pcf.PolicyChangeWizardChargesStepScreen) : void {
      def.onEnter(policyPeriod, policyChange, chargeToInvoicingOverridesViewMap, chargesStepView)
    }
    
    // 'screen' attribute on WizardStep (id=BasicsStep) at PolicyChangeWizard.pcf: line 29, column 93
    function screen_refreshVariables_4 (def :  pcf.PolicyChangeWizardBasicsStepScreen) : void {
      def.refreshVariables(policyChange)
    }
    
    // 'screen' attribute on WizardStep (id=ChargeStep) at PolicyChangeWizard.pcf: line 35, column 94
    function screen_refreshVariables_6 (def :  pcf.PolicyChangeWizardChargesStepScreen) : void {
      def.refreshVariables(policyPeriod, policyChange, chargeToInvoicingOverridesViewMap, chargesStepView)
    }
    
    // 'tabBar' attribute on Wizard (id=PolicyChangeWizard) at PolicyChangeWizard.pcf: line 7, column 29
    function tabBar_onEnter_8 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=PolicyChangeWizard) at PolicyChangeWizard.pcf: line 7, column 29
    function tabBar_refreshVariables_9 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.PolicyChangeWizard {
      return super.CurrentLocation as pcf.PolicyChangeWizard
    }
    
    property get chargeToInvoicingOverridesViewMap () : java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView> {
      return getVariableValue("chargeToInvoicingOverridesViewMap", 0) as java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>
    }
    
    property set chargeToInvoicingOverridesViewMap ($arg :  java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>) {
      setVariableValue("chargeToInvoicingOverridesViewMap", 0, $arg)
    }
    
    property get chargesStepView () : gw.web.policy.PolicyWizardChargeStepScreenView {
      return getVariableValue("chargesStepView", 0) as gw.web.policy.PolicyWizardChargeStepScreenView
    }
    
    property set chargesStepView ($arg :  gw.web.policy.PolicyWizardChargeStepScreenView) {
      setVariableValue("chargesStepView", 0, $arg)
    }
    
    property get policyChange () : PolicyChange {
      return getVariableValue("policyChange", 0) as PolicyChange
    }
    
    property set policyChange ($arg :  PolicyChange) {
      setVariableValue("policyChange", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    function initializePolicyChange() : PolicyChange {
      var newPolicyChange = new entity.PolicyChange(policyPeriod.Currency)
      newPolicyChange.AssociatedPolicyPeriod = policyPeriod
      newPolicyChange.ModificationDate = gw.api.util.DateUtil.currentDate()
      return newPolicyChange
    }
    
    function executePolicyChange() {
      policyChange.ModificationDate = policyChange.ModificationDate.trimToMidnight()
      
      addModifiersBeforeExecute()
      policyChange.execute()
      policyPeriod.addHistoryFromGosu( gw.api.util.DateUtil.currentDate(), HistoryEventType.TC_POLICYCHANGED, 
          DisplayKey.get("Java.PolicyHistory.PolicyChanged", policyPeriod.PolicyNumberLong), null as Transaction, null, true )
    }
    
    function addModifiersBeforeExecute() {
      if (policyChange.ModificationDate == policyPeriod.EffectiveDate) {
        var issuancePaymentPlan = policyChange.AssociatedPolicyPeriod.PaymentPlan
        if (issuancePaymentPlan.HasDownPayment and issuancePaymentPlan.PolicyChange.ChargeSlicingOverrides.HasDownPayment == Boolean.FALSE /*because HasDownPayment might be null for an override*/) {
          var paymentPlanOverride = new DownPaymentOverride()
          paymentPlanOverride.DownPaymentPercent = issuancePaymentPlan.DownPaymentPercent
          policyChange.addToPaymentPlanModifiers(paymentPlanOverride)
        }
      }
    }
    
    
  }
  
  
}