package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketTabbedDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TroubleTicketTabbedDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketTabbedDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TroubleTicketTabbedDetailDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=AppliedHoldType_Cell) at TroubleTicketTabbedDetailDV.pcf: line 94, column 51
    function valueRoot_21 () : java.lang.Object {
      return appliedHoldType
    }
    
    // 'value' attribute on TypeKeyCell (id=AppliedHoldType_Cell) at TroubleTicketTabbedDetailDV.pcf: line 94, column 51
    function value_20 () : typekey.HoldType {
      return appliedHoldType.HoldType
    }
    
    // 'value' attribute on DateCell (id=ReleaseDate_Cell) at TroubleTicketTabbedDetailDV.pcf: line 98, column 58
    function value_23 () : java.util.Date {
      return appliedHoldType.ReleaseDate
    }
    
    property get appliedHoldType () : entity.HoldTypeEntry {
      return getIteratedValue(1) as entity.HoldTypeEntry
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketTabbedDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TroubleTicketTabbedDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=HoldDetailsButton) at TroubleTicketTabbedDetailDV.pcf: line 80, column 89
    function action_16 () : void {
      HoldDetailsPopup.push(troubleTicket.Hold)
    }
    
    // 'action' attribute on ToolbarButton (id=newNote) at TroubleTicketTabbedDetailDV.pcf: line 44, column 92
    function action_4 () : void {
      NewNoteOnTroubleTicketPopup.push(troubleTicket)
    }
    
    // 'action' attribute on ToolbarButton (id=HoldDetailsButton) at TroubleTicketTabbedDetailDV.pcf: line 80, column 89
    function action_dest_17 () : pcf.api.Destination {
      return pcf.HoldDetailsPopup.createDestination(troubleTicket.Hold)
    }
    
    // 'action' attribute on ToolbarButton (id=newNote) at TroubleTicketTabbedDetailDV.pcf: line 44, column 92
    function action_dest_5 () : pcf.api.Destination {
      return pcf.NewNoteOnTroubleTicketPopup.createDestination(troubleTicket)
    }
    
    // 'available' attribute on ToolbarButton (id=newNote) at TroubleTicketTabbedDetailDV.pcf: line 44, column 92
    function available_3 () : java.lang.Boolean {
      return !troubleTicket.IsClosed
    }
    
    // 'def' attribute on PanelRef at TroubleTicketTabbedDetailDV.pcf: line 22, column 89
    function def_onEnter_1 (def :  pcf.TroubleTicketRelatedEntitiesDV) : void {
      def.onEnter(troubleTicket, CreateTroubleTicketHelper)
    }
    
    // 'def' attribute on PanelRef at TroubleTicketTabbedDetailDV.pcf: line 58, column 57
    function def_onEnter_11 (def :  pcf.TroubleTicketActivitiesDV) : void {
      def.onEnter(troubleTicket)
    }
    
    // 'def' attribute on PanelRef at TroubleTicketTabbedDetailDV.pcf: line 64, column 66
    function def_onEnter_13 (def :  pcf.TroubleTicketRelatedTransactionsDV) : void {
      def.onEnter(troubleTicket)
    }
    
    // 'def' attribute on ListViewInput at TroubleTicketTabbedDetailDV.pcf: line 37, column 31
    function def_onEnter_8 (def :  pcf.ViewNotesLV) : void {
      def.onEnter(troubleTicket.Notes as gw.api.database.IQueryBeanResult<Note>)
    }
    
    // 'def' attribute on PanelRef at TroubleTicketTabbedDetailDV.pcf: line 58, column 57
    function def_refreshVariables_12 (def :  pcf.TroubleTicketActivitiesDV) : void {
      def.refreshVariables(troubleTicket)
    }
    
    // 'def' attribute on PanelRef at TroubleTicketTabbedDetailDV.pcf: line 64, column 66
    function def_refreshVariables_14 (def :  pcf.TroubleTicketRelatedTransactionsDV) : void {
      def.refreshVariables(troubleTicket)
    }
    
    // 'def' attribute on PanelRef at TroubleTicketTabbedDetailDV.pcf: line 22, column 89
    function def_refreshVariables_2 (def :  pcf.TroubleTicketRelatedEntitiesDV) : void {
      def.refreshVariables(troubleTicket, CreateTroubleTicketHelper)
    }
    
    // 'def' attribute on ListViewInput at TroubleTicketTabbedDetailDV.pcf: line 37, column 31
    function def_refreshVariables_9 (def :  pcf.ViewNotesLV) : void {
      def.refreshVariables(troubleTicket.Notes as gw.api.database.IQueryBeanResult<Note>)
    }
    
    // 'initialValue' attribute on Variable at TroubleTicketTabbedDetailDV.pcf: line 17, column 44
    function initialValue_0 () : gw.api.assignment.Assignee[] {
      return troubleTicket.InitialAssigneeForPicker
    }
    
    // 'value' attribute on TypeKeyCell (id=AppliedHoldType_Cell) at TroubleTicketTabbedDetailDV.pcf: line 94, column 51
    function sortValue_18 (appliedHoldType :  entity.HoldTypeEntry) : java.lang.Object {
      return appliedHoldType.HoldType
    }
    
    // 'value' attribute on DateCell (id=ReleaseDate_Cell) at TroubleTicketTabbedDetailDV.pcf: line 98, column 58
    function sortValue_19 (appliedHoldType :  entity.HoldTypeEntry) : java.lang.Object {
      return appliedHoldType.ReleaseDate
    }
    
    // 'value' attribute on RowIterator at TroubleTicketTabbedDetailDV.pcf: line 88, column 52
    function value_26 () : entity.HoldTypeEntry[] {
      return troubleTicket.Hold.HoldTypes
    }
    
    property get AssigneeHolder () : gw.api.assignment.Assignee[] {
      return getVariableValue("AssigneeHolder", 0) as gw.api.assignment.Assignee[]
    }
    
    property set AssigneeHolder ($arg :  gw.api.assignment.Assignee[]) {
      setVariableValue("AssigneeHolder", 0, $arg)
    }
    
    property get CreateTroubleTicketHelper () : CreateTroubleTicketHelper {
      return getRequireValue("CreateTroubleTicketHelper", 0) as CreateTroubleTicketHelper
    }
    
    property set CreateTroubleTicketHelper ($arg :  CreateTroubleTicketHelper) {
      setRequireValue("CreateTroubleTicketHelper", 0, $arg)
    }
    
    property get troubleTicket () : TroubleTicket {
      return getRequireValue("troubleTicket", 0) as TroubleTicket
    }
    
    property set troubleTicket ($arg :  TroubleTicket) {
      setRequireValue("troubleTicket", 0, $arg)
    }
    
    
  }
  
  
}