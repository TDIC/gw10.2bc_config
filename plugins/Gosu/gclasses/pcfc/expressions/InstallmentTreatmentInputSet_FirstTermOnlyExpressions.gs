package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/payment/InstallmentTreatmentInputSet.FirstTermOnly.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InstallmentTreatmentInputSet_FirstTermOnlyExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/payment/InstallmentTreatmentInputSet.FirstTermOnly.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InstallmentTreatmentInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=SubsequentTermsDaysFromEventToSecondInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 201, column 39
    function defaultSetter_104 (__VALUE_TO_SET :  java.lang.Object) : void {
      subViewHelper.DaysFromReferenceDateToSecondInstallment = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeInput (id=SubsequentTermsSecondInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 212, column 47
    function defaultSetter_108 (__VALUE_TO_SET :  java.lang.Object) : void {
      subViewHelper.BeforeAfterSecondInstallment = (__VALUE_TO_SET as gw.admin.paymentplan.When)
    }
    
    // 'value' attribute on TypeKeyInput (id=SubsequentTermsScheduleSecondInstallmentAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 222, column 52
    function defaultSetter_116 (__VALUE_TO_SET :  java.lang.Object) : void {
      subViewHelper.SecondInstallmentAfter = (__VALUE_TO_SET as typekey.PaymentScheduledAfter)
    }
    
    // 'value' attribute on TextInput (id=SubsequentTermsDaysFromEventToOneTimeCharge_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 234, column 39
    function defaultSetter_123 (__VALUE_TO_SET :  java.lang.Object) : void {
      subViewHelper.DaysFromReferenceDateToOneTimeCharge = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeInput (id=SubsequentTermsOneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 244, column 47
    function defaultSetter_127 (__VALUE_TO_SET :  java.lang.Object) : void {
      subViewHelper.BeforeAfterOneTimeCharge = (__VALUE_TO_SET as gw.admin.paymentplan.When)
    }
    
    // 'value' attribute on TypeKeyInput (id=SubsequentTermsScheduleOneTimeChargeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 254, column 52
    function defaultSetter_135 (__VALUE_TO_SET :  java.lang.Object) : void {
      subViewHelper.OneTimeChargeAfter = (__VALUE_TO_SET as typekey.PaymentScheduledAfter)
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToDownPayment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 44, column 39
    function defaultSetter_15 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.DaysFromReferenceDateToDownPayment = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeInput (id=DownPaymentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 55, column 47
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.BeforeAfterDownPayment = (__VALUE_TO_SET as gw.admin.paymentplan.When)
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleDownPaymentAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 65, column 52
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.DownPaymentAfter = (__VALUE_TO_SET as typekey.PaymentScheduledAfter)
    }
    
    // 'value' attribute on TextInput (id=MaximumNumberOfInstallments_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 25, column 38
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.MaximumNumberOfInstallments = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToFirstInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 77, column 39
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.DaysFromReferenceDateToFirstInstallment = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeInput (id=FirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 88, column 47
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.BeforeAfterFirstInstallment = (__VALUE_TO_SET as gw.admin.paymentplan.When)
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleFirstInstallmentAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 98, column 52
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.FirstInstallmentAfter = (__VALUE_TO_SET as typekey.PaymentScheduledAfter)
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToOneTimeCharge_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 110, column 39
    function defaultSetter_51 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.DaysFromReferenceDateToOneTimeCharge = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeInput (id=OneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 121, column 47
    function defaultSetter_55 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.BeforeAfterOneTimeCharge = (__VALUE_TO_SET as gw.admin.paymentplan.When)
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleOneTimeChargeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 131, column 52
    function defaultSetter_63 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.OneTimeChargeAfter = (__VALUE_TO_SET as typekey.PaymentScheduledAfter)
    }
    
    // 'value' attribute on TextInput (id=SubsequentTermsMaximumNumberOfInstallments_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 142, column 38
    function defaultSetter_69 (__VALUE_TO_SET :  java.lang.Object) : void {
      subViewHelper.MaximumNumberOfInstallments = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 153, column 64
    function defaultSetter_76 (__VALUE_TO_SET :  java.lang.Object) : void {
      subViewHelper.SecondInstallmentChoice = (__VALUE_TO_SET as gw.admin.paymentplan.SecondInstallmentChoice)
    }
    
    // 'value' attribute on TextInput (id=SubsequentTermsDaysFromEventToFirstInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 167, column 39
    function defaultSetter_86 (__VALUE_TO_SET :  java.lang.Object) : void {
      subViewHelper.DaysFromReferenceDateToFirstInstallment = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=DownPaymentPercent_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 37, column 43
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      viewHelper.DownPaymentPercent = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on RangeInput (id=SubsequentTermsFirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 178, column 47
    function defaultSetter_90 (__VALUE_TO_SET :  java.lang.Object) : void {
      subViewHelper.BeforeAfterFirstInstallment = (__VALUE_TO_SET as gw.admin.paymentplan.When)
    }
    
    // 'value' attribute on TypeKeyInput (id=SubsequentTermsScheduleFirstInstallmentAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 188, column 52
    function defaultSetter_98 (__VALUE_TO_SET :  java.lang.Object) : void {
      subViewHelper.FirstInstallmentAfter = (__VALUE_TO_SET as typekey.PaymentScheduledAfter)
    }
    
    // 'initialValue' attribute on Variable at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 15, column 58
    function initialValue_0 () : gw.admin.paymentplan.InstallmentViewHelper {
      return viewHelper.SubViewHelper
    }
    
    // 'noneSelectedLabel' attribute on TypeKeyInput (id=ScheduleDownPaymentAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 65, column 52
    function noneSelectedLabel_29 () : java.lang.String {
      return viewHelper.NoneSelectedLabel
    }
    
    // 'noneSelectedLabel' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 153, column 64
    function noneSelectedLabel_78 () : java.lang.String {
      return subViewHelper.NoneSelectedLabel
    }
    
    // 'onChange' attribute on PostOnChange at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 155, column 66
    function onChange_72 () : void {
      subViewHelper.onHasSecondInstallmentChange()
    }
    
    // 'required' attribute on TextInput (id=MaximumNumberOfInstallments_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 25, column 38
    function required_1 () : java.lang.Boolean {
      return viewHelper.RequiredFieldCalculator.MaximumNumberOfInstallmentsRequired
    }
    
    // 'required' attribute on TextInput (id=SubsequentTermsDaysFromEventToSecondInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 201, column 39
    function required_102 () : java.lang.Boolean {
      return subViewHelper.RequiredFieldCalculator.SecondInstallmentFieldsRequired
    }
    
    // 'required' attribute on TextInput (id=SubsequentTermsDaysFromEventToOneTimeCharge_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 234, column 39
    function required_121 () : java.lang.Boolean {
      return subViewHelper.RequiredFieldCalculator.OneTimeChargesFieldsRequired
    }
    
    // 'required' attribute on TextInput (id=DaysFromEventToFirstInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 77, column 39
    function required_31 () : java.lang.Boolean {
      return viewHelper.RequiredFieldCalculator.FirstInstallmentFieldsRequired
    }
    
    // 'required' attribute on TextInput (id=DaysFromEventToOneTimeCharge_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 110, column 39
    function required_49 () : java.lang.Boolean {
      return viewHelper.RequiredFieldCalculator.OneTimeChargesFieldsRequired
    }
    
    // 'required' attribute on TextInput (id=SubsequentTermsMaximumNumberOfInstallments_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 142, column 38
    function required_67 () : java.lang.Boolean {
      return subViewHelper.RequiredFieldCalculator.MaximumNumberOfInstallmentsRequired
    }
    
    // 'required' attribute on TextInput (id=DownPaymentPercent_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 37, column 43
    function required_7 () : java.lang.Boolean {
      return viewHelper.RequiredFieldCalculator.DownPaymentFieldsRequired
    }
    
    // 'required' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 153, column 64
    function required_74 () : java.lang.Boolean {
      return subViewHelper.RequiredFieldCalculator.HasSecondInstallmentRequired
    }
    
    // 'required' attribute on TextInput (id=SubsequentTermsDaysFromEventToFirstInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 167, column 39
    function required_84 () : java.lang.Boolean {
      return subViewHelper.RequiredFieldCalculator.FirstInstallmentFieldsRequired
    }
    
    // 'validationExpression' attribute on TextInput (id=DownPaymentPercent_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 37, column 43
    function validationExpression_6 () : java.lang.Object {
      return viewHelper.validateDownPayment()
    }
    
    // 'validationExpression' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 153, column 64
    function validationExpression_73 () : java.lang.Object {
      return subViewHelper.validateSecondInstallmentFields()
    }
    
    // 'valueRange' attribute on RangeInput (id=DownPaymentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 55, column 47
    function valueRange_21 () : java.lang.Object {
      return gw.admin.paymentplan.When.AllValues
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 153, column 64
    function valueRange_79 () : java.lang.Object {
      return gw.admin.paymentplan.SecondInstallmentChoice.getValues(false)
    }
    
    // 'value' attribute on TextInput (id=MaximumNumberOfInstallments_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 25, column 38
    function valueRoot_4 () : java.lang.Object {
      return viewHelper
    }
    
    // 'value' attribute on TextInput (id=SubsequentTermsMaximumNumberOfInstallments_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 142, column 38
    function valueRoot_70 () : java.lang.Object {
      return subViewHelper
    }
    
    // 'value' attribute on TextInput (id=SubsequentTermsDaysFromEventToSecondInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 201, column 39
    function value_103 () : java.lang.Integer {
      return subViewHelper.DaysFromReferenceDateToSecondInstallment
    }
    
    // 'value' attribute on RangeInput (id=SubsequentTermsSecondInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 212, column 47
    function value_107 () : gw.admin.paymentplan.When {
      return subViewHelper.BeforeAfterSecondInstallment
    }
    
    // 'value' attribute on TypeKeyInput (id=SubsequentTermsScheduleSecondInstallmentAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 222, column 52
    function value_115 () : typekey.PaymentScheduledAfter {
      return subViewHelper.SecondInstallmentAfter
    }
    
    // 'value' attribute on TextInput (id=SubsequentTermsDaysFromEventToOneTimeCharge_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 234, column 39
    function value_122 () : java.lang.Integer {
      return subViewHelper.DaysFromReferenceDateToOneTimeCharge
    }
    
    // 'value' attribute on RangeInput (id=SubsequentTermsOneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 244, column 47
    function value_126 () : gw.admin.paymentplan.When {
      return subViewHelper.BeforeAfterOneTimeCharge
    }
    
    // 'value' attribute on TypeKeyInput (id=SubsequentTermsScheduleOneTimeChargeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 254, column 52
    function value_134 () : typekey.PaymentScheduledAfter {
      return subViewHelper.OneTimeChargeAfter
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToDownPayment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 44, column 39
    function value_14 () : java.lang.Integer {
      return viewHelper.DaysFromReferenceDateToDownPayment
    }
    
    // 'value' attribute on RangeInput (id=DownPaymentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 55, column 47
    function value_18 () : gw.admin.paymentplan.When {
      return viewHelper.BeforeAfterDownPayment
    }
    
    // 'value' attribute on TextInput (id=MaximumNumberOfInstallments_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 25, column 38
    function value_2 () : java.lang.Integer {
      return viewHelper.MaximumNumberOfInstallments
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleDownPaymentAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 65, column 52
    function value_26 () : typekey.PaymentScheduledAfter {
      return viewHelper.DownPaymentAfter
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToFirstInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 77, column 39
    function value_32 () : java.lang.Integer {
      return viewHelper.DaysFromReferenceDateToFirstInstallment
    }
    
    // 'value' attribute on RangeInput (id=FirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 88, column 47
    function value_36 () : gw.admin.paymentplan.When {
      return viewHelper.BeforeAfterFirstInstallment
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleFirstInstallmentAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 98, column 52
    function value_44 () : typekey.PaymentScheduledAfter {
      return viewHelper.FirstInstallmentAfter
    }
    
    // 'value' attribute on TextInput (id=DaysFromEventToOneTimeCharge_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 110, column 39
    function value_50 () : java.lang.Integer {
      return viewHelper.DaysFromReferenceDateToOneTimeCharge
    }
    
    // 'value' attribute on RangeInput (id=OneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 121, column 47
    function value_54 () : gw.admin.paymentplan.When {
      return viewHelper.BeforeAfterOneTimeCharge
    }
    
    // 'value' attribute on TypeKeyInput (id=ScheduleOneTimeChargeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 131, column 52
    function value_62 () : typekey.PaymentScheduledAfter {
      return viewHelper.OneTimeChargeAfter
    }
    
    // 'value' attribute on TextInput (id=SubsequentTermsMaximumNumberOfInstallments_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 142, column 38
    function value_68 () : java.lang.Integer {
      return subViewHelper.MaximumNumberOfInstallments
    }
    
    // 'value' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 153, column 64
    function value_75 () : gw.admin.paymentplan.SecondInstallmentChoice {
      return subViewHelper.SecondInstallmentChoice
    }
    
    // 'value' attribute on TextInput (id=DownPaymentPercent_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 37, column 43
    function value_8 () : java.math.BigDecimal {
      return viewHelper.DownPaymentPercent
    }
    
    // 'value' attribute on TextInput (id=SubsequentTermsDaysFromEventToFirstInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 167, column 39
    function value_85 () : java.lang.Integer {
      return subViewHelper.DaysFromReferenceDateToFirstInstallment
    }
    
    // 'value' attribute on RangeInput (id=SubsequentTermsFirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 178, column 47
    function value_89 () : gw.admin.paymentplan.When {
      return subViewHelper.BeforeAfterFirstInstallment
    }
    
    // 'value' attribute on TypeKeyInput (id=SubsequentTermsScheduleFirstInstallmentAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 188, column 52
    function value_97 () : typekey.PaymentScheduledAfter {
      return subViewHelper.FirstInstallmentAfter
    }
    
    // 'valueRange' attribute on RangeInput (id=SubsequentTermsSecondInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 212, column 47
    function verifyValueRangeIsAllowedType_111 ($$arg :  gw.admin.paymentplan.When[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SubsequentTermsSecondInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 212, column 47
    function verifyValueRangeIsAllowedType_111 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SubsequentTermsOneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 244, column 47
    function verifyValueRangeIsAllowedType_130 ($$arg :  gw.admin.paymentplan.When[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SubsequentTermsOneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 244, column 47
    function verifyValueRangeIsAllowedType_130 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DownPaymentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 55, column 47
    function verifyValueRangeIsAllowedType_22 ($$arg :  gw.admin.paymentplan.When[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DownPaymentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 55, column 47
    function verifyValueRangeIsAllowedType_22 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=FirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 88, column 47
    function verifyValueRangeIsAllowedType_40 ($$arg :  gw.admin.paymentplan.When[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=FirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 88, column 47
    function verifyValueRangeIsAllowedType_40 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 121, column 47
    function verifyValueRangeIsAllowedType_58 ($$arg :  gw.admin.paymentplan.When[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=OneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 121, column 47
    function verifyValueRangeIsAllowedType_58 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 153, column 64
    function verifyValueRangeIsAllowedType_80 ($$arg :  gw.admin.paymentplan.SecondInstallmentChoice[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 153, column 64
    function verifyValueRangeIsAllowedType_80 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SubsequentTermsFirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 178, column 47
    function verifyValueRangeIsAllowedType_93 ($$arg :  gw.admin.paymentplan.When[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SubsequentTermsFirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 178, column 47
    function verifyValueRangeIsAllowedType_93 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=SubsequentTermsSecondInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 212, column 47
    function verifyValueRange_112 () : void {
      var __valueRangeArg = gw.admin.paymentplan.When.AllValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_111(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=SubsequentTermsOneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 244, column 47
    function verifyValueRange_131 () : void {
      var __valueRangeArg = gw.admin.paymentplan.When.AllValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_130(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=DownPaymentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 55, column 47
    function verifyValueRange_23 () : void {
      var __valueRangeArg = gw.admin.paymentplan.When.AllValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_22(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=FirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 88, column 47
    function verifyValueRange_41 () : void {
      var __valueRangeArg = gw.admin.paymentplan.When.AllValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_40(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=OneTimeChargeBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 121, column 47
    function verifyValueRange_59 () : void {
      var __valueRangeArg = gw.admin.paymentplan.When.AllValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_58(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=SubsequentTermsScheduleSecondInstallment_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 153, column 64
    function verifyValueRange_81 () : void {
      var __valueRangeArg = gw.admin.paymentplan.SecondInstallmentChoice.getValues(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_80(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=SubsequentTermsFirstInstallmentBeforeAfter_Input) at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 178, column 47
    function verifyValueRange_94 () : void {
      var __valueRangeArg = gw.admin.paymentplan.When.AllValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_93(__valueRangeArg)
    }
    
    // 'visible' attribute on InputSet at InstallmentTreatmentInputSet.FirstTermOnly.pcf: line 191, column 86
    function visible_120 () : java.lang.Boolean {
      return subViewHelper.RequiredFieldCalculator.CanShowSecondInstallmentFields
    }
    
    property get subViewHelper () : gw.admin.paymentplan.InstallmentViewHelper {
      return getVariableValue("subViewHelper", 0) as gw.admin.paymentplan.InstallmentViewHelper
    }
    
    property set subViewHelper ($arg :  gw.admin.paymentplan.InstallmentViewHelper) {
      setVariableValue("subViewHelper", 0, $arg)
    }
    
    property get viewHelper () : gw.admin.paymentplan.InstallmentViewHelper {
      return getRequireValue("viewHelper", 0) as gw.admin.paymentplan.InstallmentViewHelper
    }
    
    property set viewHelper ($arg :  gw.admin.paymentplan.InstallmentViewHelper) {
      setRequireValue("viewHelper", 0, $arg)
    }
    
    
  }
  
  
}