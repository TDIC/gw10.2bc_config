package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/Policy360View.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class Policy360ViewExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/acct360/Policy360View.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class Policy360ViewExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'def' attribute on ScreenRef at Policy360View.pcf: line 16, column 74
    function def_onEnter_0 (def :  pcf.AccountView360Screen) : void {
      def.onEnter(plcyPeriod.Account, plcyPeriod.Policy)
    }
    
    // 'def' attribute on ScreenRef at Policy360View.pcf: line 16, column 74
    function def_refreshVariables_1 (def :  pcf.AccountView360Screen) : void {
      def.refreshVariables(plcyPeriod.Account, plcyPeriod.Policy)
    }
    
    // Page (id=Policy360View) at Policy360View.pcf: line 9, column 72
    static function parent_2 (plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyGroup.createDestination(plcyPeriod)
    }
    
    override property get CurrentLocation () : pcf.Policy360View {
      return super.CurrentLocation as pcf.Policy360View
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 0, $arg)
    }
    
    
  }
  
  
}