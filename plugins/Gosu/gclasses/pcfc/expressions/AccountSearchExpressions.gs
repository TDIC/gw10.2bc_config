package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/AccountSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountSearchExpressions {
  @javax.annotation.Generated("config/web/pcf/search/AccountSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountSearchExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=AccountSearch) at AccountSearch.pcf: line 8, column 67
    static function canVisit_2 () : java.lang.Boolean {
      return perm.System.viewsearch and perm.System.acctsearch
    }
    
    // 'def' attribute on ScreenRef at AccountSearch.pcf: line 10, column 46
    function def_onEnter_0 (def :  pcf.AccountSearchScreen) : void {
      def.onEnter(true, true)
    }
    
    // 'def' attribute on ScreenRef at AccountSearch.pcf: line 10, column 46
    function def_refreshVariables_1 (def :  pcf.AccountSearchScreen) : void {
      def.refreshVariables(true, true)
    }
    
    // Page (id=AccountSearch) at AccountSearch.pcf: line 8, column 67
    static function parent_3 () : pcf.api.Destination {
      return pcf.SearchGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.AccountSearch {
      return super.CurrentLocation as pcf.AccountSearch
    }
    
    
  }
  
  
}