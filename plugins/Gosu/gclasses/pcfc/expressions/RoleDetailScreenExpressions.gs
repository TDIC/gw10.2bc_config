package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/roles/RoleDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class RoleDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/roles/RoleDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class RoleDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at RoleDetailScreen.pcf: line 19, column 37
    function def_onEnter_2 (def :  pcf.RoleDetailDV) : void {
      def.onEnter(Role)
    }
    
    // 'def' attribute on PanelRef at RoleDetailScreen.pcf: line 21, column 212
    function def_onEnter_4 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(Role, { "Name", "Description"}, { DisplayKey.get("Web.Admin.RoleDetailDV.Name"), DisplayKey.get("Web.Admin.RoleDetailDV.Description")})
    }
    
    // 'def' attribute on PanelRef at RoleDetailScreen.pcf: line 28, column 69
    function def_onEnter_7 (def :  pcf.RoleUsersLV) : void {
      def.onEnter(Role)
    }
    
    // 'def' attribute on PanelRef at RoleDetailScreen.pcf: line 19, column 37
    function def_refreshVariables_3 (def :  pcf.RoleDetailDV) : void {
      def.refreshVariables(Role)
    }
    
    // 'def' attribute on PanelRef at RoleDetailScreen.pcf: line 21, column 212
    function def_refreshVariables_5 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(Role, { "Name", "Description"}, { DisplayKey.get("Web.Admin.RoleDetailDV.Name"), DisplayKey.get("Web.Admin.RoleDetailDV.Description")})
    }
    
    // 'def' attribute on PanelRef at RoleDetailScreen.pcf: line 28, column 69
    function def_refreshVariables_8 (def :  pcf.RoleUsersLV) : void {
      def.refreshVariables(Role)
    }
    
    // 'editable' attribute on PanelRef at RoleDetailScreen.pcf: line 28, column 69
    function editable_6 () : java.lang.Boolean {
      return perm.User.grantroles and perm.System.useradmin
    }
    
    // EditButtons at RoleDetailScreen.pcf: line 12, column 39
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'editVisible' attribute on EditButtons at RoleDetailScreen.pcf: line 12, column 39
    function visible_0 () : java.lang.Boolean {
      return perm.Role.edit
    }
    
    property get Role () : Role {
      return getRequireValue("Role", 0) as Role
    }
    
    property set Role ($arg :  Role) {
      setRequireValue("Role", 0, $arg)
    }
    
    
  }
  
  
}