package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewFundsTransferReversalSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewFundsTransferReversalSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewFundsTransferReversalSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=Select) at NewFundsTransferReversalSearchScreen.pcf: line 81, column 60
    function action_22 () : void {
      transferFundReversalContext.TransferFundTransaction = transferFund; (CurrentLocation as pcf.api.Wizard).next();
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at NewFundsTransferReversalSearchScreen.pcf: line 104, column 50
    function currency_32 () : typekey.Currency {
      return transferFund.Currency
    }
    
    // 'value' attribute on DateCell (id=WriteoffDate_Cell) at NewFundsTransferReversalSearchScreen.pcf: line 87, column 54
    function valueRoot_24 () : java.lang.Object {
      return transferFund
    }
    
    // 'value' attribute on DateCell (id=WriteoffDate_Cell) at NewFundsTransferReversalSearchScreen.pcf: line 87, column 54
    function value_23 () : java.util.Date {
      return transferFund.CreateTime
    }
    
    // 'value' attribute on TextCell (id=Source_Cell) at NewFundsTransferReversalSearchScreen.pcf: line 92, column 72
    function value_26 () : java.lang.String {
      return getSourceName(transferFund.FundsTransfer)
    }
    
    // 'value' attribute on TextCell (id=Destination_Cell) at NewFundsTransferReversalSearchScreen.pcf: line 97, column 77
    function value_28 () : java.lang.String {
      return getDestinationName(transferFund.FundsTransfer)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at NewFundsTransferReversalSearchScreen.pcf: line 104, column 50
    function value_30 () : gw.pl.currency.MonetaryAmount {
      return transferFund.Amount
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at NewFundsTransferReversalSearchScreen.pcf: line 109, column 60
    function value_34 () : java.lang.String {
      return transferFund.ShortDisplayName
    }
    
    // 'visible' attribute on Link (id=Select) at NewFundsTransferReversalSearchScreen.pcf: line 81, column 60
    function visible_21 () : java.lang.Boolean {
      return transferFund.canReverse()
    }
    
    property get transferFund () : entity.TransferTransaction {
      return getIteratedValue(2) as entity.TransferTransaction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/NewFundsTransferReversalSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewFundsTransferReversalSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get transferFundReversalContext () : gw.transaction.TransferFundsReversalWizardContext {
      return getRequireValue("transferFundReversalContext", 0) as gw.transaction.TransferFundsReversalWizardContext
    }
    
    property set transferFundReversalContext ($arg :  gw.transaction.TransferFundsReversalWizardContext) {
      setRequireValue("transferFundReversalContext", 0, $arg)
    }
    
    function getSourceName(transfer : FundsTransfer) : String{
        if (transfer.SourceUnapplied != null) {
          return transfer.SourceUnapplied.TransactionSpecialDisplayName;
        } else {
          return transfer.SourceProducer.DisplayName
        }
      }
      
      
      function getDestinationName(transfer : FundsTransfer) : String{
        if (transfer.TargetUnapplied != null) {
          return transfer.TargetUnapplied.TransactionSpecialDisplayName;
        } else {
          return transfer.TargetProducer.DisplayName
        }
      }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/NewFundsTransferReversalSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends NewFundsTransferReversalSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at NewFundsTransferReversalSearchScreen.pcf: line 59, column 58
    function def_onEnter_19 (def :  pcf.SearchLinksInputSet) : void {
      def.onEnter(true, true)
    }
    
    // 'def' attribute on InputSetRef at NewFundsTransferReversalSearchScreen.pcf: line 59, column 58
    function def_refreshVariables_20 (def :  pcf.SearchLinksInputSet) : void {
      def.refreshVariables(true, true)
    }
    
    // 'value' attribute on TextInput (id=TransactionNumberCriterion_Input) at NewFundsTransferReversalSearchScreen.pcf: line 34, column 59
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.TransactionNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at NewFundsTransferReversalSearchScreen.pcf: line 49, column 54
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.EarliestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at NewFundsTransferReversalSearchScreen.pcf: line 54, column 52
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.LatestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeInput (id=TransactionTypeCriterion_Input) at NewFundsTransferReversalSearchScreen.pcf: line 42, column 86
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.TransferType  = (__VALUE_TO_SET as gw.search.TransferTransactionSearchCriteria.TransferType)
    }
    
    // 'searchCriteria' attribute on SearchPanel at NewFundsTransferReversalSearchScreen.pcf: line 23, column 91
    function searchCriteria_39 () : gw.search.TransferTransactionSearchCriteria {
      return new gw.search.TransferTransactionSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at NewFundsTransferReversalSearchScreen.pcf: line 23, column 91
    function search_38 () : java.lang.Object {
      return searchCriteria.performSearch()
    }
    
    // 'valueRange' attribute on RangeInput (id=TransactionTypeCriterion_Input) at NewFundsTransferReversalSearchScreen.pcf: line 42, column 86
    function valueRange_7 () : java.lang.Object {
      return gw.search.TransferTransactionSearchCriteria.TransferType.AllValues.where( \ c -> c == gw.search.TransferTransactionSearchCriteria.TransferType.FromAccountToDifferentAccount)
    }
    
    // 'value' attribute on TextInput (id=TransactionNumberCriterion_Input) at NewFundsTransferReversalSearchScreen.pcf: line 34, column 59
    function valueRoot_2 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TextInput (id=TransactionNumberCriterion_Input) at NewFundsTransferReversalSearchScreen.pcf: line 34, column 59
    function value_0 () : java.lang.String {
      return searchCriteria.TransactionNumber
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at NewFundsTransferReversalSearchScreen.pcf: line 49, column 54
    function value_11 () : java.util.Date {
      return searchCriteria.EarliestDate
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at NewFundsTransferReversalSearchScreen.pcf: line 54, column 52
    function value_15 () : java.util.Date {
      return searchCriteria.LatestDate
    }
    
    // 'value' attribute on RowIterator at NewFundsTransferReversalSearchScreen.pcf: line 71, column 96
    function value_37 () : gw.api.database.IQueryBeanResult<entity.TransferTransaction> {
      return transferFunds
    }
    
    // 'value' attribute on RangeInput (id=TransactionTypeCriterion_Input) at NewFundsTransferReversalSearchScreen.pcf: line 42, column 86
    function value_4 () : gw.search.TransferTransactionSearchCriteria.TransferType {
      return searchCriteria.TransferType 
    }
    
    // 'valueRange' attribute on RangeInput (id=TransactionTypeCriterion_Input) at NewFundsTransferReversalSearchScreen.pcf: line 42, column 86
    function verifyValueRangeIsAllowedType_8 ($$arg :  gw.search.TransferTransactionSearchCriteria.TransferType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=TransactionTypeCriterion_Input) at NewFundsTransferReversalSearchScreen.pcf: line 42, column 86
    function verifyValueRangeIsAllowedType_8 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=TransactionTypeCriterion_Input) at NewFundsTransferReversalSearchScreen.pcf: line 42, column 86
    function verifyValueRange_9 () : void {
      var __valueRangeArg = gw.search.TransferTransactionSearchCriteria.TransferType.AllValues.where( \ c -> c == gw.search.TransferTransactionSearchCriteria.TransferType.FromAccountToDifferentAccount)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_8(__valueRangeArg)
    }
    
    property get searchCriteria () : gw.search.TransferTransactionSearchCriteria {
      return getCriteriaValue(1) as gw.search.TransferTransactionSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.TransferTransactionSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    property get transferFunds () : gw.api.database.IQueryBeanResult<TransferTransaction> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<TransferTransaction>
    }
    
    
  }
  
  
}