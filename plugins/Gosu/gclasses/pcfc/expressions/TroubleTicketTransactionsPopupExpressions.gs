package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketTransactionsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TroubleTicketTransactionsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketTransactionsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TroubleTicketTransactionsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (TroubleTicket :  TroubleTicket) : int {
      return 0
    }
    
    // 'def' attribute on PanelRef at TroubleTicketTransactionsPopup.pcf: line 30, column 66
    function def_onEnter_1 (def :  pcf.TroubleTicketRelatedTransactionsDV) : void {
      def.onEnter(TroubleTicket)
    }
    
    // 'def' attribute on PanelRef at TroubleTicketTransactionsPopup.pcf: line 30, column 66
    function def_refreshVariables_2 (def :  pcf.TroubleTicketRelatedTransactionsDV) : void {
      def.refreshVariables(TroubleTicket)
    }
    
    // EditButtons at TroubleTicketTransactionsPopup.pcf: line 20, column 23
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    override property get CurrentLocation () : pcf.TroubleTicketTransactionsPopup {
      return super.CurrentLocation as pcf.TroubleTicketTransactionsPopup
    }
    
    property get TroubleTicket () : TroubleTicket {
      return getVariableValue("TroubleTicket", 0) as TroubleTicket
    }
    
    property set TroubleTicket ($arg :  TroubleTicket) {
      setVariableValue("TroubleTicket", 0, $arg)
    }
    
    
  }
  
  
}