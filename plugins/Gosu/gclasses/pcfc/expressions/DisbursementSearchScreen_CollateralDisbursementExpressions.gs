package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DisbursementSearchScreen.CollateralDisbursement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DisbursementSearchScreen_CollateralDisbursementExpressions {
  @javax.annotation.Generated("config/web/pcf/search/DisbursementSearchScreen.CollateralDisbursement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DisbursementSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get disbursementSubtypeHolder () : typekey.Disbursement[] {
      return getRequireValue("disbursementSubtypeHolder", 0) as typekey.Disbursement[]
    }
    
    property set disbursementSubtypeHolder ($arg :  typekey.Disbursement[]) {
      setRequireValue("disbursementSubtypeHolder", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/DisbursementSearchScreen.CollateralDisbursement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends DisbursementSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at DisbursementSearchScreen.CollateralDisbursement.pcf: line 21, column 45
    function def_onEnter_0 (def :  pcf.DisbursementSearchDV_AccountDisbursement) : void {
      def.onEnter(searchCriteria, disbursementSubtypeHolder)
    }
    
    // 'def' attribute on PanelRef at DisbursementSearchScreen.CollateralDisbursement.pcf: line 21, column 45
    function def_onEnter_2 (def :  pcf.DisbursementSearchDV_AgencyDisbursement) : void {
      def.onEnter(searchCriteria, disbursementSubtypeHolder)
    }
    
    // 'def' attribute on PanelRef at DisbursementSearchScreen.CollateralDisbursement.pcf: line 26, column 45
    function def_onEnter_5 (def :  pcf.DisbursementSearchResultsLV_default) : void {
      def.onEnter(disbursementSearchViews)
    }
    
    // 'def' attribute on PanelRef at DisbursementSearchScreen.CollateralDisbursement.pcf: line 21, column 45
    function def_refreshVariables_1 (def :  pcf.DisbursementSearchDV_AccountDisbursement) : void {
      def.refreshVariables(searchCriteria, disbursementSubtypeHolder)
    }
    
    // 'def' attribute on PanelRef at DisbursementSearchScreen.CollateralDisbursement.pcf: line 21, column 45
    function def_refreshVariables_3 (def :  pcf.DisbursementSearchDV_AgencyDisbursement) : void {
      def.refreshVariables(searchCriteria, disbursementSubtypeHolder)
    }
    
    // 'def' attribute on PanelRef at DisbursementSearchScreen.CollateralDisbursement.pcf: line 26, column 45
    function def_refreshVariables_6 (def :  pcf.DisbursementSearchResultsLV_default) : void {
      def.refreshVariables(disbursementSearchViews)
    }
    
    // 'maxSearchResults' attribute on SearchPanel at DisbursementSearchScreen.CollateralDisbursement.pcf: line 18, column 86
    function maxSearchResults_8 () : java.lang.Object {
      return gw.api.system.BCConfigParameters.MaxSearchResults.Value
    }
    
    // 'mode' attribute on PanelRef at DisbursementSearchScreen.CollateralDisbursement.pcf: line 21, column 45
    function mode_4 () : java.lang.Object {
      return disbursementSubtypeHolder[0]
    }
    
    // 'searchCriteria' attribute on SearchPanel at DisbursementSearchScreen.CollateralDisbursement.pcf: line 18, column 86
    function searchCriteria_10 () : gw.search.CollDisbSearchCriteria {
      return new gw.search.CollDisbSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at DisbursementSearchScreen.CollateralDisbursement.pcf: line 18, column 86
    function search_9 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria)
    }
    
    property get disbursementSearchViews () : gw.api.database.IQueryBeanResult<CollDisbSearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<CollDisbSearchView>
    }
    
    property get searchCriteria () : gw.search.CollDisbSearchCriteria {
      return getCriteriaValue(1) as gw.search.CollDisbSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.CollDisbSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}