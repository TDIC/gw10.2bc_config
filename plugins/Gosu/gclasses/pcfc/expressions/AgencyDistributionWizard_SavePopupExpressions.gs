package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_SavePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyDistributionWizard_SavePopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_SavePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyDistributionWizard_SavePopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=FinishButton) at AgencyDistributionWizard_SavePopup.pcf: line 24, column 58
    function action_1 () : void {
      wizardState.finishSave(CurrentLocation)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at AgencyDistributionWizard_SavePopup.pcf: line 57, column 44
    function action_17 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // 'action' attribute on ToolbarButton (id=CancelButton) at AgencyDistributionWizard_SavePopup.pcf: line 28, column 62
    function action_2 () : void {
      wizardState.cancelSave(CurrentLocation)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at AgencyDistributionWizard_SavePopup.pcf: line 57, column 44
    function action_dest_18 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyDistributionWizard_SavePopup.pcf: line 53, column 35
    function currency_15 () : typekey.Currency {
      return wizardState.MoneySetup.Producer.Currency
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AgencyDistributionWizard_SavePopup.pcf: line 66, column 93
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      wizardState.AgencyCycleDistView.AgencyCycleDist.BaseMoneyReceived.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_SavePopup.pcf: line 18, column 40
    function initialValue_0 () : entity.BaseMoneyReceived {
      return wizardState.MoneySetup.Money
    }
    
    // 'label' attribute on AlertBar (id=SimilarDistributionExists) at AgencyDistributionWizard_SavePopup.pcf: line 34, column 63
    function label_4 () : java.lang.Object {
      return getSimilarDistributionText()
    }
    
    // 'title' attribute on Popup (id=AgencyDistributionWizard_SavePopup) at AgencyDistributionWizard_SavePopup.pcf: line 9, column 46
    static function title_28 (wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : java.lang.Object {
      return generateWizardTitle(wizardState)
    }
    
    // 'value' attribute on TextInput (id=PaymentInstrument_Input) at AgencyDistributionWizard_SavePopup.pcf: line 47, column 46
    function valueRoot_10 () : java.lang.Object {
      return wizardState.PaymentMoneySetup.Money
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyDistributionWizard_SavePopup.pcf: line 53, column 35
    function valueRoot_14 () : java.lang.Object {
      return money
    }
    
    // 'value' attribute on AltUserInput (id=SavedBy_Input) at AgencyDistributionWizard_SavePopup.pcf: line 57, column 44
    function valueRoot_20 () : java.lang.Object {
      return User.util
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AgencyDistributionWizard_SavePopup.pcf: line 66, column 93
    function valueRoot_26 () : java.lang.Object {
      return wizardState.AgencyCycleDistView.AgencyCycleDist.BaseMoneyReceived
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at AgencyDistributionWizard_SavePopup.pcf: line 41, column 42
    function valueRoot_6 () : java.lang.Object {
      return wizardState.MoneySetup
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyDistributionWizard_SavePopup.pcf: line 53, column 35
    function value_13 () : gw.pl.currency.MonetaryAmount {
      return money.Amount
    }
    
    // 'value' attribute on AltUserInput (id=SavedBy_Input) at AgencyDistributionWizard_SavePopup.pcf: line 57, column 44
    function value_19 () : entity.User {
      return User.util.CurrentUser
    }
    
    // 'value' attribute on DateInput (id=SavedDate_Input) at AgencyDistributionWizard_SavePopup.pcf: line 61, column 57
    function value_22 () : java.util.Date {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AgencyDistributionWizard_SavePopup.pcf: line 66, column 93
    function value_24 () : java.lang.String {
      return wizardState.AgencyCycleDistView.AgencyCycleDist.BaseMoneyReceived.Name
    }
    
    // 'value' attribute on TextInput (id=Producer_Input) at AgencyDistributionWizard_SavePopup.pcf: line 41, column 42
    function value_5 () : entity.Producer {
      return wizardState.MoneySetup.Producer
    }
    
    // 'value' attribute on TextInput (id=PaymentInstrument_Input) at AgencyDistributionWizard_SavePopup.pcf: line 47, column 46
    function value_9 () : entity.PaymentInstrument {
      return wizardState.PaymentMoneySetup.Money.PaymentInstrument
    }
    
    // 'visible' attribute on AlertBar (id=SimilarDistributionExists) at AgencyDistributionWizard_SavePopup.pcf: line 34, column 63
    function visible_3 () : java.lang.Boolean {
      return wizardState.SimilarSavedDistributionExists
    }
    
    // 'visible' attribute on TextInput (id=PaymentInstrument_Input) at AgencyDistributionWizard_SavePopup.pcf: line 47, column 46
    function visible_8 () : java.lang.Boolean {
      return wizardState.IsPayment
    }
    
    override property get CurrentLocation () : pcf.AgencyDistributionWizard_SavePopup {
      return super.CurrentLocation as pcf.AgencyDistributionWizard_SavePopup
    }
    
    property get money () : entity.BaseMoneyReceived {
      return getVariableValue("money", 0) as entity.BaseMoneyReceived
    }
    
    property set money ($arg :  entity.BaseMoneyReceived) {
      setVariableValue("money", 0, $arg)
    }
    
    property get wizardState () : gw.agencybill.AgencyDistributionWizardHelper {
      return getVariableValue("wizardState", 0) as gw.agencybill.AgencyDistributionWizardHelper
    }
    
    property set wizardState ($arg :  gw.agencybill.AgencyDistributionWizardHelper) {
      setVariableValue("wizardState", 0, $arg)
    }
    
    function getSimilarDistributionText() : String {
      if (wizardState.IsCreditDistribution) {
        return DisplayKey.get("Web.AgencyDistributionWizard.SavePopup.SimilarCreditDistributionExists")
      } else if (wizardState.IsPayment) {
        return DisplayKey.get("Web.AgencyDistributionWizard.SavePopup.SimilarPaymentExists")  
      } else {
        return DisplayKey.get("Web.AgencyDistributionWizard.SavePopup.SimilarPromiseExists")
      }
    }
    
    static function generateWizardTitle(wizardHelper: gw.agencybill.AgencyDistributionWizardHelper) : String {
      if (wizardHelper.IsCreditDistribution) {
        return DisplayKey.get("Web.AgencyDistributionWizard.SavePopup.Title.CreditDistribution")
      } else if (wizardHelper.IsPayment) {
        return DisplayKey.get("Web.AgencyDistributionWizard.SavePopup.Title.Payment")
      } else {
        return DisplayKey.get("Web.AgencyDistributionWizard.SavePopup.Title.Promise")
      }  
    }
    
    
  }
  
  
}