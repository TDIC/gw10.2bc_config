package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExecutedPromises.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillExecutedPromisesExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExecutedPromises.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillExecutedPromisesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    static function __constructorIndex (producer :  Producer, initialSelectedPromise :  AgencyCyclePromise) : int {
      return 1
    }
    
    // 'canVisit' attribute on Page (id=AgencyBillExecutedPromises) at AgencyBillExecutedPromises.pcf: line 8, column 79
    static function canVisit_88 (initialSelectedPromise :  AgencyCyclePromise, producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodpromview
    }
    
    // Page (id=AgencyBillExecutedPromises) at AgencyBillExecutedPromises.pcf: line 8, column 79
    static function parent_89 (initialSelectedPromise :  AgencyCyclePromise, producer :  Producer) : pcf.api.Destination {
      return pcf.AgencyBillPromises.createDestination(producer)
    }
    
    override property get CurrentLocation () : pcf.AgencyBillExecutedPromises {
      return super.CurrentLocation as pcf.AgencyBillExecutedPromises
    }
    
    property get initialSelectedPromise () : AgencyCyclePromise {
      return getVariableValue("initialSelectedPromise", 0) as AgencyCyclePromise
    }
    
    property set initialSelectedPromise ($arg :  AgencyCyclePromise) {
      setVariableValue("initialSelectedPromise", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExecutedPromises.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillExecutedPromises.pcf: line 86, column 58
    function condition_11 () : java.lang.Boolean {
      return promise.hasFrozenDistItem()
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillExecutedPromises.pcf: line 89, column 32
    function condition_12 () : java.lang.Boolean {
      return promise.Frozen
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillExecutedPromises.pcf: line 92, column 43
    function condition_13 () : java.lang.Boolean {
      return promise.Active and not promise.Modified
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillExecutedPromises.pcf: line 95, column 39
    function condition_14 () : java.lang.Boolean {
      return promise.SuspDistItemsThatHaveNotBeenReleased.HasElements
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at AgencyBillExecutedPromises.pcf: line 131, column 46
    function currency_32 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'initialValue' attribute on Variable at AgencyBillExecutedPromises.pcf: line 81, column 50
    function initialValue_10 () : entity.BaseMoneyReceived {
      return promise.BaseMoneyReceived
    }
    
    // RowIterator (id=PromisesIterator) at AgencyBillExecutedPromises.pcf: line 77, column 93
    function initializeVariables_49 () : void {
        money = promise.BaseMoneyReceived;

    }
    
    // 'value' attribute on DateCell (id=PromiseReceived_Cell) at AgencyBillExecutedPromises.pcf: line 102, column 47
    function valueRoot_16 () : java.lang.Object {
      return money
    }
    
    // 'value' attribute on BooleanRadioCell (id=Paid_Cell) at AgencyBillExecutedPromises.pcf: line 119, column 44
    function valueRoot_25 () : java.lang.Object {
      return promise
    }
    
    // 'value' attribute on DateCell (id=PaymentDate_Cell) at AgencyBillExecutedPromises.pcf: line 124, column 84
    function valueRoot_28 () : java.lang.Object {
      return promise.PaymentAppliedTo.BaseMoneyReceived
    }
    
    // 'value' attribute on DateCell (id=PromiseReceived_Cell) at AgencyBillExecutedPromises.pcf: line 102, column 47
    function value_15 () : java.util.Date {
      return money.ReceivedDate
    }
    
    // 'value' attribute on DateCell (id=SavedDate_Cell) at AgencyBillExecutedPromises.pcf: line 108, column 45
    function value_18 () : java.util.Date {
      return money.UpdateTime
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AgencyBillExecutedPromises.pcf: line 114, column 39
    function value_21 () : java.lang.String {
      return money.Name
    }
    
    // 'value' attribute on BooleanRadioCell (id=Paid_Cell) at AgencyBillExecutedPromises.pcf: line 119, column 44
    function value_24 () : java.lang.Boolean {
      return promise.Applied
    }
    
    // 'value' attribute on DateCell (id=PaymentDate_Cell) at AgencyBillExecutedPromises.pcf: line 124, column 84
    function value_27 () : java.util.Date {
      return promise.PaymentAppliedTo.BaseMoneyReceived.ReceivedDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AgencyBillExecutedPromises.pcf: line 131, column 46
    function value_30 () : gw.pl.currency.MonetaryAmount {
      return money.TotalAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Distributed_Cell) at AgencyBillExecutedPromises.pcf: line 138, column 65
    function value_34 () : gw.pl.currency.MonetaryAmount {
      return promise.NetDistributedToInvoiceItems
    }
    
    // 'value' attribute on MonetaryAmountCell (id=InSuspense_Cell) at AgencyBillExecutedPromises.pcf: line 145, column 50
    function value_38 () : gw.pl.currency.MonetaryAmount {
      return promise.NetInSuspense
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Remaining_Cell) at AgencyBillExecutedPromises.pcf: line 152, column 52
    function value_42 () : gw.pl.currency.MonetaryAmount {
      return promise.RemainingAmount
    }
    
    // 'value' attribute on BooleanRadioCell (id=Reversed_Cell) at AgencyBillExecutedPromises.pcf: line 157, column 45
    function value_46 () : java.lang.Boolean {
      return promise.Reversed
    }
    
    property get money () : entity.BaseMoneyReceived {
      return getVariableValue("money", 2) as entity.BaseMoneyReceived
    }
    
    property set money ($arg :  entity.BaseMoneyReceived) {
      setVariableValue("money", 2, $arg)
    }
    
    property get promise () : entity.AgencyCyclePromise {
      return getIteratedValue(2) as entity.AgencyCyclePromise
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExecutedPromises.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends AgencyBillExecutedPromisesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=Edit) at AgencyBillExecutedPromises.pcf: line 49, column 100
    function checkedRowAction_4 (promise :  entity.AgencyCyclePromise, CheckedValue :  entity.AgencyCyclePromise) : void {
      AgencyDistributionWizard.go(producer, CheckedValue.BaseMoneyReceived)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=Reverse) at AgencyBillExecutedPromises.pcf: line 57, column 29
    function checkedRowAction_5 (promise :  entity.AgencyCyclePromise, CheckedValue :  entity.AgencyCyclePromise) : void {
      AgencyDistributionReversalConfirmationPopup.push(CheckedValue)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=ViewSuspenseItems) at AgencyBillExecutedPromises.pcf: line 65, column 29
    function checkedRowAction_6 (promise :  entity.AgencyCyclePromise, CheckedValue :  entity.AgencyCyclePromise) : void {
      AgencySuspenseItemsPopup.push(CheckedValue, false)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyBillExecutedPromises.pcf: line 200, column 88
    function currency_66 () : typekey.Currency {
      return selectedPromise.Currency
    }
    
    // 'def' attribute on PanelRef at AgencyBillExecutedPromises.pcf: line 167, column 76
    function def_onEnter_51 (def :  pcf.AgencyBillDistArchiveWarningPanelSet) : void {
      def.onEnter(selectedPromise)
    }
    
    // 'def' attribute on InputSetRef at AgencyBillExecutedPromises.pcf: line 184, column 89
    function def_onEnter_59 (def :  pcf.AgencyBillPromises_PromiseApplicationInputSet) : void {
      def.onEnter(selectedPromise)
    }
    
    // 'def' attribute on PanelRef at AgencyBillExecutedPromises.pcf: line 229, column 64
    function def_onEnter_84 (def :  pcf.AgencyDistItemsReadOnlyCV) : void {
      def.onEnter(selectedPromise)
    }
    
    // 'def' attribute on PanelRef at AgencyBillExecutedPromises.pcf: line 167, column 76
    function def_refreshVariables_52 (def :  pcf.AgencyBillDistArchiveWarningPanelSet) : void {
      def.refreshVariables(selectedPromise)
    }
    
    // 'def' attribute on InputSetRef at AgencyBillExecutedPromises.pcf: line 184, column 89
    function def_refreshVariables_60 (def :  pcf.AgencyBillPromises_PromiseApplicationInputSet) : void {
      def.refreshVariables(selectedPromise)
    }
    
    // 'def' attribute on PanelRef at AgencyBillExecutedPromises.pcf: line 229, column 64
    function def_refreshVariables_85 (def :  pcf.AgencyDistItemsReadOnlyCV) : void {
      def.refreshVariables(selectedPromise)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillExecutedPromises.pcf: line 32, column 130
    function filter_0 () : gw.api.filters.IFilter {
      return new gw.payment.BaseDistReceivedDateFilter(30)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillExecutedPromises.pcf: line 35, column 130
    function filter_1 () : gw.api.filters.IFilter {
      return new gw.payment.BaseDistReceivedDateFilter(60)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillExecutedPromises.pcf: line 38, column 130
    function filter_2 () : gw.api.filters.IFilter {
      return new gw.payment.BaseDistReceivedDateFilter(90)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillExecutedPromises.pcf: line 41, column 123
    function filter_3 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("All", \ qf -> {})
    }
    
    // 'selectionOnEnter' attribute on ListDetailPanel at AgencyBillExecutedPromises.pcf: line 23, column 44
    function selectionOnEnter_87 () : java.lang.Object {
      return initialSelectedPromise
    }
    
    // 'sortBy' attribute on DateCell (id=PromiseReceived_Cell) at AgencyBillExecutedPromises.pcf: line 102, column 47
    function sortValue_7 (promise :  entity.AgencyCyclePromise) : java.lang.Object {
      var money : entity.BaseMoneyReceived = (promise.BaseMoneyReceived)
return promise.BaseMoneyReceived.ReceivedDate
    }
    
    // 'value' attribute on DateCell (id=SavedDate_Cell) at AgencyBillExecutedPromises.pcf: line 108, column 45
    function sortValue_8 (promise :  entity.AgencyCyclePromise) : java.lang.Object {
      var money : entity.BaseMoneyReceived = (promise.BaseMoneyReceived)
return money.UpdateTime
    }
    
    // 'sortBy' attribute on TextCell (id=Name_Cell) at AgencyBillExecutedPromises.pcf: line 114, column 39
    function sortValue_9 (promise :  entity.AgencyCyclePromise) : java.lang.Object {
      var money : entity.BaseMoneyReceived = (promise.BaseMoneyReceived)
return promise.BaseMoneyReceived.Name
    }
    
    // 'title' attribute on Card (id=PromiseDetail) at AgencyBillExecutedPromises.pcf: line 165, column 153
    function title_86 () : java.lang.String {
      return DisplayKey.get("Web.AgencyBillPromises.Executed.PromiseDetail", selectedPromise.BaseMoneyReceived.ReceivedDate.AsUIStyle)
    }
    
    // 'value' attribute on DateInput (id=PromiseReceived_Input) at AgencyBillExecutedPromises.pcf: line 176, column 75
    function valueRoot_54 () : java.lang.Object {
      return selectedPromise.BaseMoneyReceived
    }
    
    // 'value' attribute on DateInput (id=PaymentDate_Input) at AgencyBillExecutedPromises.pcf: line 194, column 94
    function valueRoot_62 () : java.lang.Object {
      return selectedPromise.PaymentAppliedTo.BaseMoneyReceived
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Distributed_Input) at AgencyBillExecutedPromises.pcf: line 206, column 92
    function valueRoot_69 () : java.lang.Object {
      return selectedPromise.PaymentAppliedTo
    }
    
    // 'value' attribute on RowIterator (id=PromisesIterator) at AgencyBillExecutedPromises.pcf: line 77, column 93
    function value_50 () : gw.api.database.IQueryBeanResult<entity.AgencyCyclePromise> {
      return producer.ExecutedPromises
    }
    
    // 'value' attribute on DateInput (id=PromiseReceived_Input) at AgencyBillExecutedPromises.pcf: line 176, column 75
    function value_53 () : java.util.Date {
      return selectedPromise.BaseMoneyReceived.ReceivedDate
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AgencyBillExecutedPromises.pcf: line 180, column 67
    function value_56 () : java.lang.String {
      return selectedPromise.BaseMoneyReceived.Name
    }
    
    // 'value' attribute on DateInput (id=PaymentDate_Input) at AgencyBillExecutedPromises.pcf: line 194, column 94
    function value_61 () : java.util.Date {
      return selectedPromise.PaymentAppliedTo.BaseMoneyReceived.ReceivedDate
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyBillExecutedPromises.pcf: line 200, column 88
    function value_64 () : gw.pl.currency.MonetaryAmount {
      return selectedPromise.PaymentAppliedTo.BaseMoneyReceived.Amount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Distributed_Input) at AgencyBillExecutedPromises.pcf: line 206, column 92
    function value_68 () : gw.pl.currency.MonetaryAmount {
      return selectedPromise.PaymentAppliedTo.NetDistributedToInvoiceItems
    }
    
    // 'value' attribute on MonetaryAmountInput (id=InSuspense_Input) at AgencyBillExecutedPromises.pcf: line 212, column 77
    function value_72 () : gw.pl.currency.MonetaryAmount {
      return selectedPromise.PaymentAppliedTo.NetInSuspense
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ProducerWriteOff_Input) at AgencyBillExecutedPromises.pcf: line 218, column 86
    function value_76 () : gw.pl.currency.MonetaryAmount {
      return selectedPromise.PaymentAppliedTo.ProducerWriteoffAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ItemWriteOff_Input) at AgencyBillExecutedPromises.pcf: line 224, column 82
    function value_80 () : gw.pl.currency.MonetaryAmount {
      return selectedPromise.PaymentAppliedTo.ItemWriteOffAmount
    }
    
    property get selectedPromise () : AgencyCyclePromise {
      return getCurrentSelection(1) as AgencyCyclePromise
    }
    
    property set selectedPromise ($arg :  AgencyCyclePromise) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  
}