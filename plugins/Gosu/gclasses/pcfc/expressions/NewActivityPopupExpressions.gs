package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/activity/NewActivityPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewActivityPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/activity/NewActivityPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewActivityPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (activityPattern :  ActivityPattern, isShared :  Boolean, troubleTicket :  TroubleTicket, account :  Account, policyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=linkDoc) at NewActivityPopup.pcf: line 44, column 106
    function action_3 () : void {
      OnBaseDocumentListPopup.push(activity, acc.onbase.configuration.DocumentLinkType.activityid, activity.Description as String, acc.onbase.api.application.DocumentLinking.getRelatedFromTroubleTicket(troubleTicket))
    }
    
    // 'action' attribute on ToolbarButton (id=linkDoc) at NewActivityPopup.pcf: line 44, column 106
    function action_dest_4 () : pcf.api.Destination {
      return pcf.OnBaseDocumentListPopup.createDestination(activity, acc.onbase.configuration.DocumentLinkType.activityid, activity.Description as String, acc.onbase.api.application.DocumentLinking.getRelatedFromTroubleTicket(troubleTicket))
    }
    
    // 'afterCommit' attribute on Popup (id=NewActivityPopup) at NewActivityPopup.pcf: line 11, column 63
    function afterCommit_9 (TopLocation :  pcf.api.Location) : void {
      gw.api.web.activity.ActivityUtil.markActivityAsViewed(activity)
    }
    
    // 'beforeCommit' attribute on Popup (id=NewActivityPopup) at NewActivityPopup.pcf: line 11, column 63
    function beforeCommit_10 (pickedValue :  java.lang.Object) : void {
      gw.acc.activityenhancement.DesktopQueuedActivitiesUtil.assignActivity(assigneeHolder[0] , activity)
    }
    
    // 'def' attribute on PanelRef at NewActivityPopup.pcf: line 47, column 59
    function def_onEnter_5 (def :  pcf.ActivityDetailDV_default) : void {
      def.onEnter(activity, assigneeHolder)
    }
    
    // 'def' attribute on PanelRef at NewActivityPopup.pcf: line 47, column 59
    function def_onEnter_7 (def :  pcf.ActivityDetailDV_disbappractivity) : void {
      def.onEnter(activity, assigneeHolder)
    }
    
    // 'def' attribute on PanelRef at NewActivityPopup.pcf: line 47, column 59
    function def_refreshVariables_6 (def :  pcf.ActivityDetailDV_default) : void {
      def.refreshVariables(activity, assigneeHolder)
    }
    
    // 'def' attribute on PanelRef at NewActivityPopup.pcf: line 47, column 59
    function def_refreshVariables_8 (def :  pcf.ActivityDetailDV_disbappractivity) : void {
      def.refreshVariables(activity, assigneeHolder)
    }
    
    // 'initialValue' attribute on Variable at NewActivityPopup.pcf: line 26, column 31
    function initialValue_0 () : entity.Activity {
      return createActivity()
    }
    
    // 'initialValue' attribute on Variable at NewActivityPopup.pcf: line 30, column 44
    function initialValue_1 () : gw.api.assignment.Assignee[] {
      return activity.InitialAssigneeForPicker
    }
    
    // EditButtons at NewActivityPopup.pcf: line 40, column 23
    function label_2 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    override property get CurrentLocation () : pcf.NewActivityPopup {
      return super.CurrentLocation as pcf.NewActivityPopup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get activity () : entity.Activity {
      return getVariableValue("activity", 0) as entity.Activity
    }
    
    property set activity ($arg :  entity.Activity) {
      setVariableValue("activity", 0, $arg)
    }
    
    property get activityPattern () : ActivityPattern {
      return getVariableValue("activityPattern", 0) as ActivityPattern
    }
    
    property set activityPattern ($arg :  ActivityPattern) {
      setVariableValue("activityPattern", 0, $arg)
    }
    
    property get assigneeHolder () : gw.api.assignment.Assignee[] {
      return getVariableValue("assigneeHolder", 0) as gw.api.assignment.Assignee[]
    }
    
    property set assigneeHolder ($arg :  gw.api.assignment.Assignee[]) {
      setVariableValue("assigneeHolder", 0, $arg)
    }
    
    property get isShared () : Boolean {
      return getVariableValue("isShared", 0) as Boolean
    }
    
    property set isShared ($arg :  Boolean) {
      setVariableValue("isShared", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get troubleTicket () : TroubleTicket {
      return getVariableValue("troubleTicket", 0) as TroubleTicket
    }
    
    property set troubleTicket ($arg :  TroubleTicket) {
      setVariableValue("troubleTicket", 0, $arg)
    }
    
    
    function createActivity() : Activity
          {
            if(isShared)
            {
              if(troubleTicket == null)
              {
                activity = new SharedActivity();
              }
              else
              {
                activity = troubleTicket.createSharedActivity();
              }
            }
            else
            {
              if(troubleTicket == null)
              {
                activity = new Activity();
              }
              else
              {
                activity = troubleTicket.createActivity();
              }
            }
    /**
             * US1186 - Attach Policy Periods to Manually Created Activities
             * 2/19/2015 Alvin Lee
             *
             * Set the policy period on the activity when one is available.
             */
            if (account != null){
              activity.Account = account
            }
            if (policyPeriod != null) {
              activity.PolicyPeriod = policyPeriod
              activity.Account = policyPeriod.Account
            }
            else if (troubleTicket != null && troubleTicket.PolicyNumber != null) {
              // If only 1 policy or policy period is associated with the trouble ticket, use the Policy Number to attach it to the activity
              var policyOrPolicyPeriods = troubleTicket.TicketRelatedEntities.where( \ relatedEntity -> relatedEntity typeis Policy || relatedEntity typeis PolicyPeriod)
              if (policyOrPolicyPeriods.Count == 1) {
                activity.PolicyPeriod = tdic.bc.config.activity.ActivityUtil.findPolicyPeriodFromNumber(troubleTicket.PolicyNumber)
                activity.Account = activity.PolicyPeriod.Account
              }
            }
            else if (troubleTicket != null && troubleTicket.AccountNumber != null) {
                // If only 1 account is associated with the trouble ticket, use the Account Number to attach it to the activity
                var associatedAccounts = troubleTicket.TicketRelatedEntities.where( \ relatedEntity -> relatedEntity typeis Account)
                if (associatedAccounts.Count == 1) {
                  activity.Account = associatedAccounts.first() as Account
                }
              }
            activity.ActivityPattern = activityPattern;
            return activity;
          }
        
    
    
  }
  
  
}