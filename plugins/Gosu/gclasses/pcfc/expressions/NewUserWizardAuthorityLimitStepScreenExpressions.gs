package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/users/NewUserWizardAuthorityLimitStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewUserWizardAuthorityLimitStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/users/NewUserWizardAuthorityLimitStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewUserWizardAuthorityLimitStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewUserWizardAuthorityLimitStepScreen.pcf: line 14, column 42
    function def_onEnter_0 (def :  pcf.UserAuthorityLimitsDV) : void {
      def.onEnter(user)
    }
    
    // 'def' attribute on PanelRef at NewUserWizardAuthorityLimitStepScreen.pcf: line 14, column 42
    function def_refreshVariables_1 (def :  pcf.UserAuthorityLimitsDV) : void {
      def.refreshVariables(user)
    }
    
    property get user () : User {
      return getRequireValue("user", 0) as User
    }
    
    property set user ($arg :  User) {
      setRequireValue("user", 0, $arg)
    }
    
    
  }
  
  
}