package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadImportScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DataUploadImportScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadImportScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DataUploadImportScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=PathToExcelFile_Input) at DataUploadImportScreen.pcf: line 37, column 54
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.RelativePath = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on FileInput (id=xlsFile_Input) at DataUploadImportScreen.pcf: line 43, column 55
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.MyFile = (__VALUE_TO_SET as gw.api.web.WebFile)
    }
    
    // 'value' attribute on BooleanRadioInput (id=IsLoadingFromConfig_Input) at DataUploadImportScreen.pcf: line 28, column 43
    function defaultSetter_3 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.IsLoadingFromConfig = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'label' attribute on BooleanRadioInput (id=IsLoadingFromConfig_Input) at DataUploadImportScreen.pcf: line 28, column 43
    function label_1 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.UploadIsLoadingFromConfig")
    }
    
    // 'label' attribute on FileInput (id=xlsFile_Input) at DataUploadImportScreen.pcf: line 43, column 55
    function label_16 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.ExcelFile")
    }
    
    // 'label' attribute on TextInput (id=PathToExcelFile_Input) at DataUploadImportScreen.pcf: line 37, column 54
    function label_8 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.UploadRelativePathToExcelFile", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.ExcelFile"))
    }
    
    // 'title' attribute on TitleBar at DataUploadImportScreen.pcf: line 19, column 110
    function title_0 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.UploadDescription")
    }
    
    // 'value' attribute on BooleanRadioInput (id=IsLoadingFromConfig_Input) at DataUploadImportScreen.pcf: line 28, column 43
    function valueRoot_4 () : java.lang.Object {
      return processor
    }
    
    // 'value' attribute on FileInput (id=xlsFile_Input) at DataUploadImportScreen.pcf: line 43, column 55
    function value_17 () : gw.api.web.WebFile {
      return processor.MyFile
    }
    
    // 'value' attribute on BooleanRadioInput (id=IsLoadingFromConfig_Input) at DataUploadImportScreen.pcf: line 28, column 43
    function value_2 () : java.lang.Boolean {
      return processor.IsLoadingFromConfig
    }
    
    // 'value' attribute on TextInput (id=PathToExcelFile_Input) at DataUploadImportScreen.pcf: line 37, column 54
    function value_9 () : java.lang.String {
      return processor.RelativePath
    }
    
    // 'visible' attribute on FileInput (id=xlsFile_Input) at DataUploadImportScreen.pcf: line 43, column 55
    function visible_15 () : java.lang.Boolean {
      return !processor.IsLoadingFromConfig
    }
    
    property get processor () : tdic.util.dataloader.processor.BCSampleDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCSampleDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    property get sampleDataImported () : String {
      return getVariableValue("sampleDataImported", 0) as String
    }
    
    property set sampleDataImported ($arg :  String) {
      setVariableValue("sampleDataImported", 0, $arg)
    }
    
    
  }
  
  
}