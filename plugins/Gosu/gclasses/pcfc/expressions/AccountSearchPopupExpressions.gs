package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/AccountSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountSearchPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/search/AccountSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountSearchPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'def' attribute on ScreenRef at AccountSearchPopup.pcf: line 12, column 48
    function def_onEnter_0 (def :  pcf.AccountSearchScreen) : void {
      def.onEnter(false, false)
    }
    
    // 'def' attribute on ScreenRef at AccountSearchPopup.pcf: line 12, column 48
    function def_refreshVariables_1 (def :  pcf.AccountSearchScreen) : void {
      def.refreshVariables(false, false)
    }
    
    override property get CurrentLocation () : pcf.AccountSearchPopup {
      return super.CurrentLocation as pcf.AccountSearchPopup
    }
    
    
  }
  
  
}