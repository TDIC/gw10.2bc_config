package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/entrypoints/AccountEntry.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountEntryEntryPointExpressions {
  @javax.annotation.Generated("config/web/pcf/entrypoints/AccountEntry.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountEntryExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on EntryPoint (id=AccountEntry) at AccountEntry.pcf: line 7, column 53
    function location_0 () : pcf.api.Destination {
      return pcf.AccountSummaryForward.createDestination(accountNumber)
    }
    
    property get accountNumber () : String {
      return getVariableValue("accountNumber", 0) as String
    }
    
    property set accountNumber ($arg :  String) {
      setVariableValue("accountNumber", 0, $arg)
    }
    
    
  }
  
  
}