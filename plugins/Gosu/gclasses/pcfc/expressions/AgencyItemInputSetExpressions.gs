package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyItemInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyItemInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyItemInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyItemInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=GrossAmount_Input) at AgencyItemInputSet.pcf: line 15, column 35
    function currency_2 () : typekey.Currency {
      return invoiceItem.Currency
    }
    
    // 'outputConversion' attribute on TextInput (id=CommissionPercentage_Input) at AgencyItemInputSet.pcf: line 29, column 41
    function outputConversion_8 (VALUE :  java.math.BigDecimal) : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(VALUE, 2)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=GrossAmount_Input) at AgencyItemInputSet.pcf: line 15, column 35
    function valueRoot_1 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on MonetaryAmountInput (id=GrossAmount_Input) at AgencyItemInputSet.pcf: line 15, column 35
    function value_0 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.Amount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=NetAmount_Input) at AgencyItemInputSet.pcf: line 36, column 38
    function value_11 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.NetAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CommissionAmount_Input) at AgencyItemInputSet.pcf: line 22, column 52
    function value_4 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.PrimaryCommissionAmount
    }
    
    // 'value' attribute on TextInput (id=CommissionPercentage_Input) at AgencyItemInputSet.pcf: line 29, column 41
    function value_9 () : java.math.BigDecimal {
      return invoiceItem.PrimaryCommissionAmount.percentageOfAsBigDecimal(invoiceItem.Amount) 
    }
    
    property get invoiceItem () : InvoiceItem {
      return getRequireValue("invoiceItem", 0) as InvoiceItem
    }
    
    property set invoiceItem ($arg :  InvoiceItem) {
      setRequireValue("invoiceItem", 0, $arg)
    }
    
    
  }
  
  
}