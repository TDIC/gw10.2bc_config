package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DisbursementSearchDV.AgencyDisbursement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DisbursementSearchDV_AgencyDisbursementExpressions {
  @javax.annotation.Generated("config/web/pcf/search/DisbursementSearchDV.AgencyDisbursement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DisbursementSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at DisbursementSearchDV.AgencyDisbursement.pcf: line 16, column 94
    function def_onEnter_0 (def :  pcf.DisbursementSearchCriteriaInputSet) : void {
      def.onEnter(searchCriteria, disbursementSubtypeHolder)
    }
    
    // 'def' attribute on InputSetRef at DisbursementSearchDV.AgencyDisbursement.pcf: line 21, column 41
    function def_onEnter_2 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at DisbursementSearchDV.AgencyDisbursement.pcf: line 16, column 94
    function def_refreshVariables_1 (def :  pcf.DisbursementSearchCriteriaInputSet) : void {
      def.refreshVariables(searchCriteria, disbursementSubtypeHolder)
    }
    
    // 'def' attribute on InputSetRef at DisbursementSearchDV.AgencyDisbursement.pcf: line 21, column 41
    function def_refreshVariables_3 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    property get disbursementSubtypeHolder () : typekey.Disbursement[] {
      return getRequireValue("disbursementSubtypeHolder", 0) as typekey.Disbursement[]
    }
    
    property set disbursementSubtypeHolder ($arg :  typekey.Disbursement[]) {
      setRequireValue("disbursementSubtypeHolder", 0, $arg)
    }
    
    property get searchCriteria () : gw.search.DisbSearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.search.DisbSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.DisbSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    
  }
  
  
}