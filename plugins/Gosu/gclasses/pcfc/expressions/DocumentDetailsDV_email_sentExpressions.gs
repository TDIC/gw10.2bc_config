package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/DocumentDetailsDV.email_sent.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DocumentDetailsDV_email_sentExpressions {
  @javax.annotation.Generated("config/web/pcf/document/DocumentDetailsDV.email_sent.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DocumentDetailsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at DocumentDetailsDV.email_sent.pcf: line 46, column 39
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=SecurityType_Input) at DocumentDetailsDV.email_sent.pcf: line 52, column 51
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.SecurityType = (__VALUE_TO_SET as typekey.DocumentSecurityType)
    }
    
    // 'value' attribute on TypeKeyInput (id=Section_Input) at DocumentDetailsDV.email_sent.pcf: line 59, column 70
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Section = (__VALUE_TO_SET as typekey.DocumentSection)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Hidden_Input) at DocumentDetailsDV.email_sent.pcf: line 64, column 36
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      document.Obsolete = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'valueRange' attribute on RangeInput (id=Language_Input) at DocumentDetailsDV.email_sent.pcf: line 38, column 67
    function valueRange_15 () : java.lang.Object {
      return LanguageType.getTypeKeys( false )
    }
    
    // 'value' attribute on TextInput (id=Author_Input) at DocumentDetailsDV.email_sent.pcf: line 18, column 34
    function valueRoot_1 () : java.lang.Object {
      return document
    }
    
    // 'value' attribute on TextInput (id=Author_Input) at DocumentDetailsDV.email_sent.pcf: line 18, column 34
    function value_0 () : java.lang.String {
      return document.Author
    }
    
    // 'value' attribute on RangeInput (id=Language_Input) at DocumentDetailsDV.email_sent.pcf: line 38, column 67
    function value_13 () : typekey.LanguageType {
      return document.Language
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at DocumentDetailsDV.email_sent.pcf: line 46, column 39
    function value_20 () : java.lang.String {
      return document.Description
    }
    
    // 'value' attribute on TypeKeyInput (id=SecurityType_Input) at DocumentDetailsDV.email_sent.pcf: line 52, column 51
    function value_24 () : typekey.DocumentSecurityType {
      return document.SecurityType
    }
    
    // 'value' attribute on TypeKeyInput (id=Section_Input) at DocumentDetailsDV.email_sent.pcf: line 59, column 70
    function value_29 () : typekey.DocumentSection {
      return document.Section
    }
    
    // 'value' attribute on TextInput (id=Recipient_Input) at DocumentDetailsDV.email_sent.pcf: line 22, column 37
    function value_3 () : java.lang.String {
      return document.Recipient
    }
    
    // 'value' attribute on BooleanRadioInput (id=Hidden_Input) at DocumentDetailsDV.email_sent.pcf: line 64, column 36
    function value_34 () : java.lang.Boolean {
      return document.Obsolete
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at DocumentDetailsDV.email_sent.pcf: line 27, column 32
    function value_6 () : java.lang.String {
      return document.Name
    }
    
    // 'value' attribute on DateInput (id=CreateTime_Input) at DocumentDetailsDV.email_sent.pcf: line 31, column 39
    function value_9 () : java.util.Date {
      return document.DateCreated
    }
    
    // 'valueRange' attribute on RangeInput (id=Language_Input) at DocumentDetailsDV.email_sent.pcf: line 38, column 67
    function verifyValueRangeIsAllowedType_16 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Language_Input) at DocumentDetailsDV.email_sent.pcf: line 38, column 67
    function verifyValueRangeIsAllowedType_16 ($$arg :  typekey.LanguageType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Language_Input) at DocumentDetailsDV.email_sent.pcf: line 38, column 67
    function verifyValueRange_17 () : void {
      var __valueRangeArg = LanguageType.getTypeKeys( false )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_16(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeInput (id=Language_Input) at DocumentDetailsDV.email_sent.pcf: line 38, column 67
    function visible_12 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count > 1
    }
    
    // 'visible' attribute on TypeKeyInput (id=Section_Input) at DocumentDetailsDV.email_sent.pcf: line 59, column 70
    function visible_28 () : java.lang.Boolean {
      return DocumentSection.getTypeKeys( false ).Count > 1
    }
    
    property get document () : Document {
      return getRequireValue("document", 0) as Document
    }
    
    property set document ($arg :  Document) {
      setRequireValue("document", 0, $arg)
    }
    
    
  }
  
  
}