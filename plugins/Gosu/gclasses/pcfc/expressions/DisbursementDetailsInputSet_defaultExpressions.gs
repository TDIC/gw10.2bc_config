package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/DisbursementDetailsInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DisbursementDetailsInputSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/disbursement/DisbursementDetailsInputSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DisbursementDetailsInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at DisbursementDetailsInputSet.default.pcf: line 15, column 54
    function action_0 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // MenuItem (id=UserBrowseMenuItem) at DisbursementDetailsInputSet.default.pcf: line 23, column 45
    function action_10 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // MenuItem (id=UserBrowseMenuItem) at DisbursementDetailsInputSet.default.pcf: line 15, column 54
    function action_dest_1 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // MenuItem (id=UserBrowseMenuItem) at DisbursementDetailsInputSet.default.pcf: line 23, column 45
    function action_dest_11 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=amount_Input) at DisbursementDetailsInputSet.default.pcf: line 44, column 36
    function currency_26 () : typekey.Currency {
      return disbursement.Currency
    }
    
    // 'value' attribute on AltUserInput (id=requestedBy_Input) at DisbursementDetailsInputSet.default.pcf: line 15, column 54
    function valueRoot_4 () : java.lang.Object {
      return disbursement
    }
    
    // 'value' attribute on AltUserInput (id=reviewwdBy_Input) at DisbursementDetailsInputSet.default.pcf: line 23, column 45
    function value_12 () : entity.User {
      return disbursement.ReviewedBy_TDIC
    }
    
    // 'value' attribute on TextInput (id=age_Input) at DisbursementDetailsInputSet.default.pcf: line 28, column 41
    function value_15 () : java.math.BigDecimal {
      return disbursement.Age
    }
    
    // 'value' attribute on TypeKeyInput (id=status_Input) at DisbursementDetailsInputSet.default.pcf: line 33, column 47
    function value_18 () : typekey.DisbursementStatus {
      return disbursement.Status
    }
    
    // 'value' attribute on TypeKeyInput (id=reason_Input) at DisbursementDetailsInputSet.default.pcf: line 38, column 35
    function value_21 () : typekey.Reason {
      return disbursement.Reason
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amount_Input) at DisbursementDetailsInputSet.default.pcf: line 44, column 36
    function value_24 () : gw.pl.currency.MonetaryAmount {
      return disbursement.Amount
    }
    
    // 'value' attribute on TypeKeyInput (id=VoidReason_Input) at DisbursementDetailsInputSet.default.pcf: line 50, column 50
    function value_29 () : typekey.VoidReason {
      return disbursement.VoidReason
    }
    
    // 'value' attribute on AltUserInput (id=requestedBy_Input) at DisbursementDetailsInputSet.default.pcf: line 15, column 54
    function value_3 () : entity.User {
      return disbursement.CreateUser
    }
    
    // 'value' attribute on DateInput (id=CreateTime_Input) at DisbursementDetailsInputSet.default.pcf: line 19, column 40
    function value_7 () : java.util.Date {
      return disbursement.CreateTime
    }
    
    // 'visible' attribute on AltUserInput (id=requestedBy_Input) at DisbursementDetailsInputSet.default.pcf: line 15, column 54
    function visible_2 () : java.lang.Boolean {
      return disbursement.Reason != TC_AUTOMATIC
    }
    
    // 'visible' attribute on TypeKeyInput (id=VoidReason_Input) at DisbursementDetailsInputSet.default.pcf: line 50, column 50
    function visible_28 () : java.lang.Boolean {
      return disbursement.VoidReason != null
    }
    
    property get disbursement () : Disbursement {
      return getRequireValue("disbursement", 0) as Disbursement
    }
    
    property set disbursement ($arg :  Disbursement) {
      setRequireValue("disbursement", 0, $arg)
    }
    
    
  }
  
  
}