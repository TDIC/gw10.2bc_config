package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewWriteoffReversalConfirmationScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends NewWriteoffReversalConfirmationScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at NewWriteoffReversalConfirmationScreen.pcf: line 61, column 52
    function currency_19 () : typekey.Currency {
      return chargeWrittenOff.Currency
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at NewWriteoffReversalConfirmationScreen.pcf: line 51, column 61
    function valueRoot_12 () : java.lang.Object {
      return chargeWrittenOff
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at NewWriteoffReversalConfirmationScreen.pcf: line 51, column 61
    function value_11 () : java.lang.String {
      return chargeWrittenOff.LongDisplayName
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at NewWriteoffReversalConfirmationScreen.pcf: line 55, column 61
    function value_14 () : java.util.Date {
      return chargeWrittenOff.TransactionDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at NewWriteoffReversalConfirmationScreen.pcf: line 61, column 52
    function value_17 () : gw.pl.currency.MonetaryAmount {
      return chargeWrittenOff.Amount
    }
    
    property get chargeWrittenOff () : entity.Transaction {
      return getIteratedValue(1) as entity.Transaction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffReversalConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewWriteoffReversalConfirmationScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffReversalConfirmationScreen.pcf: line 82, column 43
    function def_onEnter_29 (def :  pcf.ApprovalActivityDV) : void {
      def.onEnter(approvalActivity)
    }
    
    // 'def' attribute on PanelRef at NewWriteoffReversalConfirmationScreen.pcf: line 82, column 43
    function def_refreshVariables_30 (def :  pcf.ApprovalActivityDV) : void {
      def.refreshVariables(approvalActivity)
    }
    
    // 'value' attribute on TypeKeyInput (id=Reason_Input) at NewWriteoffReversalConfirmationScreen.pcf: line 76, column 55
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      reversal.Reason = (__VALUE_TO_SET as typekey.WriteoffReversalReason)
    }
    
    // 'initialValue' attribute on Variable at NewWriteoffReversalConfirmationScreen.pcf: line 16, column 43
    function initialValue_0 () : entity.WtoffRevApprActivity {
      return reversal.OpenApprovalActivity
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at NewWriteoffReversalConfirmationScreen.pcf: line 61, column 52
    function sortValue_10 (chargeWrittenOff :  entity.Transaction) : java.lang.Object {
      return chargeWrittenOff.Amount
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at NewWriteoffReversalConfirmationScreen.pcf: line 51, column 61
    function sortValue_8 (chargeWrittenOff :  entity.Transaction) : java.lang.Object {
      return chargeWrittenOff.LongDisplayName
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at NewWriteoffReversalConfirmationScreen.pcf: line 55, column 61
    function sortValue_9 (chargeWrittenOff :  entity.Transaction) : java.lang.Object {
      return chargeWrittenOff.TransactionDate
    }
    
    // 'value' attribute on TypeKeyInput (id=Reason_Input) at NewWriteoffReversalConfirmationScreen.pcf: line 76, column 55
    function valueRoot_26 () : java.lang.Object {
      return reversal
    }
    
    // 'value' attribute on DateInput (id=TransactionDate_Input) at NewWriteoffReversalConfirmationScreen.pcf: line 30, column 52
    function valueRoot_3 () : java.lang.Object {
      return reversal.Writeoff
    }
    
    // 'value' attribute on DateInput (id=TransactionDate_Input) at NewWriteoffReversalConfirmationScreen.pcf: line 30, column 52
    function value_2 () : java.util.Date {
      return reversal.Writeoff.ExecutionDate
    }
    
    // 'value' attribute on RowIterator at NewWriteoffReversalConfirmationScreen.pcf: line 46, column 86
    function value_21 () : gw.api.database.IQueryBeanResult<entity.Transaction> {
      return reversal.Writeoff.WriteoffTxns
    }
    
    // 'value' attribute on TextInput (id=NewWriteoff_Input) at NewWriteoffReversalConfirmationScreen.pcf: line 70, column 122
    function value_22 () : java.lang.String {
      return DisplayKey.get("Java.WriteoffReversal.WriteoffReversalName", reversal.Writeoff.DisplayName)
    }
    
    // 'value' attribute on TypeKeyInput (id=Reason_Input) at NewWriteoffReversalConfirmationScreen.pcf: line 76, column 55
    function value_24 () : typekey.WriteoffReversalReason {
      return reversal.Reason
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at NewWriteoffReversalConfirmationScreen.pcf: line 34, column 50
    function value_5 () : java.lang.String {
      return reversal.Writeoff.DisplayName
    }
    
    // 'visible' attribute on AlertBar (id=ApprovalActivityAlertBar) at NewWriteoffReversalConfirmationScreen.pcf: line 23, column 43
    function visible_1 () : java.lang.Boolean {
      return approvalActivity != null
    }
    
    property get approvalActivity () : entity.WtoffRevApprActivity {
      return getVariableValue("approvalActivity", 0) as entity.WtoffRevApprActivity
    }
    
    property set approvalActivity ($arg :  entity.WtoffRevApprActivity) {
      setVariableValue("approvalActivity", 0, $arg)
    }
    
    property get reversal () : WriteoffReversal {
      return getRequireValue("reversal", 0) as WriteoffReversal
    }
    
    property set reversal ($arg :  WriteoffReversal) {
      setRequireValue("reversal", 0, $arg)
    }
    
    
  }
  
  
}