package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketInfoPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TroubleTicketInfoPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketInfoPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TroubleTicketInfoPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (troubleTicket :  TroubleTicket, assigneeHolder :  gw.api.assignment.Assignee[]) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=TroubleTicketInfoPopup_UpdateButton) at TroubleTicketInfoPopup.pcf: line 29, column 62
    function action_0 () : void {
      troubleTicket.assignTo_Ext(assigneeHolder[0]); CurrentLocation.commit()
    }
    
    // 'def' attribute on PanelRef at TroubleTicketInfoPopup.pcf: line 35, column 67
    function def_onEnter_2 (def :  pcf.TroubleTicketInfoDV) : void {
      def.onEnter(troubleTicket, assigneeHolder)
    }
    
    // 'def' attribute on PanelRef at TroubleTicketInfoPopup.pcf: line 35, column 67
    function def_refreshVariables_3 (def :  pcf.TroubleTicketInfoDV) : void {
      def.refreshVariables(troubleTicket, assigneeHolder)
    }
    
    // EditButtons at TroubleTicketInfoPopup.pcf: line 32, column 34
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    override property get CurrentLocation () : pcf.TroubleTicketInfoPopup {
      return super.CurrentLocation as pcf.TroubleTicketInfoPopup
    }
    
    property get assigneeHolder () : gw.api.assignment.Assignee[] {
      return getVariableValue("assigneeHolder", 0) as gw.api.assignment.Assignee[]
    }
    
    property set assigneeHolder ($arg :  gw.api.assignment.Assignee[]) {
      setVariableValue("assigneeHolder", 0, $arg)
    }
    
    property get troubleTicket () : TroubleTicket {
      return getVariableValue("troubleTicket", 0) as TroubleTicket
    }
    
    property set troubleTicket ($arg :  TroubleTicket) {
      setVariableValue("troubleTicket", 0, $arg)
    }
    
    
  }
  
  
}