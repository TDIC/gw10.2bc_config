package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopCreatedActivities.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopCreatedActivitiesExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopCreatedActivities.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopCreatedActivitiesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=DesktopActivities_AssignButton) at DesktopCreatedActivities.pcf: line 42, column 43
    function allCheckedRowsAction_5 (CheckedValues :  Activity[], CheckedValuesErrors :  java.util.Map) : void {
      AssignActivitiesPopup.push(new gw.api.web.activity.ActivityAssignmentPopup(CheckedValues), null)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=DesktopActivities_CompleteButton) at DesktopCreatedActivities.pcf: line 51, column 48
    function allCheckedRowsAction_7 (CheckedValues :  Activity[], CheckedValuesErrors :  java.util.Map) : void {
      gw.api.web.activity.ActivityUtil.completeActivities(CheckedValues); CurrentLocation.startEditing(); CurrentLocation.commit()
    }
    
    // 'canVisit' attribute on Page (id=DesktopCreatedActivities) at DesktopCreatedActivities.pcf: line 9, column 75
    static function canVisit_76 () : java.lang.Boolean {
      return perm.System.actview and perm.System.viewdesktop
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopCreatedActivities.pcf: line 73, column 100
    function filter_10 () : gw.api.filters.IFilter {
      var localLastMonth = lastMonth; return new gw.api.filters.StandardQueryFilter("CompletedInLast30Days", \ qf -> {qf.compare("Status", Equals, ActivityStatus.TC_COMPLETE); qf.compare("CloseDate", GreaterThanOrEquals, localLastMonth)})
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopCreatedActivities.pcf: line 76, column 86
    function filter_12 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("OpenUrgent", \ qf -> {qf.compare("Status", Equals, ActivityStatus.TC_OPEN); qf.compare("Priority", Equals, Priority.TC_URGENT)})
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopCreatedActivities.pcf: line 79, column 91
    function filter_13 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("EscalatedFilter", \ qf -> {qf.compare("Escalated", Equals, Boolean.TRUE)})
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopCreatedActivities.pcf: line 86, column 51
    function filter_14 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("All", \ qf -> {})
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopCreatedActivities.pcf: line 89, column 81
    function filter_16 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("Owned", \ qf -> {qf.compare("AssignedUser", Equals, User.util.CurrentUser)})
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopCreatedActivities.pcf: line 93, column 51
    function filter_17 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("Shared", \ qf -> {qf.compare("Subtype", Equals, typekey.Activity.TC_SHAREDACTIVITY)})
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopCreatedActivities.pcf: line 67, column 83
    function filter_8 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("AllOpen", \ qf -> {qf.compare("Status", Equals, ActivityStatus.TC_OPEN)})
    }
    
    // 'filter' attribute on ToolbarFilterOption at DesktopCreatedActivities.pcf: line 70, column 90
    function filter_9 () : gw.api.filters.IFilter {
      var localWeekStart = weekStart; return new gw.api.filters.StandardQueryFilter("OpenedThisWeek", \ qf -> {qf.compare("Status", Equals, ActivityStatus.TC_OPEN); qf.compare("CreateTime", GreaterThanOrEquals, localWeekStart)})
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at DesktopCreatedActivities.pcf: line 98, column 83
    function filters_19 () : gw.api.filters.IFilter[] {
      return new gw.activity.ActivityPatternFilterSet().FilterOptions
    }
    
    // 'initialValue' attribute on Variable at DesktopCreatedActivities.pcf: line 16, column 64
    function initialValue_0 () : gw.api.database.IQueryBeanResult<Activity> {
      return tdic.bc.config.activity.ActivityUtil.findActivitiesCreatedBy(User.util.CurrentUser)
    }
    
    // 'initialValue' attribute on Variable at DesktopCreatedActivities.pcf: line 20, column 60
    function initialValue_1 () : gw.api.web.activity.DesktopActivityFilterSet {
      return new gw.api.web.activity.DesktopActivityFilterSet()
    }
    
    // 'initialValue' attribute on Variable at DesktopCreatedActivities.pcf: line 24, column 30
    function initialValue_2 () : java.util.Date {
      return com.guidewire.pl.system.util.DateTimeUtil.getWeekStartDate()
    }
    
    // 'initialValue' attribute on Variable at DesktopCreatedActivities.pcf: line 28, column 20
    function initialValue_3 () : Date {
      return gw.api.util.DateUtil.currentDate().addMonths( -1 )
    }
    
    // 'label' attribute on ToolbarFilterOption at DesktopCreatedActivities.pcf: line 73, column 100
    function label_11 () : java.lang.Object {
      return DisplayKey.get("Java.ActivityFilterSet.CompletedInLastNDays", 30)
    }
    
    // Page (id=DesktopCreatedActivities) at DesktopCreatedActivities.pcf: line 9, column 75
    static function parent_77 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'value' attribute on TextCell (id=Escalated_Cell) at DesktopCreatedActivities.pcf: line 123, column 46
    function sortValue_20 (activity :  Activity) : java.lang.Object {
      return activity.Escalated
    }
    
    // 'value' attribute on DateCell (id=CreateDate_Cell) at DesktopCreatedActivities.pcf: line 127, column 44
    function sortValue_21 (activity :  Activity) : java.lang.Object {
      return activity.CreateTime
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at DesktopCreatedActivities.pcf: line 133, column 44
    function sortValue_22 (activity :  Activity) : java.lang.Object {
      return activity.TargetDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at DesktopCreatedActivities.pcf: line 138, column 45
    function sortValue_23 (activity :  Activity) : java.lang.Object {
      return activity.Priority
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DesktopCreatedActivities.pcf: line 143, column 51
    function sortValue_24 (activity :  Activity) : java.lang.Object {
      return activity.Status
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at DesktopCreatedActivities.pcf: line 148, column 41
    function sortValue_25 (activity :  Activity) : java.lang.Object {
      return activity.Subject
    }
    
    // 'value' attribute on TextCell (id=Account_Cell) at DesktopCreatedActivities.pcf: line 153, column 55
    function sortValue_26 (activity :  Activity) : java.lang.Object {
      return activity.Account.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=PolicyPeriod_Cell) at DesktopCreatedActivities.pcf: line 158, column 63
    function sortValue_27 (activity :  Activity) : java.lang.Object {
      return activity.PolicyPeriod.PolicyNumberLong
    }
    
    // 'value' attribute on TextCell (id=AssignedUser_Cell) at DesktopCreatedActivities.pcf: line 163, column 40
    function sortValue_28 (activity :  Activity) : java.lang.Object {
      return activity.AssignedUser
    }
    
    // 'value' attribute on TextCell (id=TroubleTicketNumber_Cell) at DesktopCreatedActivities.pcf: line 168, column 67
    function sortValue_29 (activity :  Activity) : java.lang.Object {
      return activity.TroubleTicket.TroubleTicketNumber
    }
    
    // 'value' attribute on RowIterator at DesktopCreatedActivities.pcf: line 62, column 72
    function value_75 () : gw.api.database.IQueryBeanResult<Activity> {
      return activities
    }
    
    // 'visible' attribute on ToolbarFilterOption at DesktopCreatedActivities.pcf: line 86, column 51
    function visible_15 () : java.lang.Boolean {
      return perm.SharedActivity.view
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=DesktopActivities_AssignButton) at DesktopCreatedActivities.pcf: line 42, column 43
    function visible_4 () : java.lang.Boolean {
      return perm.System.actraown
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=DesktopActivities_CompleteButton) at DesktopCreatedActivities.pcf: line 51, column 48
    function visible_6 () : java.lang.Boolean {
      return perm.System.actapproveany
    }
    
    override property get CurrentLocation () : pcf.DesktopCreatedActivities {
      return super.CurrentLocation as pcf.DesktopCreatedActivities
    }
    
    property get activities () : gw.api.database.IQueryBeanResult<Activity> {
      return getVariableValue("activities", 0) as gw.api.database.IQueryBeanResult<Activity>
    }
    
    property set activities ($arg :  gw.api.database.IQueryBeanResult<Activity>) {
      setVariableValue("activities", 0, $arg)
    }
    
    property get filterSet () : gw.api.web.activity.DesktopActivityFilterSet {
      return getVariableValue("filterSet", 0) as gw.api.web.activity.DesktopActivityFilterSet
    }
    
    property set filterSet ($arg :  gw.api.web.activity.DesktopActivityFilterSet) {
      setVariableValue("filterSet", 0, $arg)
    }
    
    property get lastMonth () : Date {
      return getVariableValue("lastMonth", 0) as Date
    }
    
    property set lastMonth ($arg :  Date) {
      setVariableValue("lastMonth", 0, $arg)
    }
    
    property get weekStart () : java.util.Date {
      return getVariableValue("weekStart", 0) as java.util.Date
    }
    
    property set weekStart ($arg :  java.util.Date) {
      setVariableValue("weekStart", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopCreatedActivities.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DesktopCreatedActivitiesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Subject_Cell) at DesktopCreatedActivities.pcf: line 148, column 41
    function action_53 () : void {
      gw.activity.ActivityMethods.viewActivityDetails(activity)
    }
    
    // 'action' attribute on TextCell (id=Account_Cell) at DesktopCreatedActivities.pcf: line 153, column 55
    function action_57 () : void {
      AccountDetailSummary.go(activity.Account)
    }
    
    // 'action' attribute on TextCell (id=PolicyPeriod_Cell) at DesktopCreatedActivities.pcf: line 158, column 63
    function action_62 () : void {
      PolicyDetailSummary.go(activity.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=TroubleTicketNumber_Cell) at DesktopCreatedActivities.pcf: line 168, column 67
    function action_70 () : void {
      TroubleTicketDetailsPopup.push(activity.TroubleTicket)
    }
    
    // 'action' attribute on TextCell (id=Account_Cell) at DesktopCreatedActivities.pcf: line 153, column 55
    function action_dest_58 () : pcf.api.Destination {
      return pcf.AccountDetailSummary.createDestination(activity.Account)
    }
    
    // 'action' attribute on TextCell (id=PolicyPeriod_Cell) at DesktopCreatedActivities.pcf: line 158, column 63
    function action_dest_63 () : pcf.api.Destination {
      return pcf.PolicyDetailSummary.createDestination(activity.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=TroubleTicketNumber_Cell) at DesktopCreatedActivities.pcf: line 168, column 67
    function action_dest_71 () : pcf.api.Destination {
      return pcf.TroubleTicketDetailsPopup.createDestination(activity.TroubleTicket)
    }
    
    // 'condition' attribute on ToolbarFlag at DesktopCreatedActivities.pcf: line 104, column 39
    function condition_30 () : java.lang.Boolean {
      return activity.canAssign() and !(activity typeis SharedActivity)
    }
    
    // 'condition' attribute on ToolbarFlag at DesktopCreatedActivities.pcf: line 107, column 41
    function condition_31 () : java.lang.Boolean {
      return activity.canComplete()
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at DesktopCreatedActivities.pcf: line 133, column 44
    function fontColor_41 () : java.lang.Object {
      return activity.Overdue == true ? "Red" : ""
    }
    
    // 'value' attribute on TextCell (id=UpdatedSinceLastViewed_Cell) at DesktopCreatedActivities.pcf: line 116, column 46
    function valueRoot_33 () : java.lang.Object {
      return activity
    }
    
    // 'value' attribute on TextCell (id=Account_Cell) at DesktopCreatedActivities.pcf: line 153, column 55
    function valueRoot_60 () : java.lang.Object {
      return activity.Account
    }
    
    // 'value' attribute on TextCell (id=PolicyPeriod_Cell) at DesktopCreatedActivities.pcf: line 158, column 63
    function valueRoot_65 () : java.lang.Object {
      return activity.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=TroubleTicketNumber_Cell) at DesktopCreatedActivities.pcf: line 168, column 67
    function valueRoot_73 () : java.lang.Object {
      return activity.TroubleTicket
    }
    
    // 'value' attribute on TextCell (id=UpdatedSinceLastViewed_Cell) at DesktopCreatedActivities.pcf: line 116, column 46
    function value_32 () : java.lang.Boolean {
      return activity.UpdatedSinceLastViewed
    }
    
    // 'value' attribute on TextCell (id=Escalated_Cell) at DesktopCreatedActivities.pcf: line 123, column 46
    function value_35 () : java.lang.Boolean {
      return activity.Escalated
    }
    
    // 'value' attribute on DateCell (id=CreateDate_Cell) at DesktopCreatedActivities.pcf: line 127, column 44
    function value_38 () : java.util.Date {
      return activity.CreateTime
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at DesktopCreatedActivities.pcf: line 133, column 44
    function value_42 () : java.util.Date {
      return activity.TargetDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at DesktopCreatedActivities.pcf: line 138, column 45
    function value_47 () : typekey.Priority {
      return activity.Priority
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at DesktopCreatedActivities.pcf: line 143, column 51
    function value_50 () : typekey.ActivityStatus {
      return activity.Status
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at DesktopCreatedActivities.pcf: line 148, column 41
    function value_54 () : java.lang.String {
      return activity.Subject
    }
    
    // 'value' attribute on TextCell (id=Account_Cell) at DesktopCreatedActivities.pcf: line 153, column 55
    function value_59 () : java.lang.String {
      return activity.Account.AccountNumber
    }
    
    // 'value' attribute on TextCell (id=PolicyPeriod_Cell) at DesktopCreatedActivities.pcf: line 158, column 63
    function value_64 () : java.lang.String {
      return activity.PolicyPeriod.PolicyNumberLong
    }
    
    // 'value' attribute on TextCell (id=AssignedUser_Cell) at DesktopCreatedActivities.pcf: line 163, column 40
    function value_67 () : entity.User {
      return activity.AssignedUser
    }
    
    // 'value' attribute on TextCell (id=TroubleTicketNumber_Cell) at DesktopCreatedActivities.pcf: line 168, column 67
    function value_72 () : java.lang.String {
      return activity.TroubleTicket.TroubleTicketNumber
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at DesktopCreatedActivities.pcf: line 133, column 44
    function verifyFontColorIsAllowedType_44 ($$arg :  gw.api.web.color.GWColor) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at DesktopCreatedActivities.pcf: line 133, column 44
    function verifyFontColorIsAllowedType_44 ($$arg :  java.lang.String) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at DesktopCreatedActivities.pcf: line 133, column 44
    function verifyFontColor_45 () : void {
      var __fontColorArg = activity.Overdue == true ? "Red" : ""
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the fontColor return type is not a valid type for use with a ValueWidget's fontColor
      // It needs to return a String or a GWColor.
      verifyFontColorIsAllowedType_44(__fontColorArg)
    }
    
    property get activity () : Activity {
      return getIteratedValue(1) as Activity
    }
    
    
  }
  
  
}