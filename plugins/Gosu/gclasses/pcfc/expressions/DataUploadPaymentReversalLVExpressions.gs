package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadPaymentReversalLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DataUploadPaymentReversalLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadPaymentReversalLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DataUploadPaymentReversalLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadPaymentReversalLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on DateCell (id=reversalDate_Cell) at DataUploadPaymentReversalLV.pcf: line 29, column 39
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.PaymentReversal.ReversalDate")
    }
    
    // 'label' attribute on TextCell (id=refnumber_Cell) at DataUploadPaymentReversalLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.PaymentReversal.ReferenceNumber")
    }
    
    // 'label' attribute on TextCell (id=transactionNumber_Cell) at DataUploadPaymentReversalLV.pcf: line 40, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.PaymentReversal.TransactionNumber")
    }
    
    // 'label' attribute on TextCell (id=amount_Cell) at DataUploadPaymentReversalLV.pcf: line 46, column 45
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.PaymentReversal.Amount")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadPaymentReversalLV.pcf: line 23, column 46
    function sortValue_1 (paymentReversal :  tdic.util.dataloader.data.sampledata.PaymentReversalData) : java.lang.Object {
      return processor.getLoadStatus(paymentReversal)
    }
    
    // 'value' attribute on TextCell (id=amount_Cell) at DataUploadPaymentReversalLV.pcf: line 46, column 45
    function sortValue_10 (paymentReversal :  tdic.util.dataloader.data.sampledata.PaymentReversalData) : java.lang.Object {
      return paymentReversal.Amount
    }
    
    // 'value' attribute on DateCell (id=reversalDate_Cell) at DataUploadPaymentReversalLV.pcf: line 29, column 39
    function sortValue_4 (paymentReversal :  tdic.util.dataloader.data.sampledata.PaymentReversalData) : java.lang.Object {
      return paymentReversal.EntryDate
    }
    
    // 'value' attribute on TextCell (id=refnumber_Cell) at DataUploadPaymentReversalLV.pcf: line 35, column 41
    function sortValue_6 (paymentReversal :  tdic.util.dataloader.data.sampledata.PaymentReversalData) : java.lang.Object {
      return paymentReversal.ReferenceNumber
    }
    
    // 'value' attribute on TextCell (id=transactionNumber_Cell) at DataUploadPaymentReversalLV.pcf: line 40, column 41
    function sortValue_8 (paymentReversal :  tdic.util.dataloader.data.sampledata.PaymentReversalData) : java.lang.Object {
      return paymentReversal.TransactionNumber
    }
    
    // 'value' attribute on RowIterator (id=PaymentID) at DataUploadPaymentReversalLV.pcf: line 15, column 103
    function value_37 () : java.util.ArrayList<tdic.util.dataloader.data.sampledata.PaymentReversalData> {
      return processor.PaymentReversalArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadPaymentReversalLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCSampleDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCSampleDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCSampleDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/SampleDataLoader/DataUploadPaymentReversalLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DataUploadPaymentReversalLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at DataUploadPaymentReversalLV.pcf: line 17, column 110
    function highlighted_36 () : java.lang.Boolean {
      return (paymentReversal.Error or paymentReversal.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at DataUploadPaymentReversalLV.pcf: line 23, column 46
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on DateCell (id=reversalDate_Cell) at DataUploadPaymentReversalLV.pcf: line 29, column 39
    function label_16 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.PaymentReversal.ReversalDate")
    }
    
    // 'label' attribute on TextCell (id=refnumber_Cell) at DataUploadPaymentReversalLV.pcf: line 35, column 41
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.PaymentReversal.ReferenceNumber")
    }
    
    // 'label' attribute on TextCell (id=transactionNumber_Cell) at DataUploadPaymentReversalLV.pcf: line 40, column 41
    function label_26 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.PaymentReversal.TransactionNumber")
    }
    
    // 'label' attribute on TextCell (id=amount_Cell) at DataUploadPaymentReversalLV.pcf: line 46, column 45
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.SampleData.PaymentReversal.Amount")
    }
    
    // 'value' attribute on DateCell (id=reversalDate_Cell) at DataUploadPaymentReversalLV.pcf: line 29, column 39
    function valueRoot_18 () : java.lang.Object {
      return paymentReversal
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at DataUploadPaymentReversalLV.pcf: line 23, column 46
    function value_12 () : java.lang.String {
      return processor.getLoadStatus(paymentReversal)
    }
    
    // 'value' attribute on DateCell (id=reversalDate_Cell) at DataUploadPaymentReversalLV.pcf: line 29, column 39
    function value_17 () : java.util.Date {
      return paymentReversal.EntryDate
    }
    
    // 'value' attribute on TextCell (id=refnumber_Cell) at DataUploadPaymentReversalLV.pcf: line 35, column 41
    function value_22 () : java.lang.String {
      return paymentReversal.ReferenceNumber
    }
    
    // 'value' attribute on TextCell (id=transactionNumber_Cell) at DataUploadPaymentReversalLV.pcf: line 40, column 41
    function value_27 () : java.lang.String {
      return paymentReversal.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=amount_Cell) at DataUploadPaymentReversalLV.pcf: line 46, column 45
    function value_32 () : java.math.BigDecimal {
      return paymentReversal.Amount
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at DataUploadPaymentReversalLV.pcf: line 23, column 46
    function visible_13 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get paymentReversal () : tdic.util.dataloader.data.sampledata.PaymentReversalData {
      return getIteratedValue(1) as tdic.util.dataloader.data.sampledata.PaymentReversalData
    }
    
    
  }
  
  
}