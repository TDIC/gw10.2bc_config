package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/PaymentSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentSearchExpressions {
  @javax.annotation.Generated("config/web/pcf/search/PaymentSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentSearchExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=PaymentSearch) at PaymentSearch.pcf: line 8, column 67
    static function canVisit_2 () : java.lang.Boolean {
      return perm.System.viewsearch and perm.System.pmntsearch
    }
    
    // 'def' attribute on ScreenRef at PaymentSearch.pcf: line 10, column 36
    function def_onEnter_0 (def :  pcf.PaymentSearchScreen) : void {
      def.onEnter()
    }
    
    // 'def' attribute on ScreenRef at PaymentSearch.pcf: line 10, column 36
    function def_refreshVariables_1 (def :  pcf.PaymentSearchScreen) : void {
      def.refreshVariables()
    }
    
    // Page (id=PaymentSearch) at PaymentSearch.pcf: line 8, column 67
    static function parent_3 () : pcf.api.Destination {
      return pcf.SearchGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.PaymentSearch {
      return super.CurrentLocation as pcf.PaymentSearch
    }
    
    
  }
  
  
}