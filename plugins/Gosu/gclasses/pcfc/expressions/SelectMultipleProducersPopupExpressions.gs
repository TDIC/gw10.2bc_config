package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultipleProducersPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SelectMultipleProducersPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/SelectMultipleProducersPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SelectMultipleProducersPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'def' attribute on ScreenRef at SelectMultipleProducersPopup.pcf: line 12, column 46
    function def_onEnter_0 (def :  pcf.SelectMultipleProducersScreen) : void {
      def.onEnter()
    }
    
    // 'def' attribute on ScreenRef at SelectMultipleProducersPopup.pcf: line 12, column 46
    function def_refreshVariables_1 (def :  pcf.SelectMultipleProducersScreen) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.SelectMultipleProducersPopup {
      return super.CurrentLocation as pcf.SelectMultipleProducersPopup
    }
    
    
  }
  
  
}