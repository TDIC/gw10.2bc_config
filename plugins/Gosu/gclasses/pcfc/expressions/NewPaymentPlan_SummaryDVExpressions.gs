package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/payment/NewPaymentPlan_SummaryDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewPaymentPlan_SummaryDVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/payment/NewPaymentPlan_SummaryDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends NewPaymentPlan_SummaryDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Override_Cell) at NewPaymentPlan_SummaryDV.pcf: line 104, column 61
    function valueRoot_27 () : java.lang.Object {
      return billingInstructionType
    }
    
    // 'value' attribute on TextCell (id=Override_Cell) at NewPaymentPlan_SummaryDV.pcf: line 104, column 61
    function value_26 () : java.lang.String {
      return billingInstructionType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=HasDownPayment_Cell) at NewPaymentPlan_SummaryDV.pcf: line 108, column 152
    function value_29 () : java.lang.String {
      return getOverridesHasDownPaymentDisplayString(paymentPlanHelper.PaymentPlan.getOverridesFor(billingInstructionType).HasDownPayment)
    }
    
    property get billingInstructionType () : typekey.BillingInstruction {
      return getIteratedValue(1) as typekey.BillingInstruction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/payment/NewPaymentPlan_SummaryDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewPaymentPlan_SummaryDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=FeeAmount_Input) at NewPaymentPlan_SummaryDV.pcf: line 37, column 42
    function currency_9 () : typekey.Currency {
      return paymentPlanHelper.PaymentPlan.getCurrencies()[0]
    }
    
    // 'onChange' attribute on PostOnChange at NewPaymentPlan_SummaryDV.pcf: line 39, column 123
    function onChange_6 () : void {
      paymentPlanHelper.PaymentPlan.setInstallmentFeeForCurrency(installmentFee.Currency, installmentFee)
    }
    
    // 'value' attribute on TextCell (id=Override_Cell) at NewPaymentPlan_SummaryDV.pcf: line 104, column 61
    function sortValue_24 (billingInstructionType :  typekey.BillingInstruction) : java.lang.Object {
      return billingInstructionType.DisplayName
    }
    
    // 'value' attribute on TextCell (id=HasDownPayment_Cell) at NewPaymentPlan_SummaryDV.pcf: line 108, column 152
    function sortValue_25 (billingInstructionType :  typekey.BillingInstruction) : java.lang.Object {
      return getOverridesHasDownPaymentDisplayString(paymentPlanHelper.PaymentPlan.getOverridesFor(billingInstructionType).HasDownPayment)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at NewPaymentPlan_SummaryDV.pcf: line 22, column 52
    function valueRoot_1 () : java.lang.Object {
      return paymentPlanHelper.PaymentPlan
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at NewPaymentPlan_SummaryDV.pcf: line 22, column 52
    function value_0 () : java.lang.String {
      return paymentPlanHelper.PaymentPlan.Name
    }
    
    // 'value' attribute on TypeKeyInput (id=BillDateOrDueDateBilling_Input) at NewPaymentPlan_SummaryDV.pcf: line 52, column 57
    function value_12 () : typekey.BillDateOrDueDateBilling {
      return paymentPlanHelper.PaymentPlan.PolicyLevelBillingBillDateOrDueDateBilling
    }
    
    // 'value' attribute on TypeKeyInput (id=InvoiceItemPlacementCutoffType_Input) at NewPaymentPlan_SummaryDV.pcf: line 65, column 63
    function value_15 () : typekey.InvoiceItemPlacementCutoffType {
      return paymentPlanHelper.PaymentPlan.AccountLevelBillingInvoiceItemPlacementCutoffType
    }
    
    // 'value' attribute on TypeKeyInput (id=InvoicingBlackoutType_Input) at NewPaymentPlan_SummaryDV.pcf: line 78, column 52
    function value_18 () : typekey.InvoicingBlackoutType {
      return paymentPlanHelper.PaymentPlan.InvoicingBlackoutType
    }
    
    // 'value' attribute on TextInput (id=LastInvoiceByDaysBeforePolicyExpiration_Input) at NewPaymentPlan_SummaryDV.pcf: line 84, column 40
    function value_21 () : java.lang.Integer {
      return paymentPlanHelper.PaymentPlan.DaysBeforePolicyExpirationForInvoicingBlackout
    }
    
    // 'value' attribute on TypeKeyInput (id=PaymentInterval_Input) at NewPaymentPlan_SummaryDV.pcf: line 30, column 42
    function value_3 () : typekey.Periodicity {
      return paymentPlanHelper.PaymentPlan.Periodicity
    }
    
    // 'value' attribute on RowIterator at NewPaymentPlan_SummaryDV.pcf: line 99, column 74
    function value_31 () : java.util.List<typekey.BillingInstruction> {
      return paymentPlanHelper.PaymentPlan.getTypesThatCanHaveOverrides().where(\ instructionType -> paymentPlanHelper.PaymentPlan.getOverridesFor(instructionType) != null)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=FeeAmount_Input) at NewPaymentPlan_SummaryDV.pcf: line 37, column 42
    function value_8 () : gw.pl.currency.MonetaryAmount {
      return installmentFee
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=FeeAmount_Input) at NewPaymentPlan_SummaryDV.pcf: line 37, column 42
    function visible_7 () : java.lang.Boolean {
      return installmentFee != null
    }
    
    property get installmentFee () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("installmentFee", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set installmentFee ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("installmentFee", 0, $arg)
    }
    
    property get paymentPlanHelper () : gw.admin.paymentplan.PaymentPlanViewHelper {
      return getRequireValue("paymentPlanHelper", 0) as gw.admin.paymentplan.PaymentPlanViewHelper
    }
    
    property set paymentPlanHelper ($arg :  gw.admin.paymentplan.PaymentPlanViewHelper) {
      setRequireValue("paymentPlanHelper", 0, $arg)
    }
    
    function getOverridesHasDownPaymentDisplayString(b: Boolean): String {
      var result: String
      if (b == null) {
        result = DisplayKey.get("Web.PaymentPlanDetailScreen.SummaryDV.NotOverridden")
      } else if (b == true) {
        result = DisplayKey.get("Web.InstallmentViewHelper.DownPaymentFrequency.Yes")
      } else if (b == false) {
        result = DisplayKey.get("Web.InstallmentViewHelper.DownPaymentFrequency.No")
      }
      return result
    }
    
    
  }
  
  
}