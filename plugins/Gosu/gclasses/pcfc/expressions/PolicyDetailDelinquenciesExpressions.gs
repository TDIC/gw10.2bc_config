package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailDelinquencies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailDelinquenciesExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailDelinquencies.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailDelinquenciesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod, selectedDelinquencyProcessOnEnter :  DelinquencyProcess) : int {
      return 1
    }
    
    // 'canVisit' attribute on Page (id=PolicyDetailDelinquencies) at PolicyDetailDelinquencies.pcf: line 10, column 77
    static function canVisit_2 (plcyPeriod :  PolicyPeriod, selectedDelinquencyProcessOnEnter :  DelinquencyProcess) : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcydelview and not plcyPeriod.Archived
    }
    
    // 'def' attribute on ScreenRef at PolicyDetailDelinquencies.pcf: line 22, column 94
    function def_onEnter_0 (def :  pcf.DelinquencyProcessesDetailScreen) : void {
      def.onEnter(plcyPeriod, selectedDelinquencyProcessOnEnter)
    }
    
    // 'def' attribute on ScreenRef at PolicyDetailDelinquencies.pcf: line 22, column 94
    function def_refreshVariables_1 (def :  pcf.DelinquencyProcessesDetailScreen) : void {
      def.refreshVariables(plcyPeriod, selectedDelinquencyProcessOnEnter)
    }
    
    // Page (id=PolicyDetailDelinquencies) at PolicyDetailDelinquencies.pcf: line 10, column 77
    static function parent_3 (plcyPeriod :  PolicyPeriod, selectedDelinquencyProcessOnEnter :  DelinquencyProcess) : pcf.api.Destination {
      return pcf.PolicyGroup.createDestination(plcyPeriod)
    }
    
    override property get CurrentLocation () : pcf.PolicyDetailDelinquencies {
      return super.CurrentLocation as pcf.PolicyDetailDelinquencies
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 0, $arg)
    }
    
    property get selectedDelinquencyProcessOnEnter () : DelinquencyProcess {
      return getVariableValue("selectedDelinquencyProcessOnEnter", 0) as DelinquencyProcess
    }
    
    property set selectedDelinquencyProcessOnEnter ($arg :  DelinquencyProcess) {
      setVariableValue("selectedDelinquencyProcessOnEnter", 0, $arg)
    }
    
    
  }
  
  
}