package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/AccountDisbursementConfirmDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDisbursementConfirmDVExpressions {
  @javax.annotation.Generated("config/web/pcf/disbursement/AccountDisbursementConfirmDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDisbursementConfirmDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=DisbursementAmount_Input) at AccountDisbursementConfirmDV.pcf: line 29, column 45
    function currency_13 () : typekey.Currency {
      return accountDisbursement.Currency
    }
    
    // 'value' attribute on TextInput (id=Account_Input) at AccountDisbursementConfirmDV.pcf: line 14, column 67
    function valueRoot_1 () : java.lang.Object {
      return accountDisbursement.Account
    }
    
    // 'value' attribute on TextInput (id=UnappliedFund_Input) at AccountDisbursementConfirmDV.pcf: line 24, column 75
    function valueRoot_8 () : java.lang.Object {
      return accountDisbursement
    }
    
    // 'value' attribute on TextInput (id=Account_Input) at AccountDisbursementConfirmDV.pcf: line 14, column 67
    function value_0 () : java.lang.String {
      return accountDisbursement.Account.AccountNameLocalized
    }
    
    // 'value' attribute on MonetaryAmountInput (id=DisbursementAmount_Input) at AccountDisbursementConfirmDV.pcf: line 29, column 45
    function value_11 () : gw.pl.currency.MonetaryAmount {
      return accountDisbursement.Amount
    }
    
    // 'value' attribute on DateInput (id=DueDate_Input) at AccountDisbursementConfirmDV.pcf: line 33, column 46
    function value_15 () : java.util.Date {
      return accountDisbursement.DueDate
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at AccountDisbursementConfirmDV.pcf: line 18, column 60
    function value_3 () : java.lang.String {
      return accountDisbursement.Account.AccountNumber
    }
    
    // 'value' attribute on TextInput (id=UnappliedFund_Input) at AccountDisbursementConfirmDV.pcf: line 24, column 75
    function value_7 () : entity.UnappliedFund {
      return accountDisbursement.UnappliedFund
    }
    
    // 'visible' attribute on TextInput (id=UnappliedFund_Input) at AccountDisbursementConfirmDV.pcf: line 24, column 75
    function visible_6 () : java.lang.Boolean {
      return accountDisbursement.Account.HasDesignatedUnappliedFund
    }
    
    property get accountDisbursement () : AccountDisbursement {
      return getRequireValue("accountDisbursement", 0) as AccountDisbursement
    }
    
    property set accountDisbursement ($arg :  AccountDisbursement) {
      setRequireValue("accountDisbursement", 0, $arg)
    }
    
    
  }
  
  
}