package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/usersandsecurity/UserSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class UserSearchExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/usersandsecurity/UserSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class UserSearchExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=UserSearch) at UserSearch.pcf: line 8, column 62
    static function canVisit_2 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.useradmin and perm.System.usersearch
    }
    
    // 'def' attribute on ScreenRef at UserSearch.pcf: line 12, column 33
    function def_onEnter_0 (def :  pcf.UserSearchScreen) : void {
      def.onEnter()
    }
    
    // 'def' attribute on ScreenRef at UserSearch.pcf: line 12, column 33
    function def_refreshVariables_1 (def :  pcf.UserSearchScreen) : void {
      def.refreshVariables()
    }
    
    // Page (id=UserSearch) at UserSearch.pcf: line 8, column 62
    static function parent_3 () : pcf.api.Destination {
      return pcf.UsersAndSecurity.createDestination()
    }
    
    override property get CurrentLocation () : pcf.UserSearch {
      return super.CurrentLocation as pcf.UserSearch
    }
    
    
  }
  
  
}