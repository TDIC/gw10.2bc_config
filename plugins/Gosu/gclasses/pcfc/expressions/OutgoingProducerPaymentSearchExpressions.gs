package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/OutgoingProducerPaymentSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class OutgoingProducerPaymentSearchExpressions {
  @javax.annotation.Generated("config/web/pcf/search/OutgoingProducerPaymentSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class OutgoingProducerPaymentSearchExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=OutgoingProducerPaymentSearch) at OutgoingProducerPaymentSearch.pcf: line 8, column 83
    static function canVisit_2 () : java.lang.Boolean {
      return false//perm.System.viewsearch and perm.System.outpmntsearch
    }
    
    // 'def' attribute on ScreenRef at OutgoingProducerPaymentSearch.pcf: line 10, column 52
    function def_onEnter_0 (def :  pcf.OutgoingProducerPaymentSearchScreen) : void {
      def.onEnter()
    }
    
    // 'def' attribute on ScreenRef at OutgoingProducerPaymentSearch.pcf: line 10, column 52
    function def_refreshVariables_1 (def :  pcf.OutgoingProducerPaymentSearchScreen) : void {
      def.refreshVariables()
    }
    
    // Page (id=OutgoingProducerPaymentSearch) at OutgoingProducerPaymentSearch.pcf: line 8, column 83
    static function parent_3 () : pcf.api.Destination {
      return pcf.SearchGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.OutgoingProducerPaymentSearch {
      return super.CurrentLocation as pcf.OutgoingProducerPaymentSearch
    }
    
    
  }
  
  
}