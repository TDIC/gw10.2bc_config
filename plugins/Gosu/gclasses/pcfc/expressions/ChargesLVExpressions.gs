package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.database.Query
uses gw.api.util.StringUtil
uses gw.api.financials.MonetaryAmounts
uses gw.api.web.accounting.ChargeView
uses gw.pl.currency.MonetaryAmount
@javax.annotation.Generated("config/web/pcf/accounting/ChargesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChargesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/accounting/ChargesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChargesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'editable' attribute on RowIterator at ChargesLV.pcf: line 55, column 35
    function editable_46 () : java.lang.Boolean {
      return field != NONE
    }
    
    // 'initialValue' attribute on Variable at ChargesLV.pcf: line 29, column 19
    function initialValue_0 () : int {
      return 0
    }
    
    // 'initialValue' attribute on Variable at ChargesLV.pcf: line 33, column 19
    function initialValue_1 () : int {
      return 1
    }
    
    // 'initialValue' attribute on Variable at ChargesLV.pcf: line 37, column 19
    function initialValue_2 () : int {
      return 2
    }
    
    // 'initialValue' attribute on Variable at ChargesLV.pcf: line 41, column 30
    function initialValue_3 () : java.util.Date {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'initialValue' attribute on Variable at ChargesLV.pcf: line 48, column 75
    function initialValue_4 () : java.util.Map<entity.Charge, entity.InitialChargeTxn> {
      return new java.util.HashMap<Charge, entity.InitialChargeTxn>().toAutoMap(\ c -> c.ChargeInitialTxn)
    }
    
    // 'value' attribute on TextCell (id=ChargeContext_Cell) at ChargesLV.pcf: line 115, column 50
    function sortValue_10 (charge :  entity.Charge) : java.lang.Object {
      return charge.BillingInstruction
    }
    
    // 'value' attribute on TextCell (id=ChargeGroup_Cell) at ChargesLV.pcf: line 119, column 39
    function sortValue_11 (charge :  entity.Charge) : java.lang.Object {
      return charge.ChargeGroup
    }
    
    // 'value' attribute on RangeCell (id=ChargeHoldStatus_Cell) at ChargesLV.pcf: line 128, column 36
    function sortValue_12 (charge :  entity.Charge) : java.lang.Object {
      return charge.HoldStatus
    }
    
    // 'value' attribute on DateCell (id=HoldReleaseDate_Cell) at ChargesLV.pcf: line 138, column 36
    function sortValue_14 (charge :  entity.Charge) : java.lang.Object {
      return charge.HoldReleaseDate
    }
    
    // 'value' attribute on TextCell (id=ChargePolicyPeriod_Cell) at ChargesLV.pcf: line 148, column 25
    function sortValue_16 (charge :  entity.Charge) : java.lang.Object {
      var chargeView : gw.api.web.accounting.ChargeView = (new gw.api.web.accounting.ChargeView(charge))
var policyPeriod : entity.PolicyPeriod = (chargeView.PolicyPeriod)
var primaryProducerCode : entity.ProducerCode = (chargeView.PrimaryProducerCode)
var isCommissionEditable : boolean = (chargeView.CommissionEditable)
return policyPeriod
    }
    
    // 'value' attribute on TextCell (id=ClaimID_Cell) at ChargesLV.pcf: line 161, column 25
    function sortValue_18 (charge :  entity.Charge) : java.lang.Object {
      return charge.ClaimID_TDIC
    }
    
    // 'value' attribute on TextCell (id=PrimaryProducer_Cell) at ChargesLV.pcf: line 168, column 64
    function sortValue_19 (charge :  entity.Charge) : java.lang.Object {
      var chargeView : gw.api.web.accounting.ChargeView = (new gw.api.web.accounting.ChargeView(charge))
var policyPeriod : entity.PolicyPeriod = (chargeView.PolicyPeriod)
var primaryProducerCode : entity.ProducerCode = (chargeView.PrimaryProducerCode)
var isCommissionEditable : boolean = (chargeView.CommissionEditable)
return primaryProducerCode
    }
    
    // 'value' attribute on TextCell (id=Commission_Cell) at ChargesLV.pcf: line 173, column 36
    function sortValue_21 (charge :  entity.Charge) : java.lang.Object {
      var chargeView : gw.api.web.accounting.ChargeView = (new gw.api.web.accounting.ChargeView(charge))
var policyPeriod : entity.PolicyPeriod = (chargeView.PolicyPeriod)
var primaryProducerCode : entity.ProducerCode = (chargeView.PrimaryProducerCode)
var isCommissionEditable : boolean = (chargeView.CommissionEditable)
return getCommission(chargeView)
    }
    
    // 'value' attribute on TextCell (id=CommissionRate_Cell) at ChargesLV.pcf: line 180, column 47
    function sortValue_23 (charge :  entity.Charge) : java.lang.Object {
      var chargeView : gw.api.web.accounting.ChargeView = (new gw.api.web.accounting.ChargeView(charge))
var policyPeriod : entity.PolicyPeriod = (chargeView.PolicyPeriod)
var primaryProducerCode : entity.ProducerCode = (chargeView.PrimaryProducerCode)
var isCommissionEditable : boolean = (chargeView.CommissionEditable)
return chargeView.getBaseCommissionRate(PolicyRole.TC_PRIMARY)
    }
    
    // 'value' attribute on TextCell (id=CommissionRateOverride_Cell) at ChargesLV.pcf: line 188, column 46
    function sortValue_25 (charge :  entity.Charge) : java.lang.Object {
      var chargeView : gw.api.web.accounting.ChargeView = (new gw.api.web.accounting.ChargeView(charge))
var policyPeriod : entity.PolicyPeriod = (chargeView.PolicyPeriod)
var primaryProducerCode : entity.ProducerCode = (chargeView.PrimaryProducerCode)
var isCommissionEditable : boolean = (chargeView.CommissionEditable)
return chargeView.PrimaryCommissionRateOverride
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionAmountOverride_Cell) at ChargesLV.pcf: line 199, column 46
    function sortValue_27 (charge :  entity.Charge) : java.lang.Object {
      var chargeView : gw.api.web.accounting.ChargeView = (new gw.api.web.accounting.ChargeView(charge))
var policyPeriod : entity.PolicyPeriod = (chargeView.PolicyPeriod)
var primaryProducerCode : entity.ProducerCode = (chargeView.PrimaryProducerCode)
var isCommissionEditable : boolean = (chargeView.CommissionEditable)
return chargeView.PrimaryCommissionAmountOverride
    }
    
    // 'value' attribute on TextCell (id=SecondaryCommission_Cell) at ChargesLV.pcf: line 207, column 36
    function sortValue_29 (charge :  entity.Charge) : java.lang.Object {
      var chargeView : gw.api.web.accounting.ChargeView = (new gw.api.web.accounting.ChargeView(charge))
var policyPeriod : entity.PolicyPeriod = (chargeView.PolicyPeriod)
var primaryProducerCode : entity.ProducerCode = (chargeView.PrimaryProducerCode)
var isCommissionEditable : boolean = (chargeView.CommissionEditable)
return getSecondaryCommission(chargeView)
    }
    
    // 'value' attribute on TextCell (id=SecondaryCommissionRate_Cell) at ChargesLV.pcf: line 214, column 47
    function sortValue_31 (charge :  entity.Charge) : java.lang.Object {
      var chargeView : gw.api.web.accounting.ChargeView = (new gw.api.web.accounting.ChargeView(charge))
var policyPeriod : entity.PolicyPeriod = (chargeView.PolicyPeriod)
var primaryProducerCode : entity.ProducerCode = (chargeView.PrimaryProducerCode)
var isCommissionEditable : boolean = (chargeView.CommissionEditable)
return chargeView.getBaseCommissionRate(PolicyRole.TC_SECONDARY)
    }
    
    // 'value' attribute on TextCell (id=SecondaryCommissionRateOverride_Cell) at ChargesLV.pcf: line 222, column 46
    function sortValue_33 (charge :  entity.Charge) : java.lang.Object {
      var chargeView : gw.api.web.accounting.ChargeView = (new gw.api.web.accounting.ChargeView(charge))
var policyPeriod : entity.PolicyPeriod = (chargeView.PolicyPeriod)
var primaryProducerCode : entity.ProducerCode = (chargeView.PrimaryProducerCode)
var isCommissionEditable : boolean = (chargeView.CommissionEditable)
return chargeView.SecondaryCommissionRateOverride
    }
    
    // 'value' attribute on MonetaryAmountCell (id=SecondaryCommissionAmountOverride_Cell) at ChargesLV.pcf: line 233, column 46
    function sortValue_35 (charge :  entity.Charge) : java.lang.Object {
      var chargeView : gw.api.web.accounting.ChargeView = (new gw.api.web.accounting.ChargeView(charge))
var policyPeriod : entity.PolicyPeriod = (chargeView.PolicyPeriod)
var primaryProducerCode : entity.ProducerCode = (chargeView.PrimaryProducerCode)
var isCommissionEditable : boolean = (chargeView.CommissionEditable)
return chargeView.SecondaryCommissionAmountOverride
    }
    
    // 'value' attribute on TextCell (id=ReferrerCommission_Cell) at ChargesLV.pcf: line 241, column 36
    function sortValue_37 (charge :  entity.Charge) : java.lang.Object {
      var chargeView : gw.api.web.accounting.ChargeView = (new gw.api.web.accounting.ChargeView(charge))
var policyPeriod : entity.PolicyPeriod = (chargeView.PolicyPeriod)
var primaryProducerCode : entity.ProducerCode = (chargeView.PrimaryProducerCode)
var isCommissionEditable : boolean = (chargeView.CommissionEditable)
return getReferrerCommission(chargeView)
    }
    
    // 'value' attribute on TextCell (id=ReferrerCommissionRate_Cell) at ChargesLV.pcf: line 248, column 47
    function sortValue_39 (charge :  entity.Charge) : java.lang.Object {
      var chargeView : gw.api.web.accounting.ChargeView = (new gw.api.web.accounting.ChargeView(charge))
var policyPeriod : entity.PolicyPeriod = (chargeView.PolicyPeriod)
var primaryProducerCode : entity.ProducerCode = (chargeView.PrimaryProducerCode)
var isCommissionEditable : boolean = (chargeView.CommissionEditable)
return chargeView.getBaseCommissionRate(PolicyRole.TC_REFERRER)
    }
    
    // 'value' attribute on TextCell (id=ReferrerCommissionRateOverride_Cell) at ChargesLV.pcf: line 256, column 46
    function sortValue_41 (charge :  entity.Charge) : java.lang.Object {
      var chargeView : gw.api.web.accounting.ChargeView = (new gw.api.web.accounting.ChargeView(charge))
var policyPeriod : entity.PolicyPeriod = (chargeView.PolicyPeriod)
var primaryProducerCode : entity.ProducerCode = (chargeView.PrimaryProducerCode)
var isCommissionEditable : boolean = (chargeView.CommissionEditable)
return chargeView.ReferrerCommissionRateOverride
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ReferrerCommissionAmountOverride_Cell) at ChargesLV.pcf: line 267, column 46
    function sortValue_43 (charge :  entity.Charge) : java.lang.Object {
      var chargeView : gw.api.web.accounting.ChargeView = (new gw.api.web.accounting.ChargeView(charge))
var policyPeriod : entity.PolicyPeriod = (chargeView.PolicyPeriod)
var primaryProducerCode : entity.ProducerCode = (chargeView.PrimaryProducerCode)
var isCommissionEditable : boolean = (chargeView.CommissionEditable)
return chargeView.ReferrerCommissionAmountOverride
    }
    
    // 'value' attribute on MonetaryAmountCell (id=WrittenOffAmount_Cell) at ChargesLV.pcf: line 274, column 51
    function sortValue_45 (charge :  entity.Charge) : java.lang.Object {
      return getChargeWrittenAmount(charge)
    }
    
    // 'value' attribute on DateCell (id=ChargeDate_Cell) at ChargesLV.pcf: line 89, column 38
    function sortValue_5 (charge :  entity.Charge) : java.lang.Object {
      return charge.ChargeDate
    }
    
    // 'value' attribute on TextCell (id=Account_Cell) at ChargesLV.pcf: line 97, column 25
    function sortValue_6 (charge :  entity.Charge) : java.lang.Object {
      var chargeView : gw.api.web.accounting.ChargeView = (new gw.api.web.accounting.ChargeView(charge))
var policyPeriod : entity.PolicyPeriod = (chargeView.PolicyPeriod)
var primaryProducerCode : entity.ProducerCode = (chargeView.PrimaryProducerCode)
var isCommissionEditable : boolean = (chargeView.CommissionEditable)
return chargeView.Account
    }
    
    // 'value' attribute on TextCell (id=DefaultPayer_Cell) at ChargesLV.pcf: line 104, column 25
    function sortValue_8 (charge :  entity.Charge) : java.lang.Object {
      return charge.DefaultPayer
    }
    
    // 'value' attribute on TextCell (id=ChargeType_Cell) at ChargesLV.pcf: line 109, column 38
    function sortValue_9 (charge :  entity.Charge) : java.lang.Object {
      return charge
    }
    
    // 'value' attribute on RowIterator at ChargesLV.pcf: line 55, column 35
    function value_192 () : entity.Charge[] {
      return charges
    }
    
    // 'visible' attribute on RangeCell (id=ChargeHoldStatus_Cell) at ChargesLV.pcf: line 128, column 36
    function visible_13 () : java.lang.Boolean {
      return showHoldColumn
    }
    
    // 'visible' attribute on DateCell (id=HoldReleaseDate_Cell) at ChargesLV.pcf: line 138, column 36
    function visible_15 () : java.lang.Boolean {
      return field == HOLD
    }
    
    // 'visible' attribute on TextCell (id=ChargePolicyPeriod_Cell) at ChargesLV.pcf: line 148, column 25
    function visible_17 () : java.lang.Boolean {
      return showPolicyColumn
    }
    
    // 'visible' attribute on TextCell (id=PrimaryProducer_Cell) at ChargesLV.pcf: line 168, column 64
    function visible_20 () : java.lang.Boolean {
      return field == NONE or field == COMMISSION_RATE
    }
    
    // 'visible' attribute on TextCell (id=Commission_Cell) at ChargesLV.pcf: line 173, column 36
    function visible_22 () : java.lang.Boolean {
      return field == NONE
    }
    
    // 'visible' attribute on TextCell (id=CommissionRate_Cell) at ChargesLV.pcf: line 180, column 47
    function visible_24 () : java.lang.Boolean {
      return field == COMMISSION_RATE
    }
    
    // 'visible' attribute on TextCell (id=Account_Cell) at ChargesLV.pcf: line 97, column 25
    function visible_7 () : java.lang.Boolean {
      return showAccount
    }
    
    property get COMMISSION_RATE () : int {
      return getVariableValue("COMMISSION_RATE", 0) as java.lang.Integer
    }
    
    property set COMMISSION_RATE ($arg :  int) {
      setVariableValue("COMMISSION_RATE", 0, $arg)
    }
    
    property get HOLD () : int {
      return getVariableValue("HOLD", 0) as java.lang.Integer
    }
    
    property set HOLD ($arg :  int) {
      setVariableValue("HOLD", 0, $arg)
    }
    
    property get NONE () : int {
      return getVariableValue("NONE", 0) as java.lang.Integer
    }
    
    property set NONE ($arg :  int) {
      setVariableValue("NONE", 0, $arg)
    }
    
    property get chargeInitialTransactions () : java.util.Map<entity.Charge, entity.InitialChargeTxn> {
      return getVariableValue("chargeInitialTransactions", 0) as java.util.Map<entity.Charge, entity.InitialChargeTxn>
    }
    
    property set chargeInitialTransactions ($arg :  java.util.Map<entity.Charge, entity.InitialChargeTxn>) {
      setVariableValue("chargeInitialTransactions", 0, $arg)
    }
    
    property get charges () : Charge[] {
      return getRequireValue("charges", 0) as Charge[]
    }
    
    property set charges ($arg :  Charge[]) {
      setRequireValue("charges", 0, $arg)
    }
    
    property get currentDate () : java.util.Date {
      return getVariableValue("currentDate", 0) as java.util.Date
    }
    
    property set currentDate ($arg :  java.util.Date) {
      setVariableValue("currentDate", 0, $arg)
    }
    
    property get field () : Number {
      return getRequireValue("field", 0) as Number
    }
    
    property set field ($arg :  Number) {
      setRequireValue("field", 0, $arg)
    }
    
    property get showAccount () : Boolean {
      return getRequireValue("showAccount", 0) as Boolean
    }
    
    property set showAccount ($arg :  Boolean) {
      setRequireValue("showAccount", 0, $arg)
    }
    
    property get showCheckboxes () : Boolean {
      return getRequireValue("showCheckboxes", 0) as Boolean
    }
    
    property set showCheckboxes ($arg :  Boolean) {
      setRequireValue("showCheckboxes", 0, $arg)
    }
    
    property get showHoldColumn () : Boolean {
      return getRequireValue("showHoldColumn", 0) as Boolean
    }
    
    property set showHoldColumn ($arg :  Boolean) {
      setRequireValue("showHoldColumn", 0, $arg)
    }
    
    property get showPolicyColumn () : Boolean {
      return getRequireValue("showPolicyColumn", 0) as Boolean
    }
    
    property set showPolicyColumn ($arg :  Boolean) {
      setRequireValue("showPolicyColumn", 0, $arg)
    }
    
    
    
        function goToPayer(charge : Charge) {
          var payer = charge.DefaultPayer
          if (payer typeis Producer) {
            ProducerDetail.push(payer)
          } else {
            AccountSummary.push(payer as Account)
          }
        }
    
        function getCommission(chargeView : ChargeView) : String {
          if (chargeView.PrimaryCommissionRateOverride != null) {
            return StringUtil.formatPercentagePoints(chargeView.PrimaryCommissionRateOverride, 2)
          }
          if (chargeView.PrimaryCommissionAmountOverride != null) {
            return MonetaryAmounts.render(chargeView.PrimaryCommissionAmountOverride)
          } else if (chargeView.getBaseCommissionRate(PolicyRole.TC_PRIMARY) != null) {
            return StringUtil.formatPercentagePoints(chargeView.getBaseCommissionRate(PolicyRole.TC_PRIMARY), 2)
          } else {
            return null
          }
        }
    
        function getSecondaryCommission(chargeView : ChargeView) : String {
          if (chargeView.SecondaryCommissionRateOverride != null) {
            return StringUtil.formatPercentagePoints(chargeView.SecondaryCommissionRateOverride, 2)
          }
          if (chargeView.SecondaryCommissionAmountOverride != null) {
            return MonetaryAmounts.render(chargeView.SecondaryCommissionAmountOverride)
          } else if (chargeView.getBaseCommissionRate(PolicyRole.TC_SECONDARY) != null) {
            return StringUtil.formatPercentagePoints(chargeView.getBaseCommissionRate(PolicyRole.TC_SECONDARY), 2)
          } else {
            return null
          }
        }
    
        function getReferrerCommission(chargeView : ChargeView) : String {
          if (chargeView.ReferrerCommissionRateOverride != null) {
            return StringUtil.formatPercentagePoints(chargeView.ReferrerCommissionRateOverride, 2)
          }
          if (chargeView.ReferrerCommissionAmountOverride != null) {
            return MonetaryAmounts.render(chargeView.ReferrerCommissionAmountOverride)
          } else if (chargeView.getBaseCommissionRate(PolicyRole.TC_REFERRER) != null) {
            return StringUtil.formatPercentagePoints(chargeView.getBaseCommissionRate(PolicyRole.TC_REFERRER), 2)
          } else {
            return null
          }
        }
    
        //we can use the index of one Billing Instruction to match a Job in PC
        function getJobIndex(offerNumber : String, charge : Charge) : int {
          //Get the PolicyPeriod with the input Offering Number
          var offeringPPeriod = Query.make(PolicyPeriod).compare("PCJobNumber_TDIC", Equals, offerNumber).select().first()
          //Get all the the Billing Instructions of this Policy Period order by creation time (each BillingInstruction has a corresponding Job in BC)
          var offeringBIs = new HashSet<BillingInstruction>(offeringPPeriod.Charges*.BillingInstruction?.toList()).orderBy(\bInstruction -> bInstruction.CreateTime);
          //Get the index of the Charge job between the list of Billing Instructions
          return offeringBIs.indexOf(charge.BillingInstruction)
        }
    
        //Returns Amount corresponding with the Sum of all the Written Off amounts of the input Charge
        function getChargeWrittenAmount(charge : Charge) : MonetaryAmount {
          //Get all the charges in the Invoices Ins of the input Charge
          var chargesInInvoices = charge.AllInvoiceItems*.Charge
          //Get all the ChargesWrittenOff corresponding with the Charges in the input Charge Invoices Items
          var chargesWO = Query.make(ChargeWrittenOff).select().where(\chargeWO -> chargesInInvoices.hasMatch(\chargeInInvoice -> chargeInInvoice == chargeWO.Charge))
          //Sum all the Written Off Amounts
          return chargesWO.sum(\chargeWO -> chargeWO.Amount)
        }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/accounting/ChargesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ChargesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=ChargePolicyPeriod_Cell) at ChargesLV.pcf: line 148, column 25
    function actionAvailable_106 () : java.lang.Boolean {
      return policyPeriod.ViewableByCurrentUser
    }
    
    // 'action' attribute on TextCell (id=ChargePolicyPeriod_Cell) at ChargesLV.pcf: line 148, column 25
    function action_104 () : void {
      PolicySummary.push(policyPeriod)
    }
    
    // 'action' attribute on TextCell (id=PrimaryProducer_Cell) at ChargesLV.pcf: line 168, column 64
    function action_117 () : void {
      ProducerDetailPopup.push(primaryProducerCode.Producer)
    }
    
    // 'action' attribute on ButtonCell (id=TransactionButton_Cell) at ChargesLV.pcf: line 85, column 82
    function action_54 () : void {
      ChargeTransactionDetailsForward.push(charge)
    }
    
    // 'action' attribute on TextCell (id=Account_Cell) at ChargesLV.pcf: line 97, column 25
    function action_59 () : void {
      AccountSummary.push(chargeView.Account)
    }
    
    // 'action' attribute on TextCell (id=DefaultPayer_Cell) at ChargesLV.pcf: line 104, column 25
    function action_65 () : void {
      goToPayer(charge)
    }
    
    // 'action' attribute on TextCell (id=ChargeContext_Cell) at ChargesLV.pcf: line 115, column 50
    function action_71 () : void {
      TDIC_PCPolicyTransaction.push(com.tdic.util.properties.PropertyUtil.getInstance().getProperty("PolicySystemURL"),charge.PolicyPeriod.PCJobNumber_TDIC, getJobIndex(charge.PolicyPeriod.PCJobNumber_TDIC, charge))
    }
    
    // 'action' attribute on TextCell (id=ChargePolicyPeriod_Cell) at ChargesLV.pcf: line 148, column 25
    function action_dest_105 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(policyPeriod)
    }
    
    // 'action' attribute on TextCell (id=PrimaryProducer_Cell) at ChargesLV.pcf: line 168, column 64
    function action_dest_118 () : pcf.api.Destination {
      return pcf.ProducerDetailPopup.createDestination(primaryProducerCode.Producer)
    }
    
    // 'action' attribute on ButtonCell (id=TransactionButton_Cell) at ChargesLV.pcf: line 85, column 82
    function action_dest_55 () : pcf.api.Destination {
      return pcf.ChargeTransactionDetailsForward.createDestination(charge)
    }
    
    // 'action' attribute on TextCell (id=Account_Cell) at ChargesLV.pcf: line 97, column 25
    function action_dest_60 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination(chargeView.Account)
    }
    
    // 'action' attribute on TextCell (id=ChargeContext_Cell) at ChargesLV.pcf: line 115, column 50
    function action_dest_72 () : pcf.api.Destination {
      return pcf.TDIC_PCPolicyTransaction.createDestination(com.tdic.util.properties.PropertyUtil.getInstance().getProperty("PolicySystemURL"),charge.PolicyPeriod.PCJobNumber_TDIC, getJobIndex(charge.PolicyPeriod.PCJobNumber_TDIC, charge))
    }
    
    // 'checkBoxVisible' attribute on RowIterator at ChargesLV.pcf: line 55, column 35
    function checkBoxVisible_190 () : java.lang.Boolean {
      return showCheckboxes
    }
    
    // 'condition' attribute on ToolbarFlag at ChargesLV.pcf: line 74, column 40
    function condition_51 () : java.lang.Boolean {
      return primaryProducerCode != null
    }
    
    // 'condition' attribute on ToolbarFlag at ChargesLV.pcf: line 77, column 38
    function condition_52 () : java.lang.Boolean {
      return isCommissionEditable
    }
    
    // 'condition' attribute on ToolbarFlag at ChargesLV.pcf: line 80, column 30
    function condition_53 () : java.lang.Boolean {
      return charge.canReverse()
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at ChargesLV.pcf: line 156, column 34
    function currency_112 () : typekey.Currency {
      return charge.Currency
    }
    
    // 'value' attribute on TextCell (id=CommissionRateOverride_Cell) at ChargesLV.pcf: line 188, column 46
    function defaultSetter_131 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeView.PrimaryCommissionRateOverride = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionAmountOverride_Cell) at ChargesLV.pcf: line 199, column 46
    function defaultSetter_138 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeView.PrimaryCommissionAmountOverride = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextCell (id=SecondaryCommissionRateOverride_Cell) at ChargesLV.pcf: line 222, column 46
    function defaultSetter_153 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeView.SecondaryCommissionRateOverride = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=SecondaryCommissionAmountOverride_Cell) at ChargesLV.pcf: line 233, column 46
    function defaultSetter_160 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeView.SecondaryCommissionAmountOverride = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextCell (id=ReferrerCommissionRateOverride_Cell) at ChargesLV.pcf: line 256, column 46
    function defaultSetter_175 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeView.ReferrerCommissionRateOverride = (__VALUE_TO_SET as java.math.BigDecimal)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ReferrerCommissionAmountOverride_Cell) at ChargesLV.pcf: line 267, column 46
    function defaultSetter_182 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeView.ReferrerCommissionAmountOverride = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on RangeCell (id=ChargeHoldStatus_Cell) at ChargesLV.pcf: line 128, column 36
    function defaultSetter_82 (__VALUE_TO_SET :  java.lang.Object) : void {
      charge.HoldStatus = (__VALUE_TO_SET as typekey.ChargeHoldStatus)
    }
    
    // 'value' attribute on DateCell (id=HoldReleaseDate_Cell) at ChargesLV.pcf: line 138, column 36
    function defaultSetter_96 (__VALUE_TO_SET :  java.lang.Object) : void {
      charge.HoldReleaseDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'editable' attribute on TextCell (id=CommissionRateOverride_Cell) at ChargesLV.pcf: line 188, column 46
    function editable_129 () : java.lang.Boolean {
      return chargeView.canEditCommission(PolicyRole.TC_PRIMARY, field as int)
    }
    
    // 'editable' attribute on TextCell (id=SecondaryCommissionRateOverride_Cell) at ChargesLV.pcf: line 222, column 46
    function editable_151 () : java.lang.Boolean {
      return chargeView.canEditCommission(PolicyRole.TC_SECONDARY, field as int)
    }
    
    // 'editable' attribute on TextCell (id=ReferrerCommissionRateOverride_Cell) at ChargesLV.pcf: line 256, column 46
    function editable_173 () : java.lang.Boolean {
      return chargeView.canEditCommission(PolicyRole.TC_REFERRER, field as int)
    }
    
    // 'editable' attribute on RangeCell (id=ChargeHoldStatus_Cell) at ChargesLV.pcf: line 128, column 36
    function editable_79 () : java.lang.Boolean {
      return chargeView.canEditHold(field as int)
    }
    
    // 'editable' attribute on DateCell (id=HoldReleaseDate_Cell) at ChargesLV.pcf: line 138, column 36
    function editable_92 () : java.lang.Boolean {
      return chargeView.canEditHoldReleaseDate(field as int)
    }
    
    // 'fontColor' attribute on RangeCell (id=ChargeHoldStatus_Cell) at ChargesLV.pcf: line 128, column 36
    function fontColor_80 () : java.lang.Object {
      return charge.isHeld() ? gw.api.web.color.GWColor.THEME_URGENCY_HIGH : null
    }
    
    // 'fontColor' attribute on DateCell (id=HoldReleaseDate_Cell) at ChargesLV.pcf: line 138, column 36
    function fontColor_93 () : java.lang.Object {
      return charge.Held ? gw.api.web.color.GWColor.THEME_URGENCY_HIGH : null
    }
    
    // 'initialValue' attribute on Variable at ChargesLV.pcf: line 59, column 50
    function initialValue_47 () : gw.api.web.accounting.ChargeView {
      return new gw.api.web.accounting.ChargeView(charge)
    }
    
    // 'initialValue' attribute on Variable at ChargesLV.pcf: line 63, column 37
    function initialValue_48 () : entity.PolicyPeriod {
      return chargeView.PolicyPeriod
    }
    
    // 'initialValue' attribute on Variable at ChargesLV.pcf: line 67, column 37
    function initialValue_49 () : entity.ProducerCode {
      return chargeView.PrimaryProducerCode
    }
    
    // 'initialValue' attribute on Variable at ChargesLV.pcf: line 71, column 25
    function initialValue_50 () : boolean {
      return chargeView.CommissionEditable
    }
    
    // RowIterator at ChargesLV.pcf: line 55, column 35
    function initializeVariables_191 () : void {
        chargeView = new gw.api.web.accounting.ChargeView(charge);
  policyPeriod = chargeView.PolicyPeriod;
  primaryProducerCode = chargeView.PrimaryProducerCode;
  isCommissionEditable = chargeView.CommissionEditable;

    }
    
    // 'outputConversion' attribute on TextCell (id=CommissionRate_Cell) at ChargesLV.pcf: line 180, column 47
    function outputConversion_125 (VALUE :  java.math.BigDecimal) : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(VALUE, 2)
    }
    
    // 'requestValidationExpression' attribute on DateCell (id=HoldReleaseDate_Cell) at ChargesLV.pcf: line 138, column 36
    function requestValidationExpression_94 (VALUE :  java.util.Date) : java.lang.Object {
      return currentDate >= VALUE ? DisplayKey.get("Web.ChargesLV.HoldReleaseDate.Invalid") : null
    }
    
    // 'valueRange' attribute on RangeCell (id=ChargeHoldStatus_Cell) at ChargesLV.pcf: line 128, column 36
    function valueRange_86 () : java.lang.Object {
      return ChargeHoldStatus.getTypeKeys(false).where(\ c -> charge.canBeHeld(c))
    }
    
    // 'value' attribute on DateCell (id=ChargeDate_Cell) at ChargesLV.pcf: line 89, column 38
    function valueRoot_57 () : java.lang.Object {
      return charge
    }
    
    // 'value' attribute on TextCell (id=Account_Cell) at ChargesLV.pcf: line 97, column 25
    function valueRoot_62 () : java.lang.Object {
      return chargeView
    }
    
    // 'value' attribute on TextCell (id=ChargePolicyPeriod_Cell) at ChargesLV.pcf: line 148, column 25
    function value_107 () : entity.PolicyPeriod {
      return policyPeriod
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at ChargesLV.pcf: line 156, column 34
    function value_110 () : gw.pl.currency.MonetaryAmount {
      return charge.Amount
    }
    
    // 'value' attribute on TextCell (id=ClaimID_Cell) at ChargesLV.pcf: line 161, column 25
    function value_114 () : java.lang.String {
      return charge.ClaimID_TDIC
    }
    
    // 'value' attribute on TextCell (id=PrimaryProducer_Cell) at ChargesLV.pcf: line 168, column 64
    function value_119 () : entity.ProducerCode {
      return primaryProducerCode
    }
    
    // 'value' attribute on TextCell (id=Commission_Cell) at ChargesLV.pcf: line 173, column 36
    function value_122 () : java.lang.String {
      return getCommission(chargeView)
    }
    
    // 'value' attribute on TextCell (id=CommissionRate_Cell) at ChargesLV.pcf: line 180, column 47
    function value_126 () : java.math.BigDecimal {
      return chargeView.getBaseCommissionRate(PolicyRole.TC_PRIMARY)
    }
    
    // 'value' attribute on TextCell (id=CommissionRateOverride_Cell) at ChargesLV.pcf: line 188, column 46
    function value_130 () : java.math.BigDecimal {
      return chargeView.PrimaryCommissionRateOverride
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionAmountOverride_Cell) at ChargesLV.pcf: line 199, column 46
    function value_137 () : gw.pl.currency.MonetaryAmount {
      return chargeView.PrimaryCommissionAmountOverride
    }
    
    // 'value' attribute on TextCell (id=SecondaryCommission_Cell) at ChargesLV.pcf: line 207, column 36
    function value_144 () : java.lang.String {
      return getSecondaryCommission(chargeView)
    }
    
    // 'value' attribute on TextCell (id=SecondaryCommissionRate_Cell) at ChargesLV.pcf: line 214, column 47
    function value_148 () : java.math.BigDecimal {
      return chargeView.getBaseCommissionRate(PolicyRole.TC_SECONDARY)
    }
    
    // 'value' attribute on TextCell (id=SecondaryCommissionRateOverride_Cell) at ChargesLV.pcf: line 222, column 46
    function value_152 () : java.math.BigDecimal {
      return chargeView.SecondaryCommissionRateOverride
    }
    
    // 'value' attribute on MonetaryAmountCell (id=SecondaryCommissionAmountOverride_Cell) at ChargesLV.pcf: line 233, column 46
    function value_159 () : gw.pl.currency.MonetaryAmount {
      return chargeView.SecondaryCommissionAmountOverride
    }
    
    // 'value' attribute on TextCell (id=ReferrerCommission_Cell) at ChargesLV.pcf: line 241, column 36
    function value_166 () : java.lang.String {
      return getReferrerCommission(chargeView)
    }
    
    // 'value' attribute on TextCell (id=ReferrerCommissionRate_Cell) at ChargesLV.pcf: line 248, column 47
    function value_170 () : java.math.BigDecimal {
      return chargeView.getBaseCommissionRate(PolicyRole.TC_REFERRER)
    }
    
    // 'value' attribute on TextCell (id=ReferrerCommissionRateOverride_Cell) at ChargesLV.pcf: line 256, column 46
    function value_174 () : java.math.BigDecimal {
      return chargeView.ReferrerCommissionRateOverride
    }
    
    // 'value' attribute on MonetaryAmountCell (id=ReferrerCommissionAmountOverride_Cell) at ChargesLV.pcf: line 267, column 46
    function value_181 () : gw.pl.currency.MonetaryAmount {
      return chargeView.ReferrerCommissionAmountOverride
    }
    
    // 'value' attribute on MonetaryAmountCell (id=WrittenOffAmount_Cell) at ChargesLV.pcf: line 274, column 51
    function value_188 () : gw.pl.currency.MonetaryAmount {
      return getChargeWrittenAmount(charge)
    }
    
    // 'value' attribute on DateCell (id=ChargeDate_Cell) at ChargesLV.pcf: line 89, column 38
    function value_56 () : java.util.Date {
      return charge.ChargeDate
    }
    
    // 'value' attribute on TextCell (id=Account_Cell) at ChargesLV.pcf: line 97, column 25
    function value_61 () : entity.Account {
      return chargeView.Account
    }
    
    // 'value' attribute on TextCell (id=DefaultPayer_Cell) at ChargesLV.pcf: line 104, column 25
    function value_66 () : gw.api.domain.invoice.InvoicePayer {
      return charge.DefaultPayer
    }
    
    // 'value' attribute on TextCell (id=ChargeType_Cell) at ChargesLV.pcf: line 109, column 38
    function value_69 () : entity.Charge {
      return charge
    }
    
    // 'value' attribute on TextCell (id=ChargeContext_Cell) at ChargesLV.pcf: line 115, column 50
    function value_73 () : entity.BillingInstruction {
      return charge.BillingInstruction
    }
    
    // 'value' attribute on TextCell (id=ChargeGroup_Cell) at ChargesLV.pcf: line 119, column 39
    function value_76 () : java.lang.String {
      return charge.ChargeGroup
    }
    
    // 'value' attribute on RangeCell (id=ChargeHoldStatus_Cell) at ChargesLV.pcf: line 128, column 36
    function value_81 () : typekey.ChargeHoldStatus {
      return charge.HoldStatus
    }
    
    // 'value' attribute on DateCell (id=HoldReleaseDate_Cell) at ChargesLV.pcf: line 138, column 36
    function value_95 () : java.util.Date {
      return charge.HoldReleaseDate
    }
    
    // 'fontColor' attribute on RangeCell (id=ChargeHoldStatus_Cell) at ChargesLV.pcf: line 128, column 36
    function verifyFontColorIsAllowedType_84 ($$arg :  gw.api.web.color.GWColor) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on RangeCell (id=ChargeHoldStatus_Cell) at ChargesLV.pcf: line 128, column 36
    function verifyFontColorIsAllowedType_84 ($$arg :  java.lang.String) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=HoldReleaseDate_Cell) at ChargesLV.pcf: line 138, column 36
    function verifyFontColorIsAllowedType_98 ($$arg :  gw.api.web.color.GWColor) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=HoldReleaseDate_Cell) at ChargesLV.pcf: line 138, column 36
    function verifyFontColorIsAllowedType_98 ($$arg :  java.lang.String) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on RangeCell (id=ChargeHoldStatus_Cell) at ChargesLV.pcf: line 128, column 36
    function verifyFontColor_85 () : void {
      var __fontColorArg = charge.isHeld() ? gw.api.web.color.GWColor.THEME_URGENCY_HIGH : null
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the fontColor return type is not a valid type for use with a ValueWidget's fontColor
      // It needs to return a String or a GWColor.
      verifyFontColorIsAllowedType_84(__fontColorArg)
    }
    
    // 'fontColor' attribute on DateCell (id=HoldReleaseDate_Cell) at ChargesLV.pcf: line 138, column 36
    function verifyFontColor_99 () : void {
      var __fontColorArg = charge.Held ? gw.api.web.color.GWColor.THEME_URGENCY_HIGH : null
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the fontColor return type is not a valid type for use with a ValueWidget's fontColor
      // It needs to return a String or a GWColor.
      verifyFontColorIsAllowedType_98(__fontColorArg)
    }
    
    // 'valueRange' attribute on RangeCell (id=ChargeHoldStatus_Cell) at ChargesLV.pcf: line 128, column 36
    function verifyValueRangeIsAllowedType_87 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ChargeHoldStatus_Cell) at ChargesLV.pcf: line 128, column 36
    function verifyValueRangeIsAllowedType_87 ($$arg :  typekey.ChargeHoldStatus[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=ChargeHoldStatus_Cell) at ChargesLV.pcf: line 128, column 36
    function verifyValueRange_88 () : void {
      var __valueRangeArg = ChargeHoldStatus.getTypeKeys(false).where(\ c -> charge.canBeHeld(c))
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_87(__valueRangeArg)
    }
    
    // 'visible' attribute on DateCell (id=HoldReleaseDate_Cell) at ChargesLV.pcf: line 138, column 36
    function visible_101 () : java.lang.Boolean {
      return field == HOLD
    }
    
    // 'valueVisible' attribute on TextCell (id=ChargePolicyPeriod_Cell) at ChargesLV.pcf: line 148, column 25
    function visible_103 () : java.lang.Boolean {
      return policyPeriod != null
    }
    
    // 'visible' attribute on TextCell (id=ChargePolicyPeriod_Cell) at ChargesLV.pcf: line 148, column 25
    function visible_108 () : java.lang.Boolean {
      return showPolicyColumn
    }
    
    // 'visible' attribute on TextCell (id=PrimaryProducer_Cell) at ChargesLV.pcf: line 168, column 64
    function visible_120 () : java.lang.Boolean {
      return field == NONE or field == COMMISSION_RATE
    }
    
    // 'visible' attribute on TextCell (id=Commission_Cell) at ChargesLV.pcf: line 173, column 36
    function visible_123 () : java.lang.Boolean {
      return field == NONE
    }
    
    // 'visible' attribute on TextCell (id=CommissionRate_Cell) at ChargesLV.pcf: line 180, column 47
    function visible_127 () : java.lang.Boolean {
      return field == COMMISSION_RATE
    }
    
    // 'visible' attribute on TextCell (id=Account_Cell) at ChargesLV.pcf: line 97, column 25
    function visible_63 () : java.lang.Boolean {
      return showAccount
    }
    
    // 'visible' attribute on RangeCell (id=ChargeHoldStatus_Cell) at ChargesLV.pcf: line 128, column 36
    function visible_90 () : java.lang.Boolean {
      return showHoldColumn
    }
    
    property get charge () : entity.Charge {
      return getIteratedValue(1) as entity.Charge
    }
    
    property get chargeView () : gw.api.web.accounting.ChargeView {
      return getVariableValue("chargeView", 1) as gw.api.web.accounting.ChargeView
    }
    
    property set chargeView ($arg :  gw.api.web.accounting.ChargeView) {
      setVariableValue("chargeView", 1, $arg)
    }
    
    property get isCommissionEditable () : boolean {
      return getVariableValue("isCommissionEditable", 1) as java.lang.Boolean
    }
    
    property set isCommissionEditable ($arg :  boolean) {
      setVariableValue("isCommissionEditable", 1, $arg)
    }
    
    property get policyPeriod () : entity.PolicyPeriod {
      return getVariableValue("policyPeriod", 1) as entity.PolicyPeriod
    }
    
    property set policyPeriod ($arg :  entity.PolicyPeriod) {
      setVariableValue("policyPeriod", 1, $arg)
    }
    
    property get primaryProducerCode () : entity.ProducerCode {
      return getVariableValue("primaryProducerCode", 1) as entity.ProducerCode
    }
    
    property set primaryProducerCode ($arg :  entity.ProducerCode) {
      setVariableValue("primaryProducerCode", 1, $arg)
    }
    
    
  }
  
  
}