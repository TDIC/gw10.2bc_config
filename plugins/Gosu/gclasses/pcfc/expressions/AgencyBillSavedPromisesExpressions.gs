package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillSavedPromises.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillSavedPromisesExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillSavedPromises.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillSavedPromisesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'canEdit' attribute on Page (id=AgencyBillSavedPromises) at AgencyBillSavedPromises.pcf: line 9, column 76
    function canEdit_65 () : java.lang.Boolean {
      return perm.System.prodpromedit
    }
    
    // 'canVisit' attribute on Page (id=AgencyBillSavedPromises) at AgencyBillSavedPromises.pcf: line 9, column 76
    static function canVisit_66 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodpromview
    }
    
    // Page (id=AgencyBillSavedPromises) at AgencyBillSavedPromises.pcf: line 9, column 76
    static function parent_67 (producer :  Producer) : pcf.api.Destination {
      return pcf.AgencyBillPromises.createDestination(producer)
    }
    
    override property get CurrentLocation () : pcf.AgencyBillSavedPromises {
      return super.CurrentLocation as pcf.AgencyBillSavedPromises
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    function discardPromises(promises : AgencyCyclePromise[]) {
      gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
        promises.each( \ promise -> {
          if (promise.Executed) {
            throw new gw.api.util.DisplayableException(DisplayKey.get("Web.BaseDistEnhancement.Error.CalledOnExecutedDist.Discard"))
          }
          promise = bundle.add(promise)
          promise.remove()
        })
      })
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillSavedPromises.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at AgencyBillSavedPromises.pcf: line 106, column 47
    function action_21 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // MenuItem (id=UserBrowseMenuItem) at AgencyBillSavedPromises.pcf: line 106, column 47
    function action_dest_22 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillSavedPromises.pcf: line 81, column 58
    function condition_12 () : java.lang.Boolean {
      return promise.hasFrozenDistItem()
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillSavedPromises.pcf: line 84, column 32
    function condition_13 () : java.lang.Boolean {
      return promise.Frozen
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillSavedPromises.pcf: line 87, column 39
    function condition_14 () : java.lang.Boolean {
      return promise.SuspDistItemsThatHaveNotBeenReleased.HasElements
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at AgencyBillSavedPromises.pcf: line 119, column 41
    function currency_31 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'initialValue' attribute on Variable at AgencyBillSavedPromises.pcf: line 76, column 50
    function initialValue_11 () : entity.BaseMoneyReceived {
      return promise.BaseMoneyReceived
    }
    
    // RowIterator (id=PromisesIterator) at AgencyBillSavedPromises.pcf: line 72, column 93
    function initializeVariables_45 () : void {
        money = promise.BaseMoneyReceived;

    }
    
    // 'value' attribute on DateCell (id=PromiseReceived_Cell) at AgencyBillSavedPromises.pcf: line 94, column 47
    function valueRoot_16 () : java.lang.Object {
      return money
    }
    
    // 'value' attribute on AltUserCell (id=SavedBy_Cell) at AgencyBillSavedPromises.pcf: line 106, column 47
    function valueRoot_24 () : java.lang.Object {
      return promise
    }
    
    // 'value' attribute on DateCell (id=PromiseReceived_Cell) at AgencyBillSavedPromises.pcf: line 94, column 47
    function value_15 () : java.util.Date {
      return money.ReceivedDate
    }
    
    // 'value' attribute on DateCell (id=SavedDate_Cell) at AgencyBillSavedPromises.pcf: line 100, column 45
    function value_18 () : java.util.Date {
      return money.UpdateTime
    }
    
    // 'value' attribute on AltUserCell (id=SavedBy_Cell) at AgencyBillSavedPromises.pcf: line 106, column 47
    function value_23 () : entity.User {
      return promise.UpdateUser
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AgencyBillSavedPromises.pcf: line 112, column 39
    function value_26 () : java.lang.String {
      return money.Name
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AgencyBillSavedPromises.pcf: line 119, column 41
    function value_29 () : gw.pl.currency.MonetaryAmount {
      return money.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Distributed_Cell) at AgencyBillSavedPromises.pcf: line 126, column 71
    function value_33 () : gw.pl.currency.MonetaryAmount {
      return promise.DistributedAmountForUnexecutedDist
    }
    
    // 'value' attribute on MonetaryAmountCell (id=InSuspense_Cell) at AgencyBillSavedPromises.pcf: line 133, column 68
    function value_37 () : gw.pl.currency.MonetaryAmount {
      return promise.SuspenseAmountForUnexecutedDist
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Remaining_Cell) at AgencyBillSavedPromises.pcf: line 140, column 52
    function value_41 () : gw.pl.currency.MonetaryAmount {
      return promise.RemainingAmount
    }
    
    property get money () : entity.BaseMoneyReceived {
      return getVariableValue("money", 2) as entity.BaseMoneyReceived
    }
    
    property set money ($arg :  entity.BaseMoneyReceived) {
      setVariableValue("money", 2, $arg)
    }
    
    property get promise () : entity.AgencyCyclePromise {
      return getIteratedValue(2) as entity.AgencyCyclePromise
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillSavedPromises.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends AgencyBillSavedPromisesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at AgencyBillSavedPromises.pcf: line 162, column 55
    function action_52 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // MenuItem (id=UserBrowseMenuItem) at AgencyBillSavedPromises.pcf: line 162, column 55
    function action_dest_53 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=Discard) at AgencyBillSavedPromises.pcf: line 60, column 51
    function allCheckedRowsAction_7 (CheckedValues :  entity.AgencyCyclePromise[], CheckedValuesErrors :  java.util.Map) : void {
      discardPromises(CheckedValues)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=EditSaved) at AgencyBillSavedPromises.pcf: line 44, column 102
    function checkedRowAction_4 (promise :  entity.AgencyCyclePromise, CheckedValue :  entity.AgencyCyclePromise) : void {
      AgencyDistributionWizard.go(producer, CheckedValue.BaseMoneyReceived)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=ViewSuspenseItems) at AgencyBillSavedPromises.pcf: line 52, column 29
    function checkedRowAction_5 (promise :  entity.AgencyCyclePromise, CheckedValue :  entity.AgencyCyclePromise) : void {
      AgencySuspenseItemsPopup.push(CheckedValue, false)
    }
    
    // 'def' attribute on PanelRef at AgencyBillSavedPromises.pcf: line 150, column 76
    function def_onEnter_47 (def :  pcf.AgencyBillDistArchiveWarningPanelSet) : void {
      def.onEnter(selectedPromise)
    }
    
    // 'def' attribute on InputSetRef at AgencyBillSavedPromises.pcf: line 170, column 89
    function def_onEnter_60 (def :  pcf.AgencyBillPromises_PromiseApplicationInputSet) : void {
      def.onEnter(selectedPromise)
    }
    
    // 'def' attribute on PanelRef at AgencyBillSavedPromises.pcf: line 174, column 64
    function def_onEnter_62 (def :  pcf.AgencyDistItemsReadOnlyCV) : void {
      def.onEnter(selectedPromise)
    }
    
    // 'def' attribute on PanelRef at AgencyBillSavedPromises.pcf: line 150, column 76
    function def_refreshVariables_48 (def :  pcf.AgencyBillDistArchiveWarningPanelSet) : void {
      def.refreshVariables(selectedPromise)
    }
    
    // 'def' attribute on InputSetRef at AgencyBillSavedPromises.pcf: line 170, column 89
    function def_refreshVariables_61 (def :  pcf.AgencyBillPromises_PromiseApplicationInputSet) : void {
      def.refreshVariables(selectedPromise)
    }
    
    // 'def' attribute on PanelRef at AgencyBillSavedPromises.pcf: line 174, column 64
    function def_refreshVariables_63 (def :  pcf.AgencyDistItemsReadOnlyCV) : void {
      def.refreshVariables(selectedPromise)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillSavedPromises.pcf: line 27, column 127
    function filter_0 () : gw.api.filters.IFilter {
      return new gw.payment.BaseDistReceivedDateFilter(30)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillSavedPromises.pcf: line 30, column 127
    function filter_1 () : gw.api.filters.IFilter {
      return new gw.payment.BaseDistReceivedDateFilter(60)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillSavedPromises.pcf: line 33, column 127
    function filter_2 () : gw.api.filters.IFilter {
      return new gw.payment.BaseDistReceivedDateFilter(90)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillSavedPromises.pcf: line 36, column 120
    function filter_3 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("All", \ qf -> {})
    }
    
    // 'sortBy' attribute on TextCell (id=Name_Cell) at AgencyBillSavedPromises.pcf: line 112, column 39
    function sortValue_10 (promise :  entity.AgencyCyclePromise) : java.lang.Object {
      var money : entity.BaseMoneyReceived = (promise.BaseMoneyReceived)
return promise.BaseMoneyReceived.Name
    }
    
    // 'sortBy' attribute on DateCell (id=PromiseReceived_Cell) at AgencyBillSavedPromises.pcf: line 94, column 47
    function sortValue_8 (promise :  entity.AgencyCyclePromise) : java.lang.Object {
      var money : entity.BaseMoneyReceived = (promise.BaseMoneyReceived)
return promise.BaseMoneyReceived.ReceivedDate
    }
    
    // 'value' attribute on DateCell (id=SavedDate_Cell) at AgencyBillSavedPromises.pcf: line 100, column 45
    function sortValue_9 (promise :  entity.AgencyCyclePromise) : java.lang.Object {
      var money : entity.BaseMoneyReceived = (promise.BaseMoneyReceived)
return money.UpdateTime
    }
    
    // 'title' attribute on Card (id=PromiseDetail) at AgencyBillSavedPromises.pcf: line 148, column 166
    function title_64 () : java.lang.String {
      return DisplayKey.get("Web.AgencyBillPromises.Saved.PromiseDetail", selectedPromise.BaseMoneyReceived.ReceivedDate.format("short"))
    }
    
    // 'value' attribute on DateInput (id=PromiseReceived_Input) at AgencyBillSavedPromises.pcf: line 158, column 75
    function valueRoot_50 () : java.lang.Object {
      return selectedPromise.BaseMoneyReceived
    }
    
    // 'value' attribute on AltUserInput (id=SavedBy_Input) at AgencyBillSavedPromises.pcf: line 162, column 55
    function valueRoot_55 () : java.lang.Object {
      return selectedPromise
    }
    
    // 'value' attribute on RowIterator (id=PromisesIterator) at AgencyBillSavedPromises.pcf: line 72, column 93
    function value_46 () : gw.api.database.IQueryBeanResult<entity.AgencyCyclePromise> {
      return producer.SavedPromises
    }
    
    // 'value' attribute on DateInput (id=PromiseReceived_Input) at AgencyBillSavedPromises.pcf: line 158, column 75
    function value_49 () : java.util.Date {
      return selectedPromise.BaseMoneyReceived.ReceivedDate
    }
    
    // 'value' attribute on AltUserInput (id=SavedBy_Input) at AgencyBillSavedPromises.pcf: line 162, column 55
    function value_54 () : entity.User {
      return selectedPromise.UpdateUser
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AgencyBillSavedPromises.pcf: line 166, column 67
    function value_57 () : java.lang.String {
      return selectedPromise.BaseMoneyReceived.Name
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=Discard) at AgencyBillSavedPromises.pcf: line 60, column 51
    function visible_6 () : java.lang.Boolean {
      return perm.System.prodpromedit
    }
    
    property get selectedPromise () : AgencyCyclePromise {
      return getCurrentSelection(1) as AgencyCyclePromise
    }
    
    property set selectedPromise ($arg :  AgencyCyclePromise) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  
}