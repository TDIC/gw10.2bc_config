package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/SelectCreditItemsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SelectCreditItemsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/SelectCreditItemsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SelectCreditItemsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=Cancel) at SelectCreditItemsScreen.pcf: line 42, column 60
    function action_4 () : void {
      (CurrentLocation as pcf.api.Wizard).cancel()
    }
    
    // 'action' attribute on PickerToolbarButton (id=AddPolicyPeriods) at SelectCreditItemsScreen.pcf: line 53, column 95
    function action_5 () : void {
      SelectMultiplePolicyPeriodsPopup.push(createPolicySearchCriteria())
    }
    
    // 'action' attribute on PickerToolbarButton (id=AddPolicyPeriods) at SelectCreditItemsScreen.pcf: line 53, column 95
    function action_dest_6 () : pcf.api.Destination {
      return pcf.SelectMultiplePolicyPeriodsPopup.createDestination(createPolicySearchCriteria())
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=SelectInvoiceItems) at SelectCreditItemsScreen.pcf: line 38, column 98
    function allCheckedRowsAction_3 (CheckedValues :  entity.InvoiceItem[], CheckedValuesErrors :  java.util.Map) : void {
      selectItems(CheckedValues.toList())
    }
    
    // 'def' attribute on PanelRef at SelectCreditItemsScreen.pcf: line 45, column 48
    function def_onEnter_8 (def :  pcf.AgencyInvoiceItemsLV) : void {
      def.onEnter(invoiceItems)
    }
    
    // 'def' attribute on PanelRef at SelectCreditItemsScreen.pcf: line 45, column 48
    function def_refreshVariables_9 (def :  pcf.AgencyInvoiceItemsLV) : void {
      def.refreshVariables(invoiceItems)
    }
    
    // 'initialValue' attribute on Variable at SelectCreditItemsScreen.pcf: line 16, column 60
    function initialValue_0 () : java.util.HashSet<entity.PolicyPeriod> {
      return new java.util.HashSet<PolicyPeriod>()
    }
    
    // 'initialValue' attribute on Variable at SelectCreditItemsScreen.pcf: line 21, column 36
    function initialValue_1 () : entity.InvoiceItem[] {
      return getNotExactlyPaidItems()
    }
    
    // 'initialValue' attribute on Variable at SelectCreditItemsScreen.pcf: line 25, column 45
    function initialValue_2 () : gw.pl.currency.MonetaryAmount {
      return 0bd.ofCurrency(producer.Currency)
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=AddPolicyPeriods) at SelectCreditItemsScreen.pcf: line 53, column 95
    function onPick_7 (PickedValue :  PolicyPeriod[]) : void {
      currentInvoiceItems.clear(); policyPeriods.addAll( PickedValue.toList() )
    }
    
    property get currentInvoiceItems () : java.util.List<InvoiceItem> {
      return getRequireValue("currentInvoiceItems", 0) as java.util.List<InvoiceItem>
    }
    
    property set currentInvoiceItems ($arg :  java.util.List<InvoiceItem>) {
      setRequireValue("currentInvoiceItems", 0, $arg)
    }
    
    property get disbursement () : AgencyDisbursement {
      return getRequireValue("disbursement", 0) as AgencyDisbursement
    }
    
    property set disbursement ($arg :  AgencyDisbursement) {
      setRequireValue("disbursement", 0, $arg)
    }
    
    property get invoiceItems () : entity.InvoiceItem[] {
      return getVariableValue("invoiceItems", 0) as entity.InvoiceItem[]
    }
    
    property set invoiceItems ($arg :  entity.InvoiceItem[]) {
      setVariableValue("invoiceItems", 0, $arg)
    }
    
    property get payCommissionReference () : boolean[] {
      return getRequireValue("payCommissionReference", 0) as boolean[]
    }
    
    property set payCommissionReference ($arg :  boolean[]) {
      setRequireValue("payCommissionReference", 0, $arg)
    }
    
    property get policyPeriods () : java.util.HashSet<entity.PolicyPeriod> {
      return getVariableValue("policyPeriods", 0) as java.util.HashSet<entity.PolicyPeriod>
    }
    
    property set policyPeriods ($arg :  java.util.HashSet<entity.PolicyPeriod>) {
      setVariableValue("policyPeriods", 0, $arg)
    }
    
    property get producer () : Producer {
      return getRequireValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setRequireValue("producer", 0, $arg)
    }
    
    property get totalNetAmount () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("totalNetAmount", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set totalNetAmount ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("totalNetAmount", 0, $arg)
    }
    
    function selectItems(items : java.util.List<InvoiceItem>){
      var amount = disbursement.computeDisbursementAmount( items, payCommissionReference[0] ).negate()
      if(amount.signum() < 0){
        currentInvoiceItems.addAll(items.toList()); 
        (CurrentLocation as pcf.api.Wizard).next()
      }else{
        throw new gw.api.util.DisplayableException(DisplayKey.get("Web.SelectCreditItemsScreen.InsufficientCredit", amount.render()))
      }
    }
    
    function getNotExactlyPaidItems() : InvoiceItem[]{
      var items = new java.util.ArrayList<InvoiceItem>()
      for ( policy in policyPeriods ) {
        items.addAll( policy.InvoiceItems.where( \ invoiceItem -> !invoiceItem.Settled ).toList() )
      }
      return items.toTypedArray()
    }
    
    function createPolicySearchCriteria() : gw.search.PolicySearchCriteria {
      var criteria = new gw.search.PolicySearchCriteria()
      criteria.Producer = producer
      criteria.BillingMethod = PolicyPeriodBillingMethod.TC_AGENCYBILL
      return criteria
    }
    
    
  }
  
  
}