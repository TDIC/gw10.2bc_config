package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/note/NewNoteOnTroubleTicketPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewNoteOnTroubleTicketPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/note/NewNoteOnTroubleTicketPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewNoteOnTroubleTicketPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (troubleTicket :  TroubleTicket) : int {
      return 0
    }
    
    // 'afterEnter' attribute on Popup (id=NewNoteOnTroubleTicketPopup) at NewNoteOnTroubleTicketPopup.pcf: line 11, column 76
    function afterEnter_3 () : void {
      troubleTicket.addNote(note)
    }
    
    // 'def' attribute on PanelRef at NewNoteOnTroubleTicketPopup.pcf: line 28, column 32
    function def_onEnter_1 (def :  pcf.NewNoteDV) : void {
      def.onEnter(note)
    }
    
    // 'def' attribute on PanelRef at NewNoteOnTroubleTicketPopup.pcf: line 28, column 32
    function def_refreshVariables_2 (def :  pcf.NewNoteDV) : void {
      def.refreshVariables(note)
    }
    
    // 'initialValue' attribute on Variable at NewNoteOnTroubleTicketPopup.pcf: line 20, column 20
    function initialValue_0 () : Note {
      return gw.pcf.note.NoteHelper.createNoteWithCurrentUsersLanguage()
    }
    
    override property get CurrentLocation () : pcf.NewNoteOnTroubleTicketPopup {
      return super.CurrentLocation as pcf.NewNoteOnTroubleTicketPopup
    }
    
    property get note () : Note {
      return getVariableValue("note", 0) as Note
    }
    
    property set note ($arg :  Note) {
      setVariableValue("note", 0, $arg)
    }
    
    property get troubleTicket () : TroubleTicket {
      return getVariableValue("troubleTicket", 0) as TroubleTicket
    }
    
    property set troubleTicket ($arg :  TroubleTicket) {
      setVariableValue("troubleTicket", 0, $arg)
    }
    
    
  }
  
  
}