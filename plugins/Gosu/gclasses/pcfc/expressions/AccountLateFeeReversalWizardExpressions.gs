package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountLateFeeReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountLateFeeReversalWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountLateFeeReversalWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountLateFeeReversalWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Wizard (id=AccountLateFeeReversalWizard) at AccountLateFeeReversalWizard.pcf: line 13, column 23
    function afterCancel_7 () : void {
      AccountOverview.go(account)
    }
    
    // 'afterCancel' attribute on Wizard (id=AccountLateFeeReversalWizard) at AccountLateFeeReversalWizard.pcf: line 13, column 23
    function afterCancel_dest_8 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(account)
    }
    
    // 'afterFinish' attribute on Wizard (id=AccountLateFeeReversalWizard) at AccountLateFeeReversalWizard.pcf: line 13, column 23
    function afterFinish_12 () : void {
      AccountOverview.go(account)
    }
    
    // 'afterFinish' attribute on Wizard (id=AccountLateFeeReversalWizard) at AccountLateFeeReversalWizard.pcf: line 13, column 23
    function afterFinish_dest_13 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(account)
    }
    
    // 'allowNext' attribute on WizardStep (id=Step1) at AccountLateFeeReversalWizard.pcf: line 29, column 96
    function allowNext_1 () : java.lang.Boolean {
      return reversal.Charge != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=AccountLateFeeReversalWizard) at AccountLateFeeReversalWizard.pcf: line 13, column 23
    function beforeCommit_9 (pickedValue :  java.lang.Object) : void {
      reversal.reverse()
    }
    
    // 'initialValue' attribute on Variable at AccountLateFeeReversalWizard.pcf: line 22, column 30
    function initialValue_0 () : ChargeReversal {
      return new ChargeReversal()
    }
    
    // 'onExit' attribute on WizardStep (id=Step1) at AccountLateFeeReversalWizard.pcf: line 29, column 96
    function onExit_2 () : void {
      reversal.initiateApprovalActivityIfUserLacksAuthority()
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountLateFeeReversalWizard.pcf: line 29, column 96
    function screen_onEnter_3 (def :  pcf.NewChargeReversalChargeSelectScreen) : void {
      def.onEnter(reversal, account.ReversibleLateFeeSearchCriteria)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountLateFeeReversalWizard.pcf: line 34, column 96
    function screen_onEnter_5 (def :  pcf.NewChargeReversalConfirmationScreen) : void {
      def.onEnter(reversal)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountLateFeeReversalWizard.pcf: line 29, column 96
    function screen_refreshVariables_4 (def :  pcf.NewChargeReversalChargeSelectScreen) : void {
      def.refreshVariables(reversal, account.ReversibleLateFeeSearchCriteria)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountLateFeeReversalWizard.pcf: line 34, column 96
    function screen_refreshVariables_6 (def :  pcf.NewChargeReversalConfirmationScreen) : void {
      def.refreshVariables(reversal)
    }
    
    // 'tabBar' attribute on Wizard (id=AccountLateFeeReversalWizard) at AccountLateFeeReversalWizard.pcf: line 13, column 23
    function tabBar_onEnter_10 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AccountLateFeeReversalWizard) at AccountLateFeeReversalWizard.pcf: line 13, column 23
    function tabBar_refreshVariables_11 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AccountLateFeeReversalWizard {
      return super.CurrentLocation as pcf.AccountLateFeeReversalWizard
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get reversal () : ChargeReversal {
      return getVariableValue("reversal", 0) as ChargeReversal
    }
    
    property set reversal ($arg :  ChargeReversal) {
      setVariableValue("reversal", 0, $arg)
    }
    
    
  }
  
  
}