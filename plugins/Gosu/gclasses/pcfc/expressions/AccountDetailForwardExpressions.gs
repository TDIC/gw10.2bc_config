package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'action' attribute on ForwardCondition at AccountDetailForward.pcf: line 15, column 45
    function action_0 () : void {
      AccountOverview.go(account)
    }
    
    // 'action' attribute on ForwardCondition at AccountDetailForward.pcf: line 15, column 45
    function action_dest_1 () : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(account)
    }
    
    // 'canVisit' attribute on Forward (id=AccountDetailForward) at AccountDetailForward.pcf: line 8, column 31
    static function canVisit_2 (account :  Account) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctsummview and account.ViewableByCurrentUser
    }
    
    override property get CurrentLocation () : pcf.AccountDetailForward {
      return super.CurrentLocation as pcf.AccountDetailForward
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  
}