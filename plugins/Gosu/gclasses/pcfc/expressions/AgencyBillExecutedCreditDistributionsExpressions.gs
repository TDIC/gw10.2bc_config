package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExecutedCreditDistributions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillExecutedCreditDistributionsExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExecutedCreditDistributions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillExecutedCreditDistributionsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=AgencyBillExecutedCreditDistributions) at AgencyBillExecutedCreditDistributions.pcf: line 8, column 90
    static function canVisit_70 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodpmntview
    }
    
    // Page (id=AgencyBillExecutedCreditDistributions) at AgencyBillExecutedCreditDistributions.pcf: line 8, column 90
    static function parent_71 (producer :  Producer) : pcf.api.Destination {
      return pcf.AgencyBillPayments.createDestination(producer)
    }
    
    override property get CurrentLocation () : pcf.AgencyBillExecutedCreditDistributions {
      return super.CurrentLocation as pcf.AgencyBillExecutedCreditDistributions
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExecutedCreditDistributions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillExecutedCreditDistributions.pcf: line 87, column 32
    function condition_10 () : java.lang.Boolean {
      return creditDistribution.Frozen
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillExecutedCreditDistributions.pcf: line 90, column 47
    function condition_11 () : java.lang.Boolean {
      return not (creditDistribution.BaseMoneyReceived.Reversed or creditDistribution.BaseMoneyReceived.Modified)
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillExecutedCreditDistributions.pcf: line 93, column 34
    function condition_12 () : java.lang.Boolean {
      return creditDistribution.BaseMoneyReceived.Executed
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillExecutedCreditDistributions.pcf: line 96, column 39
    function condition_13 () : java.lang.Boolean {
      return creditDistribution.SuspDistItemsThatHaveNotBeenReleased.HasElements
    }
    
    // 'condition' attribute on ToolbarFlag at AgencyBillExecutedCreditDistributions.pcf: line 84, column 43
    function condition_9 () : java.lang.Boolean {
      return creditDistribution.hasFrozenDistItem()
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Distributed_Cell) at AgencyBillExecutedCreditDistributions.pcf: line 116, column 76
    function currency_22 () : typekey.Currency {
      return producer.Currency
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AgencyBillExecutedCreditDistributions.pcf: line 104, column 78
    function valueRoot_15 () : java.lang.Object {
      return creditDistribution.BaseMoneyReceived
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Distributed_Cell) at AgencyBillExecutedCreditDistributions.pcf: line 116, column 76
    function valueRoot_21 () : java.lang.Object {
      return creditDistribution
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AgencyBillExecutedCreditDistributions.pcf: line 104, column 78
    function value_14 () : java.util.Date {
      return creditDistribution.BaseMoneyReceived.ReceivedDate
    }
    
    // 'value' attribute on BooleanRadioCell (id=Reversed_Cell) at AgencyBillExecutedCreditDistributions.pcf: line 109, column 74
    function value_17 () : java.lang.Boolean {
      return creditDistribution.BaseMoneyReceived.Reversed
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Distributed_Cell) at AgencyBillExecutedCreditDistributions.pcf: line 116, column 76
    function value_20 () : gw.pl.currency.MonetaryAmount {
      return creditDistribution.NetDistributedToInvoiceItems
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Suspense_Cell) at AgencyBillExecutedCreditDistributions.pcf: line 123, column 61
    function value_24 () : gw.pl.currency.MonetaryAmount {
      return creditDistribution.NetInSuspense
    }
    
    // 'value' attribute on MonetaryAmountCell (id=WriteOff_Cell) at AgencyBillExecutedCreditDistributions.pcf: line 130, column 204
    function value_28 () : gw.pl.currency.MonetaryAmount {
      return creditDistribution.ProducerWriteoffAmount == null ? new gw.pl.currency.MonetaryAmount(java.math.BigDecimal.ZERO, producer.Currency) : creditDistribution.ProducerWriteoffAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Total_Cell) at AgencyBillExecutedCreditDistributions.pcf: line 137, column 111
    function value_31 () : gw.pl.currency.MonetaryAmount {
      return creditDistribution.NetDistributedToInvoiceItems + creditDistribution.NetInSuspense
    }
    
    property get creditDistribution () : entity.AgencyCyclePayment {
      return getIteratedValue(2) as entity.AgencyCyclePayment
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExecutedCreditDistributions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends AgencyBillExecutedCreditDistributionsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=Edit) at AgencyBillExecutedCreditDistributions.pcf: line 52, column 29
    function checkedRowAction_5 (creditDistribution :  entity.AgencyCyclePayment, CheckedValue :  entity.AgencyCyclePayment) : void {
      AgencyDistributionWizard.go(producer, CheckedValue.BaseMoneyReceived)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=Reverse) at AgencyBillExecutedCreditDistributions.pcf: line 60, column 29
    function checkedRowAction_6 (creditDistribution :  entity.AgencyCyclePayment, CheckedValue :  entity.AgencyCyclePayment) : void {
      AgencyDistributionReversalConfirmationPopup.push(CheckedValue)
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=ViewSuspenseItems) at AgencyBillExecutedCreditDistributions.pcf: line 68, column 29
    function checkedRowAction_7 (creditDistribution :  entity.AgencyCyclePayment, CheckedValue :  entity.AgencyCyclePayment) : void {
      AgencySuspenseItemsPopup.push(CheckedValue, false)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=DistributedAmount_Input) at AgencyBillExecutedCreditDistributions.pcf: line 180, column 84
    function currency_53 () : typekey.Currency {
      return selectedCreditDistribution.Currency
    }
    
    // 'def' attribute on PanelRef at AgencyBillExecutedCreditDistributions.pcf: line 147, column 87
    function def_onEnter_35 (def :  pcf.AgencyBillDistArchiveWarningPanelSet) : void {
      def.onEnter(selectedCreditDistribution)
    }
    
    // 'def' attribute on PanelRef (id=AgencyDistItemsReadOnlyPanel) at AgencyBillExecutedCreditDistributions.pcf: line 203, column 49
    function def_onEnter_67 (def :  pcf.AgencyDistItemsReadOnlyCV) : void {
      def.onEnter(selectedCreditDistribution)
    }
    
    // 'def' attribute on PanelRef at AgencyBillExecutedCreditDistributions.pcf: line 147, column 87
    function def_refreshVariables_36 (def :  pcf.AgencyBillDistArchiveWarningPanelSet) : void {
      def.refreshVariables(selectedCreditDistribution)
    }
    
    // 'def' attribute on PanelRef (id=AgencyDistItemsReadOnlyPanel) at AgencyBillExecutedCreditDistributions.pcf: line 203, column 49
    function def_refreshVariables_68 (def :  pcf.AgencyDistItemsReadOnlyCV) : void {
      def.refreshVariables(selectedCreditDistribution)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillExecutedCreditDistributions.pcf: line 34, column 153
    function filter_1 () : gw.api.filters.IFilter {
      return new gw.payment.BaseDistReceivedDateFilter(30)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillExecutedCreditDistributions.pcf: line 37, column 153
    function filter_2 () : gw.api.filters.IFilter {
      return new gw.payment.BaseDistReceivedDateFilter(60)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillExecutedCreditDistributions.pcf: line 40, column 153
    function filter_3 () : gw.api.filters.IFilter {
      return new gw.payment.BaseDistReceivedDateFilter(90)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillExecutedCreditDistributions.pcf: line 43, column 146
    function filter_4 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("All", \ qf -> {})
    }
    
    // 'initialValue' attribute on Variable at AgencyBillExecutedCreditDistributions.pcf: line 22, column 85
    function initialValue_0 () : gw.api.database.IQueryBeanResult<entity.AgencyCyclePayment> {
      return producer.ExecutedCreditDistributions
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AgencyBillExecutedCreditDistributions.pcf: line 104, column 78
    function sortValue_8 (creditDistribution :  entity.AgencyCyclePayment) : java.lang.Object {
      return creditDistribution.BaseMoneyReceived.ReceivedDate
    }
    
    // 'title' attribute on Card (id=CreditDistributionDetailCard) at AgencyBillExecutedCreditDistributions.pcf: line 145, column 186
    function title_69 () : java.lang.String {
      return DisplayKey.get("Web.AgencyBillPayments.CreditDistributions.CreditDistributionDetail", selectedCreditDistribution.BaseMoneyReceived.ReceivedDate.AsUIStyle)
    }
    
    // 'value' attribute on TypeKeyInput (id=Reversed_Input) at AgencyBillExecutedCreditDistributions.pcf: line 158, column 84
    function valueRoot_39 () : java.lang.Object {
      return selectedCreditDistribution.BaseMoneyReceived
    }
    
    // 'value' attribute on TextInput (id=PaymentDescription_Input) at AgencyBillExecutedCreditDistributions.pcf: line 170, column 87
    function valueRoot_49 () : java.lang.Object {
      return selectedCreditDistribution.AgencyMoneyReceived
    }
    
    // 'value' attribute on MonetaryAmountInput (id=DistributedAmount_Input) at AgencyBillExecutedCreditDistributions.pcf: line 180, column 84
    function valueRoot_52 () : java.lang.Object {
      return selectedCreditDistribution
    }
    
    // 'value' attribute on RowIterator (id=CreditDistributionsIterator) at AgencyBillExecutedCreditDistributions.pcf: line 79, column 93
    function value_34 () : gw.api.database.IQueryBeanResult<entity.AgencyCyclePayment> {
      return creditDistributions
    }
    
    // 'value' attribute on TypeKeyInput (id=Reversed_Input) at AgencyBillExecutedCreditDistributions.pcf: line 158, column 84
    function value_38 () : typekey.PaymentReversalReason {
      return selectedCreditDistribution.BaseMoneyReceived.ReversalReason
    }
    
    // 'value' attribute on DateInput (id=CreateDate_Input) at AgencyBillExecutedCreditDistributions.pcf: line 162, column 86
    function value_42 () : java.util.Date {
      return selectedCreditDistribution.BaseMoneyReceived.ReceivedDate
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AgencyBillExecutedCreditDistributions.pcf: line 166, column 78
    function value_45 () : java.lang.String {
      return selectedCreditDistribution.BaseMoneyReceived.Name
    }
    
    // 'value' attribute on TextInput (id=PaymentDescription_Input) at AgencyBillExecutedCreditDistributions.pcf: line 170, column 87
    function value_48 () : java.lang.String {
      return selectedCreditDistribution.AgencyMoneyReceived.Description
    }
    
    // 'value' attribute on MonetaryAmountInput (id=DistributedAmount_Input) at AgencyBillExecutedCreditDistributions.pcf: line 180, column 84
    function value_51 () : gw.pl.currency.MonetaryAmount {
      return selectedCreditDistribution.NetDistributedToInvoiceItems
    }
    
    // 'value' attribute on MonetaryAmountInput (id=SuspenseAmount_Input) at AgencyBillExecutedCreditDistributions.pcf: line 186, column 69
    function value_55 () : gw.pl.currency.MonetaryAmount {
      return selectedCreditDistribution.NetInSuspense
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ProducerWriteoffAmount_Input) at AgencyBillExecutedCreditDistributions.pcf: line 192, column 78
    function value_59 () : gw.pl.currency.MonetaryAmount {
      return selectedCreditDistribution.ProducerWriteoffAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ItemWriteOff_Input) at AgencyBillExecutedCreditDistributions.pcf: line 198, column 74
    function value_63 () : gw.pl.currency.MonetaryAmount {
      return selectedCreditDistribution.ItemWriteOffAmount
    }
    
    // 'visible' attribute on TypeKeyInput (id=Reversed_Input) at AgencyBillExecutedCreditDistributions.pcf: line 158, column 84
    function visible_37 () : java.lang.Boolean {
      return selectedCreditDistribution.BaseMoneyReceived.Reversed
    }
    
    property get creditDistributions () : gw.api.database.IQueryBeanResult<entity.AgencyCyclePayment> {
      return getVariableValue("creditDistributions", 1) as gw.api.database.IQueryBeanResult<entity.AgencyCyclePayment>
    }
    
    property set creditDistributions ($arg :  gw.api.database.IQueryBeanResult<entity.AgencyCyclePayment>) {
      setVariableValue("creditDistributions", 1, $arg)
    }
    
    property get initialCreditDistribution () : AgencyCyclePayment {
      return getVariableValue("initialCreditDistribution", 1) as AgencyCyclePayment
    }
    
    property set initialCreditDistribution ($arg :  AgencyCyclePayment) {
      setVariableValue("initialCreditDistribution", 1, $arg)
    }
    
    property get selectedCreditDistribution () : AgencyCyclePayment {
      return getCurrentSelection(1) as AgencyCyclePayment
    }
    
    property set selectedCreditDistribution ($arg :  AgencyCyclePayment) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  
}