package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailContacts.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailContactsExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailContacts.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at PolicyDetailContacts.pcf: line 83, column 45
    function valueRoot_19 () : java.lang.Object {
      return policyPeriodContact.Contact
    }
    
    // 'value' attribute on TextCell (id=ContactName_Cell) at PolicyDetailContacts.pcf: line 78, column 57
    function value_16 () : entity.PolicyPeriodContact {
      return policyPeriodContact
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at PolicyDetailContacts.pcf: line 83, column 45
    function value_18 () : entity.Address {
      return policyPeriodContact.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=ContactRoles_Cell) at PolicyDetailContacts.pcf: line 87, column 101
    function value_21 () : java.lang.String {
      return gw.api.web.policy.PolicyPeriodUtil.getRolesForDisplay(policyPeriodContact)
    }
    
    property get policyPeriodContact () : entity.PolicyPeriodContact {
      return getIteratedValue(2) as entity.PolicyPeriodContact
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailContacts.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends PolicyDetailContactsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at PolicyDetailContacts.pcf: line 96, column 68
    function def_onEnter_25 (def :  pcf.PolicyContactDetailDV) : void {
      def.onEnter(selectedContact, false)
    }
    
    // 'def' attribute on PanelRef at PolicyDetailContacts.pcf: line 96, column 68
    function def_refreshVariables_26 (def :  pcf.PolicyContactDetailDV) : void {
      def.refreshVariables(selectedContact, false)
    }
    
    // 'selectionOnEnter' attribute on ListDetailPanel (id=ListDetailPanel) at PolicyDetailContacts.pcf: line 61, column 45
    function selectionOnEnter_28 () : java.lang.Object {
      return contactToSelectOnEnter
    }
    
    // 'value' attribute on TextCell (id=ContactName_Cell) at PolicyDetailContacts.pcf: line 78, column 57
    function sortValue_13 (policyPeriodContact :  entity.PolicyPeriodContact) : java.lang.Object {
      return policyPeriodContact
    }
    
    // 'value' attribute on TextCell (id=ContactAddress_Cell) at PolicyDetailContacts.pcf: line 83, column 45
    function sortValue_14 (policyPeriodContact :  entity.PolicyPeriodContact) : java.lang.Object {
      return policyPeriodContact.Contact.PrimaryAddress
    }
    
    // 'value' attribute on TextCell (id=ContactRoles_Cell) at PolicyDetailContacts.pcf: line 87, column 101
    function sortValue_15 (policyPeriodContact :  entity.PolicyPeriodContact) : java.lang.Object {
      return gw.api.web.policy.PolicyPeriodUtil.getRolesForDisplay(policyPeriodContact)
    }
    
    // 'title' attribute on Card (id=ContactDetailCard) at PolicyDetailContacts.pcf: line 94, column 193
    function title_27 () : java.lang.String {
      return org.apache.commons.lang.StringUtils.isNotBlank(selectedContact.DisplayName) ? selectedContact.DisplayName : DisplayKey.get("Web.PolicyDetailContacts.NewContact")
    }
    
    // 'toRemove' attribute on RowIterator (id=policyContactIterator) at PolicyDetailContacts.pcf: line 71, column 54
    function toRemove_23 (policyPeriodContact :  entity.PolicyPeriodContact) : void {
      plcyPeriod.removeFromContacts(policyPeriodContact)
    }
    
    // 'value' attribute on RowIterator (id=policyContactIterator) at PolicyDetailContacts.pcf: line 71, column 54
    function value_24 () : entity.PolicyPeriodContact[] {
      return plcyPeriod.Contacts
    }
    
    property get selectedContact () : PolicyPeriodContact {
      return getCurrentSelection(1) as PolicyPeriodContact
    }
    
    property set selectedContact ($arg :  PolicyPeriodContact) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailContacts.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailContactsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod, contactToSelectOnEnter :  PolicyPeriodContact) : int {
      return 1
    }
    
    // 'action' attribute on MenuItem (id=addNewCompany) at PolicyDetailContacts.pcf: line 38, column 89
    function action_3 () : void {
      NewPolicyContactPopup.push(plcyPeriod, Company)
    }
    
    // 'action' attribute on MenuItem (id=addNewPerson) at PolicyDetailContacts.pcf: line 42, column 88
    function action_5 () : void {
      NewPolicyContactPopup.push(plcyPeriod, Person)
    }
    
    // 'action' attribute on PickerToolbarButton (id=addExistingContact) at PolicyDetailContacts.pcf: line 50, column 98
    function action_8 () : void {
      ContactSearchPopup.push(false)
    }
    
    // 'action' attribute on MenuItem (id=addNewCompany) at PolicyDetailContacts.pcf: line 38, column 89
    function action_dest_4 () : pcf.api.Destination {
      return pcf.NewPolicyContactPopup.createDestination(plcyPeriod, Company)
    }
    
    // 'action' attribute on MenuItem (id=addNewPerson) at PolicyDetailContacts.pcf: line 42, column 88
    function action_dest_6 () : pcf.api.Destination {
      return pcf.NewPolicyContactPopup.createDestination(plcyPeriod, Person)
    }
    
    // 'action' attribute on PickerToolbarButton (id=addExistingContact) at PolicyDetailContacts.pcf: line 50, column 98
    function action_dest_9 () : pcf.api.Destination {
      return pcf.ContactSearchPopup.createDestination(false)
    }
    
    // 'canEdit' attribute on Page (id=PolicyDetailContacts) at PolicyDetailContacts.pcf: line 10, column 72
    function canEdit_29 () : java.lang.Boolean {
      return perm.PolicyPeriodContact.edit
    }
    
    // 'canVisit' attribute on Page (id=PolicyDetailContacts) at PolicyDetailContacts.pcf: line 10, column 72
    static function canVisit_30 (contactToSelectOnEnter :  PolicyPeriodContact, plcyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcycontview
    }
    
    // EditButtons at PolicyDetailContacts.pcf: line 25, column 50
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'label' attribute on AlertBar (id=ArchivedAlert) at PolicyDetailContacts.pcf: line 56, column 40
    function label_12 () : java.lang.Object {
      return DisplayKey.get("Web.Archive.PolicyPeriod.ArchivedAlertBar", plcyPeriod.ArchiveDate.AsUIStyle)
    }
    
    // 'onPick' attribute on PickerToolbarButton (id=addExistingContact) at PolicyDetailContacts.pcf: line 50, column 98
    function onPick_10 (PickedValue :  gw.plugin.contact.ContactResult) : void {
      gw.contact.ContactConnection.connectContactToPolicy(PickedValue, plcyPeriod)
    }
    
    // Page (id=PolicyDetailContacts) at PolicyDetailContacts.pcf: line 10, column 72
    static function parent_31 (contactToSelectOnEnter :  PolicyPeriodContact, plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyGroup.createDestination(plcyPeriod)
    }
    
    // 'editVisible' attribute on EditButtons at PolicyDetailContacts.pcf: line 25, column 50
    function visible_0 () : java.lang.Boolean {
      return perm.System.plcycntedit
    }
    
    // 'visible' attribute on AlertBar (id=ArchivedAlert) at PolicyDetailContacts.pcf: line 56, column 40
    function visible_11 () : java.lang.Boolean {
      return plcyPeriod.Archived
    }
    
    // 'removeVisible' attribute on IteratorButtons at PolicyDetailContacts.pcf: line 29, column 54
    function visible_2 () : java.lang.Boolean {
      return perm.System.plcycntdelete
    }
    
    // 'visible' attribute on ToolbarButton (id=addNewContact) at PolicyDetailContacts.pcf: line 34, column 47
    function visible_7 () : java.lang.Boolean {
      return perm.System.plcycntcreate
    }
    
    override property get CurrentLocation () : pcf.PolicyDetailContacts {
      return super.CurrentLocation as pcf.PolicyDetailContacts
    }
    
    property get contactToSelectOnEnter () : PolicyPeriodContact {
      return getVariableValue("contactToSelectOnEnter", 0) as PolicyPeriodContact
    }
    
    property set contactToSelectOnEnter ($arg :  PolicyPeriodContact) {
      setVariableValue("contactToSelectOnEnter", 0, $arg)
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 0, $arg)
    }
    
    
  }
  
  
}