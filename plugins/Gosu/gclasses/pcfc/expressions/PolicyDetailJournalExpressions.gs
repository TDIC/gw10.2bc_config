package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailJournal.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailJournalExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailJournal.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailJournalExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=PolicyDetailJournal) at PolicyDetailJournal.pcf: line 9, column 71
    static function canVisit_2 (plcyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcyjournalview
    }
    
    // 'def' attribute on ScreenRef at PolicyDetailJournal.pcf: line 16, column 130
    function def_onEnter_0 (def :  pcf.JournalScreen) : void {
      def.onEnter(plcyPeriod, gw.api.web.policy.PolicyPeriodUtil.findAllPolicyCommissionsWithChargeDetails(plcyPeriod))
    }
    
    // 'def' attribute on ScreenRef at PolicyDetailJournal.pcf: line 16, column 130
    function def_refreshVariables_1 (def :  pcf.JournalScreen) : void {
      def.refreshVariables(plcyPeriod, gw.api.web.policy.PolicyPeriodUtil.findAllPolicyCommissionsWithChargeDetails(plcyPeriod))
    }
    
    // Page (id=PolicyDetailJournal) at PolicyDetailJournal.pcf: line 9, column 71
    static function parent_3 (plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyGroup.createDestination(plcyPeriod)
    }
    
    override property get CurrentLocation () : pcf.PolicyDetailJournal {
      return super.CurrentLocation as pcf.PolicyDetailJournal
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 0, $arg)
    }
    
    
  }
  
  
}