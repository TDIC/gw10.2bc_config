package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ActivitySearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ActivitySearchExpressions {
  @javax.annotation.Generated("config/web/pcf/search/ActivitySearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ActivitySearchExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=ActivitySearch) at ActivitySearch.pcf: line 8, column 69
    static function canVisit_2 () : java.lang.Boolean {
      return perm.System.viewsearch and perm.System.actsearch
    }
    
    // 'def' attribute on ScreenRef at ActivitySearch.pcf: line 10, column 37
    function def_onEnter_0 (def :  pcf.ActivitySearchScreen) : void {
      def.onEnter()
    }
    
    // 'def' attribute on ScreenRef at ActivitySearch.pcf: line 10, column 37
    function def_refreshVariables_1 (def :  pcf.ActivitySearchScreen) : void {
      def.refreshVariables()
    }
    
    // Page (id=ActivitySearch) at ActivitySearch.pcf: line 8, column 69
    static function parent_3 () : pcf.api.Destination {
      return pcf.SearchGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.ActivitySearch {
      return super.CurrentLocation as pcf.ActivitySearch
    }
    
    
  }
  
  
}