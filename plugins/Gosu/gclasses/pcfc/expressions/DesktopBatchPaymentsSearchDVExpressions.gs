package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopBatchPaymentsSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopBatchPaymentsSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopBatchPaymentsSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopBatchPaymentsSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerInput (id=CreatedBy_Input) at DesktopBatchPaymentsSearchDV.pcf: line 61, column 27
    function action_32 () : void {
      UserSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=LastEditedBy_Input) at DesktopBatchPaymentsSearchDV.pcf: line 68, column 27
    function action_38 () : void {
      UserSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=PostedBy_Input) at DesktopBatchPaymentsSearchDV.pcf: line 88, column 59
    function action_56 () : void {
      UserSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=ReversedBy_Input) at DesktopBatchPaymentsSearchDV.pcf: line 108, column 61
    function action_76 () : void {
      UserSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=CreatedBy_Input) at DesktopBatchPaymentsSearchDV.pcf: line 61, column 27
    function action_dest_33 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'pickLocation' attribute on PickerInput (id=LastEditedBy_Input) at DesktopBatchPaymentsSearchDV.pcf: line 68, column 27
    function action_dest_39 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'pickLocation' attribute on PickerInput (id=PostedBy_Input) at DesktopBatchPaymentsSearchDV.pcf: line 88, column 59
    function action_dest_57 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'pickLocation' attribute on PickerInput (id=ReversedBy_Input) at DesktopBatchPaymentsSearchDV.pcf: line 108, column 61
    function action_dest_77 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'available' attribute on MonetaryAmountInput (id=MinAmount_Input) at DesktopBatchPaymentsSearchDV.pcf: line 45, column 48
    function available_18 () : java.lang.Boolean {
      return batchSearchCriteria.MonetaryAmountInputAvailable
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=MinAmount_Input) at DesktopBatchPaymentsSearchDV.pcf: line 45, column 48
    function currency_22 () : typekey.Currency {
      return gw.search.SearchHelper.getDefaultCurrencyIfNull(batchSearchCriteria.Currency)
    }
    
    // 'def' attribute on InputSetRef at DesktopBatchPaymentsSearchDV.pcf: line 112, column 41
    function def_onEnter_84 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at DesktopBatchPaymentsSearchDV.pcf: line 112, column 41
    function def_refreshVariables_85 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TextInput (id=BatchNumber_Input) at DesktopBatchPaymentsSearchDV.pcf: line 19, column 29
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      batchSearchCriteria.BatchNumber = (__VALUE_TO_SET as String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at DesktopBatchPaymentsSearchDV.pcf: line 35, column 66
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      batchSearchCriteria.Currency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmount_Input) at DesktopBatchPaymentsSearchDV.pcf: line 45, column 48
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      batchSearchCriteria.MinAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmount_Input) at DesktopBatchPaymentsSearchDV.pcf: line 52, column 48
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      batchSearchCriteria.MaxAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on PickerInput (id=CreatedBy_Input) at DesktopBatchPaymentsSearchDV.pcf: line 61, column 27
    function defaultSetter_35 (__VALUE_TO_SET :  java.lang.Object) : void {
      batchSearchCriteria.CreatedBy = (__VALUE_TO_SET as User)
    }
    
    // 'value' attribute on PickerInput (id=LastEditedBy_Input) at DesktopBatchPaymentsSearchDV.pcf: line 68, column 27
    function defaultSetter_41 (__VALUE_TO_SET :  java.lang.Object) : void {
      batchSearchCriteria.LastEditedBy = (__VALUE_TO_SET as User)
    }
    
    // 'value' attribute on DateInput (id=EarliestPostedDate_Input) at DesktopBatchPaymentsSearchDV.pcf: line 74, column 59
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      batchSearchCriteria.EarliestPostedDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on RangeInput (id=BatchStatus_Input) at DesktopBatchPaymentsSearchDV.pcf: line 26, column 41
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      batchSearchCriteria.BatchStatus = (__VALUE_TO_SET as BatchPaymentsStatus)
    }
    
    // 'value' attribute on DateInput (id=LatestPostedDate_Input) at DesktopBatchPaymentsSearchDV.pcf: line 80, column 59
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      batchSearchCriteria.LatestPostedDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on PickerInput (id=PostedBy_Input) at DesktopBatchPaymentsSearchDV.pcf: line 88, column 59
    function defaultSetter_60 (__VALUE_TO_SET :  java.lang.Object) : void {
      batchSearchCriteria.PostedBy = (__VALUE_TO_SET as User)
    }
    
    // 'value' attribute on DateInput (id=EarliestReversedDate_Input) at DesktopBatchPaymentsSearchDV.pcf: line 94, column 61
    function defaultSetter_66 (__VALUE_TO_SET :  java.lang.Object) : void {
      batchSearchCriteria.EarliestReversedDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=LatestReversedDate_Input) at DesktopBatchPaymentsSearchDV.pcf: line 100, column 61
    function defaultSetter_72 (__VALUE_TO_SET :  java.lang.Object) : void {
      batchSearchCriteria.LatestReversedDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on PickerInput (id=ReversedBy_Input) at DesktopBatchPaymentsSearchDV.pcf: line 108, column 61
    function defaultSetter_80 (__VALUE_TO_SET :  java.lang.Object) : void {
      batchSearchCriteria.ReversedBy = (__VALUE_TO_SET as User)
    }
    
    // 'onChange' attribute on PostOnChange at DesktopBatchPaymentsSearchDV.pcf: line 37, column 74
    function onChange_11 () : void {
      batchSearchCriteria.clearMinimumAndMaximumFields()
    }
    
    // 'valueRange' attribute on RangeInput (id=BatchStatus_Input) at DesktopBatchPaymentsSearchDV.pcf: line 26, column 41
    function valueRange_7 () : java.lang.Object {
      return BatchPaymentsStatus.getTypeKeys(false)
    }
    
    // 'value' attribute on TextInput (id=BatchNumber_Input) at DesktopBatchPaymentsSearchDV.pcf: line 19, column 29
    function valueRoot_2 () : java.lang.Object {
      return batchSearchCriteria
    }
    
    // 'value' attribute on TextInput (id=BatchNumber_Input) at DesktopBatchPaymentsSearchDV.pcf: line 19, column 29
    function value_0 () : String {
      return batchSearchCriteria.BatchNumber
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at DesktopBatchPaymentsSearchDV.pcf: line 35, column 66
    function value_13 () : typekey.Currency {
      return batchSearchCriteria.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmount_Input) at DesktopBatchPaymentsSearchDV.pcf: line 45, column 48
    function value_19 () : gw.pl.currency.MonetaryAmount {
      return batchSearchCriteria.MinAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmount_Input) at DesktopBatchPaymentsSearchDV.pcf: line 52, column 48
    function value_26 () : gw.pl.currency.MonetaryAmount {
      return batchSearchCriteria.MaxAmount
    }
    
    // 'value' attribute on PickerInput (id=CreatedBy_Input) at DesktopBatchPaymentsSearchDV.pcf: line 61, column 27
    function value_34 () : User {
      return batchSearchCriteria.CreatedBy
    }
    
    // 'value' attribute on RangeInput (id=BatchStatus_Input) at DesktopBatchPaymentsSearchDV.pcf: line 26, column 41
    function value_4 () : BatchPaymentsStatus {
      return batchSearchCriteria.BatchStatus
    }
    
    // 'value' attribute on PickerInput (id=LastEditedBy_Input) at DesktopBatchPaymentsSearchDV.pcf: line 68, column 27
    function value_40 () : User {
      return batchSearchCriteria.LastEditedBy
    }
    
    // 'value' attribute on DateInput (id=EarliestPostedDate_Input) at DesktopBatchPaymentsSearchDV.pcf: line 74, column 59
    function value_45 () : java.util.Date {
      return batchSearchCriteria.EarliestPostedDate
    }
    
    // 'value' attribute on DateInput (id=LatestPostedDate_Input) at DesktopBatchPaymentsSearchDV.pcf: line 80, column 59
    function value_51 () : java.util.Date {
      return batchSearchCriteria.LatestPostedDate
    }
    
    // 'value' attribute on PickerInput (id=PostedBy_Input) at DesktopBatchPaymentsSearchDV.pcf: line 88, column 59
    function value_59 () : User {
      return batchSearchCriteria.PostedBy
    }
    
    // 'value' attribute on DateInput (id=EarliestReversedDate_Input) at DesktopBatchPaymentsSearchDV.pcf: line 94, column 61
    function value_65 () : java.util.Date {
      return batchSearchCriteria.EarliestReversedDate
    }
    
    // 'value' attribute on DateInput (id=LatestReversedDate_Input) at DesktopBatchPaymentsSearchDV.pcf: line 100, column 61
    function value_71 () : java.util.Date {
      return batchSearchCriteria.LatestReversedDate
    }
    
    // 'value' attribute on PickerInput (id=ReversedBy_Input) at DesktopBatchPaymentsSearchDV.pcf: line 108, column 61
    function value_79 () : User {
      return batchSearchCriteria.ReversedBy
    }
    
    // 'valueRange' attribute on RangeInput (id=BatchStatus_Input) at DesktopBatchPaymentsSearchDV.pcf: line 26, column 41
    function verifyValueRangeIsAllowedType_8 ($$arg :  BatchPaymentsStatus[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BatchStatus_Input) at DesktopBatchPaymentsSearchDV.pcf: line 26, column 41
    function verifyValueRangeIsAllowedType_8 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BatchStatus_Input) at DesktopBatchPaymentsSearchDV.pcf: line 26, column 41
    function verifyValueRangeIsAllowedType_8 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BatchStatus_Input) at DesktopBatchPaymentsSearchDV.pcf: line 26, column 41
    function verifyValueRange_9 () : void {
      var __valueRangeArg = BatchPaymentsStatus.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_8(__valueRangeArg)
    }
    
    // 'visible' attribute on TypeKeyInput (id=Currency_Input) at DesktopBatchPaymentsSearchDV.pcf: line 35, column 66
    function visible_12 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on DateInput (id=EarliestPostedDate_Input) at DesktopBatchPaymentsSearchDV.pcf: line 74, column 59
    function visible_44 () : java.lang.Boolean {
      return batchSearchCriteria.PostedStatusChosen
    }
    
    // 'visible' attribute on DateInput (id=EarliestReversedDate_Input) at DesktopBatchPaymentsSearchDV.pcf: line 94, column 61
    function visible_64 () : java.lang.Boolean {
      return batchSearchCriteria.ReversedStatusChosen
    }
    
    property get batchSearchCriteria () : gw.search.BatchPaymentsSearchCriteria {
      return getRequireValue("batchSearchCriteria", 0) as gw.search.BatchPaymentsSearchCriteria
    }
    
    property set batchSearchCriteria ($arg :  gw.search.BatchPaymentsSearchCriteria) {
      setRequireValue("batchSearchCriteria", 0, $arg)
    }
    
    
  }
  
  
}