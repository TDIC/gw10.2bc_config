package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/AccountNewWriteoffWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountNewWriteoffWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/AccountNewWriteoffWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountNewWriteoffWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'allowFinish' attribute on WizardStep (id=ConfirmationStep) at AccountNewWriteoffWizard.pcf: line 41, column 85
    function allowFinish_10 () : java.lang.Boolean {
      return uiWriteoff != null && uiWriteoff.Amount != null
    }
    
    // 'allowNext' attribute on WizardStep (id=TargetStep) at AccountNewWriteoffWizard.pcf: line 28, column 40
    function allowNext_2 () : java.lang.Boolean {
      return targetOfWriteoff != null && ! (targetOfWriteoff.TAccountOwner typeis Account)
    }
    
    // 'beforeCommit' attribute on Wizard (id=AccountNewWriteoffWizard) at AccountNewWriteoffWizard.pcf: line 8, column 35
    function beforeCommit_13 (pickedValue :  java.lang.Object) : void {
      uiWriteoff.WriteOff.doWriteOffWithApproval()
    }
    
    // 'initialValue' attribute on Variable at AccountNewWriteoffWizard.pcf: line 17, column 60
    function initialValue_0 () : gw.api.web.accounting.TAccountOwnerReference {
      return createTargetOfWriteOff()
    }
    
    // 'onEnter' attribute on WizardStep (id=DetailsStep) at AccountNewWriteoffWizard.pcf: line 35, column 80
    function onEnter_6 () : void {
      createUIWriteOff()
    }
    
    // 'onExit' attribute on WizardStep (id=DetailsStep) at AccountNewWriteoffWizard.pcf: line 35, column 80
    function onExit_7 () : void {
      onExitFromDetailsStep()
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at AccountNewWriteoffWizard.pcf: line 41, column 85
    function screen_onEnter_11 (def :  pcf.NewWriteoffWizardConfirmationStepScreen) : void {
      def.onEnter(uiWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=TargetStep) at AccountNewWriteoffWizard.pcf: line 28, column 40
    function screen_onEnter_3 (def :  pcf.AccountNewWriteoffWizardTargetStepScreen) : void {
      def.onEnter(account, targetOfWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=DetailsStep) at AccountNewWriteoffWizard.pcf: line 35, column 80
    function screen_onEnter_8 (def :  pcf.NewWriteoffWizardDetailsStepScreen) : void {
      def.onEnter(uiWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at AccountNewWriteoffWizard.pcf: line 41, column 85
    function screen_refreshVariables_12 (def :  pcf.NewWriteoffWizardConfirmationStepScreen) : void {
      def.refreshVariables(uiWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=TargetStep) at AccountNewWriteoffWizard.pcf: line 28, column 40
    function screen_refreshVariables_4 (def :  pcf.AccountNewWriteoffWizardTargetStepScreen) : void {
      def.refreshVariables(account, targetOfWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=DetailsStep) at AccountNewWriteoffWizard.pcf: line 35, column 80
    function screen_refreshVariables_9 (def :  pcf.NewWriteoffWizardDetailsStepScreen) : void {
      def.refreshVariables(uiWriteoff)
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewWriteoffWizard) at AccountNewWriteoffWizard.pcf: line 8, column 35
    function tabBar_onEnter_14 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AccountNewWriteoffWizard) at AccountNewWriteoffWizard.pcf: line 8, column 35
    function tabBar_refreshVariables_15 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    // 'visible' attribute on WizardStep (id=TargetStep) at AccountNewWriteoffWizard.pcf: line 28, column 40
    function visible_1 () : java.lang.Boolean {
      return !account.isListBill()
    }
    
    override property get CurrentLocation () : pcf.AccountNewWriteoffWizard {
      return super.CurrentLocation as pcf.AccountNewWriteoffWizard
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get targetOfWriteoff () : gw.api.web.accounting.TAccountOwnerReference {
      return getVariableValue("targetOfWriteoff", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set targetOfWriteoff ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setVariableValue("targetOfWriteoff", 0, $arg)
    }
    
    property get uiWriteoff () : gw.api.web.accounting.UIWriteOffCreation {
      return getVariableValue("uiWriteoff", 0) as gw.api.web.accounting.UIWriteOffCreation
    }
    
    property set uiWriteoff ($arg :  gw.api.web.accounting.UIWriteOffCreation) {
      setVariableValue("uiWriteoff", 0, $arg)
    }
    
    
                  function onExitFromDetailsStep() {
                    uiWriteoff.initiateApprovalActivityIfUserLacksAuthority()
                  }
    
                  function createUIWriteOff() {
                    if (uiWriteoff == null || uiWriteoff.WriteOff.TAccountOwner != targetOfWriteoff.TAccountOwner) {
                      if (uiWriteoff != null) {
                        uiWriteoff.cleanupBeforeDiscard()
                      }
                      var writeOff = new gw.api.web.accounting.WriteOffFactory(CurrentLocation).createChargeGrossWriteOff(targetOfWriteoff.TAccountOwner)
                      uiWriteoff = new gw.api.web.accounting.UIWriteOffCreation(writeOff)
    /**
                       * US69 
                       * 09/19/2014 Hermia Kho
                       * 
                       * Default the Charge Pattern to "Premium" instead of "<none>"
                       * Default the Writeoff amount to $0, instead of the Unpaid Amount
                       * Set the UseFullAmount to false - setting it to true default the Writeoff Amount with the Unpaid Amount
                       */
                      
                      uiWriteoff.UseFullAmount = false
                      uiWriteoff.Amount = 0bd.ofDefaultCurrency()
                      uiWriteoff.ChargePattern = uiWriteoff.WriteOff.TAccountOwner.ChargePatterns.firstWhere( \ pattern -> pattern.ChargeName == "Premium")
                    }
                  }
    
                  function createTargetOfWriteOff() : gw.api.web.accounting.TAccountOwnerReference {
                    var reference = new gw.api.web.accounting.TAccountOwnerReference()
                    reference.TAccountOwner = account
                    return reference
                  }
          
    
    
  }
  
  
}