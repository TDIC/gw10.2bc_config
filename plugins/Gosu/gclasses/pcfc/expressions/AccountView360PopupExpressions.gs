package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/acct360/AccountView360Popup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountView360PopupExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/acct360/AccountView360Popup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountView360PopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'canVisit' attribute on Popup (id=AccountView360Popup) at AccountView360Popup.pcf: line 9, column 72
    static function canVisit_2 (account :  Account) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctsummview
    }
    
    // 'def' attribute on ScreenRef at AccountView360Popup.pcf: line 16, column 50
    function def_onEnter_0 (def :  pcf.AccountView360Screen) : void {
      def.onEnter(account, null)
    }
    
    // 'def' attribute on ScreenRef at AccountView360Popup.pcf: line 16, column 50
    function def_refreshVariables_1 (def :  pcf.AccountView360Screen) : void {
      def.refreshVariables(account, null)
    }
    
    override property get CurrentLocation () : pcf.AccountView360Popup {
      return super.CurrentLocation as pcf.AccountView360Popup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  
}