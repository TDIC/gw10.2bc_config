package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/businesssettings/ChargePatterns.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChargePatternsExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/ChargePatterns.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChargePatternsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=ChargePatterns) at ChargePatterns.pcf: line 8, column 66
    static function canVisit_45 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.chargepatternview
    }
    
    // Page (id=ChargePatterns) at ChargePatterns.pcf: line 8, column 66
    static function parent_46 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    // 'value' attribute on TextCell (id=ChargeCode_Cell) at ChargePatterns.pcf: line 25, column 49
    function sortValue_0 (chargePattern :  entity.ChargePattern) : java.lang.Object {
      return chargePattern.ChargeCode
    }
    
    // 'value' attribute on TextCell (id=ChargeName_Cell) at ChargePatterns.pcf: line 31, column 49
    function sortValue_1 (chargePattern :  entity.ChargePattern) : java.lang.Object {
      return chargePattern.ChargeName
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ChargePatterns.pcf: line 37, column 29
    function sortValue_2 (chargePattern :  entity.ChargePattern) : java.lang.Object {
      return chargePattern.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Category_Cell) at ChargePatterns.pcf: line 42, column 51
    function sortValue_3 (chargePattern :  entity.ChargePattern) : java.lang.Object {
      return chargePattern.Category
    }
    
    // 'value' attribute on TextCell (id=TAccountOwner_Cell) at ChargePatterns.pcf: line 47, column 56
    function sortValue_4 (chargePattern :  entity.ChargePattern) : java.lang.Object {
      return chargePattern.TAccountOwnerPattern
    }
    
    // 'value' attribute on TypeKeyCell (id=InvoiceTreatment_Cell) at ChargePatterns.pcf: line 52, column 53
    function sortValue_5 (chargePattern :  entity.ChargePattern) : java.lang.Object {
      return chargePattern.InvoiceTreatment
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentPriority_Cell) at ChargePatterns.pcf: line 57, column 51
    function sortValue_6 (chargePattern :  entity.ChargePattern) : java.lang.Object {
      return chargePattern.Priority
    }
    
    // 'value' attribute on TypeKeyCell (id=PrintPriority_Cell) at ChargePatterns.pcf: line 62, column 61
    function sortValue_7 (chargePattern :  entity.ChargePattern) : java.lang.Object {
      return chargePattern.PrintPriority_TDIC
    }
    
    // 'value' attribute on BooleanRadioCell (id=InternalCharge_Cell) at ChargePatterns.pcf: line 66, column 53
    function sortValue_8 (chargePattern :  entity.ChargePattern) : java.lang.Object {
      return chargePattern.InternalCharge
    }
    
    // 'value' attribute on BooleanRadioCell (id=IncludedInEquityDating_Cell) at ChargePatterns.pcf: line 70, column 61
    function sortValue_9 (chargePattern :  entity.ChargePattern) : java.lang.Object {
      return chargePattern.IncludedInEquityDating
    }
    
    // 'value' attribute on RowIterator at ChargePatterns.pcf: line 19, column 84
    function value_44 () : gw.api.database.IQueryBeanResult<entity.ChargePattern> {
      return gw.api.database.Query.make(ChargePattern).select()
    }
    
    override property get CurrentLocation () : pcf.ChargePatterns {
      return super.CurrentLocation as pcf.ChargePatterns
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/ChargePatterns.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ChargePatternsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=ChargeCode_Cell) at ChargePatterns.pcf: line 25, column 49
    function action_10 () : void {
      ChargePatternDetail.go(chargePattern)
    }
    
    // 'action' attribute on TextCell (id=ChargeName_Cell) at ChargePatterns.pcf: line 31, column 49
    function action_15 () : void {
      ChargePatternDetail.go(chargePattern)
    }
    
    // 'action' attribute on TextCell (id=ChargeCode_Cell) at ChargePatterns.pcf: line 25, column 49
    function action_dest_11 () : pcf.api.Destination {
      return pcf.ChargePatternDetail.createDestination(chargePattern)
    }
    
    // 'action' attribute on TextCell (id=ChargeName_Cell) at ChargePatterns.pcf: line 31, column 49
    function action_dest_16 () : pcf.api.Destination {
      return pcf.ChargePatternDetail.createDestination(chargePattern)
    }
    
    // 'value' attribute on TextCell (id=ChargeCode_Cell) at ChargePatterns.pcf: line 25, column 49
    function valueRoot_13 () : java.lang.Object {
      return chargePattern
    }
    
    // 'value' attribute on TextCell (id=ChargeCode_Cell) at ChargePatterns.pcf: line 25, column 49
    function value_12 () : java.lang.String {
      return chargePattern.ChargeCode
    }
    
    // 'value' attribute on TextCell (id=ChargeName_Cell) at ChargePatterns.pcf: line 31, column 49
    function value_17 () : java.lang.String {
      return chargePattern.ChargeName
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ChargePatterns.pcf: line 37, column 29
    function value_20 () : typekey.ChargePattern {
      return chargePattern.Subtype
    }
    
    // 'value' attribute on TypeKeyCell (id=Category_Cell) at ChargePatterns.pcf: line 42, column 51
    function value_23 () : typekey.ChargeCategory {
      return chargePattern.Category
    }
    
    // 'value' attribute on TextCell (id=TAccountOwner_Cell) at ChargePatterns.pcf: line 47, column 56
    function value_26 () : entity.TAccountOwnerPattern {
      return chargePattern.TAccountOwnerPattern
    }
    
    // 'value' attribute on TypeKeyCell (id=InvoiceTreatment_Cell) at ChargePatterns.pcf: line 52, column 53
    function value_29 () : typekey.InvoiceTreatment {
      return chargePattern.InvoiceTreatment
    }
    
    // 'value' attribute on TypeKeyCell (id=PaymentPriority_Cell) at ChargePatterns.pcf: line 57, column 51
    function value_32 () : typekey.ChargePriority {
      return chargePattern.Priority
    }
    
    // 'value' attribute on TypeKeyCell (id=PrintPriority_Cell) at ChargePatterns.pcf: line 62, column 61
    function value_35 () : typekey.ChargePrintPriority_TDIC {
      return chargePattern.PrintPriority_TDIC
    }
    
    // 'value' attribute on BooleanRadioCell (id=InternalCharge_Cell) at ChargePatterns.pcf: line 66, column 53
    function value_38 () : java.lang.Boolean {
      return chargePattern.InternalCharge
    }
    
    // 'value' attribute on BooleanRadioCell (id=IncludedInEquityDating_Cell) at ChargePatterns.pcf: line 70, column 61
    function value_41 () : java.lang.Boolean {
      return chargePattern.IncludedInEquityDating
    }
    
    property get chargePattern () : entity.ChargePattern {
      return getIteratedValue(1) as entity.ChargePattern
    }
    
    
  }
  
  
}