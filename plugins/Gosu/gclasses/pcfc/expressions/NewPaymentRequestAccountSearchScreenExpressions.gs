package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/NewPaymentRequestAccountSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewPaymentRequestAccountSearchScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/NewPaymentRequestAccountSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Number_Cell) at NewPaymentRequestAccountSearchScreen.pcf: line 38, column 58
    function action_5 () : void {
      setAccountOnRequest(accountSearchView);(CurrentLocation as pcf.api.Wizard).next()
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at NewPaymentRequestAccountSearchScreen.pcf: line 38, column 58
    function valueRoot_7 () : java.lang.Object {
      return accountSearchView
    }
    
    // 'value' attribute on DateCell (id=CreationDate_Cell) at NewPaymentRequestAccountSearchScreen.pcf: line 50, column 55
    function value_12 () : java.util.Date {
      return accountSearchView.CreateTime
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at NewPaymentRequestAccountSearchScreen.pcf: line 38, column 58
    function value_6 () : java.lang.String {
      return accountSearchView.AccountNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Segment_Cell) at NewPaymentRequestAccountSearchScreen.pcf: line 44, column 53
    function value_9 () : typekey.AccountSegment {
      return accountSearchView.Segment
    }
    
    property get accountSearchView () : entity.AccountSearchView {
      return getIteratedValue(2) as entity.AccountSearchView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/payment/NewPaymentRequestAccountSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewPaymentRequestAccountSearchScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get tAccountOwnerReference () : gw.api.web.accounting.TAccountOwnerReference {
      return getRequireValue("tAccountOwnerReference", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set tAccountOwnerReference ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setRequireValue("tAccountOwnerReference", 0, $arg)
    }
    
    function setAccountOnRequest(accountSearchView : AccountSearchView){
       tAccountOwnerReference.TAccountOwner = accountSearchView.Account
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/payment/NewPaymentRequestAccountSearchScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends NewPaymentRequestAccountSearchScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewPaymentRequestAccountSearchScreen.pcf: line 18, column 47
    function def_onEnter_0 (def :  pcf.AccountSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at NewPaymentRequestAccountSearchScreen.pcf: line 18, column 47
    function def_refreshVariables_1 (def :  pcf.AccountSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'searchCriteria' attribute on SearchPanel at NewPaymentRequestAccountSearchScreen.pcf: line 16, column 85
    function searchCriteria_17 () : gw.search.AccountSearchCriteria {
      return new gw.search.AccountSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at NewPaymentRequestAccountSearchScreen.pcf: line 16, column 85
    function search_16 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria, StringCriterionMode.TC_STARTSWITH)
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at NewPaymentRequestAccountSearchScreen.pcf: line 38, column 58
    function sortValue_2 (accountSearchView :  entity.AccountSearchView) : java.lang.Object {
      return accountSearchView.AccountNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Segment_Cell) at NewPaymentRequestAccountSearchScreen.pcf: line 44, column 53
    function sortValue_3 (accountSearchView :  entity.AccountSearchView) : java.lang.Object {
      return accountSearchView.Segment
    }
    
    // 'sortBy' attribute on DateCell (id=CreationDate_Cell) at NewPaymentRequestAccountSearchScreen.pcf: line 50, column 55
    function sortValue_4 (accountSearchView :  entity.AccountSearchView) : java.lang.Object {
      return accountSearchView.CreateTime
    }
    
    // 'value' attribute on RowIterator at NewPaymentRequestAccountSearchScreen.pcf: line 31, column 90
    function value_15 () : gw.api.database.IQueryBeanResult<entity.AccountSearchView> {
      return accountSearchViews
    }
    
    property get accountSearchViews () : gw.api.database.IQueryBeanResult<AccountSearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<AccountSearchView>
    }
    
    property get searchCriteria () : gw.search.AccountSearchCriteria {
      return getCriteriaValue(1) as gw.search.AccountSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.AccountSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}