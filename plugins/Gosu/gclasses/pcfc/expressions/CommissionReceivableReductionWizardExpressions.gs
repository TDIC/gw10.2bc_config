package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/CommissionReceivableReductionWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionReceivableReductionWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/CommissionReceivableReductionWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionReceivableReductionWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Wizard (id=CommissionReceivableReductionWizard) at CommissionReceivableReductionWizard.pcf: line 7, column 46
    function beforeCommit_5 (pickedValue :  java.lang.Object) : void {
      helper.doWriteoff()
    }
    
    // 'initialValue' attribute on Variable at CommissionReceivableReductionWizard.pcf: line 16, column 63
    function initialValue_0 () : gw.producer.CommissionReceivableReductionHelper {
      return new gw.producer.CommissionReceivableReductionHelper(producer)
    }
    
    // 'screen' attribute on WizardStep (id=TargetStep) at CommissionReceivableReductionWizard.pcf: line 21, column 98
    function screen_onEnter_1 (def :  pcf.CommissionReceivableReductionWizardTargetsStepScreen) : void {
      def.onEnter(helper)
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at CommissionReceivableReductionWizard.pcf: line 27, column 103
    function screen_onEnter_3 (def :  pcf.CommissionReceivableReductionWizardConfirmationStepScreen) : void {
      def.onEnter(helper)
    }
    
    // 'screen' attribute on WizardStep (id=TargetStep) at CommissionReceivableReductionWizard.pcf: line 21, column 98
    function screen_refreshVariables_2 (def :  pcf.CommissionReceivableReductionWizardTargetsStepScreen) : void {
      def.refreshVariables(helper)
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at CommissionReceivableReductionWizard.pcf: line 27, column 103
    function screen_refreshVariables_4 (def :  pcf.CommissionReceivableReductionWizardConfirmationStepScreen) : void {
      def.refreshVariables(helper)
    }
    
    // 'tabBar' attribute on Wizard (id=CommissionReceivableReductionWizard) at CommissionReceivableReductionWizard.pcf: line 7, column 46
    function tabBar_onEnter_6 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=CommissionReceivableReductionWizard) at CommissionReceivableReductionWizard.pcf: line 7, column 46
    function tabBar_refreshVariables_7 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.CommissionReceivableReductionWizard {
      return super.CurrentLocation as pcf.CommissionReceivableReductionWizard
    }
    
    property get helper () : gw.producer.CommissionReceivableReductionHelper {
      return getVariableValue("helper", 0) as gw.producer.CommissionReceivableReductionHelper
    }
    
    property set helper ($arg :  gw.producer.CommissionReceivableReductionHelper) {
      setVariableValue("helper", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}