package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.database.Query
uses gw.api.web.admin.UserAuthorityLimitUtil
@javax.annotation.Generated("config/web/pcf/admin/authoritylimits/UserAuthorityLimitsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class UserAuthorityLimitsDVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/authoritylimits/UserAuthorityLimitsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class UserAuthorityLimitsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on ListViewInput (id=AuthorityLimits) at UserAuthorityLimitsDV.pcf: line 35, column 177
    function def_onEnter_12 (def :  pcf.EditableAuthorityLimitsLV) : void {
      def.onEnter(user)
    }
    
    // 'def' attribute on ListViewInput (id=AuthorityLimits) at UserAuthorityLimitsDV.pcf: line 35, column 177
    function def_refreshVariables_13 (def :  pcf.EditableAuthorityLimitsLV) : void {
      def.refreshVariables(user)
    }
    
    // 'value' attribute on RangeInput (id=AuthorityLimitsProfile_Input) at UserAuthorityLimitsDV.pcf: line 22, column 50
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      user.ModifiedAuthorityProfile = (__VALUE_TO_SET as entity.AuthorityLimitProfile)
    }
    
    // 'editable' attribute on ListViewInput (id=AuthorityLimits) at UserAuthorityLimitsDV.pcf: line 35, column 177
    function editable_10 () : java.lang.Boolean {
      return user.AuthorityProfile != null and user.AuthorityProfile.Custom
    }
    
    // 'validationExpression' attribute on ListViewInput (id=AuthorityLimits) at UserAuthorityLimitsDV.pcf: line 35, column 177
    function validationExpression_11 () : java.lang.Object {
      return user.AuthorityProfile != null and !user.AuthorityProfile.resolveAuthorityLimits() ? DisplayKey.get("Java.Error.AuthorityLimit") : null
    }
    
    // 'valueRange' attribute on RangeInput (id=AuthorityLimitsProfile_Input) at UserAuthorityLimitsDV.pcf: line 22, column 50
    function valueRange_3 () : java.lang.Object {
      return getValueRange()
    }
    
    // 'value' attribute on RangeInput (id=AuthorityLimitsProfile_Input) at UserAuthorityLimitsDV.pcf: line 22, column 50
    function valueRoot_2 () : java.lang.Object {
      return user
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at UserAuthorityLimitsDV.pcf: line 29, column 52
    function valueRoot_8 () : java.lang.Object {
      return user.AuthorityProfile
    }
    
    // 'value' attribute on RangeInput (id=AuthorityLimitsProfile_Input) at UserAuthorityLimitsDV.pcf: line 22, column 50
    function value_0 () : entity.AuthorityLimitProfile {
      return user.ModifiedAuthorityProfile
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at UserAuthorityLimitsDV.pcf: line 29, column 52
    function value_7 () : java.lang.String {
      return user.AuthorityProfile.Description
    }
    
    // 'valueRange' attribute on RangeInput (id=AuthorityLimitsProfile_Input) at UserAuthorityLimitsDV.pcf: line 22, column 50
    function verifyValueRangeIsAllowedType_4 ($$arg :  entity.AuthorityLimitProfile[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AuthorityLimitsProfile_Input) at UserAuthorityLimitsDV.pcf: line 22, column 50
    function verifyValueRangeIsAllowedType_4 ($$arg :  gw.api.database.IQueryBeanResult<entity.AuthorityLimitProfile>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AuthorityLimitsProfile_Input) at UserAuthorityLimitsDV.pcf: line 22, column 50
    function verifyValueRangeIsAllowedType_4 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=AuthorityLimitsProfile_Input) at UserAuthorityLimitsDV.pcf: line 22, column 50
    function verifyValueRange_5 () : void {
      var __valueRangeArg = getValueRange()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_4(__valueRangeArg)
    }
    
    property get customProfile () : entity.AuthorityLimitProfile {
      return getVariableValue("customProfile", 0) as entity.AuthorityLimitProfile
    }
    
    property set customProfile ($arg :  entity.AuthorityLimitProfile) {
      setVariableValue("customProfile", 0, $arg)
    }
    
    property get user () : User {
      return getRequireValue("user", 0) as User
    }
    
    property set user ($arg :  User) {
      setRequireValue("user", 0, $arg)
    }
    
    
    function getValueRange(): java.util.List <AuthorityLimitProfile> {
      var profiles = UserAuthorityLimitUtil.getNonCustomProfiles()
      profiles.add(0, customProfile)
      return profiles
    }
    
    
  }
  
  
}