package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/businesssettings/ActivityPatterns.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ActivityPatternsExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/ActivityPatterns.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ActivityPatternsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=ActivityPatterns_NewActivityPatternButton) at ActivityPatterns.pcf: line 24, column 27
    function action_2 () : void {
      NewActivityPattern.go(gw.api.web.admin.ActivityPatternsUtil.getCurrentCategory())
    }
    
    // 'action' attribute on ToolbarButton (id=ActivityPatterns_NewActivityPatternButton) at ActivityPatterns.pcf: line 24, column 27
    function action_dest_3 () : pcf.api.Destination {
      return pcf.NewActivityPattern.createDestination(gw.api.web.admin.ActivityPatternsUtil.getCurrentCategory())
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=ActivityPatterns_DeleteButton) at ActivityPatterns.pcf: line 33, column 27
    function allCheckedRowsAction_5 (CheckedValues :  entity.ActivityPattern[], CheckedValuesErrors :  java.util.Map) : void {
      gw.api.admin.BaseAdminUtil.deleteActivityPatterns(CheckedValues)
    }
    
    // 'available' attribute on ToolbarButton (id=ActivityPatterns_NewActivityPatternButton) at ActivityPatterns.pcf: line 24, column 27
    function available_1 () : java.lang.Boolean {
      return gw.api.web.admin.ActivityPatternsUtil.canAddPatternWithCategory(gw.api.web.admin.ActivityPatternsUtil.getCurrentCategory()) and perm.ActivityPattern.create
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=ActivityPatterns_DeleteButton) at ActivityPatterns.pcf: line 33, column 27
    function available_4 () : java.lang.Boolean {
      return perm.ActivityPattern.delete
    }
    
    // 'canVisit' attribute on Page (id=ActivityPatterns) at ActivityPatterns.pcf: line 8, column 68
    static function canVisit_44 () : java.lang.Boolean {
      return perm.ActivityPattern.view
    }
    
    // 'initialValue' attribute on Variable at ActivityPatterns.pcf: line 14, column 71
    function initialValue_0 () : gw.api.database.IQueryBeanResult<ActivityPattern> {
      return gw.api.database.Query.make(ActivityPattern).select()
    }
    
    // Page (id=ActivityPatterns) at ActivityPatterns.pcf: line 8, column 68
    static function parent_45 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    override property get CurrentLocation () : pcf.ActivityPatterns {
      return super.CurrentLocation as pcf.ActivityPatterns
    }
    
    property get activityPatternList () : gw.api.database.IQueryBeanResult<ActivityPattern> {
      return getVariableValue("activityPatternList", 0) as gw.api.database.IQueryBeanResult<ActivityPattern>
    }
    
    property set activityPatternList ($arg :  gw.api.database.IQueryBeanResult<ActivityPattern>) {
      setVariableValue("activityPatternList", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/ActivityPatterns.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ActivityPatternsLVExpressionsImpl extends ActivityPatternsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'filter' attribute on ToolbarFilterOption at ActivityPatterns.pcf: line 57, column 39
    function filter_7 () : gw.api.filters.IFilter {
      return filterSet.AllFilter
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at ActivityPatterns.pcf: line 59, column 68
    function filters_8 () : gw.api.filters.IFilter[] {
      return filterSet.CategoryFilters.FilterOptions
    }
    
    // 'initialValue' attribute on Variable at ActivityPatterns.pcf: line 41, column 63
    function initialValue_6 () : gw.api.web.admin.ActivityPatternFilterSet {
      return new gw.api.web.admin.ActivityPatternFilterSet()
    }
    
    // 'value' attribute on TypeKeyCell (id=ActivityClass_Cell) at ActivityPatterns.pcf: line 75, column 52
    function sortValue_10 (activityPattern :  entity.ActivityPattern) : java.lang.Object {
      return activityPattern.ActivityClass
    }
    
    // 'value' attribute on TypeKeyCell (id=Category_Cell) at ActivityPatterns.pcf: line 81, column 55
    function sortValue_11 (activityPattern :  entity.ActivityPattern) : java.lang.Object {
      return activityPattern.Category
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ActivityPatterns.pcf: line 87, column 51
    function sortValue_12 (activityPattern :  entity.ActivityPattern) : java.lang.Object {
      return activityPattern.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at ActivityPatterns.pcf: line 92, column 47
    function sortValue_13 (activityPattern :  entity.ActivityPattern) : java.lang.Object {
      return activityPattern.Priority
    }
    
    // 'value' attribute on BooleanRadioCell (id=Mandatory_Cell) at ActivityPatterns.pcf: line 96, column 52
    function sortValue_14 (activityPattern :  entity.ActivityPattern) : java.lang.Object {
      return activityPattern.Mandatory
    }
    
    // 'value' attribute on BooleanRadioCell (id=AutomatedOnly_Cell) at ActivityPatterns.pcf: line 100, column 56
    function sortValue_15 (activityPattern :  entity.ActivityPattern) : java.lang.Object {
      return activityPattern.AutomatedOnly
    }
    
    // 'value' attribute on TextCell (id=AssignableQueue_Cell) at ActivityPatterns.pcf: line 105, column 46
    function sortValue_16 (activityPattern :  entity.ActivityPattern) : java.lang.Object {
      return activityPattern.AssignableQueue_Ext
    }
    
    // 'sortBy' attribute on TextCell (id=Subject_Cell) at ActivityPatterns.pcf: line 69, column 144
    function sortValue_9 (activityPattern :  entity.ActivityPattern) : java.lang.Object {
      return activityPattern.Subject
    }
    
    // 'value' attribute on RowIterator at ActivityPatterns.pcf: line 48, column 88
    function value_43 () : gw.api.database.IQueryBeanResult<entity.ActivityPattern> {
      return activityPatternList
    }
    
    property get filterSet () : gw.api.web.admin.ActivityPatternFilterSet {
      return getVariableValue("filterSet", 1) as gw.api.web.admin.ActivityPatternFilterSet
    }
    
    property set filterSet ($arg :  gw.api.web.admin.ActivityPatternFilterSet) {
      setVariableValue("filterSet", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/businesssettings/ActivityPatterns.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ActivityPatternsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Subject_Cell) at ActivityPatterns.pcf: line 69, column 144
    function action_18 () : void {
      ActivityPatternDetail.go(activityPattern)
    }
    
    // 'action' attribute on TextCell (id=Subject_Cell) at ActivityPatterns.pcf: line 69, column 144
    function action_dest_19 () : pcf.api.Destination {
      return pcf.ActivityPatternDetail.createDestination(activityPattern)
    }
    
    // 'condition' attribute on ToolbarFlag at ActivityPatterns.pcf: line 51, column 49
    function condition_17 () : java.lang.Boolean {
      return !activityPattern.SystemPattern
    }
    
    // 'value' attribute on TypeKeyCell (id=ActivityClass_Cell) at ActivityPatterns.pcf: line 75, column 52
    function valueRoot_23 () : java.lang.Object {
      return activityPattern
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at ActivityPatterns.pcf: line 69, column 144
    function value_20 () : java.lang.String {
      return activityPattern.Subject != null ? activityPattern.Subject : DisplayKey.get("Web.ActivityPattern.NoSubject")
    }
    
    // 'value' attribute on TypeKeyCell (id=ActivityClass_Cell) at ActivityPatterns.pcf: line 75, column 52
    function value_22 () : typekey.ActivityClass {
      return activityPattern.ActivityClass
    }
    
    // 'value' attribute on TypeKeyCell (id=Category_Cell) at ActivityPatterns.pcf: line 81, column 55
    function value_25 () : typekey.ActivityCategory {
      return activityPattern.Category
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at ActivityPatterns.pcf: line 87, column 51
    function value_28 () : typekey.ActivityType {
      return activityPattern.Type
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at ActivityPatterns.pcf: line 92, column 47
    function value_31 () : typekey.Priority {
      return activityPattern.Priority
    }
    
    // 'value' attribute on BooleanRadioCell (id=Mandatory_Cell) at ActivityPatterns.pcf: line 96, column 52
    function value_34 () : java.lang.Boolean {
      return activityPattern.Mandatory
    }
    
    // 'value' attribute on BooleanRadioCell (id=AutomatedOnly_Cell) at ActivityPatterns.pcf: line 100, column 56
    function value_37 () : java.lang.Boolean {
      return activityPattern.AutomatedOnly
    }
    
    // 'value' attribute on TextCell (id=AssignableQueue_Cell) at ActivityPatterns.pcf: line 105, column 46
    function value_40 () : AssignableQueue {
      return activityPattern.AssignableQueue_Ext
    }
    
    property get activityPattern () : entity.ActivityPattern {
      return getIteratedValue(2) as entity.ActivityPattern
    }
    
    
  }
  
  
}