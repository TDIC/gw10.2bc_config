package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/Admin.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/Admin.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Admin.pcf: line 15, column 38
    function action_10 () : void {
      pcf.UsersAndSecurity.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Admin.pcf: line 17, column 38
    function action_40 () : void {
      pcf.BusinessSettings.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Admin.pcf: line 19, column 32
    function action_48 () : void {
      pcf.Monitoring.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Admin.pcf: line 21, column 31
    function action_66 () : void {
      pcf.Utilities.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Admin.pcf: line 23, column 32
    function action_68 () : void {
      pcf.AdminAudit.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Admin.pcf: line 15, column 38
    function action_dest_11 () : pcf.api.Destination {
      return pcf.UsersAndSecurity.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Admin.pcf: line 17, column 38
    function action_dest_41 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Admin.pcf: line 19, column 32
    function action_dest_49 () : pcf.api.Destination {
      return pcf.Monitoring.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Admin.pcf: line 21, column 31
    function action_dest_67 () : pcf.api.Destination {
      return pcf.Utilities.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Admin.pcf: line 23, column 32
    function action_dest_69 () : pcf.api.Destination {
      return pcf.AdminAudit.createDestination()
    }
    
    // 'canVisit' attribute on LocationGroup (id=Admin) at Admin.pcf: line 11, column 57
    static function canVisit_70 () : java.lang.Boolean {
      return perm.System.admintabview
    }
    
    // LocationGroup (id=Admin) at Admin.pcf: line 11, column 57
    static function firstVisitableChildDestinationMethod_77 () : pcf.api.Destination {
      var dest : pcf.api.Destination
      dest = pcf.UsersAndSecurity.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.BusinessSettings.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.Monitoring.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.Utilities.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.AdminAudit.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      return null
    }
    
    // 'infoBar' attribute on LocationGroup (id=Admin) at Admin.pcf: line 11, column 57
    function infoBar_onEnter_71 (def :  pcf.CurrentDateInfoBar) : void {
      def.onEnter()
    }
    
    // 'infoBar' attribute on LocationGroup (id=Admin) at Admin.pcf: line 11, column 57
    function infoBar_refreshVariables_72 (def :  pcf.CurrentDateInfoBar) : void {
      def.refreshVariables()
    }
    
    // 'menuActions' attribute on LocationGroup (id=Admin) at Admin.pcf: line 11, column 57
    function menuActions_onEnter_73 (def :  pcf.AdminMenuActions) : void {
      def.onEnter()
    }
    
    // 'menuActions' attribute on LocationGroup (id=Admin) at Admin.pcf: line 11, column 57
    function menuActions_refreshVariables_74 (def :  pcf.AdminMenuActions) : void {
      def.refreshVariables()
    }
    
    // 'tabBar' attribute on LocationGroup (id=Admin) at Admin.pcf: line 11, column 57
    function tabBar_onEnter_75 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on LocationGroup (id=Admin) at Admin.pcf: line 11, column 57
    function tabBar_refreshVariables_76 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.Admin {
      return super.CurrentLocation as pcf.Admin
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/Admin.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationGroupMenuItem2ExpressionsImpl extends AdminExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 10, column 38
    function action_12 () : void {
      pcf.ActivityPatterns.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 12, column 37
    function action_14 () : void {
      pcf.AgencyBillPlans.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 14, column 34
    function action_16 () : void {
      pcf.BillingPlans.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 16, column 50
    function action_18 () : void {
      pcf.ChargeBreakdownCategoryTypes.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 18, column 36
    function action_20 () : void {
      pcf.ChargePatterns.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 20, column 40
    function action_22 () : void {
      pcf.CollectionAgencies.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 22, column 37
    function action_24 () : void {
      pcf.CommissionPlans.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 24, column 38
    function action_26 () : void {
      pcf.DelinquencyPlans.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 26, column 40
    function action_28 () : void {
      pcf.TDIC_GLIntegration.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 28, column 30
    function action_30 () : void {
      pcf.Holidays.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 30, column 44
    function action_32 () : void {
      pcf.PaymentAllocationPlans.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 32, column 34
    function action_34 () : void {
      pcf.PaymentPlans.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 34, column 40
    function action_36 () : void {
      pcf.ReturnPremiumPlans.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 36, column 39
    function action_38 () : void {
      pcf.ClearOpenHandlers.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 10, column 38
    function action_dest_13 () : pcf.api.Destination {
      return pcf.ActivityPatterns.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 12, column 37
    function action_dest_15 () : pcf.api.Destination {
      return pcf.AgencyBillPlans.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 14, column 34
    function action_dest_17 () : pcf.api.Destination {
      return pcf.BillingPlans.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 16, column 50
    function action_dest_19 () : pcf.api.Destination {
      return pcf.ChargeBreakdownCategoryTypes.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 18, column 36
    function action_dest_21 () : pcf.api.Destination {
      return pcf.ChargePatterns.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 20, column 40
    function action_dest_23 () : pcf.api.Destination {
      return pcf.CollectionAgencies.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 22, column 37
    function action_dest_25 () : pcf.api.Destination {
      return pcf.CommissionPlans.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 24, column 38
    function action_dest_27 () : pcf.api.Destination {
      return pcf.DelinquencyPlans.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 26, column 40
    function action_dest_29 () : pcf.api.Destination {
      return pcf.TDIC_GLIntegration.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 28, column 30
    function action_dest_31 () : pcf.api.Destination {
      return pcf.Holidays.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 30, column 44
    function action_dest_33 () : pcf.api.Destination {
      return pcf.PaymentAllocationPlans.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 32, column 34
    function action_dest_35 () : pcf.api.Destination {
      return pcf.PaymentPlans.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 34, column 40
    function action_dest_37 () : pcf.api.Destination {
      return pcf.ReturnPremiumPlans.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at BusinessSettings.pcf: line 36, column 39
    function action_dest_39 () : pcf.api.Destination {
      return pcf.ClearOpenHandlers.createDestination()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/Admin.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationGroupMenuItem3ExpressionsImpl extends LocationGroupMenuItem2ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/Admin.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationGroupMenuItem4ExpressionsImpl extends AdminExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Monitoring.pcf: line 11, column 53
    function action_42 () : void {
      pcf.MessagingDestinationControlList.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Monitoring.pcf: line 14, column 36
    function action_44 () : void {
      pcf.WorkflowSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Monitoring.pcf: line 16, column 35
    function action_46 () : void {
      pcf.WorkflowStats.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Monitoring.pcf: line 11, column 53
    function action_dest_43 () : pcf.api.Destination {
      return pcf.MessagingDestinationControlList.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Monitoring.pcf: line 14, column 36
    function action_dest_45 () : pcf.api.Destination {
      return pcf.WorkflowSearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Monitoring.pcf: line 16, column 35
    function action_dest_47 () : pcf.api.Destination {
      return pcf.WorkflowStats.createDestination()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/Admin.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationGroupMenuItem5ExpressionsImpl extends AdminExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Utilities.pcf: line 12, column 21
    function action_50 () : void {
      pcf.ImportWizard.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Utilities.pcf: line 16, column 21
    function action_52 () : void {
      pcf.ExportData.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Utilities.pcf: line 18, column 42
    function action_54 () : void {
      pcf.ScriptParametersPage.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Utilities.pcf: line 20, column 36
    function action_56 () : void {
      pcf.DataChangePage.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Utilities.pcf: line 22, column 32
    function action_58 () : void {
      pcf.Properties.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Utilities.pcf: line 24, column 39
    function action_60 () : void {
      pcf.InboundFileSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Utilities.pcf: line 26, column 40
    function action_62 () : void {
      pcf.OutboundFileSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Utilities.pcf: line 29, column 52
    function action_64 () : void {
      pcf.Import_AddressBookUID_Mappings.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Utilities.pcf: line 12, column 21
    function action_dest_51 () : pcf.api.Destination {
      return pcf.ImportWizard.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Utilities.pcf: line 16, column 21
    function action_dest_53 () : pcf.api.Destination {
      return pcf.ExportData.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Utilities.pcf: line 18, column 42
    function action_dest_55 () : pcf.api.Destination {
      return pcf.ScriptParametersPage.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Utilities.pcf: line 20, column 36
    function action_dest_57 () : pcf.api.Destination {
      return pcf.DataChangePage.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Utilities.pcf: line 22, column 32
    function action_dest_59 () : pcf.api.Destination {
      return pcf.Properties.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Utilities.pcf: line 24, column 39
    function action_dest_61 () : pcf.api.Destination {
      return pcf.InboundFileSearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Utilities.pcf: line 26, column 40
    function action_dest_63 () : pcf.api.Destination {
      return pcf.OutboundFileSearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at Utilities.pcf: line 29, column 52
    function action_dest_65 () : pcf.api.Destination {
      return pcf.Import_AddressBookUID_Mappings.createDestination()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/Admin.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationGroupMenuItemExpressionsImpl extends AdminExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at UsersAndSecurity.pcf: line 11, column 32
    function action_0 () : void {
      pcf.UserSearch.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at UsersAndSecurity.pcf: line 13, column 28
    function action_2 () : void {
      pcf.Groups.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at UsersAndSecurity.pcf: line 15, column 27
    function action_4 () : void {
      pcf.Roles.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at UsersAndSecurity.pcf: line 17, column 35
    function action_6 () : void {
      pcf.SecurityZones.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at UsersAndSecurity.pcf: line 19, column 44
    function action_8 () : void {
      pcf.AuthorityLimitProfiles.go()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at UsersAndSecurity.pcf: line 11, column 32
    function action_dest_1 () : pcf.api.Destination {
      return pcf.UserSearch.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at UsersAndSecurity.pcf: line 13, column 28
    function action_dest_3 () : pcf.api.Destination {
      return pcf.Groups.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at UsersAndSecurity.pcf: line 15, column 27
    function action_dest_5 () : pcf.api.Destination {
      return pcf.Roles.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at UsersAndSecurity.pcf: line 17, column 35
    function action_dest_7 () : pcf.api.Destination {
      return pcf.SecurityZones.createDestination()
    }
    
    // 'location' attribute on LocationGroup (id=Admin) at UsersAndSecurity.pcf: line 19, column 44
    function action_dest_9 () : pcf.api.Destination {
      return pcf.AuthorityLimitProfiles.createDestination()
    }
    
    
  }
  
  
}