package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses java.util.ArrayList
@javax.annotation.Generated("config/web/pcf/admin/invoice/ReturnPremiumPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ReturnPremiumPlanDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/invoice/ReturnPremiumPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends ReturnPremiumPlanDetailScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=MoveUp) at ReturnPremiumPlanDetailScreen.pcf: line 178, column 118
    function action_57 () : void {
      moveUp(returnPremiumScheme)
    }
    
    // 'action' attribute on Link (id=MoveDown) at ReturnPremiumPlanDetailScreen.pcf: line 186, column 61
    function action_60 () : void {
      moveDown(returnPremiumScheme)
    }
    
    // 'available' attribute on Link (id=MoveUp) at ReturnPremiumPlanDetailScreen.pcf: line 178, column 118
    function available_55 () : java.lang.Boolean {
      return perm.System.retpremplanedit && CurrentLocation.InEditMode
    }
    
    // 'condition' attribute on ToolbarFlag at ReturnPremiumPlanDetailScreen.pcf: line 118, column 30
    function condition_31 () : java.lang.Boolean {
      return returnPremiumScheme.HandlingCondition != TC_OTHER
    }
    
    // 'condition' attribute on ToolbarFlag at ReturnPremiumPlanDetailScreen.pcf: line 121, column 32
    function condition_32 () : java.lang.Boolean {
      return availableConditions().Count > 0
    }
    
    // 'value' attribute on TypeKeyCell (id=StartDateOption_Cell) at ReturnPremiumPlanDetailScreen.pcf: line 143, column 29
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      returnPremiumScheme.StartDateOption = (__VALUE_TO_SET as typekey.ReturnPremiumStartDateOption)
    }
    
    // 'value' attribute on TypeKeyCell (id=AllocateTiming_Cell) at ReturnPremiumPlanDetailScreen.pcf: line 151, column 29
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      returnPremiumScheme.AllocateTiming = (__VALUE_TO_SET as typekey.ReturnPremiumAllocateTiming)
    }
    
    // 'value' attribute on TypeKeyCell (id=AllocateMethod_Cell) at ReturnPremiumPlanDetailScreen.pcf: line 159, column 29
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      returnPremiumScheme.AllocateMethod = (__VALUE_TO_SET as typekey.ReturnPremiumAllocateMethod)
    }
    
    // 'value' attribute on TypeKeyCell (id=ExcessTreatment_Cell) at ReturnPremiumPlanDetailScreen.pcf: line 167, column 29
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      returnPremiumScheme.ExcessTreatment = (__VALUE_TO_SET as typekey.ReturnPremiumExcessTreatment)
    }
    
    // 'value' attribute on TextCell (id=Priority_Cell) at ReturnPremiumPlanDetailScreen.pcf: line 128, column 46
    function valueRoot_34 () : java.lang.Object {
      return returnPremiumScheme
    }
    
    // 'value' attribute on TextCell (id=Condition_Cell) at ReturnPremiumPlanDetailScreen.pcf: line 135, column 67
    function valueRoot_37 () : java.lang.Object {
      return returnPremiumScheme.HandlingCondition
    }
    
    // 'value' attribute on TextCell (id=Priority_Cell) at ReturnPremiumPlanDetailScreen.pcf: line 128, column 46
    function value_33 () : java.lang.Integer {
      return returnPremiumScheme.Priority
    }
    
    // 'value' attribute on TextCell (id=Condition_Cell) at ReturnPremiumPlanDetailScreen.pcf: line 135, column 67
    function value_36 () : typekey.ReturnPremiumHandlingCondition {
      return returnPremiumScheme.HandlingCondition.Value
    }
    
    // 'value' attribute on TypeKeyCell (id=StartDateOption_Cell) at ReturnPremiumPlanDetailScreen.pcf: line 143, column 29
    function value_39 () : typekey.ReturnPremiumStartDateOption {
      return returnPremiumScheme.StartDateOption
    }
    
    // 'value' attribute on TypeKeyCell (id=AllocateTiming_Cell) at ReturnPremiumPlanDetailScreen.pcf: line 151, column 29
    function value_43 () : typekey.ReturnPremiumAllocateTiming {
      return returnPremiumScheme.AllocateTiming
    }
    
    // 'value' attribute on TypeKeyCell (id=AllocateMethod_Cell) at ReturnPremiumPlanDetailScreen.pcf: line 159, column 29
    function value_47 () : typekey.ReturnPremiumAllocateMethod {
      return returnPremiumScheme.AllocateMethod
    }
    
    // 'value' attribute on TypeKeyCell (id=ExcessTreatment_Cell) at ReturnPremiumPlanDetailScreen.pcf: line 167, column 29
    function value_51 () : typekey.ReturnPremiumExcessTreatment {
      return returnPremiumScheme.ExcessTreatment
    }
    
    // 'visible' attribute on Link (id=MoveUp) at ReturnPremiumPlanDetailScreen.pcf: line 178, column 118
    function visible_56 () : java.lang.Boolean {
      return returnPremiumScheme.Priority > 1 and returnPremiumScheme.HandlingCondition != TC_OTHER
    }
    
    // 'visible' attribute on Link (id=MoveDown) at ReturnPremiumPlanDetailScreen.pcf: line 186, column 61
    function visible_59 () : java.lang.Boolean {
      return canMoveDown(returnPremiumScheme)
    }
    
    property get returnPremiumScheme () : entity.ReturnPremiumHandlingScheme {
      return getIteratedValue(1) as entity.ReturnPremiumHandlingScheme
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/invoice/ReturnPremiumPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ReturnPremiumPlanDetailScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=AddSchemeMenuItem) at ReturnPremiumPlanDetailScreen.pcf: line 95, column 68
    function label_27 () : java.lang.Object {
      return schemeConditionSelector
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AddSchemeMenuItem) at ReturnPremiumPlanDetailScreen.pcf: line 95, column 68
    function toCreateAndAdd_28 (CheckedValues :  Object[]) : java.lang.Object {
      return addScheme(schemeConditionSelector)
    }
    
    property get schemeConditionSelector () : typekey.ReturnPremiumHandlingCondition {
      return getIteratedValue(1) as typekey.ReturnPremiumHandlingCondition
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/invoice/ReturnPremiumPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ReturnPremiumPlanDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=Clone) at ReturnPremiumPlanDetailScreen.pcf: line 21, column 50
    function action_3 () : void {
      CloneReturnPremiumPlan.go(returnPremiumPlan)
    }
    
    // 'action' attribute on ToolbarButton (id=Clone) at ReturnPremiumPlanDetailScreen.pcf: line 21, column 50
    function action_dest_4 () : pcf.api.Destination {
      return pcf.CloneReturnPremiumPlan.createDestination(returnPremiumPlan)
    }
    
    // 'def' attribute on PanelRef at ReturnPremiumPlanDetailScreen.pcf: line 193, column 237
    function def_onEnter_63 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(returnPremiumPlan, { "Name", "Description" }, { DisplayKey.get("Web.ReturnPremiumPlanDetailDV.Name"), DisplayKey.get("Web.ReturnPremiumPlanDetailDV.Description") })
    }
    
    // 'def' attribute on PanelRef at ReturnPremiumPlanDetailScreen.pcf: line 193, column 237
    function def_refreshVariables_64 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(returnPremiumPlan, { "Name", "Description" }, { DisplayKey.get("Web.ReturnPremiumPlanDetailDV.Name"), DisplayKey.get("Web.ReturnPremiumPlanDetailDV.Description") })
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at ReturnPremiumPlanDetailScreen.pcf: line 42, column 49
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      returnPremiumPlan.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at ReturnPremiumPlanDetailScreen.pcf: line 50, column 52
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      returnPremiumPlan.EffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at ReturnPremiumPlanDetailScreen.pcf: line 56, column 53
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      returnPremiumPlan.ExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=ChargeQualification_Input) at ReturnPremiumPlanDetailScreen.pcf: line 63, column 65
    function defaultSetter_24 (__VALUE_TO_SET :  java.lang.Object) : void {
      returnPremiumPlan.ChargeQualification = (__VALUE_TO_SET as typekey.ReturnPremiumChargeQualification)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at ReturnPremiumPlanDetailScreen.pcf: line 35, column 42
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      returnPremiumPlan.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at ReturnPremiumPlanDetailScreen.pcf: line 13, column 23
    function initialValue_0 () : boolean {
      return gw.api.util.LocaleUtil.getAllLanguages().Count > 1
    }
    
    // EditButtons at ReturnPremiumPlanDetailScreen.pcf: line 15, column 21
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'value' attribute on TextCell (id=Priority_Cell) at ReturnPremiumPlanDetailScreen.pcf: line 128, column 46
    function sortValue_30 (returnPremiumScheme :  entity.ReturnPremiumHandlingScheme) : java.lang.Object {
      return returnPremiumScheme.Priority
    }
    
    // 'toRemove' attribute on RowIterator (id=HandlingSchemesIterator) at ReturnPremiumPlanDetailScreen.pcf: line 115, column 80
    function toRemove_61 (returnPremiumScheme :  entity.ReturnPremiumHandlingScheme) : void {
      if (returnPremiumScheme.HandlingCondition != TC_OTHER) returnPremiumPlan.removeHandlingScheme(returnPremiumScheme)
    }
    
    // 'validationExpression' attribute on DateInput (id=ExpirationDate_Input) at ReturnPremiumPlanDetailScreen.pcf: line 56, column 53
    function validationExpression_17 () : java.lang.Object {
      return returnPremiumPlan.hasValidExpirationDate() ? null : DisplayKey.get("Web.Plan.ExpirationDate.ValidationError")
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at ReturnPremiumPlanDetailScreen.pcf: line 35, column 42
    function valueRoot_7 () : java.lang.Object {
      return returnPremiumPlan
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at ReturnPremiumPlanDetailScreen.pcf: line 50, column 52
    function value_13 () : java.util.Date {
      return returnPremiumPlan.EffectiveDate
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at ReturnPremiumPlanDetailScreen.pcf: line 56, column 53
    function value_18 () : java.util.Date {
      return returnPremiumPlan.ExpirationDate
    }
    
    // 'value' attribute on TypeKeyInput (id=ChargeQualification_Input) at ReturnPremiumPlanDetailScreen.pcf: line 63, column 65
    function value_23 () : typekey.ReturnPremiumChargeQualification {
      return returnPremiumPlan.ChargeQualification
    }
    
    // 'value' attribute on AddMenuItemIterator (id=schemeCondition) at ReturnPremiumPlanDetailScreen.pcf: line 90, column 86
    function value_29 () : java.util.List<typekey.ReturnPremiumHandlingCondition> {
      return availableConditions()
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at ReturnPremiumPlanDetailScreen.pcf: line 35, column 42
    function value_5 () : java.lang.String {
      return returnPremiumPlan.Name
    }
    
    // 'value' attribute on RowIterator (id=HandlingSchemesIterator) at ReturnPremiumPlanDetailScreen.pcf: line 115, column 80
    function value_62 () : java.util.List<entity.ReturnPremiumHandlingScheme> {
      return java.util.Arrays.asList(returnPremiumPlan.getReturnPremiumHandlingSchemes())
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at ReturnPremiumPlanDetailScreen.pcf: line 42, column 49
    function value_9 () : java.lang.String {
      return returnPremiumPlan.Description
    }
    
    // 'visible' attribute on ToolbarButton (id=Clone) at ReturnPremiumPlanDetailScreen.pcf: line 21, column 50
    function visible_2 () : java.lang.Boolean {
      return perm.System.retpremplancreate
    }
    
    property get hasMultipleLanguages () : boolean {
      return getVariableValue("hasMultipleLanguages", 0) as java.lang.Boolean
    }
    
    property set hasMultipleLanguages ($arg :  boolean) {
      setVariableValue("hasMultipleLanguages", 0, $arg)
    }
    
    property get returnPremiumPlan () : ReturnPremiumPlan {
      return getRequireValue("returnPremiumPlan", 0) as ReturnPremiumPlan
    }
    
    property set returnPremiumPlan ($arg :  ReturnPremiumPlan) {
      setRequireValue("returnPremiumPlan", 0, $arg)
    }
    
    
    function addScheme(identifyingCondition : ReturnPremiumHandlingCondition) : ReturnPremiumHandlingScheme {
        return returnPremiumPlan.addHandlingSchemeFor(identifyingCondition)
    }
    
    function availableConditions() : java.util.List<ReturnPremiumHandlingCondition> {
        // don't include those for existing schemes because schemes are unique by condition...
        var conditions = new ArrayList<ReturnPremiumHandlingCondition>(ReturnPremiumHandlingCondition.getTypeKeys(false))
        conditions.removeAll(returnPremiumPlan.ReturnPremiumHandlingSchemes.toList().map(\ scheme -> scheme.HandlingCondition))
        return conditions
    }
    
    function moveUp(returnPremiumScheme : ReturnPremiumHandlingScheme) {
        returnPremiumPlan.increaseHandlingSchemePriority(returnPremiumScheme)
    }
    
    function moveDown(returnPremiumScheme : ReturnPremiumHandlingScheme) {
        returnPremiumPlan.decreaseHandlingSchemePriority(returnPremiumScheme)
    }
    
    function canMoveDown(returnPremiumScheme : ReturnPremiumHandlingScheme) : boolean {
      final var schemes = returnPremiumPlan.ReturnPremiumHandlingSchemes
      final var defaultSchemePriority = returnPremiumPlan.DefaultHandlingSchemePriority
      return schemes.Count > 2 and returnPremiumScheme.Priority < defaultSchemePriority - 1
        // there is a scheme with lower priority that is not the default
        // (which is the absolute lowest)
        and schemes.hasMatch(\ scheme -> scheme.Priority != defaultSchemePriority
              and scheme.Priority > returnPremiumScheme.Priority)
    }
    
    
  }
  
  
}