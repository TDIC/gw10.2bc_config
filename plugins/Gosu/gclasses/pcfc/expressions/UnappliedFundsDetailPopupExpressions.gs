package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/UnappliedFundsDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class UnappliedFundsDetailPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/UnappliedFundsDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=UnappliedFundName_Cell) at UnappliedFundsDetailPopup.pcf: line 31, column 52
    function valueRoot_5 () : java.lang.Object {
      return unappliedFund
    }
    
    // 'value' attribute on TextCell (id=UnappliedFundName_Cell) at UnappliedFundsDetailPopup.pcf: line 31, column 52
    function value_4 () : java.lang.String {
      return unappliedFund.DisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnappliedFundValue_Cell) at UnappliedFundsDetailPopup.pcf: line 36, column 48
    function value_7 () : gw.pl.currency.MonetaryAmount {
      return unappliedFund.Balance
    }
    
    property get unappliedFund () : entity.UnappliedFund {
      return getIteratedValue(2) as entity.UnappliedFund
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/UnappliedFundsDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends UnappliedFundsDetailPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at UnappliedFundsDetailPopup.pcf: line 46, column 58
    function def_onEnter_11 (def :  pcf.UnappliedFundsDetailLV) : void {
      def.onEnter(selectedFund)
    }
    
    // 'def' attribute on PanelRef at UnappliedFundsDetailPopup.pcf: line 46, column 58
    function def_refreshVariables_12 (def :  pcf.UnappliedFundsDetailLV) : void {
      def.refreshVariables(selectedFund)
    }
    
    // 'value' attribute on TextCell (id=UnappliedFundName_Cell) at UnappliedFundsDetailPopup.pcf: line 31, column 52
    function sortValue_0 (unappliedFund :  entity.UnappliedFund) : java.lang.Object {
      return unappliedFund.DisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnappliedFundValue_Cell) at UnappliedFundsDetailPopup.pcf: line 36, column 48
    function sortValue_1 (unappliedFund :  entity.UnappliedFund) : java.lang.Object {
      return unappliedFund.Balance
    }
    
    // '$$sumValue' attribute on RowIterator at UnappliedFundsDetailPopup.pcf: line 36, column 48
    function sumValueRoot_3 (unappliedFund :  entity.UnappliedFund) : java.lang.Object {
      return unappliedFund
    }
    
    // 'footerSumValue' attribute on RowIterator at UnappliedFundsDetailPopup.pcf: line 36, column 48
    function sumValue_2 (unappliedFund :  entity.UnappliedFund) : java.lang.Object {
      return unappliedFund.Balance
    }
    
    // 'title' attribute on Card (id=FundPanel) at UnappliedFundsDetailPopup.pcf: line 44, column 83
    function title_14 () : java.lang.String {
      return selectedFund.DisplayName
    }
    
    // 'value' attribute on RowIterator at UnappliedFundsDetailPopup.pcf: line 25, column 68
    function value_10 () : java.util.List<entity.UnappliedFund> {
      return account.UnappliedFundsSortedByDisplayName
    }
    
    // 'visible' attribute on Card (id=FundPanel) at UnappliedFundsDetailPopup.pcf: line 44, column 83
    function visible_13 () : java.lang.Boolean {
      return gw.api.domain.fundstracking.FundsTrackingSwitch.isEnabled()
    }
    
    property get selectedFund () : UnappliedFund {
      return getCurrentSelection(1) as UnappliedFund
    }
    
    property set selectedFund ($arg :  UnappliedFund) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/UnappliedFundsDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class UnappliedFundsDetailPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'afterEnter' attribute on Popup (id=UnappliedFundsDetailPopup) at UnappliedFundsDetailPopup.pcf: line 8, column 72
    function afterEnter_15 () : void {
      gw.pcf.fundstracking.AccountDetailFundsTrackingHelper.allotFundsOnEnter(account)
    }
    
    override property get CurrentLocation () : pcf.UnappliedFundsDetailPopup {
      return super.CurrentLocation as pcf.UnappliedFundsDetailPopup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  
}