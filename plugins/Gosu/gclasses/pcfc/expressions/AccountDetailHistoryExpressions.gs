package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
@javax.annotation.Generated("config/web/pcf/account/AccountDetailHistory.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailHistoryExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailHistory.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailHistoryExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=AccountDetailHistory) at AccountDetailHistory.pcf: line 9, column 72
    static function canVisit_41 (account :  Account) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.accthistview
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountDetailHistory.pcf: line 30, column 85
    function filter_0 () : gw.api.filters.IFilter {
      return new gw.api.web.history.AccountHistoriesFilters.Last30Days()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountDetailHistory.pcf: line 32, column 85
    function filter_1 () : gw.api.filters.IFilter {
      return new gw.api.web.history.AccountHistoriesFilters.Last60Days()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountDetailHistory.pcf: line 34, column 85
    function filter_2 () : gw.api.filters.IFilter {
      return new gw.api.web.history.AccountHistoriesFilters.Last90Days()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountDetailHistory.pcf: line 36, column 86
    function filter_3 () : gw.api.filters.IFilter {
      return new gw.api.web.history.AccountHistoriesFilters.Last120Days()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountDetailHistory.pcf: line 39, column 37
    function filter_4 () : gw.api.filters.IFilter {
      return new gw.api.web.history.AccountHistoriesFilters.Last180Days()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountDetailHistory.pcf: line 41, column 83
    function filter_5 () : gw.api.filters.IFilter {
      return new gw.api.web.history.AccountHistoriesFilters.LastYear()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountDetailHistory.pcf: line 43, column 85
    function filter_6 () : gw.api.filters.IFilter {
      return new gw.api.web.history.AccountHistoriesFilters.Last3Years()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountDetailHistory.pcf: line 45, column 78
    function filter_7 () : gw.api.filters.IFilter {
      return new gw.api.web.history.AccountHistoriesFilters.All()
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at AccountDetailHistory.pcf: line 51, column 143
    function filters_8 () : gw.api.filters.IFilter[] {
      return new gw.api.filters.TypeKeyFilterSet( History.Type.TypeInfo.getProperty( "EventType" ) ).getFilterOptions()
    }
    
    // Page (id=AccountDetailHistory) at AccountDetailHistory.pcf: line 9, column 72
    static function parent_42 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    // 'value' attribute on TextCell (id=Transaction_Cell) at AccountDetailHistory.pcf: line 64, column 69
    function sortValue_10 (accountHistory :  entity.AccountHistory) : java.lang.Object {
      return accountHistory.Transaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AccountDetailHistory.pcf: line 72, column 51
    function sortValue_11 (accountHistory :  entity.AccountHistory) : java.lang.Object {
      return accountHistory.Description
    }
    
    // 'value' attribute on TextCell (id=RefNumber_Cell) at AccountDetailHistory.pcf: line 77, column 49
    function sortValue_12 (accountHistory :  entity.AccountHistory) : java.lang.Object {
      return accountHistory.RefNumber
    }
    
    // 'value' attribute on TextCell (id=User_Cell) at AccountDetailHistory.pcf: line 92, column 29
    function sortValue_13 (accountHistory :  entity.AccountHistory) : java.lang.Object {
      return accountHistory.User
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AccountDetailHistory.pcf: line 58, column 49
    function sortValue_9 (accountHistory :  entity.AccountHistory) : java.lang.Object {
      return accountHistory.EventDate
    }
    
    // 'value' attribute on RowIterator at AccountDetailHistory.pcf: line 25, column 85
    function value_40 () : gw.api.database.IQueryBeanResult<entity.AccountHistory> {
      return getAccountHistories()
    }
    
    override property get CurrentLocation () : pcf.AccountDetailHistory {
      return super.CurrentLocation as pcf.AccountDetailHistory
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
    function goToPolicyTransferDetail(item : AccountHistory) {
      PolicyTransferDetail.go(item as PolicyHistory);
    }
    
    function isPolicyTransfer(item : AccountHistory) : Boolean {
      return (item.EventType == TC_POLICYTRANSFERRED)
    }
    
    function getAccountHistories() : IQueryBeanResult<AccountHistory> {
      var accountQuery = Query.make(AccountHistory)
      accountQuery.compare("Subtype", NotEquals, typekey.History.TC_POLICYHISTORY)
      accountQuery.or(\ restriction -> {
        restriction.compare("Account", Equals, account)
        restriction.compare("OtherAccount", Equals, account)
      })  
      return accountQuery.select()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailHistory.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountDetailHistoryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Transaction_Cell) at AccountDetailHistory.pcf: line 64, column 69
    function action_17 () : void {
      TransactionDetailPopup.push(accountHistory.Transaction)
    }
    
    // 'action' attribute on TextCell (id=Description_Cell) at AccountDetailHistory.pcf: line 72, column 51
    function action_23 () : void {
      goToPolicyTransferDetail(accountHistory)
    }
    
    // 'action' attribute on TextCell (id=User_Cell) at AccountDetailHistory.pcf: line 92, column 29
    function action_35 () : void {
      UserDetailPage.push(accountHistory.User)
    }
    
    // 'action' attribute on TextCell (id=Transaction_Cell) at AccountDetailHistory.pcf: line 64, column 69
    function action_dest_18 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(accountHistory.Transaction)
    }
    
    // 'action' attribute on TextCell (id=User_Cell) at AccountDetailHistory.pcf: line 92, column 29
    function action_dest_36 () : pcf.api.Destination {
      return pcf.UserDetailPage.createDestination(accountHistory.User)
    }
    
    // 'available' attribute on TextCell (id=Description_Cell) at AccountDetailHistory.pcf: line 72, column 51
    function available_22 () : java.lang.Boolean {
      return isPolicyTransfer(accountHistory)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=TransactionAmount_Cell) at AccountDetailHistory.pcf: line 85, column 46
    function currency_33 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AccountDetailHistory.pcf: line 58, column 49
    function valueRoot_15 () : java.lang.Object {
      return accountHistory
    }
    
    // 'value' attribute on TextCell (id=Transaction_Cell) at AccountDetailHistory.pcf: line 64, column 69
    function valueRoot_20 () : java.lang.Object {
      return accountHistory.Transaction
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AccountDetailHistory.pcf: line 58, column 49
    function value_14 () : java.util.Date {
      return accountHistory.EventDate
    }
    
    // 'value' attribute on TextCell (id=Transaction_Cell) at AccountDetailHistory.pcf: line 64, column 69
    function value_19 () : java.lang.String {
      return accountHistory.Transaction.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AccountDetailHistory.pcf: line 72, column 51
    function value_24 () : java.lang.String {
      return accountHistory.Description
    }
    
    // 'value' attribute on TextCell (id=RefNumber_Cell) at AccountDetailHistory.pcf: line 77, column 49
    function value_28 () : java.lang.String {
      return accountHistory.RefNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TransactionAmount_Cell) at AccountDetailHistory.pcf: line 85, column 46
    function value_31 () : gw.pl.currency.MonetaryAmount {
      return accountHistory.Amount
    }
    
    // 'value' attribute on TextCell (id=User_Cell) at AccountDetailHistory.pcf: line 92, column 29
    function value_37 () : entity.User {
      return accountHistory.User
    }
    
    property get accountHistory () : entity.AccountHistory {
      return getIteratedValue(1) as entity.AccountHistory
    }
    
    
  }
  
  
}