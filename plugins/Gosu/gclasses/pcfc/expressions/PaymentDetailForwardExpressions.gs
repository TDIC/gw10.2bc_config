package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/PaymentDetailForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentDetailForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/PaymentDetailForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentDetailForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (paymentReceipt :  PaymentReceipt) : int {
      return 0
    }
    
    // 'action' attribute on ForwardCondition at PaymentDetailForward.pcf: line 14, column 58
    function action_0 () : void {
      OutgoingPaymentDetailPopup.push(paymentReceipt as OutgoingPayment)
    }
    
    // 'action' attribute on ForwardCondition at PaymentDetailForward.pcf: line 16, column 58
    function action_3 () : void {
      PaymentDetailsPopup.push(paymentReceipt)
    }
    
    // 'action' attribute on ForwardCondition at PaymentDetailForward.pcf: line 14, column 58
    function action_dest_1 () : pcf.api.Destination {
      return pcf.OutgoingPaymentDetailPopup.createDestination(paymentReceipt as OutgoingPayment)
    }
    
    // 'action' attribute on ForwardCondition at PaymentDetailForward.pcf: line 16, column 58
    function action_dest_4 () : pcf.api.Destination {
      return pcf.PaymentDetailsPopup.createDestination(paymentReceipt)
    }
    
    // 'condition' attribute on ForwardCondition at PaymentDetailForward.pcf: line 14, column 58
    function condition_2 () : java.lang.Boolean {
      return paymentReceipt typeis OutgoingPayment
    }
    
    override property get CurrentLocation () : pcf.PaymentDetailForward {
      return super.CurrentLocation as pcf.PaymentDetailForward
    }
    
    property get paymentReceipt () : PaymentReceipt {
      return getVariableValue("paymentReceipt", 0) as PaymentReceipt
    }
    
    property set paymentReceipt ($arg :  PaymentReceipt) {
      setVariableValue("paymentReceipt", 0, $arg)
    }
    
    
  }
  
  
}