package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentBonusDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewCommissionPaymentBonusDVExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentBonusDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewCommissionPaymentBonusDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=PolicyPicker) at NewCommissionPaymentBonusDV.pcf: line 48, column 41
    function action_13 () : void {
      PolicySearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=PolicyPicker) at NewCommissionPaymentBonusDV.pcf: line 48, column 41
    function action_dest_14 () : pcf.api.Destination {
      return pcf.PolicySearchPopup.createDestination()
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at NewCommissionPaymentBonusDV.pcf: line 34, column 38
    function currency_4 () : typekey.Currency {
      return bonusPayment.Currency
    }
    
    // 'value' attribute on TextInput (id=Policy_Input) at NewCommissionPaymentBonusDV.pcf: line 48, column 41
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectedPolicyPeriod = (__VALUE_TO_SET as entity.PolicyPeriod)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at NewCommissionPaymentBonusDV.pcf: line 34, column 38
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      bonusPayment.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on RangeInput (id=PolicyCommission_Input) at NewCommissionPaymentBonusDV.pcf: line 62, column 45
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      bonusPayment.PolicyCommission = (__VALUE_TO_SET as entity.PolicyCommission)
    }
    
    // 'value' attribute on RangeInput (id=ProducerCode_Input) at NewCommissionPaymentBonusDV.pcf: line 41, column 42
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      bonusPayment.ProducerCode = (__VALUE_TO_SET as entity.ProducerCode)
    }
    
    // 'editable' attribute on DetailViewPanel (id=NewCommissionPaymentBonusDV) at NewCommissionPaymentBonusDV.pcf: line 7, column 38
    function editable_26 () : java.lang.Boolean {
      return canEdit
    }
    
    // 'initialValue' attribute on Variable at NewCommissionPaymentBonusDV.pcf: line 23, column 55
    function initialValue_0 () : gw.api.web.policy.PolicySearchConverter {
      return new gw.api.web.policy.PolicySearchConverter()
    }
    
    // 'inputConversion' attribute on TextInput (id=Policy_Input) at NewCommissionPaymentBonusDV.pcf: line 48, column 41
    function inputConversion_15 (VALUE :  java.lang.String) : java.lang.Object {
      return policySearchConverter.getPolicyPeriod(VALUE)
    }
    
    // 'valueRange' attribute on RangeInput (id=PolicyCommission_Input) at NewCommissionPaymentBonusDV.pcf: line 62, column 45
    function valueRange_22 () : java.lang.Object {
      return PolicyCommissionsForThisProducer
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at NewCommissionPaymentBonusDV.pcf: line 41, column 42
    function valueRange_9 () : java.lang.Object {
      return producer.ProducerCodes
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at NewCommissionPaymentBonusDV.pcf: line 34, column 38
    function valueRoot_3 () : java.lang.Object {
      return bonusPayment
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at NewCommissionPaymentBonusDV.pcf: line 34, column 38
    function value_1 () : gw.pl.currency.MonetaryAmount {
      return bonusPayment.Amount
    }
    
    // 'value' attribute on TextInput (id=Policy_Input) at NewCommissionPaymentBonusDV.pcf: line 48, column 41
    function value_16 () : entity.PolicyPeriod {
      return selectedPolicyPeriod
    }
    
    // 'value' attribute on RangeInput (id=PolicyCommission_Input) at NewCommissionPaymentBonusDV.pcf: line 62, column 45
    function value_19 () : entity.PolicyCommission {
      return bonusPayment.PolicyCommission
    }
    
    // 'value' attribute on RangeInput (id=ProducerCode_Input) at NewCommissionPaymentBonusDV.pcf: line 41, column 42
    function value_6 () : entity.ProducerCode {
      return bonusPayment.ProducerCode
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at NewCommissionPaymentBonusDV.pcf: line 41, column 42
    function verifyValueRangeIsAllowedType_10 ($$arg :  entity.ProducerCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at NewCommissionPaymentBonusDV.pcf: line 41, column 42
    function verifyValueRangeIsAllowedType_10 ($$arg :  gw.api.database.IQueryBeanResult<entity.ProducerCode>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at NewCommissionPaymentBonusDV.pcf: line 41, column 42
    function verifyValueRangeIsAllowedType_10 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PolicyCommission_Input) at NewCommissionPaymentBonusDV.pcf: line 62, column 45
    function verifyValueRangeIsAllowedType_23 ($$arg :  entity.PolicyCommission[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PolicyCommission_Input) at NewCommissionPaymentBonusDV.pcf: line 62, column 45
    function verifyValueRangeIsAllowedType_23 ($$arg :  gw.api.database.IQueryBeanResult<entity.PolicyCommission>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PolicyCommission_Input) at NewCommissionPaymentBonusDV.pcf: line 62, column 45
    function verifyValueRangeIsAllowedType_23 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProducerCode_Input) at NewCommissionPaymentBonusDV.pcf: line 41, column 42
    function verifyValueRange_11 () : void {
      var __valueRangeArg = producer.ProducerCodes
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_10(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=PolicyCommission_Input) at NewCommissionPaymentBonusDV.pcf: line 62, column 45
    function verifyValueRange_24 () : void {
      var __valueRangeArg = PolicyCommissionsForThisProducer
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_23(__valueRangeArg)
    }
    
    property get bonusPayment () : BonusCmsnPayment {
      return getRequireValue("bonusPayment", 0) as BonusCmsnPayment
    }
    
    property set bonusPayment ($arg :  BonusCmsnPayment) {
      setRequireValue("bonusPayment", 0, $arg)
    }
    
    property get canEdit () : Boolean {
      return getRequireValue("canEdit", 0) as Boolean
    }
    
    property set canEdit ($arg :  Boolean) {
      setRequireValue("canEdit", 0, $arg)
    }
    
    property get policySearchConverter () : gw.api.web.policy.PolicySearchConverter {
      return getVariableValue("policySearchConverter", 0) as gw.api.web.policy.PolicySearchConverter
    }
    
    property set policySearchConverter ($arg :  gw.api.web.policy.PolicySearchConverter) {
      setVariableValue("policySearchConverter", 0, $arg)
    }
    
    property get producer () : Producer {
      return getRequireValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setRequireValue("producer", 0, $arg)
    }
    
    property get selectedPolicyPeriod () : PolicyPeriod {
      return getVariableValue("selectedPolicyPeriod", 0) as PolicyPeriod
    }
    
    property set selectedPolicyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("selectedPolicyPeriod", 0, $arg)
    }
    
    property get PolicyCommissionsForThisProducer() : PolicyCommission[] {
      if (selectedPolicyPeriod != null) {
        return selectedPolicyPeriod.PolicyCommissions.where( \ elt -> elt.ProducerCode.Producer.equals(producer))
      }
      
      return null
    }
    
    
  }
  
  
}