package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerStatementPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerStatementPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerStatementPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ProducerStatementPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=eventDate_Cell) at ProducerStatementPopup.pcf: line 53, column 120
    function valueRoot_17 () : java.lang.Object {
      return (gw.api.upgrade.Coercions.makeDateFrom(eventDisplayMap.get("EventDate")))
    }
    
    // 'value' attribute on TextCell (id=Event_Cell) at ProducerStatementPopup.pcf: line 49, column 73
    function value_13 () : java.lang.Object {
      return eventDisplayMap.get("EarningType") //itemEvent.EarningType;
    }
    
    // 'value' attribute on TextCell (id=eventDate_Cell) at ProducerStatementPopup.pcf: line 53, column 120
    function value_16 () : java.lang.String {
      return (gw.api.upgrade.Coercions.makeDateFrom(eventDisplayMap.get("EventDate"))).AsUIStyle
    }
    
    // 'value' attribute on TextCell (id=accountName_Cell) at ProducerStatementPopup.pcf: line 59, column 47
    function value_19 () : java.lang.Object {
      return eventDisplayMap.get("RelatedAccount")
    }
    
    // 'value' attribute on TextCell (id=policyNumber_Cell) at ProducerStatementPopup.pcf: line 65, column 47
    function value_21 () : java.lang.Object {
      return eventDisplayMap.get("RelatedPolicyPeriod")
    }
    
    // 'value' attribute on TextCell (id=chargeType_Cell) at ProducerStatementPopup.pcf: line 71, column 47
    function value_23 () : java.lang.Object {
      return eventDisplayMap.get("ChargeName")
    }
    
    // 'value' attribute on TextCell (id=itemType_Cell) at ProducerStatementPopup.pcf: line 78, column 73
    function value_25 () : java.lang.Object {
      return eventDisplayMap.get("ItemType")
    }
    
    // 'value' attribute on TextCell (id=basis_Cell) at ProducerStatementPopup.pcf: line 85, column 47
    function value_28 () : java.lang.Object {
      return eventDisplayMap.get("Basis")
    }
    
    // 'value' attribute on TextCell (id=commissionPercentage_Cell) at ProducerStatementPopup.pcf: line 90, column 171
    function value_30 () : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(gw.api.upgrade.Coercions.makeDoubleFrom(eventDisplayMap.get("CommissionPercentage")), 2)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=commissionAmount_Cell) at ProducerStatementPopup.pcf: line 97, column 109
    function value_32 () : gw.pl.currency.MonetaryAmount {
      return eventDisplayMap.get("CommissionAmount") as gw.pl.currency.MonetaryAmount
    }
    
    // 'visible' attribute on TextCell (id=Event_Cell) at ProducerStatementPopup.pcf: line 49, column 73
    function visible_14 () : java.lang.Boolean {
      return aggregation == PolicyActivityAggType.TC_NONE
    }
    
    property get eventDisplayMap () : java.util.Map<java.lang.String, java.lang.Object> {
      return getIteratedValue(1) as java.util.Map<java.lang.String, java.lang.Object>
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/producer/ProducerStatementPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerStatementPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyNumber :  String, producerStatement :  ProducerStatement) : int {
      return 0
    }
    
    // 'value' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at ProducerStatementPopup.pcf: line 30, column 55
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      aggregation = (__VALUE_TO_SET as typekey.PolicyActivityAggType)
    }
    
    // 'initialValue' attribute on Variable at ProducerStatementPopup.pcf: line 19, column 37
    function initialValue_0 () : PolicyActivityAggType {
      return PolicyActivityAggType.TC_NONE
    }
    
    // 'value' attribute on TextCell (id=eventDate_Cell) at ProducerStatementPopup.pcf: line 53, column 120
    function sortValue_8 (eventDisplayMap :  java.util.Map<java.lang.String, java.lang.Object>) : java.lang.Object {
      return (gw.api.upgrade.Coercions.makeDateFrom(eventDisplayMap.get("EventDate"))).AsUIStyle
    }
    
    // 'footerSumValue' attribute on RowIterator at ProducerStatementPopup.pcf: line 97, column 109
    function sumValue_12 (eventDisplayMap :  java.util.Map<java.lang.String, java.lang.Object>) : java.lang.Object {
      return eventDisplayMap.get("CommissionAmount") as gw.pl.currency.MonetaryAmount
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at ProducerStatementPopup.pcf: line 30, column 55
    function valueRange_3 () : java.lang.Object {
      return typekey.PolicyActivityAggType.getTypeKeys(false)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at ProducerStatementPopup.pcf: line 30, column 55
    function value_1 () : typekey.PolicyActivityAggType {
      return aggregation
    }
    
    // 'value' attribute on RowIterator at ProducerStatementPopup.pcf: line 40, column 103
    function value_34 () : java.util.List<java.util.Map<java.lang.String, java.lang.Object>> {
      return producerStatement.getItemEventsWithAggregation(aggregation, policyNumber).toList()
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at ProducerStatementPopup.pcf: line 30, column 55
    function verifyValueRangeIsAllowedType_4 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at ProducerStatementPopup.pcf: line 30, column 55
    function verifyValueRangeIsAllowedType_4 ($$arg :  typekey.PolicyActivityAggType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at ProducerStatementPopup.pcf: line 30, column 55
    function verifyValueRange_5 () : void {
      var __valueRangeArg = typekey.PolicyActivityAggType.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_4(__valueRangeArg)
    }
    
    // 'visible' attribute on PanelRef at ProducerStatementPopup.pcf: line 22, column 81
    function visible_35 () : java.lang.Boolean {
      return producerStatement.Type == ProducerStatementType.TC_PRODUCTION
    }
    
    // 'visible' attribute on TextCell (id=Event_Cell) at ProducerStatementPopup.pcf: line 49, column 73
    function visible_7 () : java.lang.Boolean {
      return aggregation == PolicyActivityAggType.TC_NONE
    }
    
    override property get CurrentLocation () : pcf.ProducerStatementPopup {
      return super.CurrentLocation as pcf.ProducerStatementPopup
    }
    
    property get aggregation () : PolicyActivityAggType {
      return getVariableValue("aggregation", 0) as PolicyActivityAggType
    }
    
    property set aggregation ($arg :  PolicyActivityAggType) {
      setVariableValue("aggregation", 0, $arg)
    }
    
    property get policyNumber () : String {
      return getVariableValue("policyNumber", 0) as String
    }
    
    property set policyNumber ($arg :  String) {
      setVariableValue("policyNumber", 0, $arg)
    }
    
    property get producerStatement () : ProducerStatement {
      return getVariableValue("producerStatement", 0) as ProducerStatement
    }
    
    property set producerStatement ($arg :  ProducerStatement) {
      setVariableValue("producerStatement", 0, $arg)
    }
    
    
  }
  
  
}