package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/TransferDetailsLV.producer.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransferDetailsLV_producerExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/TransferDetailsLV.producer.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TransferDetailsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerCell (id=TargetProducer_Cell) at TransferDetailsLV.producer.pcf: line 41, column 39
    function action_7 () : void {
      ProducerSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerCell (id=TargetProducer_Cell) at TransferDetailsLV.producer.pcf: line 41, column 39
    function action_dest_8 () : pcf.api.Destination {
      return pcf.ProducerSearchPopup.createDestination()
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at TransferDetailsLV.producer.pcf: line 51, column 42
    function currency_20 () : typekey.Currency {
      return singleTransfer.Currency
    }
    
    // 'value' attribute on PickerCell (id=TargetProducer_Cell) at TransferDetailsLV.producer.pcf: line 41, column 39
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      singleTransfer.TargetProducer = (__VALUE_TO_SET as entity.Producer)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at TransferDetailsLV.producer.pcf: line 51, column 42
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      singleTransfer.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TypeKeyCell (id=Reason_Cell) at TransferDetailsLV.producer.pcf: line 57, column 47
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      singleTransfer.Reason = (__VALUE_TO_SET as typekey.TransferReason)
    }
    
    // 'editable' attribute on PickerCell (id=TargetProducer_Cell) at TransferDetailsLV.producer.pcf: line 41, column 39
    function editable_9 () : java.lang.Boolean {
      return canEditPage
    }
    
    // 'highlighted' attribute on Row at TransferDetailsLV.producer.pcf: line 32, column 67
    function highlighted_32 () : java.lang.Boolean {
      return singleTransfer.OpenApprovalActivity != null
    }
    
    // 'inputConversion' attribute on PickerCell (id=TargetProducer_Cell) at TransferDetailsLV.producer.pcf: line 41, column 39
    function inputConversion_10 (VALUE :  java.lang.String) : java.lang.Object {
      return producerSearchConverter.getProducer( VALUE )
    }
    
    // 'value' attribute on PickerCell (id=TargetProducer_Cell) at TransferDetailsLV.producer.pcf: line 41, column 39
    function valueRoot_13 () : java.lang.Object {
      return singleTransfer
    }
    
    // 'value' attribute on PickerCell (id=TargetProducer_Cell) at TransferDetailsLV.producer.pcf: line 41, column 39
    function value_11 () : entity.Producer {
      return singleTransfer.TargetProducer
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at TransferDetailsLV.producer.pcf: line 51, column 42
    function value_17 () : gw.pl.currency.MonetaryAmount {
      return singleTransfer.Amount
    }
    
    // 'value' attribute on TypeKeyCell (id=Reason_Cell) at TransferDetailsLV.producer.pcf: line 57, column 47
    function value_24 () : typekey.TransferReason {
      return singleTransfer.Reason
    }
    
    // 'value' attribute on TextCell (id=Activity_Cell) at TransferDetailsLV.producer.pcf: line 63, column 38
    function value_29 () : java.lang.String {
      return singleTransfer.ApprovalRequiredForCurrentUser ? DisplayKey.get("Web.TransferDetailsDV.ApprovalRequired") : null
    }
    
    // 'visible' attribute on TextCell (id=Activity_Cell) at TransferDetailsLV.producer.pcf: line 63, column 38
    function visible_30 () : java.lang.Boolean {
      return not canEditPage
    }
    
    property get singleTransfer () : entity.FundsTransfer {
      return getIteratedValue(1) as entity.FundsTransfer
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/TransferDetailsLV.producer.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransferDetailsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'editable' attribute on RowIterator at TransferDetailsLV.producer.pcf: line 30, column 42
    function editable_6 () : java.lang.Boolean {
      return canEditPage
    }
    
    // 'initialValue' attribute on Variable at TransferDetailsLV.producer.pcf: line 22, column 59
    function initialValue_0 () : gw.api.web.producer.ProducerSearchConverter {
      return new gw.api.web.producer.ProducerSearchConverter()
    }
    
    // 'value' attribute on PickerCell (id=TargetProducer_Cell) at TransferDetailsLV.producer.pcf: line 41, column 39
    function sortValue_1 (singleTransfer :  entity.FundsTransfer) : java.lang.Object {
      return singleTransfer.TargetProducer
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at TransferDetailsLV.producer.pcf: line 51, column 42
    function sortValue_2 (singleTransfer :  entity.FundsTransfer) : java.lang.Object {
      return singleTransfer.Amount
    }
    
    // 'value' attribute on TypeKeyCell (id=Reason_Cell) at TransferDetailsLV.producer.pcf: line 57, column 47
    function sortValue_3 (singleTransfer :  entity.FundsTransfer) : java.lang.Object {
      return singleTransfer.Reason
    }
    
    // 'value' attribute on TextCell (id=Activity_Cell) at TransferDetailsLV.producer.pcf: line 63, column 38
    function sortValue_4 (singleTransfer :  entity.FundsTransfer) : java.lang.Object {
      return singleTransfer.ApprovalRequiredForCurrentUser ? DisplayKey.get("Web.TransferDetailsDV.ApprovalRequired") : null
    }
    
    // 'toCreateAndAdd' attribute on RowIterator at TransferDetailsLV.producer.pcf: line 30, column 42
    function toCreateAndAdd_33 () : entity.FundsTransfer {
      return createAndAddTransfer()
    }
    
    // 'toRemove' attribute on RowIterator at TransferDetailsLV.producer.pcf: line 30, column 42
    function toRemove_34 (singleTransfer :  entity.FundsTransfer) : void {
      removeRow( singleTransfer )
    }
    
    // 'value' attribute on RowIterator at TransferDetailsLV.producer.pcf: line 30, column 42
    function value_35 () : entity.FundsTransfer[] {
      return fundsTransferUtil.TransferTargets
    }
    
    // 'visible' attribute on TextCell (id=Activity_Cell) at TransferDetailsLV.producer.pcf: line 63, column 38
    function visible_5 () : java.lang.Boolean {
      return not canEditPage
    }
    
    property get canEditPage () : boolean {
      return getRequireValue("canEditPage", 0) as java.lang.Boolean
    }
    
    property set canEditPage ($arg :  boolean) {
      setRequireValue("canEditPage", 0, $arg)
    }
    
    property get fundsTransferUtil () : gw.api.web.transaction.FundsTransferUtil {
      return getRequireValue("fundsTransferUtil", 0) as gw.api.web.transaction.FundsTransferUtil
    }
    
    property set fundsTransferUtil ($arg :  gw.api.web.transaction.FundsTransferUtil) {
      setRequireValue("fundsTransferUtil", 0, $arg)
    }
    
    property get producerSearchConverter () : gw.api.web.producer.ProducerSearchConverter {
      return getVariableValue("producerSearchConverter", 0) as gw.api.web.producer.ProducerSearchConverter
    }
    
    property set producerSearchConverter ($arg :  gw.api.web.producer.ProducerSearchConverter) {
      setVariableValue("producerSearchConverter", 0, $arg)
    }
    
    function removeRow(transfer : FundsTransfer) {
      transfer.remove();
      fundsTransferUtil.removeFromTransfers(transfer);
    }
    
    function createAndAddTransfer() : FundsTransfer {
      var transfer =  new FundsTransfer(CurrentLocation, fundsTransferUtil.SourceOwner.Currency)
      fundsTransferUtil.addToTransfers( transfer )
      return transfer
    }
    
    
  }
  
  
}