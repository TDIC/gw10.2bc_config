package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/DirectBillSuspenseItemSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DirectBillSuspenseItemSearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/DirectBillSuspenseItemSearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DirectBillSuspenseItemSearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at DirectBillSuspenseItemSearchDV.pcf: line 40, column 43
    function available_15 () : java.lang.Boolean {
      return (searchCriteria.Currency != null or gw.api.util.CurrencyUtil.isSingleCurrencyMode())
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at DirectBillSuspenseItemSearchDV.pcf: line 40, column 43
    function currency_19 () : typekey.Currency {
      return gw.search.SearchHelper.getDefaultCurrencyIfNull(searchCriteria.Currency)
    }
    
    // 'def' attribute on InputSetRef at DirectBillSuspenseItemSearchDV.pcf: line 51, column 41
    function def_onEnter_29 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at DirectBillSuspenseItemSearchDV.pcf: line 51, column 41
    function def_refreshVariables_30 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at DirectBillSuspenseItemSearchDV.pcf: line 18, column 46
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.EarliestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at DirectBillSuspenseItemSearchDV.pcf: line 30, column 66
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Currency = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at DirectBillSuspenseItemSearchDV.pcf: line 40, column 43
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MinAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at DirectBillSuspenseItemSearchDV.pcf: line 47, column 43
    function defaultSetter_24 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.MaxAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at DirectBillSuspenseItemSearchDV.pcf: line 23, column 44
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.LatestDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'onChange' attribute on PostOnChange at DirectBillSuspenseItemSearchDV.pcf: line 32, column 54
    function onChange_8 () : void {
      blankMinimumAndMaximumFields()
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at DirectBillSuspenseItemSearchDV.pcf: line 18, column 46
    function valueRoot_2 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on DateInput (id=EarliestDateCriterion_Input) at DirectBillSuspenseItemSearchDV.pcf: line 18, column 46
    function value_0 () : java.util.Date {
      return searchCriteria.EarliestDate
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at DirectBillSuspenseItemSearchDV.pcf: line 30, column 66
    function value_10 () : typekey.Currency {
      return searchCriteria.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MinAmountCriterion_Input) at DirectBillSuspenseItemSearchDV.pcf: line 40, column 43
    function value_16 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MinAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=MaxAmountCriterion_Input) at DirectBillSuspenseItemSearchDV.pcf: line 47, column 43
    function value_23 () : gw.pl.currency.MonetaryAmount {
      return searchCriteria.MaxAmount
    }
    
    // 'value' attribute on DateInput (id=LatestDateCriterion_Input) at DirectBillSuspenseItemSearchDV.pcf: line 23, column 44
    function value_4 () : java.util.Date {
      return searchCriteria.LatestDate
    }
    
    // 'visible' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at DirectBillSuspenseItemSearchDV.pcf: line 30, column 66
    function visible_9 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    property get searchCriteria () : gw.search.DirectSuspPmntItemSearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.search.DirectSuspPmntItemSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.DirectSuspPmntItemSearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    function blankMinimumAndMaximumFields() {
      searchCriteria.MinAmount = null
      searchCriteria.MaxAmount = null
    }
    
    
  }
  
  
}