package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.util.CurrencyUtil
@javax.annotation.Generated("config/web/pcf/admin/AdminMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminMenuActionsExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/AdminMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminMenuActionsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewBillingPlan) at AdminMenuActions.pcf: line 11, column 74
    function action_1 () : void {
      NewBillingPlan.go(getCurrency())
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewDelinquencyPlan) at AdminMenuActions.pcf: line 26, column 72
    function action_10 () : void {
      NewDelinquencyPlan.go(getCurrency())
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewAgencyBillPlan) at AdminMenuActions.pcf: line 31, column 79
    function action_13 () : void {
      NewAgencyBillPlan.go(getCurrency())
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewReturnPremiumPlan) at AdminMenuActions.pcf: line 36, column 77
    function action_16 () : void {
      NewReturnPremiumPlan.go()
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewPaymentAllocationPlan) at AdminMenuActions.pcf: line 41, column 78
    function action_19 () : void {
      NewPaymentAllocationPlan.go()
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewChargeBreakdownCategoryType) at AdminMenuActions.pcf: line 46, column 93
    function action_22 () : void {
      NewChargeBreakdownCategoryType.go()
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewCollectionAgency) at AdminMenuActions.pcf: line 51, column 75
    function action_25 () : void {
      NewCollectionAgency.go()
    }
    
    // 'action' attribute on MenuItem (id=NewImmediateCharge) at AdminMenuActions.pcf: line 59, column 81
    function action_28 () : void {
      NewChargePattern.go("ImmediateCharge")
    }
    
    // 'action' attribute on MenuItem (id=NewPassThroughCharge) at AdminMenuActions.pcf: line 64, column 81
    function action_31 () : void {
      NewChargePattern.go("PassThroughCharge")
    }
    
    // 'action' attribute on MenuItem (id=NewProRataCharge) at AdminMenuActions.pcf: line 69, column 81
    function action_34 () : void {
      NewChargePattern.go("ProRataCharge")
    }
    
    // 'action' attribute on MenuItem (id=NewRecaptureCharge) at AdminMenuActions.pcf: line 74, column 81
    function action_37 () : void {
      NewChargePattern.go("RecaptureCharge")
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewPaymentPlan) at AdminMenuActions.pcf: line 16, column 73
    function action_4 () : void {
      NewPaymentPlanWizard.go(getCurrency())
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewUser) at AdminMenuActions.pcf: line 80, column 61
    function action_40 () : void {
      NewUserWizard.go()
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewRole) at AdminMenuActions.pcf: line 85, column 35
    function action_43 () : void {
      NewRole.go()
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewGroup) at AdminMenuActions.pcf: line 90, column 36
    function action_46 () : void {
      NewGroup.go()
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewAuthorityLimitProfile) at AdminMenuActions.pcf: line 95, column 40
    function action_49 () : void {
      NewAuthorityLimitProfile.go()
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewCommissionPlan) at AdminMenuActions.pcf: line 21, column 73
    function action_7 () : void {
      NewCommissionPlan.go(getCurrency())
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewDelinquencyPlan) at AdminMenuActions.pcf: line 26, column 72
    function action_dest_11 () : pcf.api.Destination {
      return pcf.NewDelinquencyPlan.createDestination(getCurrency())
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewAgencyBillPlan) at AdminMenuActions.pcf: line 31, column 79
    function action_dest_14 () : pcf.api.Destination {
      return pcf.NewAgencyBillPlan.createDestination(getCurrency())
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewReturnPremiumPlan) at AdminMenuActions.pcf: line 36, column 77
    function action_dest_17 () : pcf.api.Destination {
      return pcf.NewReturnPremiumPlan.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewBillingPlan) at AdminMenuActions.pcf: line 11, column 74
    function action_dest_2 () : pcf.api.Destination {
      return pcf.NewBillingPlan.createDestination(getCurrency())
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewPaymentAllocationPlan) at AdminMenuActions.pcf: line 41, column 78
    function action_dest_20 () : pcf.api.Destination {
      return pcf.NewPaymentAllocationPlan.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewChargeBreakdownCategoryType) at AdminMenuActions.pcf: line 46, column 93
    function action_dest_23 () : pcf.api.Destination {
      return pcf.NewChargeBreakdownCategoryType.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewCollectionAgency) at AdminMenuActions.pcf: line 51, column 75
    function action_dest_26 () : pcf.api.Destination {
      return pcf.NewCollectionAgency.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=NewImmediateCharge) at AdminMenuActions.pcf: line 59, column 81
    function action_dest_29 () : pcf.api.Destination {
      return pcf.NewChargePattern.createDestination("ImmediateCharge")
    }
    
    // 'action' attribute on MenuItem (id=NewPassThroughCharge) at AdminMenuActions.pcf: line 64, column 81
    function action_dest_32 () : pcf.api.Destination {
      return pcf.NewChargePattern.createDestination("PassThroughCharge")
    }
    
    // 'action' attribute on MenuItem (id=NewProRataCharge) at AdminMenuActions.pcf: line 69, column 81
    function action_dest_35 () : pcf.api.Destination {
      return pcf.NewChargePattern.createDestination("ProRataCharge")
    }
    
    // 'action' attribute on MenuItem (id=NewRecaptureCharge) at AdminMenuActions.pcf: line 74, column 81
    function action_dest_38 () : pcf.api.Destination {
      return pcf.NewChargePattern.createDestination("RecaptureCharge")
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewUser) at AdminMenuActions.pcf: line 80, column 61
    function action_dest_41 () : pcf.api.Destination {
      return pcf.NewUserWizard.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewRole) at AdminMenuActions.pcf: line 85, column 35
    function action_dest_44 () : pcf.api.Destination {
      return pcf.NewRole.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewGroup) at AdminMenuActions.pcf: line 90, column 36
    function action_dest_47 () : pcf.api.Destination {
      return pcf.NewGroup.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewPaymentPlan) at AdminMenuActions.pcf: line 16, column 73
    function action_dest_5 () : pcf.api.Destination {
      return pcf.NewPaymentPlanWizard.createDestination(getCurrency())
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewAuthorityLimitProfile) at AdminMenuActions.pcf: line 95, column 40
    function action_dest_50 () : pcf.api.Destination {
      return pcf.NewAuthorityLimitProfile.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=AdminMenuActions_NewCommissionPlan) at AdminMenuActions.pcf: line 21, column 73
    function action_dest_8 () : pcf.api.Destination {
      return pcf.NewCommissionPlan.createDestination(getCurrency())
    }
    
    // 'visible' attribute on MenuItem (id=AdminMenuActions_NewBillingPlan) at AdminMenuActions.pcf: line 11, column 74
    function visible_0 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.billplancreate
    }
    
    // 'visible' attribute on MenuItem (id=AdminMenuActions_NewAgencyBillPlan) at AdminMenuActions.pcf: line 31, column 79
    function visible_12 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.agencybillplancreate
    }
    
    // 'visible' attribute on MenuItem (id=AdminMenuActions_NewReturnPremiumPlan) at AdminMenuActions.pcf: line 36, column 77
    function visible_15 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.retpremplancreate
    }
    
    // 'visible' attribute on MenuItem (id=AdminMenuActions_NewPaymentAllocationPlan) at AdminMenuActions.pcf: line 41, column 78
    function visible_18 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.payallocplancreate
    }
    
    // 'visible' attribute on MenuItem (id=AdminMenuActions_NewChargeBreakdownCategoryType) at AdminMenuActions.pcf: line 46, column 93
    function visible_21 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.chargebreakdowncategorytypecreate
    }
    
    // 'visible' attribute on MenuItem (id=AdminMenuActions_NewCollectionAgency) at AdminMenuActions.pcf: line 51, column 75
    function visible_24 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.colagencycreate
    }
    
    // 'visible' attribute on MenuItem (id=NewImmediateCharge) at AdminMenuActions.pcf: line 59, column 81
    function visible_27 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.chargepatterncreate
    }
    
    // 'visible' attribute on MenuItem (id=AdminMenuActions_NewPaymentPlan) at AdminMenuActions.pcf: line 16, column 73
    function visible_3 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.pmntplancreate
    }
    
    // 'visible' attribute on MenuItem (id=AdminMenuActions_NewUser) at AdminMenuActions.pcf: line 80, column 61
    function visible_39 () : java.lang.Boolean {
      return perm.User.create and perm.System.useradmin
    }
    
    // 'visible' attribute on MenuItem (id=AdminMenuActions_NewRole) at AdminMenuActions.pcf: line 85, column 35
    function visible_42 () : java.lang.Boolean {
      return perm.Role.create
    }
    
    // 'visible' attribute on MenuItem (id=AdminMenuActions_NewGroup) at AdminMenuActions.pcf: line 90, column 36
    function visible_45 () : java.lang.Boolean {
      return perm.Group.create
    }
    
    // 'visible' attribute on MenuItem (id=AdminMenuActions_NewAuthorityLimitProfile) at AdminMenuActions.pcf: line 95, column 40
    function visible_48 () : java.lang.Boolean {
      return perm.System.alpmanage
    }
    
    // 'visible' attribute on MenuItem (id=AdminMenuActions_NewCommissionPlan) at AdminMenuActions.pcf: line 21, column 73
    function visible_6 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.commplancreate
    }
    
    // 'visible' attribute on MenuItem (id=AdminMenuActions_NewDelinquencyPlan) at AdminMenuActions.pcf: line 26, column 72
    function visible_9 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.delplancreate
    }
    
    function getCurrency() : Currency {
      if(!CurrencyUtil.isMultiCurrencyMode()){
        return CurrencyUtil.getDefaultCurrency()
      } else if (Currency.getTypeKeys(true).Count == 1){
        return Currency.getTypeKeys(true)[0]
      } 
      return null
    }
    
    function isMultiCurrencyMode(): boolean {
      return CurrencyUtil.isMultiCurrencyMode() and Currency.getTypeKeys(true).Count > 1
    }
    
    
  }
  
  
}