package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountPaymentsExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountPaymentsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    static function __constructorIndex (account :  Account, initialSelectedMoneyRcvd :  DirectBillMoneyRcvd) : int {
      return 1
    }
    
    // 'canVisit' attribute on Page (id=AccountPayments) at AccountPayments.pcf: line 11, column 67
    static function canVisit_9 (account :  Account, initialSelectedMoneyRcvd :  DirectBillMoneyRcvd) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctpmntview
    }
    
    // Page (id=AccountPayments) at AccountPayments.pcf: line 11, column 67
    static function parent_10 (account :  Account, initialSelectedMoneyRcvd :  DirectBillMoneyRcvd) : pcf.api.Destination {
      return pcf.AccountDetailPayments.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.AccountPayments {
      return super.CurrentLocation as pcf.AccountPayments
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get initialSelectedMoneyRcvd () : DirectBillMoneyRcvd {
      return getVariableValue("initialSelectedMoneyRcvd", 0) as DirectBillMoneyRcvd
    }
    
    property set initialSelectedMoneyRcvd ($arg :  DirectBillMoneyRcvd) {
      setVariableValue("initialSelectedMoneyRcvd", 0, $arg)
    }
    
    property get selectedPaymentRequest () : PaymentRequest {
      return getVariableValue("selectedPaymentRequest", 0) as PaymentRequest
    }
    
    property set selectedPaymentRequest ($arg :  PaymentRequest) {
      setVariableValue("selectedPaymentRequest", 0, $arg)
    }
    
    function checkInStoppableState(paymentRequest : PaymentRequest) : boolean {
          return (paymentRequest.Status == PaymentRequestStatus.TC_CREATED || paymentRequest.Status ==
          PaymentRequestStatus.TC_REQUESTED);
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountPayments.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DirectBillPaymentsListDetailExpressionsImpl extends AccountPaymentsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at AccountPayments.pcf: line 34, column 109
    function def_onEnter_4 (def :  pcf.AccountDBPaymentsLV) : void {
      def.onEnter(account, account.findReceivedPaymentMoneysSortedByReceivedDate(), true)
    }
    
    // 'def' attribute on PanelRef at AccountPayments.pcf: line 56, column 67
    function def_onEnter_6 (def :  pcf.AccountPaymentDistributionItemsCV) : void {
      def.onEnter(moneyReceived)
    }
    
    // 'def' attribute on PanelRef at AccountPayments.pcf: line 34, column 109
    function def_refreshVariables_5 (def :  pcf.AccountDBPaymentsLV) : void {
      def.refreshVariables(account, account.findReceivedPaymentMoneysSortedByReceivedDate(), true)
    }
    
    // 'def' attribute on PanelRef at AccountPayments.pcf: line 56, column 67
    function def_refreshVariables_7 (def :  pcf.AccountPaymentDistributionItemsCV) : void {
      def.refreshVariables(moneyReceived)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountPayments.pcf: line 42, column 108
    function filter_0 () : gw.api.filters.IFilter {
      return new gw.payment.BaseMoneyReceivedDateFilter(30)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountPayments.pcf: line 45, column 108
    function filter_1 () : gw.api.filters.IFilter {
      return new gw.payment.BaseMoneyReceivedDateFilter(60)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountPayments.pcf: line 48, column 108
    function filter_2 () : gw.api.filters.IFilter {
      return new gw.payment.BaseMoneyReceivedDateFilter(90)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountPayments.pcf: line 51, column 101
    function filter_3 () : gw.api.filters.IFilter {
      return new gw.api.filters.StandardQueryFilter("All", \ qf -> {})
    }
    
    // 'selectionOnEnter' attribute on ListDetailPanel (id=DirectBillPaymentsListDetail) at AccountPayments.pcf: line 32, column 45
    function selectionOnEnter_8 () : java.lang.Object {
      return initialSelectedMoneyRcvd
    }
    
    property get moneyReceived () : DirectBillMoneyRcvd {
      return getCurrentSelection(1) as DirectBillMoneyRcvd
    }
    
    property set moneyReceived ($arg :  DirectBillMoneyRcvd) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  
}