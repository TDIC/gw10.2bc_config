package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.web.payment.ModifyingDirectBillPaymentView
@javax.annotation.Generated("config/web/pcf/payment/ModifyDirectBillPaymentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ModifyDirectBillPaymentPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/ModifyDirectBillPaymentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ModifyDirectBillPaymentPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account, originalMoney :  DirectBillMoneyRcvd, whenModifyingDirectBillMoney :  gw.api.web.payment.WhenModifyingDirectBillMoney) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=ModifyDirectBillPaymentPopup) at ModifyDirectBillPaymentPopup.pcf: line 11, column 75
    function beforeCommit_3 (pickedValue :  java.lang.Object) : void {
      paymentView.execute( CurrentLocation )
    }
    
    // 'canVisit' attribute on Popup (id=ModifyDirectBillPaymentPopup) at ModifyDirectBillPaymentPopup.pcf: line 11, column 75
    static function canVisit_4 (account :  Account, originalMoney :  DirectBillMoneyRcvd, whenModifyingDirectBillMoney :  gw.api.web.payment.WhenModifyingDirectBillMoney) : java.lang.Boolean {
      return perm.DirectBillMoneyRcvd.pmntmanproc
    }
    
    // 'def' attribute on ScreenRef at ModifyDirectBillPaymentPopup.pcf: line 28, column 54
    function def_onEnter_1 (def :  pcf.EditDBPaymentScreen) : void {
      def.onEnter(paymentView, null) 
    }
    
    // 'def' attribute on ScreenRef at ModifyDirectBillPaymentPopup.pcf: line 28, column 54
    function def_refreshVariables_2 (def :  pcf.EditDBPaymentScreen) : void {
      def.refreshVariables(paymentView, null) 
    }
    
    // 'initialValue' attribute on Variable at ModifyDirectBillPaymentPopup.pcf: line 26, column 65
    function initialValue_0 () : gw.api.web.payment.ModifyingDirectBillPaymentView {
      return createPaymentView()
    }
    
    override property get CurrentLocation () : pcf.ModifyDirectBillPaymentPopup {
      return super.CurrentLocation as pcf.ModifyDirectBillPaymentPopup
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get originalMoney () : DirectBillMoneyRcvd {
      return getVariableValue("originalMoney", 0) as DirectBillMoneyRcvd
    }
    
    property set originalMoney ($arg :  DirectBillMoneyRcvd) {
      setVariableValue("originalMoney", 0, $arg)
    }
    
    property get paymentView () : gw.api.web.payment.ModifyingDirectBillPaymentView {
      return getVariableValue("paymentView", 0) as gw.api.web.payment.ModifyingDirectBillPaymentView
    }
    
    property set paymentView ($arg :  gw.api.web.payment.ModifyingDirectBillPaymentView) {
      setVariableValue("paymentView", 0, $arg)
    }
    
    property get whenModifyingDirectBillMoney () : gw.api.web.payment.WhenModifyingDirectBillMoney {
      return getVariableValue("whenModifyingDirectBillMoney", 0) as gw.api.web.payment.WhenModifyingDirectBillMoney
    }
    
    property set whenModifyingDirectBillMoney ($arg :  gw.api.web.payment.WhenModifyingDirectBillMoney) {
      setVariableValue("whenModifyingDirectBillMoney", 0, $arg)
    }
    
    
    function createPaymentView() : ModifyingDirectBillPaymentView{
      var view = new ModifyingDirectBillPaymentView(originalMoney, DBPmntGroupingCriteria.TC_INVOICE, DBPmntAggregateCriteria.TC_ITEM, whenModifyingDirectBillMoney)
      if (account.ListBill) {
        view.IncludeOnlyCriteria = DistributionLimitType.TC_OUTSTANDING
      } else {
        view.IncludeOnlyCriteria = account.DistributionLimitTypeFromPlan;
      }
      return view
    }
    
    
  }
  
  
}