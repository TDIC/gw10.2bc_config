package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/ChooseInvoicePlacementPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChooseInvoicePlacementPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/invoice/ChooseInvoicePlacementPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChooseInvoicePlacementPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (entry :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry, payer :  gw.api.domain.invoice.InvoicePayer) : int {
      return 0
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at ChooseInvoicePlacementPopup.pcf: line 35, column 35
    function currency_8 () : typekey.Currency {
      return entry.Amount.Currency
    }
    
    // 'def' attribute on ListViewInput (id=targetInvoicesLV) at ChooseInvoicePlacementPopup.pcf: line 42, column 31
    function def_onEnter_10 (def :  pcf.InvoiceSelectorSimpleLV) : void {
      def.onEnter(payer)
    }
    
    // 'def' attribute on ListViewInput (id=targetInvoicesLV) at ChooseInvoicePlacementPopup.pcf: line 42, column 31
    function def_refreshVariables_11 (def :  pcf.InvoiceSelectorSimpleLV) : void {
      def.refreshVariables(payer)
    }
    
    // 'value' attribute on DateInput (id=EventDate_Input) at ChooseInvoicePlacementPopup.pcf: line 25, column 38
    function valueRoot_1 () : java.lang.Object {
      return entry
    }
    
    // 'value' attribute on DateInput (id=EventDate_Input) at ChooseInvoicePlacementPopup.pcf: line 25, column 38
    function value_0 () : java.util.Date {
      return entry.EventDate
    }
    
    // 'value' attribute on TextInput (id=Invoice_Input) at ChooseInvoicePlacementPopup.pcf: line 30, column 41
    function value_3 () : entity.Invoice {
      return entry.Invoice
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at ChooseInvoicePlacementPopup.pcf: line 35, column 35
    function value_6 () : gw.pl.currency.MonetaryAmount {
      return entry.Amount
    }
    
    override property get CurrentLocation () : pcf.ChooseInvoicePlacementPopup {
      return super.CurrentLocation as pcf.ChooseInvoicePlacementPopup
    }
    
    property get entry () : gw.api.domain.invoice.ChargeInstallmentChanger.Entry {
      return getVariableValue("entry", 0) as gw.api.domain.invoice.ChargeInstallmentChanger.Entry
    }
    
    property set entry ($arg :  gw.api.domain.invoice.ChargeInstallmentChanger.Entry) {
      setVariableValue("entry", 0, $arg)
    }
    
    property get payer () : gw.api.domain.invoice.InvoicePayer {
      return getVariableValue("payer", 0) as gw.api.domain.invoice.InvoicePayer
    }
    
    property set payer ($arg :  gw.api.domain.invoice.InvoicePayer) {
      setVariableValue("payer", 0, $arg)
    }
    
    
  }
  
  
}