package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/NewDocumentFromTemplateScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewDocumentFromTemplateScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/document/NewDocumentFromTemplateScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewDocumentFromTemplateScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewDocumentFromTemplateScreen.pcf: line 26, column 52
    function def_onEnter_3 (def :  pcf.NewTemplateDocumentDV) : void {
      def.onEnter(documentBCContext, documentCreationInfo, documentContainer)
    }
    
    // 'def' attribute on PanelRef at NewDocumentFromTemplateScreen.pcf: line 30, column 127
    function def_onEnter_5 (def :  pcf.DocumentDetailsEditDVPanelSet) : void {
      def.onEnter(new gw.document.DocumentMetadataBCHelper(documentCreationInfo.Document), true)
    }
    
    // 'def' attribute on PanelRef at NewDocumentFromTemplateScreen.pcf: line 26, column 52
    function def_refreshVariables_4 (def :  pcf.NewTemplateDocumentDV) : void {
      def.refreshVariables(documentBCContext, documentCreationInfo, documentContainer)
    }
    
    // 'def' attribute on PanelRef at NewDocumentFromTemplateScreen.pcf: line 30, column 127
    function def_refreshVariables_6 (def :  pcf.DocumentDetailsEditDVPanelSet) : void {
      def.refreshVariables(new gw.document.DocumentMetadataBCHelper(documentCreationInfo.Document), true)
    }
    
    // 'initialValue' attribute on Variable at NewDocumentFromTemplateScreen.pcf: line 16, column 45
    function initialValue_0 () : gw.document.DocumentBCContext {
      return new gw.document.DocumentBCContext(documentContainer)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at NewDocumentFromTemplateScreen.pcf: line 19, column 102
    function toolbarButtonSet_onEnter_1 (def :  pcf.DocumentCreationToolbarButtonSet) : void {
      def.onEnter(documentBCContext,documentCreationInfo)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at NewDocumentFromTemplateScreen.pcf: line 19, column 102
    function toolbarButtonSet_refreshVariables_2 (def :  pcf.DocumentCreationToolbarButtonSet) : void {
      def.refreshVariables(documentBCContext,documentCreationInfo)
    }
    
    property get documentBCContext () : gw.document.DocumentBCContext {
      return getVariableValue("documentBCContext", 0) as gw.document.DocumentBCContext
    }
    
    property set documentBCContext ($arg :  gw.document.DocumentBCContext) {
      setVariableValue("documentBCContext", 0, $arg)
    }
    
    property get documentContainer () : DocumentContainer {
      return getRequireValue("documentContainer", 0) as DocumentContainer
    }
    
    property set documentContainer ($arg :  DocumentContainer) {
      setRequireValue("documentContainer", 0, $arg)
    }
    
    property get documentCreationInfo () : gw.document.DocumentCreationInfo {
      return getRequireValue("documentCreationInfo", 0) as gw.document.DocumentCreationInfo
    }
    
    property set documentCreationInfo ($arg :  gw.document.DocumentCreationInfo) {
      setRequireValue("documentCreationInfo", 0, $arg)
    }
    
    
  }
  
  
}