package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanCloneToolbarButtonSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyPlanCloneToolbarButtonSet_defaultExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/delinquency/DelinquencyPlanCloneToolbarButtonSet.default.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyPlanCloneToolbarButtonSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=Clone) at DelinquencyPlanCloneToolbarButtonSet.default.pcf: line 16, column 43
    function action_1 () : void {
      CloneDelinquencyPlan.go(delinquencyPlan, false)
    }
    
    // 'action' attribute on ToolbarButton (id=Clone) at DelinquencyPlanCloneToolbarButtonSet.default.pcf: line 16, column 43
    function action_dest_2 () : pcf.api.Destination {
      return pcf.CloneDelinquencyPlan.createDestination(delinquencyPlan, false)
    }
    
    // 'visible' attribute on ToolbarButton (id=Clone) at DelinquencyPlanCloneToolbarButtonSet.default.pcf: line 16, column 43
    function visible_0 () : java.lang.Boolean {
      return perm.System.delplancreate
    }
    
    property get delinquencyPlan () : DelinquencyPlan {
      return getRequireValue("delinquencyPlan", 0) as DelinquencyPlan
    }
    
    property set delinquencyPlan ($arg :  DelinquencyPlan) {
      setRequireValue("delinquencyPlan", 0, $arg)
    }
    
    
  }
  
  
}