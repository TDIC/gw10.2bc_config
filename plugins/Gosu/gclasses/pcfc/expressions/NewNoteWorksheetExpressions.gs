package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.system.BCLoggerCategory
@javax.annotation.Generated("config/web/pcf/note/NewNoteWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewNoteWorksheetExpressions {
  @javax.annotation.Generated("config/web/pcf/note/NewNoteWorksheet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewNoteWorksheetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (srcBean :  KeyableBean, noteContainer :  NoteContainer) : int {
      return 0
    }
    
    static function __constructorIndex (srcBean :  KeyableBean, noteContainer :  NoteContainer, noteTemplateStr :  String) : int {
      return 1
    }
    
    // 'def' attribute on ScreenRef at NewNoteWorksheet.pcf: line 39, column 43
    function def_onEnter_3 (def :  pcf.NewNoteScreen) : void {
      def.onEnter(srcBean, Note)
    }
    
    // 'def' attribute on ScreenRef at NewNoteWorksheet.pcf: line 39, column 43
    function def_refreshVariables_4 (def :  pcf.NewNoteScreen) : void {
      def.refreshVariables(srcBean, Note)
    }
    
    // 'initialValue' attribute on Variable at NewNoteWorksheet.pcf: line 29, column 31
    function initialValue_0 () : gw.i18n.ILocale {
      return gw.api.util.LocaleUtil.getDefaultLocale()
    }
    
    // 'initialValue' attribute on Variable at NewNoteWorksheet.pcf: line 33, column 50
    function initialValue_1 () : java.util.Map<String, Object> {
      return gw.api.util.SymbolTableUtil.populateBeans( srcBean )
    }
    
    // 'initialValue' attribute on Variable at NewNoteWorksheet.pcf: line 37, column 20
    function initialValue_2 () : Note {
      return createNote()
    }
    
    // 'tabLabel' attribute on Worksheet (id=NewNoteWorksheet) at NewNoteWorksheet.pcf: line 10, column 59
    function tabLabel_5 () : java.lang.String {
      return DisplayKey.get("Web.NewNote.Tab", noteContainer.DisplayName, srcBean.DisplayName)
    }
    
    override property get CurrentLocation () : pcf.NewNoteWorksheet {
      return super.CurrentLocation as pcf.NewNoteWorksheet
    }
    
    property get Note () : Note {
      return getVariableValue("Note", 0) as Note
    }
    
    property set Note ($arg :  Note) {
      setVariableValue("Note", 0, $arg)
    }
    
    property get locale () : gw.i18n.ILocale {
      return getVariableValue("locale", 0) as gw.i18n.ILocale
    }
    
    property set locale ($arg :  gw.i18n.ILocale) {
      setVariableValue("locale", 0, $arg)
    }
    
    property get noteContainer () : NoteContainer {
      return getVariableValue("noteContainer", 0) as NoteContainer
    }
    
    property set noteContainer ($arg :  NoteContainer) {
      setVariableValue("noteContainer", 0, $arg)
    }
    
    property get noteTemplateStr () : String {
      return getVariableValue("noteTemplateStr", 0) as String
    }
    
    property set noteTemplateStr ($arg :  String) {
      setVariableValue("noteTemplateStr", 0, $arg)
    }
    
    property get srcBean () : KeyableBean {
      return getVariableValue("srcBean", 0) as KeyableBean
    }
    
    property set srcBean ($arg :  KeyableBean) {
      setVariableValue("srcBean", 0, $arg)
    }
    
    property get symbolTable () : java.util.Map<String, Object> {
      return getVariableValue("symbolTable", 0) as java.util.Map<String, Object>
    }
    
    property set symbolTable ($arg :  java.util.Map<String, Object>) {
      setVariableValue("symbolTable", 0, $arg)
    }
    
    function createNote() : Note {
      var rtn = new Note()
      noteContainer.addNote(rtn)
      if (noteTemplateStr != null) {
        var templatePlugin = gw.plugin.Plugins.get(gw.plugin.note.INoteTemplateSource)
        var template = templatePlugin.getNoteTemplate(locale, noteTemplateStr)
        if (template == null) {
          BCLoggerCategory.DOCUMENT.warn("Could not find " + noteTemplateStr + " in " + locale.Code)
        } else {
          rtn.useTemplate(template, symbolTable)
        }
      }
      return rtn
    }
          
        
    
    
  }
  
  
}