package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicySummaryFinancialsDV.Account.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicySummaryFinancialsDV_AccountExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicySummaryFinancialsDV.Account.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicySummaryFinancialsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get summaryHelper () : gw.web.policy.PolicySummaryHelper {
      return getRequireValue("summaryHelper", 0) as gw.web.policy.PolicySummaryHelper
    }
    
    property set summaryHelper ($arg :  gw.web.policy.PolicySummaryHelper) {
      setRequireValue("summaryHelper", 0, $arg)
    }
    
    
  }
  
  
}