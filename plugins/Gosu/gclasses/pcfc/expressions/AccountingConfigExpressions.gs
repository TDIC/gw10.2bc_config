package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/tools/AccountingConfig.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountingConfigExpressions {
  @javax.annotation.Generated("config/web/pcf/tools/AccountingConfig.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountingConfigExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'def' attribute on PanelRef at AccountingConfig.pcf: line 122, column 47
    function def_onEnter_34 (def :  pcf.UnsupportedToolsDisclaimerDV) : void {
      def.onEnter()
    }
    
    // 'def' attribute on PanelRef at AccountingConfig.pcf: line 122, column 47
    function def_refreshVariables_35 (def :  pcf.UnsupportedToolsDisclaimerDV) : void {
      def.refreshVariables()
    }
    
    // 'initialValue' attribute on Variable at AccountingConfig.pcf: line 13, column 58
    function initialValue_0 () : gw.api.web.accounting.AccountingConfigUtil {
      return new gw.api.web.accounting.AccountingConfigUtil()
    }
    
    // Page (id=AccountingConfig) at AccountingConfig.pcf: line 7, column 76
    static function parent_36 () : pcf.api.Destination {
      return pcf.UnsupportedTools.createDestination()
    }
    
    // 'value' attribute on TextCell (id=TAccountOwner_Cell) at AccountingConfig.pcf: line 34, column 77
    function sortValue_1 (tAccountPattern :  entity.TAccountPattern) : java.lang.Object {
      return tAccountPattern.TAccountOwnerPattern.DisplayName
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AccountingConfig.pcf: line 67, column 49
    function sortValue_13 (transactionInfo :  gw.api.tools.EntityDisplayInfo) : java.lang.Object {
      return transactionInfo.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AccountingConfig.pcf: line 71, column 49
    function sortValue_14 (transactionInfo :  gw.api.tools.EntityDisplayInfo) : java.lang.Object {
      return transactionInfo.Desc
    }
    
    // 'value' attribute on TextCell (id=TAccountName_Cell) at AccountingConfig.pcf: line 40, column 55
    function sortValue_2 (tAccountPattern :  entity.TAccountPattern) : java.lang.Object {
      return tAccountPattern
    }
    
    // 'value' attribute on TextCell (id=ChargeName_Cell) at AccountingConfig.pcf: line 92, column 53
    function sortValue_22 (chargePattern :  entity.ChargePattern) : java.lang.Object {
      return chargePattern.ChargeName
    }
    
    // 'value' attribute on TypeKeyCell (id=TAccountType_Cell) at AccountingConfig.pcf: line 46, column 53
    function sortValue_3 (tAccountPattern :  entity.TAccountPattern) : java.lang.Object {
      return tAccountPattern.TAccountType
    }
    
    // 'value' attribute on RowIterator at AccountingConfig.pcf: line 28, column 52
    function value_12 () : entity.TAccountPattern[] {
      return accountingConfigUtil.getAllTAccountPatterns()
    }
    
    // 'value' attribute on RowIterator at AccountingConfig.pcf: line 61, column 60
    function value_21 () : gw.api.tools.EntityDisplayInfo[] {
      return accountingConfigUtil.getAllTransactionInfos()
    }
    
    // 'value' attribute on RowIterator at AccountingConfig.pcf: line 86, column 50
    function value_33 () : entity.ChargePattern[] {
      return accountingConfigUtil.getAllChargePatterns()
    }
    
    override property get CurrentLocation () : pcf.AccountingConfig {
      return super.CurrentLocation as pcf.AccountingConfig
    }
    
    property get accountingConfigUtil () : gw.api.web.accounting.AccountingConfigUtil {
      return getVariableValue("accountingConfigUtil", 0) as gw.api.web.accounting.AccountingConfigUtil
    }
    
    property set accountingConfigUtil ($arg :  gw.api.web.accounting.AccountingConfigUtil) {
      setVariableValue("accountingConfigUtil", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/tools/AccountingConfig.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends AccountingConfigExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AccountingConfig.pcf: line 67, column 49
    function valueRoot_16 () : java.lang.Object {
      return transactionInfo
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AccountingConfig.pcf: line 67, column 49
    function value_15 () : java.lang.String {
      return transactionInfo.Name
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at AccountingConfig.pcf: line 71, column 49
    function value_18 () : java.lang.String {
      return transactionInfo.Desc
    }
    
    property get transactionInfo () : gw.api.tools.EntityDisplayInfo {
      return getIteratedValue(1) as gw.api.tools.EntityDisplayInfo
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/tools/AccountingConfig.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends AccountingConfigExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=ChargeName_Cell) at AccountingConfig.pcf: line 92, column 53
    function valueRoot_24 () : java.lang.Object {
      return chargePattern
    }
    
    // 'value' attribute on TextCell (id=ChargeName_Cell) at AccountingConfig.pcf: line 92, column 53
    function value_23 () : java.lang.String {
      return chargePattern.ChargeName
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at AccountingConfig.pcf: line 97, column 94
    function value_26 () : java.lang.String {
      return accountingConfigUtil.getFirstTAccountOwnerTAccount(chargePattern)
    }
    
    // 'value' attribute on RowIterator (id=OtherTAccountsAndTransctionNames) at AccountingConfig.pcf: line 106, column 48
    function value_32 () : java.lang.String[] {
      return accountingConfigUtil.getLastTAccountOwnerTAccounts(chargePattern)
    }
    
    property get chargePattern () : entity.ChargePattern {
      return getIteratedValue(1) as entity.ChargePattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/tools/AccountingConfig.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends IteratorEntry3ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=ChargeName_Cell) at AccountingConfig.pcf: line 111, column 51
    function value_28 () : java.lang.Object {
      return null
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at AccountingConfig.pcf: line 114, column 54
    function value_30 () : java.lang.String {
      return TAccountTransactionName
    }
    
    property get TAccountTransactionName () : java.lang.String {
      return getIteratedValue(2) as java.lang.String
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/tools/AccountingConfig.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountingConfigExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TypeKeyCell (id=TAccountType_Cell) at AccountingConfig.pcf: line 46, column 53
    function valueRoot_10 () : java.lang.Object {
      return tAccountPattern
    }
    
    // 'value' attribute on TextCell (id=TAccountOwner_Cell) at AccountingConfig.pcf: line 34, column 77
    function valueRoot_5 () : java.lang.Object {
      return tAccountPattern.TAccountOwnerPattern
    }
    
    // 'value' attribute on TextCell (id=TAccountOwner_Cell) at AccountingConfig.pcf: line 34, column 77
    function value_4 () : java.lang.String {
      return tAccountPattern.TAccountOwnerPattern.DisplayName
    }
    
    // 'value' attribute on TextCell (id=TAccountName_Cell) at AccountingConfig.pcf: line 40, column 55
    function value_7 () : entity.TAccountPattern {
      return tAccountPattern
    }
    
    // 'value' attribute on TypeKeyCell (id=TAccountType_Cell) at AccountingConfig.pcf: line 46, column 53
    function value_9 () : typekey.TAccountType {
      return tAccountPattern.TAccountType
    }
    
    property get tAccountPattern () : entity.TAccountPattern {
      return getIteratedValue(1) as entity.TAccountPattern
    }
    
    
  }
  
  
}