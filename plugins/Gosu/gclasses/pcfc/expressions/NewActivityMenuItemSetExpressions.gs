package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/NewActivityMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewActivityMenuItemSetExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/NewActivityMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=item) at NewActivityMenuItemSet.pcf: line 37, column 121
    function action_1 () : void {
      NewActivityPopup.push(pattern, isShared, troubleTicket, account, policyPeriod)
    }
    
    // 'action' attribute on MenuItem (id=item) at NewActivityMenuItemSet.pcf: line 37, column 121
    function action_dest_2 () : pcf.api.Destination {
      return pcf.NewActivityPopup.createDestination(pattern, isShared, troubleTicket, account, policyPeriod)
    }
    
    // 'label' attribute on MenuItem (id=item) at NewActivityMenuItemSet.pcf: line 37, column 121
    function label_3 () : java.lang.Object {
      return pattern.Subject == null ? DisplayKey.get("Java.NewActivity.NoSubject") : pattern.Subject
    }
    
    property get pattern () : entity.ActivityPattern {
      return getIteratedValue(2) as entity.ActivityPattern
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/NewActivityMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends NewActivityMenuItemSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on MenuItem (id=NewActivityMenuItemSet_Category) at NewActivityMenuItemSet.pcf: line 29, column 149
    function label_5 () : java.lang.Object {
      return categoryMenuItem.Category == null ? DisplayKey.get("Java.NewActivity.NoCategory") : categoryMenuItem.Category.DisplayName
    }
    
    // 'value' attribute on MenuItemIterator at NewActivityMenuItemSet.pcf: line 33, column 48
    function value_4 () : entity.ActivityPattern[] {
      return categoryMenuItem.Patterns
    }
    
    property get categoryMenuItem () : gw.api.activity.ActivityPatternMenuCategory {
      return getIteratedValue(1) as gw.api.activity.ActivityPatternMenuCategory
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/NewActivityMenuItemSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewActivityMenuItemSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at NewActivityMenuItemSet.pcf: line 22, column 40
    function initialValue_0 () : entity.ActivityPattern[] {
      return gw.api.web.admin.ActivityPatternsUtil.AllManualActivityPatterns
    }
    
    // 'value' attribute on MenuItemIterator at NewActivityMenuItemSet.pcf: line 26, column 65
    function value_6 () : gw.api.activity.ActivityPatternMenuCategory[] {
      return gw.api.activity.NewActivityMenuUtil.createMenuItems(activityPatterns)
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get activityPatterns () : entity.ActivityPattern[] {
      return getVariableValue("activityPatterns", 0) as entity.ActivityPattern[]
    }
    
    property set activityPatterns ($arg :  entity.ActivityPattern[]) {
      setVariableValue("activityPatterns", 0, $arg)
    }
    
    property get isShared () : Boolean {
      return getRequireValue("isShared", 0) as Boolean
    }
    
    property set isShared ($arg :  Boolean) {
      setRequireValue("isShared", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    property get troubleTicket () : TroubleTicket {
      return getRequireValue("troubleTicket", 0) as TroubleTicket
    }
    
    property set troubleTicket ($arg :  TroubleTicket) {
      setRequireValue("troubleTicket", 0, $arg)
    }
    
    
  }
  
  
}