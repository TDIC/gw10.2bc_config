package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/document/DocumentDetailsEditDVPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DocumentDetailsEditDVPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/document/DocumentDetailsEditDVPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DocumentDetailsEditDVPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at DocumentDetailsEditDVPanelSet.pcf: line 13, column 93
    function def_onEnter_0 (def :  pcf.DocumentMetadataEditDV_default) : void {
      def.onEnter(documentDetailsApplicationHelper, fromTemplate)
    }
    
    // 'def' attribute on PanelRef at DocumentDetailsEditDVPanelSet.pcf: line 13, column 93
    function def_onEnter_2 (def :  pcf.DocumentMetadataEditDV_email_sent) : void {
      def.onEnter(documentDetailsApplicationHelper, fromTemplate)
    }
    
    // 'def' attribute on PanelRef at DocumentDetailsEditDVPanelSet.pcf: line 13, column 93
    function def_refreshVariables_1 (def :  pcf.DocumentMetadataEditDV_default) : void {
      def.refreshVariables(documentDetailsApplicationHelper, fromTemplate)
    }
    
    // 'def' attribute on PanelRef at DocumentDetailsEditDVPanelSet.pcf: line 13, column 93
    function def_refreshVariables_3 (def :  pcf.DocumentMetadataEditDV_email_sent) : void {
      def.refreshVariables(documentDetailsApplicationHelper, fromTemplate)
    }
    
    property get documentDetailsApplicationHelper () : gw.document.DocumentDetailsApplicationHelper {
      return getRequireValue("documentDetailsApplicationHelper", 0) as gw.document.DocumentDetailsApplicationHelper
    }
    
    property set documentDetailsApplicationHelper ($arg :  gw.document.DocumentDetailsApplicationHelper) {
      setRequireValue("documentDetailsApplicationHelper", 0, $arg)
    }
    
    property get fromTemplate () : boolean {
      return getRequireValue("fromTemplate", 0) as java.lang.Boolean
    }
    
    property set fromTemplate ($arg :  boolean) {
      setRequireValue("fromTemplate", 0, $arg)
    }
    
    
  }
  
  
}