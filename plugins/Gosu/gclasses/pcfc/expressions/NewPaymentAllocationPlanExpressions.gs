package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/allocationplan/NewPaymentAllocationPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewPaymentAllocationPlanExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/allocationplan/NewPaymentAllocationPlan.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewPaymentAllocationPlanExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=NewPaymentAllocationPlan) at NewPaymentAllocationPlan.pcf: line 14, column 76
    function afterCancel_3 () : void {
      PaymentAllocationPlans.go()
    }
    
    // 'afterCancel' attribute on Page (id=NewPaymentAllocationPlan) at NewPaymentAllocationPlan.pcf: line 14, column 76
    function afterCancel_dest_4 () : pcf.api.Destination {
      return pcf.PaymentAllocationPlans.createDestination()
    }
    
    // 'afterCommit' attribute on Page (id=NewPaymentAllocationPlan) at NewPaymentAllocationPlan.pcf: line 14, column 76
    function afterCommit_5 (TopLocation :  pcf.api.Location) : void {
      PaymentAllocationPlans.go()
    }
    
    // 'canVisit' attribute on Page (id=NewPaymentAllocationPlan) at NewPaymentAllocationPlan.pcf: line 14, column 76
    static function canVisit_6 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.payallocplancreate
    }
    
    // 'def' attribute on ScreenRef at NewPaymentAllocationPlan.pcf: line 22, column 71
    function def_onEnter_1 (def :  pcf.PaymentAllocationPlanDetailScreen) : void {
      def.onEnter(paymentAllocationPlan)
    }
    
    // 'def' attribute on ScreenRef at NewPaymentAllocationPlan.pcf: line 22, column 71
    function def_refreshVariables_2 (def :  pcf.PaymentAllocationPlanDetailScreen) : void {
      def.refreshVariables(paymentAllocationPlan)
    }
    
    // 'initialValue' attribute on Variable at NewPaymentAllocationPlan.pcf: line 20, column 37
    function initialValue_0 () : PaymentAllocationPlan {
      return initPaymentAllocationPlan()
    }
    
    // 'parent' attribute on Page (id=NewPaymentAllocationPlan) at NewPaymentAllocationPlan.pcf: line 14, column 76
    static function parent_7 () : pcf.api.Destination {
      return pcf.PaymentAllocationPlans.createDestination()
    }
    
    override property get CurrentLocation () : pcf.NewPaymentAllocationPlan {
      return super.CurrentLocation as pcf.NewPaymentAllocationPlan
    }
    
    property get paymentAllocationPlan () : PaymentAllocationPlan {
      return getVariableValue("paymentAllocationPlan", 0) as PaymentAllocationPlan
    }
    
    property set paymentAllocationPlan ($arg :  PaymentAllocationPlan) {
      setVariableValue("paymentAllocationPlan", 0, $arg)
    }
    
    function initPaymentAllocationPlan(): PaymentAllocationPlan {
      var newPaymentAllocationPlan = new PaymentAllocationPlan(CurrentLocation);
      newPaymentAllocationPlan.EffectiveDate = gw.api.util.DateUtil.currentDate();
      return newPaymentAllocationPlan;
    }
    
    
  }
  
  
}