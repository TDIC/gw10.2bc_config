package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/CreateSubrogationTargetsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreateSubrogationTargetsScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/account/CreateSubrogationTargetsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreateSubrogationTargetsScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at CreateSubrogationTargetsScreen.pcf: line 16, column 57
    function initialValue_0 () : gw.api.web.account.AccountSearchConverter {
      return new gw.api.web.account.AccountSearchConverter()
    }
    
    property get account () : gw.api.web.accounting.TAccountOwnerReference {
      return getRequireValue("account", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set account ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setRequireValue("account", 0, $arg)
    }
    
    property get accountSearchConverter () : gw.api.web.account.AccountSearchConverter {
      return getVariableValue("accountSearchConverter", 0) as gw.api.web.account.AccountSearchConverter
    }
    
    property set accountSearchConverter ($arg :  gw.api.web.account.AccountSearchConverter) {
      setVariableValue("accountSearchConverter", 0, $arg)
    }
    
    property get subrogation () : Subrogation {
      return getRequireValue("subrogation", 0) as Subrogation
    }
    
    property set subrogation ($arg :  Subrogation) {
      setRequireValue("subrogation", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/CreateSubrogationTargetsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=Select) at CreateSubrogationTargetsScreen.pcf: line 57, column 48
    function action_6 () : void {
      account.TAccountOwner = accountSearchView.Account; (CurrentLocation as pcf.api.Wizard).next()
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at CreateSubrogationTargetsScreen.pcf: line 64, column 35
    function valueRoot_8 () : java.lang.Object {
      return accountSearchView
    }
    
    // 'value' attribute on TypeKeyCell (id=Segment_Cell) at CreateSubrogationTargetsScreen.pcf: line 70, column 57
    function value_10 () : typekey.AccountSegment {
      return accountSearchView.Segment
    }
    
    // 'value' attribute on DateCell (id=CreationDate_Cell) at CreateSubrogationTargetsScreen.pcf: line 76, column 59
    function value_13 () : java.util.Date {
      return accountSearchView.CreateTime
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at CreateSubrogationTargetsScreen.pcf: line 64, column 35
    function value_7 () : java.lang.String {
      return accountSearchView.AccountNumber
    }
    
    property get accountSearchView () : entity.AccountSearchView {
      return getIteratedValue(2) as entity.AccountSearchView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/CreateSubrogationTargetsScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends CreateSubrogationTargetsScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at CreateSubrogationTargetsScreen.pcf: line 33, column 51
    function def_onEnter_1 (def :  pcf.AccountSearchDV) : void {
      def.onEnter(searchCriteria)
    }
    
    // 'def' attribute on PanelRef at CreateSubrogationTargetsScreen.pcf: line 33, column 51
    function def_refreshVariables_2 (def :  pcf.AccountSearchDV) : void {
      def.refreshVariables(searchCriteria)
    }
    
    // 'searchCriteria' attribute on SearchPanel at CreateSubrogationTargetsScreen.pcf: line 31, column 89
    function searchCriteria_18 () : gw.search.AccountSearchCriteria {
      return new gw.search.AccountSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at CreateSubrogationTargetsScreen.pcf: line 31, column 89
    function search_17 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria, StringCriterionMode.TC_STARTSWITH)
    }
    
    // 'value' attribute on TextCell (id=Number_Cell) at CreateSubrogationTargetsScreen.pcf: line 64, column 35
    function sortValue_3 (accountSearchView :  entity.AccountSearchView) : java.lang.Object {
      return accountSearchView.AccountNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Segment_Cell) at CreateSubrogationTargetsScreen.pcf: line 70, column 57
    function sortValue_4 (accountSearchView :  entity.AccountSearchView) : java.lang.Object {
      return accountSearchView.Segment
    }
    
    // 'sortBy' attribute on DateCell (id=CreationDate_Cell) at CreateSubrogationTargetsScreen.pcf: line 76, column 59
    function sortValue_5 (accountSearchView :  entity.AccountSearchView) : java.lang.Object {
      return accountSearchView.CreateTime
    }
    
    // 'value' attribute on RowIterator at CreateSubrogationTargetsScreen.pcf: line 44, column 94
    function value_16 () : gw.api.database.IQueryBeanResult<entity.AccountSearchView> {
      return accountSearchViews
    }
    
    property get accountSearchViews () : gw.api.database.IQueryBeanResult<AccountSearchView> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<AccountSearchView>
    }
    
    property get searchCriteria () : gw.search.AccountSearchCriteria {
      return getCriteriaValue(1) as gw.search.AccountSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.AccountSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}