package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountPaymentRequests.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountPaymentRequestsExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountPaymentRequests.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountPaymentRequestsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    static function __constructorIndex (account :  Account, initialSelectedMoneyRcvd :  DirectBillMoneyRcvd) : int {
      return 1
    }
    
    // 'canVisit' attribute on Page (id=AccountPaymentRequests) at AccountPaymentRequests.pcf: line 11, column 74
    static function canVisit_55 (account :  Account, initialSelectedMoneyRcvd :  DirectBillMoneyRcvd) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctpmntview
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=closeRequest) at AccountPaymentRequests.pcf: line 59, column 74
    function checkedRowAction_7 (paymentRequest :  entity.PaymentRequest, CheckedValue :  entity.PaymentRequest) : void {
      CheckedValue.cancelRequest()
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=editRequest) at AccountPaymentRequests.pcf: line 65, column 74
    function checkedRowAction_8 (paymentRequest :  entity.PaymentRequest, CheckedValue :  entity.PaymentRequest) : void {
      PaymentRequestDetailPage.push(CheckedValue, true)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountPaymentRequests.pcf: line 35, column 84
    function filter_0 () : gw.api.filters.IFilter {
      return new gw.api.web.payment.PaymentRequestFilters.SentRange(30)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountPaymentRequests.pcf: line 37, column 84
    function filter_1 () : gw.api.filters.IFilter {
      return new gw.api.web.payment.PaymentRequestFilters.SentRange(60)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountPaymentRequests.pcf: line 39, column 84
    function filter_2 () : gw.api.filters.IFilter {
      return new gw.api.web.payment.PaymentRequestFilters.SentRange(90)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountPaymentRequests.pcf: line 41, column 76
    function filter_3 () : gw.api.filters.IFilter {
      return new gw.api.web.payment.PaymentRequestFilters.All()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountPaymentRequests.pcf: line 49, column 80
    function filter_5 () : gw.api.filters.IFilter {
      return new gw.api.web.payment.PaymentRequestFilters.AllOpen()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountPaymentRequests.pcf: line 51, column 83
    function filter_6 () : gw.api.filters.IFilter {
      return new gw.api.web.payment.PaymentRequestFilters.Last30Days()
    }
    
    // Page (id=AccountPaymentRequests) at AccountPaymentRequests.pcf: line 11, column 74
    static function parent_56 (account :  Account, initialSelectedMoneyRcvd :  DirectBillMoneyRcvd) : pcf.api.Destination {
      return pcf.AccountDetailPayments.createDestination(account)
    }
    
    // 'sortBy' attribute on DateCell (id=invoiceDate_Cell) at AccountPaymentRequests.pcf: line 93, column 59
    function sortValue_10 (paymentRequest :  entity.PaymentRequest) : java.lang.Object {
      return true
    }
    
    // 'sortBy' attribute on DateCell (id=requestDate_Cell) at AccountPaymentRequests.pcf: line 98, column 53
    function sortValue_11 (paymentRequest :  entity.PaymentRequest) : java.lang.Object {
      return false
    }
    
    // 'sortBy' attribute on DateCell (id=changeDeadlineDate_Cell) at AccountPaymentRequests.pcf: line 103, column 60
    function sortValue_12 (paymentRequest :  entity.PaymentRequest) : java.lang.Object {
      return false
    }
    
    // 'sortBy' attribute on DateCell (id=draftDate_Cell) at AccountPaymentRequests.pcf: line 108, column 51
    function sortValue_13 (paymentRequest :  entity.PaymentRequest) : java.lang.Object {
      return false
    }
    
    // 'value' attribute on TextCell (id=invoiceNumber_Cell) at AccountPaymentRequests.pcf: line 119, column 63
    function sortValue_14 (paymentRequest :  entity.PaymentRequest) : java.lang.Object {
      return paymentRequest.Invoice.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=unappliedFund_Cell) at AccountPaymentRequests.pcf: line 124, column 51
    function sortValue_15 (paymentRequest :  entity.PaymentRequest) : java.lang.Object {
      return paymentRequest.UnappliedFund_TDIC
    }
    
    // 'value' attribute on MonetaryAmountCell (id=paymentAmount_Cell) at AccountPaymentRequests.pcf: line 130, column 48
    function sortValue_16 (paymentRequest :  entity.PaymentRequest) : java.lang.Object {
      return paymentRequest.Amount
    }
    
    // 'value' attribute on TextCell (id=payRequestBy_Cell) at AccountPaymentRequests.pcf: line 135, column 42
    function sortValue_17 (paymentRequest :  entity.PaymentRequest) : java.lang.Object {
      return paymentRequest.CreateUser
    }
    
    // 'value' attribute on TypeKeyCell (id=requestStatus_Cell) at AccountPaymentRequests.pcf: line 87, column 31
    function sortValue_9 (paymentRequest :  entity.PaymentRequest) : java.lang.Object {
      return paymentRequest.Status
    }
    
    // '$$sumValue' attribute on RowIterator (id=paymentRequestList) at AccountPaymentRequests.pcf: line 130, column 48
    function sumValueRoot_19 (paymentRequest :  entity.PaymentRequest) : java.lang.Object {
      return paymentRequest
    }
    
    // 'footerSumValue' attribute on RowIterator (id=paymentRequestList) at AccountPaymentRequests.pcf: line 130, column 48
    function sumValue_18 (paymentRequest :  entity.PaymentRequest) : java.lang.Object {
      return paymentRequest.Amount
    }
    
    // 'value' attribute on RowIterator (id=paymentRequestList) at AccountPaymentRequests.pcf: line 77, column 87
    function value_54 () : gw.api.database.IQueryBeanResult<entity.PaymentRequest> {
      return account.PaymentRequests as gw.api.database.IQueryBeanResult<PaymentRequest>
    }
    
    override property get CurrentLocation () : pcf.AccountPaymentRequests {
      return super.CurrentLocation as pcf.AccountPaymentRequests
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get initialSelectedMoneyRcvd () : DirectBillMoneyRcvd {
      return getVariableValue("initialSelectedMoneyRcvd", 0) as DirectBillMoneyRcvd
    }
    
    property set initialSelectedMoneyRcvd ($arg :  DirectBillMoneyRcvd) {
      setVariableValue("initialSelectedMoneyRcvd", 0, $arg)
    }
    
    property get selectedPaymentRequest () : PaymentRequest {
      return getVariableValue("selectedPaymentRequest", 0) as PaymentRequest
    }
    
    property set selectedPaymentRequest ($arg :  PaymentRequest) {
      setVariableValue("selectedPaymentRequest", 0, $arg)
    }
    
    function checkInStoppableState(paymentRequest : PaymentRequest) : boolean {
          return (paymentRequest.Status == PaymentRequestStatus.TC_CREATED || paymentRequest.Status ==
          PaymentRequestStatus.TC_REQUESTED);
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountPaymentRequests.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountPaymentRequestsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=invoiceNumber_Cell) at AccountPaymentRequests.pcf: line 119, column 63
    function action_38 () : void {
      AccountDetailInvoices.push(paymentRequest.Invoice.Account, paymentRequest.Invoice)
    }
    
    // 'action' attribute on TextCell (id=invoiceNumber_Cell) at AccountPaymentRequests.pcf: line 119, column 63
    function action_dest_39 () : pcf.api.Destination {
      return pcf.AccountDetailInvoices.createDestination(paymentRequest.Invoice.Account, paymentRequest.Invoice)
    }
    
    // 'checkBoxVisible' attribute on RowIterator (id=paymentRequestList) at AccountPaymentRequests.pcf: line 77, column 87
    function checkBoxVisible_53 () : java.lang.Boolean {
      return checkInStoppableState(paymentRequest)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=paymentAmount_Cell) at AccountPaymentRequests.pcf: line 130, column 48
    function currency_48 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on TypeKeyCell (id=requestStatus_Cell) at AccountPaymentRequests.pcf: line 87, column 31
    function valueRoot_21 () : java.lang.Object {
      return paymentRequest
    }
    
    // 'value' attribute on DateCell (id=invoiceDate_Cell) at AccountPaymentRequests.pcf: line 93, column 59
    function valueRoot_24 () : java.lang.Object {
      return paymentRequest.Invoice
    }
    
    // 'value' attribute on TypeKeyCell (id=requestStatus_Cell) at AccountPaymentRequests.pcf: line 87, column 31
    function value_20 () : typekey.PaymentRequestStatus {
      return paymentRequest.Status
    }
    
    // 'value' attribute on DateCell (id=invoiceDate_Cell) at AccountPaymentRequests.pcf: line 93, column 59
    function value_23 () : java.util.Date {
      return paymentRequest.Invoice.EventDate
    }
    
    // 'value' attribute on DateCell (id=requestDate_Cell) at AccountPaymentRequests.pcf: line 98, column 53
    function value_26 () : java.util.Date {
      return paymentRequest.RequestDate
    }
    
    // 'value' attribute on DateCell (id=changeDeadlineDate_Cell) at AccountPaymentRequests.pcf: line 103, column 60
    function value_29 () : java.util.Date {
      return paymentRequest.ChangeDeadlineDate
    }
    
    // 'value' attribute on DateCell (id=draftDate_Cell) at AccountPaymentRequests.pcf: line 108, column 51
    function value_32 () : java.util.Date {
      return paymentRequest.DraftDate
    }
    
    // 'value' attribute on DateCell (id=dueDate_Cell) at AccountPaymentRequests.pcf: line 113, column 49
    function value_35 () : java.util.Date {
      return paymentRequest.DueDate
    }
    
    // 'value' attribute on TextCell (id=invoiceNumber_Cell) at AccountPaymentRequests.pcf: line 119, column 63
    function value_40 () : java.lang.String {
      return paymentRequest.Invoice.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=unappliedFund_Cell) at AccountPaymentRequests.pcf: line 124, column 51
    function value_43 () : entity.UnappliedFund {
      return paymentRequest.UnappliedFund_TDIC
    }
    
    // 'value' attribute on MonetaryAmountCell (id=paymentAmount_Cell) at AccountPaymentRequests.pcf: line 130, column 48
    function value_46 () : gw.pl.currency.MonetaryAmount {
      return paymentRequest.Amount
    }
    
    // 'value' attribute on TextCell (id=payRequestBy_Cell) at AccountPaymentRequests.pcf: line 135, column 42
    function value_50 () : entity.User {
      return paymentRequest.CreateUser
    }
    
    property get paymentRequest () : entity.PaymentRequest {
      return getIteratedValue(1) as entity.PaymentRequest
    }
    
    
  }
  
  
}