package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketActivitiesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TroubleTicketActivitiesDVExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketActivitiesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TroubleTicketActivitiesDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=ViewDetails_Cell) at TroubleTicketActivitiesDV.pcf: line 82, column 69
    function action_34 () : void {
      ActivityDetailForward.goInWorkspace(activity)
    }
    
    // 'action' attribute on Link (id=LinkedDocument) at TroubleTicketActivitiesDV.pcf: line 88, column 162
    function action_36 () : void {
      OnBaseDocumentListPopup.push(activity, acc.onbase.configuration.DocumentLinkType.activityid, activity.Description as String, acc.onbase.api.application.DocumentLinking.getRelatedFromTroubleTicket(troubleTicket))  
    }
    
    // 'action' attribute on TextCell (id=ViewDetails_Cell) at TroubleTicketActivitiesDV.pcf: line 82, column 69
    function action_dest_35 () : pcf.api.Destination {
      return pcf.ActivityDetailForward.createDestination(activity)
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at TroubleTicketActivitiesDV.pcf: line 58, column 46
    function fontColor_17 () : java.lang.Object {
      return activity.Overdue == true ? gw.api.web.color.GWColor.THEME_URGENCY_HIGH : null
    }
    
    // 'iconColor' attribute on BooleanRadioCell (id=Escalated_Cell) at TroubleTicketActivitiesDV.pcf: line 52, column 45
    function iconColor_15 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_URGENCY_HIGH
    }
    
    // 'label' attribute on Link (id=LinkedDocument) at TroubleTicketActivitiesDV.pcf: line 88, column 162
    function label_37 () : java.lang.Object {
      return acc.onbase.api.application.DocumentLinking.getLinkingDocumentUILabel(activity, acc.onbase.configuration.DocumentLinkType.activityid) 
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at TroubleTicketActivitiesDV.pcf: line 52, column 45
    function valueRoot_14 () : java.lang.Object {
      return activity
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at TroubleTicketActivitiesDV.pcf: line 52, column 45
    function value_13 () : java.lang.Boolean {
      return activity.Escalated
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at TroubleTicketActivitiesDV.pcf: line 58, column 46
    function value_18 () : java.util.Date {
      return activity.TargetDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at TroubleTicketActivitiesDV.pcf: line 63, column 47
    function value_23 () : typekey.Priority {
      return activity.Priority
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at TroubleTicketActivitiesDV.pcf: line 67, column 50
    function value_26 () : java.lang.String {
      return activity.ActivityStatus
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at TroubleTicketActivitiesDV.pcf: line 71, column 43
    function value_29 () : java.lang.String {
      return activity.Subject
    }
    
    // 'value' attribute on TextCell (id=AssignedTo_Cell) at TroubleTicketActivitiesDV.pcf: line 76, column 47
    function value_32 () : java.lang.Object {
      return gw.api.web.activity.ActivityUtil.isShared(activity) ? DisplayKey.get("Web.ActivityDetail.AssignedToShared") : activity.AssignedTo
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at TroubleTicketActivitiesDV.pcf: line 58, column 46
    function verifyFontColorIsAllowedType_20 ($$arg :  gw.api.web.color.GWColor) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at TroubleTicketActivitiesDV.pcf: line 58, column 46
    function verifyFontColorIsAllowedType_20 ($$arg :  java.lang.String) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at TroubleTicketActivitiesDV.pcf: line 58, column 46
    function verifyFontColor_21 () : void {
      var __fontColorArg = activity.Overdue == true ? gw.api.web.color.GWColor.THEME_URGENCY_HIGH : null
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the fontColor return type is not a valid type for use with a ValueWidget's fontColor
      // It needs to return a String or a GWColor.
      verifyFontColorIsAllowedType_20(__fontColorArg)
    }
    
    property get activity () : entity.Activity {
      return getIteratedValue(1) as entity.Activity
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/troubleticket/TroubleTicketActivitiesDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TroubleTicketActivitiesDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on ToolbarButton (id=NewActivityButton) at TroubleTicketActivitiesDV.pcf: line 22, column 90
    function available_2 () : java.lang.Boolean {
      return !troubleTicket.IsClosed
    }
    
    // 'def' attribute on MenuItemSetRef at TroubleTicketActivitiesDV.pcf: line 24, column 77
    function def_onEnter_0 (def :  pcf.NewActivityMenuItemSet) : void {
      def.onEnter(false, troubleTicket,null,null)
    }
    
    // 'def' attribute on MenuItemSetRef at TroubleTicketActivitiesDV.pcf: line 31, column 78
    function def_onEnter_3 (def :  pcf.NewActivityMenuItemSet) : void {
      def.onEnter(true, troubleTicket, null, null)
    }
    
    // 'def' attribute on MenuItemSetRef at TroubleTicketActivitiesDV.pcf: line 24, column 77
    function def_refreshVariables_1 (def :  pcf.NewActivityMenuItemSet) : void {
      def.refreshVariables(false, troubleTicket,null,null)
    }
    
    // 'def' attribute on MenuItemSetRef at TroubleTicketActivitiesDV.pcf: line 31, column 78
    function def_refreshVariables_4 (def :  pcf.NewActivityMenuItemSet) : void {
      def.refreshVariables(true, troubleTicket, null, null)
    }
    
    // 'iconColor' attribute on BooleanRadioCell (id=Escalated_Cell) at TroubleTicketActivitiesDV.pcf: line 52, column 45
    function iconColor_6 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_URGENCY_HIGH
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at TroubleTicketActivitiesDV.pcf: line 67, column 50
    function sortValue_10 (activity :  entity.Activity) : java.lang.Object {
      return activity.ActivityStatus
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at TroubleTicketActivitiesDV.pcf: line 71, column 43
    function sortValue_11 (activity :  entity.Activity) : java.lang.Object {
      return activity.Subject
    }
    
    // 'value' attribute on TextCell (id=AssignedTo_Cell) at TroubleTicketActivitiesDV.pcf: line 76, column 47
    function sortValue_12 (activity :  entity.Activity) : java.lang.Object {
      return gw.api.web.activity.ActivityUtil.isShared(activity) ? DisplayKey.get("Web.ActivityDetail.AssignedToShared") : activity.AssignedTo
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at TroubleTicketActivitiesDV.pcf: line 52, column 45
    function sortValue_7 (activity :  entity.Activity) : java.lang.Object {
      return activity.Escalated
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at TroubleTicketActivitiesDV.pcf: line 58, column 46
    function sortValue_8 (activity :  entity.Activity) : java.lang.Object {
      return activity.TargetDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at TroubleTicketActivitiesDV.pcf: line 63, column 47
    function sortValue_9 (activity :  entity.Activity) : java.lang.Object {
      return activity.Priority
    }
    
    // 'toRemove' attribute on RowIterator at TroubleTicketActivitiesDV.pcf: line 43, column 43
    function toRemove_38 (activity :  entity.Activity) : void {
      troubleTicket.removeFromActivities(activity)
    }
    
    // 'value' attribute on RowIterator at TroubleTicketActivitiesDV.pcf: line 43, column 43
    function value_39 () : entity.Activity[] {
      return troubleTicket.Activities
    }
    
    property get troubleTicket () : TroubleTicket {
      return getRequireValue("troubleTicket", 0) as TroubleTicket
    }
    
    property set troubleTicket ($arg :  TroubleTicket) {
      setRequireValue("troubleTicket", 0, $arg)
    }
    
    
  }
  
  
}