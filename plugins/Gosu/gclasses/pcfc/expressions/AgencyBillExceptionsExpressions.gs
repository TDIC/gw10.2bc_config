package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExceptions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillExceptionsExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillExceptions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillExceptionsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    static function __constructorIndex (producer :  Producer, tabToSelect :  int) : int {
      return 1
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesMenuItem (id=Commission) at AgencyBillExceptions.pcf: line 73, column 61
    function allCheckedRowsAction_10 (CheckedValues :  gw.api.web.invoice.ExceptionItemView[], CheckedValuesErrors :  java.util.Map) : void {
      writeOffExceptions(CheckedValues, AgencyWriteoffType.TC_COMMISSION)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=StartDelinquency) at AgencyBillExceptions.pcf: line 81, column 104
    function allCheckedRowsAction_11 (CheckedValues :  gw.api.web.invoice.ExceptionItemView[], CheckedValuesErrors :  java.util.Map) : void {
      StartDelinquencyProcessPopup.push(gw.api.web.producer.agencybill.ExceptionItemUtil.getPolicyPeriodsAssociatedWithSelectedExceptionItemViews( CheckedValues*.ExceptionItem ))
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=CarryForward) at AgencyBillExceptions.pcf: line 87, column 100
    function allCheckedRowsAction_12 (CheckedValues :  gw.api.web.invoice.ExceptionItemView[], CheckedValuesErrors :  java.util.Map) : void {
      gw.api.web.producer.agencybill.ExceptionItemUtil.carryForwardPaymentExceptions( CheckedValues, CurrentLocation )
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=EditComments) at AgencyBillExceptions.pcf: line 99, column 100
    function allCheckedRowsAction_14 (CheckedValues :  gw.api.web.invoice.ExceptionItemView[], CheckedValuesErrors :  java.util.Map) : void {
      AgencyExceptionItemCommentsPopup.push(CheckedValues)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=CarryForward) at AgencyBillExceptions.pcf: line 122, column 100
    function allCheckedRowsAction_20 (CheckedValues :  gw.api.web.invoice.ExceptionItemView[], CheckedValuesErrors :  java.util.Map) : void {
      gw.api.web.producer.agencybill.ExceptionItemUtil.carryForwardPromiseExceptions( CheckedValues, CurrentLocation )
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=Dismiss) at AgencyBillExceptions.pcf: line 151, column 111
    function allCheckedRowsAction_27 (CheckedValues :  entity.AgencyCycleProcess[], CheckedValuesErrors :  java.util.Map) : void {
      dismissExceptions(CheckedValues, true)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=EditComments) at AgencyBillExceptions.pcf: line 157, column 116
    function allCheckedRowsAction_28 (CheckedValues :  entity.AgencyCycleProcess[], CheckedValuesErrors :  java.util.Map) : void {
      AgencyCycleExceptionCommentsPopup.push(CheckedValues, true)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=Dismiss) at AgencyBillExceptions.pcf: line 174, column 111
    function allCheckedRowsAction_33 (CheckedValues :  entity.AgencyCycleProcess[], CheckedValuesErrors :  java.util.Map) : void {
      dismissExceptions(CheckedValues, false)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=EditComments) at AgencyBillExceptions.pcf: line 180, column 116
    function allCheckedRowsAction_34 (CheckedValues :  entity.AgencyCycleProcess[], CheckedValuesErrors :  java.util.Map) : void {
      AgencyCycleExceptionCommentsPopup.push(CheckedValues, false)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesMenuItem (id=Both) at AgencyBillExceptions.pcf: line 65, column 55
    function allCheckedRowsAction_6 (CheckedValues :  gw.api.web.invoice.ExceptionItemView[], CheckedValuesErrors :  java.util.Map) : void {
      writeOffExceptions(CheckedValues, AgencyWriteoffType.TC_BOTH)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesMenuItem (id=Gross) at AgencyBillExceptions.pcf: line 69, column 56
    function allCheckedRowsAction_8 (CheckedValues :  gw.api.web.invoice.ExceptionItemView[], CheckedValuesErrors :  java.util.Map) : void {
      writeOffExceptions(CheckedValues, AgencyWriteoffType.TC_GROSS)
    }
    
    // 'canVisit' attribute on Page (id=AgencyBillExceptions) at AgencyBillExceptions.pcf: line 8, column 82
    static function canVisit_39 (producer :  Producer, tabToSelect :  int) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodabexceptionsview
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=OverrideCommission) at AgencyBillExceptions.pcf: line 94, column 104
    function checkedRowAction_13 (element :  gw.api.web.invoice.ExceptionItemView, CheckedValue :  gw.api.web.invoice.ExceptionItemView) : void {
      CommissionRatesPopup.push(gw.api.domain.accounting.ChargeUtil.createChargeArray(CheckedValue.ExceptionItem, CurrentLocation));
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=EditItem) at AgencyBillExceptions.pcf: line 56, column 96
    function checkedRowAction_4 (element :  gw.api.web.invoice.ExceptionItemView, CheckedValue :  gw.api.web.invoice.ExceptionItemView) : void {
      InvoiceItemHistoryPopup.push(CheckedValue.ExceptionItem.InvoiceItem)
    }
    
    // 'def' attribute on PanelRef (id=PaymentExceptionsPanel) at AgencyBillExceptions.pcf: line 49, column 41
    function def_onEnter_15 (def :  pcf.AgencyBillExceptionsLV) : void {
      def.onEnter(paymentExceptions, true, false)
    }
    
    // 'def' attribute on PanelRef (id=PromiseExceptionsPanel) at AgencyBillExceptions.pcf: line 109, column 41
    function def_onEnter_23 (def :  pcf.AgencyBillExceptionsLV) : void {
      def.onEnter(promiseExceptions, true, false )
    }
    
    // 'def' attribute on PanelRef (id=MissingPaymentsPanel) at AgencyBillExceptions.pcf: line 144, column 39
    function def_onEnter_29 (def :  pcf.CycleExceptionsLV) : void {
      def.onEnter(agencyCyclesWithLatePaymentException, true)
    }
    
    // 'def' attribute on PanelRef (id=MissingPromisesPanel) at AgencyBillExceptions.pcf: line 167, column 39
    function def_onEnter_35 (def :  pcf.CycleExceptionsLV) : void {
      def.onEnter(agencyCyclesWithLatePromiseException, false)
    }
    
    // 'def' attribute on PanelRef (id=PaymentExceptionsPanel) at AgencyBillExceptions.pcf: line 49, column 41
    function def_refreshVariables_16 (def :  pcf.AgencyBillExceptionsLV) : void {
      def.refreshVariables(paymentExceptions, true, false)
    }
    
    // 'def' attribute on PanelRef (id=PromiseExceptionsPanel) at AgencyBillExceptions.pcf: line 109, column 41
    function def_refreshVariables_24 (def :  pcf.AgencyBillExceptionsLV) : void {
      def.refreshVariables(promiseExceptions, true, false )
    }
    
    // 'def' attribute on PanelRef (id=MissingPaymentsPanel) at AgencyBillExceptions.pcf: line 144, column 39
    function def_refreshVariables_30 (def :  pcf.CycleExceptionsLV) : void {
      def.refreshVariables(agencyCyclesWithLatePaymentException, true)
    }
    
    // 'def' attribute on PanelRef (id=MissingPromisesPanel) at AgencyBillExceptions.pcf: line 167, column 39
    function def_refreshVariables_36 (def :  pcf.CycleExceptionsLV) : void {
      def.refreshVariables(agencyCyclesWithLatePromiseException, false)
    }
    
    // 'initialValue' attribute on Variable at AgencyBillExceptions.pcf: line 23, column 54
    function initialValue_0 () : gw.api.web.invoice.ExceptionItemView[] {
      return findPaymentExceptions()
    }
    
    // 'initialValue' attribute on Variable at AgencyBillExceptions.pcf: line 28, column 54
    function initialValue_1 () : gw.api.web.invoice.ExceptionItemView[] {
      return findPromiseExceptions()
    }
    
    // 'initialValue' attribute on Variable at AgencyBillExceptions.pcf: line 33, column 36
    function initialValue_2 () : AgencyCycleProcess[] {
      return producer.findAgencyCyclesWithLatePaymentException()
    }
    
    // 'initialValue' attribute on Variable at AgencyBillExceptions.pcf: line 38, column 36
    function initialValue_3 () : AgencyCycleProcess[] {
      return producer.findAgencyCyclesWithLatePromiseException()
    }
    
    // 'label' attribute on CheckedValuesMenuItem (id=Both) at AgencyBillExceptions.pcf: line 65, column 55
    function label_5 () : java.lang.Object {
      return AgencyWriteoffType.TC_BOTH
    }
    
    // 'label' attribute on CheckedValuesMenuItem (id=Gross) at AgencyBillExceptions.pcf: line 69, column 56
    function label_7 () : java.lang.Object {
      return AgencyWriteoffType.TC_GROSS
    }
    
    // 'label' attribute on CheckedValuesMenuItem (id=Commission) at AgencyBillExceptions.pcf: line 73, column 61
    function label_9 () : java.lang.Object {
      return AgencyWriteoffType.TC_COMMISSION
    }
    
    // Page (id=AgencyBillExceptions) at AgencyBillExceptions.pcf: line 8, column 82
    static function parent_40 (producer :  Producer, tabToSelect :  int) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    // 'selectOnEnter' attribute on Card (id=PaymentMismatchExceptions) at AgencyBillExceptions.pcf: line 46, column 116
    function selectOnEnter_17 () : java.lang.Boolean {
      return tabToSelect == 0
    }
    
    // 'selectOnEnter' attribute on Card (id=PromiseMismatchExceptions) at AgencyBillExceptions.pcf: line 106, column 116
    function selectOnEnter_25 () : java.lang.Boolean {
      return tabToSelect == 1
    }
    
    // 'selectOnEnter' attribute on Card (id=LatePayments) at AgencyBillExceptions.pcf: line 141, column 129
    function selectOnEnter_31 () : java.lang.Boolean {
      return tabToSelect == 2
    }
    
    // 'selectOnEnter' attribute on Card (id=LatePromises) at AgencyBillExceptions.pcf: line 164, column 129
    function selectOnEnter_37 () : java.lang.Boolean {
      return tabToSelect == 3
    }
    
    // 'title' attribute on Card (id=PaymentMismatchExceptions) at AgencyBillExceptions.pcf: line 46, column 116
    function title_18 () : java.lang.String {
      return DisplayKey.get("Web.AgencyBillExceptions.PaymentExceptions", paymentExceptions.length)
    }
    
    // 'title' attribute on Card (id=PromiseMismatchExceptions) at AgencyBillExceptions.pcf: line 106, column 116
    function title_26 () : java.lang.String {
      return DisplayKey.get("Web.AgencyBillExceptions.PromiseExceptions", promiseExceptions.length)
    }
    
    // 'title' attribute on Card (id=LatePayments) at AgencyBillExceptions.pcf: line 141, column 129
    function title_32 () : java.lang.String {
      return DisplayKey.get("Web.AgencyBillExceptions.LatePayments", agencyCyclesWithLatePaymentException.Count)
    }
    
    // 'title' attribute on Card (id=LatePromises) at AgencyBillExceptions.pcf: line 164, column 129
    function title_38 () : java.lang.String {
      return DisplayKey.get("Web.AgencyBillExceptions.LatePromises", agencyCyclesWithLatePromiseException.Count)
    }
    
    // 'title' attribute on Page (id=AgencyBillExceptions) at AgencyBillExceptions.pcf: line 8, column 82
    static function title_41 (producer :  Producer, tabToSelect :  int) : java.lang.Object {
      return DisplayKey.get("Web.AgencyBillExceptions.Title", producer)
    }
    
    override property get CurrentLocation () : pcf.AgencyBillExceptions {
      return super.CurrentLocation as pcf.AgencyBillExceptions
    }
    
    property get agencyCyclesWithLatePaymentException () : AgencyCycleProcess[] {
      return getVariableValue("agencyCyclesWithLatePaymentException", 0) as AgencyCycleProcess[]
    }
    
    property set agencyCyclesWithLatePaymentException ($arg :  AgencyCycleProcess[]) {
      setVariableValue("agencyCyclesWithLatePaymentException", 0, $arg)
    }
    
    property get agencyCyclesWithLatePromiseException () : AgencyCycleProcess[] {
      return getVariableValue("agencyCyclesWithLatePromiseException", 0) as AgencyCycleProcess[]
    }
    
    property set agencyCyclesWithLatePromiseException ($arg :  AgencyCycleProcess[]) {
      setVariableValue("agencyCyclesWithLatePromiseException", 0, $arg)
    }
    
    property get paymentExceptions () : gw.api.web.invoice.ExceptionItemView[] {
      return getVariableValue("paymentExceptions", 0) as gw.api.web.invoice.ExceptionItemView[]
    }
    
    property set paymentExceptions ($arg :  gw.api.web.invoice.ExceptionItemView[]) {
      setVariableValue("paymentExceptions", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    property get promiseExceptions () : gw.api.web.invoice.ExceptionItemView[] {
      return getVariableValue("promiseExceptions", 0) as gw.api.web.invoice.ExceptionItemView[]
    }
    
    property set promiseExceptions ($arg :  gw.api.web.invoice.ExceptionItemView[]) {
      setVariableValue("promiseExceptions", 0, $arg)
    }
    
    property get tabToSelect () : int {
      return getVariableValue("tabToSelect", 0) as java.lang.Integer
    }
    
    property set tabToSelect ($arg :  int) {
      setVariableValue("tabToSelect", 0, $arg)
    }
    
    function dismissExceptions(exceptionProcesses : AgencyCycleProcess[], isLatePayments : boolean ) {
      gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
        for (process in exceptionProcesses) {
          process = bundle.add(process)
          if (isLatePayments) {
            process.PastDueExceptionDismissed = true
          } else {
            process.PromiseExceptionDismissed = true
          }
        }
      })
    }
    
    function findPaymentExceptions() : gw.api.web.invoice.ExceptionItemView[]{
      var exceptionItems = producer.findPaymentsExceptionItems()
      return gw.api.web.producer.agencybill.ExceptionItemUtil.createExceptionItemViews( exceptionItems )
    }
    
    function findPromiseExceptions() : gw.api.web.invoice.ExceptionItemView[]{
      var exceptionItems = producer.findPromisesExceptionItems()
      return gw.api.web.producer.agencybill.ExceptionItemUtil.createExceptionItemViews( exceptionItems )
    }
    
    function writeOffExceptions(exceptionItemViews : gw.api.web.invoice.ExceptionItemView[], writeOffType : AgencyWriteoffType ){
       AgencyBillExceptionsConfirmationPopup.push(exceptionItemViews, writeOffType )
     }
    
    
  }
  
  
}