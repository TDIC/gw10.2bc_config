package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/TabBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TabBarExpressions {
  @javax.annotation.Generated("config/web/pcf/TabBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdministrationTabMenuItemExpressionsImpl extends TabBarExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Admin.pcf: line 15, column 38
    function action_103 () : void {
      pcf.UsersAndSecurity.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Admin.pcf: line 17, column 38
    function action_133 () : void {
      pcf.BusinessSettings.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Admin.pcf: line 19, column 32
    function action_141 () : void {
      pcf.Monitoring.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Admin.pcf: line 21, column 31
    function action_159 () : void {
      pcf.Utilities.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Admin.pcf: line 23, column 32
    function action_161 () : void {
      pcf.AdminAudit.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Admin.pcf: line 15, column 38
    function action_dest_104 () : pcf.api.Destination {
      return pcf.UsersAndSecurity.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Admin.pcf: line 17, column 38
    function action_dest_134 () : pcf.api.Destination {
      return pcf.BusinessSettings.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Admin.pcf: line 19, column 32
    function action_dest_142 () : pcf.api.Destination {
      return pcf.Monitoring.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Admin.pcf: line 21, column 31
    function action_dest_160 () : pcf.api.Destination {
      return pcf.Utilities.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Admin.pcf: line 23, column 32
    function action_dest_162 () : pcf.api.Destination {
      return pcf.AdminAudit.createDestination()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/TabBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopTabMenuItemExpressionsImpl extends TabBarExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 16, column 39
    function action_0 () : void {
      pcf.DesktopActivities.go()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 26, column 42
    function action_10 () : void {
      pcf.DesktopDelinquencies.go()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 28, column 42
    function action_12 () : void {
      pcf.DesktopDisbursements.go()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 30, column 45
    function action_14 () : void {
      pcf.DesktopSuspensePayments.go()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 32, column 40
    function action_16 () : void {
      pcf.DesktopAgencyItems.go()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 34, column 40
    function action_18 () : void {
      pcf.DesktopHeldCharges.go()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 18, column 46
    function action_2 () : void {
      pcf.DesktopCreatedActivities.go()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 36, column 48
    function action_20 () : void {
      pcf.DesktopBatchPaymentsSearch.go()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 20, column 35
    function action_4 () : void {
      pcf.DesktopQueues.go()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 22, column 38
    function action_6 () : void {
      pcf.DesktopApprovals.go()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 24, column 43
    function action_8 () : void {
      pcf.DesktopTroubleTickets.go()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 16, column 39
    function action_dest_1 () : pcf.api.Destination {
      return pcf.DesktopActivities.createDestination()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 26, column 42
    function action_dest_11 () : pcf.api.Destination {
      return pcf.DesktopDelinquencies.createDestination()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 28, column 42
    function action_dest_13 () : pcf.api.Destination {
      return pcf.DesktopDisbursements.createDestination()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 30, column 45
    function action_dest_15 () : pcf.api.Destination {
      return pcf.DesktopSuspensePayments.createDestination()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 32, column 40
    function action_dest_17 () : pcf.api.Destination {
      return pcf.DesktopAgencyItems.createDestination()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 34, column 40
    function action_dest_19 () : pcf.api.Destination {
      return pcf.DesktopHeldCharges.createDestination()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 36, column 48
    function action_dest_21 () : pcf.api.Destination {
      return pcf.DesktopBatchPaymentsSearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 18, column 46
    function action_dest_3 () : pcf.api.Destination {
      return pcf.DesktopCreatedActivities.createDestination()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 20, column 35
    function action_dest_5 () : pcf.api.Destination {
      return pcf.DesktopQueues.createDestination()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 22, column 38
    function action_dest_7 () : pcf.api.Destination {
      return pcf.DesktopApprovals.createDestination()
    }
    
    // 'location' attribute on Tab (id=DesktopTab) at DesktopGroup.pcf: line 24, column 43
    function action_dest_9 () : pcf.api.Destination {
      return pcf.DesktopTroubleTickets.createDestination()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/TabBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends TabBarExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=PolicyPeriodSessionItemId) at TabBar.pcf: line 61, column 132
    function action_41 () : void {
      policyPeriodSessionItem.goToDestination()
    }
    
    // 'label' attribute on MenuItem (id=PolicyPeriodSessionItemId) at TabBar.pcf: line 61, column 132
    function label_42 () : java.lang.Object {
      return DisplayKey.get("Java.PolicyPeriodSessionItem.Label", policyPeriodSessionItem.Entity.PolicyNumberLong)
    }
    
    property get policyPeriodSessionItem () : gw.api.web.controller.SessionItemInfo<entity.PolicyPeriod> {
      return getIteratedValue(1) as gw.api.web.controller.SessionItemInfo<entity.PolicyPeriod>
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/TabBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends TabBarExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=ProducerSessionItemId) at TabBar.pcf: line 86, column 119
    function action_52 () : void {
      ProducerPolicies.go(producerSessionItem.getEntity())
    }
    
    // 'action' attribute on MenuItem (id=ProducerSessionItemId) at TabBar.pcf: line 86, column 119
    function action_dest_53 () : pcf.api.Destination {
      return pcf.ProducerPolicies.createDestination(producerSessionItem.getEntity())
    }
    
    // 'label' attribute on MenuItem (id=ProducerSessionItemId) at TabBar.pcf: line 86, column 119
    function label_54 () : java.lang.Object {
      return DisplayKey.get("Java.ProducerSessionItem.Label", producerSessionItem.Entity.DisplayName)
    }
    
    property get producerSessionItem () : gw.api.web.controller.SessionItemInfo<entity.Producer> {
      return getIteratedValue(1) as gw.api.web.controller.SessionItemInfo<entity.Producer>
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/TabBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends TabBarExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=langs) at TabBar.pcf: line 133, column 29
    function action_171 () : void {
      gw.api.admin.BaseAdminUtil.setCurrentUsersLanguage(lang)
    }
    
    // 'available' attribute on MenuItem (id=langs) at TabBar.pcf: line 133, column 29
    function available_170 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.getCurrentUserLanguage() != lang
    }
    
    // 'checked' attribute on MenuItem (id=langs) at TabBar.pcf: line 133, column 29
    function checked_173 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.getCurrentLanguageType() == lang
    }
    
    // 'label' attribute on MenuItem (id=langs) at TabBar.pcf: line 133, column 29
    function label_172 () : java.lang.Object {
      return gw.api.util.LocaleUtil.getLanguageLabel(lang)
    }
    
    property get lang () : typekey.LanguageType {
      return getIteratedValue(1) as typekey.LanguageType
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/TabBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry5ExpressionsImpl extends TabBarExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=locales) at TabBar.pcf: line 151, column 29
    function action_177 () : void {
      gw.api.admin.BaseAdminUtil.setCurrentUsersLocale(locale)
    }
    
    // 'available' attribute on MenuItem (id=locales) at TabBar.pcf: line 151, column 29
    function available_176 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.getCurrentUserLocale() != locale
    }
    
    // 'checked' attribute on MenuItem (id=locales) at TabBar.pcf: line 151, column 29
    function checked_179 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.getCurrentLocaleType() == locale
    }
    
    // 'label' attribute on MenuItem (id=locales) at TabBar.pcf: line 151, column 29
    function label_178 () : java.lang.Object {
      return gw.api.util.LocaleUtil.getLocaleLabel(locale)
    }
    
    property get locale () : typekey.LocaleType {
      return getIteratedValue(1) as typekey.LocaleType
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/TabBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TabBarExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=AccountSessionItemId) at TabBar.pcf: line 36, column 167
    function action_30 () : void {
      accountSessionItem.goToDestination()
    }
    
    // 'label' attribute on MenuItem (id=AccountSessionItemId) at TabBar.pcf: line 36, column 167
    function label_31 () : java.lang.Object {
      return DisplayKey.get("Java.AccountSessionItem.Label", accountSessionItem.Entity.AccountNumber, accountSessionItem.Entity.AccountNameLocalized)
    }
    
    property get accountSessionItem () : gw.api.web.controller.SessionItemInfo<entity.Account> {
      return getIteratedValue(1) as gw.api.web.controller.SessionItemInfo<entity.Account>
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/TabBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationGroupMenuItem2ExpressionsImpl extends AdministrationTabMenuItemExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 10, column 38
    function action_105 () : void {
      pcf.ActivityPatterns.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 12, column 37
    function action_107 () : void {
      pcf.AgencyBillPlans.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 14, column 34
    function action_109 () : void {
      pcf.BillingPlans.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 16, column 50
    function action_111 () : void {
      pcf.ChargeBreakdownCategoryTypes.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 18, column 36
    function action_113 () : void {
      pcf.ChargePatterns.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 20, column 40
    function action_115 () : void {
      pcf.CollectionAgencies.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 22, column 37
    function action_117 () : void {
      pcf.CommissionPlans.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 24, column 38
    function action_119 () : void {
      pcf.DelinquencyPlans.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 26, column 40
    function action_121 () : void {
      pcf.TDIC_GLIntegration.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 28, column 30
    function action_123 () : void {
      pcf.Holidays.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 30, column 44
    function action_125 () : void {
      pcf.PaymentAllocationPlans.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 32, column 34
    function action_127 () : void {
      pcf.PaymentPlans.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 34, column 40
    function action_129 () : void {
      pcf.ReturnPremiumPlans.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 36, column 39
    function action_131 () : void {
      pcf.ClearOpenHandlers.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 10, column 38
    function action_dest_106 () : pcf.api.Destination {
      return pcf.ActivityPatterns.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 12, column 37
    function action_dest_108 () : pcf.api.Destination {
      return pcf.AgencyBillPlans.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 14, column 34
    function action_dest_110 () : pcf.api.Destination {
      return pcf.BillingPlans.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 16, column 50
    function action_dest_112 () : pcf.api.Destination {
      return pcf.ChargeBreakdownCategoryTypes.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 18, column 36
    function action_dest_114 () : pcf.api.Destination {
      return pcf.ChargePatterns.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 20, column 40
    function action_dest_116 () : pcf.api.Destination {
      return pcf.CollectionAgencies.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 22, column 37
    function action_dest_118 () : pcf.api.Destination {
      return pcf.CommissionPlans.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 24, column 38
    function action_dest_120 () : pcf.api.Destination {
      return pcf.DelinquencyPlans.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 26, column 40
    function action_dest_122 () : pcf.api.Destination {
      return pcf.TDIC_GLIntegration.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 28, column 30
    function action_dest_124 () : pcf.api.Destination {
      return pcf.Holidays.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 30, column 44
    function action_dest_126 () : pcf.api.Destination {
      return pcf.PaymentAllocationPlans.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 32, column 34
    function action_dest_128 () : pcf.api.Destination {
      return pcf.PaymentPlans.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 34, column 40
    function action_dest_130 () : pcf.api.Destination {
      return pcf.ReturnPremiumPlans.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at BusinessSettings.pcf: line 36, column 39
    function action_dest_132 () : pcf.api.Destination {
      return pcf.ClearOpenHandlers.createDestination()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/TabBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationGroupMenuItem3ExpressionsImpl extends LocationGroupMenuItem2ExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 3)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/TabBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationGroupMenuItem4ExpressionsImpl extends AdministrationTabMenuItemExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Monitoring.pcf: line 11, column 53
    function action_135 () : void {
      pcf.MessagingDestinationControlList.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Monitoring.pcf: line 14, column 36
    function action_137 () : void {
      pcf.WorkflowSearch.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Monitoring.pcf: line 16, column 35
    function action_139 () : void {
      pcf.WorkflowStats.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Monitoring.pcf: line 11, column 53
    function action_dest_136 () : pcf.api.Destination {
      return pcf.MessagingDestinationControlList.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Monitoring.pcf: line 14, column 36
    function action_dest_138 () : pcf.api.Destination {
      return pcf.WorkflowSearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Monitoring.pcf: line 16, column 35
    function action_dest_140 () : pcf.api.Destination {
      return pcf.WorkflowStats.createDestination()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/TabBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationGroupMenuItem5ExpressionsImpl extends AdministrationTabMenuItemExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Utilities.pcf: line 12, column 21
    function action_143 () : void {
      pcf.ImportWizard.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Utilities.pcf: line 16, column 21
    function action_145 () : void {
      pcf.ExportData.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Utilities.pcf: line 18, column 42
    function action_147 () : void {
      pcf.ScriptParametersPage.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Utilities.pcf: line 20, column 36
    function action_149 () : void {
      pcf.DataChangePage.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Utilities.pcf: line 22, column 32
    function action_151 () : void {
      pcf.Properties.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Utilities.pcf: line 24, column 39
    function action_153 () : void {
      pcf.InboundFileSearch.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Utilities.pcf: line 26, column 40
    function action_155 () : void {
      pcf.OutboundFileSearch.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Utilities.pcf: line 29, column 52
    function action_157 () : void {
      pcf.Import_AddressBookUID_Mappings.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Utilities.pcf: line 12, column 21
    function action_dest_144 () : pcf.api.Destination {
      return pcf.ImportWizard.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Utilities.pcf: line 16, column 21
    function action_dest_146 () : pcf.api.Destination {
      return pcf.ExportData.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Utilities.pcf: line 18, column 42
    function action_dest_148 () : pcf.api.Destination {
      return pcf.ScriptParametersPage.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Utilities.pcf: line 20, column 36
    function action_dest_150 () : pcf.api.Destination {
      return pcf.DataChangePage.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Utilities.pcf: line 22, column 32
    function action_dest_152 () : pcf.api.Destination {
      return pcf.Properties.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Utilities.pcf: line 24, column 39
    function action_dest_154 () : pcf.api.Destination {
      return pcf.InboundFileSearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Utilities.pcf: line 26, column 40
    function action_dest_156 () : pcf.api.Destination {
      return pcf.OutboundFileSearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at Utilities.pcf: line 29, column 52
    function action_dest_158 () : pcf.api.Destination {
      return pcf.Import_AddressBookUID_Mappings.createDestination()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/TabBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class LocationGroupMenuItemExpressionsImpl extends AdministrationTabMenuItemExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at UsersAndSecurity.pcf: line 19, column 44
    function action_101 () : void {
      pcf.AuthorityLimitProfiles.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at UsersAndSecurity.pcf: line 11, column 32
    function action_93 () : void {
      pcf.UserSearch.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at UsersAndSecurity.pcf: line 13, column 28
    function action_95 () : void {
      pcf.Groups.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at UsersAndSecurity.pcf: line 15, column 27
    function action_97 () : void {
      pcf.Roles.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at UsersAndSecurity.pcf: line 17, column 35
    function action_99 () : void {
      pcf.SecurityZones.go()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at UsersAndSecurity.pcf: line 17, column 35
    function action_dest_100 () : pcf.api.Destination {
      return pcf.SecurityZones.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at UsersAndSecurity.pcf: line 19, column 44
    function action_dest_102 () : pcf.api.Destination {
      return pcf.AuthorityLimitProfiles.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at UsersAndSecurity.pcf: line 11, column 32
    function action_dest_94 () : pcf.api.Destination {
      return pcf.UserSearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at UsersAndSecurity.pcf: line 13, column 28
    function action_dest_96 () : pcf.api.Destination {
      return pcf.Groups.createDestination()
    }
    
    // 'location' attribute on Tab (id=AdministrationTab) at UsersAndSecurity.pcf: line 15, column 27
    function action_dest_98 () : pcf.api.Destination {
      return pcf.Roles.createDestination()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/TabBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ReportTabMenuItemExpressionsImpl extends TabBarExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/TabBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchTabMenuItemExpressionsImpl extends TabBarExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 15, column 35
    function action_62 () : void {
      pcf.AccountSearch.go()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 18, column 34
    function action_64 () : void {
      pcf.PolicySearch.go()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 21, column 35
    function action_66 () : void {
      pcf.ContactSearch.go()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 24, column 35
    function action_68 () : void {
      pcf.InvoiceSearch.go()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 27, column 35
    function action_70 () : void {
      pcf.PaymentSearch.go()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 30, column 36
    function action_72 () : void {
      pcf.ProducerSearch.go()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 33, column 39
    function action_74 () : void {
      pcf.TransactionSearch.go()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 36, column 36
    function action_76 () : void {
      pcf.ActivitySearch.go()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 39, column 41
    function action_78 () : void {
      pcf.TroubleTicketSearch.go()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 42, column 46
    function action_80 () : void {
      pcf.DelinquencyProcessSearch.go()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 45, column 40
    function action_82 () : void {
      pcf.DisbursementSearch.go()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 48, column 51
    function action_84 () : void {
      pcf.OutgoingProducerPaymentSearch.go()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 51, column 42
    function action_86 () : void {
      pcf.PaymentRequestSearch.go()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 54, column 50
    function action_88 () : void {
      pcf.DirectBillSuspenseItemSearch.go()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 15, column 35
    function action_dest_63 () : pcf.api.Destination {
      return pcf.AccountSearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 18, column 34
    function action_dest_65 () : pcf.api.Destination {
      return pcf.PolicySearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 21, column 35
    function action_dest_67 () : pcf.api.Destination {
      return pcf.ContactSearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 24, column 35
    function action_dest_69 () : pcf.api.Destination {
      return pcf.InvoiceSearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 27, column 35
    function action_dest_71 () : pcf.api.Destination {
      return pcf.PaymentSearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 30, column 36
    function action_dest_73 () : pcf.api.Destination {
      return pcf.ProducerSearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 33, column 39
    function action_dest_75 () : pcf.api.Destination {
      return pcf.TransactionSearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 36, column 36
    function action_dest_77 () : pcf.api.Destination {
      return pcf.ActivitySearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 39, column 41
    function action_dest_79 () : pcf.api.Destination {
      return pcf.TroubleTicketSearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 42, column 46
    function action_dest_81 () : pcf.api.Destination {
      return pcf.DelinquencyProcessSearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 45, column 40
    function action_dest_83 () : pcf.api.Destination {
      return pcf.DisbursementSearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 48, column 51
    function action_dest_85 () : pcf.api.Destination {
      return pcf.OutgoingProducerPaymentSearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 51, column 42
    function action_dest_87 () : pcf.api.Destination {
      return pcf.PaymentRequestSearch.createDestination()
    }
    
    // 'location' attribute on Tab (id=SearchTab) at SearchGroup.pcf: line 54, column 50
    function action_dest_89 () : pcf.api.Destination {
      return pcf.DirectBillSuspenseItemSearch.createDestination()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/TabBar.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TabBarExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Tab (id=AdministrationTab) at TabBar.pcf: line 106, column 43
    function action_164 () : void {
      Admin.go()
    }
    
    // 'action' attribute on Tab (id=TDIC_BuildInfolnkTab) at TabBar.pcf: line 111, column 167
    function action_167 () : void {
      TDIC_BuildInfo.push()
    }
    
    // 'action' attribute on TabBarLink (id=HelpTabBarLink) at TabBar.pcf: line 159, column 24
    function action_183 () : void {
      Help.push()
    }
    
    // 'action' attribute on TabBarLogout (id=SafeLogoutTabBarLink) at TabBar.pcf: line 183, column 114
    function action_189 () : void {
      tdic.bc.integ.services.creditcard.TDIC_AuthorizeNetCreditCardHandler.cleanupSession(); TDIC_Logout.push("http://" + gw.api.system.server.ServerUtil.getServerId() + ":8580/bc")
    }
    
    // 'action' attribute on HiddenLink (id=ProfilerHiddenLink) at TabBar.pcf: line 187, column 26
    function action_191 () : void {
      ProfilerPopup.push()
    }
    
    // 'action' attribute on HiddenLink (id=InternalToolsHiddenLink) at TabBar.pcf: line 191, column 26
    function action_193 () : void {
      InternalTools.go()
    }
    
    // 'action' attribute on Tab (id=DesktopTab) at TabBar.pcf: line 13, column 42
    function action_23 () : void {
      DesktopGroup.go()
    }
    
    // 'action' attribute on Tab (id=AccountsTab) at TabBar.pcf: line 19, column 41
    function action_34 () : void {
      AccountForward.go()
    }
    
    // 'action' attribute on Tab (id=PoliciesTab) at TabBar.pcf: line 44, column 41
    function action_45 () : void {
      PolicyForward.go()
    }
    
    // 'action' attribute on Tab (id=ProducersTab) at TabBar.pcf: line 69, column 41
    function action_57 () : void {
      ProducerForward.go()
    }
    
    // 'action' attribute on Tab (id=ReportTab) at TabBar.pcf: line 94, column 127
    function action_60 () : void {
      Reports.go()
    }
    
    // 'action' attribute on Tab (id=SearchTab) at TabBar.pcf: line 100, column 41
    function action_91 () : void {
      SearchGroup.go()
    }
    
    // 'action' attribute on Tab (id=AdministrationTab) at TabBar.pcf: line 106, column 43
    function action_dest_165 () : pcf.api.Destination {
      return pcf.Admin.createDestination()
    }
    
    // 'action' attribute on Tab (id=TDIC_BuildInfolnkTab) at TabBar.pcf: line 111, column 167
    function action_dest_168 () : pcf.api.Destination {
      return pcf.TDIC_BuildInfo.createDestination()
    }
    
    // 'action' attribute on TabBarLink (id=HelpTabBarLink) at TabBar.pcf: line 159, column 24
    function action_dest_184 () : pcf.api.Destination {
      return pcf.Help.createDestination()
    }
    
    // 'action' attribute on HiddenLink (id=ProfilerHiddenLink) at TabBar.pcf: line 187, column 26
    function action_dest_192 () : pcf.api.Destination {
      return pcf.ProfilerPopup.createDestination()
    }
    
    // 'action' attribute on HiddenLink (id=InternalToolsHiddenLink) at TabBar.pcf: line 191, column 26
    function action_dest_194 () : pcf.api.Destination {
      return pcf.InternalTools.createDestination()
    }
    
    // 'action' attribute on Tab (id=DesktopTab) at TabBar.pcf: line 13, column 42
    function action_dest_24 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'action' attribute on Tab (id=AccountsTab) at TabBar.pcf: line 19, column 41
    function action_dest_35 () : pcf.api.Destination {
      return pcf.AccountForward.createDestination()
    }
    
    // 'action' attribute on Tab (id=PoliciesTab) at TabBar.pcf: line 44, column 41
    function action_dest_46 () : pcf.api.Destination {
      return pcf.PolicyForward.createDestination()
    }
    
    // 'action' attribute on Tab (id=ProducersTab) at TabBar.pcf: line 69, column 41
    function action_dest_58 () : pcf.api.Destination {
      return pcf.ProducerForward.createDestination()
    }
    
    // 'action' attribute on Tab (id=ReportTab) at TabBar.pcf: line 94, column 127
    function action_dest_61 () : pcf.api.Destination {
      return pcf.Reports.createDestination()
    }
    
    // 'action' attribute on Tab (id=SearchTab) at TabBar.pcf: line 100, column 41
    function action_dest_92 () : pcf.api.Destination {
      return pcf.SearchGroup.createDestination()
    }
    
    // 'afterFailure' attribute on SearchItem (id=AccountNumberSearchItem_widget) at TabBar.pcf: line 27, column 114
    function afterFailure_25 (searchCriteria :  java.lang.Object, searchText :  java.lang.String) : void {
      (searchCriteria as gw.search.AccountSearchCriteria).AccountNumber = searchText
    }
    
    // 'afterFailure' attribute on SearchItem (id=PolicyNumberSearchItem_widget) at TabBar.pcf: line 52, column 112
    function afterFailure_36 (searchCriteria :  java.lang.Object, searchText :  java.lang.String) : void {
      (searchCriteria as gw.search.PolicySearchCriteria).PolicyNumber = searchText
    }
    
    // 'afterFailure' attribute on SearchItem (id=ProducerNameSearchItem_widget) at TabBar.pcf: line 77, column 114
    function afterFailure_47 (searchCriteria :  java.lang.Object, searchText :  java.lang.String) : void {
      (searchCriteria as gw.search.ProducerSearchCriteria).ProducerName = searchText
    }
    
    // 'label' attribute on Tab (id=TDIC_BuildInfolnkTab) at TabBar.pcf: line 111, column 167
    function label_169 () : java.lang.Object {
      return tdic.util.build.BuildInfo.getBuildInfo()
    }
    
    // 'label' attribute on TabBarLogout (id=LogoutTabBarLink) at TabBar.pcf: line 178, column 118
    function label_187 () : java.lang.Object {
      return DisplayKey.get("Web.TabBar.Logout", entity.User.util.CurrentUser)
    }
    
    // 'onFailure' attribute on SearchItem (id=AccountNumberSearchItem_widget) at TabBar.pcf: line 27, column 114
    function onFailure_26 (searchText :  java.lang.String) : pcf.api.Location {
      return Accounts.go()
    }
    
    // 'onFailure' attribute on SearchItem (id=PolicyNumberSearchItem_widget) at TabBar.pcf: line 52, column 112
    function onFailure_37 (searchText :  java.lang.String) : pcf.api.Location {
      return Policies.go()
    }
    
    // 'onFailure' attribute on SearchItem (id=ProducerNameSearchItem_widget) at TabBar.pcf: line 77, column 114
    function onFailure_48 (searchText :  java.lang.String) : pcf.api.Location {
      return Producers.go()
    }
    
    // 'onResult' attribute on SearchItem (id=AccountNumberSearchItem_widget) at TabBar.pcf: line 27, column 114
    function onResult_27 (result :  java.lang.Object) : void {
      AccountDetailForward.go(gw.api.web.search.SearchPopupUtil.getAccount(result as AccountSearchView))
    }
    
    // 'onResult' attribute on SearchItem (id=PolicyNumberSearchItem_widget) at TabBar.pcf: line 52, column 112
    function onResult_38 (result :  java.lang.Object) : void {
      PolicyDetailForward.go(gw.api.web.search.SearchPopupUtil.getPolicyPeriod(result as PolicySearchView))
    }
    
    // 'onResult' attribute on SearchItem (id=ProducerNameSearchItem_widget) at TabBar.pcf: line 77, column 114
    function onResult_49 (result :  java.lang.Object) : void {
      ProducerDetailForward.go(gw.api.web.search.SearchPopupUtil.getProducer(result as ProducerSearchView))
    }
    
    // 'search' attribute on SearchItem (id=AccountNumberSearchItem_widget) at TabBar.pcf: line 27, column 114
    function search_28 (searchText :  java.lang.String) : java.lang.Object {
      return gw.search.AccountSearchCriteria.searchAccountsByNumber(searchText)
    }
    
    // 'search' attribute on SearchItem (id=PolicyNumberSearchItem_widget) at TabBar.pcf: line 52, column 112
    function search_39 (searchText :  java.lang.String) : java.lang.Object {
      return gw.search.PolicySearchCriteria.searchPoliciesByNumber(searchText)
    }
    
    // 'search' attribute on SearchItem (id=ProducerNameSearchItem_widget) at TabBar.pcf: line 77, column 114
    function search_50 (searchText :  java.lang.String) : java.lang.Object {
      return gw.search.ProducerSearchCriteria.searchProducersByName(searchText)
    }
    
    // 'systemAlertBar' attribute on TabBar (id=TabBar) at TabBar.pcf: line 7, column 39
    function systemAlertBar_onEnter_195 (def :  pcf.SystemAlertBar) : void {
      def.onEnter()
    }
    
    // 'systemAlertBar' attribute on TabBar (id=TabBar) at TabBar.pcf: line 7, column 39
    function systemAlertBar_refreshVariables_196 (def :  pcf.SystemAlertBar) : void {
      def.refreshVariables()
    }
    
    // 'validationError' attribute on SearchItem (id=AccountNumberSearchItem_widget) at TabBar.pcf: line 27, column 114
    function validationError_29 (searchText :  java.lang.String) : java.lang.String {
      return DisplayKey.get("Java.TabBar.Account.AccountNumberSearch.Error", searchText)
    }
    
    // 'validationError' attribute on SearchItem (id=PolicyNumberSearchItem_widget) at TabBar.pcf: line 52, column 112
    function validationError_40 (searchText :  java.lang.String) : java.lang.String {
      return DisplayKey.get("Java.TabBar.Policy.PolicyNumberSearch.Error", searchText)
    }
    
    // 'validationError' attribute on SearchItem (id=ProducerNameSearchItem_widget) at TabBar.pcf: line 77, column 114
    function validationError_51 (searchText :  java.lang.String) : java.lang.String {
      return DisplayKey.get("Java.TabBar.Producer.ProducerNameSearch.Error", searchText)
    }
    
    // 'value' attribute on MenuItemIterator at TabBar.pcf: line 125, column 46
    function value_174 () : typekey.LanguageType[] {
      return gw.api.util.LocaleUtil.getAllLanguages()?.toTypedArray()
    }
    
    // 'value' attribute on MenuItemIterator at TabBar.pcf: line 143, column 44
    function value_180 () : typekey.LocaleType[] {
      return gw.api.util.LocaleUtil.getAllLocales()?.toTypedArray()
    }
    
    // 'value' attribute on MenuItemIterator (id=RecentlyViewedAccountsId) at TabBar.pcf: line 32, column 103
    function value_32 () : java.util.List<gw.api.web.controller.SessionItemInfo<entity.Account>> {
      return gw.api.web.account.AccountUtil.RecentAccounts
    }
    
    // 'value' attribute on MenuItemIterator (id=RecentlyViewedPoliciesId) at TabBar.pcf: line 57, column 108
    function value_43 () : java.util.List<gw.api.web.controller.SessionItemInfo<entity.PolicyPeriod>> {
      return gw.api.web.policy.PolicyPeriodUtil.RecentPolicyPeriods
    }
    
    // 'value' attribute on MenuItemIterator (id=RecentlyViewedProducersId) at TabBar.pcf: line 82, column 104
    function value_55 () : java.util.List<gw.api.web.controller.SessionItemInfo<entity.Producer>> {
      return gw.api.web.producer.ProducerUtil.RecentProducers
    }
    
    // 'visible' attribute on Tab (id=AdministrationTab) at TabBar.pcf: line 106, column 43
    function visible_163 () : java.lang.Boolean {
      return perm.System.admintabview
    }
    
    // 'visible' attribute on Tab (id=TDIC_BuildInfolnkTab) at TabBar.pcf: line 111, column 167
    function visible_166 () : java.lang.Boolean {
      return gw.api.system.server.ServerModeUtil.isDev() or gw.api.system.server.ServerModeUtil.isTest() or (User.util.CurrentUser == User.util.UnrestrictedUser)
    }
    
    // 'visible' attribute on MenuItem (id=languageSwitcher) at TabBar.pcf: line 121, column 62
    function visible_175 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.canSwitchLanguage()
    }
    
    // 'visible' attribute on MenuItem (id=localeSwitcher) at TabBar.pcf: line 139, column 60
    function visible_181 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.canSwitchLocale()
    }
    
    // 'visible' attribute on TabBarLink (id=LanguageTabBarLink) at TabBar.pcf: line 117, column 104
    function visible_182 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.canSwitchLanguage() || gw.api.util.LocaleUtil.canSwitchLocale()
    }
    
    // 'visible' attribute on TabBarLink (id=ReloadPCFTabBarLink) at TabBar.pcf: line 172, column 57
    function visible_185 () : java.lang.Boolean {
      return gw.api.tools.InternalTools.isEnabled()
    }
    
    // 'visible' attribute on TabBarLogout (id=LogoutTabBarLink) at TabBar.pcf: line 178, column 118
    function visible_186 () : java.lang.Boolean {
      return not tdic.bc.integ.services.creditcard.TDIC_AuthorizeNetCreditCardHandler.isSessionCleanupRequired()
    }
    
    // 'visible' attribute on TabBarLogout (id=SafeLogoutTabBarLink) at TabBar.pcf: line 183, column 114
    function visible_188 () : java.lang.Boolean {
      return tdic.bc.integ.services.creditcard.TDIC_AuthorizeNetCreditCardHandler.isSessionCleanupRequired()
    }
    
    // 'visible' attribute on Tab (id=DesktopTab) at TabBar.pcf: line 13, column 42
    function visible_22 () : java.lang.Boolean {
      return perm.System.viewdesktop
    }
    
    // 'visible' attribute on Tab (id=AccountsTab) at TabBar.pcf: line 19, column 41
    function visible_33 () : java.lang.Boolean {
      return perm.System.accttabview
    }
    
    // 'visible' attribute on Tab (id=PoliciesTab) at TabBar.pcf: line 44, column 41
    function visible_44 () : java.lang.Boolean {
      return perm.System.plcytabview
    }
    
    // 'visible' attribute on Tab (id=ProducersTab) at TabBar.pcf: line 69, column 41
    function visible_56 () : java.lang.Boolean {
      return perm.System.prodtabview
    }
    
    // 'visible' attribute on Tab (id=ReportTab) at TabBar.pcf: line 94, column 127
    function visible_59 () : java.lang.Boolean {
      return perm.System.reporting_view and gw.api.util.BCPlugins.isStartablePluginStarted(gw.plugin.cognos.CognosPlugin)
    }
    
    // 'visible' attribute on Tab (id=SearchTab) at TabBar.pcf: line 100, column 41
    function visible_90 () : java.lang.Boolean {
      return perm.System.viewsearch
    }
    
    
  }
  
  
}