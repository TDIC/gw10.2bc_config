package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlansLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionPlansLVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlansLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionPlansLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'filters' attribute on ToolbarFilterOptionGroup at CommissionPlansLV.pcf: line 24, column 76
    function filters_1 () : gw.api.filters.IFilter[] {
      return new gw.api.web.plan.PlanListFilterSet().FilterOptions
    }
    
    // 'initialValue' attribute on Variable at CommissionPlansLV.pcf: line 14, column 42
    function initialValue_0 () : gw.api.web.plan.PlanHelper {
      return new gw.api.web.plan.PlanHelper()
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at CommissionPlansLV.pcf: line 33, column 42
    function sortValue_2 (CommissionPlan :  entity.CommissionPlan) : java.lang.Object {
      return CommissionPlan.PlanOrder
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CommissionPlansLV.pcf: line 39, column 40
    function sortValue_3 (CommissionPlan :  entity.CommissionPlan) : java.lang.Object {
      return CommissionPlan.Name
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at CommissionPlansLV.pcf: line 55, column 49
    function sortValue_4 (CommissionPlan :  entity.CommissionPlan) : java.lang.Object {
      return CommissionPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at CommissionPlansLV.pcf: line 59, column 50
    function sortValue_5 (CommissionPlan :  entity.CommissionPlan) : java.lang.Object {
      return CommissionPlan.ExpirationDate
    }
    
    // 'value' attribute on RowIterator at CommissionPlansLV.pcf: line 19, column 81
    function value_31 () : gw.api.database.IQueryBeanResult<entity.CommissionPlan> {
      return commissionPlans
    }
    
    property get PlanHelper () : gw.api.web.plan.PlanHelper {
      return getVariableValue("PlanHelper", 0) as gw.api.web.plan.PlanHelper
    }
    
    property set PlanHelper ($arg :  gw.api.web.plan.PlanHelper) {
      setVariableValue("PlanHelper", 0, $arg)
    }
    
    property get commissionPlans () : gw.api.database.IQueryBeanResult<CommissionPlan> {
      return getRequireValue("commissionPlans", 0) as gw.api.database.IQueryBeanResult<CommissionPlan>
    }
    
    property set commissionPlans ($arg :  gw.api.database.IQueryBeanResult<CommissionPlan>) {
      setRequireValue("commissionPlans", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/commission/CommissionPlansLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CommissionPlansLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=MoveUp) at CommissionPlansLV.pcf: line 70, column 56
    function action_27 () : void {
      PlanHelper.moveUp(CommissionPlan);
    }
    
    // 'action' attribute on Link (id=MoveDown) at CommissionPlansLV.pcf: line 78, column 62
    function action_30 () : void {
      PlanHelper.moveDown(CommissionPlan);
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at CommissionPlansLV.pcf: line 39, column 40
    function action_9 () : void {
      CommissionPlanDetail.go(CommissionPlan)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at CommissionPlansLV.pcf: line 39, column 40
    function action_dest_10 () : pcf.api.Destination {
      return pcf.CommissionPlanDetail.createDestination(CommissionPlan)
    }
    
    // 'available' attribute on Link (id=MoveUp) at CommissionPlansLV.pcf: line 70, column 56
    function available_25 () : java.lang.Boolean {
      return perm.System.commplanedit
    }
    
    // 'outputConversion' attribute on TextCell (id=BasePercentage_Cell) at CommissionPlansLV.pcf: line 46, column 45
    function outputConversion_14 (VALUE :  java.math.BigDecimal) : java.lang.String {
      return gw.api.util.StringUtil.formatPercentagePoints(VALUE, 2)
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at CommissionPlansLV.pcf: line 33, column 42
    function valueRoot_7 () : java.lang.Object {
      return CommissionPlan
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at CommissionPlansLV.pcf: line 39, column 40
    function value_11 () : java.lang.String {
      return CommissionPlan.Name
    }
    
    // 'value' attribute on TextCell (id=BasePercentage_Cell) at CommissionPlansLV.pcf: line 46, column 45
    function value_15 () : java.math.BigDecimal {
      return CommissionPlan.DefaultSubPlan.getBaseRate(TC_PRIMARY)
    }
    
    // 'value' attribute on BooleanRadioCell (id=IncludesIncentives_Cell) at CommissionPlansLV.pcf: line 51, column 75
    function value_17 () : java.lang.Boolean {
      return CommissionPlan.DefaultSubPlan.Incentives.length > 0
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at CommissionPlansLV.pcf: line 55, column 49
    function value_19 () : java.util.Date {
      return CommissionPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at CommissionPlansLV.pcf: line 59, column 50
    function value_22 () : java.util.Date {
      return CommissionPlan.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=PlanOrder_Cell) at CommissionPlansLV.pcf: line 33, column 42
    function value_6 () : java.lang.Integer {
      return CommissionPlan.PlanOrder
    }
    
    // 'visible' attribute on Link (id=MoveUp) at CommissionPlansLV.pcf: line 70, column 56
    function visible_26 () : java.lang.Boolean {
      return CommissionPlan.PlanOrder > 1
    }
    
    // 'visible' attribute on Link (id=MoveDown) at CommissionPlansLV.pcf: line 78, column 62
    function visible_29 () : java.lang.Boolean {
      return !CommissionPlan.hasHighestPlanOrder()
    }
    
    property get CommissionPlan () : entity.CommissionPlan {
      return getIteratedValue(1) as entity.CommissionPlan
    }
    
    
  }
  
  
}