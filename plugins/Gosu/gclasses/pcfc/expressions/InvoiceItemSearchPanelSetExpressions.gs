package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/InvoiceItemSearchPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceItemSearchPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/InvoiceItemSearchPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceItemSearchPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at InvoiceItemSearchPanelSet.pcf: line 23, column 57
    function initialValue_0 () : gw.api.web.account.AccountSearchConverter {
      return new gw.api.web.account.AccountSearchConverter(StringCriterionMode.TC_STARTSWITH)
    }
    
    // 'initialValue' attribute on Variable at InvoiceItemSearchPanelSet.pcf: line 27, column 59
    function initialValue_1 () : gw.api.web.producer.ProducerSearchConverter {
      return new gw.api.web.producer.ProducerSearchConverter(StringCriterionMode.TC_STARTSWITH)
    }
    
    property get accountSearchConverter () : gw.api.web.account.AccountSearchConverter {
      return getVariableValue("accountSearchConverter", 0) as gw.api.web.account.AccountSearchConverter
    }
    
    property set accountSearchConverter ($arg :  gw.api.web.account.AccountSearchConverter) {
      setVariableValue("accountSearchConverter", 0, $arg)
    }
    
    property get baseDist () : BaseDist {
      return getRequireValue("baseDist", 0) as BaseDist
    }
    
    property set baseDist ($arg :  BaseDist) {
      setRequireValue("baseDist", 0, $arg)
    }
    
    property get initialPayerAccount () : Account {
      return getRequireValue("initialPayerAccount", 0) as Account
    }
    
    property set initialPayerAccount ($arg :  Account) {
      setRequireValue("initialPayerAccount", 0, $arg)
    }
    
    property get initialPayerProducer () : Producer {
      return getRequireValue("initialPayerProducer", 0) as Producer
    }
    
    property set initialPayerProducer ($arg :  Producer) {
      setRequireValue("initialPayerProducer", 0, $arg)
    }
    
    property get producerSearchConverter () : gw.api.web.producer.ProducerSearchConverter {
      return getVariableValue("producerSearchConverter", 0) as gw.api.web.producer.ProducerSearchConverter
    }
    
    property set producerSearchConverter ($arg :  gw.api.web.producer.ProducerSearchConverter) {
      setVariableValue("producerSearchConverter", 0, $arg)
    }
    
    function createNewSearchCriteria(): gw.search.InvoiceItemSearchCriteria {
              var criteria = new gw.search.InvoiceItemSearchCriteria()
              criteria.PayingAccount = initialPayerAccount
              criteria.PayerAccountNumber = initialPayerAccount as String
              criteria.PayingProducer = initialPayerProducer
              criteria.PayerProducerName = initialPayerProducer as String
              return criteria
            }
    
            property get PolicyPeriodLabel(): String {
              return baseDist typeis DirectBillPayment ? DisplayKey.get("Web.AddInvoiceItemsPopup.InvoiceItemsLV.PolicyPeriod") : DisplayKey.get("Web.AddInvoiceItemsPopup.InvoiceItemsLV.Policy")
            }
    
            property get OwnerLabel(): String {
              return baseDist typeis DirectBillPayment ? DisplayKey.get("Web.AddInvoiceItemsPopup.InvoiceItemsLV.Owner") : DisplayKey.get("Web.AddInvoiceItemsPopup.InvoiceItemsLV.Account")
            }
    
            property get InvoiceDateLabel(): String {
              return baseDist typeis DirectBillPayment ? DisplayKey.get("Web.AddInvoiceItemsPopup.InvoiceItemsLV.InvoiceDate") : DisplayKey.get("Web.AddInvoiceItemsPopup.InvoiceItemsLV.StatementDate")
            }
    
            property get CommissionLabel(): String {
              return baseDist typeis DirectBillPayment ? DisplayKey.get("Web.AddInvoiceItemsPopup.InvoiceItemsLV.CommissionAmount") : DisplayKey.get("Web.AddInvoiceItemsPopup.InvoiceItemsLV.Commission")
            }
    
            function getProducerCode(invoiceItem: InvoiceItem): ProducerCode {
              return baseDist typeis DirectBillPayment ? invoiceItem.PolicyPeriod.PrimaryPolicyCommission.ProducerCode : invoiceItem.ActivePrimaryItemCommission.PolicyCommission.ProducerCode;
            }
    
            function filterBilledStatus(value: InvoiceStatus): boolean {
              if (baseDist typeis AgencyCycleDist) {
                return (value != InvoiceStatus.TC_CARRIEDFORWARD && value != InvoiceStatus.TC_WRITTENOFF)
              } else {
                return true
              }
            }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/payment/InvoiceItemSearchPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SearchPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=PolicyPeriod_Cell) at InvoiceItemSearchPanelSet.pcf: line 152, column 48
    function actionAvailable_82 () : java.lang.Boolean {
      return invoiceItemSearchView.PolicyPeriod != null
    }
    
    // 'action' attribute on TextCell (id=PolicyPeriod_Cell) at InvoiceItemSearchPanelSet.pcf: line 152, column 48
    function action_79 () : void {
      PolicySummary.push(invoiceItemSearchView.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=PolicyPeriod_Cell) at InvoiceItemSearchPanelSet.pcf: line 152, column 48
    function action_dest_80 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(invoiceItemSearchView.PolicyPeriod)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=GrossAmount_Cell) at InvoiceItemSearchPanelSet.pcf: line 208, column 53
    function currency_112 () : typekey.Currency {
      return invoiceItemSearchView.Invoice.Currency
    }
    
    // 'label' attribute on DateCell (id=InvoiceDate_Cell) at InvoiceItemSearchPanelSet.pcf: line 196, column 64
    function label_102 () : java.lang.Object {
      return InvoiceDateLabel
    }
    
    // 'label' attribute on MonetaryAmountCell (id=CommissionAmount_Cell) at InvoiceItemSearchPanelSet.pcf: line 221, column 70
    function label_118 () : java.lang.Object {
      return CommissionLabel
    }
    
    // 'label' attribute on TextCell (id=PolicyPeriod_Cell) at InvoiceItemSearchPanelSet.pcf: line 152, column 48
    function label_81 () : java.lang.Object {
      return PolicyPeriodLabel
    }
    
    // 'value' attribute on TextCell (id=InvoiceStatus_Cell) at InvoiceItemSearchPanelSet.pcf: line 189, column 66
    function valueRoot_100 () : java.lang.Object {
      return invoiceItemSearchView.Invoice
    }
    
    // 'value' attribute on TextCell (id=Item_Cell) at InvoiceItemSearchPanelSet.pcf: line 136, column 68
    function valueRoot_73 () : java.lang.Object {
      return invoiceItemSearchView
    }
    
    // 'value' attribute on TextCell (id=ItemContext_Cell) at InvoiceItemSearchPanelSet.pcf: line 143, column 58
    function valueRoot_76 () : java.lang.Object {
      return invoiceItemSearchView.Charge
    }
    
    // 'value' attribute on TextCell (id=Producer_Cell) at InvoiceItemSearchPanelSet.pcf: line 176, column 58
    function valueRoot_92 () : java.lang.Object {
      return getProducerCode(invoiceItemSearchView).Producer
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at InvoiceItemSearchPanelSet.pcf: line 183, column 58
    function valueRoot_96 () : java.lang.Object {
      return getProducerCode(invoiceItemSearchView)
    }
    
    // 'value' attribute on DateCell (id=InvoiceDate_Cell) at InvoiceItemSearchPanelSet.pcf: line 196, column 64
    function value_103 () : java.util.Date {
      return invoiceItemSearchView.Invoice.EventDate
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at InvoiceItemSearchPanelSet.pcf: line 202, column 62
    function value_107 () : java.util.Date {
      return invoiceItemSearchView.Invoice.DueDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmount_Cell) at InvoiceItemSearchPanelSet.pcf: line 208, column 53
    function value_110 () : gw.pl.currency.MonetaryAmount {
      return invoiceItemSearchView.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetAmount_Cell) at InvoiceItemSearchPanelSet.pcf: line 215, column 56
    function value_114 () : gw.pl.currency.MonetaryAmount {
      return invoiceItemSearchView.NetAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionAmount_Cell) at InvoiceItemSearchPanelSet.pcf: line 221, column 70
    function value_119 () : gw.pl.currency.MonetaryAmount {
      return invoiceItemSearchView.PrimaryCommissionAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AmountPaid_Cell) at InvoiceItemSearchPanelSet.pcf: line 228, column 263
    function value_124 () : gw.pl.currency.MonetaryAmount {
      return invoiceItemSearchView.Amount - (baseDist typeis AgencyCyclePayment ? invoiceItemSearchView.PaidAmount : invoiceItemSearchView.PromisedAndPaidAmount) - invoiceItemSearchView.GrossAmountWrittenOff - invoiceItemSearchView.UnsettledCommission
    }
    
    // 'value' attribute on TextCell (id=Item_Cell) at InvoiceItemSearchPanelSet.pcf: line 136, column 68
    function value_72 () : java.lang.String {
      return invoiceItemSearchView.DisplayNameAsItemType
    }
    
    // 'value' attribute on TextCell (id=ItemContext_Cell) at InvoiceItemSearchPanelSet.pcf: line 143, column 58
    function value_75 () : entity.BillingInstruction {
      return invoiceItemSearchView.Charge.BillingInstruction
    }
    
    // 'value' attribute on TextCell (id=PolicyPeriod_Cell) at InvoiceItemSearchPanelSet.pcf: line 152, column 48
    function value_83 () : entity.PolicyPeriod {
      return invoiceItemSearchView.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=Payer_Cell) at InvoiceItemSearchPanelSet.pcf: line 169, column 60
    function value_87 () : gw.api.domain.invoice.InvoicePayer {
      return invoiceItemSearchView.Payer
    }
    
    // 'value' attribute on TextCell (id=Producer_Cell) at InvoiceItemSearchPanelSet.pcf: line 176, column 58
    function value_91 () : java.lang.String {
      return getProducerCode(invoiceItemSearchView).Producer.DisplayName
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at InvoiceItemSearchPanelSet.pcf: line 183, column 58
    function value_95 () : java.lang.String {
      return getProducerCode(invoiceItemSearchView).Code
    }
    
    // 'value' attribute on TextCell (id=InvoiceStatus_Cell) at InvoiceItemSearchPanelSet.pcf: line 189, column 66
    function value_99 () : java.lang.String {
      return invoiceItemSearchView.Invoice.StatusForUI
    }
    
    // 'visible' attribute on TextCell (id=ItemContext_Cell) at InvoiceItemSearchPanelSet.pcf: line 143, column 58
    function visible_77 () : java.lang.Boolean {
      return baseDist typeis AgencyCycleDist
    }
    
    // 'visible' attribute on TextCell (id=Payer_Cell) at InvoiceItemSearchPanelSet.pcf: line 169, column 60
    function visible_89 () : java.lang.Boolean {
      return baseDist typeis DirectBillPayment
    }
    
    property get invoiceItemSearchView () : entity.InvoiceItem {
      return getIteratedValue(2) as entity.InvoiceItem
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/payment/InvoiceItemSearchPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends InvoiceItemSearchPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerInput (id=PayerProducer_Input) at InvoiceItemSearchPanelSet.pcf: line 65, column 57
    function action_13 () : void {
      ProducerSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=PayerProducerKanji_Input) at InvoiceItemSearchPanelSet.pcf: line 74, column 125
    function action_26 () : void {
      ProducerSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=PolicyPeriod_Input) at InvoiceItemSearchPanelSet.pcf: line 89, column 43
    function action_37 () : void {
      PolicySearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=PayerAccount_Input) at InvoiceItemSearchPanelSet.pcf: line 56, column 56
    function action_5 () : void {
      AccountSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=PayerProducer_Input) at InvoiceItemSearchPanelSet.pcf: line 65, column 57
    function action_dest_14 () : pcf.api.Destination {
      return pcf.ProducerSearchPopup.createDestination()
    }
    
    // 'pickLocation' attribute on PickerInput (id=PayerProducerKanji_Input) at InvoiceItemSearchPanelSet.pcf: line 74, column 125
    function action_dest_27 () : pcf.api.Destination {
      return pcf.ProducerSearchPopup.createDestination()
    }
    
    // 'pickLocation' attribute on PickerInput (id=PolicyPeriod_Input) at InvoiceItemSearchPanelSet.pcf: line 89, column 43
    function action_dest_38 () : pcf.api.Destination {
      return pcf.PolicySearchPopup.createDestination()
    }
    
    // 'pickLocation' attribute on PickerInput (id=PayerAccount_Input) at InvoiceItemSearchPanelSet.pcf: line 56, column 56
    function action_dest_6 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'def' attribute on InputSetRef at InvoiceItemSearchPanelSet.pcf: line 116, column 45
    function def_onEnter_59 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at InvoiceItemSearchPanelSet.pcf: line 116, column 45
    function def_refreshVariables_60 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on PickerInput (id=PayerProducer_Input) at InvoiceItemSearchPanelSet.pcf: line 65, column 57
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.PayerProducerName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PickerInput (id=PayerProducerKanji_Input) at InvoiceItemSearchPanelSet.pcf: line 74, column 125
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.PayerProducerNameKanji = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on PickerInput (id=PolicyPeriod_Input) at InvoiceItemSearchPanelSet.pcf: line 89, column 43
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.PolicyPeriod = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=ChargeGroup_Input) at InvoiceItemSearchPanelSet.pcf: line 96, column 49
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ChargeGroup = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=ChargePattern_Input) at InvoiceItemSearchPanelSet.pcf: line 103, column 47
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ChargePattern = (__VALUE_TO_SET as entity.ChargePattern)
    }
    
    // 'value' attribute on TypeKeyInput (id=InvoiceStatus_Input) at InvoiceItemSearchPanelSet.pcf: line 110, column 48
    function defaultSetter_55 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.BilledStatus = (__VALUE_TO_SET as typekey.InvoiceStatus)
    }
    
    // 'editable' attribute on PickerInput (id=PayerProducer_Input) at InvoiceItemSearchPanelSet.pcf: line 65, column 57
    function editable_15 () : java.lang.Boolean {
      return baseDist typeis DirectBillPayment
    }
    
    // 'filter' attribute on TypeKeyInput (id=InvoiceStatus_Input) at InvoiceItemSearchPanelSet.pcf: line 110, column 48
    function filter_57 (VALUE :  typekey.InvoiceStatus, VALUES :  typekey.InvoiceStatus[]) : java.lang.Boolean {
      return filterBilledStatus(VALUE)
    }
    
    // 'inputConversion' attribute on PickerInput (id=PayerProducer_Input) at InvoiceItemSearchPanelSet.pcf: line 65, column 57
    function inputConversion_18 (VALUE :  java.lang.String) : java.lang.Object {
      return producerSearchConverter.getProducer(VALUE)
    }
    
    // 'inputConversion' attribute on PickerInput (id=PayerAccount_Input) at InvoiceItemSearchPanelSet.pcf: line 56, column 56
    function inputConversion_8 (VALUE :  java.lang.String) : java.lang.Object {
      return accountSearchConverter.getAccount(VALUE)
    }
    
    // 'label' attribute on PickerInput (id=PayerProducer_Input) at InvoiceItemSearchPanelSet.pcf: line 65, column 57
    function label_17 () : java.lang.Object {
      return gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP ? DisplayKey.get("Web.AddInvoiceItemsPopup.PayerProducerPhonetic") : DisplayKey.get("Web.AddInvoiceItemsPopup.PayerProducer")
    }
    
    // 'label' attribute on TextCell (id=PolicyPeriod_Cell) at InvoiceItemSearchPanelSet.pcf: line 152, column 48
    function label_63 () : java.lang.Object {
      return PolicyPeriodLabel
    }
    
    // 'label' attribute on DateCell (id=InvoiceDate_Cell) at InvoiceItemSearchPanelSet.pcf: line 196, column 64
    function label_68 () : java.lang.Object {
      return InvoiceDateLabel
    }
    
    // 'label' attribute on MonetaryAmountCell (id=CommissionAmount_Cell) at InvoiceItemSearchPanelSet.pcf: line 221, column 70
    function label_70 () : java.lang.Object {
      return CommissionLabel
    }
    
    // 'searchCriteria' attribute on SearchPanel at InvoiceItemSearchPanelSet.pcf: line 36, column 79
    function searchCriteria_129 () : gw.search.InvoiceItemSearchCriteria {
      return createNewSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at InvoiceItemSearchPanelSet.pcf: line 36, column 79
    function search_128 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria, baseDist)
    }
    
    // 'value' attribute on TextCell (id=ItemContext_Cell) at InvoiceItemSearchPanelSet.pcf: line 143, column 58
    function sortValue_61 (invoiceItemSearchView :  entity.InvoiceItem) : java.lang.Object {
      return invoiceItemSearchView.Charge.BillingInstruction
    }
    
    // 'value' attribute on TextCell (id=PolicyPeriod_Cell) at InvoiceItemSearchPanelSet.pcf: line 152, column 48
    function sortValue_64 (invoiceItemSearchView :  entity.InvoiceItem) : java.lang.Object {
      return invoiceItemSearchView.PolicyPeriod
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmount_Cell) at InvoiceItemSearchPanelSet.pcf: line 208, column 53
    function sortValue_69 (invoiceItemSearchView :  entity.InvoiceItem) : java.lang.Object {
      return invoiceItemSearchView.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionAmount_Cell) at InvoiceItemSearchPanelSet.pcf: line 221, column 70
    function sortValue_71 (invoiceItemSearchView :  entity.InvoiceItem) : java.lang.Object {
      return invoiceItemSearchView.PrimaryCommissionAmount
    }
    
    // 'valueRange' attribute on RangeInput (id=ChargePattern_Input) at InvoiceItemSearchPanelSet.pcf: line 103, column 47
    function valueRange_50 () : java.lang.Object {
      return gw.api.database.Query.make(ChargePattern).select()
    }
    
    // 'value' attribute on BooleanRadioInput (id=AccountOrProducerPayer_Input) at InvoiceItemSearchPanelSet.pcf: line 46, column 53
    function valueRoot_3 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on RowIterator at InvoiceItemSearchPanelSet.pcf: line 127, column 82
    function value_127 () : gw.api.database.IQueryBeanResult<entity.InvoiceItem> {
      return invoiceItemSearchViews
    }
    
    // 'value' attribute on PickerInput (id=PayerProducer_Input) at InvoiceItemSearchPanelSet.pcf: line 65, column 57
    function value_19 () : java.lang.String {
      return searchCriteria.PayerProducerName
    }
    
    // 'value' attribute on BooleanRadioInput (id=AccountOrProducerPayer_Input) at InvoiceItemSearchPanelSet.pcf: line 46, column 53
    function value_2 () : java.lang.Boolean {
      return searchCriteria.PayerIsAnAccount
    }
    
    // 'value' attribute on PickerInput (id=PayerProducerKanji_Input) at InvoiceItemSearchPanelSet.pcf: line 74, column 125
    function value_31 () : java.lang.String {
      return searchCriteria.PayerProducerNameKanji
    }
    
    // 'value' attribute on PickerInput (id=PolicyPeriod_Input) at InvoiceItemSearchPanelSet.pcf: line 89, column 43
    function value_39 () : java.lang.String {
      return searchCriteria.PolicyPeriod
    }
    
    // 'value' attribute on TextInput (id=ChargeGroup_Input) at InvoiceItemSearchPanelSet.pcf: line 96, column 49
    function value_43 () : java.lang.String {
      return searchCriteria.ChargeGroup
    }
    
    // 'value' attribute on RangeInput (id=ChargePattern_Input) at InvoiceItemSearchPanelSet.pcf: line 103, column 47
    function value_47 () : entity.ChargePattern {
      return searchCriteria.ChargePattern
    }
    
    // 'value' attribute on TypeKeyInput (id=InvoiceStatus_Input) at InvoiceItemSearchPanelSet.pcf: line 110, column 48
    function value_54 () : typekey.InvoiceStatus {
      return searchCriteria.BilledStatus
    }
    
    // 'value' attribute on PickerInput (id=PayerAccount_Input) at InvoiceItemSearchPanelSet.pcf: line 56, column 56
    function value_9 () : java.lang.String {
      return searchCriteria.PayerAccountNumber
    }
    
    // 'valueRange' attribute on RangeInput (id=ChargePattern_Input) at InvoiceItemSearchPanelSet.pcf: line 103, column 47
    function verifyValueRangeIsAllowedType_51 ($$arg :  entity.ChargePattern[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ChargePattern_Input) at InvoiceItemSearchPanelSet.pcf: line 103, column 47
    function verifyValueRangeIsAllowedType_51 ($$arg :  gw.api.database.IQueryBeanResult<entity.ChargePattern>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ChargePattern_Input) at InvoiceItemSearchPanelSet.pcf: line 103, column 47
    function verifyValueRangeIsAllowedType_51 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ChargePattern_Input) at InvoiceItemSearchPanelSet.pcf: line 103, column 47
    function verifyValueRange_52 () : void {
      var __valueRangeArg = gw.api.database.Query.make(ChargePattern).select()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_51(__valueRangeArg)
    }
    
    // 'visible' attribute on PickerInput (id=PayerProducer_Input) at InvoiceItemSearchPanelSet.pcf: line 65, column 57
    function visible_16 () : java.lang.Boolean {
      return !searchCriteria.PayerIsAnAccount
    }
    
    // 'visible' attribute on PickerInput (id=PayerProducerKanji_Input) at InvoiceItemSearchPanelSet.pcf: line 74, column 125
    function visible_29 () : java.lang.Boolean {
      return gw.api.util.LocaleUtil.CurrentLocaleType == LocaleType.TC_JA_JP and !searchCriteria.PayerIsAnAccount
    }
    
    // 'visible' attribute on TextCell (id=ItemContext_Cell) at InvoiceItemSearchPanelSet.pcf: line 143, column 58
    function visible_62 () : java.lang.Boolean {
      return baseDist typeis AgencyCycleDist
    }
    
    property get invoiceItemSearchViews () : gw.api.database.IQueryBeanResult<InvoiceItem> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<InvoiceItem>
    }
    
    property get searchCriteria () : gw.search.InvoiceItemSearchCriteria {
      return getCriteriaValue(1) as gw.search.InvoiceItemSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.InvoiceItemSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}