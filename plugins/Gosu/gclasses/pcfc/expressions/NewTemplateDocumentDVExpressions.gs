package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.pl.persistence.core.Bean
uses gw.document.SimpleSymbol
uses gw.api.util.LocaleUtil
uses gw.document.DocumentsUtilBase
@javax.annotation.Generated("config/web/pcf/document/NewTemplateDocumentDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewTemplateDocumentDVExpressions {
  @javax.annotation.Generated("config/web/pcf/document/NewTemplateDocumentDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewTemplateDocumentDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on DocumentTemplateInput (id=TemplatePicker_Input) at NewTemplateDocumentDV.pcf: line 61, column 69
    function action_6 () : void {
      DocumentTemplateSearchPopup.push(documentContainer, documentCreationInfo)
    }
    
    // 'pickLocation' attribute on DocumentTemplateInput (id=TemplatePicker_Input) at NewTemplateDocumentDV.pcf: line 61, column 69
    function action_dest_7 () : pcf.api.Destination {
      return pcf.DocumentTemplateSearchPopup.createDestination(documentContainer, documentCreationInfo)
    }
    
    // 'def' attribute on InputSetRef at NewTemplateDocumentDV.pcf: line 80, column 45
    function def_onEnter_23 (def :  pcf.DocumentTemplateContextObjectIteratorInputSet_default) : void {
      def.onEnter(documentBCContext, documentCreationInfo)
    }
    
    // 'def' attribute on InputSetRef at NewTemplateDocumentDV.pcf: line 80, column 45
    function def_onEnter_25 (def :  pcf.DocumentTemplateContextObjectIteratorInputSet_noticeofofferofmodoraltworkdwcad1013353pdf) : void {
      def.onEnter(documentBCContext, documentCreationInfo)
    }
    
    // 'def' attribute on InputSetRef at NewTemplateDocumentDV.pcf: line 83, column 45
    function def_onEnter_28 (def :  pcf.DocumentCreationInputSet) : void {
      def.onEnter(documentBCContext, documentCreationInfo, Step3Label)
    }
    
    // 'def' attribute on InputSetRef at NewTemplateDocumentDV.pcf: line 80, column 45
    function def_refreshVariables_24 (def :  pcf.DocumentTemplateContextObjectIteratorInputSet_default) : void {
      def.refreshVariables(documentBCContext, documentCreationInfo)
    }
    
    // 'def' attribute on InputSetRef at NewTemplateDocumentDV.pcf: line 80, column 45
    function def_refreshVariables_26 (def :  pcf.DocumentTemplateContextObjectIteratorInputSet_noticeofofferofmodoraltworkdwcad1013353pdf) : void {
      def.refreshVariables(documentBCContext, documentCreationInfo)
    }
    
    // 'def' attribute on InputSetRef at NewTemplateDocumentDV.pcf: line 83, column 45
    function def_refreshVariables_29 (def :  pcf.DocumentCreationInputSet) : void {
      def.refreshVariables(documentBCContext, documentCreationInfo, Step3Label)
    }
    
    // 'value' attribute on DocumentTemplateInput (id=TemplatePicker_Input) at NewTemplateDocumentDV.pcf: line 61, column 69
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      documentCreationInfo.DocumentTemplateDescriptor = (__VALUE_TO_SET as gw.plugin.document.IDocumentTemplateDescriptor)
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at NewTemplateDocumentDV.pcf: line 68, column 66
    function defaultSetter_19 (__VALUE_TO_SET :  java.lang.Object) : void {
      languageType = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'editable' attribute on DocumentTemplateInput (id=TemplatePicker_Input) at NewTemplateDocumentDV.pcf: line 61, column 69
    function editable_8 () : java.lang.Boolean {
      return documentBCContext.DocumentCreationReadOnly
    }
    
    // 'initialValue' attribute on Variable at NewTemplateDocumentDV.pcf: line 19, column 24
    function initialValue_0 () : Document {
      return documentCreationInfo.Document
    }
    
    // 'initialValue' attribute on Variable at NewTemplateDocumentDV.pcf: line 23, column 23
    function initialValue_1 () : Account {
      return documentContainer typeis Account ? documentContainer : null
    }
    
    // 'initialValue' attribute on Variable at NewTemplateDocumentDV.pcf: line 27, column 22
    function initialValue_2 () : Policy {
      return documentContainer typeis Policy ? documentContainer : null
    }
    
    // 'initialValue' attribute on Variable at NewTemplateDocumentDV.pcf: line 31, column 24
    function initialValue_3 () : Producer {
      return documentContainer typeis Producer ? documentContainer : null
    }
    
    // 'initialValue' attribute on Variable at NewTemplateDocumentDV.pcf: line 35, column 28
    function initialValue_4 () : LanguageType {
      return document.Language
    }
    
    // 'initialValue' attribute on Variable at NewTemplateDocumentDV.pcf: line 43, column 23
    function initialValue_5 () : boolean {
      return initDV()
    }
    
    // 'mode' attribute on InputSetRef at NewTemplateDocumentDV.pcf: line 80, column 45
    function mode_27 () : java.lang.Object {
      return documentCreationInfo.DocumentTemplateDescriptor.TemplateId
    }
    
    // 'onChange' attribute on PostOnChange at NewTemplateDocumentDV.pcf: line 71, column 40
    function onChange_15 () : void {
      changeLanguage()
    }
    
    // 'onPick' attribute on DocumentTemplateInput (id=TemplatePicker_Input) at NewTemplateDocumentDV.pcf: line 61, column 69
    function onPick_9 (PickedValue :  gw.plugin.document.IDocumentTemplateDescriptor) : void {
      documentBCContext.resetTemplateBasedInfo(documentCreationInfo, documentCreationInfo.DocumentTemplateDescriptor); documentCreationInfo.evaluateDynamicWidgets()
    }
    
    // 'value' attribute on DocumentTemplateInput (id=TemplatePicker_Input) at NewTemplateDocumentDV.pcf: line 61, column 69
    function valueRoot_12 () : java.lang.Object {
      return documentCreationInfo
    }
    
    // 'value' attribute on DocumentTemplateInput (id=TemplatePicker_Input) at NewTemplateDocumentDV.pcf: line 61, column 69
    function value_10 () : gw.plugin.document.IDocumentTemplateDescriptor {
      return documentCreationInfo.DocumentTemplateDescriptor
    }
    
    // 'value' attribute on TypeKeyInput (id=Language_Input) at NewTemplateDocumentDV.pcf: line 68, column 66
    function value_18 () : typekey.LanguageType {
      return languageType
    }
    
    // 'visible' attribute on TypeKeyInput (id=Language_Input) at NewTemplateDocumentDV.pcf: line 68, column 66
    function visible_17 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count > 1
    }
    
    // 'visible' attribute on InputSet at NewTemplateDocumentDV.pcf: line 74, column 75
    function visible_30 () : java.lang.Boolean {
      return documentCreationInfo.DocumentTemplateDescriptor != null
    }
    
    property get Step3Label () : String {
      return getVariableValue("Step3Label", 0) as String
    }
    
    property set Step3Label ($arg :  String) {
      setVariableValue("Step3Label", 0, $arg)
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get document () : Document {
      return getVariableValue("document", 0) as Document
    }
    
    property set document ($arg :  Document) {
      setVariableValue("document", 0, $arg)
    }
    
    property get documentBCContext () : gw.document.DocumentBCContext {
      return getRequireValue("documentBCContext", 0) as gw.document.DocumentBCContext
    }
    
    property set documentBCContext ($arg :  gw.document.DocumentBCContext) {
      setRequireValue("documentBCContext", 0, $arg)
    }
    
    property get documentContainer () : DocumentContainer {
      return getRequireValue("documentContainer", 0) as DocumentContainer
    }
    
    property set documentContainer ($arg :  DocumentContainer) {
      setRequireValue("documentContainer", 0, $arg)
    }
    
    property get documentCreationInfo () : gw.document.DocumentCreationInfo {
      return getRequireValue("documentCreationInfo", 0) as gw.document.DocumentCreationInfo
    }
    
    property set documentCreationInfo ($arg :  gw.document.DocumentCreationInfo) {
      setRequireValue("documentCreationInfo", 0, $arg)
    }
    
    property get initialized () : boolean {
      return getVariableValue("initialized", 0) as java.lang.Boolean
    }
    
    property set initialized ($arg :  boolean) {
      setVariableValue("initialized", 0, $arg)
    }
    
    property get languageType () : LanguageType {
      return getVariableValue("languageType", 0) as LanguageType
    }
    
    property set languageType ($arg :  LanguageType) {
      setVariableValue("languageType", 0, $arg)
    }
    
    property get policy () : Policy {
      return getVariableValue("policy", 0) as Policy
    }
    
    property set policy ($arg :  Policy) {
      setVariableValue("policy", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
          function initDV(): boolean {
            documentCreationInfo.addSymbols({
              // Full set of symbols available to Template Descriptors in earlier versions of CC
              // Included for backwards compatibility and can likely be pruned
              "DocumentCreationInfo"->documentCreationInfo,
              "Document"->document,
              "language"->languageType,
              "documentContainer"->documentContainer,
    
              // The following are added with explicit types to facilitate
              // null-safe access within template descriptor Gosu expressions.
              // Otherwise, null values would be mapped to the Object type.
              "Account"->new SimpleSymbol(Account, account),
              "Policy"->new SimpleSymbol(Policy, policy),
              "Producer"->new SimpleSymbol(Producer, producer)
            })
            if (documentCreationInfo.DocumentTemplateDescriptor != null) {
              documentCreationInfo.evaluateDynamicWidgets()
            }
            return true
          }
    
          function changeLanguage() {
            documentBCContext.resetTemplateBasedInfo(documentCreationInfo,
              DocumentsUtilBase.fetchDocumentTemplate(documentCreationInfo.DocumentTemplateDescriptor.TemplateId,
                LocaleUtil.toLanguage(languageType), CurrentLocation))
            documentCreationInfo.evaluateDynamicWidgets()
          }
    
        
    
    
  }
  
  
}