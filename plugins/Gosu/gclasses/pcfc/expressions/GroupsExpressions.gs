package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.path.Paths
uses gw.api.database.QuerySelectColumns
@javax.annotation.Generated("config/web/pcf/admin/usersandsecurity/Groups.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class GroupsExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/usersandsecurity/Groups.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class GroupsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=Groups_NewGroupButton) at Groups.pcf: line 26, column 27
    function action_1 () : void {
      NewGroup.push()
    }
    
    // 'action' attribute on ToolbarButton (id=Groups_NewGroupButton) at Groups.pcf: line 26, column 27
    function action_dest_2 () : pcf.api.Destination {
      return pcf.NewGroup.createDestination()
    }
    
    // 'afterCancel' attribute on Page (id=Groups) at Groups.pcf: line 12, column 64
    function afterCancel_5 () : void {
      Groups.go()
    }
    
    // 'afterCancel' attribute on Page (id=Groups) at Groups.pcf: line 12, column 64
    function afterCancel_dest_6 () : pcf.api.Destination {
      return pcf.Groups.createDestination()
    }
    
    // 'afterCommit' attribute on Page (id=Groups) at Groups.pcf: line 12, column 64
    function afterCommit_7 (TopLocation :  pcf.api.Location) : void {
      Groups.go()
    }
    
    // 'canEdit' attribute on Page (id=Groups) at Groups.pcf: line 12, column 64
    function canEdit_8 () : java.lang.Boolean {
      return perm.Group.edit
    }
    
    // 'canVisit' attribute on Page (id=Groups) at Groups.pcf: line 12, column 64
    static function canVisit_9 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.useradmin
    }
    
    // 'def' attribute on PanelRef at Groups.pcf: line 20, column 35
    function def_onEnter_3 (def :  pcf.GroupsLV) : void {
      def.onEnter(AllGroups)
    }
    
    // 'def' attribute on PanelRef at Groups.pcf: line 20, column 35
    function def_refreshVariables_4 (def :  pcf.GroupsLV) : void {
      def.refreshVariables(AllGroups)
    }
    
    // 'initialValue' attribute on Variable at Groups.pcf: line 16, column 61
    function initialValue_0 () : gw.api.database.IQueryBeanResult<Group> {
      return getGroupQuerySortedByGroupName()
    }
    
    // Page (id=Groups) at Groups.pcf: line 12, column 64
    static function parent_10 () : pcf.api.Destination {
      return pcf.UsersAndSecurity.createDestination()
    }
    
    property get AllGroups () : gw.api.database.IQueryBeanResult<Group> {
      return getVariableValue("AllGroups", 0) as gw.api.database.IQueryBeanResult<Group>
    }
    
    property set AllGroups ($arg :  gw.api.database.IQueryBeanResult<Group>) {
      setVariableValue("AllGroups", 0, $arg)
    }
    
    override property get CurrentLocation () : pcf.Groups {
      return super.CurrentLocation as pcf.Groups
    }
    
    
          function getGroupQuerySortedByGroupName(): gw.api.database.IQueryBeanResult<Group> {
            var query = gw.api.database.Query.make(Group).select()
            query.orderBy(QuerySelectColumns.path(Paths.make(Group#Name)))
            return query
          }
    
    
  }
  
  
}