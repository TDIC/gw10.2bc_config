package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.admin.paymentplan.ChargeSlicingOverridesViewHelper
uses java.util.ArrayList
@javax.annotation.Generated("config/web/pcf/admin/payment/PaymentPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentPlanDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/payment/PaymentPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CardViewPanelExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at PaymentPlanDetailScreen.pcf: line 319, column 131
    function def_onEnter_121 (def :  pcf.InstallmentTreatmentInputSet_EveryTerm) : void {
      def.onEnter(selectedOverridesViewHelper)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at PaymentPlanDetailScreen.pcf: line 319, column 131
    function def_onEnter_123 (def :  pcf.InstallmentTreatmentInputSet_FirstTermOnly) : void {
      def.onEnter(selectedOverridesViewHelper)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at PaymentPlanDetailScreen.pcf: line 319, column 131
    function def_onEnter_125 (def :  pcf.InstallmentTreatmentInputSet_None) : void {
      def.onEnter(selectedOverridesViewHelper)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at PaymentPlanDetailScreen.pcf: line 319, column 131
    function def_refreshVariables_122 (def :  pcf.InstallmentTreatmentInputSet_EveryTerm) : void {
      def.refreshVariables(selectedOverridesViewHelper)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at PaymentPlanDetailScreen.pcf: line 319, column 131
    function def_refreshVariables_124 (def :  pcf.InstallmentTreatmentInputSet_FirstTermOnly) : void {
      def.refreshVariables(selectedOverridesViewHelper)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at PaymentPlanDetailScreen.pcf: line 319, column 131
    function def_refreshVariables_126 (def :  pcf.InstallmentTreatmentInputSet_None) : void {
      def.refreshVariables(selectedOverridesViewHelper)
    }
    
    // 'value' attribute on RangeRadioInput (id=DownPaymentFrequencyOverride_Input) at PaymentPlanDetailScreen.pcf: line 310, column 71
    function defaultSetter_115 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectedOverridesViewHelper.FrequencyOfDownPayment = (__VALUE_TO_SET as gw.admin.paymentplan.DownPaymentFrequency)
    }
    
    // 'initialValue' attribute on Variable at PaymentPlanDetailScreen.pcf: line 279, column 49
    function initialValue_112 () : entity.ChargeSlicingOverrides {
      return selectedOverridesViewHelper.ChargeSlicingOverrides
    }
    
    // 'mode' attribute on InputSetRef (id=InstallmentTreatment) at PaymentPlanDetailScreen.pcf: line 319, column 131
    function mode_127 () : java.lang.Object {
      return selectedOverridesViewHelper == null ? "EveryTerm" : selectedOverridesViewHelper.DownPaymentMode
    }
    
    // 'onChange' attribute on PostOnChange at PaymentPlanDetailScreen.pcf: line 312, column 90
    function onChange_113 () : void {
      selectedOverridesViewHelper.onDownPaymentFrequencyChange()
    }
    
    // 'title' attribute on Card (id=OverridesCard) at PaymentPlanDetailScreen.pcf: line 282, column 78
    function title_128 () : java.lang.String {
      return selectedOverridesViewHelper.BillingInstruction.DisplayName
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DownPaymentFrequencyOverride_Input) at PaymentPlanDetailScreen.pcf: line 310, column 71
    function valueRange_117 () : java.lang.Object {
      return gw.admin.paymentplan.DownPaymentFrequency.getValuesForDetailsScreenOverrides()
    }
    
    // 'value' attribute on RangeRadioInput (id=DownPaymentFrequencyOverride_Input) at PaymentPlanDetailScreen.pcf: line 310, column 71
    function valueRoot_116 () : java.lang.Object {
      return selectedOverridesViewHelper
    }
    
    // 'value' attribute on RangeRadioInput (id=DownPaymentFrequencyOverride_Input) at PaymentPlanDetailScreen.pcf: line 310, column 71
    function value_114 () : gw.admin.paymentplan.DownPaymentFrequency {
      return selectedOverridesViewHelper.FrequencyOfDownPayment
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DownPaymentFrequencyOverride_Input) at PaymentPlanDetailScreen.pcf: line 310, column 71
    function verifyValueRangeIsAllowedType_118 ($$arg :  gw.admin.paymentplan.DownPaymentFrequency[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DownPaymentFrequencyOverride_Input) at PaymentPlanDetailScreen.pcf: line 310, column 71
    function verifyValueRangeIsAllowedType_118 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DownPaymentFrequencyOverride_Input) at PaymentPlanDetailScreen.pcf: line 310, column 71
    function verifyValueRange_119 () : void {
      var __valueRangeArg = gw.admin.paymentplan.DownPaymentFrequency.getValuesForDetailsScreenOverrides()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_118(__valueRangeArg)
    }
    
    property get chargeSlicingOverrides () : entity.ChargeSlicingOverrides {
      return getVariableValue("chargeSlicingOverrides", 2) as entity.ChargeSlicingOverrides
    }
    
    property set chargeSlicingOverrides ($arg :  entity.ChargeSlicingOverrides) {
      setVariableValue("chargeSlicingOverrides", 2, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/payment/PaymentPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'checkBoxVisible' attribute on RowIterator (id=ContextBillingInstructionTypeIterator) at PaymentPlanDetailScreen.pcf: line 260, column 101
    function checkBoxVisible_109 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    // 'value' attribute on TypeKeyCell (id=BillingInstructionType_Cell) at PaymentPlanDetailScreen.pcf: line 269, column 57
    function valueRoot_107 () : java.lang.Object {
      return chargeSlicingOverridesView
    }
    
    // 'value' attribute on TypeKeyCell (id=BillingInstructionType_Cell) at PaymentPlanDetailScreen.pcf: line 269, column 57
    function value_106 () : typekey.BillingInstruction {
      return chargeSlicingOverridesView.BillingInstruction
    }
    
    property get chargeSlicingOverridesView () : gw.admin.paymentplan.ChargeSlicingOverridesViewHelper {
      return getIteratedValue(2) as gw.admin.paymentplan.ChargeSlicingOverridesViewHelper
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/payment/PaymentPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ListDetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on AddMenuItem (id=AddButtonMenuItem) at PaymentPlanDetailScreen.pcf: line 240, column 88
    function label_101 () : java.lang.Object {
      return billingInstructionType
    }
    
    // 'toCreateAndAdd' attribute on AddMenuItem (id=AddButtonMenuItem) at PaymentPlanDetailScreen.pcf: line 240, column 88
    function toCreateAndAdd_102 (CheckedValues :  Object[]) : java.lang.Object {
      return addOverrides(billingInstructionType, biOverridesViewHelperList)
    }
    
    // 'visible' attribute on AddMenuItem (id=AddButtonMenuItem) at PaymentPlanDetailScreen.pcf: line 240, column 88
    function visible_100 () : java.lang.Boolean {
      return paymentPlan.getOverridesFor(billingInstructionType) == null
    }
    
    property get billingInstructionType () : typekey.BillingInstruction {
      return getIteratedValue(2) as typekey.BillingInstruction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/payment/PaymentPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ListDetailPanelExpressionsImpl extends PaymentPlanDetailScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at PaymentPlanDetailScreen.pcf: line 219, column 93
    function initialValue_99 () : java.util.List<gw.admin.paymentplan.ChargeSlicingOverridesViewHelper> {
      return getChargeSlicingOverridesForUI()
    }
    
    // 'sortBy' attribute on IteratorSort at PaymentPlanDetailScreen.pcf: line 263, column 30
    function sortBy_104 (chargeSlicingOverridesView :  gw.admin.paymentplan.ChargeSlicingOverridesViewHelper) : java.lang.Object {
      return chargeSlicingOverridesView.BillingInstruction
    }
    
    // 'value' attribute on TypeKeyCell (id=BillingInstructionType_Cell) at PaymentPlanDetailScreen.pcf: line 269, column 57
    function sortValue_105 (chargeSlicingOverridesView :  gw.admin.paymentplan.ChargeSlicingOverridesViewHelper) : java.lang.Object {
      return chargeSlicingOverridesView.BillingInstruction
    }
    
    // 'toRemove' attribute on RowIterator (id=ContextBillingInstructionTypeIterator) at PaymentPlanDetailScreen.pcf: line 260, column 101
    function toRemove_110 (chargeSlicingOverridesView :  gw.admin.paymentplan.ChargeSlicingOverridesViewHelper) : void {
      removeOverride(chargeSlicingOverridesView, biOverridesViewHelperList)
    }
    
    // 'value' attribute on AddMenuItemIterator at PaymentPlanDetailScreen.pcf: line 234, column 82
    function value_103 () : gw.util.IOrderedList<typekey.BillingInstruction> {
      return paymentPlan.getTypesThatCanHaveOverrides().order()
    }
    
    // 'value' attribute on RowIterator (id=ContextBillingInstructionTypeIterator) at PaymentPlanDetailScreen.pcf: line 260, column 101
    function value_111 () : java.util.List<gw.admin.paymentplan.ChargeSlicingOverridesViewHelper> {
      return biOverridesViewHelperList
    }
    
    property get biOverridesViewHelperList () : java.util.List<gw.admin.paymentplan.ChargeSlicingOverridesViewHelper> {
      return getVariableValue("biOverridesViewHelperList", 1) as java.util.List<gw.admin.paymentplan.ChargeSlicingOverridesViewHelper>
    }
    
    property set biOverridesViewHelperList ($arg :  java.util.List<gw.admin.paymentplan.ChargeSlicingOverridesViewHelper>) {
      setVariableValue("biOverridesViewHelperList", 1, $arg)
    }
    
    property get selectedOverridesViewHelper () : gw.admin.paymentplan.ChargeSlicingOverridesViewHelper {
      return getCurrentSelection(1) as gw.admin.paymentplan.ChargeSlicingOverridesViewHelper
    }
    
    property set selectedOverridesViewHelper ($arg :  gw.admin.paymentplan.ChargeSlicingOverridesViewHelper) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/admin/payment/PaymentPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentPlanDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=Clone) at PaymentPlanDetailScreen.pcf: line 36, column 67
    function action_6 () : void {
      ClonePaymentPlan.go(paymentPlan)
    }
    
    // 'action' attribute on ToolbarButton (id=Clone) at PaymentPlanDetailScreen.pcf: line 36, column 67
    function action_dest_7 () : pcf.api.Destination {
      return pcf.ClonePaymentPlan.createDestination(paymentPlan)
    }
    
    // 'def' attribute on PanelRef at PaymentPlanDetailScreen.pcf: line 327, column 41
    function def_onEnter_130 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(paymentPlan, { "Name", "Description" }, { DisplayKey.get("Web.PaymentPlanDetailDV.Name"), DisplayKey.get("Web.PaymentPlanDetailDV.Description") })
    }
    
    // 'def' attribute on InputSetRef at PaymentPlanDetailScreen.pcf: line 82, column 57
    function def_onEnter_31 (def :  pcf.PlanMultiCurrencyInputSet) : void {
      def.onEnter(paymentPlan)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at PaymentPlanDetailScreen.pcf: line 109, column 53
    function def_onEnter_45 (def :  pcf.InstallmentTreatmentInputSet_EveryTerm) : void {
      def.onEnter(paymentPlanHelper)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at PaymentPlanDetailScreen.pcf: line 109, column 53
    function def_onEnter_47 (def :  pcf.InstallmentTreatmentInputSet_FirstTermOnly) : void {
      def.onEnter(paymentPlanHelper)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at PaymentPlanDetailScreen.pcf: line 109, column 53
    function def_onEnter_49 (def :  pcf.InstallmentTreatmentInputSet_None) : void {
      def.onEnter(paymentPlanHelper)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentAlignment) at PaymentPlanDetailScreen.pcf: line 114, column 57
    function def_onEnter_54 (def :  pcf.InstallmentAlignmentInputSet) : void {
      def.onEnter(paymentPlan)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentFeesDefaults) at PaymentPlanDetailScreen.pcf: line 183, column 41
    function def_onEnter_80 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.onEnter(paymentPlan, PaymentPlan#InstallmentFeeDefaults.PropertyInfo, DisplayKey.get("Web.PaymentPlanDetailDV.FeeAmount"), false, true, null)
    }
    
    // 'def' attribute on PanelRef at PaymentPlanDetailScreen.pcf: line 327, column 41
    function def_refreshVariables_131 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(paymentPlan, { "Name", "Description" }, { DisplayKey.get("Web.PaymentPlanDetailDV.Name"), DisplayKey.get("Web.PaymentPlanDetailDV.Description") })
    }
    
    // 'def' attribute on InputSetRef at PaymentPlanDetailScreen.pcf: line 82, column 57
    function def_refreshVariables_32 (def :  pcf.PlanMultiCurrencyInputSet) : void {
      def.refreshVariables(paymentPlan)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at PaymentPlanDetailScreen.pcf: line 109, column 53
    function def_refreshVariables_46 (def :  pcf.InstallmentTreatmentInputSet_EveryTerm) : void {
      def.refreshVariables(paymentPlanHelper)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at PaymentPlanDetailScreen.pcf: line 109, column 53
    function def_refreshVariables_48 (def :  pcf.InstallmentTreatmentInputSet_FirstTermOnly) : void {
      def.refreshVariables(paymentPlanHelper)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentTreatment) at PaymentPlanDetailScreen.pcf: line 109, column 53
    function def_refreshVariables_50 (def :  pcf.InstallmentTreatmentInputSet_None) : void {
      def.refreshVariables(paymentPlanHelper)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentAlignment) at PaymentPlanDetailScreen.pcf: line 114, column 57
    function def_refreshVariables_55 (def :  pcf.InstallmentAlignmentInputSet) : void {
      def.refreshVariables(paymentPlan)
    }
    
    // 'def' attribute on InputSetRef (id=InstallmentFeesDefaults) at PaymentPlanDetailScreen.pcf: line 183, column 41
    function def_refreshVariables_81 (def :  pcf.PlanMultiCurrencyFeeThresholdInputSet) : void {
      def.refreshVariables(paymentPlan, PaymentPlan#InstallmentFeeDefaults.PropertyInfo, DisplayKey.get("Web.PaymentPlanDetailDV.FeeAmount"), false, true, null)
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at PaymentPlanDetailScreen.pcf: line 57, column 43
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at PaymentPlanDetailScreen.pcf: line 65, column 46
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.EffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at PaymentPlanDetailScreen.pcf: line 71, column 47
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.ExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on BooleanRadioInput (id=Reporting_Input) at PaymentPlanDetailScreen.pcf: line 77, column 41
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.Reporting = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TypeKeyInput (id=PaymentInterval_Input) at PaymentPlanDetailScreen.pcf: line 89, column 44
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.Periodicity = (__VALUE_TO_SET as typekey.Periodicity)
    }
    
    // 'value' attribute on RangeRadioInput (id=DownPaymentFrequency_Input) at PaymentPlanDetailScreen.pcf: line 102, column 65
    function defaultSetter_39 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlanHelper.FrequencyOfDownPayment = (__VALUE_TO_SET as gw.admin.paymentplan.DownPaymentFrequency)
    }
    
    // 'value' attribute on TypeKeyInput (id=BillDateOrDueDateBilling_Input) at PaymentPlanDetailScreen.pcf: line 132, column 59
    function defaultSetter_57 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.PolicyLevelBillingBillDateOrDueDateBilling = (__VALUE_TO_SET as typekey.BillDateOrDueDateBilling)
    }
    
    // 'value' attribute on TypeKeyInput (id=InvoiceItemPlacementCutoffType_Input) at PaymentPlanDetailScreen.pcf: line 150, column 65
    function defaultSetter_62 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.AccountLevelBillingInvoiceItemPlacementCutoffType = (__VALUE_TO_SET as typekey.InvoiceItemPlacementCutoffType)
    }
    
    // 'value' attribute on TypeKeyInput (id=InvoicingBlackoutType_Input) at PaymentPlanDetailScreen.pcf: line 170, column 54
    function defaultSetter_73 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.InvoicingBlackoutType = (__VALUE_TO_SET as typekey.InvoicingBlackoutType)
    }
    
    // 'value' attribute on TextInput (id=LastInvoiceByDaysBeforePolicyExpiration_Input) at PaymentPlanDetailScreen.pcf: line 177, column 42
    function defaultSetter_77 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.DaysBeforePolicyExpirationForInvoicingBlackout = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SkipFeeForDownPayment_Input) at PaymentPlanDetailScreen.pcf: line 189, column 54
    function defaultSetter_83 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.SkipFeeForDownPayment = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=EnableEquityWarnings_Input) at PaymentPlanDetailScreen.pcf: line 200, column 115
    function defaultSetter_89 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.EquityWarningsEnabled = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at PaymentPlanDetailScreen.pcf: line 50, column 36
    function defaultSetter_9 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=EquityBuffer_Input) at PaymentPlanDetailScreen.pcf: line 210, column 127
    function defaultSetter_95 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentPlan.EquityBuffer = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'editable' attribute on PanelRef at PaymentPlanDetailScreen.pcf: line 327, column 41
    function editable_129 () : java.lang.Boolean {
      return not paymentPlan.InUse
    }
    
    // 'editable' attribute on InputSetRef (id=InstallmentAlignment) at PaymentPlanDetailScreen.pcf: line 114, column 57
    function editable_52 () : java.lang.Boolean {
      return paymentPlan.UpgradedFromBefore9
    }
    
    // 'initialValue' attribute on Variable at PaymentPlanDetailScreen.pcf: line 16, column 23
    function initialValue_0 () : boolean {
      return gw.api.util.LocaleUtil.getAllLanguages().Count > 1
    }
    
    // 'initialValue' attribute on Variable at PaymentPlanDetailScreen.pcf: line 20, column 58
    function initialValue_1 () : gw.admin.paymentplan.PaymentPlanViewHelper {
      return new gw.admin.paymentplan.PaymentPlanViewHelper(paymentPlan, isClone?:false)
    }
    
    // 'initialValue' attribute on Variable at PaymentPlanDetailScreen.pcf: line 24, column 23
    function initialValue_2 () : Boolean {
      return gw.api.web.plan.PaymentPlans.areThereVisiblePaymentPlansBefore9()
    }
    
    // 'initialValue' attribute on Variable at PaymentPlanDetailScreen.pcf: line 28, column 45
    function initialValue_3 () : gw.pl.currency.MonetaryAmount {
      return paymentPlan.getInstallmentFee(paymentPlan.getCurrencies()[0])
    }
    
    // EditButtons at PaymentPlanDetailScreen.pcf: line 30, column 21
    function label_4 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'mode' attribute on InputSetRef (id=InstallmentTreatment) at PaymentPlanDetailScreen.pcf: line 109, column 53
    function mode_51 () : java.lang.Object {
      return paymentPlanHelper.DownPaymentMode
    }
    
    // 'onChange' attribute on PostOnChange at PaymentPlanDetailScreen.pcf: line 79, column 69
    function onChange_26 () : void {
      setVisibilityForEnableEquityWarningsField()
    }
    
    // 'onChange' attribute on PostOnChange at PaymentPlanDetailScreen.pcf: line 104, column 74
    function onChange_37 () : void {
      paymentPlanHelper.onDownPaymentFrequencyChange()
    }
    
    // 'validationExpression' attribute on DateInput (id=ExpirationDate_Input) at PaymentPlanDetailScreen.pcf: line 71, column 47
    function validationExpression_20 () : java.lang.Object {
      return paymentPlan.hasValidExpirationDate() ? null : DisplayKey.get("Web.Plan.ExpirationDate.ValidationError")
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DownPaymentFrequency_Input) at PaymentPlanDetailScreen.pcf: line 102, column 65
    function valueRange_41 () : java.lang.Object {
      return gw.admin.paymentplan.DownPaymentFrequency.getValuesForDetailsScreen()
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at PaymentPlanDetailScreen.pcf: line 50, column 36
    function valueRoot_10 () : java.lang.Object {
      return paymentPlan
    }
    
    // 'value' attribute on RangeRadioInput (id=DownPaymentFrequency_Input) at PaymentPlanDetailScreen.pcf: line 102, column 65
    function valueRoot_40 () : java.lang.Object {
      return paymentPlanHelper
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at PaymentPlanDetailScreen.pcf: line 57, column 43
    function value_12 () : java.lang.String {
      return paymentPlan.Description
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at PaymentPlanDetailScreen.pcf: line 65, column 46
    function value_16 () : java.util.Date {
      return paymentPlan.EffectiveDate
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at PaymentPlanDetailScreen.pcf: line 71, column 47
    function value_21 () : java.util.Date {
      return paymentPlan.ExpirationDate
    }
    
    // 'value' attribute on BooleanRadioInput (id=Reporting_Input) at PaymentPlanDetailScreen.pcf: line 77, column 41
    function value_27 () : java.lang.Boolean {
      return paymentPlan.Reporting
    }
    
    // 'value' attribute on TypeKeyInput (id=PaymentInterval_Input) at PaymentPlanDetailScreen.pcf: line 89, column 44
    function value_33 () : typekey.Periodicity {
      return paymentPlan.Periodicity
    }
    
    // 'value' attribute on RangeRadioInput (id=DownPaymentFrequency_Input) at PaymentPlanDetailScreen.pcf: line 102, column 65
    function value_38 () : gw.admin.paymentplan.DownPaymentFrequency {
      return paymentPlanHelper.FrequencyOfDownPayment
    }
    
    // 'value' attribute on TypeKeyInput (id=BillDateOrDueDateBilling_Input) at PaymentPlanDetailScreen.pcf: line 132, column 59
    function value_56 () : typekey.BillDateOrDueDateBilling {
      return paymentPlan.PolicyLevelBillingBillDateOrDueDateBilling
    }
    
    // 'value' attribute on TypeKeyInput (id=InvoiceItemPlacementCutoffType_Input) at PaymentPlanDetailScreen.pcf: line 150, column 65
    function value_61 () : typekey.InvoiceItemPlacementCutoffType {
      return paymentPlan.AccountLevelBillingInvoiceItemPlacementCutoffType
    }
    
    // 'value' attribute on TypeKeyInput (id=InvoicingBlackoutType_Input) at PaymentPlanDetailScreen.pcf: line 170, column 54
    function value_72 () : typekey.InvoicingBlackoutType {
      return paymentPlan.InvoicingBlackoutType
    }
    
    // 'value' attribute on TextInput (id=LastInvoiceByDaysBeforePolicyExpiration_Input) at PaymentPlanDetailScreen.pcf: line 177, column 42
    function value_76 () : java.lang.Integer {
      return paymentPlan.DaysBeforePolicyExpirationForInvoicingBlackout
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at PaymentPlanDetailScreen.pcf: line 50, column 36
    function value_8 () : java.lang.String {
      return paymentPlan.Name
    }
    
    // 'value' attribute on BooleanRadioInput (id=SkipFeeForDownPayment_Input) at PaymentPlanDetailScreen.pcf: line 189, column 54
    function value_82 () : java.lang.Boolean {
      return paymentPlan.SkipFeeForDownPayment
    }
    
    // 'value' attribute on BooleanRadioInput (id=EnableEquityWarnings_Input) at PaymentPlanDetailScreen.pcf: line 200, column 115
    function value_88 () : java.lang.Boolean {
      return paymentPlan.EquityWarningsEnabled
    }
    
    // 'value' attribute on TextInput (id=EquityBuffer_Input) at PaymentPlanDetailScreen.pcf: line 210, column 127
    function value_94 () : java.lang.Integer {
      return paymentPlan.EquityBuffer
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DownPaymentFrequency_Input) at PaymentPlanDetailScreen.pcf: line 102, column 65
    function verifyValueRangeIsAllowedType_42 ($$arg :  gw.admin.paymentplan.DownPaymentFrequency[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DownPaymentFrequency_Input) at PaymentPlanDetailScreen.pcf: line 102, column 65
    function verifyValueRangeIsAllowedType_42 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=DownPaymentFrequency_Input) at PaymentPlanDetailScreen.pcf: line 102, column 65
    function verifyValueRange_43 () : void {
      var __valueRangeArg = gw.admin.paymentplan.DownPaymentFrequency.getValuesForDetailsScreen()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_42(__valueRangeArg)
    }
    
    // 'visible' attribute on ToolbarButton (id=Clone) at PaymentPlanDetailScreen.pcf: line 36, column 67
    function visible_5 () : java.lang.Boolean {
      return perm.System.pmntplancreate and isClone != null
    }
    
    // 'visible' attribute on InputSetRef (id=InstallmentAlignment) at PaymentPlanDetailScreen.pcf: line 114, column 57
    function visible_53 () : java.lang.Boolean {
      return areThereVisiblePaymentPlansBefore9
    }
    
    // 'visible' attribute on InputGroup (id=PolicyLevelBillingInputGroup) at PaymentPlanDetailScreen.pcf: line 125, column 61
    function visible_60 () : java.lang.Boolean {
      return paymentPlan.AlignInstallmentsToInvoices
    }
    
    // 'visible' attribute on TypeKeyInput (id=LegacyInvoiceItemPlacementCutoffType_Input) at PaymentPlanDetailScreen.pcf: line 160, column 66
    function visible_66 () : java.lang.Boolean {
      return not paymentPlan.AlignInstallmentsToInvoices
    }
    
    // 'visible' attribute on Label at PaymentPlanDetailScreen.pcf: line 193, column 116
    function visible_86 () : java.lang.Boolean {
      return gw.api.system.BCConfigParameters.EnableEquityWarnings.Value && !paymentPlan.Reporting
    }
    
    // 'visible' attribute on TextInput (id=EquityBuffer_Input) at PaymentPlanDetailScreen.pcf: line 210, column 127
    function visible_93 () : java.lang.Boolean {
      return gw.api.system.BCConfigParameters.EnableEquityWarnings.Value && paymentPlan.EquityWarningsEnabled
    }
    
    property get areThereVisiblePaymentPlansBefore9 () : Boolean {
      return getVariableValue("areThereVisiblePaymentPlansBefore9", 0) as Boolean
    }
    
    property set areThereVisiblePaymentPlansBefore9 ($arg :  Boolean) {
      setVariableValue("areThereVisiblePaymentPlansBefore9", 0, $arg)
    }
    
    property get hasMultipleLanguages () : boolean {
      return getVariableValue("hasMultipleLanguages", 0) as java.lang.Boolean
    }
    
    property set hasMultipleLanguages ($arg :  boolean) {
      setVariableValue("hasMultipleLanguages", 0, $arg)
    }
    
    property get installmentFee () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("installmentFee", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set installmentFee ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("installmentFee", 0, $arg)
    }
    
    property get isClone () : Boolean {
      return getRequireValue("isClone", 0) as Boolean
    }
    
    property set isClone ($arg :  Boolean) {
      setRequireValue("isClone", 0, $arg)
    }
    
    property get paymentPlan () : PaymentPlan {
      return getRequireValue("paymentPlan", 0) as PaymentPlan
    }
    
    property set paymentPlan ($arg :  PaymentPlan) {
      setRequireValue("paymentPlan", 0, $arg)
    }
    
    property get paymentPlanHelper () : gw.admin.paymentplan.PaymentPlanViewHelper {
      return getVariableValue("paymentPlanHelper", 0) as gw.admin.paymentplan.PaymentPlanViewHelper
    }
    
    property set paymentPlanHelper ($arg :  gw.admin.paymentplan.PaymentPlanViewHelper) {
      setVariableValue("paymentPlanHelper", 0, $arg)
    }
    
    
    function addOverrides(billingInstructionType: typekey.BillingInstruction, biOverridesViewHelperList : List<ChargeSlicingOverridesViewHelper>) : ChargeSlicingOverridesViewHelper {
      var overrides = new ChargeSlicingOverrides();
      paymentPlan.setOverridesFor(billingInstructionType, overrides)
      var overrideViewHelper = new gw.admin.paymentplan.ChargeSlicingOverridesViewHelper(billingInstructionType, overrides, paymentPlan);
      biOverridesViewHelperList.add(overrideViewHelper)
      return overrideViewHelper
    }
    
    function removeOverride(overrideViewHelper : ChargeSlicingOverridesViewHelper, biOverridesViewHelperList : List<ChargeSlicingOverridesViewHelper>){
      paymentPlan.removeOverridesFor(overrideViewHelper.BillingInstruction)
      biOverridesViewHelperList.remove(overrideViewHelper)
    }
    
    function getChargeSlicingOverridesForUI(): java.util.List<ChargeSlicingOverridesViewHelper> {
      var chargeSlicingOverrides = new ArrayList<ChargeSlicingOverridesViewHelper>()
    
    
      var billingInstructions = paymentPlan.getTypesThatCanHaveOverrides()
          .where(\billingInstructionType -> paymentPlan.getOverridesFor(billingInstructionType) != null)
    
      for (bi in billingInstructions) {
        chargeSlicingOverrides.add(new ChargeSlicingOverridesViewHelper(bi, paymentPlan.getOverridesFor(bi), paymentPlan, isClone))
      }
      return chargeSlicingOverrides
    }
    
    function setVisibilityForEnableEquityWarningsField() {
      if (paymentPlan.Reporting) {
        paymentPlan.EquityWarningsEnabled = false;
      }
    }
    
    
  }
  
  
}