package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/CommissionPayableReductionWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CommissionPayableReductionWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/CommissionPayableReductionWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CommissionPayableReductionWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producerID :  gw.pl.persistence.core.Key) : int {
      return 0
    }
    
    // 'allowNext' attribute on WizardStep (id=PolicySearchStep) at CommissionPayableReductionWizard.pcf: line 32, column 100
    function allowNext_2 () : java.lang.Boolean {
      return tAccountOwnerReference.TAccountOwner != null
    }
    
    // 'initialValue' attribute on Variable at CommissionPayableReductionWizard.pcf: line 15, column 60
    function initialValue_0 () : gw.api.web.accounting.TAccountOwnerReference {
      return new gw.api.web.accounting.TAccountOwnerReference()
    }
    
    // 'initialValue' attribute on Variable at CommissionPayableReductionWizard.pcf: line 25, column 35
    function initialValue_1 () : List<Boolean> {
      return {false}
    }
    
    // 'onExit' attribute on WizardStep (id=PolicySearchStep) at CommissionPayableReductionWizard.pcf: line 32, column 100
    function onExit_3 () : void {
      chargeCommissions = (tAccountOwnerReference.TAccountOwner as PolicyPeriod).getCommissionableCharges(); currency = (tAccountOwnerReference.TAccountOwner as PolicyPeriod).Account.Currency
    }
    
    // 'screen' attribute on WizardStep (id=PolicySearchStep) at CommissionPayableReductionWizard.pcf: line 32, column 100
    function screen_onEnter_4 (def :  pcf.CommissionReductionWizardPolicySearchScreen) : void {
      def.onEnter(tAccountOwnerReference)
    }
    
    // 'screen' attribute on WizardStep (id=TargetStep) at CommissionPayableReductionWizard.pcf: line 37, column 95
    function screen_onEnter_6 (def :  pcf.CommissionPayableReductionWizardTargetsStepScreen) : void {
      def.onEnter(chargeCommissions, approvalRequiredForWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at CommissionPayableReductionWizard.pcf: line 43, column 100
    function screen_onEnter_8 (def :  pcf.CommissionPayableReductionWizardConfirmationStepScreen) : void {
      def.onEnter(chargeCommissions, currency, approvalRequiredForWriteoff.first())
    }
    
    // 'screen' attribute on WizardStep (id=PolicySearchStep) at CommissionPayableReductionWizard.pcf: line 32, column 100
    function screen_refreshVariables_5 (def :  pcf.CommissionReductionWizardPolicySearchScreen) : void {
      def.refreshVariables(tAccountOwnerReference)
    }
    
    // 'screen' attribute on WizardStep (id=TargetStep) at CommissionPayableReductionWizard.pcf: line 37, column 95
    function screen_refreshVariables_7 (def :  pcf.CommissionPayableReductionWizardTargetsStepScreen) : void {
      def.refreshVariables(chargeCommissions, approvalRequiredForWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at CommissionPayableReductionWizard.pcf: line 43, column 100
    function screen_refreshVariables_9 (def :  pcf.CommissionPayableReductionWizardConfirmationStepScreen) : void {
      def.refreshVariables(chargeCommissions, currency, approvalRequiredForWriteoff.first())
    }
    
    // 'tabBar' attribute on Wizard (id=CommissionPayableReductionWizard) at CommissionPayableReductionWizard.pcf: line 6, column 43
    function tabBar_onEnter_10 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=CommissionPayableReductionWizard) at CommissionPayableReductionWizard.pcf: line 6, column 43
    function tabBar_refreshVariables_11 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.CommissionPayableReductionWizard {
      return super.CurrentLocation as pcf.CommissionPayableReductionWizard
    }
    
    property get approvalRequiredForWriteoff () : List<Boolean> {
      return getVariableValue("approvalRequiredForWriteoff", 0) as List<Boolean>
    }
    
    property set approvalRequiredForWriteoff ($arg :  List<Boolean>) {
      setVariableValue("approvalRequiredForWriteoff", 0, $arg)
    }
    
    property get chargeCommissions () : ChargeCommission[] {
      return getVariableValue("chargeCommissions", 0) as ChargeCommission[]
    }
    
    property set chargeCommissions ($arg :  ChargeCommission[]) {
      setVariableValue("chargeCommissions", 0, $arg)
    }
    
    property get currency () : Currency {
      return getVariableValue("currency", 0) as Currency
    }
    
    property set currency ($arg :  Currency) {
      setVariableValue("currency", 0, $arg)
    }
    
    property get producerID () : gw.pl.persistence.core.Key {
      return getVariableValue("producerID", 0) as gw.pl.persistence.core.Key
    }
    
    property set producerID ($arg :  gw.pl.persistence.core.Key) {
      setVariableValue("producerID", 0, $arg)
    }
    
    property get tAccountOwnerReference () : gw.api.web.accounting.TAccountOwnerReference {
      return getVariableValue("tAccountOwnerReference", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set tAccountOwnerReference ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setVariableValue("tAccountOwnerReference", 0, $arg)
    }
    
    
  }
  
  
}