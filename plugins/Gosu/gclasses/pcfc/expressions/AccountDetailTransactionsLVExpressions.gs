package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailTransactionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailTransactionsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailTransactionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailTransactionsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at AccountDetailTransactionsLV.pcf: line 14, column 59
    function initialValue_0 () : gw.api.web.transaction.TransactionFilterSet {
      return new gw.api.web.transaction.TransactionFilterSet()
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AccountDetailTransactionsLV.pcf: line 25, column 40
    function sortValue_1 (trx :  entity.Transaction) : java.lang.Object {
      return trx.TransactionDate
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at AccountDetailTransactionsLV.pcf: line 32, column 25
    function sortValue_2 (trx :  entity.Transaction) : java.lang.Object {
      return trx.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=TransactionDesc_Cell) at AccountDetailTransactionsLV.pcf: line 37, column 36
    function sortValue_3 (trx :  entity.Transaction) : java.lang.Object {
      return trx.DisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AccountDetailTransactionsLV.pcf: line 42, column 31
    function sortValue_4 (trx :  entity.Transaction) : java.lang.Object {
      return trx.Amount
    }
    
    // 'value' attribute on RowIterator (id=TransactionIterator) at AccountDetailTransactionsLV.pcf: line 20, column 40
    function value_20 () : entity.Transaction[] {
      return transactions
    }
    
    property get filterset () : gw.api.web.transaction.TransactionFilterSet {
      return getVariableValue("filterset", 0) as gw.api.web.transaction.TransactionFilterSet
    }
    
    property set filterset ($arg :  gw.api.web.transaction.TransactionFilterSet) {
      setVariableValue("filterset", 0, $arg)
    }
    
    property get transactions () : Transaction[] {
      return getRequireValue("transactions", 0) as Transaction[]
    }
    
    property set transactions ($arg :  Transaction[]) {
      setRequireValue("transactions", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailTransactionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountDetailTransactionsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at AccountDetailTransactionsLV.pcf: line 32, column 25
    function action_8 () : void {
      TransactionDetailPopup.push(trx)
    }
    
    // 'action' attribute on TextCell (id=TransactionNumber_Cell) at AccountDetailTransactionsLV.pcf: line 32, column 25
    function action_dest_9 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(trx)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at AccountDetailTransactionsLV.pcf: line 42, column 31
    function currency_18 () : typekey.Currency {
      return trx.Currency
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AccountDetailTransactionsLV.pcf: line 25, column 40
    function valueRoot_6 () : java.lang.Object {
      return trx
    }
    
    // 'value' attribute on TextCell (id=TransactionNumber_Cell) at AccountDetailTransactionsLV.pcf: line 32, column 25
    function value_10 () : java.lang.String {
      return trx.TransactionNumber
    }
    
    // 'value' attribute on TextCell (id=TransactionDesc_Cell) at AccountDetailTransactionsLV.pcf: line 37, column 36
    function value_13 () : java.lang.String {
      return trx.DisplayName
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AccountDetailTransactionsLV.pcf: line 42, column 31
    function value_16 () : gw.pl.currency.MonetaryAmount {
      return trx.Amount
    }
    
    // 'value' attribute on DateCell (id=Date_Cell) at AccountDetailTransactionsLV.pcf: line 25, column 40
    function value_5 () : java.util.Date {
      return trx.TransactionDate
    }
    
    property get trx () : entity.Transaction {
      return getIteratedValue(1) as entity.Transaction
    }
    
    
  }
  
  
}