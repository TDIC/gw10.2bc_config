package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadBillingPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanDataUploadBillingPlanLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadBillingPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PlanDataUploadBillingPlanLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at PlanDataUploadBillingPlanLV.pcf: line 17, column 102
    function highlighted_183 () : java.lang.Boolean {
      return (billingPlan.Error or billingPlan.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=paymentReversalFee_Cell) at PlanDataUploadBillingPlanLV.pcf: line 75, column 45
    function label_103 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.PaymentReversalFee")
    }
    
    // 'label' attribute on BooleanRadioCell (id=skipInstallmentFees_Cell) at PlanDataUploadBillingPlanLV.pcf: line 80, column 42
    function label_108 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.SkipInstallmentFees")
    }
    
    // 'label' attribute on TypeKeyCell (id=lineItemsShow_Cell) at PlanDataUploadBillingPlanLV.pcf: line 85, column 48
    function label_113 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.LineItemsShow")
    }
    
    // 'label' attribute on BooleanRadioCell (id=SuppressLowBalInvoices_Cell) at PlanDataUploadBillingPlanLV.pcf: line 90, column 42
    function label_118 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.SuppressLowBalanceInvoices")
    }
    
    // 'label' attribute on TextCell (id=lowBalanceThreshold_Cell) at PlanDataUploadBillingPlanLV.pcf: line 95, column 45
    function label_123 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.LowBalanceThreshold")
    }
    
    // 'label' attribute on TypeKeyCell (id=lowBalanceMethod_Cell) at PlanDataUploadBillingPlanLV.pcf: line 100, column 49
    function label_128 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.LowBalanceMethod")
    }
    
    // 'label' attribute on TextCell (id=reviewDisbursementsOver_Cell) at PlanDataUploadBillingPlanLV.pcf: line 105, column 45
    function label_133 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.ReviewDisbursementsOver")
    }
    
    // 'label' attribute on TextCell (id=delayDisbursement_Cell) at PlanDataUploadBillingPlanLV.pcf: line 110, column 42
    function label_138 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.DelayDisbursements")
    }
    
    // 'label' attribute on TextCell (id=automaticDisbursementOver_Cell) at PlanDataUploadBillingPlanLV.pcf: line 115, column 45
    function label_143 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.AutomaticDisbursementsOver")
    }
    
    // 'label' attribute on TypeKeyCell (id=availableDisbAmtType_Cell) at PlanDataUploadBillingPlanLV.pcf: line 120, column 53
    function label_148 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.AvailableDisbursementAmountType")
    }
    
    // 'label' attribute on BooleanRadioCell (id=createApprActForAutoDisb_Cell) at PlanDataUploadBillingPlanLV.pcf: line 125, column 42
    function label_153 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.CreateApprovalActivityForAutoDisbursement")
    }
    
    // 'label' attribute on BooleanRadioCell (id=sendAutoDisbAwaitingApproval_Cell) at PlanDataUploadBillingPlanLV.pcf: line 130, column 42
    function label_158 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.SendAutoDisbursementAwaitingApproval")
    }
    
    // 'label' attribute on TextCell (id=requestInterval_Cell) at PlanDataUploadBillingPlanLV.pcf: line 135, column 42
    function label_163 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.RequestInterval")
    }
    
    // 'label' attribute on TextCell (id=changeDeadlineInterval_Cell) at PlanDataUploadBillingPlanLV.pcf: line 140, column 42
    function label_168 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.ChangeDeadlineInterval")
    }
    
    // 'label' attribute on TextCell (id=draftInterval_Cell) at PlanDataUploadBillingPlanLV.pcf: line 145, column 42
    function label_173 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.DraftInterval")
    }
    
    // 'label' attribute on TypeKeyCell (id=draftDayLogic_Cell) at PlanDataUploadBillingPlanLV.pcf: line 150, column 48
    function label_178 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.FixDraftDayOn")
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadBillingPlanLV.pcf: line 23, column 46
    function label_53 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at PlanDataUploadBillingPlanLV.pcf: line 28, column 41
    function label_58 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.PublicID")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadBillingPlanLV.pcf: line 34, column 41
    function label_63 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.PlanName")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at PlanDataUploadBillingPlanLV.pcf: line 39, column 41
    function label_68 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.Description")
    }
    
    // 'label' attribute on TextCell (id=leadTime_Cell) at PlanDataUploadBillingPlanLV.pcf: line 45, column 42
    function label_73 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.LeadTime")
    }
    
    // 'label' attribute on TextCell (id=nonResponsiveLeadTime_Cell) at PlanDataUploadBillingPlanLV.pcf: line 50, column 42
    function label_78 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.NonResponsiveLeadTime")
    }
    
    // 'label' attribute on TypeKeyCell (id=fixPaymentDueDateOn_Cell) at PlanDataUploadBillingPlanLV.pcf: line 55, column 48
    function label_83 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.FixPaymentDueDateOn")
    }
    
    // 'label' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadBillingPlanLV.pcf: line 60, column 39
    function label_88 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.EffectiveDate")
    }
    
    // 'label' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadBillingPlanLV.pcf: line 65, column 39
    function label_93 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.ExpirationDate")
    }
    
    // 'label' attribute on TextCell (id=invoiceFee_Cell) at PlanDataUploadBillingPlanLV.pcf: line 70, column 45
    function label_98 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.InvoiceFee")
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadBillingPlanLV.pcf: line 28, column 41
    function valueRoot_60 () : java.lang.Object {
      return billingPlan
    }
    
    // 'value' attribute on TextCell (id=paymentReversalFee_Cell) at PlanDataUploadBillingPlanLV.pcf: line 75, column 45
    function value_104 () : java.math.BigDecimal {
      return billingPlan.PaymentReversalFee
    }
    
    // 'value' attribute on BooleanRadioCell (id=skipInstallmentFees_Cell) at PlanDataUploadBillingPlanLV.pcf: line 80, column 42
    function value_109 () : java.lang.Boolean {
      return billingPlan.SkipInstallmentFees
    }
    
    // 'value' attribute on TypeKeyCell (id=lineItemsShow_Cell) at PlanDataUploadBillingPlanLV.pcf: line 85, column 48
    function value_114 () : typekey.AggregationType {
      return billingPlan.LineItemsShow
    }
    
    // 'value' attribute on BooleanRadioCell (id=SuppressLowBalInvoices_Cell) at PlanDataUploadBillingPlanLV.pcf: line 90, column 42
    function value_119 () : java.lang.Boolean {
      return billingPlan.SuppressLowBalInvoices
    }
    
    // 'value' attribute on TextCell (id=lowBalanceThreshold_Cell) at PlanDataUploadBillingPlanLV.pcf: line 95, column 45
    function value_124 () : java.math.BigDecimal {
      return billingPlan.LowBalanceThreshold
    }
    
    // 'value' attribute on TypeKeyCell (id=lowBalanceMethod_Cell) at PlanDataUploadBillingPlanLV.pcf: line 100, column 49
    function value_129 () : typekey.LowBalanceMethod {
      return billingPlan.LowBalanceMethod
    }
    
    // 'value' attribute on TextCell (id=reviewDisbursementsOver_Cell) at PlanDataUploadBillingPlanLV.pcf: line 105, column 45
    function value_134 () : java.math.BigDecimal {
      return billingPlan.ReviewDisbursementsOver
    }
    
    // 'value' attribute on TextCell (id=delayDisbursement_Cell) at PlanDataUploadBillingPlanLV.pcf: line 110, column 42
    function value_139 () : java.lang.Integer {
      return billingPlan.DelayDisbursement
    }
    
    // 'value' attribute on TextCell (id=automaticDisbursementOver_Cell) at PlanDataUploadBillingPlanLV.pcf: line 115, column 45
    function value_144 () : java.math.BigDecimal {
      return billingPlan.AutomaticDisbursementOver
    }
    
    // 'value' attribute on TypeKeyCell (id=availableDisbAmtType_Cell) at PlanDataUploadBillingPlanLV.pcf: line 120, column 53
    function value_149 () : typekey.AvailableDisbAmtType {
      return billingPlan.AvailableDisbAmtType
    }
    
    // 'value' attribute on BooleanRadioCell (id=createApprActForAutoDisb_Cell) at PlanDataUploadBillingPlanLV.pcf: line 125, column 42
    function value_154 () : java.lang.Boolean {
      return billingPlan.CreateApprActForAutoDisb
    }
    
    // 'value' attribute on BooleanRadioCell (id=sendAutoDisbAwaitingApproval_Cell) at PlanDataUploadBillingPlanLV.pcf: line 130, column 42
    function value_159 () : java.lang.Boolean {
      return billingPlan.SendAutoDisbAwaitingApproval
    }
    
    // 'value' attribute on TextCell (id=requestInterval_Cell) at PlanDataUploadBillingPlanLV.pcf: line 135, column 42
    function value_164 () : java.lang.Integer {
      return billingPlan.RequestInterval
    }
    
    // 'value' attribute on TextCell (id=changeDeadlineInterval_Cell) at PlanDataUploadBillingPlanLV.pcf: line 140, column 42
    function value_169 () : java.lang.Integer {
      return billingPlan.ChangeDeadlineInterval
    }
    
    // 'value' attribute on TextCell (id=draftInterval_Cell) at PlanDataUploadBillingPlanLV.pcf: line 145, column 42
    function value_174 () : java.lang.Integer {
      return billingPlan.DraftInterval
    }
    
    // 'value' attribute on TypeKeyCell (id=draftDayLogic_Cell) at PlanDataUploadBillingPlanLV.pcf: line 150, column 48
    function value_179 () : typekey.DayOfMonthLogic {
      return billingPlan.DraftDayLogic
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadBillingPlanLV.pcf: line 23, column 46
    function value_54 () : java.lang.String {
      return processor.getLoadStatus(billingPlan)
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadBillingPlanLV.pcf: line 28, column 41
    function value_59 () : java.lang.String {
      return billingPlan.PublicID
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadBillingPlanLV.pcf: line 34, column 41
    function value_64 () : java.lang.String {
      return billingPlan.Name
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at PlanDataUploadBillingPlanLV.pcf: line 39, column 41
    function value_69 () : java.lang.String {
      return billingPlan.Description
    }
    
    // 'value' attribute on TextCell (id=leadTime_Cell) at PlanDataUploadBillingPlanLV.pcf: line 45, column 42
    function value_74 () : java.lang.Integer {
      return billingPlan.LeadTime
    }
    
    // 'value' attribute on TextCell (id=nonResponsiveLeadTime_Cell) at PlanDataUploadBillingPlanLV.pcf: line 50, column 42
    function value_79 () : java.lang.Integer {
      return billingPlan.NonResponsiveLeadTime
    }
    
    // 'value' attribute on TypeKeyCell (id=fixPaymentDueDateOn_Cell) at PlanDataUploadBillingPlanLV.pcf: line 55, column 48
    function value_84 () : typekey.DayOfMonthLogic {
      return billingPlan.FixPaymentDueDateOn
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadBillingPlanLV.pcf: line 60, column 39
    function value_89 () : java.util.Date {
      return billingPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadBillingPlanLV.pcf: line 65, column 39
    function value_94 () : java.util.Date {
      return billingPlan.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=invoiceFee_Cell) at PlanDataUploadBillingPlanLV.pcf: line 70, column 45
    function value_99 () : java.math.BigDecimal {
      return billingPlan.InvoiceFee
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadBillingPlanLV.pcf: line 23, column 46
    function visible_55 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get billingPlan () : tdic.util.dataloader.data.plandata.BillingPlanData {
      return getIteratedValue(1) as tdic.util.dataloader.data.plandata.BillingPlanData
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadBillingPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanDataUploadBillingPlanLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadBillingPlanLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=nonResponsiveLeadTime_Cell) at PlanDataUploadBillingPlanLV.pcf: line 50, column 42
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.NonResponsiveLeadTime")
    }
    
    // 'label' attribute on TypeKeyCell (id=fixPaymentDueDateOn_Cell) at PlanDataUploadBillingPlanLV.pcf: line 55, column 48
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.FixPaymentDueDateOn")
    }
    
    // 'label' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadBillingPlanLV.pcf: line 60, column 39
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.EffectiveDate")
    }
    
    // 'label' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadBillingPlanLV.pcf: line 65, column 39
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.ExpirationDate")
    }
    
    // 'label' attribute on TextCell (id=invoiceFee_Cell) at PlanDataUploadBillingPlanLV.pcf: line 70, column 45
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.InvoiceFee")
    }
    
    // 'label' attribute on TextCell (id=paymentReversalFee_Cell) at PlanDataUploadBillingPlanLV.pcf: line 75, column 45
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.PaymentReversalFee")
    }
    
    // 'label' attribute on BooleanRadioCell (id=skipInstallmentFees_Cell) at PlanDataUploadBillingPlanLV.pcf: line 80, column 42
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.SkipInstallmentFees")
    }
    
    // 'label' attribute on TypeKeyCell (id=lineItemsShow_Cell) at PlanDataUploadBillingPlanLV.pcf: line 85, column 48
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.LineItemsShow")
    }
    
    // 'label' attribute on BooleanRadioCell (id=SuppressLowBalInvoices_Cell) at PlanDataUploadBillingPlanLV.pcf: line 90, column 42
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.SuppressLowBalanceInvoices")
    }
    
    // 'label' attribute on TextCell (id=lowBalanceThreshold_Cell) at PlanDataUploadBillingPlanLV.pcf: line 95, column 45
    function label_29 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.LowBalanceThreshold")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at PlanDataUploadBillingPlanLV.pcf: line 28, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.PublicID")
    }
    
    // 'label' attribute on TypeKeyCell (id=lowBalanceMethod_Cell) at PlanDataUploadBillingPlanLV.pcf: line 100, column 49
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.LowBalanceMethod")
    }
    
    // 'label' attribute on TextCell (id=reviewDisbursementsOver_Cell) at PlanDataUploadBillingPlanLV.pcf: line 105, column 45
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.ReviewDisbursementsOver")
    }
    
    // 'label' attribute on TextCell (id=delayDisbursement_Cell) at PlanDataUploadBillingPlanLV.pcf: line 110, column 42
    function label_35 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.DelayDisbursements")
    }
    
    // 'label' attribute on TextCell (id=automaticDisbursementOver_Cell) at PlanDataUploadBillingPlanLV.pcf: line 115, column 45
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.AutomaticDisbursementsOver")
    }
    
    // 'label' attribute on TypeKeyCell (id=availableDisbAmtType_Cell) at PlanDataUploadBillingPlanLV.pcf: line 120, column 53
    function label_39 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.AvailableDisbursementAmountType")
    }
    
    // 'label' attribute on BooleanRadioCell (id=createApprActForAutoDisb_Cell) at PlanDataUploadBillingPlanLV.pcf: line 125, column 42
    function label_41 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.CreateApprovalActivityForAutoDisbursement")
    }
    
    // 'label' attribute on BooleanRadioCell (id=sendAutoDisbAwaitingApproval_Cell) at PlanDataUploadBillingPlanLV.pcf: line 130, column 42
    function label_43 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.SendAutoDisbursementAwaitingApproval")
    }
    
    // 'label' attribute on TextCell (id=requestInterval_Cell) at PlanDataUploadBillingPlanLV.pcf: line 135, column 42
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.RequestInterval")
    }
    
    // 'label' attribute on TextCell (id=changeDeadlineInterval_Cell) at PlanDataUploadBillingPlanLV.pcf: line 140, column 42
    function label_47 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.ChangeDeadlineInterval")
    }
    
    // 'label' attribute on TextCell (id=draftInterval_Cell) at PlanDataUploadBillingPlanLV.pcf: line 145, column 42
    function label_49 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.DraftInterval")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadBillingPlanLV.pcf: line 34, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.PlanName")
    }
    
    // 'label' attribute on TypeKeyCell (id=draftDayLogic_Cell) at PlanDataUploadBillingPlanLV.pcf: line 150, column 48
    function label_51 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.FixDraftDayOn")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at PlanDataUploadBillingPlanLV.pcf: line 39, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.Description")
    }
    
    // 'label' attribute on TextCell (id=leadTime_Cell) at PlanDataUploadBillingPlanLV.pcf: line 45, column 42
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlan.LeadTime")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadBillingPlanLV.pcf: line 23, column 46
    function sortValue_1 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return processor.getLoadStatus(billingPlan)
    }
    
    // 'value' attribute on TextCell (id=leadTime_Cell) at PlanDataUploadBillingPlanLV.pcf: line 45, column 42
    function sortValue_10 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.LeadTime
    }
    
    // 'value' attribute on TextCell (id=nonResponsiveLeadTime_Cell) at PlanDataUploadBillingPlanLV.pcf: line 50, column 42
    function sortValue_12 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.NonResponsiveLeadTime
    }
    
    // 'value' attribute on TypeKeyCell (id=fixPaymentDueDateOn_Cell) at PlanDataUploadBillingPlanLV.pcf: line 55, column 48
    function sortValue_14 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.FixPaymentDueDateOn
    }
    
    // 'value' attribute on DateCell (id=effectiveDate_Cell) at PlanDataUploadBillingPlanLV.pcf: line 60, column 39
    function sortValue_16 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=expirationDate_Cell) at PlanDataUploadBillingPlanLV.pcf: line 65, column 39
    function sortValue_18 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=invoiceFee_Cell) at PlanDataUploadBillingPlanLV.pcf: line 70, column 45
    function sortValue_20 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.InvoiceFee
    }
    
    // 'value' attribute on TextCell (id=paymentReversalFee_Cell) at PlanDataUploadBillingPlanLV.pcf: line 75, column 45
    function sortValue_22 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.PaymentReversalFee
    }
    
    // 'value' attribute on BooleanRadioCell (id=skipInstallmentFees_Cell) at PlanDataUploadBillingPlanLV.pcf: line 80, column 42
    function sortValue_24 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.SkipInstallmentFees
    }
    
    // 'value' attribute on TypeKeyCell (id=lineItemsShow_Cell) at PlanDataUploadBillingPlanLV.pcf: line 85, column 48
    function sortValue_26 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.LineItemsShow
    }
    
    // 'value' attribute on BooleanRadioCell (id=SuppressLowBalInvoices_Cell) at PlanDataUploadBillingPlanLV.pcf: line 90, column 42
    function sortValue_28 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.SuppressLowBalInvoices
    }
    
    // 'value' attribute on TextCell (id=lowBalanceThreshold_Cell) at PlanDataUploadBillingPlanLV.pcf: line 95, column 45
    function sortValue_30 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.LowBalanceThreshold
    }
    
    // 'value' attribute on TypeKeyCell (id=lowBalanceMethod_Cell) at PlanDataUploadBillingPlanLV.pcf: line 100, column 49
    function sortValue_32 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.LowBalanceMethod
    }
    
    // 'value' attribute on TextCell (id=reviewDisbursementsOver_Cell) at PlanDataUploadBillingPlanLV.pcf: line 105, column 45
    function sortValue_34 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.ReviewDisbursementsOver
    }
    
    // 'value' attribute on TextCell (id=delayDisbursement_Cell) at PlanDataUploadBillingPlanLV.pcf: line 110, column 42
    function sortValue_36 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.DelayDisbursement
    }
    
    // 'value' attribute on TextCell (id=automaticDisbursementOver_Cell) at PlanDataUploadBillingPlanLV.pcf: line 115, column 45
    function sortValue_38 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.AutomaticDisbursementOver
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadBillingPlanLV.pcf: line 28, column 41
    function sortValue_4 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.PublicID
    }
    
    // 'value' attribute on TypeKeyCell (id=availableDisbAmtType_Cell) at PlanDataUploadBillingPlanLV.pcf: line 120, column 53
    function sortValue_40 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.AvailableDisbAmtType
    }
    
    // 'value' attribute on BooleanRadioCell (id=createApprActForAutoDisb_Cell) at PlanDataUploadBillingPlanLV.pcf: line 125, column 42
    function sortValue_42 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.CreateApprActForAutoDisb
    }
    
    // 'value' attribute on BooleanRadioCell (id=sendAutoDisbAwaitingApproval_Cell) at PlanDataUploadBillingPlanLV.pcf: line 130, column 42
    function sortValue_44 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.SendAutoDisbAwaitingApproval
    }
    
    // 'value' attribute on TextCell (id=requestInterval_Cell) at PlanDataUploadBillingPlanLV.pcf: line 135, column 42
    function sortValue_46 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.RequestInterval
    }
    
    // 'value' attribute on TextCell (id=changeDeadlineInterval_Cell) at PlanDataUploadBillingPlanLV.pcf: line 140, column 42
    function sortValue_48 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.ChangeDeadlineInterval
    }
    
    // 'value' attribute on TextCell (id=draftInterval_Cell) at PlanDataUploadBillingPlanLV.pcf: line 145, column 42
    function sortValue_50 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.DraftInterval
    }
    
    // 'value' attribute on TypeKeyCell (id=draftDayLogic_Cell) at PlanDataUploadBillingPlanLV.pcf: line 150, column 48
    function sortValue_52 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.DraftDayLogic
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadBillingPlanLV.pcf: line 34, column 41
    function sortValue_6 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.Name
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at PlanDataUploadBillingPlanLV.pcf: line 39, column 41
    function sortValue_8 (billingPlan :  tdic.util.dataloader.data.plandata.BillingPlanData) : java.lang.Object {
      return billingPlan.Description
    }
    
    // 'value' attribute on RowIterator (id=BillingPlan) at PlanDataUploadBillingPlanLV.pcf: line 15, column 97
    function value_184 () : java.util.ArrayList<tdic.util.dataloader.data.plandata.BillingPlanData> {
      return processor.BillingPlanArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadBillingPlanLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}