package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerDetailForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerDetailForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerDetailForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerDetailForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'action' attribute on ForwardCondition at ProducerDetailForward.pcf: line 15, column 45
    function action_0 () : void {
      ProducerDetail.go(producer)
    }
    
    // 'action' attribute on ForwardCondition at ProducerDetailForward.pcf: line 15, column 45
    function action_dest_1 () : pcf.api.Destination {
      return pcf.ProducerDetail.createDestination(producer)
    }
    
    // 'canVisit' attribute on Forward (id=ProducerDetailForward) at ProducerDetailForward.pcf: line 8, column 32
    static function canVisit_2 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodsummview and producer.ViewableByCurrentUser
    }
    
    override property get CurrentLocation () : pcf.ProducerDetailForward {
      return super.CurrentLocation as pcf.ProducerDetailForward
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}