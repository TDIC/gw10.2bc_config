package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/collateral/NewCollateralChargePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewCollateralChargePopupExpressions {
  @javax.annotation.Generated("config/web/pcf/collateral/NewCollateralChargePopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewCollateralChargePopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (collateral :  Collateral) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Popup (id=NewCollateralChargePopup) at NewCollateralChargePopup.pcf: line 10, column 76
    function beforeCommit_15 (pickedValue :  java.lang.Object) : void {
      collateralUtil.createNewCollateralCharge( collateral, chargePattern, chargeAmount )
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=amount_Input) at NewCollateralChargePopup.pcf: line 51, column 35
    function currency_12 () : typekey.Currency {
      return collateral.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amount_Input) at NewCollateralChargePopup.pcf: line 51, column 35
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargeAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on RangeInput (id=chargeType_Input) at NewCollateralChargePopup.pcf: line 43, column 47
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargePattern = (__VALUE_TO_SET as entity.ChargePattern)
    }
    
    // 'initialValue' attribute on Variable at NewCollateralChargePopup.pcf: line 22, column 45
    function initialValue_0 () : gw.pl.currency.MonetaryAmount {
      return 0bd.ofCurrency(collateral.Currency)
    }
    
    // 'initialValue' attribute on Variable at NewCollateralChargePopup.pcf: line 26, column 49
    function initialValue_1 () : gw.api.web.account.CollateralUtil {
      return new gw.api.web.account.CollateralUtil()
    }
    
    // EditButtons at NewCollateralChargePopup.pcf: line 30, column 32
    function label_2 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=amount_Input) at NewCollateralChargePopup.pcf: line 51, column 35
    function validationExpression_9 () : java.lang.Object {
      return chargeAmount.IsPositive ? null : DisplayKey.get("Web.NewCollateralChargePopup.AmountWarning")
    }
    
    // 'valueRange' attribute on RangeInput (id=chargeType_Input) at NewCollateralChargePopup.pcf: line 43, column 47
    function valueRange_5 () : java.lang.Object {
      return gw.api.web.accounting.ChargePatternHelper.getChargePatterns( entity.Collateral )
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amount_Input) at NewCollateralChargePopup.pcf: line 51, column 35
    function value_10 () : gw.pl.currency.MonetaryAmount {
      return chargeAmount
    }
    
    // 'value' attribute on RangeInput (id=chargeType_Input) at NewCollateralChargePopup.pcf: line 43, column 47
    function value_3 () : entity.ChargePattern {
      return chargePattern
    }
    
    // 'valueRange' attribute on RangeInput (id=chargeType_Input) at NewCollateralChargePopup.pcf: line 43, column 47
    function verifyValueRangeIsAllowedType_6 ($$arg :  entity.ChargePattern[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=chargeType_Input) at NewCollateralChargePopup.pcf: line 43, column 47
    function verifyValueRangeIsAllowedType_6 ($$arg :  gw.api.database.IQueryBeanResult<entity.ChargePattern>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=chargeType_Input) at NewCollateralChargePopup.pcf: line 43, column 47
    function verifyValueRangeIsAllowedType_6 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=chargeType_Input) at NewCollateralChargePopup.pcf: line 43, column 47
    function verifyValueRange_7 () : void {
      var __valueRangeArg = gw.api.web.accounting.ChargePatternHelper.getChargePatterns( entity.Collateral )
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_6(__valueRangeArg)
    }
    
    override property get CurrentLocation () : pcf.NewCollateralChargePopup {
      return super.CurrentLocation as pcf.NewCollateralChargePopup
    }
    
    property get chargeAmount () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("chargeAmount", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set chargeAmount ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("chargeAmount", 0, $arg)
    }
    
    property get chargePattern () : ChargePattern {
      return getVariableValue("chargePattern", 0) as ChargePattern
    }
    
    property set chargePattern ($arg :  ChargePattern) {
      setVariableValue("chargePattern", 0, $arg)
    }
    
    property get collateral () : Collateral {
      return getVariableValue("collateral", 0) as Collateral
    }
    
    property set collateral ($arg :  Collateral) {
      setVariableValue("collateral", 0, $arg)
    }
    
    property get collateralUtil () : gw.api.web.account.CollateralUtil {
      return getVariableValue("collateralUtil", 0) as gw.api.web.account.CollateralUtil
    }
    
    property set collateralUtil ($arg :  gw.api.web.account.CollateralUtil) {
      setVariableValue("collateralUtil", 0, $arg)
    }
    
    
  }
  
  
}