package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadCommissionSubPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanDataUploadCommissionSubPlanLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadCommissionSubPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PlanDataUploadCommissionSubPlanLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at PlanDataUploadCommissionSubPlanLV.pcf: line 17, column 114
    function highlighted_151 () : java.lang.Boolean {
      return (commissionSubPlan.Error or commissionSubPlan.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=commissionableItem1_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 79, column 41
    function label_100 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.CommissionableItem1")
    }
    
    // 'label' attribute on TextCell (id=commissionableItem2_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 84, column 41
    function label_104 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.CommissionableItem2")
    }
    
    // 'label' attribute on TextCell (id=commissionableItem3_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 89, column 41
    function label_108 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.CommissionableItem3")
    }
    
    // 'label' attribute on TextCell (id=commissionableItem4_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 94, column 41
    function label_112 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.CommissionableItem4")
    }
    
    // 'label' attribute on TypeKeyCell (id=assignedRisk_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 99, column 56
    function label_116 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.AssignedRisk")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allEvaluations_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 104, column 42
    function label_121 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.AllEvaluations")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allLOBcodes_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 109, column 42
    function label_126 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.AllLOBCodes")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allSegments_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 114, column 42
    function label_131 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.AllSegments")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allJurisdictions_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 119, column 42
    function label_136 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.AllJurisdictions")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allTerms_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 124, column 42
    function label_141 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.AllTerms")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allUWCompanies_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 129, column 42
    function label_146 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.AllUWCompanies")
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 23, column 46
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=PlanName_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 28, column 41
    function label_50 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.PlanName")
    }
    
    // 'label' attribute on TextCell (id=SubPlanName_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 34, column 41
    function label_55 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.SubPlanName")
    }
    
    // 'label' attribute on TextCell (id=Priority_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 39, column 42
    function label_60 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.Priority")
    }
    
    // 'label' attribute on TextCell (id=primaryRate_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 44, column 45
    function label_65 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.PrimaryRate")
    }
    
    // 'label' attribute on TextCell (id=secondaryRate_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 49, column 45
    function label_70 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.SecondaryRate")
    }
    
    // 'label' attribute on TextCell (id=referrerRate_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 54, column 45
    function label_75 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.ReferrerRate")
    }
    
    // 'label' attribute on TextCell (id=earn_Commissions_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 59, column 41
    function label_80 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.EarnCommissions")
    }
    
    // 'label' attribute on BooleanRadioCell (id=Suspend_for_Delinquency_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 64, column 42
    function label_85 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.SuspendForDelinquency")
    }
    
    // 'label' attribute on TextCell (id=premiumIncentive_Bonus_Percentage_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 69, column 45
    function label_90 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.PremiumIncentiveBonusPercentage")
    }
    
    // 'label' attribute on TextCell (id=premiumIncentive_Threshold_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 74, column 45
    function label_95 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.PremiumIncentiveThreshold")
    }
    
    // 'value' attribute on TextCell (id=PlanName_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 28, column 41
    function valueRoot_52 () : java.lang.Object {
      return commissionSubPlan
    }
    
    // 'value' attribute on TextCell (id=primaryRate_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 44, column 45
    function valueRoot_67 () : java.lang.Object {
      return commissionSubPlan.SubPlanRates.firstWhere(\ c -> c.Role == "primary" )
    }
    
    // 'value' attribute on TextCell (id=secondaryRate_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 49, column 45
    function valueRoot_72 () : java.lang.Object {
      return commissionSubPlan.SubPlanRates.firstWhere(\ c -> c.Role == "secondary" )
    }
    
    // 'value' attribute on TextCell (id=referrerRate_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 54, column 45
    function valueRoot_77 () : java.lang.Object {
      return commissionSubPlan.SubPlanRates.firstWhere(\ c -> c.Role == "referrer" )
    }
    
    // 'value' attribute on TextCell (id=commissionableItem1_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 79, column 41
    function value_101 () : java.lang.String {
      return commissionSubPlan.CommissionItems.length > 0 ?  commissionSubPlan.CommissionItems[0].ChargePattern : null
    }
    
    // 'value' attribute on TextCell (id=commissionableItem2_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 84, column 41
    function value_105 () : java.lang.String {
      return commissionSubPlan.CommissionItems.length > 1 ? commissionSubPlan.CommissionItems[1].ChargePattern : null
    }
    
    // 'value' attribute on TextCell (id=commissionableItem3_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 89, column 41
    function value_109 () : java.lang.String {
      return commissionSubPlan.CommissionItems.length > 2 ? commissionSubPlan.CommissionItems[2].ChargePattern : null
    }
    
    // 'value' attribute on TextCell (id=commissionableItem4_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 94, column 41
    function value_113 () : java.lang.String {
      return commissionSubPlan.CommissionItems.length > 3 ? commissionSubPlan.CommissionItems[3].ChargePattern : null
    }
    
    // 'value' attribute on TypeKeyCell (id=assignedRisk_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 99, column 56
    function value_117 () : typekey.AssignedRiskRestriction {
      return commissionSubPlan.AssignedRisk
    }
    
    // 'value' attribute on BooleanRadioCell (id=allEvaluations_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 104, column 42
    function value_122 () : java.lang.Boolean {
      return commissionSubPlan.AllEvaluations
    }
    
    // 'value' attribute on BooleanRadioCell (id=allLOBcodes_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 109, column 42
    function value_127 () : java.lang.Boolean {
      return commissionSubPlan.AllLOBCodes
    }
    
    // 'value' attribute on BooleanRadioCell (id=allSegments_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 114, column 42
    function value_132 () : java.lang.Boolean {
      return commissionSubPlan.AllSegments
    }
    
    // 'value' attribute on BooleanRadioCell (id=allJurisdictions_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 119, column 42
    function value_137 () : java.lang.Boolean {
      return commissionSubPlan.AllJurisdictions
    }
    
    // 'value' attribute on BooleanRadioCell (id=allTerms_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 124, column 42
    function value_142 () : java.lang.Boolean {
      return commissionSubPlan.AllTerms
    }
    
    // 'value' attribute on BooleanRadioCell (id=allUWCompanies_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 129, column 42
    function value_147 () : java.lang.Boolean {
      return commissionSubPlan.AllUWCompanies
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 23, column 46
    function value_46 () : java.lang.String {
      return processor.getLoadStatus(commissionSubPlan)
    }
    
    // 'value' attribute on TextCell (id=PlanName_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 28, column 41
    function value_51 () : java.lang.String {
      return commissionSubPlan.PlanName
    }
    
    // 'value' attribute on TextCell (id=SubPlanName_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 34, column 41
    function value_56 () : java.lang.String {
      return commissionSubPlan.SubPlanName
    }
    
    // 'value' attribute on TextCell (id=Priority_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 39, column 42
    function value_61 () : java.lang.Integer {
      return commissionSubPlan.Priority
    }
    
    // 'value' attribute on TextCell (id=primaryRate_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 44, column 45
    function value_66 () : java.math.BigDecimal {
      return commissionSubPlan.SubPlanRates.firstWhere(\ c -> c.Role == "primary" ).Rate
    }
    
    // 'value' attribute on TextCell (id=secondaryRate_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 49, column 45
    function value_71 () : java.math.BigDecimal {
      return commissionSubPlan.SubPlanRates.firstWhere(\ c -> c.Role == "secondary" ).Rate
    }
    
    // 'value' attribute on TextCell (id=referrerRate_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 54, column 45
    function value_76 () : java.math.BigDecimal {
      return commissionSubPlan.SubPlanRates.firstWhere(\ c -> c.Role == "referrer" ).Rate
    }
    
    // 'value' attribute on TextCell (id=earn_Commissions_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 59, column 41
    function value_81 () : java.lang.String {
      return commissionSubPlan.Earn_Commissions
    }
    
    // 'value' attribute on BooleanRadioCell (id=Suspend_for_Delinquency_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 64, column 42
    function value_86 () : java.lang.Boolean {
      return commissionSubPlan.Suspend_for_Delinquency
    }
    
    // 'value' attribute on TextCell (id=premiumIncentive_Bonus_Percentage_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 69, column 45
    function value_91 () : java.math.BigDecimal {
      return commissionSubPlan.PremiumIncentive_Bonus_Percentage
    }
    
    // 'value' attribute on TextCell (id=premiumIncentive_Threshold_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 74, column 45
    function value_96 () : java.math.BigDecimal {
      return commissionSubPlan.PremiumIncentive_Threshold
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 23, column 46
    function visible_47 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get commissionSubPlan () : tdic.util.dataloader.data.plandata.CommissionSubPlanData {
      return getIteratedValue(1) as tdic.util.dataloader.data.plandata.CommissionSubPlanData
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadCommissionSubPlanLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanDataUploadCommissionSubPlanLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=secondaryRate_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 49, column 45
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.SecondaryRate")
    }
    
    // 'label' attribute on TextCell (id=referrerRate_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 54, column 45
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.ReferrerRate")
    }
    
    // 'label' attribute on TextCell (id=earn_Commissions_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 59, column 41
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.EarnCommissions")
    }
    
    // 'label' attribute on BooleanRadioCell (id=Suspend_for_Delinquency_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 64, column 42
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.SuspendForDelinquency")
    }
    
    // 'label' attribute on TextCell (id=premiumIncentive_Bonus_Percentage_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 69, column 45
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.PremiumIncentiveBonusPercentage")
    }
    
    // 'label' attribute on TextCell (id=premiumIncentive_Threshold_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 74, column 45
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.PremiumIncentiveThreshold")
    }
    
    // 'label' attribute on TextCell (id=commissionableItem1_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 79, column 41
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.CommissionableItem1")
    }
    
    // 'label' attribute on TextCell (id=commissionableItem2_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 84, column 41
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.CommissionableItem2")
    }
    
    // 'label' attribute on TextCell (id=commissionableItem3_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 89, column 41
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.CommissionableItem3")
    }
    
    // 'label' attribute on TextCell (id=commissionableItem4_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 94, column 41
    function label_29 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.CommissionableItem4")
    }
    
    // 'label' attribute on TextCell (id=PlanName_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 28, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.PlanName")
    }
    
    // 'label' attribute on TypeKeyCell (id=assignedRisk_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 99, column 56
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.AssignedRisk")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allEvaluations_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 104, column 42
    function label_33 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.AllEvaluations")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allLOBcodes_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 109, column 42
    function label_35 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.AllLOBCodes")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allSegments_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 114, column 42
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.AllSegments")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allJurisdictions_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 119, column 42
    function label_39 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.AllJurisdictions")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allTerms_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 124, column 42
    function label_41 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.AllTerms")
    }
    
    // 'label' attribute on BooleanRadioCell (id=allUWCompanies_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 129, column 42
    function label_43 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.AllUWCompanies")
    }
    
    // 'label' attribute on TextCell (id=SubPlanName_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 34, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.SubPlanName")
    }
    
    // 'label' attribute on TextCell (id=Priority_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 39, column 42
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.Priority")
    }
    
    // 'label' attribute on TextCell (id=primaryRate_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 44, column 45
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlan.PrimaryRate")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 23, column 46
    function sortValue_1 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return processor.getLoadStatus(commissionSubPlan)
    }
    
    // 'value' attribute on TextCell (id=primaryRate_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 44, column 45
    function sortValue_10 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.SubPlanRates.firstWhere(\ c -> c.Role == "primary" ).Rate
    }
    
    // 'value' attribute on TextCell (id=secondaryRate_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 49, column 45
    function sortValue_12 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.SubPlanRates.firstWhere(\ c -> c.Role == "secondary" ).Rate
    }
    
    // 'value' attribute on TextCell (id=referrerRate_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 54, column 45
    function sortValue_14 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.SubPlanRates.firstWhere(\ c -> c.Role == "referrer" ).Rate
    }
    
    // 'value' attribute on TextCell (id=earn_Commissions_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 59, column 41
    function sortValue_16 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.Earn_Commissions
    }
    
    // 'value' attribute on BooleanRadioCell (id=Suspend_for_Delinquency_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 64, column 42
    function sortValue_18 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.Suspend_for_Delinquency
    }
    
    // 'value' attribute on TextCell (id=premiumIncentive_Bonus_Percentage_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 69, column 45
    function sortValue_20 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.PremiumIncentive_Bonus_Percentage
    }
    
    // 'value' attribute on TextCell (id=premiumIncentive_Threshold_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 74, column 45
    function sortValue_22 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.PremiumIncentive_Threshold
    }
    
    // 'value' attribute on TextCell (id=commissionableItem1_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 79, column 41
    function sortValue_24 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.CommissionItems.length > 0 ?  commissionSubPlan.CommissionItems[0].ChargePattern : null
    }
    
    // 'value' attribute on TextCell (id=commissionableItem2_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 84, column 41
    function sortValue_26 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.CommissionItems.length > 1 ? commissionSubPlan.CommissionItems[1].ChargePattern : null
    }
    
    // 'value' attribute on TextCell (id=commissionableItem3_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 89, column 41
    function sortValue_28 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.CommissionItems.length > 2 ? commissionSubPlan.CommissionItems[2].ChargePattern : null
    }
    
    // 'value' attribute on TextCell (id=commissionableItem4_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 94, column 41
    function sortValue_30 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.CommissionItems.length > 3 ? commissionSubPlan.CommissionItems[3].ChargePattern : null
    }
    
    // 'value' attribute on TypeKeyCell (id=assignedRisk_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 99, column 56
    function sortValue_32 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.AssignedRisk
    }
    
    // 'value' attribute on BooleanRadioCell (id=allEvaluations_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 104, column 42
    function sortValue_34 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.AllEvaluations
    }
    
    // 'value' attribute on BooleanRadioCell (id=allLOBcodes_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 109, column 42
    function sortValue_36 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.AllLOBCodes
    }
    
    // 'value' attribute on BooleanRadioCell (id=allSegments_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 114, column 42
    function sortValue_38 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.AllSegments
    }
    
    // 'value' attribute on TextCell (id=PlanName_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 28, column 41
    function sortValue_4 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.PlanName
    }
    
    // 'value' attribute on BooleanRadioCell (id=allJurisdictions_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 119, column 42
    function sortValue_40 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.AllJurisdictions
    }
    
    // 'value' attribute on BooleanRadioCell (id=allTerms_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 124, column 42
    function sortValue_42 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.AllTerms
    }
    
    // 'value' attribute on BooleanRadioCell (id=allUWCompanies_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 129, column 42
    function sortValue_44 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.AllUWCompanies
    }
    
    // 'value' attribute on TextCell (id=SubPlanName_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 34, column 41
    function sortValue_6 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.SubPlanName
    }
    
    // 'value' attribute on TextCell (id=Priority_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 39, column 42
    function sortValue_8 (commissionSubPlan :  tdic.util.dataloader.data.plandata.CommissionSubPlanData) : java.lang.Object {
      return commissionSubPlan.Priority
    }
    
    // 'value' attribute on RowIterator (id=CommissionSubPlan) at PlanDataUploadCommissionSubPlanLV.pcf: line 15, column 103
    function value_152 () : java.util.ArrayList<tdic.util.dataloader.data.plandata.CommissionSubPlanData> {
      return processor.CommissionSubPlanArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadCommissionSubPlanLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}