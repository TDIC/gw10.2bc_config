package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountDetailInvoices.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountDetailInvoicesExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailInvoices.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountDetailInvoicesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    static function __constructorIndex (account :  Account, selectedInvoice :  AccountInvoice) : int {
      return 1
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailInvoices_InvoiceSentAlertBar) at AccountDetailInvoices.pcf: line 52, column 42
    function action_7 () : void {
      invoiceResent = null
    }
    
    // 'beforeCommit' attribute on Page (id=AccountDetailInvoices) at AccountDetailInvoices.pcf: line 11, column 73
    function beforeCommit_89 (pickedValue :  java.lang.Object) : void {
      invoices = account.InvoicesSortedByDate; canEditNextInvoiceDate = (account.NextInvoice != null);
    }
    
    // 'canEdit' attribute on Page (id=AccountDetailInvoices) at AccountDetailInvoices.pcf: line 11, column 73
    function canEdit_90 () : java.lang.Boolean {
      return perm.System.invcdateedit
    }
    
    // 'canVisit' attribute on Page (id=AccountDetailInvoices) at AccountDetailInvoices.pcf: line 11, column 73
    static function canVisit_91 (account :  Account, selectedInvoice :  AccountInvoice) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctinvcview
    }
    
    // 'initialValue' attribute on Variable at AccountDetailInvoices.pcf: line 19, column 30
    function initialValue_0 () : java.util.Date {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'initialValue' attribute on Variable at AccountDetailInvoices.pcf: line 26, column 39
    function initialValue_1 () : entity.AccountInvoice[] {
      return account.NonBlankInvoicesSortedByDate
    }
    
    // 'initialValue' attribute on Variable at AccountDetailInvoices.pcf: line 33, column 22
    function initialValue_2 () : String {
      return null
    }
    
    // 'initialValue' attribute on Variable at AccountDetailInvoices.pcf: line 37, column 31
    function initialValue_3 () : AggregationType {
      return account.BillingPlan.Aggregation
    }
    
    // 'initialValue' attribute on Variable at AccountDetailInvoices.pcf: line 41, column 23
    function initialValue_4 () : boolean {
      return account.NextInvoice != null
    }
    
    // 'initialValue' attribute on Variable at AccountDetailInvoices.pcf: line 45, column 29
    function initialValue_5 () : InvoiceStream {
      return null
    }
    
    // 'label' attribute on AlertBar (id=AccountDetailInvoices_InvoiceSentAlertBar) at AccountDetailInvoices.pcf: line 52, column 42
    function label_8 () : java.lang.Object {
      return DisplayKey.get("Web.AccountDetailInvoices.InvoiceSent", invoiceResent)
    }
    
    // Page (id=AccountDetailInvoices) at AccountDetailInvoices.pcf: line 11, column 73
    static function parent_92 (account :  Account, selectedInvoice :  AccountInvoice) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailInvoices_InvoiceSentAlertBar) at AccountDetailInvoices.pcf: line 52, column 42
    function visible_6 () : java.lang.Boolean {
      return invoiceResent != null
    }
    
    override property get CurrentLocation () : pcf.AccountDetailInvoices {
      return super.CurrentLocation as pcf.AccountDetailInvoices
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get aggregation () : AggregationType {
      return getVariableValue("aggregation", 0) as AggregationType
    }
    
    property set aggregation ($arg :  AggregationType) {
      setVariableValue("aggregation", 0, $arg)
    }
    
    property get canEditNextInvoiceDate () : boolean {
      return getVariableValue("canEditNextInvoiceDate", 0) as java.lang.Boolean
    }
    
    property set canEditNextInvoiceDate ($arg :  boolean) {
      setVariableValue("canEditNextInvoiceDate", 0, $arg)
    }
    
    property get invoiceResent () : String {
      return getVariableValue("invoiceResent", 0) as String
    }
    
    property set invoiceResent ($arg :  String) {
      setVariableValue("invoiceResent", 0, $arg)
    }
    
    property get invoices () : entity.AccountInvoice[] {
      return getVariableValue("invoices", 0) as entity.AccountInvoice[]
    }
    
    property set invoices ($arg :  entity.AccountInvoice[]) {
      setVariableValue("invoices", 0, $arg)
    }
    
    property get selectedInvoice () : AccountInvoice {
      return getVariableValue("selectedInvoice", 0) as AccountInvoice
    }
    
    property set selectedInvoice ($arg :  AccountInvoice) {
      setVariableValue("selectedInvoice", 0, $arg)
    }
    
    property get selectedInvoiceStream () : InvoiceStream {
      return getVariableValue("selectedInvoiceStream", 0) as InvoiceStream
    }
    
    property set selectedInvoiceStream ($arg :  InvoiceStream) {
      setVariableValue("selectedInvoiceStream", 0, $arg)
    }
    
    property get today () : java.util.Date {
      return getVariableValue("today", 0) as java.util.Date
    }
    
    property set today ($arg :  java.util.Date) {
      setVariableValue("today", 0, $arg)
    }
    
    function updateInvoices() {
          invoices = selectedInvoiceStream != null
            ? account.InvoicesSortedByDate.where(\ i -> i.InvoiceStream == selectedInvoiceStream)
            : account.InvoicesSortedByDate
        }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailInvoices.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CardViewPanelExpressionsImpl extends DetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'updateConfirmMessage' attribute on EditButtons at AccountDetailInvoices.pcf: line 159, column 120
    function confirmMessage_62 () : java.lang.String {
      return gw.api.util.EquityValidationHelper.isEquityViolatedCheckAllPolicies(account)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=PriorAmountDue_Input) at AccountDetailInvoices.pcf: line 186, column 64
    function currency_69 () : typekey.Currency {
      return account.Currency
    }
    
    // 'def' attribute on InputSetRef at AccountDetailInvoices.pcf: line 175, column 91
    function def_onEnter_64 (def :  pcf.AccountInvoiceInformationInputSet_default) : void {
      def.onEnter(invoice)
    }
    
    // 'def' attribute on PanelRef at AccountDetailInvoices.pcf: line 206, column 84
    function def_onEnter_84 (def :  pcf.AccountInvoiceItemsLV_default) : void {
      def.onEnter(invoice, aggregation)
    }
    
    // 'def' attribute on InputSetRef at AccountDetailInvoices.pcf: line 175, column 91
    function def_refreshVariables_65 (def :  pcf.AccountInvoiceInformationInputSet_default) : void {
      def.refreshVariables(invoice)
    }
    
    // 'def' attribute on PanelRef at AccountDetailInvoices.pcf: line 206, column 84
    function def_refreshVariables_85 (def :  pcf.AccountInvoiceItemsLV_default) : void {
      def.refreshVariables(invoice, aggregation)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at AccountDetailInvoices.pcf: line 216, column 55
    function defaultSetter_79 (__VALUE_TO_SET :  java.lang.Object) : void {
      aggregation = (__VALUE_TO_SET as typekey.AggregationType)
    }
    
    // 'initialValue' attribute on Variable at AccountDetailInvoices.pcf: line 150, column 43
    function initialValue_60 () : entity.AccountInvoice {
      return invoice.PreviousInvoice
    }
    
    // EditButtons at AccountDetailInvoices.pcf: line 159, column 120
    function label_63 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'mode' attribute on InputSetRef at AccountDetailInvoices.pcf: line 175, column 91
    function mode_66 () : java.lang.Object {
      return gw.plugin.Plugins.get(gw.module.IFeesThresholds).getPCFMode()
    }
    
    // 'title' attribute on Card (id=InvoiceDetailCard) at AccountDetailInvoices.pcf: line 153, column 167
    function title_87 () : java.lang.String {
      return DisplayKey.get("Web.AccountDetailInvoices.InvoiceDetail.Title", gw.api.util.StringUtil.formatDate(invoice.EventDate,"short"))
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at AccountDetailInvoices.pcf: line 216, column 55
    function valueRange_80 () : java.lang.Object {
      return typekey.AggregationType.getTypeKeys(false)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PriorAmountDue_Input) at AccountDetailInvoices.pcf: line 186, column 64
    function valueRoot_68 () : java.lang.Object {
      return previousInvoice
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalAmountDue_Input) at AccountDetailInvoices.pcf: line 200, column 56
    function valueRoot_75 () : java.lang.Object {
      return invoice
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PriorAmountDue_Input) at AccountDetailInvoices.pcf: line 186, column 64
    function value_67 () : gw.pl.currency.MonetaryAmount {
      return previousInvoice.OutstandingAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalCharges_Input) at AccountDetailInvoices.pcf: line 193, column 83
    function value_71 () : gw.pl.currency.MonetaryAmount {
      return invoice.Status != TC_PLANNED ? invoice.Amount : null
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalAmountDue_Input) at AccountDetailInvoices.pcf: line 200, column 56
    function value_74 () : gw.pl.currency.MonetaryAmount {
      return invoice.OutstandingAmount
    }
    
    // 'value' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at AccountDetailInvoices.pcf: line 216, column 55
    function value_78 () : typekey.AggregationType {
      return aggregation
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at AccountDetailInvoices.pcf: line 216, column 55
    function verifyValueRangeIsAllowedType_81 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at AccountDetailInvoices.pcf: line 216, column 55
    function verifyValueRangeIsAllowedType_81 ($$arg :  typekey.AggregationType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=AggregationTypeSelector_Input) at AccountDetailInvoices.pcf: line 216, column 55
    function verifyValueRange_82 () : void {
      var __valueRangeArg = typekey.AggregationType.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_81(__valueRangeArg)
    }
    
    // 'editVisible' attribute on EditButtons at AccountDetailInvoices.pcf: line 159, column 120
    function visible_61 () : java.lang.Boolean {
      return invoice.Status == TC_PLANNED and perm.Account.invcdateedit
    }
    
    property get previousInvoice () : entity.AccountInvoice {
      return getVariableValue("previousInvoice", 2) as entity.AccountInvoice
    }
    
    property set previousInvoice ($arg :  entity.AccountInvoice) {
      setVariableValue("previousInvoice", 2, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailInvoices.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailPanelExpressionsImpl extends AccountDetailInvoicesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=AccountDetailInvoices_NewInvoiceButton) at AccountDetailInvoices.pcf: line 75, column 31
    function action_17 () : void {
      NewInvoice.go(account, selectedInvoiceStream)
    }
    
    // 'action' attribute on ToolbarButton (id=AccountDetailInvoices_NewInvoiceButton) at AccountDetailInvoices.pcf: line 75, column 31
    function action_dest_18 () : pcf.api.Destination {
      return pcf.NewInvoice.createDestination(account, selectedInvoiceStream)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=AccountDetailInvoices_RemoveInvoicesButton) at AccountDetailInvoices.pcf: line 84, column 31
    function allCheckedRowsAction_20 (CheckedValues :  entity.AccountInvoice[], CheckedValuesErrors :  java.util.Map) : void {
      gw.api.web.invoice.InvoiceUtil.deleteInvoices(CheckedValues); invoices = account.InvoicesSortedByDate; canEditNextInvoiceDate = (account.NextInvoice != null);
    }
    
    // 'available' attribute on ToolbarButton (id=AccountDetailInvoices_NewInvoiceButton) at AccountDetailInvoices.pcf: line 75, column 31
    function available_16 () : java.lang.Boolean {
      return perm.Account.invccreate and  selectedInvoiceStream != null
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=AccountDetailInvoices_RemoveInvoicesButton) at AccountDetailInvoices.pcf: line 84, column 31
    function available_19 () : java.lang.Boolean {
      return perm.Account.invcremove
    }
    
    // 'value' attribute on ToolbarRangeInput (id=InvoiceStreamFilter_Input) at AccountDetailInvoices.pcf: line 66, column 48
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      selectedInvoiceStream = (__VALUE_TO_SET as entity.InvoiceStream)
    }
    
    // 'onChange' attribute on PostOnChange at AccountDetailInvoices.pcf: line 68, column 46
    function onChange_9 () : void {
      updateInvoices()
    }
    
    // 'selectionOnEnter' attribute on ListDetailPanel (id=DetailPanel) at AccountDetailInvoices.pcf: line 57, column 40
    function selectionOnEnter_88 () : java.lang.Object {
      return selectedInvoice
    }
    
    // 'value' attribute on DateCell (id=InvoiceDate_Cell) at AccountDetailInvoices.pcf: line 104, column 49
    function sortValue_21 (invoiceRow :  entity.AccountInvoice) : java.lang.Object {
      return invoiceRow.EventDate
    }
    
    // 'value' attribute on DateCell (id=InvoiceDueDate_Cell) at AccountDetailInvoices.pcf: line 108, column 54
    function sortValue_22 (invoiceRow :  entity.AccountInvoice) : java.lang.Object {
      return invoiceRow.PaymentDueDate
    }
    
    // 'value' attribute on TextCell (id=InvoiceNumber_Cell) at AccountDetailInvoices.pcf: line 112, column 53
    function sortValue_23 (invoiceRow :  entity.AccountInvoice) : java.lang.Object {
      return invoiceRow.InvoiceNumber
    }
    
    // 'value' attribute on BooleanRadioCell (id=InvoiceAdHoc_Cell) at AccountDetailInvoices.pcf: line 116, column 49
    function sortValue_24 (invoiceRow :  entity.AccountInvoice) : java.lang.Object {
      return invoiceRow.isAdHoc()
    }
    
    // 'sortBy' attribute on TextCell (id=Status_Cell) at AccountDetailInvoices.pcf: line 121, column 51
    function sortValue_25 (invoiceRow :  entity.AccountInvoice) : java.lang.Object {
      return invoiceRow.Status
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AccountDetailInvoices.pcf: line 128, column 46
    function sortValue_26 (invoiceRow :  entity.AccountInvoice) : java.lang.Object {
      return invoiceRow.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CurrentAmountDue_Cell) at AccountDetailInvoices.pcf: line 135, column 49
    function sortValue_27 (invoiceRow :  entity.AccountInvoice) : java.lang.Object {
      return invoiceRow.AmountDue
    }
    
    // 'value' attribute on TextCell (id=InvoiceStream_Cell) at AccountDetailInvoices.pcf: line 141, column 53
    function sortValue_28 (invoiceRow :  entity.AccountInvoice) : java.lang.Object {
      return invoiceRow.InvoiceStream
    }
    
    // '$$sumValue' attribute on RowIterator at AccountDetailInvoices.pcf: line 128, column 46
    function sumValueRoot_30 (invoiceRow :  entity.AccountInvoice) : java.lang.Object {
      return invoiceRow
    }
    
    // 'footerSumValue' attribute on RowIterator at AccountDetailInvoices.pcf: line 128, column 46
    function sumValue_29 (invoiceRow :  entity.AccountInvoice) : java.lang.Object {
      return invoiceRow.Amount
    }
    
    // 'footerSumValue' attribute on RowIterator at AccountDetailInvoices.pcf: line 135, column 49
    function sumValue_31 (invoiceRow :  entity.AccountInvoice) : java.lang.Object {
      return invoiceRow.AmountDue
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=InvoiceStreamFilter_Input) at AccountDetailInvoices.pcf: line 66, column 48
    function valueRange_12 () : java.lang.Object {
      return account.InvoiceStreams.sort(\ invoiceStream1, invoiceStream2 -> invoiceStream1.DisplayName <= invoiceStream2.DisplayName)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=InvoiceStreamFilter_Input) at AccountDetailInvoices.pcf: line 66, column 48
    function value_10 () : entity.InvoiceStream {
      return selectedInvoiceStream
    }
    
    // 'value' attribute on RowIterator at AccountDetailInvoices.pcf: line 95, column 51
    function value_59 () : entity.AccountInvoice[] {
      return invoices
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=InvoiceStreamFilter_Input) at AccountDetailInvoices.pcf: line 66, column 48
    function verifyValueRangeIsAllowedType_13 ($$arg :  entity.InvoiceStream[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=InvoiceStreamFilter_Input) at AccountDetailInvoices.pcf: line 66, column 48
    function verifyValueRangeIsAllowedType_13 ($$arg :  gw.api.database.IQueryBeanResult<entity.InvoiceStream>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=InvoiceStreamFilter_Input) at AccountDetailInvoices.pcf: line 66, column 48
    function verifyValueRangeIsAllowedType_13 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=InvoiceStreamFilter_Input) at AccountDetailInvoices.pcf: line 66, column 48
    function verifyValueRange_14 () : void {
      var __valueRangeArg = account.InvoiceStreams.sort(\ invoiceStream1, invoiceStream2 -> invoiceStream1.DisplayName <= invoiceStream2.DisplayName)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_13(__valueRangeArg)
    }
    
    property get invoice () : AccountInvoice {
      return getCurrentSelection(1) as AccountInvoice
    }
    
    property set invoice ($arg :  AccountInvoice) {
      setCurrentSelection(1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountDetailInvoices.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends DetailPanelExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'condition' attribute on ToolbarFlag at AccountDetailInvoices.pcf: line 98, column 43
    function condition_33 () : java.lang.Boolean {
      return !invoiceRow.isSent() and invoiceRow.InvoiceItems.length == 0
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at AccountDetailInvoices.pcf: line 128, column 46
    function currency_50 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on DateCell (id=InvoiceDate_Cell) at AccountDetailInvoices.pcf: line 104, column 49
    function valueRoot_35 () : java.lang.Object {
      return invoiceRow
    }
    
    // 'value' attribute on DateCell (id=InvoiceDate_Cell) at AccountDetailInvoices.pcf: line 104, column 49
    function value_34 () : java.util.Date {
      return invoiceRow.EventDate
    }
    
    // 'value' attribute on DateCell (id=InvoiceDueDate_Cell) at AccountDetailInvoices.pcf: line 108, column 54
    function value_37 () : java.util.Date {
      return invoiceRow.PaymentDueDate
    }
    
    // 'value' attribute on TextCell (id=InvoiceNumber_Cell) at AccountDetailInvoices.pcf: line 112, column 53
    function value_40 () : java.lang.String {
      return invoiceRow.InvoiceNumber
    }
    
    // 'value' attribute on BooleanRadioCell (id=InvoiceAdHoc_Cell) at AccountDetailInvoices.pcf: line 116, column 49
    function value_43 () : java.lang.Boolean {
      return invoiceRow.isAdHoc()
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at AccountDetailInvoices.pcf: line 121, column 51
    function value_45 () : java.lang.String {
      return invoiceRow.StatusForUI
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at AccountDetailInvoices.pcf: line 128, column 46
    function value_48 () : gw.pl.currency.MonetaryAmount {
      return invoiceRow.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CurrentAmountDue_Cell) at AccountDetailInvoices.pcf: line 135, column 49
    function value_52 () : gw.pl.currency.MonetaryAmount {
      return invoiceRow.AmountDue
    }
    
    // 'value' attribute on TextCell (id=InvoiceStream_Cell) at AccountDetailInvoices.pcf: line 141, column 53
    function value_56 () : entity.InvoiceStream {
      return invoiceRow.InvoiceStream
    }
    
    property get invoiceRow () : entity.AccountInvoice {
      return getIteratedValue(2) as entity.AccountInvoice
    }
    
    
  }
  
  
}