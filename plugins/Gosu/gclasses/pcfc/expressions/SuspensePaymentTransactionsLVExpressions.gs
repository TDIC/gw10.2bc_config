package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/suspensepayment/SuspensePaymentTransactionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SuspensePaymentTransactionsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/suspensepayment/SuspensePaymentTransactionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SuspensePaymentTransactionsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=transactionNumber_Cell) at SuspensePaymentTransactionsLV.pcf: line 25, column 51
    function action_3 () : void {
      TransactionDetailPopup.push(transactions)
    }
    
    // 'action' attribute on TextCell (id=transactionNumber_Cell) at SuspensePaymentTransactionsLV.pcf: line 25, column 51
    function action_dest_4 () : pcf.api.Destination {
      return pcf.TransactionDetailPopup.createDestination(transactions)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=transactionAmount_Cell) at SuspensePaymentTransactionsLV.pcf: line 30, column 40
    function currency_10 () : typekey.Currency {
      return transactions.Currency
    }
    
    // 'value' attribute on TextCell (id=transactionNumber_Cell) at SuspensePaymentTransactionsLV.pcf: line 25, column 51
    function valueRoot_6 () : java.lang.Object {
      return transactions
    }
    
    // 'value' attribute on TextCell (id=transactionNumber_Cell) at SuspensePaymentTransactionsLV.pcf: line 25, column 51
    function value_5 () : java.lang.String {
      return transactions.TransactionNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=transactionAmount_Cell) at SuspensePaymentTransactionsLV.pcf: line 30, column 40
    function value_8 () : gw.pl.currency.MonetaryAmount {
      return transactions.Amount
    }
    
    property get transactions () : entity.Transaction {
      return getIteratedValue(1) as entity.Transaction
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/suspensepayment/SuspensePaymentTransactionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SuspensePaymentTransactionsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'initialValue' attribute on Variable at SuspensePaymentTransactionsLV.pcf: line 13, column 67
    function initialValue_0 () : gw.api.database.IQueryBeanResult<Transaction> {
      return suspensePayment.Transactions as gw.api.database.IQueryBeanResult<Transaction>
    }
    
    // 'value' attribute on TextCell (id=transactionNumber_Cell) at SuspensePaymentTransactionsLV.pcf: line 25, column 51
    function sortValue_1 (transactions :  entity.Transaction) : java.lang.Object {
      return transactions.TransactionNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=transactionAmount_Cell) at SuspensePaymentTransactionsLV.pcf: line 30, column 40
    function sortValue_2 (transactions :  entity.Transaction) : java.lang.Object {
      return transactions.Amount
    }
    
    // 'value' attribute on RowIterator (id=spTransactions) at SuspensePaymentTransactionsLV.pcf: line 19, column 78
    function value_12 () : gw.api.database.IQueryBeanResult<entity.Transaction> {
      return transactionsQuery
    }
    
    property get suspensePayment () : SuspensePayment {
      return getRequireValue("suspensePayment", 0) as SuspensePayment
    }
    
    property set suspensePayment ($arg :  SuspensePayment) {
      setRequireValue("suspensePayment", 0, $arg)
    }
    
    property get transactionsQuery () : gw.api.database.IQueryBeanResult<Transaction> {
      return getVariableValue("transactionsQuery", 0) as gw.api.database.IQueryBeanResult<Transaction>
    }
    
    property set transactionsQuery ($arg :  gw.api.database.IQueryBeanResult<Transaction>) {
      setVariableValue("transactionsQuery", 0, $arg)
    }
    
    
  }
  
  
}