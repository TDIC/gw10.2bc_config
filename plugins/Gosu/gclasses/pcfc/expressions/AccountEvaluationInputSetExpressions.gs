package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountEvaluationInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountEvaluationInputSetExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountEvaluationInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountEvaluationInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on Label (id=NumberOfDelinquencies) at AccountEvaluationInputSet.pcf: line 19, column 51
    function label_0 () : java.lang.String {
      return account.TextForDelinquenciesMetric
    }
    
    // 'label' attribute on Label (id=NumberOfDelinquenciesPastGracePeriod) at AccountEvaluationInputSet.pcf: line 23, column 66
    function label_1 () : java.lang.String {
      return account.TextForDelinquenciesPastGracePeriodMetric
    }
    
    // 'label' attribute on Label (id=NumberOfPejorativePaymentReversals) at AccountEvaluationInputSet.pcf: line 27, column 64
    function label_2 () : java.lang.String {
      return account.TextForPejorativePaymentReversalsMetric
    }
    
    // 'label' attribute on Label (id=NumberOfPolicyCancellations) at AccountEvaluationInputSet.pcf: line 31, column 57
    function label_3 () : java.lang.String {
      return account.TextForPolicyCancellationsMetric
    }
    
    // 'label' attribute on Label (id=AccountEvaluationValueID) at AccountEvaluationInputSet.pcf: line 39, column 53
    function label_4 () : java.lang.String {
      return account.AccountEvaluationDisplayText
    }
    
    // 'value' attribute on TextCell (id=ViewDelinquencyProcessDetails_Cell) at AccountEvaluationInputSet.pcf: line 82, column 111
    function sortValue_10 (delinquencyProcess :  entity.DelinquencyProcess) : java.lang.Object {
      return ( null != delinquencyProcess ? getEventDisplayName(delinquencyProcess) : "")
    }
    
    // 'value' attribute on DateCell (id=StartDate_Cell) at AccountEvaluationInputSet.pcf: line 57, column 53
    function sortValue_5 (delinquencyProcess :  entity.DelinquencyProcess) : java.lang.Object {
      return delinquencyProcess.StartDate
    }
    
    // 'value' attribute on TextCell (id=DelinquencyPolicy_Cell) at AccountEvaluationInputSet.pcf: line 63, column 72
    function sortValue_6 (delinquencyProcess :  entity.DelinquencyProcess) : java.lang.Object {
      return delinquencyProcess.Target
    }
    
    // 'value' attribute on TextCell (id=GoodStandingDate_Cell) at AccountEvaluationInputSet.pcf: line 67, column 65
    function sortValue_7 (delinquencyProcess :  entity.DelinquencyProcess) : java.lang.Object {
      return delinquencyProcess.ExitDateDisplayString
    }
    
    // 'value' attribute on TextCell (id=DelinquencyAge_Cell) at AccountEvaluationInputSet.pcf: line 71, column 66
    function sortValue_8 (delinquencyProcess :  entity.DelinquencyProcess) : java.lang.Object {
      return delinquencyProcess.AgeInDaysDisplayString
    }
    
    // 'value' attribute on MonetaryAmountCell (id=DelinquencyAmount_Cell) at AccountEvaluationInputSet.pcf: line 77, column 50
    function sortValue_9 (delinquencyProcess :  entity.DelinquencyProcess) : java.lang.Object {
      return delinquencyProcess.Amount
    }
    
    // 'value' attribute on RowIterator at AccountEvaluationInputSet.pcf: line 52, column 51
    function value_32 () : entity.DelinquencyProcess[] {
      return account.AccountEvaluationDelinquencies
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get contextDelinquencyProcess () : DelinquencyProcess {
      return getRequireValue("contextDelinquencyProcess", 0) as DelinquencyProcess
    }
    
    property set contextDelinquencyProcess ($arg :  DelinquencyProcess) {
      setRequireValue("contextDelinquencyProcess", 0, $arg)
    }
    
    function getEventDisplayName(delinquencyProcess : DelinquencyProcess) : String {
      return delinquencyProcess.PreviousEvent.EventName == DelinquencyEventName.TC_DUNNINGLETTER1 ? DisplayKey.get("TDIC.Web.DelinquencyProcessEventsLV.EventName.Label") : delinquencyProcess.PreviousEvent.EventName.Description
    } 
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountEvaluationInputSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountEvaluationInputSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=DelinquencyPolicy_Cell) at AccountEvaluationInputSet.pcf: line 63, column 72
    function action_14 () : void {
      DelinquencyTargetDetailsForward.go(delinquencyProcess.Target)
    }
    
    // 'action' attribute on TextCell (id=ViewDelinquencyProcessDetails_Cell) at AccountEvaluationInputSet.pcf: line 82, column 111
    function action_29 () : void {
      if (delinquencyProcess != contextDelinquencyProcess) pcf.DelinquencyProcessDetailPopup.push(delinquencyProcess)
    }
    
    // 'action' attribute on TextCell (id=DelinquencyPolicy_Cell) at AccountEvaluationInputSet.pcf: line 63, column 72
    function action_dest_15 () : pcf.api.Destination {
      return pcf.DelinquencyTargetDetailsForward.createDestination(delinquencyProcess.Target)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=DelinquencyAmount_Cell) at AccountEvaluationInputSet.pcf: line 77, column 50
    function currency_27 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on DateCell (id=StartDate_Cell) at AccountEvaluationInputSet.pcf: line 57, column 53
    function valueRoot_12 () : java.lang.Object {
      return delinquencyProcess
    }
    
    // 'value' attribute on DateCell (id=StartDate_Cell) at AccountEvaluationInputSet.pcf: line 57, column 53
    function value_11 () : java.util.Date {
      return delinquencyProcess.StartDate
    }
    
    // 'value' attribute on TextCell (id=DelinquencyPolicy_Cell) at AccountEvaluationInputSet.pcf: line 63, column 72
    function value_16 () : gw.api.domain.delinquency.DelinquencyTarget {
      return delinquencyProcess.Target
    }
    
    // 'value' attribute on TextCell (id=GoodStandingDate_Cell) at AccountEvaluationInputSet.pcf: line 67, column 65
    function value_19 () : java.lang.String {
      return delinquencyProcess.ExitDateDisplayString
    }
    
    // 'value' attribute on TextCell (id=DelinquencyAge_Cell) at AccountEvaluationInputSet.pcf: line 71, column 66
    function value_22 () : java.lang.String {
      return delinquencyProcess.AgeInDaysDisplayString
    }
    
    // 'value' attribute on MonetaryAmountCell (id=DelinquencyAmount_Cell) at AccountEvaluationInputSet.pcf: line 77, column 50
    function value_25 () : gw.pl.currency.MonetaryAmount {
      return delinquencyProcess.Amount
    }
    
    // 'value' attribute on TextCell (id=ViewDelinquencyProcessDetails_Cell) at AccountEvaluationInputSet.pcf: line 82, column 111
    function value_30 () : java.lang.String {
      return ( null != delinquencyProcess ? getEventDisplayName(delinquencyProcess) : "")
    }
    
    property get delinquencyProcess () : entity.DelinquencyProcess {
      return getIteratedValue(1) as entity.DelinquencyProcess
    }
    
    
  }
  
  
}