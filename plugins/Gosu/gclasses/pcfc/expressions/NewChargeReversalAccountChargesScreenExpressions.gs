package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.database.Relop
uses gw.api.database.Query
@javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalAccountChargesScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewChargeReversalAccountChargesScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalAccountChargesScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewChargeReversalAccountChargesScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=Charge_Input) at NewChargeReversalAccountChargesScreen.pcf: line 26, column 38
    function valueRoot_1 () : java.lang.Object {
      return reversal
    }
    
    // 'value' attribute on TextInput (id=Charge_Input) at NewChargeReversalAccountChargesScreen.pcf: line 26, column 38
    function value_0 () : entity.Charge {
      return reversal.Charge
    }
    
    property get accountID () : gw.pl.persistence.core.Key {
      return getRequireValue("accountID", 0) as gw.pl.persistence.core.Key
    }
    
    property set accountID ($arg :  gw.pl.persistence.core.Key) {
      setRequireValue("accountID", 0, $arg)
    }
    
    property get reversal () : ChargeReversal {
      return getRequireValue("reversal", 0) as ChargeReversal
    }
    
    property set reversal ($arg :  ChargeReversal) {
      setRequireValue("reversal", 0, $arg)
    }
    
    
        function initializeSearchCriteria(): gw.search.ReversibleChargeSearchCriteria {
          var criteria = new gw.search.ReversibleChargeSearchCriteria();
          criteria.Account = Query.make(Account).compare("ID", Relop.Equals, accountID).select().AtMostOneRow;
          criteria.Currency = criteria.Account.Currency;
          return criteria;
        }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/transaction/NewChargeReversalAccountChargesScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends NewChargeReversalAccountChargesScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalAccountChargesScreen.pcf: line 42, column 57
    function def_onEnter_3 (def :  pcf.ChargeSearchDV) : void {
      def.onEnter(searchCriteria, false)
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalAccountChargesScreen.pcf: line 46, column 66
    function def_onEnter_5 (def :  pcf.NewChargeReversalChargesLV) : void {
      def.onEnter(reversal, charges)
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalAccountChargesScreen.pcf: line 42, column 57
    function def_refreshVariables_4 (def :  pcf.ChargeSearchDV) : void {
      def.refreshVariables(searchCriteria, false)
    }
    
    // 'def' attribute on PanelRef at NewChargeReversalAccountChargesScreen.pcf: line 46, column 66
    function def_refreshVariables_6 (def :  pcf.NewChargeReversalChargesLV) : void {
      def.refreshVariables(reversal, charges)
    }
    
    // 'searchCriteria' attribute on SearchPanel at NewChargeReversalAccountChargesScreen.pcf: line 40, column 78
    function searchCriteria_8 () : gw.search.ReversibleChargeSearchCriteria {
      return initializeSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel at NewChargeReversalAccountChargesScreen.pcf: line 40, column 78
    function search_7 () : java.lang.Object {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria)
    }
    
    property get charges () : gw.api.database.IQueryBeanResult<Charge> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<Charge>
    }
    
    property get searchCriteria () : gw.search.ReversibleChargeSearchCriteria {
      return getCriteriaValue(1) as gw.search.ReversibleChargeSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.ReversibleChargeSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}