package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/MovePaymentToSuspense.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class MovePaymentToSuspenseExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/MovePaymentToSuspense.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class MovePaymentToSuspenseExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account, originalMoney :  DirectBillMoneyRcvd) : int {
      return 0
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at MovePaymentToSuspense.pcf: line 101, column 37
    function action_31 () : void {
      AccountSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=PolicyPicker) at MovePaymentToSuspense.pcf: line 120, column 36
    function action_41 () : void {
      PolicySearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at MovePaymentToSuspense.pcf: line 101, column 37
    function action_dest_32 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=PolicyPicker) at MovePaymentToSuspense.pcf: line 120, column 36
    function action_dest_42 () : pcf.api.Destination {
      return pcf.PolicySearchPopup.createDestination()
    }
    
    // 'available' attribute on TextInput (id=AccountNumber_Input) at MovePaymentToSuspense.pcf: line 101, column 37
    function available_33 () : java.lang.Boolean {
      return accountOrPolicy
    }
    
    // 'available' attribute on TextInput (id=PolicyNumber_Input) at MovePaymentToSuspense.pcf: line 120, column 36
    function available_43 () : java.lang.Boolean {
      return !accountOrPolicy
    }
    
    // 'beforeCommit' attribute on Page (id=MovePaymentToSuspense) at MovePaymentToSuspense.pcf: line 12, column 73
    function beforeCommit_51 (pickedValue :  java.lang.Object) : void {
      suspensePaymentUtil.createSuspensePaymentFromPayment(originalMoney, accountNumber, policyNumber, accountOrPolicy); createHistoryEvent()
    }
    
    // 'canVisit' attribute on Page (id=MovePaymentToSuspense) at MovePaymentToSuspense.pcf: line 12, column 73
    static function canVisit_52 (account :  Account, originalMoney :  DirectBillMoneyRcvd) : java.lang.Boolean {
      return perm.DirectBillMoneyRcvd.pmntmanproc
    }
    
    // 'conversionExpression' attribute on TextInput (id=AccountNumber_Input) at MovePaymentToSuspense.pcf: line 101, column 37
    function conversionExpression_34 (PickedValue :  Account) : java.lang.String {
      return (PickedValue as Account).AccountNumber
    }
    
    // 'conversionExpression' attribute on TextInput (id=PolicyNumber_Input) at MovePaymentToSuspense.pcf: line 120, column 36
    function conversionExpression_44 (PickedValue :  PolicyPeriod) : java.lang.String {
      return (PickedValue as PolicyPeriod).PolicyNumber
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at MovePaymentToSuspense.pcf: line 69, column 42
    function currency_15 () : typekey.Currency {
      return originalMoney.Currency
    }
    
    // 'def' attribute on InputSetRef at MovePaymentToSuspense.pcf: line 83, column 67
    function def_onEnter_20 (def :  pcf.PaymentInstrumentInputSet_ach) : void {
      def.onEnter(originalMoney.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at MovePaymentToSuspense.pcf: line 83, column 67
    function def_onEnter_22 (def :  pcf.PaymentInstrumentInputSet_creditcard) : void {
      def.onEnter(originalMoney.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at MovePaymentToSuspense.pcf: line 83, column 67
    function def_onEnter_24 (def :  pcf.PaymentInstrumentInputSet_default) : void {
      def.onEnter(originalMoney.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at MovePaymentToSuspense.pcf: line 83, column 67
    function def_onEnter_26 (def :  pcf.PaymentInstrumentInputSet_misc) : void {
      def.onEnter(originalMoney.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at MovePaymentToSuspense.pcf: line 83, column 67
    function def_onEnter_28 (def :  pcf.PaymentInstrumentInputSet_wire) : void {
      def.onEnter(originalMoney.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at MovePaymentToSuspense.pcf: line 83, column 67
    function def_refreshVariables_21 (def :  pcf.PaymentInstrumentInputSet_ach) : void {
      def.refreshVariables(originalMoney.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at MovePaymentToSuspense.pcf: line 83, column 67
    function def_refreshVariables_23 (def :  pcf.PaymentInstrumentInputSet_creditcard) : void {
      def.refreshVariables(originalMoney.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at MovePaymentToSuspense.pcf: line 83, column 67
    function def_refreshVariables_25 (def :  pcf.PaymentInstrumentInputSet_default) : void {
      def.refreshVariables(originalMoney.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at MovePaymentToSuspense.pcf: line 83, column 67
    function def_refreshVariables_27 (def :  pcf.PaymentInstrumentInputSet_misc) : void {
      def.refreshVariables(originalMoney.PaymentInstrument)
    }
    
    // 'def' attribute on InputSetRef at MovePaymentToSuspense.pcf: line 83, column 67
    function def_refreshVariables_29 (def :  pcf.PaymentInstrumentInputSet_wire) : void {
      def.refreshVariables(originalMoney.PaymentInstrument)
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at MovePaymentToSuspense.pcf: line 101, column 37
    function defaultSetter_36 (__VALUE_TO_SET :  java.lang.Object) : void {
      accountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on Choice (id=AccountChoice) at MovePaymentToSuspense.pcf: line 94, column 43
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      accountOrPolicy = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at MovePaymentToSuspense.pcf: line 120, column 36
    function defaultSetter_46 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at MovePaymentToSuspense.pcf: line 30, column 54
    function initialValue_0 () : gw.api.web.payment.SuspensePaymentUtil {
      return new gw.api.web.payment.SuspensePaymentUtil()
    }
    
    // 'mode' attribute on InputSetRef at MovePaymentToSuspense.pcf: line 83, column 67
    function mode_30 () : java.lang.Object {
      return originalMoney.PaymentInstrument.PaymentMethod
    }
    
    // 'parent' attribute on Page (id=MovePaymentToSuspense) at MovePaymentToSuspense.pcf: line 12, column 73
    static function parent_53 (account :  Account, originalMoney :  DirectBillMoneyRcvd) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination( account )
    }
    
    // 'value' attribute on TextInput (id=OriginatingAccountNumber_Input) at MovePaymentToSuspense.pcf: line 49, column 41
    function valueRoot_2 () : java.lang.Object {
      return originalMoney
    }
    
    // 'value' attribute on TextInput (id=OriginatingAccountName_Input) at MovePaymentToSuspense.pcf: line 53, column 65
    function valueRoot_5 () : java.lang.Object {
      return originalMoney.Account
    }
    
    // 'value' attribute on TextInput (id=OriginatingAccountNumber_Input) at MovePaymentToSuspense.pcf: line 49, column 41
    function value_1 () : entity.Account {
      return originalMoney.Account
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at MovePaymentToSuspense.pcf: line 63, column 48
    function value_10 () : java.lang.String {
      return originalMoney.Description
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at MovePaymentToSuspense.pcf: line 69, column 42
    function value_13 () : gw.pl.currency.MonetaryAmount {
      return originalMoney.Amount
    }
    
    // 'value' attribute on TextInput (id=PaymentInstrument_Input) at MovePaymentToSuspense.pcf: line 80, column 51
    function value_17 () : entity.PaymentInstrument {
      return originalMoney.PaymentInstrument
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at MovePaymentToSuspense.pcf: line 101, column 37
    function value_35 () : java.lang.String {
      return accountNumber
    }
    
    // 'value' attribute on TextInput (id=OriginatingAccountName_Input) at MovePaymentToSuspense.pcf: line 53, column 65
    function value_4 () : java.lang.String {
      return originalMoney.Account.AccountNameLocalized
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at MovePaymentToSuspense.pcf: line 120, column 36
    function value_45 () : java.lang.String {
      return policyNumber
    }
    
    // 'value' attribute on DateInput (id=PaymentDate_Input) at MovePaymentToSuspense.pcf: line 59, column 49
    function value_7 () : java.util.Date {
      return originalMoney.ReceivedDate
    }
    
    override property get CurrentLocation () : pcf.MovePaymentToSuspense {
      return super.CurrentLocation as pcf.MovePaymentToSuspense
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get accountNumber () : String {
      return getVariableValue("accountNumber", 0) as String
    }
    
    property set accountNumber ($arg :  String) {
      setVariableValue("accountNumber", 0, $arg)
    }
    
    property get accountOrPolicy () : Boolean {
      return getVariableValue("accountOrPolicy", 0) as Boolean
    }
    
    property set accountOrPolicy ($arg :  Boolean) {
      setVariableValue("accountOrPolicy", 0, $arg)
    }
    
    property get originalMoney () : DirectBillMoneyRcvd {
      return getVariableValue("originalMoney", 0) as DirectBillMoneyRcvd
    }
    
    property set originalMoney ($arg :  DirectBillMoneyRcvd) {
      setVariableValue("originalMoney", 0, $arg)
    }
    
    property get policyNumber () : String {
      return getVariableValue("policyNumber", 0) as String
    }
    
    property set policyNumber ($arg :  String) {
      setVariableValue("policyNumber", 0, $arg)
    }
    
    property get suspensePaymentUtil () : gw.api.web.payment.SuspensePaymentUtil {
      return getVariableValue("suspensePaymentUtil", 0) as gw.api.web.payment.SuspensePaymentUtil
    }
    
    property set suspensePaymentUtil ($arg :  gw.api.web.payment.SuspensePaymentUtil) {
      setVariableValue("suspensePaymentUtil", 0, $arg)
    }
    
    function createHistoryEvent() {
      var description = DisplayKey.get("Accelerator.History.AccountHistory.MovePaymentToSuspense", originalMoney.PaymentInstrument.DisplayName, Date.CurrentDate, originalMoney.Amount)
      var hist = account.addHistoryFromGosu(Date.CurrentDate,HistoryEventType.TC_PMNTMOVESUSPENSE_TDIC, description, null, null, true)
    }
    
    
  }
  
  
}