package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadAttributeLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadAttributeLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadAttributeLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadAttributeLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadAttributeLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on BooleanRadioCell (id=active_Cell) at AdminDataUploadAttributeLV.pcf: line 50, column 42
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Attribute.Active")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadAttributeLV.pcf: line 55, column 42
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at AdminDataUploadAttributeLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Attribute.Name")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadAttributeLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Attribute.PublicID")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at AdminDataUploadAttributeLV.pcf: line 40, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Attribute.Description")
    }
    
    // 'label' attribute on TypeKeyCell (id=userAttributeType_Cell) at AdminDataUploadAttributeLV.pcf: line 45, column 50
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Attribute.AttributeType")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadAttributeLV.pcf: line 23, column 46
    function sortValue_1 (attribute :  tdic.util.dataloader.data.admindata.AttributeData) : java.lang.Object {
      return processor.getLoadStatus(attribute)
    }
    
    // 'value' attribute on TypeKeyCell (id=userAttributeType_Cell) at AdminDataUploadAttributeLV.pcf: line 45, column 50
    function sortValue_10 (attribute :  tdic.util.dataloader.data.admindata.AttributeData) : java.lang.Object {
      return attribute.UserAttributeType
    }
    
    // 'value' attribute on BooleanRadioCell (id=active_Cell) at AdminDataUploadAttributeLV.pcf: line 50, column 42
    function sortValue_12 (attribute :  tdic.util.dataloader.data.admindata.AttributeData) : java.lang.Object {
      return attribute.Active
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadAttributeLV.pcf: line 55, column 42
    function sortValue_14 (attribute :  tdic.util.dataloader.data.admindata.AttributeData) : java.lang.Object {
      return attribute.Exclude
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadAttributeLV.pcf: line 29, column 41
    function sortValue_4 (attribute :  tdic.util.dataloader.data.admindata.AttributeData) : java.lang.Object {
      return attribute.Name
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadAttributeLV.pcf: line 35, column 41
    function sortValue_6 (attribute :  tdic.util.dataloader.data.admindata.AttributeData) : java.lang.Object {
      return attribute.PublicID
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at AdminDataUploadAttributeLV.pcf: line 40, column 41
    function sortValue_8 (attribute :  tdic.util.dataloader.data.admindata.AttributeData) : java.lang.Object {
      return attribute.Description
    }
    
    // 'value' attribute on RowIterator (id=Attribute) at AdminDataUploadAttributeLV.pcf: line 15, column 96
    function value_51 () : java.util.ArrayList<tdic.util.dataloader.data.admindata.AttributeData> {
      return processor.AttributeArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadAttributeLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadAttributeLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminDataUploadAttributeLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at AdminDataUploadAttributeLV.pcf: line 17, column 98
    function highlighted_50 () : java.lang.Boolean {
      return (attribute.Error or attribute.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadAttributeLV.pcf: line 23, column 46
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at AdminDataUploadAttributeLV.pcf: line 29, column 41
    function label_20 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Attribute.Name")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadAttributeLV.pcf: line 35, column 41
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Attribute.PublicID")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at AdminDataUploadAttributeLV.pcf: line 40, column 41
    function label_30 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Attribute.Description")
    }
    
    // 'label' attribute on TypeKeyCell (id=userAttributeType_Cell) at AdminDataUploadAttributeLV.pcf: line 45, column 50
    function label_35 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Attribute.AttributeType")
    }
    
    // 'label' attribute on BooleanRadioCell (id=active_Cell) at AdminDataUploadAttributeLV.pcf: line 50, column 42
    function label_40 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.Attribute.Active")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadAttributeLV.pcf: line 55, column 42
    function label_45 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadAttributeLV.pcf: line 29, column 41
    function valueRoot_22 () : java.lang.Object {
      return attribute
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadAttributeLV.pcf: line 23, column 46
    function value_16 () : java.lang.String {
      return processor.getLoadStatus(attribute)
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadAttributeLV.pcf: line 29, column 41
    function value_21 () : java.lang.String {
      return attribute.Name
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadAttributeLV.pcf: line 35, column 41
    function value_26 () : java.lang.String {
      return attribute.PublicID
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at AdminDataUploadAttributeLV.pcf: line 40, column 41
    function value_31 () : java.lang.String {
      return attribute.Description
    }
    
    // 'value' attribute on TypeKeyCell (id=userAttributeType_Cell) at AdminDataUploadAttributeLV.pcf: line 45, column 50
    function value_36 () : typekey.UserAttributeType {
      return attribute.UserAttributeType
    }
    
    // 'value' attribute on BooleanRadioCell (id=active_Cell) at AdminDataUploadAttributeLV.pcf: line 50, column 42
    function value_41 () : java.lang.Boolean {
      return attribute.Active
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadAttributeLV.pcf: line 55, column 42
    function value_46 () : java.lang.Boolean {
      return attribute.Exclude
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadAttributeLV.pcf: line 23, column 46
    function visible_17 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get attribute () : tdic.util.dataloader.data.admindata.AttributeData {
      return getIteratedValue(1) as tdic.util.dataloader.data.admindata.AttributeData
    }
    
    
  }
  
  
}