package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/shared/ChargeBreakdownItemsDisplayLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChargeBreakdownItemsDisplayLVExpressions {
  @javax.annotation.Generated("config/web/pcf/shared/ChargeBreakdownItemsDisplayLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChargeBreakdownItemsDisplayLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at ChargeBreakdownItemsDisplayLV.pcf: line 35, column 24
    function sortValue_0 (item :  entity.ChargeBreakdownItem) : java.lang.Object {
      return item.Description
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at ChargeBreakdownItemsDisplayLV.pcf: line 42, column 35
    function sortValue_1 (item :  entity.ChargeBreakdownItem) : java.lang.Object {
      return item.Amount
    }
    
    // '$$sumValue' attribute on RowIterator (id=ItemIterator) at ChargeBreakdownItemsDisplayLV.pcf: line 42, column 35
    function sumValueRoot_3 (item :  entity.ChargeBreakdownItem) : java.lang.Object {
      return item
    }
    
    // 'footerSumValue' attribute on RowIterator (id=ItemIterator) at ChargeBreakdownItemsDisplayLV.pcf: line 42, column 35
    function sumValue_2 (item :  entity.ChargeBreakdownItem) : java.lang.Object {
      return item.Amount
    }
    
    // 'value' attribute on RowIterator (id=ItemIterator) at ChargeBreakdownItemsDisplayLV.pcf: line 15, column 48
    function value_17 () : entity.ChargeBreakdownItem[] {
      return charge.BreakdownItems
    }
    
    property get charge () : entity.Charge {
      return getRequireValue("charge", 0) as entity.Charge
    }
    
    property set charge ($arg :  entity.Charge) {
      setRequireValue("charge", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/ChargeBreakdownItemsDisplayLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=Category_Input) at ChargeBreakdownItemsDisplayLV.pcf: line 27, column 45
    function valueRoot_5 () : java.lang.Object {
      return category
    }
    
    // 'value' attribute on TextInput (id=Category_Input) at ChargeBreakdownItemsDisplayLV.pcf: line 27, column 45
    function value_4 () : java.lang.String {
      return category.DisplayName
    }
    
    property get category () : entity.ChargeBreakdownCategory {
      return getIteratedValue(2) as entity.ChargeBreakdownCategory
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/shared/ChargeBreakdownItemsDisplayLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ChargeBreakdownItemsDisplayLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at ChargeBreakdownItemsDisplayLV.pcf: line 42, column 35
    function currency_15 () : typekey.Currency {
      return charge.BillingInstruction.Currency
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at ChargeBreakdownItemsDisplayLV.pcf: line 35, column 24
    function valueRoot_11 () : java.lang.Object {
      return item
    }
    
    // 'value' attribute on TextCell (id=Description_Cell) at ChargeBreakdownItemsDisplayLV.pcf: line 35, column 24
    function value_10 () : java.lang.String {
      return item.Description
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at ChargeBreakdownItemsDisplayLV.pcf: line 42, column 35
    function value_13 () : gw.pl.currency.MonetaryAmount {
      return item.Amount
    }
    
    // 'value' attribute on InputIterator at ChargeBreakdownItemsDisplayLV.pcf: line 24, column 68
    function value_7 () : List<entity.ChargeBreakdownCategory> {
      return item.Categories.sortBy(\category -> category.DisplayName)
    }
    
    // 'valueType' attribute on InputIterator at ChargeBreakdownItemsDisplayLV.pcf: line 24, column 68
    function verifyValueTypeIsAllowedType_8 ($$arg :  gw.api.database.IQueryBeanResult) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on InputIterator at ChargeBreakdownItemsDisplayLV.pcf: line 24, column 68
    function verifyValueTypeIsAllowedType_8 ($$arg :  gw.api.iterator.IteratorBackingData) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on InputIterator at ChargeBreakdownItemsDisplayLV.pcf: line 24, column 68
    function verifyValueTypeIsAllowedType_8 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueType' attribute on InputIterator at ChargeBreakdownItemsDisplayLV.pcf: line 24, column 68
    function verifyValueType_9 () : void {
      var __valueTypeArg : List<entity.ChargeBreakdownCategory>
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the valueType is not a valid type for use with an iterator
      // The valueType for an iterator must be an array or extend from List or IQueryBeanResult
      verifyValueTypeIsAllowedType_8(__valueTypeArg)
    }
    
    property get item () : entity.ChargeBreakdownItem {
      return getIteratedValue(1) as entity.ChargeBreakdownItem
    }
    
    
  }
  
  
}