package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.pl.currency.MonetaryAmount
@javax.annotation.Generated("config/web/pcf/payment/EditDBPaymentScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class EditDBPaymentScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/EditDBPaymentScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class EditDBPaymentScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=OverrideModeButton) at EditDBPaymentScreen.pcf: line 263, column 40
    function action_163 () : void {
      isInOverrideMode = !isInOverrideMode; resetDistItemsWithNewCriteria()
    }
    
    // 'action' attribute on ToolbarButton (id=AddItems) at EditDBPaymentScreen.pcf: line 310, column 88
    function action_187 () : void {
      DirectBillAddInvoiceItemsPopup.push( paymentView, AmountAvailableToDistributeLessNonReceivableAmount )
    }
    
    // 'action' attribute on ToolbarButton (id=GoButton) at EditDBPaymentScreen.pcf: line 373, column 82
    function action_231 () : void {
      invalidateDistItemsIterator()
    }
    
    // 'action' attribute on ToolbarButton (id=ClearButton) at EditDBPaymentScreen.pcf: line 377, column 85
    function action_232 () : void {
      paymentView.GoToNumber = null; invalidateDistItemsIterator();
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at EditDBPaymentScreen.pcf: line 85, column 38
    function action_24 () : void {
      AccountSearchPopup.push()
    }
    
    // 'action' attribute on ToolbarButton (id=ExecuteWithoutDistribution) at EditDBPaymentScreen.pcf: line 52, column 175
    function action_8 () : void {
      paymentView.setIsExecuteWithoutDistribution(); CurrentLocation.commit()
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at EditDBPaymentScreen.pcf: line 162, column 53
    function action_92 () : void {
      NewPaymentInstrumentPopup.push(tdic.bc.config.payment.PaymentInstrumentFilters.newPaymentInstrumentFilterCreditCardOnly, paymentView.TargetAccount, true, creditCardHandler)
    }
    
    // 'action' attribute on ToolbarButton (id=AddItems) at EditDBPaymentScreen.pcf: line 310, column 88
    function action_dest_188 () : pcf.api.Destination {
      return pcf.DirectBillAddInvoiceItemsPopup.createDestination( paymentView, AmountAvailableToDistributeLessNonReceivableAmount )
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at EditDBPaymentScreen.pcf: line 85, column 38
    function action_dest_25 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at EditDBPaymentScreen.pcf: line 162, column 53
    function action_dest_93 () : pcf.api.Destination {
      return pcf.NewPaymentInstrumentPopup.createDestination(tdic.bc.config.payment.PaymentInstrumentFilters.newPaymentInstrumentFilterCreditCardOnly, paymentView.TargetAccount, true, creditCardHandler)
    }
    
    // 'available' attribute on RangeInput (id=UnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 128, column 93
    function available_56 () : java.lang.Boolean {
      return paymentView.CreditDistribution ? anyUnappliedFundHasMoney() : true
    }
    
    // 'available' attribute on CheckBoxInput (id=UseUnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 138, column 82
    function available_72 () : java.lang.Boolean {
      return UnappliedAmountAvailable.IsPositive
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at EditDBPaymentScreen.pcf: line 114, column 57
    function currency_50 () : typekey.Currency {
      return paymentView.TargetAccount.Currency
    }
    
    // 'def' attribute on InputSetRef at EditDBPaymentScreen.pcf: line 208, column 77
    function def_onEnter_133 (def :  pcf.PaymentReceiptInputSet_ach) : void {
      def.onEnter(paymentView.MoneyReceived)
    }
    
    // 'def' attribute on InputSetRef at EditDBPaymentScreen.pcf: line 208, column 77
    function def_onEnter_135 (def :  pcf.PaymentReceiptInputSet_check) : void {
      def.onEnter(paymentView.MoneyReceived)
    }
    
    // 'def' attribute on InputSetRef at EditDBPaymentScreen.pcf: line 208, column 77
    function def_onEnter_137 (def :  pcf.PaymentReceiptInputSet_creditcard) : void {
      def.onEnter(paymentView.MoneyReceived)
    }
    
    // 'def' attribute on InputSetRef at EditDBPaymentScreen.pcf: line 208, column 77
    function def_onEnter_139 (def :  pcf.PaymentReceiptInputSet_default) : void {
      def.onEnter(paymentView.MoneyReceived)
    }
    
    // 'def' attribute on InputSetRef at EditDBPaymentScreen.pcf: line 208, column 77
    function def_onEnter_141 (def :  pcf.PaymentReceiptInputSet_wire) : void {
      def.onEnter(paymentView.MoneyReceived)
    }
    
    // 'def' attribute on InputSetRef at EditDBPaymentScreen.pcf: line 208, column 77
    function def_refreshVariables_134 (def :  pcf.PaymentReceiptInputSet_ach) : void {
      def.refreshVariables(paymentView.MoneyReceived)
    }
    
    // 'def' attribute on InputSetRef at EditDBPaymentScreen.pcf: line 208, column 77
    function def_refreshVariables_136 (def :  pcf.PaymentReceiptInputSet_check) : void {
      def.refreshVariables(paymentView.MoneyReceived)
    }
    
    // 'def' attribute on InputSetRef at EditDBPaymentScreen.pcf: line 208, column 77
    function def_refreshVariables_138 (def :  pcf.PaymentReceiptInputSet_creditcard) : void {
      def.refreshVariables(paymentView.MoneyReceived)
    }
    
    // 'def' attribute on InputSetRef at EditDBPaymentScreen.pcf: line 208, column 77
    function def_refreshVariables_140 (def :  pcf.PaymentReceiptInputSet_default) : void {
      def.refreshVariables(paymentView.MoneyReceived)
    }
    
    // 'def' attribute on InputSetRef at EditDBPaymentScreen.pcf: line 208, column 77
    function def_refreshVariables_142 (def :  pcf.PaymentReceiptInputSet_wire) : void {
      def.refreshVariables(paymentView.MoneyReceived)
    }
    
    // 'value' attribute on TypeKeyInput (id=CreditCardType_Input) at EditDBPaymentScreen.pcf: line 183, column 111
    function defaultSetter_114 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentView.MoneyReceived.PaymentInstrument.CreditCardType_TDIC = (__VALUE_TO_SET as typekey.CreditCardType_TDIC)
    }
    
    // 'value' attribute on TypeKeyInput (id=waiverreason_Input) at EditDBPaymentScreen.pcf: line 190, column 54
    function defaultSetter_120 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentView.MoneyReceived.CCFeeWaiverReason_TDIC = (__VALUE_TO_SET as typekey.CCFeeWaiverReason_TDIC)
    }
    
    // 'value' attribute on TextInput (id=waiverdesc_Input) at EditDBPaymentScreen.pcf: line 198, column 73
    function defaultSetter_125 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentView.MoneyReceived.CCFeeWaiverReasonDesc_TDIC = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CheckBoxInput (id=fundUsedForTransfer_Input) at EditDBPaymentScreen.pcf: line 204, column 71
    function defaultSetter_129 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentView.MoneyReceived.FundUsedForTransfer_TDIC = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=selectItemsWhere_Input) at EditDBPaymentScreen.pcf: line 281, column 60
    function defaultSetter_167 (__VALUE_TO_SET :  java.lang.Object) : void {
      accountRole = (__VALUE_TO_SET as typekey.DirectBillPmntAcctRole)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=selectItemsIs_Input) at EditDBPaymentScreen.pcf: line 292, column 57
    function defaultSetter_174 (__VALUE_TO_SET :  java.lang.Object) : void {
      accountsSelected = (__VALUE_TO_SET as typekey.DirectBillPmntAccts)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=includeOnly_Input) at EditDBPaymentScreen.pcf: line 302, column 59
    function defaultSetter_181 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentView.IncludeOnlyCriteria = (__VALUE_TO_SET as typekey.DistributionLimitType)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=GroupBy_Input) at EditDBPaymentScreen.pcf: line 328, column 60
    function defaultSetter_193 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentView.GroupByOption = (__VALUE_TO_SET as typekey.DBPmntGroupingCriteria)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=Aggregate_Input) at EditDBPaymentScreen.pcf: line 339, column 54
    function defaultSetter_202 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentView.AggregateOption = (__VALUE_TO_SET as typekey.DBPmntAggregateCriteria)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=InvoiceSortBy_Input) at EditDBPaymentScreen.pcf: line 350, column 101
    function defaultSetter_211 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentView.InvoiceSortOption = (__VALUE_TO_SET as typekey.DBPmntInvoiceSort)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=TAccountOwnerSortBy_Input) at EditDBPaymentScreen.pcf: line 360, column 107
    function defaultSetter_220 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentView.TAccountOwnerSortOption = (__VALUE_TO_SET as typekey.TAccountOwnerSort)
    }
    
    // 'value' attribute on TextValue (id=GoTo) at EditDBPaymentScreen.pcf: line 369, column 47
    function defaultSetter_228 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentView.GoToNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at EditDBPaymentScreen.pcf: line 85, column 38
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentView.TargetAccount = (__VALUE_TO_SET as entity.Account)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at EditDBPaymentScreen.pcf: line 114, column 57
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentView.MoneyReceived.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on RangeInput (id=UnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 128, column 93
    function defaultSetter_61 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentView.TargetUnappliedFund = (__VALUE_TO_SET as entity.UnappliedFund)
    }
    
    // 'value' attribute on CheckBoxInput (id=UseUnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 138, column 82
    function defaultSetter_77 (__VALUE_TO_SET :  java.lang.Object) : void {
      shouldUseUnappliedFund = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=CreditDistDescription_Input) at EditDBPaymentScreen.pcf: line 147, column 53
    function defaultSetter_85 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentView.MoneyReceived.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=PaymentInstrument_Input) at EditDBPaymentScreen.pcf: line 162, column 53
    function defaultSetter_98 (__VALUE_TO_SET :  java.lang.Object) : void {
      paymentView.MoneyReceived.PaymentInstrument = (__VALUE_TO_SET as entity.PaymentInstrument)
    }
    
    // 'dismissed' attribute on AlertBar (id=FrozenInvoiceItemAlert) at EditDBPaymentScreen.pcf: line 58, column 89
    function dismissed_11 () : java.lang.Boolean {
      return paymentView._dismissedAlertBar
    }
    
    // 'editable' attribute on InputSetRef at EditDBPaymentScreen.pcf: line 208, column 77
    function editable_132 () : java.lang.Boolean {
      return !paymentView.isCreditDistribution() and !paymentView.isMove()
    }
    
    // 'editable' attribute on MonetaryAmountInput (id=Amount_Input) at EditDBPaymentScreen.pcf: line 114, column 57
    function editable_44 () : java.lang.Boolean {
      return !paymentView.isMove() and !paymentView.Modifying
    }
    
    // 'editable' attribute on RangeInput (id=UnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 128, column 93
    function editable_57 () : java.lang.Boolean {
      return paymentView.CanChangeUnapplied
    }
    
    // 'editable' attribute on CheckBoxInput (id=UseUnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 138, column 82
    function editable_73 () : java.lang.Boolean {
      return paymentView.Modifying || paymentView.CreditDistribution
    }
    
    // 'editable' attribute on RangeInput (id=PaymentInstrument_Input) at EditDBPaymentScreen.pcf: line 162, column 53
    function editable_94 () : java.lang.Boolean {
      return !paymentView.isCreditDistribution() and !paymentView.isMove() and !paymentView.Modifying and paymentView.MoneyReceived.PaymentInstrument.PaymentMethod != typekey.PaymentMethod.TC_CREDITCARD
    }
    
    // 'initialValue' attribute on Variable at EditDBPaymentScreen.pcf: line 20, column 23
    function initialValue_0 () : boolean {
      return initializeShouldUseUnappliedFund()
    }
    
    // 'initialValue' attribute on Variable at EditDBPaymentScreen.pcf: line 25, column 69
    function initialValue_1 () : List<gw.api.web.payment.DBPaymentDistItemGroup> {
      return paymentView.Groups
    }
    
    // 'initialValue' attribute on Variable at EditDBPaymentScreen.pcf: line 29, column 38
    function initialValue_2 () : DirectBillPmntAcctRole {
      return DirectBillPmntAcctRole.TC_PAYER
    }
    
    // 'initialValue' attribute on Variable at EditDBPaymentScreen.pcf: line 33, column 35
    function initialValue_3 () : DirectBillPmntAccts {
      return DirectBillPmntAccts.TC_ACCOUNT
    }
    
    // 'initialValue' attribute on Variable at EditDBPaymentScreen.pcf: line 37, column 57
    function initialValue_4 () : gw.api.web.account.AccountSearchConverter {
      return new gw.api.web.account.AccountSearchConverter()
    }
    
    // 'initialValue' attribute on Variable at EditDBPaymentScreen.pcf: line 41, column 49
    function initialValue_5 () : gw.payment.PaymentInstrumentRange {
      return new gw.payment.PaymentInstrumentRange((paymentView.TargetAccount.PaymentInstruments))
    }
    
    // 'inputConversion' attribute on TextInput (id=AccountNumber_Input) at EditDBPaymentScreen.pcf: line 85, column 38
    function inputConversion_29 (VALUE :  java.lang.String) : java.lang.Object {
      return accountSearchConverter.getAccount(VALUE)
    }
    
    // 'label' attribute on Label at EditDBPaymentScreen.pcf: line 212, column 215
    function label_144 () : java.lang.String {
      return (paymentView.isCreditDistribution()) ? DisplayKey.get("Web.NewDirectBillPayment.ThisCreditDistribution.Label") : DisplayKey.get("Web.NewDirectBillPayment.ThisPayment.Label") 
    }
    
    // 'label' attribute on Label at EditDBPaymentScreen.pcf: line 99, column 215
    function label_39 () : java.lang.String {
      return  (paymentView.isCreditDistribution()) ? DisplayKey.get("Web.NewDirectBillPayment.CreditDistributionData.Label") : DisplayKey.get("Web.NewDirectBillPayment.PaymentData.Label")
    }
    
    // 'label' attribute on CheckBoxInput (id=UseUnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 138, column 82
    function label_75 () : java.lang.Object {
      return DisplayKey.get("Web.NewDirectBillPayment.PaymentData.UseUnappliedFundAmount", UnappliedAmountAvailable.render())
    }
    
    // 'label' attribute on Label at EditDBPaymentScreen.pcf: line 152, column 54
    function label_90 () : java.lang.String {
      return (paymentView.isCreditDistribution()) ? DisplayKey.get("Web.NewDirectBillPayment.CreditDistributionDetails") :DisplayKey.get("Web.NewDirectBillPayment.PaymentDetails")
    }
    
    // 'mode' attribute on InputSetRef at EditDBPaymentScreen.pcf: line 208, column 77
    function mode_143 () : java.lang.Object {
      return paymentView.MoneyReceived.PaymentInstrument.PaymentMethod
    }
    
    // 'onChange' attribute on PostOnChange at EditDBPaymentScreen.pcf: line 283, column 63
    function onChange_165 () : void {
      resetDistItemsWithNewCriteria()
    }
    
    // 'onChange' attribute on PostOnChange at EditDBPaymentScreen.pcf: line 294, column 63
    function onChange_172 () : void {
      resetDistItemsWithNewCriteria()
    }
    
    // 'onChange' attribute on PostOnChange at EditDBPaymentScreen.pcf: line 304, column 63
    function onChange_179 () : void {
      resetDistItemsWithNewCriteria()
    }
    
    // 'onChange' attribute on PostOnChange at EditDBPaymentScreen.pcf: line 330, column 61
    function onChange_191 () : void {
      invalidateDistItemsIterator()
    }
    
    // 'onChange' attribute on PostOnChange at EditDBPaymentScreen.pcf: line 341, column 111
    function onChange_199 () : void {
      paymentView.setDismissedAlertBar(paymentView.AggregateBySummary ? false : true)
    }
    
    // 'onChange' attribute on PostOnChange at EditDBPaymentScreen.pcf: line 92, column 52
    function onChange_23 () : void {
      resetIncludeOnlyCriteria()
    }
    
    // 'onChange' attribute on PostOnChange at EditDBPaymentScreen.pcf: line 116, column 159
    function onChange_43 () : void {
      updateWaiver(); if (!paymentView.DoNotEditDistribution) { if (isInOverrideMode) recalculateDistribution() else reallocatePayment() };
    }
    
    // 'onChange' attribute on PostOnChange at EditDBPaymentScreen.pcf: line 130, column 84
    function onChange_55 () : void {
      shouldUseUnappliedFund = false; recalculateDistribution();
    }
    
    // 'onChange' attribute on PostOnChange at EditDBPaymentScreen.pcf: line 140, column 98
    function onChange_71 () : void {
      if (isInOverrideMode) recalculateDistribution() else reallocatePayment()
    }
    
    // 'onPick' attribute on TextInput (id=AccountNumber_Input) at EditDBPaymentScreen.pcf: line 85, column 38
    function onPick_28 (PickedValue :  Account) : void {
      resetIncludeOnlyCriteria()
    }
    
    // 'onPick' attribute on RangeInput (id=PaymentInstrument_Input) at EditDBPaymentScreen.pcf: line 162, column 53
    function onPick_96 (PickedValue :  PaymentInstrument) : void {
      paymentInstrumentRange.addPaymentInstrument(paymentView.MoneyReceived.PaymentInstrument)
    }
    
    // 'requestValidationExpression' attribute on TypeKeyInput (id=waiverreason_Input) at EditDBPaymentScreen.pcf: line 190, column 54
    function requestValidationExpression_118 (VALUE :  typekey.CCFeeWaiverReason_TDIC) : java.lang.Object {
      return (VALUE == CCFeeWaiverReason_TDIC.TC_LESSTHANOREQUALTO100 and paymentView.MoneyReceived.Amount.Amount > 100) ? DisplayKey.get("TDIC.Validator.CreditCard.Fee.WaiverReason", VALUE):null
    }
    
    // 'required' attribute on TextInput (id=waiverdesc_Input) at EditDBPaymentScreen.pcf: line 198, column 73
    function required_123 () : java.lang.Boolean {
      return paymentView.MoneyReceived.CCFeeWaiverReason_TDIC != null && paymentView.MoneyReceived.CCFeeWaiverReason_TDIC != CCFeeWaiverReason_TDIC.TC_LESSTHANOREQUALTO100
    }
    
    // 'dismissed' attribute on AlertBar (id=FrozenInvoiceItemAlert) at EditDBPaymentScreen.pcf: line 58, column 89
    function setDismissed_12 (__VALUE :  java.lang.Boolean) : void {
      paymentView._dismissedAlertBar = __VALUE
    }
    
    // 'showConfirmMessage' attribute on ToolbarButton (id=ExecuteWithoutDistribution) at EditDBPaymentScreen.pcf: line 52, column 175
    function showConfirmMessage_9 () : java.lang.Boolean {
      return paymentView.OneOrMoreItemsHaveOverrides or paymentView.Payment.SuspDistItemsThatHaveNotBeenReleased.HasElements
    }
    
    // 'value' attribute on TextCell (id=InvoiceNumber_Cell) at EditDBPaymentScreen.pcf: line 565, column 52
    function sortValue_308 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      var invoice : AccountInvoice = (paymentView.isGroupByInvoice() ? (group as gw.api.web.payment.DBPaymentDistItemsForInvoice).Invoice as AccountInvoice : null)
return invoice.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at EditDBPaymentScreen.pcf: line 572, column 49
    function sortValue_309 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      var invoice : AccountInvoice = (paymentView.isGroupByInvoice() ? (group as gw.api.web.payment.DBPaymentDistItemsForInvoice).Invoice as AccountInvoice : null)
return invoice.Account
    }
    
    // 'value' attribute on DateCell (id=InvoiceDate_Cell) at EditDBPaymentScreen.pcf: line 576, column 48
    function sortValue_310 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      var invoice : AccountInvoice = (paymentView.isGroupByInvoice() ? (group as gw.api.web.payment.DBPaymentDistItemsForInvoice).Invoice as AccountInvoice : null)
return invoice.EventDate
    }
    
    // 'value' attribute on DateCell (id=InvoiceDueDate_Cell) at EditDBPaymentScreen.pcf: line 580, column 46
    function sortValue_311 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      var invoice : AccountInvoice = (paymentView.isGroupByInvoice() ? (group as gw.api.web.payment.DBPaymentDistItemsForInvoice).Invoice as AccountInvoice : null)
return invoice.DueDate
    }
    
    // 'value' attribute on TextCell (id=InvoiceStatus_Cell) at EditDBPaymentScreen.pcf: line 584, column 50
    function sortValue_312 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      var invoice : AccountInvoice = (paymentView.isGroupByInvoice() ? (group as gw.api.web.payment.DBPaymentDistItemsForInvoice).Invoice as AccountInvoice : null)
return invoice.StatusForUI
    }
    
    // 'value' attribute on MonetaryAmountCell (id=InvoiceUnpaidAmount_Cell) at EditDBPaymentScreen.pcf: line 591, column 49
    function sortValue_313 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      return group.UnpaidAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=DistributionAmount_Cell) at EditDBPaymentScreen.pcf: line 598, column 47
    function sortValue_314 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      return group.Allocation
    }
    
    // 'value' attribute on TextCell (id=TAccountType_Cell) at EditDBPaymentScreen.pcf: line 641, column 113
    function sortValue_360 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      var tAccountOwner : TAccountOwner = (paymentView.isGroupByTAccountOwner() ? (group as gw.api.web.payment.DBPaymentDistItemsForTAccountOwner).TAccountOwner : null)
return (group as gw.api.web.payment.DBPaymentDistItemsForTAccountOwner).TAccountOwnerType
    }
    
    // 'value' attribute on TextCell (id=AggAccountNumber_Cell) at EditDBPaymentScreen.pcf: line 649, column 49
    function sortValue_361 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      var tAccountOwner : TAccountOwner = (paymentView.isGroupByTAccountOwner() ? (group as gw.api.web.payment.DBPaymentDistItemsForTAccountOwner).TAccountOwner : null)
return getAccount(tAccountOwner)
    }
    
    // 'value' attribute on TextCell (id=AggPolicyNumber_Cell) at EditDBPaymentScreen.pcf: line 657, column 35
    function sortValue_362 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      var tAccountOwner : TAccountOwner = (paymentView.isGroupByTAccountOwner() ? (group as gw.api.web.payment.DBPaymentDistItemsForTAccountOwner).TAccountOwner : null)
return getPolicyPeriod(tAccountOwner)
    }
    
    // 'value' attribute on DateCell (id=PolicyEffectiveDate_Cell) at EditDBPaymentScreen.pcf: line 662, column 104
    function sortValue_363 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      var tAccountOwner : TAccountOwner = (paymentView.isGroupByTAccountOwner() ? (group as gw.api.web.payment.DBPaymentDistItemsForTAccountOwner).TAccountOwner : null)
return tAccountOwner typeis PolicyPeriod ? tAccountOwner.PolicyPerEffDate : null
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnbilledBalance_Cell) at EditDBPaymentScreen.pcf: line 669, column 51
    function sortValue_364 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      return group.UnbilledAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CurrentBalance_Cell) at EditDBPaymentScreen.pcf: line 676, column 49
    function sortValue_365 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      return group.BilledAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PastDueBalance_Cell) at EditDBPaymentScreen.pcf: line 683, column 46
    function sortValue_366 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      return group.DueAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalBalance_Cell) at EditDBPaymentScreen.pcf: line 690, column 43
    function sortValue_367 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      return group.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PaidBalance_Cell) at EditDBPaymentScreen.pcf: line 697, column 47
    function sortValue_368 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      return group.PaidAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnpaidBalance_Cell) at EditDBPaymentScreen.pcf: line 704, column 49
    function sortValue_369 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      return group.UnpaidAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AggDistributionAmount_Cell) at EditDBPaymentScreen.pcf: line 711, column 47
    function sortValue_370 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      return group.Allocation
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at EditDBPaymentScreen.pcf: line 765, column 35
    function sortValue_446 (item :  entity.BaseSuspDistItem) : java.lang.Object {
      return item.PolicyNumber
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at EditDBPaymentScreen.pcf: line 784, column 51
    function sortValue_447 (item :  entity.BaseSuspDistItem) : java.lang.Object {
      return item.PaymentComments
    }
    
    // 'value' attribute on TextCell (id=Collateral_Cell) at EditDBPaymentScreen.pcf: line 823, column 52
    function sortValue_476 (item :  entity.CollateralPaymentItem) : java.lang.Object {
      return paymentView.TargetAccount.Collateral
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at EditDBPaymentScreen.pcf: line 842, column 51
    function sortValue_477 (item :  entity.CollateralPaymentItem) : java.lang.Object {
      return item.PaymentComments
    }
    
    // '$$sumValue' attribute on RowIterator (id=groupsAggInvoice) at EditDBPaymentScreen.pcf: line 591, column 49
    function sumValueRoot_317 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      return group
    }
    
    // '$$sumValue' attribute on RowIterator (id=SuspenseItems) at EditDBPaymentScreen.pcf: line 776, column 53
    function sumValueRoot_449 (item :  entity.BaseSuspDistItem) : java.lang.Object {
      return item
    }
    
    // '$$sumValue' attribute on RowIterator (id=CollateralItems) at EditDBPaymentScreen.pcf: line 834, column 53
    function sumValueRoot_479 (item :  entity.CollateralPaymentItem) : java.lang.Object {
      return item
    }
    
    // 'footerSumValue' attribute on RowIterator (id=groupsAggInvoice) at EditDBPaymentScreen.pcf: line 591, column 49
    function sumValue_316 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      return group.UnpaidAmount
    }
    
    // 'footerSumValue' attribute on RowIterator (id=groupsAggInvoice) at EditDBPaymentScreen.pcf: line 598, column 47
    function sumValue_318 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      return group.Allocation
    }
    
    // 'footerSumValue' attribute on RowIterator (id=groupsAggTAccountOwner) at EditDBPaymentScreen.pcf: line 669, column 51
    function sumValue_372 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      return group.UnbilledAmount
    }
    
    // 'footerSumValue' attribute on RowIterator (id=groupsAggTAccountOwner) at EditDBPaymentScreen.pcf: line 676, column 49
    function sumValue_374 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      return group.BilledAmount
    }
    
    // 'footerSumValue' attribute on RowIterator (id=groupsAggTAccountOwner) at EditDBPaymentScreen.pcf: line 683, column 46
    function sumValue_376 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      return group.DueAmount
    }
    
    // 'footerSumValue' attribute on RowIterator (id=groupsAggTAccountOwner) at EditDBPaymentScreen.pcf: line 690, column 43
    function sumValue_378 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      return group.Amount
    }
    
    // 'footerSumValue' attribute on RowIterator (id=groupsAggTAccountOwner) at EditDBPaymentScreen.pcf: line 697, column 47
    function sumValue_380 (group :  gw.api.web.payment.DBPaymentDistItemGroup) : java.lang.Object {
      return group.PaidAmount
    }
    
    // 'footerSumValue' attribute on RowIterator (id=SuspenseItems) at EditDBPaymentScreen.pcf: line 776, column 53
    function sumValue_448 (item :  entity.BaseSuspDistItem) : java.lang.Object {
      return item.GrossAmountToApply
    }
    
    // 'footerSumValue' attribute on RowIterator (id=CollateralItems) at EditDBPaymentScreen.pcf: line 834, column 53
    function sumValue_478 (item :  entity.CollateralPaymentItem) : java.lang.Object {
      return item.GrossAmountToApply
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=SuspenseItems) at EditDBPaymentScreen.pcf: line 754, column 55
    function toCreateAndAdd_471 () : entity.BaseSuspDistItem {
      var item = paymentView.MoneyReceived.BaseDist.createAndAddSuspDistItem(); recalculateDistribution(); return item;
    }
    
    // 'toCreateAndAdd' attribute on RowIterator (id=CollateralItems) at EditDBPaymentScreen.pcf: line 813, column 80
    function toCreateAndAdd_498 () : entity.CollateralPaymentItem {
      var item =  paymentView.Payment.createAndAddCollateralPaymentItem(); recalculateDistribution(); return item;
    }
    
    // 'toRemove' attribute on RowIterator (id=SuspenseItems) at EditDBPaymentScreen.pcf: line 754, column 55
    function toRemove_472 (item :  entity.BaseSuspDistItem) : void {
      item.release(); recalculateDistribution();
    }
    
    // 'toRemove' attribute on RowIterator (id=CollateralItems) at EditDBPaymentScreen.pcf: line 813, column 80
    function toRemove_499 (item :  entity.CollateralPaymentItem) : void {
      item.removeFromPayment(); recalculateDistribution();
    }
    
    // 'validationExpression' attribute on TextInput (id=AccountNumber_Input) at EditDBPaymentScreen.pcf: line 85, column 38
    function validationExpression_27 () : java.lang.Object {
      return !paymentView.Payment.Account.HasDesignatedUnappliedFund ? validateAccountNumber() : null;
    }
    
    // 'validationExpression' attribute on ListViewPanel (id=DistributionAmountsLV) at EditDBPaymentScreen.pcf: line 381, column 54
    function validationExpression_444 () : java.lang.Object {
      return validateItems()
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=Amount_Input) at EditDBPaymentScreen.pcf: line 114, column 57
    function validationExpression_45 () : java.lang.Object {
      return paymentView.MoneyReceived.Amount.IsZero && !paymentView.isCreditDistribution() ?  DisplayKey.get("Web.NewDirectBillPayment.Error.InvalidAmount") : null
    }
    
    // 'validationExpression' attribute on RangeInput (id=UnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 128, column 93
    function validationExpression_58 () : java.lang.Object {
      return validateDesignatedUnapplied()
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at EditDBPaymentScreen.pcf: line 162, column 53
    function valueRange_100 () : java.lang.Object {
      return gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, tdic.bc.config.payment.PaymentInstrumentFilters.newPaymentInstrumentFilterCreditCardOnly)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=selectItemsWhere_Input) at EditDBPaymentScreen.pcf: line 281, column 60
    function valueRange_168 () : java.lang.Object {
      return DirectBillPmntAcctRole.getTypeKeys(false)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=selectItemsIs_Input) at EditDBPaymentScreen.pcf: line 292, column 57
    function valueRange_175 () : java.lang.Object {
      return DirectBillPmntAccts.getTypeKeys(false)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=includeOnly_Input) at EditDBPaymentScreen.pcf: line 302, column 59
    function valueRange_183 () : java.lang.Object {
      return DistributionLimitType.getTypeKeys(false)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=GroupBy_Input) at EditDBPaymentScreen.pcf: line 328, column 60
    function valueRange_195 () : java.lang.Object {
      return DBPmntGroupingCriteria.getTypeKeys(false)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=Aggregate_Input) at EditDBPaymentScreen.pcf: line 339, column 54
    function valueRange_204 () : java.lang.Object {
      return DBPmntAggregateCriteria.getTypeKeys(false)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=InvoiceSortBy_Input) at EditDBPaymentScreen.pcf: line 350, column 101
    function valueRange_213 () : java.lang.Object {
      return typekey.DBPmntInvoiceSort.getTypeKeys(false)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=TAccountOwnerSortBy_Input) at EditDBPaymentScreen.pcf: line 360, column 107
    function valueRange_222 () : java.lang.Object {
      return typekey.TAccountOwnerSort.getTypeKeys(false)
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 128, column 93
    function valueRange_63 () : java.lang.Object {
      return tdic.bc.integ.services.creditcard.TDIC_PCFHelper.getUnappliedFunds(paymentView.Payment.Account)
    }
    
    // 'value' attribute on TypeKeyInput (id=CreditCardType_Input) at EditDBPaymentScreen.pcf: line 183, column 111
    function valueRoot_115 () : java.lang.Object {
      return paymentView.MoneyReceived.PaymentInstrument
    }
    
    // 'value' attribute on TextInput (id=OriginatingAccountName_Input) at EditDBPaymentScreen.pcf: line 69, column 43
    function valueRoot_15 () : java.lang.Object {
      return paymentView
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at EditDBPaymentScreen.pcf: line 97, column 67
    function valueRoot_37 () : java.lang.Object {
      return paymentView.TargetAccount
    }
    
    // 'value' attribute on DateInput (id=PaymentDate_Input) at EditDBPaymentScreen.pcf: line 104, column 59
    function valueRoot_41 () : java.lang.Object {
      return paymentView.MoneyReceived
    }
    
    // 'value' attribute on TypeKeyInput (id=CreditCardType_Input) at EditDBPaymentScreen.pcf: line 183, column 111
    function value_113 () : typekey.CreditCardType_TDIC {
      return paymentView.MoneyReceived.PaymentInstrument.CreditCardType_TDIC
    }
    
    // 'value' attribute on TypeKeyInput (id=waiverreason_Input) at EditDBPaymentScreen.pcf: line 190, column 54
    function value_119 () : typekey.CCFeeWaiverReason_TDIC {
      return paymentView.MoneyReceived.CCFeeWaiverReason_TDIC
    }
    
    // 'value' attribute on TextInput (id=waiverdesc_Input) at EditDBPaymentScreen.pcf: line 198, column 73
    function value_124 () : java.lang.String {
      return paymentView.MoneyReceived.CCFeeWaiverReasonDesc_TDIC
    }
    
    // 'value' attribute on CheckBoxInput (id=fundUsedForTransfer_Input) at EditDBPaymentScreen.pcf: line 204, column 71
    function value_128 () : java.lang.Boolean {
      return paymentView.MoneyReceived.FundUsedForTransfer_TDIC
    }
    
    // 'value' attribute on TextInput (id=OriginatingAccountName_Input) at EditDBPaymentScreen.pcf: line 69, column 43
    function value_14 () : entity.Account {
      return paymentView.OriginatingAccount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=AmountAvailableToDistribute_Input) at EditDBPaymentScreen.pcf: line 220, column 83
    function value_146 () : gw.pl.currency.MonetaryAmount {
      return AmountAvailableToDistribute
    }
    
    // 'value' attribute on MonetaryAmountInput (id=DistributedAmount_Input) at EditDBPaymentScreen.pcf: line 227, column 38
    function value_150 () : gw.pl.currency.MonetaryAmount {
      return DistributedAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=SuspenseAmount_Input) at EditDBPaymentScreen.pcf: line 234, column 35
    function value_153 () : gw.pl.currency.MonetaryAmount {
      return SuspenseAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=CollateralAmount_Input) at EditDBPaymentScreen.pcf: line 241, column 37
    function value_156 () : gw.pl.currency.MonetaryAmount {
      return CollateralAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=RemainingAmount_Input) at EditDBPaymentScreen.pcf: line 248, column 36
    function value_159 () : gw.pl.currency.MonetaryAmount {
      return RemainingAmount
    }
    
    // 'value' attribute on ToolbarRangeInput (id=selectItemsWhere_Input) at EditDBPaymentScreen.pcf: line 281, column 60
    function value_166 () : typekey.DirectBillPmntAcctRole {
      return accountRole
    }
    
    // 'value' attribute on ToolbarRangeInput (id=selectItemsIs_Input) at EditDBPaymentScreen.pcf: line 292, column 57
    function value_173 () : typekey.DirectBillPmntAccts {
      return accountsSelected
    }
    
    // 'value' attribute on ToolbarRangeInput (id=includeOnly_Input) at EditDBPaymentScreen.pcf: line 302, column 59
    function value_180 () : typekey.DistributionLimitType {
      return paymentView.IncludeOnlyCriteria
    }
    
    // 'value' attribute on TextInput (id=OriginatingUnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 75, column 119
    function value_19 () : entity.UnappliedFund {
      return paymentView.OriginatingUnappliedFund
    }
    
    // 'value' attribute on ToolbarRangeInput (id=GroupBy_Input) at EditDBPaymentScreen.pcf: line 328, column 60
    function value_192 () : typekey.DBPmntGroupingCriteria {
      return paymentView.GroupByOption
    }
    
    // 'value' attribute on ToolbarRangeInput (id=Aggregate_Input) at EditDBPaymentScreen.pcf: line 339, column 54
    function value_201 () : typekey.DBPmntAggregateCriteria {
      return paymentView.AggregateOption
    }
    
    // 'value' attribute on ToolbarRangeInput (id=InvoiceSortBy_Input) at EditDBPaymentScreen.pcf: line 350, column 101
    function value_210 () : typekey.DBPmntInvoiceSort {
      return paymentView.InvoiceSortOption
    }
    
    // 'value' attribute on ToolbarRangeInput (id=TAccountOwnerSortBy_Input) at EditDBPaymentScreen.pcf: line 360, column 107
    function value_219 () : typekey.TAccountOwnerSort {
      return paymentView.TAccountOwnerSortOption
    }
    
    // 'value' attribute on TextValue (id=GoTo) at EditDBPaymentScreen.pcf: line 369, column 47
    function value_227 () : java.lang.String {
      return paymentView.GoToNumber
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at EditDBPaymentScreen.pcf: line 85, column 38
    function value_30 () : entity.Account {
      return paymentView.TargetAccount
    }
    
    // 'value' attribute on RowIterator (id=groups) at EditDBPaymentScreen.pcf: line 430, column 59
    function value_307 () : java.util.List<gw.api.web.payment.DBPaymentDistItemGroup> {
      return itemGroups
    }
    
    // 'value' attribute on TextInput (id=AccountName_Input) at EditDBPaymentScreen.pcf: line 97, column 67
    function value_36 () : java.lang.String {
      return paymentView.TargetAccount.AccountNameLocalized
    }
    
    // 'value' attribute on DateInput (id=PaymentDate_Input) at EditDBPaymentScreen.pcf: line 104, column 59
    function value_40 () : java.util.Date {
      return paymentView.MoneyReceived.ReceivedDate
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at EditDBPaymentScreen.pcf: line 114, column 57
    function value_47 () : gw.pl.currency.MonetaryAmount {
      return paymentView.MoneyReceived.Amount
    }
    
    // 'value' attribute on RowIterator (id=SuspenseItems) at EditDBPaymentScreen.pcf: line 754, column 55
    function value_473 () : entity.BaseSuspDistItem[] {
      return paymentView.MoneyReceived.BaseDist.SuspDistItemsThatHaveNotBeenReleased
    }
    
    // 'value' attribute on RowIterator (id=CollateralItems) at EditDBPaymentScreen.pcf: line 813, column 80
    function value_500 () : java.util.List<entity.CollateralPaymentItem> {
      return (paymentView.MoneyReceived.BaseDist as DirectBillPayment).NonReversedCollateralItems
    }
    
    // 'value' attribute on RangeInput (id=UnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 128, column 93
    function value_60 () : entity.UnappliedFund {
      return paymentView.TargetUnappliedFund
    }
    
    // 'value' attribute on CheckBoxInput (id=UseUnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 138, column 82
    function value_76 () : java.lang.Boolean {
      return shouldUseUnappliedFund
    }
    
    // 'value' attribute on TextInput (id=CreditDistDescription_Input) at EditDBPaymentScreen.pcf: line 147, column 53
    function value_84 () : java.lang.String {
      return paymentView.MoneyReceived.Description
    }
    
    // 'value' attribute on RangeInput (id=PaymentInstrument_Input) at EditDBPaymentScreen.pcf: line 162, column 53
    function value_97 () : entity.PaymentInstrument {
      return paymentView.MoneyReceived.PaymentInstrument
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at EditDBPaymentScreen.pcf: line 162, column 53
    function verifyValueRangeIsAllowedType_101 ($$arg :  entity.PaymentInstrument[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at EditDBPaymentScreen.pcf: line 162, column 53
    function verifyValueRangeIsAllowedType_101 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentInstrument>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at EditDBPaymentScreen.pcf: line 162, column 53
    function verifyValueRangeIsAllowedType_101 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=selectItemsWhere_Input) at EditDBPaymentScreen.pcf: line 281, column 60
    function verifyValueRangeIsAllowedType_169 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=selectItemsWhere_Input) at EditDBPaymentScreen.pcf: line 281, column 60
    function verifyValueRangeIsAllowedType_169 ($$arg :  typekey.DirectBillPmntAcctRole[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=selectItemsIs_Input) at EditDBPaymentScreen.pcf: line 292, column 57
    function verifyValueRangeIsAllowedType_176 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=selectItemsIs_Input) at EditDBPaymentScreen.pcf: line 292, column 57
    function verifyValueRangeIsAllowedType_176 ($$arg :  typekey.DirectBillPmntAccts[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=includeOnly_Input) at EditDBPaymentScreen.pcf: line 302, column 59
    function verifyValueRangeIsAllowedType_184 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=includeOnly_Input) at EditDBPaymentScreen.pcf: line 302, column 59
    function verifyValueRangeIsAllowedType_184 ($$arg :  typekey.DistributionLimitType[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=GroupBy_Input) at EditDBPaymentScreen.pcf: line 328, column 60
    function verifyValueRangeIsAllowedType_196 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=GroupBy_Input) at EditDBPaymentScreen.pcf: line 328, column 60
    function verifyValueRangeIsAllowedType_196 ($$arg :  typekey.DBPmntGroupingCriteria[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=Aggregate_Input) at EditDBPaymentScreen.pcf: line 339, column 54
    function verifyValueRangeIsAllowedType_205 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=Aggregate_Input) at EditDBPaymentScreen.pcf: line 339, column 54
    function verifyValueRangeIsAllowedType_205 ($$arg :  typekey.DBPmntAggregateCriteria[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=InvoiceSortBy_Input) at EditDBPaymentScreen.pcf: line 350, column 101
    function verifyValueRangeIsAllowedType_214 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=InvoiceSortBy_Input) at EditDBPaymentScreen.pcf: line 350, column 101
    function verifyValueRangeIsAllowedType_214 ($$arg :  typekey.DBPmntInvoiceSort[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=TAccountOwnerSortBy_Input) at EditDBPaymentScreen.pcf: line 360, column 107
    function verifyValueRangeIsAllowedType_223 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=TAccountOwnerSortBy_Input) at EditDBPaymentScreen.pcf: line 360, column 107
    function verifyValueRangeIsAllowedType_223 ($$arg :  typekey.TAccountOwnerSort[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 128, column 93
    function verifyValueRangeIsAllowedType_64 ($$arg :  entity.UnappliedFund[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 128, column 93
    function verifyValueRangeIsAllowedType_64 ($$arg :  gw.api.database.IQueryBeanResult<entity.UnappliedFund>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 128, column 93
    function verifyValueRangeIsAllowedType_64 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at EditDBPaymentScreen.pcf: line 162, column 53
    function verifyValueRange_102 () : void {
      var __valueRangeArg = gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, tdic.bc.config.payment.PaymentInstrumentFilters.newPaymentInstrumentFilterCreditCardOnly)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_101(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=selectItemsWhere_Input) at EditDBPaymentScreen.pcf: line 281, column 60
    function verifyValueRange_170 () : void {
      var __valueRangeArg = DirectBillPmntAcctRole.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_169(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=selectItemsIs_Input) at EditDBPaymentScreen.pcf: line 292, column 57
    function verifyValueRange_177 () : void {
      var __valueRangeArg = DirectBillPmntAccts.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_176(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=includeOnly_Input) at EditDBPaymentScreen.pcf: line 302, column 59
    function verifyValueRange_185 () : void {
      var __valueRangeArg = DistributionLimitType.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_184(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=GroupBy_Input) at EditDBPaymentScreen.pcf: line 328, column 60
    function verifyValueRange_197 () : void {
      var __valueRangeArg = DBPmntGroupingCriteria.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_196(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=Aggregate_Input) at EditDBPaymentScreen.pcf: line 339, column 54
    function verifyValueRange_206 () : void {
      var __valueRangeArg = DBPmntAggregateCriteria.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_205(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=InvoiceSortBy_Input) at EditDBPaymentScreen.pcf: line 350, column 101
    function verifyValueRange_215 () : void {
      var __valueRangeArg = typekey.DBPmntInvoiceSort.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_214(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=TAccountOwnerSortBy_Input) at EditDBPaymentScreen.pcf: line 360, column 107
    function verifyValueRange_224 () : void {
      var __valueRangeArg = typekey.TAccountOwnerSort.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_223(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=UnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 128, column 93
    function verifyValueRange_65 () : void {
      var __valueRangeArg = tdic.bc.integ.services.creditcard.TDIC_PCFHelper.getUnappliedFunds(paymentView.Payment.Account)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_64(__valueRangeArg)
    }
    
    // 'visible' attribute on AlertBar (id=FrozenInvoiceItemAlert) at EditDBPaymentScreen.pcf: line 58, column 89
    function visible_10 () : java.lang.Boolean {
      return paymentView.AggregateBySummary && paymentView.hasFrozenItems()
    }
    
    // 'visible' attribute on TypeKeyInput (id=CreditCardType_Input) at EditDBPaymentScreen.pcf: line 183, column 111
    function visible_112 () : java.lang.Boolean {
      return paymentView.MoneyReceived.PaymentInstrument.PaymentMethod == PaymentMethod.TC_CREDITCARD
    }
    
    // 'visible' attribute on TextInput (id=OriginatingAccountName_Input) at EditDBPaymentScreen.pcf: line 69, column 43
    function visible_13 () : java.lang.Boolean {
      return paymentView.isMove()
    }
    
    // 'visible' attribute on ToolbarButton (id=OverrideModeButton) at EditDBPaymentScreen.pcf: line 263, column 40
    function visible_162 () : java.lang.Boolean {
      return !isInOverrideMode
    }
    
    // 'visible' attribute on Toolbar (id=OverrideToolbar) at EditDBPaymentScreen.pcf: line 257, column 54
    function visible_164 () : java.lang.Boolean {
      return !paymentView.DoNotEditDistribution
    }
    
    // 'visible' attribute on TextInput (id=OriginatingUnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 75, column 119
    function visible_18 () : java.lang.Boolean {
      return paymentView.isMove() and paymentView.OriginatingUnappliedFund.Account.HasDesignatedUnappliedFund
    }
    
    // 'visible' attribute on Toolbar at EditDBPaymentScreen.pcf: line 273, column 42
    function visible_189 () : java.lang.Boolean {
      return isInOverrideMode
    }
    
    // 'visible' attribute on PanelRef (id=SelectItemsPanel) at EditDBPaymentScreen.pcf: line 271, column 96
    function visible_190 () : java.lang.Boolean {
      return !paymentView.DoNotEditDistribution and paymentView.TargetAccount != null
    }
    
    // 'visible' attribute on ToolbarRangeInput (id=Aggregate_Input) at EditDBPaymentScreen.pcf: line 339, column 54
    function visible_200 () : java.lang.Boolean {
      return !paymentView.GroupByCustom
    }
    
    // 'visible' attribute on ToolbarRangeInput (id=InvoiceSortBy_Input) at EditDBPaymentScreen.pcf: line 350, column 101
    function visible_209 () : java.lang.Boolean {
      return paymentView.isAggregateByItem() && paymentView.isGroupByInvoice()
    }
    
    // 'visible' attribute on ToolbarRangeInput (id=TAccountOwnerSortBy_Input) at EditDBPaymentScreen.pcf: line 360, column 107
    function visible_218 () : java.lang.Boolean {
      return paymentView.isAggregateByItem() && paymentView.isGroupByTAccountOwner()
    }
    
    // 'visible' attribute on Row at EditDBPaymentScreen.pcf: line 384, column 59
    function visible_234 () : java.lang.Boolean {
      return paymentView.isAggregateByItem()
    }
    
    // 'visible' attribute on RowIterator (id=groupsAggInvoice) at EditDBPaymentScreen.pcf: line 555, column 104
    function visible_322 () : java.lang.Boolean {
      return paymentView.isAggregateBySummary() && paymentView.isGroupByInvoice()
    }
    
    // 'visible' attribute on RowIterator (id=groupsAggTAccountOwner) at EditDBPaymentScreen.pcf: line 630, column 110
    function visible_388 () : java.lang.Boolean {
      return paymentView.isAggregateBySummary() && paymentView.isGroupByTAccountOwner()
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=Amount_Input) at EditDBPaymentScreen.pcf: line 114, column 57
    function visible_46 () : java.lang.Boolean {
      return !paymentView.isCreditDistribution()
    }
    
    // 'visible' attribute on Card (id=SuspenseItemCard) at EditDBPaymentScreen.pcf: line 734, column 138
    function visible_474 () : java.lang.Boolean {
      return isInOverrideMode or (paymentView.Modifying and paymentView.Payment.SuspDistItemsThatHaveNotBeenReleased.HasElements)
    }
    
    // 'addVisible' attribute on IteratorButtons (id=collateralButtons) at EditDBPaymentScreen.pcf: line 800, column 46
    function visible_475 () : java.lang.Boolean {
      return  paymentView.Payment.NonReversedCollateralItems.Count < 1
    }
    
    // 'visible' attribute on Card (id=CollateralItemCard) at EditDBPaymentScreen.pcf: line 793, column 175
    function visible_501 () : java.lang.Boolean {
      return (isInOverrideMode  or (paymentView.Modifying and paymentView.Payment.NonReversedCollateralItems.HasElements)) and !paymentView.TargetAccount.isListBill()
    }
    
    // 'visible' attribute on RangeInput (id=UnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 128, column 93
    function visible_59 () : java.lang.Boolean {
      return paymentView.Payment.Account.DesignatedUnappliedFundsOrdered.HasElements
    }
    
    // 'updateVisible' attribute on EditButtons at EditDBPaymentScreen.pcf: line 45, column 242
    function visible_6 () : java.lang.Boolean {
      return (paymentView.Payment.DistItems.HasElements or paymentView.Payment.SuspDistItemsThatHaveNotBeenReleased.HasElements or paymentView.Payment.NonReversedCollateralItems.HasElements) and paymentView.TargetAccount != null
    }
    
    // 'visible' attribute on ToolbarButton (id=ExecuteWithoutDistribution) at EditDBPaymentScreen.pcf: line 52, column 175
    function visible_7 () : java.lang.Boolean {
      return paymentView.CanExecuteWithoutDistribution and paymentView.TargetAccount != null and ( paymentView.Modifying or !paymentView.Payment.DistItems.HasElements)
    }
    
    // 'visible' attribute on CheckBoxInput (id=UseUnappliedFunds_Input) at EditDBPaymentScreen.pcf: line 138, column 82
    function visible_74 () : java.lang.Boolean {
      return !paymentView.DoNotEditDistribution and !paymentView.isMove()
    }
    
    // 'visible' attribute on TextInput (id=CreditDistDescription_Input) at EditDBPaymentScreen.pcf: line 147, column 53
    function visible_83 () : java.lang.Boolean {
      return paymentView.CreditDistribution
    }
    
    // 'visible' attribute on Label at EditDBPaymentScreen.pcf: line 152, column 54
    function visible_89 () : java.lang.Boolean {
      return !paymentView.CreditDistribution
    }
    
    // 'visible' attribute on MenuItem (id=CreateNewPaymentInstrument) at EditDBPaymentScreen.pcf: line 162, column 53
    function visible_91 () : java.lang.Boolean {
      return !paymentView.isCreditDistribution() and !paymentView.isMove() and !paymentView.Modifying
    }
    
    property get accountRole () : DirectBillPmntAcctRole {
      return getVariableValue("accountRole", 0) as DirectBillPmntAcctRole
    }
    
    property set accountRole ($arg :  DirectBillPmntAcctRole) {
      setVariableValue("accountRole", 0, $arg)
    }
    
    property get accountSearchConverter () : gw.api.web.account.AccountSearchConverter {
      return getVariableValue("accountSearchConverter", 0) as gw.api.web.account.AccountSearchConverter
    }
    
    property set accountSearchConverter ($arg :  gw.api.web.account.AccountSearchConverter) {
      setVariableValue("accountSearchConverter", 0, $arg)
    }
    
    property get accountsSelected () : DirectBillPmntAccts {
      return getVariableValue("accountsSelected", 0) as DirectBillPmntAccts
    }
    
    property set accountsSelected ($arg :  DirectBillPmntAccts) {
      setVariableValue("accountsSelected", 0, $arg)
    }
    
    property get creditCardHandler () : tdic.bc.integ.services.creditcard.TDIC_CreditCardHandler {
      return getRequireValue("creditCardHandler", 0) as tdic.bc.integ.services.creditcard.TDIC_CreditCardHandler
    }
    
    property set creditCardHandler ($arg :  tdic.bc.integ.services.creditcard.TDIC_CreditCardHandler) {
      setRequireValue("creditCardHandler", 0, $arg)
    }
    
    property get isInOverrideMode () : Boolean {
      return getVariableValue("isInOverrideMode", 0) as Boolean
    }
    
    property set isInOverrideMode ($arg :  Boolean) {
      setVariableValue("isInOverrideMode", 0, $arg)
    }
    
    property get itemGroups () : List<gw.api.web.payment.DBPaymentDistItemGroup> {
      return getVariableValue("itemGroups", 0) as List<gw.api.web.payment.DBPaymentDistItemGroup>
    }
    
    property set itemGroups ($arg :  List<gw.api.web.payment.DBPaymentDistItemGroup>) {
      setVariableValue("itemGroups", 0, $arg)
    }
    
    property get paymentInstrumentRange () : gw.payment.PaymentInstrumentRange {
      return getVariableValue("paymentInstrumentRange", 0) as gw.payment.PaymentInstrumentRange
    }
    
    property set paymentInstrumentRange ($arg :  gw.payment.PaymentInstrumentRange) {
      setVariableValue("paymentInstrumentRange", 0, $arg)
    }
    
    property get paymentView () : gw.api.web.payment.DirectBillPaymentView {
      return getRequireValue("paymentView", 0) as gw.api.web.payment.DirectBillPaymentView
    }
    
    property set paymentView ($arg :  gw.api.web.payment.DirectBillPaymentView) {
      setRequireValue("paymentView", 0, $arg)
    }
    
    property get shouldUseUnappliedFund () : boolean {
      return getVariableValue("shouldUseUnappliedFund", 0) as java.lang.Boolean
    }
    
    property set shouldUseUnappliedFund ($arg :  boolean) {
      setVariableValue("shouldUseUnappliedFund", 0, $arg)
    }
    
    
    function validateItems(): String {
    
      if(paymentView.Payment.DistItems.hasMatch( \distItem -> distItem.InvoiceItem.Retired )){
        resetDistItemsWithNewCriteria()
        return DisplayKey.get("Web.NewDirectBillPayment.InvoiceItemRetiredConcurrently")
      }
    
      if (!paymentView.isExecuteWithoutDistribution()){
        if (isEmptyPayment()){
          return DisplayKey.get("Web.NewDirectBillPayment.AtLeastOneItemMustHaveAnAllocation")
        }
    
        if (paymentView.Payment.DistItems.sum(paymentView.Currency, \b -> b.GrossAmountToApply).add(paymentView.Payment.SuspDistItemsThatHaveNotBeenReleased.sum(paymentView.Currency, \b -> b.GrossAmountToApply)).add(paymentView.Payment.NonReversedCollateralItems.sum(paymentView.Currency, \b -> b.GrossAmountToApply))
                > AmountAvailableToDistribute) {
          return DisplayKey.get("Web.NewDirectBillPayment.SumOfPaymentItemsIsHigherThanAmountAvailable")
        }
    
        if (isInOverrideMode){
          return paymentView.isModifying() ? verifyModifiedItems() : verifyNewlyAddedItems()
        }
      }
      if(paymentView.isOriginalMoneyDistributed() && isEmptyPayment()){
        return DisplayKey.get("Web.NewDirectBillPayment.CannotExecuteEmptyPayment")
      }
      return null
    }
    
      private function isEmptyPayment() : boolean{
        return !paymentView.Payment.DistItems.hasMatch(\b -> !b.GrossAmountToApply.IsZero)
            and !paymentView.Payment.SuspDistItemsThatHaveNotBeenReleased.HasElements
            and !paymentView.Payment.NonReversedCollateralItems.HasElements
      }
    
    function validateAccountNumber(): String {
      if (paymentView.TargetAccount == null) {
        return DisplayKey.get("Java.Error.InsufficientInformation")
      } else if (paymentView.MovedToSelf){
        if (paymentView.TargetAccount.PolicyLevelBillingWithDesignatedUnapplied) {
          return DisplayKey.get("Web.MoveDirectBillPayment.CannotMoveToSamePolicy")
        } else {
          return DisplayKey.get("Web.NewDirectBillPayment.CannotMoveToSelf")
        }
      }
      return null
    }
    
    function validateDesignatedUnapplied(): String {
      if (paymentView.MovedToSelf){
        return DisplayKey.get("Web.MoveDirectBillPayment.CannotMoveToSamePolicy")
      }
      return null
    }
    
    function verifyModifiedItems(): String {
      var modifiedItems = (paymentView as gw.api.web.payment.ModifyingDirectBillPaymentView).PotentiallyModifiedItems
      for (modifiedItem in modifiedItems) {
        if (modifiedItem.DifferentFromModifiedItem){
          if (modifiedItem.GrossAmountOwed < modifiedItem.GrossAmountToApply and modifiedItem.InvoiceItem.Amount.IsPositive){
              return DisplayKey.get("Web.NewDirectBillPayment.AmountAllocatedIsTooHigh")
          }
          if (modifiedItem.GrossAmountOwed > modifiedItem.GrossAmountToApply and modifiedItem.InvoiceItem.Amount.IsNegative){
              return DisplayKey.get("Web.NewDirectBillPayment.NegativeItemAllocatedTooMuch")
          }
          if ((modifiedItem.GrossAmountOwed.Amount > 0 and modifiedItem.GrossAmountToApply.Amount < 0) or
            (modifiedItem.GrossAmountOwed.Amount < 0 and modifiedItem.GrossAmountToApply.Amount > 0)) {
            return DisplayKey.get("Web.NewDirectBillPayment.DifferentSigns")
          }
        }
      }
      return null
    }
    
    function verifyNewlyAddedItems(): String {
      var newlyAddedItems = paymentView.NewlyAddedItems
      if (newlyAddedItems.hasMatch(\b -> b.GrossAmountToApply > b.InvoiceItem.GrossUnsettledAmount and b.InvoiceItem.GrossUnsettledAmount.IsPositive)){
        return DisplayKey.get("Web.NewDirectBillPayment.AmountAllocatedIsTooHigh")
      }
      if (newlyAddedItems.hasMatch(\b -> b.GrossAmountToApply < b.InvoiceItem.GrossUnsettledAmount and b.InvoiceItem.GrossUnsettledAmount.IsNegative)){
        return DisplayKey.get("Web.NewDirectBillPayment.NegativeItemAllocatedTooMuch")
      }
      if (paymentView.Payment.DistItems.hasMatch(\b -> not (
              (b.InvoiceItem.GrossUnsettledAmount.Amount > 0 and b.GrossAmountToApply.Amount >= 0) or
                      (b.InvoiceItem.GrossUnsettledAmount.Amount < 0 and b.GrossAmountToApply.Amount <= 0)))){
          //correct if the unpaid amount is positive the gross amount to apply has to be zero or positive,  if the unpaid amount is negative the gross amount to apply has to be negative or zero (ie not positive)
        return DisplayKey.get("Web.NewDirectBillPayment.DifferentSigns")
      }
      return null
    }
    
    property get AmountAvailableToDistribute(): gw.pl.currency.MonetaryAmount {
      if (paymentView.MoneyReceived.Amount == null) {
        return 0bd.ofCurrency(paymentView.Currency)
      }
      return shouldUseUnappliedFund
              ? paymentView.MoneyReceived.Amount.add(UnappliedAmountAvailable)
              : paymentView.MoneyReceived.Amount
    }
    
    property get AmountAvailableToDistributeLessNonReceivableAmount(): gw.pl.currency.MonetaryAmount {
      return AmountAvailableToDistribute.subtract(SuspenseAmount).subtract(CollateralAmount)
    }
    
    property get DistributedAmount(): gw.pl.currency.MonetaryAmount {
      return itemGroups.sum(paymentView.Currency, \group -> group.DistItems.sum(paymentView.Currency, \distItem -> distItem.GrossAmountToApply))
    }
    
    property get SuspenseAmount(): gw.pl.currency.MonetaryAmount {
      return paymentView.Payment.SuspDistItemsThatHaveNotBeenReleased.sum(paymentView.Currency, \b -> b.GrossAmountToApply)
    }
    
    property get CollateralAmount(): gw.pl.currency.MonetaryAmount {
      return paymentView.Payment.NonReversedCollateralItems.sum(paymentView.Currency, \b -> b.GrossAmountToApply)
    }
    
    property get RemainingAmount(): gw.pl.currency.MonetaryAmount {
      return AmountAvailableToDistribute.subtract(DistributedAmount).subtract(CollateralAmount).subtract(SuspenseAmount)
    }
    
    property get UnappliedAmountAvailable(): gw.pl.currency.MonetaryAmount {
      if (!paymentView.Modifying) {
        return paymentView.TargetUnappliedFund.Balance
      }
      var modifiedMoney = (paymentView as gw.api.web.payment.ModifyingDirectBillPaymentView).MoneyBeingModified
      var maximumAmountAvailable = paymentView.TargetUnappliedFund.Balance - modifiedMoney.UndistributedAmount
      if (modifiedMoney.DistributedDenorm){
        maximumAmountAvailable = maximumAmountAvailable + modifiedMoney.AmountDistributedFromUnapplied
      }
      return maximumAmountAvailable.IsPositive ? maximumAmountAvailable : 0bd.ofCurrency(maximumAmountAvailable.Currency)
    }
    
    function initializeShouldUseUnappliedFund(): boolean {
      //note: moneyReceived hasn't been initialized Yet when this is called so have to call through the payment view
      return (paymentView typeis gw.api.web.payment.ModifyingDirectBillPaymentView )
              and paymentView.MoneyBeingModified.AmountDistributedFromUnapplied.IsPositive
    }
    
    function invalidateDistItemsIterator() {
      gw.api.web.PebblesUtil.invalidateIterators(CurrentLocation, BaseDistItem)
    }
    
    function getAccount(tAccountOwner: TAccountOwner): Account {
      if (tAccountOwner typeis Account) {
        return tAccountOwner
      } else if (tAccountOwner typeis Collateral) {
        return tAccountOwner.Account
      } else if (tAccountOwner typeis CollateralRequirement) {
        return tAccountOwner.Collateral.Account
      } else if (tAccountOwner typeis PolicyPeriod) {
        return tAccountOwner.Account
      }
      return null
    }
    
    function getPolicyPeriod(tAccountOwner: TAccountOwner): PolicyPeriod {
      if (tAccountOwner typeis PolicyPeriod) {
        return tAccountOwner
      }
      return null
    }
    
        function resetIncludeOnlyCriteria(){
          if (paymentView.TargetAccount.ListBill) {
            paymentView.IncludeOnlyCriteria = DistributionLimitType.TC_OUTSTANDING
          } else {
            paymentView.IncludeOnlyCriteria = paymentView.TargetAccount.DistributionLimitTypeFromPlan;
          }
          resetDistItemsWithNewCriteria()
    
        }
    function resetDistItemsWithNewCriteria() {
      paymentView.repopulatePaymentItems(accountRole, accountsSelected, paymentView.IncludeOnlyCriteria)
      invalidateDistItemsIterator()
      recalculateDistribution()
    }
    
    function reallocatePayment() {
      resetNonOverriddenOrFrozenItems()
      
      var frozenItemsTotal = paymentView.AggregateBySummary ? 
          itemGroups.where( \ b -> b.hasfrozenDistItem()).sum(paymentView.Currency, \ b -> b.Allocation) : 
          paymentView.Payment.DistItemsList.where(\ b -> b.Frozen).sum(paymentView.Currency, \ b -> b.GrossAmountToApply)
      
      var amountAvailableToDistributeLessNonRecievableAmountAndLessFrozenAmount = AmountAvailableToDistributeLessNonReceivableAmount.subtract(frozenItemsTotal)
      
      if(amountAvailableToDistributeLessNonRecievableAmountAndLessFrozenAmount.IsPositive) {
        paymentView.Plugin.allocatePayment(paymentView.Payment, amountAvailableToDistributeLessNonRecievableAmountAndLessFrozenAmount)
      }
    }
    
    function getNonOverriddenOrFrozenItems() : List<BaseDistItem> {
      var baseDistItems = paymentView.Payment.DistItemsList    
      var overrideInvoiceItems = paymentView.OverrideItemsAndAmount.keySet()
      
      return baseDistItems.where(\ b -> !overrideInvoiceItems.contains(b.InvoiceItem) && !b.Frozen)
    }
    
    function getNonOverriddenOrFrozenGroups() : List<BaseDistItem> {  
      var itemGroupsWithFrozenItem = itemGroups.where(\ b -> b.hasfrozenDistItem())  
      var overrideInvoiceItems = paymentView.OverrideItemsAndAmount.keySet()
      
      return paymentView.Payment.DistItems.where(\ b -> 
          not overrideInvoiceItems.contains(b.InvoiceItem) and itemGroupsWithFrozenItem.where(\ c -> c.DistItems.contains(b)).Empty).toList()
    }
    
      function recalculateDistribution() {
      if (paymentView.MoneyReceived.Amount == null) return;
      resetNonOverriddenOrFrozenItems()
      paymentView.recalculateDistribution(AmountAvailableToDistributeLessNonReceivableAmount)
    }
    
    function resetNonOverriddenOrFrozenItems() {
      var notOverriddenOrFrozenItems = paymentView.AggregateBySummary ? getNonOverriddenOrFrozenGroups() : getNonOverriddenOrFrozenItems()
      notOverriddenOrFrozenItems.each( \ item -> {item.GrossAmountToApply = 0bd.ofCurrency(item.Currency)})
    }
    
    function updateWaiver() {
      if (paymentView.MoneyReceived.Amount.Amount <= 100) {
        paymentView.MoneyReceived.CCFeeWaiverReason_TDIC = CCFeeWaiverReason_TDIC.TC_LESSTHANOREQUALTO100
      } else {
        paymentView.MoneyReceived.CCFeeWaiverReason_TDIC = null
      } 
    }
    
    function anyUnappliedFundHasMoney() : boolean {
      var account = paymentView.Payment.Account
      return account.getDefaultUnappliedFund().Balance.IsPositive ||
          account.DesignatedUnappliedFunds.hasMatch(\unappliedFund -> unappliedFund.Balance.IsPositive)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/payment/EditDBPaymentScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry2ExpressionsImpl extends IteratorEntryExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=InvoiceItemPolicyNumber_Cell) at EditDBPaymentScreen.pcf: line 486, column 37
    function action_262 () : void {
      PolicySummary.push(invoiceItem.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=itemOwner_Cell) at EditDBPaymentScreen.pcf: line 504, column 51
    function action_276 () : void {
      AccountSummaryPopup.push(invoiceItem.Owner)
    }
    
    // 'action' attribute on TextCell (id=itemPayer_Cell) at EditDBPaymentScreen.pcf: line 510, column 71
    function action_281 () : void {
      if ( invoiceItem.Payer typeis Account ) { AccountSummaryPopup.push(invoiceItem.Payer) } else { ProducerDetailPopup.push(invoiceItem.Payer as Producer) }
    }
    
    // 'action' attribute on TextCell (id=InvoiceItemPolicyNumber_Cell) at EditDBPaymentScreen.pcf: line 486, column 37
    function action_dest_263 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(invoiceItem.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=itemOwner_Cell) at EditDBPaymentScreen.pcf: line 504, column 51
    function action_dest_277 () : pcf.api.Destination {
      return pcf.AccountSummaryPopup.createDestination(invoiceItem.Owner)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at EditDBPaymentScreen.pcf: line 517, column 50
    function currency_287 () : typekey.Currency {
      return distItem.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at EditDBPaymentScreen.pcf: line 540, column 50
    function defaultSetter_300 (__VALUE_TO_SET :  java.lang.Object) : void {
      overrideAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'editable' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at EditDBPaymentScreen.pcf: line 540, column 50
    function editable_298 () : java.lang.Boolean {
      return isInOverrideMode and !invoiceItem.Frozen
    }
    
    // 'iconLabel' attribute on BooleanRadioCell (id=ManuallyAdded_Cell) at EditDBPaymentScreen.pcf: line 473, column 52
    function iconLabel_252 () : java.lang.String {
      return isManuallyAddedItem ? DisplayKey.get("DirectBillDistItem.ManuallyAdded") : null
    }
    
    // 'initialValue' attribute on Variable at EditDBPaymentScreen.pcf: line 452, column 41
    function initialValue_248 () : InvoiceItem {
      return distItem.InvoiceItem
    }
    
    // 'initialValue' attribute on Variable at EditDBPaymentScreen.pcf: line 456, column 37
    function initialValue_249 () : Invoice {
      return invoiceItem.Invoice
    }
    
    // 'initialValue' attribute on Variable at EditDBPaymentScreen.pcf: line 461, column 59
    function initialValue_250 () : gw.pl.currency.MonetaryAmount {
      return paymentView.OverrideItemsAndAmount.get(distItem.InvoiceItem)
    }
    
    // 'initialValue' attribute on Variable at EditDBPaymentScreen.pcf: line 465, column 37
    function initialValue_251 () : boolean {
      return paymentView.isManuallyAddedItem(distItem.InvoiceItem)
    }
    
    // RowIterator (id=distItems) at EditDBPaymentScreen.pcf: line 448, column 73
    function initializeVariables_305 () : void {
        invoiceItem = distItem.InvoiceItem;
  invoice = invoiceItem.Invoice;
  overrideAmount = paymentView.OverrideItemsAndAmount.get(distItem.InvoiceItem);
  isManuallyAddedItem = paymentView.isManuallyAddedItem(distItem.InvoiceItem);

    }
    
    // 'onChange' attribute on PostOnChange at EditDBPaymentScreen.pcf: line 542, column 120
    function onChange_297 () : void {
      paymentView.onOverrideChange(distItem, overrideAmount); recalculateDistribution();
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at EditDBPaymentScreen.pcf: line 479, column 53
    function valueRange_257 () : java.lang.Object {
      return gw.api.web.invoice.InvoiceUtil.getUserVisibleInvoiceItemTypes().map(\ t -> t.Code)
    }
    
    // 'value' attribute on RangeCell (id=Description_Cell) at EditDBPaymentScreen.pcf: line 479, column 53
    function valueRoot_256 () : java.lang.Object {
      return invoiceItem
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at EditDBPaymentScreen.pcf: line 490, column 66
    function valueRoot_268 () : java.lang.Object {
      return invoiceItem.Invoice
    }
    
    // 'value' attribute on MonetaryAmountCell (id=DistributionAmount_Cell) at EditDBPaymentScreen.pcf: line 532, column 60
    function valueRoot_294 () : java.lang.Object {
      return distItem
    }
    
    // 'value' attribute on BooleanRadioCell (id=ManuallyAdded_Cell) at EditDBPaymentScreen.pcf: line 473, column 52
    function value_253 () : java.lang.Boolean {
      return isManuallyAddedItem
    }
    
    // 'value' attribute on RangeCell (id=Description_Cell) at EditDBPaymentScreen.pcf: line 479, column 53
    function value_255 () : java.lang.String {
      return invoiceItem.DisplayNameAsItemType
    }
    
    // 'value' attribute on TextCell (id=InvoiceItemPolicyNumber_Cell) at EditDBPaymentScreen.pcf: line 486, column 37
    function value_264 () : entity.PolicyPeriod {
      return invoiceItem.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=InstallmentNumber_Cell) at EditDBPaymentScreen.pcf: line 490, column 66
    function value_267 () : java.lang.String {
      return invoiceItem.Invoice.InvoiceNumber
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at EditDBPaymentScreen.pcf: line 498, column 59
    function value_273 () : java.util.Date {
      return invoiceItem.InvoiceDueDate
    }
    
    // 'value' attribute on TextCell (id=itemOwner_Cell) at EditDBPaymentScreen.pcf: line 504, column 51
    function value_278 () : entity.Account {
      return invoiceItem.Owner
    }
    
    // 'value' attribute on TextCell (id=itemPayer_Cell) at EditDBPaymentScreen.pcf: line 510, column 71
    function value_282 () : gw.api.domain.invoice.InvoicePayer {
      return invoiceItem.Payer
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at EditDBPaymentScreen.pcf: line 517, column 50
    function value_285 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnpaidAmount_Cell) at EditDBPaymentScreen.pcf: line 526, column 65
    function value_289 () : gw.pl.currency.MonetaryAmount {
      return invoiceItem.GrossUnsettledAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=DistributionAmount_Cell) at EditDBPaymentScreen.pcf: line 532, column 60
    function value_293 () : gw.pl.currency.MonetaryAmount {
      return distItem.GrossAmountToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at EditDBPaymentScreen.pcf: line 540, column 50
    function value_299 () : gw.pl.currency.MonetaryAmount {
      return overrideAmount
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at EditDBPaymentScreen.pcf: line 479, column 53
    function verifyValueRangeIsAllowedType_258 ($$arg :  java.lang.String[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at EditDBPaymentScreen.pcf: line 479, column 53
    function verifyValueRangeIsAllowedType_258 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeCell (id=Description_Cell) at EditDBPaymentScreen.pcf: line 479, column 53
    function verifyValueRange_259 () : void {
      var __valueRangeArg = gw.api.web.invoice.InvoiceUtil.getUserVisibleInvoiceItemTypes().map(\ t -> t.Code)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_258(__valueRangeArg)
    }
    
    // 'valueVisible' attribute on TextCell (id=InvoiceItemPolicyNumber_Cell) at EditDBPaymentScreen.pcf: line 486, column 37
    function visible_261 () : java.lang.Boolean {
      return invoiceItem.PolicyPeriod != null
    }
    
    // 'visible' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at EditDBPaymentScreen.pcf: line 540, column 50
    function visible_303 () : java.lang.Boolean {
      return isInOverrideMode
    }
    
    property get distItem () : entity.BaseDistItem {
      return getIteratedValue(2) as entity.BaseDistItem
    }
    
    property get invoice () : Invoice {
      return getVariableValue("invoice", 2) as Invoice
    }
    
    property set invoice ($arg :  Invoice) {
      setVariableValue("invoice", 2, $arg)
    }
    
    property get invoiceItem () : InvoiceItem {
      return getVariableValue("invoiceItem", 2) as InvoiceItem
    }
    
    property set invoiceItem ($arg :  InvoiceItem) {
      setVariableValue("invoiceItem", 2, $arg)
    }
    
    property get isManuallyAddedItem () : boolean {
      return getVariableValue("isManuallyAddedItem", 2) as java.lang.Boolean
    }
    
    property set isManuallyAddedItem ($arg :  boolean) {
      setVariableValue("isManuallyAddedItem", 2, $arg)
    }
    
    property get overrideAmount () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("overrideAmount", 2) as gw.pl.currency.MonetaryAmount
    }
    
    property set overrideAmount ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("overrideAmount", 2, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/payment/EditDBPaymentScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry3ExpressionsImpl extends EditDBPaymentScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at EditDBPaymentScreen.pcf: line 572, column 49
    function action_327 () : void {
      AccountSummary.push(invoice.Account)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at EditDBPaymentScreen.pcf: line 572, column 49
    function action_dest_328 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination(invoice.Account)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=InvoiceUnpaidAmount_Cell) at EditDBPaymentScreen.pcf: line 591, column 49
    function currency_343 () : typekey.Currency {
      return invoice.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at EditDBPaymentScreen.pcf: line 607, column 48
    function defaultSetter_352 (__VALUE_TO_SET :  java.lang.Object) : void {
      group.OverrideAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'editable' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at EditDBPaymentScreen.pcf: line 607, column 48
    function editable_350 () : java.lang.Boolean {
      return isInOverrideMode and !invoice.Frozen
    }
    
    // 'initialValue' attribute on Variable at EditDBPaymentScreen.pcf: line 559, column 42
    function initialValue_323 () : AccountInvoice {
      return paymentView.isGroupByInvoice() ? (group as gw.api.web.payment.DBPaymentDistItemsForInvoice).Invoice as AccountInvoice : null
    }
    
    // RowIterator (id=groupsAggInvoice) at EditDBPaymentScreen.pcf: line 555, column 104
    function initializeVariables_358 () : void {
        invoice = paymentView.isGroupByInvoice() ? (group as gw.api.web.payment.DBPaymentDistItemsForInvoice).Invoice as AccountInvoice : null;

    }
    
    // 'onChange' attribute on PostOnChange at EditDBPaymentScreen.pcf: line 610, column 61
    function onChange_349 () : void {
      recalculateDistribution()
    }
    
    // 'value' attribute on TextCell (id=InvoiceNumber_Cell) at EditDBPaymentScreen.pcf: line 565, column 52
    function valueRoot_325 () : java.lang.Object {
      return invoice
    }
    
    // 'value' attribute on MonetaryAmountCell (id=InvoiceUnpaidAmount_Cell) at EditDBPaymentScreen.pcf: line 591, column 49
    function valueRoot_342 () : java.lang.Object {
      return group
    }
    
    // 'value' attribute on TextCell (id=InvoiceNumber_Cell) at EditDBPaymentScreen.pcf: line 565, column 52
    function value_324 () : java.lang.String {
      return invoice.InvoiceNumber
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at EditDBPaymentScreen.pcf: line 572, column 49
    function value_329 () : entity.Account {
      return invoice.Account
    }
    
    // 'value' attribute on DateCell (id=InvoiceDate_Cell) at EditDBPaymentScreen.pcf: line 576, column 48
    function value_332 () : java.util.Date {
      return invoice.EventDate
    }
    
    // 'value' attribute on DateCell (id=InvoiceDueDate_Cell) at EditDBPaymentScreen.pcf: line 580, column 46
    function value_335 () : java.util.Date {
      return invoice.DueDate
    }
    
    // 'value' attribute on TextCell (id=InvoiceStatus_Cell) at EditDBPaymentScreen.pcf: line 584, column 50
    function value_338 () : java.lang.String {
      return invoice.StatusForUI
    }
    
    // 'value' attribute on MonetaryAmountCell (id=InvoiceUnpaidAmount_Cell) at EditDBPaymentScreen.pcf: line 591, column 49
    function value_341 () : gw.pl.currency.MonetaryAmount {
      return group.UnpaidAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=DistributionAmount_Cell) at EditDBPaymentScreen.pcf: line 598, column 47
    function value_345 () : gw.pl.currency.MonetaryAmount {
      return group.Allocation
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at EditDBPaymentScreen.pcf: line 607, column 48
    function value_351 () : gw.pl.currency.MonetaryAmount {
      return group.OverrideAmount
    }
    
    // 'visible' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at EditDBPaymentScreen.pcf: line 607, column 48
    function visible_356 () : java.lang.Boolean {
      return isInOverrideMode
    }
    
    property get group () : gw.api.web.payment.DBPaymentDistItemGroup {
      return getIteratedValue(1) as gw.api.web.payment.DBPaymentDistItemGroup
    }
    
    property get invoice () : AccountInvoice {
      return getVariableValue("invoice", 1) as AccountInvoice
    }
    
    property set invoice ($arg :  AccountInvoice) {
      setVariableValue("invoice", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/payment/EditDBPaymentScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry4ExpressionsImpl extends EditDBPaymentScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=AggAccountNumber_Cell) at EditDBPaymentScreen.pcf: line 649, column 49
    function action_393 () : void {
      AccountSummary.push(getAccount(tAccountOwner))
    }
    
    // 'action' attribute on TextCell (id=AggPolicyNumber_Cell) at EditDBPaymentScreen.pcf: line 657, column 35
    function action_398 () : void {
      PolicySummary.push(getPolicyPeriod(tAccountOwner))
    }
    
    // 'action' attribute on TextCell (id=AggAccountNumber_Cell) at EditDBPaymentScreen.pcf: line 649, column 49
    function action_dest_394 () : pcf.api.Destination {
      return pcf.AccountSummary.createDestination(getAccount(tAccountOwner))
    }
    
    // 'action' attribute on TextCell (id=AggPolicyNumber_Cell) at EditDBPaymentScreen.pcf: line 657, column 35
    function action_dest_399 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(getPolicyPeriod(tAccountOwner))
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=UnbilledBalance_Cell) at EditDBPaymentScreen.pcf: line 669, column 51
    function currency_406 () : typekey.Currency {
      return tAccountOwner.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at EditDBPaymentScreen.pcf: line 721, column 48
    function defaultSetter_436 (__VALUE_TO_SET :  java.lang.Object) : void {
      group.OverrideAmount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'editable' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at EditDBPaymentScreen.pcf: line 721, column 48
    function editable_433 () : java.lang.Boolean {
      return isInOverrideMode and !group.hasfrozenDistItem()
    }
    
    // 'initialValue' attribute on Variable at EditDBPaymentScreen.pcf: line 634, column 41
    function initialValue_389 () : TAccountOwner {
      return paymentView.isGroupByTAccountOwner() ? (group as gw.api.web.payment.DBPaymentDistItemsForTAccountOwner).TAccountOwner : null
    }
    
    // RowIterator (id=groupsAggTAccountOwner) at EditDBPaymentScreen.pcf: line 630, column 110
    function initializeVariables_442 () : void {
        tAccountOwner = paymentView.isGroupByTAccountOwner() ? (group as gw.api.web.payment.DBPaymentDistItemsForTAccountOwner).TAccountOwner : null;

    }
    
    // 'onChange' attribute on PostOnChange at EditDBPaymentScreen.pcf: line 724, column 61
    function onChange_432 () : void {
      recalculateDistribution()
    }
    
    // 'onPick' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at EditDBPaymentScreen.pcf: line 721, column 48
    function onPick_434 (PickedValue :  java.lang.Object) : void {
      recalculateDistribution()
    }
    
    // 'value' attribute on TextCell (id=TAccountType_Cell) at EditDBPaymentScreen.pcf: line 641, column 113
    function valueRoot_391 () : java.lang.Object {
      return (group as gw.api.web.payment.DBPaymentDistItemsForTAccountOwner)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnbilledBalance_Cell) at EditDBPaymentScreen.pcf: line 669, column 51
    function valueRoot_405 () : java.lang.Object {
      return group
    }
    
    // 'value' attribute on TextCell (id=TAccountType_Cell) at EditDBPaymentScreen.pcf: line 641, column 113
    function value_390 () : java.lang.String {
      return (group as gw.api.web.payment.DBPaymentDistItemsForTAccountOwner).TAccountOwnerType
    }
    
    // 'value' attribute on TextCell (id=AggAccountNumber_Cell) at EditDBPaymentScreen.pcf: line 649, column 49
    function value_395 () : entity.Account {
      return getAccount(tAccountOwner)
    }
    
    // 'value' attribute on TextCell (id=AggPolicyNumber_Cell) at EditDBPaymentScreen.pcf: line 657, column 35
    function value_400 () : entity.PolicyPeriod {
      return getPolicyPeriod(tAccountOwner)
    }
    
    // 'value' attribute on DateCell (id=PolicyEffectiveDate_Cell) at EditDBPaymentScreen.pcf: line 662, column 104
    function value_402 () : java.util.Date {
      return tAccountOwner typeis PolicyPeriod ? tAccountOwner.PolicyPerEffDate : null
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnbilledBalance_Cell) at EditDBPaymentScreen.pcf: line 669, column 51
    function value_404 () : gw.pl.currency.MonetaryAmount {
      return group.UnbilledAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CurrentBalance_Cell) at EditDBPaymentScreen.pcf: line 676, column 49
    function value_408 () : gw.pl.currency.MonetaryAmount {
      return group.BilledAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PastDueBalance_Cell) at EditDBPaymentScreen.pcf: line 683, column 46
    function value_412 () : gw.pl.currency.MonetaryAmount {
      return group.DueAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalBalance_Cell) at EditDBPaymentScreen.pcf: line 690, column 43
    function value_416 () : gw.pl.currency.MonetaryAmount {
      return group.Amount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=PaidBalance_Cell) at EditDBPaymentScreen.pcf: line 697, column 47
    function value_420 () : gw.pl.currency.MonetaryAmount {
      return group.PaidAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnpaidBalance_Cell) at EditDBPaymentScreen.pcf: line 704, column 49
    function value_424 () : gw.pl.currency.MonetaryAmount {
      return group.UnpaidAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=AggDistributionAmount_Cell) at EditDBPaymentScreen.pcf: line 711, column 47
    function value_428 () : gw.pl.currency.MonetaryAmount {
      return group.Allocation
    }
    
    // 'value' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at EditDBPaymentScreen.pcf: line 721, column 48
    function value_435 () : gw.pl.currency.MonetaryAmount {
      return group.OverrideAmount
    }
    
    // 'valueVisible' attribute on TextCell (id=AggPolicyNumber_Cell) at EditDBPaymentScreen.pcf: line 657, column 35
    function visible_397 () : java.lang.Boolean {
      return getPolicyPeriod(tAccountOwner) != null
    }
    
    // 'visible' attribute on MonetaryAmountCell (id=OverrideAmount_Cell) at EditDBPaymentScreen.pcf: line 721, column 48
    function visible_440 () : java.lang.Boolean {
      return isInOverrideMode
    }
    
    property get group () : gw.api.web.payment.DBPaymentDistItemGroup {
      return getIteratedValue(1) as gw.api.web.payment.DBPaymentDistItemGroup
    }
    
    property get tAccountOwner () : TAccountOwner {
      return getVariableValue("tAccountOwner", 1) as TAccountOwner
    }
    
    property set tAccountOwner ($arg :  TAccountOwner) {
      setVariableValue("tAccountOwner", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/payment/EditDBPaymentScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry5ExpressionsImpl extends EditDBPaymentScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at EditDBPaymentScreen.pcf: line 776, column 53
    function currency_463 () : typekey.Currency {
      return paymentView.OriginatingAccount.Currency
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at EditDBPaymentScreen.pcf: line 765, column 35
    function defaultSetter_453 (__VALUE_TO_SET :  java.lang.Object) : void {
      item.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at EditDBPaymentScreen.pcf: line 776, column 53
    function defaultSetter_461 (__VALUE_TO_SET :  java.lang.Object) : void {
      item.GrossAmountToApply = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at EditDBPaymentScreen.pcf: line 784, column 51
    function defaultSetter_468 (__VALUE_TO_SET :  java.lang.Object) : void {
      item.PaymentComments = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextCell (id=PolicyNumber_Cell) at EditDBPaymentScreen.pcf: line 765, column 35
    function editable_451 () : java.lang.Boolean {
      return !item.Executed
    }
    
    // 'onChange' attribute on PostOnChange at EditDBPaymentScreen.pcf: line 778, column 61
    function onChange_457 () : void {
      recalculateDistribution()
    }
    
    // 'validationExpression' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at EditDBPaymentScreen.pcf: line 776, column 53
    function validationExpression_459 () : java.lang.Object {
      return !item.GrossAmountToApply.IsPositive ? DisplayKey.get("Web.NewDirectBillPayment.SuspenseGrossNegative") : null
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at EditDBPaymentScreen.pcf: line 765, column 35
    function valueRoot_454 () : java.lang.Object {
      return item
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at EditDBPaymentScreen.pcf: line 765, column 35
    function value_452 () : java.lang.String {
      return item.PolicyNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at EditDBPaymentScreen.pcf: line 776, column 53
    function value_460 () : gw.pl.currency.MonetaryAmount {
      return item.GrossAmountToApply
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at EditDBPaymentScreen.pcf: line 784, column 51
    function value_467 () : java.lang.String {
      return item.PaymentComments
    }
    
    property get item () : entity.BaseSuspDistItem {
      return getIteratedValue(1) as entity.BaseSuspDistItem
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/payment/EditDBPaymentScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntry6ExpressionsImpl extends EditDBPaymentScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at EditDBPaymentScreen.pcf: line 834, column 53
    function currency_490 () : typekey.Currency {
      return paymentView.OriginatingAccount.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at EditDBPaymentScreen.pcf: line 834, column 53
    function defaultSetter_488 (__VALUE_TO_SET :  java.lang.Object) : void {
      item.GrossAmountToApply = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at EditDBPaymentScreen.pcf: line 842, column 51
    function defaultSetter_495 (__VALUE_TO_SET :  java.lang.Object) : void {
      item.PaymentComments = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at EditDBPaymentScreen.pcf: line 834, column 53
    function editable_485 () : java.lang.Boolean {
      return !item.Executed
    }
    
    // 'onChange' attribute on PostOnChange at EditDBPaymentScreen.pcf: line 836, column 61
    function onChange_484 () : void {
      recalculateDistribution()
    }
    
    // 'validationExpression' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at EditDBPaymentScreen.pcf: line 834, column 53
    function validationExpression_486 () : java.lang.Object {
      return !item.GrossAmountToApply.IsPositive ? DisplayKey.get("Web.NewDirectBillPayment.CollateralGrossNegative") : null
    }
    
    // 'value' attribute on TextCell (id=Collateral_Cell) at EditDBPaymentScreen.pcf: line 823, column 52
    function valueRoot_482 () : java.lang.Object {
      return paymentView.TargetAccount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at EditDBPaymentScreen.pcf: line 834, column 53
    function valueRoot_489 () : java.lang.Object {
      return item
    }
    
    // 'value' attribute on TextCell (id=Collateral_Cell) at EditDBPaymentScreen.pcf: line 823, column 52
    function value_481 () : entity.Collateral {
      return paymentView.TargetAccount.Collateral
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmountToApply_Cell) at EditDBPaymentScreen.pcf: line 834, column 53
    function value_487 () : gw.pl.currency.MonetaryAmount {
      return item.GrossAmountToApply
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at EditDBPaymentScreen.pcf: line 842, column 51
    function value_494 () : java.lang.String {
      return item.PaymentComments
    }
    
    property get item () : entity.CollateralPaymentItem {
      return getIteratedValue(1) as entity.CollateralPaymentItem
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/payment/EditDBPaymentScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends EditDBPaymentScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // '$$sumValue' attribute on RowIterator (id=distItems) at EditDBPaymentScreen.pcf: line 517, column 50
    function sumValueRoot_242 (distItem :  entity.BaseDistItem) : java.lang.Object {
      return distItem.InvoiceItem
    }
    
    // '$$sumValue' attribute on RowIterator (id=distItems) at EditDBPaymentScreen.pcf: line 532, column 60
    function sumValueRoot_246 (distItem :  entity.BaseDistItem) : java.lang.Object {
      return distItem
    }
    
    // 'footerSumValue' attribute on RowIterator (id=distItems) at EditDBPaymentScreen.pcf: line 517, column 50
    function sumValue_241 (distItem :  entity.BaseDistItem) : java.lang.Object {
      return distItem.InvoiceItem.Amount
    }
    
    // 'footerSumValue' attribute on RowIterator (id=distItems) at EditDBPaymentScreen.pcf: line 526, column 65
    function sumValue_243 (distItem :  entity.BaseDistItem) : java.lang.Object {
      return distItem.InvoiceItem.GrossUnsettledAmount
    }
    
    // 'footerSumValue' attribute on RowIterator (id=distItems) at EditDBPaymentScreen.pcf: line 532, column 60
    function sumValue_245 (distItem :  entity.BaseDistItem) : java.lang.Object {
      return distItem.GrossAmountToApply
    }
    
    // 'value' attribute on TextCell (id=invoiceDescription_Cell) at EditDBPaymentScreen.pcf: line 436, column 48
    function valueRoot_238 () : java.lang.Object {
      return group
    }
    
    // 'value' attribute on TextCell (id=invoiceDescription_Cell) at EditDBPaymentScreen.pcf: line 436, column 48
    function value_237 () : java.lang.String {
      return group.DisplayName
    }
    
    // 'value' attribute on RowIterator (id=distItems) at EditDBPaymentScreen.pcf: line 448, column 73
    function value_306 () : java.util.List<entity.BaseDistItem> {
      return group.DistItems
    }
    
    // 'visible' attribute on TextCell (id=invoiceDescriptionSpacer_Cell) at EditDBPaymentScreen.pcf: line 439, column 49
    function visible_240 () : java.lang.Boolean {
      return isInOverrideMode
    }
    
    property get group () : gw.api.web.payment.DBPaymentDistItemGroup {
      return getIteratedValue(1) as gw.api.web.payment.DBPaymentDistItemGroup
    }
    
    
  }
  
  
}