package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountSummaryScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountSummaryScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountSummaryScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountPolicyPeriodsLVExpressionsImpl extends AccountSummaryScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountSummaryScreen.pcf: line 437, column 54
    function filter_174 () : gw.api.filters.IFilter {
      return policySummaryHelper.FILTER_ALL
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountSummaryScreen.pcf: line 439, column 57
    function filter_175 () : gw.api.filters.IFilter {
      return policySummaryHelper.FILTER_ACTIVE
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountSummaryScreen.pcf: line 441, column 60
    function filter_176 () : gw.api.filters.IFilter {
      return policySummaryHelper.FILTER_CANCELLED
    }
    
    // 'filter' attribute on ToolbarFilterOption at AccountSummaryScreen.pcf: line 443, column 61
    function filter_177 () : gw.api.filters.IFilter {
      return policySummaryHelper.FILTER_REINSTATED
    }
    
    // 'initialValue' attribute on Variable at AccountSummaryScreen.pcf: line 422, column 59
    function initialValue_173 () : gw.web.account.AccountSummaryPolicyHelper {
      return new gw.web.account.AccountSummaryPolicyHelper(account)
    }
    
    // 'sortBy' attribute on IteratorSort at AccountSummaryScreen.pcf: line 447, column 26
    function sortBy_178 (policySummaryView :  gw.web.account.AccountPolicySummaryView) : java.lang.Object {
      return policySummaryView.PolicyPeriod.PolicyPerEffDate
    }
    
    // 'sortBy' attribute on IteratorSort at AccountSummaryScreen.pcf: line 450, column 26
    function sortBy_179 (policySummaryView :  gw.web.account.AccountPolicySummaryView) : java.lang.Object {
      return policySummaryView.PolicyPeriod.PolicyPerExpirDate
    }
    
    // 'sortBy' attribute on TextCell (id=PolicyNumber_Cell) at AccountSummaryScreen.pcf: line 484, column 27
    function sortValue_180 (policySummaryView :  gw.web.account.AccountPolicySummaryView) : java.lang.Object {
      var plcyPeriod : PolicyPeriod = (policySummaryView.PolicyPeriod)
return plcyPeriod.PolicyNumber
    }
    
    // 'sortBy' attribute on TextCell (id=PolicyNumber_Cell) at AccountSummaryScreen.pcf: line 484, column 27
    function sortValue_181 (policySummaryView :  gw.web.account.AccountPolicySummaryView) : java.lang.Object {
      var plcyPeriod : PolicyPeriod = (policySummaryView.PolicyPeriod)
return  plcyPeriod.TermNumber
    }
    
    // 'value' attribute on DateCell (id=PolicyPerEffDate_Cell) at AccountSummaryScreen.pcf: line 495, column 50
    function sortValue_182 (policySummaryView :  gw.web.account.AccountPolicySummaryView) : java.lang.Object {
      var plcyPeriod : PolicyPeriod = (policySummaryView.PolicyPeriod)
return plcyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=PolicyPerExpirDate_Cell) at AccountSummaryScreen.pcf: line 499, column 52
    function sortValue_183 (policySummaryView :  gw.web.account.AccountPolicySummaryView) : java.lang.Object {
      var plcyPeriod : PolicyPeriod = (policySummaryView.PolicyPeriod)
return plcyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalValue_Cell) at AccountSummaryScreen.pcf: line 515, column 44
    function sortValue_184 (policySummaryView :  gw.web.account.AccountPolicySummaryView) : java.lang.Object {
      var plcyPeriod : PolicyPeriod = (policySummaryView.PolicyPeriod)
return plcyPeriod.TotalValue
    }
    
    // 'value' attribute on RowIterator (id=AccountPolicyPeriods) at AccountSummaryScreen.pcf: line 429, column 83
    function value_217 () : java.util.List<gw.web.account.AccountPolicySummaryView> {
      return policySummaryHelper.listPolicySummaryViews()
    }
    
    // 'visible' attribute on ListViewPanel (id=AccountPolicyPeriodsLV) at AccountSummaryScreen.pcf: line 418, column 35
    function visible_218 () : java.lang.Boolean {
      return !account.ListBill
    }
    
    property get policySummaryHelper () : gw.web.account.AccountSummaryPolicyHelper {
      return getVariableValue("policySummaryHelper", 1) as gw.web.account.AccountSummaryPolicyHelper
    }
    
    property set policySummaryHelper ($arg :  gw.web.account.AccountSummaryPolicyHelper) {
      setVariableValue("policySummaryHelper", 1, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountSummaryScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountSummaryScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextInput (id=ActivePolicies_Input) at AccountSummaryScreen.pcf: line 249, column 46
    function actionAvailable_104 () : java.lang.Boolean {
      return account.ActivePolicyPeriods.Count > 0
    }
    
    // 'action' attribute on TextInput (id=ActivePolicies_Input) at AccountSummaryScreen.pcf: line 249, column 46
    function action_102 () : void {
      AccountDetailPolicies.push(account)
    }
    
    // 'action' attribute on ToolbarButton (id=CloseDelinquencyButton) at AccountSummaryScreen.pcf: line 46, column 48
    function action_13 () : void {
      TDIC_CloseDelinquencyProcessPopup.push(account.OpenDelinquencyTargets)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_TroubleTicketAlertAlertBar) at AccountSummaryScreen.pcf: line 53, column 50
    function action_17 () : void {
      TroubleTicketAlertForward.push(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_DelinquencyAlertAlertBar) at AccountSummaryScreen.pcf: line 59, column 67
    function action_22 () : void {
      AccountDetailDelinquencies.go(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_ActivitiesAlertBar) at AccountSummaryScreen.pcf: line 69, column 115
    function action_29 () : void {
      AccountActivitiesPage.go(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_PaymentRequestAlertBar) at AccountSummaryScreen.pcf: line 74, column 166
    function action_32 () : void {
      AccountPaymentRequests.go(account)
    }
    
    // 'action' attribute on TextInput (id=Name_Input) at AccountSummaryScreen.pcf: line 91, column 53
    function action_34 () : void {
      AccountDetailSummary.go(account)
    }
    
    // 'action' attribute on TextInput (id=PrimaryContact_Input) at AccountSummaryScreen.pcf: line 115, column 57
    function action_45 () : void {
      AccountDetailContacts.go(account, account.PrimaryPayer)
    }
    
    // 'action' attribute on Link (id=DelinquencyFlag) at AccountSummaryScreen.pcf: line 136, column 62
    function action_55 () : void {
      AccountDetailDelinquencies.go(account)
    }
    
    // 'action' attribute on ToolbarButton (id=CloseAccountButton) at AccountSummaryScreen.pcf: line 34, column 39
    function action_6 () : void {
      account.close(); account.getBundle().commit();
    }
    
    // 'action' attribute on Link (id=NotesAddIcon) at AccountSummaryScreen.pcf: line 181, column 45
    function action_69 () : void {
      AccountNewNoteWorksheet.goInWorkspace(account)
    }
    
    // 'action' attribute on Link (id=NotesEditIcon) at AccountSummaryScreen.pcf: line 192, column 41
    function action_72 () : void {
      AccountNewNoteWorksheet.goInWorkspace(account, note)
    }
    
    // 'action' attribute on TextInput (id=NoteLink_Input) at AccountSummaryScreen.pcf: line 199, column 39
    function action_75 () : void {
      AccountDetailNotes.push(account, false, note)
    }
    
    // 'action' attribute on ToolbarButton (id=StartDelinquencyButton) at AccountSummaryScreen.pcf: line 40, column 48
    function action_9 () : void {
      StartDelinquencyProcessPopup.push(account.OpenDelinquencyTargets)
    }
    
    // 'action' attribute on ToolbarButton (id=StartDelinquencyButton) at AccountSummaryScreen.pcf: line 40, column 48
    function action_dest_10 () : pcf.api.Destination {
      return pcf.StartDelinquencyProcessPopup.createDestination(account.OpenDelinquencyTargets)
    }
    
    // 'action' attribute on TextInput (id=ActivePolicies_Input) at AccountSummaryScreen.pcf: line 249, column 46
    function action_dest_103 () : pcf.api.Destination {
      return pcf.AccountDetailPolicies.createDestination(account)
    }
    
    // 'action' attribute on ToolbarButton (id=CloseDelinquencyButton) at AccountSummaryScreen.pcf: line 46, column 48
    function action_dest_14 () : pcf.api.Destination {
      return pcf.TDIC_CloseDelinquencyProcessPopup.createDestination(account.OpenDelinquencyTargets)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_TroubleTicketAlertAlertBar) at AccountSummaryScreen.pcf: line 53, column 50
    function action_dest_18 () : pcf.api.Destination {
      return pcf.TroubleTicketAlertForward.createDestination(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_DelinquencyAlertAlertBar) at AccountSummaryScreen.pcf: line 59, column 67
    function action_dest_23 () : pcf.api.Destination {
      return pcf.AccountDetailDelinquencies.createDestination(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_ActivitiesAlertBar) at AccountSummaryScreen.pcf: line 69, column 115
    function action_dest_30 () : pcf.api.Destination {
      return pcf.AccountActivitiesPage.createDestination(account)
    }
    
    // 'action' attribute on AlertBar (id=AccountDetailSummary_PaymentRequestAlertBar) at AccountSummaryScreen.pcf: line 74, column 166
    function action_dest_33 () : pcf.api.Destination {
      return pcf.AccountPaymentRequests.createDestination(account)
    }
    
    // 'action' attribute on TextInput (id=Name_Input) at AccountSummaryScreen.pcf: line 91, column 53
    function action_dest_35 () : pcf.api.Destination {
      return pcf.AccountDetailSummary.createDestination(account)
    }
    
    // 'action' attribute on TextInput (id=PrimaryContact_Input) at AccountSummaryScreen.pcf: line 115, column 57
    function action_dest_46 () : pcf.api.Destination {
      return pcf.AccountDetailContacts.createDestination(account, account.PrimaryPayer)
    }
    
    // 'action' attribute on Link (id=DelinquencyFlag) at AccountSummaryScreen.pcf: line 136, column 62
    function action_dest_56 () : pcf.api.Destination {
      return pcf.AccountDetailDelinquencies.createDestination(account)
    }
    
    // 'action' attribute on Link (id=NotesAddIcon) at AccountSummaryScreen.pcf: line 181, column 45
    function action_dest_70 () : pcf.api.Destination {
      return pcf.AccountNewNoteWorksheet.createDestination(account)
    }
    
    // 'action' attribute on Link (id=NotesEditIcon) at AccountSummaryScreen.pcf: line 192, column 41
    function action_dest_73 () : pcf.api.Destination {
      return pcf.AccountNewNoteWorksheet.createDestination(account, note)
    }
    
    // 'action' attribute on TextInput (id=NoteLink_Input) at AccountSummaryScreen.pcf: line 199, column 39
    function action_dest_76 () : pcf.api.Destination {
      return pcf.AccountDetailNotes.createDestination(account, false, note)
    }
    
    // 'available' attribute on ToolbarButton (id=CloseDelinquencyButton) at AccountSummaryScreen.pcf: line 46, column 48
    function available_11 () : java.lang.Boolean {
      return !CurrentLocation.InEditMode && !account.isTargetOfHoldType(HoldType.TC_DELINQUENCY) && account.hasActiveDelinquencyProcess()
    }
    
    // 'available' attribute on AlertBar (id=AccountDetailSummary_TroubleTicketAlertAlertBar) at AccountSummaryScreen.pcf: line 53, column 50
    function available_15 () : java.lang.Boolean {
      return perm.System.acctttktview
    }
    
    // 'available' attribute on AlertBar (id=AccountDetailSummary_DelinquencyAlertAlertBar) at AccountSummaryScreen.pcf: line 59, column 67
    function available_20 () : java.lang.Boolean {
      return perm.System.acctdelview
    }
    
    // 'available' attribute on ToolbarButton (id=CloseAccountButton) at AccountSummaryScreen.pcf: line 34, column 39
    function available_4 () : java.lang.Boolean {
      return account.canClose() and perm.Account.close
    }
    
    // 'available' attribute on ToolbarButton (id=StartDelinquencyButton) at AccountSummaryScreen.pcf: line 40, column 48
    function available_7 () : java.lang.Boolean {
      return !CurrentLocation.InEditMode && !account.isTargetOfHoldType(HoldType.TC_DELINQUENCY)
    }
    
    // 'iconColor' attribute on Link (id=ServiceTierIcon) at AccountSummaryScreen.pcf: line 101, column 74
    function iconColor_42 () : gw.api.web.color.GWColor {
      return gw.web.util.BCIconHelper.getIconColor(account.ServiceTier)
    }
    
    // 'iconColor' attribute on Link (id=DelinquencyFlag) at AccountSummaryScreen.pcf: line 136, column 62
    function iconColor_59 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_PROGRESS_OVERDUE
    }
    
    // 'icon' attribute on Link (id=ServiceTierIcon) at AccountSummaryScreen.pcf: line 101, column 74
    function icon_41 () : java.lang.String {
      return summaryHelper.ServiceTierIcon
    }
    
    // 'icon' attribute on Link (id=DelinquencyFlag) at AccountSummaryScreen.pcf: line 136, column 62
    function icon_58 () : java.lang.String {
      return summaryHelper.DelinquencyIcon
    }
    
    // 'icon' attribute on Link (id=NotesSpacer) at AccountSummaryScreen.pcf: line 211, column 35
    function icon_80 () : java.lang.String {
      return 'blank_16.png'
    }
    
    // 'initialValue' attribute on Variable at AccountSummaryScreen.pcf: line 13, column 51
    function initialValue_0 () : gw.web.account.AccountSummaryHelper {
      return new gw.web.account.AccountSummaryHelper(account)
    }
    
    // 'initialValue' attribute on Variable at AccountSummaryScreen.pcf: line 17, column 23
    function initialValue_1 () : boolean {
      return account.UnappliedFunds.length > 1
    }
    
    // 'initialValue' attribute on Variable at AccountSummaryScreen.pcf: line 22, column 60
    function initialValue_2 () : gw.api.database.IQueryBeanResult<Note> {
      return createNoteSearchCriteria().performSearch(true)
    }
    
    // 'initialValue' attribute on Variable at AccountSummaryScreen.pcf: line 27, column 20
    function initialValue_3 () : Note {
      return noteQuery.FirstResult
    }
    
    // 'label' attribute on AlertBar (id=AccountDetailSummary_TroubleTicketAlertAlertBar) at AccountSummaryScreen.pcf: line 53, column 50
    function label_19 () : java.lang.Object {
      return account.AlertBarDisplayText
    }
    
    // 'label' attribute on AlertBar (id=AccountDetailSummary_DelinquencyAlertAlertBar) at AccountSummaryScreen.pcf: line 59, column 67
    function label_24 () : java.lang.Object {
      return DisplayKey.get("Web.AccountDetailSummary.DelinquencyAlert", account.DelinquencyReasons)
    }
    
    // 'label' attribute on AlertBar (id=AccountDetailSummary_PreDueAlertBar) at AccountSummaryScreen.pcf: line 64, column 203
    function label_27 () : java.lang.Object {
      return DisplayKey.get("Web.AccountDetailSummary.PreDueAlert", account.getPaymentDueDateToAvoidDelinquencyOn(Date.Today).AsUIStyle)
    }
    
    // 'label' attribute on Link (id=AccountSinceValue) at AccountSummaryScreen.pcf: line 109, column 66
    function label_44 () : java.lang.Object {
      return summaryHelper.TimeSinceInceptionDisplay
    }
    
    // 'label' attribute on Link (id=description) at AccountSummaryScreen.pcf: line 140, column 45
    function label_60 () : java.lang.Object {
      return DisplayKey.get("Web.AccountSummary.Overview.Delinquencies.HowMany", summaryHelper.RecentDelinquenciesCount)
    }
    
    // 'tooltip' attribute on Link (id=ServiceTierIcon) at AccountSummaryScreen.pcf: line 101, column 74
    function tooltip_40 () : java.lang.String {
      return summaryHelper.ServiceTierTooltip
    }
    
    // 'tooltip' attribute on Link (id=DelinquencyFlag) at AccountSummaryScreen.pcf: line 136, column 62
    function tooltip_57 () : java.lang.String {
      return summaryHelper.RecentDelinquenciesTooltip
    }
    
    // 'value' attribute on TextInput (id=ActivePolicies_Input) at AccountSummaryScreen.pcf: line 249, column 46
    function valueRoot_106 () : java.lang.Object {
      return account.ActivePolicyPeriods
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AccountSummaryScreen.pcf: line 91, column 53
    function valueRoot_37 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on TextInput (id=PrimaryContact_Input) at AccountSummaryScreen.pcf: line 115, column 57
    function valueRoot_48 () : java.lang.Object {
      return account.PrimaryPayer
    }
    
    // 'value' attribute on DateInput (id=NoteDate_Input) at AccountSummaryScreen.pcf: line 221, column 39
    function valueRoot_84 () : java.lang.Object {
      return note
    }
    
    // 'value' attribute on TextInput (id=ActivePolicies_Input) at AccountSummaryScreen.pcf: line 249, column 46
    function value_105 () : java.lang.Integer {
      return account.ActivePolicyPeriods.Count
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at AccountSummaryScreen.pcf: line 91, column 53
    function value_36 () : java.lang.String {
      return account.AccountNameLocalized
    }
    
    // 'value' attribute on TextInput (id=PrimaryContact_Input) at AccountSummaryScreen.pcf: line 115, column 57
    function value_47 () : java.lang.String {
      return account.PrimaryPayer.DisplayName
    }
    
    // 'value' attribute on TextInput (id=Address_Input) at AccountSummaryScreen.pcf: line 120, column 44
    function value_50 () : java.lang.String {
      return new gw.api.address.AddressFormatter().format(account.PrimaryPayer.Contact.PrimaryAddress, ", ")
    }
    
    // 'value' attribute on TextInput (id=Phone_Input) at AccountSummaryScreen.pcf: line 124, column 94
    function value_52 () : java.lang.String {
      return summaryHelper.getPhoneEmailDisplayValue(account.PrimaryPayer.Contact)
    }
    
    // 'value' attribute on TypeKeyInput (id=Currency_Input) at AccountSummaryScreen.pcf: line 148, column 45
    function value_61 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on TextInput (id=SendInvoicesBy_Input) at AccountSummaryScreen.pcf: line 152, column 118
    function value_64 () : java.lang.String {
      return account.InvoiceDeliveryType != null ? account.InvoiceDeliveryType.DisplayName : "-"
    }
    
    // 'value' attribute on TextInput (id=PaymentMethod_Input) at AccountSummaryScreen.pcf: line 157, column 53
    function value_66 () : entity.PaymentInstrument {
      return account.DefaultPaymentInstrument
    }
    
    // 'value' attribute on TextInput (id=NoteLink_Input) at AccountSummaryScreen.pcf: line 199, column 39
    function value_77 () : java.lang.String {
      return noteQuery.Count == 1 ? DisplayKey.get("Web.Summary.Overview.Notes.LinkSingle") : DisplayKey.get("Web.Summary.Overview.Notes.Link", noteQuery.Count)
    }
    
    // 'value' attribute on DateInput (id=NoteDate_Input) at AccountSummaryScreen.pcf: line 221, column 39
    function value_83 () : java.util.Date {
      return note.AuthoringDate
    }
    
    // 'value' attribute on TextInput (id=NoteTopic_Input) at AccountSummaryScreen.pcf: line 227, column 39
    function value_88 () : typekey.NoteTopicType {
      return note.Topic
    }
    
    // 'value' attribute on TextInput (id=NoteSubject_Input) at AccountSummaryScreen.pcf: line 232, column 39
    function value_93 () : java.lang.String {
      return note.Subject
    }
    
    // 'value' attribute on TextAreaInput (id=NoteText_Input) at AccountSummaryScreen.pcf: line 238, column 39
    function value_98 () : java.lang.String {
      return note.Body
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailSummary_TroubleTicketAlertAlertBar) at AccountSummaryScreen.pcf: line 53, column 50
    function visible_16 () : java.lang.Boolean {
      return account.HasActiveTroubleTickets
    }
    
    // 'visible' attribute on PanelSet at AccountSummaryScreen.pcf: line 379, column 34
    function visible_171 () : java.lang.Boolean {
      return account.ListBill
    }
    
    // 'visible' attribute on DetailViewPanel at AccountSummaryScreen.pcf: line 408, column 35
    function visible_172 () : java.lang.Boolean {
      return !account.ListBill
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailSummary_DelinquencyAlertAlertBar) at AccountSummaryScreen.pcf: line 59, column 67
    function visible_21 () : java.lang.Boolean {
      return account.hasActiveDelinquenciesOutOfGracePeriod()
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailSummary_PreDueAlertBar) at AccountSummaryScreen.pcf: line 64, column 203
    function visible_26 () : java.lang.Boolean {
      return account.PreDueUnsettledAmount.IsPositive && !account.hasActiveDelinquenciesOutOfGracePeriod() && account.getPaymentDueDateToAvoidDelinquencyOn(Date.Today) == Date.Today
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailSummary_ActivitiesAlertBar) at AccountSummaryScreen.pcf: line 69, column 115
    function visible_28 () : java.lang.Boolean {
      return account.Activities_Ext.where(\ a -> a.Status == typekey.ActivityStatus.TC_OPEN).length > 0
    }
    
    // 'visible' attribute on AlertBar (id=AccountDetailSummary_PaymentRequestAlertBar) at AccountSummaryScreen.pcf: line 74, column 166
    function visible_31 () : java.lang.Boolean {
      return account.PaymentRequests.where(\ a -> a.Status == PaymentRequestStatus.TC_CREATED or a.Status == PaymentRequestStatus.TC_REQUESTED).length > 0
    }
    
    // 'visible' attribute on Link (id=ServiceTierIcon) at AccountSummaryScreen.pcf: line 101, column 74
    function visible_39 () : java.lang.Boolean {
      return summaryHelper.ServiceTierIcon != ""
    }
    
    // 'visible' attribute on ToolbarButton (id=CloseAccountButton) at AccountSummaryScreen.pcf: line 34, column 39
    function visible_5 () : java.lang.Boolean {
      return not account.Closed
    }
    
    // 'visible' attribute on Link (id=DelinquencyFlag) at AccountSummaryScreen.pcf: line 136, column 62
    function visible_54 () : java.lang.Boolean {
      return summaryHelper.ShowDelinquencyIcon
    }
    
    // 'visible' attribute on Link (id=NotesEditIcon) at AccountSummaryScreen.pcf: line 192, column 41
    function visible_71 () : java.lang.Boolean {
      return note != null
    }
    
    // 'visible' attribute on ToolbarButton (id=StartDelinquencyButton) at AccountSummaryScreen.pcf: line 40, column 48
    function visible_8 () : java.lang.Boolean {
      return perm.System.manualDlnq_TDIC
    }
    
    // 'visible' attribute on ContentInput at AccountSummaryScreen.pcf: line 203, column 38
    function visible_81 () : java.lang.Boolean {
      return note == null
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get note () : Note {
      return getVariableValue("note", 0) as Note
    }
    
    property set note ($arg :  Note) {
      setVariableValue("note", 0, $arg)
    }
    
    property get noteQuery () : gw.api.database.IQueryBeanResult<Note> {
      return getVariableValue("noteQuery", 0) as gw.api.database.IQueryBeanResult<Note>
    }
    
    property set noteQuery ($arg :  gw.api.database.IQueryBeanResult<Note>) {
      setVariableValue("noteQuery", 0, $arg)
    }
    
    property get showTotalUnapplied () : boolean {
      return getVariableValue("showTotalUnapplied", 0) as java.lang.Boolean
    }
    
    property set showTotalUnapplied ($arg :  boolean) {
      setVariableValue("showTotalUnapplied", 0, $arg)
    }
    
    property get summaryHelper () : gw.web.account.AccountSummaryHelper {
      return getVariableValue("summaryHelper", 0) as gw.web.account.AccountSummaryHelper
    }
    
    property set summaryHelper ($arg :  gw.web.account.AccountSummaryHelper) {
      setVariableValue("summaryHelper", 0, $arg)
    }
    
    function createNoteSearchCriteria() : NoteSearchCriteria {
      var nsq = new NoteSearchCriteria() 
      nsq.Account = account 
      nsq.SortBy = SortByRange.TC_DATE 
      return nsq
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountSummaryScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AccountPolicyPeriodsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at AccountSummaryScreen.pcf: line 484, column 27
    function action_192 () : void {
      PolicyOverview.go(plcyPeriod)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at AccountSummaryScreen.pcf: line 484, column 27
    function action_dest_193 () : pcf.api.Destination {
      return pcf.PolicyOverview.createDestination(plcyPeriod)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=TotalValue_Cell) at AccountSummaryScreen.pcf: line 515, column 44
    function currency_214 () : typekey.Currency {
      return plcyPeriod.Currency
    }
    
    // 'iconColor' attribute on BooleanRadioCell (id=IsPastDue_Cell) at AccountSummaryScreen.pcf: line 468, column 34
    function iconColor_189 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_PROGRESS_OVERDUE
    }
    
    // 'iconLabel' attribute on BooleanRadioCell (id=Product_Cell) at AccountSummaryScreen.pcf: line 459, column 34
    function iconLabel_186 () : java.lang.String {
      return policySummaryView.ProductCodeIconTooltip
    }
    
    // 'iconLabel' attribute on BooleanRadioCell (id=IsActive_Cell) at AccountSummaryScreen.pcf: line 476, column 34
    function iconLabel_190 () : java.lang.String {
      return policySummaryView.InForceTooltip
    }
    
    // 'icon' attribute on BooleanRadioCell (id=Product_Cell) at AccountSummaryScreen.pcf: line 459, column 34
    function icon_187 () : java.lang.String {
      return policySummaryView.ProductCodeIcon
    }
    
    // 'icon' attribute on BooleanRadioCell (id=IsPastDue_Cell) at AccountSummaryScreen.pcf: line 468, column 34
    function icon_188 () : java.lang.String {
      return policySummaryView.PastDueIcon
    }
    
    // 'icon' attribute on BooleanRadioCell (id=IsActive_Cell) at AccountSummaryScreen.pcf: line 476, column 34
    function icon_191 () : java.lang.String {
      return policySummaryView.InForceIcon
    }
    
    // 'initialValue' attribute on Variable at AccountSummaryScreen.pcf: line 433, column 32
    function initialValue_185 () : PolicyPeriod {
      return policySummaryView.PolicyPeriod
    }
    
    // RowIterator (id=AccountPolicyPeriods) at AccountSummaryScreen.pcf: line 429, column 83
    function initializeVariables_216 () : void {
        plcyPeriod = policySummaryView.PolicyPeriod;

    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AccountSummaryScreen.pcf: line 484, column 27
    function valueRoot_195 () : java.lang.Object {
      return plcyPeriod
    }
    
    // 'value' attribute on DateCell (id=CancellationDate_Cell) at AccountSummaryScreen.pcf: line 504, column 70
    function valueRoot_207 () : java.lang.Object {
      return policySummaryView.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AccountSummaryScreen.pcf: line 484, column 27
    function value_194 () : java.lang.String {
      return plcyPeriod.DisplayName
    }
    
    // 'value' attribute on TextCell (id=InsuredName_Cell) at AccountSummaryScreen.pcf: line 491, column 53
    function value_197 () : entity.PolicyPeriodContact {
      return plcyPeriod.PrimaryInsured
    }
    
    // 'value' attribute on DateCell (id=PolicyPerEffDate_Cell) at AccountSummaryScreen.pcf: line 495, column 50
    function value_200 () : java.util.Date {
      return plcyPeriod.PolicyPerEffDate
    }
    
    // 'value' attribute on DateCell (id=PolicyPerExpirDate_Cell) at AccountSummaryScreen.pcf: line 499, column 52
    function value_203 () : java.util.Date {
      return plcyPeriod.PolicyPerExpirDate
    }
    
    // 'value' attribute on DateCell (id=CancellationDate_Cell) at AccountSummaryScreen.pcf: line 504, column 70
    function value_206 () : java.util.Date {
      return policySummaryView.PolicyPeriod.CancellationDate
    }
    
    // 'value' attribute on DateCell (id=ReinstatementDate_Cell) at AccountSummaryScreen.pcf: line 509, column 71
    function value_209 () : java.util.Date {
      return policySummaryView.PolicyPeriod.ReinstatementDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=TotalValue_Cell) at AccountSummaryScreen.pcf: line 515, column 44
    function value_212 () : gw.pl.currency.MonetaryAmount {
      return plcyPeriod.TotalValue
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 2) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 2, $arg)
    }
    
    property get policySummaryView () : gw.web.account.AccountPolicySummaryView {
      return getIteratedValue(2) as gw.web.account.AccountPolicySummaryView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/AccountSummaryScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PanelSetExpressionsImpl extends AccountSummaryScreenExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on MonetaryAmountInput (id=NextInvoiceDue_Input) at AccountSummaryScreen.pcf: line 313, column 76
    function actionAvailable_129 () : java.lang.Boolean {
      return summaryFinancialsHelper.NextInvoiceDue != null
    }
    
    // 'actionAvailable' attribute on MonetaryAmountInput (id=LastPaymentReceived_Input) at AccountSummaryScreen.pcf: line 319, column 195
    function actionAvailable_138 () : java.lang.Boolean {
      return summaryFinancialsHelper.LastPaymentReceived != null
    }
    
    // 'action' attribute on MonetaryAmountInput (id=ExpectedPayment_Input) at AccountSummaryScreen.pcf: line 276, column 66
    function action_110 () : void {
      AccountDetailInvoices.go(account)
    }
    
    // 'action' attribute on MenuItem (id=ReverseLateFeeAction) at AccountSummaryScreen.pcf: line 302, column 101
    function action_120 () : void {
      AccountLateFeeReversalWizard.go(account)
    }
    
    // 'action' attribute on MonetaryAmountInput (id=NextInvoiceDue_Input) at AccountSummaryScreen.pcf: line 313, column 76
    function action_126 () : void {
      AccountDetailInvoices.go(account)
    }
    
    // 'action' attribute on MonetaryAmountInput (id=LastPaymentReceived_Input) at AccountSummaryScreen.pcf: line 319, column 195
    function action_135 () : void {
      AccountPayments.go(account)
    }
    
    // 'action' attribute on MonetaryAmountInput (id=TotalUnappliedAmount_Input) at AccountSummaryScreen.pcf: line 355, column 47
    function action_152 () : void {
      UnappliedFundsDetailPopup.push(account)
    }
    
    // 'action' attribute on TextInput (id=NumPlannedInvoices_Input) at AccountSummaryScreen.pcf: line 372, column 48
    function action_164 () : void {
      AccountDetailInvoices.go(account)
    }
    
    // 'action' attribute on MonetaryAmountInput (id=ExpectedPayment_Input) at AccountSummaryScreen.pcf: line 276, column 66
    function action_dest_111 () : pcf.api.Destination {
      return pcf.AccountDetailInvoices.createDestination(account)
    }
    
    // 'action' attribute on MenuItem (id=ReverseLateFeeAction) at AccountSummaryScreen.pcf: line 302, column 101
    function action_dest_121 () : pcf.api.Destination {
      return pcf.AccountLateFeeReversalWizard.createDestination(account)
    }
    
    // 'action' attribute on MonetaryAmountInput (id=NextInvoiceDue_Input) at AccountSummaryScreen.pcf: line 313, column 76
    function action_dest_127 () : pcf.api.Destination {
      return pcf.AccountDetailInvoices.createDestination(account)
    }
    
    // 'action' attribute on MonetaryAmountInput (id=LastPaymentReceived_Input) at AccountSummaryScreen.pcf: line 319, column 195
    function action_dest_136 () : pcf.api.Destination {
      return pcf.AccountPayments.createDestination(account)
    }
    
    // 'action' attribute on MonetaryAmountInput (id=TotalUnappliedAmount_Input) at AccountSummaryScreen.pcf: line 355, column 47
    function action_dest_153 () : pcf.api.Destination {
      return pcf.UnappliedFundsDetailPopup.createDestination(account)
    }
    
    // 'action' attribute on TextInput (id=NumPlannedInvoices_Input) at AccountSummaryScreen.pcf: line 372, column 48
    function action_dest_165 () : pcf.api.Destination {
      return pcf.AccountDetailInvoices.createDestination(account)
    }
    
    // 'available' attribute on MonetaryAmountInput (id=ExpectedPayment_Input) at AccountSummaryScreen.pcf: line 276, column 66
    function available_109 () : java.lang.Boolean {
      return summaryFinancialsHelper.ExpectedPayment.IsNotZero
    }
    
    // 'available' attribute on MenuItem (id=ReverseLateFeeAction) at AccountSummaryScreen.pcf: line 302, column 101
    function available_119 () : java.lang.Boolean {
      return summaryFinancialsHelper.UnsettledLateFeesAmount.IsNotZero
    }
    
    // 'def' attribute on PanelRef at AccountSummaryScreen.pcf: line 338, column 78
    function def_onEnter_144 (def :  pcf.SummaryChartPanelSet) : void {
      def.onEnter(summaryFinancialsHelper.ChartValues)
    }
    
    // 'def' attribute on PanelRef at AccountSummaryScreen.pcf: line 338, column 78
    function def_refreshVariables_145 (def :  pcf.SummaryChartPanelSet) : void {
      def.refreshVariables(summaryFinancialsHelper.ChartValues)
    }
    
    // 'iconColor' attribute on Link (id=PastDueIcon) at AccountSummaryScreen.pcf: line 287, column 66
    function iconColor_117 () : gw.api.web.color.GWColor {
      return gw.api.web.color.GWColor.THEME_PROGRESS_OVERDUE
    }
    
    // 'initialValue' attribute on Variable at AccountSummaryScreen.pcf: line 260, column 63
    function initialValue_108 () : gw.web.account.AccountSummaryFinancialsHelper {
      return new gw.web.account.AccountSummaryFinancialsHelper(account)
    }
    
    // 'label' attribute on Link (id=PastDueAmount) at AccountSummaryScreen.pcf: line 290, column 74
    function label_118 () : java.lang.Object {
      return account.DelinquentAmount.renderWithZeroDash()
    }
    
    // 'label' attribute on MonetaryAmountInput (id=NextInvoiceDue_Input) at AccountSummaryScreen.pcf: line 313, column 76
    function label_128 () : java.lang.Object {
      return summaryFinancialsHelper.NextInvoiceDueLabel
    }
    
    // 'label' attribute on MonetaryAmountInput (id=LastPaymentReceived_Input) at AccountSummaryScreen.pcf: line 319, column 195
    function label_137 () : java.lang.Object {
      return summaryFinancialsHelper.LastPaymentReceivedLabel
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ExpectedPayment_Input) at AccountSummaryScreen.pcf: line 276, column 66
    function valueRoot_113 () : java.lang.Object {
      return summaryFinancialsHelper
    }
    
    // 'value' attribute on MonetaryAmountInput (id=AccountUnappliedAmount_Input) at AccountSummaryScreen.pcf: line 348, column 48
    function valueRoot_148 () : java.lang.Object {
      return account.DefaultUnappliedFund
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalUnappliedAmount_Input) at AccountSummaryScreen.pcf: line 355, column 47
    function valueRoot_155 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on MonetaryAmountInput (id=ExpectedPayment_Input) at AccountSummaryScreen.pcf: line 276, column 66
    function value_112 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.ExpectedPayment
    }
    
    // 'value' attribute on MonetaryAmountInput (id=LateFees_Input) at AccountSummaryScreen.pcf: line 296, column 73
    function value_122 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.UnsettledLateFeesAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=NextInvoiceDue_Input) at AccountSummaryScreen.pcf: line 313, column 76
    function value_130 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.NextInvoiceDue
    }
    
    // 'value' attribute on MonetaryAmountInput (id=LastPaymentReceived_Input) at AccountSummaryScreen.pcf: line 319, column 195
    function value_139 () : gw.pl.currency.MonetaryAmount {
      return (summaryFinancialsHelper.LastPaymentReceived != null) ? summaryFinancialsHelper.LastPaymentReceived.Amount : new gw.pl.currency.MonetaryAmount(0, account.getCurrency())
    }
    
    // 'value' attribute on MonetaryAmountInput (id=AccountUnappliedAmount_Input) at AccountSummaryScreen.pcf: line 348, column 48
    function value_147 () : gw.pl.currency.MonetaryAmount {
      return account.DefaultUnappliedFund.Balance
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalUnappliedAmount_Input) at AccountSummaryScreen.pcf: line 355, column 47
    function value_154 () : gw.pl.currency.MonetaryAmount {
      return account.TotalUnappliedAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Suspense_Input) at AccountSummaryScreen.pcf: line 360, column 49
    function value_158 () : gw.pl.currency.MonetaryAmount {
      return account.SuspenseAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=PayoffAmount_Input) at AccountSummaryScreen.pcf: line 365, column 63
    function value_161 () : gw.pl.currency.MonetaryAmount {
      return summaryFinancialsHelper.PayoffAmount
    }
    
    // 'value' attribute on TextInput (id=NumPlannedInvoices_Input) at AccountSummaryScreen.pcf: line 372, column 48
    function value_166 () : java.lang.Integer {
      return summaryFinancialsHelper.PlannedInvoicesCount
    }
    
    // 'visible' attribute on Link (id=PastDueIcon) at AccountSummaryScreen.pcf: line 287, column 66
    function visible_116 () : java.lang.Boolean {
      return account.DelinquentAmount.IsPositive
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=NextInvoiceDue_Input) at AccountSummaryScreen.pcf: line 313, column 76
    function visible_125 () : java.lang.Boolean {
      return account.BillingLevel == BillingLevel.TC_ACCOUNT
    }
    
    // 'visible' attribute on InputDivider at AccountSummaryScreen.pcf: line 321, column 76
    function visible_142 () : java.lang.Boolean {
      return account.BillingLevel != BillingLevel.TC_ACCOUNT
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=AccountUnappliedAmount_Input) at AccountSummaryScreen.pcf: line 348, column 48
    function visible_146 () : java.lang.Boolean {
      return !showTotalUnapplied
    }
    
    // 'visible' attribute on MonetaryAmountInput (id=TotalUnappliedAmount_Input) at AccountSummaryScreen.pcf: line 355, column 47
    function visible_151 () : java.lang.Boolean {
      return showTotalUnapplied
    }
    
    // 'visible' attribute on PanelSet at AccountSummaryScreen.pcf: line 256, column 35
    function visible_169 () : java.lang.Boolean {
      return !account.ListBill
    }
    
    property get summaryFinancialsHelper () : gw.web.account.AccountSummaryFinancialsHelper {
      return getVariableValue("summaryFinancialsHelper", 1) as gw.web.account.AccountSummaryFinancialsHelper
    }
    
    property set summaryFinancialsHelper ($arg :  gw.web.account.AccountSummaryFinancialsHelper) {
      setVariableValue("summaryFinancialsHelper", 1, $arg)
    }
    
    
  }
  
  
}