package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountToCollateralXFerTransaction.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountToCollateralXFerTransactionExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountToCollateralXFerTransaction.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountToCollateralXFerTransactionExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'afterCommit' attribute on Page (id=AccountToCollateralXFerTransaction) at AccountToCollateralXFerTransaction.pcf: line 12, column 86
    function afterCommit_10 (TopLocation :  pcf.api.Location) : void {
      AccountCollateral.push(account)
    }
    
    // 'beforeCommit' attribute on Page (id=AccountToCollateralXFerTransaction) at AccountToCollateralXFerTransaction.pcf: line 12, column 86
    function beforeCommit_11 (pickedValue :  java.lang.Object) : void {
      account.Collateral.transferDefaultUnappliedToCollateral( amount )
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=amountInAccount_Input) at AccountToCollateralXFerTransaction.pcf: line 36, column 46
    function currency_2 () : typekey.Currency {
      return account.Currency
    }
    
    // 'value' attribute on MonetaryAmountInput (id=transferAmount_Input) at AccountToCollateralXFerTransaction.pcf: line 44, column 29
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'parent' attribute on Page (id=AccountToCollateralXFerTransaction) at AccountToCollateralXFerTransaction.pcf: line 12, column 86
    static function parent_12 (account :  Account) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(account)
    }
    
    // 'validationExpression' attribute on MonetaryAmountInput (id=transferAmount_Input) at AccountToCollateralXFerTransaction.pcf: line 44, column 29
    function validationExpression_4 () : java.lang.Object {
      return validateTransferAmount()
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amountInAccount_Input) at AccountToCollateralXFerTransaction.pcf: line 36, column 46
    function valueRoot_1 () : java.lang.Object {
      return account
    }
    
    // 'value' attribute on MonetaryAmountInput (id=amountInAccount_Input) at AccountToCollateralXFerTransaction.pcf: line 36, column 46
    function value_0 () : gw.pl.currency.MonetaryAmount {
      return account.UnappliedAmount
    }
    
    // 'value' attribute on MonetaryAmountInput (id=transferAmount_Input) at AccountToCollateralXFerTransaction.pcf: line 44, column 29
    function value_5 () : gw.pl.currency.MonetaryAmount {
      return amount
    }
    
    override property get CurrentLocation () : pcf.AccountToCollateralXFerTransaction {
      return super.CurrentLocation as pcf.AccountToCollateralXFerTransaction
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get amount () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("amount", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set amount ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("amount", 0, $arg)
    }
    
    function validateTransferAmount() : String {
            if (amount.IsNegative)
              return DisplayKey.get("Web.AccountToCollateralXferTransaction.TransferAmountNegative")
            if (amount > account.UnappliedAmount)
              return DisplayKey.get("Web.AccountToCollateralXferTransaction.TransferAmountMoreThanAvailable")
            return null
    }
    
    
  }
  
  
}