package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.acc.acct360.accountview.AccountBalanceTxnUtil
@javax.annotation.Generated("config/web/pcf/account/AssignInvoiceItemsToNewPayerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AssignInvoiceItemsToNewPayerPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AssignInvoiceItemsToNewPayerPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AssignInvoiceItemsToNewPayerPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (invoiceItems :  InvoiceItem[], currentAccount :  Account, chargesToAssign :  Charge[]) : int {
      return 0
    }
    
    // 'pickLocation' attribute on PickerInput (id=PayerAccount_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 75, column 40
    function action_9 () : void {
      AccountSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=PayerAccount_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 75, column 40
    function action_dest_10 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'beforeCommit' attribute on Popup (id=AssignInvoiceItemsToNewPayerPopup) at AssignInvoiceItemsToNewPayerPopup.pcf: line 10, column 85
    function beforeCommit_35 (pickedValue :  java.lang.Object) : void {
      assignItemsToPayer()
    }
    
    // 'def' attribute on PanelRef (id=invoiceItemsLV) at AssignInvoiceItemsToNewPayerPopup.pcf: line 118, column 34
    function def_onEnter_33 (def :  pcf.InvoiceItemsLV) : void {
      def.onEnter(itemsToAssign, null, false)
    }
    
    // 'def' attribute on PanelRef (id=invoiceItemsLV) at AssignInvoiceItemsToNewPayerPopup.pcf: line 118, column 34
    function def_refreshVariables_34 (def :  pcf.InvoiceItemsLV) : void {
      def.refreshVariables(itemsToAssign, null, false)
    }
    
    // 'value' attribute on PickerInput (id=PayerAccount_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 75, column 40
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      newPayerAccount = (__VALUE_TO_SET as entity.Account)
    }
    
    // 'value' attribute on RangeInput (id=InvoiceStream_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 86, column 47
    function defaultSetter_18 (__VALUE_TO_SET :  java.lang.Object) : void {
      overridingInvoiceStream = (__VALUE_TO_SET as entity.InvoiceStream)
    }
    
    // 'value' attribute on BooleanRadioInput (id=WhatItemsToAssign_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 93, column 46
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      isAssignAllItemsSelected = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=ReversePayments_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 103, column 48
    function defaultSetter_28 (__VALUE_TO_SET :  java.lang.Object) : void {
      isReversePaymentsSelected = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on BooleanRadioInput (id=CreateInvoiceForToday_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 108, column 44
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      createInvoiceForToday = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'initialValue' attribute on Variable at AssignInvoiceItemsToNewPayerPopup.pcf: line 25, column 23
    function initialValue_0 () : boolean {
      return gw.account.AssignBooleanChoice.ASSIGN_NOT_FULLY_PAID.BooleanValue
    }
    
    // 'initialValue' attribute on Variable at AssignInvoiceItemsToNewPayerPopup.pcf: line 29, column 29
    function initialValue_1 () : InvoiceItem[] {
      return computeItemsToAssign()
    }
    
    // 'initialValue' attribute on Variable at AssignInvoiceItemsToNewPayerPopup.pcf: line 33, column 57
    function initialValue_2 () : gw.api.web.account.AccountSearchConverter {
      return new gw.api.web.account.AccountSearchConverter()
    }
    
    // 'initialValue' attribute on Variable at AssignInvoiceItemsToNewPayerPopup.pcf: line 48, column 29
    function initialValue_3 () : InvoiceStream {
      return null
    }
    
    // 'inputConversion' attribute on PickerInput (id=PayerAccount_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 75, column 40
    function inputConversion_12 (VALUE :  java.lang.String) : java.lang.Object {
      return accountSearchConverter.getAccount( VALUE )
    }
    
    // 'label' attribute on AlertBar (id=notOwnerOrPayerWarning) at AssignInvoiceItemsToNewPayerPopup.pcf: line 60, column 44
    function label_6 () : java.lang.Object {
      return DisplayKey.get("Web.AssignInvoiceItemsToNewPayerPopup.NotOwnerOrPayerWarning", currentAccount.DisplayName)
    }
    
    // 'onChange' attribute on PostOnChange at AssignInvoiceItemsToNewPayerPopup.pcf: line 95, column 66
    function onChange_23 () : void {
      itemsToAssign = computeItemsToAssign()
    }
    
    // 'onChange' attribute on PostOnChange at AssignInvoiceItemsToNewPayerPopup.pcf: line 77, column 58
    function onChange_8 () : void {
      overridingInvoiceStream = null
    }
    
    // 'onPick' attribute on PickerInput (id=PayerAccount_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 75, column 40
    function onPick_11 (PickedValue :  Account) : void {
      overridingInvoiceStream = null
    }
    
    // 'required' attribute on RangeInput (id=InvoiceStream_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 86, column 47
    function required_16 () : java.lang.Boolean {
      return newPayerAccount.ListBill
    }
    
    // 'valueRange' attribute on RangeInput (id=InvoiceStream_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 86, column 47
    function valueRange_19 () : java.lang.Object {
      return newPayerAccount.InvoiceStreams
    }
    
    // 'value' attribute on PickerInput (id=PayerAccount_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 75, column 40
    function value_13 () : entity.Account {
      return newPayerAccount
    }
    
    // 'value' attribute on RangeInput (id=InvoiceStream_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 86, column 47
    function value_17 () : entity.InvoiceStream {
      return overridingInvoiceStream
    }
    
    // 'value' attribute on BooleanRadioInput (id=WhatItemsToAssign_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 93, column 46
    function value_24 () : java.lang.Boolean {
      return isAssignAllItemsSelected
    }
    
    // 'value' attribute on BooleanRadioInput (id=ReversePayments_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 103, column 48
    function value_27 () : java.lang.Boolean {
      return isReversePaymentsSelected
    }
    
    // 'value' attribute on BooleanRadioInput (id=CreateInvoiceForToday_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 108, column 44
    function value_30 () : java.lang.Boolean {
      return createInvoiceForToday
    }
    
    // 'valueRange' attribute on RangeInput (id=InvoiceStream_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 86, column 47
    function verifyValueRangeIsAllowedType_20 ($$arg :  entity.InvoiceStream[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=InvoiceStream_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 86, column 47
    function verifyValueRangeIsAllowedType_20 ($$arg :  gw.api.database.IQueryBeanResult<entity.InvoiceStream>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=InvoiceStream_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 86, column 47
    function verifyValueRangeIsAllowedType_20 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=InvoiceStream_Input) at AssignInvoiceItemsToNewPayerPopup.pcf: line 86, column 47
    function verifyValueRange_21 () : void {
      var __valueRangeArg = newPayerAccount.InvoiceStreams
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_20(__valueRangeArg)
    }
    
    // 'updateVisible' attribute on EditButtons at AssignInvoiceItemsToNewPayerPopup.pcf: line 55, column 52
    function visible_4 () : java.lang.Boolean {
      return newPayerAccount != null
    }
    
    // 'visible' attribute on AlertBar (id=notOwnerOrPayerWarning) at AssignInvoiceItemsToNewPayerPopup.pcf: line 60, column 44
    function visible_5 () : java.lang.Boolean {
      return hasNonOwnedOrPaidItem()
    }
    
    // 'visible' attribute on AlertBar (id=filteringFrozenItemsWarning) at AssignInvoiceItemsToNewPayerPopup.pcf: line 64, column 54
    function visible_7 () : java.lang.Boolean {
      return showFilteringFrozenItemsWarning()
    }
    
    override property get CurrentLocation () : pcf.AssignInvoiceItemsToNewPayerPopup {
      return super.CurrentLocation as pcf.AssignInvoiceItemsToNewPayerPopup
    }
    
    property get accountSearchConverter () : gw.api.web.account.AccountSearchConverter {
      return getVariableValue("accountSearchConverter", 0) as gw.api.web.account.AccountSearchConverter
    }
    
    property set accountSearchConverter ($arg :  gw.api.web.account.AccountSearchConverter) {
      setVariableValue("accountSearchConverter", 0, $arg)
    }
    
    property get chargesToAssign () : Charge[] {
      return getVariableValue("chargesToAssign", 0) as Charge[]
    }
    
    property set chargesToAssign ($arg :  Charge[]) {
      setVariableValue("chargesToAssign", 0, $arg)
    }
    
    property get createInvoiceForToday () : boolean {
      return getVariableValue("createInvoiceForToday", 0) as java.lang.Boolean
    }
    
    property set createInvoiceForToday ($arg :  boolean) {
      setVariableValue("createInvoiceForToday", 0, $arg)
    }
    
    property get currentAccount () : Account {
      return getVariableValue("currentAccount", 0) as Account
    }
    
    property set currentAccount ($arg :  Account) {
      setVariableValue("currentAccount", 0, $arg)
    }
    
    property get invoiceItems () : InvoiceItem[] {
      return getVariableValue("invoiceItems", 0) as InvoiceItem[]
    }
    
    property set invoiceItems ($arg :  InvoiceItem[]) {
      setVariableValue("invoiceItems", 0, $arg)
    }
    
    property get isAssignAllItemsSelected () : boolean {
      return getVariableValue("isAssignAllItemsSelected", 0) as java.lang.Boolean
    }
    
    property set isAssignAllItemsSelected ($arg :  boolean) {
      setVariableValue("isAssignAllItemsSelected", 0, $arg)
    }
    
    property get isReversePaymentsSelected () : boolean {
      return getVariableValue("isReversePaymentsSelected", 0) as java.lang.Boolean
    }
    
    property set isReversePaymentsSelected ($arg :  boolean) {
      setVariableValue("isReversePaymentsSelected", 0, $arg)
    }
    
    property get itemsToAssign () : InvoiceItem[] {
      return getVariableValue("itemsToAssign", 0) as InvoiceItem[]
    }
    
    property set itemsToAssign ($arg :  InvoiceItem[]) {
      setVariableValue("itemsToAssign", 0, $arg)
    }
    
    property get newPayerAccount () : Account {
      return getVariableValue("newPayerAccount", 0) as Account
    }
    
    property set newPayerAccount ($arg :  Account) {
      setVariableValue("newPayerAccount", 0, $arg)
    }
    
    property get overridingInvoiceStream () : InvoiceStream {
      return getVariableValue("overridingInvoiceStream", 0) as InvoiceStream
    }
    
    property set overridingInvoiceStream ($arg :  InvoiceStream) {
      setVariableValue("overridingInvoiceStream", 0, $arg)
    }
    
    
        function hasNonOwnedOrPaidItem() : boolean {
          return invoiceItems.hasMatch(\invoiceItem -> (invoiceItem.getOwner() != currentAccount) and (invoiceItem.getPayer() != currentAccount))
        }
    
        function getNotFullyPaidInvoiceItems() : InvoiceItem[] {
          return invoiceItems.where(\invoiceItem -> !invoiceItem.FullyConsumed)
        }
    
        function computeItemsBasedOnSelection() : InvoiceItem[] {
          return isAssignAllItemsSelected ? invoiceItems : getNotFullyPaidInvoiceItems()
        }
    
        function computeItemsToAssign() : InvoiceItem[] {
          return computeItemsBasedOnSelection().where(\invoiceItem -> !invoiceItem.Frozen)
        }
    
        function showFilteringFrozenItemsWarning() : boolean {
          return computeItemsBasedOnSelection().hasMatch(\item -> item.Frozen)
        }
    
        function assignItemsToPayer() {
          newPayerAccount.becomePayerOfInvoiceItems(
            createInvoiceForToday,
            overridingInvoiceStream,
            isReversePaymentsSelected
              ? gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem.Yes
              : gw.api.domain.invoice.ReversePaymentsWhenMovingInvoiceItem.No,
            itemsToAssign)
          AccountBalanceTxnUtil.createMoveItemRecord(currentAccount,
            itemsToAssign,
            AcctBalItemMovedType_Ext.TC_PAYERCHANGED)
          makeOverridesDefaultForAllPassedInCharges()
        }
    
        function makeOverridesDefaultForAllPassedInCharges() {
          for (chargeToAssign in chargesToAssign) {
            chargeToAssign.updateWith(new gw.api.web.invoice.InvoicingOverrider()
              .withOverridingPayerAccount(newPayerAccount)
              .withOverridingInvoiceStream(overridingInvoiceStream)
              .withShouldMoveInvoiceItems(false))
          }
        }
    
    
  }
  
  
}