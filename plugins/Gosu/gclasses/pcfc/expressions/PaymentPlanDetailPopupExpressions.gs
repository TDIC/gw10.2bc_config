package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PaymentPlanDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentPlanDetailPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PaymentPlanDetailPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentPlanDetailPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (paymentPlan :  PaymentPlan) : int {
      return 0
    }
    
    // 'canEdit' attribute on Popup (id=PaymentPlanDetailPopup) at PaymentPlanDetailPopup.pcf: line 8, column 87
    function canEdit_2 () : java.lang.Boolean {
      return perm.System.pmntplanedit
    }
    
    // 'def' attribute on ScreenRef at PaymentPlanDetailPopup.pcf: line 15, column 89
    function def_onEnter_0 (def :  pcf.PaymentPlanDetailScreen) : void {
      def.onEnter(paymentPlan, false/* null if not allowing edit */)
    }
    
    // 'def' attribute on ScreenRef at PaymentPlanDetailPopup.pcf: line 15, column 89
    function def_refreshVariables_1 (def :  pcf.PaymentPlanDetailScreen) : void {
      def.refreshVariables(paymentPlan, false/* null if not allowing edit */)
    }
    
    // 'title' attribute on Popup (id=PaymentPlanDetailPopup) at PaymentPlanDetailPopup.pcf: line 8, column 87
    static function title_3 (paymentPlan :  PaymentPlan) : java.lang.Object {
      return DisplayKey.get("Web.PaymentPlanDetail.Title", paymentPlan.Name)
    }
    
    override property get CurrentLocation () : pcf.PaymentPlanDetailPopup {
      return super.CurrentLocation as pcf.PaymentPlanDetailPopup
    }
    
    property get paymentPlan () : PaymentPlan {
      return getVariableValue("paymentPlan", 0) as PaymentPlan
    }
    
    property set paymentPlan ($arg :  PaymentPlan) {
      setVariableValue("paymentPlan", 0, $arg)
    }
    
    
  }
  
  
}