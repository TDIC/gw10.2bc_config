package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadAuthLimitProfileLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadAuthLimitProfileLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadAuthLimitProfileLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadAuthLimitProfileLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=Name_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.AuthorityLimitProfile.Name")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.AuthorityLimitProfile.PublicID")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 40, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.AuthorityLimitProfile.Description")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 45, column 42
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 23, column 46
    function sortValue_1 (authLimitProfile :  tdic.util.dataloader.data.admindata.AuthorityLimitProfileData) : java.lang.Object {
      return processor.getLoadStatus(authLimitProfile)
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 45, column 42
    function sortValue_10 (authLimitProfile :  tdic.util.dataloader.data.admindata.AuthorityLimitProfileData) : java.lang.Object {
      return authLimitProfile.Exclude
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 29, column 41
    function sortValue_4 (authLimitProfile :  tdic.util.dataloader.data.admindata.AuthorityLimitProfileData) : java.lang.Object {
      return authLimitProfile.Name
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 35, column 41
    function sortValue_6 (authLimitProfile :  tdic.util.dataloader.data.admindata.AuthorityLimitProfileData) : java.lang.Object {
      return authLimitProfile.PublicID
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 40, column 41
    function sortValue_8 (authLimitProfile :  tdic.util.dataloader.data.admindata.AuthorityLimitProfileData) : java.lang.Object {
      return authLimitProfile.Description
    }
    
    // 'value' attribute on RowIterator (id=AuthLimitProfile) at AdminDataUploadAuthLimitProfileLV.pcf: line 15, column 108
    function value_37 () : java.util.ArrayList<tdic.util.dataloader.data.admindata.AuthorityLimitProfileData> {
      return processor.AuthorityLimitProfileArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadAuthLimitProfileLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminDataUploadAuthLimitProfileLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at AdminDataUploadAuthLimitProfileLV.pcf: line 17, column 112
    function highlighted_36 () : java.lang.Boolean {
      return (authLimitProfile.Error or authLimitProfile.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 23, column 46
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=Name_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 29, column 41
    function label_16 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.AuthorityLimitProfile.Name")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 35, column 41
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.AuthorityLimitProfile.PublicID")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 40, column 41
    function label_26 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.AuthorityLimitProfile.Description")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 45, column 42
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 29, column 41
    function valueRoot_18 () : java.lang.Object {
      return authLimitProfile
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 23, column 46
    function value_12 () : java.lang.String {
      return processor.getLoadStatus(authLimitProfile)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 29, column 41
    function value_17 () : java.lang.String {
      return authLimitProfile.Name
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 35, column 41
    function value_22 () : java.lang.String {
      return authLimitProfile.PublicID
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 40, column 41
    function value_27 () : java.lang.String {
      return authLimitProfile.Description
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 45, column 42
    function value_32 () : java.lang.Boolean {
      return authLimitProfile.Exclude
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadAuthLimitProfileLV.pcf: line 23, column 46
    function visible_13 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get authLimitProfile () : tdic.util.dataloader.data.admindata.AuthorityLimitProfileData {
      return getIteratedValue(1) as tdic.util.dataloader.data.admindata.AuthorityLimitProfileData
    }
    
    
  }
  
  
}