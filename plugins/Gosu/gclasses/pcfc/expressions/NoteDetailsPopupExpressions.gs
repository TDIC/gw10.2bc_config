package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/note/NoteDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NoteDetailsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/note/NoteDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NoteDetailsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (Note :  Note) : int {
      return 0
    }
    
    // 'canEdit' attribute on Popup (id=NoteDetailsPopup) at NoteDetailsPopup.pcf: line 10, column 62
    function canEdit_3 () : java.lang.Boolean {
      return perm.Note.edit(Note)
    }
    
    // 'canVisit' attribute on Popup (id=NoteDetailsPopup) at NoteDetailsPopup.pcf: line 10, column 62
    static function canVisit_4 (Note :  Note) : java.lang.Boolean {
      return perm.Note.view(Note)
    }
    
    // 'def' attribute on PanelRef at NoteDetailsPopup.pcf: line 22, column 32
    function def_onEnter_1 (def :  pcf.NewNoteDV) : void {
      def.onEnter(Note)
    }
    
    // 'def' attribute on PanelRef at NoteDetailsPopup.pcf: line 22, column 32
    function def_refreshVariables_2 (def :  pcf.NewNoteDV) : void {
      def.refreshVariables(Note)
    }
    
    // EditButtons at NoteDetailsPopup.pcf: line 19, column 23
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    override property get CurrentLocation () : pcf.NoteDetailsPopup {
      return super.CurrentLocation as pcf.NoteDetailsPopup
    }
    
    property get Note () : Note {
      return getVariableValue("Note", 0) as Note
    }
    
    property set Note ($arg :  Note) {
      setVariableValue("Note", 0, $arg)
    }
    
    
  }
  
  
}