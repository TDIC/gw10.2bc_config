package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/AccountNewNegativeWriteoffWizardTargetStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountNewNegativeWriteoffWizardTargetStepScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/AccountNewNegativeWriteoffWizardTargetStepScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountNewNegativeWriteoffWizardTargetStepScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=SourceAccount_Input) at AccountNewNegativeWriteoffWizardTargetStepScreen.pcf: line 24, column 45
    function valueRoot_1 () : java.lang.Object {
      return uiWriteoff.WriteOff
    }
    
    // 'value' attribute on TextInput (id=SourceAccount_Input) at AccountNewNegativeWriteoffWizardTargetStepScreen.pcf: line 24, column 45
    function value_0 () : entity.TAccountOwner {
      return uiWriteoff.WriteOff.TAccountOwner
    }
    
    property get account () : Account {
      return getRequireValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setRequireValue("account", 0, $arg)
    }
    
    property get uiWriteoff () : gw.api.web.accounting.UIWriteOffCreation {
      return getRequireValue("uiWriteoff", 0) as gw.api.web.accounting.UIWriteOffCreation
    }
    
    property set uiWriteoff ($arg :  gw.api.web.accounting.UIWriteOffCreation) {
      setRequireValue("uiWriteoff", 0, $arg)
    }
    
    
  }
  
  
}