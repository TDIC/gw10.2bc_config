package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountSummary.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountSummaryExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountSummary.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountSummaryExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=AccountSummary) at AccountSummary.pcf: line 9, column 66
    static function canVisit_2 (account :  Account) : java.lang.Boolean {
      return perm.System.accttabview and perm.System.acctsummview
    }
    
    // 'def' attribute on ScreenRef at AccountSummary.pcf: line 16, column 44
    function def_onEnter_0 (def :  pcf.AccountSummaryScreen) : void {
      def.onEnter(account)
    }
    
    // 'def' attribute on ScreenRef at AccountSummary.pcf: line 16, column 44
    function def_refreshVariables_1 (def :  pcf.AccountSummaryScreen) : void {
      def.refreshVariables(account)
    }
    
    // Page (id=AccountSummary) at AccountSummary.pcf: line 9, column 66
    static function parent_3 (account :  Account) : pcf.api.Destination {
      return pcf.AccountOverview.createDestination(account)
    }
    
    override property get CurrentLocation () : pcf.AccountSummary {
      return super.CurrentLocation as pcf.AccountSummary
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    
  }
  
  
}