package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/UserSearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class UserSearchResultsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/UserSearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends UserSearchResultsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at UserSearchResultsLV.pcf: line 28, column 25
    function action_1 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // 'action' attribute on AltUserCell (id=DisplayName_Cell) at UserSearchResultsLV.pcf: line 28, column 25
    function action_3 () : void {
      UserDetailPage.go(User)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at UserSearchResultsLV.pcf: line 28, column 25
    function action_dest_2 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'action' attribute on AltUserCell (id=DisplayName_Cell) at UserSearchResultsLV.pcf: line 28, column 25
    function action_dest_4 () : pcf.api.Destination {
      return pcf.UserDetailPage.createDestination(User)
    }
    
    // 'canPick' attribute on RowIterator at UserSearchResultsLV.pcf: line 18, column 71
    function canPick_16 () : java.lang.Boolean {
      return User.isEditable()
    }
    
    // 'value' attribute on TextCell (id=Roles_Cell) at UserSearchResultsLV.pcf: line 41, column 57
    function valueRoot_11 () : java.lang.Object {
      return User
    }
    
    // 'value' attribute on TextCell (id=Username_Cell) at UserSearchResultsLV.pcf: line 34, column 25
    function valueRoot_8 () : java.lang.Object {
      return User.Credential
    }
    
    // 'value' attribute on TextCell (id=Roles_Cell) at UserSearchResultsLV.pcf: line 41, column 57
    function value_10 () : java.util.Set<entity.Role> {
      return User.AllRoles
    }
    
    // 'value' attribute on TextCell (id=GroupName_Cell) at UserSearchResultsLV.pcf: line 48, column 62
    function value_13 () : java.util.Set<java.lang.Object> {
      return User.AllGroups
    }
    
    // 'value' attribute on AltUserCell (id=DisplayName_Cell) at UserSearchResultsLV.pcf: line 28, column 25
    function value_5 () : entity.User {
      return User
    }
    
    // 'value' attribute on TextCell (id=Username_Cell) at UserSearchResultsLV.pcf: line 34, column 25
    function value_7 () : java.lang.String {
      return User.Credential.UserName
    }
    
    property get User () : entity.User {
      return getIteratedValue(1) as entity.User
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/UserSearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class UserSearchResultsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=Username_Cell) at UserSearchResultsLV.pcf: line 34, column 25
    function sortValue_0 (User :  entity.User) : java.lang.Object {
      return User.Credential.UserName
    }
    
    // 'value' attribute on RowIterator at UserSearchResultsLV.pcf: line 18, column 71
    function value_18 () : gw.api.database.IQueryBeanResult<entity.User> {
      return Users
    }
    
    property get Users () : gw.api.database.IQueryBeanResult<User> {
      return getRequireValue("Users", 0) as gw.api.database.IQueryBeanResult<User>
    }
    
    property set Users ($arg :  gw.api.database.IQueryBeanResult<User>) {
      setRequireValue("Users", 0, $arg)
    }
    
    
  }
  
  
}