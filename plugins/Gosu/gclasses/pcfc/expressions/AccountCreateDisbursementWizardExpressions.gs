package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/AccountCreateDisbursementWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountCreateDisbursementWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/AccountCreateDisbursementWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountCreateDisbursementWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (account :  Account) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Wizard (id=AccountCreateDisbursementWizard) at AccountCreateDisbursementWizard.pcf: line 10, column 42
    function beforeCommit_6 (pickedValue :  java.lang.Object) : void {
      disbursement.executeDisbursementOrCreateApprovalActivityIfNeeded()
    }
    
    // 'canVisit' attribute on Wizard (id=AccountCreateDisbursementWizard) at AccountCreateDisbursementWizard.pcf: line 10, column 42
    static function canVisit_7 (account :  Account) : java.lang.Boolean {
      return perm.Transaction.disbcreate
    }
    
    // 'initialValue' attribute on Variable at AccountCreateDisbursementWizard.pcf: line 19, column 35
    function initialValue_0 () : AccountDisbursement {
      return gw.account.CreateDisbursementWizardHelper.createDisbursement(account)
    }
    
    // 'onExit' attribute on WizardStep (id=Step1) at AccountCreateDisbursementWizard.pcf: line 25, column 97
    function onExit_1 () : void {
      initiateApprovalActivityIfUserLacksAuthority_TDIC(disbursement)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountCreateDisbursementWizard.pcf: line 25, column 97
    function screen_onEnter_2 (def :  pcf.CreateDisbursementDetailScreen) : void {
      def.onEnter(disbursement)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountCreateDisbursementWizard.pcf: line 30, column 97
    function screen_onEnter_4 (def :  pcf.CreateDisbursementConfirmScreen) : void {
      def.onEnter(disbursement)
    }
    
    // 'screen' attribute on WizardStep (id=Step1) at AccountCreateDisbursementWizard.pcf: line 25, column 97
    function screen_refreshVariables_3 (def :  pcf.CreateDisbursementDetailScreen) : void {
      def.refreshVariables(disbursement)
    }
    
    // 'screen' attribute on WizardStep (id=Step2) at AccountCreateDisbursementWizard.pcf: line 30, column 97
    function screen_refreshVariables_5 (def :  pcf.CreateDisbursementConfirmScreen) : void {
      def.refreshVariables(disbursement)
    }
    
    // 'tabBar' attribute on Wizard (id=AccountCreateDisbursementWizard) at AccountCreateDisbursementWizard.pcf: line 10, column 42
    function tabBar_onEnter_8 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=AccountCreateDisbursementWizard) at AccountCreateDisbursementWizard.pcf: line 10, column 42
    function tabBar_refreshVariables_9 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AccountCreateDisbursementWizard {
      return super.CurrentLocation as pcf.AccountCreateDisbursementWizard
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get disbursement () : AccountDisbursement {
      return getVariableValue("disbursement", 0) as AccountDisbursement
    }
    
    property set disbursement ($arg :  AccountDisbursement) {
      setVariableValue("disbursement", 0, $arg)
    }
    
    function initiateApprovalActivityIfUserLacksAuthority_TDIC(disburse : Disbursement) {
      //VIJAY B - Sprint-4 - GBC-1053 - Manual disbursements are assigning to the Approval Disbursement Queue
      var activity = disbursement.initiateApprovalActivityIfUserLacksAuthority()
      var groupName = gw.command.demo.GeneralUtil.findGroupByUserName("Approve Disbursements")
      if (activity?.assignGroup(groupName)) {
        var queueName = activity.AssignedGroup.getQueue("Approve Disbursements")
        if (queueName != null) {
          activity.assignActivityToQueue(queueName, groupName)
        }
      }
    }
    
    
  }
  
  
}