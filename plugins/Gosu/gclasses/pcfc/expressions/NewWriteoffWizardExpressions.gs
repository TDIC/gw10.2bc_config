package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewWriteoffWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/transaction/NewWriteoffWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewWriteoffWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    static function __constructorIndex (passedInTarget :  entity.TAccountOwner) : int {
      return 1
    }
    
    // 'afterCancel' attribute on Wizard (id=NewWriteoffWizard) at NewWriteoffWizard.pcf: line 11, column 28
    function afterCancel_11 () : void {
      DesktopGroup.go()
    }
    
    // 'afterCancel' attribute on Wizard (id=NewWriteoffWizard) at NewWriteoffWizard.pcf: line 11, column 28
    function afterCancel_dest_12 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewWriteoffWizard) at NewWriteoffWizard.pcf: line 11, column 28
    function afterFinish_17 () : void {
      DesktopGroup.go()
    }
    
    // 'afterFinish' attribute on Wizard (id=NewWriteoffWizard) at NewWriteoffWizard.pcf: line 11, column 28
    function afterFinish_dest_18 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    // 'allowFinish' attribute on WizardStep (id=ConfirmationStep) at NewWriteoffWizard.pcf: line 44, column 85
    function allowFinish_8 () : java.lang.Boolean {
      return uiWriteoff.WriteOff.Amount != null
    }
    
    // 'allowNext' attribute on WizardStep (id=TargetStep) at NewWriteoffWizard.pcf: line 32, column 79
    function allowNext_1 () : java.lang.Boolean {
      return targetOfWriteoff.TAccountOwner != null
    }
    
    // 'beforeCommit' attribute on Wizard (id=NewWriteoffWizard) at NewWriteoffWizard.pcf: line 11, column 28
    function beforeCommit_13 (pickedValue :  java.lang.Object) : void {
      uiWriteoff.doWriteOff()
    }
    
    // 'canVisit' attribute on Wizard (id=NewWriteoffWizard) at NewWriteoffWizard.pcf: line 11, column 28
    static function canVisit_14 (passedInTarget :  entity.TAccountOwner) : java.lang.Boolean {
      return perm.Transaction.acctwo or perm.Transaction.plcywo
    }
    
    // 'initialValue' attribute on Variable at NewWriteoffWizard.pcf: line 22, column 60
    function initialValue_0 () : gw.api.web.accounting.TAccountOwnerReference {
      return createTargetOfWriteoffReference()
    }
    
    // 'onExit' attribute on WizardStep (id=TargetStep) at NewWriteoffWizard.pcf: line 32, column 79
    function onExit_2 () : void {
      onExitTargetStep()
    }
    
    // 'onExit' attribute on WizardStep (id=DetailsStep) at NewWriteoffWizard.pcf: line 38, column 80
    function onExit_5 () : void {
      uiWriteoff.initiateApprovalActivityIfUserLacksAuthority()
    }
    
    // 'screen' attribute on WizardStep (id=TargetStep) at NewWriteoffWizard.pcf: line 32, column 79
    function screen_onEnter_3 (def :  pcf.WizardsStep1AccountPolicySearchScreen) : void {
      def.onEnter(targetOfWriteoff, selectPolicyOrAccount(), false, true)
    }
    
    // 'screen' attribute on WizardStep (id=DetailsStep) at NewWriteoffWizard.pcf: line 38, column 80
    function screen_onEnter_6 (def :  pcf.NewWriteoffWizardDetailsStepScreen) : void {
      def.onEnter(uiWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at NewWriteoffWizard.pcf: line 44, column 85
    function screen_onEnter_9 (def :  pcf.NewWriteoffWizardConfirmationStepScreen) : void {
      def.onEnter(uiWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at NewWriteoffWizard.pcf: line 44, column 85
    function screen_refreshVariables_10 (def :  pcf.NewWriteoffWizardConfirmationStepScreen) : void {
      def.refreshVariables(uiWriteoff)
    }
    
    // 'screen' attribute on WizardStep (id=TargetStep) at NewWriteoffWizard.pcf: line 32, column 79
    function screen_refreshVariables_4 (def :  pcf.WizardsStep1AccountPolicySearchScreen) : void {
      def.refreshVariables(targetOfWriteoff, selectPolicyOrAccount(), false, true)
    }
    
    // 'screen' attribute on WizardStep (id=DetailsStep) at NewWriteoffWizard.pcf: line 38, column 80
    function screen_refreshVariables_7 (def :  pcf.NewWriteoffWizardDetailsStepScreen) : void {
      def.refreshVariables(uiWriteoff)
    }
    
    // 'tabBar' attribute on Wizard (id=NewWriteoffWizard) at NewWriteoffWizard.pcf: line 11, column 28
    function tabBar_onEnter_15 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=NewWriteoffWizard) at NewWriteoffWizard.pcf: line 11, column 28
    function tabBar_refreshVariables_16 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.NewWriteoffWizard {
      return super.CurrentLocation as pcf.NewWriteoffWizard
    }
    
    property get passedInTarget () : TAccountOwner {
      return getVariableValue("passedInTarget", 0) as TAccountOwner
    }
    
    property set passedInTarget ($arg :  TAccountOwner) {
      setVariableValue("passedInTarget", 0, $arg)
    }
    
    property get targetOfWriteoff () : gw.api.web.accounting.TAccountOwnerReference {
      return getVariableValue("targetOfWriteoff", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set targetOfWriteoff ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setVariableValue("targetOfWriteoff", 0, $arg)
    }
    
    property get uiWriteoff () : gw.api.web.accounting.UIWriteOffCreation {
      return getVariableValue("uiWriteoff", 0) as gw.api.web.accounting.UIWriteOffCreation
    }
    
    property set uiWriteoff ($arg :  gw.api.web.accounting.UIWriteOffCreation) {
      setVariableValue("uiWriteoff", 0, $arg)
    }
    
    
                  function selectPolicyOrAccount() : TAccountOwnerType {
                    if (perm.Transaction.acctwo) {
                      if (perm.Transaction.plcywo) {
                        return null;
                        // both are allowed
                      } else {
                        return TAccountOwnerType.TC_ACCOUNT;
                      }
                    }
                    // assuming that at least one of them is allowed
                    return TAccountOwnerType.TC_POLICYPERIOD;
                  }
    
                  function onExitTargetStep() {
                    if (uiWriteoff == null || uiWriteoff.WriteOff.TAccountOwner != targetOfWriteoff.TAccountOwner) {
                      createNewUIWriteoff()
                    }
                  }
    
                  function createNewUIWriteoff() {
                    if (uiWriteoff != null) {
                      uiWriteoff.cleanupBeforeDiscard()
                    }
                    var writeOffFactory = new gw.api.web.accounting.WriteOffFactory(CurrentLocation)
                    var writeOff = writeOffFactory.createChargeGrossWriteOff(targetOfWriteoff.TAccountOwner)
                    uiWriteoff = new gw.api.web.accounting.UIWriteOffCreation(writeOff)
                  }
    
                  function createTargetOfWriteoffReference() : gw.api.web.accounting.TAccountOwnerReference {
                    var target = new gw.api.web.accounting.TAccountOwnerReference()
                    target.TAccountOwner = passedInTarget
                    return target
                  }
          
    
    
  }
  
  
}