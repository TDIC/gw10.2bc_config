package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/audit_ext/AdminAuditEntriesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminAuditEntriesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/audit_ext/AdminAuditEntriesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminAuditEntriesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=modifiedEntity_Cell) at AdminAuditEntriesLV.pcf: line 19, column 45
    function sortValue_0 (entry :  AdminAudit_Ext) : java.lang.Object {
      return entry.ModifiedEntityName
    }
    
    // 'value' attribute on TextCell (id=TiggeringEvent_Cell) at AdminAuditEntriesLV.pcf: line 24, column 41
    function sortValue_1 (entry :  AdminAudit_Ext) : java.lang.Object {
      return entry.TriggerEventName
    }
    
    // 'value' attribute on TextCell (id=ModifiedObjectName_Cell) at AdminAuditEntriesLV.pcf: line 29, column 41
    function sortValue_2 (entry :  AdminAudit_Ext) : java.lang.Object {
      return entry.ModifiedObjectName
    }
    
    // 'value' attribute on TextCell (id=modifiedField_Cell) at AdminAuditEntriesLV.pcf: line 38, column 44
    function sortValue_3 (entry :  AdminAudit_Ext) : java.lang.Object {
      return entry.ModifiedFieldName
    }
    
    // 'value' attribute on TextCell (id=originalValue_Cell) at AdminAuditEntriesLV.pcf: line 42, column 40
    function sortValue_4 (entry :  AdminAudit_Ext) : java.lang.Object {
      return entry.OriginalValue
    }
    
    // 'value' attribute on TextCell (id=newValue_Cell) at AdminAuditEntriesLV.pcf: line 46, column 35
    function sortValue_5 (entry :  AdminAudit_Ext) : java.lang.Object {
      return entry.NewValue
    }
    
    // 'value' attribute on TextCell (id=modifiedByUserName_Cell) at AdminAuditEntriesLV.pcf: line 50, column 45
    function sortValue_6 (entry :  AdminAudit_Ext) : java.lang.Object {
      return entry.ModifiedByUserName
    }
    
    // 'value' attribute on TextCell (id=modifiedByUserId_Cell) at AdminAuditEntriesLV.pcf: line 54, column 43
    function sortValue_7 (entry :  AdminAudit_Ext) : java.lang.Object {
      return entry.ModifiedByUserID
    }
    
    // 'value' attribute on RowIterator at AdminAuditEntriesLV.pcf: line 14, column 74
    function value_35 () : gw.api.database.IQueryBeanResult<AdminAudit_Ext> {
      return AuditEntries
    }
    
    property get AuditEntries () : gw.api.database.IQueryBeanResult<AdminAudit_Ext> {
      return getRequireValue("AuditEntries", 0) as gw.api.database.IQueryBeanResult<AdminAudit_Ext>
    }
    
    property set AuditEntries ($arg :  gw.api.database.IQueryBeanResult<AdminAudit_Ext>) {
      setRequireValue("AuditEntries", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/audit_ext/AdminAuditEntriesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminAuditEntriesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextCell (id=modifiedEntity_Cell) at AdminAuditEntriesLV.pcf: line 19, column 45
    function valueRoot_9 () : java.lang.Object {
      return entry
    }
    
    // 'value' attribute on TextCell (id=TiggeringEvent_Cell) at AdminAuditEntriesLV.pcf: line 24, column 41
    function value_11 () : java.lang.String {
      return entry.TriggerEventName
    }
    
    // 'value' attribute on TextCell (id=ModifiedObjectName_Cell) at AdminAuditEntriesLV.pcf: line 29, column 41
    function value_14 () : java.lang.String {
      return entry.ModifiedObjectName
    }
    
    // 'value' attribute on DateCell (id=modifiedDate_Cell) at AdminAuditEntriesLV.pcf: line 34, column 39
    function value_17 () : java.util.Date {
      return entry.ModifiedDate
    }
    
    // 'value' attribute on TextCell (id=modifiedField_Cell) at AdminAuditEntriesLV.pcf: line 38, column 44
    function value_20 () : java.lang.String {
      return entry.ModifiedFieldName
    }
    
    // 'value' attribute on TextCell (id=originalValue_Cell) at AdminAuditEntriesLV.pcf: line 42, column 40
    function value_23 () : java.lang.String {
      return entry.OriginalValue
    }
    
    // 'value' attribute on TextCell (id=newValue_Cell) at AdminAuditEntriesLV.pcf: line 46, column 35
    function value_26 () : java.lang.String {
      return entry.NewValue
    }
    
    // 'value' attribute on TextCell (id=modifiedByUserName_Cell) at AdminAuditEntriesLV.pcf: line 50, column 45
    function value_29 () : java.lang.String {
      return entry.ModifiedByUserName
    }
    
    // 'value' attribute on TextCell (id=modifiedByUserId_Cell) at AdminAuditEntriesLV.pcf: line 54, column 43
    function value_32 () : java.lang.String {
      return entry.ModifiedByUserID
    }
    
    // 'value' attribute on TextCell (id=modifiedEntity_Cell) at AdminAuditEntriesLV.pcf: line 19, column 45
    function value_8 () : java.lang.String {
      return entry.ModifiedEntityName
    }
    
    property get entry () : AdminAudit_Ext {
      return getIteratedValue(1) as AdminAudit_Ext
    }
    
    
  }
  
  
}