package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/CreateSuspenseDisbursementDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreateSuspenseDisbursementDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/disbursement/CreateSuspenseDisbursementDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreateSuspenseDisbursementDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on ListViewInput (id=suspensePaymentTransactions) at CreateSuspenseDisbursementDetailScreen.pcf: line 52, column 28
    function def_onEnter_16 (def :  pcf.SuspensePaymentTransactionsLV) : void {
      def.onEnter(disbursementVar.SuspensePayment)
    }
    
    // 'def' attribute on PanelRef at CreateSuspenseDisbursementDetailScreen.pcf: line 58, column 65
    function def_onEnter_18 (def :  pcf.CreateDisbursementDetailDV) : void {
      def.onEnter(disbursementVar, false)
    }
    
    // 'def' attribute on ListViewInput (id=suspensePaymentTransactions) at CreateSuspenseDisbursementDetailScreen.pcf: line 52, column 28
    function def_refreshVariables_17 (def :  pcf.SuspensePaymentTransactionsLV) : void {
      def.refreshVariables(disbursementVar.SuspensePayment)
    }
    
    // 'def' attribute on PanelRef at CreateSuspenseDisbursementDetailScreen.pcf: line 58, column 65
    function def_refreshVariables_19 (def :  pcf.CreateDisbursementDetailDV) : void {
      def.refreshVariables(disbursementVar, false)
    }
    
    // 'initialValue' attribute on Variable at CreateSuspenseDisbursementDetailScreen.pcf: line 13, column 30
    function initialValue_0 () : java.util.Date {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'initialValue' attribute on Variable at CreateSuspenseDisbursementDetailScreen.pcf: line 17, column 23
    function initialValue_1 () : Boolean {
      return perm.Transaction.disbpayeeedit
    }
    
    // 'initialValue' attribute on Variable at CreateSuspenseDisbursementDetailScreen.pcf: line 21, column 67
    function initialValue_2 () : gw.api.database.IQueryBeanResult<Transaction> {
      return disbursementVar.SuspensePayment.Transactions as gw.api.database.IQueryBeanResult<Transaction>
    }
    
    // 'value' attribute on TextInput (id=accountNumber_Input) at CreateSuspenseDisbursementDetailScreen.pcf: line 34, column 76
    function valueRoot_5 () : java.lang.Object {
      return disbursementVar.SuspensePayment
    }
    
    // 'value' attribute on DateInput (id=paymentDate_Input) at CreateSuspenseDisbursementDetailScreen.pcf: line 43, column 64
    function value_13 () : java.util.Date {
      return disbursementVar.SuspensePayment.PaymentDate
    }
    
    // 'value' attribute on TextInput (id=accountNumber_Input) at CreateSuspenseDisbursementDetailScreen.pcf: line 34, column 76
    function value_4 () : java.lang.String {
      return disbursementVar.SuspensePayment.AccountNumber
    }
    
    // 'value' attribute on TextInput (id=policyNumber_Input) at CreateSuspenseDisbursementDetailScreen.pcf: line 39, column 75
    function value_9 () : java.lang.String {
      return disbursementVar.SuspensePayment.PolicyNumber
    }
    
    // 'visible' attribute on TextInput (id=accountNumber_Input) at CreateSuspenseDisbursementDetailScreen.pcf: line 34, column 76
    function visible_3 () : java.lang.Boolean {
      return disbursementVar.SuspensePayment.AccountNumber != null
    }
    
    // 'visible' attribute on TextInput (id=policyNumber_Input) at CreateSuspenseDisbursementDetailScreen.pcf: line 39, column 75
    function visible_8 () : java.lang.Boolean {
      return disbursementVar.SuspensePayment.PolicyNumber != null
    }
    
    property get CanEditDisbursementPayee () : Boolean {
      return getVariableValue("CanEditDisbursementPayee", 0) as Boolean
    }
    
    property set CanEditDisbursementPayee ($arg :  Boolean) {
      setVariableValue("CanEditDisbursementPayee", 0, $arg)
    }
    
    property get disbursementVar () : SuspenseDisbursement {
      return getRequireValue("disbursementVar", 0) as SuspenseDisbursement
    }
    
    property set disbursementVar ($arg :  SuspenseDisbursement) {
      setRequireValue("disbursementVar", 0, $arg)
    }
    
    property get today () : java.util.Date {
      return getVariableValue("today", 0) as java.util.Date
    }
    
    property set today ($arg :  java.util.Date) {
      setVariableValue("today", 0, $arg)
    }
    
    property get transactionsQuery () : gw.api.database.IQueryBeanResult<Transaction> {
      return getVariableValue("transactionsQuery", 0) as gw.api.database.IQueryBeanResult<Transaction>
    }
    
    property set transactionsQuery ($arg :  gw.api.database.IQueryBeanResult<Transaction>) {
      setVariableValue("transactionsQuery", 0, $arg)
    }
    
    
  }
  
  
}