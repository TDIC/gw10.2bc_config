package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/payment/PaymentRequestDetailPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PaymentRequestDetailPageExpressions {
  @javax.annotation.Generated("config/web/pcf/payment/PaymentRequestDetailPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PaymentRequestDetailPageExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (oldPaymentRequest :  PaymentRequest, shouldStartInEditMode :  Boolean) : int {
      return 0
    }
    
    // 'def' attribute on PanelRef at PaymentRequestDetailPage.pcf: line 29, column 25
    function def_onEnter_2 (def :  pcf.PaymentRequestDV) : void {
      def.onEnter(newPaymentRequest)
    }
    
    // 'def' attribute on PanelRef at PaymentRequestDetailPage.pcf: line 29, column 25
    function def_refreshVariables_3 (def :  pcf.PaymentRequestDV) : void {
      def.refreshVariables(newPaymentRequest)
    }
    
    // 'initialValue' attribute on Variable at PaymentRequestDetailPage.pcf: line 22, column 30
    function initialValue_0 () : PaymentRequest {
      return oldPaymentRequest.editPaymentRequest()
    }
    
    // EditButtons at PaymentRequestDetailPage.pcf: line 25, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'parent' attribute on Page (id=PaymentRequestDetailPage) at PaymentRequestDetailPage.pcf: line 10, column 76
    static function parent_4 (oldPaymentRequest :  PaymentRequest, shouldStartInEditMode :  Boolean) : pcf.api.Destination {
      return pcf.AccountGroup.createDestination(oldPaymentRequest.Account)
    }
    
    // 'startInEditMode' attribute on Page (id=PaymentRequestDetailPage) at PaymentRequestDetailPage.pcf: line 10, column 76
    function startInEditMode_5 () : java.lang.Boolean {
      return shouldStartInEditMode
    }
    
    override property get CurrentLocation () : pcf.PaymentRequestDetailPage {
      return super.CurrentLocation as pcf.PaymentRequestDetailPage
    }
    
    property get newPaymentRequest () : PaymentRequest {
      return getVariableValue("newPaymentRequest", 0) as PaymentRequest
    }
    
    property set newPaymentRequest ($arg :  PaymentRequest) {
      setVariableValue("newPaymentRequest", 0, $arg)
    }
    
    property get oldPaymentRequest () : PaymentRequest {
      return getVariableValue("oldPaymentRequest", 0) as PaymentRequest
    }
    
    property set oldPaymentRequest ($arg :  PaymentRequest) {
      setVariableValue("oldPaymentRequest", 0, $arg)
    }
    
    property get shouldStartInEditMode () : Boolean {
      return getVariableValue("shouldStartInEditMode", 0) as Boolean
    }
    
    property set shouldStartInEditMode ($arg :  Boolean) {
      setVariableValue("shouldStartInEditMode", 0, $arg)
    }
    
    
  }
  
  
}