package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillStatementDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillStatementDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillStatementDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillStatementDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (cycle :  AgencyBillCycle) : int {
      return 0
    }
    
    // 'def' attribute on ScreenRef at AgencyBillStatementDetail.pcf: line 17, column 53
    function def_onEnter_0 (def :  pcf.AgencyBillStatementDetailScreen) : void {
      def.onEnter(cycle)
    }
    
    // 'def' attribute on ScreenRef at AgencyBillStatementDetail.pcf: line 17, column 53
    function def_refreshVariables_1 (def :  pcf.AgencyBillStatementDetailScreen) : void {
      def.refreshVariables(cycle)
    }
    
    // 'parent' attribute on Page (id=AgencyBillStatementDetail) at AgencyBillStatementDetail.pcf: line 10, column 115
    static function parent_2 (cycle :  AgencyBillCycle) : pcf.api.Destination {
      return pcf.ProducerAgencyBillCycles.createDestination(cycle.Producer)
    }
    
    // 'title' attribute on Page (id=AgencyBillStatementDetail) at AgencyBillStatementDetail.pcf: line 10, column 115
    static function title_3 (cycle :  AgencyBillCycle) : java.lang.Object {
      return DisplayKey.get("Web.AgencyBillStatementDetail.Title", cycle.StatementInvoice.InvoiceNumber)
    }
    
    override property get CurrentLocation () : pcf.AgencyBillStatementDetail {
      return super.CurrentLocation as pcf.AgencyBillStatementDetail
    }
    
    property get cycle () : AgencyBillCycle {
      return getVariableValue("cycle", 0) as AgencyBillCycle
    }
    
    property set cycle ($arg :  AgencyBillCycle) {
      setVariableValue("cycle", 0, $arg)
    }
    
    
  }
  
  
}