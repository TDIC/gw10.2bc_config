package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/troubleticket/CreateTroubleTicketEntitiesScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreateTroubleTicketEntitiesScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/troubleticket/CreateTroubleTicketEntitiesScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreateTroubleTicketEntitiesScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at CreateTroubleTicketEntitiesScreen.pcf: line 22, column 86
    function def_onEnter_0 (def :  pcf.TroubleTicketRelatedEntitiesDV) : void {
      def.onEnter(TroubleTicket, CreateTroubleTicketHelper)
    }
    
    // 'def' attribute on PanelRef at CreateTroubleTicketEntitiesScreen.pcf: line 22, column 86
    function def_refreshVariables_1 (def :  pcf.TroubleTicketRelatedEntitiesDV) : void {
      def.refreshVariables(TroubleTicket, CreateTroubleTicketHelper)
    }
    
    property get AssigneeHolder () : gw.api.assignment.Assignee[] {
      return getRequireValue("AssigneeHolder", 0) as gw.api.assignment.Assignee[]
    }
    
    property set AssigneeHolder ($arg :  gw.api.assignment.Assignee[]) {
      setRequireValue("AssigneeHolder", 0, $arg)
    }
    
    property get CreateTroubleTicketHelper () : CreateTroubleTicketHelper {
      return getRequireValue("CreateTroubleTicketHelper", 0) as CreateTroubleTicketHelper
    }
    
    property set CreateTroubleTicketHelper ($arg :  CreateTroubleTicketHelper) {
      setRequireValue("CreateTroubleTicketHelper", 0, $arg)
    }
    
    property get TroubleTicket () : TroubleTicket {
      return getRequireValue("TroubleTicket", 0) as TroubleTicket
    }
    
    property set TroubleTicket ($arg :  TroubleTicket) {
      setRequireValue("TroubleTicket", 0, $arg)
    }
    
    
  }
  
  
}