package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/desktop/DesktopDisbursements.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DesktopDisbursementsExpressions {
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopDisbursements.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopDisbursementsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=DesktopDisbursements) at DesktopDisbursements.pcf: line 9, column 66
    static function canVisit_22 () : java.lang.Boolean {
      return perm.System.mydisbview and perm.System.viewdesktop
    }
    
    // Page (id=DesktopDisbursements) at DesktopDisbursements.pcf: line 9, column 66
    static function parent_23 () : pcf.api.Destination {
      return pcf.DesktopGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.DesktopDisbursements {
      return super.CurrentLocation as pcf.DesktopDisbursements
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/desktop/DesktopDisbursements.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DesktopDisbursementsScreenExpressionsImpl extends DesktopDisbursementsExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=NewDisbursement) at DesktopDisbursements.pcf: line 58, column 85
    function action_17 () : void {
      CreateDisbursementWizard.go()
    }
    
    // 'action' attribute on ToolbarButton (id=NewDisbursement) at DesktopDisbursements.pcf: line 58, column 85
    function action_dest_18 () : pcf.api.Destination {
      return pcf.CreateDisbursementWizard.createDestination()
    }
    
    // 'checkedRowAction' attribute on CheckedValuesToolbarButton (id=Approve_TDIC) at DesktopDisbursements.pcf: line 65, column 63
    function checkedRowAction_19 (element :  Disbursement, CheckedValue :  Disbursement) : void {
      CheckedValue.getOpenApprovalActivity().approve();
    }
    
    // 'def' attribute on PanelRef at DesktopDisbursements.pcf: line 31, column 53
    function def_onEnter_20 (def :  pcf.DesktopDisbursementsLV) : void {
      def.onEnter(disbursements)
    }
    
    // 'def' attribute on PanelRef at DesktopDisbursements.pcf: line 31, column 53
    function def_refreshVariables_21 (def :  pcf.DesktopDisbursementsLV) : void {
      def.refreshVariables(disbursements)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=disbursementDisplayDateRange_Input) at DesktopDisbursements.pcf: line 52, column 78
    function defaultSetter_11 (__VALUE_TO_SET :  java.lang.Object) : void {
      disbursementDisplayDateRange = (__VALUE_TO_SET as gw.api.web.disbursement.DisbursementDisplayDateRange)
    }
    
    // 'value' attribute on ToolbarRangeInput (id=disbursementDisplayStatus_Input) at DesktopDisbursements.pcf: line 41, column 75
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      disbursementDisplayStatus = (__VALUE_TO_SET as gw.api.web.disbursement.DisbursementDisplayStatus)
    }
    
    // 'initialValue' attribute on Variable at DesktopDisbursements.pcf: line 17, column 70
    function initialValue_0 () : gw.api.web.disbursement.DisbursementDisplayDateRange {
      return gw.api.web.disbursement.DisbursementDisplayDateRange.LAST7DAYS
    }
    
    // 'initialValue' attribute on Variable at DesktopDisbursements.pcf: line 21, column 67
    function initialValue_1 () : gw.api.web.disbursement.DisbursementDisplayStatus {
      return gw.api.web.disbursement.DisbursementDisplayStatus.OPEN_ONLY
    }
    
    // 'initialValue' attribute on Variable at DesktopDisbursements.pcf: line 26, column 70
    function initialValue_2 () : gw.api.database.IQueryBeanResult<Disbursement> {
      return new gw.api.web.disbursement.DisbursementUtil().getDisbursements(disbursementDisplayStatus, disbursementDisplayDateRange) as gw.api.database.IQueryBeanResult<Disbursement>
    }
    
    // 'optionLabel' attribute on ToolbarRangeInput (id=disbursementDisplayDateRange_Input) at DesktopDisbursements.pcf: line 52, column 78
    function optionLabel_12 (VALUE :  gw.api.web.disbursement.DisbursementDisplayDateRange) : java.lang.String {
      return VALUE.DisplayKey as String
    }
    
    // 'optionLabel' attribute on ToolbarRangeInput (id=disbursementDisplayStatus_Input) at DesktopDisbursements.pcf: line 41, column 75
    function optionLabel_5 (VALUE :  gw.api.web.disbursement.DisbursementDisplayStatus) : java.lang.String {
      return VALUE.DisplayKey as String
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=disbursementDisplayDateRange_Input) at DesktopDisbursements.pcf: line 52, column 78
    function valueRange_13 () : java.lang.Object {
      return gw.api.web.disbursement.DisbursementDisplayDateRange.AllDisbursementDisplayDateRanges
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=disbursementDisplayStatus_Input) at DesktopDisbursements.pcf: line 41, column 75
    function valueRange_6 () : java.lang.Object {
      return gw.api.web.disbursement.DisbursementDisplayStatus.AllDisbursementDisplayStatuses
    }
    
    // 'value' attribute on ToolbarRangeInput (id=disbursementDisplayDateRange_Input) at DesktopDisbursements.pcf: line 52, column 78
    function value_10 () : gw.api.web.disbursement.DisbursementDisplayDateRange {
      return disbursementDisplayDateRange
    }
    
    // 'value' attribute on ToolbarRangeInput (id=disbursementDisplayStatus_Input) at DesktopDisbursements.pcf: line 41, column 75
    function value_3 () : gw.api.web.disbursement.DisbursementDisplayStatus {
      return disbursementDisplayStatus
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=disbursementDisplayDateRange_Input) at DesktopDisbursements.pcf: line 52, column 78
    function verifyValueRangeIsAllowedType_14 ($$arg :  gw.api.web.disbursement.DisbursementDisplayDateRange[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=disbursementDisplayDateRange_Input) at DesktopDisbursements.pcf: line 52, column 78
    function verifyValueRangeIsAllowedType_14 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=disbursementDisplayStatus_Input) at DesktopDisbursements.pcf: line 41, column 75
    function verifyValueRangeIsAllowedType_7 ($$arg :  gw.api.web.disbursement.DisbursementDisplayStatus[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=disbursementDisplayStatus_Input) at DesktopDisbursements.pcf: line 41, column 75
    function verifyValueRangeIsAllowedType_7 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=disbursementDisplayDateRange_Input) at DesktopDisbursements.pcf: line 52, column 78
    function verifyValueRange_15 () : void {
      var __valueRangeArg = gw.api.web.disbursement.DisbursementDisplayDateRange.AllDisbursementDisplayDateRanges
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_14(__valueRangeArg)
    }
    
    // 'valueRange' attribute on ToolbarRangeInput (id=disbursementDisplayStatus_Input) at DesktopDisbursements.pcf: line 41, column 75
    function verifyValueRange_8 () : void {
      var __valueRangeArg = gw.api.web.disbursement.DisbursementDisplayStatus.AllDisbursementDisplayStatuses
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_7(__valueRangeArg)
    }
    
    property get disbursementDisplayDateRange () : gw.api.web.disbursement.DisbursementDisplayDateRange {
      return getVariableValue("disbursementDisplayDateRange", 1) as gw.api.web.disbursement.DisbursementDisplayDateRange
    }
    
    property set disbursementDisplayDateRange ($arg :  gw.api.web.disbursement.DisbursementDisplayDateRange) {
      setVariableValue("disbursementDisplayDateRange", 1, $arg)
    }
    
    property get disbursementDisplayStatus () : gw.api.web.disbursement.DisbursementDisplayStatus {
      return getVariableValue("disbursementDisplayStatus", 1) as gw.api.web.disbursement.DisbursementDisplayStatus
    }
    
    property set disbursementDisplayStatus ($arg :  gw.api.web.disbursement.DisbursementDisplayStatus) {
      setVariableValue("disbursementDisplayStatus", 1, $arg)
    }
    
    property get disbursements () : gw.api.database.IQueryBeanResult<Disbursement> {
      return getVariableValue("disbursements", 1) as gw.api.database.IQueryBeanResult<Disbursement>
    }
    
    property set disbursements ($arg :  gw.api.database.IQueryBeanResult<Disbursement>) {
      setVariableValue("disbursements", 1, $arg)
    }
    
    
  }
  
  
}