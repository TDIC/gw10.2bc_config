package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/ProducerDetailTroubleTickets.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerDetailTroubleTicketsExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/ProducerDetailTroubleTickets.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerDetailTroubleTicketsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'action' attribute on ToolbarButton (id=NewTroubleTicket) at ProducerDetailTroubleTickets.pcf: line 25, column 50
    function action_1 () : void {
      CreateTroubleTicketWizard.push(producer)
    }
    
    // 'action' attribute on ToolbarButton (id=NewTroubleTicket) at ProducerDetailTroubleTickets.pcf: line 25, column 50
    function action_dest_2 () : pcf.api.Destination {
      return pcf.CreateTroubleTicketWizard.createDestination(producer)
    }
    
    // 'canVisit' attribute on Page (id=ProducerDetailTroubleTickets) at ProducerDetailTroubleTickets.pcf: line 9, column 80
    static function canVisit_5 (producer :  Producer) : java.lang.Boolean {
      return perm.System.prodtabview and perm.System.prodttktview
    }
    
    // 'def' attribute on PanelRef at ProducerDetailTroubleTickets.pcf: line 18, column 57
    function def_onEnter_3 (def :  pcf.TroubleTicketsLV) : void {
      def.onEnter(producer.TroubleTickets)
    }
    
    // 'def' attribute on PanelRef at ProducerDetailTroubleTickets.pcf: line 18, column 57
    function def_refreshVariables_4 (def :  pcf.TroubleTicketsLV) : void {
      def.refreshVariables(producer.TroubleTickets)
    }
    
    // Page (id=ProducerDetailTroubleTickets) at ProducerDetailTroubleTickets.pcf: line 9, column 80
    static function parent_6 (producer :  Producer) : pcf.api.Destination {
      return pcf.ProducerDetailGroup.createDestination(producer)
    }
    
    // 'visible' attribute on ToolbarButton (id=NewTroubleTicket) at ProducerDetailTroubleTickets.pcf: line 25, column 50
    function visible_0 () : java.lang.Boolean {
      return perm.TroubleTicket.create
    }
    
    override property get CurrentLocation () : pcf.ProducerDetailTroubleTickets {
      return super.CurrentLocation as pcf.ProducerDetailTroubleTickets
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    
  }
  
  
}