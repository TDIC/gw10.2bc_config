package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'action' attribute on ForwardCondition at AccountForward.pcf: line 18, column 122
    function action_0 () : void {
      gw.api.web.account.AccountUtil.getMostRecentlyViewedAccountInfo().goToDestination()
    }
    
    // 'action' attribute on ForwardCondition at AccountForward.pcf: line 20, column 36
    function action_2 () : void {
      AccountsGroup.go()
    }
    
    // 'action' attribute on ForwardCondition at AccountForward.pcf: line 20, column 36
    function action_dest_3 () : pcf.api.Destination {
      return pcf.AccountsGroup.createDestination()
    }
    
    // 'canVisit' attribute on Forward (id=AccountForward) at AccountForward.pcf: line 7, column 25
    static function canVisit_4 () : java.lang.Boolean {
      return perm.System.accttabview
    }
    
    // 'condition' attribute on ForwardCondition at AccountForward.pcf: line 18, column 122
    function condition_1 () : java.lang.Boolean {
      return gotoMostRecentAccount and (gw.api.web.account.AccountUtil.getMostRecentlyViewedAccountInfo() != null)
    }
    
    override property get CurrentLocation () : pcf.AccountForward {
      return super.CurrentLocation as pcf.AccountForward
    }
    
    property get gotoMostRecentAccount () : Boolean {
      return getVariableValue("gotoMostRecentAccount", 0) as Boolean
    }
    
    property set gotoMostRecentAccount ($arg :  Boolean) {
      setVariableValue("gotoMostRecentAccount", 0, $arg)
    }
    
    
  }
  
  
}