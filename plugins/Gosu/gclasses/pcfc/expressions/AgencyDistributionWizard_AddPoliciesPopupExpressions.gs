package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.search.InvoiceItemQueryHelper.DistributionTypeEnum
uses gw.search.PolicyPeriodSearchCriteria.SearchContextEnum
uses gw.search.PolicyPeriodSearchCriteria
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_AddPoliciesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyDistributionWizard_AddPoliciesPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_AddPoliciesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyDistributionWizard_AddPoliciesPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (wizardState :  gw.agencybill.AgencyDistributionWizardHelper) : int {
      return 0
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 17, column 38
    function initialValue_0 () : AgencyCycleDistPrefill {
      return AgencyCycleDistPrefill.TC_UNPAID
    }
    
    override property get CurrentLocation () : pcf.AgencyDistributionWizard_AddPoliciesPopup {
      return super.CurrentLocation as pcf.AgencyDistributionWizard_AddPoliciesPopup
    }
    
    property get prefill () : AgencyCycleDistPrefill {
      return getVariableValue("prefill", 0) as AgencyCycleDistPrefill
    }
    
    property set prefill ($arg :  AgencyCycleDistPrefill) {
      setVariableValue("prefill", 0, $arg)
    }
    
    property get wizardState () : gw.agencybill.AgencyDistributionWizardHelper {
      return getVariableValue("wizardState", 0) as gw.agencybill.AgencyDistributionWizardHelper
    }
    
    property set wizardState ($arg :  gw.agencybill.AgencyDistributionWizardHelper) {
      setVariableValue("wizardState", 0, $arg)
    }
    
    
    function createSearchCriteria() : gw.search.PolicyPeriodSearchCriteria {
      var distributionType = wizardState.IsPromise ?
        DistributionTypeEnum.Promise :
        DistributionTypeEnum.Payment
      var searchContext = wizardState.AgencyCycleDistView != null ? 
        SearchContextEnum.AnyPolicy : //DistributionScreen
        SearchContextEnum.PoliciesOnGivenStatements //MoneyDetails Screen
        
      var searchCriteria = new gw.search.PolicyPeriodSearchCriteria(distributionType, searchContext, wizardState.MoneySetup.Producer)
      if (wizardState.AgencyCycleDistView != null) { //DistributionScreen
        searchCriteria.InvoiceItemsToExclude = wizardState.AgencyCycleDistView.AgencyCycleDist.DistItems.map(\ di -> di.InvoiceItem)
      } else { //MoneyDetails Screen
        searchCriteria.StatementInvoices = wizardState.MoneySetup.DistributeToStatements
      }
      return searchCriteria
    }
    
    function doSearch(searchCriteria : gw.search.PolicyPeriodSearchCriteria) : gw.api.database.IQueryBeanResult<PolicyPeriod>  {
      return gw.search.SearchMethods.validateAndSearch(searchCriteria) 
    }
    
    function addItemsFromSelectedPolicies(policyPeriods : PolicyPeriod[], filterByStatusIsBilledOrDue : boolean, policyPeriodSearchCriteria : PolicyPeriodSearchCriteria) {
      var searchCriteria = new gw.search.InvoiceItemSearchCriteria()
      searchCriteria.IncludeOnlyBilledAndDueItems = filterByStatusIsBilledOrDue
      searchCriteria.PayingProducer = wizardState.MoneySetup.Producer
      if (policyPeriodSearchCriteria.PayerTypeIsAccount && policyPeriodSearchCriteria.PayerAccount != null) {
        searchCriteria.PayerIsAnAccount = true
        searchCriteria.PayerAccountNumber = policyPeriodSearchCriteria.PayerAccount.AccountNumber
      } else if (policyPeriodSearchCriteria.PayerTypeIsProducer) {
        searchCriteria.PayerIsAnAccount = false
        searchCriteria.PayerProducerName = wizardState.MoneySetup.Producer.Name
      }
      searchCriteria.DistributionTypeIsPromise = wizardState.IsPromise
      searchCriteria.restrictToInvoiceItemsOnPolicyPeriods(policyPeriods)
      var invoiceItems = searchCriteria.performSearch(wizardState.AgencyCycleDistView.AgencyCycleDist).map(\ ii -> ii)
      wizardState.AgencyCycleDistView.addInvoiceItems(invoiceItems?.toTypedArray(), prefill)
      CurrentLocation.commit()
    }
    
    function getPolicyDelinquencyStatusDescription(policyPeriod : PolicyPeriod) : String {
      return policyPeriod.Delinquent ? DisplayKey.get("Web.AccountPoliciesLV.PastDue") : DisplayKey.get("Web.AccountPoliciesLV.GoodStanding")
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_AddPoliciesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DetailViewPanelExpressionsImpl extends SpecificPoliciesPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerInput (id=PayerAccount_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 109, column 62
    function action_27 () : void {
      AccountSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=OwnerAccount_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 124, column 43
    function action_41 () : void {
      AccountSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=PayerAccount_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 109, column 62
    function action_dest_28 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'pickLocation' attribute on PickerInput (id=OwnerAccount_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 124, column 43
    function action_dest_42 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'value' attribute on RangeRadioInput (id=PayerType_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 95, column 65
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.PayerType = (__VALUE_TO_SET as gw.search.PolicyPeriodSearchCriteria.PayerTypeEnum)
    }
    
    // 'value' attribute on PickerInput (id=PayerAccount_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 109, column 62
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.PayerAccount = (__VALUE_TO_SET as entity.Account)
    }
    
    // 'value' attribute on PickerInput (id=OwnerAccount_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 124, column 43
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.OwnerAccount = (__VALUE_TO_SET as entity.Account)
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 131, column 52
    function defaultSetter_49 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Product_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 137, column 44
    function defaultSetter_53 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.Product = (__VALUE_TO_SET as typekey.LOBCode)
    }
    
    // 'initialValue' attribute on Variable at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 85, column 63
    function initialValue_17 () : gw.api.web.account.AccountSearchConverter {
      return new gw.api.web.account.AccountSearchConverter()
    }
    
    // 'inputConversion' attribute on PickerInput (id=PayerAccount_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 109, column 62
    function inputConversion_30 (VALUE :  java.lang.String) : java.lang.Object {
      return accountSearchConverter.getAccount(VALUE)
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=PayerType_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 95, column 65
    function valueRange_22 () : java.lang.Object {
      return gw.search.PolicyPeriodSearchCriteria.PayerTypeEnum.AllValues
    }
    
    // 'value' attribute on RangeRadioInput (id=PayerType_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 95, column 65
    function valueRoot_21 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on RangeRadioInput (id=PayerType_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 95, column 65
    function value_19 () : gw.search.PolicyPeriodSearchCriteria.PayerTypeEnum {
      return searchCriteria.PayerType
    }
    
    // 'value' attribute on PickerInput (id=PayerAccount_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 109, column 62
    function value_31 () : entity.Account {
      return searchCriteria.PayerAccount
    }
    
    // 'value' attribute on TextInput (id=PayerProducer_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 115, column 63
    function value_37 () : entity.Producer {
      return searchCriteria.PayerProducer
    }
    
    // 'value' attribute on PickerInput (id=OwnerAccount_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 124, column 43
    function value_44 () : entity.Account {
      return searchCriteria.OwnerAccount
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 131, column 52
    function value_48 () : java.lang.String {
      return searchCriteria.PolicyNumber
    }
    
    // 'value' attribute on TypeKeyInput (id=Product_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 137, column 44
    function value_52 () : typekey.LOBCode {
      return searchCriteria.Product
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=PayerType_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 95, column 65
    function verifyValueRangeIsAllowedType_23 ($$arg :  gw.search.PolicyPeriodSearchCriteria.PayerTypeEnum[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=PayerType_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 95, column 65
    function verifyValueRangeIsAllowedType_23 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeRadioInput (id=PayerType_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 95, column 65
    function verifyValueRange_24 () : void {
      var __valueRangeArg = gw.search.PolicyPeriodSearchCriteria.PayerTypeEnum.AllValues
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_23(__valueRangeArg)
    }
    
    // 'visible' attribute on RangeRadioInput (id=PayerType_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 95, column 65
    function visible_18 () : java.lang.Boolean {
      return wizardState.AgencyCycleDistView != null
    }
    
    // 'visible' attribute on PickerInput (id=PayerAccount_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 109, column 62
    function visible_29 () : java.lang.Boolean {
      return searchCriteria.PayerTypeIsAccount
    }
    
    // 'visible' attribute on TextInput (id=PayerProducer_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 115, column 63
    function visible_36 () : java.lang.Boolean {
      return searchCriteria.PayerTypeIsProducer
    }
    
    property get accountSearchConverter () : gw.api.web.account.AccountSearchConverter {
      return getVariableValue("accountSearchConverter", 2) as gw.api.web.account.AccountSearchConverter
    }
    
    property set accountSearchConverter ($arg :  gw.api.web.account.AccountSearchConverter) {
      setVariableValue("accountSearchConverter", 2, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_AddPoliciesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends SpecificPoliciesPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 2)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 183, column 52
    function action_64 () : void {
      PolicySummary.push(policy)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 188, column 64
    function action_69 () : void {
      AccountSummaryPopup.push(policy.Policy.Account)
    }
    
    // 'action' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 183, column 52
    function action_dest_65 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(policy)
    }
    
    // 'action' attribute on TextCell (id=AccountNumber_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 188, column 64
    function action_dest_70 () : pcf.api.Destination {
      return pcf.AccountSummaryPopup.createDestination(policy.Policy.Account)
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 183, column 52
    function valueRoot_67 () : java.lang.Object {
      return policy
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 188, column 64
    function valueRoot_72 () : java.lang.Object {
      return policy.Policy.Account
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 193, column 48
    function valueRoot_75 () : java.lang.Object {
      return policy.Policy
    }
    
    // 'value' attribute on TextCell (id=Producer_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 212, column 48
    function valueRoot_86 () : java.lang.Object {
      return policy.PrimaryPolicyCommission.ProducerCode
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 183, column 52
    function value_66 () : java.lang.String {
      return policy.PolicyNumberLong
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 188, column 64
    function value_71 () : java.lang.String {
      return policy.Policy.Account.AccountNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 193, column 48
    function value_74 () : typekey.LOBCode {
      return policy.Policy.LOBCode
    }
    
    // 'value' attribute on TextCell (id=PaymentStatus_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 197, column 74
    function value_77 () : java.lang.String {
      return getPolicyDelinquencyStatusDescription(policy)
    }
    
    // 'value' attribute on DateCell (id=EffectiveDate_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 202, column 49
    function value_79 () : java.util.Date {
      return policy.EffectiveDate
    }
    
    // 'value' attribute on DateCell (id=ExpirationDate_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 207, column 50
    function value_82 () : java.util.Date {
      return policy.ExpirationDate
    }
    
    // 'value' attribute on TextCell (id=Producer_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 212, column 48
    function value_85 () : entity.Producer {
      return policy.PrimaryPolicyCommission.ProducerCode.Producer
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 216, column 77
    function value_88 () : java.lang.String {
      return policy.PrimaryPolicyCommission.ProducerCode.Code
    }
    
    property get policy () : entity.PolicyPeriod {
      return getIteratedValue(2) as entity.PolicyPeriod
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistributionWizard_AddPoliciesPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SpecificPoliciesPanelSetExpressionsImpl extends AgencyDistributionWizard_AddPoliciesPopupExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=Search) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 154, column 43
    function action_56 () : void {
      gw.api.util.SearchUtil.search()
    }
    
    // 'action' attribute on Link (id=Reset) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 160, column 43
    function action_57 () : void {
      gw.api.util.SearchUtil.reset()
    }
    
    // 'action' attribute on ToolbarButton (id=cancel) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 56, column 66
    function action_7 () : void {
      CurrentLocation.cancel()
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=AllPoliciesWithAllItems) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 45, column 115
    function allCheckedRowsAction_4 (CheckedValues :  entity.PolicyPeriod[], CheckedValuesErrors :  java.util.Map) : void {
      addItemsFromSelectedPolicies(CheckedValues, false, searchCriteria); 
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=AddPoliciesWithBilledAndDueItems) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 52, column 115
    function allCheckedRowsAction_6 (CheckedValues :  entity.PolicyPeriod[], CheckedValuesErrors :  java.util.Map) : void {
      addItemsFromSelectedPolicies(CheckedValues, true, searchCriteria)
    }
    
    // 'value' attribute on RangeInput (id=Prefill_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 69, column 117
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      prefill = (__VALUE_TO_SET as typekey.AgencyCycleDistPrefill)
    }
    
    // 'pickValue' attribute on CheckedValuesToolbarButton (id=SelectPolicies) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 38, column 66
    function pickValue_2 (CheckedValues :  entity.PolicyPeriod[]) : PolicyPeriod[] {
      return CheckedValues
    }
    
    // 'searchCriteria' attribute on SearchPanel (id=SpecificPoliciesPanelSet) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 29, column 82
    function searchCriteria_93 () : gw.search.PolicyPeriodSearchCriteria {
      return createSearchCriteria()
    }
    
    // 'search' attribute on SearchPanel (id=SpecificPoliciesPanelSet) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 29, column 82
    function search_92 () : java.lang.Object {
      return doSearch(searchCriteria)
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 183, column 52
    function sortValue_58 (policy :  entity.PolicyPeriod) : java.lang.Object {
      return policy.PolicyNumberLong
    }
    
    // 'value' attribute on TextCell (id=AccountNumber_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 188, column 64
    function sortValue_59 (policy :  entity.PolicyPeriod) : java.lang.Object {
      return policy.Policy.Account.AccountNumber
    }
    
    // 'value' attribute on TypeKeyCell (id=Product_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 193, column 48
    function sortValue_60 (policy :  entity.PolicyPeriod) : java.lang.Object {
      return policy.Policy.LOBCode
    }
    
    // 'value' attribute on TextCell (id=PaymentStatus_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 197, column 74
    function sortValue_61 (policy :  entity.PolicyPeriod) : java.lang.Object {
      return getPolicyDelinquencyStatusDescription(policy)
    }
    
    // 'value' attribute on TextCell (id=Producer_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 212, column 48
    function sortValue_62 (policy :  entity.PolicyPeriod) : java.lang.Object {
      return policy.PrimaryPolicyCommission.ProducerCode.Producer
    }
    
    // 'value' attribute on TextCell (id=ProducerCode_Cell) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 216, column 77
    function sortValue_63 (policy :  entity.PolicyPeriod) : java.lang.Object {
      return policy.PrimaryPolicyCommission.ProducerCode.Code
    }
    
    // 'valueRange' attribute on RangeInput (id=Prefill_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 69, column 117
    function valueRange_11 () : java.lang.Object {
      return new AgencyCycleDistPrefill[]{AgencyCycleDistPrefill.TC_UNPAID, AgencyCycleDistPrefill.TC_ZERO}
    }
    
    // 'value' attribute on RangeInput (id=Prefill_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 69, column 117
    function value_9 () : typekey.AgencyCycleDistPrefill {
      return prefill
    }
    
    // 'value' attribute on RowIterator (id=PoliciesIterator) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 174, column 87
    function value_91 () : gw.api.database.IQueryBeanResult<entity.PolicyPeriod> {
      return foundPolicies
    }
    
    // 'valueRange' attribute on RangeInput (id=Prefill_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 69, column 117
    function verifyValueRangeIsAllowedType_12 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Prefill_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 69, column 117
    function verifyValueRangeIsAllowedType_12 ($$arg :  typekey.AgencyCycleDistPrefill[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Prefill_Input) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 69, column 117
    function verifyValueRange_13 () : void {
      var __valueRangeArg = new AgencyCycleDistPrefill[]{AgencyCycleDistPrefill.TC_UNPAID, AgencyCycleDistPrefill.TC_ZERO}
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_12(__valueRangeArg)
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=SelectPolicies) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 38, column 66
    function visible_1 () : java.lang.Boolean {
      return wizardState.AgencyCycleDistView == null
    }
    
    // 'visible' attribute on CheckedValuesToolbarButton (id=AllPoliciesWithAllItems) at AgencyDistributionWizard_AddPoliciesPopup.pcf: line 45, column 115
    function visible_3 () : java.lang.Boolean {
      return wizardState.AgencyCycleDistView != null //only visible if coming from DistributionScreen
    }
    
    property get foundPolicies () : gw.api.database.IQueryBeanResult<PolicyPeriod> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<PolicyPeriod>
    }
    
    property get searchCriteria () : gw.search.PolicyPeriodSearchCriteria {
      return getCriteriaValue(1) as gw.search.PolicyPeriodSearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.PolicyPeriodSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    
  }
  
  
}