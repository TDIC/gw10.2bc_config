package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadSecZoneLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadSecZoneLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadSecZoneLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadSecZoneLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadSecZoneLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=Name_Cell) at AdminDataUploadSecZoneLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.SecurityZone.Name")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadSecZoneLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.SecurityZone.PublicID")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at AdminDataUploadSecZoneLV.pcf: line 40, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.SecurityZone.Description")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadSecZoneLV.pcf: line 45, column 42
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadSecZoneLV.pcf: line 23, column 46
    function sortValue_1 (securityZone :  tdic.util.dataloader.data.admindata.SecurityZoneData) : java.lang.Object {
      return processor.getLoadStatus(securityZone)
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadSecZoneLV.pcf: line 45, column 42
    function sortValue_10 (securityZone :  tdic.util.dataloader.data.admindata.SecurityZoneData) : java.lang.Object {
      return securityZone.Exclude
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AdminDataUploadSecZoneLV.pcf: line 29, column 41
    function sortValue_4 (securityZone :  tdic.util.dataloader.data.admindata.SecurityZoneData) : java.lang.Object {
      return securityZone.Name
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadSecZoneLV.pcf: line 35, column 41
    function sortValue_6 (securityZone :  tdic.util.dataloader.data.admindata.SecurityZoneData) : java.lang.Object {
      return securityZone.PublicID
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at AdminDataUploadSecZoneLV.pcf: line 40, column 41
    function sortValue_8 (securityZone :  tdic.util.dataloader.data.admindata.SecurityZoneData) : java.lang.Object {
      return securityZone.Description
    }
    
    // 'value' attribute on RowIterator (id=securityZone) at AdminDataUploadSecZoneLV.pcf: line 15, column 99
    function value_37 () : java.util.ArrayList<tdic.util.dataloader.data.admindata.SecurityZoneData> {
      return processor.SecurityZoneArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadSecZoneLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadSecZoneLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminDataUploadSecZoneLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at AdminDataUploadSecZoneLV.pcf: line 17, column 104
    function highlighted_36 () : java.lang.Boolean {
      return (securityZone.Error or securityZone.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadSecZoneLV.pcf: line 23, column 46
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=Name_Cell) at AdminDataUploadSecZoneLV.pcf: line 29, column 41
    function label_16 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.SecurityZone.Name")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadSecZoneLV.pcf: line 35, column 41
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.SecurityZone.PublicID")
    }
    
    // 'label' attribute on TextCell (id=description_Cell) at AdminDataUploadSecZoneLV.pcf: line 40, column 41
    function label_26 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.SecurityZone.Description")
    }
    
    // 'label' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadSecZoneLV.pcf: line 45, column 42
    function label_31 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AdminDataUploadSecZoneLV.pcf: line 29, column 41
    function valueRoot_18 () : java.lang.Object {
      return securityZone
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadSecZoneLV.pcf: line 23, column 46
    function value_12 () : java.lang.String {
      return processor.getLoadStatus(securityZone)
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at AdminDataUploadSecZoneLV.pcf: line 29, column 41
    function value_17 () : java.lang.String {
      return securityZone.Name
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadSecZoneLV.pcf: line 35, column 41
    function value_22 () : java.lang.String {
      return securityZone.PublicID
    }
    
    // 'value' attribute on TextCell (id=description_Cell) at AdminDataUploadSecZoneLV.pcf: line 40, column 41
    function value_27 () : java.lang.String {
      return securityZone.Description
    }
    
    // 'value' attribute on BooleanRadioCell (id=exclude_Cell) at AdminDataUploadSecZoneLV.pcf: line 45, column 42
    function value_32 () : java.lang.Boolean {
      return securityZone.Exclude
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadSecZoneLV.pcf: line 23, column 46
    function visible_13 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get securityZone () : tdic.util.dataloader.data.admindata.SecurityZoneData {
      return getIteratedValue(1) as tdic.util.dataloader.data.admindata.SecurityZoneData
    }
    
    
  }
  
  
}