package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/InvoiceStreamAnchorDateInputSet.everyotherweek.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceStreamAnchorDateInputSet_everyotherweekExpressions {
  @javax.annotation.Generated("config/web/pcf/account/InvoiceStreamAnchorDateInputSet.everyotherweek.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceStreamAnchorDateInputSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on DateInput (id=OverridingEveryOtherWeekInvoiceAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.everyotherweek.pcf: line 33, column 60
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceDayChangeHelper.OverridingAnchorDateViews[0].Date = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverrideInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.everyotherweek.pcf: line 24, column 44
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceDayChangeHelper.OverrideAnchorDates = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'editable' attribute on DateInput (id=OverridingEveryOtherWeekInvoiceAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.everyotherweek.pcf: line 33, column 60
    function editable_10 () : java.lang.Boolean {
      return isEditMode && invoiceDayChangeHelper.OverrideAnchorDates
    }
    
    // 'editable' attribute on BooleanRadioInput (id=OverrideInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.everyotherweek.pcf: line 24, column 44
    function editable_2 () : java.lang.Boolean {
      return isEditMode
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverrideInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.everyotherweek.pcf: line 24, column 44
    function valueRoot_6 () : java.lang.Object {
      return invoiceDayChangeHelper
    }
    
    // 'value' attribute on DateInput (id=AccountEveryOtherWeekInvoiceAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.everyotherweek.pcf: line 17, column 75
    function value_0 () : java.util.Date {
      return invoiceDayChangeHelper.PayerDefaultAnchorDateViews[0].Date
    }
    
    // 'value' attribute on DateInput (id=OverridingEveryOtherWeekInvoiceAnchorDate_Input) at InvoiceStreamAnchorDateInputSet.everyotherweek.pcf: line 33, column 60
    function value_12 () : java.util.Date {
      return invoiceDayChangeHelper.OverridingAnchorDateViews[0].Date
    }
    
    // 'value' attribute on BooleanRadioInput (id=OverrideInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.everyotherweek.pcf: line 24, column 44
    function value_4 () : java.lang.Boolean {
      return invoiceDayChangeHelper.OverrideAnchorDates
    }
    
    // 'visible' attribute on BooleanRadioInput (id=OverrideInvoiceDayOfMonth_Input) at InvoiceStreamAnchorDateInputSet.everyotherweek.pcf: line 24, column 44
    function visible_3 () : java.lang.Boolean {
      return CurrentLocation.InEditMode
    }
    
    property get invoiceDayChangeHelper () : gw.api.web.invoice.InvoiceDayChangeHelper {
      return getRequireValue("invoiceDayChangeHelper", 0) as gw.api.web.invoice.InvoiceDayChangeHelper
    }
    
    property set invoiceDayChangeHelper ($arg :  gw.api.web.invoice.InvoiceDayChangeHelper) {
      setRequireValue("invoiceDayChangeHelper", 0, $arg)
    }
    
    property get isEditMode () : boolean {
      return getRequireValue("isEditMode", 0) as java.lang.Boolean
    }
    
    property set isEditMode ($arg :  boolean) {
      setRequireValue("isEditMode", 0, $arg)
    }
    
    
  }
  
  
}