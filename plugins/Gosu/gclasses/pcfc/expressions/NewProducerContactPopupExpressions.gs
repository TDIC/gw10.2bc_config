package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewProducerContactPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewProducerContactPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/NewProducerContactPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewProducerContactPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer, contactSubtype :  Type<Contact>) : int {
      return 0
    }
    
    // 'canVisit' attribute on Popup (id=NewProducerContactPopup) at NewProducerContactPopup.pcf: line 11, column 70
    static function canVisit_5 (contactSubtype :  Type<Contact>, producer :  Producer) : java.lang.Boolean {
      return perm.ProducerContact.create
    }
    
    // 'def' attribute on PanelRef at NewProducerContactPopup.pcf: line 31, column 55
    function def_onEnter_3 (def :  pcf.ProducerContactDetailDV) : void {
      def.onEnter(contact, true)
    }
    
    // 'def' attribute on PanelRef at NewProducerContactPopup.pcf: line 31, column 55
    function def_refreshVariables_4 (def :  pcf.ProducerContactDetailDV) : void {
      def.refreshVariables(contact, true)
    }
    
    // 'initialValue' attribute on Variable at NewProducerContactPopup.pcf: line 20, column 31
    function initialValue_0 () : ProducerContact {
      return initNewProducerContact()
    }
    
    // EditButtons at NewProducerContactPopup.pcf: line 28, column 32
    function label_2 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'pickValue' attribute on EditButtons at NewProducerContactPopup.pcf: line 28, column 32
    function pickValue_1 () : ProducerContact {
      return contact
    }
    
    override property get CurrentLocation () : pcf.NewProducerContactPopup {
      return super.CurrentLocation as pcf.NewProducerContactPopup
    }
    
    property get contact () : ProducerContact {
      return getVariableValue("contact", 0) as ProducerContact
    }
    
    property set contact ($arg :  ProducerContact) {
      setVariableValue("contact", 0, $arg)
    }
    
    property get contactSubtype () : Type<Contact> {
      return getVariableValue("contactSubtype", 0) as Type<Contact>
    }
    
    property set contactSubtype ($arg :  Type<Contact>) {
      setVariableValue("contactSubtype", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    function initNewProducerContact() : ProducerContact {
      var newProducerContact = new ProducerContact();
      var newContact = instantiateContactSubtype()
      newProducerContact.Contact = newContact
      producer.addToContacts(newProducerContact);
      return newProducerContact;
    }
    
    private function instantiateContactSubtype() : Contact {
      // Instantiate the appropriate contact subtype (e.g. a Person or a Company)
      return contactSubtype.TypeInfo.getConstructor(null).Constructor.newInstance(null) as Contact  
    }
    
    
  }
  
  
}