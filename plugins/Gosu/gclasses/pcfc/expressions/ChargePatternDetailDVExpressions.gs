package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/chargepatterns/ChargePatternDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ChargePatternDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/chargepatterns/ChargePatternDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ChargePatternDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on TypeKeyInput (id=Category_Input) at ChargePatternDetailDV.pcf: line 36, column 45
    function available_13 () : java.lang.Boolean {
      return chargePattern.Subtype != TC_RECAPTURECHARGE
    }
    
    // 'available' attribute on RangeInput (id=TAccountOwner_Input) at ChargePatternDetailDV.pcf: line 44, column 49
    function available_22 () : java.lang.Boolean {
      return chargePattern.Subtype != TC_PRORATACHARGE && chargePattern.Subtype != TC_RECAPTURECHARGE
    }
    
    // 'value' attribute on TypeKeyInput (id=Category_Input) at ChargePatternDetailDV.pcf: line 36, column 45
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargePattern.Category = (__VALUE_TO_SET as typekey.ChargeCategory)
    }
    
    // 'value' attribute on TextInput (id=ChargeCode_Input) at ChargePatternDetailDV.pcf: line 17, column 43
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargePattern.ChargeCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=TAccountOwner_Input) at ChargePatternDetailDV.pcf: line 44, column 49
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargePattern.TAccountOwnerPattern = (__VALUE_TO_SET as entity.TAccountOwnerPattern)
    }
    
    // 'value' attribute on RangeInput (id=InvoiceTreatment_Input) at ChargePatternDetailDV.pcf: line 54, column 47
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargePattern.InvoiceTreatment = (__VALUE_TO_SET as typekey.InvoiceTreatment)
    }
    
    // 'value' attribute on TypeKeyInput (id=Periodicity_Input) at ChargePatternDetailDV.pcf: line 62, column 61
    function defaultSetter_43 (__VALUE_TO_SET :  java.lang.Object) : void {
      (chargePattern as ProRataCharge).Periodicity = (__VALUE_TO_SET as typekey.Periodicity)
    }
    
    // 'value' attribute on TypeKeyInput (id=Priority_Input) at ChargePatternDetailDV.pcf: line 68, column 45
    function defaultSetter_48 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargePattern.Priority = (__VALUE_TO_SET as typekey.ChargePriority)
    }
    
    // 'value' attribute on TypeKeyInput (id=PrintPriority_Input) at ChargePatternDetailDV.pcf: line 74, column 55
    function defaultSetter_52 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargePattern.PrintPriority_TDIC = (__VALUE_TO_SET as typekey.ChargePrintPriority_TDIC)
    }
    
    // 'value' attribute on BooleanRadioInput (id=IncludedInEquityDating_Input) at ChargePatternDetailDV.pcf: line 79, column 55
    function defaultSetter_56 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargePattern.IncludedInEquityDating = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=ChargeName_Input) at ChargePatternDetailDV.pcf: line 23, column 43
    function defaultSetter_7 (__VALUE_TO_SET :  java.lang.Object) : void {
      chargePattern.ChargeName = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'editable' attribute on TextInput (id=ChargeCode_Input) at ChargePatternDetailDV.pcf: line 17, column 43
    function editable_0 () : java.lang.Boolean {
      return !chargePattern.InternalCharge
    }
    
    // 'filter' attribute on TypeKeyInput (id=Category_Input) at ChargePatternDetailDV.pcf: line 36, column 45
    function filter_18 (VALUE :  typekey.ChargeCategory, VALUES :  typekey.ChargeCategory[]) : java.util.List<typekey.ChargeCategory> {
      return filterChargeCategories( VALUES )
    }
    
    // 'valueRange' attribute on RangeInput (id=TAccountOwner_Input) at ChargePatternDetailDV.pcf: line 44, column 49
    function valueRange_27 () : java.lang.Object {
      return chargePattern.getValidTAccountOwners()
    }
    
    // 'valueRange' attribute on RangeInput (id=InvoiceTreatment_Input) at ChargePatternDetailDV.pcf: line 54, column 47
    function valueRange_36 () : java.lang.Object {
      return gw.api.web.accounting.ChargePatternHelper.getAvailableInvoiceTreatments(chargePattern)
    }
    
    // 'value' attribute on TextInput (id=ChargeCode_Input) at ChargePatternDetailDV.pcf: line 17, column 43
    function valueRoot_3 () : java.lang.Object {
      return chargePattern
    }
    
    // 'value' attribute on TypeKeyInput (id=Periodicity_Input) at ChargePatternDetailDV.pcf: line 62, column 61
    function valueRoot_44 () : java.lang.Object {
      return (chargePattern as ProRataCharge)
    }
    
    // 'value' attribute on TextInput (id=ChargeCode_Input) at ChargePatternDetailDV.pcf: line 17, column 43
    function value_1 () : java.lang.String {
      return chargePattern.ChargeCode
    }
    
    // 'value' attribute on TypeKeyInput (id=Subtype_Input) at ChargePatternDetailDV.pcf: line 28, column 44
    function value_10 () : typekey.ChargePattern {
      return chargePattern.Subtype
    }
    
    // 'value' attribute on TypeKeyInput (id=Category_Input) at ChargePatternDetailDV.pcf: line 36, column 45
    function value_15 () : typekey.ChargeCategory {
      return chargePattern.Category
    }
    
    // 'value' attribute on RangeInput (id=TAccountOwner_Input) at ChargePatternDetailDV.pcf: line 44, column 49
    function value_24 () : entity.TAccountOwnerPattern {
      return chargePattern.TAccountOwnerPattern
    }
    
    // 'value' attribute on RangeInput (id=InvoiceTreatment_Input) at ChargePatternDetailDV.pcf: line 54, column 47
    function value_33 () : typekey.InvoiceTreatment {
      return chargePattern.InvoiceTreatment
    }
    
    // 'value' attribute on TypeKeyInput (id=Periodicity_Input) at ChargePatternDetailDV.pcf: line 62, column 61
    function value_42 () : typekey.Periodicity {
      return (chargePattern as ProRataCharge).Periodicity
    }
    
    // 'value' attribute on TypeKeyInput (id=Priority_Input) at ChargePatternDetailDV.pcf: line 68, column 45
    function value_47 () : typekey.ChargePriority {
      return chargePattern.Priority
    }
    
    // 'value' attribute on TypeKeyInput (id=PrintPriority_Input) at ChargePatternDetailDV.pcf: line 74, column 55
    function value_51 () : typekey.ChargePrintPriority_TDIC {
      return chargePattern.PrintPriority_TDIC
    }
    
    // 'value' attribute on BooleanRadioInput (id=IncludedInEquityDating_Input) at ChargePatternDetailDV.pcf: line 79, column 55
    function value_55 () : java.lang.Boolean {
      return chargePattern.IncludedInEquityDating
    }
    
    // 'value' attribute on TextInput (id=ChargeName_Input) at ChargePatternDetailDV.pcf: line 23, column 43
    function value_6 () : java.lang.String {
      return chargePattern.ChargeName
    }
    
    // 'valueRange' attribute on RangeInput (id=TAccountOwner_Input) at ChargePatternDetailDV.pcf: line 44, column 49
    function verifyValueRangeIsAllowedType_28 ($$arg :  entity.TAccountOwnerPattern[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=TAccountOwner_Input) at ChargePatternDetailDV.pcf: line 44, column 49
    function verifyValueRangeIsAllowedType_28 ($$arg :  gw.api.database.IQueryBeanResult<entity.TAccountOwnerPattern>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=TAccountOwner_Input) at ChargePatternDetailDV.pcf: line 44, column 49
    function verifyValueRangeIsAllowedType_28 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=InvoiceTreatment_Input) at ChargePatternDetailDV.pcf: line 54, column 47
    function verifyValueRangeIsAllowedType_37 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=InvoiceTreatment_Input) at ChargePatternDetailDV.pcf: line 54, column 47
    function verifyValueRangeIsAllowedType_37 ($$arg :  typekey.InvoiceTreatment[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=TAccountOwner_Input) at ChargePatternDetailDV.pcf: line 44, column 49
    function verifyValueRange_29 () : void {
      var __valueRangeArg = chargePattern.getValidTAccountOwners()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_28(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=InvoiceTreatment_Input) at ChargePatternDetailDV.pcf: line 54, column 47
    function verifyValueRange_38 () : void {
      var __valueRangeArg = gw.api.web.accounting.ChargePatternHelper.getAvailableInvoiceTreatments(chargePattern)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_37(__valueRangeArg)
    }
    
    // 'visible' attribute on TypeKeyInput (id=Periodicity_Input) at ChargePatternDetailDV.pcf: line 62, column 61
    function visible_40 () : java.lang.Boolean {
      return chargePattern.Subtype ==TC_PRORATACHARGE
    }
    
    property get chargePattern () : ChargePattern {
      return getRequireValue("chargePattern", 0) as ChargePattern
    }
    
    property set chargePattern ($arg :  ChargePattern) {
      setRequireValue("chargePattern", 0, $arg)
    }
    
    
    function filterChargeCategories(categories : ChargeCategory[]) : java.util.List<ChargeCategory> {
      if (chargePattern.Subtype == TC_RECAPTURECHARGE) {
        return categories.where( \ c -> c == ChargeCategory.TC_RECAPTURE).toList()
      } else {
        return categories.where( \ c -> c != ChargeCategory.TC_RECAPTURE).toList()
      }
    }
          
        
    
    
  }
  
  
}