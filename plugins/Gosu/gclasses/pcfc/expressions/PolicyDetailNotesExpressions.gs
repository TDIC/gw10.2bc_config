package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailNotes.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailNotesExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailNotes.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailNotesExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod, isClearBundle :  Boolean) : int {
      return 0
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod, isClearBundle :  Boolean, latestNote :  Note) : int {
      return 1
    }
    
    // 'canVisit' attribute on Page (id=PolicyDetailNotes) at PolicyDetailNotes.pcf: line 11, column 69
    static function canVisit_69 (isClearBundle :  Boolean, latestNote :  Note, policyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return perm.System.plcytabview and perm.System.plcynoteview and not policyPeriod.Archived
    }
    
    // 'parent' attribute on Page (id=PolicyDetailNotes) at PolicyDetailNotes.pcf: line 11, column 69
    static function parent_70 (isClearBundle :  Boolean, latestNote :  Note, policyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyGroup.createDestination(policyPeriod)
    }
    
    override property get CurrentLocation () : pcf.PolicyDetailNotes {
      return super.CurrentLocation as pcf.PolicyDetailNotes
    }
    
    property get isClearBundle () : Boolean {
      return getVariableValue("isClearBundle", 0) as Boolean
    }
    
    property set isClearBundle ($arg :  Boolean) {
      setVariableValue("isClearBundle", 0, $arg)
    }
    
    property get latestNote () : Note {
      return getVariableValue("latestNote", 0) as Note
    }
    
    property set latestNote ($arg :  Note) {
      setVariableValue("latestNote", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailNotes.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SearchPanelExpressionsImpl extends PolicyDetailNotesExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'available' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at PolicyDetailNotes.pcf: line 73, column 63
    function available_19 () : java.lang.Boolean {
      return NoteSearchCriteria.DateCriterionChoice.DateSearchType == DateSearchType.TC_FROMLIST
    }
    
    // 'available' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at PolicyDetailNotes.pcf: line 73, column 63
    function available_28 () : java.lang.Boolean {
      return NoteSearchCriteria.DateCriterionChoice.DateSearchType == DateSearchType.TC_ENTEREDRANGE
    }
    
    // 'cachingEnabled' attribute on SearchPanel at PolicyDetailNotes.pcf: line 36, column 74
    function cachingEnabled_65 () : java.lang.Boolean {
      return latestNote == null
    }
    
    // 'def' attribute on InputSetRef at PolicyDetailNotes.pcf: line 100, column 47
    function def_onEnter_61 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on PanelRef at PolicyDetailNotes.pcf: line 104, column 52
    function def_onEnter_63 (def :  pcf.NotesLV) : void {
      def.onEnter(notes,policyPeriod.Policy)
    }
    
    // 'def' attribute on InputSetRef at PolicyDetailNotes.pcf: line 100, column 47
    function def_refreshVariables_62 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'def' attribute on PanelRef at PolicyDetailNotes.pcf: line 104, column 52
    function def_refreshVariables_64 (def :  pcf.NotesLV) : void {
      def.refreshVariables(notes,policyPeriod.Policy)
    }
    
    // 'value' attribute on TextInput (id=TextSearch_Input) at PolicyDetailNotes.pcf: line 44, column 48
    function defaultSetter_1 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.Text = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=RelatedToSearch_Input) at PolicyDetailNotes.pcf: line 59, column 46
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.RelatedToEntity = (__VALUE_TO_SET as typekey.RelatedTo)
    }
    
    // 'value' attribute on TypeKeyInput (id=Topic_Input) at PolicyDetailNotes.pcf: line 66, column 50
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.Topic = (__VALUE_TO_SET as typekey.NoteTopicType)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at PolicyDetailNotes.pcf: line 73, column 63
    function defaultSetter_21 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.DateCriterionChoice.DateRangeChoice = (__VALUE_TO_SET as typekey.DateRangeChoiceType)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at PolicyDetailNotes.pcf: line 73, column 63
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.DateCriterionChoice.DateSearchType = (__VALUE_TO_SET as typekey.DateSearchType)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at PolicyDetailNotes.pcf: line 73, column 63
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.DateCriterionChoice.StartDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at PolicyDetailNotes.pcf: line 73, column 63
    function defaultSetter_36 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.DateCriterionChoice.EndDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at PolicyDetailNotes.pcf: line 73, column 63
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.DateCriterionChoice = (__VALUE_TO_SET as entity.DateCriterionChoice)
    }
    
    // 'value' attribute on TypeKeyInput (id=LanguageSearch_Input) at PolicyDetailNotes.pcf: line 80, column 73
    function defaultSetter_49 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.Language = (__VALUE_TO_SET as typekey.LanguageType)
    }
    
    // 'value' attribute on RangeInput (id=Author_Input) at PolicyDetailNotes.pcf: line 52, column 40
    function defaultSetter_5 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.Author = (__VALUE_TO_SET as entity.User)
    }
    
    // 'value' attribute on TypeKeyInput (id=SortByOption_Input) at PolicyDetailNotes.pcf: line 89, column 48
    function defaultSetter_54 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.SortBy = (__VALUE_TO_SET as typekey.SortByRange)
    }
    
    // 'value' attribute on BooleanRadioInput (id=SortAscending_Input) at PolicyDetailNotes.pcf: line 96, column 57
    function defaultSetter_58 (__VALUE_TO_SET :  java.lang.Object) : void {
      NoteSearchCriteria.SortAscending = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'searchCriteria' attribute on SearchPanel at PolicyDetailNotes.pcf: line 36, column 74
    function searchCriteria_67 () : entity.NoteSearchCriteria {
      return new NoteSearchCriteria() {:PolicyPeriod = policyPeriod, :DateCriterionChoice = (latestNote != null) ? new DateCriterionChoice() {:SearchType = TC_NOTE, :DateSearchType = TC_ENTEREDRANGE, :StartDate = latestNote.AuthoringDate.addWeeks(-1)} : null}
    }
    
    // 'searchOnEnter' attribute on SearchPanel at PolicyDetailNotes.pcf: line 36, column 74
    function searchOnEnter_68 () : java.lang.Boolean {
      return latestNote != null
    }
    
    // 'search' attribute on SearchPanel at PolicyDetailNotes.pcf: line 36, column 74
    function search_66 () : java.lang.Object {
      return NoteSearchCriteria.performSearch(isClearBundle)
    }
    
    // 'valueRange' attribute on RangeInput (id=Author_Input) at PolicyDetailNotes.pcf: line 52, column 40
    function valueRange_7 () : java.lang.Object {
      return policyPeriod.Policy.NoteAuthors
    }
    
    // 'value' attribute on TextInput (id=TextSearch_Input) at PolicyDetailNotes.pcf: line 44, column 48
    function valueRoot_2 () : java.lang.Object {
      return NoteSearchCriteria
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at PolicyDetailNotes.pcf: line 73, column 63
    function valueRoot_22 () : java.lang.Object {
      return NoteSearchCriteria.DateCriterionChoice
    }
    
    // 'value' attribute on TextInput (id=TextSearch_Input) at PolicyDetailNotes.pcf: line 44, column 48
    function value_0 () : java.lang.String {
      return NoteSearchCriteria.Text
    }
    
    // 'value' attribute on TypeKeyInput (id=RelatedToSearch_Input) at PolicyDetailNotes.pcf: line 59, column 46
    function value_11 () : typekey.RelatedTo {
      return NoteSearchCriteria.RelatedToEntity
    }
    
    // 'value' attribute on TypeKeyInput (id=Topic_Input) at PolicyDetailNotes.pcf: line 66, column 50
    function value_15 () : typekey.NoteTopicType {
      return NoteSearchCriteria.Topic
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at PolicyDetailNotes.pcf: line 73, column 63
    function value_20 () : typekey.DateRangeChoiceType {
      return NoteSearchCriteria.DateCriterionChoice.DateRangeChoice
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at PolicyDetailNotes.pcf: line 73, column 63
    function value_24 () : java.lang.Object {
      return NoteSearchCriteria.DateCriterionChoice.DateRangeChoice
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at PolicyDetailNotes.pcf: line 73, column 63
    function value_25 () : typekey.DateSearchType {
      return NoteSearchCriteria.DateCriterionChoice.DateSearchType
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at PolicyDetailNotes.pcf: line 73, column 63
    function value_29 () : java.util.Date {
      return NoteSearchCriteria.DateCriterionChoice.StartDate
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at PolicyDetailNotes.pcf: line 73, column 63
    function value_35 () : java.util.Date {
      return NoteSearchCriteria.DateCriterionChoice.EndDate
    }
    
    // 'value' attribute on RangeInput (id=Author_Input) at PolicyDetailNotes.pcf: line 52, column 40
    function value_4 () : entity.User {
      return NoteSearchCriteria.Author
    }
    
    // 'value' attribute on DateCriterionChoiceInput (id=DateSearch2_Input) at PolicyDetailNotes.pcf: line 73, column 63
    function value_43 () : entity.DateCriterionChoice {
      return NoteSearchCriteria.DateCriterionChoice
    }
    
    // 'value' attribute on TypeKeyInput (id=LanguageSearch_Input) at PolicyDetailNotes.pcf: line 80, column 73
    function value_48 () : typekey.LanguageType {
      return NoteSearchCriteria.Language
    }
    
    // 'value' attribute on TypeKeyInput (id=SortByOption_Input) at PolicyDetailNotes.pcf: line 89, column 48
    function value_53 () : typekey.SortByRange {
      return NoteSearchCriteria.SortBy
    }
    
    // 'value' attribute on BooleanRadioInput (id=SortAscending_Input) at PolicyDetailNotes.pcf: line 96, column 57
    function value_57 () : java.lang.Boolean {
      return NoteSearchCriteria.SortAscending
    }
    
    // 'valueRange' attribute on RangeInput (id=Author_Input) at PolicyDetailNotes.pcf: line 52, column 40
    function verifyValueRangeIsAllowedType_8 ($$arg :  entity.User[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Author_Input) at PolicyDetailNotes.pcf: line 52, column 40
    function verifyValueRangeIsAllowedType_8 ($$arg :  gw.api.database.IQueryBeanResult<entity.User>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Author_Input) at PolicyDetailNotes.pcf: line 52, column 40
    function verifyValueRangeIsAllowedType_8 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=Author_Input) at PolicyDetailNotes.pcf: line 52, column 40
    function verifyValueRange_9 () : void {
      var __valueRangeArg = policyPeriod.Policy.NoteAuthors
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_8(__valueRangeArg)
    }
    
    // 'visible' attribute on TypeKeyInput (id=LanguageSearch_Input) at PolicyDetailNotes.pcf: line 80, column 73
    function visible_47 () : java.lang.Boolean {
      return LanguageType.getTypeKeys( false ).Count > 1
    }
    
    property get NoteSearchCriteria () : entity.NoteSearchCriteria {
      return getCriteriaValue(1) as entity.NoteSearchCriteria
    }
    
    property set NoteSearchCriteria ($arg :  entity.NoteSearchCriteria) {
      setCriteriaValue(1, $arg)
    }
    
    property get notes () : gw.api.database.IQueryBeanResult<Note> {
      return getResultsValue(1) as gw.api.database.IQueryBeanResult<Note>
    }
    
    
  }
  
  
}