package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillPolicyDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillPolicyDetailsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillPolicyDetailsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillPolicyDetailsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'def' attribute on PanelRef at AgencyBillPolicyDetailsPopup.pcf: line 15, column 56
    function def_onEnter_0 (def :  pcf.AgencyBillPolicySummaryDV) : void {
      def.onEnter(policyPeriod)
    }
    
    // 'def' attribute on PanelRef at AgencyBillPolicyDetailsPopup.pcf: line 17, column 52
    function def_onEnter_2 (def :  pcf.AgencyBillPolicyTxnsLV) : void {
      def.onEnter(policyPeriod)
    }
    
    // 'def' attribute on PanelRef at AgencyBillPolicyDetailsPopup.pcf: line 15, column 56
    function def_refreshVariables_1 (def :  pcf.AgencyBillPolicySummaryDV) : void {
      def.refreshVariables(policyPeriod)
    }
    
    // 'def' attribute on PanelRef at AgencyBillPolicyDetailsPopup.pcf: line 17, column 52
    function def_refreshVariables_3 (def :  pcf.AgencyBillPolicyTxnsLV) : void {
      def.refreshVariables(policyPeriod)
    }
    
    override property get CurrentLocation () : pcf.AgencyBillPolicyDetailsPopup {
      return super.CurrentLocation as pcf.AgencyBillPolicyDetailsPopup
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}