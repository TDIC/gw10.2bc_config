package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/note/NewNoteScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewNoteScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/note/NewNoteScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewNoteScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=NewNoteScreen_UpdateButton) at NewNoteScreen.pcf: line 22, column 23
    function action_1 () : void {
      CurrentLocation.commit()
    }
    
    // 'action' attribute on ToolbarButton (id=NewNoteScreen_CancelButton) at NewNoteScreen.pcf: line 27, column 23
    function action_2 () : void {
      CurrentLocation.cancel(); gw.api.web.workspace.WorkspaceUtil.closeWorksheetIfActiveAndRefreshTop(CurrentLocation)
    }
    
    // 'def' attribute on PanelRef at NewNoteScreen.pcf: line 36, column 30
    function def_onEnter_3 (def :  pcf.NewNoteDV) : void {
      def.onEnter(Note)
    }
    
    // 'def' attribute on PanelRef at NewNoteScreen.pcf: line 36, column 30
    function def_refreshVariables_4 (def :  pcf.NewNoteDV) : void {
      def.refreshVariables(Note)
    }
    
    // 'initialValue' attribute on Variable at NewNoteScreen.pcf: line 16, column 50
    function initialValue_0 () : java.util.Map<String, Object> {
      return gw.api.util.SymbolTableUtil.populateBeans( srcBean )
    }
    
    property get Note () : Note {
      return getRequireValue("Note", 0) as Note
    }
    
    property set Note ($arg :  Note) {
      setRequireValue("Note", 0, $arg)
    }
    
    property get srcBean () : KeyableBean {
      return getRequireValue("srcBean", 0) as KeyableBean
    }
    
    property set srcBean ($arg :  KeyableBean) {
      setRequireValue("srcBean", 0, $arg)
    }
    
    property get symbolTable () : java.util.Map<String, Object> {
      return getVariableValue("symbolTable", 0) as java.util.Map<String, Object>
    }
    
    property set symbolTable ($arg :  java.util.Map<String, Object>) {
      setVariableValue("symbolTable", 0, $arg)
    }
    
    
    function createSearchCriteria() : NoteTemplateSearchCriteria {
      var rtn = new NoteTemplateSearchCriteria()
      // rtn.Language = Account.AccountHolder.Language 
      rtn.AvailableSymbols = symbolTable.Keys.join( "," )
      var policy = symbolTable.get( "policy" ) as Policy
      if (policy.LOBCode != null) {
        rtn.LOB = policy.LOBCode
      }
      return rtn  
    }
          
        
    
    
  }
  
  
}