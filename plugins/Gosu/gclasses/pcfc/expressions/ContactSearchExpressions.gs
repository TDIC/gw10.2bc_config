package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/ContactSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ContactSearchExpressions {
  @javax.annotation.Generated("config/web/pcf/search/ContactSearch.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ContactSearchExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'canVisit' attribute on Page (id=ContactSearch) at ContactSearch.pcf: line 12, column 67
    static function canVisit_2 () : java.lang.Boolean {
      return perm.System.viewsearch and (perm.System.acctsearch or perm.System.plcysearch or perm.System.prodsearch) and !perm.System.hidecontsearch_TDIC
    }
    
    // 'def' attribute on ScreenRef at ContactSearch.pcf: line 14, column 41
    function def_onEnter_0 (def :  pcf.ContactSearchScreen) : void {
      def.onEnter(false)
    }
    
    // 'def' attribute on ScreenRef at ContactSearch.pcf: line 14, column 41
    function def_refreshVariables_1 (def :  pcf.ContactSearchScreen) : void {
      def.refreshVariables(false)
    }
    
    // Page (id=ContactSearch) at ContactSearch.pcf: line 12, column 67
    static function parent_3 () : pcf.api.Destination {
      return pcf.SearchGroup.createDestination()
    }
    
    override property get CurrentLocation () : pcf.ContactSearch {
      return super.CurrentLocation as pcf.ContactSearch
    }
    
    
  }
  
  
}