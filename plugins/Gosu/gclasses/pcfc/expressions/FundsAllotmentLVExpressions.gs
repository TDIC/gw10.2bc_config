package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/FundsAllotmentLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class FundsAllotmentLVExpressions {
  @javax.annotation.Generated("config/web/pcf/account/FundsAllotmentLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class FundsAllotmentLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=unallottedAmount_Cell) at FundsAllotmentLV.pcf: line 48, column 48
    function currency_18 () : typekey.Currency {
      return fundsTracker.Currency
    }
    
    // 'initialValue' attribute on Variable at FundsAllotmentLV.pcf: line 13, column 65
    function initialValue_0 () : gw.api.web.fundstracking.FundsAllotmentViewItem[] {
      return new gw.api.web.fundstracking.FundsAllotmentsView(fundsTracker).FundsAllotmentViewItems
    }
    
    // 'value' attribute on TextCell (id=eventDescription_Cell) at FundsAllotmentLV.pcf: line 24, column 55
    function sortValue_1 (fundsAllotmentViewItem :  gw.api.web.fundstracking.FundsAllotmentViewItem) : java.lang.Object {
      return fundsAllotmentViewItem.Description
    }
    
    // 'value' attribute on DateCell (id=eventDate_Cell) at FundsAllotmentLV.pcf: line 28, column 63
    function sortValue_2 (fundsAllotmentViewItem :  gw.api.web.fundstracking.FundsAllotmentViewItem) : java.lang.Object {
      return fundsAllotmentViewItem.OtherSide.EventDate
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amount_Cell) at FundsAllotmentLV.pcf: line 34, column 58
    function sortValue_3 (fundsAllotmentViewItem :  gw.api.web.fundstracking.FundsAllotmentViewItem) : java.lang.Object {
      return fundsAllotmentViewItem.AmountAllotted
    }
    
    // 'value' attribute on MonetaryAmountCell (id=unallottedAmount_Cell) at FundsAllotmentLV.pcf: line 48, column 48
    function valueRoot_17 () : java.lang.Object {
      return fundsTracker
    }
    
    // 'value' attribute on RowIterator at FundsAllotmentLV.pcf: line 18, column 69
    function value_15 () : gw.api.web.fundstracking.FundsAllotmentViewItem[] {
      return fundsAllotmentViewItems
    }
    
    // 'value' attribute on MonetaryAmountCell (id=unallottedAmount_Cell) at FundsAllotmentLV.pcf: line 48, column 48
    function value_16 () : gw.pl.currency.MonetaryAmount {
      return fundsTracker.UnallottedAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=totalAmount_Cell) at FundsAllotmentLV.pcf: line 62, column 43
    function value_20 () : gw.pl.currency.MonetaryAmount {
      return fundsTracker.TotalAmount
    }
    
    property get fundsAllotmentViewItems () : gw.api.web.fundstracking.FundsAllotmentViewItem[] {
      return getVariableValue("fundsAllotmentViewItems", 0) as gw.api.web.fundstracking.FundsAllotmentViewItem[]
    }
    
    property set fundsAllotmentViewItems ($arg :  gw.api.web.fundstracking.FundsAllotmentViewItem[]) {
      setVariableValue("fundsAllotmentViewItems", 0, $arg)
    }
    
    property get fundsTracker () : FundsTracker {
      return getRequireValue("fundsTracker", 0) as FundsTracker
    }
    
    property set fundsTracker ($arg :  FundsTracker) {
      setRequireValue("fundsTracker", 0, $arg)
    }
    
    function openAllotmentsFor(theFundsTracker : FundsTracker) : pcf.api.Location {
      return theFundsTracker typeis FundsUseTracker
        ? UseOfFundsPopup.push(theFundsTracker)
        : SourceOfFundsPopup.push(theFundsTracker as FundsSourceTracker)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/account/FundsAllotmentLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends FundsAllotmentLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=eventDescription_Cell) at FundsAllotmentLV.pcf: line 24, column 55
    function action_4 () : void {
      openAllotmentsFor(fundsAllotmentViewItem.OtherSide)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=amount_Cell) at FundsAllotmentLV.pcf: line 34, column 58
    function currency_13 () : typekey.Currency {
      return fundsAllotmentViewItem.AmountAllotted.Currency
    }
    
    // 'value' attribute on TextCell (id=eventDescription_Cell) at FundsAllotmentLV.pcf: line 24, column 55
    function valueRoot_6 () : java.lang.Object {
      return fundsAllotmentViewItem
    }
    
    // 'value' attribute on DateCell (id=eventDate_Cell) at FundsAllotmentLV.pcf: line 28, column 63
    function valueRoot_9 () : java.lang.Object {
      return fundsAllotmentViewItem.OtherSide
    }
    
    // 'value' attribute on MonetaryAmountCell (id=amount_Cell) at FundsAllotmentLV.pcf: line 34, column 58
    function value_11 () : gw.pl.currency.MonetaryAmount {
      return fundsAllotmentViewItem.AmountAllotted
    }
    
    // 'value' attribute on TextCell (id=eventDescription_Cell) at FundsAllotmentLV.pcf: line 24, column 55
    function value_5 () : java.lang.String {
      return fundsAllotmentViewItem.Description
    }
    
    // 'value' attribute on DateCell (id=eventDate_Cell) at FundsAllotmentLV.pcf: line 28, column 63
    function value_8 () : java.util.Date {
      return fundsAllotmentViewItem.OtherSide.EventDate
    }
    
    property get fundsAllotmentViewItem () : gw.api.web.fundstracking.FundsAllotmentViewItem {
      return getIteratedValue(1) as gw.api.web.fundstracking.FundsAllotmentViewItem
    }
    
    
  }
  
  
}