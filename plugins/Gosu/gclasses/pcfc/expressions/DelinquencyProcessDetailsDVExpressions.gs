package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DelinquencyProcessDetailsDVExpressions {
  @javax.annotation.Generated("config/web/pcf/delinquency/DelinquencyProcessDetailsDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DelinquencyProcessDetailsDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextInput (id=DelinquencyPlan_Input) at DelinquencyProcessDetailsDV.pcf: line 31, column 45
    function action_3 () : void {
      pcf.DelinquencyPlanDetail.go(delinquencyProcess.DelinquencyPlan)
    }
    
    // 'action' attribute on TextInput (id=DelinquencyPlan_Input) at DelinquencyProcessDetailsDV.pcf: line 31, column 45
    function action_dest_4 () : pcf.api.Destination {
      return pcf.DelinquencyPlanDetail.createDestination(delinquencyProcess.DelinquencyPlan)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=AmountPastDue_Input) at DelinquencyProcessDetailsDV.pcf: line 82, column 134
    function currency_35 () : typekey.Currency {
      return delinquencyProcess.Currency
    }
    
    // 'def' attribute on InputSetRef (id=GracePeriodInputSet) at DelinquencyProcessDetailsDV.pcf: line 50, column 79
    function def_onEnter_17 (def :  pcf.DelinquencyProcessGracePeriodInputSet_default) : void {
      def.onEnter(delinquencyProcess)
    }
    
    // 'def' attribute on InputSetRef (id=GracePeriodInputSet) at DelinquencyProcessDetailsDV.pcf: line 50, column 79
    function def_refreshVariables_18 (def :  pcf.DelinquencyProcessGracePeriodInputSet_default) : void {
      def.refreshVariables(delinquencyProcess)
    }
    
    // 'initialValue' attribute on Variable at DelinquencyProcessDetailsDV.pcf: line 13, column 30
    function initialValue_0 () : java.util.Date {
      return gw.api.util.DateUtil.currentDate()
    }
    
    // 'initialValue' attribute on Variable at DelinquencyProcessDetailsDV.pcf: line 18, column 23
    function initialValue_1 () : boolean {
      return 0 < delinquencyProcess.Workflows.length
    }
    
    // 'initialValue' attribute on Variable at DelinquencyProcessDetailsDV.pcf: line 22, column 61
    function initialValue_2 () : gw.web.account.AccountSummaryFinancialsHelper {
      return delinquencyProcess == null ? null : new gw.web.account.AccountSummaryFinancialsHelper(delinquencyProcess.Account)
    }
    
    // 'mode' attribute on InputSetRef (id=GracePeriodInputSet) at DelinquencyProcessDetailsDV.pcf: line 50, column 79
    function mode_19 () : java.lang.Object {
      return gw.plugin.Plugins.get(gw.module.IFeesThresholds).getPCFMode()
    }
    
    // 'value' attribute on DateInput (id=LastPaymentDate_Input) at DelinquencyProcessDetailsDV.pcf: line 103, column 128
    function valueRoot_44 () : java.lang.Object {
      return delinquencyProcess.Account.findReceivedPaymentMoneysSortedByReceivedDateDescending().FirstResult
    }
    
    // 'value' attribute on TextInput (id=DelinquencyPlan_Input) at DelinquencyProcessDetailsDV.pcf: line 31, column 45
    function valueRoot_6 () : java.lang.Object {
      return delinquencyProcess
    }
    
    // 'value' attribute on DateInput (id=StartDate_Input) at DelinquencyProcessDetailsDV.pcf: line 40, column 47
    function value_11 () : java.util.Date {
      return delinquencyProcess.StartDate
    }
    
    // 'value' attribute on TypeKeyInput (id=Status_Input) at DelinquencyProcessDetailsDV.pcf: line 45, column 55
    function value_14 () : typekey.DelinquencyProcessStatus {
      return delinquencyProcess.Status
    }
    
    // 'value' attribute on TypeKeyInput (id=WorkflowState_Input) at DelinquencyProcessDetailsDV.pcf: line 60, column 41
    function value_22 () : typekey.WorkflowState {
      return delinquencyProcess.WorkflowState
    }
    
    // 'value' attribute on TextInput (id=CurrentEvent_Input) at DelinquencyProcessDetailsDV.pcf: line 65, column 41
    function value_27 () : java.lang.String {
      return delinquencyProcess.PreviousEvent.EventName == DelinquencyEventName.TC_DUNNINGLETTER1 ? DisplayKey.get("TDIC.Web.DelinquencyProcessEventsLV.EventName.Label") : delinquencyProcess.PreviousEvent.EventName.Description
    }
    
    // 'value' attribute on TextInput (id=NextEvent_Input) at DelinquencyProcessDetailsDV.pcf: line 70, column 41
    function value_31 () : java.lang.String {
      return delinquencyProcess.NextEvent.EventName == DelinquencyEventName.TC_DUNNINGLETTER1 ? DisplayKey.get("TDIC.Web.DelinquencyProcessEventsLV.EventName.Label") : delinquencyProcess.NextEvent.EventName.Description
    }
    
    // 'value' attribute on MonetaryAmountInput (id=AmountPastDue_Input) at DelinquencyProcessDetailsDV.pcf: line 82, column 134
    function value_34 () : gw.pl.currency.MonetaryAmount {
      return (delinquencyProcess as PolicyDlnqProcess).PolicyPeriod.Policy.OrderedPolicyPeriods.sum(\s -> s.DelinquentAmount)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalBilledAndOutstanding_Input) at DelinquencyProcessDetailsDV.pcf: line 89, column 144
    function value_37 () : gw.pl.currency.MonetaryAmount {
      return (delinquencyProcess as PolicyDlnqProcess).PolicyPeriod.Policy.OrderedPolicyPeriods.sum(\s -> s.OutstandingUnsettledAmount)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=TotalUnbilled_Input) at DelinquencyProcessDetailsDV.pcf: line 96, column 141
    function value_40 () : gw.pl.currency.MonetaryAmount {
      return (delinquencyProcess as PolicyDlnqProcess).PolicyPeriod.Policy.OrderedPolicyPeriods.sum(\s -> s.UnbilledUnsettledAmount)
    }
    
    // 'value' attribute on DateInput (id=LastPaymentDate_Input) at DelinquencyProcessDetailsDV.pcf: line 103, column 128
    function value_43 () : java.util.Date {
      return delinquencyProcess.Account.findReceivedPaymentMoneysSortedByReceivedDateDescending().FirstResult.ReceivedDate
    }
    
    // 'value' attribute on MonetaryAmountInput (id=LastPaymentAmount_Input) at DelinquencyProcessDetailsDV.pcf: line 109, column 122
    function value_46 () : gw.pl.currency.MonetaryAmount {
      return delinquencyProcess.Account.findReceivedPaymentMoneysSortedByReceivedDateDescending().FirstResult.Amount
    }
    
    // 'value' attribute on TextInput (id=DelinquencyPlan_Input) at DelinquencyProcessDetailsDV.pcf: line 31, column 45
    function value_5 () : entity.DelinquencyPlan {
      return delinquencyProcess.DelinquencyPlan
    }
    
    // 'value' attribute on TypeKeyInput (id=DelinquencyReason_Input) at DelinquencyProcessDetailsDV.pcf: line 36, column 48
    function value_8 () : typekey.DelinquencyReason {
      return delinquencyProcess.Reason
    }
    
    // 'visible' attribute on Label at DelinquencyProcessDetailsDV.pcf: line 54, column 41
    function visible_20 () : java.lang.Boolean {
      return workflowStateVisible
    }
    
    property get delinquencyProcess () : DelinquencyProcess {
      return getRequireValue("delinquencyProcess", 0) as DelinquencyProcess
    }
    
    property set delinquencyProcess ($arg :  DelinquencyProcess) {
      setRequireValue("delinquencyProcess", 0, $arg)
    }
    
    property get financialsHelper () : gw.web.account.AccountSummaryFinancialsHelper {
      return getVariableValue("financialsHelper", 0) as gw.web.account.AccountSummaryFinancialsHelper
    }
    
    property set financialsHelper ($arg :  gw.web.account.AccountSummaryFinancialsHelper) {
      setVariableValue("financialsHelper", 0, $arg)
    }
    
    property get today () : java.util.Date {
      return getVariableValue("today", 0) as java.util.Date
    }
    
    property set today ($arg :  java.util.Date) {
      setVariableValue("today", 0, $arg)
    }
    
    property get workflowStateVisible () : boolean {
      return getVariableValue("workflowStateVisible", 0) as java.lang.Boolean
    }
    
    property set workflowStateVisible ($arg :  boolean) {
      setVariableValue("workflowStateVisible", 0, $arg)
    }
    
    
  }
  
  
}