package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BillingPlanDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BillingPlanDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at BillingPlanDetailScreen.pcf: line 17, column 47
    function def_onEnter_4 (def :  pcf.BillingPlanDetailDV) : void {
      def.onEnter(billingPlan)
    }
    
    // 'def' attribute on PanelRef at BillingPlanDetailScreen.pcf: line 20, column 41
    function def_onEnter_7 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(billingPlan, { "Name", "Description"}, { DisplayKey.get("Web.BillingPlanDetailDV.Name"), DisplayKey.get("Web.BillingPlanDetailDV.Description") })
    }
    
    // 'def' attribute on PanelRef at BillingPlanDetailScreen.pcf: line 17, column 47
    function def_refreshVariables_5 (def :  pcf.BillingPlanDetailDV) : void {
      def.refreshVariables(billingPlan)
    }
    
    // 'def' attribute on PanelRef at BillingPlanDetailScreen.pcf: line 20, column 41
    function def_refreshVariables_8 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(billingPlan, { "Name", "Description"}, { DisplayKey.get("Web.BillingPlanDetailDV.Name"), DisplayKey.get("Web.BillingPlanDetailDV.Description") })
    }
    
    // 'editable' attribute on PanelRef at BillingPlanDetailScreen.pcf: line 20, column 41
    function editable_6 () : java.lang.Boolean {
      return not billingPlan.InUse
    }
    
    // EditButtons at BillingPlanDetailScreen.pcf: line 11, column 21
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'mode' attribute on ToolbarButtonSetRef at BillingPlanDetailScreen.pcf: line 14, column 75
    function mode_1 () : java.lang.Object {
      return gw.plugin.Plugins.get(gw.module.IFeesThresholds).getPCFMode()
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at BillingPlanDetailScreen.pcf: line 14, column 75
    function toolbarButtonSet_onEnter_2 (def :  pcf.BillingPlanCloneToolbarButtonSet_default) : void {
      def.onEnter(billingPlan)
    }
    
    // 'toolbarButtonSet' attribute on ToolbarButtonSetRef at BillingPlanDetailScreen.pcf: line 14, column 75
    function toolbarButtonSet_refreshVariables_3 (def :  pcf.BillingPlanCloneToolbarButtonSet_default) : void {
      def.refreshVariables(billingPlan)
    }
    
    property get billingPlan () : BillingPlan {
      return getRequireValue("billingPlan", 0) as BillingPlan
    }
    
    property set billingPlan ($arg :  BillingPlan) {
      setRequireValue("billingPlan", 0, $arg)
    }
    
    
  }
  
  
}