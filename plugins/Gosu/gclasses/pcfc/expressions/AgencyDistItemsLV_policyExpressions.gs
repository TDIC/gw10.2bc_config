package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistItemsLV.policy.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyDistItemsLV_policyExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistItemsLV.policy.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyDistItemsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on TextCell (id=PolicyDescription_Cell) at AgencyDistItemsLV.policy.pcf: line 29, column 99
    function sortValue_0 (policyPeriodView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) : java.lang.Object {
      return policyPeriodView.ChargeOwnerTypeName
    }
    
    // 'sortBy' attribute on TextCell (id=PolicyDescription_Cell) at AgencyDistItemsLV.policy.pcf: line 29, column 99
    function sortValue_1 (policyPeriodView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) : java.lang.Object {
      return  policyPeriodView.DisplayName
    }
    
    // 'value' attribute on TextCell (id=OwnerAccount_Cell) at AgencyDistItemsLV.policy.pcf: line 34, column 60
    function sortValue_2 (policyPeriodView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) : java.lang.Object {
      return getAccountDisplayName(policyPeriodView)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NewOwed_Cell) at AgencyDistItemsLV.policy.pcf: line 40, column 51
    function sortValue_3 (policyPeriodView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) : java.lang.Object {
      return policyPeriodView.NetAmountOwed
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetToApply_Cell) at AgencyDistItemsLV.policy.pcf: line 59, column 63
    function sortValue_4 (policyPeriodView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) : java.lang.Object {
      return policyPeriodView.TotalNetAmountToDistribute
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Difference_Cell) at AgencyDistItemsLV.policy.pcf: line 79, column 97
    function sortValue_5 (policyPeriodView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) : java.lang.Object {
      return policyPeriodView.NetAmountOwed - policyPeriodView.TotalNetAmountToDistribute
    }
    
    // 'value' attribute on BooleanRadioCell (id=Comments_Cell) at AgencyDistItemsLV.policy.pcf: line 87, column 60
    function sortValue_6 (policyPeriodView :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) : java.lang.Object {
      return policyPeriodView.hasDistItemsComments()
    }
    
    // 'value' attribute on RowIterator (id=PolicyPeriodInvoiceItems) at AgencyDistItemsLV.policy.pcf: line 20, column 122
    function value_39 () : java.util.List<gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView> {
      return agencyCycleDistView.ChargeOwners
    }
    
    property get agencyCycleDistView () : gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistView {
      return getRequireValue("agencyCycleDistView", 0) as gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistView
    }
    
    property set agencyCycleDistView ($arg :  gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistView) {
      setRequireValue("agencyCycleDistView", 0, $arg)
    }
    
    property get wizardState () : gw.agencybill.AgencyDistributionWizardHelper {
      return getRequireValue("wizardState", 0) as gw.agencybill.AgencyDistributionWizardHelper
    }
    
    property set wizardState ($arg :  gw.agencybill.AgencyDistributionWizardHelper) {
      setRequireValue("wizardState", 0, $arg)
    }
    
    function getAccountDisplayName(chargeOwnerView : gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView) : String {
      return chargeOwnerView typeis gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistPolicyView ? chargeOwnerView.Account.DisplayName : chargeOwnerView.DisplayName
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/distributionwizard/AgencyDistItemsLV.policy.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AgencyDistItemsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=PolicyDescription_Cell) at AgencyDistItemsLV.policy.pcf: line 29, column 99
    function actionAvailable_10 () : java.lang.Boolean {
      return policyPeriodView.ChargeOwner typeis PolicyPeriod or policyPeriodView.ChargeOwner typeis Account
    }
    
    // 'action' attribute on TextCell (id=OwnerAccount_Cell) at AgencyDistItemsLV.policy.pcf: line 34, column 60
    function action_14 () : void {
      AccountSummaryPopup.push(policyPeriodView.Account)
    }
    
    // 'action' attribute on Link (id=FillDefaultsButton) at AgencyDistItemsLV.policy.pcf: line 51, column 55
    function action_23 () : void {
      policyPeriodView.TotalNetAmountToDistribute = policyPeriodView.NetAmountOwed
    }
    
    // 'action' attribute on Link (id=DetailsButton) at AgencyDistItemsLV.policy.pcf: line 72, column 139
    function action_32 () : void {
      AgencyDistributionWizard_DetailsPopup.push(policyPeriodView, policyPeriodView.TotalNetAmountToDistribute, wizardState)
    }
    
    // 'action' attribute on TextCell (id=PolicyDescription_Cell) at AgencyDistItemsLV.policy.pcf: line 29, column 99
    function action_8 () : void {
      AgencyChargeOwnerDetailForward.push(policyPeriodView)
    }
    
    // 'action' attribute on TextCell (id=OwnerAccount_Cell) at AgencyDistItemsLV.policy.pcf: line 34, column 60
    function action_dest_15 () : pcf.api.Destination {
      return pcf.AccountSummaryPopup.createDestination(policyPeriodView.Account)
    }
    
    // 'action' attribute on Link (id=DetailsButton) at AgencyDistItemsLV.policy.pcf: line 72, column 139
    function action_dest_33 () : pcf.api.Destination {
      return pcf.AgencyDistributionWizard_DetailsPopup.createDestination(policyPeriodView, policyPeriodView.TotalNetAmountToDistribute, wizardState)
    }
    
    // 'action' attribute on TextCell (id=PolicyDescription_Cell) at AgencyDistItemsLV.policy.pcf: line 29, column 99
    function action_dest_9 () : pcf.api.Destination {
      return pcf.AgencyChargeOwnerDetailForward.createDestination(policyPeriodView)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=NewOwed_Cell) at AgencyDistItemsLV.policy.pcf: line 40, column 51
    function currency_20 () : typekey.Currency {
      return policyPeriodView.NetAmountOwed.Currency
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=NetToApply_Cell) at AgencyDistItemsLV.policy.pcf: line 59, column 63
    function currency_29 () : typekey.Currency {
      return policyPeriodView.TotalNetAmountToDistribute.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetToApply_Cell) at AgencyDistItemsLV.policy.pcf: line 59, column 63
    function defaultSetter_27 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyPeriodView.TotalNetAmountToDistribute = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'onChange' attribute on PostOnChange at AgencyDistItemsLV.policy.pcf: line 62, column 216
    function onChange_24 () : void {
      if (policyPeriodView.NetAmountToApplyException) throw new gw.api.util.DisplayableException(DisplayKey.get("Java.Error.AgencyCycleDistChargeOwnerView.InvalidNetDistributionAmount"))
    }
    
    // 'value' attribute on TextCell (id=PolicyDescription_Cell) at AgencyDistItemsLV.policy.pcf: line 29, column 99
    function valueRoot_12 () : java.lang.Object {
      return policyPeriodView
    }
    
    // 'value' attribute on TextCell (id=PolicyDescription_Cell) at AgencyDistItemsLV.policy.pcf: line 29, column 99
    function value_11 () : java.lang.String {
      return policyPeriodView.DisplayName
    }
    
    // 'value' attribute on TextCell (id=OwnerAccount_Cell) at AgencyDistItemsLV.policy.pcf: line 34, column 60
    function value_16 () : java.lang.String {
      return getAccountDisplayName(policyPeriodView)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NewOwed_Cell) at AgencyDistItemsLV.policy.pcf: line 40, column 51
    function value_18 () : gw.pl.currency.MonetaryAmount {
      return policyPeriodView.NetAmountOwed
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetToApply_Cell) at AgencyDistItemsLV.policy.pcf: line 59, column 63
    function value_26 () : gw.pl.currency.MonetaryAmount {
      return policyPeriodView.TotalNetAmountToDistribute
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Difference_Cell) at AgencyDistItemsLV.policy.pcf: line 79, column 97
    function value_34 () : gw.pl.currency.MonetaryAmount {
      return policyPeriodView.NetAmountOwed - policyPeriodView.TotalNetAmountToDistribute
    }
    
    // 'value' attribute on BooleanRadioCell (id=Comments_Cell) at AgencyDistItemsLV.policy.pcf: line 87, column 60
    function value_37 () : java.lang.Boolean {
      return policyPeriodView.hasDistItemsComments()
    }
    
    // 'visible' attribute on Link (id=FillDefaultsButton) at AgencyDistItemsLV.policy.pcf: line 51, column 55
    function visible_22 () : java.lang.Boolean {
      return policyPeriodView.NetModifiable
    }
    
    // 'valueVisible' attribute on TextCell (id=PolicyDescription_Cell) at AgencyDistItemsLV.policy.pcf: line 29, column 99
    function visible_7 () : java.lang.Boolean {
      return PolicyPeriod.Type.isAssignableFrom(typeof policyPeriodView.ChargeOwner)
    }
    
    property get policyPeriodView () : gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView {
      return getIteratedValue(1) as gw.api.web.producer.agencybill.distributionwizard.AgencyCycleDistChargeOwnerView
    }
    
    
  }
  
  
}