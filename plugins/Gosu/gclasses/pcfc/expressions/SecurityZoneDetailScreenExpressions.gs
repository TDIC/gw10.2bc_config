package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/securityzones/SecurityZoneDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SecurityZoneDetailScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/securityzones/SecurityZoneDetailScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SecurityZoneDetailScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at SecurityZoneDetailScreen.pcf: line 14, column 49
    function def_onEnter_1 (def :  pcf.SecurityZoneDetailDV) : void {
      def.onEnter(securityZone)
    }
    
    // 'def' attribute on PanelRef at SecurityZoneDetailScreen.pcf: line 16, column 234
    function def_onEnter_3 (def :  pcf.LocalizedValuesDV) : void {
      def.onEnter(securityZone, { "Name", "Description" }, { DisplayKey.get("Web.Admin.SecurityZoneDetailDV.Name"), DisplayKey.get("Web.Admin.SecurityZoneDetailDV.Description") })
    }
    
    // 'def' attribute on PanelRef at SecurityZoneDetailScreen.pcf: line 14, column 49
    function def_refreshVariables_2 (def :  pcf.SecurityZoneDetailDV) : void {
      def.refreshVariables(securityZone)
    }
    
    // 'def' attribute on PanelRef at SecurityZoneDetailScreen.pcf: line 16, column 234
    function def_refreshVariables_4 (def :  pcf.LocalizedValuesDV) : void {
      def.refreshVariables(securityZone, { "Name", "Description" }, { DisplayKey.get("Web.Admin.SecurityZoneDetailDV.Name"), DisplayKey.get("Web.Admin.SecurityZoneDetailDV.Description") })
    }
    
    // EditButtons at SecurityZoneDetailScreen.pcf: line 11, column 21
    function label_0 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    property get securityZone () : SecurityZone {
      return getRequireValue("securityZone", 0) as SecurityZone
    }
    
    property set securityZone ($arg :  SecurityZone) {
      setRequireValue("securityZone", 0, $arg)
    }
    
    
  }
  
  
}