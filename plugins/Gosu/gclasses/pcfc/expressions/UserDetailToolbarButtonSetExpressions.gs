package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/users/UserDetailToolbarButtonSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class UserDetailToolbarButtonSetExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/users/UserDetailToolbarButtonSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class UserDetailToolbarButtonSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on ToolbarButton (id=UserDetailToolbarButtons_DeleteUserButton) at UserDetailToolbarButtonSet.pcf: line 21, column 83
    function action_5 () : void {
      gw.api.web.admin.UserUtil.delete(User); Admin.go()
    }
    
    // 'available' attribute on ToolbarButton (id=UserDetailToolbarButtons_DeleteUserButton) at UserDetailToolbarButtonSet.pcf: line 21, column 83
    function available_3 () : java.lang.Boolean {
      return User.SafeToDelete
    }
    
    // EditButtons at UserDetailToolbarButtonSet.pcf: line 12, column 42
    function label_2 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'editVisible' attribute on EditButtons at UserDetailToolbarButtonSet.pcf: line 12, column 42
    function visible_0 () : java.lang.Boolean {
      return User.isEditable()
    }
    
    // 'visible' attribute on ToolbarButton (id=UserDetailToolbarButtons_DeleteUserButton) at UserDetailToolbarButtonSet.pcf: line 21, column 83
    function visible_4 () : java.lang.Boolean {
      return perm.User.delete and perm.System.useradmin and User.isEditable()
    }
    
    property get User () : User {
      return getRequireValue("User", 0) as User
    }
    
    property set User ($arg :  User) {
      setRequireValue("User", 0, $arg)
    }
    
    
  }
  
  
}