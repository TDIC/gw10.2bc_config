package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadPaymentPlanOverridesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanDataUploadPaymentPlanOverridesLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadPaymentPlanOverridesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PlanDataUploadPaymentPlanOverridesLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at PlanDataUploadPaymentPlanOverridesLV.pcf: line 17, column 118
    function highlighted_92 () : java.lang.Boolean {
      return (paymentPlanOverride.Error or paymentPlanOverride.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 23, column 46
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 29, column 41
    function label_32 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.PlanName")
    }
    
    // 'label' attribute on TextCell (id=downPaymentPercent_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 34, column 45
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.DownPaymentPercentage")
    }
    
    // 'label' attribute on TextCell (id=maximumNumberOfInstallments_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 39, column 42
    function label_42 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.MaxInstallments")
    }
    
    // 'label' attribute on TextCell (id=downPaymentDays_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 44, column 42
    function label_47 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.DaysToDownPayment")
    }
    
    // 'label' attribute on TypeKeyCell (id=downPaymentAfter_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 49, column 54
    function label_52 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.DownPaymentDaysAfter")
    }
    
    // 'label' attribute on TextCell (id=daysToFirstInstallment_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 54, column 42
    function label_57 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.DaysToFirstInstallment")
    }
    
    // 'label' attribute on TypeKeyCell (id=FirstInstallmentAfter_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 59, column 54
    function label_62 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.FirstInstallmentDaysAfter")
    }
    
    // 'label' attribute on TypeKeyCell (id=DownPaymentSecondInstallment_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 64, column 61
    function label_67 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.DownPaymentSecondInstallment")
    }
    
    // 'label' attribute on TextCell (id=daysToSecondInstallment_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 69, column 42
    function label_72 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.DaysToSecondInstallment")
    }
    
    // 'label' attribute on TypeKeyCell (id=SecondInstallmentAfter_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 74, column 54
    function label_77 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.SecondInstallmentDaysAfter")
    }
    
    // 'label' attribute on TextCell (id=daysToOneTimeCharge_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 79, column 42
    function label_82 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.DaysToOneTimeCharge")
    }
    
    // 'label' attribute on TypeKeyCell (id=oneTimeChargeAfter_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 84, column 54
    function label_87 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.OneTimeChargeDaysAfter")
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 29, column 41
    function valueRoot_34 () : java.lang.Object {
      return paymentPlanOverride
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 23, column 46
    function value_28 () : java.lang.String {
      return processor.getLoadStatus(paymentPlanOverride)
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 29, column 41
    function value_33 () : java.lang.String {
      return paymentPlanOverride.LookupPaymentPlanData
    }
    
    // 'value' attribute on TextCell (id=downPaymentPercent_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 34, column 45
    function value_38 () : java.math.BigDecimal {
      return paymentPlanOverride.DownPaymentPercent
    }
    
    // 'value' attribute on TextCell (id=maximumNumberOfInstallments_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 39, column 42
    function value_43 () : java.lang.Integer {
      return paymentPlanOverride.MaximumNumberOfInstallments
    }
    
    // 'value' attribute on TextCell (id=downPaymentDays_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 44, column 42
    function value_48 () : java.lang.Integer {
      return paymentPlanOverride.DaysFromReferenceDateToDownPayment
    }
    
    // 'value' attribute on TypeKeyCell (id=downPaymentAfter_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 49, column 54
    function value_53 () : typekey.PaymentScheduledAfter {
      return paymentPlanOverride.DownPaymentAfter
    }
    
    // 'value' attribute on TextCell (id=daysToFirstInstallment_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 54, column 42
    function value_58 () : java.lang.Integer {
      return paymentPlanOverride.DaysFromReferenceDateToFirstInstallment
    }
    
    // 'value' attribute on TypeKeyCell (id=FirstInstallmentAfter_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 59, column 54
    function value_63 () : typekey.PaymentScheduledAfter {
      return paymentPlanOverride.FirstInstallmentAfter
    }
    
    // 'value' attribute on TypeKeyCell (id=DownPaymentSecondInstallment_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 64, column 61
    function value_68 () : typekey.DownPaymentSecondInstallment {
      return paymentPlanOverride.DownPaymentSecondInstallment
    }
    
    // 'value' attribute on TextCell (id=daysToSecondInstallment_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 69, column 42
    function value_73 () : java.lang.Integer {
      return paymentPlanOverride.DaysFromReferenceDateToSecondInstallment
    }
    
    // 'value' attribute on TypeKeyCell (id=SecondInstallmentAfter_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 74, column 54
    function value_78 () : typekey.PaymentScheduledAfter {
      return paymentPlanOverride.SecondInstallmentAfter
    }
    
    // 'value' attribute on TextCell (id=daysToOneTimeCharge_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 79, column 42
    function value_83 () : java.lang.Integer {
      return paymentPlanOverride.DaysFromReferenceDateToOneTimeCharge
    }
    
    // 'value' attribute on TypeKeyCell (id=oneTimeChargeAfter_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 84, column 54
    function value_88 () : typekey.PaymentScheduledAfter {
      return paymentPlanOverride.OneTimeChargeAfter
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 23, column 46
    function visible_29 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get paymentPlanOverride () : tdic.util.dataloader.data.plandata.PaymentPlanOverrideData {
      return getIteratedValue(1) as tdic.util.dataloader.data.plandata.PaymentPlanOverrideData
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadPaymentPlanOverridesLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanDataUploadPaymentPlanOverridesLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TypeKeyCell (id=downPaymentAfter_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 49, column 54
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.DownPaymentDaysAfter")
    }
    
    // 'label' attribute on TextCell (id=daysToFirstInstallment_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 54, column 42
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.DaysToFirstInstallment")
    }
    
    // 'label' attribute on TypeKeyCell (id=FirstInstallmentAfter_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 59, column 54
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.FirstInstallmentDaysAfter")
    }
    
    // 'label' attribute on TypeKeyCell (id=DownPaymentSecondInstallment_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 64, column 61
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.DownPaymentSecondInstallment")
    }
    
    // 'label' attribute on TextCell (id=daysToSecondInstallment_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 69, column 42
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.DaysToSecondInstallment")
    }
    
    // 'label' attribute on TypeKeyCell (id=SecondInstallmentAfter_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 74, column 54
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.SecondInstallmentDaysAfter")
    }
    
    // 'label' attribute on TextCell (id=daysToOneTimeCharge_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 79, column 42
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.DaysToOneTimeCharge")
    }
    
    // 'label' attribute on TypeKeyCell (id=oneTimeChargeAfter_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 84, column 54
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.OneTimeChargeDaysAfter")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.PlanName")
    }
    
    // 'label' attribute on TextCell (id=downPaymentPercent_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 34, column 45
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.DownPaymentPercentage")
    }
    
    // 'label' attribute on TextCell (id=maximumNumberOfInstallments_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 39, column 42
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.MaxInstallments")
    }
    
    // 'label' attribute on TextCell (id=downPaymentDays_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 44, column 42
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverride.DaysToDownPayment")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 23, column 46
    function sortValue_1 (paymentPlanOverride :  tdic.util.dataloader.data.plandata.PaymentPlanOverrideData) : java.lang.Object {
      return processor.getLoadStatus(paymentPlanOverride)
    }
    
    // 'value' attribute on TextCell (id=downPaymentDays_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 44, column 42
    function sortValue_10 (paymentPlanOverride :  tdic.util.dataloader.data.plandata.PaymentPlanOverrideData) : java.lang.Object {
      return paymentPlanOverride.DaysFromReferenceDateToDownPayment
    }
    
    // 'value' attribute on TypeKeyCell (id=downPaymentAfter_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 49, column 54
    function sortValue_12 (paymentPlanOverride :  tdic.util.dataloader.data.plandata.PaymentPlanOverrideData) : java.lang.Object {
      return paymentPlanOverride.DownPaymentAfter
    }
    
    // 'value' attribute on TextCell (id=daysToFirstInstallment_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 54, column 42
    function sortValue_14 (paymentPlanOverride :  tdic.util.dataloader.data.plandata.PaymentPlanOverrideData) : java.lang.Object {
      return paymentPlanOverride.DaysFromReferenceDateToFirstInstallment
    }
    
    // 'value' attribute on TypeKeyCell (id=FirstInstallmentAfter_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 59, column 54
    function sortValue_16 (paymentPlanOverride :  tdic.util.dataloader.data.plandata.PaymentPlanOverrideData) : java.lang.Object {
      return paymentPlanOverride.FirstInstallmentAfter
    }
    
    // 'value' attribute on TypeKeyCell (id=DownPaymentSecondInstallment_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 64, column 61
    function sortValue_18 (paymentPlanOverride :  tdic.util.dataloader.data.plandata.PaymentPlanOverrideData) : java.lang.Object {
      return paymentPlanOverride.DownPaymentSecondInstallment
    }
    
    // 'value' attribute on TextCell (id=daysToSecondInstallment_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 69, column 42
    function sortValue_20 (paymentPlanOverride :  tdic.util.dataloader.data.plandata.PaymentPlanOverrideData) : java.lang.Object {
      return paymentPlanOverride.DaysFromReferenceDateToSecondInstallment
    }
    
    // 'value' attribute on TypeKeyCell (id=SecondInstallmentAfter_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 74, column 54
    function sortValue_22 (paymentPlanOverride :  tdic.util.dataloader.data.plandata.PaymentPlanOverrideData) : java.lang.Object {
      return paymentPlanOverride.SecondInstallmentAfter
    }
    
    // 'value' attribute on TextCell (id=daysToOneTimeCharge_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 79, column 42
    function sortValue_24 (paymentPlanOverride :  tdic.util.dataloader.data.plandata.PaymentPlanOverrideData) : java.lang.Object {
      return paymentPlanOverride.DaysFromReferenceDateToOneTimeCharge
    }
    
    // 'value' attribute on TypeKeyCell (id=oneTimeChargeAfter_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 84, column 54
    function sortValue_26 (paymentPlanOverride :  tdic.util.dataloader.data.plandata.PaymentPlanOverrideData) : java.lang.Object {
      return paymentPlanOverride.OneTimeChargeAfter
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 29, column 41
    function sortValue_4 (paymentPlanOverride :  tdic.util.dataloader.data.plandata.PaymentPlanOverrideData) : java.lang.Object {
      return paymentPlanOverride.LookupPaymentPlanData
    }
    
    // 'value' attribute on TextCell (id=downPaymentPercent_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 34, column 45
    function sortValue_6 (paymentPlanOverride :  tdic.util.dataloader.data.plandata.PaymentPlanOverrideData) : java.lang.Object {
      return paymentPlanOverride.DownPaymentPercent
    }
    
    // 'value' attribute on TextCell (id=maximumNumberOfInstallments_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 39, column 42
    function sortValue_8 (paymentPlanOverride :  tdic.util.dataloader.data.plandata.PaymentPlanOverrideData) : java.lang.Object {
      return paymentPlanOverride.MaximumNumberOfInstallments
    }
    
    // 'value' attribute on RowIterator (id=PaymentPlanOverride) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 15, column 105
    function value_93 () : java.util.ArrayList<tdic.util.dataloader.data.plandata.PaymentPlanOverrideData> {
      return processor.PaymentPlanOverrideArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadPaymentPlanOverridesLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}