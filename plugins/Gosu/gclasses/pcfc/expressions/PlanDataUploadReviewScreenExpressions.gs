package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadReviewScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanDataUploadReviewScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadReviewScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanDataUploadReviewScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 145, column 68
    function def_onEnter_100 (def :  pcf.PlanDataUploadDelinquencyPlanWorkflowLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 153, column 59
    function def_onEnter_102 (def :  pcf.PlanDataUploadCommissionPlanLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 161, column 62
    function def_onEnter_104 (def :  pcf.PlanDataUploadCommissionSubPlanLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 169, column 58
    function def_onEnter_106 (def :  pcf.PlanDataUploadChargePatternLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 177, column 66
    function def_onEnter_108 (def :  pcf.PlanDataUploadPaymentAllocationPlanLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 185, column 62
    function def_onEnter_110 (def :  pcf.PlanDataUploadReturnPremiumPlanLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 193, column 64
    function def_onEnter_112 (def :  pcf.PlanDataUploadReturnPremiumSchemeLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 201, column 58
    function def_onEnter_114 (def :  pcf.TDIC_PlanDataUploadGLFilterLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 209, column 70
    function def_onEnter_116 (def :  pcf.TDIC_PlanDataUploadGLTransactionMappingLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 217, column 71
    function def_onEnter_118 (def :  pcf.TDIC_PlanDataUploadGLTAccountNameMappingLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 105, column 56
    function def_onEnter_90 (def :  pcf.PlanDataUploadBillingPlanLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 113, column 59
    function def_onEnter_92 (def :  pcf.PlanDataUploadAgencyBillPlanLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 121, column 56
    function def_onEnter_94 (def :  pcf.PlanDataUploadPaymentPlanLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 129, column 65
    function def_onEnter_96 (def :  pcf.PlanDataUploadPaymentPlanOverridesLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 137, column 60
    function def_onEnter_98 (def :  pcf.PlanDataUploadDelinquencyPlanLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 145, column 68
    function def_refreshVariables_101 (def :  pcf.PlanDataUploadDelinquencyPlanWorkflowLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 153, column 59
    function def_refreshVariables_103 (def :  pcf.PlanDataUploadCommissionPlanLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 161, column 62
    function def_refreshVariables_105 (def :  pcf.PlanDataUploadCommissionSubPlanLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 169, column 58
    function def_refreshVariables_107 (def :  pcf.PlanDataUploadChargePatternLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 177, column 66
    function def_refreshVariables_109 (def :  pcf.PlanDataUploadPaymentAllocationPlanLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 185, column 62
    function def_refreshVariables_111 (def :  pcf.PlanDataUploadReturnPremiumPlanLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 193, column 64
    function def_refreshVariables_113 (def :  pcf.PlanDataUploadReturnPremiumSchemeLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 201, column 58
    function def_refreshVariables_115 (def :  pcf.TDIC_PlanDataUploadGLFilterLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 209, column 70
    function def_refreshVariables_117 (def :  pcf.TDIC_PlanDataUploadGLTransactionMappingLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 217, column 71
    function def_refreshVariables_119 (def :  pcf.TDIC_PlanDataUploadGLTAccountNameMappingLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 105, column 56
    function def_refreshVariables_91 (def :  pcf.PlanDataUploadBillingPlanLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 113, column 59
    function def_refreshVariables_93 (def :  pcf.PlanDataUploadAgencyBillPlanLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 121, column 56
    function def_refreshVariables_95 (def :  pcf.PlanDataUploadPaymentPlanLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 129, column 65
    function def_refreshVariables_97 (def :  pcf.PlanDataUploadPaymentPlanOverridesLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadReviewScreen.pcf: line 137, column 60
    function def_refreshVariables_99 (def :  pcf.PlanDataUploadDelinquencyPlanLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkPaymentPlan_Input) at PlanDataUploadReviewScreen.pcf: line 32, column 45
    function defaultSetter_14 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkPaymentPlan = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkBillingPlan_Input) at PlanDataUploadReviewScreen.pcf: line 22, column 45
    function defaultSetter_2 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkBillingPlan = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=ChkPaymentPlanOverride_Input) at PlanDataUploadReviewScreen.pcf: line 37, column 53
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkPaymentPlanOverride = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkDelinquencyPlan_Input) at PlanDataUploadReviewScreen.pcf: line 42, column 49
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkDelinquencyPlan = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkDelinquencyPlanWorkflow_Input) at PlanDataUploadReviewScreen.pcf: line 47, column 57
    function defaultSetter_32 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkDelinquencyPlanWorkflow = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkCommissionPlan_Input) at PlanDataUploadReviewScreen.pcf: line 55, column 48
    function defaultSetter_38 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkCommissionPlan = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkCommissionSubPlan_Input) at PlanDataUploadReviewScreen.pcf: line 60, column 51
    function defaultSetter_44 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkCommissionSubPlan = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkChargePattern_Input) at PlanDataUploadReviewScreen.pcf: line 65, column 47
    function defaultSetter_50 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkChargePattern = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkPaymentAllocationPlan_Input) at PlanDataUploadReviewScreen.pcf: line 70, column 55
    function defaultSetter_56 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkPaymentAllocationPlan = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkReturnPremiumPlan_Input) at PlanDataUploadReviewScreen.pcf: line 75, column 51
    function defaultSetter_62 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkReturnPremiumPlan = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkReturnPremiumScheme_Input) at PlanDataUploadReviewScreen.pcf: line 80, column 53
    function defaultSetter_68 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkReturnPremiumScheme = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkGLTransactionFilter_Input) at PlanDataUploadReviewScreen.pcf: line 87, column 53
    function defaultSetter_74 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkGLTransactionFilter = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=ChkAgencyBillPlan_Input) at PlanDataUploadReviewScreen.pcf: line 27, column 48
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkAgencyBillPlan = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkGLTransactionNameMapping_Input) at PlanDataUploadReviewScreen.pcf: line 92, column 58
    function defaultSetter_80 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkGLTransactionNameMapping = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkGLTAccountNameMapping_Input) at PlanDataUploadReviewScreen.pcf: line 97, column 55
    function defaultSetter_86 (__VALUE_TO_SET :  java.lang.Object) : void {
      processor.ChkGLTAccountNameMapping = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkBillingPlan_Input) at PlanDataUploadReviewScreen.pcf: line 22, column 45
    function label_0 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlans"), processor.BillingPlanArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkPaymentPlan_Input) at PlanDataUploadReviewScreen.pcf: line 32, column 45
    function label_12 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlans"), processor.PaymentPlanArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=ChkPaymentPlanOverride_Input) at PlanDataUploadReviewScreen.pcf: line 37, column 53
    function label_18 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverrides"), processor.PaymentPlanOverrideArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkDelinquencyPlan_Input) at PlanDataUploadReviewScreen.pcf: line 42, column 49
    function label_24 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlans"), processor.DelinquencyPlanArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkDelinquencyPlanWorkflow_Input) at PlanDataUploadReviewScreen.pcf: line 47, column 57
    function label_30 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflowSteps"), processor.DelinquencyPlanWorkflowArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkCommissionPlan_Input) at PlanDataUploadReviewScreen.pcf: line 55, column 48
    function label_36 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionPlans"), processor.CommissionPlanArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkCommissionSubPlan_Input) at PlanDataUploadReviewScreen.pcf: line 60, column 51
    function label_42 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlans"), processor.CommissionSubPlanArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkChargePattern_Input) at PlanDataUploadReviewScreen.pcf: line 65, column 47
    function label_48 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePatterns"), processor.ChargePatternArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkPaymentAllocationPlan_Input) at PlanDataUploadReviewScreen.pcf: line 70, column 55
    function label_54 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentAllocationPlans"), processor.PaymentAllocationPlanArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=ChkAgencyBillPlan_Input) at PlanDataUploadReviewScreen.pcf: line 27, column 48
    function label_6 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.AgencyBillPlans"), processor.AgencyBillPlanArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkReturnPremiumPlan_Input) at PlanDataUploadReviewScreen.pcf: line 75, column 51
    function label_60 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumPlans"), processor.ReturnPremiumPlanArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkReturnPremiumScheme_Input) at PlanDataUploadReviewScreen.pcf: line 80, column 53
    function label_66 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumSchemes"), processor.ReturnPremiumSchemeArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkGLTransactionFilter_Input) at PlanDataUploadReviewScreen.pcf: line 87, column 53
    function label_72 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.GLTransactionFilter"), processor.GLTransactionFilterArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkGLTransactionNameMapping_Input) at PlanDataUploadReviewScreen.pcf: line 92, column 58
    function label_78 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.GLTransactionNameMapping"), processor.GLTransactionNameMappingArray.Count)
    }
    
    // 'label' attribute on CheckBoxInput (id=chkGLTAccountNameMapping_Input) at PlanDataUploadReviewScreen.pcf: line 97, column 55
    function label_84 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ReviewIncludeLabel", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.GLTAccountNameMapping"), processor.GLTAccountNameMappingArray.Count)
    }
    
    // 'value' attribute on CheckBoxInput (id=chkBillingPlan_Input) at PlanDataUploadReviewScreen.pcf: line 22, column 45
    function valueRoot_3 () : java.lang.Object {
      return processor
    }
    
    // 'value' attribute on CheckBoxInput (id=chkBillingPlan_Input) at PlanDataUploadReviewScreen.pcf: line 22, column 45
    function value_1 () : java.lang.Boolean {
      return processor.ChkBillingPlan
    }
    
    // 'value' attribute on CheckBoxInput (id=chkPaymentPlan_Input) at PlanDataUploadReviewScreen.pcf: line 32, column 45
    function value_13 () : java.lang.Boolean {
      return processor.ChkPaymentPlan
    }
    
    // 'value' attribute on CheckBoxInput (id=ChkPaymentPlanOverride_Input) at PlanDataUploadReviewScreen.pcf: line 37, column 53
    function value_19 () : java.lang.Boolean {
      return processor.ChkPaymentPlanOverride
    }
    
    // 'value' attribute on CheckBoxInput (id=chkDelinquencyPlan_Input) at PlanDataUploadReviewScreen.pcf: line 42, column 49
    function value_25 () : java.lang.Boolean {
      return processor.ChkDelinquencyPlan
    }
    
    // 'value' attribute on CheckBoxInput (id=chkDelinquencyPlanWorkflow_Input) at PlanDataUploadReviewScreen.pcf: line 47, column 57
    function value_31 () : java.lang.Boolean {
      return processor.ChkDelinquencyPlanWorkflow
    }
    
    // 'value' attribute on CheckBoxInput (id=chkCommissionPlan_Input) at PlanDataUploadReviewScreen.pcf: line 55, column 48
    function value_37 () : java.lang.Boolean {
      return processor.ChkCommissionPlan
    }
    
    // 'value' attribute on CheckBoxInput (id=chkCommissionSubPlan_Input) at PlanDataUploadReviewScreen.pcf: line 60, column 51
    function value_43 () : java.lang.Boolean {
      return processor.ChkCommissionSubPlan
    }
    
    // 'value' attribute on CheckBoxInput (id=chkChargePattern_Input) at PlanDataUploadReviewScreen.pcf: line 65, column 47
    function value_49 () : java.lang.Boolean {
      return processor.ChkChargePattern
    }
    
    // 'value' attribute on CheckBoxInput (id=chkPaymentAllocationPlan_Input) at PlanDataUploadReviewScreen.pcf: line 70, column 55
    function value_55 () : java.lang.Boolean {
      return processor.ChkPaymentAllocationPlan
    }
    
    // 'value' attribute on CheckBoxInput (id=chkReturnPremiumPlan_Input) at PlanDataUploadReviewScreen.pcf: line 75, column 51
    function value_61 () : java.lang.Boolean {
      return processor.ChkReturnPremiumPlan
    }
    
    // 'value' attribute on CheckBoxInput (id=chkReturnPremiumScheme_Input) at PlanDataUploadReviewScreen.pcf: line 80, column 53
    function value_67 () : java.lang.Boolean {
      return processor.ChkReturnPremiumScheme
    }
    
    // 'value' attribute on CheckBoxInput (id=ChkAgencyBillPlan_Input) at PlanDataUploadReviewScreen.pcf: line 27, column 48
    function value_7 () : java.lang.Boolean {
      return processor.ChkAgencyBillPlan
    }
    
    // 'value' attribute on CheckBoxInput (id=chkGLTransactionFilter_Input) at PlanDataUploadReviewScreen.pcf: line 87, column 53
    function value_73 () : java.lang.Boolean {
      return processor.ChkGLTransactionFilter
    }
    
    // 'value' attribute on CheckBoxInput (id=chkGLTransactionNameMapping_Input) at PlanDataUploadReviewScreen.pcf: line 92, column 58
    function value_79 () : java.lang.Boolean {
      return processor.ChkGLTransactionNameMapping
    }
    
    // 'value' attribute on CheckBoxInput (id=chkGLTAccountNameMapping_Input) at PlanDataUploadReviewScreen.pcf: line 97, column 55
    function value_85 () : java.lang.Boolean {
      return processor.ChkGLTAccountNameMapping
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}