package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/AccountsGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AccountsGroupExpressions {
  @javax.annotation.Generated("config/web/pcf/account/AccountsGroup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AccountsGroupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'location' attribute on LocationGroup (id=AccountsGroup) at AccountsGroup.pcf: line 15, column 30
    function action_0 () : void {
      pcf.Accounts.go()
    }
    
    // 'location' attribute on LocationGroup (id=AccountsGroup) at AccountsGroup.pcf: line 15, column 30
    function action_dest_1 () : pcf.api.Destination {
      return pcf.Accounts.createDestination()
    }
    
    // LocationGroup (id=AccountsGroup) at AccountsGroup.pcf: line 11, column 26
    static function firstVisitableChildDestinationMethod_9 () : pcf.api.Destination {
      var dest : pcf.api.Destination
      dest = pcf.Accounts.createDestination()
      if (dest.canVisitSelf()) {
        return dest
      }
      return null
    }
    
    // 'infoBar' attribute on LocationGroup (id=AccountsGroup) at AccountsGroup.pcf: line 11, column 26
    function infoBar_onEnter_2 (def :  pcf.CurrentDateInfoBar) : void {
      def.onEnter()
    }
    
    // 'infoBar' attribute on LocationGroup (id=AccountsGroup) at AccountsGroup.pcf: line 11, column 26
    function infoBar_refreshVariables_3 (def :  pcf.CurrentDateInfoBar) : void {
      def.refreshVariables()
    }
    
    // 'menuActions' attribute on LocationGroup (id=AccountsGroup) at AccountsGroup.pcf: line 11, column 26
    function menuActions_onEnter_4 (def :  pcf.AccountsMenuActions) : void {
      def.onEnter()
    }
    
    // 'menuActions' attribute on LocationGroup (id=AccountsGroup) at AccountsGroup.pcf: line 11, column 26
    function menuActions_refreshVariables_5 (def :  pcf.AccountsMenuActions) : void {
      def.refreshVariables()
    }
    
    // 'parent' attribute on LocationGroup (id=AccountsGroup) at AccountsGroup.pcf: line 11, column 26
    static function parent_6 () : pcf.api.Destination {
      return pcf.AccountForward.createDestination()
    }
    
    // 'tabBar' attribute on LocationGroup (id=AccountsGroup) at AccountsGroup.pcf: line 11, column 26
    function tabBar_onEnter_7 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on LocationGroup (id=AccountsGroup) at AccountsGroup.pcf: line 11, column 26
    function tabBar_refreshVariables_8 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.AccountsGroup {
      return super.CurrentLocation as pcf.AccountsGroup
    }
    
    
  }
  
  
}