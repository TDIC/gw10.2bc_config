package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/acc/activityenhancement/desktop/ActivityLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ActivityLVExpressions {
  @javax.annotation.Generated("config/web/pcf/acc/activityenhancement/desktop/ActivityLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ActivityLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on IteratorSort at ActivityLV.pcf: line 31, column 24
    function sortBy_0 (activity :  Activity) : java.lang.Object {
      return activity.AssignmentDate
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at ActivityLV.pcf: line 47, column 42
    function sortValue_1 (activity :  Activity) : java.lang.Object {
      return activity.Escalated
    }
    
    // 'value' attribute on TextCell (id=producer_Cell) at ActivityLV.pcf: line 99, column 33
    function sortValue_10 (activity :  Activity) : java.lang.Object {
      return activity.Producer_Ext
    }
    
    // 'value' attribute on DateCell (id=CreateDate_Cell) at ActivityLV.pcf: line 51, column 40
    function sortValue_2 (activity :  Activity) : java.lang.Object {
      return activity.CreateTime
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at ActivityLV.pcf: line 57, column 40
    function sortValue_3 (activity :  Activity) : java.lang.Object {
      return activity.TargetDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at ActivityLV.pcf: line 62, column 33
    function sortValue_4 (activity :  Activity) : java.lang.Object {
      return activity.Priority
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at ActivityLV.pcf: line 67, column 39
    function sortValue_5 (activity :  Activity) : java.lang.Object {
      return activity.Status
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at ActivityLV.pcf: line 72, column 37
    function sortValue_6 (activity :  Activity) : java.lang.Object {
      return activity.Subject
    }
    
    // 'value' attribute on TextCell (id=TroubleTicketNumber_Cell) at ActivityLV.pcf: line 78, column 63
    function sortValue_7 (activity :  Activity) : java.lang.Object {
      return activity.TroubleTicket.TroubleTicketNumber
    }
    
    // 'value' attribute on TextCell (id=acctcell_Cell) at ActivityLV.pcf: line 85, column 32
    function sortValue_8 (activity :  Activity) : java.lang.Object {
      return activity.Account
    }
    
    // 'value' attribute on TextCell (id=policyperiod_Cell) at ActivityLV.pcf: line 92, column 37
    function sortValue_9 (activity :  Activity) : java.lang.Object {
      return activity.PolicyPeriod
    }
    
    // 'value' attribute on RowIterator at ActivityLV.pcf: line 20, column 50
    function value_66 () : java.util.List<Activity> {
      return activities
    }
    
    property get activities () : java.util.List<Activity> {
      return getRequireValue("activities", 0) as java.util.List<Activity>
    }
    
    property set activities ($arg :  java.util.List<Activity>) {
      setRequireValue("activities", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/acc/activityenhancement/desktop/ActivityLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ActivityLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'actionAvailable' attribute on TextCell (id=TroubleTicketNumber_Cell) at ActivityLV.pcf: line 78, column 63
    function actionAvailable_43 () : java.lang.Boolean {
      return activity.TroubleTicket != null
    }
    
    // 'actionAvailable' attribute on TextCell (id=acctcell_Cell) at ActivityLV.pcf: line 85, column 32
    function actionAvailable_49 () : java.lang.Boolean {
      return activity.Account != null
    }
    
    // 'actionAvailable' attribute on TextCell (id=policyperiod_Cell) at ActivityLV.pcf: line 92, column 37
    function actionAvailable_55 () : java.lang.Boolean {
      return activity.PolicyPeriod != null
    }
    
    // 'actionAvailable' attribute on TextCell (id=producer_Cell) at ActivityLV.pcf: line 99, column 33
    function actionAvailable_61 () : java.lang.Boolean {
      return activity.Producer_Ext != null
    }
    
    // 'action' attribute on TextCell (id=Subject_Cell) at ActivityLV.pcf: line 72, column 37
    function action_36 () : void {
      ActivityDetailWorksheet.goInWorkspace(activity, false, false)
    }
    
    // 'action' attribute on TextCell (id=TroubleTicketNumber_Cell) at ActivityLV.pcf: line 78, column 63
    function action_41 () : void {
      TroubleTicketDetailsPopup.push(activity.TroubleTicket)
    }
    
    // 'action' attribute on TextCell (id=acctcell_Cell) at ActivityLV.pcf: line 85, column 32
    function action_47 () : void {
      AccountDetailSummary.push(activity.Account)
    }
    
    // 'action' attribute on TextCell (id=policyperiod_Cell) at ActivityLV.pcf: line 92, column 37
    function action_53 () : void {
      PolicyDetailSummaryPopup.push(activity.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=producer_Cell) at ActivityLV.pcf: line 99, column 33
    function action_59 () : void {
      ProducerDetailPopup.push(activity.Producer_Ext)
    }
    
    // 'action' attribute on TextCell (id=Subject_Cell) at ActivityLV.pcf: line 72, column 37
    function action_dest_37 () : pcf.api.Destination {
      return pcf.ActivityDetailWorksheet.createDestination(activity, false, false)
    }
    
    // 'action' attribute on TextCell (id=TroubleTicketNumber_Cell) at ActivityLV.pcf: line 78, column 63
    function action_dest_42 () : pcf.api.Destination {
      return pcf.TroubleTicketDetailsPopup.createDestination(activity.TroubleTicket)
    }
    
    // 'action' attribute on TextCell (id=acctcell_Cell) at ActivityLV.pcf: line 85, column 32
    function action_dest_48 () : pcf.api.Destination {
      return pcf.AccountDetailSummary.createDestination(activity.Account)
    }
    
    // 'action' attribute on TextCell (id=policyperiod_Cell) at ActivityLV.pcf: line 92, column 37
    function action_dest_54 () : pcf.api.Destination {
      return pcf.PolicyDetailSummaryPopup.createDestination(activity.PolicyPeriod)
    }
    
    // 'action' attribute on TextCell (id=producer_Cell) at ActivityLV.pcf: line 99, column 33
    function action_dest_60 () : pcf.api.Destination {
      return pcf.ProducerDetailPopup.createDestination(activity.Producer_Ext)
    }
    
    // 'checkBoxVisible' attribute on RowIterator at ActivityLV.pcf: line 20, column 50
    function checkBoxVisible_65 () : java.lang.Boolean {
      return perm.System.actquepick_Ext
    }
    
    // 'condition' attribute on ToolbarFlag at ActivityLV.pcf: line 25, column 35
    function condition_11 () : java.lang.Boolean {
      return activity.canAssign() and !(activity typeis SharedActivity)
    }
    
    // 'condition' attribute on ToolbarFlag at ActivityLV.pcf: line 28, column 37
    function condition_12 () : java.lang.Boolean {
      return activity.canComplete()
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at ActivityLV.pcf: line 57, column 40
    function fontColor_22 () : java.lang.Object {
      return activity.Overdue == true ? "Red" : ""
    }
    
    // 'value' attribute on BooleanRadioCell (id=UpdatedSinceLastViewed_Cell) at ActivityLV.pcf: line 40, column 42
    function valueRoot_14 () : java.lang.Object {
      return activity
    }
    
    // 'value' attribute on TextCell (id=TroubleTicketNumber_Cell) at ActivityLV.pcf: line 78, column 63
    function valueRoot_45 () : java.lang.Object {
      return activity.TroubleTicket
    }
    
    // 'value' attribute on BooleanRadioCell (id=UpdatedSinceLastViewed_Cell) at ActivityLV.pcf: line 40, column 42
    function value_13 () : java.lang.Boolean {
      return activity.UpdatedSinceLastViewed
    }
    
    // 'value' attribute on BooleanRadioCell (id=Escalated_Cell) at ActivityLV.pcf: line 47, column 42
    function value_16 () : java.lang.Boolean {
      return activity.Escalated
    }
    
    // 'value' attribute on DateCell (id=CreateDate_Cell) at ActivityLV.pcf: line 51, column 40
    function value_19 () : java.util.Date {
      return activity.CreateTime
    }
    
    // 'value' attribute on DateCell (id=DueDate_Cell) at ActivityLV.pcf: line 57, column 40
    function value_23 () : java.util.Date {
      return activity.TargetDate
    }
    
    // 'value' attribute on TypeKeyCell (id=Priority_Cell) at ActivityLV.pcf: line 62, column 33
    function value_28 () : Priority {
      return activity.Priority
    }
    
    // 'value' attribute on TypeKeyCell (id=Status_Cell) at ActivityLV.pcf: line 67, column 39
    function value_32 () : ActivityStatus {
      return activity.Status
    }
    
    // 'value' attribute on TextCell (id=Subject_Cell) at ActivityLV.pcf: line 72, column 37
    function value_38 () : java.lang.String {
      return activity.Subject
    }
    
    // 'value' attribute on TextCell (id=TroubleTicketNumber_Cell) at ActivityLV.pcf: line 78, column 63
    function value_44 () : java.lang.String {
      return activity.TroubleTicket.TroubleTicketNumber
    }
    
    // 'value' attribute on TextCell (id=acctcell_Cell) at ActivityLV.pcf: line 85, column 32
    function value_50 () : Account {
      return activity.Account
    }
    
    // 'value' attribute on TextCell (id=policyperiod_Cell) at ActivityLV.pcf: line 92, column 37
    function value_56 () : PolicyPeriod {
      return activity.PolicyPeriod
    }
    
    // 'value' attribute on TextCell (id=producer_Cell) at ActivityLV.pcf: line 99, column 33
    function value_62 () : Producer {
      return activity.Producer_Ext
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at ActivityLV.pcf: line 57, column 40
    function verifyFontColorIsAllowedType_25 ($$arg :  gw.api.web.color.GWColor) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at ActivityLV.pcf: line 57, column 40
    function verifyFontColorIsAllowedType_25 ($$arg :  java.lang.String) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'fontColor' attribute on DateCell (id=DueDate_Cell) at ActivityLV.pcf: line 57, column 40
    function verifyFontColor_26 () : void {
      var __fontColorArg = activity.Overdue == true ? "Red" : ""
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the fontColor return type is not a valid type for use with a ValueWidget's fontColor
      // It needs to return a String or a GWColor.
      verifyFontColorIsAllowedType_25(__fontColorArg)
    }
    
    // 'valueType' attribute on TypeKeyCell (id=Priority_Cell) at ActivityLV.pcf: line 62, column 33
    function verifyValueType_31 () : void {
      var __valueTypeArg : Priority
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : gw.entity.TypeKey = __valueTypeArg
    }
    
    // 'valueType' attribute on TypeKeyCell (id=Status_Cell) at ActivityLV.pcf: line 67, column 39
    function verifyValueType_35 () : void {
      var __valueTypeArg : ActivityStatus
      // If this assignment statement fails to compile, that means that the declared valueType
      // is not a valid type for use with this Input or Cell type
      var __requiredTypeArg : gw.entity.TypeKey = __valueTypeArg
    }
    
    property get activity () : Activity {
      return getIteratedValue(1) as Activity
    }
    
    
  }
  
  
}