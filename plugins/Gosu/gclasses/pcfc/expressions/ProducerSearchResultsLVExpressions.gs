package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
uses gw.api.address.AddressFormatter
@javax.annotation.Generated("config/web/pcf/search/ProducerSearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ProducerSearchResultsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/ProducerSearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends ProducerSearchResultsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at ProducerSearchResultsLV.pcf: line 61, column 117
    function action_10 () : void {
      ProducerPolicies.go(producerSearchView.Producer)
    }
    
    // 'action' attribute on Link (id=Select) at ProducerSearchResultsLV.pcf: line 50, column 38
    function action_7 () : void {
      tAccountOwnerReference.TAccountOwner = producerSearchView.Producer; (CurrentLocation as pcf.api.Wizard).next();
    }
    
    // 'action' attribute on TextCell (id=Name_Cell) at ProducerSearchResultsLV.pcf: line 61, column 117
    function action_dest_11 () : pcf.api.Destination {
      return pcf.ProducerPolicies.createDestination(producerSearchView.Producer)
    }
    
    // 'available' attribute on TextCell (id=Name_Cell) at ProducerSearchResultsLV.pcf: line 61, column 117
    function available_9 () : java.lang.Boolean {
      return showHyperlinks
    }
    
    // 'canPick' attribute on RowIterator at ProducerSearchResultsLV.pcf: line 36, column 85
    function canPick_21 () : java.lang.Boolean {
      return !isWizard
    }
    
    // 'checkBoxVisible' attribute on RowIterator at ProducerSearchResultsLV.pcf: line 36, column 85
    function checkBoxVisible_22 () : java.lang.Boolean {
      return showCheckboxes
    }
    
    // 'pickValue' attribute on RowIterator at ProducerSearchResultsLV.pcf: line 36, column 85
    function pickValue_23 () : java.lang.Object {
      return gw.api.web.search.SearchPopupUtil.getProducer(producerSearchView)
    }
    
    // 'value' attribute on TypeKeyCell (id=Currency_Cell) at ProducerSearchResultsLV.pcf: line 67, column 69
    function valueRoot_16 () : java.lang.Object {
      return producerSearchView
    }
    
    // 'value' attribute on TextCell (id=Name_Cell) at ProducerSearchResultsLV.pcf: line 61, column 117
    function value_12 () : java.lang.String {
      return producerSearchView.NameKanji.HasContent ? producerSearchView.NameKanji : producerSearchView.Name
    }
    
    // 'value' attribute on TypeKeyCell (id=Currency_Cell) at ProducerSearchResultsLV.pcf: line 67, column 69
    function value_15 () : typekey.Currency {
      return producerSearchView.Currency
    }
    
    // 'value' attribute on TextCell (id=Address_Cell) at ProducerSearchResultsLV.pcf: line 73, column 57
    function value_19 () : java.lang.String {
      return formattedAddress(producerSearchView)
    }
    
    // 'visible' attribute on TypeKeyCell (id=Currency_Cell) at ProducerSearchResultsLV.pcf: line 67, column 69
    function visible_17 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on LinkCell (id=SelectCell) at ProducerSearchResultsLV.pcf: line 45, column 50
    function visible_8 () : java.lang.Boolean {
      return isWizard and !showCheckboxes
    }
    
    property get producerSearchView () : entity.ProducerSearchView {
      return getIteratedValue(1) as entity.ProducerSearchView
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/search/ProducerSearchResultsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ProducerSearchResultsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'sortBy' attribute on TextCell (id=Name_Cell) at ProducerSearchResultsLV.pcf: line 61, column 117
    function sortValue_1 (producerSearchView :  entity.ProducerSearchView) : java.lang.Object {
      return producerSearchView.Name
    }
    
    // 'value' attribute on TypeKeyCell (id=Currency_Cell) at ProducerSearchResultsLV.pcf: line 67, column 69
    function sortValue_2 (producerSearchView :  entity.ProducerSearchView) : java.lang.Object {
      return producerSearchView.Currency
    }
    
    // 'sortBy' attribute on TextCell (id=Address_Cell) at ProducerSearchResultsLV.pcf: line 73, column 57
    function sortValue_4 (producerSearchView :  entity.ProducerSearchView) : java.lang.Object {
      return producerSearchView.Country
    }
    
    // 'sortBy' attribute on TextCell (id=Address_Cell) at ProducerSearchResultsLV.pcf: line 73, column 57
    function sortValue_5 (producerSearchView :  entity.ProducerSearchView) : java.lang.Object {
      return  producerSearchView.State
    }
    
    // 'sortBy' attribute on TextCell (id=Address_Cell) at ProducerSearchResultsLV.pcf: line 73, column 57
    function sortValue_6 (producerSearchView :  entity.ProducerSearchView) : java.lang.Object {
      return  producerSearchView.City
    }
    
    // 'value' attribute on RowIterator at ProducerSearchResultsLV.pcf: line 36, column 85
    function value_24 () : gw.api.database.IQueryBeanResult<entity.ProducerSearchView> {
      return producerSearchViews
    }
    
    // 'visible' attribute on LinkCell (id=SelectCell) at ProducerSearchResultsLV.pcf: line 45, column 50
    function visible_0 () : java.lang.Boolean {
      return isWizard and !showCheckboxes
    }
    
    // 'visible' attribute on TypeKeyCell (id=Currency_Cell) at ProducerSearchResultsLV.pcf: line 67, column 69
    function visible_3 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    property get isWizard () : boolean {
      return getRequireValue("isWizard", 0) as java.lang.Boolean
    }
    
    property set isWizard ($arg :  boolean) {
      setRequireValue("isWizard", 0, $arg)
    }
    
    property get producerSearchViews () : gw.api.database.IQueryBeanResult<ProducerSearchView> {
      return getRequireValue("producerSearchViews", 0) as gw.api.database.IQueryBeanResult<ProducerSearchView>
    }
    
    property set producerSearchViews ($arg :  gw.api.database.IQueryBeanResult<ProducerSearchView>) {
      setRequireValue("producerSearchViews", 0, $arg)
    }
    
    property get showCheckboxes () : Boolean {
      return getRequireValue("showCheckboxes", 0) as Boolean
    }
    
    property set showCheckboxes ($arg :  Boolean) {
      setRequireValue("showCheckboxes", 0, $arg)
    }
    
    property get showHyperlinks () : boolean {
      return getRequireValue("showHyperlinks", 0) as java.lang.Boolean
    }
    
    property set showHyperlinks ($arg :  boolean) {
      setRequireValue("showHyperlinks", 0, $arg)
    }
    
    property get tAccountOwnerReference () : gw.api.web.accounting.TAccountOwnerReference {
      return getRequireValue("tAccountOwnerReference", 0) as gw.api.web.accounting.TAccountOwnerReference
    }
    
    property set tAccountOwnerReference ($arg :  gw.api.web.accounting.TAccountOwnerReference) {
      setRequireValue("tAccountOwnerReference", 0, $arg)
    }
    
    
    function formattedAddress(producerSearchView : ProducerSearchView) : String {
      var af = new AddressFormatter() { :Country = producerSearchView.Country, :State = producerSearchView.State, :City = producerSearchView.City, :CityKanji = producerSearchView.CityKanji,
                      :AddressLine1 = producerSearchView.AddressLine1, :AddressLine1Kanji = producerSearchView.AddressLine1Kanji, :PostalCode = producerSearchView.PostalCode }
      return af.format(af, ", ")
    }
    
    
  }
  
  
}