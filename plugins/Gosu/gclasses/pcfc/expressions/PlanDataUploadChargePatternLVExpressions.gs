package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadChargePatternLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanDataUploadChargePatternLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadChargePatternLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends PlanDataUploadChargePatternLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at PlanDataUploadChargePatternLV.pcf: line 17, column 106
    function highlighted_92 () : java.lang.Boolean {
      return (chargePattern.Error or chargePattern.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadChargePatternLV.pcf: line 23, column 46
    function label_27 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at PlanDataUploadChargePatternLV.pcf: line 28, column 41
    function label_32 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.PublicID")
    }
    
    // 'label' attribute on TextCell (id=chargeCode_Cell) at PlanDataUploadChargePatternLV.pcf: line 33, column 41
    function label_37 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.ChargeCode")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadChargePatternLV.pcf: line 39, column 41
    function label_42 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.Name")
    }
    
    // 'label' attribute on TextCell (id=chargeType_Cell) at PlanDataUploadChargePatternLV.pcf: line 44, column 41
    function label_47 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.ChargeType")
    }
    
    // 'label' attribute on TypeKeyCell (id=Periodicity_Cell) at PlanDataUploadChargePatternLV.pcf: line 49, column 44
    function label_52 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.Frequency")
    }
    
    // 'label' attribute on TypeKeyCell (id=category_Cell) at PlanDataUploadChargePatternLV.pcf: line 54, column 47
    function label_57 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.Category")
    }
    
    // 'label' attribute on TextCell (id=tAccount_Owner_Cell) at PlanDataUploadChargePatternLV.pcf: line 59, column 41
    function label_62 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.TAccountOwner")
    }
    
    // 'label' attribute on TypeKeyCell (id=invoicingApproach_Cell) at PlanDataUploadChargePatternLV.pcf: line 64, column 49
    function label_67 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.InvoicingApproach")
    }
    
    // 'label' attribute on TypeKeyCell (id=priority_Cell) at PlanDataUploadChargePatternLV.pcf: line 69, column 47
    function label_72 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.Priority")
    }
    
    // 'label' attribute on TypeKeyCell (id=printPriority_Cell) at PlanDataUploadChargePatternLV.pcf: line 74, column 57
    function label_77 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.Web.ChargePatternsLV.PrintPriority")
    }
    
    // 'label' attribute on BooleanRadioCell (id=include_In_Equity_Dating_Cell) at PlanDataUploadChargePatternLV.pcf: line 79, column 42
    function label_82 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.IncludeInEquityDating")
    }
    
    // 'label' attribute on BooleanRadioCell (id=Exclude_Cell) at PlanDataUploadChargePatternLV.pcf: line 84, column 42
    function label_87 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadChargePatternLV.pcf: line 28, column 41
    function valueRoot_34 () : java.lang.Object {
      return chargePattern
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadChargePatternLV.pcf: line 23, column 46
    function value_28 () : java.lang.String {
      return processor.getLoadStatus(chargePattern)
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadChargePatternLV.pcf: line 28, column 41
    function value_33 () : java.lang.String {
      return chargePattern.PublicID
    }
    
    // 'value' attribute on TextCell (id=chargeCode_Cell) at PlanDataUploadChargePatternLV.pcf: line 33, column 41
    function value_38 () : java.lang.String {
      return chargePattern.ChargeCode
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadChargePatternLV.pcf: line 39, column 41
    function value_43 () : java.lang.String {
      return chargePattern.ChargeName
    }
    
    // 'value' attribute on TextCell (id=chargeType_Cell) at PlanDataUploadChargePatternLV.pcf: line 44, column 41
    function value_48 () : java.lang.String {
      return chargePattern.ChargeType
    }
    
    // 'value' attribute on TypeKeyCell (id=Periodicity_Cell) at PlanDataUploadChargePatternLV.pcf: line 49, column 44
    function value_53 () : typekey.Periodicity {
      return chargePattern.Periodicity
    }
    
    // 'value' attribute on TypeKeyCell (id=category_Cell) at PlanDataUploadChargePatternLV.pcf: line 54, column 47
    function value_58 () : typekey.ChargeCategory {
      return chargePattern.Category
    }
    
    // 'value' attribute on TextCell (id=tAccount_Owner_Cell) at PlanDataUploadChargePatternLV.pcf: line 59, column 41
    function value_63 () : java.lang.String {
      return chargePattern.TAccount_Owner
    }
    
    // 'value' attribute on TypeKeyCell (id=invoicingApproach_Cell) at PlanDataUploadChargePatternLV.pcf: line 64, column 49
    function value_68 () : typekey.InvoiceTreatment {
      return chargePattern.InvoicingApproach
    }
    
    // 'value' attribute on TypeKeyCell (id=priority_Cell) at PlanDataUploadChargePatternLV.pcf: line 69, column 47
    function value_73 () : typekey.ChargePriority {
      return chargePattern.Priority
    }
    
    // 'value' attribute on TypeKeyCell (id=printPriority_Cell) at PlanDataUploadChargePatternLV.pcf: line 74, column 57
    function value_78 () : typekey.ChargePrintPriority_TDIC {
      return chargePattern.PrintPriority
    }
    
    // 'value' attribute on BooleanRadioCell (id=include_In_Equity_Dating_Cell) at PlanDataUploadChargePatternLV.pcf: line 79, column 42
    function value_83 () : java.lang.Boolean {
      return chargePattern.Include_In_Equity_Dating
    }
    
    // 'value' attribute on BooleanRadioCell (id=Exclude_Cell) at PlanDataUploadChargePatternLV.pcf: line 84, column 42
    function value_88 () : java.lang.Boolean {
      return chargePattern.Exclude
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadChargePatternLV.pcf: line 23, column 46
    function visible_29 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get chargePattern () : tdic.util.dataloader.data.plandata.ChargePatternData {
      return getIteratedValue(1) as tdic.util.dataloader.data.plandata.ChargePatternData
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadChargePatternLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanDataUploadChargePatternLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at PlanDataUploadChargePatternLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TypeKeyCell (id=Periodicity_Cell) at PlanDataUploadChargePatternLV.pcf: line 49, column 44
    function label_11 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.Frequency")
    }
    
    // 'label' attribute on TypeKeyCell (id=category_Cell) at PlanDataUploadChargePatternLV.pcf: line 54, column 47
    function label_13 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.Category")
    }
    
    // 'label' attribute on TextCell (id=tAccount_Owner_Cell) at PlanDataUploadChargePatternLV.pcf: line 59, column 41
    function label_15 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.TAccountOwner")
    }
    
    // 'label' attribute on TypeKeyCell (id=invoicingApproach_Cell) at PlanDataUploadChargePatternLV.pcf: line 64, column 49
    function label_17 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.InvoicingApproach")
    }
    
    // 'label' attribute on TypeKeyCell (id=priority_Cell) at PlanDataUploadChargePatternLV.pcf: line 69, column 47
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.Priority")
    }
    
    // 'label' attribute on TypeKeyCell (id=printPriority_Cell) at PlanDataUploadChargePatternLV.pcf: line 74, column 57
    function label_21 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.Web.ChargePatternsLV.PrintPriority")
    }
    
    // 'label' attribute on BooleanRadioCell (id=include_In_Equity_Dating_Cell) at PlanDataUploadChargePatternLV.pcf: line 79, column 42
    function label_23 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.IncludeInEquityDating")
    }
    
    // 'label' attribute on BooleanRadioCell (id=Exclude_Cell) at PlanDataUploadChargePatternLV.pcf: line 84, column 42
    function label_25 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.Exclude")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at PlanDataUploadChargePatternLV.pcf: line 28, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.PublicID")
    }
    
    // 'label' attribute on TextCell (id=chargeCode_Cell) at PlanDataUploadChargePatternLV.pcf: line 33, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.ChargeCode")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at PlanDataUploadChargePatternLV.pcf: line 39, column 41
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.Name")
    }
    
    // 'label' attribute on TextCell (id=chargeType_Cell) at PlanDataUploadChargePatternLV.pcf: line 44, column 41
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePattern.ChargeType")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at PlanDataUploadChargePatternLV.pcf: line 23, column 46
    function sortValue_1 (chargePattern :  tdic.util.dataloader.data.plandata.ChargePatternData) : java.lang.Object {
      return processor.getLoadStatus(chargePattern)
    }
    
    // 'value' attribute on TextCell (id=chargeType_Cell) at PlanDataUploadChargePatternLV.pcf: line 44, column 41
    function sortValue_10 (chargePattern :  tdic.util.dataloader.data.plandata.ChargePatternData) : java.lang.Object {
      return chargePattern.ChargeType
    }
    
    // 'value' attribute on TypeKeyCell (id=Periodicity_Cell) at PlanDataUploadChargePatternLV.pcf: line 49, column 44
    function sortValue_12 (chargePattern :  tdic.util.dataloader.data.plandata.ChargePatternData) : java.lang.Object {
      return chargePattern.Periodicity
    }
    
    // 'value' attribute on TypeKeyCell (id=category_Cell) at PlanDataUploadChargePatternLV.pcf: line 54, column 47
    function sortValue_14 (chargePattern :  tdic.util.dataloader.data.plandata.ChargePatternData) : java.lang.Object {
      return chargePattern.Category
    }
    
    // 'value' attribute on TextCell (id=tAccount_Owner_Cell) at PlanDataUploadChargePatternLV.pcf: line 59, column 41
    function sortValue_16 (chargePattern :  tdic.util.dataloader.data.plandata.ChargePatternData) : java.lang.Object {
      return chargePattern.TAccount_Owner
    }
    
    // 'value' attribute on TypeKeyCell (id=invoicingApproach_Cell) at PlanDataUploadChargePatternLV.pcf: line 64, column 49
    function sortValue_18 (chargePattern :  tdic.util.dataloader.data.plandata.ChargePatternData) : java.lang.Object {
      return chargePattern.InvoicingApproach
    }
    
    // 'value' attribute on TypeKeyCell (id=priority_Cell) at PlanDataUploadChargePatternLV.pcf: line 69, column 47
    function sortValue_20 (chargePattern :  tdic.util.dataloader.data.plandata.ChargePatternData) : java.lang.Object {
      return chargePattern.Priority
    }
    
    // 'value' attribute on TypeKeyCell (id=printPriority_Cell) at PlanDataUploadChargePatternLV.pcf: line 74, column 57
    function sortValue_22 (chargePattern :  tdic.util.dataloader.data.plandata.ChargePatternData) : java.lang.Object {
      return chargePattern.PrintPriority
    }
    
    // 'value' attribute on BooleanRadioCell (id=include_In_Equity_Dating_Cell) at PlanDataUploadChargePatternLV.pcf: line 79, column 42
    function sortValue_24 (chargePattern :  tdic.util.dataloader.data.plandata.ChargePatternData) : java.lang.Object {
      return chargePattern.Include_In_Equity_Dating
    }
    
    // 'value' attribute on BooleanRadioCell (id=Exclude_Cell) at PlanDataUploadChargePatternLV.pcf: line 84, column 42
    function sortValue_26 (chargePattern :  tdic.util.dataloader.data.plandata.ChargePatternData) : java.lang.Object {
      return chargePattern.Exclude
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at PlanDataUploadChargePatternLV.pcf: line 28, column 41
    function sortValue_4 (chargePattern :  tdic.util.dataloader.data.plandata.ChargePatternData) : java.lang.Object {
      return chargePattern.PublicID
    }
    
    // 'value' attribute on TextCell (id=chargeCode_Cell) at PlanDataUploadChargePatternLV.pcf: line 33, column 41
    function sortValue_6 (chargePattern :  tdic.util.dataloader.data.plandata.ChargePatternData) : java.lang.Object {
      return chargePattern.ChargeCode
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at PlanDataUploadChargePatternLV.pcf: line 39, column 41
    function sortValue_8 (chargePattern :  tdic.util.dataloader.data.plandata.ChargePatternData) : java.lang.Object {
      return chargePattern.ChargeName
    }
    
    // 'value' attribute on RowIterator (id=ChargePattern) at PlanDataUploadChargePatternLV.pcf: line 15, column 99
    function value_93 () : java.util.ArrayList<tdic.util.dataloader.data.plandata.ChargePatternData> {
      return processor.ChargePatternArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at PlanDataUploadChargePatternLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  
}