package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyAddChargesListDetailPanel.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyAddChargesListDetailPanelExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyAddChargesListDetailPanel.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyAddChargesListDetailPanelExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at PolicyAddChargesListDetailPanel.pcf: line 19, column 101
    function def_onEnter_0 (def :  pcf.PolicyAddChargesLV) : void {
      def.onEnter(billingInstruction, policyPeriod, chargeToInvoicingOverridesViewMap)
    }
    
    // 'def' attribute on PanelRef at PolicyAddChargesListDetailPanel.pcf: line 31, column 59
    function def_onEnter_2 (def :  pcf.ChargeBreakdownItemsLV) : void {
      def.onEnter(chargeInitializer)
    }
    
    // 'def' attribute on PanelRef at PolicyAddChargesListDetailPanel.pcf: line 19, column 101
    function def_refreshVariables_1 (def :  pcf.PolicyAddChargesLV) : void {
      def.refreshVariables(billingInstruction, policyPeriod, chargeToInvoicingOverridesViewMap)
    }
    
    // 'def' attribute on PanelRef at PolicyAddChargesListDetailPanel.pcf: line 31, column 59
    function def_refreshVariables_3 (def :  pcf.ChargeBreakdownItemsLV) : void {
      def.refreshVariables(chargeInitializer)
    }
    
    property get billingInstruction () : PlcyBillingInstruction {
      return getRequireValue("billingInstruction", 0) as PlcyBillingInstruction
    }
    
    property set billingInstruction ($arg :  PlcyBillingInstruction) {
      setRequireValue("billingInstruction", 0, $arg)
    }
    
    property get chargeInitializer () : gw.api.domain.charge.ChargeInitializer {
      return getCurrentSelection(0) as gw.api.domain.charge.ChargeInitializer
    }
    
    property set chargeInitializer ($arg :  gw.api.domain.charge.ChargeInitializer) {
      setCurrentSelection(0, $arg)
    }
    
    property get chargeToInvoicingOverridesViewMap () : java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView> {
      return getRequireValue("chargeToInvoicingOverridesViewMap", 0) as java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>
    }
    
    property set chargeToInvoicingOverridesViewMap ($arg :  java.util.HashMap<gw.api.domain.charge.ChargeInitializer, gw.invoice.InvoicingOverridesView>) {
      setRequireValue("chargeToInvoicingOverridesViewMap", 0, $arg)
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}