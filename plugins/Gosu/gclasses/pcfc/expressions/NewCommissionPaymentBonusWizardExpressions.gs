package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentBonusWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewCommissionPaymentBonusWizardExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/NewCommissionPaymentBonusWizard.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewCommissionPaymentBonusWizardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (producer :  Producer) : int {
      return 0
    }
    
    // 'beforeCommit' attribute on Wizard (id=NewCommissionPaymentBonusWizard) at NewCommissionPaymentBonusWizard.pcf: line 9, column 42
    function beforeCommit_6 (pickedValue :  java.lang.Object) : void {
      gw.api.web.producer.ProducerUtil.schedule(bonusPayment)
    }
    
    // 'initialValue' attribute on Variable at NewCommissionPaymentBonusWizard.pcf: line 18, column 39
    function initialValue_0 () : entity.BonusCmsnPayment {
      return newBonusPayment()
    }
    
    // 'onExit' attribute on WizardStep (id=DetailsStep) at NewCommissionPaymentBonusWizard.pcf: line 24, column 94
    function onExit_1 () : void {
      bonusPayment.initiateApprovalActivityIfUserLacksAuthority()
    }
    
    // 'screen' attribute on WizardStep (id=DetailsStep) at NewCommissionPaymentBonusWizard.pcf: line 24, column 94
    function screen_onEnter_2 (def :  pcf.NewCommissionPaymentBonusDetailsScreen) : void {
      def.onEnter(bonusPayment, producer)
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at NewCommissionPaymentBonusWizard.pcf: line 30, column 99
    function screen_onEnter_4 (def :  pcf.NewCommissionPaymentBonusConfirmationScreen) : void {
      def.onEnter(bonusPayment, producer)
    }
    
    // 'screen' attribute on WizardStep (id=DetailsStep) at NewCommissionPaymentBonusWizard.pcf: line 24, column 94
    function screen_refreshVariables_3 (def :  pcf.NewCommissionPaymentBonusDetailsScreen) : void {
      def.refreshVariables(bonusPayment, producer)
    }
    
    // 'screen' attribute on WizardStep (id=ConfirmationStep) at NewCommissionPaymentBonusWizard.pcf: line 30, column 99
    function screen_refreshVariables_5 (def :  pcf.NewCommissionPaymentBonusConfirmationScreen) : void {
      def.refreshVariables(bonusPayment, producer)
    }
    
    // 'tabBar' attribute on Wizard (id=NewCommissionPaymentBonusWizard) at NewCommissionPaymentBonusWizard.pcf: line 9, column 42
    function tabBar_onEnter_7 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on Wizard (id=NewCommissionPaymentBonusWizard) at NewCommissionPaymentBonusWizard.pcf: line 9, column 42
    function tabBar_refreshVariables_8 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.NewCommissionPaymentBonusWizard {
      return super.CurrentLocation as pcf.NewCommissionPaymentBonusWizard
    }
    
    property get bonusPayment () : entity.BonusCmsnPayment {
      return getVariableValue("bonusPayment", 0) as entity.BonusCmsnPayment
    }
    
    property set bonusPayment ($arg :  entity.BonusCmsnPayment) {
      setVariableValue("bonusPayment", 0, $arg)
    }
    
    property get producer () : Producer {
      return getVariableValue("producer", 0) as Producer
    }
    
    property set producer ($arg :  Producer) {
      setVariableValue("producer", 0, $arg)
    }
    
    function newBonusPayment() : BonusCmsnPayment {
            var bcp = new BonusCmsnPayment(producer.Currency)
            bcp.Producer = producer;
            bcp.PaymentTime = TC_IMMEDIATELY;
            return bcp;
          }
    
    
  }
  
  
}