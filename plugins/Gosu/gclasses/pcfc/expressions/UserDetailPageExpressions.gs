package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/users/UserDetailPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class UserDetailPageExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/users/UserDetailPage.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class UserDetailPageExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (User :  User) : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=UserDetailPage) at UserDetailPage.pcf: line 13, column 80
    function afterCancel_2 () : void {
      UserDetailPage.go(User)
    }
    
    // 'afterCancel' attribute on Page (id=UserDetailPage) at UserDetailPage.pcf: line 13, column 80
    function afterCancel_dest_3 () : pcf.api.Destination {
      return pcf.UserDetailPage.createDestination(User)
    }
    
    // 'afterCommit' attribute on Page (id=UserDetailPage) at UserDetailPage.pcf: line 13, column 80
    function afterCommit_4 (TopLocation :  pcf.api.Location) : void {
      UserDetailPage.go(User)
    }
    
    // 'canEdit' attribute on Page (id=UserDetailPage) at UserDetailPage.pcf: line 13, column 80
    function canEdit_5 () : java.lang.Boolean {
      return perm.User.edit and perm.System.useradmin
    }
    
    // 'canVisit' attribute on Page (id=UserDetailPage) at UserDetailPage.pcf: line 13, column 80
    static function canVisit_6 (User :  User) : java.lang.Boolean {
      return perm.User.view
    }
    
    // 'def' attribute on ScreenRef at UserDetailPage.pcf: line 20, column 37
    function def_onEnter_0 (def :  pcf.UserDetailScreen) : void {
      def.onEnter(User)
    }
    
    // 'def' attribute on ScreenRef at UserDetailPage.pcf: line 20, column 37
    function def_refreshVariables_1 (def :  pcf.UserDetailScreen) : void {
      def.refreshVariables(User)
    }
    
    // 'parent' attribute on Page (id=UserDetailPage) at UserDetailPage.pcf: line 13, column 80
    static function parent_7 (User :  User) : pcf.api.Destination {
      return pcf.Admin.createDestination()
    }
    
    // 'title' attribute on Page (id=UserDetailPage) at UserDetailPage.pcf: line 13, column 80
    static function title_8 (User :  User) : java.lang.Object {
      return DisplayKey.get("Web.Admin.UserDetail", User.DisplayName)
    }
    
    override property get CurrentLocation () : pcf.UserDetailPage {
      return super.CurrentLocation as pcf.UserDetailPage
    }
    
    property get User () : User {
      return getVariableValue("User", 0) as User
    }
    
    property set User ($arg :  User) {
      setVariableValue("User", 0, $arg)
    }
    
    
  }
  
  
}