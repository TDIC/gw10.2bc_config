package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/contact/ContactSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class ContactSearchPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/contact/ContactSearchPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class ContactSearchPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (isProducerContact :  boolean) : int {
      return 0
    }
    
    // 'def' attribute on ScreenRef at ContactSearchPopup.pcf: line 15, column 53
    function def_onEnter_0 (def :  pcf.ContactSearchScreen) : void {
      def.onEnter(isProducerContact)
    }
    
    // 'def' attribute on ScreenRef at ContactSearchPopup.pcf: line 15, column 53
    function def_refreshVariables_1 (def :  pcf.ContactSearchScreen) : void {
      def.refreshVariables(isProducerContact)
    }
    
    override property get CurrentLocation () : pcf.ContactSearchPopup {
      return super.CurrentLocation as pcf.ContactSearchPopup
    }
    
    property get isProducerContact () : boolean {
      return getVariableValue("isProducerContact", 0) as java.lang.Boolean
    }
    
    property set isProducerContact ($arg :  boolean) {
      setVariableValue("isProducerContact", 0, $arg)
    }
    
    
  }
  
  
}