package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class BillingPlanDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/billing/BillingPlanDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class BillingPlanDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on InputSetRef at BillingPlanDetailDV.pcf: line 56, column 55
    function def_onEnter_35 (def :  pcf.PlanMultiCurrencyInputSet) : void {
      def.onEnter(billingPlan)
    }
    
    // 'def' attribute on InputSetRef at BillingPlanDetailDV.pcf: line 63, column 79
    function def_onEnter_37 (def :  pcf.BillingPlanPaymentDueIntervalInputSet_default) : void {
      def.onEnter(billingPlan, planNotInUse)
    }
    
    // 'def' attribute on InputSetRef at BillingPlanDetailDV.pcf: line 69, column 79
    function def_onEnter_40 (def :  pcf.BillingPlanFeeHandlingInputSet_default) : void {
      def.onEnter(billingPlan, planNotInUse)
    }
    
    // 'def' attribute on InputSetRef at BillingPlanDetailDV.pcf: line 85, column 79
    function def_onEnter_49 (def :  pcf.BillingPlanLowBalanceInvoicesInputSet_default) : void {
      def.onEnter(billingPlan, planNotInUse)
    }
    
    // 'def' attribute on InputSetRef (id=DisbursementInputSet) at BillingPlanDetailDV.pcf: line 93, column 90
    function def_onEnter_52 (def :  pcf.BillingPlanDisbursementInputSet_default) : void {
      def.onEnter(billingPlan, planNotInUse)
    }
    
    // 'def' attribute on InputSetRef at BillingPlanDetailDV.pcf: line 56, column 55
    function def_refreshVariables_36 (def :  pcf.PlanMultiCurrencyInputSet) : void {
      def.refreshVariables(billingPlan)
    }
    
    // 'def' attribute on InputSetRef at BillingPlanDetailDV.pcf: line 63, column 79
    function def_refreshVariables_38 (def :  pcf.BillingPlanPaymentDueIntervalInputSet_default) : void {
      def.refreshVariables(billingPlan, planNotInUse)
    }
    
    // 'def' attribute on InputSetRef at BillingPlanDetailDV.pcf: line 69, column 79
    function def_refreshVariables_41 (def :  pcf.BillingPlanFeeHandlingInputSet_default) : void {
      def.refreshVariables(billingPlan, planNotInUse)
    }
    
    // 'def' attribute on InputSetRef at BillingPlanDetailDV.pcf: line 85, column 79
    function def_refreshVariables_50 (def :  pcf.BillingPlanLowBalanceInvoicesInputSet_default) : void {
      def.refreshVariables(billingPlan, planNotInUse)
    }
    
    // 'def' attribute on InputSetRef (id=DisbursementInputSet) at BillingPlanDetailDV.pcf: line 93, column 90
    function def_refreshVariables_53 (def :  pcf.BillingPlanDisbursementInputSet_default) : void {
      def.refreshVariables(billingPlan, planNotInUse)
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at BillingPlanDetailDV.pcf: line 32, column 41
    function defaultSetter_10 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.Description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on RangeInput (id=PaymentDueDayLogic_Input) at BillingPlanDetailDV.pcf: line 42, column 46
    function defaultSetter_16 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.PaymentDueDayLogic = (__VALUE_TO_SET as typekey.DayOfMonthLogic)
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at BillingPlanDetailDV.pcf: line 48, column 44
    function defaultSetter_25 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.EffectiveDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at BillingPlanDetailDV.pcf: line 54, column 45
    function defaultSetter_31 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.ExpirationDate = (__VALUE_TO_SET as java.util.Date)
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at BillingPlanDetailDV.pcf: line 25, column 34
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.Name = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=Aggregation_Input) at BillingPlanDetailDV.pcf: line 78, column 46
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.Aggregation = (__VALUE_TO_SET as typekey.AggregationType)
    }
    
    // 'value' attribute on TextInput (id=RequestDate_Input) at BillingPlanDetailDV.pcf: line 108, column 40
    function defaultSetter_58 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.RequestIntervalDayCount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=FixDate_Input) at BillingPlanDetailDV.pcf: line 115, column 40
    function defaultSetter_66 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.ChangeDeadlineIntervalDayCount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on TextInput (id=DraftDate_Input) at BillingPlanDetailDV.pcf: line 122, column 40
    function defaultSetter_74 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.DraftIntervalDayCount = (__VALUE_TO_SET as java.lang.Integer)
    }
    
    // 'value' attribute on RangeInput (id=DraftDayLogic_Input) at BillingPlanDetailDV.pcf: line 135, column 46
    function defaultSetter_81 (__VALUE_TO_SET :  java.lang.Object) : void {
      billingPlan.DraftDayLogic = (__VALUE_TO_SET as typekey.DayOfMonthLogic)
    }
    
    // 'editable' attribute on TextInput (id=Name_Input) at BillingPlanDetailDV.pcf: line 25, column 34
    function editable_2 () : java.lang.Boolean {
      return planNotInUse
    }
    
    // 'initialValue' attribute on Variable at BillingPlanDetailDV.pcf: line 14, column 23
    function initialValue_0 () : Boolean {
      return not billingPlan.InUse
    }
    
    // 'initialValue' attribute on Variable at BillingPlanDetailDV.pcf: line 18, column 23
    function initialValue_1 () : boolean {
      return gw.api.util.LocaleUtil.getAllLanguages().Count > 1
    }
    
    // 'label' attribute on TextInput (id=RequestDate_Input) at BillingPlanDetailDV.pcf: line 108, column 40
    function label_56 () : java.lang.Object {
      return DisplayKey.get("Web.BillingPlanDetailDV.RequestInterval", gw.web.admin.BillingPlanHelper.getDayLabel(billingPlan))
    }
    
    // 'label' attribute on TextInput (id=FixDate_Input) at BillingPlanDetailDV.pcf: line 115, column 40
    function label_64 () : java.lang.Object {
      return DisplayKey.get("Web.BillingPlanDetailDV.ChangeInterval", gw.web.admin.BillingPlanHelper.getDayLabel(billingPlan))
    }
    
    // 'label' attribute on TextInput (id=DraftDate_Input) at BillingPlanDetailDV.pcf: line 122, column 40
    function label_72 () : java.lang.Object {
      return DisplayKey.get("Web.BillingPlanDetailDV.DraftInterval", gw.web.admin.BillingPlanHelper.getDayLabel(billingPlan))
    }
    
    // 'mode' attribute on InputSetRef at BillingPlanDetailDV.pcf: line 63, column 79
    function mode_39 () : java.lang.Object {
      return gw.plugin.Plugins.get(gw.module.IFeesThresholds).getPCFMode()
    }
    
    // 'validationExpression' attribute on DateInput (id=ExpirationDate_Input) at BillingPlanDetailDV.pcf: line 54, column 45
    function validationExpression_29 () : java.lang.Object {
      return billingPlan.hasValidExpirationDate() ? null : DisplayKey.get("Web.Plan.ExpirationDate.ValidationError")
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentDueDayLogic_Input) at BillingPlanDetailDV.pcf: line 42, column 46
    function valueRange_18 () : java.lang.Object {
      return new DayOfMonthLogic[]{DayOfMonthLogic.TC_EXACT, DayOfMonthLogic.TC_NEXTBUSINESSDAY, DayOfMonthLogic.TC_PREVBUSINESSDAY}
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at BillingPlanDetailDV.pcf: line 25, column 34
    function valueRoot_5 () : java.lang.Object {
      return billingPlan
    }
    
    // 'value' attribute on RangeInput (id=PaymentDueDayLogic_Input) at BillingPlanDetailDV.pcf: line 42, column 46
    function value_15 () : typekey.DayOfMonthLogic {
      return billingPlan.PaymentDueDayLogic
    }
    
    // 'value' attribute on DateInput (id=EffectiveDate_Input) at BillingPlanDetailDV.pcf: line 48, column 44
    function value_24 () : java.util.Date {
      return billingPlan.EffectiveDate
    }
    
    // 'value' attribute on TextInput (id=Name_Input) at BillingPlanDetailDV.pcf: line 25, column 34
    function value_3 () : java.lang.String {
      return billingPlan.Name
    }
    
    // 'value' attribute on DateInput (id=ExpirationDate_Input) at BillingPlanDetailDV.pcf: line 54, column 45
    function value_30 () : java.util.Date {
      return billingPlan.ExpirationDate
    }
    
    // 'value' attribute on TypeKeyInput (id=Aggregation_Input) at BillingPlanDetailDV.pcf: line 78, column 46
    function value_44 () : typekey.AggregationType {
      return billingPlan.Aggregation
    }
    
    // 'value' attribute on TextInput (id=RequestDate_Input) at BillingPlanDetailDV.pcf: line 108, column 40
    function value_57 () : java.lang.Integer {
      return billingPlan.RequestIntervalDayCount
    }
    
    // 'value' attribute on TextInput (id=FixDate_Input) at BillingPlanDetailDV.pcf: line 115, column 40
    function value_65 () : java.lang.Integer {
      return billingPlan.ChangeDeadlineIntervalDayCount
    }
    
    // 'value' attribute on TextInput (id=DraftDate_Input) at BillingPlanDetailDV.pcf: line 122, column 40
    function value_73 () : java.lang.Integer {
      return billingPlan.DraftIntervalDayCount
    }
    
    // 'value' attribute on RangeInput (id=DraftDayLogic_Input) at BillingPlanDetailDV.pcf: line 135, column 46
    function value_80 () : typekey.DayOfMonthLogic {
      return billingPlan.DraftDayLogic
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at BillingPlanDetailDV.pcf: line 32, column 41
    function value_9 () : java.lang.String {
      return billingPlan.Description
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentDueDayLogic_Input) at BillingPlanDetailDV.pcf: line 42, column 46
    function verifyValueRangeIsAllowedType_19 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentDueDayLogic_Input) at BillingPlanDetailDV.pcf: line 42, column 46
    function verifyValueRangeIsAllowedType_19 ($$arg :  typekey.DayOfMonthLogic[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DraftDayLogic_Input) at BillingPlanDetailDV.pcf: line 135, column 46
    function verifyValueRangeIsAllowedType_84 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=DraftDayLogic_Input) at BillingPlanDetailDV.pcf: line 135, column 46
    function verifyValueRangeIsAllowedType_84 ($$arg :  typekey.DayOfMonthLogic[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentDueDayLogic_Input) at BillingPlanDetailDV.pcf: line 42, column 46
    function verifyValueRange_20 () : void {
      var __valueRangeArg = new DayOfMonthLogic[]{DayOfMonthLogic.TC_EXACT, DayOfMonthLogic.TC_NEXTBUSINESSDAY, DayOfMonthLogic.TC_PREVBUSINESSDAY}
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_19(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=DraftDayLogic_Input) at BillingPlanDetailDV.pcf: line 135, column 46
    function verifyValueRange_85 () : void {
      var __valueRangeArg = new DayOfMonthLogic[]{DayOfMonthLogic.TC_EXACT, DayOfMonthLogic.TC_NEXTBUSINESSDAY, DayOfMonthLogic.TC_PREVBUSINESSDAY}
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_84(__valueRangeArg)
    }
    
    property get billingPlan () : BillingPlan {
      return getRequireValue("billingPlan", 0) as BillingPlan
    }
    
    property set billingPlan ($arg :  BillingPlan) {
      setRequireValue("billingPlan", 0, $arg)
    }
    
    property get hasMultipleLanguages () : boolean {
      return getVariableValue("hasMultipleLanguages", 0) as java.lang.Boolean
    }
    
    property set hasMultipleLanguages ($arg :  boolean) {
      setVariableValue("hasMultipleLanguages", 0, $arg)
    }
    
    property get planNotInUse () : Boolean {
      return getVariableValue("planNotInUse", 0) as Boolean
    }
    
    property set planNotInUse ($arg :  Boolean) {
      setVariableValue("planNotInUse", 0, $arg)
    }
    
    
  }
  
  
}