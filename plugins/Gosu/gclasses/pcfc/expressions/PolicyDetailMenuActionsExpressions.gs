package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyDetailMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyDetailMenuActionsExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyDetailMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyDetailMenuActionsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=PolicyDetailMenuActions_NewNote) at PolicyDetailMenuActions.pcf: line 35, column 35
    function action_12 () : void {
      PolicyNewNoteWorksheet.goInWorkspace( policyPeriod )
    }
    
    // 'action' attribute on MenuItem (id=PolicyDetailMenuActions_Renew) at PolicyDetailMenuActions.pcf: line 41, column 42
    function action_16 () : void {
      RenewPolicyWizard.go(policyPeriod)
    }
    
    // 'action' attribute on MenuItem (id=ReopenClosedPolicy_TDICMenuItem) at PolicyDetailMenuActions.pcf: line 47, column 280
    function action_19 () : void {
      policyPeriod.reopenClosedPolicyPeriod_TDIC()
    }
    
    // 'action' attribute on MenuItem (id=RequestRetrieve) at PolicyDetailMenuActions.pcf: line 15, column 52
    function action_2 () : void {
      RequestRetrievePopup.push(policyPeriod)
    }
    
    // 'action' attribute on MenuItem (id=NewEmail) at PolicyDetailMenuActions.pcf: line 63, column 84
    function action_24 () : void {
      SampleEmailWorksheet.goInWorkspace(policyPeriod, policyPeriod.Policy)
    }
    
    // 'action' attribute on MenuItem (id=NewEmailWithTemplate) at PolicyDetailMenuActions.pcf: line 67, column 96
    function action_26 () : void {
      SampleEmailWorksheet.goInWorkspace(policyPeriod, policyPeriod.Policy, "EmailReceived.gosu")
    }
    
    // 'action' attribute on MenuItem (id=SuspendResumeArchiving) at PolicyDetailMenuActions.pcf: line 78, column 158
    function action_30 () : void {
      ArchiveSuspendResumePopup.push(policyPeriod.Policy)
    }
    
    // 'action' attribute on MenuItem (id=PolicyDetailMenuActions_ChangePolicy) at PolicyDetailMenuActions.pcf: line 20, column 85
    function action_5 () : void {
      PolicyChangeWizard.push(policyPeriod)
    }
    
    // 'action' attribute on MenuItem (id=PolicyDetailMenuActions_NewNote) at PolicyDetailMenuActions.pcf: line 35, column 35
    function action_dest_13 () : pcf.api.Destination {
      return pcf.PolicyNewNoteWorksheet.createDestination( policyPeriod )
    }
    
    // 'action' attribute on MenuItem (id=PolicyDetailMenuActions_Renew) at PolicyDetailMenuActions.pcf: line 41, column 42
    function action_dest_17 () : pcf.api.Destination {
      return pcf.RenewPolicyWizard.createDestination(policyPeriod)
    }
    
    // 'action' attribute on MenuItem (id=NewEmail) at PolicyDetailMenuActions.pcf: line 63, column 84
    function action_dest_25 () : pcf.api.Destination {
      return pcf.SampleEmailWorksheet.createDestination(policyPeriod, policyPeriod.Policy)
    }
    
    // 'action' attribute on MenuItem (id=NewEmailWithTemplate) at PolicyDetailMenuActions.pcf: line 67, column 96
    function action_dest_27 () : pcf.api.Destination {
      return pcf.SampleEmailWorksheet.createDestination(policyPeriod, policyPeriod.Policy, "EmailReceived.gosu")
    }
    
    // 'action' attribute on MenuItem (id=RequestRetrieve) at PolicyDetailMenuActions.pcf: line 15, column 52
    function action_dest_3 () : pcf.api.Destination {
      return pcf.RequestRetrievePopup.createDestination(policyPeriod)
    }
    
    // 'action' attribute on MenuItem (id=SuspendResumeArchiving) at PolicyDetailMenuActions.pcf: line 78, column 158
    function action_dest_31 () : pcf.api.Destination {
      return pcf.ArchiveSuspendResumePopup.createDestination(policyPeriod.Policy)
    }
    
    // 'action' attribute on MenuItem (id=PolicyDetailMenuActions_ChangePolicy) at PolicyDetailMenuActions.pcf: line 20, column 85
    function action_dest_6 () : pcf.api.Destination {
      return pcf.PolicyChangeWizard.createDestination(policyPeriod)
    }
    
    // 'available' attribute on MenuItem (id=RequestRetrieve) at PolicyDetailMenuActions.pcf: line 15, column 52
    function available_0 () : java.lang.Boolean {
      return policyPeriod.Archived
    }
    
    // 'available' attribute on MenuItem (id=PolicyDetailMenuActions_NewNote) at PolicyDetailMenuActions.pcf: line 35, column 35
    function available_10 () : java.lang.Boolean {
      return !policyPeriod.Archived
    }
    
    // 'available' attribute on MenuItem (id=ReopenClosedPolicy_TDICMenuItem) at PolicyDetailMenuActions.pcf: line 47, column 280
    function available_18 () : java.lang.Boolean {
      return policyPeriod.Closed and perm.System.plcyreopen_TDIC
    }
    
    // 'available' attribute on MenuItem (id=NewDocument) at PolicyDetailMenuActions.pcf: line 52, column 38
    function available_22 () : java.lang.Boolean {
      return !policyPeriod.Archived && gw.plugin.Plugins.isEnabled(gw.plugin.document.IDocumentContentSource)
    }
    
    // 'available' attribute on MenuItem (id=PolicyDetailMenuActions_ChangePolicy) at PolicyDetailMenuActions.pcf: line 20, column 85
    function available_4 () : java.lang.Boolean {
      return not policyPeriod.Closed and perm.PolicyPeriod.plcychange
    }
    
    // 'def' attribute on MenuItemSetRef at PolicyDetailMenuActions.pcf: line 54, column 60
    function def_onEnter_20 (def :  pcf.NewDocumentMenuItemSet) : void {
      def.onEnter(policyPeriod.Policy)
    }
    
    // 'def' attribute on MenuItemSetRef at PolicyDetailMenuActions.pcf: line 27, column 72
    function def_onEnter_7 (def :  pcf.NewActivityMenuItemSet) : void {
      def.onEnter(false, null, null, policyPeriod)
    }
    
    // 'def' attribute on MenuItemSetRef at PolicyDetailMenuActions.pcf: line 54, column 60
    function def_refreshVariables_21 (def :  pcf.NewDocumentMenuItemSet) : void {
      def.refreshVariables(policyPeriod.Policy)
    }
    
    // 'def' attribute on MenuItemSetRef at PolicyDetailMenuActions.pcf: line 27, column 72
    function def_refreshVariables_8 (def :  pcf.NewActivityMenuItemSet) : void {
      def.refreshVariables(false, null, null, policyPeriod)
    }
    
    // 'visible' attribute on MenuItem (id=RequestRetrieve) at PolicyDetailMenuActions.pcf: line 15, column 52
    function visible_1 () : java.lang.Boolean {
      return perm.PolicyPeriod.archiveretrieve
    }
    
    // 'visible' attribute on MenuItem (id=PolicyDetailMenuActions_NewNote) at PolicyDetailMenuActions.pcf: line 35, column 35
    function visible_11 () : java.lang.Boolean {
      return perm.Note.create
    }
    
    // 'visible' attribute on MenuItem (id=PolicyDetailMenuActions_Renew) at PolicyDetailMenuActions.pcf: line 41, column 42
    function visible_15 () : java.lang.Boolean {
      return policyPeriod.canRenew()
    }
    
    // 'visible' attribute on MenuItem (id=NewDocument) at PolicyDetailMenuActions.pcf: line 52, column 38
    function visible_23 () : java.lang.Boolean {
      return perm.Document.create
    }
    
    // 'visible' attribute on MenuItem (id=SuspendResumeArchiving) at PolicyDetailMenuActions.pcf: line 78, column 158
    function visible_29 () : java.lang.Boolean {
      return (policyPeriod.Policy.DoNotArchive and perm.Policy.archiveenable) or (not policyPeriod.Policy.DoNotArchive and perm.Policy.archivedisable)
    }
    
    // 'visible' attribute on MenuItem (id=Archiving) at PolicyDetailMenuActions.pcf: line 73, column 71
    function visible_32 () : java.lang.Boolean {
      return gw.api.system.PLConfigParameters.ArchiveEnabled.Value
    }
    
    // 'visible' attribute on MenuItem (id=NewAssignedActivity) at PolicyDetailMenuActions.pcf: line 25, column 38
    function visible_9 () : java.lang.Boolean {
      return perm.Activity.create
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getRequireValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setRequireValue("policyPeriod", 0, $arg)
    }
    
    
  }
  
  
}