package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/accounting/TransactionDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransactionDetailDVExpressions {
  @javax.annotation.Generated("config/web/pcf/accounting/TransactionDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends TransactionDetailDVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=Amount_Cell) at TransactionDetailDV.pcf: line 42, column 42
    function currency_16 () : typekey.Currency {
      return LineItem.Currency
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at TransactionDetailDV.pcf: line 42, column 42
    function valueRoot_15 () : java.lang.Object {
      return LineItem
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at TransactionDetailDV.pcf: line 51, column 78
    function valueRoot_22 () : java.lang.Object {
      return LineItem.TAccount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at TransactionDetailDV.pcf: line 42, column 42
    function value_14 () : gw.pl.currency.MonetaryAmount {
      return LineItem.Amount
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at TransactionDetailDV.pcf: line 47, column 49
    function value_18 () : typekey.LedgerSide {
      return LineItem.Type
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at TransactionDetailDV.pcf: line 51, column 78
    function value_21 () : java.lang.String {
      return LineItem.TAccount.TAccountOwnerTypeNameTAccountName
    }
    
    property get LineItem () : entity.LineItem {
      return getIteratedValue(1) as entity.LineItem
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/accounting/TransactionDetailDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransactionDetailDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // MenuItem (id=UserBrowseMenuItem) at TransactionDetailDV.pcf: line 23, column 41
    function action_6 () : void {
      pcf.UserSearchPopup.push()
    }
    
    // MenuItem (id=UserBrowseMenuItem) at TransactionDetailDV.pcf: line 23, column 41
    function action_dest_7 () : pcf.api.Destination {
      return pcf.UserSearchPopup.createDestination()
    }
    
    // 'value' attribute on MonetaryAmountCell (id=Amount_Cell) at TransactionDetailDV.pcf: line 42, column 42
    function sortValue_11 (LineItem :  entity.LineItem) : java.lang.Object {
      return LineItem.Amount
    }
    
    // 'value' attribute on TypeKeyCell (id=Type_Cell) at TransactionDetailDV.pcf: line 47, column 49
    function sortValue_12 (LineItem :  entity.LineItem) : java.lang.Object {
      return LineItem.Type
    }
    
    // 'value' attribute on TextCell (id=TAccount_Cell) at TransactionDetailDV.pcf: line 51, column 78
    function sortValue_13 (LineItem :  entity.LineItem) : java.lang.Object {
      return LineItem.TAccount.TAccountOwnerTypeNameTAccountName
    }
    
    // 'value' attribute on DateInput (id=TransactionDate_Input) at TransactionDetailDV.pcf: line 14, column 46
    function valueRoot_1 () : java.lang.Object {
      return Transaction
    }
    
    // 'value' attribute on DateInput (id=TransactionDate_Input) at TransactionDetailDV.pcf: line 14, column 46
    function value_0 () : java.util.Date {
      return Transaction.TransactionDate
    }
    
    // 'value' attribute on RowIterator at TransactionDetailDV.pcf: line 35, column 43
    function value_24 () : entity.LineItem[] {
      return Transaction.LineItems
    }
    
    // 'value' attribute on TextInput (id=Description_Input) at TransactionDetailDV.pcf: line 18, column 46
    function value_3 () : java.lang.String {
      return Transaction.LongDisplayName
    }
    
    // 'value' attribute on AltUserInput (id=CreateUser_Input) at TransactionDetailDV.pcf: line 23, column 41
    function value_8 () : entity.User {
      return Transaction.CreateUser
    }
    
    property get Transaction () : Transaction {
      return getRequireValue("Transaction", 0) as Transaction
    }
    
    property set Transaction ($arg :  Transaction) {
      setRequireValue("Transaction", 0, $arg)
    }
    
    
  }
  
  
}