package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PoliciesMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PoliciesMenuActionsExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PoliciesMenuActions.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PoliciesMenuActionsExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    
  }
  
  
}