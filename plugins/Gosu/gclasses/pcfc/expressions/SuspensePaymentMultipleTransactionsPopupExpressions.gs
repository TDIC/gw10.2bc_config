package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/suspensepayment/SuspensePaymentMultipleTransactionsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class SuspensePaymentMultipleTransactionsPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/suspensepayment/SuspensePaymentMultipleTransactionsPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class SuspensePaymentMultipleTransactionsPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (suspensePayment :  SuspensePayment) : int {
      return 0
    }
    
    // 'def' attribute on ListViewInput (id=suspensePaymentTransactions) at SuspensePaymentMultipleTransactionsPopup.pcf: line 18, column 46
    function def_onEnter_0 (def :  pcf.SuspensePaymentTransactionsLV) : void {
      def.onEnter(suspensePayment)
    }
    
    // 'def' attribute on ListViewInput (id=suspensePaymentTransactions) at SuspensePaymentMultipleTransactionsPopup.pcf: line 18, column 46
    function def_refreshVariables_1 (def :  pcf.SuspensePaymentTransactionsLV) : void {
      def.refreshVariables(suspensePayment)
    }
    
    override property get CurrentLocation () : pcf.SuspensePaymentMultipleTransactionsPopup {
      return super.CurrentLocation as pcf.SuspensePaymentMultipleTransactionsPopup
    }
    
    property get suspensePayment () : SuspensePayment {
      return getVariableValue("suspensePayment", 0) as SuspensePayment
    }
    
    property set suspensePayment ($arg :  SuspensePayment) {
      setVariableValue("suspensePayment", 0, $arg)
    }
    
    
  }
  
  
}