package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadConfirmation.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PlanDataUploadConfirmationExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/PlanDataLoader/PlanDataUploadConfirmation.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PlanDataUploadConfirmationExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (processor :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) : int {
      return 0
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 211, column 60
    function def_onEnter_100 (def :  pcf.TDIC_PlanDataUploadGLFilterLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 219, column 72
    function def_onEnter_102 (def :  pcf.TDIC_PlanDataUploadGLTransactionMappingLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 227, column 73
    function def_onEnter_104 (def :  pcf.TDIC_PlanDataUploadGLTAccountNameMappingLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 235, column 59
    function def_onEnter_106 (def :  pcf.PlanDataUploadLocalizationLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 115, column 58
    function def_onEnter_64 (def :  pcf.PlanDataUploadBillingPlanLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 123, column 61
    function def_onEnter_67 (def :  pcf.PlanDataUploadAgencyBillPlanLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 131, column 58
    function def_onEnter_70 (def :  pcf.PlanDataUploadPaymentPlanLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 139, column 67
    function def_onEnter_73 (def :  pcf.PlanDataUploadPaymentPlanOverridesLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 147, column 62
    function def_onEnter_76 (def :  pcf.PlanDataUploadDelinquencyPlanLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 155, column 70
    function def_onEnter_79 (def :  pcf.PlanDataUploadDelinquencyPlanWorkflowLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 163, column 61
    function def_onEnter_82 (def :  pcf.PlanDataUploadCommissionPlanLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 171, column 64
    function def_onEnter_85 (def :  pcf.PlanDataUploadCommissionSubPlanLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 179, column 60
    function def_onEnter_88 (def :  pcf.PlanDataUploadChargePatternLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 187, column 68
    function def_onEnter_91 (def :  pcf.PlanDataUploadPaymentAllocationPlanLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 195, column 64
    function def_onEnter_94 (def :  pcf.PlanDataUploadReturnPremiumPlanLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 203, column 66
    function def_onEnter_97 (def :  pcf.PlanDataUploadReturnPremiumSchemeLV) : void {
      def.onEnter(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 211, column 60
    function def_refreshVariables_101 (def :  pcf.TDIC_PlanDataUploadGLFilterLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 219, column 72
    function def_refreshVariables_103 (def :  pcf.TDIC_PlanDataUploadGLTransactionMappingLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 227, column 73
    function def_refreshVariables_105 (def :  pcf.TDIC_PlanDataUploadGLTAccountNameMappingLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 235, column 59
    function def_refreshVariables_107 (def :  pcf.PlanDataUploadLocalizationLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 115, column 58
    function def_refreshVariables_65 (def :  pcf.PlanDataUploadBillingPlanLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 123, column 61
    function def_refreshVariables_68 (def :  pcf.PlanDataUploadAgencyBillPlanLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 131, column 58
    function def_refreshVariables_71 (def :  pcf.PlanDataUploadPaymentPlanLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 139, column 67
    function def_refreshVariables_74 (def :  pcf.PlanDataUploadPaymentPlanOverridesLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 147, column 62
    function def_refreshVariables_77 (def :  pcf.PlanDataUploadDelinquencyPlanLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 155, column 70
    function def_refreshVariables_80 (def :  pcf.PlanDataUploadDelinquencyPlanWorkflowLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 163, column 61
    function def_refreshVariables_83 (def :  pcf.PlanDataUploadCommissionPlanLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 171, column 64
    function def_refreshVariables_86 (def :  pcf.PlanDataUploadCommissionSubPlanLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 179, column 60
    function def_refreshVariables_89 (def :  pcf.PlanDataUploadChargePatternLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 187, column 68
    function def_refreshVariables_92 (def :  pcf.PlanDataUploadPaymentAllocationPlanLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 195, column 64
    function def_refreshVariables_95 (def :  pcf.PlanDataUploadReturnPremiumPlanLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'def' attribute on PanelRef at PlanDataUploadConfirmation.pcf: line 203, column 66
    function def_refreshVariables_98 (def :  pcf.PlanDataUploadReturnPremiumSchemeLV) : void {
      def.refreshVariables(processor)
    }
    
    // 'label' attribute on TextInput (id=noBillingPlan_Input) at PlanDataUploadConfirmation.pcf: line 26, column 44
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlans"))
    }
    
    // 'label' attribute on TextInput (id=noPaymentPlanOverrides_Input) at PlanDataUploadConfirmation.pcf: line 41, column 44
    function label_12 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverrides"))
    }
    
    // 'label' attribute on TextInput (id=noDelinquencyPlan_Input) at PlanDataUploadConfirmation.pcf: line 46, column 44
    function label_16 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlans"))
    }
    
    // 'label' attribute on TextInput (id=noDelinquencyPlanWorkflow_Input) at PlanDataUploadConfirmation.pcf: line 51, column 44
    function label_20 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflowSteps"))
    }
    
    // 'label' attribute on TextInput (id=noCommissionPlan_Input) at PlanDataUploadConfirmation.pcf: line 58, column 44
    function label_24 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionPlans"))
    }
    
    // 'label' attribute on TextInput (id=noCommissionSubPlan_Input) at PlanDataUploadConfirmation.pcf: line 63, column 44
    function label_28 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlans"))
    }
    
    // 'label' attribute on TextInput (id=noChargePattern_Input) at PlanDataUploadConfirmation.pcf: line 68, column 44
    function label_32 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePatterns"))
    }
    
    // 'label' attribute on TextInput (id=noPaymentAllocationPlan_Input) at PlanDataUploadConfirmation.pcf: line 73, column 44
    function label_36 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentAllocationPlans"))
    }
    
    // 'label' attribute on TextInput (id=noAgencyBillPlan_Input) at PlanDataUploadConfirmation.pcf: line 31, column 44
    function label_4 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.AgencyBillPlans"))
    }
    
    // 'label' attribute on TextInput (id=noReturnPremiumPlan_Input) at PlanDataUploadConfirmation.pcf: line 78, column 44
    function label_40 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumPlans"))
    }
    
    // 'label' attribute on TextInput (id=noReturnPremiumScheme_Input) at PlanDataUploadConfirmation.pcf: line 83, column 44
    function label_44 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumSchemes"))
    }
    
    // 'label' attribute on TextInput (id=noGLFilter_Input) at PlanDataUploadConfirmation.pcf: line 90, column 44
    function label_48 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.GLTransactionFilter"))
    }
    
    // 'label' attribute on TextInput (id=noGLTransactionNameMapping_Input) at PlanDataUploadConfirmation.pcf: line 95, column 44
    function label_52 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.GLTransactionNameMapping"))
    }
    
    // 'label' attribute on TextInput (id=noGLTAccountMapping_Input) at PlanDataUploadConfirmation.pcf: line 100, column 44
    function label_56 () : java.lang.Object {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", DisplayKey.get("TDIC.ExcelDataLoader.PlanData.GLTAccountNameMapping"))
    }
    
    // 'label' attribute on TextInput (id=noLocalizations_Input) at PlanDataUploadConfirmation.pcf: line 107, column 44
    function label_60 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations"))
    }
    
    // 'label' attribute on TextInput (id=noPaymentPlan_Input) at PlanDataUploadConfirmation.pcf: line 36, column 44
    function label_8 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.ConfirmationNumberOf", gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlans"))
    }
    
    // 'parent' attribute on Page (id=PlanDataUploadConfirmation) at PlanDataUploadConfirmation.pcf: line 8, column 84
    static function parent_109 (processor :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) : pcf.api.Destination {
      return pcf.ServerTools.createDestination()
    }
    
    // 'title' attribute on Card (id=localizations) at PlanDataUploadConfirmation.pcf: line 233, column 105
    function title_108 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.Localizations")
    }
    
    // 'title' attribute on Card (id=billingPlan) at PlanDataUploadConfirmation.pcf: line 113, column 104
    function title_66 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.BillingPlans")
    }
    
    // 'title' attribute on Card (id=agencyBillPlan) at PlanDataUploadConfirmation.pcf: line 121, column 107
    function title_69 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.AgencyBillPlans")
    }
    
    // 'title' attribute on Card (id=paymentPlan) at PlanDataUploadConfirmation.pcf: line 129, column 104
    function title_72 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlans")
    }
    
    // 'title' attribute on Card (id=paymentPlanOverrides) at PlanDataUploadConfirmation.pcf: line 137, column 112
    function title_75 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentPlanOverrides")
    }
    
    // 'title' attribute on Card (id=delinquencyPlan) at PlanDataUploadConfirmation.pcf: line 145, column 108
    function title_78 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyPlans")
    }
    
    // 'title' attribute on Card (id=delinquencyPlanWorkflow) at PlanDataUploadConfirmation.pcf: line 153, column 111
    function title_81 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.DelinquencyWorkflow")
    }
    
    // 'title' attribute on Card (id=commissionPlan) at PlanDataUploadConfirmation.pcf: line 161, column 107
    function title_84 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionPlans")
    }
    
    // 'title' attribute on Card (id=commissionSubPlan) at PlanDataUploadConfirmation.pcf: line 169, column 110
    function title_87 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.CommissionSubPlans")
    }
    
    // 'title' attribute on Card (id=chargePattern) at PlanDataUploadConfirmation.pcf: line 177, column 106
    function title_90 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ChargePatterns")
    }
    
    // 'title' attribute on Card (id=paymentAllocationPlan) at PlanDataUploadConfirmation.pcf: line 185, column 114
    function title_93 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.PaymentAllocationPlans")
    }
    
    // 'title' attribute on Card (id=returnPremiumPlan) at PlanDataUploadConfirmation.pcf: line 193, column 110
    function title_96 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumPlans")
    }
    
    // 'title' attribute on Card (id=returnPremiumScheme) at PlanDataUploadConfirmation.pcf: line 201, column 112
    function title_99 () : java.lang.String {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.PlanData.ReturnPremiumSchemes")
    }
    
    // 'value' attribute on TextInput (id=noBillingPlan_Input) at PlanDataUploadConfirmation.pcf: line 26, column 44
    function value_1 () : java.lang.Integer {
      return processor.BillingPlanArray == null ? 0 : processor.BillingPlanArray.countWhere(\ a -> a.Skipped == false and a.Error== false )
    }
    
    // 'value' attribute on TextInput (id=noPaymentPlanOverrides_Input) at PlanDataUploadConfirmation.pcf: line 41, column 44
    function value_13 () : java.lang.Integer {
      return processor.PaymentPlanOverrideArray == null ? 0 : processor.PaymentPlanOverrideArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    // 'value' attribute on TextInput (id=noDelinquencyPlan_Input) at PlanDataUploadConfirmation.pcf: line 46, column 44
    function value_17 () : java.lang.Integer {
      return processor.DelinquencyPlanArray == null ? 0 : processor.DelinquencyPlanArray.countWhere(\ i -> i.Skipped == false and i.Error == false )
    }
    
    // 'value' attribute on TextInput (id=noDelinquencyPlanWorkflow_Input) at PlanDataUploadConfirmation.pcf: line 51, column 44
    function value_21 () : java.lang.Integer {
      return processor.DelinquencyPlanWorkflowArray == null ? 0 : processor.DelinquencyPlanWorkflowArray.countWhere(\ i -> i.Skipped == false and i.Error == false )
    }
    
    // 'value' attribute on TextInput (id=noCommissionPlan_Input) at PlanDataUploadConfirmation.pcf: line 58, column 44
    function value_25 () : java.lang.Integer {
      return processor.CommissionPlanArray == null ? 0 : processor.CommissionPlanArray.countWhere(\ a -> a.Skipped == false and a.Error== false )
    }
    
    // 'value' attribute on TextInput (id=noCommissionSubPlan_Input) at PlanDataUploadConfirmation.pcf: line 63, column 44
    function value_29 () : java.lang.Integer {
      return processor.CommissionSubPlanArray == null ? 0 : processor.CommissionSubPlanArray.countWhere(\ a -> a.Skipped == false and a.Error== false )
    }
    
    // 'value' attribute on TextInput (id=noChargePattern_Input) at PlanDataUploadConfirmation.pcf: line 68, column 44
    function value_33 () : java.lang.Integer {
      return processor.ChargePatternArray == null ? 0 : processor.ChargePatternArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    // 'value' attribute on TextInput (id=noPaymentAllocationPlan_Input) at PlanDataUploadConfirmation.pcf: line 73, column 44
    function value_37 () : java.lang.Integer {
      return processor.PaymentAllocationPlanArray == null ? 0 : processor.PaymentAllocationPlanArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    // 'value' attribute on TextInput (id=noReturnPremiumPlan_Input) at PlanDataUploadConfirmation.pcf: line 78, column 44
    function value_41 () : java.lang.Integer {
      return processor.ReturnPremiumPlanArray == null ? 0 : processor.ReturnPremiumPlanArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    // 'value' attribute on TextInput (id=noReturnPremiumScheme_Input) at PlanDataUploadConfirmation.pcf: line 83, column 44
    function value_45 () : java.lang.Integer {
      return processor.ReturnPremiumSchemeArray == null ? 0 : processor.ReturnPremiumSchemeArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    // 'value' attribute on TextInput (id=noGLFilter_Input) at PlanDataUploadConfirmation.pcf: line 90, column 44
    function value_49 () : java.lang.Integer {
      return processor.GLTransactionFilterArray == null ? 0 : processor.GLTransactionFilterArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    // 'value' attribute on TextInput (id=noAgencyBillPlan_Input) at PlanDataUploadConfirmation.pcf: line 31, column 44
    function value_5 () : java.lang.Integer {
      return processor.AgencyBillPlanArray == null ? 0 : processor.AgencyBillPlanArray.countWhere(\ a -> a.Skipped == false and a.Error== false )
    }
    
    // 'value' attribute on TextInput (id=noGLTransactionNameMapping_Input) at PlanDataUploadConfirmation.pcf: line 95, column 44
    function value_53 () : java.lang.Integer {
      return processor.GLTransactionNameMappingArray == null ? 0 : processor.GLTransactionNameMappingArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    // 'value' attribute on TextInput (id=noGLTAccountMapping_Input) at PlanDataUploadConfirmation.pcf: line 100, column 44
    function value_57 () : java.lang.Integer {
      return processor.GLTAccountNameMappingArray == null ? 0 : processor.GLTAccountNameMappingArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    // 'value' attribute on TextInput (id=noLocalizations_Input) at PlanDataUploadConfirmation.pcf: line 107, column 44
    function value_61 () : java.lang.Integer {
      return processor.LocalizationArray == null ? 0 : processor.LocalizationArray.countWhere(\ a -> a.Skipped == false and a.Error== false )
    }
    
    // 'value' attribute on TextInput (id=noPaymentPlan_Input) at PlanDataUploadConfirmation.pcf: line 36, column 44
    function value_9 () : java.lang.Integer {
      return processor.PaymentPlanArray == null ? 0 : processor.PaymentPlanArray.countWhere(\ p -> p.Skipped == false and p.Error == false)
    }
    
    override property get CurrentLocation () : pcf.PlanDataUploadConfirmation {
      return super.CurrentLocation as pcf.PlanDataUploadConfirmation
    }
    
    property get processor () : tdic.util.dataloader.processor.BCPlanDataLoaderProcessor {
      return getVariableValue("processor", 0) as tdic.util.dataloader.processor.BCPlanDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCPlanDataLoaderProcessor) {
      setVariableValue("processor", 0, $arg)
    }
    
    
  }
  
  
}