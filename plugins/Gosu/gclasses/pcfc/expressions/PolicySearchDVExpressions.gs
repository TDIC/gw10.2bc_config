package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/search/PolicySearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicySearchDVExpressions {
  @javax.annotation.Generated("config/web/pcf/search/PolicySearchDV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicySearchDVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountNumberCriterion_Input) at PolicySearchDV.pcf: line 24, column 39
    function action_0 () : void {
      AccountSearchPopup.push()
    }
    
    // 'pickLocation' attribute on PickerInput (id=AccountNumberCriterion_Input) at PolicySearchDV.pcf: line 24, column 39
    function action_dest_1 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'conversionExpression' attribute on PickerInput (id=AccountNumberCriterion_Input) at PolicySearchDV.pcf: line 24, column 39
    function conversionExpression_2 (PickedValue :  Account) : java.lang.String {
      return PickedValue.DisplayName
    }
    
    // 'def' attribute on InputSetRef at PolicySearchDV.pcf: line 77, column 77
    function def_onEnter_49 (def :  pcf.ContactCriteriaInputSet) : void {
      def.onEnter(searchCriteria.ContactCriteria,true)
    }
    
    // 'def' attribute on InputSetRef at PolicySearchDV.pcf: line 81, column 41
    function def_onEnter_51 (def :  pcf.SearchAndResetInputSet) : void {
      def.onEnter()
    }
    
    // 'def' attribute on InputSetRef at PolicySearchDV.pcf: line 77, column 77
    function def_refreshVariables_50 (def :  pcf.ContactCriteriaInputSet) : void {
      def.refreshVariables(searchCriteria.ContactCriteria,true)
    }
    
    // 'def' attribute on InputSetRef at PolicySearchDV.pcf: line 81, column 41
    function def_refreshVariables_52 (def :  pcf.SearchAndResetInputSet) : void {
      def.refreshVariables()
    }
    
    // 'value' attribute on TextInput (id=LegacyPolicyNumberCriterion_Input) at PolicySearchDV.pcf: line 34, column 52
    function defaultSetter_12 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.LegacyPolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at PolicySearchDV.pcf: line 41, column 67
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.CurrencyCriterion = (__VALUE_TO_SET as typekey.Currency)
    }
    
    // 'value' attribute on TypeKeyInput (id=StatusCriterion_Input) at PolicySearchDV.pcf: line 47, column 50
    function defaultSetter_22 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ClosureStatus = (__VALUE_TO_SET as typekey.PolicyClosureStatus)
    }
    
    // 'value' attribute on RangeInput (id=ProductCriterion_Input) at PolicySearchDV.pcf: line 54, column 38
    function defaultSetter_26 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.LOBCode = (__VALUE_TO_SET as typekey.LOBCode)
    }
    
    // 'value' attribute on RangeInput (id=BillingMethodCriterion_Input) at PolicySearchDV.pcf: line 61, column 56
    function defaultSetter_33 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.BillingMethod = (__VALUE_TO_SET as typekey.PolicyPeriodBillingMethod)
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at PolicySearchDV.pcf: line 24, column 39
    function defaultSetter_4 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.AccountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=ProducerCodeCriterion_Input) at PolicySearchDV.pcf: line 67, column 46
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.ProducerCode = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on CheckBoxInput (id=ArchivedCriterion_Input) at PolicySearchDV.pcf: line 73, column 108
    function defaultSetter_45 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.IncludeArchived = (__VALUE_TO_SET as java.lang.Boolean)
    }
    
    // 'value' attribute on TextInput (id=PolicyNumberCriterion_Input) at PolicySearchDV.pcf: line 29, column 46
    function defaultSetter_8 (__VALUE_TO_SET :  java.lang.Object) : void {
      searchCriteria.PolicyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'valueRange' attribute on RangeInput (id=ProductCriterion_Input) at PolicySearchDV.pcf: line 54, column 38
    function valueRange_28 () : java.lang.Object {
      return tdic.bc.config.policy.PolicyHelper.tdicProductLines()
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingMethodCriterion_Input) at PolicySearchDV.pcf: line 61, column 56
    function valueRange_35 () : java.lang.Object {
      return PolicyPeriodBillingMethod.getTypeKeys(false)
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at PolicySearchDV.pcf: line 24, column 39
    function valueRoot_5 () : java.lang.Object {
      return searchCriteria
    }
    
    // 'value' attribute on TextInput (id=LegacyPolicyNumberCriterion_Input) at PolicySearchDV.pcf: line 34, column 52
    function value_11 () : java.lang.String {
      return searchCriteria.LegacyPolicyNumber
    }
    
    // 'value' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at PolicySearchDV.pcf: line 41, column 67
    function value_16 () : typekey.Currency {
      return searchCriteria.CurrencyCriterion
    }
    
    // 'value' attribute on TypeKeyInput (id=StatusCriterion_Input) at PolicySearchDV.pcf: line 47, column 50
    function value_21 () : typekey.PolicyClosureStatus {
      return searchCriteria.ClosureStatus
    }
    
    // 'value' attribute on RangeInput (id=ProductCriterion_Input) at PolicySearchDV.pcf: line 54, column 38
    function value_25 () : typekey.LOBCode {
      return searchCriteria.LOBCode
    }
    
    // 'value' attribute on PickerInput (id=AccountNumberCriterion_Input) at PolicySearchDV.pcf: line 24, column 39
    function value_3 () : java.lang.String {
      return searchCriteria.AccountNumber
    }
    
    // 'value' attribute on RangeInput (id=BillingMethodCriterion_Input) at PolicySearchDV.pcf: line 61, column 56
    function value_32 () : typekey.PolicyPeriodBillingMethod {
      return searchCriteria.BillingMethod
    }
    
    // 'value' attribute on TextInput (id=ProducerCodeCriterion_Input) at PolicySearchDV.pcf: line 67, column 46
    function value_39 () : java.lang.String {
      return searchCriteria.ProducerCode
    }
    
    // 'value' attribute on CheckBoxInput (id=ArchivedCriterion_Input) at PolicySearchDV.pcf: line 73, column 108
    function value_44 () : java.lang.Boolean {
      return searchCriteria.IncludeArchived
    }
    
    // 'value' attribute on TextInput (id=PolicyNumberCriterion_Input) at PolicySearchDV.pcf: line 29, column 46
    function value_7 () : java.lang.String {
      return searchCriteria.PolicyNumber
    }
    
    // 'valueRange' attribute on RangeInput (id=ProductCriterion_Input) at PolicySearchDV.pcf: line 54, column 38
    function verifyValueRangeIsAllowedType_29 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProductCriterion_Input) at PolicySearchDV.pcf: line 54, column 38
    function verifyValueRangeIsAllowedType_29 ($$arg :  typekey.LOBCode[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingMethodCriterion_Input) at PolicySearchDV.pcf: line 61, column 56
    function verifyValueRangeIsAllowedType_36 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingMethodCriterion_Input) at PolicySearchDV.pcf: line 61, column 56
    function verifyValueRangeIsAllowedType_36 ($$arg :  typekey.PolicyPeriodBillingMethod[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=ProductCriterion_Input) at PolicySearchDV.pcf: line 54, column 38
    function verifyValueRange_30 () : void {
      var __valueRangeArg = tdic.bc.config.policy.PolicyHelper.tdicProductLines()
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_29(__valueRangeArg)
    }
    
    // 'valueRange' attribute on RangeInput (id=BillingMethodCriterion_Input) at PolicySearchDV.pcf: line 61, column 56
    function verifyValueRange_37 () : void {
      var __valueRangeArg = PolicyPeriodBillingMethod.getTypeKeys(false)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_36(__valueRangeArg)
    }
    
    // 'visible' attribute on TypeKeyInput (id=CurrencyCriterion_Input) at PolicySearchDV.pcf: line 41, column 67
    function visible_15 () : java.lang.Boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode()
    }
    
    // 'visible' attribute on CheckBoxInput (id=ArchivedCriterion_Input) at PolicySearchDV.pcf: line 73, column 108
    function visible_43 () : java.lang.Boolean {
      return allowsArchiveInclusion && gw.api.system.PLConfigParameters.ArchiveEnabled.Value
    }
    
    property get allowsArchiveInclusion () : boolean {
      return getRequireValue("allowsArchiveInclusion", 0) as java.lang.Boolean
    }
    
    property set allowsArchiveInclusion ($arg :  boolean) {
      setRequireValue("allowsArchiveInclusion", 0, $arg)
    }
    
    property get searchCriteria () : gw.search.PolicySearchCriteria {
      return getRequireValue("searchCriteria", 0) as gw.search.PolicySearchCriteria
    }
    
    property set searchCriteria ($arg :  gw.search.PolicySearchCriteria) {
      setRequireValue("searchCriteria", 0, $arg)
    }
    
    
  }
  
  
}