package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyOverview.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyOverviewExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyOverview.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyOverviewExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (plcyPeriod :  PolicyPeriod) : int {
      return 0
    }
    
    // 'location' attribute on LocationGroup (id=PolicyOverview) at PolicyOverview.pcf: line 16, column 45
    function action_0 () : void {
      pcf.PolicySummary.go(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyOverview) at PolicyOverview.pcf: line 19, column 51
    function action_2 () : void {
      pcf.PolicyDetailSummary.go(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyOverview) at PolicyOverview.pcf: line 16, column 45
    function action_dest_1 () : pcf.api.Destination {
      return pcf.PolicySummary.createDestination(plcyPeriod)
    }
    
    // 'location' attribute on LocationGroup (id=PolicyOverview) at PolicyOverview.pcf: line 19, column 51
    function action_dest_3 () : pcf.api.Destination {
      return pcf.PolicyDetailSummary.createDestination(plcyPeriod)
    }
    
    // 'canVisit' attribute on LocationGroup (id=PolicyOverview) at PolicyOverview.pcf: line 8, column 66
    static function canVisit_4 (plcyPeriod :  PolicyPeriod) : java.lang.Boolean {
      return plcyPeriod.ViewableByCurrentUser
    }
    
    // LocationGroup (id=PolicyOverview) at PolicyOverview.pcf: line 8, column 66
    static function firstVisitableChildDestinationMethod_8 (plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
      var dest : pcf.api.Destination
      dest = pcf.PolicySummary.createDestination(plcyPeriod)
      if (dest.canVisitSelf()) {
        return dest
      }
      dest = pcf.PolicyDetailSummary.createDestination(plcyPeriod)
      if (dest.canVisitSelf()) {
        return dest
      }
      return null
    }
    
    // LocationGroup (id=PolicyOverview) at PolicyOverview.pcf: line 8, column 66
    static function parent_5 (plcyPeriod :  PolicyPeriod) : pcf.api.Destination {
      return pcf.PolicyGroup.createDestination(plcyPeriod)
    }
    
    // 'tabBar' attribute on LocationGroup (id=PolicyOverview) at PolicyOverview.pcf: line 8, column 66
    function tabBar_onEnter_6 (def :  pcf.TabBar) : void {
      def.onEnter()
    }
    
    // 'tabBar' attribute on LocationGroup (id=PolicyOverview) at PolicyOverview.pcf: line 8, column 66
    function tabBar_refreshVariables_7 (def :  pcf.TabBar) : void {
      def.refreshVariables()
    }
    
    override property get CurrentLocation () : pcf.PolicyOverview {
      return super.CurrentLocation as pcf.PolicyOverview
    }
    
    property get plcyPeriod () : PolicyPeriod {
      return getVariableValue("plcyPeriod", 0) as PolicyPeriod
    }
    
    property set plcyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("plcyPeriod", 0, $arg)
    }
    
    
  }
  
  
}