package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/producer/AgencyCommissionPaymentWizard_SelectProducerScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyCommissionPaymentWizard_SelectProducerScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/producer/AgencyCommissionPaymentWizard_SelectProducerScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyCommissionPaymentWizard_SelectProducerScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on MenuItem (id=SelectProducerPicker) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 33, column 39
    function action_2 () : void {
      ProducerSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 50, column 48
    function action_9 () : void {
      NewPaymentInstrumentPopup.push(gw.payment.PaymentInstrumentFilters.agencyPaymentInstrumentOptions,incomingProducerPayment.Producer,true)
    }
    
    // 'action' attribute on MenuItem (id=CreateNewPaymentInstrument) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 50, column 48
    function action_dest_10 () : pcf.api.Destination {
      return pcf.NewPaymentInstrumentPopup.createDestination(gw.payment.PaymentInstrumentFilters.agencyPaymentInstrumentOptions,incomingProducerPayment.Producer,true)
    }
    
    // 'action' attribute on MenuItem (id=SelectProducerPicker) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 33, column 39
    function action_dest_3 () : pcf.api.Destination {
      return pcf.ProducerSearchPopup.createDestination()
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 71, column 51
    function currency_26 () : typekey.Currency {
      return incomingProducerPayment.Currency
    }
    
    // 'value' attribute on RangeInput (id=PaymentInstrument_Input) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 50, column 48
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      incomingProducerPayment.PaymentInstrument = (__VALUE_TO_SET as entity.PaymentInstrument)
    }
    
    // 'value' attribute on TextInput (id=RefNumber_Input) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 62, column 53
    function defaultSetter_20 (__VALUE_TO_SET :  java.lang.Object) : void {
      incomingProducerPayment.RefNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 71, column 51
    function defaultSetter_24 (__VALUE_TO_SET :  java.lang.Object) : void {
      incomingProducerPayment.Amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextInput (id=SelectProducer_Input) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 33, column 39
    function defaultSetter_6 (__VALUE_TO_SET :  java.lang.Object) : void {
      incomingProducerPayment.Producer = (__VALUE_TO_SET as entity.Producer)
    }
    
    // 'initialValue' attribute on Variable at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 13, column 59
    function initialValue_0 () : gw.api.web.producer.ProducerSearchConverter {
      return new gw.api.web.producer.ProducerSearchConverter()
    }
    
    // 'initialValue' attribute on Variable at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 17, column 49
    function initialValue_1 () : gw.payment.PaymentInstrumentRange {
      return new gw.payment.PaymentInstrumentRange(incomingProducerPayment.Producer.PaymentInstruments)
    }
    
    // 'inputConversion' attribute on TextInput (id=SelectProducer_Input) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 33, column 39
    function inputConversion_4 (VALUE :  java.lang.String) : java.lang.Object {
      return producerSearchConverter.getProducer(VALUE)
    }
    
    // 'onPick' attribute on RangeInput (id=PaymentInstrument_Input) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 50, column 48
    function onPick_11 (PickedValue :  PaymentInstrument) : void {
      paymentInstrumentRange.addPaymentInstrument(incomingProducerPayment.PaymentInstrument)
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 50, column 48
    function valueRange_15 () : java.lang.Object {
      return gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, gw.payment.PaymentInstrumentFilters.agencyPaymentInstrumentFilter)
    }
    
    // 'value' attribute on TextInput (id=SelectProducer_Input) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 33, column 39
    function valueRoot_7 () : java.lang.Object {
      return incomingProducerPayment
    }
    
    // 'value' attribute on RangeInput (id=PaymentInstrument_Input) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 50, column 48
    function value_12 () : entity.PaymentInstrument {
      return incomingProducerPayment.PaymentInstrument
    }
    
    // 'value' attribute on TextInput (id=RefNumber_Input) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 62, column 53
    function value_19 () : java.lang.String {
      return incomingProducerPayment.RefNumber
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 71, column 51
    function value_23 () : gw.pl.currency.MonetaryAmount {
      return incomingProducerPayment.Amount
    }
    
    // 'value' attribute on TextInput (id=SelectProducer_Input) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 33, column 39
    function value_5 () : entity.Producer {
      return incomingProducerPayment.Producer
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 50, column 48
    function verifyValueRangeIsAllowedType_16 ($$arg :  entity.PaymentInstrument[]) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 50, column 48
    function verifyValueRangeIsAllowedType_16 ($$arg :  gw.api.database.IQueryBeanResult<entity.PaymentInstrument>) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 50, column 48
    function verifyValueRangeIsAllowedType_16 ($$arg :  java.util.List) : void {
      // No-op:  This method is only for verification purposes and is never actually executed
    }
    
    // 'valueRange' attribute on RangeInput (id=PaymentInstrument_Input) at AgencyCommissionPaymentWizard_SelectProducerScreen.pcf: line 50, column 48
    function verifyValueRange_17 () : void {
      var __valueRangeArg = gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, gw.payment.PaymentInstrumentFilters.agencyPaymentInstrumentFilter)
      // If this call fails to compile, possibly with an error saying it's an ambiguous method call,
      // that means that the type of the valueRange is not compatible with the valueType 
      // The valueRange must be an array, list or query whose member type matches the valueType
      verifyValueRangeIsAllowedType_16(__valueRangeArg)
    }
    
    property get incomingProducerPayment () : IncomingProducerPayment {
      return getRequireValue("incomingProducerPayment", 0) as IncomingProducerPayment
    }
    
    property set incomingProducerPayment ($arg :  IncomingProducerPayment) {
      setRequireValue("incomingProducerPayment", 0, $arg)
    }
    
    property get paymentInstrumentRange () : gw.payment.PaymentInstrumentRange {
      return getVariableValue("paymentInstrumentRange", 0) as gw.payment.PaymentInstrumentRange
    }
    
    property set paymentInstrumentRange ($arg :  gw.payment.PaymentInstrumentRange) {
      setVariableValue("paymentInstrumentRange", 0, $arg)
    }
    
    property get producerSearchConverter () : gw.api.web.producer.ProducerSearchConverter {
      return getVariableValue("producerSearchConverter", 0) as gw.api.web.producer.ProducerSearchConverter
    }
    
    property set producerSearchConverter ($arg :  gw.api.web.producer.ProducerSearchConverter) {
      setVariableValue("producerSearchConverter", 0, $arg)
    }
    
    
  }
  
  
}