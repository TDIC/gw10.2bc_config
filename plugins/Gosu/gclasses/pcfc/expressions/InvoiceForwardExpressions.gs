package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/invoice/InvoiceForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class InvoiceForwardExpressions {
  @javax.annotation.Generated("config/web/pcf/invoice/InvoiceForward.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class InvoiceForwardExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (invoice :  Invoice) : int {
      return 0
    }
    
    static function __constructorIndex (invoice :  Invoice, account :  Account) : int {
      return 1
    }
    
    // 'action' attribute on ForwardCondition at InvoiceForward.pcf: line 23, column 33
    function action_1 () : void {
      AgencyBillStatementDetail.go( ( invoice as StatementInvoice ).AgencyBillCycle )
    }
    
    // 'action' attribute on ForwardCondition at InvoiceForward.pcf: line 26, column 34
    function action_4 () : void {
      AccountDetailInvoices.go(account, invoice as AccountInvoice)
    }
    
    // 'action' attribute on ForwardCondition at InvoiceForward.pcf: line 23, column 33
    function action_dest_2 () : pcf.api.Destination {
      return pcf.AgencyBillStatementDetail.createDestination( ( invoice as StatementInvoice ).AgencyBillCycle )
    }
    
    // 'action' attribute on ForwardCondition at InvoiceForward.pcf: line 26, column 34
    function action_dest_5 () : pcf.api.Destination {
      return pcf.AccountDetailInvoices.createDestination(account, invoice as AccountInvoice)
    }
    
    // 'condition' attribute on ForwardCondition at InvoiceForward.pcf: line 23, column 33
    function condition_3 () : java.lang.Boolean {
      return isAgencyBill
    }
    
    // 'condition' attribute on ForwardCondition at InvoiceForward.pcf: line 26, column 34
    function condition_6 () : java.lang.Boolean {
      return !isAgencyBill
    }
    
    // 'initialValue' attribute on Variable at InvoiceForward.pcf: line 20, column 23
    function initialValue_0 () : boolean {
      return invoice typeis StatementInvoice
    }
    
    override property get CurrentLocation () : pcf.InvoiceForward {
      return super.CurrentLocation as pcf.InvoiceForward
    }
    
    property get account () : Account {
      return getVariableValue("account", 0) as Account
    }
    
    property set account ($arg :  Account) {
      setVariableValue("account", 0, $arg)
    }
    
    property get invoice () : Invoice {
      return getVariableValue("invoice", 0) as Invoice
    }
    
    property set invoice ($arg :  Invoice) {
      setVariableValue("invoice", 0, $arg)
    }
    
    property get isAgencyBill () : boolean {
      return getVariableValue("isAgencyBill", 0) as java.lang.Boolean
    }
    
    property set isAgencyBill ($arg :  boolean) {
      setVariableValue("isAgencyBill", 0, $arg)
    }
    
    
  }
  
  
}