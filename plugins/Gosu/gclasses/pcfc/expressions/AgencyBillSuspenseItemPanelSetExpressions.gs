package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillSuspenseItemPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillSuspenseItemPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillSuspenseItemPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillSuspenseItemPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'allCheckedRowsAction' attribute on CheckedValuesToolbarButton (id=ReleaseItems) at AgencyBillSuspenseItemPanelSet.pcf: line 22, column 92
    function allCheckedRowsAction_1 (CheckedValues :  entity.BaseSuspDistItem[], CheckedValuesErrors :  java.util.Map) : void {
      gw.agencybill.AgencyBillSuspenseItemsHelper.release(CheckedValues)
    }
    
    // 'available' attribute on CheckedValuesToolbarButton (id=ReleaseItems) at AgencyBillSuspenseItemPanelSet.pcf: line 22, column 92
    function available_0 () : java.lang.Boolean {
      return perm.System.prodsuspitemsedit
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillSuspenseItemPanelSet.pcf: line 29, column 95
    function filter_2 () : gw.api.filters.IFilter {
      return new gw.api.web.producer.agencybill.AgencyBillSuspenseItemsFilters.All()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillSuspenseItemPanelSet.pcf: line 32, column 35
    function filter_3 () : gw.api.filters.IFilter {
      return new gw.api.web.producer.agencybill.AgencyBillSuspenseItemsFilters.Executed()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillSuspenseItemPanelSet.pcf: line 35, column 48
    function filter_4 () : gw.api.filters.IFilter {
      return new gw.api.web.producer.agencybill.AgencyBillSuspenseItemsFilters.Released()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillSuspenseItemPanelSet.pcf: line 37, column 100
    function filter_6 () : gw.api.filters.IFilter {
      return new gw.api.web.producer.agencybill.AgencyBillSuspenseItemsFilters.Reversed()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillSuspenseItemPanelSet.pcf: line 39, column 97
    function filter_7 () : gw.api.filters.IFilter {
      return new gw.api.web.producer.agencybill.AgencyBillSuspenseItemsFilters.Saved()
    }
    
    // 'filter' attribute on ToolbarFilterOption at AgencyBillSuspenseItemPanelSet.pcf: line 42, column 48
    function filter_8 () : gw.api.filters.IFilter {
      return new gw.api.web.producer.agencybill.AgencyBillSuspenseItemsFilters.Applied()
    }
    
    // 'value' attribute on DateCell (id=ReceivedDate_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 73, column 77
    function sortValue_10 (suspenseItem :  entity.BaseSuspDistItem) : java.lang.Object {
      return suspenseItem.BaseDist.BaseMoneyReceived.ReceivedDate
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 77, column 44
    function sortValue_11 (suspenseItem :  entity.BaseSuspDistItem) : java.lang.Object {
      return suspenseItem.Status
    }
    
    // 'value' attribute on DateCell (id=ExecutionDate_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 81, column 50
    function sortValue_12 (suspenseItem :  entity.BaseSuspDistItem) : java.lang.Object {
      return suspenseItem.ExecutedDate
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 86, column 50
    function sortValue_13 (suspenseItem :  entity.BaseSuspDistItem) : java.lang.Object {
      return suspenseItem.PolicyNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmount_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 93, column 56
    function sortValue_14 (suspenseItem :  entity.BaseSuspDistItem) : java.lang.Object {
      return suspenseItem.GrossAmountToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionAmount_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 99, column 61
    function sortValue_15 (suspenseItem :  entity.BaseSuspDistItem) : java.lang.Object {
      return suspenseItem.CommissionAmountToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetAmount_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 105, column 54
    function sortValue_16 (suspenseItem :  entity.BaseSuspDistItem) : java.lang.Object {
      return suspenseItem.NetAmountToApply
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 110, column 53
    function sortValue_17 (suspenseItem :  entity.BaseSuspDistItem) : java.lang.Object {
      return suspenseItem.PaymentComments
    }
    
    // 'value' attribute on RowIterator (id=AgencyBillSuspenseItemsIterator) at AgencyBillSuspenseItemPanelSet.pcf: line 56, column 49
    function value_49 () : entity.BaseSuspDistItem[] {
      return suspenseItems
    }
    
    // 'visible' attribute on ToolbarFilterOption at AgencyBillSuspenseItemPanelSet.pcf: line 35, column 48
    function visible_5 () : java.lang.Boolean {
      return suspenseType == Payment
    }
    
    // 'visible' attribute on ToolbarFilterOption at AgencyBillSuspenseItemPanelSet.pcf: line 42, column 48
    function visible_9 () : java.lang.Boolean {
      return suspenseType == Promise
    }
    
    property get suspenseItems () : BaseSuspDistItem[] {
      return getRequireValue("suspenseItems", 0) as BaseSuspDistItem[]
    }
    
    property set suspenseItems ($arg :  BaseSuspDistItem[]) {
      setRequireValue("suspenseItems", 0, $arg)
    }
    
    property get suspenseType () : gw.agencybill.AgencyBillSuspenseType {
      return getRequireValue("suspenseType", 0) as gw.agencybill.AgencyBillSuspenseType
    }
    
    property set suspenseType ($arg :  gw.agencybill.AgencyBillSuspenseType) {
      setRequireValue("suspenseType", 0, $arg)
    }
    
    function isEditable(suspenseItem : BaseSuspDistItem):boolean {
      return suspenseItem.HasContainingDistribution and !suspenseItem.BaseDist.Reversed and (suspenseType == Payment or !(suspenseItem.BaseDist as AgencyCyclePromise).Applied) and !suspenseItem.isReleased()
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillSuspenseItemPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AgencyBillSuspenseItemPanelSetExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on Link (id=GoToContainingPayment) at AgencyBillSuspenseItemPanelSet.pcf: line 68, column 125
    function action_19 () : void {
      suspenseItem.goToSuspenseItemPopup()
    }
    
    // 'available' attribute on Link (id=GoToContainingPayment) at AgencyBillSuspenseItemPanelSet.pcf: line 68, column 125
    function available_18 () : java.lang.Boolean {
      return isEditable(suspenseItem)
    }
    
    // 'checkBoxVisible' attribute on RowIterator (id=AgencyBillSuspenseItemsIterator) at AgencyBillSuspenseItemPanelSet.pcf: line 56, column 49
    function checkBoxVisible_48 () : java.lang.Boolean {
      return suspenseItem.CanRelease
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=GrossAmount_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 93, column 56
    function currency_35 () : typekey.Currency {
      return suspenseItem.Currency
    }
    
    // 'tooltip' attribute on Link (id=GoToContainingPayment) at AgencyBillSuspenseItemPanelSet.pcf: line 68, column 125
    function tooltip_20 () : java.lang.String {
      return isEditable(suspenseItem) ? DisplayKey.get("Web.AgencyBillSuspenseItems.Tooltip"): null
    }
    
    // 'value' attribute on DateCell (id=ReceivedDate_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 73, column 77
    function valueRoot_22 () : java.lang.Object {
      return suspenseItem.BaseDist.BaseMoneyReceived
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 77, column 44
    function valueRoot_25 () : java.lang.Object {
      return suspenseItem
    }
    
    // 'value' attribute on DateCell (id=ReceivedDate_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 73, column 77
    function value_21 () : java.util.Date {
      return suspenseItem.BaseDist.BaseMoneyReceived.ReceivedDate
    }
    
    // 'value' attribute on TextCell (id=Status_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 77, column 44
    function value_24 () : java.lang.String {
      return suspenseItem.Status
    }
    
    // 'value' attribute on DateCell (id=ExecutionDate_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 81, column 50
    function value_27 () : java.util.Date {
      return suspenseItem.ExecutedDate
    }
    
    // 'value' attribute on TextCell (id=PolicyNumber_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 86, column 50
    function value_30 () : java.lang.String {
      return suspenseItem.PolicyNumber
    }
    
    // 'value' attribute on MonetaryAmountCell (id=GrossAmount_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 93, column 56
    function value_33 () : gw.pl.currency.MonetaryAmount {
      return suspenseItem.GrossAmountToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=CommissionAmount_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 99, column 61
    function value_37 () : gw.pl.currency.MonetaryAmount {
      return suspenseItem.CommissionAmountToApply
    }
    
    // 'value' attribute on MonetaryAmountCell (id=NetAmount_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 105, column 54
    function value_41 () : gw.pl.currency.MonetaryAmount {
      return suspenseItem.NetAmountToApply
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at AgencyBillSuspenseItemPanelSet.pcf: line 110, column 53
    function value_45 () : java.lang.String {
      return suspenseItem.PaymentComments
    }
    
    property get suspenseItem () : entity.BaseSuspDistItem {
      return getIteratedValue(1) as entity.BaseSuspDistItem
    }
    
    
  }
  
  
}