package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillDistArchiveWarningPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AgencyBillDistArchiveWarningPanelSetExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/AgencyBillDistArchiveWarningPanelSet.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AgencyBillDistArchiveWarningPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'value' attribute on TextInput (id=spaceInput_Input) at AgencyBillDistArchiveWarningPanelSet.pcf: line 26, column 41
    function value_0 () : java.lang.Object {
      return null
    }
    
    // 'visible' attribute on PanelSet (id=AgencyBillDistArchiveWarningPanelSet) at AgencyBillDistArchiveWarningPanelSet.pcf: line 7, column 27
    function visible_2 () : java.lang.Boolean {
      return dist.Frozen
    }
    
    property get dist () : BaseDist {
      return getRequireValue("dist", 0) as BaseDist
    }
    
    property set dist ($arg :  BaseDist) {
      setRequireValue("dist", 0, $arg)
    }
    
    
  }
  
  
}