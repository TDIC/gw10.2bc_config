package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/agencybill/CycleExceptionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CycleExceptionsLVExpressions {
  @javax.annotation.Generated("config/web/pcf/agencybill/CycleExceptionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CycleExceptionsLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on MonetaryAmountCell (id=UnpaidAmount_Cell) at CycleExceptionsLV.pcf: line 59, column 128
    function label_5 () : java.lang.Object {
      return isLatePayments ? DisplayKey.get("Web.AgencyBillExceptions.OtherExceptions.UnpaidAmount") : DisplayKey.get("Web.AgencyBillExceptions.OtherExceptions.UnpromisedAmount")
    }
    
    // 'value' attribute on TextCell (id=statementNumber_Cell) at CycleExceptionsLV.pcf: line 36, column 51
    function sortValue_0 (agencyCycleWithException :  entity.AgencyCycleProcess) : java.lang.Object {
      var statementInvoice : entity.StatementInvoice = (agencyCycleWithException.AgencyBillCycle.StatementInvoice)
return statementInvoice.InvoiceNumber
    }
    
    // 'value' attribute on DateCell (id=statementDate_Cell) at CycleExceptionsLV.pcf: line 40, column 47
    function sortValue_1 (agencyCycleWithException :  entity.AgencyCycleProcess) : java.lang.Object {
      var statementInvoice : entity.StatementInvoice = (agencyCycleWithException.AgencyBillCycle.StatementInvoice)
return statementInvoice.EventDate
    }
    
    // 'sortBy' attribute on TextCell (id=statementStatus_Cell) at CycleExceptionsLV.pcf: line 45, column 49
    function sortValue_2 (agencyCycleWithException :  entity.AgencyCycleProcess) : java.lang.Object {
      var statementInvoice : entity.StatementInvoice = (agencyCycleWithException.AgencyBillCycle.StatementInvoice)
return statementInvoice.Status
    }
    
    // 'value' attribute on DateCell (id=dueDate_Cell) at CycleExceptionsLV.pcf: line 49, column 57
    function sortValue_3 (agencyCycleWithException :  entity.AgencyCycleProcess) : java.lang.Object {
      return getDueDate(agencyCycleWithException)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=StatementAmount_Cell) at CycleExceptionsLV.pcf: line 54, column 47
    function sortValue_4 (agencyCycleWithException :  entity.AgencyCycleProcess) : java.lang.Object {
      var statementInvoice : entity.StatementInvoice = (agencyCycleWithException.AgencyBillCycle.StatementInvoice)
return statementInvoice.NetAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnpaidAmount_Cell) at CycleExceptionsLV.pcf: line 59, column 128
    function sortValue_6 (agencyCycleWithException :  entity.AgencyCycleProcess) : java.lang.Object {
      var statementInvoice : entity.StatementInvoice = (agencyCycleWithException.AgencyBillCycle.StatementInvoice)
return isLatePayments ? statementInvoice.NetAmountUnsettled : statementInvoice.NetAmountUnsettledIncludingPromises
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at CycleExceptionsLV.pcf: line 64, column 134
    function sortValue_7 (agencyCycleWithException :  entity.AgencyCycleProcess) : java.lang.Object {
      return isLatePayments? agencyCycleWithException.PaymentPastDueComments : agencyCycleWithException.PromisePastDueComments
    }
    
    // 'value' attribute on RowIterator at CycleExceptionsLV.pcf: line 24, column 47
    function value_34 () : entity.AgencyCycleProcess[] {
      return agencyCyclesWithException
    }
    
    property get agencyCyclesWithException () : AgencyCycleProcess[] {
      return getRequireValue("agencyCyclesWithException", 0) as AgencyCycleProcess[]
    }
    
    property set agencyCyclesWithException ($arg :  AgencyCycleProcess[]) {
      setRequireValue("agencyCyclesWithException", 0, $arg)
    }
    
    property get isLatePayments () : boolean {
      return getRequireValue("isLatePayments", 0) as java.lang.Boolean
    }
    
    property set isLatePayments ($arg :  boolean) {
      setRequireValue("isLatePayments", 0, $arg)
    }
    
    
    
    function getDueDate(agencyCycleWithException : AgencyCycleProcess) : java.util.Date {
      return isLatePayments ? agencyCycleWithException.AgencyBillCycle.StatementInvoice.DueDate : agencyCycleWithException.AgencyBillCycle.StatementInvoice.PromiseDueDate
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/agencybill/CycleExceptionsLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends CycleExceptionsLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextCell (id=statementNumber_Cell) at CycleExceptionsLV.pcf: line 36, column 51
    function action_9 () : void {
      AgencyBillStatementDetail.go(agencyCycleWithException.AgencyBillCycle)
    }
    
    // 'action' attribute on TextCell (id=statementNumber_Cell) at CycleExceptionsLV.pcf: line 36, column 51
    function action_dest_10 () : pcf.api.Destination {
      return pcf.AgencyBillStatementDetail.createDestination(agencyCycleWithException.AgencyBillCycle)
    }
    
    // 'currency' attribute on MonetaryAmountCell (id=StatementAmount_Cell) at CycleExceptionsLV.pcf: line 54, column 47
    function currency_24 () : typekey.Currency {
      return statementInvoice.Currency
    }
    
    // 'initialValue' attribute on Variable at CycleExceptionsLV.pcf: line 28, column 41
    function initialValue_8 () : entity.StatementInvoice {
      return agencyCycleWithException.AgencyBillCycle.StatementInvoice
    }
    
    // RowIterator at CycleExceptionsLV.pcf: line 24, column 47
    function initializeVariables_33 () : void {
        statementInvoice = agencyCycleWithException.AgencyBillCycle.StatementInvoice;

    }
    
    // 'label' attribute on MonetaryAmountCell (id=UnpaidAmount_Cell) at CycleExceptionsLV.pcf: line 59, column 128
    function label_26 () : java.lang.Object {
      return isLatePayments ? DisplayKey.get("Web.AgencyBillExceptions.OtherExceptions.UnpaidAmount") : DisplayKey.get("Web.AgencyBillExceptions.OtherExceptions.UnpromisedAmount")
    }
    
    // 'value' attribute on TextCell (id=statementNumber_Cell) at CycleExceptionsLV.pcf: line 36, column 51
    function valueRoot_12 () : java.lang.Object {
      return statementInvoice
    }
    
    // 'value' attribute on TextCell (id=statementNumber_Cell) at CycleExceptionsLV.pcf: line 36, column 51
    function value_11 () : java.lang.String {
      return statementInvoice.InvoiceNumber
    }
    
    // 'value' attribute on DateCell (id=statementDate_Cell) at CycleExceptionsLV.pcf: line 40, column 47
    function value_14 () : java.util.Date {
      return statementInvoice.EventDate
    }
    
    // 'value' attribute on TextCell (id=statementStatus_Cell) at CycleExceptionsLV.pcf: line 45, column 49
    function value_17 () : java.lang.String {
      return statementInvoice.StatusForUI
    }
    
    // 'value' attribute on DateCell (id=dueDate_Cell) at CycleExceptionsLV.pcf: line 49, column 57
    function value_20 () : java.util.Date {
      return getDueDate(agencyCycleWithException)
    }
    
    // 'value' attribute on MonetaryAmountCell (id=StatementAmount_Cell) at CycleExceptionsLV.pcf: line 54, column 47
    function value_22 () : gw.pl.currency.MonetaryAmount {
      return statementInvoice.NetAmount
    }
    
    // 'value' attribute on MonetaryAmountCell (id=UnpaidAmount_Cell) at CycleExceptionsLV.pcf: line 59, column 128
    function value_27 () : gw.pl.currency.MonetaryAmount {
      return isLatePayments ? statementInvoice.NetAmountUnsettled : statementInvoice.NetAmountUnsettledIncludingPromises
    }
    
    // 'value' attribute on TextCell (id=Comments_Cell) at CycleExceptionsLV.pcf: line 64, column 134
    function value_31 () : java.lang.String {
      return isLatePayments? agencyCycleWithException.PaymentPastDueComments : agencyCycleWithException.PromisePastDueComments
    }
    
    property get agencyCycleWithException () : entity.AgencyCycleProcess {
      return getIteratedValue(1) as entity.AgencyCycleProcess
    }
    
    property get statementInvoice () : entity.StatementInvoice {
      return getVariableValue("statementInvoice", 1) as entity.StatementInvoice
    }
    
    property set statementInvoice ($arg :  entity.StatementInvoice) {
      setVariableValue("statementInvoice", 1, $arg)
    }
    
    
  }
  
  
}