package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/account/CreateSubrogationConfirmScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class CreateSubrogationConfirmScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/account/CreateSubrogationConfirmScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class CreateSubrogationConfirmScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'action' attribute on TextInput (id=Account_Input) at CreateSubrogationConfirmScreen.pcf: line 24, column 60
    function action_0 () : void {
      AccountSummaryPopup.push(subrogation.SourceAccount)
    }
    
    // 'action' attribute on TextInput (id=Account_Input) at CreateSubrogationConfirmScreen.pcf: line 24, column 60
    function action_dest_1 () : pcf.api.Destination {
      return pcf.AccountSummaryPopup.createDestination(subrogation.SourceAccount)
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at CreateSubrogationConfirmScreen.pcf: line 42, column 39
    function currency_13 () : typekey.Currency {
      return subrogation.Currency
    }
    
    // 'value' attribute on TextInput (id=Account_Input) at CreateSubrogationConfirmScreen.pcf: line 24, column 60
    function valueRoot_3 () : java.lang.Object {
      return subrogation.SourceAccount
    }
    
    // 'value' attribute on TextInput (id=ClaimNumber_Input) at CreateSubrogationConfirmScreen.pcf: line 34, column 44
    function valueRoot_9 () : java.lang.Object {
      return subrogation
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at CreateSubrogationConfirmScreen.pcf: line 42, column 39
    function value_11 () : gw.pl.currency.MonetaryAmount {
      return subrogation.Amount
    }
    
    // 'value' attribute on TextInput (id=Account_Input) at CreateSubrogationConfirmScreen.pcf: line 24, column 60
    function value_2 () : java.lang.String {
      return subrogation.SourceAccount.AccountNumber
    }
    
    // 'value' attribute on TextInput (id=AccountInformation_Input) at CreateSubrogationConfirmScreen.pcf: line 28, column 67
    function value_5 () : java.lang.String {
      return subrogation.SourceAccount.AccountNameLocalized
    }
    
    // 'value' attribute on TextInput (id=ClaimNumber_Input) at CreateSubrogationConfirmScreen.pcf: line 34, column 44
    function value_8 () : java.lang.String {
      return subrogation.ClaimNumber
    }
    
    property get subrogation () : Subrogation {
      return getRequireValue("subrogation", 0) as Subrogation
    }
    
    property set subrogation ($arg :  Subrogation) {
      setRequireValue("subrogation", 0, $arg)
    }
    
    
  }
  
  
}