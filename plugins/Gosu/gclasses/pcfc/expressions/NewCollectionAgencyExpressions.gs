package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/admin/collection/NewCollectionAgency.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class NewCollectionAgencyExpressions {
  @javax.annotation.Generated("config/web/pcf/admin/collection/NewCollectionAgency.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class NewCollectionAgencyExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex () : int {
      return 0
    }
    
    // 'afterCancel' attribute on Page (id=NewCollectionAgency) at NewCollectionAgency.pcf: line 14, column 71
    function afterCancel_4 () : void {
      CollectionAgencies.go()
    }
    
    // 'afterCancel' attribute on Page (id=NewCollectionAgency) at NewCollectionAgency.pcf: line 14, column 71
    function afterCancel_dest_5 () : pcf.api.Destination {
      return pcf.CollectionAgencies.createDestination()
    }
    
    // 'afterCommit' attribute on Page (id=NewCollectionAgency) at NewCollectionAgency.pcf: line 14, column 71
    function afterCommit_6 (TopLocation :  pcf.api.Location) : void {
      CollectionAgencies.go()
    }
    
    // 'canVisit' attribute on Page (id=NewCollectionAgency) at NewCollectionAgency.pcf: line 14, column 71
    static function canVisit_7 () : java.lang.Boolean {
      return perm.System.admintabview and perm.System.colagencycreate
    }
    
    // 'def' attribute on PanelRef at NewCollectionAgency.pcf: line 27, column 59
    function def_onEnter_2 (def :  pcf.CollectionAgencyDetailDV) : void {
      def.onEnter(collectionAgency)
    }
    
    // 'def' attribute on PanelRef at NewCollectionAgency.pcf: line 27, column 59
    function def_refreshVariables_3 (def :  pcf.CollectionAgencyDetailDV) : void {
      def.refreshVariables(collectionAgency)
    }
    
    // 'initialValue' attribute on Variable at NewCollectionAgency.pcf: line 20, column 32
    function initialValue_0 () : CollectionAgency {
      return initCollectionAgency()
    }
    
    // EditButtons at NewCollectionAgency.pcf: line 24, column 23
    function label_1 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'parent' attribute on Page (id=NewCollectionAgency) at NewCollectionAgency.pcf: line 14, column 71
    static function parent_8 () : pcf.api.Destination {
      return pcf.CollectionAgencies.createDestination()
    }
    
    override property get CurrentLocation () : pcf.NewCollectionAgency {
      return super.CurrentLocation as pcf.NewCollectionAgency
    }
    
    property get collectionAgency () : CollectionAgency {
      return getVariableValue("collectionAgency", 0) as CollectionAgency
    }
    
    property set collectionAgency ($arg :  CollectionAgency) {
      setVariableValue("collectionAgency", 0, $arg)
    }
    
    
    function initCollectionAgency() : CollectionAgency {
            var newCollectionAgency = new CollectionAgency();
            newCollectionAgency.Name = "";
            return newCollectionAgency;
          }
        
    
    
  }
  
  
}