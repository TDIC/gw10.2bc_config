package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadUserGroupLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class AdminDataUploadUserGroupLVExpressions {
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadUserGroupLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class AdminDataUploadUserGroupLVExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadUserGroupLV.pcf: line 23, column 46
    function label_0 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at AdminDataUploadUserGroupLV.pcf: line 29, column 41
    function label_3 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserGroup.UserName")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadUserGroupLV.pcf: line 35, column 41
    function label_5 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserGroup.UserPublicID")
    }
    
    // 'label' attribute on TextCell (id=groups_Cell) at AdminDataUploadUserGroupLV.pcf: line 40, column 42
    function label_7 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserGroup.NumberOfGroups")
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadUserGroupLV.pcf: line 23, column 46
    function sortValue_1 (userGroup :  tdic.util.dataloader.data.admindata.UserGroupData) : java.lang.Object {
      return processor.getLoadStatus(userGroup)
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadUserGroupLV.pcf: line 29, column 41
    function sortValue_4 (userGroup :  tdic.util.dataloader.data.admindata.UserGroupData) : java.lang.Object {
      return userGroup.UserReferenceName
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadUserGroupLV.pcf: line 35, column 41
    function sortValue_6 (userGroup :  tdic.util.dataloader.data.admindata.UserGroupData) : java.lang.Object {
      return userGroup.UserPublicID
    }
    
    // 'value' attribute on TextCell (id=groups_Cell) at AdminDataUploadUserGroupLV.pcf: line 40, column 42
    function sortValue_8 (userGroup :  tdic.util.dataloader.data.admindata.UserGroupData) : java.lang.Object {
      return userGroup.LoadFactor.countWhere(\ o  -> o !=null and o as String != ""  )
    }
    
    // 'value' attribute on RowIterator (id=userGroupID) at AdminDataUploadUserGroupLV.pcf: line 15, column 96
    function value_29 () : java.util.ArrayList<tdic.util.dataloader.data.admindata.UserGroupData> {
      return processor.UserGroupArray
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadUserGroupLV.pcf: line 23, column 46
    function visible_2 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get processor () : tdic.util.dataloader.processor.BCAdminDataLoaderProcessor {
      return getRequireValue("processor", 0) as tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
    }
    
    property set processor ($arg :  tdic.util.dataloader.processor.BCAdminDataLoaderProcessor) {
      setRequireValue("processor", 0, $arg)
    }
    
    
  }
  
  @javax.annotation.Generated("config/web/pcf/DataLoader/AdminDataLoader/AdminDataUploadUserGroupLV.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class IteratorEntryExpressionsImpl extends AdminDataUploadUserGroupLVExpressionsImpl {
    public construct(widget :  Object) {
      super(widget, 1)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'highlighted' attribute on Row at AdminDataUploadUserGroupLV.pcf: line 17, column 98
    function highlighted_28 () : java.lang.Boolean {
      return (userGroup.Error or userGroup.Skipped)  && processor.LoadCompleted
    }
    
    // 'label' attribute on TextCell (id=name_Cell) at AdminDataUploadUserGroupLV.pcf: line 29, column 41
    function label_14 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserGroup.UserName")
    }
    
    // 'label' attribute on TextCell (id=publicID_Cell) at AdminDataUploadUserGroupLV.pcf: line 35, column 41
    function label_19 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserGroup.UserPublicID")
    }
    
    // 'label' attribute on TextCell (id=groups_Cell) at AdminDataUploadUserGroupLV.pcf: line 40, column 42
    function label_24 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.AdminData.UserGroup.NumberOfGroups")
    }
    
    // 'label' attribute on TextCell (id=status_Cell) at AdminDataUploadUserGroupLV.pcf: line 23, column 46
    function label_9 () : java.lang.Object {
      return gw.api.locale.DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus")
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadUserGroupLV.pcf: line 29, column 41
    function valueRoot_16 () : java.lang.Object {
      return userGroup
    }
    
    // 'value' attribute on TextCell (id=status_Cell) at AdminDataUploadUserGroupLV.pcf: line 23, column 46
    function value_10 () : java.lang.String {
      return processor.getLoadStatus(userGroup)
    }
    
    // 'value' attribute on TextCell (id=name_Cell) at AdminDataUploadUserGroupLV.pcf: line 29, column 41
    function value_15 () : java.lang.String {
      return userGroup.UserReferenceName
    }
    
    // 'value' attribute on TextCell (id=publicID_Cell) at AdminDataUploadUserGroupLV.pcf: line 35, column 41
    function value_20 () : java.lang.String {
      return userGroup.UserPublicID
    }
    
    // 'value' attribute on TextCell (id=groups_Cell) at AdminDataUploadUserGroupLV.pcf: line 40, column 42
    function value_25 () : java.lang.Integer {
      return userGroup.LoadFactor.countWhere(\ o  -> o !=null and o as String != ""  )
    }
    
    // 'visible' attribute on TextCell (id=status_Cell) at AdminDataUploadUserGroupLV.pcf: line 23, column 46
    function visible_11 () : java.lang.Boolean {
      return processor.LoadCompleted
    }
    
    property get userGroup () : tdic.util.dataloader.data.admindata.UserGroupData {
      return getIteratedValue(1) as tdic.util.dataloader.data.admindata.UserGroupData
    }
    
    
  }
  
  
}