package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/suspensepayment/EditSuspensePaymentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class EditSuspensePaymentPopupExpressions {
  @javax.annotation.Generated("config/web/pcf/suspensepayment/EditSuspensePaymentPopup.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class EditSuspensePaymentPopupExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (suspensePayment :  SuspensePayment) : int {
      return 0
    }
    
    // 'action' attribute on MenuItem (id=PolicyPicker) at EditSuspensePaymentPopup.pcf: line 86, column 36
    function action_19 () : void {
      PolicySearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at EditSuspensePaymentPopup.pcf: line 68, column 37
    function action_9 () : void {
      AccountSearchPopup.push()
    }
    
    // 'action' attribute on MenuItem (id=AccountPicker) at EditSuspensePaymentPopup.pcf: line 68, column 37
    function action_dest_10 () : pcf.api.Destination {
      return pcf.AccountSearchPopup.createDestination()
    }
    
    // 'action' attribute on MenuItem (id=PolicyPicker) at EditSuspensePaymentPopup.pcf: line 86, column 36
    function action_dest_20 () : pcf.api.Destination {
      return pcf.PolicySearchPopup.createDestination()
    }
    
    // 'beforeCommit' attribute on Popup (id=EditSuspensePaymentPopup) at EditSuspensePaymentPopup.pcf: line 10, column 76
    function beforeCommit_44 (pickedValue :  java.lang.Object) : void {
      finalizeSuspensePayment()
    }
    
    // 'conversionExpression' attribute on TextInput (id=AccountNumber_Input) at EditSuspensePaymentPopup.pcf: line 68, column 37
    function conversionExpression_11 (PickedValue :  Account) : java.lang.String {
      return (PickedValue as Account).AccountNumber
    }
    
    // 'conversionExpression' attribute on TextInput (id=PolicyNumber_Input) at EditSuspensePaymentPopup.pcf: line 86, column 36
    function conversionExpression_21 (PickedValue :  PolicyPeriod) : java.lang.String {
      return (PickedValue as PolicyPeriod).PolicyNumber
    }
    
    // 'currency' attribute on MonetaryAmountInput (id=Amount_Input) at EditSuspensePaymentPopup.pcf: line 121, column 29
    function currency_31 () : typekey.Currency {
      return suspensePayment.Currency
    }
    
    // 'def' attribute on PanelRef at EditSuspensePaymentPopup.pcf: line 141, column 63
    function def_onEnter_42 (def :  pcf.SuspensePaymentTransactionsLV) : void {
      def.onEnter(suspensePayment)
    }
    
    // 'def' attribute on PanelRef at EditSuspensePaymentPopup.pcf: line 141, column 63
    function def_refreshVariables_43 (def :  pcf.SuspensePaymentTransactionsLV) : void {
      def.refreshVariables(suspensePayment)
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at EditSuspensePaymentPopup.pcf: line 68, column 37
    function defaultSetter_13 (__VALUE_TO_SET :  java.lang.Object) : void {
      accountNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on Choice (id=AccountChoice) at EditSuspensePaymentPopup.pcf: line 62, column 56
    function defaultSetter_17 (__VALUE_TO_SET :  java.lang.Object) : void {
      applyTo = (__VALUE_TO_SET as typekey.SuspensePaymentApplyTo)
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at EditSuspensePaymentPopup.pcf: line 86, column 36
    function defaultSetter_23 (__VALUE_TO_SET :  java.lang.Object) : void {
      policyNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at EditSuspensePaymentPopup.pcf: line 121, column 29
    function defaultSetter_30 (__VALUE_TO_SET :  java.lang.Object) : void {
      amount = (__VALUE_TO_SET as gw.pl.currency.MonetaryAmount)
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at EditSuspensePaymentPopup.pcf: line 127, column 34
    function defaultSetter_34 (__VALUE_TO_SET :  java.lang.Object) : void {
      description = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=InvoiceNumber_Input) at EditSuspensePaymentPopup.pcf: line 132, column 36
    function defaultSetter_37 (__VALUE_TO_SET :  java.lang.Object) : void {
      invoiceNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'value' attribute on TextInput (id=CheckNumber_Input) at EditSuspensePaymentPopup.pcf: line 137, column 34
    function defaultSetter_40 (__VALUE_TO_SET :  java.lang.Object) : void {
      checkNumber = (__VALUE_TO_SET as java.lang.String)
    }
    
    // 'initialValue' attribute on Variable at EditSuspensePaymentPopup.pcf: line 19, column 46
    function initialValue_0 () : typekey.SuspensePaymentApplyTo {
      return suspensePayment.CurrentApplyTo
    }
    
    // 'initialValue' attribute on Variable at EditSuspensePaymentPopup.pcf: line 23, column 32
    function initialValue_1 () : java.lang.String {
      return suspensePayment.AccountNumber
    }
    
    // 'initialValue' attribute on Variable at EditSuspensePaymentPopup.pcf: line 27, column 32
    function initialValue_2 () : java.lang.String {
      return suspensePayment.PolicyNumber
    }
    
    // 'initialValue' attribute on Variable at EditSuspensePaymentPopup.pcf: line 31, column 32
    function initialValue_3 () : java.lang.String {
      return suspensePayment.ProducerName
    }
    
    // 'initialValue' attribute on Variable at EditSuspensePaymentPopup.pcf: line 35, column 45
    function initialValue_4 () : gw.pl.currency.MonetaryAmount {
      return suspensePayment.Amount
    }
    
    // 'initialValue' attribute on Variable at EditSuspensePaymentPopup.pcf: line 39, column 32
    function initialValue_5 () : java.lang.String {
      return suspensePayment.Description
    }
    
    // 'initialValue' attribute on Variable at EditSuspensePaymentPopup.pcf: line 43, column 32
    function initialValue_6 () : java.lang.String {
      return suspensePayment.InvoiceNumber
    }
    
    // 'initialValue' attribute on Variable at EditSuspensePaymentPopup.pcf: line 47, column 32
    function initialValue_7 () : java.lang.String {
      return suspensePayment.RefNumber
    }
    
    // EditButtons at EditSuspensePaymentPopup.pcf: line 51, column 23
    function label_8 () : java.lang.Object {
      return gw.api.util.LocationUtil.hasOwnBundle(CurrentLocation) ? DisplayKey.get("Button.Update") : DisplayKey.get("Button.OK")
    }
    
    // 'option' attribute on Choice (id=AccountChoice) at EditSuspensePaymentPopup.pcf: line 62, column 56
    function option_15 () : java.lang.Object {
      return SuspensePaymentApplyTo.TC_ACCOUNT.Code
    }
    
    // 'option' attribute on Choice (id=PolicyChoice) at EditSuspensePaymentPopup.pcf: line 80, column 56
    function option_25 () : java.lang.Object {
      return SuspensePaymentApplyTo.TC_POLICY.Code
    }
    
    // 'value' attribute on TextInput (id=AccountNumber_Input) at EditSuspensePaymentPopup.pcf: line 68, column 37
    function value_12 () : java.lang.String {
      return accountNumber
    }
    
    // 'value' attribute on Choice (id=AccountChoice) at EditSuspensePaymentPopup.pcf: line 62, column 56
    function value_16 () : typekey.SuspensePaymentApplyTo {
      return applyTo
    }
    
    // 'value' attribute on TextInput (id=PolicyNumber_Input) at EditSuspensePaymentPopup.pcf: line 86, column 36
    function value_22 () : java.lang.String {
      return policyNumber
    }
    
    // 'value' attribute on MonetaryAmountInput (id=Amount_Input) at EditSuspensePaymentPopup.pcf: line 121, column 29
    function value_29 () : gw.pl.currency.MonetaryAmount {
      return amount
    }
    
    // 'value' attribute on TextAreaInput (id=Description_Input) at EditSuspensePaymentPopup.pcf: line 127, column 34
    function value_33 () : java.lang.String {
      return description
    }
    
    // 'value' attribute on TextInput (id=InvoiceNumber_Input) at EditSuspensePaymentPopup.pcf: line 132, column 36
    function value_36 () : java.lang.String {
      return invoiceNumber
    }
    
    // 'value' attribute on TextInput (id=CheckNumber_Input) at EditSuspensePaymentPopup.pcf: line 137, column 34
    function value_39 () : java.lang.String {
      return checkNumber
    }
    
    override property get CurrentLocation () : pcf.EditSuspensePaymentPopup {
      return super.CurrentLocation as pcf.EditSuspensePaymentPopup
    }
    
    property get accountNumber () : java.lang.String {
      return getVariableValue("accountNumber", 0) as java.lang.String
    }
    
    property set accountNumber ($arg :  java.lang.String) {
      setVariableValue("accountNumber", 0, $arg)
    }
    
    property get amount () : gw.pl.currency.MonetaryAmount {
      return getVariableValue("amount", 0) as gw.pl.currency.MonetaryAmount
    }
    
    property set amount ($arg :  gw.pl.currency.MonetaryAmount) {
      setVariableValue("amount", 0, $arg)
    }
    
    property get applyTo () : typekey.SuspensePaymentApplyTo {
      return getVariableValue("applyTo", 0) as typekey.SuspensePaymentApplyTo
    }
    
    property set applyTo ($arg :  typekey.SuspensePaymentApplyTo) {
      setVariableValue("applyTo", 0, $arg)
    }
    
    property get checkNumber () : java.lang.String {
      return getVariableValue("checkNumber", 0) as java.lang.String
    }
    
    property set checkNumber ($arg :  java.lang.String) {
      setVariableValue("checkNumber", 0, $arg)
    }
    
    property get description () : java.lang.String {
      return getVariableValue("description", 0) as java.lang.String
    }
    
    property set description ($arg :  java.lang.String) {
      setVariableValue("description", 0, $arg)
    }
    
    property get invoiceNumber () : java.lang.String {
      return getVariableValue("invoiceNumber", 0) as java.lang.String
    }
    
    property set invoiceNumber ($arg :  java.lang.String) {
      setVariableValue("invoiceNumber", 0, $arg)
    }
    
    property get policyNumber () : java.lang.String {
      return getVariableValue("policyNumber", 0) as java.lang.String
    }
    
    property set policyNumber ($arg :  java.lang.String) {
      setVariableValue("policyNumber", 0, $arg)
    }
    
    property get producerName () : java.lang.String {
      return getVariableValue("producerName", 0) as java.lang.String
    }
    
    property set producerName ($arg :  java.lang.String) {
      setVariableValue("producerName", 0, $arg)
    }
    
    property get suspensePayment () : SuspensePayment {
      return getVariableValue("suspensePayment", 0) as SuspensePayment
    }
    
    property set suspensePayment ($arg :  SuspensePayment) {
      setVariableValue("suspensePayment", 0, $arg)
    }
    
    function finalizeSuspensePayment() {
      suspensePayment.setApplyToTarget(applyTo, accountNumber, policyNumber, producerName)
      suspensePayment.updateAmount(amount)
      suspensePayment.Description = description
      suspensePayment.InvoiceNumber = invoiceNumber
    }
    
    
  }
  
  
}