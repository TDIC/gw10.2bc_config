package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/policy/PolicyTransactionDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class PolicyTransactionDetailExpressions {
  @javax.annotation.Generated("config/web/pcf/policy/PolicyTransactionDetail.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class PolicyTransactionDetailExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    static function __constructorIndex (policyPeriod :  PolicyPeriod, transaction :  Transaction) : int {
      return 0
    }
    
    // 'def' attribute on PanelRef at PolicyTransactionDetail.pcf: line 22, column 49
    function def_onEnter_0 (def :  pcf.TransactionDetailDV) : void {
      def.onEnter(transaction)
    }
    
    // 'def' attribute on PanelRef at PolicyTransactionDetail.pcf: line 22, column 49
    function def_refreshVariables_1 (def :  pcf.TransactionDetailDV) : void {
      def.refreshVariables(transaction)
    }
    
    // 'parent' attribute on Page (id=PolicyTransactionDetail) at PolicyTransactionDetail.pcf: line 9, column 104
    static function parent_2 (policyPeriod :  PolicyPeriod, transaction :  Transaction) : pcf.api.Destination {
      return pcf.PolicyDetailTransactions.createDestination(policyPeriod)
    }
    
    // 'title' attribute on Page (id=PolicyTransactionDetail) at PolicyTransactionDetail.pcf: line 9, column 104
    static function title_3 (policyPeriod :  PolicyPeriod, transaction :  Transaction) : java.lang.Object {
      return DisplayKey.get("Web.PolicyTransactionDetail.Title", transaction.LongDisplayName)
    }
    
    override property get CurrentLocation () : pcf.PolicyTransactionDetail {
      return super.CurrentLocation as pcf.PolicyTransactionDetail
    }
    
    property get policyPeriod () : PolicyPeriod {
      return getVariableValue("policyPeriod", 0) as PolicyPeriod
    }
    
    property set policyPeriod ($arg :  PolicyPeriod) {
      setVariableValue("policyPeriod", 0, $arg)
    }
    
    property get transaction () : Transaction {
      return getVariableValue("transaction", 0) as Transaction
    }
    
    property set transaction ($arg :  Transaction) {
      setVariableValue("transaction", 0, $arg)
    }
    
    
  }
  
  
}