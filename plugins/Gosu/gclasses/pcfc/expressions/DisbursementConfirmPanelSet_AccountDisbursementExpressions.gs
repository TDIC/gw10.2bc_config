package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/disbursement/DisbursementConfirmPanelSet.AccountDisbursement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class DisbursementConfirmPanelSet_AccountDisbursementExpressions {
  @javax.annotation.Generated("config/web/pcf/disbursement/DisbursementConfirmPanelSet.AccountDisbursement.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class DisbursementConfirmPanelSetExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on PanelRef at DisbursementConfirmPanelSet.AccountDisbursement.pcf: line 12, column 80
    function def_onEnter_0 (def :  pcf.AccountDisbursementConfirmDV) : void {
      def.onEnter(disbursement as AccountDisbursement)
    }
    
    // 'def' attribute on PanelRef at DisbursementConfirmPanelSet.AccountDisbursement.pcf: line 12, column 80
    function def_refreshVariables_1 (def :  pcf.AccountDisbursementConfirmDV) : void {
      def.refreshVariables(disbursement as AccountDisbursement)
    }
    
    property get disbursement () : Disbursement {
      return getRequireValue("disbursement", 0) as Disbursement
    }
    
    property set disbursement ($arg :  Disbursement) {
      setRequireValue("disbursement", 0, $arg)
    }
    
    
  }
  
  
}