package pcfc.expressions

uses pcf.*
uses entity.*
uses typekey.*
uses gw.api.locale.DisplayKey
@javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
class TransferPolicyConfirmationScreenExpressions {
  @javax.annotation.Generated("config/web/pcf/transferpolicy/TransferPolicyConfirmationScreen.pcf", "", "com.guidewire.pcfgen.PCFClassGenerator")
  public static class TransferPolicyConfirmationScreenExpressionsImpl extends gw.api.web.ScopeBaseClass {
    public construct(widget :  Object) {
      super(widget, 0)
    }
    
    protected construct(widget :  Object, scopeDepth :  int) {
      super(widget, scopeDepth)
    }
    
    // 'def' attribute on ListViewInput at TransferPolicyConfirmationScreen.pcf: line 52, column 29
    function def_onEnter_11 (def :  pcf.TransferPolicyConfirmationLV) : void {
      def.onEnter(policyTransfer)
    }
    
    // 'def' attribute on ListViewInput at TransferPolicyConfirmationScreen.pcf: line 52, column 29
    function def_refreshVariables_12 (def :  pcf.TransferPolicyConfirmationLV) : void {
      def.refreshVariables(policyTransfer)
    }
    
    // 'value' attribute on TextInput (id=producer_Input) at TransferPolicyConfirmationScreen.pcf: line 47, column 72
    function valueRoot_9 () : java.lang.Object {
      return policyTransfer.CommissionTransferOption
    }
    
    // 'value' attribute on TextInput (id=sourceproducer_Input) at TransferPolicyConfirmationScreen.pcf: line 24, column 41
    function value_0 () : java.lang.Object {
      return getValueOrNone(policyTransfer.SourceProducerCode.Producer)
    }
    
    // 'value' attribute on TextInput (id=sourceproducerCode_Input) at TransferPolicyConfirmationScreen.pcf: line 29, column 41
    function value_2 () : java.lang.Object {
      return getValueOrNone(policyTransfer.SourceProducerCode.Code)
    }
    
    // 'value' attribute on TextInput (id=destproducer_Input) at TransferPolicyConfirmationScreen.pcf: line 36, column 41
    function value_4 () : java.lang.Object {
      return getValueOrNone(policyTransfer.DestinationProducerCode.Producer)
    }
    
    // 'value' attribute on TextInput (id=destproducerCode_Input) at TransferPolicyConfirmationScreen.pcf: line 41, column 41
    function value_6 () : java.lang.Object {
      return getValueOrNone(policyTransfer.DestinationProducerCode.Code)
    }
    
    // 'value' attribute on TextInput (id=producer_Input) at TransferPolicyConfirmationScreen.pcf: line 47, column 72
    function value_8 () : java.lang.String {
      return policyTransfer.CommissionTransferOption.Description
    }
    
    property get policyTransfer () : PolicyTransfer {
      return getRequireValue("policyTransfer", 0) as PolicyTransfer
    }
    
    property set policyTransfer ($arg :  PolicyTransfer) {
      setRequireValue("policyTransfer", 0, $arg)
    }
    
    
    function getValueOrNone(value : Object) : Object {
            return value == null ? DisplayKey.get("Web.TransferPolicyConfirmation.None") : value.toString();
          }
        
    
    
  }
  
  
}