package com.tdic.util.flatfile.model.gx.recordmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement RecordEnhancement : com.tdic.util.flatfile.model.gx.recordmodel.Record {
  public static function create(object : com.tdic.util.flatfile.model.classes.Record) : com.tdic.util.flatfile.model.gx.recordmodel.Record {
    return new com.tdic.util.flatfile.model.gx.recordmodel.Record(object)
  }

  public static function create(object : com.tdic.util.flatfile.model.classes.Record, options : gw.api.gx.GXOptions) : com.tdic.util.flatfile.model.gx.recordmodel.Record {
    return new com.tdic.util.flatfile.model.gx.recordmodel.Record(object, options)
  }

}