package com.tdic.util.flatfile.model.gx.fieldmodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement FieldEnhancement : com.tdic.util.flatfile.model.gx.fieldmodel.Field {
  public static function create(object : com.tdic.util.flatfile.model.classes.Field) : com.tdic.util.flatfile.model.gx.fieldmodel.Field {
    return new com.tdic.util.flatfile.model.gx.fieldmodel.Field(object)
  }

  public static function create(object : com.tdic.util.flatfile.model.classes.Field, options : gw.api.gx.GXOptions) : com.tdic.util.flatfile.model.gx.fieldmodel.Field {
    return new com.tdic.util.flatfile.model.gx.fieldmodel.Field(object, options)
  }

}