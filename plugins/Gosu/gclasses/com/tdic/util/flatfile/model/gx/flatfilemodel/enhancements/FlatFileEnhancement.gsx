package com.tdic.util.flatfile.model.gx.flatfilemodel.enhancements

@javax.annotation.Generated("gw.xml.codegen.XmlCodeGenerator")
enhancement FlatFileEnhancement : com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile {
  public static function create(object : com.tdic.util.flatfile.model.classes.FlatFile) : com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile {
    return new com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile(object)
  }

  public static function create(object : com.tdic.util.flatfile.model.classes.FlatFile, options : gw.api.gx.GXOptions) : com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile {
    return new com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile(object, options)
  }

}