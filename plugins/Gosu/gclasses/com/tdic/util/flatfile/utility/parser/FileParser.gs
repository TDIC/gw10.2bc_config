package com.tdic.util.flatfile.utility.parser

uses com.tdic.util.flatfile.model.classes.Record

/**
 * US688
 * 05/09/2014 shanem
 *
 *  Parser classes must extend this type to be returned from the FlatFileUtility class
 */
abstract class FileParser {
  /**
   * US688
   * 05/09/2014 shanem
   *
   * Parse the file based on the first character of each line
   */
  protected abstract function parse(aPath: String, vendorDef: com.tdic.util.flatfile.model.gx.flatfilemodel.FlatFile): Record[]

  /**
   * US688
   * 05/09/2014 shanem
   *
   * Writes resulting XML file to a file
   */
  abstract function writeToFile(aWritePath: String): void
}