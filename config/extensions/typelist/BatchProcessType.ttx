<?xml version="1.0" encoding="UTF-8"?>
<typelistextension
  desc="Types of batch processes"
  name="BatchProcessType"
  xmlns="http://guidewire.com/typelists">
  <!-- batch processes -->
  <typecode
    code="ActivityEsc"
    desc="Activity escalation monitor"
    name="Activity Escalation">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="Archive"
    desc="Policy period archiving monitor that checks for eligible policy periods and schedules them to be archived."
    name="Archive policy periods">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="BulkPurge"
    desc="Purge records through table updates"
    name="BulkPurge"
    retired="true"/>
  <typecode
    code="DataDistribution"
    desc="Data distribution for the database"
    name="Data Distribution">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="DBStats"
    desc="Database statistics"
    name="Database statistics">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="GroupException"
    desc="Group exception Monitor"
    name="Group Exception">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="UserException"
    desc="User exception Monitor"
    name="User Exception">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="Workflow"
    desc="Workflows work queue writer."
    name="Workflow Writer">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <!-- work queues -->
  <typecode
    code="ContactAutoSync"
    desc="Automatically synchronize the local contact that are out of syn and marked &apos;allow&apos; auto-sync."
    name="ContactAutoSync">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="Geocode"
    desc="Geocoding Addresses queue writer."
    name="Geocode Writer">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <!-- unused -->
  <typecode
    code="StatReport"
    desc="Stat Report work queue writer"
    name="Stat Report Writer"
    retired="true"/>
  <typecode
    code="PurgeProfilerData"
    desc="Purge profiler data at regular intervals"
    name="Purge Profiler Data">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PhoneNumberNormalizer"
    desc="Performs a normalization of phone numbers contact"
    name="Phone number normalizer">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="TroubleTicketEsc"
    desc="Trouble Ticket escalation monitor"
    name="Trouble Ticket Escalation">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="AccountInactivity"
    desc="Checks for accounts that have been inactive for many days and creates notification activities for those accounts."
    name="Account Inactivity">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="AdvanceExpiration"
    desc="Checks for commission advances whose &quot;maintain until&quot; dates have passed and expires them. Results in any remaining advance balance becoming owed by the producer."
    name="Advance Expiration">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="AgencySuspensePayment"
    desc="Process agency cycle payments and promises that match policy periods"
    name="Agency Suspense Payment">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="AutomaticDisbursement"
    desc="Checks for accounts whose unapplied funds&apos; amount less the (amount under contract + amount slated for disbursement) is greater than billing-plan defined &quot;Disbursement Over&quot; amount and creates a disbursement for such accounts."
    name="Automatic Disbursement">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ChargeProRataTx"
    desc="Moves part of the total charge amount from the pro-rata charge&apos;s unearned T-Account (liability) to its revenue T-Account (income)."
    name="Charge ProRata Tx">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="CmsnPayable"
    desc="Checks for commissions that need to be made payable."
    name="Commission Payable Calculations">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="CollEffective"
    desc="Checks for Pending Collateral Requirements whose effective dates have passed"
    name="Collateral Requirement Effective">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="CollExpiration"
    desc="Checks for Active Collateral Requirements whose expiration dates have passed"
    name="Collateral Requirement Expiration">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="CommissionPmt"
    desc="Checks for manual commission payments that have been scheduled to occur on a future date. Any commission payment whose scheduled date has passed is executed by this process."
    name="Commission Payment">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="Disbursement"
    desc="Checks for disbursements whose due dates have passed and are approved and pays that amount to the account."
    name="Disbursement">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="FullPayDiscount"
    desc="Evaluates whether a policy is eligible for a full pay discount"
    name="Full Pay Discount">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="Invoice"
    desc="Changes the status of planned invoices whose billing date has passed to &quot;billed&quot;. As a consequence of this change, the invoices are sent to the account."
    name="Invoice">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="InvoiceDue"
    desc="Changes the status of billed invoices whose due date has passed to &quot;due&quot;. Delinquency will be triggered for any policies with past due amounts."
    name="Invoice Due">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="LetterOfCredit"
    desc="Changes the status of Current LOCs to Expired when their expiration date has passed"
    name="Letter Of Credit">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="LegacyAgencyBill"
    desc="Starts workflow on staging table imported active agency bill cycle"
    name="Legacy Agency Bill">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="LegacyDelinquency"
    desc="Starts workflow on staging table imported active delinquency processes"
    name="Legacy Delinquency">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="NewPayment"
    desc="Distributes a payment, moving funds from the account&apos;s unapplied T-Account to the account&apos;s policies."
    name="New Payment">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PaymentRequest"
    desc="Checks for need to change payment request status"
    name="Payment Request">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PolicyClosure"
    desc="Checks for policies that are not locked and that have been fully paid. Any such policy is closed by this process."
    name="Policy Closure">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PremiumReportReportDue"
    desc="Checks to see if any premium reports are past due, kicks off delinquency if they are."
    name="Premium Reporting Report Due">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ProducerPayment"
    desc="Pays producers whose next scheduled auto-payment date has passed. Payment includes the amount currently payable to the producer, plus any manual commission payments that have been scheduled to occur with next payment."
    name="Producer Payment">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PurgeMessageHistory"
    desc="Purges old messages from the message history table"
    name="Purge Message History">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ReleaseTtktHoldTypes"
    desc="Checks for hold types on trouble tickets whose released dates have passed and removes those hold types from their holds."
    name="Release Trouble Ticket Hold Types">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ReleaseChargeHolds"
    desc="Checks for charge holds whose released dates have passed and release them."
    name="Release Charge Holds">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="StatementBilled"
    desc="Changes the status of &quot;Planned&quot; Agency Bill Statements whose Bill Date has passed to &quot;Billed&quot;."
    name="Statement Billed">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="StatementDue"
    desc="Changes the status of &quot;Billed&quot; Agency Bill Statements whose Due Date has passed to &quot;Due&quot;."
    name="Statement Due">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="SuspensePayment"
    desc="Process the suspense payments who match accounts"
    name="Suspense Payment">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="WriteoffStaging"
    desc="Distributes any undistributed imported writeoffs, to be used after staging table loading."
    name="Writeoff Staging">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="LegacyLetterOfCredit"
    desc="Runs allocate on Collateral with LOCs but no cash"
    name="Legacy Letter Of Credit">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="UpgradeBillingInstructionPaymentPlan"
    desc="Initializes policy billing instructions&apos; payment plans after upgrade"
    name="Upgrade Billing Instruction Payment Plan">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="FundsAllotment"
    desc="Allots funds from fund sources to fund uses for funds tracking.  Allots funds for all accounts that have unallotted or partially unallotted funds."
    name="Allot Funds for Funds Tracking">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="LegacyCollateral"
    desc="Runs allocate on Collateral with requirements"
    name="Legacy Collateral">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="Statistics"
    desc="Statistics calculator"
    name="Statistics">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="BankEFTBatchProcess"
    desc="Bank EFT Batch Process"
    name="Finance Interface Bank EFT Batch Process">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="BankEFTUploadBatchProcess"
    desc="Bank EFT Upload Batch Process"
    name="Finance Interface Bank EFT Upload Batch Process">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="DocumentCreationBatch"
    desc="Batch Process to Create Document Asynchronously using Command Center"
    name="Document Creation Batch">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"
      xmlns=""/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"
      xmlns=""/>
  </typecode>
  <typecode
    code="CacheManager"
    desc="Rebuild the cache from GWINT database"
    name="Cache Manager"
    retired="true">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="BankLockboxFileProcess"
    desc="Bank Lockbox File Process Batch Process"
    name="Finance Interface Bank Lockbox File Process">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"
      xmlns=""/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"
      xmlns=""/>
  </typecode>
  <typecode
    code="BankLockbox"
    desc="Bank Lockbox Batch Process"
    name="Finance Interface Bank Lockbox"
    retired="true">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"
      xmlns=""/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"
      xmlns=""/>
  </typecode>
  <typecode
    code="GeneralLedger"
    desc="General Ledger"
    name="Finance Interface General Ledger">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"
      xmlns=""/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"
      xmlns=""/>
  </typecode>
  <typecode
    code="CashReceipts"
    desc="Cash Receipts"
    name="Finance Interface Cash Receipts">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"
      xmlns=""/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"
      xmlns=""/>
  </typecode>
  <typecode
    code="AuthorizeNetCreditCardHandlerCleanup_TDIC"
    desc="Batch for cleaning up Authorize.Net Credit Card Handlers"
    name="Finance Interface Authorize.Net Credit Card Cleanup">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="OutgoingPayment"
    desc="Outgoing Payment Batch Process"
    name="Finance Interface Outgoing Payment Batch Process">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"
      xmlns=""/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"
      xmlns=""/>
  </typecode>
  <typecode
    code="EFTReversalBatchProcess"
    desc="EFT Reversal and Change Notifications Batch Process"
    name="Finance Interface EFT Returns Batch Process"
    retired="true">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PolicyPeriodEffectiveStatus"
    desc="Policy Period Effective Date Status"
    name="Finance Interface Policy Period Effective Date">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="CheckStatusUpdate"
    desc="Check Status Update Batch Process"
    name="Finance Interface Check Status Update"
    retired="true">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="QuarterlyPlanChanger_TDIC"
    desc="This process changes the payment plan from Annual to Quarterly for Migrated active policies"
    name="Quarterly Plan Changer"
    retired="true">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="CheckStatusUpdate_TDIC"
    desc="Check Status Update Batch Process"
    name="Finance Interface Check Status Update">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="BankLockboxFileDownload"
    desc="Bank Lockbox File Download Batch Process"
    name="Finance Interface Bank Lockbox File Download">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="EFTReversalFileProcessBatchProcess"
    desc="EFT Reversal and Change Notifications File Process Batch Process"
    name="Finance Interface EFT Returns File Process Batch Process">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="EFTReversalFileDownloadBatchProcess"
    desc="EFT Reversal and Change Notifications File Download Batch Process"
    name="Finance Interface EFT Returns File Download Batch Process">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="AuthorizeNetOpenHandlerCleanupBatch_TDIC"
    desc="Batch for cleaning up AuthorizeNet Open handler"
    name="Finance Interface AuthorizeNet Openhandler Cleanup Batch">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ResendInvoices_TDIC"
    desc="Resend Invoices"
    name="Resend Invoices">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ChangeQuarterlyV2toV3_TDIC"
    desc="Move policy periods from Quarterly v2 to v3 payment plan"
    name="Move policy periods from Quarterly v2 to v3 payment plan">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="UnappliedFundInvoice_TDIC"
    desc="Calculate amount of unapplied funds targeted to planned invoices"
    name="Calculate amount of unapplied funds targeted to planned invoices">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="UpgradeProducerStatementHoldLimits"
    desc="Sets defaults on the producer&apos;s statement hold limits based on payment threshold post upgrade"
    name="Upgrade Producer Statement Hold Limits">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="RetrievePolicyPeriod"
    desc="Retrieves an archived policy period."
    name="Retrieve PolicyPeriod">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ArchiveReferenceTrackingSync"
    desc="Ensures that the archive document references table is in sync with the archive store."
    name="Archive Reference Tracking Sync">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ReverseBatchPayments"
    desc="Reverses payments created from entries in batch payments whose status is reversing"
    name="Reverse Batch Payments">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PostBatchPayments"
    desc="Creates payments for entries in batch payments whose status is posting."
    name="Post Batch Payments">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="DeleteAccountCommissionsExpense"
    desc="Deletes the Account Commissions Expense TAccount since it is no longer used"
    name="Delete Account Commissions Expense TAccount">
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="BankEFTPrenoteBatchProcess_TDIC"
    desc="Bank EFT Prenote Validation Batch Process"
    name="Finance Interface Bank EFT Prenote Batch Process">
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="UpdateReturnPremiumPlan_TDIC"
    desc="Update Return Premium Plan for WC Policies"
    name="Update Return Premium Plan for WC Policies">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="RemoveGhostInvoice_TDIC"
    desc="Remove all planned ghost invoices from the system"
    name="Remove all planned ghost invoices">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="SecurityZoneUpate_TDIC"
    desc="Update SecurityZone on all Existing Producers"
    name="Update SecurityZone on all Existing Producers">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="UpdateBillingPlan_TDIC"
    desc="Update Billing Plan for TDIC"
    name="Update Billing Plan for TDIC">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="UpdateDelinquencyPlan_TDIC"
    desc="Update Delinquency Plan for WC Policies"
    name="Update Delinquency Plan for WC Policies">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="AccountSecurityZoneUpate_TDIC"
    desc="Update SecurityZone on all Existing Accounts"
    name="Update SecurityZone on all Existing Accounts">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="PolicyPeriodSecurityZoneUpate_TDIC"
    desc="Update SecurityZone on all Existing PolicyPeriods"
    name="Update SecurityZone on all Existing PolicyPeriods">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="TroubleTicketsDaily_TDIC"
    desc="Process to create daily COVID Q120 trouble tickets (until 7/14/2020)"
    name="COVID Q120 trouble tickets daily batch">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
  <typecode
    code="ReturnCreditAllocationToAccountDefault_TDIC"
    desc="Allocate Return Credits to Account Default"
    name="Allocate Return Credits to Account Default">
    <category
      code="APIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="UIRunnable"
      typelist="BatchProcessTypeUsage"/>
    <category
      code="Schedulable"
      typelist="BatchProcessTypeUsage"/>
  </typecode>
</typelistextension>
