package rules.Preupdate.BaseMoneyReceivedPreupdate_dir

uses gw.api.util.CurrencyUtil
uses gw.pl.currency.MonetaryAmount
uses org.slf4j.LoggerFactory

@gw.rules.RuleName("BRPU11000 - Activity for Payment during Delinquency")
internal class BRPU11000ActivityforPaymentduringDelinquency {
  static function doCondition(baseMoneyReceived : entity.BaseMoneyReceived) : boolean {
/*start00rule*/
    return baseMoneyReceived typeis DirectBillMoneyRcvd and not baseMoneyReceived.Reversed and
        not (baseMoneyReceived typeis ZeroDollarDMR)
/*end00rule*/
  }

  static function doAction(baseMoneyReceived : entity.BaseMoneyReceived, actions : gw.rules.Action) {
/*start00rule*/
    // GBC-3132 - Activity is not created when Lockbox payment does not satisfy the delinquency
    // Payment may come for any term but the delinquency will always be open on the latest term
    // Hence pick the latest term for the activity creation. Applicable only for the delinquency
    // with multiple term policyperiods.
    var paymentAmount = (baseMoneyReceived as DirectBillMoneyRcvd).Amount
    var pp = (baseMoneyReceived as DirectBillMoneyRcvd).UnappliedFund.PolicyPeriod_TDIC.LatestPolicyPeriod
    var amountZero = new MonetaryAmount(0bd, CurrencyUtil.DefaultCurrency)
    var delinquentAmount = pp?.Policy?.OrderedPolicyPeriods?.sum(\s -> s?.DelinquentAmount) == null ? amountZero :
        pp.Policy.OrderedPolicyPeriods.sum(\s -> s.DelinquentAmount)
    if(paymentAmount != null and paymentAmount.IsPositive
        and pp != null and pp.hasActiveDelinquencyProcess()
        and pp.ActiveDelinquencyProcesses.firstWhere(\elt -> elt?.AssociatedPolicyPeriodsCount > 1) != null
        and (delinquentAmount - paymentAmount) > pp.Account.DelinquencyPlan.ExitDelinquencyThreshold){
      // create the activity and assign to the UW Admin queue.
      var bundle = gw.transaction.Transaction.getCurrent()
      var actvty = new Activity(bundle)
      actvty.ActivityPattern = gw.api.web.admin.ActivityPatternsUtil.getActivityPattern("notsufficient_to_close_delinquency")
      actvty.Priority = Priority.TC_URGENT
      actvty.Account = pp.Account
      var groupName = gw.command.demo.GeneralUtil.findGroupByUserName("Underwriting Admin")
      var queueName = groupName.getQueue("UW Admin")
      if (actvty?.assignGroup(groupName) and queueName != null) {
        actvty.assignActivityToQueue(queueName, groupName)
      }
      var _logger = LoggerFactory.getLogger("Rules")
      if (actvty.assignActivityToQueue(queueName, groupName)) {
        _logger.debug("BaseMoneyRcvdPreupdate - Not sufficient to close delinquency Activity : assignment successful. Assigned Queue : " + actvty.AssignedQueue.DisplayName)
      } else {
        _logger.warn("BaseMoneyRcvdPreupdate - Not sufficient to close delinquency Activity : " + actvty.ActivityPattern.DisplayName)
      }
    }
    actions.exit()
/*end00rule*/
  }
}
