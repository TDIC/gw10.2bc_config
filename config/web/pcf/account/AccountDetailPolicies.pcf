<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <Page
    canVisit="perm.System.accttabview and perm.System.acctplcyview"
    id="AccountDetailPolicies"
    showUpLink="true"
    title="DisplayKey.get(&quot;Web.AccountDetailPolicies.Title&quot;)">
    <LocationEntryPoint
      signature="AccountDetailPolicies(account : Account)"/>
    <Variable
      name="account"
      type="Account"/>
    <Screen
      id="AccountDetailPoliciesScreen">
      <Variable
        initialValue="null"
        name="policyStartsWith"
        type="String"/>
      <Toolbar/>
      <CardViewPanel>
        <Card
          id="OwnedTab"
          title="DisplayKey.get(&quot;Web.AccountPoliciesLV.OwnedTab.Title&quot;)"
          visible="!account.Policies.where(\ policy -&gt; policy.PolicyPeriods.length &gt; 0).IsEmpty">
          <PanelIterator
            elementName="policy"
            id="OwnerPolicies"
            value="account.Policies.where(\ policy -&gt; policy.PolicyPeriods.length &gt; 0)"
            valueType="entity.Policy[]">
            <PanelRef
              def="PolicyPeriodPanelLV(policy.PolicyPeriods.toList())"
              id="OwnedPolicyPeriods">
              <TitleBar
                title="policy.NumberAndLOB"/>
              <Toolbar/>
            </PanelRef>
          </PanelIterator>
          <ListViewPanel
            id="GrandTotalLV"
            stretch="true">
            <Row>
              <TextCell
                align="left"
                bold="true"
                grow="true"
                id="Number"
                value="DisplayKey.get(&quot;Web.AccountDetailPoliciesScreen.TotalValue&quot;)"/>
              <MonetaryAmountCell
                bold="true"
                currency="account.Currency"
                formatType="currency"
                id="TotalValue"
                value="account.TotalValue - account.RawTotalValue"/>
            </Row>
          </ListViewPanel>
        </Card>
        <Card
          id="PayerTab"
          title="DisplayKey.get(&quot;Web.AccountPoliciesLV.PayerTab.Title&quot;)"
          visible="!gw.api.web.account.Policies.findPoliciesWhereAccountIsOverridingPayerOnAtLeastOnePeriod(account, null).Empty">
          <ListDetailPanel
            id="PayerPolicies"
            selectionName="policy"
            selectionType="Policy">
            <Variable
              initialValue="gw.api.web.account.Policies.findPoliciesWhereAccountIsOverridingPayerOnAtLeastOnePeriod(account, policyStartsWith)"
              name="policies"
              type="gw.api.database.IQueryBeanResult&lt;Policy&gt;"/>
            <PanelRef
              def="PoliciesLV(policies)">
              <Toolbar>
                <ToolbarInput
                  editable="true"
                  id="PolicyPeriodFilter"
                  label="DisplayKey.get(&quot;Web.AccountPoliciesLV.PayerTab.PolicyStartsWith&quot;)"
                  value="policyStartsWith"
                  valueType="java.lang.String"/>
                <ToolbarButton
                  action="policies = gw.api.web.account.Policies.findPoliciesWhereAccountIsOverridingPayerOnAtLeastOnePeriod(account, policyStartsWith)"
                  default="true"
                  id="PolicyFilterGoButton"
                  label="DisplayKey.get(&quot;Web.AccountPoliciesLV.PayerTab.PolicyStartsWithFilterGoButton&quot;)"/>
                <ToolbarButton
                  action="policyStartsWith = null; policies= gw.api.web.account.Policies.findPoliciesWhereAccountIsOverridingPayerOnAtLeastOnePeriod(account, policyStartsWith)"
                  id="PolicyFilterClearButton"
                  label="DisplayKey.get(&quot;Web.AccountPoliciesLV.PayerTab.PolicyStartsWithFilterClearButton&quot;)"/>
              </Toolbar>
            </PanelRef>
            <CardViewPanel>
              <Card
                id="PolicyDetailCard"
                title="DisplayKey.get(&quot;Web.AccountPoliciesLV.PayerTab.PolicyPeriods&quot;)">
                <PanelRef
                  def="PolicyPeriodPanelLV(gw.api.web.account.Policies.findPolicyPeriodsFromPolicyWhereAccountIsOverridingPayer(account, policy, policyStartsWith).toList())"
                  id="PayerPolicyPeriods">
                  <Toolbar/>
                </PanelRef>
              </Card>
            </CardViewPanel>
          </ListDetailPanel>
        </Card>
      </CardViewPanel>
    </Screen>
  </Page>
</PCF>