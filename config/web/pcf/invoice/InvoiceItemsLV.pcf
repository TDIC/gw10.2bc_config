<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <ListViewPanel
    editable="false"
    id="InvoiceItemsLV"
    stretch="true">
    <ExposeIterator
      flags="notFrozen"
      valueType="entity.InvoiceItem"
      widget="InvoiceItemsLV"/>
    <Require
      name="invoiceItems"
      type="InvoiceItem[]"/>
    <Require
      name="charge"
      type="Charge"/>
    <Require
      name="showInstallmentNumber"
      type="Boolean"/>
    <RowIterator
      checkBoxVisible="charge != null &amp;&amp; invoiceItem.canReverse() &amp;&amp; invoiceItem.Type != InvoiceItemType.TC_COMMISSIONREMAINDER"
      editable="charge != null"
      elementName="invoiceItem"
      toRemove="invoiceItem.canRemove()"
      value="invoiceItems"
      valueType="entity.InvoiceItem[]">
      <ToolbarFlag
        condition="!invoiceItem.Frozen"
        name="notFrozen"/>
      <Row>
        <TextCell
          align="right"
          footerLabel="DisplayKey.get(&quot;Web.InvoiceItemsLV.Total&quot;)"
          id="InstallmentNumber"
          label="DisplayKey.get(&quot;Web.InvoiceItemsLV.InstallmentNumber&quot;)"
          value="invoiceItem.InstallmentNumber"
          valueType="java.lang.Integer"
          visible="showInstallmentNumber"/>
        <DateCell
          action="InvoiceItemDetailPopup.push(invoiceItem)"
          editable="invoiceItem.New"
          id="PaymentDate"
          label="DisplayKey.get(&quot;Web.InvoiceItemsLV.EventDate&quot;)"
          required="true"
          value="invoiceItem.EventDateAndInvoice">
          <PostOnChange/>
        </DateCell>
        <TextCell
          id="Invoice"
          label="DisplayKey.get(&quot;Web.InvoiceItemsLV.Invoice&quot;)"
          value="invoiceItem.Invoice"
          valueType="entity.Invoice"
          wrap="false"/>
        <DateCell
          id="DueDate"
          label="DisplayKey.get(&quot;Web.InvoiceItemsLV.DueDate&quot;)"
          value="invoiceItem.InvoiceDueDate"/>
        <TextCell
          action="AccountSummary.push(invoiceItem.Owner)"
          enableSort="false"
          grow="true"
          id="itemOwner"
          label="DisplayKey.get(&quot;Web.InvoiceItemsLV.Owner&quot;)"
          value="invoiceItem.Owner"
          valueType="entity.Account"/>
        <TextCell
          action="goToPayer(invoiceItem)"
          enableSort="false"
          grow="true"
          id="itemPayer"
          label="DisplayKey.get(&quot;Web.InvoiceItemsLV.Payer&quot;)"
          value="getPayerDisplay(invoiceItem)"/>
        <TextCell
          id="Context"
          label="DisplayKey.get(&quot;Web.InvoiceItemsLV.Context&quot;)"
          value="invoiceItem.Charge.BillingInstruction"
          valueType="entity.BillingInstruction"/>
        <RangeCell
          editable="invoiceItem.New"
          id="Type"
          label="DisplayKey.get(&quot;Web.InvoiceItemsLV.Type&quot;)"
          required="true"
          value="invoiceItem.Type"
          valueRange="gw.api.web.invoice.InvoiceUtil.getUserVisibleInvoiceItemTypes()"
          valueType="typekey.InvoiceItemType"
          wrap="false"/>
        <MonetaryAmountCell
          action="InvoiceItemDetailPopup.push(invoiceItem)"
          align="right"
          currency="invoiceItem.Currency"
          editable="invoiceItem.New or ( invoiceItem.Invoice.Status == InvoiceStatus.TC_PLANNED and invoiceItem.Type != InvoiceItemType.TC_COMMISSIONADJUSTMENT and invoiceItem.Type != InvoiceItemType.TC_COMMISSIONREMAINDER )"
          footerSumValue="invoiceItem.Amount"
          formatType="currency"
          id="Amount"
          label="DisplayKey.get(&quot;Web.InvoiceItemsLV.Amount&quot;)"
          required="true"
          value="invoiceItem.Amount">
          <PostOnChange/>
        </MonetaryAmountCell>
        <MonetaryAmountCell
          align="right"
          currency="invoiceItem.Currency"
          footerSumValue="invoiceItem.PaidAmount"
          formatType="currency"
          id="PaidAmount"
          label="DisplayKey.get(&quot;Web.InvoiceItemsLV.PaidAmount&quot;)"
          required="true"
          value="invoiceItem.PaidAmount"/>
        <TextCell
          id="InvoiceStream"
          label="DisplayKey.get(&quot;Web.InvoiceItemsLV.InvoiceStream&quot;)"
          value="invoiceItem.Invoice.InvoiceStream"
          valueType="entity.InvoiceStream"/>
      </Row>
    </RowIterator>
    <Row
      hideIfReadOnly="true"
      highlighted="true">
      <CheckBoxCell
        id="Spacer0"/>
      <TextCell
        align="right"
        bold="true"
        id="Unallocated"
        value="DisplayKey.get(&quot;Web.InvoiceItemsLV.Unallocated&quot;)"/>
      <TextCell
        align="center"
        id="Spacer1"
        value="null"
        valueType="java.lang.Object"/>
      <TextCell
        align="left"
        bold="true"
        id="Spacer2"
        value="null"
        valueType="java.lang.Object"/>
      <TextCell
        align="left"
        bold="true"
        id="Spacer3"
        value="null"
        valueType="java.lang.Object"/>
      <TextCell
        id="Spacer4"
        value="null"
        valueType="java.lang.Object"/>
      <TextCell
        id="Spacer5"
        value="null"
        valueType="java.lang.Object"/>
      <MonetaryAmountCell
        align="left"
        bold="true"
        currency="charge != null ? charge.Currency : null"
        formatType="currency"
        id="Amount"
        value="charge != null ? charge.Amount - charge.getInvoiceItemTotal() : null"/>
      <TextCell
        id="Spacer6"
        value="null"
        valueType="java.lang.Object"/>
      <TextCell
        id="Spacer7"
        value="null"
        valueType="java.lang.Object"/>
      <TextCell
        id="Spacer8"
        value="null"
        valueType="java.lang.Object"/>
    </Row>
    <Code><![CDATA[
              function goToPayer(invoiceItem: InvoiceItem) {
                var payer = invoiceItem.getPayer()
                if (payer typeis Producer) {
                  ProducerDetail.push(payer)
                } else {
                  AccountSummary.push(payer as Account)
                }
              }

              function getPayerDisplay(invoiceItem: InvoiceItem): String {
                var invoiceHolder = invoiceItem.getPayer()
                if (invoiceHolder == null) {
                  return charge.OwnerAccount as String
                }
                if (typeof invoiceHolder == Account or not invoiceItem.PolicyPeriodItem) {
                  return invoiceHolder as String
                }
                var itemCommission = invoiceItem.ActivePrimaryItemCommission
                var producerCode = itemCommission.PolicyCommission.ProducerCode
                var producer = producerCode.Producer
                return producer.DisplayName + " " + producerCode.Code
              }
      ]]></Code>
  </ListViewPanel>
</PCF>