<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../pcf.xsd">
  <Screen
    id="AgencyDistributionWizard_MoneyDetailsScreen">
    <Require
      name="wizardState"
      type="gw.agencybill.AgencyDistributionWizardHelper"/>
    <Variable
      initialValue="wizardState.MoneySetup"
      name="moneySetup"
      type="gw.api.web.producer.agencybill.AgencyBillMoneyReceivedSetup"/>
    <Variable
      initialValue="moneySetup.Money"
      name="money"
      type="BaseMoneyReceived"/>
    <Toolbar>
      <WizardButtons/>
    </Toolbar>
    <DetailViewPanel>
      <Variable
        initialValue="new gw.payment.PaymentInstrumentRange(moneySetup.Producer.PaymentInstruments) "
        name="paymentInstrumentRange"
        type="gw.payment.PaymentInstrumentRange"/>
      <Variable
        initialValue="gw.payment.PaymentInstrumentFilters.agencyPaymentInstrumentFilter"
        name="paymentInstrumentFilter"
        type="com.google.common.collect.ImmutableList&lt;typekey.PaymentMethod&gt;"/>
      <Variable
        initialValue="wizardState.PaymentMoneySetup.Money"
        name="paymentMoney"
        type="entity.AgencyBillMoneyRcvd"/>
      <InputColumn>
        <Label
          label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.ProducerHeader&quot;)"/>
        <TextInput
          id="Producer"
          label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.Producer&quot;)"
          value="moneySetup.Producer"
          valueType="entity.Producer"/>
        <MonetaryAmountInput
          currency="moneySetup.Producer.Currency"
          formatType="currency"
          id="UnappliedBalance"
          label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.UnappliedBalance&quot;)"
          value="moneySetup.Producer.UnappliedAmount"/>
        <Label
          label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.DistributionHeader&quot;, moneySetup.DistributionTypeName)"/>
        <DateInput
          editable="!wizardState.IsCreditDistribution"
          focusOnStartEditing="true"
          id="ReceivedDate"
          label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.ReceivedDate&quot;)"
          required="true"
          value="money.ReceivedDate"/>
        <MonetaryAmountInput
          currency="moneySetup.Producer.Currency"
          editable="true"
          formatType="currency"
          id="Amount"
          label="AmountLabel"
          required="true"
          validationExpression="return !money.Amount.IsPositive ? DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.Error.InvalidAmount&quot;)  : null"
          value="money.Amount"
          visible="!wizardState.IsCreditDistribution"/>
      </InputColumn>
      <InputColumn>
        <Label
          label="Header"/>
        <InputSet
          visible="paymentMoney != null">
          <RangeInput
            editable="true"
            id="PaymentInstrument"
            label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.PaymentInstrument&quot;)"
            onPick="paymentInstrumentRange.addPaymentInstrument(paymentMoney.PaymentInstrument)"
            required="true"
            value="paymentMoney.PaymentInstrument"
            valueRange="gw.payment.PaymentInstrumentFilters.applyFilter(paymentInstrumentRange, paymentInstrumentFilter)  "
            valueType="entity.PaymentInstrument"
            visible="!wizardState.IsCreditDistribution">
            <MenuItem
              action="NewPaymentInstrumentPopup.push(gw.payment.PaymentInstrumentFilters.agencyPaymentInstrumentOptions,moneySetup.Producer,true)"
              id="CreateNewPaymentInstrument"
              label="DisplayKey.get(&quot;Web.NewPaymentInstrument.CreateNewPaymentInstrument&quot;)"/>
          </RangeInput>
          <TextInput
            editable="true"
            id="RefNumber"
            label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.RefNumber&quot;)"
            value="paymentMoney.RefNumber"
            visible="!wizardState.IsCreditDistribution"/>
        </InputSet>
        <TextInput
          editable="true"
          id="Name"
          label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.Name&quot;)"
          value="money.Name"
          visible="true"/>
        <TextInput
          editable="true"
          id="Description"
          label="DistributionDescription"
          value="paymentMoney.Description"
          visible="paymentMoney != null"/>
      </InputColumn>
    </DetailViewPanel>
    <AlertBar
      id="CannotChangeDistributionAlert"
      label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.CannotChangeDistributionAlert&quot;)"
      visible="!wizardState.AllowChangeOfDistribution"/>
    <PanelRef>
      <TitleBar
        id="DistributionInstructionsHeader"
        title="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.DistributionInstructions&quot;)"/>
      <DetailViewPanel
        editable="wizardState.AllowChangeOfDistribution"
        id="DistributionDetailsDV">
        <InputColumn>
          <RangeRadioInput
            editable="true"
            id="DistributeTo"
            label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.DistributeTo&quot;)"
            required="true"
            stacked="true"
            value="wizardState.DistributeTo"
            valueRange="wizardState.DistributeToValues"
            valueType="gw.agencybill.AgencyDistributionWizardHelper.DistributeToEnum"
            visible="true">
            <PostOnChange/>
          </RangeRadioInput>
        </InputColumn>
        <InputColumn
          id="DistributeColumn">
          <InputSet
            visible="wizardState.DistributeToStatementsAndPolicies || wizardState.DistributeToPromise">
            <RangeRadioInput
              editable="true"
              id="DistributeAmounts"
              label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.DistributeAmounts&quot;)"
              required="true"
              stacked="true"
              value="wizardState.DistributeAmounts"
              valueRange="wizardState.DistributeAmountsValues"
              valueType="gw.agencybill.AgencyDistributionWizardHelper.DistributeAmountsEnum"
              visible="(wizardState.DistributeToStatementsAndPolicies || wizardState.DistributeToPromise)">
              <PostOnChange/>
            </RangeRadioInput>
            <RangeRadioInput
              editable="true"
              id="DistributeBy"
              label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.DistributeBy&quot;)"
              required="true"
              stacked="true"
              value="wizardState.DistributeBy"
              valueRange="wizardState.DistributeByValues"
              valueType="gw.agencybill.AgencyDistributionWizardHelper.DistributeByEnum"
              visible="(wizardState.DistributeToStatementsAndPolicies or wizardState.DistributeToPromise) and wizardState.SelectedEditDistribution "/>
            <TypeKeyRadioInput
              editable="true"
              id="Prefill"
              label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.Prefill&quot;)"
              required="true"
              stacked="true"
              value="moneySetup.Prefill"
              valueType="typekey.AgencyCycleDistPrefill"
              visible="wizardState.DistributeToStatementsAndPolicies and wizardState.SelectedEditDistribution"/>
          </InputSet>
        </InputColumn>
        <InputFooterSection>
          <InputSet
            id="Statements"
            visible="wizardState.DistributeToStatementsAndPolicies">
            <Label
              label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.StatementsLV&quot;)"/>
            <ListViewInput
              id="StatementsLV"
              labelAbove="true">
              <Toolbar>
                <PickerToolbarButton
                  action="AgencyDistributionWizard_AddStatementsPopup.push(wizardState)"
                  hideIfReadOnly="true"
                  id="AddStatements"
                  label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.StatementsLV.Add&quot;)"
                  onPick="moneySetup.addDistributeToStatements(PickedValue)"
                  shortcut="A"/>
                <RemoveButton
                  flags="any Row"
                  hideIfReadOnly="true"
                  id="RemoveStatements"
                  iterator="StatementsIterator"
                  label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.StatementsLV.Remove&quot;)"
                  shortcut="e"/>
              </Toolbar>
              <ListViewPanel>
                <RowIterator
                  checkBoxVisible="wizardState.AllowChangeOfDistribution"
                  editable="true"
                  elementName="statement"
                  hasCheckBoxes="true"
                  id="StatementsIterator"
                  toRemove="moneySetup.removeDistributeToStatements({statement})"
                  value="moneySetup.DistributeToStatements"
                  valueType="entity.StatementInvoice[]">
                  <ToolbarFlag
                    name="Row"/>
                  <Row>
                    <TextCell
                      action="AgencyBillStatementDetailPopup.push(statement)"
                      footerLabel="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Statements.Total&quot;)"
                      id="StatementNumber"
                      label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.StatementsLV.StatementNumber&quot;)"
                      value="statement.InvoiceNumber"/>
                    <TextCell
                      id="Status"
                      label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.StatementsLV.Status&quot;)"
                      value="statement.DisplayStatus"/>
                    <MonetaryAmountCell
                      currency="moneySetup.Producer.Currency"
                      footerSumValue="statement.NetAmountDue"
                      formatType="currency"
                      id="NetOwed"
                      label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.StatementsLV.NetOwed&quot;)"
                      value="statement.NetAmountDue"/>
                    <DateCell
                      id="BillDate"
                      label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.StatementsLV.BillDate&quot;)"
                      sortOrder="1"
                      value="statement.EventDate"/>
                    <DateCell
                      id="DueDate"
                      label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.StatementsLV.DueDate&quot;)"
                      value="statement.DueDate"/>
                  </Row>
                </RowIterator>
              </ListViewPanel>
            </ListViewInput>
          </InputSet>
          <InputSet
            id="Policies"
            visible="wizardState.DistributeToStatementsAndPolicies &amp;&amp; !moneySetup.DistributeToStatements.IsEmpty">
            <Label
              label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.PolicyFilterHeader&quot;)"/>
            <RangeInput
              editable="true"
              id="PolicyFilter"
              label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.PolicyFilter&quot;)"
              required="true"
              value="wizardState.PolicyFilter"
              valueRange="wizardState.PolicyFilterValues"
              valueType="gw.api.web.producer.agencybill.AgencyBillMoneyReceivedSetup.PolicyFilterOption">
              <PostOnChange
                deferUpdate="false"/>
            </RangeInput>
            <ListViewInput
              id="SpecificPoliciesLV"
              visible="wizardState.ShowSpecificPolicies">
              <Toolbar>
                <PickerToolbarButton
                  action="AgencyDistributionWizard_AddPoliciesPopup.push(wizardState)"
                  hideIfReadOnly="true"
                  id="AddSpecificPolicies"
                  label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.PoliciesLV.Add&quot;)"
                  onPick="moneySetup.addPolicies(PickedValue)"
                  shortcut="i"/>
                <RemoveButton
                  flags="any Row"
                  hideIfReadOnly="true"
                  id="RemoveSpecificPolicies"
                  iterator="PoliciesIterator"
                  label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.PoliciesLV.Remove&quot;)"
                  shortcut="m"/>
              </Toolbar>
              <ListViewPanel>
                <RowIterator
                  checkBoxVisible="wizardState.AllowChangeOfDistribution"
                  editable="false"
                  elementName="policy"
                  hasCheckBoxes="true"
                  id="PoliciesIterator"
                  numEntriesRequired="1"
                  toRemove="moneySetup.removePolicies(new PolicyPeriod[]{policy})"
                  validationLabel="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.MustPickAPolicyAlert&quot;)"
                  value="moneySetup.PoliciesToInclude"
                  valueType="entity.PolicyPeriod[]">
                  <ToolbarFlag
                    name="Row"/>
                  <Row>
                    <TextCell
                      action="PolicySummary.push(policy)"
                      id="PolicyNumber"
                      label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.PoliciesLV.PolicyNumber&quot;)"
                      sortOrder="1"
                      value="policy.PolicyNumberLong"/>
                    <TextCell
                      action="AccountSummaryPopup.push(policy.Policy.Account)"
                      id="AccountNumber"
                      label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.PoliciesLV.AccountNumber&quot;)"
                      value="policy.Policy.Account.AccountNumber"/>
                    <TypeKeyCell
                      id="Product"
                      label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.PoliciesLV.Product&quot;)"
                      value="policy.Policy.LOBCode"
                      valueType="typekey.LOBCode"/>
                    <TypeKeyCell
                      id="PaymentStatus"
                      label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.PoliciesLV.PaymentStatus&quot;)"
                      value="policy.Policy.Account.DelinquencyStatus"
                      valueType="typekey.DelinquencyStatus"/>
                    <DateCell
                      enableSort="false"
                      id="EffectiveDate"
                      label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.PoliciesLV.EffectiveDate&quot;)"
                      value="policy.EffectiveDate"/>
                    <DateCell
                      enableSort="false"
                      id="ExpirationDate"
                      label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.PoliciesLV.ExpirationDate&quot;)"
                      value="policy.ExpirationDate">
                      <PostOnChange/>
                    </DateCell>
                    <TextCell
                      id="Producer"
                      label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.PoliciesLV.Producer&quot;)"
                      value="policy.PrimaryPolicyCommission.ProducerCode.Producer"
                      valueType="entity.Producer"/>
                    <TextCell
                      id="ProducerCode"
                      label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.PoliciesLV.ProducerCode&quot;)"
                      value="policy.PrimaryPolicyCommission.ProducerCode.Code"/>
                  </Row>
                </RowIterator>
              </ListViewPanel>
            </ListViewInput>
          </InputSet>
          <InputSet
            id="Promises"
            visible="wizardState.DistributeToPromise">
            <RangeInput
              editable="true"
              id="SelectPromise"
              label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.Promise&quot;)"
              required="true"
              value="wizardState.PaymentMoneySetup.AppliedFromPromise"
              valueRange="moneySetup.Producer.findActivePromises()"
              valueType="entity.AgencyCyclePromise"
              visible="wizardState.PaymentMoneySetup != null">
              <PostOnChange
                deferUpdate="false"/>
            </RangeInput>
          </InputSet>
          <InputSet
            id="SavedDistribution"
            visible="wizardState.DistributeToSaved">
            <TextInput
              id="SelectSavedDistribution"
              label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.SavedDistribution&quot;, moneySetup.DistributionTypeName)"
              required="true"
              value="moneySetup.SavedDistribution"
              valueType="entity.AgencyCycleDist"/>
          </InputSet>
          <InputSet
            id="ModifyingDistribution"
            visible="moneySetup.EditingExecutedDistribution">
            <TextInput
              id="ModifyingDistribution"
              label="DisplayKey.get(&quot;Web.AgencyDistributionWizard.Step.MoneyDetails.ModifyingDistribution&quot;, moneySetup.DistributionTypeName)"
              required="true"
              value="moneySetup.PreviouslyExecutedDistribution"
              valueType="entity.AgencyCycleDist"/>
          </InputSet>
        </InputFooterSection>
      </DetailViewPanel>
    </PanelRef>
    <Code><![CDATA[property get Header() : String {
  if (wizardState.IsCreditDistribution) {
    return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.DistributionDetailsHeader.CreditDistribution")
  } else if (wizardState.IsPayment) {
    return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.DistributionDetailsHeader.Payment")
  } else {
    return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.DistributionDetailsHeader.Promise")
  }
}

property get DistributionDescription() : String {
  if (wizardState.IsCreditDistribution) {
    return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.DistributionDescription.CreditDistribution")
  } else if (wizardState.IsPayment) {
    return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.DistributionDescription.Payment")
  } else {
    return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.DistributionDescription.Promise")
  }
}

property get AmountLabel() : String {
if (wizardState.IsPayment) {
    return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.Amount.Payment")
  } else if (wizardState.IsPromise) {
    return DisplayKey.get("Web.AgencyDistributionWizard.Step.MoneyDetails.Amount.Promise")
  } else {
    return ""
  }
}]]></Code>
  </Screen>
</PCF>