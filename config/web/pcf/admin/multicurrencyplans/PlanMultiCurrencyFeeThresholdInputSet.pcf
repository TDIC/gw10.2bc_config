<?xml version="1.0"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../../pcf.xsd">
  <InputSet
    id="PlanMultiCurrencyFeeThresholdInputSet">
    <Require
      name="plan"
      type="MultiCurrencyPlan"/>
    <Require
      name="feeThresholdDefaultPropertyInfo"
      type="gw.lang.reflect.IPropertyInfo"/>
    <Require
      name="feeThresholdLabel"
      type="String"/>
    <Require
      name="required"
      type="boolean"/>
    <Require
      name="alwaysEditable"
      type="Boolean"/>
    <Require
      name="validationFunc"
      type="gw.lang.function.IBlock"/>
    <Variable
      initialValue="plan.Currencies"
      name="currencies"
      recalculateOnRefresh="true"
      type="Currency[]"/>
    <Variable
      initialValue="isMultiCurrencyMode()"
      name="multiCurrencyMode"
      type="Boolean"/>
    <MonetaryAmountInput
      currency="plan.Currencies[0]"
      editable="!plan.InUse || alwaysEditable"
      id="FeeThresholdSingleCurrency"
      label="feeThresholdLabel"
      required="required"
      validationExpression="validationFunc?.invokeWithArgs({plan, plan.Currencies[0]})"
      value="(plan[feeThresholdDefaultPropertyInfo.Name + plan.Currencies[0].Code] as FeeThresholdDefault).Value"
      visible="!isMultiCurrencyMode()"/>
    <InputSet
      id="FeeThresholdInputGroup"
      visible="isMultiCurrencyMode()">
      <InputColumnSubGroup
        label="feeThresholdLabel"
        useStandardInputLabelStyle="true">

        <InputIterator
          elementName="currency"
          forceRefreshDespiteChangedEntries="true"
          value="currencies //Changing this to be backed by a query will almost certainly cause this page to break because of the use of forceRefresh property on the iterator"
          valueType="Currency[]">
          <MonetaryAmountInput
            currency="currency"
            editable="alwaysEditable || !plan.InUse || (plan[feeThresholdDefaultPropertyInfo.Name + currency.Code] as KeyableBean).New"
            id="FeeThreshold"
            label="currency.DisplayName"
            required="required"
            validationExpression="validationFunc?.invokeWithArgs({plan, currency})"
            value="(plan[feeThresholdDefaultPropertyInfo.Name + currency.Code] as FeeThresholdDefault).Value"/>
        </InputIterator>
      </InputColumnSubGroup>
    </InputSet>
    <Code><![CDATA[function isMultiCurrencyMode() : boolean {
      return gw.api.util.CurrencyUtil.isMultiCurrencyMode() and Currency.getTypeKeys(true).Count > 1
    }]]></Code>
  </InputSet>
</PCF>