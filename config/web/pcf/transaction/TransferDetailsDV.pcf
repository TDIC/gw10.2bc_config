<?xml version="1.0" encoding="UTF-8"?>
<PCF
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="../../../../../pcf.xsd">
  <DetailViewPanel
    id="TransferDetailsDV">
    <Require
      name="fundsTransferUtil"
      type="gw.api.web.transaction.FundsTransferUtil"/>
    <Require
      name="canEditPage"
      type="Boolean"/>
    <InputColumn>
      <InputSet
        editable="canEditPage"
        id="AccountInfo"
        visible="fundsTransferUtil.SourceType == TAccountOwnerType.TC_ACCOUNT">
        <Variable
          initialValue="fundsTransferUtil.SourceType == TAccountOwnerType.TC_ACCOUNT ? fundsTransferUtil.SourceOwner as Account : null"
          name="account"
          type="Account"/>
        <TextInput
          id="AccountNumber"
          label="DisplayKey.get(&quot;Web.TransferDetailsDV.AccountNumber&quot;)"
          value="account.AccountNumber"/>
        <RangeInput
          editable="true"
          id="UnappliedFunds"
          label="DisplayKey.get(&quot;Web.TransferDetailsDV.UnappliedFund&quot;)"
          required="true"
          value="fundsTransferUtil.SourceUnappliedFunds"
          valueRange="account.UnappliedFundsSortedByDisplayName"
          valueType="entity.UnappliedFund"
          visible="account.HasDesignatedUnappliedFund">
          <PostOnChange/>
        </RangeInput>
        <MonetaryAmountInput
          align="right"
          currency="account.Currency"
          formatType="currency"
          id="UnappliedAmount"
          label="DisplayKey.get(&quot;Web.TransferDetailsDV.UnappliedAmount&quot;)"
          value="fundsTransferUtil.SourceUnappliedFunds.Balance"
          visible="true"/>
      </InputSet>
      <InputSet
        editable="canEditPage"
        id="producerInfo"
        visible="fundsTransferUtil.SourceType == TAccountOwnerType.TC_PRODUCER">
        <Variable
          initialValue="fundsTransferUtil.SourceType == TAccountOwnerType.TC_PRODUCER ? fundsTransferUtil.SourceOwner as Producer : null"
          name="producer"
          type="Producer"/>
        <TextInput
          id="ProducerName"
          label="DisplayKey.get(&quot;Web.TransferDetailsDV.ProducerName&quot;)"
          value="producer.DisplayName"/>
        <MonetaryAmountInput
          align="right"
          currency="producer.Currency"
          formatType="currency"
          id="AvailableTransferAmount"
          label="DisplayKey.get(&quot;Web.TransferDetailsDV.UnappliedAmount&quot;)"
          value="producer.UnappliedAmount"/>
      </InputSet>
      <Label
        label="DisplayKey.get(&quot;Web.TransferDetailsDV.Details&quot;)"/>
      <RangeRadioInput
        editable="canEditPage"
        id="Target"
        label="DisplayKey.get(&quot;Web.TransferDetailsDV.Target&quot;)"
        required="true"
        stacked="true"
        value="fundsTransferUtil.TargetType"
        valueRange="getAvailableSourceTypes()"
        valueType="typekey.TAccountOwnerType"
        visible="canEditPage">
        <PostOnChange
          onChange="cleanRows()"/>
      </RangeRadioInput>
      <ListViewInput
        def="TransferDetailsLV(fundsTransferUtil, canEditPage)"
        labelAbove="true"
        mode="fundsTransferUtil.TargetType.Code">
        <Toolbar>
          <IteratorButtons
            addVisible="canEditPage"
            iterator="TransferDetailsLV.TransferDetailsLV"
            removeVisible="canEditPage"/>
        </Toolbar>
      </ListViewInput>
    </InputColumn>
    <Code><![CDATA[function removeRow(transfer : FundsTransfer) {
  transfer.remove();
  fundsTransferUtil.removeFromTransfers(transfer);
}

function cleanRows() {
  fundsTransferUtil.TransferTargets.each(\ transfer : FundsTransfer -> removeRow(transfer));
  fundsTransferUtil.addToTransfers(new FundsTransfer(fundsTransferUtil.SourceOwner.Currency))
}

public static function validateAmount(value : gw.pl.currency.MonetaryAmount) : String{
  return value.IsPositive ? null : "Amount has to be a positive number."
}


public static function validateAmount(pFundsTransferUtil : gw.api.web.transaction.FundsTransferUtil) : String {
  if (!pFundsTransferUtil.validateAmounts()) {
    return DisplayKey.get("Web.TransferDetailsDV.CannotCreateTransfer");
  } else {
    return null;
  }
}
function getAvailableSourceTypes() : TAccountOwnerType[] {
  return new TAccountOwnerType[]{TAccountOwnerType.TC_ACCOUNT} 
  //  HermiaK 10/28/2014 US70 hide the Producer radio button 
  //  TAccountOwnerType.TC_PRODUCER}
}]]></Code>
  </DetailViewPanel>
</PCF>