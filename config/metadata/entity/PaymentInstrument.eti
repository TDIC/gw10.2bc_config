<?xml version="1.0"?>
<entity
  xmlns="http://guidewire.com/datamodel"
  desc="A PaymentInstrument is a financial instrument that can be or has been used to make a payment.     If the PaymentInstrument points at an Account or a Producer, the payment instrument can be used to request more payments.     For example, a credit card payment instrument that has enough information to be used to request     more payments from the payment system can have an Account set on it.     We also request money from the Responsive (PaymentMethod) payment instruments by asking the Payer to send the payment and waiting for     them to send the money."
  entity="PaymentInstrument"
  loadable="true"
  table="paymentinstrument"
  type="retireable">
  <implementsInterface
    iface="com.guidewire.pl.system.bundle.UpdateCallback"
    impl="com.guidewire.bc.domain.payment.impl.PaymentInstrumentImpl"/>
  <implementsInterface
    iface="com.guidewire.bc.domain.payment.PaymentInstrumentPublicMethods"
    impl="com.guidewire.bc.domain.payment.impl.PaymentInstrumentImpl"/>
  <implementsInterface
    iface="com.guidewire.bc.domain.payment.impl.PaymentInstrumentInternalMethods"
    impl="com.guidewire.bc.domain.payment.impl.PaymentInstrumentImpl"/>
  <implementsInterface
    iface="com.guidewire.pl.system.bundle.RemoveCallback"
    impl="com.guidewire.bc.domain.payment.impl.PaymentInstrumentImpl"/>
  <implementsEntity
    name="NotInCurrencySilo"/>
  <typekey
    desc="The method of payment used by this instrument (credit card, EFT, etc)"
    name="PaymentMethod"
    nullok="false"
    typelist="PaymentMethod"/>
  <foreignkey
    columnName="AccountID"
    desc="The Account which has this instrument as one of its available instruments for initiating payments.             A PaymentInstrument may only belong to one Account or Producer (or neither), so if this field is set,             Producer must be null."
    exportasid="true"
    fkentity="Account"
    name="Account"
    nullok="true"/>
  <foreignkey
    columnName="ProducerID"
    desc="The Producer which has this instrument as one of its available instruments for initiating payments.             A PaymentInstrument may only belong to one Account or Producer (or neither), so if this field is set,             Account must be null."
    exportasid="true"
    fkentity="Producer"
    name="Producer"
    nullok="true"/>
  <column
    default="false"
    desc="Once set, this flag indicates that the PaymentInstrument can no longer be modified or removed.  It is used for singleton PaymentMethods like Cash."
    name="Immutable"
    nullok="false"
    setterScriptability="doesNotExist"
    type="bit"/>
  <column
    desc="The identifier of the PaymentInstrument in the external payment system"
    name="Token"
    nullok="true"
    type="shorttext"/>
  <column
    desc="A text field to contain display information."
    name="Detail"
    nullok="true"
    type="shorttext"/>
  <checkconstraint
    desc="A PaymentInstrument may be associated with either an Account or a Producer for re-use, but not both."
    predicate="((AccountID IS NULL) AND (ProducerID IS NULL))                     OR ((AccountID IS NOT NULL) AND (ProducerID IS NULL))                     OR ((AccountID IS NULL) AND (ProducerID IS NOT NULL))"/>
  <index
    desc="Immutable PaymentInstruments per PaymentMethod"
    name="Immutables"
    unique="true">
    <indexcol
      keyposition="1"
      name="Immutable"/>
    <indexcol
      keyposition="2"
      name="PaymentMethod"/>
    <indexcol
      keyposition="3"
      name="Retired"/>
    <indexcol
      keyposition="4"
      name="ID"/>
  </index>
  <index
    desc="Searches on Token"
    name="Token">
    <indexcol
      keyposition="1"
      name="Token"/>
    <indexcol
      keyposition="2"
      name="Retired"/>
  </index>
</entity>
