<?xml version="1.0"?>
<subtype
  supertype="MultiCurrencyPlan"
  xmlns="http://guidewire.com/datamodel"
  desc="A delinquency plan containing a set of triggers to execute on a delinquency process.             Also holds information about conditions for delinquency, such as threshold overdue premium amount."
  entity="DelinquencyPlan"
  setterScriptability="all">
  <implementsInterface
    iface="com.guidewire.bc.domain.delinquency.impl.DelinquencyPlanInternalMethods"
    impl="com.guidewire.bc.domain.delinquency.impl.DelinquencyPlanImpl"/>
  <implementsInterface
    iface="com.guidewire.bc.domain.delinquency.DelinquencyPlanPublicMethods"
    impl="com.guidewire.bc.domain.delinquency.impl.DelinquencyPlanImpl"/>
  <column
    default="0"
    name="GracePeriodDays"
    nullok="false"
    type="integer"/>
  <typekey
    default="calendar"
    name="GracePeriodDayUnit"
    nullok="false"
    typelist="DayUnit"/>
  <column
    default="false"
    desc="If true, InvoiceItems associated with delinquent policy periods will be held"
    name="HoldInvoicingOnDlnqPolicies"
    nullok="false"
    type="bit"/>
  <typekey
    desc="The market segments for which this delinquency plan may be used"
    name="ApplicableSegments"
    nullok="true"
    typelist="ApplicableSegments"/>
  <typekey
    name="CancellationTarget"
    nullok="false"
    typelist="CancellationTarget"/>
  <array
    arrayentity="DelinquencyPlanReason"
    name="DelinquencyPlanReasons"
    owner="true">
    <link-association>
      <typelist-map
        field="DelinquencyReason"/>
    </link-association>
  </array>
  <array
    arrayentity="LateFeeAmountDefault"
    desc="The default late fee for each supported currency"
    name="LateFeeAmountDefaults"
    owner="true"
    setterScriptability="doesNotExist">
    <link-association>
      <typelist-map
        field="Currency"
        propertyPrefix="LateFeeAmountDefaults"/>
    </link-association>
  </array>
  <array
    arrayentity="ReinstatemntFeeAmtDefault"
    desc="The default reinstatement fee for each supported currency"
    name="ReinstatementFeeAmountDefaults"
    owner="true"
    setterScriptability="doesNotExist">
    <link-association>
      <typelist-map
        field="Currency"
        propertyPrefix="ReinstatementFeeAmountDefaults"/>
    </link-association>
  </array>
  <array
    arrayentity="WriteoffThresholdDefault"
    desc="The default writeoff threshold for each supported currency"
    name="WriteoffThresholdDefaults"
    owner="true"
    setterScriptability="doesNotExist">
    <link-association>
      <typelist-map
        field="Currency"
        propertyPrefix="WriteoffThresholdDefaults"/>
    </link-association>
  </array>
  <array
    arrayentity="CancellationThresholdDefault"
    desc="The default cancellation threshold for each supported currency"
    name="CancellationThresholdDefaults"
    owner="true"
    setterScriptability="doesNotExist">
    <link-association>
      <typelist-map
        field="Currency"
        propertyPrefix="CancellationThresholdDefaults"/>
    </link-association>
  </array>
  <array
    arrayentity="ExitDlnqThresholdDefault"
    desc="The default exit delinquency threshold for each supported currency"
    name="ExitDelinquencyThresholdDefaults"
    owner="true"
    setterScriptability="doesNotExist">
    <link-association>
      <typelist-map
        field="Currency"
        propertyPrefix="ExitDelinquencyThresholdDefaults"/>
    </link-association>
  </array>
  <array
    arrayentity="PolEntDlnqThresholdDefault"
    desc="The default policy enter delinquency threshold for each supported currency"
    name="PolEnterDelinquencyThresholdDefaults"
    owner="true"
    setterScriptability="doesNotExist">
    <link-association>
      <typelist-map
        field="Currency"
        propertyPrefix="PolEnterDelinquencyThresholdDefaults"/>
    </link-association>
  </array>
  <array
    arrayentity="AcctEntDlnqThresholdDefault"
    desc="The default account enter delinquency threshold for each supported currency"
    name="AcctEnterDelinquencyThresholdDefaults"
    owner="true"
    setterScriptability="doesNotExist">
    <link-association>
      <typelist-map
        field="Currency"
        propertyPrefix="AcctEnterDelinquencyThresholdDefaults"/>
    </link-association>
  </array>
  <dbcheckbuilder
    className="com.guidewire.bc.system.database.dbchecks.delinquency.AccountEnterDelinquencyThresholdDBCheckBuilder"/>
  <dbcheckbuilder
    className="com.guidewire.bc.system.database.dbchecks.delinquency.PolicyEnterDelinquencyThresholdDBCheckBuilder"/>
  <dbcheckbuilder
    className="com.guidewire.bc.system.database.dbchecks.multicurrency.DelinquencyPlanMultiCurrencyDBCheckBuilder"/>
</subtype>
