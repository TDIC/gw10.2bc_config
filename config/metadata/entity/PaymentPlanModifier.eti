<?xml version="1.0"?>
<entity
  xmlns="http://guidewire.com/datamodel"
  abstract="true"
  desc="A PaymentPlanModifier modifies a provided payment plan. PaymentPlanModifier itself is abstract and serves as the supertype of other PaymentPlanModifier subtypes. A subtype of PaymentPlanModifier does not need to have a subclass of this class -- it will automatically delegate the implementation of PaymentPlanModifierMethods to its custom class of PaymentPlanModifierMethodsImpl. That custom class is specified in the subtype's .eti file with the implementsInterface element."
  entity="PaymentPlanModifier"
  exportable="true"
  final="false"
  table="paymentplanmodifier"
  type="retireable">
  <implementsInterface
    iface="com.guidewire.bc.domain.invoice.impl.PaymentPlanModifierInternalMethods"
    impl="com.guidewire.bc.domain.invoice.impl.PaymentPlanModifierImpl"/>
  <!-- Subtype .eti files should also specify implementsInterface, with the same iface but with impl= a class implementing PaymentPlanModifierMethods -->
  <implementsInterface
    iface="gw.api.domain.invoice.PaymentPlanModifierMethods"/>
  <implementsEntity
    name="NotInCurrencySilo"/>
  <implementsEntity
    name="Extractable"/>
  <foreignkey
    columnName="PolicyBillingInstructionID"
    desc="The policy billing instruction which uses this modifier.  Null if the modifier is not used by a billing instruction."
    fkentity="PlcyBillingInstruction"
    name="policyBillingInstruction"
    nullok="true"
    setterScriptability="hidden"/>
  <column
    desc="The order in which the modifier's billing instruction applies this modifier when the billing instruction has multiple modifiers. Null if the modifier is not used by a billing instruction."
    getterScriptability="hidden"
    name="ApplicationOrder"
    nullok="true"
    setterScriptability="hidden"
    type="integer"/>
</entity>
