<?xml version="1.0"?>
<entity
  xmlns="http://guidewire.com/datamodel"
  desc="An default unapplied or designated unapplied and set of associated invoice streams for the purposes of being able separate cash buckets"
  entity="UnappliedFund"
  table="unappliedfund"
  type="editable">
  <implementsInterface
    iface="com.guidewire.bc.domain.fundstracking.impl.FundsUse"
    impl="com.guidewire.bc.domain.accounting.impl.UnappliedFundImpl"/>
  <implementsInterface
    iface="com.guidewire.pl.system.bundle.UpdateCallback"
    impl="com.guidewire.bc.domain.accounting.impl.UnappliedFundImpl"/>
  <implementsInterface
    iface="com.guidewire.bc.domain.accounting.UnappliedFundPublicMethods"
    impl="com.guidewire.bc.domain.accounting.impl.UnappliedFundImpl"/>
  <implementsInterface
    iface="com.guidewire.bc.domain.accounting.impl.UnappliedFundInternalMethods"
    impl="com.guidewire.bc.domain.accounting.impl.UnappliedFundImpl"/>
  <implementsInterface
    iface="com.guidewire.bc.domain.fundstracking.impl.FundsSource"
    impl="com.guidewire.bc.domain.accounting.impl.UnappliedFundImpl"/>
  <implementsEntity
    name="InCurrencySilo"/>
  <foreignkey
    archivingOwner="source"
    columnName="TAccountID"
    desc="The default unapplied or designated unapplied t-account that is wrapped by this unappliedfund object.  There should be one and only one unapplied t-account per unappliedfund and vice versa."
    exportable="false"
    fkentity="TAccount"
    getterScriptability="hidden"
    name="TAccount"
    nullok="false"
    overwrittenInStagingTable="true"
    setterScriptability="doesNotExist"/>
  <foreignkey
    columnName="AccountID"
    desc="The account on which this unappliedfund belongs.  This is a denormalization of TAccount.TAccountOwner."
    exportable="false"
    exportasid="true"
    fkentity="Account"
    name="Account"
    nullok="false"
    setterScriptability="doesNotExist"/>
  <foreignkey
    columnName="DefaultAccountID"
    desc="If not null, the account for which this is the default unappliedfund. Referenced by onetoone"
    exportable="false"
    fkentity="Account"
    getterScriptability="doesNotExist"
    importableagainstexistingobject="false"
    name="DefaultAccount"
    nullok="true"
    setterScriptability="doesNotExist"/>
  <foreignkey
    columnName="PolicyID"
    desc="The optional policy for which this unappliedfund was created using policy level billing/designated unapplied."
    exportable="false"
    exportasid="true"
    fkentity="Policy"
    name="Policy"
    nullok="true"
    setterScriptability="doesNotExist"/>
  <foreignkey
    columnName="ReportingGroupID"
    desc="The reporting group assigned, if any. Once a reporting group is assigned, it may never be updated."
    exportasid="true"
    fkentity="ReportingGroup"
    name="ReportingGroup"
    nullok="true"/>
  <column
    desc="Denorm of the TAccount description, also used for staging TAccounts."
    name="Description"
    nullok="true"
    setterScriptability="doesNotExist"
    type="shorttext"/>
  <index
    desc="There should be one and only one unappliedfund per unapplied t-account."
    name="taccountunique"
    unique="true">
    <indexcol
      keyposition="1"
      name="TAccountID"/>
  </index>
  <checkconstraint
    desc="The DefaultAccount must point to the Account if it is not null."
    predicate="(DefaultAccountID IS NOT NULL AND DefaultAccountID = AccountID) OR DefaultAccountID IS NULL"/>
  <dbcheckbuilder
    className="com.guidewire.bc.system.database.dbchecks.accounting.UnappliedFundsDBCheckBuilder"/>
</entity>
