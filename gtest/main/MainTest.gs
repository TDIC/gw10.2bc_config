package main

uses java.io.File
uses java.io.FilenameFilter
uses gw.lang.reflect.TypeSystem
uses java.io.FileFilter
uses java.lang.StringBuffer
uses java.lang.System
uses java.util.Date
uses gw.testharness.TestBase
uses org.slf4j.Logger
uses java.lang.Exception
uses java.lang.Throwable
uses gw.lang.reflect.ITypeInfoMethodInfo
uses gw.lang.reflect.IMethodInfo
uses org.slf4j.LoggerFactory

/**
 * Base class to run all the tests, this will ensure that the server configuration is loaded only once,
 * This should considerably help reduce the time required to run GUnits
 *
 * @author Ravi Ganta
 */

@gw.testharness.ServerTest
class MainTest extends gw.testharness.TestBase {

  public static var LOGGER_CATEGORY : String = "ABC.GUnit"
  var _logger : Logger

  private static final var REPORT_FILE_NAME : String    = "TestReport"
  private static final var REPORT_FILE_TYPE : String    = ".xml"
  private static final var REPORT_DIR = "reportdir";
  private static final var PACKAGES_UNDER_TEST = "testpackage";
  private var suiteErrors :int  = 0
  private var suiteFailures : int = 0

  private var reportIndex : int = 0
  private var reportDir : String = ""

  var testsuitebegin = "<testsuite name=\"%s\" errors=\"%d\" failures=\"%d\" tests=\"%d\" time=\"%s\" timestamp=\"%s\">"
  var testsuiteend = "</testsuite>"

  // Filter to get the Test classes
  var testsfilter : FilenameFilter = new FilenameFilter() {
    override function accept(file : File, filename : String) : boolean {
      return filename.endsWith("Test.gs")
    }
  }
  // Filter to get the sub-directories
  var dirfilter : FileFilter = new FileFilter() {
    override function accept(file : File) : boolean {
      return file.Directory
    }
  }

  // Utility function to replace XML delimiter characters
  function escapeXMLCharacters(xmlString : String) : String {
    xmlString = xmlString.replaceAll("&", "&amp;")
    xmlString = xmlString.replaceAll("'", "&apos;")
    xmlString = xmlString.replaceAll("\"", "&quot;")
    xmlString = xmlString.replaceAll("<", "&lt;")
    xmlString = xmlString.replaceAll(">", "&gt;")
    return xmlString
  }

/**
 * Run all methods within a class statring with test
 *
 * @param clazz Class fully qualified name
 */
    function runTestMethods(instance:Object,aMethod : IMethodInfo,bMethod : IMethodInfo,testmethod: IMethodInfo) :String {

      var methodName : String = ""
      var testcasebegin = "<testcase classname=\"%s\" name=\"%s\" time=\"%s\">"
      var errorbegin = "<error message=\"%s\" />"
      var failurebegin = "<failure message=\"%s\" type=\"%s\" />"
      var suitereport = new StringBuffer()
      var testresultstr = new StringBuffer()
      var errors :int  = 0
      var failures : int = 0
      var testscount : int = 0
      var testsuiteStarttime : long = new Date().getTime()
      var testcaseStarttime : long
      var errorStr : String = ""


      // Initialise the counter and get the starttime
      errorStr = ""
      methodName = ""
      testscount = testscount + 1
      testcaseStarttime = new Date().getTime()

      try {
        // Call the beforemethod before invoking a test method to initialize method variables
        if (bMethod != null) {
          methodName = bMethod.DisplayName
          bMethod.CallHandler.handleCall(instance, null)
        }

        // Call the actual test method
        methodName = testmethod.DisplayName
        _logger.info("**** Executing test method  " + methodName )
        testmethod.CallHandler.handleCall(instance, null)

        // Call the aftermethod after invoking a test method to cleanup
        if (aMethod != null) {
          methodName = aMethod.DisplayName
          aMethod.CallHandler.handleCall(instance, null)
        }

      } catch (et:Throwable) {
        if (et typeis java.lang.Error) {
          errorStr = String.format(errorbegin, new Object[]{escapeXMLCharacters(et.StackTraceAsString)})
          suiteErrors++
        } else {
          errorStr = String.format(failurebegin, new Object[]{escapeXMLCharacters(et.StackTraceAsString), escapeXMLCharacters(et.toString())})
          suiteFailures++
        }
      }

      try
      {
          var testcaseEndtime : long = new Date().getTime()
          var testcaseElapsedTime: double = (testcaseEndtime - testcaseStarttime) / 1000.00

          // Build the report for the testcase
          testresultstr.append(String.format(testcasebegin, new Object[]{instance.toString(), methodName, testcaseElapsedTime}))
          testresultstr.append(errorStr)
          testresultstr.append("</testcase>")
      }
      catch (exf:Exception) {
              _logger.error("Error executing finally test blo0ck ",exf)
      }
      return testresultstr as String
    }

      /**
   * Run all methods within a class statring with test
   *
   * @param clazz Class fully qualified name
   */
  function runAllTestsInClass(clazz : String)  {

    var suitereport = new StringBuffer()
    var testresultstr = new StringBuffer()
    var testscount : int = 0
    var testsuiteStarttime : long = new Date().getTime()
    var type = TypeSystem.getByFullName(clazz)

    var instance :Object
    try
    {
      instance = type.TypeInfo.getConstructor(null).Constructor.newInstance(null)
    }
        catch (e:Exception )
        {
          _logger.error("Cannot create an instance- " + clazz,e)
          return
        }



    // Get the beforeXXX and afterXXX methods
    var bClassMethod = (typeof instance).TypeInfo.Methods.firstWhere(\ method -> method.Name.equalsIgnoreCase("beforeClass()"))
    var aClassMethod = (typeof instance).TypeInfo.Methods.firstWhere(\ method -> method.Name.equalsIgnoreCase("afterClass()"))

    var bMethod = (typeof instance).TypeInfo.Methods.firstWhere(\ method -> method.Name.equalsIgnoreCase("beforeMethod()"))
    var aMethod = (typeof instance).TypeInfo.Methods.firstWhere(\ method -> method.Name.equalsIgnoreCase("afterMethod()"))

    // Get all the test methods for the Test class
    var testmethods = (typeof instance).TypeInfo.Methods.where(\ method -> method.Name.startsWith("test"))

    var testcaseStarttime : long
    var errorStr : String = ""

    // Call the BeforeClass method
    if (bClassMethod != null) {
      try {
        bClassMethod.CallHandler.handleCall(instance, null)
      }catch (e) {
        _logger.error("Error executing BeforeClassMethod for class - " + clazz,e)
        return // Go back to the next class
      }
    }

    // Execute the test methods
    try
    {
      if(testmethods!=null)
      {
        _logger.info("****************************** START TEST METHODS ****************** ")
        for(method in testmethods)
        {
          testresultstr.append(runTestMethods(instance,aMethod,bMethod,method))
          testscount++;
         }
      }
    }
        catch (e:Exception) {
          _logger.error("Exception test method ",e)
        }

    _logger.info("****************************** END TEST METHODS ****************** ")

    // Call the afterClass method
    if (aClassMethod != null) {
      try {
        aClassMethod.CallHandler.handleCall(instance, null)
      } catch (e) {
        _logger.error((\ -> "Error executing AfterClassMethod for class - " + clazz) as String)
      }
    }

    // calculate time difference and convert to seconds
    var testsuiteEndtime : long = new Date().getTime()
    var elapsedtime : double = (testsuiteEndtime - testsuiteStarttime) / 1000.00

    var elapsedtimeStr = String.valueOf(elapsedtime)

    // Only add if atleast one test was run in a package

    if (testscount>0) {
      suitereport.append(String.format(testsuitebegin, new Object[]{clazz, suiteErrors, suiteFailures, testscount, elapsedtimeStr, new Date().toGMTString()}))
      suiteErrors=0
      suiteFailures=0
      suitereport.append(testresultstr)
      suitereport.append(testsuiteend)

      // Generate the report file
      reportIndex = reportIndex + 1
      var reportFile = reportDir + File.separatorChar + REPORT_FILE_NAME + reportIndex + REPORT_FILE_TYPE
      TestUtil.createReport(reportFile, suitereport.toString())
    }
  }


  /**
   * Run a specific class tests
   *
   * @param pkg Package name
   * @param test  Class file to test
   */
  function runClass(pkg : String,test : File)  {

    // Variables to capture the report for each test
    // Build the package name of the Test class
    var clazz = pkg + "." + test.Name.replaceAll(".gs", "")
    var type = TypeSystem.getByFullName(clazz)
    _logger.info("**** START TEST : " + clazz + "*****")
    if (not type.Abstract and   type.AllTypesInHierarchy.contains(TestBase)){
      try
      {
        runAllTestsInClass(clazz)
      }
          catch(e:Exception)
          {
            _logger.error("**** runClass: error running clazz tests ",e)
          }
    }
    _logger.info("**** END TEST : " + clazz + "*****")

  }

  /**
   * Helper method to recursively run all the tests
   *
   * @param testdir folder containing the test classes
   * @param pkg     base package name for the tests
   */
  function runTests(testdir : String, pkg : String)  {


    // Get all the GUnit test classes in a folder
    var basedir = new File(testdir)
    var tests : File[] = basedir.listFiles(testsfilter)

    // Run each test class
    if (tests!=null and tests.length>0)
    {


      try
      {
        for(test in tests)
          runClass(pkg ,test)
      }
          catch(e:Exception)
          {
            _logger.error("** ERROR TEST ** : " ,e)
          }

    }
    // Get all the sub-directories and then recursively run the tests under each sub-directory
    var dirs = basedir.listFiles(dirfilter)

    if (dirs!=null)
      dirs.each(\ dir -> {
        var newpkg = pkg + "." + dir.Name
        runTests(dir.AbsolutePath, newpkg)
      })

  }

  /**
   * Main method to run the tests
   */
  function testAll() {

    _logger = LoggerFactory.getLogger(LOGGER_CATEGORY)

    _logger.info("**** testAll START **** ")

    // Read the basetestpackage and reportfile
    var testpackages = getPackages()

    reportDir = System.getProperty(REPORT_DIR)

    if (testpackages == null || reportDir == null) {
      fail("Missing environment properties, testpackage and reportdir are required.")
    }

    // Delete the existing files from the report directory
    var rptDir = new File(reportDir)
    var oldFiles = rptDir.listFiles()
    if (oldFiles != null) {
      oldFiles.each(\ oldFile -> {
        oldFile.delete()
      })
    }

    // Build the folder paths
    var configfolder = gw.api.util.ConfigAccess.getModuleRoot("configuration").AbsolutePath


    // run tests and build report
    for (basetestpackage in testpackages)
    {
      var testsfolder = configfolder + File.separatorChar + "gtest" + File.separatorChar + basetestpackage.replaceAll("\\.", "/")
      runTests(testsfolder, basetestpackage)
    }
    _logger.info("**** testAll END **** ")


  }

  private function getPackages() :String[] {
    var packageString = System.getProperty(PACKAGES_UNDER_TEST)
    if (packageString!=null)
      return packageString.split(",")
    else
      return null
  }
}