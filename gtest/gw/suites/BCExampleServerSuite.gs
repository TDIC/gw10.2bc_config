package gw.suites

uses gw.api.test.BCServerTestClassBase
uses gw.api.test.SuiteBuilder
uses junit.framework.Test

@Export
class BCExampleServerSuite {

  public static final var NAME : String = "BCExampleServerSuite"

  public static function suite() : Test {
    return new SuiteBuilder(BCServerTestClassBase)
        .withSuiteName(NAME)
        .build()
  }

}
