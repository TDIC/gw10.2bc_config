package tdic.holiday

uses gw.api.util.DateUtil

/**
 * GUnit test for holidays and business days.
 * This test has a dependency on the holidays that are loaded through the Excel Data Loader / Admin Data Loader,
 * and therefore needs the database to be up.
 */
@gw.testharness.ServerTest
class HolidayTest extends gw.testharness.TestBase {

  /**
   * Weekends are configured in config.xml.  A failed test indicates a bad configuration in config.xml.
   */
  function testWeekend() {
    print("Testing weekend day...")
    var testDate = DateUtil.createDateInstance(8, 9, 2014)
    print("  Test Date: " + testDate + " " + testDate.DayOfWeekName)
    assertFalse(DateUtil.isBusinessDay(testDate, HolidayTagCode.TC_GENERAL))
    print("  Not a business day.")
    print("  Adding 1 business day.")
    testDate = DateUtil.addBusinessDays(testDate, 1)
    print("  New Date: " + testDate + " " + testDate.DayOfWeekName)
    assertTrue(DateUtil.isBusinessDay(testDate, HolidayTagCode.TC_GENERAL))
    print("  Is a business day.")
    assertEquals(testDate.DayOfMonth, 11)
  }

  /**
   * Holidays are entities within BillingCenter.  A failed test indicates an admin data loader issue.
   */
  function testHoliday() {
    print("Testing holiday (Thanksgiving 2014)...")
    var testDate = DateUtil.createDateInstance(11, 27, 2014)
    print("  Test Date: " + testDate + " " + testDate.DayOfWeekName)
    assertFalse(DateUtil.isBusinessDay(testDate, HolidayTagCode.TC_GENERAL))
    print("  Not a business day.")
    testDate = testDate.addDays(-1)
    print("  Subtracting 1 day.  New Date: " + testDate + " " + testDate.DayOfWeekName)
    assertTrue(DateUtil.isBusinessDay(testDate, HolidayTagCode.TC_GENERAL))
    print("  Adding 1 business day.")
    testDate = DateUtil.addBusinessDays(testDate, 1)
    print("  New Date: " + testDate + " " + testDate.DayOfWeekName)
    assertTrue(DateUtil.isBusinessDay(testDate, HolidayTagCode.TC_GENERAL))
    print("  Is a business day.")
    assertEquals(testDate.DayOfMonth, 28)
    assertTrue(true)
  }

  /**
   * Combination of the weekend test and the holiday test above.
   */
  function testWeekendHolidayCombination() {
    print("Testing weekend day combined with holiday (Labor Day 2014)...")
    var testDate = DateUtil.createDateInstance(8, 30, 2014)
    print("  Test Date: " + testDate + " " + testDate.DayOfWeekName)
    assertFalse(DateUtil.isBusinessDay(testDate, HolidayTagCode.TC_GENERAL))
    print("  Not a business day.")
    print("  Adding 1 business day.")
    testDate = DateUtil.addBusinessDays(testDate, 1)
    print("  New Date: " + testDate + " " + testDate.DayOfWeekName)
    assertTrue(DateUtil.isBusinessDay(testDate, HolidayTagCode.TC_GENERAL))
    print("  Is a business day.")
    assertEquals(testDate.MonthOfYear, 9)
    assertEquals(testDate.DayOfMonth, 2)
  }

}