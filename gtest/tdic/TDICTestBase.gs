package tdic

uses gw.api.system.PLLoggerCategory
uses gw.api.databuilder.AccountBuilder
uses gw.api.databuilder.PaymentInstrumentBuilder
uses gw.api.databuilder.PolicyPeriodBuilder
uses gw.api.databuilder.ChargeBuilder
uses gw.pl.currency.MonetaryAmount
uses tdic.util.dataloader.command.PlanData
uses java.util.Date
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory

/**
 * TestBase class that every TDIC GUnit test should extend. Initializes the environment and sets up the _logger
 */
@gw.testharness.ServerTest
abstract class TDICTestBase extends gw.testharness.TestBase {
  protected static var _logger: Logger
  construct() {
  }

  override function beforeClass() {
    _logger = LoggerFactory.getLogger("Test")

    var planData = new PlanData()
    planData.loadPlanData()

  }

  /**
   * Creates a test account.
   */
  @Returns("the new Account")
  protected function createTestAccount() : Account {

    var account = new AccountBuilder().withNumber("0000000000").create()
    return account
  }

  /**
   * Creates a test payment instrument.
   */
  @Returns("the new Account")
  protected function createTestCreditCardPaymentInstrument() : PaymentInstrument {

    var paymentInstrument = new PaymentInstrumentBuilder().create()
    return paymentInstrument
  }

  /**
   * Creates a test Credit Card payment instrument.
   */
  @Returns("the new Account")
  protected function createTestCashPaymentInstrument() : PaymentInstrument {

    var paymentInstrument = new PaymentInstrumentBuilder().withPaymentMethod(PaymentMethod.TC_CASH).create()
    return paymentInstrument
  }

  /**
   * Creates a test ACH payment instrument.
   */
  @Returns("ACH payment instrument.")
  protected function createTestACHPaymentInstrument() : PaymentInstrument {
    var paymentInstrument = new PaymentInstrumentBuilder().withPaymentMethod(PaymentMethod.TC_ACH).create()
    return paymentInstrument
  }

  /**
   * Creates a new policy period with the input charge and payment plan
   */
  @Returns("A new policy period with the input charge and payment plan")
  protected function createPolicyPeriod(effectiveDate: Date, expirationDate: Date, charge: MonetaryAmount, account: Account, paymentPlan: PaymentPlan): PolicyPeriod {
    var premiumCharge = new ChargeBuilder()
        .withAmount(charge)
        .withCurrency(account.Currency)
        .asPremium()

    var policyPeriodBuilder = new PolicyPeriodBuilder()
        .withCurrency( account.Currency )
        .onAccount( account )
        .issuedWithCharge(premiumCharge)
        .withPaymentPlan( paymentPlan ) //Default payment plan Semi-Annual
        .withEffectiveDate(effectiveDate)
        .withExpirationDate(expirationDate)

    return policyPeriodBuilder.createAndCommit()
  }

  /**
   * Creates a new account with the input billing plan and invoice day of month
   */
  @Returns("A new policy period with the input charge and payment plan")
  protected function createAccount(billingPlan: BillingPlan, invoiceDayOfMonth: int): Account {
    var accountBuilder = new AccountBuilder()
        .withInvoiceDayOfMonth(invoiceDayOfMonth)
        .withBillingPlan(billingPlan)

    return accountBuilder.createAndCommit()
  }
}