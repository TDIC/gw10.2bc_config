package tdic.bc.config.payment

uses gw.api.util.DateUtil
uses gw.api.databuilder.AccountBuilder
uses tdic.bc.config.payment.PaymentRequestUtil

/**
 * GUnit test for holidays and business days.
 * This test has a dependency on the holidays that are loaded through the Excel Data Loader / Admin Data Loader,
 * and therefore needs the database to be up.
 */
@gw.testharness.ServerTest
class PaymentRequestUtilTest extends tdic.TDICTestBase {

  /**
   * Tests getting the next available draft date.
   */
  function testGetNextAvailableDraftDate1() {
    // First test Script Parameters to be sure they are of the correct day of month values
    print("First APW Invoice Day Of Month: " + ScriptParameters.APWInvoiceDayOfMonth1)
    assertEquals(ScriptParameters.APWInvoiceDayOfMonth1, 5)
    print("Second APW Invoice Day Of Month: " + ScriptParameters.APWInvoiceDayOfMonth2)
    assertEquals(ScriptParameters.APWInvoiceDayOfMonth2, 20)
    var tdicBillingPlan = gw.api.database.Query.make(entity.BillingPlan).compare(BillingPlan#Name, Equals, "TDIC Billing Plan").select().FirstResult
    var testAccount : Account
    gw.transaction.Transaction.runWithNewBundle( \ bundle -> {
      testAccount = createAccount(tdicBillingPlan, DateUtil.currentDate().DayOfMonth)
    }, "su")
    var dateToTest = DateUtil.createDateInstance(7, 11, 2015)
    print("Testing Next Available Date from Starting Date: " + dateToTest.toString())
    var nextDraftDate = PaymentRequestUtil.getNextAvailableDraftDate(dateToTest, testAccount)
    print("Next Draft Date: " + nextDraftDate.toString())
    var correctDate = DateUtil.createDateInstance(7, 17, 2015)
    assertTrue(DateUtil.compareIgnoreTime(nextDraftDate, correctDate) == 0)
  }

  /**
   * Tests getting the next available draft date.
   */
  function testGetNextAvailableDraftDate2() {
    // First test Script Parameters to be sure they are of the correct day of month values
    print("First APW Invoice Day Of Month: " + ScriptParameters.APWInvoiceDayOfMonth1)
    assertEquals(ScriptParameters.APWInvoiceDayOfMonth1, 5)
    print("Second APW Invoice Day Of Month: " + ScriptParameters.APWInvoiceDayOfMonth2)
    assertEquals(ScriptParameters.APWInvoiceDayOfMonth2, 20)
    var tdicBillingPlan = gw.api.database.Query.make(entity.BillingPlan).compare(BillingPlan#Name, Equals, "TDIC Billing Plan").select().FirstResult
    var testAccount : Account
    gw.transaction.Transaction.runWithNewBundle( \ bundle -> {
      testAccount = createAccount(tdicBillingPlan, DateUtil.currentDate().DayOfMonth)
    }, "su")
    var dateToTest = DateUtil.createDateInstance(7, 18, 2015)
    print("Testing Next Available Date from Starting Date: " + dateToTest.toString())
    var nextDraftDate = PaymentRequestUtil.getNextAvailableDraftDate(dateToTest, testAccount)
    print("Next Draft Date: " + nextDraftDate.toString())
    var correctDate = DateUtil.createDateInstance(8, 3, 2015)
    assertTrue(DateUtil.compareIgnoreTime(nextDraftDate, correctDate) == 0)
  }

  /**
   * Tests the nudging of the draft date depending on the possible draft logic of a Billing Plan.
   * Tests when nudging is required.
   */
  function testNudgeToDraftDayLogic1() {
    var dateToTest = DateUtil.createDateInstance(7, 11, 2015)
    print("Testing Nudging Date: " + dateToTest.toString())
    var prevBusinessDay = PaymentRequestUtil.nudgeToDraftDayLogic(dateToTest, DayOfMonthLogic.TC_PREVBUSINESSDAY)
    print("Previous Business Day: " + prevBusinessDay.toString())
    var correctDate = DateUtil.createDateInstance(7, 10, 2015)
    assertTrue(DateUtil.compareIgnoreTime(prevBusinessDay, correctDate) == 0)
    var nextBusinessDay = PaymentRequestUtil.nudgeToDraftDayLogic(dateToTest, DayOfMonthLogic.TC_NEXTBUSINESSDAY)
    print("Next Business Day: " + nextBusinessDay.toString())
    correctDate = DateUtil.createDateInstance(7, 13, 2015)
    assertTrue(DateUtil.compareIgnoreTime(nextBusinessDay, correctDate) == 0)
  }

  /**
   * Tests the nudging of the draft date depending on the possible draft logic of a Billing Plan.
   * Tests when nudging is NOT required.
   */
  function testNudgeToDraftDayLogic2() {
    var dateToTest = DateUtil.createDateInstance(7, 14, 2015)
    print("Testing Nudging Date: " + dateToTest.toString())
    var prevBusinessDay = PaymentRequestUtil.nudgeToDraftDayLogic(dateToTest, DayOfMonthLogic.TC_PREVBUSINESSDAY)
    print("Previous Business Day: " + prevBusinessDay.toString())
    var correctDate = DateUtil.createDateInstance(7, 14, 2015)
    assertTrue(DateUtil.compareIgnoreTime(prevBusinessDay, correctDate) == 0)
    var nextBusinessDay = PaymentRequestUtil.nudgeToDraftDayLogic(dateToTest, DayOfMonthLogic.TC_NEXTBUSINESSDAY)
    print("Next Business Day: " + nextBusinessDay.toString())
    correctDate = DateUtil.createDateInstance(7, 14, 2015)
    assertTrue(DateUtil.compareIgnoreTime(nextBusinessDay, correctDate) == 0)
  }

}