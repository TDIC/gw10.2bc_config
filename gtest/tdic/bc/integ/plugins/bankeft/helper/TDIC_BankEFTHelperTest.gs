package tdic.bc.integ.plugins.bankeft.helper

uses gw.api.util.DateUtil
uses tdic.bc.integ.plugins.bankeft.dto.BankEFT.BankEFTFile
uses tdic.bc.integ.plugins.bankeft.dto.BankEFT.PaymentRequestWritableBean
uses tdic.bc.integ.plugins.bankeft.helper.pymntrequest.TDIC_BankEFTHelper

uses java.text.SimpleDateFormat
uses java.math.BigDecimal

@gw.testharness.ServerTest

/**
 * US568 - Bank/EFT Integration
 * 01/05/2015 Alvin Lee
 *
 * GUnit test class for the Bank/EFT Integration.
 */
    class TDIC_BankEFTHelperTest extends gw.testharness.TestBase {

  /**
   * Test header lines in the Bank/EFT flat file.
   */
  function testConvertDataToFlatFileLines_HeaderLines() {
    print("Testing Header Lines...")
    var flatFileData = new BankEFTFile()
    var bean1 = SamplePaymentRequestLine
    flatFileData.addToPaymentRequests(bean1)
    var lines = TDIC_BankEFTHelper.convertDataToFlatFileLines(flatFileData)
    assertNotNull(lines)
    var fileHeader = lines.get(0)
    assertNotNull(fileHeader)
    print("File Header: " + fileHeader)
    var batchHeader = lines.get(1)
    assertNotNull(batchHeader)
    print("Batch Header: " + batchHeader)
    var currentFormattedDate = new SimpleDateFormat("yyMMddHHmm").format(DateUtil.currentDate())
    assertEquals(fileHeader, "101 1110000253680065239" + currentFormattedDate + "A094101BANK OF AMERICA        THE DENTISTS INSURANCE TDIC APW")
    assertEquals(batchHeader, "5200THE DENTISTS INS                    3680065239PPDTDIC PREM 150103150103   1111000020000001")
  }

  /**
   * Test payment line in the Bank/EFT flat file.
   */
  function testConvertDataToFlatFileLines_PaymentLine() {
    print("Testing Payment Lines...")
    var flatFileData = new BankEFTFile()
    var bean1 = SamplePaymentRequestLine
    flatFileData.addToPaymentRequests(bean1)
    var lines = TDIC_BankEFTHelper.convertDataToFlatFileLines(flatFileData)
    assertNotNull(lines)
    var paymentLine = lines.get(2)
    assertNotNull(paymentLine)
    print("Payment Line: " + paymentLine)
    assertEquals(paymentLine, "6279876543213245872          00000100201000000283     TEST VERY LONG NAME NE1?0111000020000000")
  }

  /**
   * Test trailer lines in the Bank/EFT flat file.
   */
  function testConvertDataToFlatFileLines_TrailerLines() {
    print("Testing Header Lines...")
    var flatFileData = new BankEFTFile()
    var bean1 = SamplePaymentRequestLine
    flatFileData.addToPaymentRequests(bean1)
    var bean2 = SamplePaymentRequestLine
    bean2.Amount = new BigDecimal("20030")
    bean2.InvoiceNumber = "1000005662"
    bean2.ContactName = "TEST NAME"
    bean2.BankRoutingNumber = "123456789"
    bean2.BankAccountNumber = "3245872"
    flatFileData.addToPaymentRequests(bean2)
    print("Payment Request Count: " + flatFileData.TotalNumPaymentRequests)
    var lines = TDIC_BankEFTHelper.convertDataToFlatFileLines(flatFileData)
    assertNotNull(lines)
    var batchTrailer = lines.get(4)
    assertNotNull(batchTrailer)
    print("Batch Trailer: " + batchTrailer)
    assertEquals(batchTrailer, "820000000201111111100000000300500000000000003680065239                         111000020000001")
    var fileTrailer = lines.get(5)
    assertNotNull(fileTrailer)
    print("File Trailer: " + fileTrailer)
    assertEquals(fileTrailer, "9000001000001000000020111111110000000030050000000000000                                       ")
  }

  /**
   * Test trailer lines in the Bank/EFT flat file, specifically having enough payment requests to force the entry hash
   * (sum of the routing numbers) to be truncated after reaching over 10 characters).
   */
  function testConvertDataToFlatFileLines_TrailerLinesEntryHash() {
    print("Testing Header Lines...")
    var flatFileData = new BankEFTFile()
    for (i in 1..200) {
      var bean = SamplePaymentRequestLine
      bean.ContactName = "TEST NAME " + i
      flatFileData.addToPaymentRequests(bean)
    }
    print("Payment Request Count: " + flatFileData.TotalNumPaymentRequests)
    var lines = TDIC_BankEFTHelper.convertDataToFlatFileLines(flatFileData)
    assertNotNull(lines)
    var batchTrailer = lines.get(202)
    assertNotNull(batchTrailer)
    print("Batch Trailer: " + batchTrailer)
    assertEquals(batchTrailer, "820000020097530864000000020040000000000000003680065239                         111000020000001")
    var fileTrailer = lines.get(203)
    assertNotNull(fileTrailer)
    print("File Trailer: " + fileTrailer)
    assertEquals(fileTrailer, "9000001000021000002009753086400000002004000000000000000                                       ")
  }

  /**
   * Gets a sample PaymentRequestWritableBean.
   */
  @Returns("A PaymentRequestWritableBean with sample data")
  property get SamplePaymentRequestLine() : PaymentRequestWritableBean {
    var paymentRequestBean = new PaymentRequestWritableBean()
    paymentRequestBean.DueDate = "2015-01-03"
    paymentRequestBean.Amount = new BigDecimal("10020")
    paymentRequestBean.InvoiceNumber = "1000000283"
    paymentRequestBean.ContactName = "TEST VERY LONG NAME NEEDING TRUNCATING"
    paymentRequestBean.BankRoutingNumber = "987654321"
    paymentRequestBean.BankAccountNumber = "3245872"
    return paymentRequestBean
  }

}