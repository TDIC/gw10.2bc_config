package tdic.bc.integ.plugins.outgoingpayment.helper

uses java.math.BigDecimal
uses tdic.bc.integ.plugins.generalledger.helper.TDIC_GLIntegrationHelper
uses java.text.SimpleDateFormat
uses gw.api.util.DateUtil
uses tdic.bc.integ.plugins.outgoingpayment.dto.OutgoingPaymentFile
uses tdic.bc.integ.plugins.outgoingpayment.dto.OutgoingPaymentWritableBean
uses tdic.bc.integ.plugins.outgoingpayment.helper.TDIC_OutgoingPaymentHelper

@gw.testharness.ServerTest

/**
 * US570 - Lawson Outgoing Payment Integration
 * 02/17/2015 Alvin Lee
 *
 * GUnit test class for the Lawson Outgoing Payment Integration.
 */
class TDIC_OutgoingPaymentHelperTest extends gw.testharness.TestBase {

  /**
   * Test getting an APC invoice flat file line from an outgoing payment.
   */
  function testGetAPCInvoiceLine() {
    var refundInvoiceNumber = "REF06082016-00002"
    var sdfFull = new SimpleDateFormat("MM/dd/yyyy")
    var currentDateString = sdfFull.format(DateUtil.currentDate())
    var sdfNoSeparators = new SimpleDateFormat("MMddyyyy")
    var currentDateStringNoSeparators = sdfNoSeparators.format(DateUtil.currentDate())
    var outgoingPaymentFile = new OutgoingPaymentFile()
    outgoingPaymentFile.addToOutgoingPayments(SampleOutgoingPaymentBean)
    var lines = TDIC_OutgoingPaymentHelper.convertDataToFlatFileLines("OutgoingPaymentInvoiceSpec.xlsx", outgoingPaymentFile)
    assertNotNull(lines)
    assertEquals(lines.Count, 1)
    var apcInvoiceLine = lines.get(0)
    print("APC Invoice Line: " + apcInvoiceLine)
    assertEquals(apcInvoiceLine, "5,999999,,REF" + currentDateStringNoSeparators + "-00001,,,,RW,5RFW,,,,,"
        + currentDateString + ",,,,,,,,0,25.19,0,0,0,0,0,,0,," + currentDateString + ",0,,80,0510,N,,,,,,,,,,1,1,"
        + currentDateString + ",,,RFW,,,N,,,,,,,,,,,,,,,,0,,0,,,IN,Y,,,bc:9001,,WC PREMIUM REFUND,,,,,,,,0,,0,,,,,,,,,,,")
  }

  /**
   * Test getting an APC distribution flat file line from an outgoing payment.
   */
  function testGetAPCDistributionLine() {
    var refundInvoiceNumber = "REF06082016-00002"
    var sdfFull = new SimpleDateFormat("MM/dd/yyyy")
    var currentDateString = sdfFull.format(DateUtil.currentDate())
    var sdfNoSeparators = new SimpleDateFormat("MMddyyyy")
    var currentDateStringNoSeparators = sdfNoSeparators.format(DateUtil.currentDate())
    var mappedTAccount = TDIC_GLIntegrationHelper.getMappedTAccountName(typekey.Transaction.TC_DISBURSEMENTPAID, LedgerSide.TC_CREDIT, false, null, GLOriginalTAccount_TDIC.TC_CASH.Code)
    mappedTAccount = mappedTAccount.replace("-", ",")
    var outgoingPaymentFile = new OutgoingPaymentFile()
    outgoingPaymentFile.addToOutgoingPayments(SampleOutgoingPaymentBean)
    var lines = TDIC_OutgoingPaymentHelper.convertDataToFlatFileLines("OutgoingPaymentDistributionSpec.xlsx", outgoingPaymentFile)
    assertNotNull(lines)
    assertEquals(lines.Count, 1)
    var apcDistributionLine = lines.get(0)
    print("APC Distribution Line: " + apcDistributionLine)
    assertEquals(apcDistributionLine, "5,999999,,REF" + currentDateStringNoSeparators
        + "-00001,,0001,25.19,0,5,0,000.06.40," + mappedTAccount + ",,,,RFND-CAWC0000008011-" + currentDateString
        + ",,,,,,,,,,,,,,0,,,0,0,,,,,,,,,,,,,,,,,,,0,0,")
  }

  /**
   * Test getting an APC Attachment flat file line from an outgoing payment.
   */
  function testGetAPCAttachmentLine() {
    var refundInvoiceNumber = "REF06082016-00002"
    var sdfFull = new SimpleDateFormat("MM/dd/yyyy")
    var currentDateString = sdfFull.format(DateUtil.currentDate())
    var sdfNoSeparators = new SimpleDateFormat("MMddyyyy")
    var currentDateStringNoSeparators = sdfNoSeparators.format(DateUtil.currentDate())
    var outgoingPaymentFile = new OutgoingPaymentFile()
    outgoingPaymentFile.addToOutgoingPayments(SampleOutgoingPaymentBean)
    var lines = TDIC_OutgoingPaymentHelper.convertDataToFlatFileLines("OutgoingPaymentAttachmentSpec.xlsx", outgoingPaymentFile)
    assertNotNull(lines)
    assertEquals(lines.Count, 1)
    var apcAttachmentLine = lines.get(0)
    print("APC Attachment Line: " + apcAttachmentLine)
    assertEquals(apcAttachmentLine, "5,999999,REF" + currentDateStringNoSeparators
        + "-00001,,1,CHECK_COMMENT,A,CAWC0000008011__________________________" + currentDateString
        + "______________________________WC_PREMIUM_REFUND_______________________       25.19______________________")
  }

  /*
   * Gets a sample Outgoing Payment in the form of a writable bean.
   */
  @Returns("An OutgoingPaymentWritableBean representing an Outgoing Payment")
  private property get SampleOutgoingPaymentBean() : OutgoingPaymentWritableBean {
    var outgoingPaymentBean = new OutgoingPaymentWritableBean()
    outgoingPaymentBean.PublicID = "bc:9001"
    outgoingPaymentBean.AccountNumber = "4607728576"
    outgoingPaymentBean.Amount = new BigDecimal("25.19")
    outgoingPaymentBean.LOB = LOBCode.TC_WC7WORKERSCOMP.Code
    outgoingPaymentBean.PolicyNumber = "0000008011"
    outgoingPaymentBean.StateCode = Jurisdiction.TC_CA.Code
    outgoingPaymentBean.VendorNumber = "999999"
    outgoingPaymentBean.MappedTAccountCode = TDIC_GLIntegrationHelper.getMappedTAccountName(typekey.Transaction.TC_DISBURSEMENTPAID,
        LedgerSide.TC_CREDIT, false, null, GLOriginalTAccount_TDIC.TC_CASH.Code).replace('-',',')
    return outgoingPaymentBean
  }

}