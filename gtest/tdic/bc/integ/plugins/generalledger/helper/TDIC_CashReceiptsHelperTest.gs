package tdic.bc.integ.plugins.generalledger.helper

uses tdic.bc.integ.plugins.generalledger.dto.CashReceiptsBucketGroup
uses tdic.bc.integ.plugins.generalledger.dto.CashReceiptsFile
uses tdic.bc.integ.plugins.generalledger.dto.CashReceiptsTransactionLineWritableBean
uses tdic.bc.integ.plugins.generalledger.dto.GLRuntimeException
uses tdic.bc.integ.plugins.generalledger.helper.TDIC_CashReceiptsHelper
uses tdic.bc.integ.plugins.generalledger.helper.TDIC_GLIntegrationHelper

uses java.math.BigDecimal
uses java.text.SimpleDateFormat

@gw.testharness.ServerTest

/**
 * US570 - General Ledger Integration
 * 12/19/2014 Alvin Lee
 *
 * GUnit test class for the Cash Receipts File GL Integration.
 */
class TDIC_CashReceiptsHelperTest extends gw.testharness.TestBase {

  /**
   * The Cash Receipts bucket group used across the unit tests.
   */
  private var _bucketGroup : CashReceiptsBucketGroup

  /**
   * Creates the group of cash receipts buckets to be used across the unit tests.
   */
  override function beforeMethod() {
    _bucketGroup = new CashReceiptsBucketGroup()
  }

  /**
   * Test processing an unapplied reversal line.
   */
  function testProcessUnapplied() {
    var eftUnappliedReversedBucket = _bucketGroup.NameBucketMap.get("EFTUnappliedReversed")
    assertNotNull(eftUnappliedReversedBucket)
    assertEquals(eftUnappliedReversedBucket.SequenceNumber, 0)
    assertEquals(eftUnappliedReversedBucket.Description, "WC EFT/ACH (MMDDYY) In-Process")

    TDIC_CashReceiptsHelper.processUnapplied(SampleUnappliedReversalTransactionLine, _bucketGroup)
    print("New total amount: " + eftUnappliedReversedBucket.TotalBucketAmount)
    assertTrue(eftUnappliedReversedBucket.TotalBucketAmount.compareTo(new BigDecimal("100.20")) == 0)
    print("Mapped T-Account code: " + eftUnappliedReversedBucket.MappedTAccountCode)
    assertEquals(eftUnappliedReversedBucket.MappedTAccountCode, "2667-0000")
  }

  /**
   * Negative test using an invalid payment method.
   */
  function testProcessUnappliedNegative() {
    var eftUnappliedReversedBucket = _bucketGroup.NameBucketMap.get("EFTUnappliedReversed")
    assertNotNull(eftUnappliedReversedBucket)

    var transLine = SampleUnappliedReversalTransactionLine
    transLine.PaymentMethod = PaymentMethod.TC_ACCOUNTUNAPPLIED.Code
    var exceptionThrown = false
    try {
      TDIC_CashReceiptsHelper.processUnapplied(transLine, _bucketGroup)
    } catch (glre : GLRuntimeException) {
      print("Caught expected exception: " + glre.LocalizedMessage)
      exceptionThrown = true
    }
    assertTrue(exceptionThrown)
  }


  /**
   * Test encoding lines in the Cash Receipts flat file.
   */
  function testConvertDataToFlatFileLines() {
    var eftUnappliedReversedBucket = _bucketGroup.NameBucketMap.get("EFTUnappliedReversed")
    assertNotNull(eftUnappliedReversedBucket)
    TDIC_CashReceiptsHelper.processUnapplied(SampleUnappliedReversalTransactionLine, _bucketGroup)
    var transLine = SampleUnappliedReversalTransactionLine
    var sdf = new SimpleDateFormat("yyyy-MM-dd")
    var cashReceiptsFile = new CashReceiptsFile()
    eftUnappliedReversedBucket.Date = sdf.parse(transLine.TransactionDate.substring(0, 10))
    eftUnappliedReversedBucket.AccountUnit = TDIC_GLIntegrationHelper.getMappedAccountUnit(CompanyAccountCode_TDIC.TC_BALSHEET, transLine.LOB, transLine.StateCode)
    eftUnappliedReversedBucket.Description = eftUnappliedReversedBucket.Description.replace("(MMDDYY)", eftUnappliedReversedBucket.ShortDate)
    eftUnappliedReversedBucket.MappedTAccountCode = eftUnappliedReversedBucket.MappedTAccountCode.replace('-',',')
    cashReceiptsFile.addToCashReceiptsBuckets(eftUnappliedReversedBucket)
    var lines = TDIC_CashReceiptsHelper.convertDataToFlatFileLines(cashReceiptsFile)
    assertNotNull(lines)
    assertEquals(lines.Count, 1)
    var cashReceiptsLine = lines.get(0)
    print("CSV-formatted Line: " + cashReceiptsLine)
    assertEquals(cashReceiptsLine, "GW-LCB-CAS,0510,NSF,5122014500,0,5,,1,IG,12/20/2014,100.20,0,,0,0,WC EFT/ACH 122014 In-Process,,000.06.40,2667,0000,,0,0,,100.20,0,,,,,,0,,O,,,,,,,,12/20/2014,,")
  }

  /**
   * Gets a sample unapplied reversal line.
   */
  @Returns("A CashReceiptsTransactionLineWritableBean representing an unapplied reversal line")
  private property get SampleUnappliedReversalTransactionLine() : CashReceiptsTransactionLineWritableBean {
    var transLine = new CashReceiptsTransactionLineWritableBean()
    transLine.Amount = new BigDecimal("100.20")
    transLine.Description = "DirectBillMoneyReceivedTxn (Reversed)"
    transLine.GWTAccountName = "Designated Unapplied"
    transLine.InvoiceStatus = InvoiceStatus.TC_BILLED.Code
    transLine.LineItemType = LedgerSide.TC_DEBIT.Code
    transLine.LOB = LOBCode.TC_WC7WORKERSCOMP.Code
    transLine.MappedTAccountName = "2667-0000"
    transLine.PaymentMethod = PaymentMethod.TC_ACH.Code
    transLine.PaymentReversalReason = PaymentReversalReason.TC_R01.Code
    transLine.PolicyPeriodStatus = PolicyPeriodStatus.TC_FUTURE.Code
    transLine.StateCode = Jurisdiction.TC_CA.Code
    transLine.TransactionDate = "2014-12-20 02:02:13.000"
    transLine.TransactionSubtype = typekey.Transaction.TC_DIRECTBILLMONEYRECEIVEDTXN.Code
    return transLine
  }

}