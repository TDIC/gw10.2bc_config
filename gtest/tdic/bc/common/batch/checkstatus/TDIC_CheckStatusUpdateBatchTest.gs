package tdic.bc.common.batch.checkstatus

uses tdic.bc.common.batch.checkstatus.TDIC_CheckStatusUpdateBatch
uses tdic.util.cache.CacheManager

uses java.lang.Exception
uses java.lang.NullPointerException

/**
 * US566
 * 02/05/2015 Kesava Tavva
 *
 * Tests the functionality of TDIC_CheckStatusUpdateBatch.
 */
@gw.testharness.ServerTest
class TDIC_CheckStatusUpdateBatchTest extends gw.testharness.TestBase {
  var batchProcess : TDIC_CheckStatusUpdateBatch
  /**
   * This method is used to initialize instances that are required for each test case execution
   */
  override function beforeMethod() {
    batchProcess = new TDIC_CheckStatusUpdateBatch()
  }

  /**
   * Test batch process with default arguments
   */
  function testDoWork(){
    try{
      batchProcess.doWork()
      assertTrue(Boolean.TRUE)
    }catch(e : Exception){
      fail(e.StackTraceAsString)
    }
  }
  /**
   * Test Check status mappings between Lawson and Billing Center typecodes.
   */
  function testCheckStatusMapping(){
    try{
      var mapper = gw.api.util.TypecodeMapperUtil.getTypecodeMapper()
      assertEquals(typekey.OutgoingPaymentStatus.TC_CLEARED.Code, mapper.getInternalCodeByAlias("OutgoingPaymentStatus","tdic:lawson","R"))
      assertEquals(typekey.OutgoingPaymentStatus.TC_ISSUED.Code, mapper.getInternalCodeByAlias("OutgoingPaymentStatus","tdic:lawson","O"))
      assertEquals(typekey.OutgoingPaymentStatus.TC_PENDINGSTOP.Code, mapper.getInternalCodeByAlias("OutgoingPaymentStatus","tdic:lawson","S"))
      assertEquals(typekey.OutgoingPaymentStatus.TC_PENDINGVOID.Code, mapper.getInternalCodeByAlias("OutgoingPaymentStatus","tdic:lawson","V"))
      assertEquals(typekey.OutgoingPaymentStatus.TC_STALEDATED.Code, mapper.getInternalCodeByAlias("OutgoingPaymentStatus","tdic:lawson","D"))
      assertEquals(typekey.OutgoingPaymentStatus.TC_ESCHEATED.Code, mapper.getInternalCodeByAlias("OutgoingPaymentStatus","tdic:lawson","E"))
      assertEquals(null, mapper.getInternalCodeByAlias("OutgoingPaymentStatus","tdic:lawson","test"))
      assertEquals(null, mapper.getInternalCodeByAlias("OutgoingPaymentStatus","tdic:lawson",""))
      try{
        assertEquals(null, mapper.getInternalCodeByAlias("OutgoingPaymentStatus","tdic:lawson",null))
      }catch(n : NullPointerException){
        assertTrue(Boolean.TRUE)
      }
    }catch(e : Exception){
      fail(e.StackTraceAsString)
    }
  }
  /**
   * Test EmailAddress from CheckInitialConditions
   */
  function testCheckInitialConditions_EmailAddress(){
    try{
      CacheManager.loadCache()
      batchProcess.checkInitialConditions()
    }catch(e : Exception){
      //Fails on h2mem env
      assertTrue(e.Message.contains("Failed to retrieve notification email addresses"))
    }
  }

  /**
   * Test database connection property details from CheckInitialConditions
   */
  function testCheckInitialConditions_DatabaseConnectionProps(){
    try{
      CacheManager.loadCache()
      batchProcess.checkInitialConditions()
    }catch(e : Exception){
      assertTrue(e.Message.contains("Failed to retrieve External App database connection details"))
    }
  }

  /**
   * Test Retrieval functionality of Outgoing payments from BC database
   */
  function testRetrieveOutgoingPayments(){
    try{      
      batchProcess.retrieveOutgoingPayments()
      assertTrue(Boolean.TRUE)
    }catch(e : Exception){
      fail(e.StackTraceAsString)
    }
  }
}