package tdic.bc.common.batch.eftreversal.helper

uses tdic.bc.common.batch.eftreversal.helper.PaymentInstrumentChangeOperation

uses java.math.BigDecimal

@gw.testharness.ServerTest
/**
 * US1126 - CR - EFT/ACH Reversal Automation
 * 03/20/2015 Alvin lee
 *
 * Unit Tests for the Payment Instrument Change Operation class.
 */
class PaymentInstrumentChangeOperationTest extends gw.testharness.TestBase {

  /**
   * True Test for if notification is required for a change code.
   */
  function testNotificationRequiredTrue() {
    print("Testing Notification Required for Change Code '" + NACHAChangeCode_TDIC.TC_C04 + "'...")
    var operation = new PaymentInstrumentChangeOperation(BigDecimal.ZERO, "9999999999", NACHAChangeCode_TDIC.TC_C04, null, null)
    var result = operation.notificationRequired(operation.NOCCode)
    print("Notification Required: " + result)
    assertTrue(result)
  }

  /**
   * False Test for if notification is required for a change code.
   */
  function testNotificationRequiredFalse() {
    print("Testing Notification Required for Change Code '" + NACHAChangeCode_TDIC.TC_C01 + "'...")
    var operation = new PaymentInstrumentChangeOperation(BigDecimal.ZERO, "9999999999", NACHAChangeCode_TDIC.TC_C01, null, null)
    var result = operation.notificationRequired(operation.NOCCode)
    print("Notification Required: " + result)
    assertFalse(result)
  }

}