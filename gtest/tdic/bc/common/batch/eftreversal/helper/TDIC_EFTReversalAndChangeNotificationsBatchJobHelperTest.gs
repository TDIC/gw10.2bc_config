package tdic.bc.common.batch.eftreversal.helper

uses tdic.bc.common.batch.eftreversal.helper.EFTReversalValidationException
uses tdic.bc.common.batch.eftreversal.helper.TDIC_EFTReversalAndChangeNotificationsBatchJobHelper

uses java.math.BigDecimal

@gw.testharness.ServerTest
/**
 * US1126 - CR - EFT/ACH Reversal Automation
 * 03/20/2015 Alvin lee
 *
 * Unit Tests for the Payment Instrument Change Operation class.
 */
class TDIC_EFTReversalAndChangeNotificationsBatchJobHelperTest extends gw.testharness.TestBase {

  /**
   * Test for getting the payment when it is an invalid invoice number.
   */
  function testGetDirectBillMoneyRcvdWithInvoiceNumberInvoiceNotFound() {
    print("Testing Invalid Invoice Number...")
    var exceptionThrown = false
    try {      
      TDIC_EFTReversalAndChangeNotificationsBatchJobHelper.getDirectBillMoneyRcvdWithInvoiceNumber(BigDecimal.ZERO, "xxxxxxxxxx")
    } catch (erve : EFTReversalValidationException) {
      print("Exception caught: " + erve.LocalizedMessage)
      exceptionThrown = true
    }
    print("Exception thrown: " + exceptionThrown)
    assertTrue(exceptionThrown)
  }

}