package tdic.bc.common.batch.banklockbox.helper

uses java.math.BigDecimal
uses gw.api.util.DateUtil
uses tdic.bc.common.batch.banklockbox.LockboxValidationException
uses tdic.bc.common.batch.banklockbox.helper.TDIC_BankLockboxHelper

@gw.testharness.ServerTest

/**
 * US567 - Bank/Lockbox Integration
 * 01/09/2015 Alvin Lee
 *
 * GUnit test class for the Bank/Lockbox Integration.
 */
class TDIC_BankLockboxHelperTest extends gw.testharness.TestBase {

  public static final var SUPER_USER : String = "iu"

  /**
   * Test getting the batch deposit date from a batch header line from the Bank/Lockbox flat file.
   */
  function testGetBatchDepositDateFromHeader() {
    print("Testing Batch Header Line...")
    var line = "5001000074247116060114898010600111000025                                        "
    var batchDepositDate = TDIC_BankLockboxHelper.getBatchDepositDateFromHeader(line)
    print("  Batch Deposit Date: " + batchDepositDate)
    assertNotNull(batchDepositDate)
    assertEquals(batchDepositDate.MonthOfYear, 6)
    assertEquals(batchDepositDate.DayOfMonth, 1)
    assertEquals(batchDepositDate.YearOfDate, 2016)
  }

  /**
   * Test getting a TransactionLine object from a transaction line from the Bank/Lockbox flat file.
   */
  function testGetTransactionLine() {
    print("Testing Transaction Line...")
    var line = "60010010000001000121141819000180843500005246                                    "
    var transLine = TDIC_BankLockboxHelper.getTransactionLine(line)
    assertNotNull(transLine)
    print("  Batch Number: " + transLine.BatchNumber)
    print("  Transaction Sequence Number: " + transLine.TransactionSequenceNumber)
    print("  Check Amount: " + transLine.CheckAmount)
    print("  Check Number: " + transLine.CheckNumber)
    assertEquals(transLine.BatchNumber, "001")
    assertEquals(transLine.TransactionSequenceNumber, "001")
    assertEquals(transLine.CheckNumber, "00005246")
    assertTrue(transLine.CheckAmount.compareTo(new BigDecimal("10")) == 0)
  }

  /**
   * Test getting a InvoiceLine object from a line line from the Bank/Lockbox flat file.
   */
  function testGetInvoiceLine() {
    print("Testing Invoice Line...")
    var line = "4001001601911111111110110000000880000100000035100                               "
    var invLine = TDIC_BankLockboxHelper.getInvoiceLine(line)
    assertNotNull(invLine)
    print("  Policy Number: " + invLine.PolicyNumber)
    print("  Term Number: " + invLine.TermNumber)
    print("  Invoice Number: " + invLine.InvoiceNumber)
    assertEquals(invLine.PolicyNumber, "1111111111")
    assertEquals(invLine.TermNumber, 1)
    assertEquals(invLine.InvoiceNumber, "1000000088")
  }

  /**
   * Test getting batch totals a batch trailer line from the Bank/Lockbox flat file.
   */
  function testGetBatchTotals() {
    print("Testing Batch Trailer Line...")
    var line = "700100007424711606010040000016000                                               "
    var batchTotals = TDIC_BankLockboxHelper.getBatchTotals(line)
    assertNotNull(batchTotals)
    print("  Batch Number: " + batchTotals.BatchNumber)
    print("  Batch Total Number of Transactions: " + batchTotals.TotalNumberTransactions)
    print("  Batch Total Amount: " + batchTotals.TotalAmount)
    assertEquals(batchTotals.BatchNumber, "001")
    assertEquals(batchTotals.TotalNumberTransactions, 4)
    assertTrue(batchTotals.TotalAmount.compareTo(new BigDecimal("160")) == 0)
  }

  /**
   * Negative test getting batch totals a batch trailer line from the Bank/Lockbox flat file.
   */
  function testGetBatchTotalsNegative() {
    print("Testing Batch Trailer Line with an invalid total amount...")
    var line = "7001000074247116060100400aaa16000                                               "
    var exceptionThrown = false
    try {
      TDIC_BankLockboxHelper.getBatchTotals(line)
    } catch (lve : LockboxValidationException) {
      print("Caught expected exception: " + lve.LocalizedMessage)
      exceptionThrown = true
    }
    assertTrue(exceptionThrown)
  }

  /**
   * Test getting file totals a lockbox trailer line from the Bank/Lockbox flat file.
   */
  function testGetFileTotals() {
    print("Testing Lockbox Trailer Line...")
    var line = "8004000074247116060100040000016000                                              "
    var fileTotals = TDIC_BankLockboxHelper.getFileTotals(line)
    assertNotNull(fileTotals)
    print("  File Total Number of Transactions: " + fileTotals.TotalNumberTransactions)
    print("  File Total Amount: " + fileTotals.TotalAmount)
    assertEquals(fileTotals.TotalNumberTransactions, 4)
    assertTrue(fileTotals.TotalAmount.compareTo(new BigDecimal("160")) == 0)
  }

  /**
   * Test processing a suspense payment using lines from the Bank/Lockbox flat file.
   */
  function testProcessPaymentSuspense() {
    print("Testing Processing Payment - Suspense Payment...")
    var line = "60010010000001000121141819000180843500005246                                    "
    var transLine = TDIC_BankLockboxHelper.getTransactionLine(line)
    line = "40010016019FAKENUMBER01TSTINVOICE0000100000035100                               "
    var invLine = TDIC_BankLockboxHelper.getInvoiceLine(line)
    var currentDate = DateUtil.currentDate()
    print("Creating suspense payment for policy number '" + invLine.PolicyNumber + "' and Invoice Number '"
        + invLine.InvoiceNumber + "'...")
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
    TDIC_BankLockboxHelper.processPayment(currentDate, invLine, transLine, bundle)
    }, SUPER_USER)
    var query = gw.api.database.Query.make(SuspensePayment)
    query.compare(SuspensePayment#InvoiceNumber, Equals, "TSTINVOICE")
    var result = query.select().last()
    assertNotNull(result)
    print("Suspense Payment Found")
    print("  Policy Number: " + result.PolicyNumber)
    print("  Invoice Number: " + result.InvoiceNumber)
    print("  Amount: " + result.Amount)
    print("  Date: " + result.PaymentDate)
    assertEquals(result.PolicyNumber, "FAKENUMBER-1")
    assertEquals(result.InvoiceNumber, "TSTINVOICE")
    assertTrue(result.Amount.Amount.compareTo(new BigDecimal("10")) == 0)
    assertTrue(result.PaymentDate.compareIgnoreTime(currentDate) == 0)
  }
}