package cmd.admindata

uses org.w3c.dom.Element
uses java.util.List

class CleansePCAdminData extends CleanseAdminData {

  /**
   * Get the list of admin data files.
   */
  override protected property get Filenames() : List<String> {
    var retval = super.Filenames;
    retval.add ("policy_forms.xml");
    retval.add ("policy_holds.xml");
    return retval;
  }

  /**
   * Cleanse the admin data file.
   */
  override protected function cleanseAdminDataFile (filename : String, import : Element) : void {
    super.cleanseAdminDataFile (filename, import);

    if (filename == "admin.xml") {
      removeElements (import, {"UWIssueType"});
    }
  }
}