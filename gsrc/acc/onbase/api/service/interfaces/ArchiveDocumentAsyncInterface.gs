package acc.onbase.api.service.interfaces

uses java.io.File

/**
 * Hyland Build Version: 4.2.0-10-ga6a8ce6

 * <p>
 * Last Changes:
 * 01/13/2015 - Daniel Q. Yu
 * * Initial implementation.
 */

/**
 * Interface to call OnBase Asynchronized Archive Document service.
 */
interface ArchiveDocumentAsyncInterface {
  /**
   * Archive document asynchronously.
   *
   * @param documentFile The document local file.
   * @param document The entity.document object.
   */
  public function archiveDocument(documentFile: File, document: Document)
}
