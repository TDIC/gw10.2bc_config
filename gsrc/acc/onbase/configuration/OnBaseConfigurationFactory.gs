package acc.onbase.configuration

/**
 * Hyland Build Version: 4.2.0-10-ga6a8ce6

 * <p>
 * Provider for OnBase configuration settings.
 * A custom implementation of IOnBaseConfiguration can be provided
 * at run-time through the Instance set method to change where and how properties are stored.
 */
class OnBaseConfigurationFactory {
  private static var _instance: IOnBaseConfiguration as Instance = new OnBaseConfiguration()
}
