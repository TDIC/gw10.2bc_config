package acc.onbase.webservice.util

uses acc.onbase.util.LoggerFactory
uses gw.api.database.Query
uses gw.api.locale.DisplayKey
uses gw.transaction.Transaction

/**
 * Hyland Build Version: 4.2.0-10-ga6a8ce6

 * <p>
 * Utility class to assist in deserializing document models received via web service calls.
 */
class DocumentModelParser {

  private static var _logger = LoggerFactory.getLogger(LoggerFactory.ServicesLoggerCategory)

  private final var _model: acc.onbase.api.si.model.documentmodel.Document


  /**
   * @param model document model
   */
  construct(model: acc.onbase.api.si.model.documentmodel.Document) {
    _model = model
  }

  /**
   * Retrieve DocumentType specified by OnBaseName
   * <p>
   * DocumentType is a required value.
   *
   * @return valid document type
   */
  public property get Type(): DocumentType {
    if (!_model.Type.OnBaseName.HasContent) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingDocumentType"))
    }

    return DocumentType.findByOnBaseName(_model.Type.OnBaseName)
  }

  /**
   * Retrieve Subtype specified by OnBaseName
   * <p>
   * Subtype is optional, but must be a valid subtype for the provided document type if present.
   *
   * @return null or valid subtype
   */
  public property get Subtype(): OnBaseDocumentSubtype_Ext {
    if (!_model.Subtype.OnBaseName.HasContent) {
      // Subtype is optional
      return null
    }

    var subtype = OnBaseDocumentSubtype_Ext.findByOnBaseName(_model.Subtype.OnBaseName)
    var doctype = Type;
    if (!subtype.hasCategory(doctype)) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_SubTypeNotRelatedToDocumentType", subtype, doctype))
    }

    return subtype
  }

  /**
   * Retrieve Account by Account Number
   * <p>
   * Account Number is optional and must provide a valid account number if present
   *
   * @return null or valid account
   */
  public property get Account(): Account {
    if (_model.Account == null || !_model.Account.AccountNumber.HasContent) {
      return null
    }

    var account: Account = null
    account = Query.make(entity.Account).compare(entity.Account#AccountNumber, Equals, _model.Account.AccountNumber).select().AtMostOneRow
    if (account == null) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_InvalidAccountNumber", _model.Account.AccountNumber))
    }

    return account
  }

  /**
   * Retrieve Producer by ProducerID
   * <p>
   * Producer ID is optional and must provide a valid producer id if present
   *
   * @return null or valid producer
   */
  public property get Producer(): Producer {
    if (_model.Producer == null || !_model.Producer.PublicID.HasContent) {
      return null
    }

    var producer: Producer = null
    producer = Query.make(entity.Producer).compare(entity.Producer#PublicID, Equals, _model.Producer.PublicID).select().getAtMostOneRow()
    if (producer == null) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_ProducerNotFound", _model.Producer.PublicID))
    }

    return producer
  }


  /**
   * Retrieve Policy by PolicyNumber
   * <p>
   * PolicyNumber is optional and must provide a valid policy number
   *
   * @return null or valid policy
   */
  public property get Policy(): Policy {
    if (_model.Policy == null || !_model.Policy.InForcePolicyNumber.HasContent) {
      return null
    }

    var policy: Policy = null
    var policyNumber = _model.Policy.InForcePolicyNumber
    if (policyNumber != null) {
      var policyPeriod = PolicyPeriod.finder.getCurrentPolicyPeriodByNumber(policyNumber)
      if (policyPeriod != null) {
        policy = policyPeriod.Policy
      }
    }
    if (policy == null) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_PolicyNotFound", policyNumber))
    }
    var account = Account
    if (account != null && policy.Account != account) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_AccountAndPolicyNotLinked", policyNumber, account.AccountNumber))
    }

    return policy
  }

  /**
   * Validate Status
   *
   * @return valid Status
   */
  public property get Status(): DocumentStatusType {
    if (_model.Status == null) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingStatus"))
    } else if (_model.Status.Retired) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_RetiredStatus", _model.Status))
    }

    return _model.Status
  }

  /**
   * Validate Mimetype
   *
   * @return valid mimetype
   */
  public property get MimeType(): String {
    if (!_model.MimeType.HasContent) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingFileType"))
    }

    return _model.MimeType
  }

  /**
   * Validate SecurityType
   *
   * @return valid security type
   */
  public property get SecurityType(): DocumentSecurityType {
    if (_model.SecurityType == null) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingSecurityType"))
    }
    return _model.SecurityType
  }

  /**
   * Verify if information regarding the entity the document is linked to, is available
   */
  public function checkIfDocumentLinked() {

    //document must be linked to at least one entity
    if (Account == null && Producer == null && Policy == null) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_AccountOrProducerRequired"))
    }
  }

  public function getPendingDocument(): Document {
    validateDocUID()
    var document = Document.findDocumentByOnBaseID(_model.DocUID)
    checkifDocumentExists(document)
    if (!_model.PendingDocUID.HasContent) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingPendingID"))
    } else {
      document = Document.findDocumentByPendingDocUID(_model.PendingDocUID)
      if (document == null) {
        logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnknownPendingID", _model.PendingDocUID))
      }
    }
    return document
  }

  public function getNewDocument(): Document {
    validateDocUID()
    checkifDocumentExists(Document.findDocumentByOnBaseID(_model.DocUID))
    return new Document()
  }

  public function getExistingDocument(): Document {
    validateDocUID()
    var document = Document.findDocumentByOnBaseID(_model.DocUID)
    if (document == null) {
      //throw error if docuid doesn't exist for reindex
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_UnknownDocUID", _model.DocUID))
    }
    return document
  }

  private function checkifDocumentExists(document: Document) {
    if (document != null) {
      //throw error if docuid already exists for async/ new document
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_DuplicateDocUID", _model.DocUID))
    }
  }

  private function validateDocUID() {
    if (!_model.DocUID.HasContent) {
      logAndThrow(DisplayKey.get("Accelerator.OnBase.WS.Error.STR_GW_MissingDocUID"))
    }
  }

  private static function logAndThrow(message: String) {
    _logger.error(message)
    throw new IllegalArgumentException(message)
  }

}