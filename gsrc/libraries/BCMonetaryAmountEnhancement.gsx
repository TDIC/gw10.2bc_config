package libraries

uses gw.api.util.Percentage
uses gw.api.financials.MonetaryAmounts
uses gw.api.util.Ratio
uses gw.pl.currency.MonetaryAmount

uses java.math.BigDecimal

@Export
enhancement BCMonetaryAmountEnhancement : MonetaryAmount {
  
  function percentageOf(base : MonetaryAmount) : Percentage {
    if (base.signum() == 0) {
      return Percentage.ZERO_PERCENT
    }
    return Percentage.fromRatioBetween( this, base )
  }
  
  function percentageOfAsBigDecimal(base : MonetaryAmount ) : BigDecimal {
    if (base.signum() == 0) {
      return BigDecimal.ZERO
    }
    return Ratio.valueOf(this, base).resolve(5).movePointRight(2)
  }
  
  function render() : String {
    return MonetaryAmounts.render(this);
  }

  function renderWithZeroDash() : String {
    if (this.IsZero) {
      return "-"
    } else {
      return render()
    }
  }

}
