package libraries

uses gw.api.database.DBFunction
uses gw.api.database.Query
uses gw.api.database.QuerySelectColumns
uses gw.api.domain.accounting.ChargePatternKey
uses gw.api.locale.DisplayKey
uses gw.api.path.Paths
uses gw.api.upgrade.Coercions
uses gw.api.util.DateUtil
uses gw.api.web.payment.DirectBillPaymentFactory
uses gw.bc.archive.ImpactedByArchiving
uses gw.pl.currency.MonetaryAmount
uses gw.plugin.pas.PASMessageTransport
uses gw.transaction.TransactionWrapper
uses org.slf4j.LoggerFactory
uses tdic.bc.config.invoice.InvoiceUtil
uses tdic.bc.config.invoice.TDIC_InvoicePURHelper
uses tdic.bc.integ.plugins.policy.TDIC_PCMessageTransport

uses java.lang.*
uses java.math.BigDecimal
uses java.math.RoundingMode
uses java.util.ArrayList
uses java.util.Date

/**
 * Defines enhancements for a {@link PolicyPeriod}.
 */
@Export
enhancement PolicyPeriodExt : entity.PolicyPeriod {
  /**
   * A reversible {@link ChargePatternKey#POLICYLATEFEE Policy Late Fee}
   * {@link //entity.ReversibleChargeSearchCriteria SearchCriteria} for
   * this {@link PolicyPeriod}.
   */
  property get ReversibleLateFeeSearchCriteria() : gw.search.ReversibleChargeSearchCriteria {
    final var criteria = new gw.search.ReversibleChargeSearchCriteria()
    criteria.PolicyPeriod = this
    criteria.ChargePattern = ChargePatternKey.POLICYLATEFEE.get()
    return criteria
  }

  /**
   * The {@link InvoiceDeliveryMethod} for this {@link PolicyPeriod}.
   */
  property get InvoiceDeliveryType() : InvoiceDeliveryMethod {
    return this.OverridingPayerAccount != null
        ? this.OverridingPayerAccount.InvoiceDeliveryType
        : (this.DefaultPayer typeis Account
        ? this.DefaultPayer.InvoiceDeliveryType : null)
  }

  /**
   * The {@link PaymentInstrument} for this {@link PolicyPeriod}.
   */
  property get PaymentInstrument() : PaymentInstrument {
    // For policy level billing, check the overriding invoice stream
    if (this.BillingMethod != TC_AGENCYBILL
        && this.Account.BillingLevel == BillingLevel.TC_POLICYDEFAULTUNAPPLIED) {
      if (this.OverridingInvoiceStream != null) return this.OverridingInvoiceStream.PaymentInstrument
    }

    return this.OverridingPayerAccount != null
        ? this.OverridingPayerAccount.DefaultPaymentInstrument
        : (this.DefaultPayer typeis Account
        ? this.DefaultPayer.DefaultPaymentInstrument : null)
  }

  /**
   * The next {@link Invoice}(s) where the {@link //Invoice#PaymentDueDate
   * PaymentDueDate} is the nearest in the future (i.e., after today). There
   * might be more than one on that date, but not any for another date.
   */
  @ImpactedByArchiving
  property get NextDueInvoices() : Invoice[] {
    final var itemQuery = Query.make(InvoiceItem)
    itemQuery.compare(InvoiceItem#PolicyPeriod, Equals, this)
    final var invoiceQuery = Query.make<Invoice>(Invoice)
    invoiceQuery.subselect(Invoice#ID, CompareIn, itemQuery, InvoiceItem#Invoice)
    invoiceQuery.compare(Invoice#PaymentDueDate, GreaterThan, Date.Today)
    var min = invoiceQuery.select({QuerySelectColumns.dbFunction(
        DBFunction.Min(invoiceQuery.getColumnRef(Invoice#EventDate.PropertyInfo.Name)))})
    invoiceQuery
        .compare(Invoice#EventDate, Equals, Coercions.makeDateFrom(min.AtMostOneRow.getColumn(0)))
    return invoiceQuery.select().toTypedArray()
  }

  /**
   * The primary default {@link ProducerCode} for this {@link PolicyPeriod}.
   * This is independent of whether the {@code PolicyPeriod} is archived.
   */
  property get PrimaryProducerCode() : ProducerCode {
    return this.Archived
        ? this.PrimaryCommissionArchiveSummary.ProducerCode
        : this.PrimaryPolicyCommission.ProducerCode
  }

  @ImpactedByArchiving
  function getPaymentDueDateToAvoidDelinquencyOn(date : Date) : Date {
    var dueInvoice = getMostRecentPastDueInvoiceAsOf(date)
    if (dueInvoice != null) {
      return dueInvoice.PaymentDueDate
    } else {
      return null
    }
  }

  /**
   * The most recent {@link Invoice} where the {@link //Invoice#PaymentDueDate
   * PaymentDueDate} is "date" or before.
   */
  @ImpactedByArchiving
  function getMostRecentPastDueInvoiceAsOf(date : Date) : Invoice {
    final var itemQuery = Query.make(InvoiceItem)
    itemQuery.compare(InvoiceItem#PolicyPeriod, Equals, this)
    final var invoiceQuery = Query.make<Invoice>(Invoice)
    invoiceQuery.subselect(Invoice#ID, CompareIn, itemQuery, InvoiceItem#Invoice)
    invoiceQuery.compare(Invoice#PaymentDueDate, LessThanOrEquals, date)
    var min = invoiceQuery.select({QuerySelectColumns.dbFunction(
        DBFunction.Max(invoiceQuery.getColumnRef(Invoice#EventDate.PropertyInfo.Name)))})
    invoiceQuery
        .compare(Invoice#EventDate, Equals, Coercions.makeDateFrom(min.AtMostOneRow.getColumn(0)))
    return invoiceQuery.select().FirstResult
  }

  @ImpactedByArchiving
  function getSafeEarnedPremium(): MonetaryAmount {
    return (this.Closed ? this.getTotalValue(TC_PREMIUM): this.getEarnedValue(TC_PREMIUM))
  }

  @ImpactedByArchiving
  function getPolicyEquityPercentage(): Double {
    return getPolicyEquityPercentage(this.TotalEquityChargeValue, this.PolicyEquity)
  }

  /**
   * Performant version of getPolicyEquityPercentage() which uses precomputed
   * variables for its calculation rather than recomputing them on the PolicyPeriod
   *
   * @param totalEquityChargeValue Typically from PolicyPeriod.TotalEquityChargeValue
   * @param policyEquity typically from PolicyPeriod.PolicyEquity
   * @return policyEquity as a percentage of the totalEquityChargeValue
   */
  function getPolicyEquityPercentage(totalEquityChargeValue: MonetaryAmount, policyEquity: MonetaryAmount): Double {
    return totalEquityChargeValue.IsZero ? null : (100 * policyEquity / totalEquityChargeValue) as Double
  }

  @ImpactedByArchiving
  function getPercentEarned(): Double {
    return getPercentEarned(this.getTotalValue(null), this.getEarnedValue(null))
  }

  /**
   * Performant version of getPercentEarned() which uses precomputed
   * variables for its calculation rather than recomputing them on the PolicyPeriod
   *
   * @param totalValue Typically from PolicyPeriod.getTotalValue(null)
   * @param earned Typically from PolicyPeriod.getEarnedValue(null)
   * @return The PolicyPeriod's earned value as a numeric percentage of total value
   */
  function getPercentEarned(totalValue: MonetaryAmount, earned: MonetaryAmount): Double {
    return totalValue.IsZero ? null : (100 * earned / totalValue) as Double
  }

  @ImpactedByArchiving
  function getDaysTillPaidThruDate(): Double {
    return getDaysTillPaidThruDate(this.PaidThroughDate)
  }

  /**
   * Performant version of getDaysTillPaidThruDate() which uses precomputed
   * variables for its calculation rather than recomputing them on the PolicyPeriod
   *
   * @param paidThroughDate Typically from PolicyPeriod.PaidThroughDate
   * @return Days until the PolicyPeriod is paid through
   */
  function getDaysTillPaidThruDate(paidThroughDate: Date): Double {
    if (paidThroughDate == null) {
      return null
    }
    var todayOrPolicyPerExpirDate = DateUtil.currentDate()
    if (this.PolicyPerExpirDate.before(todayOrPolicyPerExpirDate)) {
      todayOrPolicyPerExpirDate = this.PolicyPerExpirDate
    }
    return todayOrPolicyPerExpirDate.differenceInDays(paidThroughDate)
  }

  @ImpactedByArchiving
  function getPaidToBilledRatio(): Double {
    var billedToday = this.getTotalValue(null) - this.UnbilledAmount
    return getPaidToBilledRatio(billedToday, this.PaidAmount)
  }

  /**
   * Performant version of getPaidToBilledRatio() which uses precomputed
   * variables for its calculation rather than recomputing them on the PolicyPeriod
   *
   * @param totalBilled Typically from PolicyPeriod.getTotalValue(null) - PolicyPeriod.UnbilledAmount
   * @param paidAmount Typically from PolicyPeriod.PaidAmount
   * @return The PolicyPeriod's ratio of Paid to Billed as a numeric percentage
   */
  function getPaidToBilledRatio(totalBilled: BigDecimal, paidAmount: BigDecimal): Double {
    return totalBilled == 0 ? null : (100 * paidAmount / totalBilled) as Double
  }

  function getPaidToValueRatio(): Double {
    return getPaidToValueRatio(this.getTotalValue(null), this.PaidAmount)
  }

  /**
   * Performant version of getPaidToValueRatio() which uses precomputed
   * variables for its calculation rather than recomputing them on the PolicyPeriod
   *
   * @param totalValue Typically from PolicyPeriod.getTotalValue(null)
   * @param paidAmount Typically from PolicyPeriod.PaidAmount
   * @return The PolicyPeriod's ratio of Paid to Value as a numeric percentage
   */
  function getPaidToValueRatio(totalValue: BigDecimal, paidAmount: BigDecimal): Double {
    return totalValue == 0 ? null : (100 * paidAmount / totalValue) as Double
  }

  /**
   * The {@link Cancellation} billing instruction for this {@link PolicyPeriod},
   * if any. (The {@code Cancellation} is archived along with the
   * {@code PolicyPeriod} and is not available when that is true; the
   * {@link #getCancellationDate CancellationDate} will always be not
   * {@code null}, if the {@code PolicyPeriod} was cancelled independent of
   * whether it is archived).
   */
  @ImpactedByArchiving
  property get Cancellation() : Cancellation {
    if (this.Canceled) {
      var query = Query.make(entity.Cancellation)
          .compare("AssociatedPolicyPeriod", Equals, this)
          .select().orderByDescending(QuerySelectColumns.path(Paths.make(entity.Cancellation#ModificationDate)))
      return query.FirstResult
    }
    return null
  }

  /**
   * The {@link Cancellation} date of this {@link PolicyPeriod}, if any. This is
   * independent of whether the {@code PolicyPeriod} is archived so a {@code
   * null} value always indicates that {@code PolicyPeriod} was never cancelled.
   */
  property get CancellationDate() : Date {
    return this.Archived
        ? this.PolicyPeriodArchiveSummary.CancellationDate
        : this.Cancellation.ModificationDate
  }

  // Fix for the Reinstatement Date of the Account Summary Screen, Policy Table
  property get Reinstatement() : Reinstatement {
      var query = Query.make(entity.Reinstatement)
          .compare("AssociatedPolicyPeriod", Equals, this)
          .select().orderByDescending(QuerySelectColumns.path(Paths.make(entity.Reinstatement#ModificationDate)))
      return query.FirstResult
  }

  property get ReinstatementDate() : Date {
    return this.Reinstatement.ModificationDate
  }

  /**
   * Determine is the policy has a pending past due cancellation.
   */
  protected function IsPendingPastDueCancellation(currentDelinquency : PolicyDlnqProcess) : boolean {
    var retval = false;

    for (delinquencyProcess in this.Policy.ActiveDelinquencyProcesses_TDIC) {
      if (delinquencyProcess != currentDelinquency) {
        if (delinquencyProcess.Reason == DelinquencyReason.TC_PASTDUE) {
          if (delinquencyProcess.CancelInfo_TDIC != null) {
            retval = true;
            break;
          }
        }
      }
    }

    return retval;
  }

  /**
   * The end date of this {@link PolicyPeriod}, which is either its
   * {@link #getCancellationDate cancellation date} or its
   * {@link PolicyPeriod#getPolicyPerExpirDate effective expiration date}.
   */
  property get EndDate() : Date {
    return this.Canceled ? this.CancellationDate : this.PolicyPerExpirDate
  }

  function cancelByDelinquencyProcess(process: DelinquencyProcess) {
    if (canBeCancelled()) {
      var delinquencyCancel = new DelProcessCancel_TDIC(process.Bundle);
      delinquencyCancel.DelinquencyProcess = process as PolicyDlnqProcess;
      delinquencyCancel.PolicyPeriod = this;

      // There can be two cancellations per term, one for NOT TAKEN and one for PAST DUE.
      if (process.Reason == DelinquencyReason.TC_NOTTAKEN) {
        // BrianS - Set the effective date of the cancel, so that it can be exposed to HP Exstream.
        delinquencyCancel.EffectiveDate = (process as PolicyDlnqProcess).calcCancelEffectiveDate_TDIC();
        this.CancelStatus = typekey.PolicyCancelStatus.TC_PENDINGCANCELLATION
        this.addHistoryFromGosu(gw.api.util.DateUtil.currentDate(),
            typekey.HistoryEventType.TC_POLICYCANCELED,
            DisplayKey.get("Java.PolicyHistory.PolicyCanceled"), null, null, false)
        //addCancellationActivity(process)
        delinquencyCancel.addEvent(PASMessageTransport.EVENT_CANCEL_NOW)
      } else if (process.Reason == DelinquencyReason.TC_PASTDUE) {
        if (this.IsPendingPastDueCancellation(process as PolicyDlnqProcess) == false) {
          // BrianS - Set the effective date of the cancel, so that it can be exposed to HP Exstream.
          delinquencyCancel.EffectiveDate = (process as PolicyDlnqProcess).calcCancelEffectiveDate_TDIC();
          this.CancelStatus = PolicyCancelStatus.TC_PENDINGCANCELLATION;
          this.addHistoryFromGosu(gw.api.util.DateUtil.currentDate(), typekey.HistoryEventType.TC_POLICYCANCELED,
              DisplayKey.get("Java.PolicyHistory.PolicyCanceled"), null, null, false);
          //addCancellationActivity(process)
          delinquencyCancel.addEvent(PASMessageTransport.EVENT_CANCEL_NOW)
        }else {
          LoggerFactory.getLogger("Application.DelinquencyProcess").info ("PolicyPeriod " + this + " is already pending past due cancellation.");
        }
      } else {
        throw new IllegalStateException ("Unexpected delinquency reeason: " + process.Reason);
      }
    } else {
      LoggerFactory.getLogger("Application.DelinquencyProcess").info ("PolicyPeriod " + this + " has a closure status of "
          + this.ClosureStatus + " and a cancel status of "
          + this.CancelStatus + ".  PolicyPeriod CANNOT be cancelled.");
    }
  }

  private function canBeCancelled(): boolean {
    return this.ClosureStatus != typekey.PolicyClosureStatus.TC_CLOSED and
        this.CancelStatus != typekey.PolicyCancelStatus.TC_CANCELED and
        this.CancelStatus != typekey.PolicyCancelStatus.TC_PENDINGCANCELLATION
  }

  private function addCancellationActivity(process: DelinquencyProcess) {
    // create an activity for Policy Delinquency
    var actvty = new Activity(process.Bundle)
    actvty.ActivityPattern = gw.api.web.admin.ActivityPatternsUtil.getActivityPattern("general")
    actvty.Subject = DisplayKey.get("Java.PolicyActivity.Cancellation.Subject", this.DisplayName)
    actvty.Description = DisplayKey.get("Java.PolicyActivity.Cancellation.Description")
    actvty.Priority = typekey.Priority.TC_NORMAL
    actvty.PolicyPeriod = this // 20150324 TJ Talluto adding FK to PolicyPeriod
    actvty.Account = this.Account // 20150330 Alvin Lee Adding FK to Account
    //gw.api.assignment.AutoAssignAssignee.INSTANCE.assignToThis(actvty) // TBD explicit Round Robin in Service Grp?
    actvty.autoAssign()
    // Assign by Assignment rules
    var _logger = LoggerFactory.getLogger("Rules")
    if (actvty.autoAssign()) {
      _logger.debug("PolicyPeriodExt - addCancellationActivity: auto assigned successful. Assigned Group : " + actvty.AssignedGroup.DisplayName + " | Assigned User: " + actvty.AssignedUser.DisplayName)
    }
    else {
      _logger.warn("PolicyPeriodExt - addCancellationActivity: " + actvty.ActivityPattern.DisplayName)
    }
    process.addToActivities(actvty)
  }

  function rescindOrReinstateInternal(delinquencyProcess : PolicyDlnqProcess) {
    if (delinquencyProcess.CancelInfo_TDIC != null and delinquencyProcess.CancelInfo_TDIC.JobNumber == null) {
      LoggerFactory.getLogger("Application.DelinquencyProcess").info ("Cancellation was started by another deliquency process.  "
          + "Suppressing rescind from delinquency process "
          + delinquencyProcess);
    } else if (this.CancelStatus == typekey.PolicyCancelStatus.TC_PENDINGCANCELLATION) {
        //this.CancelStatus = typekey.PolicyCancelStatus.TC_PENDINGRESCINDCANCEL
      if (delinquencyProcess.CancelInfo_TDIC != null) {
        delinquencyProcess.CancelInfo_TDIC.addEvent(PASMessageTransport.EVENT_RESCIND_CANCELLATION)
      } else {
        this.addEvent(PASMessageTransport.EVENT_RESCIND_CANCELLATION);
      }
      // BrianS - Set status to Open, based upon the assumption that cancellation will be rescinded.
      this.CancelStatus = PolicyCancelStatus.TC_OPEN
    } else if (this.CancelStatus == PolicyCancelStatus.TC_CANCELED) {
      // TODO: Minh VU: it may be more appropriate to create an activity for UW
      // rather than to go and reinstate the policy period

      //GWPS-136 - Reinstatement request activity created without payment
      // Added the conditon to check if there should be no due under the policy
      // and if the Cancellation date crossed more then 10 days of the payment date these cases we are skipping the activity .

      //taking charge date for Cancellation BI as the Cancellation date is returning effective date if it is Flat Cancel then
      // Date would be PolicyEffective Date and to reach Cancel BI step it takes 22 days.So the value will never be less than 10 days
      //And removing !this.DueAmount.IsPositive condition as this will not satisfy when user have due amount which falls under ExitThreshold amount
      //and the exit delinquency will be called only if user pays the required amount
      //GWPS-770 and GWPS-2040
      var latestCancelChargeDate = this.Charges.where(\bi -> bi.BillingInstruction typeis Cancellation)?.orderByDescending(\charge -> charge.ChargeDate)?.first()?.ChargeDate
      if(gw.api.util.DateUtil.currentDate() <= DateUtil.addDays(latestCancelChargeDate, 10)){
        this.addEvent(TDIC_PCMessageTransport.EVENT_PCREINSTATE_REQUEUST)
      }

      // BrianS - Leave status as cancelled, since policy may or may not be reinstated.
      //this.CancelStatus = typekey.PolicyCancelStatus.TC_PENDINGREINSTATEMENT
    } else {
      LoggerFactory.getLogger("Application.DelinquencyProcess").error ("Unexpected cancel status " + this.CancelStatus
          + " occurred while processing rescindOrReinstate.");
      // NOT CANCELED and NOT PENDING CANCELLATION (this is NOT TAKEN inside the pending cancellation window)
      //this.addEvent(TDIC_PCMessageTransport.EVENT_BC_CANCEL_PROCESS_DATE_PUSH)
      //this.CancelStatus = typekey.PolicyCancelStatus.TC_OPEN   //FIXME TJT
    }
  }

  function sendDunningLetterInternal(delinquencyProcess : DelinquencyProcess) {

    // TO DO - In general, it is not a good idea to execute a bundle commit or run transactions
    //         in a new bundle inside an existing transaction.  Both occur in the code below.
    //         It could give unexpected results when an error occurs in the main process, since
    //         only some of the transaction will be rolled back.

    /*
    US669
    05/11/2015 Shane Murphy
    Creating documents in this bundle causes an exception to be thrown as the account has already been edited.
    Explicitly committing the bundle is a temporary measure which occurred after we removed an async workflow
    which performed the same task.
    */

    // TODO: Revisit this code. Invoice barcode generation.
   // this.Bundle?.commit()

    if (this.PaymentPlan.Name != "TDIC Monthly APW") {
      var delinquencyProess = delinquencyProcess as PolicyDlnqProcess
      var invoice = delinquencyProess.Invoice_TDIC;
      var docCreationEvent = TDIC_DocCreationEventType.TC_REMINDERNOTICE

      gw.transaction.Transaction.runWithNewBundle (\newBundle -> {
          invoice = newBundle.add(invoice)
          LoggerFactory.getLogger("Application.DelinquencyProcess").trace("Existing OCRBarCode for invoice: " + invoice
              + " is " + invoice.BarCode_TDIC)
          invoice.BarCode_TDIC = TDIC_InvoicePURHelper.generateOCRBarCode(invoice);
          LoggerFactory.getLogger("Application.DelinquencyProcess").trace ("Reminder Notice OCRBarCode for invoice: " + invoice
              + " is " + invoice.BarCode_TDIC)
        })

      if(delinquencyProess.AmountDue_TDIC  > InvoiceUtil.InvoiceThreshold){
        delinquencyProess = gw.transaction.Transaction.getCurrent().add(delinquencyProess)
        LoggerFactory.getLogger("Application.DelinquencyProcess").info(docCreationEvent + "request for invoice: " + invoice.InvoiceNumber + "; Policy period: " + this.PolicyNumberLong);
        delinquencyProess.createDocumentsForEvent (docCreationEvent.Code, invoice.InvoiceStream.PolicyPeriod_TDIC)
      } else {
        LoggerFactory.getLogger("Application.DelinquencyProcess").info (docCreationEvent + " doc skipped for invoice: " + invoice.InvoiceNumber+ "; AmountDue_TDIC: ${delinquencyProcess.AmountDue_TDIC}");
      }
    }

    this.addHistoryFromGosu(gw.api.util.DateUtil.currentDate(),
        HistoryEventType.TC_DUNNINGLETTERSENT,
        DisplayKey.get("Java.PolicyHistory.DunningLetterSent"), null, null, false)
  }

  function getDelinquencyReasons(): String {
    return this.ActiveDelinquencyProcesses.reduce("", \q, t -> {
      var currentReason = t.Reason.DisplayName
      if (not q.contains(currentReason)) {
        return q + ( q == "" ? "" : ", " ) + currentReason
      } else {
        return q
      }
    })
  }

  function getDelinquencyReason() : DelinquencyReason {
    var reason : DelinquencyReason
    var hasNotTakenReason = this.DelinquencyPlan.DelinquencyPlanReasons
        .hasMatch(\d -> d.DelinquencyReason == TC_NOTTAKEN)
    if (hasNotTakenReason
        and this.getActiveDelinquencyProcesses(TC_NOTTAKEN).Empty
        and this.PaidAmount.IsZero) {
      reason = TC_NOTTAKEN
    } else if (this.getActiveDelinquencyProcesses(TC_PASTDUE).Empty){
      reason = TC_PASTDUE
    }
    return reason
  }

  property get CancellationProcessEvent(): DelinquencyProcessEvent {
    var event: DelinquencyProcessEvent;
    for (dlnqProcess in this.DelinquencyProcesses) {
      var events = dlnqProcess.DelinquencyEvents
      var cancelEvent = events.firstWhere(
          \t -> t.EventName == DelinquencyEventName.TC_CANCELLATION)
      if (cancelEvent != null
          and (event == null or event.TargetDate.after(cancelEvent.TargetDate))){
        event = cancelEvent
      }
    }
    return event;
  }

  @ImpactedByArchiving
  function getChargeTransactionWrappers(): TransactionWrapper[] {
    var transactions = new ArrayList<TransactionWrapper>()
    for (charge in this.Charges)
    {
      transactions.add(
          new TransactionWrapper(charge.ChargeInitialTxn, TransactionWrapper.TransactionType.CHARGE)
      )
    }
    // commission adjust transactions
    var txns = this.getCommissionAdjustedTransactions()
    for (txn in txns.iterator())
    {
      // don't show the reversed transaction, just show the commission amount changed
      if (not txn.isReversal()){
        transactions.add(
            new TransactionWrapper(txn, TransactionWrapper.TransactionType.CHARGE)
        )
      }
    }
    transactions.addAll(
        (this.getChargePaymentTransactions().map(\transaction -> new TransactionWrapper(transaction, TransactionWrapper.TransactionType.PAYMENTANDCREDIT)))?.toList()
    )
    return transactions.sortBy(\c -> c.transaction.TransactionDate).toTypedArray()
  }

  function getTotalCommission(): MonetaryAmount {
    return this.PrimaryPolicyCommission.CommissionExpenseBalance
  }

  @ImpactedByArchiving
  function getChargeCommissionRate(): BigDecimal {
    if (this.TotalValue.signum() == 0){
      return 0;
    }
    return getTotalCommission().multiply(100).divide(this.TotalValue, 2, RoundingMode.DOWN)
  }

  @ImpactedByArchiving
  @java.lang.Deprecated
  /**
   *  This method uses the deprecated field CommissionPaid, which returns the "Commission Expense" - "Commission Reserve"
   *  (commission settled instead of commission that has been paid). If interested in the same amount, please use
   *  {@link #getSettledCommissionRate()} if interesed on the same amount, or {@link #getCommissionPaidRate()} if interested
   *  on the paid commission rate.
   */
  function getPaidCommissionRate(): BigDecimal {
    return getCommissionSettledRate()
  }

  function getCommissionSettledRate() : BigDecimal {
    if (this.PaidOrWrittenOffAmount.signum() == 0){
      return 0;
    }
    return this.PrimaryPolicyCommission.CommissionSettled.multiply(100).divide(this.PaidOrWrittenOffAmount, 2, RoundingMode.DOWN)
  }

  function getCommissionPaidRate(): BigDecimal {
    if (this.PaidOrWrittenOffAmount.signum() == 0){
      return 0;
    }
    return this.PrimaryPolicyCommission.PaidCommission.multiply(100).divide(this.PaidOrWrittenOffAmount, 2, RoundingMode.DOWN)
  }

  property set RequireFinalAudit(required: boolean) {
    if (required) {
      this.scheduleFinalAudit()
    } else {
      this.waiveFinalAudit()
    }
  }

  property get RequireFinalAudit(): boolean {
    return this.ClosureStatus == PolicyClosureStatus.TC_OPENLOCKED ? true : false
  }

  @ImpactedByArchiving
  function hasPastInvoiceItems(): boolean {
    var invoices = this.InvoiceItemsSortedByEventDate
    if (invoices.length == 0) return false;
    return invoices[0].EventDate.before(DateUtil.currentDate())
  }

  function makeSingleCashPaymentUsingNewBundle(amount: MonetaryAmount) {
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      var policyPeriod = bundle.add(Query.make(PolicyPeriod).compare("ID", Equals, this.ID).select().AtMostOneRow)
      DirectBillPaymentFactory.payPolicyPeriod(policyPeriod.Account, policyPeriod, amount)
    })
  }

  property get IsInForce(): boolean {
    return (this.Status == PolicyPeriodStatus.TC_INFORCE) &&
        (
            this.CancelStatus == PolicyCancelStatus.TC_OPEN ||
                this.CancelStatus == PolicyCancelStatus.TC_PENDINGCANCELLATION ||
                this.CancelStatus == PolicyCancelStatus.TC_PENDINGRESCINDCANCEL
        )
  }

  property get HasCATDICProducerCode() : boolean {
    return this.PrimaryProducerCode.Code == "CATDIC"
  }

  property get HasEREChargeOnCanceledPolicyPeriod() : boolean {
    return this?.Canceled and this?.Charges?.firstWhere(\elt ->
        elt.ChargePattern?.ChargeCode == "EREPremium_TDIC")?.InvoiceItems?.HasElements
  }
}
