package libraries

uses gw.api.database.DBFunction
uses gw.api.database.Query
uses gw.api.database.QueryBeanResultWithSummary
uses gw.api.database.QuerySelectColumns
uses gw.api.database.Relop
uses gw.api.path.Paths
uses gw.api.system.BCConfigParameters
uses gw.api.system.PLConfigParameters
uses gw.bc.archive.ImpactedByArchiving
uses gw.pl.currency.MonetaryAmount
uses gw.pl.persistence.core.Key
uses gw.util.Pair

uses java.lang.IllegalStateException
uses java.math.BigDecimal
uses java.util.Date
uses java.util.HashMap
uses java.util.Map

uses entity.ProducerContext
uses typekey.*

/**
 * Defines enhancements for a {@link ProducerStatement}.
 */
@Export
enhancement ProducerStatementExt : entity.ProducerStatement {

  @ImpactedByArchiving
  function getItemEventsWithAggregation(aggregation : PolicyActivityAggType, policyNumber : String) : List<Map<String, Object>> {
    return (aggregation == TC_CHARGE)
        ? getItemEventsGroupedByCharge(policyNumber)
        : getItemEventsUngrouped(policyNumber)
  }

  @ImpactedByArchiving
  property get NumberOfReversalTransactionsWithoutItemEvents() : int {
    return TransactionsWithoutItemEventsQuery
      .compare("ReversedTransaction", NotEquals, null)
      .select()
      .Count
  }

  @ImpactedByArchiving
  property get NumberOfItemEvents() : int {
    return OldItemEventsQuery.select().Count
  }

  @ImpactedByArchiving
  property get TransactionsWithoutItemEvents() : QueryBeanResultWithSummary<ReserveCmsnEarned, Map<String, BigDecimal>> {
    var q = TransactionsWithoutItemEventsQuery
    final var sumQ = TransactionsWithoutItemEventsQuery
    final var sumRes = sumQ
        .select({
            QuerySelectColumns.dbFunction(DBFunction.Sum(Paths.make(ReserveCmsnEarned#Amount_amt)))
        })
        .transformQueryRow(\ row -> ({
            "producerTransaction.PayableAmount" -> row.getColumn(0) as BigDecimal
        }))
    final var valueMapper = new QueryBeanResultWithSummary.ValueMapper<Map<String, BigDecimal>>() {
      override
      function extractValue(summaryResult : Map<String, BigDecimal>, path : String) : java.lang.Number {
        return summaryResult.get(path)
      }

      override
      function extractCurrency(summaryResult : Map<String, BigDecimal>, valuePath : String): Currency {
        return null
      }
    }

    return new QueryBeanResultWithSummary<ReserveCmsnEarned, Map<String, BigDecimal>>(
        q.select(), sumRes, valueMapper)
  }

  @ImpactedByArchiving
  function getItemEventsUngrouped(policyNumber : String) : List<Map<String, Object>> {
    final var CHARGE_GROUP = "ChargeGroup"
    final var CHARGE_NAME = "ChargeName"
    final var EVENT_ID = "EventID"
    final var EVENT_DATE = "EventDate"
    final var EVENT_TYPE = "EventType"
    final var INSTALLMENT_NUMBER = "InstallmentNumber"
    final var INVOICE_ID = "InvoiceID"
    final var INVOICE_ITEM_AMOUNT = "InvoiceItemAmount"
    final var INVOICE_ITEM_AMOUNT_VALUE = "InvoiceItemAmountValue"
    final var INVOICE_ITEM_TYPE = "InvoiceItemType"
    final var TRANSACTION_ID = "TransactionID"
    final var TRANSACTION_AMOUNT = "TransactionAmount"

    final var query = getItemEventsQuery(policyNumber, false, false)

    final var transactionEarningTypes = lookupTransactionPayableCriteria(query)
    final var accountNames = lookupAccountInvoiceAccountNames(query)
    final var results = query
        .select({
            QuerySelectColumns.pathWithAlias(EVENT_ID, Paths.make(ItemEvent#ID)),
            QuerySelectColumns.pathWithAlias(TRANSACTION_ID, Paths.make(ItemEvent#Transaction)),
            QuerySelectColumns.pathWithAlias(EVENT_TYPE, Paths.make(ItemEvent#EventType)),
            QuerySelectColumns.pathWithAlias(EVENT_DATE, Paths.make(ItemEvent#EventDate)),
            QuerySelectColumns.pathWithAlias(INVOICE_ID, Paths.make(ItemEvent#InvoiceItem, InvoiceItem#Invoice)),
            QuerySelectColumns.pathWithAlias(CHARGE_NAME, Paths.make(ItemEvent#InvoiceItem, InvoiceItem#Charge, Charge#ChargePattern, ChargePattern#ChargeName)),
            QuerySelectColumns.pathWithAlias(CHARGE_GROUP, Paths.make(ItemEvent#InvoiceItem, InvoiceItem#Charge, Charge#ChargeGroup)),
            QuerySelectColumns.pathWithAlias(INVOICE_ITEM_TYPE, Paths.make(ItemEvent#InvoiceItem, InvoiceItem#Type)),
            QuerySelectColumns.pathWithAlias(INSTALLMENT_NUMBER, Paths.make(ItemEvent#InvoiceItem, InvoiceItem#InstallmentNumber)),
            QuerySelectColumns.pathWithAlias(INVOICE_ITEM_AMOUNT, Paths.make(ItemEvent#InvoiceItem, InvoiceItem#Amount)),
            QuerySelectColumns.pathWithAlias(INVOICE_ITEM_AMOUNT_VALUE, Paths.make(ItemEvent#InvoiceItem, InvoiceItem#Amount_amt)),
            QuerySelectColumns.pathWithAlias(TRANSACTION_AMOUNT, Paths.make(ItemEvent#Transaction, Transaction#Amount))
        })
        .transformQueryRow(\ row ->
            new HashMap<String, Object>() {

                "EventID" -> row.getColumn(EVENT_ID),

                "EarningType" -> getEarningType(
                    transactionEarningTypes.get(row.getColumn(TRANSACTION_ID)), row.getColumn(EVENT_TYPE) as ItemEventType),

                "EventDate" -> row.getColumn(EVENT_DATE),

                "RelatedAccount" ->
                    accountNames.get(row.getColumn(INVOICE_ID))?.AccountDisplayName,

                "RelatedPolicyPeriod" -> policyNumber,

                "ChargeName" -> getChargeDisplayName(
                    row.getColumn(CHARGE_NAME) as String,
                    row.getColumn(CHARGE_GROUP) as String),

                "ItemType" -> row.getColumn(INVOICE_ITEM_TYPE) == InvoiceItemType.TC_INSTALLMENT
                    ? InvoiceItemType.TC_INSTALLMENT.DisplayName + " "
                    + row.getColumn(INSTALLMENT_NUMBER)
                    : row.getColumn(INVOICE_ITEM_TYPE),

                "Basis" -> row.getColumn(INVOICE_ITEM_AMOUNT),

                "CommissionPercentage" -> (row.getColumn(INVOICE_ITEM_AMOUNT_VALUE) as BigDecimal) == 0bd
                    ? 0
                    : calculateCommissionPercentage(
                    row.getColumn(TRANSACTION_AMOUNT) as MonetaryAmount, row.getColumn(INVOICE_ITEM_AMOUNT) as MonetaryAmount),
                "CommissionAmount" -> row.getColumn(EVENT_TYPE) == ItemEventType.TC_COMMISSIONMOVEDTO
                    ? -(row.getColumn(TRANSACTION_AMOUNT) as MonetaryAmount) : row.getColumn(TRANSACTION_AMOUNT)
            })
    // See JIRA PL-10899
    return results.toList()
  }

  @ImpactedByArchiving
  function getItemEventsGroupedByCharge(policyNumber : String) : List<Map<String, Object>> {
    final var CHARGE_AMOUNT = "ChargeAmount"
    final var CHARGE_DATE = "ChargeDate"
    final var CHARGE_GROUP = "ChargeGroup"
    final var CHARGE_ID = "ChargeID"
    final var CHARGE_NAME = "ChargeName"
    final var TACCOUNT_CONTAINER_ID = "TAccountContainerID"
    final var TACCOUNT_CONTAINER_SUBTYPE = "TAccountContainerSubtype"
    final var TRANSACTION_AMOUNT_SUM = "TransactionAmountSum"

    final var query = getItemEventsQuery(policyNumber, false, true)

    final var accountNames = lookupPolTAcctAccountNames(query)
    final var results = query
        .select({
            QuerySelectColumns.pathWithAlias(CHARGE_ID, Paths.make(ItemEvent#InvoiceItem, InvoiceItem#Charge)),
            QuerySelectColumns.pathWithAlias(CHARGE_DATE, Paths.make(ItemEvent#InvoiceItem, InvoiceItem#Charge, Charge#ChargeDate)),
            QuerySelectColumns.pathWithAlias(TACCOUNT_CONTAINER_ID, Paths.make(ItemEvent#InvoiceItem, InvoiceItem#Charge, Charge#TAccountContainer)),
            QuerySelectColumns.pathWithAlias(TACCOUNT_CONTAINER_SUBTYPE, Paths.make(ItemEvent#InvoiceItem, InvoiceItem#Charge, Charge#TAccountContainer, TAccountContainer#Subtype)),
            QuerySelectColumns.pathWithAlias(CHARGE_NAME, Paths.make(ItemEvent#InvoiceItem, InvoiceItem#Charge, Charge#ChargePattern, ChargePattern#ChargeName)),
            QuerySelectColumns.pathWithAlias(CHARGE_GROUP, Paths.make(ItemEvent#InvoiceItem, InvoiceItem#Charge, Charge#ChargeGroup)),
            QuerySelectColumns.pathWithAlias(CHARGE_AMOUNT, Paths.make(ItemEvent#InvoiceItem, InvoiceItem#Charge, Charge#Amount)),
            QuerySelectColumns.dbFunctionWithAlias(TRANSACTION_AMOUNT_SUM, DBFunction.Sum(Paths.make(ItemEvent#Transaction, Transaction#Amount_amt)))
        })
        .transformQueryRow(\ row ->
            new HashMap<String, Object>() {

                "ChargeID" -> row.getColumn(CHARGE_ID),

                "EventDate" -> row.getColumn(CHARGE_DATE),

                "RelatedAccount" ->
                    accountNames.get(row.getColumn(TACCOUNT_CONTAINER_ID))
                        ?.AccountDisplayName,
                "RelatedPolicyPeriod" -> (PolTAcctContainer.Type.isAssignableFrom((row.getColumn(TACCOUNT_CONTAINER_SUBTYPE) as typekey.TAccountContainer).getEntityType()))
                    ? policyNumber : null,

                "ChargeName" -> getChargeDisplayName(
                    row.getColumn(CHARGE_NAME) as String,
                    row.getColumn(CHARGE_GROUP) as String),

                "Basis" -> row.getColumn(CHARGE_AMOUNT),

                "CommissionAmount" ->
                    (row.getColumn(TRANSACTION_AMOUNT_SUM) as BigDecimal).ofCurrency(this.Currency),
                "CommissionPercentage" -> calculateCommissionPercentage(
                    (row.getColumn(TRANSACTION_AMOUNT_SUM) as BigDecimal).ofCurrency(this.Currency),
                    row.getColumn(CHARGE_AMOUNT) as MonetaryAmount)
            })
    // See JIRA PL-10899
    return results.toList()
  }

  function resendStatement() {
    // no base implementation.
  }

  @ImpactedByArchiving
  property get AutoPaymentAmount() : MonetaryAmount {
    if (!this.isProcessed()) {
      throw new IllegalStateException("This is not available until the producer statement is processed");
    }
    var producerPaymentSentQuery = Query.make(ProducerPaymentSent)
    // We want to filter out only reversal txns., not reversed, as the reversed txn. is still the one
    // that provides the auto payment amount for the period it was issued in.
    producerPaymentSentQuery.compare(ProducerPaymentSent.REVERSEDTRANSACTION_PROP.get(), Relop.Equals, null)
    var prodPymtSentContextTable = producerPaymentSentQuery.join(ProdPymtSentContext.TRANSACTION_PROP.get())
    prodPymtSentContextTable.compare(ProducerContext.STATEMENT_PROP.get(), Relop.Equals, this)
    var producerPaymentTable = prodPymtSentContextTable.join(ProdPymtSentContext.PRODUCERPAYMENT_PROP.get());
    producerPaymentTable.compare(ProducerPayment.SUBTYPE_PROP.get(), Relop.Equals, typekey.ProducerPayment.TC_AUTOPAYMENT);

    var producerPaymentSentAmount : BigDecimal = 0
    var amountColumn = QuerySelectColumns.path(ProducerPaymentSent.AMOUNT_AMT_PROP.get())
    var queryRow =  producerPaymentSentQuery
        .select({amountColumn})
        .getAtMostOneRow()
    if (queryRow != null ) {
      producerPaymentSentAmount = queryRow.getColumn(0) as BigDecimal
    }
    return producerPaymentSentAmount.ofCurrency(this.Currency)
  }

  /**
   * Whether any {@link ChargeCommission} or {@link PolicyCommission}
   * transactions that were sent on this producer statement possibly are
   * archived along with their {@link PolicyPeriod}. If this is {@code true},
   * then the values on this statement may not be accurate. Note this may return
   * a false positive, but should never return a false negative.
   */
  property get HasPossiblyArchivedPolicies() : boolean {
    return PLConfigParameters.ArchiveEnabled.Value
        and this.StatementDate.addDays(
                BCConfigParameters.ArchivePolicyPeriodDays.Value)
            .before(Date.Now)
        and not this.Producer.ArchivedPolicyCommissions.Empty
  }

  private property get TransactionsWithoutItemEventsQuery() : Query<ReserveCmsnEarned> {
    var transactions = Query.make(ReserveCmsnEarned)
    transactions.subselect("ID", CompareNotIn, Query.make(ItemEvent), "Transaction")
    transactions.join(ProducerContext.Type, "Transaction")
      .compare("Statement", Equals, this)
    return transactions
  }

  function getItemEvents(policyNumber : String, isPartialSearch : boolean) : List<Map<String, Object>> {
    final var POLICY_NUMBER_LONG = "PolicyNumberLong"
    final var POLICY_PERIOD_COUNT = "PolicyPeriodCount"
    final var TRANSACTION_AMOUNT_SUM = "TransactionAmountSum"

    var query = getItemEventsQuery(policyNumber, isPartialSearch, true)
    var results = query
        .select({
            QuerySelectColumns.pathWithAlias(POLICY_NUMBER_LONG, Paths.make(ItemEvent#PolicyCommission, PolicyCommission#PolicyPeriod, PolicyPeriod#PolicyNumberLong)),
            QuerySelectColumns.dbFunctionWithAlias(POLICY_PERIOD_COUNT, DBFunction.Count(Paths.make(ItemEvent#PolicyCommission, PolicyCommission#PolicyPeriod))),
            QuerySelectColumns.dbFunctionWithAlias(TRANSACTION_AMOUNT_SUM, DBFunction.Sum(Paths.make(ItemEvent#Transaction, Transaction#Amount_amt)))
        })
        .transformQueryRow(\ row ->
            new HashMap<String, Object>() {
                "PolicyNumber" -> row.getColumn(POLICY_NUMBER_LONG),

                "NumberEarned" -> row.getColumn(POLICY_PERIOD_COUNT),

                "CommissionAmount" -> row.getColumn(TRANSACTION_AMOUNT_SUM)
            })
    // See JIRA PL-10899
    return results.toList()
  }

  private function getItemEventsQuery(policyNumber : String, isPartialSearch : boolean, grouped : boolean) : Query<ItemEvent> {
    var query = Query.make(ItemEvent)
    var itemEventTable = query
    var transactionTable = itemEventTable.join("Transaction")
    var producerTransactionTable = transactionTable.cast( ProducerTransaction)
    var producerContextTable = producerTransactionTable.join("ProducerContext")
    producerContextTable.compare("Statement", Equals, this)
    var policyCommissionTable = itemEventTable.join("PolicyCommission")
    var policyPeriodTable = policyCommissionTable.join("PolicyPeriod")
    if (grouped) {
      restrictByItemEventTypeForGrouping(query)
    } else {
      restrictByItemEventTypeForNoGrouping(query)
    }
    if (policyNumber != null) {
      if (!isPartialSearch) {
        policyPeriodTable.compare("PolicyNumberLong", Equals, policyNumber)
      } else {
        policyPeriodTable.startsWith("PolicyNumberLong", policyNumber, true)
      }
    }
    return query
  }

  // Ignoring ItemEventType.TC_COMMISSIONMOVEDFROM and ItemEventType.TC_COMMISSIONMOVEDTO here as they offset each other
  private function restrictByItemEventTypeForGrouping(itemEventQuery : Query<ItemEvent>) {
    itemEventQuery.compareIn("EventType",
        new ItemEventType[] {ItemEventType.TC_EARNED})
  }

  private function restrictByItemEventTypeForNoGrouping(itemEventQuery : Query<ItemEvent>) {
    itemEventQuery.compareIn("EventType",
        new ItemEventType[] {TC_EARNED, TC_COMMISSIONMOVEDFROM, TC_COMMISSIONMOVEDTO})
  }

  private property get OldItemEventsQuery() : Query<ItemEvent> {
    var query = Query.make( ItemEvent)
    var itemEventTable = query
    var transactionTable = itemEventTable.join("Transaction")
    var producerTransactionTable = transactionTable.cast( ProducerTransaction)
    var producerContextTable = producerTransactionTable.subselect("Id", CompareIn, ProducerContext, "Transaction")
    restrictByItemEventTypeForNoGrouping(itemEventTable)
    producerContextTable.compare( "Statement", Equals, this )
    return query
  }

  private function getEarningType(payableCriteria : PayableCriteria, eventType : ItemEventType) : String {
    return payableCriteria != null
        ? eventType.DisplayName + " (" + payableCriteria.DisplayName + ")"
        : eventType.DisplayName
  }

  /**
   * Look up the {@link ReserveCmsnEarned} transaction {@link PayableCriteria}
   *   value for those associated with the {@link ItemEvent}s identified by the
   *   specified query.
   *
   * @param itemEventQuery the {@link ItemEvent} query source.
   * @return A map of the {@link ReserveCmsnEarned} identifier keys to each
   *    associated {@link PayableCriteria}.
   */
  private function lookupTransactionPayableCriteria(itemEventQuery : Query<ItemEvent>) : Map<Key, PayableCriteria> {
    final var PAYABLE_CRITERIA = "PayableCriteria"
    final var RESERVE_CMSN_EARNED_ID = "ReserveCmsnEarnedID"

    final var transactionQry = Query.make(ReserveCmsnEarned)
    transactionQry.subselect("ID", CompareIn, itemEventQuery, "Transaction")
    return transactionQry
      .select({
          QuerySelectColumns.pathWithAlias(RESERVE_CMSN_EARNED_ID, Paths.make(ReserveCmsnEarned#ID)),
          QuerySelectColumns.pathWithAlias(PAYABLE_CRITERIA, Paths.make(ReserveCmsnEarned#ProducerContext, ProducerContext#PayableCriteria))
      })
      .transformQueryRow(\ row ->
          new Pair<Key, PayableCriteria>(row.getColumn(RESERVE_CMSN_EARNED_ID) as Key, row.getColumn(PAYABLE_CRITERIA) as PayableCriteria))
      .partitionUniquely(\ entryPair -> entryPair.First)
      .mapValues(\ pair -> pair.Second)
  }

  /**
   * Look up the {@link AccountInvoice} {@link Account} name values for those
   *   associated with the {@link ItemEvent}s identified by the specified query.
   *
   * @param itemEventQuery the {@link ItemEvent} query source.
   * @return A map of the {@link AccountInvoice} identifier keys to a map of
   *    each associated {@link Account} name values.
   */
  private function lookupAccountInvoiceAccountNames(itemEventQuery: Query<ItemEvent>) : Map<Key, AccountNames> {
    final var ID = "ID"
    final var ACCOUNT_NAME = "AccountName"
    final var ACCOUNT_NAME_KANJI = "AccountNameKanji"
    final var ACCOUNT_NUMBER = "AccountNumber"

    final var invoiceItemQry = Query.make(InvoiceItem)
    invoiceItemQry.subselect("ID", CompareIn, itemEventQuery, "InvoiceItem")
    final var invoiceQry = Query.make(AccountInvoice)
    invoiceQry.subselect("ID", CompareIn, invoiceItemQry, "Invoice")
    return invoiceQry
      .select({
          QuerySelectColumns.pathWithAlias(ID, Paths.make(AccountInvoice#ID)),
          QuerySelectColumns.pathWithAlias(ACCOUNT_NAME_KANJI, Paths.make(AccountInvoice#Account, Account#AccountNameKanji)),
          QuerySelectColumns.pathWithAlias(ACCOUNT_NAME, Paths.make(AccountInvoice#Account, Account#AccountName)),
          QuerySelectColumns.pathWithAlias(ACCOUNT_NUMBER, Paths.make(AccountInvoice#Account, Account#AccountNumber))
      })
      .transformQueryRow(\row -> new AccountNames(row.getColumn(ID) as Key,
          row.getColumn(ACCOUNT_NAME_KANJI) != null
              ? row.getColumn(ACCOUNT_NAME_KANJI) as String : row.getColumn(ACCOUNT_NAME) as String,
          row.getColumn(ACCOUNT_NUMBER) as String))
      .partitionUniquely(\accountNames -> accountNames.ContainerID)
  }

  /**
   * Look up the {@link PolTAcctContainer} {@link Account} name values for those
   *   associated with the {@link ItemEvent}s identified by the specified query.
   *
   * @param itemEventQuery the {@link ItemEvent} query source.
   * @return A map of the {@link PolTAcctContainer} identifier keys to a map of
   *    each associated {@link Account} name values.
   */
  private function lookupPolTAcctAccountNames(itemEventQuery : Query<ItemEvent>) : Map<Key, AccountNames> {
    final var ID = "ID"
    final var ACCOUNT_NAME = "AccountName"
    final var ACCOUNT_NAME_KANJI = "AccountNameKanji"
    final var ACCOUNT_NUMBER = "AccountNumber"

    final var invoiceItemQry = Query.make(InvoiceItem)
    invoiceItemQry.subselect("ID", CompareIn, itemEventQuery, "InvoiceItem")
    final var chargeQry = Query.make(Charge)
    chargeQry.subselect("ID", CompareIn, invoiceItemQry, "Charge")
    final var tAcctContainerQry = Query.make(PolTAcctContainer)
    tAcctContainerQry.subselect("ID", CompareIn, chargeQry, "TAccountContainer")
    return tAcctContainerQry
        .select({
            QuerySelectColumns.pathWithAlias(ID, Paths.make(PolTAcctContainer#ID)),
            QuerySelectColumns.pathWithAlias(ACCOUNT_NAME_KANJI, Paths.make(PolTAcctContainer#PolicyPeriod, PolicyPeriod#Policy, Policy#Account, Account#AccountNameKanji)),
            QuerySelectColumns.pathWithAlias(ACCOUNT_NAME, Paths.make(PolTAcctContainer#PolicyPeriod, PolicyPeriod#Policy, Policy#Account, Account#AccountName)),
            QuerySelectColumns.pathWithAlias(ACCOUNT_NUMBER, Paths.make(PolTAcctContainer#PolicyPeriod, PolicyPeriod#Policy, Policy#Account, Account#AccountNumber))
        })
        .transformQueryRow(\row -> new AccountNames(row.getColumn(ID) as Key,
            row.getColumn(ACCOUNT_NAME_KANJI) != null
                ? row.getColumn(ACCOUNT_NAME_KANJI) as String
                : row.getColumn(ACCOUNT_NAME) as String,
            row.getColumn(ACCOUNT_NUMBER) as String))
        .partitionUniquely(\accountNames -> accountNames.ContainerID)
  }

  private function getChargeDisplayName(chargeName : String, chargeGroup : String) : String {
    var displayName = chargeName
    if (chargeGroup != null) {
      displayName += " - " + chargeGroup
    }

    return displayName
  }

  private function
  calculateCommissionPercentage(commissionAmount : MonetaryAmount, basis : MonetaryAmount)
      : BigDecimal {
    return (commissionAmount * 100bd).divide(basis, HALF_UP)
  }
}
