package libraries

uses gw.api.locale.DisplayKey
uses gw.api.util.CurrencyUtil
uses gw.pl.currency.MonetaryAmount
uses gw.transaction.ChargePatternHelper
uses tdic.bc.integ.plugins.hpexstream.bo.TDIC_InvoiceItemsByTerm

uses java.math.BigDecimal

@Export
enhancement InvoiceExt : Invoice {
  /**
   * @returns the Invoice Status, plus the Frozen status.  This is primarily for use in the UI where we want to display the two values together.
   */
  property get StatusForUI() : String {
    var statusString = this.Status.DisplayName

    if (this.Frozen) {
      return DisplayKey.get('DisplayName.Invoice.Status.Frozen', statusString)
    } else {
      return statusString
    }
  }

  // Utility methods for the invoice document.

  property get PolicyRecapture_TDIC(): BigDecimal {
    return this.InvoiceItems.where(\elt -> elt.PolicyPeriod != null && elt.Charge.ChargePattern.ChargeCode.equalsIgnoreCase("PolicyRecapture")).sum(\s -> s.OutstandingBalanceTDIC)
  }

  property get PaymentReversalFee_TDIC(): BigDecimal {
    return this.InvoiceItems.where(\elt -> elt.PolicyPeriod != null && elt.Charge.ChargePattern.ChargeCode.equalsIgnoreCase("PolicyPaymentReversalFee")).sum(\s -> s.OutstandingBalanceTDIC)
  }

  property get CreditCardFee_TDIC(): BigDecimal {
    return this.InvoiceItems.where(\elt -> elt.PolicyPeriod != null && elt.Charge.ChargePattern.ChargeCode.equalsIgnoreCase("CreditCardUsageFee")).sum(\s -> s.OutstandingBalanceTDIC)
  }

  property get InvoiceItems_TDIC(): ArrayList<TDIC_InvoiceItemsByTerm> {
    var notToIncludeCharges = {ChargePatternHelper.getChargePattern("PolicyPaymentReversalFee"),
        ChargePatternHelper.getChargePattern("CreditCardUsageFee"), ChargePatternHelper.getChargePattern("PolicyRecapture")}
    var invoiceItems = new ArrayList<TDIC_InvoiceItemsByTerm>(){}
    var terms = this.InvoiceItems*.Charge*.PolicyPeriod*.TermNumber.toSet().toTypedArray()
    if (terms != null && terms.HasElements) {
      foreach (term in terms) {
        var invoiceItemByTerm = new TDIC_InvoiceItemsByTerm()
        invoiceItemByTerm.TermNumber = term
        invoiceItemByTerm.InvoiceItems = this.InvoiceItems.where(\invoiceItem -> term == invoiceItem.PolicyPeriod.TermNumber
            and !invoiceItem.FullyConsumed
            and !notToIncludeCharges.contains(invoiceItem.Charge.ChargePattern))
            .orderBy(\invoiceItem -> invoiceItem.Charge.ChargePattern.PrintPriority_TDIC)
            .thenBy(\invoiceItem -> invoiceItem.Charge.BillingInstruction.DisplayName).toTypedArray()
        invoiceItems.add(invoiceItemByTerm)
      }
    }
    return invoiceItems
  }

  property get PriorBalancePayload_TDIC() : MonetaryAmount {
    var previousBalance = new MonetaryAmount(0bd, CurrencyUtil.DefaultCurrency)
    previousBalance = this.InvoiceStream.InvoicesSortedByEventDate.where(\elt -> elt.BilledOrDue
        and elt.InvoiceNumber != this.InvoiceNumber).sum(\a -> a.TotalOutstandingAmountTDIC)
    if (previousBalance != null) {
      return previousBalance
    } else {
      return new MonetaryAmount(0bd, CurrencyUtil.DefaultCurrency)
    }
  }
}
