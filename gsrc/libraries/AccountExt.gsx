package libraries

uses gw.api.database.DBFunction
uses gw.api.database.Query
uses gw.api.database.QuerySelectColumns
uses gw.api.domain.accounting.ChargePatternKey
uses gw.api.domain.delinquency.DelinquencyTarget
uses gw.api.locale.DisplayKey
uses gw.api.upgrade.Coercions
uses gw.api.util.DateUtil
uses gw.api.web.payment.DirectBillPaymentFactory
uses gw.pl.currency.MonetaryAmount
uses gw.search.ReversibleChargeSearchCriteria
uses org.slf4j.LoggerFactory

/**
 * Defines enhancements for an {@link Account}.
 */
@Export
enhancement AccountExt : Account {
  /**
   * A reversible {@link ChargePatternKey#ACCOUNTLATEFEE Account Late Fee}
   * or {@link ChargePatternKey#POLICYLATEFEE Policy Late Fee}
   * {@link ReversibleChargeSearchCriteria SearchCriteria} for this
   * {@link Account}.
   */
  property get ReversibleLateFeeSearchCriteria() : ReversibleChargeSearchCriteria {
    final var criteria = new ReversibleChargeSearchCriteria()
    criteria.Account = this
    criteria.ChargePatterns =
        {ChargePatternKey.ACCOUNTLATEFEE.get(), ChargePatternKey.POLICYLATEFEE.get()}
    return criteria
  }

  property get OpenDelinquencyTargets(): DelinquencyTarget[] {
    var all: List<DelinquencyTarget> = {}
    all.add(this)
    all.addAll(this.OpenPolicyPeriods?.toList())
    return all.toTypedArray()
  }

  function getPaymentDueDateToAvoidDelinquencyOn(date : Date) : Date {
    var dueInvoice = getMostRecentPastDueInvoiceAsOf(date)
    if (dueInvoice != null) {
      return dueInvoice.PaymentDueDate
    } else {
      return null
    }
  }

  /**
   * The most recent {@link AccountInvoice} where the {@link //AccountInvoice#PaymentDueDate
   * PaymentDueDate} is "date" or before.
   */
  function getMostRecentPastDueInvoiceAsOf(date : Date) : Invoice {
    final var invoiceQuery = Query.make<Invoice>(AccountInvoice)
    invoiceQuery.compare(AccountInvoice#Account, Equals, this)
    invoiceQuery.compare(AccountInvoice#PaymentDueDate, LessThanOrEquals, date)
    var min = invoiceQuery.select({QuerySelectColumns.dbFunction(
        DBFunction.Max(invoiceQuery.getColumnRef(AccountInvoice#EventDate.PropertyInfo.Name)))})
    invoiceQuery
        .compare(AccountInvoice#EventDate, Equals, Coercions.makeDateFrom(min.AtMostOneRow.getColumn(0)))
    return invoiceQuery.select().FirstResult
  }

  function discoverDelinquencies(): List<DelinquencyProcess> {
    var logger = LoggerFactory.getLogger("Application.DelinquencyProcess");
    var LOG_TAG = "DiscoverDelinquencies - ";
    var newDelinquencyProcesses: List<DelinquencyProcess> = {}

    logger.debug (LOG_TAG + "Discovering delinquencies for Account " + this);

    // Added by SN: 08/22/2019
    // look for the open policyperiods {exclude the periods which are already closed} of the account and store all the periods in the list.

    // SN: 04/10/2020 GBC-3118 - two term delinquency, Past Due used when Not Taken expected
    // Once the delinquency is open on the latest term for any term charge, other terms are suppressed to check its payment criteria.
    // Situation may occur that latest term is not paid at all and the associated prior term audit charge may open a past due delinquency
    // on the latest term instead of a not taken delinquency trialled for the latest term itself.
    // Since the policyperiodList has stored all policyperiods in a random manner for the delinquency open trial,
    // if multiple term charges are involved in the delinquency then before opening the delinquency
    // need to check the payment criteria on the latest term of policy for the appropriate delinquency reason.
    // If the policyPeriodList stores all values in descending order of policyperiod then latest term will be picked first to check
    // the payment criteria before opening the delinquency on the latest term and prior term payment criteria will be suppressed.

    // SN: 04/16/2020 GBC-3080 - Delinquency is not triggering for ERE enrolled policy
    // Ideally we don't want to pick the canceled policyperiods for opening delinquency but
    // need to add the ERE enrolled policyperiods for opening delinquency irrespective of their canceled status.
    var policyPeriodList = this.OpenPolicyPeriods.where(\pp -> pp.IsInForce or pp.Status == PolicyPeriodStatus.TC_EXPIRED
        or pp.Status == PolicyPeriodStatus.TC_FUTURE or pp.HasEREChargeOnCanceledPolicyPeriod).toList()
    //SN: 04/10/2020 GBC-3118 - two term delinquency, See above notes
    Collections.sort(policyPeriodList, Collections.reverseOrder())

    // start policyDlnqProcesses on not taken or past due policies
    for (policyPeriod in policyPeriodList) {
      if (not policyPeriod.LatestPolicyPeriod.hasActiveDelinquencyProcess() /*or not policyPeriod.hasActiveDelinquencyProcess()*/) {
        logger.info(LOG_TAG + "Checking if delinquency should be started for PolicyPeriod " + policyPeriod)
        var isPendingCancellation = policyPeriod.CancelStatus == TC_PENDINGCANCELLATION

      if (policyPeriod.EligibleToStartDelinquency
          and not policyPeriod.isDirectOrImpliedTargetOfHoldType(TC_DELINQUENCY)
          and not policyPeriod.Account.isDirectOrImpliedTargetOfHoldType(TC_DELINQUENCY)
          and not isPendingCancellation) {
        var process : DelinquencyProcess
        var hasNotTakenReason = policyPeriod.DelinquencyPlan.DelinquencyPlanReasons
            .hasMatch(\ d -> d.DelinquencyReason == DelinquencyReason.TC_NOTTAKEN)

        // 20160901 TJT: GW-2275 this is the OOTB logic.  Because TDIC allows flat cancels to be scheduled instead of
        // cancelling them immediately, there is a new possibility that two delinquencies may be active at the same time
        // so we're circumventing this.  See GW Support Incident: 160901-000021
        /*
        if( hasNotTakenReason
            and  policyPeriod.getActiveDelinquencyProcesses(DelinquencyReason.TC_NOTTAKEN).Empty
              and policyPeriod.PaidAmount.IsZero){
          process = policyPeriod.startDelinquency(DelinquencyReason.TC_NOTTAKEN);
          newDelinquencyProcesses.add(process)
        } else if ( policyPeriod.getActiveDelinquencyProcesses( DelinquencyReason.TC_PASTDUE ).Empty){
          process = policyPeriod.startDelinquency(DelinquencyReason.TC_PASTDUE);
          newDelinquencyProcesses.add(process)
        }
        */

        //20160909 TJT: GW-2275 (see above)
        // SN : 08/22/2019
        // For new business and renewal term if the first invoice is not paid then open not-taken delinquency else past due delinquency.
        if(policyPeriod.getActiveDelinquencyProcesses(DelinquencyReason.TC_NOTTAKEN).Empty
            and policyPeriod.getActiveDelinquencyProcesses( DelinquencyReason.TC_PASTDUE ).Empty){
          // Can't check for the latest period before this line else will lose the prior policyperiod charges which may satisfy the delinquency criteria
          // Below if loop will check for any term to open the delinquency based on the payment criteria
          if( hasNotTakenReason and policyPeriod.PaidAmount.IsZero) {
            logger.info (LOG_TAG + "Starting Not Taken delinquency for PolicyPeriod " + policyPeriod);
            // SN: 01/23/2020 find latest term of the policyperiods to open the delinquency
            // SN: 04/06/2020 GBC 3026 if the latest term is closed then use the originated term
            if(policyPeriod != policyPeriod.LatestPolicyPeriod and policyPeriod.LatestPolicyPeriod.Closed){
              policyPeriod = policyPeriod
            } else {
              policyPeriod = policyPeriod.LatestPolicyPeriod
            }
            process = policyPeriod.startDelinquency(DelinquencyReason.TC_NOTTAKEN);
            newDelinquencyProcesses.add(process)
          } else {
            logger.info (LOG_TAG + "Starting Past Due delinquency for PolicyPeriod " + policyPeriod);
            // SN: 01/23/2020 find latest term of the policyperiods to open the delinquency
            // SN: 04/06/2020 GBC-3026 if the latest term is closed then use the originated term
            if(policyPeriod != policyPeriod.LatestPolicyPeriod and policyPeriod.LatestPolicyPeriod.Closed){
              policyPeriod = policyPeriod
            } else {
              policyPeriod = policyPeriod.LatestPolicyPeriod
            }
            process = policyPeriod.startDelinquency(DelinquencyReason.TC_PASTDUE);
            newDelinquencyProcesses.add(process)
          }
        }
        // TODO : Revisit this code.
        // V10 ootb code
        /*var reason = policyPeriod.getDelinquencyReason()
        if (reason != null) {
          process = policyPeriod.startDelinquency(reason)
          newDelinquencyProcesses.add(process)
        }*/
       }
      }
    }

    // start DelinquencyProcesses on past due account level charges
    /*if (( this.getActiveAccountLevelDelinquencyProcess(TC_PASTDUE) == null )
        and this.EligibleToStartDelinquency) {
      logger.info (LOG_TAG + "Starting Past Due delinquency for Account " + this);
      var process = this.startDelinquency(TC_PASTDUE)
      newDelinquencyProcesses.add(process)
    }*/

    if (this.Collateral.DueUnsettledAmount.compareTo(this.DelinquencyPlan.getPolEnterDelinquencyThreshold(this.getCurrency())) == 1) {
      // var process = start a delinquency process for past due collateral charges
      // newDelinquencyProcesses.add(process)

      // NOTE: Collateral Delinquency Processes do not exist, so if you choose to instead start a Policy or
      // Account level delinquency process, this should be carefully marked, so that is recognizable
      // as a Collateral Delinquency Process in the future.  See DelinquencyProcessExt.onChargesPaid()
    }
    return newDelinquencyProcesses
  }

  /**
   * Find the latest due invoice.
   */
  function getLatestDueInvoice_TDIC (policyPeriod : PolicyPeriod) : Invoice {
    var targetInvoice = policyPeriod.Invoices.where(\i -> i.Status == InvoiceStatus.TC_DUE)
        .orderByDescending(\i -> i.DueDate)
        .thenByDescending(\i -> i.CreateTime)
        .first();
    LoggerFactory.getLogger("Application.DelinquencyProcess").info("GetLatestDueInvoice for PolicyTerm ${policyPeriod.PolicyNumberLong}- InvoiceNumber: ${targetInvoice?.InvoiceNumber}; DueAmount: ${targetInvoice?.AmountDue_amt}; ${targetInvoice}");
    return targetInvoice
  }

  /**
   * Find the latest Billed invoice.
   */
  function getLatestBilledInvoice_TDIC (policyPeriod : PolicyPeriod) : Invoice {
    var targetInvoice = policyPeriod.Invoices.where(\i -> i.Status == InvoiceStatus.TC_BILLED)
        .orderByDescending(\i -> i.UpdateTime)
        .first();
    LoggerFactory.getLogger("Application.DelinquencyProcess").info("GetLatestBilledInvoice for PolicyTerm ${policyPeriod.PolicyNumberLong}- InvoiceNumber: ${targetInvoice?.InvoiceNumber}; DueAmount: ${targetInvoice?.AmountDue_amt}; ${targetInvoice}");
    return targetInvoice
  }
  property get DelinquencyReasons(): String {
    var reasons: List<String> = {}
    for (p in this.ActiveDelinquencyProcesses) {
      var reason = p.Reason.DisplayName
      if (not reasons.contains(reason)) {
        reasons.add(reason)
      }
    }
    var result: String
    for (s in reasons index i) {
      if (i == 0) {
        result = s
      }
      else {
        result = result + ", " + s
      }
    }
    return result
  }

  // methods used for Account-level delinquency processes (via workflow) ==========================================

  function sendDunningLetterInternal() {
    this.addHistoryFromGosu(gw.api.util.DateUtil.currentDate(),
        HistoryEventType.TC_DUNNINGLETTERSENT,
        DisplayKey.get("Java.AccountHistory.DunningLetterSent"), null, null, false)
  }

  // methods used by new transaction wizards ======================================================================

  function makeSingleCashPaymentUsingNewBundle(amount: MonetaryAmount) {
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      var account = bundle.add(gw.api.database.Query.make(Account).compare("ID", Equals, this.ID).select().AtMostOneRow)
      DirectBillPaymentFactory.pay(account, amount)
    })
  }

  function makeSingleCashDirectBillMoneyReceivedUsingNewBundle(amount: MonetaryAmount) {
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      var account = bundle.add(gw.api.database.Query.make(Account).compare("ID", Equals, this.ID).select().AtMostOneRow)
      var cashInstrument = new PaymentInstrument()
      cashInstrument.PaymentMethod = PaymentMethod.TC_CASH
      var moneyReceived = DirectBillPaymentFactory.createDirectBillMoneyRcvd(account, cashInstrument, amount)
      moneyReceived.execute()
    })
  }
}