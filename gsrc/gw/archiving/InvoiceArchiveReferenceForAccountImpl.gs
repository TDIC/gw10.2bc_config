package gw.archiving

@Export
class InvoiceArchiveReferenceForAccountImpl implements ArchiveReferenceByChargeOwner {
  var _invoiceArchiveReferenceForAccount: InvoiceArchiveReferenceForAccount

  construct(invoiceArchiveReferenceForAccount: InvoiceArchiveReferenceForAccount) {
      _invoiceArchiveReferenceForAccount = invoiceArchiveReferenceForAccount
  }

  override property get ChargeTAccountOwner() : TAccountOwner {
    return _invoiceArchiveReferenceForAccount.Account
  }
}