package gw.archiving

uses java.lang.IllegalArgumentException

/**
 * Factory class for creating archive references on
 * on {@link Invoice}s that are frozen during archiving.
 */
@Export
class InvoiceArchiveReferenceFactory {

  function createArchiveReferences(policyPeriod : PolicyPeriod) {
    for (invoiceID in policyPeriod.FreezeSet.invoicesToBeFrozen()) {
      var invoice = policyPeriod.Bundle.loadBean(invoiceID) as Invoice
      if (invoice.FreezingNow) {
        createArchiveReference(invoice)
      }
    }
  }

  private function createArchiveReference(invoice : Invoice) {
    var invoiceArchiveReference = new InvoiceArchiveReference(invoice.Currency)
    invoiceArchiveReference.Invoice = invoice

    var invoiceItemsByTAccountOwner = invoice.InvoiceItems.partition(\ item -> item.Charge.TAccountOwner)
    for (taccountOwner in invoiceItemsByTAccountOwner.keySet()) {
      var invoiceArchiveReferenceByChargeOwner = createTypedInvoiceArchiveReferenceByChargeOwner(invoice, taccountOwner)
      invoiceArchiveReferenceByChargeOwner.InvoiceArchiveReference = invoiceArchiveReference
      setSumsFromInvoiceItems(invoiceArchiveReferenceByChargeOwner, invoiceItemsByTAccountOwner.get(taccountOwner))
    }
  }

  private function createTypedInvoiceArchiveReferenceByChargeOwner(invoice: Invoice, taccountOwner: TAccountOwner) : InvoiceArchiveReferenceByChargeOwner {
    switch (taccountOwner.IntrinsicType) {
      case Account.Type :
        var invoiceArchiveReferenceForAccount = new InvoiceArchiveReferenceForAccount(invoice, invoice.Currency)
        invoiceArchiveReferenceForAccount.Account = taccountOwner as Account
        return invoiceArchiveReferenceForAccount

      case Collateral.Type :
        var invoiceArchiveReference = new InvoiceArchiveReferenceForCollateral(invoice, invoice.Currency)
        invoiceArchiveReference.Collateral = taccountOwner as Collateral
        return invoiceArchiveReference

      case CollateralRequirement.Type :
        var invoiceArchiveReference = new InvoiceArchiveReferenceForCollateralRequirement(invoice, invoice.Currency)
        invoiceArchiveReference.CollateralRequirement = taccountOwner as CollateralRequirement
        return invoiceArchiveReference

      case PolicyPeriod.Type :
        var invoiceArchiveReference = new InvoiceArchiveReferenceForPolicyPeriod(invoice, invoice.Currency)
        invoiceArchiveReference.PolicyPeriod = taccountOwner as PolicyPeriod
        return invoiceArchiveReference
    }
    throw new IllegalArgumentException(
        "T-account owner of type " +
            taccountOwner.IntrinsicType + " with name " + taccountOwner.DisplayName +
            " is not a recognized charge t-account owner")

  }

  private function setSumsFromInvoiceItems(invoiceArchiveReference: InvoiceArchiveReferenceByChargeOwner, invoiceItems: List<InvoiceItem>) {
    var invoiceCurrency = invoiceArchiveReference.Currency
    invoiceArchiveReference.Gross = invoiceItems.sum(invoiceCurrency, \ item -> item.Amount)
    invoiceArchiveReference.GrossPaid = invoiceItems.sum(invoiceCurrency, \ item -> item.PaidAmount)
    invoiceArchiveReference.GrossWrittenOff = invoiceItems.sum(invoiceCurrency, \ item -> item.GrossAmountWrittenOff)
    var itemCommissions = invoiceItems.map( \ item -> item.ActivePrimaryItemCommission).where( \ itemCommission -> itemCommission != null)
    invoiceArchiveReference.Commission = itemCommissions.sum(invoiceCurrency, \ itemCommission -> itemCommission.CommissionAmount)
    invoiceArchiveReference.CommissionApplied = itemCommissions.sum(invoiceCurrency, \ itemCommission -> itemCommission.DirectBillEarned.add(itemCommission.AgencyBillRetained))
    invoiceArchiveReference.CommissionWrittenOff = itemCommissions.sum(invoiceCurrency, \ itemCommission -> itemCommission.WrittenOffCommission)
  }

}