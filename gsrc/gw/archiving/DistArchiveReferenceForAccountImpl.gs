package gw.archiving

@Export
class DistArchiveReferenceForAccountImpl implements ArchiveReferenceByChargeOwner {
  var _distArchiveReferenceForAccount : DistArchiveReferenceForAccount

  construct(distArchiveReferenceForAccount: DistArchiveReferenceForAccount) {
      _distArchiveReferenceForAccount = distArchiveReferenceForAccount
  }

  override property get ChargeTAccountOwner() : TAccountOwner {
    return _distArchiveReferenceForAccount.Account
  }
}