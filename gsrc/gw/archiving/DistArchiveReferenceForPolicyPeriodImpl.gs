package gw.archiving

@Export
class DistArchiveReferenceForPolicyPeriodImpl implements ArchiveReferenceByChargeOwner {
  var _distArchiveReferenceForPolicyPeriod: DistArchiveReferenceForPolicyPeriod

  construct(distArchiveReferenceForPolicyPeriod: DistArchiveReferenceForPolicyPeriod) {
      _distArchiveReferenceForPolicyPeriod = distArchiveReferenceForPolicyPeriod
  }

  override property get ChargeTAccountOwner() : TAccountOwner {
    return _distArchiveReferenceForPolicyPeriod.PolicyPeriod
  }
}