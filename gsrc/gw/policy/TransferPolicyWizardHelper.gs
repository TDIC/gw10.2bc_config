package gw.policy

uses com.google.common.collect.Lists
uses java.util.List

@Export
class TransferPolicyWizardHelper {

  private var _policyTransfer : PolicyTransfer;
  private var _destProducer : Producer;

  construct(policyTransfer : PolicyTransfer) {
    _policyTransfer = policyTransfer;
  }

  property get DestinationProducer() : Producer {
    return _destProducer != null ? _destProducer : _policyTransfer.DestinationProducerCode.Producer;
  }

  property set DestinationProducer(producer : Producer) {
    _destProducer = producer;
  }

  public function autoSelectSoleProducerCode() {
    var activeProducerCodes = DestinationProducer.ActiveProducerCodes;
    if (activeProducerCodes.length == 1) {
      _policyTransfer.DestinationProducerCode = activeProducerCodes[0];
    }
  }

  public property get CommissionTransferOptions() : List<CommissionTransferOption> {
    var commissionTransferOptions = CommissionTransferOption.getTypeKeys(false)
    if (_policyTransfer.PolicyPeriodsToTransfer*.InvoiceItems.countWhere( \ elt -> elt.Frozen) == 0) {
      return commissionTransferOptions
    }
    else {
      var modifiableCommissionTransferOptions = commissionTransferOptions.copy() // getTypeKeys returns Unmodifiable collection
      modifiableCommissionTransferOptions.remove(CommissionTransferOption.TC_RETROACTIVE)
      return modifiableCommissionTransferOptions
    }
  }
}