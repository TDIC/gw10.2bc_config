package gw.payment

uses gw.agencybill.AgencyDistributionWizardHelper
uses gw.api.web.PebblesUtil
uses pcf.AgencyDistributionWizard

@Export
enhancement BaseSuspDistItemEnhancement: BaseSuspDistItem {
  /**
   * Send the user into the Agency Bill Payment/Promise Wizard for the distribution of this agency suspense item
   * and push the user to the Suspense Items popup from within the wizard. This function
   * does not execute the release of a suspense item but instead is just a UI bridge to get the user into
   * the wizard and onto the Suspense Items popup.
   */
  function goToSuspenseItemPopup() {
    var location = AgencyDistributionWizard.go(this.BaseDist.BaseMoneyReceived.Payer as Producer, this.BaseDist.BaseMoneyReceived)
    if (location typeis pcf.api.Wizard) {
      var wizard = location as pcf.api.Wizard

      PebblesUtil.goToWizardStep(wizard, "Distribution")
      var wizardState = PebblesUtil.getWizardVariable(wizard, "wizardState") as AgencyDistributionWizardHelper
      var newDistribution = wizardState.AgencyCycleDistView.AgencyCycleDist
      pcf.AgencySuspenseItemsPopup.push(newDistribution, true)
    }
  }
}
