package gw.plugin.account.impl

uses gw.plugin.account.IAutomaticDisbursement

@Export
class AutomaticDisbursement implements IAutomaticDisbursement {
  construct() {
  }

  override function shouldHold(account: Account) : boolean  {
    return false
  }

  override function shouldHold(unappliedFund : UnappliedFund) : boolean  {
/*    var retval = false;

    // Do not disburse cash under the following circumstances:
    //    Policy is canceled and final audit hasn't occurred.
    //    Policy is expired and final audit hasn't occurred.
    var policyPeriod = unappliedFund.PolicyPeriod_TDIC;
    if (policyPeriod != null) {
      if (policyPeriod.ClosureStatus == PolicyClosureStatus.TC_OPENLOCKED) {
        if (policyPeriod.Canceled) {
          retval = true;
        } else {
          var today = Date.Today;
          if (policyPeriod.PolicyPerExpirDate <= today) {
            retval = true;
          }
        }
      }
    }*/

    return false
  }
}