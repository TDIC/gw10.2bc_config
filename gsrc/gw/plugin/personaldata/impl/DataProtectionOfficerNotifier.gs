package gw.plugin.personaldata.impl

uses gw.api.database.Query
uses gw.api.locale.DisplayKey
uses gw.api.web.admin.ActivityPatternsUtil
uses gw.bc.activity.ActivityFactory
uses gw.pl.persistence.core.Bundle

@Export
class DataProtectionOfficerNotifier {

  private var _dataProtectionOfficers : List<User>
  private var _activityPattern : ActivityPattern
  private var _title : String
  private var _description: String
  private var _errorDate : Date
  private final static var DATA_DESTRUCTION_ACTIVITY_PATTERN = "personal_data_destruction_error"
  private final static var DATA_PROTECTION_OFFICER_PUBLIC_ID = "dp_officer"

  /**
   * DataProtectionOfficerNotifier constructor
   *
   * @param title - The activity title
   * @param message - The message describing the associated entity and the error which prevented the data from being destroyed
   * @param errorDate - The date the error occurred
   */
  construct(title : String, message : String, errorDate : Date) {
    _dataProtectionOfficers = AllDataProtectionOfficers
    _activityPattern = ActivityPatternsUtil.getActivityPattern(DATA_DESTRUCTION_ACTIVITY_PATTERN)
    _title = title
    _description = DisplayKey.get("PersonalData.NotifyProtectionOfficer.MustDestroyError.Description", message)
    _errorDate = errorDate
  }

  public function notify(root: DestructionRootPinnable) {
    if (root typeis UserContact) {
      notify(root)
    } else {
      throw new IllegalArgumentException("Invalid root type '" + root.IntrinsicType.DisplayName)
    }
  }

  private function notify(contact: Contact) {
    createActivityWithNewBundle(\bundle -> ActivityFactory.createContactActivity(bundle, contact, _activityPattern,
        _title, _description, TC_URGENT, false, _errorDate, null))
  }

  private function createActivityWithNewBundle (createActivity : block(bundle : Bundle) : Activity)  {

    // is there are no users associated to the Data Protection Officer role, we need to throw an error
    if (_dataProtectionOfficers.isEmpty()) {
      throw new IllegalStateException(DisplayKey.get("PersonalData.Error.Activity.DataProtectionRoleHasNoUsers"))
    }

    gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
      _dataProtectionOfficers.each(\officer -> {
        var activity = createActivity(bundle)
        assignToDataProtectionOfficer(activity, officer)
      })
    })
  }

  private property get AllDataProtectionOfficers() : List<User> {
    var criteria : UserSearchCriteria
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      criteria = new UserSearchCriteria(bundle)
      var roleQuery = Query.make(entity.Role).compare(Role#PublicID, Equals, DATA_PROTECTION_OFFICER_PUBLIC_ID)
      criteria.Role = roleQuery.select().single()
    })
    return criteria.performSearch().toList() as List<User>
  }

  private function assignToDataProtectionOfficer(activity: Activity, officer: User) {
    var assigned = activity.assignUserAndDefaultGroup(officer)
    if (!assigned) {
      throw new IllegalStateException(DisplayKey.get("PersonalData.Error.Activity.ErrorAssigningActivity", officer.DisplayName))
    }
  }

}