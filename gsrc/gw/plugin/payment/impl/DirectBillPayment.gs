package gw.plugin.payment.impl

uses com.google.common.collect.Lists
uses gw.api.path.Paths
uses gw.api.restriction.RestrictionBuilder
uses gw.api.web.invoice.InvoiceItems
uses gw.api.web.payment.AllocationPool
uses gw.api.web.payment.AllocationStrategy
uses gw.api.web.payment.DBPaymentDistItemGroup
uses gw.api.web.payment.InvoiceItemAllocation
uses gw.api.web.payment.ProRataRedistributionStrategy
uses gw.api.web.payment.ProRataReversedPaymentAmountRedistributionStrategy
uses gw.payment.PaymentAllocationStrategy
uses gw.pl.currency.MonetaryAmount
uses gw.plugin.payment.IDirectBillPayment
uses java.lang.Iterable
uses java.util.ArrayList
uses java.util.Date
uses java.util.HashSet
uses java.util.Map
uses java.util.Set
uses gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange
uses gw.api.domain.invoice.PaymentPlanChanger
uses gw.invoice.InvoiceItemFilter
uses java.util.List
uses org.slf4j.LoggerFactory

@Export
class DirectBillPayment implements IDirectBillPayment {

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("Application.Invoice")

  construct() {}

  override function allocatePayment(payment : DirectBillPayment, amount : MonetaryAmount)  {
    _logger.debug("DirectBillPayment::allocatePayment - Entering - Account " + payment.Account.AccountNumber
                      + ", Amount = " + amount);

    var amountToDistribute = AllocationPool.withGross(amount)
    _logger.debug("DirectBillPayment::allocatePayment - Amount to distribute: " + amountToDistribute.GrossAmount.Amount)

    paymentAllocationStrategy(payment).allocate(payment.DistItemsList, amountToDistribute)
     // TODO: Revisit this piece of code for exiting delinquency.
    // BrianS - If no money was allocated, then check to see if we should exit delinquency.
    // commenting this piece of code to avoid invoiceDue batch errors. We shouldn't be exiting delinquency unless the allocation happens.
   /* if (payment.DistItemsList.Empty) {
      var policyDelinquencyUtil : PolicyDelinquencyUtil;
      for (policy in payment.Account.Policies) {
        policyDelinquencyUtil = new PolicyDelinquencyUtil (policy);
        policyDelinquencyUtil.exitDelinquencies();
      }
    }*/

    _logger.debug("DirectBillPayment::allocatePayment - Exiting")
  }

  override function allocateCollateral(payment : DirectBillPayment, amount : MonetaryAmount) {
    _logger.debug("DirectBillPayment::allocateCollateral - Entering - Account " + payment.Account.AccountNumber
                    + ", Amount = " + amount);
    var amountToDistribute = AllocationPool.withGross(amount)
    paymentAllocationStrategy(payment).allocate(payment.DistItemsList, amountToDistribute)
  }

  override function allocateWithOverrides(payment : DirectBillPayment, amount : MonetaryAmount,
                                          overrideItemsAndAmounts : Map<InvoiceItem, MonetaryAmount>) {
    _logger.debug("DirectBillPayment::allocateWithOverrides (overrideItemsAndAmounts) - Entering - Account "
                    + payment.Account.AccountNumber + ", Amount = " + amount);

    var eligibleItems = payment.DistItemsList
    var overrideInvoiceItems = overrideItemsAndAmounts.keySet()
    var notOverriddenItems = eligibleItems.where(\ b -> !overrideInvoiceItems.contains(b.InvoiceItem))
    var amountAllocatedByOverride = overrideItemsAndAmounts.Values.sum(amount.Currency, \ b -> b)

    var leftoverAmount = amount.subtract(amountAllocatedByOverride)
    var amountToDistribute = AllocationPool.withGross(leftoverAmount.IsNegative ? 0bd.ofCurrency(amount.Currency) : leftoverAmount)
    paymentAllocationStrategy(payment).allocate(notOverriddenItems, amountToDistribute)
  }

  override function allocateWithOverrides(payment : DirectBillPayment, amount : MonetaryAmount,
                                          groupsToDistribute : List<DBPaymentDistItemGroup>) {
    _logger.debug("DirectBillPayment::allocateWithOverrides (groupsToDistribute) - Entering - Account "
                    + payment.Account.AccountNumber + ", Amount = " + amount);
    var itemsNotOverridden = new ArrayList<BaseDistItem>()

    var frozenAmount = groupsToDistribute.where( \ group -> group.hasfrozenDistItem())
                                          .sum(payment.Currency, \ group -> group.OverrideAmount)
    var leftoverAmount = amount.subtract(frozenAmount)
    groupsToDistribute = groupsToDistribute.where( \ group -> !group.hasfrozenDistItem())
    var amountToDistribute = AllocationPool.withGross(0bd.ofCurrency(payment.Currency));

    for (var group in groupsToDistribute) {
      if (group.OverrideAmount == null) {
        itemsNotOverridden.addAll(group.DistItems)
      } else {
        amountToDistribute.setGrossAmount(group.OverrideAmount)
        paymentAllocationStrategy(payment).allocate(group.DistItems, amountToDistribute)
        leftoverAmount -= group.OverrideAmount
      }
    }
    if (!itemsNotOverridden.Empty){
      amountToDistribute.setGrossAmount(leftoverAmount.IsNegative ? 0bd.ofCurrency(amount.Currency) : leftoverAmount)
      paymentAllocationStrategy(payment).allocate(itemsNotOverridden, amountToDistribute)
    }
  }
  /**
   *  GW2265 Negative Unapplied. Modify function to include GW workaround for this issue (BC-11398). Issue addressed in version 8.0.5
   *  Hermia Kho - 8/19/2016
   */
  override function allocatePolicyPeriod(payment : DirectBillPayment, amount : MonetaryAmount,
                                         otherEligiblePayerInvoiceItems : Set<InvoiceItem>) {
    _logger.debug("DirectBillPayment::allocatePolicyPeriod - Entering - Account " + payment.Account.AccountNumber
                      + ", Amount = " + amount);
    var min = amount.min(payment.DirectBillMoneyRcvd.UnappliedFund.Balance)
    // Adding this if clause to fix the invoice batch error.
    //java.lang.IllegalArgumentException: Amount to allocate cannot be negative.
    if (min.IsNegative) {min = 0bd.ofDefaultCurrency()}
    var amountToDistribute = AllocationPool.withGross(min)
    payment.addInvoiceItems(otherEligiblePayerInvoiceItems)

    _logger.debug("DirectBillPayment::allocatePolicyPeriod - Final Distribution item list: ")
    if (_logger.DebugEnabled) {
      for(item in payment.DistItemsList){
        _logger.debug("DirectBillPayment::allocatePolicyPeriod -  Item: " +item.InvoiceItem)
      }
    }

    paymentAllocationStrategy(payment).allocate(payment.DistItemsList, amountToDistribute)
    _logger.debug("DirectBillPayment::allocatePolicyPeriod - Exiting");
  }

  override function allocateForRedistribution(payment : DirectBillPayment, amount : MonetaryAmount) {
    _logger.debug("DirectBillPayment::allocateForRedistribution - Entering - Account "
                      + payment.Account.AccountNumber + ", Amount = " + amount);
    var amountToDistribute = AllocationPool.withGross(amount)
    new ProRataRedistributionStrategy().allocate(payment.DistItemsList, amountToDistribute)
  }

  override function allocateCredits(payment : DirectBillPayment) {
    _logger.debug("DirectBillPayment::allocateCredits - Entering - Account " + payment.Account.AccountNumber);

    var distItems = payment.DistItemsList
    var negativeDistItems = distItems.where(\distItem -> distItem.InvoiceItem.Amount.IsNegative)
    if (negativeDistItems.Empty) {
      return
    }

    var amountToAllocate = negativeDistItems
        .sum(payment.Currency, \ elt -> elt.GrossAmountOwed - elt.GrossAmountToApply)
        .negate()
    _logger.debug ("DirectBillPayment::allocateCredits - amountToAllocate = " + amountToAllocate);
    if (amountToAllocate.IsZero) {
      return
    }

    var creditScheme : ReturnPremiumHandlingScheme
    var policyPeriod = negativeDistItems.first().PolicyPeriod
    if (policyPeriod != null) {
      var negativeInvoiceItems = Lists.newArrayList(negativeDistItems*.InvoiceItem)
      creditScheme = policyPeriod.ReturnPremiumPlan.getReturnPremiumHandlingSchemeFor(negativeInvoiceItems)
      _logger.debug("DirectBillPayment::allocateCredits - ReturnPremiumPlan = " + creditScheme.ReturnPremiumPlan.Name);
      _logger.debug("DirectBillPayment::allocateCredits - creditScheme.Context = " + creditScheme.HandlingCondition);
      _logger.debug("DirectBillPayment::allocateCredits - creditScheme.StartDate = " + creditScheme.StartDateOption);
      _logger.debug("DirectBillPayment::allocateCredits - creditScheme.AllocateTiming = " + creditScheme.AllocateTiming);
      _logger.debug("DirectBillPayment::allocateCredits - creditScheme.Method = " + creditScheme.AllocateMethod);

      for (negativeDistItem in negativeDistItems) {
        _logger.debug("DirectBillPayment::allocateCredits - NegativeDistItem - GrossAmountOwed = "
            + negativeDistItem.GrossAmountOwed + ", GrossAmountToApply = "
            + negativeDistItem.GrossAmountToApply + ", Distributed = " + negativeDistItem.Distributed);
      }

    }

    if (creditScheme == null) {
      // Account or Collateral negative items.
      paymentAllocationStrategy(payment).allocate(distItems, AllocationPool.withGross(amountToAllocate))
    } else {
      creditScheme.allocate(distItems, AllocationPool.withGross(amountToAllocate))
    }

    // Pay all negative items in full.
    negativeDistItems.each(\ item -> {item.GrossAmountToApply = item.GrossAmountOwed})

    // BrianS - Set distribution context for delinquency cancels
    var isDelinquencyCancel = isPaymentForDeliquencyCancel(payment);
    var distributionContext = payment.DirectBillMoneyRcvd.DBPmntDistributionContext;
    if (isDelinquencyCancel == true) {
      for (distItem in distItems) {
        if (distItem typeis DirectBillPaymentItem) {
          distItem.DBPmntDistributionContext_TDIC = distributionContext;
        }
      }
    }

    _logger.debug("DirectBillPayment::allocateCredits - Exiting")
  }

  /**
   * Determine if the payment is for a delinquency cancel, by examining the bundle associated with the
   * payment being processed.
  */
  protected function isPaymentForDeliquencyCancel (payment : DirectBillPayment) : boolean {
    var retval = false;
    var bundle = payment.Bundle;

    var cancellation = bundle.InsertedBeans.whereTypeIs(Cancellation).first();

    if (cancellation != null) {
      retval = cancellation.IsDelinquencyCancel;
    }

    return retval;
  }

  override function allocateFromUnapplied(payment: DirectBillPayment, amount: MonetaryAmount) {
    _logger.debug("DirectBillPayment::allocateFromUnapplied - Entering - Account " + payment.Account.AccountNumber
                    + ", Amount = " + amount);
    paymentAllocationStrategy(payment).allocate(payment.DistItemsList, AllocationPool.withGross(amount))
  }

  private function isContextBilled(moneyRcvd : DirectBillMoneyRcvd) : boolean {
    return moneyRcvd.DBPmntDistributionContext == DBPmntDistributionContext.TC_INVOICEBILLED
  }

  private function isContextDue(moneyRcvd : DirectBillMoneyRcvd) : boolean {
    return moneyRcvd.DBPmntDistributionContext == DBPmntDistributionContext.TC_INVOICEDUE
  }

  override function allocateWriteoffs(writeoff : ChargeGrossWriteoff) : List<InvoiceItemAllocation> {
    _logger.debug("DirectBillPayment::allocateWriteoffs - Entering");
    return gw.api.web.invoice.InvoiceItems.proRataAllocation(writeoff.EligibleInvoiceItems, writeoff.Amount)
  }

  override function allocateForRedistribution(payment : DirectBillPayment, amountToChargeMap: Map <Charge, AllocationPool>) {
    // BrianS - Function modified from OOTB based upon Knowledge Base article "Payment Plan Factoring with Return
    //          Premium".  TDIC wants the return premium plan to be used when allocating credits during a payment
    //          plan change.

    _logger.debug("DirectBillPayment::allocateForRedistribution (amountToChargeMap) - Entering - Account " + payment.Account.AccountNumber);

    var distItems = payment.DistItemsList
    var negativeDistItems = distItems.where(\distItem -> distItem.InvoiceItem.Amount.IsNegative)
    if (negativeDistItems.Empty) {
      // Original Code
      var distributionStrategy = new ProRataReversedPaymentAmountRedistributionStrategy()
      var charges = amountToChargeMap.keySet()
      for (var charge in charges) {
        var amountToDistribute = amountToChargeMap.get(charge)
        var invoiceItems = new HashSet<InvoiceItem>(InvoiceItems.withoutOffsetsOrCommissionRemainderOrFrozen(charge.InvoiceItems)
            // the below is to get only items matching the allocation amount in gross unless zero as the strategy depends on this
            .where( \ invoiceItem -> distributionStrategy.canRedistributeDirectBillPaymentToInvoiceItem(invoiceItem, null, amountToDistribute)))
        distributionStrategy.allocate(payment.getDistItemsFor(invoiceItems), amountToDistribute)
      }
    } else {
      var amountToAllocate = negativeDistItems
          .sum(payment.Currency, \ elt -> elt.GrossAmountOwed - elt.GrossAmountToApply)
          .negate()
      if (amountToAllocate.IsZero) {
        return
      }
      var creditScheme : ReturnPremiumHandlingScheme
      var policyPeriod = negativeDistItems.first().PolicyPeriod
      if (policyPeriod != null) {
        var negativeInvoiceItems = Lists.newArrayList(negativeDistItems*.InvoiceItem)
        creditScheme = policyPeriod.ReturnPremiumPlan.getReturnPremiumHandlingSchemeFor(negativeInvoiceItems)
      }
      if (creditScheme == null) {
        // Account or Collateral negative items.
        paymentAllocationStrategy(payment).allocate(distItems, AllocationPool.withGross(amountToAllocate))
      } else {
        creditScheme.allocate(distItems, AllocationPool.withGross(amountToAllocate))
      }
      // Pay all negative items in full.
      negativeDistItems.each(\ item -> {item.GrossAmountToApply = item.GrossAmountOwed})
    }
  }

  override function addPositiveItemsDistributionCriteria(negativeInvoiceItems : Iterable<InvoiceItem>) : RestrictionBuilder<InvoiceItem> {
    final var representativeItem = negativeInvoiceItems.first()
    final var returnPremiumCharge = representativeItem.Charge
    var restrictions = RestrictionBuilder.make(InvoiceItem)

    final var chargeTargetQualification = returnPremiumCharge.PolicyPeriod.ReturnPremiumPlan.ChargeQualification
    final var returnPremiumHandling = returnPremiumCharge.PolicyPeriod.ReturnPremiumPlan.getReturnPremiumHandlingSchemeFor(negativeInvoiceItems)

    final var itemEffectiveDate = lookupPositiveItemEffectiveDateCriteriaValue(returnPremiumHandling, returnPremiumCharge)
    if (itemEffectiveDate != null) {
      restrictions.compare(Paths.make(InvoiceItem#EventDate), GreaterThanOrEquals, itemEffectiveDate)
    }

    if (chargeTargetQualification == TC_ACCOUNT) {
      return restrictions
    }
    if (chargeTargetQualification == TC_POLICY) {
      restrictions.compare(Paths.make(
          InvoiceItem#PolicyPeriod, PolicyPeriod#Policy),
          Equals, representativeItem.PolicyPeriod.Policy)
    } else {
      restrictions.compare(Paths.make(
          InvoiceItem#PolicyPeriod),
          Equals, representativeItem.PolicyPeriod)
      if (chargeTargetQualification == TC_CHARGEPATTERN) {
        restrictions.compare(Paths.make(
            InvoiceItem#Charge, Charge#ChargePattern),
            Equals, returnPremiumCharge.ChargePattern)
      } else if (chargeTargetQualification == TC_CHARGECATEGORY) {
        restrictions.compare(Paths.make(
            InvoiceItem#Charge, Charge#ChargePattern, ChargePattern#Category),
            Equals, returnPremiumCharge.ChargePattern.Category)
      } else if (chargeTargetQualification == TC_CHARGEGROUP) {
        restrictions.compare(Paths.make(
            InvoiceItem#Charge, Charge#ChargeGroup),
            Equals, returnPremiumCharge.ChargeGroup)
      }
    }

    return restrictions
  }

  /**
   * @param the return premium charge from which to determine the positive item effective date
   * @return the positive item effective date criteria value, or null to indicate no date restriction
   */
  private function lookupPositiveItemEffectiveDateCriteriaValue(returnPremiumHandling: ReturnPremiumHandlingScheme, charge : Charge) : Date {
    if (returnPremiumHandling.StartDateOption == TC_CHARGEEFFECTIVEDATE) {
      // Don't filter positive items by date if charge effective date is same as policy effective date
      return charge.EffectiveDate.isSameDayIgnoringTime(charge.PolicyPeriod.EffectiveDate) ? null : charge.EffectiveDate
    }
    return null
  }

  override function addInvoiceItemsDistributionCriteria(moneyRcvd: DirectBillMoneyRcvd): RestrictionBuilder<InvoiceItem> {
    var unappliedFundRestrictionBuilder = makeUnappliedFundRestrictionBuilder(moneyRcvd)

    return addCommonRestrictions(moneyRcvd, unappliedFundRestrictionBuilder)
        .union(addCommonRestrictionsWithoutDistributionLimits(moneyRcvd,
            makePremiumReportBIRestrictionBuilder()))
    // 4/2/2015 Alvin Lee - Comment out collateral for payment distribution performance
            //.union(makeCollateralItemsRestrictionBuilder())
            //.union(makeCollateralRequirementItemsRestrictionBuilder()))
  }

  override function addInvoiceItemsDistributionCriteriaForUnappliedFunds(zeroDollarDMR : ZeroDollarDMR): RestrictionBuilder<InvoiceItem> {
    return addCommonRestrictions(zeroDollarDMR,
        makeUnappliedFundRestrictionBuilder(zeroDollarDMR))
        .union(addCommonRestrictionsWithoutDistributionLimits(zeroDollarDMR,
            makePremiumReportBIRestrictionBuilder()))
    // 4/2/2015 Alvin Lee - Comment out collateral for payment distribution performance
            //.union(makeCollateralItemsRestrictionBuilder())
            //.union(makeCollateralRequirementItemsRestrictionBuilder()))
  }

  override function addInvoiceItemsDistributionCriteriaForAccountCredits(
      account : Account, negativeInvoiceItems: Iterable<InvoiceItem>)
      : RestrictionBuilder<InvoiceItem> {
    return addCommonRestrictionsWithoutDistributionLimits(null, account.PaymentAllocationPlan,
        makeAccountCreditsDistributionRestrictionBuilder()
            .union(makePastDueRestrictionBuilder())
            .union(makePremiumReportBIRestrictionBuilder()))
    // 4/2/2015 Alvin Lee - Comment out collateral for payment distribution performance
            //.union(makeCollateralItemsRestrictionBuilder())
            //.union(makeCollateralRequirementItemsRestrictionBuilder()))
  }

  private function makeUnappliedFundRestrictionBuilder(moneyRcvd : DirectBillMoneyRcvd) : RestrictionBuilder<InvoiceItem> {
    var restrictions = makeDistributionRestriction()
    addNotHeldRestriction(restrictions)

    return restrictions
        .union(makePastDueRestrictionBuilder())
  }

  // Account credits ignore distribution limit filters.
  private function
      makeAccountCreditsDistributionRestrictionBuilder() : RestrictionBuilder<InvoiceItem> {
    var restrictions = makeDistributionRestriction()
    addNotHeldRestriction(restrictions)
    return restrictions
  }

  private function addNotHeldRestriction(restrictions : RestrictionBuilder<InvoiceItem>) {
    restrictions.compareSet(Paths.make(InvoiceItem#Charge, Charge#HoldStatus), CompareIn, {null, TC_NONE})
  }

  private function makePastDueRestrictionBuilder() : RestrictionBuilder<InvoiceItem> {
    var restrictions = makeDistributionRestriction()
    restrictions.compare(Paths.make(InvoiceItem#Invoice, entity.Invoice#Status), Equals, TC_DUE)
    return restrictions
  }

  /**
   * 4/2/2015 Alvin Lee
   *
   * Since collateral is not being used at TDIC, comment out OOTB function for payment distribution performance
   * purposes, as described by the Guidewire Resource Portal article:
   * https://guidewire.custhelp.com/app/answers/detail/a_id/3103/kw/3103
   */
/*
  private function makeCollateralItemsRestrictionBuilder() : RestrictionBuilder<InvoiceItem> {
    var restrictions = makeDistributionRestriction()
    restrictions.compare(Paths.make(
        InvoiceItem#Charge, Charge#TAccountContainer, ChargeTAcctContainer#Subtype),
        Equals, TC_COLLTACCTCONTAINER)
    addNotHeldRestriction(restrictions)
    return restrictions
  }
*/

  /**
   * 4/2/2015 Alvin Lee
   *
   * Since collateral is not being used at TDIC, comment out OOTB function for payment distribution performance
   * purposes, as described by the Guidewire Resource Portal article:
   * https://guidewire.custhelp.com/app/answers/detail/a_id/3103/kw/3103
   */
/*
  private function makeCollateralRequirementItemsRestrictionBuilder() : RestrictionBuilder<InvoiceItem> {
    var restrictions = makeDistributionRestriction()
    restrictions.compare(Paths.make(
        InvoiceItem#Charge, Charge#TAccountContainer, ChargeTAcctContainer#Subtype),
        Equals, TC_COLLREQTACCTCONTAINER)
    addNotHeldRestriction(restrictions)
    return restrictions
  }
*/

  private function makePremiumReportBIRestrictionBuilder() : RestrictionBuilder<InvoiceItem> {
    var restrictions = makeDistributionRestriction()
    restrictions.compare(Paths.make(
        InvoiceItem#Charge, Charge#BillingInstruction, PremiumReportBI#PaymentReceived),
        Equals, true)
    return restrictions
  }

  private function addCommonRestrictions(
      moneyRcvd : DirectBillMoneyRcvd, restriction : RestrictionBuilder<InvoiceItem>)
      : RestrictionBuilder<InvoiceItem> {
    addCommonRestrictions(moneyRcvd, moneyRcvd.Account.PaymentAllocationPlan, restriction)
    restriction.compare(Paths.make(InvoiceItem#Invoice, AccountInvoice#InvoiceStream, InvoiceStream#UnappliedFund),
        Equals, moneyRcvd.UnappliedFund)
    return restriction
  }

  private function addCommonRestrictionsWithoutDistributionLimits(
      moneyRcvd : DirectBillMoneyRcvd,
      restriction : RestrictionBuilder<InvoiceItem>) : RestrictionBuilder<InvoiceItem> {
    addCommonRestrictionsWithoutDistributionLimits(moneyRcvd, moneyRcvd.Account.PaymentAllocationPlan, restriction)
    restriction.compare(Paths.make(InvoiceItem#Invoice, AccountInvoice#InvoiceStream, InvoiceStream#UnappliedFund),
        Equals, moneyRcvd.UnappliedFund)
    return restriction
  }

  private function addCommonRestrictions(moneyRcvd : DirectBillMoneyRcvd,
                                         account: Account, restriction : RestrictionBuilder<InvoiceItem>) : RestrictionBuilder<InvoiceItem> {
    // Restricting to invoice items for the account is done by the caller.
    return addCommonRestrictions(moneyRcvd, account.PaymentAllocationPlan, restriction)
  }

  private function makeDistributionRestriction() : RestrictionBuilder<InvoiceItem> {
    return RestrictionBuilder.make(InvoiceItem)
  }

  private function addCommonRestrictions(final moneyRcvd : DirectBillMoneyRcvd,
                                         final paymentAllocationPlan : PaymentAllocationPlan,
                                         final restrictions : RestrictionBuilder<InvoiceItem>)
      : RestrictionBuilder<InvoiceItem> {
    paymentAllocationPlan.applyDistributionCriteriaFilters(restrictions, moneyRcvd)
    return restrictions
  }

  private function addCommonRestrictionsWithoutDistributionLimits(final moneyRcvd : DirectBillMoneyRcvd,
                                                                  final paymentAllocationPlan : PaymentAllocationPlan,
                                                                  final restrictions : RestrictionBuilder<InvoiceItem>)
      : RestrictionBuilder<InvoiceItem> {
    paymentAllocationPlan.applyDistributionCriteriaFiltersWithoutDistributionLimits(restrictions, moneyRcvd)
    return restrictions
  }

  private function
  paymentAllocationStrategy(directBillPayment : DirectBillPayment) : AllocationStrategy {
    return new PaymentAllocationStrategy(
        directBillPayment.Account.PaymentAllocationPlan, directBillPayment.DirectBillMoneyRcvd)
  }

  /**
   * Get the payment plan that corresponds to the payment amount based upon the amount in the
   * offered payment plans.
   */
  // TODO : Revisit this method.
  static public function getNewPaymentPlan_TDIC (policyPeriod : PolicyPeriod,
                                                 paymentAmount : MonetaryAmount) : OfferedPaymentPlan_TDIC {
    var retval : OfferedPaymentPlan_TDIC;

    _logger.trace ("DirectBillPayment.getNewPaymentPlan - Entering");

    for (paymentPlan in policyPeriod.OfferedPaymentPlans_TDIC.orderBy(\opp -> opp.DownPayment)
                                                             .thenByDescending(\opp -> opp.PaymentPlan.PlanOrder)) {
      _logger.trace (paymentPlan.PaymentPlan.Name + " - Down Payment = " + paymentPlan.DownPayment);
      if (paymentAmount >= paymentPlan.DownPayment) {
        retval = paymentPlan;
      } else {
        break;
      }
    }

    _logger.trace ("New Payment Plan = " + retval.PaymentPlan);
    _logger.trace ("DirectBillPayment.getNewPaymentPlan - Exiting");

    return retval;
  }

  /**
   * Change the payment plan on the specified policy period.
   */
  // TODO : Revisit this method.
  static public function changePaymentPlan_TDIC (policyPeriod : PolicyPeriod, newPaymentPlan : PaymentPlan) : void {
    _logger.debug ("Changing payment plan on policy " + policyPeriod.PolicyNumberLong + " from "
                      + policyPeriod.PaymentPlan.Name + " to " + newPaymentPlan.Name);

    // Logic from OOTB ChangePaymentPlanPopup.pcf

    // Redistribute Payments - Method for handling existing payments.
    //    ReverseAndRedistribute - reverses the payments and redistributes them
    //    ReverseOnly - reverses the payments, but does not redistribute them
    var redistributePayments = PaymentHandlingDuringPaymentPlanChange.ReverseAndRedistribute;

    // Invoice Item Filter Type - Type of invoice items to process
    //    allitems - processes all the invoice items associated with the policy
    //    planneditems - processes only invoice items that have not yet been invoiced
    //    notfullypaiditems - processes only invoice items that have not yet been paid in full
    var itemFilterType = InvoiceItemFilterType.TC_ALLITEMS;

    // Include Down Payment Items - If an invoice item type other than All Items is selected, the Include Down Payment
    // Items checkbox can be checked to include down payment invoice items in the recalculation process. If the down
    // payment was previously billed under the original payment plan, that charge is reversed and a new invoice item is
    // created according to the new payment plan.
    var includeDownPaymentItems = true;

    var changer = new PaymentPlanChanger (policyPeriod, newPaymentPlan, redistributePayments,
                                          InvoiceItemFilter.getFilter(itemFilterType, includeDownPaymentItems));
    changer.execute();
  }

  /**
   * Create an activity for an offered payment mismatch.
  */
  /*static public function createOfferedPaymentPlanMismatchActivity_TDIC (policyPeriod : PolicyPeriod,
                                                                        newPaymentPlan : OfferedPaymentPlan_TDIC,
                                                                        amount : MonetaryAmount) : void {
    var description : String;

    _logger.info ("Creating Offered Payment Plan Mismatch Activity");

    if (newPaymentPlan != null) {
      description = DisplayKey.get("TDIC.Activity.OfferedPaymentPlanMismatch_TDIC.Description", amount.DisplayValue, newPaymentPlan.DownPayment.DisplayValue, newPaymentPlan.PaymentPlan.Name);
    } else {
      description = DisplayKey.get("TDIC.Activity.OfferedPaymentPlanMismatch_TDIC.Description.NoMatch", amount.DisplayValue);
    }

    var activityPattern = ActivityPattern.OfferedPaymentPlanMismatchActivity_TDIC;
    var activity = policyPeriod.createActivity(activityPattern, activityPattern.Subject, description,
                                               null *//* target date*//*, null *//* escalation date *//*, true *//* mandatory *//*);
    activity.PolicyPeriod = policyPeriod;
    activity.Account = policyPeriod.Account;
    activity.autoAssign();
  }*/
}