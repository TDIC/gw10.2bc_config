package gw.api.web.admin

@Export
class BCGroupUtil extends BCGroupUtilBase {

  /**
   * Never instantiated
   */
  private construct() {
    super()
  }

  static function createGroup() : Group {
    var newGroup = BCGroupUtilBase.createGroup()
    newGroup.setGroupType(GroupType.TC_GENERAL)
    return newGroup
  }
}