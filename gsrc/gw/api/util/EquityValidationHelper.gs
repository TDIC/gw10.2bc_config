package gw.api.util

uses gw.api.domain.invoice.ChargeInstallmentChanger
uses gw.api.domain.invoice.PaymentPlanChanger
uses gw.api.financials.EquityValidationUtility
uses gw.api.locale.DisplayKey
uses gw.api.system.BCConfigParameters
uses gw.api.web.account.Policies
uses gw.pl.currency.MonetaryAmount
uses gw.util.Pair

uses java.util.ArrayList
uses java.util.Arrays
uses java.util.Date
uses java.util.List

@Export
class EquityValidationHelper {
  /**
   * Validates whether the given changes will violate equity and if so returns a prompt message
   *
   * @param policyPeriod the period we are validating the changes on
   * @param changer staged changes we are validating
   * @return A prompt message if there is a violation
   */
  static function isEquityViolated(policyPeriod : PolicyPeriod, changer : ChargeInstallmentChanger) : String {
    if(not shouldEvaluateEquity(policyPeriod)){
      return null
    }

    var equityCharges = policyPeriod.EquityCharges
    if (!equityCharges.contains(changer.Charge)) {
      return null//if the charge we are modifying is not an equity charge, it will have no effect on equity
    }

    var graph = policyPeriod.EarnedPremiumGraph
    var paidAmount = policyPeriod.EquityPaidAmount

    var proposedSchedule = new ArrayList<Pair<Date, MonetaryAmount>>()

    for (var entry in changer.Entries) {
      if (entry.Invoice == null) {
        return null//we don't have a valid set of entries
      }
      proposedSchedule.add(new Pair<Date, MonetaryAmount>(entry.Invoice.DueDate, entry.Amount))
    }

    for (var equityCharge in equityCharges) {
      if (equityCharge == changer.Charge) {
        continue//entries were handled above
      }
      for (var invoiceItem in equityCharge.InvoiceItems) {
        proposedSchedule.add(new Pair<Date, MonetaryAmount>(invoiceItem.InvoiceDueDate, invoiceItem.Amount))
      }
    }

    var warningInfo = EquityValidationUtility.isEquityViolated(graph, policyPeriod.EquityBuffer, paidAmount, proposedSchedule, Now)

    if (warningInfo.hasWarnings()) {
      var firstWarning = warningInfo.FirstWarning
      return DisplayKey.get("Web.InvoiceItemsLV.EquityViolation", firstWarning.Key.AsUIStyle, firstWarning.Value.AsUIStyle, warningInfo.MaxViolation, policyPeriod.EquityBuffer)
    }
    return null
  }

  /**
   * Validates whether the given payment plan changes will violate equity and returns an prompt message if so
   *
   * @param paymentPlanChanger the {@link PaymentPlanChanger} for which to validate equity.
   * @return A prompt message if there is a violation
   */
  static function isEquityViolated(paymentPlanChanger : PaymentPlanChanger) : String {
    final var policyPeriod = paymentPlanChanger.PolicyPeriod

    if (not shouldEvaluateEquity(policyPeriod)) {
      return null
    }

    final var graph = policyPeriod.EarnedPremiumGraph

    if (graph == null) {
      return null// does not violate if no charges or graph...
    }

    var paidAmount = policyPeriod.EquityPaidAmount
    if (!paymentPlanChanger.RedistributePayments) {
      //payments and writeoffs are reversed, so the paid amount is $0
      paidAmount = new MonetaryAmount(0, paidAmount.Currency)
    } else if (BCConfigParameters.EquityDatingIncludeWriteoffs.Value) {
      //if writeoffs are included in equity dating, must subtract them
      final var totalWrittenOff = paymentPlanChanger.InstallmentPreview.reduce(
          new MonetaryAmount(0, paidAmount.Currency), \ writtenOff, entry ->
            entry.InvoiceItem != null
                ? writtenOff.add(entry.InvoiceItem.GrossAmountWrittenOff)
                : writtenOff
      )
      paidAmount = paidAmount.subtract(totalWrittenOff)//we are redistributing payments, but not writeoffs
    }

    final var proposedSchedule = paymentPlanChanger.EquityPreview
        .map(\entry -> new Pair<Date, MonetaryAmount>(entry.Invoice.DueDate, entry.Amount))

    if (EquityValidationUtility.isEquityViolated(graph, policyPeriod.EquityBuffer, paidAmount, proposedSchedule, Now).hasWarnings()){
      return DisplayKey.get("Web.PolicyDetailPayments.EquityWarning", policyPeriod.EquityBuffer)
    }

    return null
  }

  /**
   * This method validates whether the given invoice move will violate equity
   *
   * @param movedItems the invoice items being moved
   * @param destinationInvoice the invoice the items are being moved to
   * @return whether or not there will be a violation
   */
  static function isEquityViolated(movedItems : List<InvoiceItem>, destinationInvoice : Invoice) : boolean {
    if (movedItems == null || destinationInvoice == null || movedItems.Empty) {
      return false // nothing to validate!
    }

    final var policyPeriod = movedItems.first().PolicyPeriod
    if (not shouldEvaluateEquity(policyPeriod)){
      return false
    }

    final var equityCharges = policyPeriod.EquityCharges
    if (!equityCharges.contains(movedItems.first().Charge)) {
      return false//if the charge isn't an equity charge, then the changes don't matter
    }

    var graph = policyPeriod.EarnedPremiumGraph
    var paidAmount = policyPeriod.EquityPaidAmount

    final var proposedSchedule = Arrays.asList(
        equityCharges*.InvoiceItems.map(\ invoiceItem ->
            new Pair<Date, MonetaryAmount>(
                //make sure we are checking the moved items on their destination invoice
                movedItems.contains(invoiceItem)
                    ? destinationInvoice.Date : invoiceItem.InvoiceDueDate,
                invoiceItem.Amount)
        )
    )

    var warningInfo = EquityValidationUtility.isEquityViolated(graph, policyPeriod.EquityBuffer, paidAmount, proposedSchedule, Now)

    return warningInfo.hasWarnings()
  }

  /**
   * Validates whether the given account is the payer for any policy periods
   * that have an equity violation.
   *
   * Policy periods that are archived or that have disabled equity warnings are
   * ignored.
   *
   * @param account the {@code Account} for which equity is being validated
   * @return an error message if there is a violation
   */
  static function isEquityViolatedCheckAllPolicies(account : Account) : String {
    if (account.ListBill) {
      return null
    }

    var errorMessage = ""

    for (var policyPeriod in Policies.findPolicyPeriodsWhereAccountIsOverridingPayer(account, null)) {
      if (shouldEvaluateEquity(policyPeriod)) {
        errorMessage += policyPeriodErrorMessage(policyPeriod, policyPeriod.EquityBuffer)
      }
    }
    for (var policyPeriod in account.AllPolicyPeriods) {
      if(policyPeriod.OverridingPayerAccount == null and shouldEvaluateEquity(policyPeriod)){
        errorMessage += policyPeriodErrorMessage(policyPeriod, policyPeriod.EquityBuffer)
      }
    }

    if (!errorMessage.Empty) {
      return DisplayKey.get("Web.AccountDetailSummary.EquityViolation", errorMessage + "\n\n")
    }

    return null
  }

  static private function policyPeriodErrorMessage(policyPeriod : PolicyPeriod, equityDays : int) : String {
    var errorMessage = ""
    var graph = policyPeriod.EarnedPremiumGraph
    var paidAmount = policyPeriod.EquityPaidAmount

    final var proposedSchedule = Arrays.asList(
      policyPeriod.EquityCharges*.InvoiceItems.map(\ invoiceItem ->
        new Pair<Date, MonetaryAmount>(invoiceItem.InvoiceDueDate, invoiceItem.Amount)
      )
    )

    var warningInfo = EquityValidationUtility.isEquityViolated(graph, equityDays, paidAmount, proposedSchedule, Now)

    if (warningInfo.hasWarnings()) {
      errorMessage += "\n\n"
      var firstWarning = warningInfo.FirstWarning
      errorMessage += DisplayKey.get("Web.AccountDetailSummary.PolicyViolation", policyPeriod.DisplayName, firstWarning.Key.AsUIStyle, firstWarning.Value.AsUIStyle, warningInfo.MaxViolation, equityDays + " ")
    }
    return errorMessage
  }

  private static property get Now() : Date {
    return DateUtil.currentDate()
  }

  /**
   * @param policyPeriod to be evaluated
   * @return true if the policy meets all the conditions to have its equity evaluated
   */
  private static function shouldEvaluateEquity (policyPeriod : PolicyPeriod) : boolean {
    return policyPeriod != null and policyPeriod.EquityWarningsEnabled and
           not policyPeriod.Archived and policyPeriod.EquityCharges.HasElements
  }
}