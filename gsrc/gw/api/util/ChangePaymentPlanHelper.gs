package gw.api.util

uses com.google.common.base.Joiner
uses gw.api.domain.invoice.PaymentPlanChanger

uses java.util.ArrayList

@Export
class ChangePaymentPlanHelper {


  /**
   * Validates whether the given payment plan changes will have unexpected issues, and returns a warning message if it does.
   * The two things it validates:
   * - The change creates a violation of equity
   * - The change won't be able to redistribute all payments because some payments were of opposite sign to the charge amount.
   *
   * @param paymentPlanChanger the {@link PaymentPlanChanger} for which to validate.
   * @param redistribute the option for whether to redistribute payments as part of this change
   * @return A prompt message if there is a violation
   */
  static function getPaymentPlanWarning(changer : PaymentPlanChanger, redistribute : gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange) : String {
    var warningMessages = new ArrayList<String>()

    var equityViolation = gw.api.util.EquityValidationHelper.isEquityViolated(changer)

    if (equityViolation != null) {
      warningMessages.add(equityViolation)
    }

    var redistributeIssue = (gw.api.domain.invoice.PaymentHandlingDuringPaymentPlanChange.ReverseAndRedistribute == redistribute) ? changer.LocalizedSummaryOfUndistributablePayments : null

    if (redistributeIssue != null) {
      warningMessages.add(redistributeIssue)
    }

    if (warningMessages.Empty) {
      return null
    } else {
      return Joiner.on("\n\n").join(warningMessages)
    }
  }
}
