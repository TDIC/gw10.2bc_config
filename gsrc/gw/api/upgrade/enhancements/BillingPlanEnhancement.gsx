package gw.api.upgrade.enhancements

uses gw.api.util.CurrencyUtil
uses gw.pl.currency.MonetaryAmount

/**
 * File created by Guidewire Upgrade Tools.
 */
enhancement BillingPlanEnhancement : BillingPlan {

  @DeprecatedAndRestoredByUpgrade
  property get InvoiceFee() : MonetaryAmount {
    return this.getInvoiceFee(CurrencyUtil.DefaultCurrency)
  }

  @DeprecatedAndRestoredByUpgrade
  property set InvoiceFee(amount : MonetaryAmount) : void {
    this.setInvoiceFeeForCurrency(CurrencyUtil.DefaultCurrency, amount)
  }

  @DeprecatedAndRestoredByUpgrade
  property get PaymentReversalFee() : MonetaryAmount {
    return this.getPaymentReversalFee(CurrencyUtil.DefaultCurrency)
  }

  @DeprecatedAndRestoredByUpgrade
  property set PaymentReversalFee(amount : MonetaryAmount) : void {
    this.setPaymentReversalFeeForCurrency(CurrencyUtil.DefaultCurrency, amount)
  }

  @DeprecatedAndRestoredByUpgrade
  property get LowBalanceThreshold() : MonetaryAmount {
    return this.getLowBalanceThreshold(CurrencyUtil.DefaultCurrency)
  }

  @DeprecatedAndRestoredByUpgrade
  property set LowBalanceThreshold(amount : MonetaryAmount) : void {
    this.setLowBalanceThresholdForCurrency(CurrencyUtil.DefaultCurrency, amount)
  }

  @DeprecatedAndRestoredByUpgrade
  property get ReviewDisbursementOver() : MonetaryAmount {
    return this.getReviewDisbursementOver(CurrencyUtil.DefaultCurrency)
  }

  @DeprecatedAndRestoredByUpgrade
  property set ReviewDisbursementOver(amount : MonetaryAmount) : void {
    this.setReviewDisbursementOverForCurrency(CurrencyUtil.DefaultCurrency, amount)
  }

  @DeprecatedAndRestoredByUpgrade
  property get DisbursementOver() : MonetaryAmount {
    return this.getDisbursementOver(CurrencyUtil.DefaultCurrency)
  }

  @DeprecatedAndRestoredByUpgrade
  property set DisbursementOver(amount : MonetaryAmount) : void {
    this.setDisbursementOverForCurrency(CurrencyUtil.DefaultCurrency, amount)
  }
}
