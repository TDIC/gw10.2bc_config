package gw.web.summary

uses gw.api.database.Query
uses gw.web.util.BCIconHelper

uses java.util.Date

/**
 * Abstract helper that encapsulates the value and display logic used to display
 * summary information that includes inception and recent delinquency state of a
 * {@link gw.api.domain.delinquency.DelinquencyTarget DelinquencyTarget} entity
 * on a summary screen.
 */
@Export
abstract class SummaryHelper {
  /**
   * The {@link Date} format to be used for formatting a day {@link Date}.
   */
  public final static var FULL_DATE_FORMAT: String = "MMMM d, yyyy"

  var _recentDelinquenciesCount = -1

  var _hasActiveDelinquency: Boolean = null

  static property get FullDateFormat(): String {
    return FULL_DATE_FORMAT
  }

  /**
   * The count of delinquencies on the
   * {@link gw.api.domain.delinquency.DelinquencyTarget DelinquencyTarget}
   * entity being summarized.
   */
  property get DelinquenciesCount(): int {
    return makeDelinquenciesQuery().select().Count
  }

  /**
   * The count of recent delinquencies for the
   * {@link gw.api.domain.delinquency.DelinquencyTarget DelinquencyTarget}
   * entity being summarized based on restriction by the defined
   * {@link #RecentPastStart()}.
   */
  final property get RecentDelinquenciesCount(): int {
//      final var recentPastStart = RecentPastStart
//      _recentDelinquenciesCount = DelinquencyProcesses.countWhere(
//          \ delinquency -> delinquency.StartDate.after(recentPastStart))
    final var recentQuery = makeRecentDelinquenciesQuery()
    recentQuery.compare(DelinquencyProcess#StartDate, GreaterThan, RecentPastStart)
    _recentDelinquenciesCount = recentQuery.select().Count
    return _recentDelinquenciesCount
  }

  /**
   * The start {@link java.util.Date Date} of the recent past for the history.
   * The period of interest can be configured here or overridden by a subclass.
   */
  protected property get RecentPastStart(): Date {
    return Date.Now.addYears(-1)
  }

  /**
   * Whether the
   * {@link gw.api.domain.delinquency.DelinquencyTarget DelinquencyTarget} is
   * active or has an active {@link entity.DelinquencyProcess
   * DelinquencyeProcess}
   */
  property get HasActiveDelinquencyTarget(): boolean {
    return HasActiveTarget or HasActiveDelinquencyProcess
  }

  /**
   * Whether the
   * {@link gw.api.domain.delinquency.DelinquencyTarget DelinquencyTarget} is
   * active.
   */
  abstract protected property get HasActiveTarget(): boolean

  /**
   * The icon (name) for the delinquency state of the entity.
   */
  property get DelinquencyIcon(): String {
    return BCIconHelper.DelinquencyIcon
  }

  /**
   * Whether to show the icon for the delinquency state of the entity.
   */
  property get ShowDelinquencyIcon(): boolean {
    return (HasActiveDelinquencyProcess or RecentDelinquenciesCount > 0)
  }

  private property get HasActiveDelinquencyProcess(): boolean {
    if (_hasActiveDelinquency == null) {
      //_hasActiveDelinquency = <target>.hasActiveDelinquencyProcess()
      //_hasActiveDelinquency = DelinquencyProcesses
      //     .hasMatch(\ delinquency -> delinquency.Active)
      _hasActiveDelinquency = makeDelinquenciesQuery()
          .compareIn(DelinquencyProcess#Status,
              DelinquencyProcess.OPEN_STATUS_VALUES.toTypedArray())
          .select()
          .Count > 0
    }
    return _hasActiveDelinquency
  }

  /**
   * The display text for the time since inception of the entity being
   * summarized.
   */
  property get TimeSinceInceptionDisplay(): String {
    return SummaryTimeSinceCalculator.calculateTimeSince(InceptionDate, null)
  }

  /**
   * The icon (name) for the time since inception of the entity being
   * summarized.
   */
  property get TimeSinceInceptionIcon(): String {
    return BCIconHelper.TimeSinceInceptionIcon
  }

  /**
   * Whether to show the icon for the time since inception of the entity being
   * summarized.
   */
  property get ShowTimeSinceInceptionIcon(): boolean {
    return !InceptionDate.addYears(1).after(Date.Now)
  }

  /**
   * The inception date of the entity being summarized.
   */
  protected abstract property get InceptionDate(): Date

  /**
   * The {@link entity.DelinquencyProcess delinquenciences} associated with the
   * entity being summarized.
   */
  protected abstract property get DelinquencyProcesses(): DelinquencyProcess[]

  /**
   * The tool tip for the overview delinquency flag and label.
   */
  abstract property get RecentDelinquenciesTooltip(): String

  /**
   * Construct a query for {@link entity.DelinquencyProcess delinquencies} on
   * the summary helper's {@link gw.api.domain.delinquency.DelinquencyTarget
   * delinquency target}.
   *
   * @return An associated {@code DelinquencyProcess} query for the summarized
   * {@code target}.
   */
  abstract protected function makeDelinquenciesQuery(): Query<DelinquencyProcess>

  /**
   * Constructs a query for possibly recent {@link entity.DelinquencyProcess
   * delinquencies} related to the helper's
   * {@link gw.api.domain.delinquency.DelinquencyTarget delinquency target}.
   * This does not apply any restriction derived from what would be considered
   * recent here, but the {@link #RecentDelinquenciesCount()} will do so
   * instead.
   *
   * @return A recent {@code DelinquencyProcess} query for the summary
   * {@code target}.
   */
  abstract protected function makeRecentDelinquenciesQuery(): Query<DelinquencyProcess>
}
