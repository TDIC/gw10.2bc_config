package gw.web.summary

uses gw.api.locale.DisplayKey
uses gw.api.util.BCDateUtil
uses java.util.Date
uses org.joda.time.Years
uses org.joda.time.Months

/**
 * Used on the Account and Policy Summary screens to display a string referencing how long the Account or Policy has existed.
 */
@Export
class SummaryTimeSinceCalculator {
  static function calculateTimeSince(date: Date, endDate : Date): String {
    if (Date.CurrentDate < date) return "";

    var createTimeDisplay = date.format("medium")
    var timeSince = BCDateUtil.yearsUntilNow(date)
    if (endDate != null) {
      timeSince = Years.yearsBetween(new org.joda.time.DateTime(date), new org.joda.time.DateTime(endDate)).getYears()
    }


    if (timeSince != 0) {
      if (timeSince == 1) {
        return DisplayKey.get('Web.SummaryTimeSinceCalculator.TimeSince.YearSingle', timeSince, createTimeDisplay)
      } else {
        return DisplayKey.get('Web.SummaryTimeSinceCalculator.TimeSince.YearPlural', timeSince, createTimeDisplay)
      }
    }

    timeSince = BCDateUtil.monthsUntilNow(date)
    if (endDate != null) {
      timeSince = Months.monthsBetween(new org.joda.time.DateTime(date), new org.joda.time.DateTime(endDate)).getMonths()
    }

    if (timeSince != 0) {
      if (timeSince == 1) {
        return DisplayKey.get('Web.SummaryTimeSinceCalculator.TimeSince.MonthSingle', timeSince, createTimeDisplay)
      } else {
        return DisplayKey.get('Web.SummaryTimeSinceCalculator.TimeSince.MonthPlural', timeSince, createTimeDisplay)
      }
    }

    if (endDate != null) {
      return DisplayKey.get('Web.SummaryTimeSinceCalculator.TimeSince.MonthSingle', 1, createTimeDisplay)
    }

    if (date.MonthOfYearEnum == Date.CurrentDate.MonthOfYearEnum){
      return DisplayKey.get('Web.SummaryTimeSinceCalculator.TimeSince.ThisMonth', createTimeDisplay)
    }

    return DisplayKey.get('Web.SummaryTimeSinceCalculator.TimeSince.LastMonth', createTimeDisplay)

  }
}