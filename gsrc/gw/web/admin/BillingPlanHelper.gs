package gw.web.admin

uses gw.api.locale.DisplayKey

@Export
class BillingPlanHelper {

  static function getDayLabel(billingPlan : BillingPlan) : String {
    return (billingPlan.LeadTimeDayUnit == DayUnit.TC_BUSINESS) ?
        DisplayKey.get('Web.BillingPlanDetailDV.BusinessDays') :
        DisplayKey.get('Web.BillingPlanDetailDV.Days')
  }
}