package gw.web.payment.batch

uses com.google.common.base.Joiner
uses gw.api.locale.DisplayKey
uses gw.api.util.ProgramCheck
uses gw.api.web.payment.PaymentInstrumentFactory
uses gw.api.web.producer.ProducerSearchConverter
uses gw.payment.InvoiceSearchConverterFrozenFiltering
uses pcf.api.Location
uses typekey.Currency

@Export
class BatchPaymentDetailsView {

  var _batchPayment: BatchPayment as readonly BatchPayment
  var _mode: Mode as readonly Mode
  var _producerConverter: ProducerSearchConverter as readonly ProducerConverter = new ProducerSearchConverter(StringCriterionMode.TC_EQUALS)
  var _invoiceConverter: InvoiceSearchConverterFrozenFiltering as readonly InvoiceConverter = new InvoiceSearchConverterFrozenFiltering(StringCriterionMode.TC_EQUALS)

  var _disbursedSuspensePaymentsWarning: String as readonly DisbursedSuspensePaymentsWarning

  property get BatchPaymentEditable(): boolean {
    return BatchPayment.isEditable()
  }

  property get EditPermitted(): boolean {
    return (gw.web.payment.batch.BatchPaymentDetailsView.Mode.MODIFICATION == this.Mode and perm.BatchPayment.modify)
  }

  property get UpdatePermitted(): boolean {
    return (this.Mode == gw.web.payment.batch.BatchPaymentDetailsView.Mode.CREATION and perm.BatchPayment.process)
        or (this.Mode == gw.web.payment.batch.BatchPaymentDetailsView.Mode.MODIFICATION and perm.BatchPayment.modify)
  }

  //Cancel is permitted if and only if Update is permitted
  property get CancelPermitted(): boolean {
    return UpdatePermitted
  }

  property get PostPermitted(): boolean {
    return perm.BatchPayment.modify or perm.BatchPayment.process
  }

  property get ReversePermitted(): boolean {
    return perm.BatchPayment.modify
  }

  property get BatchPaymentEntries(): BatchPaymentEntry[] {
    return BatchPayment.Payments
  }

  property get Posted(): boolean {
    return BatchPayment.hasBeenPosted()
  }

  property get Reversed(): boolean {
    return BatchPayment.BatchStatus == BatchPaymentsStatus.TC_REVERSED
  }

  property get BatchPaymentInstruments(): List<PaymentInstrument> {
    var instruments = new ArrayList<PaymentInstrument>(){
        PaymentInstrumentFactory.getCashPaymentInstrument(),
        PaymentInstrumentFactory.getCheckPaymentInstrument()
    }
    return instruments
  }

  private construct(batchPayment: BatchPayment, mode: Mode) {
    _batchPayment = batchPayment
    _mode = mode

    _disbursedSuspensePaymentsWarning = generateWarningForDisbursedSuspensePayments()
  }

  public static function newModificationInstance(batchPayment: BatchPayment): BatchPaymentDetailsView {
    ProgramCheck.checkNotNull(batchPayment, "batchPayment", "Batch Payment Argument required")
    return new BatchPaymentDetailsView(batchPayment, Mode.MODIFICATION)
  }

  public static function newCreationInstance(location: Location, currency: Currency): BatchPaymentDetailsView {
    ProgramCheck.checkNotNull(location, "location", "Location Argument required")
    ProgramCheck.checkNotNull(currency, "currency", "Currency Argument required")
    return new BatchPaymentDetailsView(new BatchPayment(location, currency), Mode.CREATION)
  }

  public function addBatchPaymentEntry(): BatchPaymentEntry {
    var entry = new BatchPaymentEntry(BatchPayment.Currency)
    BatchPayment.addToPayments(entry)
    return entry
  }

  public function removeBatchPaymentEntry(batchPaymentEntry: BatchPaymentEntry) {
    BatchPayment.removeFromPayments(batchPaymentEntry)
  }

  public function postAsync() {
    var temp: BatchPayment
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      temp = bundle.add(_batchPayment)
      temp.postAsync()
    })
    _batchPayment = temp
  }

  public function reverseAsync() {
    var temp: BatchPayment
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      temp = bundle.add(_batchPayment)
      temp.reverseAsync()
    })
    _batchPayment = temp
  }

  private function generateWarningForDisbursedSuspensePayments(): String {
    var warnings = generateWarningWithListOfPayments(BatchPayment.DisbursedSuspensePayments)
        .map(\description -> DisplayKey.get("Web.BatchPaymentDetailsScreen.SuspenseHasBeenDisbursedDescription", Joiner.on(", ").join(description)))

    return Joiner.on("\n").join(warnings)
  }

  private function generateWarningWithListOfPayments(entries: BatchPaymentEntry[]): List<String>[] {
    return entries
        .map(\entry -> {
          var details = {
              DisplayKey.get("Web.BatchPaymentEntriesLV.PaymentType") + ": " + entry.PaymentType,
              DisplayKey.get("Web.BatchPaymentEntriesLV.PaymentDate") + ": " + entry.PaymentDate,
              DisplayKey.get("Web.BatchPaymentEntriesLV.PaymentInstrument") + ": " + entry.PaymentInstrument,
              DisplayKey.get("Web.BatchPaymentEntriesLV.Amount") + ": " + entry.Amount.DisplayValue
          }

          Optional.ofNullable(entry.RefNumber).ifPresent(\data -> details.add(3, DisplayKey.get("Web.BatchPaymentEntriesLV.RefNumber") + ": " + data))
          Optional.ofNullable(entry.Account).ifPresent(\data -> details.add(DisplayKey.get("Web.BatchPaymentEntriesLV.Account") + ": " + data))
          Optional.ofNullable(entry.Invoice).ifPresent(\data -> details.add(DisplayKey.get("Web.BatchPaymentEntriesLV.Invoice") + ": " + data.InvoiceNumber))
          Optional.ofNullable(entry.PolicyPeriod).ifPresent(\data -> details.add(DisplayKey.get("Web.BatchPaymentEntriesLV.Policy") + ": " + data))
          Optional.ofNullable(entry.Producer).ifPresent(\data -> details.add(DisplayKey.get("Web.BatchPaymentEntriesLV.Producer") + ": " + data))
          Optional.ofNullable(entry.Description).ifPresent(\data -> details.add(DisplayKey.get("Web.BatchPaymentEntriesLV.Description") + ": " + data))

          return details
        })
  }

  enum Mode {
    CREATION, MODIFICATION
  }
}