package gw.web.account

uses com.google.common.collect.Lists
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.api.domain.accounting.ChargePatternKey
uses gw.api.locale.DisplayKey
uses gw.api.web.color.GWColor
uses gw.pl.currency.MonetaryAmount
uses gw.web.summary.SummaryChartEntry
uses gw.web.summary.SummaryChartMonetaryEntry
uses gw.web.summary.SummaryFinancialsHelper

uses java.math.BigDecimal

@Export
class AccountSummaryFinancialsHelper extends SummaryFinancialsHelper {
  var _account: Account as readonly Account

  construct(account: Account) {
    _account = account
    _tAccountOwner = account
  }

  property get LastPaymentReceived(): DirectBillMoneyRcvd {
    return _account
        .findReceivedPaymentMoneysSortedByReceivedDateDescending()
        .firstWhere(\elt -> !elt.isReversed())
  }

  override property get LastPaymentReceivedLabel(): String {
    var receivedDate = LastPaymentReceived.ReceivedDate
    if (receivedDate != null) {
      return DisplayKey.get('Web.AccountSummary.Financials.LastPaymentReceived',
          getBestDateFormatAccordingToLocale(ON_DATE_LABEL_FORMAT, receivedDate))
    } else {
      return DisplayKey.get('Web.AccountSummary.Financials.LastPaymentReceived.NoPayment')
    }
  }

  override property get NextInvoiceDueLabel(): String {
    var nextDueInvoice = _account.NextDueInvoice
    if (nextDueInvoice != null) {
      return DisplayKey.get('Web.AccountSummary.Financials.NextDueInvoice',
          getBestDateFormatAccordingToLocale(ON_DATE_LABEL_FORMAT, nextDueInvoice.DueDate))
    } else {
      return DisplayKey.get('Web.AccountSummary.Financials.NextDueInvoice.NoInvoices')
    }
  }

  property get NextInvoiceDue(): MonetaryAmount {
    var dueInvoices = _account.AllNextDueInvoicesWithSameDueDate
    var amountDue = new MonetaryAmount(BigDecimal.ZERO, _account.Currency)
    foreach (var invoice in dueInvoices) {
      amountDue += invoice.AmountDue
    }

    return amountDue
  }

  property get PlannedInvoicesCount(): int {
    var plannedInvoices = Query.make(AccountInvoice)
        .compare(AccountInvoice.ACCOUNT_PROP.get(), Relop.Equals, _account)
        .compare(AccountInvoice.STATUS_PROP.get(), Relop.Equals, InvoiceStatus.TC_PLANNED);
    return plannedInvoices.select().Count
  }

  property get ExpectedPayment(): MonetaryAmount {
    return OutstandingAmount
  }

  property get PayoffAmount(): MonetaryAmount {
    return RemainingBalance
  }

  property get ChartValues(): java.util.List < SummaryChartEntry > {
    var entries = Lists.newArrayList<SummaryChartEntry>()
    /* in legend order */
    entries.add(new SummaryChartMonetaryEntry('Web.AccountSummary.Financials.Chart.Unbilled',
        UnbilledAmount, GWColor.THEME_PROGRESS_UNSTARTED))
    entries.add(new SummaryChartMonetaryEntry('Web.AccountSummary.Financials.Chart.Paid',
        PaidAmount, GWColor.THEME_PROGRESS_COMPLETE))
    entries.add(new SummaryChartMonetaryEntry('Web.AccountSummary.Financials.Chart.WrittenOff',
        WrittenOffAmount, GWColor.THEME_PROGRESS_ABANDONDED))
    entries.add(new SummaryChartMonetaryEntry('Web.AccountSummary.Financials.Chart.Billed',
        BilledAmount, GWColor.THEME_PROGRESS_INPROGRESS))
    entries.add(new SummaryChartMonetaryEntry('Web.AccountSummary.Financials.Chart.PastDue',
        DueAmount, GWColor.THEME_PROGRESS_OVERDUE))
    return entries
  }

  // ==== Late Fee Amount ====

  /**
   * The total unsettled amount of all late fee ({@link
   * ChargePatternKey#ACCOUNTLATEFEE AccountLateFee} and
   * {@link ChargePatternKey#POLICYLATEFEE PolicyLateFee}) {@link Charge}s
   * owned by the {@link Account}.
   */
  property get UnsettledLateFeesAmount() : MonetaryAmount {
    final var lateFeePatterns =
        {ChargePatternKey.ACCOUNTLATEFEE.get(),
            ChargePatternKey.POLICYLATEFEE.get()}
    final var lateFees = _account.getChargesRelatedToAccount(TC_OWNED)
        .where(\ charge -> lateFeePatterns.contains(charge.ChargePattern)
            and not (charge.Reversed or charge.Reversal))
    final var total = lateFees.flatMap(\ lateCharge -> lateCharge.InvoiceItems)
        .sum(_account.Currency,
            \ lateChargeInvoiceItem -> lateChargeInvoiceItem.GrossUnsettledAmount)

    return total.IsNegative ? 0bd.ofCurrency(total.Currency) : total
  }

  // Amounts for Account-level charges

  property get AccountUnbilledAmount() : MonetaryAmount {
    return _account.AccountUnbilledUnsettledAmount
  }

  property get AccountBilledAmount() : MonetaryAmount {
    return _account.AccountBilledUnsettledAmount
  }

  property get AccountDueAmount() : MonetaryAmount {
    return _account.AccountDueUnsettledAmount
  }

  property get AccountDelinquentAmount() : MonetaryAmount {
    return _account.RawDelinquentAmount
  }

  property get AccountOutstandingAmount() : MonetaryAmount {
    return _account.AccountOutstandingUnsettledAmount
  }

  property get AccountUnpaidAmount() : MonetaryAmount {
    return _account.RawUnpaidAmount
  }


  // Aggregate amounts on child policies

  property get PoliciesUnbilledAmount() : MonetaryAmount {
    var policyPeriods = _account.OpenPolicyPeriods
    return policyPeriods.sum(_account.Currency, \ pp -> pp.UnbilledUnsettledAmount)
  }

  property get PoliciesBilledAmount() : MonetaryAmount {
    var policyPeriods = _account.OpenPolicyPeriods
    return policyPeriods.sum(_account.Currency, \ pp -> pp.BilledUnsettledAmount)
  }

  property get PoliciesDueAmount() : MonetaryAmount {
    var policyPeriods = _account.OpenPolicyPeriods
    return policyPeriods.sum(_account.Currency, \ pp -> pp.DueUnsettledAmount)
  }

  property get PoliciesDelinquentAmount() : MonetaryAmount {
    var policyPeriods = _account.OpenPolicyPeriods
    return policyPeriods.sum(_account.Currency, \ pp -> pp.DelinquentAmount)
  }

  property get PoliciesOutstandingAmount() : MonetaryAmount {
    var policyPeriods = _account.OpenPolicyPeriods
    return policyPeriods.sum(_account.Currency, \ pp -> pp.OutstandingUnsettledAmount)
  }

  property get PoliciesTotalValue() : MonetaryAmount {
    var policyPeriods = _account.OpenPolicyPeriods
    return policyPeriods.sum(_account.Currency, \ pp -> pp.TotalValue)
  }

  property get PoliciesPaidAmount() : MonetaryAmount {
    var policyPeriods = _account.OpenPolicyPeriods
    return policyPeriods.sum(_account.Currency, \ pp -> pp.PaidAmount)
  }
}