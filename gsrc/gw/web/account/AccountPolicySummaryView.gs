package gw.web.account

uses java.util.Date
uses gw.api.locale.DisplayKey
uses gw.web.util.BCIconHelper

/**
 * A view helper class for the {@link pcf.AccountSummaryScreen Account Summary
 * Screen}. A row in the {@code AccountPolicyPeriods ListView} on this screen
 * shows various attributes from the {@link entity.PolicyPeriod PolicyPeriiod}
 * but also {@link java.util.Date Date} attributes from the
 * related {@link entity.BillingInstruction BillingInstructions},
 * {@link entity.Cancellation Cancellation} and
 * {@link entity.Reinstatement Reinstatement}.
 *
 * This in conjunction with {@link gw.web.account.AccountSummaryPolicyHelper
 * AccountSummaryPolicyHelper}, helps optimize database retrieval that would
 * have been more expensive using only existing API calls.
 */
@Export
class AccountPolicySummaryView {

  private var _policyPeriod : PolicyPeriod as readonly PolicyPeriod
  private var _cancellationDate : Date as readonly CancellationDate
  private var _reinstatementDate : Date as ReinstatementDate

  construct(policyPeriod: PolicyPeriod, cancellationDate : Date = null, reinstatementDate : Date = null) {
    _policyPeriod = policyPeriod
    _cancellationDate = cancellationDate
    _reinstatementDate = reinstatementDate
  }

  property get HasProductCode(): boolean {
    return _policyPeriod.Policy.LOBCode != null
  }

  property get ProductCodeIcon(): String {
    return BCIconHelper.getIcon(PolicyPeriod.Policy.LOBCode)
  }

  property get ProductCodeIconTooltip(): String {
    return HasProductCode
        ? _policyPeriod.Policy.LOBCode.DisplayName
        : DisplayKey.get('Web.AccountSummary.Policy')
  }

  property get PastDueIcon(): String {
    return BCIconHelper.getPolicyPeriodPastDueIcon(PolicyPeriod)
  }

  property get InForceIcon() : String {
    return BCIconHelper.getPolicyPeriodInForceIcon(PolicyPeriod)
  }

  property get InForceTooltip() : String {
    if (_policyPeriod.IsInForce) {
      return DisplayKey.get('Web.PolicyInfoBar.InForce');
    } else {
      return _policyPeriod.Archived ? DisplayKey.get('Web.PolicyInfoBar.Archived') : DisplayKey.get('Web.PolicyInfoBar.NotInForce');
    }
  }
}