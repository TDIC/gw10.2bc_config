package gw.web.account

uses gw.api.database.Query
uses gw.api.locale.DisplayKey
uses gw.web.summary.SummaryHelper
uses gw.web.util.BCIconHelper

uses java.util.Date

/**
 * Encapsulates the value and display logic used to display the information
 * for an {@link entity.Account Account} on a summary display.
 */
@Export
class AccountSummaryHelper extends SummaryHelper {
  var _account : Account as readonly Account

  public construct(account: Account) {
    super()
    _account = account
  }

  override final property get HasActiveTarget() : boolean {
    return not _account.Closed
  }

  override property get InceptionDate() : Date {
    return _account.CreateTime
  }

  override property get DelinquencyProcesses() : DelinquencyProcess[] {
    return _account.DelinquencyProcesses
  }

  function getPhoneEmailDisplayValue(contact : Contact) : String {
    var phoneNumber = contact.PrimaryPhoneValue != null ? contact.PrimaryPhoneValue : "-"
    var email = contact.EmailAddress1 != null ? contact.EmailAddress1 : "-"
    return DisplayKey.get('Web.AccountSummary.Overview.PhoneEmailDisplay', phoneNumber, email)
  }

  override property get RecentDelinquenciesTooltip() : String {
    if (_account.hasActiveDelinquencyProcess()) return DisplayKey.get('Web.AccountInfoBar.Delinquent')

    var recent = RecentDelinquenciesCount
    if (recent == 0) {
      return DisplayKey.get('Web.AccountSummary.Overview.NoRecentDelinquency')
    } else if (recent == 1) {
      return DisplayKey.get('Web.AccountSummary.Overview.RecentDelinquency')
    }

    return DisplayKey.get('Web.AccountSummary.Overview.RecentDelinquencies')
  }

  override function makeDelinquenciesQuery() : Query<DelinquencyProcess> {
    final var delinquenciesQuery = Query.make(DelinquencyProcess)
    delinquenciesQuery.compare(DelinquencyProcess#Account, Equals, _account)
    return delinquenciesQuery
  }

  override function makeRecentDelinquenciesQuery() : Query<DelinquencyProcess> {
    return makeDelinquenciesQuery()
  }

  property get ServiceTierIcon() : String {
    return BCIconHelper.getIcon(_account.ServiceTier)
  }

  property get ServiceTierTooltip() : String {
    switch (_account.ServiceTier) {
      case CustomerServiceTier.TC_GOLD: return DisplayKey.get('Web.AccountSummary.Overview.ServiceTierIcon.Gold')
      case CustomerServiceTier.TC_PLATINUM: return DisplayKey.get('Web.AccountSummary.Overview.ServiceTierIcon.Platinum')
      case CustomerServiceTier.TC_SILVER: return DisplayKey.get('Web.AccountSummary.Overview.ServiceTierIcon.Silver')
        default: return ""
    }
  }
}