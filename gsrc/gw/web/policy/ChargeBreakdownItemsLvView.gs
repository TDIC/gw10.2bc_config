package gw.web.policy

uses entity.ChargeBreakdownCategory
uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses gw.api.domain.charge.ChargeInitializer
uses gw.api.locale.DisplayKey
uses gw.util.Pair

uses gw.api.database.Relop#Equals

@Export
class ChargeBreakdownItemsLvView {

  private var _initializer : ChargeInitializer

  construct(initializer : ChargeInitializer) {
    _initializer = initializer
  }

  function setChargeAmountFromBreakdownAmount() {
    _initializer.setAmount(_initializer.getTotalBreakdownAmount());
  }

  function removeItem(item : ChargeBreakdownItem) {
    item.Categories.forEach(\category -> category.remove())
    _initializer.removeBreakdownItem(item)
    setChargeAmountFromBreakdownAmount()
  }

  function allCategoryTypes() : IQueryBeanResult<ChargeBreakdownCategoryType> {
    return Query.make(ChargeBreakdownCategoryType).select()
  }

  function createAndAddItem() : ChargeBreakdownItem {
    return _initializer.addBreakdownItem()
  }

  function createAndAddBreakdownCategory(item : ChargeBreakdownItem) : ChargeBreakdownCategory {
    return item.addCategory()
  }

  function isSavedCategory(breakdownCategory : ChargeBreakdownCategory) : boolean {
    return findEquivalentSavedCategory(breakdownCategory).HasElements
  }

  function onCategorySwitch(breakdownCategory : ChargeBreakdownCategory) {
    var equivalentCategory = getEquivalentNewCategories(breakdownCategory)?.first()
        ?: findEquivalentSavedCategory(breakdownCategory).AtMostOneRow
    if (equivalentCategory != null) {
      breakdownCategory.setCategoryName(equivalentCategory.CategoryName)
    }
  }

  function onCategoryNameChange(breakdownCategory : ChargeBreakdownCategory) {
    getEquivalentNewCategories(breakdownCategory).forEach(\otherCategory ->
        otherCategory.setCategoryName(breakdownCategory.CategoryName))
  }

  function removeBreakdownCategory(item : ChargeBreakdownItem, breakdownCategory : ChargeBreakdownCategory) {
    item.removeCategory(breakdownCategory)
    breakdownCategory.remove()
  }

  function getCategoryLinkText(numCategories : int) : String {
    return numCategories > 0 ? DisplayKey.get("Web.ChargeBreakdownItem.EditCategories") : DisplayKey.get("Web.ChargeBreakdownItem.AddCategories")
  }

  function validateItemsHaveUniqueCategories() : String {
    var categorySetForEachItem = _initializer.BreakdownItems*.Categories.map(\categoriesForOneItem ->
        categoriesForOneItem.map(\eachCategory -> identifyingFieldsFor(eachCategory)).toSet())
    var numberOfUniqueSetsOfCategories = categorySetForEachItem.toSet().size()
    var numberOfBreakdownItems = _initializer.BreakdownItems.size()
    return numberOfUniqueSetsOfCategories == numberOfBreakdownItems
        ? null
        : DisplayKey.get("Java.ChargeInitializer.BreakdownItemsHaveSameCategories")
  }

  /**
   * The UI creates a separate instance for each entered ChargeBreakdownCategory even when the same category
   * has been entered for more than one breakdown item, and then the UI collapses the separate instances into a single
   * category instance during commit. Because of that, during editing compare the identifyingFields for two categories
   * to determine if they're equivalent.
   */
  private function identifyingFieldsFor(category : ChargeBreakdownCategory) : Pair<ChargeBreakdownCategoryType, String> {
    return new Pair<ChargeBreakdownCategoryType, String>(category.CategoryType, category.CategoryIdentifier)
  }

  private function findEquivalentSavedCategory(breakdownCategory : ChargeBreakdownCategory) : IQueryBeanResult<ChargeBreakdownCategory> {
    return Query.make(ChargeBreakdownCategory)
        .compare(ChargeBreakdownCategory#CategoryIdentifier, Equals, breakdownCategory.CategoryIdentifier)
        .compare(ChargeBreakdownCategory#CategoryType, Equals, breakdownCategory.CategoryType)
        .select()
  }

  private function getEquivalentNewCategories(breakdownCategory : ChargeBreakdownCategory) : List<ChargeBreakdownCategory> {
    return (breakdownCategory.Bundle.getBeansByRootType(ChargeBreakdownCategory.TYPE.get()) as Collection<ChargeBreakdownCategory>)
        .where(\eachNewCategory -> areEquivalentNewCategories(breakdownCategory, eachNewCategory))
  }

  private function areEquivalentNewCategories(category1 : ChargeBreakdownCategory, category2 : ChargeBreakdownCategory) : boolean {
    return category1.New && category2.New
        && category1 != category2
        && category1.CategoryType == category2.CategoryType
        && category1.CategoryIdentifier == category2.CategoryIdentifier
  }

}