package gw.webservice.policycenter.bc900

uses gw.xml.ws.annotation.WsiExportable

@WsiExportable( "http://guidewire.com/bc/ws/gw/webservice/policycenter/bc900/ContactSummary" )
@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
final class ContactSummary {
  public var Name : String
  public var Address : String
  public var Phone : String

  construct() {
  }

}
