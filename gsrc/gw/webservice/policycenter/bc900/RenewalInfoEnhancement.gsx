package gw.webservice.policycenter.bc900

uses gw.api.locale.DisplayKey
uses gw.api.system.BCLoggerCategory
uses gw.api.web.policy.NewPolicyUtil
uses gw.api.webservice.exception.EntityStateException
uses gw.webservice.policycenter.bc900.entity.types.complex.RenewalInfo

/**
 * Defines behavior for the 900 API version of the WSDL entity that specifies
 * the data used to create a {@link entity.Renewal Renewal} billing instruction.
 */
@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
enhancement RenewalInfoEnhancement : RenewalInfo {
  function executeRenewalBI() : BillingInstruction {
    return executedRenewalBISupplier()
  }

  function findPriorPolicyPeriod() : PolicyPeriod {
    final var priorPeriod = this.findByPolicyPublicIDOrPolicyNumber(
        this.PCPolicyPublicID, TermNumberToFind, PolicyNumberToFind)
    if (priorPeriod != null && priorPeriod.Archived) {
      throw new EntityStateException(
          DisplayKey.get('Webservice.Error.OperationNotPermittedOnArchivedPolicyPeriod')
      )
    }
    return gw.transaction.Transaction.Current.add(priorPeriod) // ensure writable...
  }

  property get PolicyNumberToFind() : String {
    return this.PriorPolicyNumber == null ? this.PolicyNumber : this.PriorPolicyNumber
  }

  property get TermNumberToFind() : int {
    // For the case when the prior policy period does not exist in PC but exist in BC.
    // As the result, PC cannot tell BC about the prior period but BC has to guess base on
    // the current policy period.
    return this.PriorTermNumber == null ? this.TermNumber - 1 : this.PriorTermNumber
  }

  function toRenewalForPreview() : NewPlcyPeriodBI {
    final var bi = createRenewalBI(true)
    this.initializeBillingInstruction(bi)
    return bi
  }

  private function createRenewalBI(isForPreview : boolean = false) : NewPlcyPeriodBI {
    var bi : NewPlcyPeriodBI
    final var priorPeriod = findPriorPolicyPeriod()
    if (priorPeriod == null) { // new renewal
      BCLoggerCategory.BILLING_API.info("Could not find prior policy period ${PolicyNumberToFind}-${TermNumberToFind}. Creating New Renewal.")
      bi = createNewPolicyBIInternal(isForPreview)
      this.initPolicyPeriodBIInternal(bi) // can occur after populate when no prior period...
    } else {
      BCLoggerCategory.BILLING_API.info("Renewing policy period ${PolicyNumberToFind}-${TermNumberToFind}.")
      bi = makeRenewalBIInternal(isForPreview, priorPeriod)
    }
    return bi
  }

  private function createNewPolicyBIInternal(isForPreview : boolean) : NewRenewal {
    final var owningAccount = isForPreview
       ? this.findOrCreateOwnerAccountForPreview() : this.findOwnerAccount()
    return NewPolicyUtil.createNewRenewal(
        owningAccount, this.createPolicyPeriod(isForPreview), this.TermNumber)
  }

  private function makeRenewalBIInternal(
      isForPreview : boolean, final priorPeriod : PolicyPeriod) : Renewal {
    final var renewalBI = (priorPeriod.Currency != this.CurrencyValue)
        ? NewPolicyUtil.createCurrencyChangeRenewal(this.findOwnerAccount(), priorPeriod)
        : NewPolicyUtil.createRenewal(priorPeriod)
    this.initPolicyPeriodBIInternal(renewalBI) // must precede populate when prior period exists...
    this.populateIssuanceInfo(isForPreview, renewalBI.NewPolicyPeriod)
    return renewalBI
  }

  function executedRenewalBISupplier(): NewPlcyPeriodBI {
    var bi = createRenewalBI()
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }
}