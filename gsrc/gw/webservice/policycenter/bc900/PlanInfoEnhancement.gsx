package gw.webservice.policycenter.bc900

uses gw.webservice.policycenter.bc900.entity.types.complex.PlanInfo

@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
enhancement PlanInfoEnhancement : PlanInfo {
  function copyPlanInfo(plan : Plan) {
    this.Name = plan.Name
    this.PublicID = plan.PublicID
  }
}
