package gw.webservice.policycenter.bc900

uses gw.pl.currency.MonetaryAmount
uses gw.xml.ws.annotation.WsiExportable

@WsiExportable( "http://guidewire.com/bc/ws/gw/webservice/policycenter/bc900/DisplayableBillingStatus" )
@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
final class DisplayableBillingStatus {
  private var _Delinquent : boolean as Delinquent
  private var _TotalBilled : MonetaryAmount as TotalBilled
  private var _PastDue : MonetaryAmount as PastDue
  private var _Unbilled : MonetaryAmount as Unbilled
  private var _BillingMethod : String as BillingMethodCode

  construct() { }

  construct(policyPeriod : PolicyPeriod) {
    Delinquent = policyPeriod.hasActiveDelinquenciesOutOfGracePeriod()
    TotalBilled = policyPeriod.BilledAmount
    PastDue = policyPeriod.DelinquentAmount
    Unbilled = policyPeriod.UnbilledAmount
    BillingMethodCode = policyPeriod.BillingMethod.Code
  }
}