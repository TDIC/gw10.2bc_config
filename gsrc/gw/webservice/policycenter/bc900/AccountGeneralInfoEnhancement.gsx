package gw.webservice.policycenter.bc900

uses gw.api.locale.DisplayKey
uses gw.api.web.accounting.ChargePatternHelper
uses gw.api.webservice.exception.SOAPException
uses gw.webservice.policycenter.bc900.entity.types.complex.AccountGeneralInfo

@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
enhancement AccountGeneralInfoEnhancement : AccountGeneralInfo {
  function executeGeneralBI() : BillingInstruction {
    return executedAccountGeneralBISupplier()
  }

  private function createAccountGeneralBI() : AccountGeneral{
    var bi = new AccountGeneral(this.CurrencyValue)
    bi.BillingInstructionDate = this.BillingInstructionDate.toCalendar().Time
    return bi
  }

  private function validateChargeCategory() {
    for (info in this.ChargeInfos) {
      var chargeCategory = ChargePatternHelper.getChargePattern(info.$TypeInstance.ChargePatternCode).Category
      if (chargeCategory != ChargeCategory.TC_FEE and chargeCategory != ChargeCategory.TC_GENERAL) {
        throw new SOAPException(DisplayKey.get("Webservice.Error.GeneralChargeCanBeOnlyOfCategoryFeeOrGeneral"))
      }
    }
  }

  function executedAccountGeneralBISupplier(): AccountGeneral {
    validateChargeCategory()
    var bi = createAccountGeneralBI()
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }
}
