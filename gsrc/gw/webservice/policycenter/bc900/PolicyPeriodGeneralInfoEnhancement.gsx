package gw.webservice.policycenter.bc900

uses gw.api.locale.DisplayKey
uses gw.api.webservice.exception.SOAPException
uses gw.transaction.ChargePatternHelper
uses gw.webservice.policycenter.bc900.entity.types.complex.PolicyPeriodGeneralInfo

@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
enhancement PolicyPeriodGeneralInfoEnhancement : PolicyPeriodGeneralInfo {
  function executeGeneralBI() : BillingInstruction {
    return executedGeneralBISupplier()
  }

  private function createGeneralBI() : General {
    var associatedPolicyPeriod = this.findPolicyPeriodForUpdate()
    var bi = new General(associatedPolicyPeriod.Currency)
    bi.AssociatedPolicyPeriod = associatedPolicyPeriod
    return bi
  }

  private function validateChargeCategory() {
    for (info in this.ChargeInfos) {
      var chargeCategory = ChargePatternHelper.getChargePattern(info.$TypeInstance.ChargePatternCode).Category
      if (chargeCategory != ChargeCategory.TC_FEE and chargeCategory != ChargeCategory.TC_GENERAL) {
        throw new SOAPException(DisplayKey.get("Webservice.Error.GeneralChargeCanBeOnlyOfCategoryFeeOrGeneral"))
      }
    }
  }

  function executedGeneralBISupplier(): General {
    validateChargeCategory()
    var bi = createGeneralBI()
    this.initializeBillingInstruction(bi)
    bi.execute()
    return bi
  }
}
