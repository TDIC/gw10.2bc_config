package gw.webservice.policycenter.bc900

uses gw.api.domain.accounting.BillingInstructionUtil
uses gw.pl.currency.MonetaryAmount
uses gw.webservice.policycenter.bc900.entity.anonymous.elements.BillingInstructionInfo_ChargeInfos
uses gw.webservice.policycenter.bc900.entity.types.complex.BillingInstructionInfo
uses gw.webservice.policycenter.bc900.entity.types.complex.ChargeInfo

uses java.lang.*

@Export
@gw.lang.Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
enhancement BillingInstructionInfoEnhancement : BillingInstructionInfo {
  /**
   * @deprecated since 9.0.1. Use {@link BillingAPITransactionUtil#executeBillingInstruction(IBlock0, transactionId)} instead.
   */
  @java.lang.Deprecated
  function execute(bi : PlcyBillingInstruction) : String {
    initializeBillingInstruction(bi)
    //noinspection GosuDeprecatedAPIUsage
    return BillingInstructionUtil.executeAndCommit(bi as BillingInstruction).PublicID
  }

  function initializeBillingInstruction(bi : PlcyBillingInstruction) {
    if (bi typeis ExistingPlcyPeriodBI) {
      initializeExistingPolicyPeriodBI(bi)
    }
    bi.Description = this.Description
    bi.OfferNumber = this.OfferNumber
    createCharges(bi)
    flagTermConfirmed(bi)
    bi.DepositRequirement = this.DepositRequirement == null ? null : new MonetaryAmount(this.DepositRequirement)
  }

  private function initializeExistingPolicyPeriodBI(bi : ExistingPlcyPeriodBI) {
    if (bi.AssociatedPolicyPeriod == null) {
      // subclass may already set this field
      bi.AssociatedPolicyPeriod = this.findPolicyPeriodForUpdate()
    }

    // modification for BC is actually effective date in PC
    bi.ModificationDate = this.EffectiveDate.toCalendar().Time
  }

  private function flagTermConfirmed(bi: BillingInstruction) {
    var termConfirmed = this.TermConfirmed == null ? true : this.TermConfirmed
    if (bi typeis PlcyBillingInstruction) {
      bi.PolicyPeriod.TermConfirmed = termConfirmed
    }
  }

  function createCharges(billingInstruction : BillingInstruction) {
    for (info in this.ChargeInfos) {
      info.$TypeInstance.toCharge(billingInstruction)
    }
  }

  function addChargeInfo(chargeInfo : ChargeInfo) {
    var elem = new BillingInstructionInfo_ChargeInfos()
    elem.$TypeInstance = chargeInfo
    this.ChargeInfos.add(elem)
  }

  /**
   * Creates an invoices summary for a policy billing instruction. The summary
   * is an array of {@link InvoiceSummaryItem}s that aggregate the amounts of
   * the {@link InvoiceItem}s of the policy period to which the instruction
   * applies based on a partitioning by each item's {@code InvoiceDueDate},
   * {@code Type}, and {@code Charge.ChargePattern.Category}.
   *<p/>
   * The summary is a preview if the billing instruction has not been persisted
   * after execution when this is invoked (i.e., the instruction was executed
   * in a non-persistent bundle before invoking this method).
   *
   * @param bi the {@link PlcyBillingInstruction} for which to create the
   *           invoices summary.
   * @return An array of {@link InvoiceSummaryItem}s that summarize the
   *         invoices for the {@link PolicyPeriod} of a {@link
   *         PlcyBillingInstruction}.
   */
  function createInvoicesSummary(bi : PlcyBillingInstruction)
      : InvoiceSummaryItem [] {
    final var policyPeriod = bi.PolicyPeriod
    var itemAmountsByPartitionMap = policyPeriod.Invoices*.InvoiceItems
        // filter added items or PolicyPeriod items
        // (can also include other filters here)
        .where(\ item -> item.New or item.PolicyPeriod == policyPeriod)
        .partition(\ item -> new InvoiceSummaryItem.InvoicePartitionKey(item))
        .mapValues(\ partitionItems ->
            partitionItems.sum(bi.Currency, \ item -> item.Amount))

    return itemAmountsByPartitionMap.Keys.map(\ partitionKey ->
        new InvoiceSummaryItem(partitionKey,
            itemAmountsByPartitionMap[partitionKey])
    ).toTypedArray()
  }
}
