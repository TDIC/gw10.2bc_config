package gw.webservice.policycenter.bc1000

uses gw.pl.currency.MonetaryAmount
uses gw.xml.ws.annotation.WsiExportable

@WsiExportable( "http://guidewire.com/bc/ws/gw/webservice/policycenter/bc1000/BCPCAccountInvoice" )
@Export
final class BCPCAccountInvoice {
  var _amount: MonetaryAmount as Amount
  var _invoiceID : String as InvoiceID
}
