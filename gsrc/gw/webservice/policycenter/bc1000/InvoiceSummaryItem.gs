package gw.webservice.policycenter.bc1000

uses gw.pl.currency.MonetaryAmount

/**
 * Defines a summary item for {@link InvoiceItem}s. The summary is an
 * aggregation amount of the {@link InvoiceItem}s partitioned by the summary
 * properties {@code InvoiceDate}, {@code InvoiceDueDate}, {@code Type}, and {@code Category}. An
 * {@code Invoice} consists of the items with the same due date.
 */
@gw.xml.ws.annotation.WsiExportable("http://guidewire.com/bc/ws/gw/webservice/policycenter/bc1000/InvoiceSummaryItem")
@Export
final class InvoiceSummaryItem {

  private var _invoiceDate : Date as InvoiceDate
  private var _invoiceDueDate : Date as InvoiceDueDate
  private var _chargeName : String as ChargeName
  private var _amount : MonetaryAmount as Amount
  private var _type : InvoiceItemType as Type
  private var _typeDisplay : String as TypeDisplay
  private var _category : ChargeCategory as Category
  private var _categoryDisplay : String as CategoryDisplay

  construct(){}

  private construct(invoiceDateIn : Date,
            invoiceDueDateIn : Date,
            chargeNameIn : String,
            amountIn : MonetaryAmount,
            typeIn : InvoiceItemType,
            chargeCategory : ChargeCategory) {
    _invoiceDate = invoiceDateIn
    _invoiceDueDate = invoiceDueDateIn
    _chargeName = chargeNameIn
    _amount = amountIn
    _type = typeIn
    _typeDisplay = _type.toString()
    _category = chargeCategory
    _categoryDisplay = _category.toString()
  }

  /**
   * Construct an {@link InvoiceSummary} from a partition key and amount.
   *
   * @param partitionKey the partition key used to summarize {@link
   *                     InvoiceItem}s.
   * @param amount the aggregated amount of the {@link InvoiceItem}s matching
   *               the partition.
   */
  construct(partitionKey : InvoicePartitionKey, amount : MonetaryAmount) {
    this(partitionKey.InvoiceDate, partitionKey.InvoiceDueDate, null, amount,
        partitionKey.ItemType, partitionKey.ChargeCategory)
  }

  /**
   * Defines a key used to partition {@link InvoiceItem}s when constructing
   * an {@link InvoiceSummaryItem}. The key components are the {@code InvoiceDate}, {@code
   * InvoiceDueDate}, {@code Type}, and {@code Charge.ChargePattern.Category}.
   */
  static internal class InvoicePartitionKey {
    var _invoiceDate : Date as readonly InvoiceDate
    var _invoiceDueDate : Date as readonly InvoiceDueDate
    var _invoiceItemType : InvoiceItemType as readonly ItemType
    var _chargeCategory : ChargeCategory as ChargeCategory

    construct(invoiceItem : InvoiceItem) {
      _invoiceDate = invoiceItem.InvoiceBilledDate
      _invoiceDueDate = invoiceItem.InvoiceDueDate
      _invoiceItemType = invoiceItem.Type
      _chargeCategory = invoiceItem.Charge.ChargePattern.Category
    }

    override function hashCode() : int {
      return  (29 * InvoiceDate.hashCode())
          ^ (29 * InvoiceDueDate.hashCode())
          ^ ItemType.hashCode()
          ^ ChargeCategory.hashCode()
    }

    override function equals(obj : Object) : boolean {
      //noinspection EqualsAsOperator
      return super.equals(obj)
        or (obj typeis InvoicePartitionKey
            and obj.InvoiceDate == this.InvoiceDate
            and obj.InvoiceDueDate == this.InvoiceDueDate
            and obj.ItemType == this.ItemType
            and obj.ChargeCategory == this.ChargeCategory)
    }

    override function toString() : String {
      return InvoiceDate + ", " + InvoiceDueDate + ", " + ItemType + "/" + ChargeCategory
    }
  }
}