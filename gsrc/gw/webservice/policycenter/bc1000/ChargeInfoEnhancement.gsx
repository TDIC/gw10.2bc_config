package gw.webservice.policycenter.bc1000

uses entity.BillingInstruction
uses entity.ChargeBreakdownCategory
uses gw.api.domain.accounting.ChargeUtil
uses gw.api.domain.charge.ChargeInitializer
uses gw.api.locale.DisplayKey
uses gw.api.util.LocaleUtil
uses gw.api.webservice.exception.BadIdentifierException
uses gw.api.webservice.exception.SOAPSenderException
uses gw.api.webservice.exception.SOAPServerException
uses gw.pl.currency.MonetaryAmount
uses gw.pl.persistence.core.Bundle
uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.BreakdownCategoryInfo_TranslatedCriteriaNames
uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.BreakdownItemInfo_Categories
uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.ChargeInfo_BreakdownItemInfos
uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.ChargeInfo_ChargeCommissionRateOverrideInfos
uses gw.webservice.policycenter.bc1000.entity.enums.EarlyItemHandlingType
uses gw.webservice.policycenter.bc1000.entity.types.complex.BreakdownItemInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.ChargeCommissionRateOverrideInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.ChargeInfo
uses gw.webservice.util.WebserviceEntityLoader

@Export
enhancement ChargeInfoEnhancement : ChargeInfo {

  function toCharge(billingInstruction : BillingInstruction) {
    var initializer = billingInstruction.buildCharge(new MonetaryAmount(this.Amount), ChargeUtil.getChargePatternByCode(this.ChargePatternCode))
    initializer.WrittenDate = this.WrittenDate?.toCalendar().Time
    initializer.ChargeGroup = this.ChargeGroup
    addBreakdownItems(initializer)

    if (this.RecaptureUnappliedFundID.HasContent) {
      var recaptureUnappliedFund = gw.api.database.Query.make(UnappliedFund)
          .compare("PublicID", Equals, this.RecaptureUnappliedFundID)
          .select().getAtMostOneRow()
      if (recaptureUnappliedFund == null) {
        throw new BadIdentifierException(this.RecaptureUnappliedFundID)
      }
      initializer.RecaptureUnappliedFund = recaptureUnappliedFund
    }

    performEarlyItemHandling(initializer, billingInstruction)

    // rate overrides are valid only for policy period charges
    if (billingInstruction typeis PlcyBillingInstruction) {
      addOverrides(initializer)
    }
  }

  /**
   * Populate all fields based on the given charge. {@link gw.webservice.policycenter.bc1000.entity.types.complex.ChargeInfo#getRecaptureUnappliedFundID()} is not populated.
   *
   * @param charge The charge to copy from
   */
  function copyChargeInfo(charge : Charge) {
    this.Amount = charge.Amount.toString()
    this.ChargePatternCode = charge.ChargePattern.ChargeCode
    this.ChargeGroup = charge.ChargeGroup
    this.WrittenDate = charge.WrittenDate.XmlDateTime
  }

  function addChargeCommissionRateOverrideInfo(chargeCommissionRateOverrideInfo : ChargeCommissionRateOverrideInfo) {
    var elem = new ChargeInfo_ChargeCommissionRateOverrideInfos()
    elem.$TypeInstance = chargeCommissionRateOverrideInfo
    this.ChargeCommissionRateOverrideInfos.add(elem)
  }

  function addBreakdownItemInfo(breakdownItemInfo : BreakdownItemInfo) : ChargeInfo_BreakdownItemInfos {
    var itemInfo = new ChargeInfo_BreakdownItemInfos()
    itemInfo.$TypeInstance = breakdownItemInfo
    this.BreakdownItemInfos.add(itemInfo)
    return itemInfo
  }

  private function performEarlyItemHandling(initializer : ChargeInitializer, billingInstruction : BillingInstruction) {
    if (this.EarlyItemHandling == EarlyItemHandlingType.Billfirstitemtoday) {
      initializer.billFirstEntryToday()
    } else if (this.EarlyItemHandling == EarlyItemHandlingType.Billduefirstitemtoday) {
      initializer.billAndDueFirstEntryToday()
    } else if (this.EarlyItemHandling == EarlyItemHandlingType.Reinstatementbilldueearlyitemstoday) {
      if (!(billingInstruction typeis Reinstatement)) {
        throw new SOAPServerException(DisplayKey.get("Webservice.Error.ReinstatementEarlyItemForNonReinstatementBI",
            (typeof billingInstruction)))
      }
      initializer.reinstatementBillDueEarlyItemsToday()
    } else if (this.EarlyItemHandling != null) {
      throw new SOAPServerException(DisplayKey.get("Webservice.Error.InvalidEarlyItemHandling", this.EarlyItemHandling))
    }
  }

  private function addBreakdownItems(initializer : ChargeInitializer) {
    this.BreakdownItemInfos.each(\itemInfo -> {
      try {
        var breakdownItem = initializer.addBreakdownItem()
        breakdownItem.Amount = new MonetaryAmount(itemInfo.Amount)
        breakdownItem.Description = itemInfo.Description
        itemInfo.Categories.each(\categoryInfo -> {
          var categoryType = createOrUpdateCategoryType(categoryInfo.CriteriaCode, categoryInfo.CriteriaName,
              categoryInfo.TranslatedCriteriaNames, breakdownItem.Bundle)
          var category = createOrUpdateCategory(categoryType, categoryInfo, breakdownItem.Bundle)
          breakdownItem.addCategory(category)
        })
      } catch (e : Exception) {
        throw new SOAPSenderException(DisplayKey.get("Webservice.Error.ChargeInfo.ErrorCreatingBreakdownItem", e.getMessage()))
      }
    })
  }

  private function createOrUpdateCategory(categoryType : ChargeBreakdownCategoryType, categoryInfo : BreakdownItemInfo_Categories,
                                          bundle : Bundle) : ChargeBreakdownCategory {
    var category : ChargeBreakdownCategory
    try {
      category = bundle.add(WebserviceEntityLoader.loadBreakdownCategory(categoryType, categoryInfo.CategoryIdentifier))
    } catch (e : Exception) {
      category = categoryWithIdentifierInBundle(categoryType, categoryInfo.CategoryIdentifier, bundle)
          ?: createCategory(categoryType, categoryInfo, bundle)
    }
    updateCategoryNames(category, categoryInfo)
    return category
  }

  private function createCategory(
      categoryType : ChargeBreakdownCategoryType, categoryInfo : BreakdownItemInfo_Categories, bundle : Bundle) : ChargeBreakdownCategory {
    var category = new ChargeBreakdownCategory(bundle){
      : CategoryType = categoryType,
      : CategoryIdentifier = categoryInfo.CategoryIdentifier,
      : CategoryName =  categoryInfo.CategoryName
    }
    return category;
  }

  private function categoryWithIdentifierInBundle(
      categoryType : ChargeBreakdownCategoryType, categoryIdentifier : String, bundle : Bundle) : ChargeBreakdownCategory {
    return bundle.getBeansByRootType(ChargeBreakdownCategory.TYPE.get())
        .firstWhere(\category -> (category as ChargeBreakdownCategory).CategoryType == categoryType
            && (category as ChargeBreakdownCategory).CategoryIdentifier == categoryIdentifier) as ChargeBreakdownCategory
  }

  private function updateCategoryNames(category : ChargeBreakdownCategory, categoryInfo : BreakdownItemInfo_Categories) {
    categoryInfo.getTranslatedCategoryNames().forEach(\translatedName -> {
      var language = LocaleUtil.toLanguage(LanguageType.get(translatedName.LanguageCode))
      if (language != null) {
        LocaleUtil.runAsCurrentLanguage(language, \-> category.setCategoryName(translatedName.Translation))
      }
    })
    category.setCategoryName(categoryInfo.CategoryName)
  }

  private function createOrUpdateCategoryType(categoryTypeCode : String, categoryTypeName : String,
                                              translatedNames : List<BreakdownCategoryInfo_TranslatedCriteriaNames>,
                                              bundle : Bundle) : ChargeBreakdownCategoryType {
    var categoryType : ChargeBreakdownCategoryType
    try {
      categoryType = bundle.add(WebserviceEntityLoader.loadBreakdownCategoryTypeByCode(categoryTypeCode))
    } catch (e : Exception) {
      categoryType = categoryTypeWithCodeInBundle(categoryTypeCode, bundle)
          ?: createCategoryType(categoryTypeCode, categoryTypeName, bundle)
    }
    updateCategoryTypeNames(categoryType, categoryTypeName, translatedNames)
    return categoryType
  }

  private function createCategoryType(categoryTypeCode : String, categoryTypeName : String, bundle : Bundle) : ChargeBreakdownCategoryType {
    return new ChargeBreakdownCategoryType(bundle) {
      : Code = categoryTypeCode,
      : Name = categoryTypeName
    }
  }

  private function categoryTypeWithCodeInBundle(categoryTypeCode : String, bundle : Bundle) : ChargeBreakdownCategoryType {
    return bundle.getBeansByRootType(ChargeBreakdownCategoryType.TYPE.get())
        .firstWhere(\categoryType -> (categoryType as ChargeBreakdownCategoryType).Code == categoryTypeCode) as ChargeBreakdownCategoryType
  }

  private function updateCategoryTypeNames(categoryType : ChargeBreakdownCategoryType, name : String,
                                           translatedNames : List<BreakdownCategoryInfo_TranslatedCriteriaNames>) {
    translatedNames.forEach(\translatedName -> {
      var language = LocaleUtil.toLanguage(LanguageType.get(translatedName.LanguageCode))
      if (language != null) {
        LocaleUtil.runAsCurrentLanguage(language, \-> categoryType.setName(translatedName.Translation))
      }
    })
    categoryType.setName(name)
  }

  private function addOverrides(chargeInitializer : ChargeInitializer) {
    for (entry in this.ChargeCommissionRateOverrideInfos.partition(\info -> info.Role).entrySet()) {
      // check that there are no duplicate overrides for the same role
      if (entry.Value.size() > 1) {
        throw new SOAPServerException(DisplayKey.get("Webservice.Error.DuplicateRolesInCommissionRateOverrides", entry.Key))
      }
      // check that the role is valid
      var role = entry.Key
      if (PolicyRole.get(role) == null) {
        throw new SOAPServerException(DisplayKey.get("Webservice.Error.InvalidRoleInCommissionRateOverride", role))
      }
    }

    for (overrideInfo in this.ChargeCommissionRateOverrideInfos) {
      if (overrideInfo.Rate != null) {
        if (overrideInfo.Amount != null) {
          throw new SOAPServerException(DisplayKey.get("Webservice.Error.InvalidOverrideHasBothRateAndAmount", overrideInfo.Role))
        }
        chargeInitializer.overrideCommissionRate(PolicyRole.get(overrideInfo.Role), overrideInfo.Rate)
      } else if (overrideInfo.Amount != null) {
        chargeInitializer.overrideCommissionAmount(PolicyRole.get(overrideInfo.Role), new MonetaryAmount(overrideInfo.Amount))
      } else {
        throw new SOAPServerException(DisplayKey.get("Webservice.Error.InvalidOverrideHasNeitherRateNorAmount", overrideInfo.Role))
      }
    }
  }
}
