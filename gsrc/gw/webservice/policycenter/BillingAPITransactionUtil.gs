package gw.webservice.policycenter

uses gw.api.domain.accounting.BillingInstructionUtil
uses gw.api.domain.accounting.ManagedContactsUtil
uses gw.pl.persistence.core.Bundle

/**
 * Class used to encapsulate some of the BillingAPI operations
 *
 */
@Export
class BillingAPITransactionUtil {

  /**
   * Method used by the BillingAPI to process transactions that DO NOT CREATE Billing Instructions. Some examples of these
   * transactions are updateAccount, updateProducer, updateProducerCode etc. The given block will be executed in a new
   * bundle and will manage contacts sync to avoid sending contact updates to CM since PC is already notifying CM
   * about Contact changes.
   * @param call            The block of code that will be executed
   * @param transactionId   The transactionId of the operation being processed
   * @return the value returned by the given block
   */
  static reified function executeTransaction<T>(call: block(b: Bundle): T, transactionId: String): T {
    return ManagedContactsUtil.runWithNewBundleWithManagedContacts(call, transactionId)
  }

  /**
   * Method used by the BillingAPI to process transactions that CREATE Billing Instructions. Some example of these
   * transactions are issuePolicyPeriod, changePolicyPeriod, cancelPolicyPeriod etc. The given block will be committed
   * in a new bundle and will manage contacts sync to avoid sending contact updates to CM since PC is already notifying
   * CM about Contact changes.
   * @param call            The block of code containing the BI that will be committed
   * @param transactionId   The transactionId of the operation being processed
   * @return the BillingInstruction PublicID
   */
  public static function executeBillingInstruction<T>(call: block(b: Bundle): BillingInstruction, transactionId: String): String {
    var bi = BillingInstructionUtil.commitWithNewBundleWithManagedContacts(call, transactionId)
    return bi.PublicID
  }
}