package gw.webservice.bc.bc1000

uses gw.pl.currency.MonetaryAmount
uses gw.xml.ws.annotation.WsiExportable

@Export
@WsiExportable("http://guidewire.com/bc/ws/gw/webservice/bc/bc1000/BatchPaymentDTO")
final class BatchPaymentDTO {

   private var _batchNumber: String as BatchNumber
   private var _amount: MonetaryAmount as Amount
   private var _payments: BatchPaymentEntryDTO[] as Payments
}