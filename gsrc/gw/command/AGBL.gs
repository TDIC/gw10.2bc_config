package gw.command

uses gw.api.database.Query
uses gw.api.databuilder.AgencyBillPaymentFixtureBuilder
uses gw.api.databuilder.AgencyBillPromiseFixtureBuilder
uses gw.api.databuilder.BCDataBuilder
uses gw.api.web.payment.PaymentInstrumentFactory
uses gw.bc.archive.BCArchiveRunCommandUtil
uses gw.pl.currency.MonetaryAmount

@Export
class AGBL extends BCBaseCommand {

  function createAgencySuspPmntItems(){
    var producer = new gw.command.Producer().withAgencyBill()
    producer.AccountRep = User.util.CurrentUser
    var agencyBillCycle = producer.AgencyBillCyclesSortedByStatementDate.get( 0 )
    new AgencyBillPaymentFixtureBuilder()
        .withProducer(agencyBillCycle.getProducer())
        .withAmount(200bd.ofCurrency(agencyBillCycle.Currency))
        .withSuspenseItem("11111", new MonetaryAmount(250bd, agencyBillCycle.Currency), new MonetaryAmount(50bd, agencyBillCycle.Currency))
        .createFixture()
    pcf.DesktopAgencyItems.go()
  }

  function withSavedSuspensePromiseItems(){
    var producer = getCurrentProducer()
    // set the current user as AccountRep so that the promise items appear on the Desktop>My Agency Items page
    producer.AccountRep = User.util.CurrentUser
    var statementInvoice = producer.AgencyBillCyclesSortedByStatementDate.first().StatementInvoice
    var firstInvoiceItem = new InvoiceItem[]{statementInvoice.InvoiceItemsWithoutCommissionRemainder[0]}
    var promise = new AgencyBillPromiseFixtureBuilder()
        .withAmount(237bd.ofCurrency(producer.Currency))
        .withFullDistributionFor(firstInvoiceItem)
        .withSuspenseItem("Policy-" + BCDataBuilder.createRandomWordPair(),
            (243bd + new Random().nextInt(100)).ofCurrency(producer.Currency),
            (36bd + new Random().nextInt(40)).ofCurrency(producer.Currency))
        .createUnexecutedFixture()
        .AgencyBillPromise;
    promise.Bundle.commit()
    pcf.ProducerDetail.go(producer)
  }

  /**
   * Create a slushy Agency Bill Renewal -- it shares a frozen invoice with the prior PolicyPeriod which is archived.
   *
   * Create a PolicyPeriod
   * Advance Clock 10 months (not a full year)
   * Renew the PolicyPeriod, that places item(s) for the renewed PolicyPeriod on invoice(s) that contain items from the original PolicyPeriod.
   * Pay the renewal's invoice items which overlap with the prior PolicyPeriod.
   * Close and archive the prior Policy Period to make the Renewed policy period slushy.
   */
  function withSlushyRenewal() {
    var renewal = BCArchiveRunCommandUtil.createSlushyRenewal(null, TC_AGENCYBILL)
    pcf.PolicySummary.go(renewal)
  }

  function toAgencyProducer() {
    var producer = getLastAGBLProducer()
    pcf.ProducerDetail.go(producer)
  }

  function toAgencyBillCycles() {
    var producer = getLastAGBLProducer()
    pcf.ProducerAgencyBillCycles.go(producer)
  }

  function toAgencyBillPolicyDetails(){
    var producer = getLastAGBLProducer()
    var firstPolicyPeriod = producer.ProducerCodes[0].PolicyCommissions.FirstResult.PolicyPeriod
    pcf.AgencyBillPolicyDetailsPopup.push(firstPolicyPeriod)
  }

  function toItemHistory(){
    var producer = getLastAGBLProducer()
    var firstInvoiceItem = producer.AgencyBillCycles.sortBy(\ a -> a.StatementInvoice.EventDate)[0]
      .StatementInvoice.InvoiceItems.sortBy(\ i -> i.EventDate)[0]
    pcf.InvoiceItemHistoryPopup.push(firstInvoiceItem)
  }

  function toCurrentAgencyPaymentWizard() {
    var producer = getCurrentProducer()

    pcf.AgencyDistributionWizard.go(producer, gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum.PAYMENT)
  }
  
  function toStatementDetails(){
    var producer = getLastAGBLProducer()
    var cycle = producer.AgencyBillCyclesSortedByStatementDate.first()
    pcf.AgencyBillStatementDetail.go( cycle )
  }


  function makeStatementBilledAndDue() : String {
    addMonths(1)
    runBatchProcess(BatchProcessType.TC_STATEMENTBILLED)
    runBatchProcess(BatchProcessType.TC_STATEMENTDUE)
    return "Today is : " + currentDate()
  }

  function payWithException() {
    var invoiceNumber : int = Arguments[0].asInt()
    var producer = getCurrentProducer()
    var agencyBillCycles = producer.AgencyBillCycles.sortBy(\ a -> a.StatementInvoice.EventDate)
    var statementInvoice = agencyBillCycles[invoiceNumber].StatementInvoice
    var currency = producer.Currency
    var invoiceItemWithException = statementInvoice.InvoiceItemsWithoutCommissionRemainder.single()
    var invoiceItemWithFullPayment = statementInvoice.InvoiceItems.where(\elt -> elt != invoiceItemWithException)
    new AgencyBillPaymentFixtureBuilder()
        .withAmount(statementInvoice.NetAmountDue)
        .withPaymentInstrument(PaymentInstrumentFactory.CashPaymentInstrument)
        .withExceptionInvoiceItems(100bd.ofCurrency(currency), 0bd.ofCurrency(currency), {invoiceItemWithException})
        .withFullDistributionFor(invoiceItemWithFullPayment)
        .createFixture()
    pcf.AgencyBillExceptions.go(producer)
  }
  
  function makeOneStatementBilled() : String {
    var producer = getCurrentProducer()
    var cycles = producer.AgencyBillCycles
    if (cycles.IsEmpty) {
      return "This Producer (" + producer.DisplayName + ") has no Agency Bill Cycles."
    }
    var firstPlannedInvoice = cycles
      .sortBy(\ a -> a.StatementInvoice.EventDate)
      .firstWhere(\ a -> a.StatementInvoice.Status == InvoiceStatus.TC_PLANNED).StatementInvoice
    var nextInvoiceDate = firstPlannedInvoice.EventDate
    var newDate = nextInvoiceDate.addDays(1)
    setDate(newDate)
    runBatchProcess(BatchProcessType.TC_STATEMENTBILLED)
    pcf.ProducerAgencyBillCycles.go(producer)
    return "Clock was advanced to " + newDate + " and StatementBilled batch process has been run."
  }
  
  function gotoAgencyPaymentWizard() {
    var producer = getCurrentProducer()
    pcf.AgencyDistributionWizard.go(producer, gw.agencybill.AgencyDistributionWizardHelper.DistributionTypeEnum.PAYMENT)
  }

  protected function getLastAGBLProducer() : Producer {
    var producers =  Query.make(Producer).select()
    var producer = producers.iterator().toList()
      .sortByDescending(\ t -> t.CreateTime)
      .firstWhere(\ t -> t.AgencyBillCycles.length > 0)
    return producer
  }
  
}
