
package gw.command

uses com.guidewire.bc.domain.invoice.impl.StatementBilledWorkQueue
uses entity.StatementBilledWorkItem

@Export
class PartialBatch extends BaseCommand {

  construct() {
    super()
  }

  @Argument("limit")
  function statementBilled() {
    var queue = new StatementBilledWorkQueue()
    var worker = queue.createWorker()
    var creator = queue.WorkItemCreator
    var targets = queue.getFinder(null).findTargets()
    var limit = Integer.parseInt(getArgumentAsString("limit"))
    var count = 0
    while(targets.hasNext() and count < limit) {
      var target = targets.next()
      var workItem : StatementBilledWorkItem
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        workItem = creator.createWorkItem(target , null, bundle)
      })
      worker.processWorkItem(workItem)
      count++
    }
  }
}
