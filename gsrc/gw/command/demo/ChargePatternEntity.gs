package gw.command.demo

uses gw.api.databuilder.ImmediateChargePatternBuilder
uses gw.command.BaseCommand
uses gw.transaction.Transaction
uses entity.ChargePattern

@Export
class ChargePatternEntity extends BaseCommand {
  construct() {
  }

  /**
  * Finds ChargePattern01 in database. <br>
  * If it finds one then it returns it. <br>
  * Else it creates and returns it.<br>
  */
  public static function getChargePattern01() : ChargePattern {
    var chargeCode = "NewBusinessFee"
    var cp = GeneralUtil.findChargePatternByChargeCode(chargeCode)
    if (cp == null) {
      Transaction.runWithNewBundle( \ bundle -> 
        {
          cp = new ImmediateChargePatternBuilder()
          .withCategory(TC_FEE)
          .withChargeCode(chargeCode)
          .withPriority(TC_MEDIUM)
          .withChargeName("New Business Fee")
          .withInvoiceTreatment(TC_SINGLEDEPOSIT)
          .forPolicyPeriod()
          .create(bundle)
        }
      )
    }
    return cp
  }
}