package gw.entity

uses gw.api.locale.DisplayKey

@Export
enhancement GWTAccountPatternEnhancement : entity.TAccountPattern {

  /**
   * Utility for finding the correct display keys for a given TAccountPattern.
   */
  public static function getDisplayKeyFor(final s : String) : String {
    var displayKeyMap = new HashMap<String, String>()

    displayKeyMap.put("Archive Commission Rollup", DisplayKey.get("Java.TAccount.ArchiveCommissionRollup"))
    displayKeyMap.put("Archive Commission Retained Rollup", DisplayKey.get("Java.TAccount.ArchiveCommissionRetainedRollup"))
    displayKeyMap.put("Archive Gross Rollup", DisplayKey.get("Java.TAccount.ArchiveGrossRollup"))
    displayKeyMap.put("Cash", DisplayKey.get("Java.TAccount.Cash"))
    displayKeyMap.put("Unapplied", DisplayKey.get("Java.TAccount.Unapplied"))
    displayKeyMap.put("Commissions Expense", DisplayKey.get("Java.TAccount.CommissionsExpense"))
    displayKeyMap.put("Commissions Payable", DisplayKey.get("Java.TAccount.CommissionsPayable"))
    displayKeyMap.put("Commissions Reserve", DisplayKey.get("Java.TAccount.CommissionsReserve"))
    displayKeyMap.put("Commissions Advance", DisplayKey.get("Java.TAccount.CommissionsAdvance"))
    displayKeyMap.put("Negative Write-Off", DisplayKey.get("Java.TAccount.NegativeWriteOff"))
    displayKeyMap.put("Collateral Reserve", DisplayKey.get("Java.TAccount.CollateralReserve"))
    displayKeyMap.put("Collateral Held", DisplayKey.get("Java.TAccount.CollateralHeld"))
    displayKeyMap.put("Positive Commission Adjustment", DisplayKey.get("Java.TAccount.PositiveCommissionAdjustment"))
    displayKeyMap.put("Negative Commission Adjustment", DisplayKey.get("Java.TAccount.NegativeCommissionAdjustment"))
    displayKeyMap.put("Suspense", DisplayKey.get("Java.TAccount.Suspense"))
    displayKeyMap.put("Write-Off Expense", DisplayKey.get("Java.TAccount.WriteOffExpense"))
    displayKeyMap.put("Goodwill Credit", DisplayKey.get("Java.TAccount.GoodWillCredit"))
    displayKeyMap.put("Interest Credit", DisplayKey.get("Java.TAccount.InterestCredit"))
    displayKeyMap.put("Collections Credit", DisplayKey.get("Java.TAccount.CollectionsCredit"))
    displayKeyMap.put("Other Credit", DisplayKey.get("Java.TAccount.OtherCredit"))
    displayKeyMap.put("Designated Unapplied", DisplayKey.get("Java.TAccount.DesignatedUnapplied"))
    displayKeyMap.put("Taxes", DisplayKey.get("Java.TAccount.Taxes"))

    return displayKeyMap.get(s)
  }
}
