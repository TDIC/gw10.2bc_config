package gw.admin.paymentplan

uses gw.api.locale.DisplayKey

@Export
enum When {
    BEFORE(\ -> DisplayKey.get("Web.InstallmentViewHelper.WhenEnum.Before")),
    AFTER(\ -> DisplayKey.get("Web.InstallmentViewHelper.WhenEnum.After"))

    private var _displayResolver : block() : String as DisplayResolver
    override function toString() : String {
      return DisplayResolver()
    }

    private construct(resolveDisplayKey() : String) {
      _displayResolver = resolveDisplayKey
    }
}