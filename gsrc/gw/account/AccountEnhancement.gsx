package gw.account

uses com.google.common.collect.Lists

@Export
enhancement AccountEnhancement: Account {

  /**
   * Returns the distribution limit type that should be used for this account
   *
   * @return the distribution limit type that should be used for this account
   */
  property get DistributionLimitTypeFromPlan(): DistributionLimitType {
    if (this.PaymentAllocationPlan == null) return DistributionLimitType.TC_OUTSTANDING

    var planFilters = this.PaymentAllocationPlan.DistributionFilters

    if (planFilters.contains(DistributionFilterType.TC_NEXTPLANNEDINVOICE)) {
      return DistributionLimitType.TC_NEXTINVOICE
    } else if (planFilters.contains(DistributionFilterType.TC_BILLEDORDUE)) {
      return DistributionLimitType.TC_OUTSTANDING
    } else if (planFilters.contains(DistributionFilterType.TC_PASTDUE)) {
      return DistributionLimitType.TC_PASTDUE
    }

    return DistributionLimitType.TC_UNDERCONTRACT
  }

  // Designated Unapplieds Sorted

  property get UnappliedFundsSortedByDisplayName() : List<UnappliedFund> {
    var unappliedFundsOrdered = {this.DefaultUnappliedFund}
    unappliedFundsOrdered.addAll(this.DesignatedUnappliedFundsSortedByDisplayName)
    return unappliedFundsOrdered
  }

  property get DesignatedUnappliedFunds() : List<UnappliedFund> {
    return Lists.newArrayList(this.UnappliedFunds
        .where(\unappliedFund -> !unappliedFund.DefaultForAccount))
  }

  property get DesignatedUnappliedFundsSortedByDisplayName() : List<UnappliedFund> {
    return this.UnappliedFunds
        .where(\unappliedFund -> !unappliedFund.DefaultForAccount)
        .orderBy(\unappliedFund -> unappliedFund.DisplayName)
  }

  property get HasDesignatedUnappliedFund() : Boolean {
    return this.UnappliedFunds
        .hasMatch(\unappliedFund -> !unappliedFund.DefaultForAccount)
  }

}
