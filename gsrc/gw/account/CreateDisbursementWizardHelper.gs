package gw.account

uses org.slf4j.LoggerFactory
uses gw.pl.currency.MonetaryAmount

/**
 * Helper for the disbursement wizards
 */
@Export
class CreateDisbursementWizardHelper {
  private static final var _logger = LoggerFactory.getLogger("Application.Invoice")
  var _disbursement : AccountDisbursement

  construct(disbursement : AccountDisbursement) {
    _disbursement = disbursement
  }

  property get TargetUnapplied() : UnappliedFund {
    return _disbursement.UnappliedFund
  }

  property set TargetUnapplied(unapplied : UnappliedFund) {
    if (_disbursement.UnappliedFund != unapplied) {
      _disbursement.setUnappliedFundsAndFields(unapplied)
    }
  }

  static function createDisbursement(account : Account) : AccountDisbursement {
    var disb = new AccountDisbursement(account.Currency)
    disb.setUnappliedFundsAndFields(account.DefaultUnappliedFund)
    return disb
  }

  // SujitN : GBC-429 - Return Premium Application
  static function createDisbursement(policyPeriod : PolicyPeriod, amount : MonetaryAmount) : AccountDisbursement {
    var disbursement = new AccountDisbursement(policyPeriod.Account.Currency)
    var unappliedFund = policyPeriod.OverridingInvoiceStream != null ? policyPeriod.OverridingInvoiceStream.UnappliedFund
        : policyPeriod.Policy.getDesignatedUnappliedFund(policyPeriod.Account)
    disbursement.setUnappliedFundsAndFields(unappliedFund)
    disbursement.Amount  = amount
    disbursement.DueDate = gw.api.util.DateUtil.currentDate()
    disbursement.Status  = DisbursementStatus.TC_AWAITINGAPPROVAL
    disbursement.Reason  = Reason.TC_AUTOMATIC
    disbursement.Address = policyPeriod.Account.PrimaryPayer.Contact.PrimaryAddress.toString()
    disbursement.MailTo  = policyPeriod.Account.PrimaryPayer.Contact.toString()
    disbursement.PayTo   = policyPeriod.Account.PrimaryPayer.Contact.toString()
    disbursement.PaymentInstrument = policyPeriod.Account.DefaultPaymentInstrument
    return disbursement
  }

  static function initiateApprovalActivityIfUserLacksAuthority_TDIC(disbursement : Disbursement) {
    var activity = disbursement.getOpenApprovalActivity()
    //Creating Disbursement Approval acitivty.
    if(activity == null) {
      disbursement.getApprovalHandler()?.approvalRequired(null)
      activity = disbursement.getOpenApprovalActivity()
    }
    //Assigning activity to designated group and queue.
    var groupName = gw.command.demo.GeneralUtil.findGroupByUserName("Approve Disbursements")
    if (activity?.assignGroup(groupName)) {
      var queueName = activity.AssignedGroup.getQueue("Approve Disbursements")
      if (queueName != null) {
        activity.assignActivityToQueue(queueName, groupName)
      }
    }
  }

  /**
   * Helper function to create Disbursement and process/disburse the same. Changes are as part of JIRA: GWPS-113.
   *
   * @param policyPeriod - PolicyPeriod associated with Disbursement.
   * @param amountToBeDisbursed -  Amount to be disbursed.
   */
  static function createAndProcessDisbursement_TDIC(policyPeriod : PolicyPeriod, amountToBeDisbursed : MonetaryAmount) {
    var disbursement = createDisbursement(policyPeriod, amountToBeDisbursed)
    _logger.info("CreateDisbursementWizardHelper :: Create and Process Disbursement " + disbursement.DisbursementNumber)
    disbursement = policyPeriod.getBundle().add(disbursement)
    initiateApprovalActivityIfUserLacksAuthority_TDIC(disbursement)
  }
}