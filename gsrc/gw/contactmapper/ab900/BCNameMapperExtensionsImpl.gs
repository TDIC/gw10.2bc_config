package gw.contactmapper.ab900

uses gw.webservice.contactapi.NameMapper

@Export
@Deprecated(:value="900 inter-app integration packages will be removed in BC11.", :version="BC 10.0.0")
class BCNameMapperExtensionsImpl implements IBCNameMapperExtensions {

  override property get Instance(): NameMapper {
    return gw.contactmapper.ab900.BCNameMapper.Instance
  }
}