package gw.acc.activityenhancement.subtypes

uses gw.acc.activityenhancement.IApprovalActivity
uses com.guidewire.bc.domain.approval.BCApprovalHandler

class FundsTransferReversalApprovalActivityApprovalMethods
    implements IApprovalActivity {

  var _activity : FundsTransferReversalApprovalActivity

  construct(activity: FundsTransferReversalApprovalActivity) {
    _activity = activity
  }

  override function associateRelatedEntities() {
    // Desktop -> Actions -> New Transaction - > Transfer Funds Reversal
    var fundsTransfer =
        _activity.FundsTransferReversal.FundsTransferTransaction.FundsTransfer

    // this will be either Unapplied or Producer, and the other will be null
    _activity.Account = fundsTransfer.TargetUnapplied.Account
    if (fundsTransfer.TargetUnapplied.Policy != null) {
      var effectivePolicyPeriod =
          fundsTransfer.TargetUnapplied.Policy.PolicyPeriods
              .firstWhere(\ period -> period.Status == TC_INFORCE)
      _activity.PolicyPeriod = effectivePolicyPeriod
    }
    _activity.Producer_Ext = fundsTransfer.TargetProducer
  }

  override property get ApprovalHandler() : BCApprovalHandler {
    return _activity.ApprovalHandler
  }
}