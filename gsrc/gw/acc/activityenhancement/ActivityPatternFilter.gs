package gw.acc.activityenhancement

uses com.guidewire.pl.system.database.Query
uses com.guidewire.pl.system.filters.QueryBasedQueryFilter
uses gw.entity.IQueryColumn

class ActivityPatternFilter implements QueryBasedQueryFilter {

  var _value:ActivityPattern
  var _column = Activity.Type.TypeInfo.getProperty( "ActivityPattern" ) as IQueryColumn

  construct(value:ActivityPattern) {
    _value = value
  }
  
  override function applyFilter(obj:Object):boolean {
    return (obj as Activity).ActivityPattern == _value
  }
  
  override function filterQuery(query : Query) : Query {
    query.PrimaryTableObject.Restriction.compareEquals(_column, _value, true)
    return query
  }
  
  override function toString():String {
    return _value.DisplayName
  }

}
