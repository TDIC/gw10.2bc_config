package gw.acc.acct360.accountview.plugin

uses gw.plugin.preupdate.IPreUpdateHandler
uses gw.api.preupdate.PreUpdateContext
uses gw.pl.persistence.core.Bundle

/**
 * Account 360 Accelerator
 * This is used to create the pre-update handler needed to support the account 360 functionality.
 * It captures the transactions necessary.
 *
 */
class Account360 implements IPreUpdateHandler {

  override function executePreUpdate(context: PreUpdateContext) {
    var bundle = bundleOf(context)
    if (bundle == null) {
      return
    }
    if (!bundle.ReadOnly) {
      TransactionCatcherCallback.getOrCreateCallbackFor(bundle)
    }
  }

  private function bundleOf(context : PreUpdateContext) : Bundle {
    var bundle = context.UpdatedBeans.first().Bundle
    return (bundle == null ? context.InsertedBeans.first().Bundle : bundle)
  }

}