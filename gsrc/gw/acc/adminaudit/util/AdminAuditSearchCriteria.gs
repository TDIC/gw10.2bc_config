package gw.acc.adminaudit.util

uses org.joda.time.DateTime

/*********************************************************************************
 * 
 * Purpose:  Data structure used to search for admin audit records.
 *  
 *********************************************************************************/
class AdminAuditSearchCriteria {

  construct() {  }

  var searchStartDate : Date as StartDate
  var searchEndDate : Date as EndDate
  var searchEntityName : String as EntityName
  var searchModifiedBy : String as ModifiedBy
}
