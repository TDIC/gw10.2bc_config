package gw.search

uses entity.ChargePattern
uses entity.Writeoff
uses gw.api.database.IQuery
uses gw.api.database.IQueryBeanResult
uses gw.api.database.InOperation
uses gw.api.database.Query
uses gw.api.locale.DisplayKey
uses gw.api.path.Paths
uses gw.api.restriction.RestrictionBuilder
uses gw.api.util.DisplayableException
uses gw.pl.currency.MonetaryAmount

uses java.io.Serializable
uses java.util.Date

/**
 * Criteria used to search for Write Offs in WriteoffSearchDV.
 */
@Export
public class WriteoffSearchCriteria implements Serializable {

  var _reason: WriteoffReason as Reason
  var _minAmount: MonetaryAmount as MinAmount
  var _maxAmount: MonetaryAmount as MaxAmount
  var _earliestDate: Date as EarliestDate
  var _latestDate: Date as LatestDate
  var _chargePattern: ChargePattern as ChargePattern
  var _account: Account as Account
  var _policyPeriod: PolicyPeriod as PolicyPeriod
  var _currency: Currency as Currency

  public construct(account: Account) {
    if (account != null) {
      Account = account
      Currency = account.Currency
    }
  }

  public function performSearch(): IQueryBeanResult<Writeoff> {
    // Report for user convenience: Need not be a strict error since the search
    // will work correctly with PolicyPeriod taking precedence and ignoring
    // Account if it not the owning one.
    verifyPolicyPeriodIsOnAccount()

    var query = (ChargePattern != null)
        ? populateQueryFromCriteria(ChargeGrossWriteoff)
        : populateQueryFromCriteria(Writeoff)

    return query.select()
  }


  public function blankMinimumAndMaximumFields() {
    MinAmount = null
    MaxAmount = null
  }


  private function populateQueryFromCriteria(writeOffType: Type<Writeoff>): IQuery<Writeoff> {
    var writeoffRestrictionBuilder = makeBaseCriteriaQuery(writeOffType)

    // If getTargetPolicyPeriod is set, then only return writeoffs that have that PolicyPeriod as TAccountContainer.
    // Note: This is the same case as if getAccount and getTargetPolicyPeriod are both set.
    if (PolicyPeriod != null) {
      writeoffRestrictionBuilder.compare(Paths.make(Writeoff#TAccountContainer), Equals, PolicyPeriod)
    } else if (Account != null) {
      // The other case is if getAccount is set but getTargetPolicyPeriod isn't.
      writeoffRestrictionBuilder.compare(Paths.make(Writeoff#TAccountContainer), Equals, Account)

      // include (through UNION) Account's PolicyPeriods' Writeoffs...
      var accountPoliciesSubselect = Query.make(entity.Policy)
      accountPoliciesSubselect.compare(Policy#Account, Equals, Account)

      var policyPeriodsSubselect = Query.make(entity.PolicyPeriod)
      policyPeriodsSubselect.subselect(entity.PolicyPeriod#Policy, CompareIn, accountPoliciesSubselect, Policy#ID)

      var writeoffAccountPoliciesQuery = assembleBaseCriteriaQueryFromRestrictionBuilder(makeBaseCriteriaQuery(writeOffType))
      writeoffAccountPoliciesQuery
          .join(Writeoff#TAccountContainer)
          .cast(PolTAcctContainer)
          .subselect(PolTAcctContainer#ID, CompareIn,
              policyPeriodsSubselect,
              entity.PolicyPeriod#HiddenTAccountContainer)

      return assembleBaseCriteriaQueryFromRestrictionBuilder(writeoffRestrictionBuilder).union(writeoffAccountPoliciesQuery)
    }

    return assembleBaseCriteriaQueryFromRestrictionBuilder(writeoffRestrictionBuilder)
  }

  public property set PolicyPeriod(pp : PolicyPeriod) {
    if (pp != null and pp.isArchived()) {
      throw new DisplayableException(DisplayKey.get("Java.Search.PolicyPeriodIsArchivedError", pp))
    } else {
      _policyPeriod = pp
    }
  }

  private function assembleBaseCriteriaQueryFromRestrictionBuilder(writeOffRestrictionBuilder: RestrictionBuilder<Writeoff>): Query<Writeoff> {
    var result = writeOffRestrictionBuilder.create().makeQuery() as Query<Writeoff>
    filterOutWriteoffsWithPendingReversalsInUnapprovedState(result)
    return result
  }

  // Filter out writeoffs that have pending reversals in the 'unapproved' state
  private function filterOutWriteoffsWithPendingReversalsInUnapprovedState(query: Query) {
    final var pendingReversalsSubselect = Query.make(WriteoffReversal)
        .compare(WriteoffReversal#ApprovalStatus, Equals, ApprovalStatus.TC_UNAPPROVED)
    query.subselect(Writeoff#ID, InOperation.CompareNotIn, pendingReversalsSubselect, WriteoffReversal#Writeoff)
  }

  private function makeBaseCriteriaQuery(writeOffType: Type<Writeoff>): RestrictionBuilder<Writeoff> {
    var writeoffQuery = RestrictionBuilder.make(writeOffType)

    writeoffQuery.compare(Paths.make(Writeoff#Reversed), Equals, false)
    writeoffQuery.compare(Paths.make(Writeoff#ReversedLink), Equals, null)

    writeoffQuery.compare(Paths.make(Writeoff#ApprovalStatus), NotEquals, ApprovalStatus.TC_REJECTED)
    writeoffQuery.compare(Paths.make(Writeoff#ApprovalStatus), NotEquals, ApprovalStatus.TC_UNAPPROVED)

    if (Reason != null) {
      writeoffQuery.compare(Paths.make(Writeoff#Reason), Equals, Reason)
    }

    if (Currency != null) {
      writeoffQuery.compare(Paths.make(Writeoff#Currency), Equals, Currency)
    }

    if (MinAmount != null or MaxAmount != null) {
      writeoffQuery.between(Paths.make(Writeoff#Amount), MinAmount, MaxAmount)
    }

    if (EarliestDate != null or LatestDate != null) {
      writeoffQuery.between(Paths.make(Writeoff#ExecutionDate), EarliestDate, LatestDate)
    }

    if (ChargePattern != null) {
      writeoffQuery.compare(Paths.make(ChargeGrossWriteoff#ChargePattern), Equals, ChargePattern)
    }

    return writeoffQuery
  }

  private function verifyPolicyPeriodIsOnAccount() {
    if (Account != null and PolicyPeriod != null and not PolicyPeriod.Account.equals(Account)) {
      throw new DisplayableException(DisplayKey.get("Java.Search.PolicyIsNotOnAccountError", PolicyPeriod, Account))
    }
  }


}