package gw.search

uses gw.api.locale.DisplayKey

@Export
enum PaymentSearchType {
  ALL(\ -> DisplayKey.get("Web.PaymentSearchTypeEnum.All")),
  DIRECTBILL_AND_SUSPENSE(\ -> DisplayKey.get("Web.PaymentSearchTypeEnum.DirectBillAndSuspense")),
  AGENCYBILL(\ -> DisplayKey.get("Web.PaymentSearchTypeEnum.AgencyBill"))


  private var _displayResolver : block() : String as DisplayResolver
  override function toString() : String {
    return DisplayResolver()
  }

  private construct(resolveDisplayKey() : String) {
    _displayResolver = resolveDisplayKey
  }
}
