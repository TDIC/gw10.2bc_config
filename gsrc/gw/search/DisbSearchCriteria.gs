package gw.search

uses gw.api.database.IQueryBeanResult
uses gw.api.database.ISelectQueryBuilder
uses gw.api.database.Query
uses gw.api.util.DateUtil
uses gw.pl.currency.MonetaryAmount

uses java.io.Serializable
uses java.util.Date

/**
 * Base class for search criteria for disbursements
 */
@Export
abstract class DisbSearchCriteria<T extends DisbursementSearchView> implements Serializable {
  final static var SHOULD_IGNORE_CASE = true

  var _earliestIssueDate : Date as EarliestIssueDate  /* Earliest disbursement issue date to search */
  var _latestIssueDate : Date as LatestIssueDate      /* Latest disbursement issue date to search */
  var _minAmount : MonetaryAmount as MinAmount        /* The minimum amount to search for */
  var _maxAmount : MonetaryAmount as MaxAmount        /* The maximum amount to search for */
  var _checkNumber : String as CheckNumber            /* The check or EFT identifier */
  var _currency : Currency as Currency                /* The currency unit to restrict the search to */
  var _token : String as Token                        /* The Token associated with the PaymentInstrument that was used to make the PaymentRequest */
  var _payee : String as Payee                        /* The recipient of the disbursement */
  var _status : DisbursementStatus as Status          /* The status of the disbursement */
  var _method : PaymentMethod as Method               /* Method of payment */
  var _reason : Reason as Reason                      /* The reason for the disbursement */

  function performSearch(): IQueryBeanResult<T> {
    SearchHelper.checkForDateExceptions(EarliestIssueDate, LatestIssueDate)
    SearchHelper.checkForNumericExceptions(MinAmount, MaxAmount)
    var query = buildQuery()
    return query.select()
  }

  function buildQuery() : Query<T> {
    var query = Query.make(T)
    addQueryRestrictions(query)
    return query
  }

  protected function addQueryRestrictions(query : Query<T>) {
    restrictSearchByPayee(query)
    restrictSearchByStatus(query)
    restrictSearchByReason(query)
    restrictSearchByHasOutgoingPayment(query)
    restrictSearchByMinAndMaxAmount(query)
    restrictSearchByCurrency(query)

    restrictSearchByPaymentInstrument(query)
    restrictSearchByUserSecurityZone(query)
  }

  protected abstract function restrictSearchByUserSecurityZone(query : Query<T>)

  private function restrictSearchByHasOutgoingPayment(query: Query<T>): void {
    if (hasOutgoingPaymentCriteria()) {
      var outgoingPaymentQuery = query.join(Disbursement#OutgoingPayment)
      restrictSearchByDate(outgoingPaymentQuery)
      restrictSearchByCheckNumber(outgoingPaymentQuery)
    }
  }

  private function hasOutgoingPaymentCriteria(): boolean {
    return EarliestIssueDate != null || LatestIssueDate != null || CheckNumber != null
  }

  private function restrictSearchByDate(query : ISelectQueryBuilder<T>) {
    if (EarliestIssueDate != null || LatestIssueDate != null) {
      // If a latest date has been entered, make sure it is set to the end of the day so that the query
      // will return results after midnight that morning
      var endOfLatestIssueDate = LatestIssueDate != null ? DateUtil.endOfDay(LatestIssueDate) : LatestIssueDate
      query.between(OutgoingPayment#IssueDate, EarliestIssueDate, endOfLatestIssueDate)
    }
  }

  private function restrictSearchByCheckNumber(query : ISelectQueryBuilder<T>) {
    if (CheckNumber.NotBlank) {
      query.compare(OutgoingPayment#RefNumber, NotEquals, null)
      query.startsWith(OutgoingPayment#RefNumber, CheckNumber, SHOULD_IGNORE_CASE)
    }
  }

  private function restrictSearchByPayee(query : Query<T>) {
    if (Payee.NotBlank) {
      query.startsWith(Disbursement#PayTo, Payee, SHOULD_IGNORE_CASE)
    }
  }

  private function restrictSearchByStatus(query : Query<T>) {
    if(Status != null) {
      query.compare(Disbursement#Status, Equals, Status)
    }
  }

  private function restrictSearchByReason(query : Query<T>) {
    if(Reason != null) {
      query.compare(Disbursement#Reason, Equals, Reason)
    }
  }

  private function restrictSearchByCurrency(query : Query<T>) {
    if (Currency != null) {
      query.compare(Disbursement#Currency, Equals, Currency)
    }
  }

  private function restrictSearchByMinAndMaxAmount(query : Query<T>) {
    if (MinAmount != null || MaxAmount != null) {
      query.between(Disbursement#Amount, MinAmount, MaxAmount)
    }
  }

  private function hasPaymentInstrumentCriteria() : boolean {
    return (Method != null || Token != null)
  }

  private function restrictSearchByPaymentInstrument(query : Query<T>) {
    if(hasPaymentInstrumentCriteria()) {
      var paymentInstrumentQuery = query.join(Disbursement#PaymentInstrument)
      if(Token.NotBlank) {
        paymentInstrumentQuery.compare(PaymentInstrument#Token, Equals, Token)
      }
      if(Method != null) {
        paymentInstrumentQuery.compare(PaymentInstrument#PaymentMethod, Equals, Method)
      }
    }
  }

}