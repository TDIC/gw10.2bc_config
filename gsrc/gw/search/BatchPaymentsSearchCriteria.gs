package gw.search

uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses gw.api.util.CurrencyUtil
uses gw.api.util.DateUtil
uses gw.pl.currency.MonetaryAmount
uses typekey.Currency

/**
 * Criteria used to perform an batch payment search in the DesktopBatchPaymentsSearchDV.
 */
@Export
class BatchPaymentsSearchCriteria implements java.io.Serializable {

  private final static var SHOULD_IGNORE_CASE = true

  private var batchNumber: String as BatchNumber
  private var status: BatchPaymentsStatus as BatchStatus

  private var currency: Currency as Currency
  private var minAmount: MonetaryAmount as MinAmount
  private var maxAmount: MonetaryAmount as MaxAmount

  private var createdBy: User as CreatedBy
  private var lastEditedBy: User as LastEditedBy

  private var postedBy: User as PostedBy
  private var earliestPostedDate: Date as EarliestPostedDate
  private var latestPostedDate: Date as LatestPostedDate

  private var reversedBy: User as ReversedBy
  private var earliestReversedDate: Date as EarliestReversedDate
  private var latestReversedDate: Date as LatestReversedDate

  construct() {
  }

  public property get MonetaryAmountInputAvailable(): boolean {
    return this.Currency != null or CurrencyUtil.isSingleCurrencyMode()
  }

  public property get ReversedStatusChosen(): boolean {
    return BatchStatus == BatchPaymentsStatus.TC_REVERSED
  }

  public property get PostedStatusChosen(): boolean {
    return BatchStatus == BatchPaymentsStatus.TC_POSTED
  }

  /**
   * Performs an BatchPayment search using this object's fields to narrow down results.
   */
  function performSearch(): IQueryBeanResult<BatchPayment> {

    var query = Query.make(BatchPayment)

    if (BatchPaymentsStatus.TC_POSTED == BatchStatus) {
      restrictSearchByPostedDateRange(query)
      restrictSearchByUserWhoPosted(query)
    }

    if (BatchPaymentsStatus.TC_REVERSED == BatchStatus) {
      restrictSearchByReversedDateRange(query)
      restrictSearchByUserWhoReversed(query)
    }

    restrictSearchByBatchNumber(query)
    restrictSearchByStatus(query)

    restrictSearchByCreatedUser(query)
    restrictSearchByUserWhoLastEdited(query)

    restrictSearchByMinMaxAmount(query)
    restrictSearchByCurrency(query)

    return query.select()
  }

  private function restrictSearchByCurrency(query: Query) {
    if (Currency != null) {
      query.compare(BatchPayment#Currency, Equals, Currency)
    }
  }

  private function restrictSearchByUserWhoPosted(query: Query) {
    if (PostedBy != null) {
      query.compare(BatchPayment#PostedBy, Equals, PostedBy.ID)
    }
  }

  private function restrictSearchByUserWhoReversed(query: Query) {
    if (ReversedBy != null) {
      query.compare(BatchPayment#ReversedByUser, Equals, ReversedBy.ID)
    }
  }

  private function restrictSearchByReversedDateRange(query: Query) {
    SearchHelper.checkForDateExceptions(EarliestReversedDate, LatestReversedDate)
    if (EarliestReversedDate != null or LatestReversedDate != null) {
      // If a latest date has been entered, make sure it is set to the end of the day so that the query
      // will return results after midnight that morning
      query.between(BatchPayment#ReversalDate, EarliestReversedDate, endOfDayFor(LatestReversedDate));
    }
  }

  private function restrictSearchByPostedDateRange(query: Query) {
    SearchHelper.checkForDateExceptions(EarliestPostedDate, LatestPostedDate)
    if (EarliestPostedDate != null or LatestPostedDate != null) {
      // If a latest date has been entered, make sure it is set to the end of the day so that the query
      // will return results after midnight that morning
      query.between(BatchPayment#PostedDate, EarliestPostedDate, endOfDayFor(LatestPostedDate));
    }
  }

  private function restrictSearchByCreatedUser(query: Query) {
    if (CreatedBy != null) {
      query.compare(BatchPayment#CreateUser, Equals, CreatedBy.ID)
    }
  }

  private function restrictSearchByUserWhoLastEdited(query: Query) {
    if (LastEditedBy != null) {
      query.compare(BatchPayment#UpdateUser, Equals, LastEditedBy.ID)
    }
  }

  private function restrictSearchByBatchNumber(query: Query) {
    if (batchNumber != null) {
      query.startsWith(BatchPayment#BatchNumber, batchNumber, SHOULD_IGNORE_CASE)
    }
  }

  private function restrictSearchByStatus(query: Query) {
    if (status != null) {
      query.compare(BatchPayment#BatchStatus, Equals, BatchStatus)
    }
  }

  private function restrictSearchByMinMaxAmount(query: Query) {
    SearchHelper.checkForNumericExceptions(MinAmount, MaxAmount)
    if (MinAmount != null or MaxAmount != null) {
      query.between(BatchPayment#Amount, MinAmount, MaxAmount)
    }
  }

  public function clearMinimumAndMaximumFields() {
    this.MinAmount = null
    this.MaxAmount = null
  }

  //Helper functions
  private static function endOfDayFor(date: Date): Date {
    return date != null ? DateUtil.endOfDay(date) : date
  }

}