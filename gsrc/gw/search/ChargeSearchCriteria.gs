package gw.search

uses com.google.common.collect.Sets
uses gw.api.database.IQueryBeanResult
uses gw.api.locale.DisplayKey
uses gw.api.util.DisplayableException
uses gw.pl.currency.MonetaryAmount

uses java.util.Date
uses java.util.Set

/**
 * Search criteria for reversible charges or writeoff
 */
@Export
abstract /*internal*/ class ChargeSearchCriteria {
  var _policyPeriod : PolicyPeriod as readonly PolicyPeriod
  var _account : Account as Account
  var _chargePatterns : Set<ChargePattern> as readonly ChargePatterns
  var _earliestDate : Date as EarliestDate
  var _latestDate : Date as LatestDate
  var _minAmount : MonetaryAmount as MinAmount
  var _maxAmount : MonetaryAmount as MaxAmount
  var _currency : Currency as Currency

  abstract function performSearch() : IQueryBeanResult<Charge>

  /**
   * Report for user convenience: Need not be a strict error since the search
   * will work correctly with PolicyPeriod taking precedence and ignoring
   * Account if it not the owning one.
   */
  protected function verifyPolicyPeriodIsOnAccount() {
    if (Account != null && PolicyPeriod != null && PolicyPeriod.Account != Account) {
      throw new DisplayableException(
          DisplayKey.get('Java.Search.PolicyIsNotOnAccountError', PolicyPeriod, Account)
      )
    }
  }

  /**
   * Report for user convenience: Need not be a strict error since there will
   * be no results when the PolicyPeriod is archived.
   */
  private function verifyPolicyPeriodIsNotArchived() {
    if (PolicyPeriod != null && PolicyPeriod.Archived) {
      throw new DisplayableException(
          DisplayKey.get('Java.Search.PolicyPeriodIsArchivedError', PolicyPeriod)
      )
    }
  }

  final property set PolicyPeriod(policyPeriod : PolicyPeriod) {
    _policyPeriod = policyPeriod
    verifyPolicyPeriodIsNotArchived()
  }

  /**
   * The singleton {@link ChargePattern} to which the {@link Charge}s should
   * all belong.
   *
   * @throws java.lang.IllegalStateException If {@link #setChargePatterns
   * ChargePatterns} was set with multiple values.
   */
  final property get ChargePattern() : ChargePattern {
    return ChargePatterns?.single()
  }

  final property set ChargePattern(chargePattern : ChargePattern) {
    _chargePatterns = Sets.newHashSet({chargePattern})
  }

  /**
   * The set of {@link ChargePattern}s one of to which each {@link Charge}s
   * should belong. Note: If any {@code ChargePattern} is not
   * {@link ChargePattern#isReversible reversible} it will be ignored.
   */
  final property set ChargePatterns(chargePatterns : Set<ChargePattern>) {
    _chargePatterns = chargePatterns
  }
}