package gw.search

uses gw.api.database.Query

/**
 * Search criteria for Account Disbursements
 */
@Export
class AcctDisbSearchCriteria extends gw.search.DisbSearchCriteria<AcctDisbSearchView> {

  final static var SHOULD_IGNORE_CASE = true

  var _accountNumber : String as AccountNumber        /* The related account number */

  protected override function addQueryRestrictions(query : Query<AcctDisbSearchView>) {
    super.addQueryRestrictions(query)
    restrictSearchByAccountNumber(query)
  }

  private function restrictSearchByAccountNumber(query : Query<AcctDisbSearchView>) {
    if(AccountNumber.NotBlank) {
      query.join(AccountDisbursement#Account)
          .startsWith(Account#AccountNumber, AccountNumber, SHOULD_IGNORE_CASE)
    }
  }

  protected override function restrictSearchByUserSecurityZone(query: Query<AcctDisbSearchView>) {
    if (perm.System.acctignoresecurityzone) {
      return // no need to consider security zones
    }
    var accountQuery = query.join(AccountDisbursement#Account)
    accountQuery.or(\restriction -> {
      restriction.compare(Account#SecurityZone, Equals, null) // Everyone can see null SecurityZone
      restriction.compareIn(Account#SecurityZone, User.util.CurrentUser.GroupUsers*.Group*.SecurityZone)
    })
  }

}
