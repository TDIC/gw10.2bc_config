package tdic.bc.common.batch.invoices

uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory

class RemoveGhostinvoices extends BatchProcessBase {

  private static var _logger = LoggerFactory.getLogger("Server.BatchProcess");
  private static final var _LOG_TAG = "${RemoveGhostinvoices.Type.RelativeName} - "

  construct() {
    super(BatchProcessType.TC_REMOVEGHOSTINVOICE_TDIC)
  }

  override function checkInitialConditions() : boolean {
    return true;
  }

  override function requestTermination() : boolean {
    return true;
  }

  protected override function doWork() {

    var shellInvoices = Query.make(AccountInvoice).compare(AccountInvoice#Status, Equals, InvoiceStatus.TC_PLANNED)
    var results = shellInvoices.select().toList().where(\elt -> elt.InvoiceItems.Count == 0)
    _logger.info(_LOG_TAG + "Found " + results.Count + "shell planned invoices.")

    for (invoice in results) {
      if (this.TerminateRequested) {
        break
      }
      // remove the shell invoices
      removeInvoice(invoice)
      incrementOperationsCompleted()
    }
  }

  protected function removeInvoice(invoice : AccountInvoice) : void {
    _logger.info(_LOG_TAG + invoice.InvoiceNumber
        + " - retiring the invoice.")
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      invoice = bundle.add(invoice)
      invoice.remove()
    },"iu")
  }
}