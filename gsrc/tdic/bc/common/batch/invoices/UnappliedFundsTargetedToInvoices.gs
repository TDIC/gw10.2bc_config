package tdic.bc.common.batch.invoices

uses gw.processes.BatchProcessBase
uses gw.api.system.PLLoggerCategory
uses gw.api.database.Query
uses gw.pl.currency.MonetaryAmount
uses gw.transaction.Transaction
uses org.slf4j.LoggerFactory

class UnappliedFundsTargetedToInvoices extends BatchProcessBase {
  private static var _logger = LoggerFactory.getLogger("Server.BatchProcess");
  private static final var _LOG_TAG = "${UnappliedFundsTargetedToInvoices.Type.RelativeName} - "

  construct() {
    super(BatchProcessType.TC_UNAPPLIEDFUNDINVOICE_TDIC);
  }

  /*
    Used to determine if conditions are met to begin processing
      Return true: doWork() will be called
      Return false: only a history event will be written
      Avoids lengthy processing in doWork() if there is nothing to do
   */
  override function checkInitialConditions() : boolean {
    return true;
  }

  override function doWork() {
    var amount : MonetaryAmount;

    // Get all the unnapplied funds.
    _logger.trace (_LOG_TAG + "Querying for Unapplied Funds.");
    var query = Query.make(UnappliedFund);
    var results = query.select();
    _logger.trace (_LOG_TAG + "Found " + results.Count + " unapplied funds.");
    OperationsExpected = results.Count;

    var interval = ((OperationsExpected -1) / 100) + 1;

    for (unappliedFund in results) {
      if (this.TerminateRequested) {
        break;
      }

      Transaction.runWithNewBundle(\bundle -> {
        unappliedFund = bundle.add(unappliedFund);
        // Call property to calculate amount
        amount = unappliedFund.TargetedForPlannedInvoices_TDIC;
      }, "iu");

      incrementOperationsCompleted();
      if (OperationsCompleted % interval == 0) {
        _logger.trace ("Processed " + OperationsCompleted + " of " + OperationsExpected
                          + " (" + (OperationsCompleted * 100 / OperationsExpected) + "%) Unaplied Funds.");
      }
    }
    _logger.info ("Exiting after processing " + OperationsCompleted + " of " + OperationsExpected
                    + " (" + (OperationsCompleted * 100 / OperationsExpected) + "%) Unaplied Funds.");
  }

  /*
  Called by Guidewire to request that process self-terminate
    Return true if request will be honored
    Return false if request will not be honored
  Handled by setting monitor flag in code and exiting any loops
  */
  override function requestTermination() : boolean {
    return true;
  }
}