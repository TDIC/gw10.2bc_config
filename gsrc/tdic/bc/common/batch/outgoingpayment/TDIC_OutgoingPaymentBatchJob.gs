package tdic.bc.common.batch.outgoingpayment

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.api.system.server.ServerUtil
uses gw.api.util.DateUtil
uses gw.pl.exception.GWConfigurationException
uses gw.processes.BatchProcessBase
uses org.apache.commons.io.FileUtils
uses org.slf4j.LoggerFactory
uses tdic.bc.integ.plugins.generalledger.helper.TDIC_GLIntegrationHelper
uses tdic.bc.integ.plugins.outgoingpayment.dto.OutgoingPaymentBeanInfo
uses tdic.bc.integ.plugins.outgoingpayment.dto.OutgoingPaymentFile
uses tdic.bc.integ.plugins.outgoingpayment.dto.OutgoingPaymentWritableBean
uses tdic.bc.integ.plugins.outgoingpayment.helper.TDIC_OutgoingPaymentHelper

uses java.io.File
uses java.io.IOException
uses java.sql.Connection
uses java.sql.ResultSet
uses java.sql.SQLException
uses java.text.SimpleDateFormat

uses entity.OutgoingPayment

/**
 * US570 - Lawson Outgoing Payments Integration
 * 11/24/2014 Alvin Lee
 *
 * Custom batch job for the Lawson integration to generate outgoing payment flat files to be sent to Lawson.
 */
class TDIC_OutgoingPaymentBatchJob extends BatchProcessBase {

  /**
   * The custom batch process does not have a current user, so a user needs to be specified in order to execute
   * the bundle which adds or makes changes to an entity.  This is currently set to the Super User (value: su).
   * TODO May need to change this user before production
   */
  /**
   * 01/19/2016 Hermia Kho
   * Replace Super_User "su" with "iu"
   */
  public static final var SUPER_USER : String = "iu"

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("OUTGOING_PAYMENT")

  /**
   * Prefix of the APC refund flat file (value: GW-LAP-REF-).
   */
  public static final var APC_FILE_PREFIX : String = "GW-LAP-REF-"

  /**
   * Suffix of the APC Invoice flat file (value: .apc).
   */
  public static final var APC_INVOICE_FILE_SUFFIX : String = ".apc"

  /**
   * Suffix of the APC Invoice Distribution flat file (value: .dis).
   */
  public static final var APC_DISTRIBUTION_FILE_SUFFIX : String = ".dis"

  /**
   * Suffix of the APC Attachments flat file (value: .dis).
   */
  public static final var APC_ATTACHMENT_FILE_SUFFIX : String = ".att"

  /**
   * Key for looking up the file output directory from the integration database (value: gl.outputdir).
   */
  public static final var OUTPUT_DIR_PATH : String = "gl.outputdir"

  /**
   * Key for looking up the notification email addresses from the integration database (value: OutgoingPaymentNotificationEmail).
   */
  public static final var NOTIFICATION_EMAIL_KEY : String = "OutgoingPaymentNotificationEmail"

  /**
   * Key for looking up the notification email addresses from the integration database (value: BCInfraIssueNotificationEmail).
   */
  public static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "BCInfraIssueNotificationEmail"

  /**
   * Absolute directory path/string to use to write files.
   */
  private var _destinationDirectoryPath : String = null

  /**
   * Notification email addresses in case of any errors in batch process.
   */
  private var _notificationEmail : String = null

  /**
   * Failure notification email addresses in case of any errors in batch process.
   */
  private var _failureNotificationEmail : String = null

  /**
   * Notification email's subject for Outgoing Payment batch job completion.
   */
  public static final var EMAIL_SUBJECT_COMPLETED: String = "Outgoing Payment Batch Job Completed on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * Notification email's subject for Outgoing Payment batch job failures.
   */
  public static final var EMAIL_SUBJECT_FAILURE: String = "Outgoing Payment Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * The connection to the external database.
   */
  private var _dbConnection : Connection = null

  /**
   * Outgoing payments to process as a part of a single batch run.
   */
  private var _outgoingPaymentResults : ResultSet = null

  /**
   * The OutgoingPaymentBeanInfo to interact with the external database.
   */
  private var _beanInfo : OutgoingPaymentBeanInfo = null

  /**
   * Key for looking up the Integration DB URL from properties file
   */
  public static final var INTEGRATION_DB_URL_KEY : String = "IntDBURL"

  /**
   * Default constructor with no parameters.
   */
  construct() {
    super(BatchProcessType.TC_OUTGOINGPAYMENT)
  }

  /**
   * Runs the actual batch process.
   */
  override function doWork() {
    _logger.info("TDIC_OutgoingPaymentBatchJob#doWork() - Entering")
    //GINTEG-1135 : Skip if we don't have any data to process
    _logger.info("TDIC_CashReceiptsBatchJob#doWork() - Skip if we don't have any data to process")
    if (checkInitialConditions_TDIC()) {
      // Set up files to write data
      var sdfFile = new SimpleDateFormat("MM-dd-yyyy-HHmmss")
      var apcInvoiceFile = new File(_destinationDirectoryPath + "/"
          + APC_FILE_PREFIX + sdfFile.format(DateUtil.currentDate()) + APC_INVOICE_FILE_SUFFIX)
      _logger.debug("TDIC_OutgoingPaymentBatchJob#doWork() - APC Invoice file constructed: ${apcInvoiceFile.AbsolutePath}")
      var apcDistributionFile = new File(_destinationDirectoryPath + "/" + APC_FILE_PREFIX
          + sdfFile.format(DateUtil.currentDate()) + APC_INVOICE_FILE_SUFFIX + APC_DISTRIBUTION_FILE_SUFFIX)
      _logger.debug("TDIC_OutgoingPaymentBatchJob#doWork() - APC Invoice Distribution file constructed: ${apcDistributionFile.AbsolutePath}")
      var apcAttachmentFile = new File(_destinationDirectoryPath + "/" + APC_FILE_PREFIX
          + sdfFile.format(DateUtil.currentDate()) + APC_INVOICE_FILE_SUFFIX + APC_ATTACHMENT_FILE_SUFFIX)
      _logger.debug("TDIC_OutgoingPaymentBatchJob#doWork() - APC Attachment file constructed: ${apcAttachmentFile.AbsolutePath}")

      var outgoingPaymentFile = new OutgoingPaymentFile()

      // Set up fields to be used across the flat files
      var mappedTAccount = TDIC_GLIntegrationHelper.getMappedTAccountName(typekey.Transaction.TC_DISBURSEMENTPAID, LedgerSide.TC_CREDIT, false, null, GLOriginalTAccount_TDIC.TC_CASH.Code)
      if (mappedTAccount == GLOriginalTAccount_TDIC.TC_CASH.Code) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_OutgoingPaymentBatchJob#doWork() - Incorrect setup of T-Account name mapping for disbursements.  Cannot proceed with Outgoing Payment batch process.")
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
            "Incorrect setup of T-Account name mapping for disbursements.  Cannot proceed with Outgoing Payment batch process.")
        throw new RuntimeException("Incorrect setup of T-Account name mapping for disbursements")
      } else {
        mappedTAccount = mappedTAccount.replace("-", ",")
      }

      try {
        gw.transaction.Transaction.runWithNewBundle( \ bundle -> {
          while(_outgoingPaymentResults.next()) {
            // Check TerminateRequested flag before proceeding with each entry
            if (TerminateRequested) {
              _logger.info("TDIC_OutgoingPaymentBatchJob#doWork() - Terminate requested during doWork() method. Progress: " + Progress + " rows completed.")
              var apcInvoiceLines = TDIC_OutgoingPaymentHelper.convertDataToFlatFileLines("OutgoingPaymentInvoiceSpec.xlsx", outgoingPaymentFile)
              var apcDistributionLines = TDIC_OutgoingPaymentHelper.convertDataToFlatFileLines("OutgoingPaymentDistributionSpec.xlsx", outgoingPaymentFile)
              var apcAttachmentLines = TDIC_OutgoingPaymentHelper.convertDataToFlatFileLines("OutgoingPaymentAttachmentSpec.xlsx", outgoingPaymentFile)
              FileUtils.writeLines(apcInvoiceFile, apcInvoiceLines)
              FileUtils.writeLines(apcDistributionFile, apcDistributionLines)
              FileUtils.writeLines(apcAttachmentFile, apcAttachmentLines)
              _dbConnection.commit()
              bundle.commit()
              return
            }

            // Increment operations completed for the purpose of keeping progress.  Errors are only incremented if does
            // prevent the batch from running; otherwise, a single failure terminates all.
            incrementOperationsCompleted()

            // Process outgoing payment
            var outgoingPaymentBean = new OutgoingPaymentWritableBean()
            outgoingPaymentBean.PublicID = _outgoingPaymentResults.getString(_beanInfo.IDColumnSQLName)
            outgoingPaymentBean.AccountNumber = _outgoingPaymentResults.getString("AccountNumber")
            outgoingPaymentBean.Amount = _outgoingPaymentResults.getBigDecimal("Amount")
            outgoingPaymentBean.LOB = _outgoingPaymentResults.getString("LOB")
            outgoingPaymentBean.PolicyNumber = _outgoingPaymentResults.getString("PolicyNumber")
            outgoingPaymentBean.StateCode = _outgoingPaymentResults.getString("StateCode")
            outgoingPaymentBean.VendorNumber = _outgoingPaymentResults.getString("VendorNumber")

            // Skip line if no vendor number; should not happen anymore since using late binding, and validation occurs in
            // transport plugin.  However, leaving the check here since it does not cause any harm, and in case the late
            // binding code changes in the future.
            if (outgoingPaymentBean.VendorNumber == null || outgoingPaymentBean.VendorNumber.Empty) {
              _logger.warn("TDIC_OutgoingPaymentBatchJob#doWork() - Vendor Number not found on outgoing payment with Public ID "
                  + outgoingPaymentBean.PublicID)
              incrementOperationsFailed()
              continue
            }

            outgoingPaymentBean.MappedTAccountCode = mappedTAccount
            outgoingPaymentFile.addToOutgoingPayments(outgoingPaymentBean)

            // Mark the line as processed
            outgoingPaymentBean.markAsProcessed(_dbConnection)

            // Change Status of OutgoingPayment to "Requested"
            var query = gw.api.database.Query.make(OutgoingPayment)
            query.compare(OutgoingPayment#PublicID, Equals, outgoingPaymentBean.PublicID)
            var targetOutgoingPayment = query.select().AtMostOneRow
            targetOutgoingPayment = bundle.add(targetOutgoingPayment)
            targetOutgoingPayment.Status = OutgoingPaymentStatus.TC_REQUESTED
          }

          // Write lines and then commit transaction
          var apcInvoiceLines = TDIC_OutgoingPaymentHelper.convertDataToFlatFileLines("OutgoingPaymentInvoiceSpec.xlsx", outgoingPaymentFile)
          var apcDistributionLines = TDIC_OutgoingPaymentHelper.convertDataToFlatFileLines("OutgoingPaymentDistributionSpec.xlsx", outgoingPaymentFile)
          var apcAttachmentLines = TDIC_OutgoingPaymentHelper.convertDataToFlatFileLines("OutgoingPaymentAttachmentSpec.xlsx", outgoingPaymentFile)
          FileUtils.writeLines(apcInvoiceFile, apcInvoiceLines)
          FileUtils.writeLines(apcDistributionFile, apcDistributionLines)
          FileUtils.writeLines(apcAttachmentFile, apcAttachmentLines)

          // Commit the database transaction and bundle
          _dbConnection.commit()
          bundle.commit()
          if (OperationsFailed > 0) {
            EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
                "The Outgoing Payment batch process has completed, but with errors.  Disbursement lines were found "
                    + "without vendor numbers in the integration database and were skipped.  Both the Contact entity and the "
                    + "integration database row must be updated with the Vendor Number to successfully process this "
                    + "disbursement.")
          }
          else {
            EmailUtil.sendEmail(_notificationEmail, EMAIL_SUBJECT_COMPLETED, "The Outgoing Payment batch process has completed successfully with no errors.")
          }
        }, SUPER_USER)
      } catch (ioe : IOException) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_OutgoingPaymentBatchJob#doWork() - Error while writing to the output files", ioe)
        // Send email notification of failure
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
            "The Outgoing Payment batch job has failed due to an error while writing to the output files: " + ioe.LocalizedMessage)
        _dbConnection.rollback()
      } catch (sqle : SQLException) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_OutgoingPaymentBatchJob#doWork() - Database connection error while attempting to access integration database table", sqle)
        // Send email notification of failure
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
            "The Outgoing Payment batch job has failed due to a database connection error while attempting to access the integration database: " + sqle.LocalizedMessage)
        _dbConnection.rollback()
      } catch (e : Exception) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_OutgoingPaymentBatchJob#doWork() - Unexpected error processing the output files: " + e.LocalizedMessage, e)
        // Send email notification of failure
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
            "The Outgoing Payment batch job has failed due to an unexpected error while processing the output files: " + e.LocalizedMessage)
        _dbConnection.rollback()
      } finally {
        try {
          if (_dbConnection != null) {
            DatabaseManager.closeConnection(_dbConnection)
          }
        } catch (e : Exception) {
          _logger.error("TDIC_OutgoingPaymentBatchJob#doWork() - Exception on closing DB (continuing)", e)
        }
      }
    }
    _logger.info("TDIC_OutgoingPaymentBatchJob#doWork() - Finished processing")
  }

  /**
   * Determines if the process should run the doWork() method, based on if there are outgoing payments to process into a flat file.
   */
  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  function checkInitialConditions_TDIC() : boolean {
    _logger.debug("TDIC_OutgoingPaymentBatchJob#checkInitialConditions() - Entering")

    var dbUrl : String
    try {
      var env = ServerUtil.getEnv()?.toLowerCase()
      if (env == null) {
        throw new GWConfigurationException("No environment specified.  Cannot load integration database.")
      }
      dbUrl = PropertyUtil.getInstance().getProperty(INTEGRATION_DB_URL_KEY)

      if (dbUrl == null) {
        throw new GWConfigurationException("Integration Database not defined.")
      }

      _notificationEmail = PropertyUtil.getInstance().getProperty(NOTIFICATION_EMAIL_KEY)
      if (_notificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + NOTIFICATION_EMAIL_KEY + "' from integration database.")
      }

      _failureNotificationEmail = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)
      if (_failureNotificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + FAILURE_NOTIFICATION_EMAIL_KEY + "' from integration database.")
      }

      _destinationDirectoryPath = PropertyUtil.getInstance().getProperty(OUTPUT_DIR_PATH)
      if (_destinationDirectoryPath == null) {
        throw new GWConfigurationException("Cannot retrieve file output path with the key '" + OUTPUT_DIR_PATH
            + "' from integration database.")
      }
    } catch (gwce : GWConfigurationException) {
      // Send email notification of failure
      EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
          "The Outgoing Payment batch job has failed due to the integration database either being not property set up or missing required properties.")
      throw gwce
    }

    try {
      // Get a connection from DatabaseManager using the GWINT DB URL
      _dbConnection = DatabaseManager.getConnection(dbUrl)
      _beanInfo = new OutgoingPaymentBeanInfo()
      var stmt = _beanInfo.createRetrieveAllSQL(_dbConnection)
      _outgoingPaymentResults = stmt.executeQuery()
      _outgoingPaymentResults.last()
      // If the last outgoing payment row is row 0, there are no outgoing payments to process
      if (_outgoingPaymentResults.Row != 0) {
        _logger.info("TDIC_OutgoingPaymentBatchJob#checkInitialConditions() - Results returned. Number of outgoing payments to be processed: " + _outgoingPaymentResults.Row)
        OperationsExpected = _outgoingPaymentResults.Row
        _outgoingPaymentResults.beforeFirst()
        return true
      }
      else {
        // Send email notification of failure
        if (_failureNotificationEmail != null) {
          EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
              "The Outgoing Payment batch job did not run, due to having no outgoing payments to be processed.")
        }
        _logger.warn("TDIC_OutgoingPaymentBatchJob#checkInitialConditions() - No outgoing payments to be processed. Exiting batch process.")
        // Attempt to close connection
        if (_dbConnection != null) {
          _logger.debug("Closing DB connection")
          try {
            DatabaseManager.closeConnection(_dbConnection)
          } catch (e : Exception) {
            _logger.error("TDIC_OutgoingPaymentBatchJob#checkInitialConditions() - Exception on closing DB (continuing)", e)
          }
        }
        return false
      }
    } catch (sqle : SQLException) {
      _logger.error("TDIC_OutgoingPaymentBatchJob#checkInitialConditions() - Database connection error while attempting to read integration database", sqle)
      // Send email notification of failure
      if (_failureNotificationEmail != null) {
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
            "The Outgoing Payment batch job has failed due to a database connection error while attempting to read the integration database: " + sqle.LocalizedMessage)
      }
      // Attempt to close connection
      if (_dbConnection != null) {
        _logger.debug("Closing DB connection")
        try {
          DatabaseManager.closeConnection(_dbConnection)
        } catch (e : Exception) {
          _logger.error("TDIC_OutgoingPaymentBatchJob#checkInitialConditions() - Exception on closing DB (continuing)", e)
        }
      }
      throw sqle
    } catch (e : Exception) {
      // Handle other errors
      _logger.error("TDIC_OutgoingPaymentBatchJob#checkInitialConditions() - Unexpected error while attempting to read integration database", e)
      // Send email notification of failure
      if (_failureNotificationEmail != null) {
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
            "The Outgoing Payment batch job has failed due to an unexpected error while attempting to read the integration database: " + e.LocalizedMessage)
      }
      // Attempt to close connection
      if (_dbConnection != null) {
        _logger.debug("Closing DB connection")
        try {
          DatabaseManager.closeConnection(_dbConnection)
        } catch (e2 : Exception) {
          _logger.error("TDIC_OutgoingPaymentBatchJob#checkInitialConditions() - Exception on closing DB (continuing)", e2)
        }
      }
      throw e
    }
  }

  /**
   * Called when the batch process is to be terminated.  This sets the TerminateRequested flag and returns true to indicate that the process can be stopped.
   *
   * @return A boolean to indicate that the process can be stopped.
   */
  override function requestTermination() : boolean {
    _logger.info("TDIC_OutgoingPaymentBatchJob#requestTermination() - Terminate requested for batch process.")
    // Set TerminateRequested to true by calling super method
    super.requestTermination()
    return true
  }

}