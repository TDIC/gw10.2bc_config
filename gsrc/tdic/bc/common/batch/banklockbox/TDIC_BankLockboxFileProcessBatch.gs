package tdic.bc.common.batch.banklockbox


uses com.tdic.util.properties.PropertyUtil
uses gw.api.locale.DisplayKey
uses gw.api.system.server.ServerUtil
uses gw.pl.exception.GWConfigurationException
uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory
uses tdic.bc.common.batch.banklockbox.dto.InvoiceLine
uses tdic.bc.common.batch.banklockbox.dto.TransactionLine
uses tdic.bc.common.batch.banklockbox.helper.TDIC_BankLockboxHelper
uses com.tdic.util.misc.EmailUtil
uses java.io.BufferedReader
uses java.io.File
uses java.io.FileReader
uses java.io.IOException
uses java.math.BigDecimal

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 10/23/15
 * US567 - Bank/Lockbox Integration
 * Implementation of a custom batch process for the Bank/Lockbox Integration to process Bank/Lockbox file.
 */
class TDIC_BankLockboxFileProcessBatch extends BatchProcessBase {

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("BANK_LOCKBOX")

  /**
   * The deposit date for all the checks within a certain batch.
   */
  private var _batchDepositDate : Date

  /**
   * A running total of all checks within a certain batch.
   */
  private var _batchRunningTotal : BigDecimal

  /**
   * A running total number of transactions within a certain batch.
   */
  private var _batchRunningTotalNumTransactions : int

  /**
   * A running total of all checks within a lockbox file.
   */
  private var _fileRunningTotal : BigDecimal

  /**
   * A running total number of all transactions within a lockbox file.
   */
  private var _fileRunningTotalNumTransactions : int

  /**
   * A Map<InvoiceLine, <Set<TransactionLine>>, mapping each invoice line to one or more transaction lines to process
   */
  private var _invoiceTransactionsMap : HashMap<InvoiceLine, Set<TransactionLine>>

  /**
   * A Set<TransactionLine>, storing one or more transaction lines to process prior to reaching each invoice line.
   */
  private var _transactionsToProcess : Set<TransactionLine>

  /**
   * Name of the vendor for purposes of connecting to the FTP (value: BofaIncoming).
   */
  public static final var VENDOR_NAME : String = "BofaIncoming"

  /**
   * Key for looking up the incoming file directory from the integration database (value: lockbox.incomingdir).
   */
  public static final var INCOMING_DIRECTORY_KEY : String = "lockbox.incomingdir"

  /**
   * Absolute directory path/string for incoming files.
   */
  private var _incomingPath : String = null

  /**
   * Key for looking up the done directory from the integration database (value: lockbox.donedir).
   */
  public static final var DONE_DIRECTORY_KEY : String = "lockbox.donedir"

  /**
   * Absolute directory path/string for completed files.
   */
  private var _donePath : String = null

  /**
   * Key for looking up the errer directory from the integration database (value: lockbox.errordir).
   */
  public static final var ERROR_DIRECTORY_KEY : String = "lockbox.errordir"

  /**
   * Absolute directory path/string for error files.
   */
  private var _errorPath : String = null

  /**
   * Key for looking up the notification email addresses from the integration database (value: PaymentNotificationEmail).
   */
  public static final var NOTIFICATION_EMAIL_KEY : String = "PaymentNotificationEmail"

  /**
   * Notification email addresses in case of any errors in batch process.
   */
  private var _notificationEmail : String = null

  /**
   * Key for looking up the failure notification email addresses from the integration database (value: BCInfraIssueNotificationEmail).
   */
  public static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "BCInfraIssueNotificationEmail"

  /**
   * Key for looking up the Integration DB URL from properties file
   */
  public static final var INTEGRATION_DB_URL_KEY : String = "IntDBURL"

  /**
   * Notification email addresses in case of any failure in batch process.
   */
  private var _failureNotificationEmail : String = null

  /**
   * Notification email's subject for Bank/Lockbox batch job completion.
   */
  public static final var EMAIL_SUBJECT_COMPLETED: String = "Bank/Lockbox Batch Job Completed on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * Notification email's subject for Bank/Lockbox batch job failures.
   */
  public static final var EMAIL_SUBJECT_FAILURE: String = "Bank/Lockbox Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * User for bundle commit.
   */
  public static final var SUPER_USER : String = "iu"

  /**
   * Default constructor with no parameters.
   */
  construct() {
    super(BatchProcessType.TC_BANKLOCKBOXFILEPROCESS)
  }

  /**
   * Runs the actual batch process.
   */
  override function doWork() {
    _logger.info("TDIC_BankLockboxFileProcessBatch#doWork() - Entering")
    //GINTEG-1135 : Skip if we don't have any data to process
    _logger.info("TDIC_BankLockboxFileProcessBatch#doWork() - Check whether we have any transactions that need to have flat files written")
    if (checkInitialConditions_TDIC()) {
      var reader : BufferedReader = null
      var incomingDirectory = new File(_incomingPath)
      for (incomingFile in incomingDirectory.listFiles()) {
        // Check TerminateRequested flag before proceeding with each file
        if (TerminateRequested) {
          _logger.warn("TDIC_BankLockboxFileProcessBatch#doWork() - Terminate requested during doWork() method. Progress: " + Progress + " files completed.")
          EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE, "The Bank/Lockbox batch process file was terminated by request on file '"
              + incomingFile.Name + "'. Progress: " + Progress + " files completed.")
          return
        }
        incrementOperationsCompleted()
        _logger.info("TDIC_BankLockboxFileProcessBatch#doWork() - Processing file: " + incomingFile.Name)
        _fileRunningTotal = BigDecimal.ZERO
        _fileRunningTotalNumTransactions = 0
        _invoiceTransactionsMap = new HashMap<InvoiceLine, Set<TransactionLine>>()
        _transactionsToProcess = new HashSet<TransactionLine>()
        _batchDepositDate = null
        try {
          var line : String = null
          // Read the file first and collect all the payment data; fail file if validation fails
          reader = new BufferedReader(new FileReader(incomingFile))
          line = reader.readLine()
          while (line != null) {
            processLine(line)
            line = reader.readLine()
          }
          // Process payments
          gw.transaction.Transaction.runWithNewBundle(\bundle -> {
            for (invLine in _invoiceTransactionsMap.keySet()) {
              for (transLine in _invoiceTransactionsMap.get(invLine)) {
                _logger.info("Processing payment for Invoice Number : " + invLine.InvoiceNumber + " Policy Number : " + invLine.PolicyNumber)
                TDIC_BankLockboxHelper.processPayment(_batchDepositDate, invLine, transLine, bundle)
              }
            }
          }, SUPER_USER)
          // Move file to done directory
          if (!moveToDone(incomingFile, reader)) {
            EmailUtil.sendEmail(_failureNotificationEmail, "Cannot move file to done directory on Server: " + gw.api.system.server.ServerUtil.ServerId, "Cannot move file to done directory:" + _donePath)
            throw new RuntimeException("Cannot move file to done directory")
          }
          _logger.info("TDIC_BankLockboxFileProcessBatch#doWork() - Finished processing file: " + incomingFile.Name)
        } catch (ioe : IOException) {
          incrementOperationsFailed()
          _logger.warn("TDIC_BankLockboxFileProcessBatch#doWork() - Error reading file: " + incomingFile.Name + ". Exception: "
              + ioe.LocalizedMessage, ioe)
          var fileMovedString = ""
          // Move file to error directory
          if (!moveToError(incomingFile, reader)) {
            _logger.warn("TDIC_BankLockboxFileProcessBatch#doWork() - Unable to move file to error directory")
            fileMovedString = ".  In addition, was not able move file to the error directory."
          }
          EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE, "The Bank/Lockbox file '" + incomingFile.Name
              + "' has failed to process due to an error reading the file.  Exception: " + ioe.LocalizedMessage + fileMovedString)
        } catch (lve : LockboxValidationException) {
          incrementOperationsFailed()
          _logger.warn("TDIC_BankLockboxFileProcessBatch#doWork() - Error validating lockbox file: " + incomingFile.Name
              + ". Exception: " + lve.LocalizedMessage, lve)
          var fileMovedString = ""
          // Move file to error directory
          if (!moveToError(incomingFile, reader)) {
            _logger.warn("TDIC_BankLockboxFileProcessBatch#doWork() - Unable to move file to error directory")
            fileMovedString = ".  In addition, was not able move file to the error directory."
          }
          EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE, "The Bank/Lockbox file '" + incomingFile.Name
              + "' has failed to process due to a file validation error: " + lve.LocalizedMessage + fileMovedString)
        } catch (e : Exception) {
          incrementOperationsFailed()
          _logger.warn("TDIC_BankLockboxFileProcessBatch#doWork() - Unexpected runtime exception: " + e.LocalizedMessage, e)
          var fileMovedString = ""
          // Move file to error directory
          if (!moveToError(incomingFile, reader)) {
            _logger.warn("TDIC_BankLockboxFileProcessBatch#doWork() - Unable to move file to error directory")
            fileMovedString = ".  In addition, was not able move file to the error directory."
          }
          EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE, "The Bank/Lockbox file '" + incomingFile.Name
              + "' has failed to process due an unexpected error: " + e.LocalizedMessage + fileMovedString)
        }
        _logger.debug("TDIC_BankLockboxFileProcessBatch#doWork() - Finished processing file: " + incomingFile.Name)
      }
      var emailBody : String = null
      if (OperationsFailed > 0) {
        emailBody = "The Bank/Lockbox batch process has completed, but with errors in " + OperationsFailed + " file(s)."
      } else {
        emailBody = "The Bank/Lockbox batch process has completed successfully with no errors."
      }
      EmailUtil.sendEmail(_notificationEmail, EMAIL_SUBJECT_COMPLETED, emailBody)
      _logger.info("TDIC_BankLockboxFileProcessBatch#doWork() - Finished running batch")
    }
  }

  /**
   * Process individual line
   */
  @Param("line", "The String text of the line to process")
  @Throws(LockboxValidationException, "If there are any validation issues processing the line")
  private function processLine(line : String) {
    _logger.trace("TDIC_BankLockboxFileProcessBatch#processLine - Processing line: " + line)
    if (line.length < 1) {
      return
    }
    var lineType = line.charAt(0)

    switch (lineType) {
      // Invoice Line
      case '4':
        // Create Invoice Line and add to HashMap
        _invoiceTransactionsMap.put(TDIC_BankLockboxHelper.getInvoiceLine(line), _transactionsToProcess)
        // Done with Invoice Line; reset Transactions to process
        _transactionsToProcess = new HashSet<TransactionLine>()
        break

      // Batch Header line
      case '5':
        _batchDepositDate = TDIC_BankLockboxHelper.getBatchDepositDateFromHeader(line)
        // Reset the Batch Running Total and the Total Number of Transactions in the batch
        _batchRunningTotal = BigDecimal.ZERO
        _batchRunningTotalNumTransactions = 0
        break

      // Transaction Line
      case '6':
        var transLine = TDIC_BankLockboxHelper.getTransactionLine(line)
        _batchRunningTotal = _batchRunningTotal.add(transLine.CheckAmount)
        _batchRunningTotalNumTransactions++
        _transactionsToProcess.add(transLine)
        break

      // Batch Trailer line
      case '7':
        var batchTotals = TDIC_BankLockboxHelper.getBatchTotals(line)
        _logger.debug("  Batch Total Amount: " + batchTotals.TotalAmount + " vs. Batch Running Total: " + _batchRunningTotal)
        _logger.debug("  Batch Total Number of Transactions: " + batchTotals.TotalNumberTransactions
            + " vs. Batch Running Total Transactions: " + _batchRunningTotalNumTransactions)
        if (batchTotals.TotalAmount.compareTo(_batchRunningTotal) != 0) {
          _logger.warn("TDIC_BankLockboxFileProcessBatch#processLine - "
              + DisplayKey.get("TDIC.Validation.BankLockbox.BatchTotalAmountMismatchError", batchTotals.TotalAmount,
              _batchRunningTotal, batchTotals.BatchNumber))
          throw new LockboxValidationException(DisplayKey.get("TDIC.Validation.BankLockbox.BatchTotalAmountMismatchError", batchTotals.TotalAmount,
              _batchRunningTotal, batchTotals.BatchNumber))
        }
        if (batchTotals.TotalNumberTransactions != _batchRunningTotalNumTransactions) {
          _logger.warn("TDIC_BankLockboxFileProcessBatch#processLine - "
              + DisplayKey.get("TDIC.Validation.BankLockbox.BatchTotalNumTransactionsMismatchError", batchTotals.TotalNumberTransactions,
              _batchRunningTotalNumTransactions, batchTotals.BatchNumber))
          throw new LockboxValidationException(DisplayKey.get("TDIC.Validation.BankLockbox.BatchTotalNumTransactionsMismatchError", batchTotals.TotalNumberTransactions,
              _batchRunningTotalNumTransactions, batchTotals.BatchNumber))
        }
        _fileRunningTotal += _batchRunningTotal
        _fileRunningTotalNumTransactions += _batchRunningTotalNumTransactions
        _batchRunningTotal = BigDecimal.ZERO
        _batchRunningTotalNumTransactions = 0
        break

      // Lockbox Trailer line
      case '8':
        var fileTotals = TDIC_BankLockboxHelper.getFileTotals(line)
        _logger.debug("  File Total Amount: " + fileTotals.TotalAmount + " vs. File Running Total: " + _fileRunningTotal)
        _logger.debug("  File Total Number of Transactions: " + fileTotals.TotalNumberTransactions
            + " vs. File Running Total Transactions: " + _fileRunningTotalNumTransactions)
        if (fileTotals.TotalNumberTransactions != _fileRunningTotalNumTransactions) {
          _logger.warn("TDIC_BankLockboxFileProcessBatch#processLine - " + DisplayKey.get("TDIC.Validation.BankLockbox.FileTotalNumTransactionsMismatchError", fileTotals.TotalNumberTransactions,
              _fileRunningTotalNumTransactions))
          throw new LockboxValidationException(DisplayKey.get("TDIC.Validation.BankLockbox.FileTotalNumTransactionsMismatchError", fileTotals.TotalNumberTransactions,
              _fileRunningTotalNumTransactions))
        }
        if (fileTotals.TotalAmount.compareTo(_fileRunningTotal) != 0) {
          _logger.warn("TDIC_BankLockboxFileProcessBatch#processLine - " + DisplayKey.get("TDIC.Validation.BankLockbox.FileTotalAmountMismatchError", fileTotals.TotalAmount,
              _fileRunningTotal))
          throw new LockboxValidationException(DisplayKey.get("TDIC.Validation.BankLockbox.FileTotalAmountMismatchError", fileTotals.TotalAmount,
              _fileRunningTotal))
        }
        break
    }
  }

  /**
   * Determines if the process should run the doWork() method, based on if there are lockbox flat files to process.
   */
  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  @Throws(GWConfigurationException, "If there are setup issues, such as issues with retrieving properties from the integration database")
  @Throws(RuntimeException, "If there are connection issues with the bank's FTP server")
  function checkInitialConditions_TDIC() : boolean {
    _logger.debug("TDIC_BankLockboxFileProcessBatch#checkInitialConditions() - Entering")

    var dbUrl : String
    try {
      // Get required values from integration database
      var env = ServerUtil.getEnv()?.toLowerCase()
      if (env == null) {
        throw new GWConfigurationException("No environment specified.  Cannot load integration database.")
      }
      dbUrl = PropertyUtil.getInstance().getProperty(INTEGRATION_DB_URL_KEY)
      if (dbUrl == null) {
        throw new GWConfigurationException("Integration Database not defined.")
      }

      _notificationEmail = PropertyUtil.getInstance().getProperty(NOTIFICATION_EMAIL_KEY)
      if (_notificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + NOTIFICATION_EMAIL_KEY + "' from integration database.")
      }

      _failureNotificationEmail = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)
      if(_failureNotificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + FAILURE_NOTIFICATION_EMAIL_KEY + "' from integration database.")
      }

      _incomingPath = PropertyUtil.getInstance().getProperty(INCOMING_DIRECTORY_KEY)
      if (_incomingPath == null) {
        throw new GWConfigurationException("Cannot retrieve incoming file path with the key '" + INCOMING_DIRECTORY_KEY
            + "' from integration database.")
      }

      _donePath = PropertyUtil.getInstance().getProperty(DONE_DIRECTORY_KEY)
      if (_donePath == null) {
        throw new GWConfigurationException("Cannot retrieve completed file path with the key '" + DONE_DIRECTORY_KEY
            + "' from integration database.")
      }

      _errorPath = PropertyUtil.getInstance().getProperty(ERROR_DIRECTORY_KEY)
      if (_errorPath == null) {
        throw new GWConfigurationException("Cannot retrieve error file path with the key '" + ERROR_DIRECTORY_KEY
            + "' from integration database.")
      }

      var errorDirectory = new File(_errorPath)
      if (!errorDirectory.exists()) {
        if (!errorDirectory.mkdirs()) {
          _logger.error("TDIC_BankLockboxFileProcessBatch#checkInitialConditions() - Failed to create error directory: " + _errorPath)
          throw new GWConfigurationException("Failed to create error directory: " + _errorPath)
        }
      }

      var incomingDirectory = new File(_incomingPath)
      if(incomingDirectory.listFiles().Count >  0) {
        _logger.info("TDIC_BankLockboxFileProcessBatch#checkInitialConditions()- Incoming files directory" + _incomingPath + " has " + incomingDirectory.listFiles().Count + " files to process." )
        return true
      }
      else {
        _logger.info("TDIC_BankLockboxFileProcessBatch#checkInitialConditions()- Incoming files directory" + _incomingPath + " has " + incomingDirectory.listFiles().Count + " files to process." )
        return false
      }


    } catch (gwce : GWConfigurationException) {
      // Log and send email notification of failure
      _logger.error("TDIC_BankLockboxFileProcessBatch#checkInitialConditions - Integration database either not property set up or missing required properties. Error: " + gwce.LocalizedMessage)
      EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
          "The Bank/Lockbox batch job has failed due to the integration database either being not property set up or missing required properties. Error: " + gwce.LocalizedMessage)
      throw gwce
    } catch (e : Exception) {
      // Log and send email notification of failure
      _logger.error("TDIC_BankLockboxFileProcessBatch#checkInitialConditions - An unexpected error occurred while determining lockbox files to process. Error: " + e.LocalizedMessage)
      EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
          "The Bank/Lockbox batch job has failed due to an unexpected error occurred while determining lockbox files to process. Error: " + e.LocalizedMessage)
      throw e
    }
  }

  /**
   * Moves a file to the done directory.
   */
  @Param("sourceFile", "A File object representing the source file to be moved")
  @Returns("A boolean indicating if the move was successful")
  private function moveToDone(sourceFile : File, reader : BufferedReader) : boolean {
    _logger.info("TDIC_BankLockboxFileProcessBatch#moveToDone() - Moving file " + sourceFile.Path + " to " + _donePath)
    // Close reader if open since it will prevent moving the file
    try {
      if (reader != null) {
        reader.close()
      }
    } catch (ioe : IOException) {
      _logger.warn("TDIC_BankLockboxFileProcessBatch#moveToDone() - Error closing file: " + sourceFile.Name, ioe)
    }
    var target = new File(_donePath + "/" + sourceFile.Name)
    return sourceFile.renameTo(target)
  }

  /**
   * Moves a file to the error directory.
   */
  @Param("sourceFile", "A File object representing the source file to be moved")
  @Returns("A boolean indicating if the move was successful")
  private function moveToError(sourceFile : File, reader : BufferedReader) : boolean {
    _logger.info("TDIC_BankLockboxFileProcessBatch#moveToError() - Moving file " + sourceFile.Path + " to " + _errorPath)
    // Close reader if open since it will prevent moving the file
    try {
      if (reader != null) {
        reader.close()
      }
    } catch (ioe : IOException) {
      _logger.warn("TDIC_BankLockboxFileProcessBatch#moveToError() - Error closing file: " + sourceFile.Name, ioe)
    }
    var target = new File(_errorPath + "/" + sourceFile.Name)
    // It is possible that the target file already exists (i.e. if the file failed already before).
    // If exists, delete the old one before moving the new one to the same location.
    if (target.exists()) {
      _logger.info("TDIC_BankLockboxFileProcessBatch#moveToError() - File already exists in error directory.  Deleting existing file.")
      if (!target.delete()) {
        _logger.warn("TDIC_BankLockboxFileProcessBatch#moveToError() - Error deleting file: " + target.Name)
      }
    }
    return sourceFile.renameTo(target)
  }

  /**
   * Called when the batch process is to be terminated.  This sets the TerminateRequested flag and returns true to indicate that the process can be stopped.
   */
  @Returns("A boolean to indicate that the process can be stopped.")
  override function requestTermination() : boolean {
    _logger.info("TDIC_BankLockboxFileProcessBatch#requestTermination() - Terminate requested for batch process.")
    // Set TerminateRequested to true by calling super method
    super.requestTermination()
    return true
  }

}