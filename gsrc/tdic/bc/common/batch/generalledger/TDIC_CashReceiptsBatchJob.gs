package tdic.bc.common.batch.generalledger

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.api.system.server.ServerUtil
uses gw.api.util.DateUtil
uses gw.pl.exception.GWConfigurationException
uses gw.processes.BatchProcessBase
uses org.apache.commons.io.FileUtils
uses org.slf4j.LoggerFactory
uses tdic.bc.integ.plugins.generalledger.dto.CashReceiptsAccount
uses tdic.bc.integ.plugins.generalledger.dto.CashReceiptsBucketGroup
uses tdic.bc.integ.plugins.generalledger.dto.CashReceiptsFile
uses tdic.bc.integ.plugins.generalledger.dto.CashReceiptsTransactionLineBeanInfo
uses tdic.bc.integ.plugins.generalledger.dto.CashReceiptsTransactionLineWritableBean
uses tdic.bc.integ.plugins.generalledger.dto.GLRuntimeException
uses tdic.bc.integ.plugins.generalledger.helper.TDIC_CashReceiptsHelper
uses tdic.bc.integ.plugins.generalledger.helper.TDIC_GLIntegrationHelper

uses java.io.File
uses java.io.IOException
uses java.sql.Connection
uses java.sql.ResultSet
uses java.sql.SQLException
uses java.text.SimpleDateFormat


/**
 * US570 - General Ledger Integration
 * 11/13/2014 Alvin Lee
 *
 * Custom batch process implementation to read the external integration database table for Cash Receipts transactions
 * and write the currently unwritten items to a file to be processed by the external system.  This custom batch process
 * can be scheduled, or executed from the UI.
 */
class TDIC_CashReceiptsBatchJob extends BatchProcessBase {

  /**
   * Standard logger for General Ledger
   */
  private static final var _logger = LoggerFactory.getLogger("GENERAL_LEDGER")

  /**
   * Prefix of the Cash Receipts flat file (value: GW-LCB-CAS-).
   */
  public static final var CAS_FILE_PREFIX : String = "GW-LCB-CAS-"

  /**
   * Suffix of the Cash Receipts flat file (value: .apc).
   */
  public static final var CAS_FILE_SUFFIX : String = ".CAS"

  /**
   * Key for looking up the file output directory from the integration database (value: gl.outputdir).
   */
  public static final var OUTPUT_DIR_PATH : String = "gl.outputdir"

  /**
   * Key for looking up the notification email addresses from the integration database (value: CashReceiptsNotificationEmail).
   */
  public static final var NOTIFICATION_EMAIL_KEY : String = "CashReceiptsNotificationEmail"

  /**
   * Key for looking up the failure notification email addresses from the integration database (value: BCInfraIssueNotificationEmail).
   */
  public static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "BCInfraIssueNotificationEmail"

  /**
   * Absolute directory path/string to use to write files.
   */
  private var _destinationDirectoryPath : String = null

  /**
   * Notification email addresses in case of any errors in batch process.
   */
  private var _notificationEmail : String = null

  /**
   * Failure notification email addresses in case of any errors in batch process.
   */
  private var _failureNotificationEmail : String = null

  /**
   * Notification email's subject for Cash Receipts batch job completion.
   */
  public static final var EMAIL_SUBJECT_COMPLETED: String = "Cash Receipts Batch Job Completed on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * Notification email's subject for Cash Receipts batch job failures.
   */
  public static final var EMAIL_SUBJECT_FAILURE: String = "Cash Receipts Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * Number of transactions to process as a part of a single batch run.
   */
  private var _transactionResults:ResultSet = null

  /**
   * The CashReceiptsTransactionLineBeanInfo to interact with the external database.
   */
  private var _beanInfo:CashReceiptsTransactionLineBeanInfo = null

  /**
   * The connection to the external database.
   */
  private var _dbConnection:Connection = null

  /**
   * Key for looking up the Integration DB URL from properties file
   */
  public static final var INTEGRATION_DB_URL_KEY : String = "IntDBURL"

  /**
   * Default constructor with no parameters.
   */
  construct() {
    super(BatchProcessType.TC_CASHRECEIPTS)
  }

  /**
   * Implementation of interface method to execute work in a batch process.  This batch process loads all unwritten transactions
   * from the external database, writes the lines to an external file, and marks the database entries as written
   */
  override function doWork() {
    _logger.info("TDIC_CashReceiptsBatchJob#doWork() - Entering")

    // GINTEG-1135 : Skip if there are no transactions that need to have flat files written.
    _logger.info("TDIC_CashReceiptsBatchJob#doWork() - Check whether we have any transactions that need to have flat files written")
    if (checkInitialConditions_TDIC()) {
      // Set up file to write data
      var sdfFile = new SimpleDateFormat("MM-dd-yyyy-HHmmss")
      var destFile = new File(_destinationDirectoryPath + "/" + CAS_FILE_PREFIX + sdfFile.format(DateUtil.currentDate()) + CAS_FILE_SUFFIX)
      _logger.debug("TDIC_CashReceiptsBatchJob#doWork() - Destination file constructed ${destFile.AbsolutePath}")

      var dateCashAccountMap = new HashMap<String, CashReceiptsAccount>()

      try {
        var metaData = _transactionResults.MetaData

        while(_transactionResults.next()) {
          // Check TerminateRequested flag before proceeding with each entry
          if (TerminateRequested) {
            _logger.info("TDIC_CashReceiptsBatchJob#doWork() - Terminate requested during doWork() method. Progress: " + Progress + " transactions to be rolled back.")
            // Rollback database transaction
            _dbConnection.rollback()
            return
          }

          // Increment operations completed for the purpose of keeping progress.  Errors are not incremented since a single failure terminates all.
          incrementOperationsCompleted()

          //GSPS-477 - Suspense Payments applied manually will create duplicate transactions and those don't need to be reported.
          //Only transactions created by the Lockbox batch needs to be included here
          if((_transactionResults.getString("PaymentMethod") == "lockbox" && _transactionResults.getString("CreatedUser") != "Intg User" && (_transactionResults.getString("PaymentReversalReason") == null))
                or
             (_transactionResults.getString("PaymentMethod") == "lockbox" && (_transactionResults.getString("PaymentReversalReason") != null and
                  !PaymentReversalReason.TF_UIREVERSALS.TypeKeys.contains(PaymentReversalReason.getTypeKey(_transactionResults.getString("PaymentReversalReason")))))){
            //GWPS-2609 - If the lockbox payment is reversed with certain reasons, then it should not considered as Duplicate.This has to go through the Batch process and sent in the CAS file.
            //Mark transactions as processed so they don't get picked up again the next day
            var transLine = new CashReceiptsTransactionLineWritableBean()
            transLine.PublicID = _transactionResults.getString(_beanInfo.IDColumnSQLName)
            transLine.markAsProcessed(_dbConnection)
            continue
          }

          // Process transaction
          var transLine = new CashReceiptsTransactionLineWritableBean()
          transLine.Amount = _transactionResults.getBigDecimal("Amount")
          transLine.Description = _transactionResults.getString("Description")
          transLine.CreditCardType = _transactionResults.getString("CreditCardType")
          transLine.DirectBillMoneyRcvdPublicID = _transactionResults.getString("DirectBillMoneyRcvdPublicID")
          transLine.GWTAccountName = _transactionResults.getString("GWTAccountName")
          transLine.LineItemType = _transactionResults.getString("LineItemType")
          transLine.LOB = _transactionResults.getString("LOB")
          transLine.MappedTAccountName = _transactionResults.getString("MappedTAccountName").replace('-',',')
          transLine.PaymentMethod = _transactionResults.getString("PaymentMethod")
          transLine.PaymentReversalReason = _transactionResults.getString("PaymentReversalReason")
          transLine.StateCode = _transactionResults.getString("StateCode")
          transLine.TransactionDate = _transactionResults.getString("TransactionDate")
          transLine.TransactionSubtype = _transactionResults.getString("TransactionSubtype")

          // If needed, Add new set of buckets for the account unit
          var accountUnitKey: String
          try{
            //GINTEG-1656
            if(transLine.MappedTAccountName?.startsWith("1") or transLine.MappedTAccountName?.startsWith("2")){
              accountUnitKey = "000.00.00"
            } else if (transLine.MappedTAccountName == "6790,0000") {
              //GWPS-926
              accountUnitKey = "100.00.00"
            } else {
              accountUnitKey = TDIC_GLIntegrationHelper.getMappedAccountUnit(CompanyAccountCode_TDIC.TC_BALSHEET, transLine.LOB, transLine.StateCode)
            }
            _logger.debug("TDIC_CashReceiptsBatchJob#doWork() - AccountUnitKey ${accountUnitKey} for CompanyAccountCode:${CompanyAccountCode_TDIC.TC_BALSHEET}:LOB:${transLine.LOB}:StateCode:${transLine.StateCode}")
          }catch(e: Exception){
            incrementOperationsFailed()
            _logger.warn("TDIC_CashReceiptsBatchJob#doWork() - Not able to find Mapped Account Unit. Line has been skipped but batch job will continue.  Reason: " + e.LocalizedMessage, e)
          }
          if(!accountUnitKey.HasContent)
            continue

          // If needed, Add new account unit for the transaction date
          var cashAccount = dateCashAccountMap.get(transLine.TransactionDate.substring(0, 10))
          if (cashAccount == null) {
            cashAccount = new CashReceiptsAccount()
            _logger.debug("TDIC_CashReceiptsBatchJob#doWork - Adding new account unit to the date: " + transLine.TransactionDate)
            dateCashAccountMap.put(transLine.TransactionDate.substring(0, 10), cashAccount)
          }

          var existingBucketGroup = cashAccount.AccountUnitBucketGroupMap.get(accountUnitKey)
          if (existingBucketGroup == null) {
            existingBucketGroup = new CashReceiptsBucketGroup()
            _logger.debug("TDIC_CashReceiptsBatchJob#doWork - Adding cash receipts buckets for new account unit: " + accountUnitKey)
            cashAccount.AccountUnitBucketGroupMap.put(accountUnitKey, existingBucketGroup)
          }

          try {
            // Cash: N/A
            // Unapplied (anywhere in name, including Designated Unapplied): Unapplied
            // <ChargeName> Due + InvoiceStatus "Due" or "Billed"): A/R Billed
            // <ChargeName> Due + InvoiceStatus "Planned"): A/R Unbilled
            if (transLine.TransactionSubtype == typekey.Transaction.TC_DIRECTBILLMONEYRECEIVEDTXN.Code
                || transLine.TransactionSubtype == typekey.Transaction.TC_SUSPPYMTTRANSACTION.Code) {
              if (transLine.GWTAccountName.endsWith("Unapplied")) {
                TDIC_CashReceiptsHelper.processUnapplied(transLine, existingBucketGroup)
              }
            } else {
              throw new GLRuntimeException("Unexpected transaction type '" + transLine.TransactionSubtype + "' in Cash Receipts table")
            }

            //Mark the line as written
            transLine.PublicID = _transactionResults.getString(_beanInfo.IDColumnSQLName)
            transLine.markAsProcessed(_dbConnection)
            _logger.trace("TDIC_CashReceiptsBatchJob#doWork() - Entry marked as written")
          } catch (glre : GLRuntimeException) {
            incrementOperationsFailed()
            _logger.warn("TDIC_CashReceiptsBatchJob#doWork() - Invalid line in the Cash Receipts integration database.  Line has been skipped but batch job will continue.  Error: " + glre.LocalizedMessage, glre)
          }
        }

        // Set up object for delimited file utility
        var cashReceiptsFile = new CashReceiptsFile()
        var sdf = new SimpleDateFormat("yyyy-MM-dd")

        for (date in dateCashAccountMap.Keys) {
          var cashAccount = dateCashAccountMap.get(date)
          _logger.debug("TDIC_CashReceiptsBatchJob#doWork() - Final account unit information for date: " + date)
          for (accountUnit in cashAccount.AccountUnitBucketGroupMap.Keys) {
            // Get all buckets
            var bucketGroup = cashAccount.AccountUnitBucketGroupMap.get(accountUnit)
            var nonZeroBuckets = bucketGroup.NameBucketMap.filterByValues( \ bucket -> !bucket.TotalBucketAmount.IsZero )

            _logger.debug("TDIC_CashReceiptsBatchJob#doWork() - Final bucket totals for account unit: " + accountUnit)
            for (bucketName in nonZeroBuckets.keySet()) {
              var cashReceiptsBucket = nonZeroBuckets.get(bucketName)
              cashReceiptsBucket.Date = sdf.parse(date)
              cashReceiptsBucket.AccountUnit = accountUnit
              cashReceiptsBucket.Description = cashReceiptsBucket.Description.replace("(MMDDYY)", cashReceiptsBucket.ShortDate)
              cashReceiptsFile.addToCashReceiptsBuckets(cashReceiptsBucket)
              _logger.debug("  " + bucketName + " - " + cashReceiptsBucket.Description + ": " + cashReceiptsBucket.TotalBucketAmount)
            }
          }
        }

        //GWPS-1702
        TDIC_CashReceiptsHelper.updateDEPorWTH(cashReceiptsFile.CashReceiptsBuckets)
        //Write lines and then commit transaction
        var lines = TDIC_CashReceiptsHelper.convertDataToFlatFileLines(cashReceiptsFile)
        FileUtils.writeLines(destFile, lines)
        _dbConnection.commit()
        if (OperationsFailed > 0) {
          EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
              "The Cash Receipts batch process has completed, but with errors.  Invalid lines were found in the Cash Receipts integration database and were skipped.")
        } else {
          EmailUtil.sendEmail(_notificationEmail, EMAIL_SUBJECT_COMPLETED, "The Cash Receipts batch process has completed successfully with no errors.")
        }
      } catch (ioe : IOException) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_CashReceiptsBatchJob#doWork() - Error while writing to output file " + destFile.AbsolutePath, ioe)
        // Send email notification of failure
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
            "The Cash Receipts batch job has failed due to an error while writing to output file: " + ioe.LocalizedMessage)
        _dbConnection.rollback()
      } catch (sqle : SQLException) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_CashReceiptsBatchJob#doWork() - Database connection error while attempting to access integration database table", sqle)
        // Send email notification of failure
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
            "The Cash Receipts batch job has failed due to a database connection error while attempting to access the integration database: " + sqle.LocalizedMessage)
        _dbConnection.rollback()
      } catch (e : Exception) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_CashReceiptsBatchJob#doWork() - Error processing output file: " + e.LocalizedMessage, e)
        // Send email notification of failure
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
            "The Cash Receipts batch job has failed due to an unexpected error while processing the output file: " + e.LocalizedMessage)
        _dbConnection.rollback()
      } finally {
        try {
          if (_dbConnection != null) {
            _logger.debug("TDIC_CashReceiptsBatchJob#doWork() - Closing database connection")
            _dbConnection.close()
          }
        } catch (e : Exception) {
          _logger.error("TDIC_CashReceiptsBatchJob#doWork() - Exception on closing DB (continuing)", e)
        }
      }
    }
    _logger.info("TDIC_CashReceiptsBatchJob#doWork() - Finished processing")
  }

  /**
   * Determines if the process should run the doWork() method, based on if there are transactions that need to have flat files written.
   *
   * @return A boolean indicating if the batch process should proceed with the doWork() method.
   */
  function checkInitialConditions_TDIC() : boolean {
    _logger.debug("TDIC_CashReceiptsBatchJob#checkInitialConditions() - Entering")

    var dbUrl : String
    try {
      var env = ServerUtil.getEnv()?.toLowerCase()
      if (env == null) {
        throw new GWConfigurationException("No environment specified.  Cannot load integration database.")
      }
      dbUrl = PropertyUtil.getInstance().getProperty(INTEGRATION_DB_URL_KEY)
      if (dbUrl == null) {
        throw new GWConfigurationException("Integration Database not defined.")
      }

      _notificationEmail = PropertyUtil.getInstance().getProperty(NOTIFICATION_EMAIL_KEY)
      if (_notificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + NOTIFICATION_EMAIL_KEY + "' from integration database.")
      }

      _failureNotificationEmail = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)
      if (_failureNotificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + FAILURE_NOTIFICATION_EMAIL_KEY + "' from integration database.")
      }

      _destinationDirectoryPath = PropertyUtil.getInstance().getProperty(OUTPUT_DIR_PATH)
      if (_destinationDirectoryPath == null) {
        throw new GWConfigurationException("Cannot retrieve file output path with the key '" + OUTPUT_DIR_PATH
            + "' from integration database.")
      }
    } catch (gwce : GWConfigurationException) {
      // Send email notification of failure
      EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
          "The Cash Receipts batch job has failed due to the integration database either being not property set up or missing required properties.")
      throw gwce
    }

    try {
      // Get a connection from DatabaseManager using the GWINT DB URL
      _dbConnection = DatabaseManager.getConnection(dbUrl)
      _beanInfo = new CashReceiptsTransactionLineBeanInfo()
      var stmt = _beanInfo.createRetrieveAllSQL(_dbConnection)
      _transactionResults = stmt.executeQuery()
      _transactionResults.last()
      //If the last transaction is row 0, there are no transactions to process
      if (_transactionResults.Row != 0) {
        _logger.debug("TDIC_CashReceiptsBatchJob#checkInitialConditions() - Results returned. Number of transactions to be processed: " + _transactionResults.Row)
        OperationsExpected = _transactionResults.Row
        _transactionResults.beforeFirst()
        return true
      }
      else {
        // Send email notification of failure
        if (_failureNotificationEmail != null) {
          EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
              "The Cash Receipts batch job did not run, due to having no transactions to be processed.")
        }
        _logger.warn("TDIC_CashReceiptsBatchJob#checkInitialConditions() - No transactions to be processed. Exiting batch process.")
        // Attempt to close connection
        if (_dbConnection != null) {
          try {
            DatabaseManager.closeConnection(_dbConnection)
          } catch (e : Exception) {
            _logger.error("TDIC_CashReceiptsBatchJob#checkInitialConditions() - Exception on closing DB (continuing)", e)
          }
        }
        return false
      }
    } catch (sqle : SQLException) {
      _logger.error("TDIC_CashReceiptsBatchJob#checkInitialConditions() - Database connection error while attempting to read integration database", sqle)
      // Send email notification of failure
      if (_failureNotificationEmail != null) {
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
            "The Cash Receipts batch job has failed due to a database connection error while attempting to read the integration database: " + sqle.LocalizedMessage)
      }
      // Attempt to close connection
      if (_dbConnection != null) {
        try {
          DatabaseManager.closeConnection(_dbConnection)
        } catch (e : Exception) {
          _logger.error("TDIC_CashReceiptsBatchJob#checkInitialConditions() - Exception on closing DB (continuing)", e)
        }
      }
      throw sqle
    } catch (e : Exception) {
      // Handle other errors
      _logger.error("TDIC_CashReceiptsBatchJob#checkInitialConditions() - Unexpected error while attempting to read integration database", e)
      // Send email notification of failure
      if (_failureNotificationEmail != null) {
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
            "The Cash Receipts batch job has failed due to an unexpected error while attempting to read the integration database: " + e.LocalizedMessage)
      }
      // Attempt to close connection
      if (_dbConnection != null) {
        try {
          DatabaseManager.closeConnection(_dbConnection)
        } catch (e2 : Exception) {
          _logger.error("TDIC_CashReceiptsBatchJob#checkInitialConditions() - Exception on closing DB (continuing)", e2)
        }
      }
      throw e
    }
  }

  /**
   * Called when the batch process is to be terminated.  This sets the TerminateRequested flag and returns true to indicate that the process can be stopped.
   *
   * @return A boolean to indicate that the process can be stopped.
   */
  override function requestTermination() : boolean {
    _logger.info("TDIC_CashReceiptsBatchJob#requestTermination - Terminate requested for batch process.")
    //Set TerminateRequested to true by calling super method
    return super.requestTermination()
  }

}