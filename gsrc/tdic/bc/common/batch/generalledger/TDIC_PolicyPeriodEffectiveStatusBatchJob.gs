package tdic.bc.common.batch.generalledger

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.api.database.IQueryBeanResult
uses gw.api.util.DateUtil
uses gw.pl.exception.GWConfigurationException
uses gw.processes.BatchProcessBase
uses org.slf4j.LoggerFactory
uses tdic.bc.integ.plugins.generalledger.helper.TDIC_GLIntegrationHelper
uses java.sql.Connection
uses java.sql.SQLException



/**
 * US570 - General Ledger Integration
 * 11/13/2014 Alvin Lee
 *
 * Custom batch process implementation to fire a custom event when a policy period becomes effective.  This batch
 * process must run once a day and set a flag on the policy period to indicate that it has been processed.  The purpose
 * of the batch job is to move appropriate funds across GL accounts when a policy period's Status goes from "Future" to
 * "In-Force", which is a derived property based on the current date and the PolicyPerEffDate.
 */
class TDIC_PolicyPeriodEffectiveStatusBatchJob extends BatchProcessBase {

  /**
   * Standard logger for General Ledger
   */
  private static final var _logger = LoggerFactory.getLogger("GENERAL_LEDGER")

  /**
   * The custom batch process does not have a current user, so a user needs to be specified in order to execute
   * the bundle which adds or makes changes to an entity.  This is currently set to the Super User (value: su).
   * TODO May need to change this user before production
   */
  /**
   * 01/19/2016 Hermia Kho
   * Replace Super_User "su" with "iu"
   */
  public static final var SUPER_USER : String = "iu"

  /**
   * Key for looking up the notification email addresses from the integration database (value: GLNotificationEmail).
   */
  public static final var NOTIFICATION_EMAIL_KEY : String = "GLNotificationEmail"

  /**
   * Notification email addresses in case of any errors in batch process.
   */
  private var _notificationEmail : String = null

  /**
   * Key for looking up the notification email addresses from the integration database (value: BCInfraIssueNotificationEmail).
   */
  public static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "BCInfraIssueNotificationEmail"

  /**
   * Notification email addresses in case of any errors in batch process.
   */
  private var _failureNotificationEmail : String = null

  /**
   * Notification email's subject for Policy Period Effective Status batch job completion.
   */
  public static final var EMAIL_SUBJECT_COMPLETED: String = "Policy Period Effective Status Batch Job Completed on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * Notification email's subject for Policy Period Effective Status batch job failures.
   */
  public static final var EMAIL_SUBJECT_FAILURE: String = "Policy Period Effective Status Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * Policy Periods to process as a part of a single batch run.
   */
  private var _policyPeriodsToProcess:IQueryBeanResult<PolicyPeriod> = null

  /**
   * Key for looking up the Integration DB URL from properties file
   */
  public static final var INTEGRATION_DB_URL_KEY : String = "IntDBURL"

  /**
   * Default constructor with no parameters.
   */
  construct() {
    super(BatchProcessType.TC_POLICYPERIODEFFECTIVESTATUS)
  }

  /**
   * Implementation of interface method to execute work in a batch process.  This batch process loads all unwritten transactions
   * from the external database, writes the lines to an external file, and marks the database entries as written
   */
  override function doWork() {
    _logger.info("TDIC_PolicyPeriodEffectiveStatusBatchJob#doWork() - Entering")
    //GINTEG-1135 : Skip if we don't have any data to process
    _logger.info("TDIC_PolicyPeriodEffectiveStatusBatchJob#doWork() - Skip if we don't have any data to process")
    if (checkInitialConditions_TDIC()) {
      for (policyPeriod in _policyPeriodsToProcess) {
        // Check TerminateRequested flag before proceeding with each entry
        if (TerminateRequested) {
          _logger.info("TDIC_PolicyPeriodEffectiveStatusBatchJob#doWork() - Terminate requested during doWork() method. Progress: " + Progress + " policy periods completed.")
          return
        }

        // Increment operations completed for the purpose of keeping progress.
        incrementOperationsCompleted()

        var dbConnection : Connection = null
        try {
          if (policyPeriod.Canceled) {
            // Exclude if policy period has been cancelled.
            _logger.debug("TDIC_PolicyPeriodEffectiveStatusBatchJob#doWork() - Policy Period '"
                + policyPeriod.PolicyNumberLong + "' has been cancelled.  No need to add transaction lines for policy "
                + "period becoming effective.")
          } else {
            // Otherwise, add transaction lines for policy period becoming effective
            _logger.debug("TDIC_PolicyPeriodEffectiveStatusBatchJob#doWork() - Adding transaction lines for policy "
                + "period becoming effective: " + policyPeriod.PolicyNumberLong)

            // Get a connection from DatabaseManager using the GWINT DB URL
            var dbUrl = PropertyUtil.getInstance().getProperty(INTEGRATION_DB_URL_KEY)
            dbConnection = DatabaseManager.getConnection(dbUrl)

            // Map fields to virtual transaction lines to be committed as a GL transaction
            var glTransaction = TDIC_GLIntegrationHelper.mapToPolicyPeriodEffectiveTransaction(policyPeriod)

            // Write lines to integration database
            for (glTransactionLine in glTransaction.LineItems) {
              glTransactionLine.executeCreate(dbConnection)
            }

            // Commit DB Connection
            if (dbConnection != null) {
              dbConnection.commit()
            } else {
              _logger.error("TDIC_GLIntegrationMessageTransport#send - Unable to get DB connection using JDBC URL: " + dbUrl)
              throw new SQLException("Unable to get DB connection using JDBC URL: " + dbUrl)
            }

            // Update Status Change flag on BillingCenter database
            gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
              _logger.debug("TDIC_PolicyPeriodEffectiveStatusBatchJob#doWork() - Marking status changed for policy period: "
                  + policyPeriod.PolicyNumberLong)
              policyPeriod = bundle.add(policyPeriod)
              policyPeriod.StatusChangeRecorded_TDIC = true
            }, SUPER_USER)
          }
        } catch(sqle : SQLException) {
          incrementOperationsFailed()
          _logger.error("TDIC_PolicyPeriodEffectiveStatusBatchJob#doWork - Database connectivity/execution error "
              + "persisting transaction for policy period: " + policyPeriod.PolicyNumberLong, sqle)
          if (dbConnection != null) {
            // Attempt to rollback
            try {
              dbConnection.rollback()
            } catch (rbe: Exception) {
              _logger.error("TDIC_PolicyPeriodEffectiveStatusBatchJob#doWork - Exception while rolling back DB", rbe)
            }
          }
        } catch (e : Exception) {
          incrementOperationsFailed()
          _logger.error("TDIC_PolicyPeriodEffectiveStatusBatchJob#doWork() - Unexpected error processing policy period: " + policyPeriod.PolicyNumberLong, e)
          if (dbConnection != null) {
            // Attempt to rollback
            try {
              dbConnection.rollback()
            } catch (rbe: Exception) {
              _logger.error("TDIC_PolicyPeriodEffectiveStatusBatchJob#doWork - Exception while rolling back DB", rbe)
            }
          }
        }
      }
      if (OperationsFailed > 0) {
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
            "The Policy Period Effective Status batch job has failed due to an unexpected error. Please see logs for details.")
      }
      else {
        EmailUtil.sendEmail(_notificationEmail, EMAIL_SUBJECT_COMPLETED,
            "The Policy Period Effective Status batch process has completed successfully with no errors.")
      }
      _logger.info("TDIC_PolicyPeriodEffectiveStatusBatchJob#doWork() - Finished processing")
    }
  }

  /**
   * Determines if the process should run the doWork() method, based on if there are policy periods that need to be processed.
   *
   * @return A boolean indicating if the batch process should proceed with the doWork() method.
   */
  function checkInitialConditions_TDIC() : boolean {
    _logger.debug("TDIC_PolicyPeriodEffectiveStatusBatchJob#checkInitialConditions() - Entering")

    _notificationEmail = PropertyUtil.getInstance().getProperty(NOTIFICATION_EMAIL_KEY)
    if (_notificationEmail == null) {
      throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
          + NOTIFICATION_EMAIL_KEY + "' from integration database.")
    }

    _failureNotificationEmail = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)
    if (_failureNotificationEmail == null) {
      throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
          + FAILURE_NOTIFICATION_EMAIL_KEY + "' from integration database.")
    }

    var currDateIgnoreTime = DateUtil.currentDate().trimToMidnight()
    var ppQuery = gw.api.database.Query.make(PolicyPeriod)
    ppQuery.compare(PolicyPeriod#StatusChangeRecorded_TDIC, Equals, false)
    ppQuery.compare(PolicyPeriod#PolicyPerEffDate, LessThanOrEquals, currDateIgnoreTime)
    _policyPeriodsToProcess = ppQuery.select()

    if (_policyPeriodsToProcess.Count == 0) {
      _logger.debug("TDIC_PolicyPeriodEffectiveStatusBatchJob#checkInitialConditions() - No policy periods to be processed. Exiting batch process.")
      return false
    }
    else {
      _logger.debug("TDIC_PolicyPeriodEffectiveStatusBatchJob#checkInitialConditions() - Results returned. Number of policy periods to be processed: " + _policyPeriodsToProcess.Count)
      OperationsExpected = _policyPeriodsToProcess.Count
      return true
    }
  }

  /**
   * Called when the batch process is to be terminated.  This sets the TerminateRequested flag and returns true to indicate that the process can be stopped.
   *
   * @return A boolean to indicate that the process can be stopped.
   */
  override function requestTermination() : boolean {
    _logger.info("TDIC_PolicyPeriodEffectiveStatusBatchJob#requestTermination - Terminate requested for batch process.")
    //Set TerminateRequested to true by calling super method
    super.requestTermination()
    return true
  }

}