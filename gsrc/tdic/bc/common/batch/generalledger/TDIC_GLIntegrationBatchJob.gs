package tdic.bc.common.batch.generalledger

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.api.system.server.ServerUtil
uses gw.api.util.DateUtil
uses gw.pl.exception.GWConfigurationException
uses gw.processes.BatchProcessBase
uses org.apache.commons.io.FileUtils
uses org.apache.commons.lang3.StringUtils
uses org.slf4j.LoggerFactory
uses tdic.bc.integ.plugins.generalledger.dto.GLAccount
uses tdic.bc.integ.plugins.generalledger.dto.GLBucketGroup
uses tdic.bc.integ.plugins.generalledger.dto.GLFile
uses tdic.bc.integ.plugins.generalledger.dto.GLRuntimeException
uses tdic.bc.integ.plugins.generalledger.dto.GLTransactionLineBeanInfo
uses tdic.bc.integ.plugins.generalledger.dto.GLTransactionLineWritableBean
uses tdic.bc.integ.plugins.generalledger.helper.TDIC_GLIntegrationHelper

uses java.io.File
uses java.io.IOException
uses java.sql.Connection
uses java.sql.ResultSet
uses java.sql.SQLException
uses java.text.SimpleDateFormat

/**
 * US570 - General Ledger Integration
 * 11/13/2014 Alvin Lee
 *
 * Custom batch process implementation to read the external integration database table for GL transactions and write the
 * currently unwritten items to a file to be processed by the external system.  This custom batch process can be
 * scheduled, or executed from the UI.
 */
class TDIC_GLIntegrationBatchJob extends BatchProcessBase {

  /**
   * Standard logger for General Ledger
   */
  private static final var _logger = LoggerFactory.getLogger("GENERAL_LEDGER")

  /**
   * Prefix of the AR and Refund flat file (value: GW-LGL-GLT-).
   */
  public static final var GLT_FILE_PREFIX : String = "GW-LGL-GLT-"

  /**
   * Suffix of the AR and Refund flat file (value: .glt).
   */
  public static final var GLT_FILE_SUFFIX : String = ".GLT"

  /**
   * Key for looking up the file output directory from the integration database (value: gl.outputdir).
   */
  public static final var OUTPUT_DIR_PATH : String = "gl.outputdir"

  /**
   * Key for looking up the notification email addresses from the integration database (value: GLNotificationEmail).
   */
  public static final var NOTIFICATION_EMAIL_KEY : String = "GLNotificationEmail"

  /**
   * Key for looking up the failure notification email addresses from the integration database (value: BCInfraIssueNotificationEmail).
   */
  public static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "BCInfraIssueNotificationEmail"

  /**
   * Absolute directory path/string to use to write files.
   */
  private var _destinationDirectoryPath : String = null

  /**
   * Notification email addresses in case of any errors in batch process.
   */
  private var _notificationEmail : String = null

  /**
   * Failure notification email addresses in case of any errors in batch process.
   */
  private var _failureNotificationEmail : String = null

  /**
   * Notification email's subject for General Ledger batch job completion.
   */
  public static final var EMAIL_SUBJECT_COMPLETED: String = "General Ledger Batch Job Completed on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * Notification email's subject for General Ledger batch job failures.
   */
  public static final var EMAIL_SUBJECT_FAILURE: String = "General Ledger Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * Number of transactions to process as a part of a single batch run.
   */
  private var _transactionResults:ResultSet = null

  /**
   * The GLTransactionLineBeanInfo to interact with the external database.
   */
  private var _beanInfo:GLTransactionLineBeanInfo = null

  /**
   * The connection to the external database.
   */
  private var _dbConnection:Connection = null

  /**
   * Key for looking up the Integration DB URL from properties file
   */
  public static final var INTEGRATION_DB_URL_KEY : String = "IntDBURL"

  private static final var surchargePatterens = {"NJ Insurance Guarantee Surcharge","Firefighter Relief Surcharge","Fire Safety Surcharge","CA Insurance Guarantee Surcharge"}

  /**
   * Default constructor with no parameters.
   */
  construct() {
    super(BatchProcessType.TC_GENERALLEDGER)
  }

  /**
   * Implementation of interface method to execute work in a batch process.  This batch process loads all unwritten transactions
   * from the external database, writes the lines to an external file, and marks the database entries as written
   */
  override function doWork() {
    _logger.info("TDIC_GLIntegrationBatchJob#doWork() - Entering")
    //GINTEG-1135 : Skip if we don't have any data to process
    _logger.info("TDIC_CashReceiptsBatchJob#doWork() - Skip if we don't have any data to process")
    if (checkInitialConditions_TDIC()) {
      // Set up file to write data
      var sdfFile = new SimpleDateFormat("MM-dd-yyyy-HHmmss")
      var destFile = new File(_destinationDirectoryPath + "/" + GLT_FILE_PREFIX + sdfFile.format(DateUtil.currentDate()) + GLT_FILE_SUFFIX)
      _logger.debug("TDIC_GLIntegrationBatchJob#doWork() - Destination file constructed ${destFile.AbsolutePath}")

      var dateGLAccountMap = new HashMap<String, GLAccount>()

      try {
        var metaData = _transactionResults.MetaData

        while (_transactionResults.next()) {
          //Check TerminateRequested flag before proceeding with each entry
          if (TerminateRequested) {
            _logger.info("TDIC_GLIntegrationBatchJob#doWork() - Terminate requested during doWork() method. Progress: " + Progress + " transactions to be rolled back.")
            // Rollback database transaction
            _dbConnection.rollback()
            return
          }

          //Increment operations completed for the purpose of keeping progress.  Errors are not incremented since a single failure terminates all.
          incrementOperationsCompleted()

          // Process transaction
          var transLine = new GLTransactionLineWritableBean()
          transLine.Amount = _transactionResults.getBigDecimal("Amount")
          transLine.Description = _transactionResults.getString("Description")
          transLine.GWTAccountName = _transactionResults.getString("GWTAccountName")
          transLine.InvoiceStatus = _transactionResults.getString("InvoiceStatus")
          transLine.LineItemType = _transactionResults.getString("LineItemType")
          transLine.LOB = _transactionResults.getString("LOB")
          transLine.MappedTAccountName = _transactionResults.getString("MappedTAccountName")?.remove("-")
          transLine.PolicyNumber = _transactionResults.getString("PolicyNumber")
          transLine.PolicyPeriodStatus = _transactionResults.getString("PolicyPeriodStatus")
          transLine.StateCode = _transactionResults.getString("StateCode")
          transLine.TermNumber = _transactionResults.getInt("TermNumber")
          transLine.TransactionDate = _transactionResults.getString("TransactionDate")
          transLine.TransactionSubtype = _transactionResults.getString("TransactionSubtype")

          // If needed, Add new set of buckets for the account unit
          var accountUnitKey : String
          try {
            var chargeName = _transactionResults.getString("ChargeName")
            var compAccCode : CompanyAccountCode_TDIC

            if(chargeName != null){
              var gwTAccLastName = StringUtils.substringAfterLast(transLine.GWTAccountName, " ")
              if(chargeName.containsIgnoreCase("Policy Payment Reversal Fee") ||
                  chargeName.containsIgnoreCase("Credit Card Usage Fee")){
                if (TDIC_GLIntegrationHelper.isValidTransactionForAccUnit(transLine) ||
                    (transLine.TransactionSubtype == typekey.Transaction.TC_CHARGEBILLED.Code && transLine.LineItemType == LedgerSide.TC_CREDIT.Code))
                  compAccCode = CompanyAccountCode_TDIC.TC_SALES
              }else if(chargeName.containsIgnoreCase("Deductible")){
                if(TDIC_GLIntegrationHelper.isValidTransactionForAccUnit(transLine) ||
                    (transLine.TransactionSubtype == typekey.Transaction.TC_CHARGEBILLED.Code && gwTAccLastName.equalsIgnoreCase(GLOriginalTAccount_TDIC.TC_UNBILLED.DisplayName)))
                  compAccCode = CompanyAccountCode_TDIC.TC_INCOMESTATEMENT
              }else if(surchargePatterens.contains(chargeName)){
                if((transLine.TransactionSubtype == typekey.Transaction.TC_CHARGEWRITTENOFF.Code && gwTAccLastName.equalsIgnoreCase(GLOriginalTAccount_TDIC.TC_WRITEOFFEXPENSE.DisplayName)) ||
                    (transLine.TransactionSubtype == typekey.Transaction.TC_ACCOUNTNEGATIVEWRITEOFFTXN.Code && gwTAccLastName.equalsIgnoreCase(GLOriginalTAccount_TDIC.TC_NEGATIVEWRITEOFF.DisplayName)))
                  compAccCode = CompanyAccountCode_TDIC.TC_BALSHEET
              } else {
                if (TDIC_GLIntegrationHelper.isValidTransactionForAccUnit(transLine))
                  compAccCode = CompanyAccountCode_TDIC.TC_SALES
              }
            }
            compAccCode = compAccCode != null ? compAccCode : CompanyAccountCode_TDIC.TC_BALSHEET
            //GINTEG-1656
            if(transLine.MappedTAccountName?.startsWith("1") or transLine.MappedTAccountName?.startsWith("2")){
              accountUnitKey = "000.00.00"
            }else {
              accountUnitKey = TDIC_GLIntegrationHelper.getMappedAccountUnit(compAccCode, transLine.LOB, transLine.StateCode)
            }

            _logger.debug("TDIC_GLIntegrationBatchJob#doWork()- AccountUnitKey ${accountUnitKey} for CompanyAccountCode:${compAccCode}:LOB:${transLine.LOB}:StateCode:${transLine.StateCode}")
          } catch (e : Exception) {
            incrementOperationsFailed()
            _logger.warn("TDIC_GLIntegrationBatchJob#doWork() - Not able to find Mapped Account Unit.  Line has been skipped but batch job will continue.  Reason: " + e.LocalizedMessage, e)
          }
          if (!accountUnitKey.HasContent)
            continue

          // If needed, Add new account unit for the transaction date
          var glAccount = dateGLAccountMap.get(transLine.TransactionDate.substring(0, 10))
          if (glAccount == null) {
            glAccount = new GLAccount()
            _logger.debug("TDIC_GLIntegrationBatchJob#doWork - Adding new account unit to the date: " + transLine.TransactionDate)
            dateGLAccountMap.put(transLine.TransactionDate.substring(0, 10), glAccount)
          }
          //As part R2 Mapping updates.Business confirmed to group GL transactions based on both AccountUnit,MappedTAccountName
          // GINTEG-1314: Some of the MappedTAccountName for ChargeBilled transaction is not written into GL file
          var accUnitBucketKey = "${accountUnitKey}_${transLine.MappedTAccountName}"
          var existingBucketGroup = glAccount.AccountUnitBucketGroupMap.get(accUnitBucketKey)
          if (existingBucketGroup == null) {
            existingBucketGroup = new GLBucketGroup()
            _logger.debug("TDIC_GLIntegrationBatchJob#doWork - Adding GL buckets for new account unit and MappedTAccountName: " + accUnitBucketKey)
            glAccount.AccountUnitBucketGroupMap.put(accUnitBucketKey, existingBucketGroup)
          }

          try {
            switch (transLine.TransactionSubtype) {
              case typekey.Transaction.TC_DISBURSEMENTPAID.Code:
                TDIC_GLIntegrationHelper.processDisbursementTransaction(transLine, existingBucketGroup)
                break

              case typekey.Transaction.TC_CHARGEWRITTENOFF.Code:
                TDIC_GLIntegrationHelper.processWriteOffTransaction(transLine, existingBucketGroup)
                break

              case typekey.Transaction.TC_ACCOUNTNEGATIVEWRITEOFFTXN.Code:
                TDIC_GLIntegrationHelper.processNegativeWriteOffTransaction(transLine, existingBucketGroup)
                break

              case typekey.Transaction.TC_CHARGEBILLED.Code:
                TDIC_GLIntegrationHelper.processChargeBilledTransaction(transLine, existingBucketGroup)
                break

              case typekey.Transaction.TC_CHARGEPAIDFROMACCOUNT.Code:
                TDIC_GLIntegrationHelper.processChargePaidFromAccountTransaction(transLine, existingBucketGroup)
                break

              case typekey.Transaction.TC_POLICYPERIODBECAMEEFFECTIVE.Code:
                TDIC_GLIntegrationHelper.processPolicyPeriodBecameEffectiveTransaction(transLine, existingBucketGroup)
                break

              case typekey.Transaction.TC_ACCOUNTCOLLECTIONTXN.Code:
                TDIC_GLIntegrationHelper.processAccountCollectionTransaction(transLine, existingBucketGroup)
                break

              case typekey.Transaction.TC_ACCOUNTGOODWILLTXN.Code:
                TDIC_GLIntegrationHelper.processAccountGoodwillTransaction(transLine, existingBucketGroup)
                break

              case typekey.Transaction.TC_ACCOUNTINTERESTTXN.Code:
                TDIC_GLIntegrationHelper.processAccountInterestTransaction(transLine, existingBucketGroup)
                break

              case typekey.Transaction.TC_ACCOUNTOTHERTXN.Code:
                TDIC_GLIntegrationHelper.processAccountOtherTransaction(transLine, existingBucketGroup)
                break

              default:
                throw new GLRuntimeException("Unexpected transaction type '" + transLine.TransactionSubtype + "' in GL table")
            }

            //Mark the line as written
            transLine.PublicID = _transactionResults.getString(_beanInfo.IDColumnSQLName)
            transLine.markAsProcessed(_dbConnection)
            _logger.trace("TDIC_GLIntegrationBatchJob#doWork() - Entry marked as written")
          } catch (glre : GLRuntimeException) {
            incrementOperationsFailed()
            _logger.warn("TDIC_GLIntegrationBatchJob#doWork() - Invalid line in the GL integration database.  Line has been skipped but batch job will continue.  Error: " + glre.LocalizedMessage, glre)
          }
        }

        // Set up object for delimited file utility
        var glFile = new GLFile()
        var sdf = new SimpleDateFormat("yyyy-MM-dd")

        for (date in dateGLAccountMap.Keys) {
          var sequenceNumberCounter = 1 //Reset sequence number for each date
          var transactionDate = sdf.parse(date)
          var glAccount = dateGLAccountMap.get(date)
          _logger.debug("TDIC_GLIntegrationBatchJob#doWork() - Final account unit information for date: " + date)
          for (accUnitBucketKey in glAccount.AccountUnitBucketGroupMap.Keys) {
            // Get all buckets
            var bucketGroup = glAccount.AccountUnitBucketGroupMap.get(accUnitBucketKey)
            var nonZeroBuckets = bucketGroup.NameBucketMap.filterByValues(\bucket -> !bucket.TotalBucketAmount.IsZero)

            _logger.debug("TDIC_GLIntegrationBatchJob#doWork() - Final bucket totals for account unit and MappedTAccountName: " + accUnitBucketKey)
            for (bucketName in nonZeroBuckets.keySet()) {
              var bucket = nonZeroBuckets.get(bucketName)
              bucket.Date = sdf.parse(date)
              bucket.AccountUnit = accUnitBucketKey.substring(0,accUnitBucketKey.indexOf("_"))
              bucket.UID = sequenceNumberCounter
              glFile.addToGLBuckets(bucket)
              _logger.debug("  " + bucketName + " - " + bucket.Description + ": " + bucket.TotalBucketAmount)
              sequenceNumberCounter++
            }
          }
        }

        //Write lines and then commit transaction
        var lines = TDIC_GLIntegrationHelper.convertDataToFlatFileLines(glFile)
        FileUtils.writeLines(destFile, lines)
        _dbConnection.commit()
        if (OperationsFailed > 0) {
          EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
              "The General Ledger batch process has completed, but with errors.  Invalid transactions were found in the GL integration database and were skipped.")
        } else {
          EmailUtil.sendEmail(_notificationEmail, EMAIL_SUBJECT_COMPLETED, "The General Ledger batch process has completed successfully with no errors.")
        }
      } catch (ioe : IOException) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_GLIntegrationBatchJob#doWork() - Error while writing to output file " + destFile.AbsolutePath, ioe)
        // Send email notification of failure
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
            "The General Ledger batch job has failed due to an error while writing to output file: " + ioe.LocalizedMessage)
        _dbConnection.rollback()
      } catch (sqle : SQLException) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_GLIntegrationBatchJob#doWork() - Database connection error while attempting to access integration database table", sqle)
        // Send email notification of failure
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
            "The General Ledger batch job has failed due to a database connection error while attempting to access the integration database: " + sqle.LocalizedMessage)
        _dbConnection.rollback()
      } catch (e : Exception) {
        OperationsFailed = OperationsExpected
        _logger.error("TDIC_GLIntegrationBatchJob#doWork() - Error processing output file: " + e.LocalizedMessage, e)
        // Send email notification of failure
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
            "The General Ledger batch job has failed due to an unexpected error while processing the output file: " + e.LocalizedMessage)
        _dbConnection.rollback()
      } finally {
        try {
          if (_dbConnection != null) {
            _logger.debug("TDIC_GLIntegrationBatchJob#doWork() - Closing database connection")
            DatabaseManager.closeConnection(_dbConnection)
          }
        } catch (e : Exception) {
          _logger.error("TDIC_GLIntegrationBatchJob#doWork() - Exception on closing DB (continuing)", e)
        }
      }
    }
    _logger.info("TDIC_GLIntegrationBatchJob#doWork() - Finished processing")
  }

    /**
     * Determines if the process should run the doWork() method, based on if there are transactions that need to have flat files written.
     *
     * @return A boolean indicating if the batch process should proceed with the doWork() method.
     */
    function checkInitialConditions_TDIC() : boolean {
      _logger.debug("TDIC_GLIntegrationBatchJob#checkInitialConditions() - Entering")

      var dbUrl : String
      try {
        var env = ServerUtil.getEnv()?.toLowerCase()
        if (env == null) {
          throw new GWConfigurationException("No environment specified.  Cannot load integration database.")
        }
        dbUrl = PropertyUtil.getInstance().getProperty(INTEGRATION_DB_URL_KEY)
        if (dbUrl == null) {
          throw new GWConfigurationException("Integration Database not defined.")
        }

        _notificationEmail = PropertyUtil.getInstance().getProperty(NOTIFICATION_EMAIL_KEY)
        if (_notificationEmail == null) {
          throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
              + NOTIFICATION_EMAIL_KEY + "' from integration database.")
        }

        _failureNotificationEmail = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)
        if (_failureNotificationEmail == null) {
          throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
              + FAILURE_NOTIFICATION_EMAIL_KEY + "' from integration database.")
        }

        _destinationDirectoryPath = PropertyUtil.getInstance().getProperty(OUTPUT_DIR_PATH)
        if (_destinationDirectoryPath == null) {
          throw new GWConfigurationException("Cannot retrieve file output path with the key '" + OUTPUT_DIR_PATH
              + "' from integration database.")
        }
      } catch (gwce : GWConfigurationException) {
        // Send email notification of failure
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
            "The General Ledger batch job has failed due to the integration database either being not property set up or missing required properties.")
        throw gwce
      }

      try {
        // Get a connection from DatabaseManager using the GWINT DB URL
        _dbConnection = DatabaseManager.getConnection(dbUrl)
        _beanInfo = new GLTransactionLineBeanInfo()
        var stmt = _beanInfo.createRetrieveAllSQL(_dbConnection)
        _transactionResults = stmt.executeQuery()
        _transactionResults.last()
        //If the last transaction is row 0, there are no transactions to process
        if (_transactionResults.Row != 0) {
          _logger.debug("TDIC_GLIntegrationBatchJob#checkInitialConditions() - Results returned. Number of transactions to be processed: " + _transactionResults.Row)
          OperationsExpected = _transactionResults.Row
          _transactionResults.beforeFirst()
          return true
        }
        else {
          // Send email notification of failure
          if (_failureNotificationEmail != null) {
            EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
                "The General Ledger batch job did not run, due to having no transactions to be processed.")
          }
          _logger.warn("TDIC_GLIntegrationBatchJob#checkInitialConditions() - No transactions to be processed. Exiting batch process.")
          // Attempt to close connection
          if (_dbConnection != null) {
            try {
              DatabaseManager.closeConnection(_dbConnection)
            } catch (e : Exception) {
              _logger.error("TDIC_GLIntegrationBatchJob#checkInitialConditions() - Exception on closing DB (continuing)", e)
            }
          }
          return false
        }
      } catch (sqle : SQLException) {
        _logger.error("TDIC_GLIntegrationBatchJob#checkInitialConditions() - Database connection error while attempting to read integration database", sqle)
        // Send email notification of failure
        if (_failureNotificationEmail != null) {
          EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
              "The General Ledger batch job has failed due to a database connection error while attempting to read the integration database: " + sqle.LocalizedMessage)
        }
        // Attempt to close connection
        if (_dbConnection != null) {
          try {
            DatabaseManager.closeConnection(_dbConnection)
          } catch (e : Exception) {
            _logger.error("TDIC_GLIntegrationBatchJob#checkInitialConditions() - Exception on closing DB (continuing)", e)
          }
        }
        throw sqle
      } catch (e : Exception) {
        // Handle other errors
        _logger.error("TDIC_GLIntegrationBatchJob#checkInitialConditions() - Unexpected error while attempting to read integration database", e)
        // Send email notification of failure
        if (_failureNotificationEmail != null) {
          EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE,
              "The General Ledger batch job has failed due to an unexpected error while attempting to read the integration database: " + e.LocalizedMessage)
        }
        // Attempt to close connection
        if (_dbConnection != null) {
          try {
            DatabaseManager.closeConnection(_dbConnection)
          } catch (e2 : Exception) {
            _logger.error("TDIC_GLIntegrationBatchJob#checkInitialConditions() - Exception on closing DB (continuing)", e2)
          }
        }
        throw e
      }
    }

    /**
     * Called when the batch process is to be terminated.  This sets the TerminateRequested flag and returns true to indicate that the process can be stopped.
     *
     * @return A boolean to indicate that the process can be stopped.
     */
    override function requestTermination() : boolean {
      _logger.info("TDIC_GLIntegrationBatchJob#requestTermination - Terminate requested for batch process.")
      //Set TerminateRequested to true by calling super method
      super.requestTermination()
      return true
    }

  }