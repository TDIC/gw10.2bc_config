package tdic.bc.common.batch.eftreversal.helper

uses com.tdic.util.properties.PropertyUtil
uses gw.datatype.DataTypeException
uses gw.api.system.BCLoggerCategory
uses java.math.BigDecimal
uses gw.pl.exception.GWConfigurationException
uses com.tdic.util.misc.EmailUtil
uses gw.api.locale.DisplayKey
uses org.slf4j.LoggerFactory

/**
 * US1126 - CR - EFT/ACH Reversal Automation
 * 02/05/2015 Vicente
 *
 * Class that extends NACHAOperation. Store invoice number and return code, correct account number and correct routing number for a
 * payment instrument notification of change.
 */
class PaymentInstrumentChangeOperation extends NACHAOperation{

  /**
   * Notification of Change Code
   */
  private var _NOCCode : NACHAChangeCode_TDIC as NOCCode

  /**
   * Corrected Account Number
   */
  private var _correctedAccountNumber : String as CorrectedAccountNumber

  /**
   * Corrected Routing Number
   */
  private var _correctedRoutingNumber : String as CorrectedRoutingNumber

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("EFT_REVERSAL")

  /**
   * The custom batch process does not have a current user, so a user needs to be specified in order to execute
   * the bundle which adds or makes changes to an entity.  This is currently set to the Super User (value: su).
   * TODO May need to change this user before production
   */
  /**
   * 01/19/2016 Hermia Kho
   * Replace Super_User "su" with "iu"
   */
  public static final var SUPER_USER : String = "iu"

  /**
   * Key for looking up the notification email addresses from the integration database (value: PaymentNotificationEmail).
   */
  public static final var NOTIFICATION_EMAIL_KEY : String = "PaymentNotificationEmail"

  /**
   * Notification email's subject for EFT/ACH Reversal and Change Notifications batch job warning/notification.
   */
  public static final var EMAIL_SUBJECT_WARNING : String = "EFT/ACH Returns Batch Job Warning Notification on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * Constructor
   */
  @Param("amount", "Amount related to the payment.")
  @Param("invoiceNumber", "Invoice number related with the payment.")
  @Param("nocCode", "Notification of change code")
  @Param("correctAccountNumber", "Returned correct account number")
  @Param("correctRoutingNumber", "Returned correct routing number")
  construct(amount : BigDecimal, invoiceNumber: String, nocCode: NACHAChangeCode_TDIC, correctedAccountNumber: String, correctedRoutingNumber: String) {
    _amount = amount
    _invoiceNumber = invoiceNumber
    _NOCCode = nocCode
    _correctedAccountNumber = correctedAccountNumber
    _correctedRoutingNumber = correctedRoutingNumber
  }

  /**
   * Process Payment instrument change operations. If AccountNumber or RoutingNumber received, the account payment Instrument is
   * changed. Also a Trouble ticket is create if the return code is C04 -C014
   */
  @Param("paymentReverseOperation", "The PaymentReverseOperation object containing the data to reverse the payment.")
  @Throws(EFTReversalValidationException, "If routing number is not valid.")
  function process() {
    _logger.debug("PaymentInstrumentChangeOperation#process - Entering")

    var directBillMoneyRcvd = TDIC_EFTReversalAndChangeNotificationsBatchJobHelper.getDirectBillMoneyRcvdWithInvoiceNumber(this.Amount, this.InvoiceNumber)
    var account = directBillMoneyRcvd.Account
    var paymentInstrument = directBillMoneyRcvd.Invoice.InvoiceStream.getOriginalPaymentInstrument()


    gw.transaction.Transaction.runWithNewBundle(\bundle -> {

      //GWPS-1718 : Compare the Payment Instrument with the one on the Invoice Stream of the invoice as it is not always at the account level.
      if (directBillMoneyRcvd.PaymentInstrument == directBillMoneyRcvd.Invoice.InvoiceStream.getOriginalPaymentInstrument()) {
        /*Create a new PaymentInstrument with the new Bank Account details sent in the Bank/EFT Return File*/
        var newPI = new PaymentInstrument()
        newPI.PaymentMethod = PaymentMethod.TC_ACH
        newPI.Account = account
        newPI.BankName_TDIC = paymentInstrument.BankName_TDIC
        newPI.BankAccountNumber_TDIC = paymentInstrument.BankAccountNumber_TDIC
        newPI.BankABARoutingNumber_TDIC = paymentInstrument.BankABARoutingNumber_TDIC

        newPI = bundle.add(newPI)

        _logger.debug("PaymentInstrumentChangeOperation#process - payment instrument did not change since the payment was done.")
        if (this.CorrectedAccountNumber != null) {
          newPI.BankAccountNumber_TDIC = this.CorrectedAccountNumber
          _logger.debug("PaymentInstrumentChangeOperation#process - Account number changed to " + this.CorrectedAccountNumber)
        }

        try {
          if (this.CorrectedRoutingNumber != null) {
            newPI.BankABARoutingNumber_TDIC = this.CorrectedRoutingNumber
            _logger.debug("PaymentInstrumentChangeOperation#process - Routing number changed to " + this.CorrectedRoutingNumber)
          }
        } catch (e : DataTypeException) {
          _logger.warn("PaymentInstrumentChangeOperation#process() - "
              + DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.RoutingNumberIsNotValid", this.CorrectedRoutingNumber, e))
          throw new EFTReversalValidationException(DisplayKey.get("TDIC.Validation.EFTReversalAndChangeNotifications.RoutingNumberIsNotValid", this.CorrectedRoutingNumber, e))
        }
        /* Set the InvoiceStream OverridingPaymentInstrument to the new paymentinstrument */
        try {
          var invStream = directBillMoneyRcvd.Invoice.InvoiceStream
          invStream = bundle.add(invStream)
          invStream.OverridingPaymentInstrument = newPI
        }catch (e : Exception){
          var message = "Unable to update the new Bank Account/Routing Number to the InvoiceStream"
          _logger.error("PaymentInstrumentChangeOperation#process - " + message)
          _logger.error(e.StackTraceAsString)
        }


      } else {
        var message = "Account's payment instrument changed since the payment was done. Not modifying payment instrument with new values from the bank."
        _logger.info("PaymentInstrumentChangeOperation#process - " + message)
        // Send warning/notification email
        var notificationEmail = PropertyUtil.getInstance().getProperty(NOTIFICATION_EMAIL_KEY)
        if (notificationEmail == null) {
          throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
              + NOTIFICATION_EMAIL_KEY + "' from integration database.")
        }
        EmailUtil.sendEmail(notificationEmail, EMAIL_SUBJECT_WARNING, "EFT/ACH Returns Batch Job warning/notification: " + message)
      }

      if (notificationRequired(this.NOCCode)) {
        _logger.debug("PaymentInstrumentChangeOperation#process - Return code is "
            + this.NOCCode + ", a trouble ticket created.")
        TDIC_EFTReversalAndChangeNotificationsBatchJobHelper.createNotificationOfChangeTroubleTicket(account, directBillMoneyRcvd, this.NOCCode)
      }
    }, SUPER_USER)

    _logger.debug("PaymentInstrumentChangeOperation#process - Exiting")
  }

  /**
   * Returns if it is necessary create a trouble ticket for notificate this change if the input notification of code
   * is processed
   */
  @Param("nocCode", "Notification of change code")
  @Returns("True if it is necessary create a trouble ticket for notificate this change if the input notification of code is processed")
  public function notificationRequired(nocCode : NACHAChangeCode_TDIC) : boolean {
    return nocCode == typekey.NACHAChangeCode_TDIC.TC_C04 or
        nocCode == typekey.NACHAChangeCode_TDIC.TC_C05 or
        nocCode == typekey.NACHAChangeCode_TDIC.TC_C06 or
        nocCode == typekey.NACHAChangeCode_TDIC.TC_C07 or
        nocCode == typekey.NACHAChangeCode_TDIC.TC_C08 or
        nocCode == typekey.NACHAChangeCode_TDIC.TC_C09 or
        nocCode == typekey.NACHAChangeCode_TDIC.TC_C13 or
        nocCode == typekey.NACHAChangeCode_TDIC.TC_C14
  }
}