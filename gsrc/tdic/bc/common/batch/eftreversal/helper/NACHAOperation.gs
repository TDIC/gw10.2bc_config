package tdic.bc.common.batch.eftreversal.helper

uses java.math.BigDecimal

/**
 * US1126 - CR - EFT/ACH Reversal Automation
 * 02/05/2015 Vicente
 *
 * Class to store invoice the number for a NACHA operation
 */
abstract class NACHAOperation {

  /**
   * Invoice Number to identify the NACHA operation
   */
  protected var _invoiceNumber : String as InvoiceNumber

  /**
   * Amount of the NACHA operation
   */
  protected var _amount : BigDecimal as Amount


}