package tdic.bc.common.batch.eftreversal

uses com.tdic.util.properties.PropertyUtil
uses gw.processes.BatchProcessBase
uses gw.api.system.BCLoggerCategory
uses gw.pl.exception.GWConfigurationException
uses gw.api.system.server.ServerUtil
uses com.tdic.util.misc.FtpUtil
uses java.io.File
uses java.util.HashSet
uses java.lang.RuntimeException
uses java.io.IOException
uses java.lang.Exception
uses gw.api.database.Query
uses com.tdic.util.misc.EmailUtil
uses org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 10/28/15
 * US1126 - CR - EFT/ACH Reversal Automation
 * Implementation of a custom batch process for the The EFT/ACH Reversal and Change Notification Integration to download EFT/ACH reversal file.
 */
class TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob extends BatchProcessBase {

  /**
   * Reference to the group where the trouble tickets are going to be assigned
   */
  /**
   * GW654 - Change Trouble tickets assignment to "Finance APW"
   * Hermia Kho - 12/22/2015
   */
  var _financeAPWGroup : Group

  /**
   * A Set<File> indicating the files that need to be processed, as more than one NACHA return file can be processed at once.
   */
  private var _incomingFilesToProcess = new HashSet<File>()

  /**
   * Name of the vendor for purposes of connecting to the FTP (value: BofaReversalIncoming).
   */
  public static final var VENDOR_NAME : String = "BofaOutgoingACH"

  /**
   * Key for looking up the incoming file directory from the integration database (value: eftreversal.incomingdir).
   */
  public static final var INCOMING_DIRECTORY_KEY : String = "eftreversal.incomingdir"

  /**
   * Absolute directory path/string for incoming files.
   */
  private var _incomingPath : String = null

  /**
   * Absolute directory path/string for completed files.
   */
  private var _donePath : String = null

  /**
   * Key for looking up the done directory from the integration database (value: eftreversal.donedir).
   */
  public static final var DONE_DIRECTORY_KEY : String = "eftreversal.donedir"

  /**
   * Standard logger.
   */
  private static final var _logger = LoggerFactory.getLogger("EFT_REVERSAL")

  /**
   * Key for looking up the notification email addresses from the integration database (value: PaymentNotificationEmail).
   */
  public static final var NOTIFICATION_EMAIL_KEY : String = "PaymentNotificationEmail"

  /**
   * Notification email addresses in case of any errors in batch process.
   */
  private var _notificationEmail : String = null

  /**
   * Key for looking up the notification failure email addresses from the integration database (value: BCInfraIssueNotificationEmail).
   */
  public static final var NOTIFICATION_FAILURE_EMAIL_KEY : String = "BCInfraIssueNotificationEmail"

  /**
   * Notification email addresses in case of any errors in batch process.
   */
  private var _notificationFailureEmail : String = null

  /**
   * Notification email's subject for EFT/ACH Reversal and Change Notifications batch job failures.
   */
  public static final var EMAIL_SUBJECT_FAILURE : String = "EFT/ACH Returns Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * Notification email's subject for EFT/ACH Reversal and Change Notifications batch job success.
   */
  public static final var EMAIL_SUBJECT_SUCCESS : String = "EFT/ACH Returns Batch Job Success on server " + gw.api.system.server.ServerUtil.ServerId


  /**
   * Default constructor with no parameters.
   */
  construct() {
    super(BatchProcessType.TC_EFTREVERSALFILEDOWNLOADBATCHPROCESS)
  }

  /**
   * Runs the actual batch process.
   */
  override function doWork() {

    try {
      var fileList = FtpUtil.getFileList(VENDOR_NAME)
      var filesToDownload = new HashSet<String>()
      if (fileList == null) {
        throw new RuntimeException("Error retrieving list of files from the FTP server. Check integration log for stack trace.")
      }
      for (fileName in fileList) {
        _logger.debug("TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob#doWork() - Checking file from FTP server: " + fileName)
        // Regex match for NACHA return files. Return file name should be TDIC_ACH_RETURN.YYYYMMDD.txt
        if (fileName.matches("TDIC_ACH_RETURN.[0-9]{8}.txt") && !isPreviouslyCompletedFile(fileName)) {
          _logger.debug("TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob#doWork() - Adding file for processing: " + fileName)
          filesToDownload.add(fileName)
        }
      }

      for (fileToDownload in filesToDownload) {
        var incomingFileToProcess = new File(_incomingPath + "/" + fileToDownload)
        // Download file from server
        if (!FtpUtil.downloadFile(VENDOR_NAME, incomingFileToProcess)) {
          throw new RuntimeException("Unable to download EFT Reversal file with name '" + fileToDownload
              + "' from the FTP server. Check integration log for stack trace.")
        }
        _incomingFilesToProcess.add(incomingFileToProcess)
      }

      if (_incomingFilesToProcess.Empty) {
        _logger.info("TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob#doWork() - Exiting with no files to process.")
        EmailUtil.sendEmail(_notificationFailureEmail, EMAIL_SUBJECT_FAILURE,
            "The EFT/ACH Reversal and Change Notifications batch job has found no files from the bank to process.")

      } else {
        OperationsExpected = _incomingFilesToProcess.Count
        _logger.info("TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob#doWork() - Exiting with "
            + _incomingFilesToProcess.Count + " files to process.")
        EmailUtil.sendEmail(_notificationEmail, EMAIL_SUBJECT_SUCCESS, "The EFT/ACH Reversal and Change Notifications batch job completed with" + _incomingFilesToProcess.Count
            + " files downloaded to incoming directory in TDIC network.")

      }
    } catch (io : IOException) {
      _logger.error("TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob#doWork() - IOException= " + io)
      EmailUtil.sendEmail(_notificationFailureEmail, EMAIL_SUBJECT_FAILURE, "The EFT/ACH Reversal and Change Notifications batch job has failed due to error:" + io.LocalizedMessage)
      throw io
    } catch (e : Exception) {
      // Log
      _logger.error("TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob#doWork() - An unexpected error occurred while determining NACHA return files to process. Error: " + e.LocalizedMessage)
      EmailUtil.sendEmail(_notificationFailureEmail, EMAIL_SUBJECT_FAILURE,
          "The EFT/ACH Reversal and Change Notifications batch job has failed due to an unexpected error occurred while determining NACHA return files to process. Error: " + e.LocalizedMessage)
      throw e
    }
  }

  /**
   * Determines if the process should run the doWork() method, based on if there are payment reversal request files to process.
   */
  /**
   * GW666 change query to compare PublicID instead of Name
   * Hermia Kho - 12/15/2015
   */
  @Returns("A boolean indicating if the batch process should proceed with the doWork() method.")
  @Throws(GWConfigurationException, "If there are setup issues, such as issues with retrieving properties from the integration database")
  @Throws(RuntimeException, "If there are connection issues with the bank's FTP server")
  override function checkInitialConditions() : boolean {
    _logger.debug("TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob#checkInitialConditions() - Entering")

    var dbUrl : String
    try {
      // Get required values from integration database
      var env = ServerUtil.getEnv()?.toLowerCase()
      if (env == null) {
        throw new GWConfigurationException("No environment specified.  Cannot load integration database.")
      }
      dbUrl = PropertyUtil.getInstance().getProperty("IntDBURL")
      if (dbUrl == null) {
        throw new GWConfigurationException("Integration Database not defined.")
      }

      _notificationFailureEmail = PropertyUtil.getInstance().getProperty(NOTIFICATION_FAILURE_EMAIL_KEY)
      if (_notificationFailureEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification failure email addresses with the key '"
            + NOTIFICATION_FAILURE_EMAIL_KEY + "' from integration database.")
      }

      _notificationEmail = PropertyUtil.getInstance().getProperty(NOTIFICATION_EMAIL_KEY)
      if (_notificationEmail == null) {
        throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"
            + NOTIFICATION_EMAIL_KEY + "' from integration database.")
      }

      _financeAPWGroup = Query.make(Group).compare(Group#PublicID, Equals, "group:3").select().FirstResult
      if (_financeAPWGroup == null) {
        throw new GWConfigurationException("Finance APW group does not exists.")
      }

      _incomingPath = PropertyUtil.getInstance().getProperty(INCOMING_DIRECTORY_KEY)
      if (_incomingPath == null) {
        throw new GWConfigurationException("Cannot retrieve incoming file path with the key '" + INCOMING_DIRECTORY_KEY
            + "' from integration database.")
      }
      var incomingDirectory = new File(_incomingPath)
      if (!incomingDirectory.exists()) {
        if (!incomingDirectory.mkdirs()) {
          _logger.error("TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob#checkInitialConditions() - Failed to create incoming directory: " + _incomingPath)
          EmailUtil.sendEmail(_notificationFailureEmail, EMAIL_SUBJECT_FAILURE, "Failed to create incoming directory:" + _incomingPath)
          throw new GWConfigurationException("Failed to create incoming directory: " + _incomingPath)
        }
      }

      _donePath = PropertyUtil.getInstance().getProperty(DONE_DIRECTORY_KEY)
      if (_donePath == null) {
        throw new GWConfigurationException("Cannot retrieve completed file path with the key '" + DONE_DIRECTORY_KEY
            + "' from integration database.")
      }
      var doneDirectory = new File(_donePath)
      if (!doneDirectory.exists()) {
        if (!doneDirectory.mkdirs()) {
          _logger.error("TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob#checkInitialConditions() - Failed to create done directory: " + _donePath)
          EmailUtil.sendEmail(_notificationFailureEmail, EMAIL_SUBJECT_FAILURE, "Failed to create done directory:" + _donePath)
          throw new GWConfigurationException("Failed to create done directory: " + _donePath)
        }
      }

      return true

    } catch (gwce : GWConfigurationException) {
      // Log
      _logger.error("TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob#checkInitialConditions - Integration database either not property set up or missing required properties or group does not exists. Error: " + gwce.LocalizedMessage)
      EmailUtil.sendEmail(_notificationFailureEmail, EMAIL_SUBJECT_FAILURE,
          "The EFT/ACH Reversal and Change Notifications batch job has failed due to the integration database being not property set up or missing required properties or group does not exists. Error: " + gwce.LocalizedMessage)
      throw gwce
    } catch (ex : Exception) {
      _logger.error("TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob#checkInitialConditions - Exception= " + ex.LocalizedMessage)
      EmailUtil.sendEmail(_notificationFailureEmail, EMAIL_SUBJECT_FAILURE, "The EFT/ACH Reversal and Change Notifications batch job has failed due to error:" + ex.LocalizedMessage)
      throw ex
    }
  }

  /**
   * Checks done directory for matching file names.  This ensures that no duplicate NACHA return files are processed.
   */
  @Param("fileName", "A String containing the file name to check")
  @Returns("A boolean indicating if the file name shows up in the done directory as a previously completed file")
  @Throws(GWConfigurationException, "If there are setup issues with the local server, such as a non-existent directory path")
  private function isPreviouslyCompletedFile(fileName : String) : boolean {
    _logger.debug("TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob#isPreviouslyCompletedFile - Entering")
    var doneDirectory = new File(_donePath)
    if (!doneDirectory.exists()) {
      _logger.info("TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob#isPreviouslyCompletedFile - Completed files directory does not exist: " + _donePath)
    } else {
      for (completedFile in doneDirectory.listFiles()) {
        if (completedFile.Name.equalsIgnoreCase(fileName)) {
          _logger.info("TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob#isPreviouslyCompletedFile - Existing file: " + fileName + ". Skipping file.")
          return true
        }
      }
    }
    _logger.debug("TDIC_EFTReversalAndChangeNotificationsFileDownloadBatchJob#isPreviouslyCompletedFile - File does not exist: " + fileName)
    return false
  }


}