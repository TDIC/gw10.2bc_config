package tdic.bc.common.batch.checkstatus.dto

uses java.math.BigDecimal
uses java.util.Date

/**
 * US566
 * 02/02/2015 Kesava Tavva
 *
 * Object used to store fields for check status update records retrieved from Lawson.
 */
class TDIC_CheckStatusRecord {
  var _transactionRecordID : String as TransactionRecordID
  var _checkDate : Date as  CheckIssueDate
  var _checkNumber : String as CheckNumber
  var _checkPayee : String as CheckPayee
  var _vendorID : String as VendorID
  var _checkStatus : String as CheckStatus
  var _checkStatusDate : Date as CheckStatusDate
  var _checkAmount : BigDecimal as CheckAmount
  var _comments : String as Comments

  /**
   * US566
   * 02/02/2015 Kesava Tavva
   *
   * Override function to return field values in a String object.
   */
  override function toString() : String{
    return "${TransactionRecordID} | ${CheckIssueDate} | ${CheckNumber} | ${CheckPayee} | ${VendorID} | ${CheckStatus} | ${CheckStatusDate} | ${CheckAmount}"
  }
}