package tdic.bc.config.enhancement

uses gw.pl.currency.MonetaryAmount

enhancement UnappliedFundEnhancement : entity.UnappliedFund {
  /**
   * Return the policy period used for invoicing.
   */
  public property get PolicyPeriod_TDIC() : PolicyPeriod {
    var retval : PolicyPeriod;
    var policyPeriods : PolicyPeriod[];

    policyPeriods = this.Account.Policies*.PolicyPeriods;

    policyPeriods = policyPeriods.where(\pp -> pp.InvoiceStream.UnappliedFund == this);
    if (policyPeriods.Count > 0) {
      retval = policyPeriods.first();
    }

    return retval;
  }

  /**
   * Is this a term-level unapplied fund.
   */
  public property get IsTermLevelUnapplied() : boolean {
    return this.PolicyPeriod_TDIC != null;
  }

  /**
   * Amount of unapplied targeted against planned invoices.
   */
  public property get TargetedForPlannedInvoices_TDIC() : MonetaryAmount {
    var amount = new MonetaryAmount (0, this.Currency);
    var policyPeriod = this.PolicyPeriod_TDIC;
    if (policyPeriod != null) {
      var invoiceAmount = policyPeriod.OverridingInvoiceStream.Invoices.sum(\i -> i.AmountDue);
      if (this.Balance.IsPositive) {
        amount = this.Balance.min(invoiceAmount);
      }
    }
    this.setFieldValue("PlannedInvoicesInt_TDIC", amount);
    return amount;
  }
}
