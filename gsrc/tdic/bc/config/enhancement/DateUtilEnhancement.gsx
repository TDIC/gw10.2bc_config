package tdic.bc.config.enhancement

uses java.util.GregorianCalendar
uses java.util.Calendar
uses java.util.Date

enhancement DateUtilEnhancement : gw.api.util.DateUtil {
  /**
   * The number or months between the two input dates
   */
  @Param("startDate","Start date to compare")
  @Param("endDate","End date to compare")
  @Returns("The number or months between the two imput dates")
  public static function getMonthsBetweenDates(startDate: Date, endDate: Date) : int {
    var startCalendar = new GregorianCalendar()
    startCalendar.setTime(startDate)
    var endCalendar = new GregorianCalendar()
    endCalendar.setTime(endDate)

    var diffYear = endCalendar.get( Calendar.YEAR) - startCalendar.get(Calendar.YEAR)
    return diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH)
  }
}
