package tdic.bc.config.enhancement

uses java.util.Set

enhancement TDIC_BaseDistEnhancement : entity.BaseDist {

 /**
  *
  * Return invoices associated with Payment reversal
  */
  property get Invoices_TDIC(): Set<Invoice>{
    return this.ReversedDistItems*.InvoiceItem*.Invoice?.toSet()
  }
}
