package tdic.bc.config.payment

uses gw.api.web.payment.DirectBillPaymentFactory
uses gw.api.webservice.exception.DataConversionException
uses gw.api.webservice.exception.RequiredFieldException
uses gw.webservice.bc.bc1000.PaymentAPI
uses gw.webservice.bc.bc1000.PaymentReceiptRecord
/*uses gw.webservice.bc.bc1000.util.WebserviceEntityLoader
uses gw.webservice.bc.bc1000.util.WebservicePreconditions*/
uses gw.webservice.bc.bc1000.PaymentReceipts
uses gw.pl.persistence.core.Bundle
uses gw.webservice.util.WebserviceEntityLoader
uses gw.webservice.util.WebservicePreconditions

class PaymentHelper extends gw.webservice.bc.bc1000.PaymentAPI {

  /**
   * Makes a payment to an Account or to a specific Policy on an Account. If no policyPeriodID is passed in, the payment will be distributed
   * to the children policies of the Account using standard payment distribution.
   * <p/>
   * If a non-null policyPeriodID parameter is passed in, the payment will be targeted at that specific PolicyPeriod. If there is any extra money left in the payment
   * after paying the target PolicyPeriod, it will be dealt with based on the PolicyLevelPaymentOption of the Account.
   * <p/>
   * @param directbillPaymentReceipt A PaymentReceiptRecord of subtype DirectBillMoneyDetails
   * @param policyPeriodID The PolicyPeriod to make the payment to
   * @return directBillMoneyRcvdID The PublicID of the payment that is created
   */
  @Throws(RequiredFieldException, "If the directbillPaymentReceipt fields AccountID, PaymentInstrumentRecord, or Amount is null")
  function makeDirectBillPaymentToPolicyPeriod(directbillPaymentReceipt: PaymentReceiptRecord, policyPeriodID: String, bundle: Bundle): String {
    validateDirectBillPaymentReceipt(directbillPaymentReceipt)
    var directBillMoneyRcvd: DirectBillMoneyRcvd
      var account = bundle.add(WebserviceEntityLoader.loadAccount(directbillPaymentReceipt.AccountID))
      var unappliedFund = account.DefaultUnappliedFund
      var policyPeriod = null as PolicyPeriod
      var isTargetedToPolicy = false
      if (policyPeriodID != null and account.PolicyLevelBillingWithDesignatedUnapplied) {
        isTargetedToPolicy = true
        policyPeriod = bundle.add(WebserviceEntityLoader.loadPolicyPeriod(policyPeriodID))
        unappliedFund = policyPeriod.OverridingInvoiceStream.UnappliedFund
        if(unappliedFund == null){
          unappliedFund = policyPeriod.Policy.getDesignatedUnappliedFund(account)
          if(unappliedFund == null){
            // The designated UnappliedFund for this policy with the given account owner does not exist
            isTargetedToPolicy = false
            unappliedFund = account.DefaultUnappliedFund
          }
        }
      }
      var directBillMoneyDetails = PaymentReceipts.toEntity(directbillPaymentReceipt, bundle)
      directBillMoneyRcvd = DirectBillPaymentFactory.createAndExecuteMoneyReceivedFromPaymentReceipt(unappliedFund, directBillMoneyDetails as DirectBillMoneyDetails, directbillPaymentReceipt.ReceivedDate, directbillPaymentReceipt.Description)
      if (isTargetedToPolicy) {
        directBillMoneyRcvd.PolicyPeriod = policyPeriod
      }
      /**
       * US567 - Bank/Lockbox Integration
       * 08/19/2014 Alvin Lee
       *
       * Set the Description on the DirectBillMoneyRcvd to include the lockbox transaction number, and distribute the payment.
       */
      directBillMoneyRcvd.Description = "Payment from Lockbox Transaction " + directbillPaymentReceipt.LockboxTransactionNumber_TDIC
      directBillMoneyRcvd.distribute()
    return directBillMoneyRcvd.PublicID
  }

  /**
   * Makes a SuspensePayment to an Account or Policy. Exactly one of the AccountNumber, PolicyNumber, and OfferNumber fields on the passed-in SuspensePayment
   * must be non-null.
   *
   * @param suspensePayment The SuspensePayment being made (a PaymentReceiptRecord of subtype SuspensePayment)
   * @return SuspensePaymentID The PublicID of the payment that is created
   */
  @Throws(DataConversionException, "If more than one of AccountNumber, PolicyNumber, and OfferNumber are all non-null")
  function makeSuspensePayment(suspensePayment: PaymentReceiptRecord,bundle: Bundle): String {
    WebservicePreconditions.notNull(suspensePayment, "suspensePayment")
    validateSuspensePayment(suspensePayment)
    var suspensePaymentEntity: SuspensePayment
      suspensePaymentEntity = PaymentReceipts.toEntity(suspensePayment, bundle) as SuspensePayment
      /**
       * US567 - Bank/Lockbox Integration
       * 08/19/2014 Alvin Lee
       *
       * Set the lockbox transaction number, the invoice number, the description, and the reference number on the SuspensePayment.
       */
      suspensePaymentEntity.InvoiceNumber = suspensePayment.InvoiceNumber_TDIC
      suspensePaymentEntity.LockboxTransactionNumber_TDIC = suspensePayment.LockboxTransactionNumber_TDIC
      suspensePaymentEntity.Description = "Suspense Payment from Lockbox Transaction " + suspensePayment.LockboxTransactionNumber_TDIC
      suspensePaymentEntity.RefNumber = suspensePayment.RefNumber
      suspensePaymentEntity.createSuspensePayment()
    return suspensePaymentEntity.PublicID;
  }

}