package tdic.bc.config.payment

uses com.google.common.base.Preconditions
uses com.google.common.collect.Multimap
uses com.google.common.collect.Ordering
uses com.google.common.collect.TreeMultimap
uses gw.api.web.payment.AllocationPool
uses gw.api.web.payment.ReturnPremiumAllocationStrategy
uses gw.bc.payment.InvoiceItemAllocationOrdering
uses gw.payment.FirstToLastAllocationStrategy
uses org.slf4j.LoggerFactory

/**
 * Defines a return premium allocation strategy that distributes credits to {@link
 *      InvoiceItem}s proportionally among all targeted.
 */
class PolicyPeriodChangeReturnPremiumAllocationStrategy implements ReturnPremiumAllocationStrategy {

  private static final var _logger = LoggerFactory.getLogger("Application.Invoice")

  override property get TypeKey() : ReturnPremiumAllocateMethod {
    return TC_PPPROPORTIONAL_TDIC
  }

  /**
   * Determine if a positive item is eligible to be pad from the specified negative item.
   *
   * @return true if the positive item is eligible to be paid from the negative item, false else.
   */
  protected function
  isPositiveItemEligibleToBePaidFrom(negativeItem : InvoiceItem) : block(item : BaseDistItem) : boolean {
    return \ item -> item.InvoiceItem.EventDate.afterOrEqual(negativeItem.EventDate)
        and item.InvoiceItem.PolicyPeriod == negativeItem.PolicyPeriod
  }

  override function allocate(distItems : List<BaseDistItem>, amountToAllocate : AllocationPool) {

    // Validate that the required negative credit and positive invoice items exist.
    if (distItems.Empty) {
      return
    }

    var targetDist = distItems[0].BaseDist
    Preconditions.checkArgument(distItems.where(\elt -> elt.BaseDist != targetDist).size() == 0,
        "All dist items must belong to the same base dist.")

    var negativeDistItems = distItems.where(\distItem -> distItem.InvoiceItem.Amount.IsNegative)
    if (negativeDistItems.Empty) {
      return
    }

    var positiveDistItems = distItems.where(\distItem -> distItem.InvoiceItem.Amount.IsPositive)
    if (positiveDistItems.Empty) {
      // for fully paid policyperiod no item available for allocation hence directly disburse the funds before return
      var policyPeriod = negativeDistItems[0].PolicyPeriod

      _logger.info("PolicyPeriodChangeReturnPremiumAllocationStrategy ::Credit Allocation target " + policyPeriod)
      _logger.info("PolicyPeriodChangeReturnPremiumAllocationStrategy ::Amount To Disburse " + amountToAllocate.GrossAmount)

      gw.account.CreateDisbursementWizardHelper.createAndProcessDisbursement_TDIC(policyPeriod, amountToAllocate.GrossAmount)
      return
    }

    // Get the policy period of the first credit item
    var policyPeriod = negativeDistItems[0].PolicyPeriod
    _logger.info("PolicyPeriodChangeReturnPremiumAllocationStrategy ::Credit Allocation target " + policyPeriod)

    // Get all the positive invoice items that have the same policy period as the credit item.
    var policyPeriodDistItems = positiveDistItems.where(\elt -> elt.PolicyPeriod == policyPeriod)

    // Allocate credit to the preferred invoice items based on a Proportional allocation method
    var order : Ordering<entity.InvoiceItem>  =  InvoiceItemAllocationOrdering.Util.getInvoiceItemOrderingsFromTypes({TC_EVENTDATE })

    // Partition negative items and order the partitions using NegativeItemComparator
    var negativeItemsPartition = TreeMultimap.create(order, Ordering.natural()) as Multimap<InvoiceItem, BaseDistItem>
    negativeDistItems.each(\elt -> negativeItemsPartition.put(elt.InvoiceItem, elt))

    var currency = distItems[0].Currency
    var amountAvailableForPolicyPeriod = negativeDistItems.sum(currency, \elt -> elt.GrossAmountOwed - elt.GrossAmountToApply).negate()
    _logger.trace("PolicyPeriodChangeReturnPremiumAllocationStrategy ::Amount Available for Allocation " + amountAvailableForPolicyPeriod)

    for (negativeItem in negativeItemsPartition.keySet()) {
      // Filter the positive items to get those eligible to be paid by this negative item
      var eligiblePositiveItems = policyPeriodDistItems.where(isPositiveItemEligibleToBePaidFrom(negativeItem))
      if (eligiblePositiveItems.Empty) {
        continue
      }
      var negativeItemCollection = negativeItemsPartition.get(negativeItem)
      var amountAvailable = negativeItemCollection.sum(currency, \elt -> elt.GrossAmountOwed - elt.GrossAmountToApply).negate()
      new FirstToLastAllocationStrategy().allocate(eligiblePositiveItems, AllocationPool.withGross(amountAvailable))
    }
    _logger.debug("PolicyPeriodChangeReturnPremiumAllocationStrategy :: proportional allocation done.")

    var excessTreatment = typekey.ReturnPremiumExcessTreatment.TC_UNAPPLIED // default setting
    excessTreatment = policyPeriod.ReturnPremiumPlan.ReturnPremiumHandlingSchemes.firstWhere(\elt -> elt.HandlingCondition ==
        ReturnPremiumHandlingCondition.TC_OTHER).ExcessTreatment

    // If there is remaining credit then it should be disbursed...
    var amountToBeDisbursed = amountToAllocate.GrossAmount - amountAvailableForPolicyPeriod
    if (amountToBeDisbursed.IsNotZero &&
        amountToBeDisbursed.IsPositive &&
        excessTreatment == ReturnPremiumExcessTreatment.TC_DISBURSEMENT_TDIC) {

      // Perform the disbursement
      _logger.info("PolicyPeriodChangeReturnPremiumAllocationStrategy :: Amount to disburse " + amountToBeDisbursed)

      gw.account.CreateDisbursementWizardHelper.createAndProcessDisbursement_TDIC(policyPeriod, amountToAllocate.GrossAmount)
    }
    _logger.debug("PolicyPeriodChangeReturnPremiumAllocationStrategy :: allocate - Exiting")
  }

}