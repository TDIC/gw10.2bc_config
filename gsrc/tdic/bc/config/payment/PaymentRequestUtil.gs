package tdic.bc.config.payment

uses gw.api.database.Query
uses gw.api.util.DisplayableException
uses java.util.Date
uses gw.api.util.DateUtil
uses gw.api.locale.DisplayKey

/**
 * US1158 - APW Payment Reversals
 * 3/11/2015 Alvin Lee
 *
 * Helper class when creating payment requests.
 */
class PaymentRequestUtil {

  /**
   * Validate that an invoice with a positive amount due was selected.
   */
  @Param("request", "The Payment Request for which the invoice must be validated")
  @Returns("A error message string if there is a validation error; null otherwise")
  public static function validateInvoice(request : PaymentRequest) : String {
    if (request.Account.PaymentRequests.hasMatch( \ payRequest -> payRequest.Invoice == request.Invoice
        && payRequest.Status != PaymentRequestStatus.TC_CANCELED && payRequest.Status != PaymentRequestStatus.TC_CLOSED)) {
      return DisplayKey.get("TDIC.Web.PaymentRequestDV.Error.PaymentRequestAlreadyExists")
    }
    if (request.Amount == null || !request.Amount.IsPositive) {
      return DisplayKey.get("TDIC.Web.PaymentRequestDV.Error.InvoiceAmountNotPositive")
    }
    return null
  }

  /**
   * Finds an Invoice by the Invoice Number.
   */
  @Param("invoiceNumber", "A String containing the Invoice Number")
  @Returns("The matching AccountInvoice entity")
  public static function findInvoiceFromNumber(invoiceNumber: String) : AccountInvoice {
    if (invoiceNumber == null) {
      return null
    }
    var invoiceQuery = Query.make(AccountInvoice)
    invoiceQuery.compare(Invoice#InvoiceNumber, Equals, invoiceNumber)
    var results = invoiceQuery.select()
    if (results.Count == 0) {
      throw new DisplayableException(DisplayKey.get("TDIC.Web.PaymentRequestDV.Error.InvoiceNotFound", invoiceNumber))
    }
    var foundInvoice = results.getAtMostOneRow()
    return foundInvoice
  }

  /**
   * Gets the next available APW draft date based on the 2 dates set from the script parameters.
   */
  @Param("startDate", "The starting date from which to calculate the next draft date. This is usually the current date (from the UI) unless more notice is needed to the payer.")
  @Param("account", "The Account entity for which the payment request is being generated")
  @Returns("The Date for the next available APW draft date")
  public static function getNextAvailableDraftDate(startDate : Date, account : Account) : Date {
    // Add a day to the draft interval since when a payment request is manually created from the UI, it will not get
    // picked up by the PaymentRequest batch job until the next day
    var draftInterval = account.BillingPlan.DraftInterval.DayCount
    var draftLogic = account.BillingPlan.DraftDayLogic
    var firstPossibleDate = startDate.nextDayOfMonth(ScriptParameters.APWInvoiceDayOfMonth1).addDays(-draftInterval)
    firstPossibleDate = nudgeToDraftDayLogic(firstPossibleDate, draftLogic)
    var secondPossibleDate = startDate.nextDayOfMonth(ScriptParameters.APWInvoiceDayOfMonth2).addDays(-draftInterval)
    secondPossibleDate = nudgeToDraftDayLogic(secondPossibleDate, draftLogic)

    // If nudging the dates pushes one into the past (or the same day, since it will not get picked up by the
    // PaymentRequest batch job until the next day), then use the other date since it will obviously be sooner than
    // adding another month.
    if (DateUtil.compareIgnoreTime(startDate, firstPossibleDate) >= 0) {
      return secondPossibleDate
    }
    else if (DateUtil.compareIgnoreTime(startDate, secondPossibleDate) >= 0) {
      return firstPossibleDate
    }
    // Otherwise, if both dates are in the future, return the earliest of the 2 dates
    else if (firstPossibleDate.before(secondPossibleDate)) {
      return firstPossibleDate
    }
    else {
      return secondPossibleDate
    }
  }

  /**
   * Nudges the date to a previous business day or a next business day based on the draft day logic
   */
  @Param("dateToNudge", "The date that needs to be nudged to the next business day or previous business day")
  @Param("draftLogic", "The DayOfMonthLogic indicating if it should go to the next or previous business day")
  @Returns("The new nudged date")
  public static function nudgeToDraftDayLogic(dateToNudge : Date, draftLogic : DayOfMonthLogic) : Date {
    if (!DateUtil.isBusinessDay(dateToNudge, HolidayTagCode.TC_GENERAL)) {
      if (draftLogic == DayOfMonthLogic.TC_PREVBUSINESSDAY) {
        dateToNudge = dateToNudge.addBusinessDays(-1)
      }
      else if (draftLogic == DayOfMonthLogic.TC_NEXTBUSINESSDAY) {
        dateToNudge = dateToNudge.addBusinessDays(1)
      }
    }
    return dateToNudge
  }

}