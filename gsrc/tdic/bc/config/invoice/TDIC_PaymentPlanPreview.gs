package tdic.bc.config.invoice

//20180529 TJT GW-3116: Generating Payment Schedule Previews that appear on printed invoices
// Family of files:
//  TDIC_InvoiceItemPreview.gs
//  TDIC_PaymentPlanPreview.gs  (this file)
//  TDIC_InvoiceEnhancement.gsx
//  TDIC_BCInvoiceModel.gx

class TDIC_PaymentPlanPreview {
    private var _paymentPlanName : String as PaymentPlanName
    private var _currentlySelected : boolean as CurrentlySelected
    private var _tdic_InvoiceItemPreviews : TDIC_InvoiceItemPreview[] as TDIC_InvoiceItemPreviews

    construct(){}

    construct(passedInPaymentPlanName : String, passedinCurrentlySelected : boolean, passedInTDIC_InvoiceItemPreviews : TDIC_InvoiceItemPreview[]) {
      _paymentPlanName = passedInPaymentPlanName
      _currentlySelected = passedinCurrentlySelected
      _tdic_InvoiceItemPreviews = passedInTDIC_InvoiceItemPreviews
    }

    // after the collection of Invoices is loaded onto the instance of TDIC_PaymentPlanPreview, the Invoices need to be numbered sequentially
    // the OOTB product doesn't number the installments until the bundle is committed, and we aren't committing a bundle to obtain this preview
    function setInstallmentNumbers(){
      var tmpInstallmentNumber : int = 1
      if (TDIC_InvoiceItemPreviews.Count > 0){
        for (each in TDIC_InvoiceItemPreviews.orderBy(\ elt -> elt.DueDate)){
          each.InstallmentNumber = tmpInstallmentNumber
          tmpInstallmentNumber += 1
        }
      }
    }
}