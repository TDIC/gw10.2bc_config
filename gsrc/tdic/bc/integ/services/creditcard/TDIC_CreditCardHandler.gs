package tdic.bc.integ.services.creditcard

uses net.authorize.ResponseField
uses org.slf4j.Logger
uses java.math.BigDecimal
uses java.util.Date
uses java.util.Map
uses org.slf4j.LoggerFactory

/**
 * US679, US680, DE67
 * 01/08/2015 Rob Kelly
 *
 * A Handler for making credit card payments.
 */
interface TDIC_CreditCardHandler {

  public static final var LOGGER : Logger = LoggerFactory.getLogger("CREDIT_CARD_HANDLER")
  public static final var INVOICE_PAYMENT_RESPONSE : String = "InvoicePayment"
 // public static final var FEE_PAYMENT_RESPONSE : String = "FeePayment"
 // public var convenienceFeeAmount : double = 30.00

  /**
   * Returns the time of day at which payments will be picked up for settlement.
   */
  @Returns("the time of day at which payments will be picked up for settlement as a Date")
  public function getPaymentCutOffTime() : Date

  /**
   * Returns the maximum number of characters permitted in the description of a credit card payment.
   *
   * @see #//submitPayment(BigDecimal,PaymentInstrument,String)
   */
  @Returns("the the maximum length in characters of the description of a credit card payment as an integer")
  public function getMaximumPaymentDescriptionLength() : int

  /**
   * Starts the credit card payment process for the specified Account and PaymentInstrument.
   * If the specified PaymentInstrument does not have a PaymentMethod of type "Credit Card" then nothing is done.
   */
  @Param("anAccount", "the account that the credit card payment pays")
  @Param("aPaymentInstrument", "the credit card payment instrument that pays the account")
  public function startCreditCardProcess(anAccount : Account, aPaymentInstrument : PaymentInstrument)

  /**
   * Populates the credit card fields of the specified PaymentInstrument.
   * If the specified PaymentInstrument does not have a PaymentMethod of type "Credit Card" or does not have an associated customer profile id then nothing is done.
   */
  @Param("aPaymentInstrument", "a credit card payment instrument")
  public function populateCreditCardFields(aPaymentInstrument : PaymentInstrument)

  /**
   * Submits a payment for the specified amount on the specified credit card payment instrument.
   * If the specified PaymentInstrument does not have a PaymentMethod of type "Credit Card" then nothing is done.
   */
  @Param("anAmount", "the amount of the credit card payment")
  @Param("aPaymentInstrument", "the credit card payment instrument with which to make the payment")
  @Param("aDescription", "a description of the credit card payment")
  @Returns("an authorization code for the payment")
  //public function submitPayment(anAmount : BigDecimal, aPaymentInstrument : PaymentInstrument, aDescription : String) : Map<ResponseField, String>

  /**
   * Finishes the credit card payment process.
   */
  public function finishCreditCardProcess()

  /**
   * Submits a payment for the specified amount on the specified credit card payment instrument.
   * If the specified PaymentInstrument does not have a PaymentMethod of type "Credit Card" then nothing is done.
   */
  @Param("anAmount", "the amount of the credit card payment")
  @Param("aPaymentInstrument", "the credit card payment instrument with which to make the payment")
  @Param("aDescription", "a description of the credit card payment")
  @Returns("an authorization code for the invoice payment and fee payment")
  public function submitPaymentAndFee(anAmount : BigDecimal, aPaymentInstrument : PaymentInstrument, aDescription : String, moneyReceived: DirectBillMoneyRcvd) : Map<String,Map<ResponseField, String>>
}