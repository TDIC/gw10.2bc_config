package tdic.bc.integ.services.creditcard

uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.pl.exception.GWConfigurationException
uses gw.processes.BatchProcessBase
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory

uses java.sql.SQLException

/**
 * Created with IntelliJ IDEA.
 * User: SunnihithB
 * Date: 1/17/17
 * Time: 1:34 PM
 * To change this template use File | Settings | File Templates.
 */
class TDIC_AuthorizeNetOpenHandlerCleanupBatch extends BatchProcessBase {

  public static final var LOGGER : Logger = LoggerFactory.getLogger("CREDIT_CARD_HANDLER")

  /**
   * The logger tag for instances of this Batch.
   */
  private static final var LOG_TAG = "TDIC_AuthorizenetCleanupBatch#"

  /**
   * Count for open handler keys in GWINT database.
   */
  private var openHandlerKeys : List

  /**
   * The name of the Authorize.Net Login ID property in cached properties.
   */
  private static final var API_LOGIN_ID_PROPERTY = "auth.net.api.login"

  /**
   * The name of the Authorize.Net Transaction Key property in cached properties.
   */
  private static final var TRANSACTION_KEY_PROPERTY = "auth.net.trans.key"

  /**
   * The name of the Authorize.Net Environment property in cached properties.
   */
  private static final var ENVIRONMENT_PROPERTY = "auth.net.env"

  /**
   * Key for looking up the failure notification email addresses from the integration database (value: InfraIssueNotificationEmail).
   */
  public static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "BCInfraIssueNotificationEmail"

  /**
   * Failure notification email addresses in case of any errors in batch process.
   */
  private var _failureNotificationEmail : String = null

  /**
   * Notification email's subject for General Ledger batch job failures.
   */
  public static final var EMAIL_SUBJECT_FAILURE: String = "Authorize net open handler Cleanup Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId

  /**
   * The root name of the Authorize.Net Open Handler properties in cached properties.
   *
   * @see TDIC_AuthorizeNetCreditCardHandler#OPEN_HANDLER_PROPERTY_ROOT_SERVERID
   */
  private static final var OPEN_HANDLER_PROPERTY_ROOT = "auth.net.openhandler."

  /**
   * The root name of the Authorize.Net Open Handler properties in cached properties for Open Credit Card Handlers on this instance.
   * IMPORTANT: These cached properties should be updated by this class only.
   *
   * //@see startCreditCardProcess(Account,PaymentInstrument)
   * //@see finishCreditCardProcess()
   * @see TDIC_AuthorizeNetCreditCardHandler#load()
   */
  private static final var OPEN_HANDLER_PROPERTY_ROOT_SERVERID = OPEN_HANDLER_PROPERTY_ROOT + gw.api.system.server.ServerUtil.getServerId() + "."



  construct() {

    super(BatchProcessType.TC_AUTHORIZENETOPENHANDLERCLEANUPBATCH_TDIC)
  }

  /**
   * Returns true if there are any Credit Card Handler properties in Property table of GWINT database.
   */
  function checkInitialConditions_TDIC() : boolean {

    var logTag = LOG_TAG + "checkInitialConditions() - "
    _failureNotificationEmail = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)
    if (_failureNotificationEmail == null) {
      throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"+ FAILURE_NOTIFICATION_EMAIL_KEY + "' from integration database.")
    }
    openHandlerKeys = PropertyUtil.getInstance().getIntProperties().keySet().where( \ k -> (k as String).contains(OPEN_HANDLER_PROPERTY_ROOT))
    if(openHandlerKeys.size() > 0) {
      LOGGER.info(logTag + this.openHandlerKeys.size() + " Open Authorize.Net handler keys found")
      return true
    }
    return false
  }

  /**
   * Cleans up the Credit Card Handler properties in Property table of GWINT database.
   */
  override function doWork() {
    var logTag = LOG_TAG + "doWork() - "
    //GINTEG-1135 : Skip if there are no transactions that need to have flat files written.
    LOGGER.info(logTag + "TDIC_AuthorizeNetOpenHandlerCleanupBatch#doWork() - Check whether we have any openHandlerKeys")
    if (checkInitialConditions_TDIC()) {
      try {
        var apiLoginID = PropertyUtil.getInstance().getProperty(API_LOGIN_ID_PROPERTY)
        var transactionKey = PropertyUtil.getInstance().getProperty(TRANSACTION_KEY_PROPERTY)
        var environment = PropertyUtil.getInstance().getProperty(ENVIRONMENT_PROPERTY)
        var merchant = TDIC_AuthorizeNetCreditCardHandler.createMerchant(environment, apiLoginID, transactionKey)
        var openHandlerCustomerProfileID : String
        for (anOpenHandlerKey in openHandlerKeys) {
          openHandlerCustomerProfileID = PropertyUtil.getInstance().getProperty(anOpenHandlerKey as String)
          if (!(TDIC_AuthorizeNetCreditCardHandler.openHandlerExists(openHandlerCustomerProfileID))) {
            var accountNumber = TDIC_AuthorizeNetCreditCardHandler.extractAccountNumberFromPropertyName(anOpenHandlerKey as String)
            LOGGER.info(logTag + "Finishing Credit Card Handler key for account " + accountNumber)
            LOGGER.info(logTag + "Deleting customer profile " + openHandlerCustomerProfileID + " for open credit card handler on account " + accountNumber)
            TDIC_AuthorizeNetCreditCardHandler.deleteCustomerProfile(merchant, openHandlerCustomerProfileID)
            PropertyUtil.getInstance().getIntProperties().remove(anOpenHandlerKey as String)
          }
        }
      } catch (sql : SQLException) {
        LOGGER.error(logTag + "SQLException= " + sql)
        if (_failureNotificationEmail == null) {
          LOGGER.error(logTag + " Cannot retrieve notification email addresses with the key" + FAILURE_NOTIFICATION_EMAIL_KEY + "from integration database.")
        } else {
          EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE, "The Authorize.Net Open handler cleanup batch  has failed due to error:  " + sql.LocalizedMessage)
        }
        throw sql
      } catch (ex : Exception) {
        LOGGER.error(logTag + "Exception= " + ex)
        if (_failureNotificationEmail == null) {
          LOGGER.error(logTag + " Cannot retrieve notification email addresses with the key" + FAILURE_NOTIFICATION_EMAIL_KEY + "from integration database.")
        } else {
          EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE, "The Authorize.Net open handler cleanup batch  has failed due to error:" + ex.LocalizedMessage)
        }
        throw ex
      }
    }
  }
}