package tdic.bc.integ.services.creditcard

uses com.tdic.util.misc.EmailUtil
uses com.tdic.util.properties.PropertyUtil
uses gw.processes.BatchProcessBase
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory


/**
 * US680
 * 11/24/2014 Rob Kelly
 *
 * A batch for cleaning up timed-out Authorize.Net Credit Card Handlers.
 */
class TDIC_AuthorizeNetCreditCardHandlerCleanupBatch extends BatchProcessBase {

  public static final var LOGGER : Logger = LoggerFactory.getLogger("CREDIT_CARD_HANDLER")

  /**
   * The logger tag for instances of this Batch.
   */
  private static final var LOG_TAG = "TDIC_AuthorizeNetCreditCardHandlerCleanupBatch#"

  /**
   * The timed-out Authorize.Net Credit Card Handlers to clean up for this batch.
   */
  private var timedOutHandlers : List<TDIC_AuthorizeNetCreditCardHandler>

  /**
   * Key for looking up the failure notification email addresses from the integration database (value: InfraIssueNotificationEmail).
   */
  public static final var FAILURE_NOTIFICATION_EMAIL_KEY : String = "BCInfraIssueNotificationEmail"

  /**
   * Failure notification email addresses in case of any errors in batch process.
   */
  private var _failureNotificationEmail : String = null

  /**
   * Notification email's subject for General Ledger batch job failures.
   */
  public static final var EMAIL_SUBJECT_FAILURE: String = "Authorize Net credit Card Handler Cleanup Batch Job Failure on server " + gw.api.system.server.ServerUtil.ServerId


  construct() {

    super(BatchProcessType.TC_AUTHORIZENETCREDITCARDHANDLERCLEANUP_TDIC)
  }

  /**
   * Returns true if there are any timed-out Authorize.Net Credit Card Handlers on this instance; false otherwise.
   */
  function checkInitialConditions_TDIC() : boolean {

    var logTag = LOG_TAG + "checkInitialConditions() - "
    _failureNotificationEmail = PropertyUtil.getInstance().getProperty(FAILURE_NOTIFICATION_EMAIL_KEY)
    // TODO: UnComment if block after PC is up.
    /*if (_failureNotificationEmail == null) {
      throw new GWConfigurationException("Cannot retrieve notification email addresses with the key '"+ FAILURE_NOTIFICATION_EMAIL_KEY + "' from integration database.")
    }*/
    this.timedOutHandlers = TDIC_AuthorizeNetCreditCardHandler.getTimedOutHandlers()
    if (LOGGER.isDebugEnabled()) LOGGER.debug(logTag + this.timedOutHandlers.size() + " Timed Out Authorize.Net Credit Card Handlers found")
    if (this.timedOutHandlers.size() > 0) {
      return true
    }
    return false
  }

  /**
   * Cleans up the timed-out Authorize.Net Credit Card Handlers on this instance.
   */
  override function doWork() {
    var logTag = LOG_TAG + "doWork() - "
    // GINTEG-1135 : Skip if there are no transactions that need to have flat files written.
    LOGGER.debug(logTag + "TDIC_AuthorizeNetCreditCardHandlerCleanupBatch#doWork() - Check whether we have any data to process")
    if (checkInitialConditions_TDIC()) {
      try {
        for (aHandler in this.timedOutHandlers) {
          if (LOGGER.isDebugEnabled())
            LOGGER.debug(logTag + "Finishing Credit Card Handler for account " + aHandler.getAccountNumber())
          aHandler.finishCreditCardProcess()
        }
      } catch (e : Exception) {
        EmailUtil.sendEmail(_failureNotificationEmail, EMAIL_SUBJECT_FAILURE, "The Authorize.Net credit card handler cleanup batch  has failed due to an unexpected error." + e.LocalizedMessage)
      }
    }
  }
}