package tdic.bc.integ.services.check.dto

uses gw.xml.ws.annotation.WsiExportable
uses tdic.bc.integ.services.util.dto.TDIC_ErrorMessageDTO

@WsiExportable
final class TDIC_CheckStatusResponseDTO {
  var _checkStatusDTO: TDIC_CheckStatusDTO as CheckStatusDTO
  var _comments : String as Comments
  var _errors : List<TDIC_ErrorMessageDTO> as Errors

  override function toString() : String{
    var response = new StringBuffer("Record: ${CheckStatusDTO.toString()} | ${Comments} | Errors: ")
    for(error in Errors){
      response.append(error.toString()+"| ")
    }
    return response.toString()
  }
}