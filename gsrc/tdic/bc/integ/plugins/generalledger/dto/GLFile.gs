package tdic.bc.integ.plugins.generalledger.dto

uses java.util.ArrayList
uses java.util.List

/**
 * US570 - Lawson General Ledger Integration
 * 5/4/2015 Alvin Lee
 *
 * A Gosu class to contain the fields needed for the GX model when generating the flat file.
 */
class GLFile {

  /**
   * The List of GL buckets to write to the flat file.
   */
  private var _glBuckets : List<GLBucket> as GLBuckets

  /**
   * Adds to the list of GL buckets.
   */
  @Param("bean", "The GLBucket to add to the list of GL buckets")
  public function addToGLBuckets(bean : GLBucket) {
    if (_glBuckets == null) {
      _glBuckets = new ArrayList<GLBucket>()
    }
    _glBuckets.add(bean)
  }

}