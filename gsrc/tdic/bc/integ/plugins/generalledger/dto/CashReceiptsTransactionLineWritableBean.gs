package tdic.bc.integ.plugins.generalledger.dto


uses java.math.BigDecimal

/**
 * US570 - General Ledger Integration
 * 11/13/2014 Alvin Lee
 *
 * Class holding values that will be written to the external table to be forwarded to the GL system
 */
class CashReceiptsTransactionLineWritableBean extends GLTransactionLineWritableBean {

  /**
   * Default constructor.
   */
  construct() {
    super(new CashReceiptsTransactionLineBeanInfo())
  }

}