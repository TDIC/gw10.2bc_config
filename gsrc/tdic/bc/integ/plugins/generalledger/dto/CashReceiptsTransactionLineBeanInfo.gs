package tdic.bc.integ.plugins.generalledger.dto

/**
 * US570 - General Ledger Integration
 * 11/13/2014 Alvin Lee
 *
 * Concrete implementation of Metadata class describing the GL transaction persistence options we want
 */
class CashReceiptsTransactionLineBeanInfo<T> extends GLTransactionLineBeanInfo<T> {
  
  /**
   * Abstract method implementation, provides the SQL name of the table for this object
   */
  override property get TableName() : String {
    return "CashReceiptsIntegration"
  }

}