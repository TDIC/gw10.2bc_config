package tdic.bc.integ.plugins.generalledger

uses com.tdic.util.database.DatabaseManager
uses com.tdic.util.properties.PropertyUtil
uses org.slf4j.LoggerFactory
uses tdic.bc.integ.plugins.generalledger.dto.CashReceiptsTransactionLineWritableBean
uses tdic.bc.integ.plugins.generalledger.dto.GLTransactionLineWritableBean
uses tdic.bc.integ.plugins.message.TDIC_MessagePlugin

uses java.sql.Connection
uses java.sql.SQLException

/**
 * US570 - General Ledger Integration
 * 11/13/2014 Alvin Lee
 *
 * Implementation of a Messaging Transport plugin to take event messages stored in a GW XML Model format, filter ones that
 * should not be sent, and write them to an external database table to be exported to an external GL system
 */
class TDIC_GLIntegrationMessageTransport extends TDIC_MessagePlugin {

  /**
   * Standard logger for General Ledger
   */
  private static final var _logger = LoggerFactory.getLogger("GENERAL_LEDGER")

  /**
   * Key for looking up the Integration DB URL from properties file
   */
  public static final var INTEGRATION_DB_URL_KEY : String = "IntDBURL"
  
  /**
   * Default constructor
   */
  construct() {
    _logger.debug("TDIC_GLIntegrationMessageTransport#construct - Creating GL Integration Transport")
  }

  /**
   * Interface method implementation resuming operations for this plugin. Connects to the database and creates a filter
   */
  override function resume() {
    _logger.info("TDIC_GLIntegrationMessageTransport#resume - Executing resume")
  }

  /**
   * Interface method implementation to shutdown operations for this plugin,  Disconnects from the database and disposes of the filter*/

  override function shutdown() {
    _logger.info("TDIC_GLIntegrationMessageTransport#shutdown - Executing shutdown")
  }

 /**
   * Interface method implementation to take in a message and send it on to the final destination (Integration DB in this instance)
   * 
   * @param msg - Message object
   * @param transformedPayload - payload value to send to the external system*/

  override function send(msg : Message, transformedPayload : String) {
    _logger.debug("TDIC_GLIntegrationMessageTransport#send - Payload: " + transformedPayload)

    // Check for duplicates
    var history = gw.api.database.Query.make(MessageHistory).compare(MessageHistory#SenderRefID, Equals, msg.SenderRefID).select().AtMostOneRow
    if (history != null) {
      _logger.warn("TDIC_GLIntegrationMessageTransport#send - Duplicate message found with SenderRefID: " + msg.SenderRefID)
      history = gw.transaction.Transaction.getCurrent().add(history)
      history.reportDuplicate()
      return
    }

    // Get GL transaction XML model
    var transactionModel = tdic.bc.integ.plugins.generalledger.gxmodel.gltransactionmodel.GLTransaction.parse(transformedPayload)

    var dbConnection : Connection = null
    try {
      // Get a connection from DatabaseManager using the GWINT DB URL
      var dbUrl = PropertyUtil.getInstance().getProperty(INTEGRATION_DB_URL_KEY)
      dbConnection = DatabaseManager.getConnection(dbUrl)

      // Get the Transaction Lines from the model
      var glTransactionLine : GLTransactionLineWritableBean
      if (msg.MessageRoot typeis DirectBillMoneyReceivedTxn || msg.MessageRoot typeis SuspPymtTransaction) {
        _logger.debug("TDIC_GLIntegrationMessageTransport#send - Sending transaction " + msg.MessageRoot + " to Cash Receipts table")
        glTransactionLine = new CashReceiptsTransactionLineWritableBean()
      }
      else {
        _logger.debug("TDIC_GLIntegrationMessageTransport#send - Sending transaction " + msg.MessageRoot + " to GL table")
        glTransactionLine = new GLTransactionLineWritableBean()
      }

      // Write lines to integration database
      var lines = transactionModel.LineItems.Entry.toList()
      for (line in lines) {
        glTransactionLine.loadFromModel(line)
        glTransactionLine.executeCreate(dbConnection)
      }

      // Commit DB Connection
      if (dbConnection != null) {
        dbConnection.commit()
      }
      else {
        _logger.error("TDIC_GLIntegrationMessageTransport#send - Unable to get DB connection using JDBC URL: " + dbUrl)
        throw new SQLException("Unable to get DB connection using JDBC URL: " + dbUrl)
      }

      //Acknowledge message
      msg.reportAck()
    } catch(sqle : SQLException) {
      // Handle database error; no need to call reportError here since we are re-throwing the exception
      _logger.error("TDIC_GLIntegrationMessageTransport#send - Database connectivity/execution error persisting entity", sqle)
      if (dbConnection != null) {
        // Attempt to rollback
        try {
          dbConnection.rollback()
        } catch (rbe: Exception) {
          _logger.error("TDIC_GLIntegrationMessageTransport#send - Exception while rolling back DB", rbe)
        }
      }
      throw sqle
    } catch (e : Exception) {
      // Handle other errors; no need to call reportError here since we are re-throwing the exception
      _logger.error("TDIC_GLIntegrationMessageTransport#send - Unexpected error persisting entity", e)
      if (dbConnection != null) {
        // Attempt to rollback
        try {
          dbConnection.rollback()
        } catch (rbe: Exception) {
          _logger.error("TDIC_GLIntegrationMessageTransport#send - Exception while rolling back DB", rbe)
        }
      }
      throw e
    } finally {
      // Close database connection
      if (dbConnection != null) {
        _logger.debug("TDIC_GLIntegrationMessageTransport#send - Closing DB connection")
        try {
          dbConnection.close()
        } catch (e : Exception) {
          _logger.error("TDIC_GLIntegrationMessageTransport#send - Exception on closing DB (continuing)", e)
        }
      }
    }
  }

  override property set DestinationID(destinationID : int) {

  }
}