package tdic.bc.integ.plugins.generalledger.helper

uses java.util.HashMap
uses gw.api.database.Query
uses gw.api.system.BCLoggerCategory
uses org.slf4j.LoggerFactory

/**
 * US570 - General Ledger Integration
 * 11/13/2014 Alvin Lee
 *
 * GL Integration Filter - Implementation making use of transaction filter entities that are saved to the database to
 * suppress information that would otherwise be sent to the integration database.
 * This is a selective deny mechanism, transaction types that are added via the UI will be suppressed
 */
class TDIC_GLIntegrationFilter {

  /**
   * Standard logger for the General Ledger Plugin.
   */
  private static final var _logger = LoggerFactory.getLogger("GENERAL_LEDGER")

  /**
   * Map of the General Ledger filters for the transactions that are to be suppressed.
   */
  private var filters = new HashMap<String, GLIntegrationTransactionFilter_TDIC>()
  
  /**
   * Create the object and populate the list of filters from the database
   */
  construct() {
    _logger.debug("TDIC_GLIntegrationFilter#construct - Loading GLIntegration transaction filters ...")
    var filterQuery = Query.make(GLIntegrationTransactionFilter_TDIC)
    var filterResults = filterQuery.select()
    for(filterResult in filterResults) {
      filters.put(filterResult.TransactionType.Code, filterResult)
    }
    _logger.debug("TDIC_GLIntegrationFilter#construct - Done loading GLIntegration transaction filters.")
  }

  /**
   * Given the string representation of a transaction type typekey, check our list of types to filter to see if it is present
   * 
   * @param trans a String containing the transaction to be tested
   * @returns boolean true if the transaction should be sent, false otherwise
   */
  function allow(trans:String):boolean {
    _logger.debug("TDIC_GLIntegrationFilter#allow - Checking if transaction ${trans} should be filtered")
    //This is a simple pass filter right now.  Enhance GLIntegrationFilter object and this class
    //if you need more complex filtering logic, but do not want to implement it at event fired rule time.
    if(filters.containsKey(trans)) {
      _logger.debug("TDIC_GLIntegrationFilter#allow - Filtering this subtype")
      return false
    }
    _logger.debug("TDIC_GLIntegrationFilter#allow - Subtype not filtered")
    return true
  }
}