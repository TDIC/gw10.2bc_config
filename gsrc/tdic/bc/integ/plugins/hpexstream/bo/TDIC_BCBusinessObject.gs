/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 * © Copyright 2009-2014 Guidewire Software, Inc.
 */
package tdic.bc.integ.plugins.hpexstream.bo
uses com.tdic.plugins.hpexstream.core.bo.TDIC_BaseBusinessObject
uses java.util.Date
uses java.util.stream.Collectors

uses gw.api.util.DateUtil

class TDIC_BCBusinessObject extends TDIC_BaseBusinessObject {

  construct() {

  }

  property get Account() : Account{
    return RootEntities[0] as Account
  }

  property get Invoices() : List<Invoice> {
     var editables = RootEntities.toList().stream().filter(\elt -> elt typeis Invoice).collect(Collectors.toList())
    return  editables as List<Invoice>
  }

  property get PolicyPeriod() : PolicyPeriod {
    return RootEntities.toList().stream().filter(\elt -> elt typeis PolicyPeriod).findAny().orElse(null) as PolicyPeriod
  }

  property get DelinquencyProcess() : PolicyDlnqProcess {
    return RootEntities.toList().stream().filter(\elt -> elt typeis PolicyDlnqProcess).findAny().orElse(null) as PolicyDlnqProcess
  }

  property get MailingDate() : Date {
    // The document mailing date is the next business day.
    var mailingDate = DateUtil.addBusinessDays(Date.Today, 1, HolidayTagCode.TC_CORPORATE_TDIC);
    return mailingDate;
  }

  /*property get Currently_Billed_InstallmentPreview_TDIC() : tdic.bc.config.invoice.TDIC_InvoiceItemPreview[] {
    return Account.getLatestBilledInvoice_TDIC(this.DocumentRequestSource.PolicyPeriodSource)?.InstallmentPreview_TDIC
        ?.orderBy(\ row -> row.PaymentPlanName)?.thenBy(\ row -> row.InstallmentNumber)?.toTypedArray()
  }*/

  property get PaymentMoneyReceived_TDIC() : PaymentMoneyReceived {
    return RootEntities.toList().stream().filter(\elt -> elt typeis PaymentMoneyReceived).findAny().orElse(null) as PaymentMoneyReceived
  }
}
