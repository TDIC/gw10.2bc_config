package tdic.bc.integ.plugins.account

/**
 * US167 - Define Account Evaluation Calculation
 * 10/29/2014 Alvin Lee
 *
 * Implementation of the IAccountEvaluationCalculator plugin for customization of the number of days.
 */
class TDIC_AccountEvaluationCalculator extends gw.plugin.account.impl.AccountEvaluationCalculator {

  /**
   * Time period in days over which system should run queries for various pieces of information we will use in
   * determining the account evaluation rating (for example: how many delinquencies on the account in the last 365 days)
   */
  private static var NumberOfDaysForMetricsSearch = 365

  /**
   * Overriding the default number of days (365) to the custom number of days (1825 days / 5 years).
   */
  override property get MetricsQueryTimePeriod() : int
  {
    return NumberOfDaysForMetricsSearch
  }

}