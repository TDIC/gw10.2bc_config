package tdic.bc.integ.plugins.bankeft.dto.BankPrenote

uses com.tdic.util.database.WritableBeanBaseImpl

/**
   * US568 - Bank/EFT Integration
   * Class holding values that will be written to the external table to be forwarded to the Bank/EFT system
   */
  class TDIC_PaymentInstrumentWritableBean extends WritableBeanBaseImpl {


    /**
     * The name of the bank on the payment instrument associated with the payment instrument.
     */
    private var _bankName : String as BankName

    /**
     * The routing number of the bank on the payment instrument associated with the payment instrument.
     */
    private var _bankRoutingNumber : String as BankRoutingNumber

    /**
     * The account number of the bank on the payment instrument associated with the payment instrument.
     */
    private var _bankAccountNumber : String as BankAccountNumber

    /**
     * The name of the primary payer of the account associated with the payment instrument.  This will either be the Name
     * field of the company or the LastName field of the person.
     */
    private var _contactName : String as ContactName


    /**
     * Default constructor.
     */
    construct() {
      super(new TDIC_PaymentInstrumentBeanInfo())
    }
  }