package tdic.bc.integ.plugins.bankeft.dto.BankEFT

uses com.tdic.util.database.WritableBeanBaseImpl
uses tdic.bc.integ.plugins.bankeft.dto.BankEFT.PaymentRequestBeanInfo

uses java.math.BigDecimal

/**
 * US568 - Bank/EFT Integration
 * 09/24/2014 Alvin Lee
 *
 * Class holding values that will be written to the external table to be forwarded to the Bank/EFT system
 */
class PaymentRequestWritableBean extends WritableBeanBaseImpl {

  /**
   * The account number associated with the payment request.
   */
  private var _accountNumber: String as AccountNumber

  /**
   * The invoice number associated with the payment request.
   */
  private var _invoiceNumber: String as InvoiceNumber

  /**
   * The amount of the payment request.
   */
  private var _amount : BigDecimal as Amount

  /**
   * The draft date the payment request.
   */
  private var _draftDate : String as DraftDate

  /**
   * The due date the payment request.
   */
  private var _dueDate : String as DueDate

  /**
   * The name of the primary payer of the account associated with the payment request.  This will either be the Name
   * field of the company or the LastName field of the person.
   */
  private var _contactName : String as ContactName

  /**
   * The name of the bank on the payment instrument associated with the payment request.
   */
  private var _bankName : String as BankName

  /**
   * The routing number of the bank on the payment instrument associated with the payment request.
   */
  private var _bankRoutingNumber : String as BankRoutingNumber

  /**
   * The account number of the bank on the payment instrument associated with the payment request.
   */
  private var _bankAccountNumber : String as BankAccountNumber

 /**
   * Default constructor.
   */
  construct() {
    super(new PaymentRequestBeanInfo())
  }
}