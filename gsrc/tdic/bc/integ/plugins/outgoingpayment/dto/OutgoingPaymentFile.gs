package tdic.bc.integ.plugins.outgoingpayment.dto

uses java.util.ArrayList
uses gw.api.util.DateUtil
uses java.text.SimpleDateFormat
uses java.util.List

/**
 * A Gosu class to contain the fields needed for the GX model when generating the flat files.
 */
class OutgoingPaymentFile {

  /**
   * The List of outgoing payments to write to the flat files.
   */
  private var _outgoingPayments : List<OutgoingPaymentWritableBean> as OutgoingPayments

  /**
   * Counter for each outgoing payment line; must be unique for each line in the outgoing payment files
   */
  private var _seqNum = 1

  /**
   * Adds to the list of outgoing payments.
   */
  @Param("bean", "The OutgoingPaymentWritableBean to add to the list of outgoing payments")
  public function addToOutgoingPayments(bean : OutgoingPaymentWritableBean) {
    if (_outgoingPayments == null) {
      _outgoingPayments = new ArrayList<OutgoingPaymentWritableBean>()
    }
    bean.SequenceNumber = _seqNum
    _seqNum++
    _outgoingPayments.add(bean)
  }

  /**
   * Formats the current date in its full format for the flat files.
   */
  @Returns("A String containing the formatted current date in its full format for the flat files")
  public static function formatCurrentDateFull() : String {
    var currentDateTime = DateUtil.currentDate()
    var dateFormat = new SimpleDateFormat("MM/dd/yyyy")
    return dateFormat.format(currentDateTime)
  }

  /**
   * Formats the current date without any separators for the flat files.
   */
  @Returns("A String containing the formatted current date without any separators for the flat files")
  public static function formatCurrentDateNoSeparators() : String {
    var currentDateTime = DateUtil.currentDate()
    var dateFormat = new SimpleDateFormat("MMddyyyy")
    return dateFormat.format(currentDateTime)
  }

}