package tdic.util.gunit.test

uses gw.api.system.server.Runlevel
uses gw.testharness.RunLevel
uses gw.testharness.ServerTest
uses gw.testharness.TestBase
uses tdic.util.gunit.GUnitAssert
uses tdic.util.gunit.test.stubs.BeforeAndAfterMethodsTest
uses tdic.util.gunit.TextTestRunner
uses java.lang.Throwable
uses java.util.Arrays
uses java.util.List

@ServerTest
@RunLevel(Runlevel.NONE)
class BeforeAndAfterMethodsTestTest extends TestBase {

  override function beforeMethod() {
    BeforeAndAfterMethodsTest.clearStaticState()
  }

  override function afterMethod(possibleException : Throwable) {
    BeforeAndAfterMethodsTest.clearStaticState()
  }

  function testMethodsCalledInExpectedOrder() {
    var runner = new TextTestRunner()

    runner.runTestsInClass(BeforeAndAfterMethodsTest)

    var expectedMethodCalls = Arrays.asList({
        "beforeClass",
        "beforeMethod",
        "testOne",
        "afterMethod",
        //
        "beforeMethod",
        "testTwo",
        "afterMethod",
        //
        "beforeMethod",
        "testThree",
        "afterMethod",
        "afterClass"
      })
    GUnitAssert.assertCollectionEquals(expectedMethodCalls, BeforeAndAfterMethodsTest._methodNamesCalled)
  }

  function testInstances() {
    var runner = new TextTestRunner()

    runner.runTestsInClass(BeforeAndAfterMethodsTest)

    var expectedMethodCalls = Arrays.asList({
        "testOne",
        "testTwo",
        "testThree"
      })
    GUnitAssert.assertCollectionEquals(expectedMethodCalls, BeforeAndAfterMethodsTest._testNameToInstanceMap.keySet())
    expectedMethodCalls = Arrays.asList({
        "beforeClass",
        "beforeMethod",
        "testOne",
        "afterMethod"
      })
    GUnitAssert.assertCollectionEquals(expectedMethodCalls, getCallsForMethod("testOne"))
    expectedMethodCalls = Arrays.asList({
        "beforeMethod",
        "testTwo",
        "afterMethod"
      })
    GUnitAssert.assertCollectionEquals(expectedMethodCalls, getCallsForMethod("testTwo"))
    expectedMethodCalls = Arrays.asList({
        "beforeMethod",
        "testThree",
        "afterMethod",
        "afterClass"
      })
    GUnitAssert.assertCollectionEquals(expectedMethodCalls, getCallsForMethod("testThree"))
  }

  private static function getCallsForMethod(methodName : String) : List<String> {
    assertTrue(BeforeAndAfterMethodsTest._testNameToInstanceMap.containsKey(methodName))
    var testInstance = BeforeAndAfterMethodsTest._testNameToInstanceMap[methodName]
    return BeforeAndAfterMethodsTest._instanceToMethodNamesCalledMap[testInstance]
  }

}
