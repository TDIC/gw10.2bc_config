package tdic.util.gunit.test.stubs

uses gw.testharness.ServerTest
uses gw.testharness.TestBase
uses gw.testharness.RunLevel
uses gw.api.system.server.Runlevel

@ServerTest
@RunLevel(Runlevel.NONE)
class DoesNotExtendTestBaseTest /* extends TestBase */ {

  function testNotRun() {
    TestBase.fail("NO tests in this class should run because this class does NOT extend from the TestBase class.")
  }

}
