package tdic.util.gunit.test.stubs

uses gw.testharness.ServerTest
uses gw.testharness.RunLevel
uses gw.testharness.TestBase
uses gw.api.system.server.Runlevel
uses java.lang.Throwable
uses java.util.ArrayList
uses java.util.HashMap
uses java.util.LinkedHashMap
uses java.util.Map
uses java.util.List

@ServerTest
@RunLevel(Runlevel.NONE)
class BeforeAndAfterMethodsTest extends TestBase {

  public static var _methodNamesCalled              : List<String>                                 = new ArrayList<String>()
  public static var _instanceToMethodNamesCalledMap : Map<BeforeAndAfterMethodsTest, List<String>> = new HashMap<BeforeAndAfterMethodsTest, List<String>>()
  public static var _testNameToInstanceMap          : Map<String, BeforeAndAfterMethodsTest>       = new LinkedHashMap<String, BeforeAndAfterMethodsTest>()

  static function clearStaticState() {
    _methodNamesCalled.clear()
    _instanceToMethodNamesCalledMap.clear()
    _testNameToInstanceMap.clear()
  }

  construct() {
    assertFalse(_instanceToMethodNamesCalledMap.containsKey(this))
    _instanceToMethodNamesCalledMap.put(this, new ArrayList<String>())
  }

  override function beforeClass() {
    logNonTestMethodCall("beforeClass")
  }

  override function beforeMethod() {
    logNonTestMethodCall("beforeMethod")
  }

  function testOne() {
    logTestMethodCall("testOne")
  }

  function testTwo() {
    logTestMethodCall("testTwo")
  }

  function testThree() {
    logTestMethodCall("testThree")
  }

  override function afterMethod(possibleException : Throwable) {
    logNonTestMethodCall("afterMethod")
  }

  override function afterClass() {
    logNonTestMethodCall("afterClass")
  }

  private function logTestMethodCall(methodName : String) {
    logNonTestMethodCall(methodName)
    _testNameToInstanceMap.put(methodName, this)
  }

  private function logNonTestMethodCall(methodName : String) {
    _methodNamesCalled.add(methodName)
    _instanceToMethodNamesCalledMap[this].add(methodName)
  }

}
