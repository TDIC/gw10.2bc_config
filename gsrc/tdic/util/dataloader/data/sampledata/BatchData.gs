package tdic.util.dataloader.data.sampledata

uses tdic.util.dataloader.data.ApplicationData
uses java.util.Date

class BatchData extends ApplicationData {

  construct() {

  }
  private var  _batchRunDate           :  Date                      as  BatchRunDate
  private var  _batchName              :  String                    as  BatchName
}
