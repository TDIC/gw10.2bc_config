package tdic.util.dataloader.data.sampledata

uses java.lang.Integer
uses java.math.BigDecimal

class NewRenewalData extends NewPolicyData {

  construct() {

  }
  private var _newRenewalPolicyPeriod     : PolicyPeriodData      as NewRenewalPolicyPeriod
  //Payment Plan Modifiers
  private var _matchplannedinstallments   : boolean               as MatchPlannedInstallments
  private var _suppressdownpayment        : boolean               as SuppressDownPayment
  private var _maxnuminstallments         : Integer               as MaxNumberOfInstallmentsOverride
  private var _downpaymentoverride        : BigDecimal            as DownPaymentOverridePercentage
}
