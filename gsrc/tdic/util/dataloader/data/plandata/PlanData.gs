package tdic.util.dataloader.data.plandata

uses tdic.util.dataloader.data.ApplicationData
uses java.util.Date

class PlanData extends ApplicationData {

  construct() {

  }
  var _Name                                  : String               as Name
  var _Description                           : String               as Description
  var _EffectiveDate                         : Date                 as EffectiveDate
  var _ExpirationDate                        : Date                 as ExpirationDate

}