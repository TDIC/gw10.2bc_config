package tdic.util.dataloader.data.plandata

uses tdic.util.dataloader.data.ColumnDescriptor

/**
 * US570 - General Ledger Integration
 * 11/14/2014 Alvin Lee
 *
 * Excel Data Loader Additions for custom GL Integration entities - An object to store the data for the
 * GLIntegrationTransactionFilter_TDIC object.
 */
class GLTransactionFilterData extends PlanData {

  /**
   * Constant for String defining what Excel sheet to use.
   */
  public static final var SHEET : String = "GLTransactionFilter"

  /**
   * Constant for String defining what Excel sheet to use.
   */
  public static final var COL_TRANS_TYPE : int = 0

  /**
   * Mapping of the columns to the fields.
   */
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_TRANS_TYPE, "Transaction Type", ColumnDescriptor.TYPE_STRING)
  }

  /**
   * Standard constructor.
   */
  construct() {
  }
  
  /**
   * The Transaction Type for the GL Filter
   */
  var _transactionType : typekey.Transaction as TransactionType

}
