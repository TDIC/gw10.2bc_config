package tdic.util.dataloader.data.plandata

uses java.lang.Integer
uses java.math.BigDecimal
uses tdic.util.dataloader.data.ColumnDescriptor

class PaymentPlanData extends PlanData {

  // constants
  public static final var SHEET : String = "PaymentPlan"
  public static final var COL_PUBLIC_ID : int = 0
  public static final var COL_NAME : int = 1
  public static final var COL_DESCRIPTION : int = 2
  public static final var COL_EFFECTIVE_DATE : int = 3
  public static final var COL_EXPIRATION_DATE : int = 4
  public static final var COL_REPORTING : int = 5
  public static final var COL_DOWNPAYMENT_PERCENT : int = 6
  public static final var COL_MAXIMUM_NUMBER_OF_INSTALLMENTS : int = 7
  public static final var COL_PERIODICITY : int = 8
  public static final var COL_INVOICE_ITEM_PLACEMENT_CUTOFF_TYPE : int = 9
  public static final var COL_INVOICING_BLACKOUT_TYPE : int = 10
  public static final var COL_DAYS_BEFORE_POLICY_EXPIRATION_FOR_INVOICING_BLACKOUT : int = 11
  public static final var COL_INSTALLMENT_FEE : int = 12
  public static final var COL_SKIP_FEE_FOR_DOWNPAYMENT : int = 13
  public static final var COL_DAYS_FROM_REFERENCE_DATE_TO_DOWNPAYMENT : int = 14
  public static final var COL_DOWNPAYMENT_AFTER : int = 15
  public static final var COL_DAYS_FROM_REFERENCE_DATE_TO_FIRST_INSTALLMENT : int = 16
  public static final var COL_FIRST_INSTALLMENT_AFTER : int = 17
  public static final var COL_DAYS_FROM_REFERENCE_DATE_TO_SECOND_INSTALLMENT : int = 18
  public static final var COL_SECOND_INSTALLMENT_AFTER : int = 19
  public static final var COL_DAYS_FROM_REFERENCE_DATE_TO_ONETIME_CHARGE : int = 20
  public static final var COL_ONETIME_CHARGE_AFTER : int = 21
  public static final var COL_EQUITY_BUFFER : int = 22
  public static final var COL_EQUITY_WARNINGS_ENABLED : int = 23
  public static final var COL_BILLDATE_OR_DUEDATE_BILLING : int = 24
  public static final var COL_ALLIGN_INSTALLMENTS_TO_INVOICES : int = 25
  public static final var COL_CURRENCY : int = 26
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_PUBLIC_ID, "Public ID", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_NAME, "Name", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_DESCRIPTION, "Description", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_EFFECTIVE_DATE, "Effective Date", ColumnDescriptor.TYPE_DATE),
    new ColumnDescriptor(COL_EXPIRATION_DATE, "Expiration Date", ColumnDescriptor.TYPE_DATE),
    new ColumnDescriptor(COL_REPORTING, "Is Reporting?", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_DOWNPAYMENT_PERCENT, "Down Payment (%)", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_MAXIMUM_NUMBER_OF_INSTALLMENTS, "Max # Payments (not including down payment)", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_PERIODICITY, "Payment Interval", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_INVOICE_ITEM_PLACEMENT_CUTOFF_TYPE, "Item Placement Cutoff Type", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_INVOICING_BLACKOUT_TYPE, "Make Last Invoice", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_DAYS_BEFORE_POLICY_EXPIRATION_FOR_INVOICING_BLACKOUT, "by # Days before Policy Expiration Date", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_INSTALLMENT_FEE, "Invoicing Fee Amount", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_SKIP_FEE_FOR_DOWNPAYMENT, "Skip Fee for Down Payments", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_DAYS_FROM_REFERENCE_DATE_TO_DOWNPAYMENT, "Down Payment Invoiced days", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_DOWNPAYMENT_AFTER, "Down Payment days after", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_DAYS_FROM_REFERENCE_DATE_TO_FIRST_INSTALLMENT, "First Installment Invoiced days", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_FIRST_INSTALLMENT_AFTER, "First Installment days after", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_DAYS_FROM_REFERENCE_DATE_TO_SECOND_INSTALLMENT, "Second Installment Invoiced days", ColumnDescriptor.TYPE_NUMBER),
      new ColumnDescriptor(COL_SECOND_INSTALLMENT_AFTER, "Second Installment days after", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_DAYS_FROM_REFERENCE_DATE_TO_ONETIME_CHARGE, "One-Time Charges Invoiced days", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_ONETIME_CHARGE_AFTER, "One-Time Charges days after", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_EQUITY_BUFFER, "Equity Buffer Days", ColumnDescriptor.TYPE_NUMBER),
      new ColumnDescriptor(COL_EQUITY_WARNINGS_ENABLED, "Equity Warning is Enabled?", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_BILLDATE_OR_DUEDATE_BILLING, "Bill Date or Due Date Billing?", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_ALLIGN_INSTALLMENTS_TO_INVOICES, "Do not want are new functionality for alignment; can have the old lumpy behavior with event date invoice item generation", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_CURRENCY, "Currency", ColumnDescriptor.TYPE_STRING)
  }

  construct() {

  }
  var _Reporting                                          : Boolean                         as Reporting
  var _DownPaymentPercent                                 : BigDecimal                      as DownPaymentPercent
  var _MaximumNumberOfInstallments                        : Integer                         as MaximumNumberOfInstallments
  var _Periodicity                                        : Periodicity                     as Periodicity
  var _InvoiceItemPlacementCutoffType                     : InvoiceItemPlacementCutoffType  as InvoiceItemPlacementCutoffType
  var _InvoicingBlackoutType                              : InvoicingBlackoutType           as InvoicingBlackoutType
  var _DaysBeforePolicyExpirationForInvoicingBlackout     : Integer                         as DaysBeforePolicyExpirationForInvoicingBlackout
  var _InstallmentFee                                     : BigDecimal                      as InstallmentFee
  var _SkipFeeForDownPayment                              : Boolean                         as SkipFeeForDownPayment
  var _DaysFromReferenceDateToDownPayment                 : Integer                         as DaysFromReferenceDateToDownPayment
  var _DownPaymentAfter                                   : PaymentScheduledAfter           as DownPaymentAfter
  var _DaysFromReferenceDateToFirstInstallment            : Integer                         as DaysFromReferenceDateToFirstInstallment
  var _FirstInstallmentAfter                              : PaymentScheduledAfter           as FirstInstallmentAfter
  var _DaysFromReferenceDateToSecondInstallment           : Integer                         as DaysFromReferenceDateToSecondInstallment
  var _SecondInstallmentAfter                             : PaymentScheduledAfter           as SecondInstallmentAfter
  var _DaysFromReferenceDateToOneTimeCharge               : Integer                         as DaysFromReferenceDateToOneTimeCharge
  var _OneTimeChargeAfter                                 : PaymentScheduledAfter           as OneTimeChargeAfter
  var _EquityBuffer                                       : Integer                         as EquityBuffer
  var _EquityWarningsEnabled                              : Boolean                         as EquityWarningsEnabled
  var _PolicyLevelBillingBillDateOrDueDateBilling         : BillDateOrDueDateBilling        as PolicyLevelBillingBillDateOrDueDateBilling
  var _AlignInstallmentsToInvoices                        : Boolean                         as AlignInstallmentsToInvoices
  var _Currency                                           : Currency                        as Currency


}
