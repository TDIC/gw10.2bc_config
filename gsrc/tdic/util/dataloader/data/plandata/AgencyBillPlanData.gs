package tdic.util.dataloader.data.plandata

uses java.lang.Integer
uses java.math.BigDecimal
uses tdic.util.dataloader.data.ColumnDescriptor

class AgencyBillPlanData extends PlanData {

  // constants
  public static final var SHEET : String = "AgencyBillPlan"
  public static final var COL_PUBLIC_ID : int = 0
  public static final var COL_NAME : int = 1
  public static final var COL_WorkflowType : int = 2
  public static final var COL_EFFECTIVE_DATE : int = 3
  public static final var COL_EXPIRATION_DATE : int = 4
  public static final var COL_CycleCloseDayOfMonthLogic : int = 5
  public static final var COL_CycleCloseDayOfMonth : int = 6
  public static final var COL_PaymentTermsInDays : int = 7
  public static final var COL_ExceptionForPastDueStatement : int = 8
  public static final var COL_StatementSentAfterCycleClose : int = 9
  public static final var COL_DaysAfterCycleCloseToSendStmnt : int = 10
  public static final var COL_SnapshotNonPastDueItems : int = 11
  public static final var COL_StatementsWithLowNetSuppressed : int = 12
  public static final var COL_NetThresholdForSuppressingStmt : int = 13
  public static final var COL_ReminderSentIfPromiseNotRcvd : int = 14
  public static final var COL_DaysUntilPromiseReminderSent : int = 15
  public static final var COL_PromiseDueInDays : int = 16
  public static final var COL_ExceptionIfPromiseNotReceived : int = 17
  public static final var COL_PromiseExceptionsSent : int = 18
  public static final var COL_AutoProcessWhenPaymentMatches : int = 19
  public static final var COL_PaymentExceptionsSent	 : int = 20
  public static final var COL_FirstDunningSentIfNotPaid : int = 21
  public static final var COL_DaysUntilFirstDunningSent : int = 22
  public static final var COL_SecondDunningSentIfNotPaid : int = 23
  public static final var COL_DaysUntilSecondDunningSent : int = 24
  public static final var COL_ProducerWriteoffThreshold : int = 25
  public static final var COL_LowCommissionCleared : int = 26
  public static final var COL_ClearCommissionThreshold : int = 27
  public static final var COL_LowGrossCleared	 : int = 28
  public static final var COL_ClearGrossThreshold	 : int = 29
  public static final var COL_ClearingLogicTarget	 : int = 30
  public static final var COL_CreateOffsetsOnBilledInvoices : int = 31
  public static final var COL_PmntSchdChngOffsetsOnBilled : int = 32
  public static final var COL_CURRENCY: int = 33
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_PUBLIC_ID, "Public ID", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_NAME, "Name", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_WorkflowType, "Workflow Type", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_EFFECTIVE_DATE, "Effective Date", ColumnDescriptor.TYPE_DATE),
    new ColumnDescriptor(COL_EXPIRATION_DATE, "Expiration date", ColumnDescriptor.TYPE_DATE),
    new ColumnDescriptor(COL_CycleCloseDayOfMonthLogic, "Cycle Close Logic", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_CycleCloseDayOfMonth, "Cycle Close Day", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_PaymentTermsInDays, "Payment Terms (days)", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_ExceptionForPastDueStatement, "Generate Exception if Past Due?", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_StatementSentAfterCycleClose, "Send statement?", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_DaysAfterCycleCloseToSendStmnt, "Statement sent (in x days after cycle close)", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_SnapshotNonPastDueItems, "Show previous non-past-due amounts?", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_StatementsWithLowNetSuppressed, "Suppress statement on low balance?", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_NetThresholdForSuppressingStmt, "Low balance threshold", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_ReminderSentIfPromiseNotRcvd, "Send reminder notice if promise not received?", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_DaysUntilPromiseReminderSent, "Promise Reminder Days", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_PromiseDueInDays, "Promise Due (in x days from previous cycle close)", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_ExceptionIfPromiseNotReceived, "Generate exception if no promise received?", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_PromiseExceptionsSent, "Statement of Exceptions after promise processed?", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_AutoProcessWhenPaymentMatches, "Auto-process on exact amount match?", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_PaymentExceptionsSent, "Statement of Exceptions after payment processed?", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_FirstDunningSentIfNotPaid, "Send first dunning notice after due date if not paid?", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_DaysUntilFirstDunningSent, "First dunning days", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_SecondDunningSentIfNotPaid, "Send Second Dunning Notice", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_DaysUntilSecondDunningSent, "Second Dunning days", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_ProducerWriteoffThreshold, "Producer Writeoff threshold", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_LowCommissionCleared, "Clear commission differences?", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ClearCommissionThreshold, "Commission threshold", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_LowGrossCleared, "Clear gross differences?", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_ClearGrossThreshold, "Gross threshold", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_ClearingLogicTarget, "Use logic for:", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_CreateOffsetsOnBilledInvoices, "Affect billed statement when an item is moved?", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_PmntSchdChngOffsetsOnBilled, "Affect billed statement when a payment schedule is changed?", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_CURRENCY, "Currency", ColumnDescriptor.TYPE_STRING)
  }

  construct() {

  }

  // properties
  var _WorkflowType                   : String                   as WorkflowType
  var _CycleCloseDayOfMonthLogic      : String                   as CycleCloseDayOfMonthLogic
  var _CycleCloseDayOfMonth           : Integer                  as CycleCloseDayOfMonth
  var _PaymentTermsInDays             : Integer                  as PaymentTermsInDays
  var _ExceptionForPastDueStatement   : Boolean                  as ExceptionForPastDueStatement
  var _StatementSentAfterCycleClose   : Boolean                  as StatementSentAfterCycleClose
  var _DaysAfterCycleCloseToSendStmnt : Integer                  as DaysAfterCycleCloseToSendStmnt
  var _SnapshotNonPastDueItems        : Boolean                  as SnapshotNonPastDueItems
  var _StatementsWithLowNetSuppressed : Boolean                  as StatementsWithLowNetSuppressed
  var _NetThresholdForSuppressingStmt : BigDecimal               as NetThresholdForSuppressingStmt
  var _ReminderSentIfPromiseNotRcvd   : Boolean                  as ReminderSentIfPromiseNotRcvd
  var _DaysUntilPromiseReminderSent   : Integer                  as DaysUntilPromiseReminderSent
  var _PromiseDueInDays               : Integer                  as PromiseDueInDays
  var _ExceptionIfPromiseNotReceived  : Boolean                  as ExceptionIfPromiseNotReceived
  var _PromiseExceptionsSent          : Boolean                  as PromiseExceptionsSent
  var _AutoProcessWhenPaymentMatches  : Boolean                  as AutoProcessWhenPaymentMatches
  var _PaymentExceptionsSent	        : Boolean                  as PaymentExceptionsSent
  var _FirstDunningSentIfNotPaid      : Boolean                  as FirstDunningSentIfNotPaid
  var _DaysUntilFirstDunningSent      : Integer                  as DaysUntilFirstDunningSent
  var _SecondDunningSentIfNotPaid     : Boolean                  as SecondDunningSentIfNotPaid
  var _DaysUntilSecondDunningSent     : Integer                  as DaysUntilSecondDunningSent
  var _ProducerWriteoffThreshold      : BigDecimal               as ProducerWriteoffThreshold
  var _LowCommissionCleared           : Boolean                  as LowCommissionCleared
  var _ClearCommisionThreshold	      : BigDecimal               as ClearCommissionThreshold
  var _LowGrossCleared	              : Boolean                  as LowGrossCleared
  var _ClearGrossThreshold	          : BigDecimal               as ClearGrossThreshold
  var _ClearingLogicTarget	          : String                   as ClearingLogicTarget
  var _CreateOffsetsOnBilledInvoices  : Boolean                  as CreateOffsetsOnBilledInvoices
  var _PmntSchdChngOffsetsOnBilled    : Boolean                  as PmntSchdChngOffsetsOnBilled
  var _Currency                       : Currency                 as Currency

}
