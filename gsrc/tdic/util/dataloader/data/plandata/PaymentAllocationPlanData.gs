package tdic.util.dataloader.data.plandata

uses tdic.util.dataloader.data.ColumnDescriptor
uses java.util.ArrayList

class PaymentAllocationPlanData extends PlanData {
  //constants
  public static final var SHEET: String = "PaymentAllocationPlan"
  public static final var COL_PUBLIC_ID: int = 0
  public static final var COL_NAME: int = 1
  public static final var COL_DESCRIPTION : int = 2
  public static final var COL_EFFECTIVE_DATE : int = 3
  public static final var COL_EXPIRATION_DATE : int = 4

  public static final var CD: ColumnDescriptor[] = {
      new ColumnDescriptor(COL_PUBLIC_ID, "Public ID", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_NAME, "Name", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_DESCRIPTION, "Description", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_EFFECTIVE_DATE, "Effective Date", ColumnDescriptor.TYPE_DATE),
      new ColumnDescriptor(COL_EXPIRATION_DATE, "Expiration Date", ColumnDescriptor.TYPE_DATE)
  }
  construct() {
  }
  var _Filters         : ArrayList<DistributionFilterType>     as Filters
  var _PayOrderTypes   : ArrayList<InvoiceItemOrderingType>    as PayOrderTypes

}