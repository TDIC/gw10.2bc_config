package tdic.util.dataloader.data.plandata

uses java.math.BigDecimal
uses java.lang.Integer
uses tdic.util.dataloader.data.ColumnDescriptor

class BillingPlanData extends PlanData {

  // constants
  public static final var SHEET : String = "BillingPlan"
  public static final var COL_PUBLIC_ID : int = 0
  public static final var COL_NAME : int = 1
  public static final var COL_DESCRIPTION : int = 2
  public static final var COL_LEADTIME : int = 3
  public static final var COL_NONRESPONSIVE_LEADTIME : int = 4
  public static final var COL_FIX_PAYMENT_DUEDATE_ON : int = 5
  public static final var COL_EFFECTIVE_DATE : int = 6
  public static final var COL_EXPIRATION_DATE : int = 7
  public static final var COL_INVOICE_FEE : int = 8
  public static final var COL_PAYMENT_REVERSAL_FEE : int = 9
  public static final var COL_SKIP_INSTALLMENT_FEES : int = 10
  public static final var COL_LINE_ITEMS_SHOW : int = 11
  public static final var COL_SUPPRESS_LOWBAL_INVOICES : int = 12
  public static final var COL_LOW_BALANCE_THRESHOLD : int = 13
  public static final var COL_LOW_BALANCE_METHOD : int = 14
  public static final var COL_REVIEW_DISBURSEMENTS_OVER : int = 15
  public static final var COL_DELAY_DISBURSEMENT : int = 16
  public static final var COL_AUTOMATIC_DISBURSEMENT_OVER : int = 17
  public static final var COL_AVAILABLE_DISB_AMT_TYPE : int = 18
  public static final var COL_CREATE_APPR_ACT_FOR_AUTO_DISB : int = 19
  public static final var COL_SEND_AUTO_DISB_AWAITING_APPROVAL : int = 20
  public static final var COL_REQUEST_INTERVAL : int = 21
  public static final var COL_CHANGE_DEADLINE_INTERVAL : int = 22
  public static final var COL_DRAFT_INTERVAL : int = 23
  public static final var COL_DRAFT_DAY_LOGIC : int = 24
  public static final var COL_CURRENCY: int = 25
  public static final var COL_LARGE_PREMIUM_THRESHOLD: int = 26
  public static final var COL_LARGE_PREMIUM_LEAD_TIME: int = 27
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_PUBLIC_ID, "Public ID", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_NAME, "Name", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_DESCRIPTION, "Description", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_LEADTIME,"Lead time", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_NONRESPONSIVE_LEADTIME, "Non-Responsive Lead Time", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_FIX_PAYMENT_DUEDATE_ON, "Fix Payment Due Date On", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_EFFECTIVE_DATE, "Effective Date", ColumnDescriptor.TYPE_DATE),
    new ColumnDescriptor(COL_EXPIRATION_DATE, "Expiration Date", ColumnDescriptor.TYPE_DATE),
    new ColumnDescriptor(COL_INVOICE_FEE, "Invoice Fee", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_PAYMENT_REVERSAL_FEE, "Payment Reversal Fee", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_SKIP_INSTALLMENT_FEES, "Skip Installments fee on Policies", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_LINE_ITEMS_SHOW, "Line Items Show", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_SUPPRESS_LOWBAL_INVOICES, "Suppress Low Balance Invoices", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_LOW_BALANCE_THRESHOLD, "Low Balance Threshold", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_LOW_BALANCE_METHOD, "Low Balance Method", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_REVIEW_DISBURSEMENTS_OVER, "Review Disbursements Over", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_DELAY_DISBURSEMENT, "Delay Disbursement Processing Days", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_AUTOMATIC_DISBURSEMENT_OVER, "Automatic disbursement when surplus exceeds amount available for disbursement +", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_AVAILABLE_DISB_AMT_TYPE, "Calculate amount available for disbursement as", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_CREATE_APPR_ACT_FOR_AUTO_DISB, "Create automatic disbursements with associated approval activities", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_SEND_AUTO_DISB_AWAITING_APPROVAL, "Send automatic disbursements awaiting approval on their due dates", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_REQUEST_INTERVAL, "# Of Days After Invoice Billed That A Payment Request Is Made", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_CHANGE_DEADLINE_INTERVAL, "# Of Days Before Draft Date That A Payment Request Is Fixed", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_DRAFT_INTERVAL, "# Of Days Before Due Date That A Payment Is Drafted", ColumnDescriptor.TYPE_NUMBER),
    new ColumnDescriptor(COL_DRAFT_DAY_LOGIC, "Fix Draft Date On", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_CURRENCY, "Currency", ColumnDescriptor.TYPE_STRING),
      new ColumnDescriptor(COL_LARGE_PREMIUM_THRESHOLD, "Large Premium Threshold", ColumnDescriptor.TYPE_NUMBER),
      new ColumnDescriptor(COL_LARGE_PREMIUM_LEAD_TIME, "Large Premium Lead Time", ColumnDescriptor.TYPE_NUMBER)
  }


  construct() {

  }
  
  // properties
  var _LeadTime                              : Integer              as LeadTime
  var _NonResponsiveLeadTime                 : Integer              as NonResponsiveLeadTime
  var _FixPaymentDueDateOn                   : DayOfMonthLogic      as FixPaymentDueDateOn
  var _InvoiceFee                            : BigDecimal           as InvoiceFee
  var _PaymentReversalFee                    : BigDecimal           as PaymentReversalFee
  var _SkipInstallmentFees                   : Boolean              as SkipInstallmentFees
  var _LineItemsShow                         : AggregationType      as LineItemsShow
  var _SuppressLowBalInvoices                : Boolean              as SuppressLowBalInvoices
  var _LowBalanceThreshold                   : BigDecimal           as LowBalanceThreshold
  var _LowBalancMethod                       : LowBalanceMethod     as LowBalanceMethod
  var _ReviewDisbursementsOver               : BigDecimal           as ReviewDisbursementsOver
  var _DelayDisbursement                     : Integer              as DelayDisbursement
  var _AutomaticDisbursementOver             : BigDecimal           as AutomaticDisbursementOver
  var _AvailableDisbAmtType                  : AvailableDisbAmtType as AvailableDisbAmtType
  var _CreateApprActForAutoDisb              : Boolean              as CreateApprActForAutoDisb
  var _SendAutoDisbAwaitingApproval          : Boolean              as SendAutoDisbAwaitingApproval
  var _RequestInterval                       : Integer              as RequestInterval
  var _ChangeDeadlineInterval                : Integer              as ChangeDeadlineInterval
  var _DraftInterval                         : Integer              as DraftInterval
  var _DraftDayLogic                         : DayOfMonthLogic      as DraftDayLogic
  var _Currency                              : Currency             as Currency
  var _LargePremiumthreshold                 : BigDecimal           as LargePremiumThreshold
  var _LargePremiumLeadTime                  : Integer			        as LargePremiumLeadTime
}
