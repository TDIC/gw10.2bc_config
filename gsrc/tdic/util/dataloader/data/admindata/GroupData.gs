package tdic.util.dataloader.data.admindata

uses tdic.util.dataloader.data.ApplicationData
uses java.lang.Integer
uses tdic.util.dataloader.data.ColumnDescriptor
uses java.util.ArrayList

class GroupData extends ApplicationData {

  // constants
  public static final var SHEET : String = "Group"
  public static final var COL_NAME : int = 0
  public static final var COL_PUBLIC_ID : int = 1
  public static final var COL_PARENT : int = 2
  public static final var COL_SUPERVISOR : int = 3
  public static final var COL_GROUP_TYPE : int = 4
  public static final var COL_EXCLUDE : int = 5
  public static final var COL_LOAD_FACTOR : int = 6
  public static final var COL_SECURTIY_ZONE : int = 7
  public static final var COL_REGION1 : int = 8
  public static final var COL_REGION2 : int = 9
  public static final var COL_REGION3 : int = 10
  public static final var CD : ColumnDescriptor[] = {
    new ColumnDescriptor(COL_NAME, "Name", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_PUBLIC_ID, "Public ID", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_PARENT, "Parent", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_SUPERVISOR, "Supervisor", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_GROUP_TYPE, "Group Type", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_EXCLUDE, "Exclude", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_LOAD_FACTOR, "Load Factor", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_SECURTIY_ZONE, "Security Zone", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_REGION1, "Region", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_REGION2, "Region", ColumnDescriptor.TYPE_STRING),
    new ColumnDescriptor(COL_REGION3, "Region", ColumnDescriptor.TYPE_STRING)
  }

  construct() {

  }
  
  // properties
  private var _name                    : String                       as Name
  private var _parent                  : String                       as Parent
  private var _supervisor              : UserData                     as Supervisor
  private var _grouptype               : GroupType                    as GroupType
  private var _loadfactor              : Integer                      as LoadFactor
  private var _securityzone            : SecurityZoneData             as SecurityZone
  private var _region1                 : RegionData                   as Region1
  private var _region2                 : RegionData                   as Region2
  private var _region3                 : RegionData                   as Region3
  
  public static function sortByHierarchy(groupArray : GroupData[]) : GroupData[] {
    var sortedGroups = new ArrayList<GroupData>(groupArray.Count)
    for (currGroup in groupArray) {
      var group = currGroup
      var i = sortedGroups.Count
      while (group != null) {
        var parent = groupArray.firstWhere(\ g -> g.Name == group.Parent)
        if (parent != null) {
          // add parent if not already in results
          if (!sortedGroups.contains(parent)) sortedGroups.add(i, parent)
        }
        group = parent
      }
      // add current group if not already in results
      if (!sortedGroups.contains(currGroup)) sortedGroups.add(currGroup)
    }
    return sortedGroups.toTypedArray()
  }
}
