package tdic.util.dataloader.processor

uses java.util.ArrayList
uses java.util.Date
uses gw.util.GWBaseDateEnhancement
uses tdic.util.dataloader.parser.ExcelFileParser
uses java.io.File
uses com.guidewire.pl.web.controller.UserDisplayableException
uses tdic.util.dataloader.data.ApplicationData
uses java.util.List
uses gw.api.locale.DisplayKey

class DataLoaderProcessor {
  construct() {
  }
  private var _myFile: gw.api.web.WebFile as MyFile
  //Excel File
  private var _path: String as Path
  //File Path
  private var _processDates: ArrayList <Date> as ProcessDates = new ArrayList <Date>()
  private var _tmpFile: File as TempFile
  private var _loadFromConfiguration: boolean as IsLoadingFromConfig = true
  private var _relativePath: String as RelativePath
  public final var dateHandlingText: List <String> = new ArrayList <String>(){
      DisplayKey.get("TDIC.ExcelDataLoader.SampleData.DateHandlingMethod.CreateDate"),
      DisplayKey.get("TDIC.ExcelDataLoader.SampleData.DateHandlingMethod.SystemDate"),
      DisplayKey.get("TDIC.ExcelDataLoader.SampleData.DateHandlingMethod.OverrideDate")}
  //Date Loading Options
  private var _dateHandling: String as DateHandling = dateHandlingText[0]
  //Default to Run with imported create date
  private var _dateHandlingOverride: Date as DateHandlingOverride = gw.api.util.DateUtil.currentDate()
  private var _startDate: Date as ProcessingStartDate
  private var _endDate: Date as ProcessingEndDate
  protected function readFile() {
    if (IsLoadingFromConfig) {
      readFileWithRelativePath()
    } else {
      //Reads Excel File loaded through UI
      var excelFileParser = new ExcelFileParser()
      TempFile = excelFileParser.readFile(MyFile)
      Path = TempFile.AbsolutePath
    }
  }

  private function readFileWithRelativePath() {
    var configRoot = gw.api.util.ConfigAccess.getModuleRoot("configuration")
    TempFile = new File(configRoot + "/" + RelativePath)
    if (!TempFile.exists() || !TempFile.canRead()) {
      throw new UserDisplayableException("Can not read the file to load: " + RelativePath)
    }
    Path = TempFile.AbsolutePath
  }

  protected function withinDateRange(newDate: Date): Boolean {
    if (DateHandling != dateHandlingText[0]){
      return true
    }
    if (ProcessingEndDate != null and ProcessingStartDate != null) {
      if (newDate.compareIgnoreTime(ProcessingStartDate) != - 1 and newDate.compareIgnoreTime(ProcessingEndDate) != 1) {
        return true
      } else {
        return false
      }
    } else {
      if (ProcessingEndDate == null and ProcessingStartDate == null) {
        return true
      } else {
        if (ProcessingEndDate == null and newDate.compareIgnoreTime(ProcessingStartDate) != - 1){
          return true
        }
        if (ProcessingStartDate == null and newDate.compareIgnoreTime(ProcessingEndDate) != 1){
          return true
        }
      }
    }
    return false
  }

  public function getLoadStatus(applicationData: ApplicationData): String {
    if (applicationData.Error) {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus.Error")
    }
    else if (applicationData.Updated) {
      return DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus.Updated")
    }
    else if (applicationData.Unchanged) {
        return DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus.Unchanged")
      }
      else if (applicationData.Skipped){
          return DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus.Skipped")
        }
    return DisplayKey.get("TDIC.ExcelDataLoader.Common.LoadStatus.Loaded")
  }

  public function showStatus(processor: BCSampleDataLoaderProcessor) {
    print ("Array:" + dateHandlingText[0] + " " + dateHandlingText[1] + " " + dateHandlingText[2])
    print ("Status: " + DateHandling + " " + DateHandlingOverride)
    print ("Processor: "+ processor.DateHandling)


  }
}