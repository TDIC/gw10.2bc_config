package tdic.util.dataloader.entityloaders.adminloaders

uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.admindata.QueueData
uses tdic.util.dataloader.util.BCAdminDataLoaderUtil
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses java.lang.Exception

class BCQueueLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aQueueData = applicationData as QueueData
    try {
      var currQueue = BCAdminDataLoaderUtil.findQueueByPublicID(aQueueData.PublicID)
      var aGroup = BCAdminDataLoaderUtil.findGroupByUserName(aQueueData.Group.Name)
      // add or update if Exclude is false
      if (aQueueData.Exclude != true and aGroup != null){
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          var aQueue: AssignableQueue
          // If the public id is not found then create a new record
          if (currQueue == null) {
            aQueue = new AssignableQueue()
          } else {
            // If the public id is found then update the existing record
            aQueue = bundle.add(currQueue)
          }
          aQueue.PublicID = aQueueData.PublicID
          aQueue.Name = aQueueData.Name
          aQueue.Description = aQueueData.Description
          aQueue.SubGroupVisible = aQueueData.SubGroupVisible
		  aGroup = bundle.add(aGroup)
          aQueue.Group = aGroup
          aGroup.addToAssignableQueues(aQueue)
          aQueueData.Skipped = false
          if (!aQueue.New) {
            if (aQueue.getChangedFields().Count > 0) {
              aQueueData.Updated = true
            } else {
              aQueueData.Unchanged = true
            }
          }
          bundle.commit()
        }, User.util.UnrestrictedUser)
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("Queue Load: " + aQueueData.Name + " for Group " + aQueueData.Group.Name + " " + e.toString())
      }
      aQueueData.Error = true
    }
  }
}
