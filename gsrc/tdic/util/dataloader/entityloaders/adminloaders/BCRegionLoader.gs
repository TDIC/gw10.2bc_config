package tdic.util.dataloader.entityloaders.adminloaders

uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.admindata.RegionData
uses tdic.util.dataloader.util.BCAdminDataLoaderUtil
uses java.lang.Exception

class BCRegionLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aRegionData = applicationData as RegionData
    try {
      var currRegion = BCAdminDataLoaderUtil.findRegionByPublicID(aRegionData.PublicID)
      // add or update if Exclude is false
      if (aRegionData.Exclude != true) {
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          var aRegion: Region
          // If the public id is not found then create a new region
          if (currRegion == null) {
            aRegion = new Region()
          } else {
            // If the public id is found then update the existing region
            aRegion = bundle.add(currRegion)
          }
          aRegion.PublicID = aRegionData.PublicID
          aRegion.Name = aRegionData.Name
          aRegionData.Skipped = false
          if (!aRegion.New) {
            if (aRegion.getChangedFields().Count > 0) {
              aRegionData.Updated = true
            } else {
              aRegionData.Unchanged = true
            }
          }
          bundle.commit()
        }, User.util.UnrestrictedUser)
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("Region Load: " + aRegionData.Name + " " + e.toString())
      }
      aRegionData.Error = true
    }
  }
}