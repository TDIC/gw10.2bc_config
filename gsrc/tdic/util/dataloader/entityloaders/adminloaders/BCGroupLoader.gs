package tdic.util.dataloader.entityloaders.adminloaders

uses java.util.ArrayList
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.admindata.GroupData
uses tdic.util.dataloader.util.BCAdminDataLoaderUtil
uses gw.command.demo.GeneralUtil
uses java.lang.Exception

class BCGroupLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aGroupData = applicationData as GroupData
    try {
      var currGroup = BCAdminDataLoaderUtil.findGroupByPublicID(aGroupData.PublicID)
      // add or update if Exclude is false
      if (aGroupData.Exclude != true) {
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          var aGroup: Group
          // If the public id is not found then create a new group and associated regions
          if (currGroup == null) {
            aGroup = new Group()
            addRegion(aGroupData.Region1.Name, aGroup)
            addRegion(aGroupData.Region2.Name, aGroup)
            addRegion(aGroupData.Region3.Name, aGroup)
          } else {
            // If the public id is found then update the group and associated regions
            aGroup = bundle.add(currGroup)
            // New Regions from the excel loader
            var newRegion = new ArrayList <Region> ()
            var existingRegion = new ArrayList <Region>()
            if (aGroupData.Region1.Name != null and aGroupData.Region1.Name != "") {
              newRegion.add(BCAdminDataLoaderUtil.findRegionByName(aGroupData.Region1.Name))
            }
            if (aGroupData.Region2.Name != null and aGroupData.Region2.Name != "") {
              newRegion.add(BCAdminDataLoaderUtil.findRegionByName(aGroupData.Region2.Name))
            }
            if (aGroupData.Region3.Name != null and aGroupData.Region3.Name != "") {
              newRegion.add(BCAdminDataLoaderUtil.findRegionByName(aGroupData.Region3.Name))
            }
            // Region currently existing in the group
            existingRegion.addAll(aGroup.Regions*.Region?.toList() as ArrayList <Region>)
            var addedRegions = newRegion.subtract(existingRegion)
            var removedRegions = existingRegion.subtract(newRegion)
            // Add the new regions to the group
            for (addRegion in addedRegions) {
              var aGroupRegion = new GroupRegion()
              aGroupRegion.Region = addRegion
              aGroup.addToRegions(aGroupRegion)
            }
            // Remove the regions which are not there in the loader
            for (remRegion in removedRegions) {
              var aGroupRegion = aGroup.Regions.firstWhere(\g -> g.Region == remRegion)
              aGroupRegion.Group = null
            }
          }
          aGroup.Name = aGroupData.Name
          aGroup.PublicID = aGroupData.PublicID
          aGroup.Parent = GeneralUtil.findGroupByUserName(aGroupData.Parent)
          aGroup.Supervisor = BCAdminDataLoaderUtil.findUserByPublicID(aGroupData.Supervisor.PublicID)
          aGroup.GroupType = aGroupData.GroupType
          aGroup.LoadFactor = aGroupData.LoadFactor
          aGroup.SecurityZone = GeneralUtil.findSecurityZoneByName(aGroupData.SecurityZone.Name)
          aGroupData.Skipped = false
          if (!aGroup.New) {
            if (aGroup.getChangedFields().Count > 0) {
              aGroupData.Updated = true
            } else {
              aGroupData.Unchanged = true
            }
          }
          bundle.commit()
        }, User.util.UnrestrictedUser)
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("Group Load: " + aGroupData.PublicID + " " + e.toString())
      }
      aGroupData.Error = true
    }
  }

  function addRegion(aRegion: String, group: Group) {
    var region = BCAdminDataLoaderUtil.findRegionByName(aRegion)
    if (region != null) {
      var aGroupRegion = new GroupRegion()
      aGroupRegion.Group = group
      aGroupRegion.Region = region
      group.addToRegions(aGroupRegion)
    }
  }
}