package tdic.util.dataloader.entityloaders.adminloaders

uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.admindata.ActivityPatternData
uses tdic.util.dataloader.util.BCAdminDataLoaderUtil
uses java.lang.Exception

class BCActivityPatternLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aActivityPatternData = applicationData as ActivityPatternData
    try {
      var currActivityPattern = BCAdminDataLoaderUtil.findActivityPatternByCode(aActivityPatternData.Code)
      // add or update if Exclude is false
      if (aActivityPatternData.Exclude != true){
        var aActivityPattern: ActivityPattern
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          if (currActivityPattern == null) {
            // add if not on file
            aActivityPattern = new ActivityPattern()
            aActivityPattern.PublicID = aActivityPatternData.PublicID
            aActivityPattern.Code = aActivityPatternData.Code
          } else {
            // update if on file
            aActivityPattern = bundle.add(currActivityPattern)
          }
          aActivityPattern.Subject = aActivityPatternData.Subject
          aActivityPattern.Description = aActivityPatternData.Description
          aActivityPattern.ActivityClass = aActivityPatternData.ActivityClass
          aActivityPattern.Type = aActivityPatternData.ActivityType
          aActivityPattern.Category = typekey.ActivityCategory.get(aActivityPatternData.Category)
          aActivityPattern.Mandatory = aActivityPatternData.Mandatory
          aActivityPattern.Priority = typekey.Priority.get(aActivityPatternData.Priority)
          aActivityPattern.Recurring = aActivityPatternData.Recurring
          aActivityPattern.TargetDays = aActivityPatternData.TargetDays
          aActivityPattern.TargetHours = aActivityPatternData.TargetHours
          aActivityPattern.TargetIncludeDays = aActivityPatternData.TargetIncludeDays
          aActivityPattern.TargetStartPoint = aActivityPatternData.TargetStartPoint
          aActivityPattern.EscalationDays = aActivityPatternData.EscalationDays
          aActivityPattern.EscalationHours = aActivityPatternData.EscalationHours
          aActivityPattern.EscalationInclDays = aActivityPatternData.EscalationInclDays
          aActivityPattern.EscalationStartPt = aActivityPatternData.EscalationStartPt
          aActivityPattern.AutomatedOnly = aActivityPatternData.AutomatedOnly
          aActivityPattern.Command = aActivityPatternData.Command
          // queue is optional so if we can't find it we'll still import the pattern
          if (aActivityPatternData.Queue != null) {
            if (!ActivityPatternData.isQueueAssignable) {
              if (LOG.InfoEnabled){
                LOG.info("Activity Pattern Load: " + aActivityPatternData.PublicID + " Queue " + aActivityPatternData.Queue.PublicID
                    + " ignored because ActivityPattern extension is missing.")
              }
            } else {
              var aQueue = BCAdminDataLoaderUtil.findQueueByDisplayName(aActivityPatternData.Queue.DisplayName)
              if (aQueue == null) {
                if (LOG.InfoEnabled){
                  LOG.info("Activity Pattern Load: " + aActivityPatternData.PublicID + " Queue " + aActivityPatternData.Queue.PublicID
                      + " ignored because it couldn't be found.")
                }
              } else {
                // use reflection to avoid compile errors if activity pattern queue extension is not used
                aActivityPatternData.setAssignableQueue(aActivityPattern, aQueue)
              }
            }
          }
          aActivityPatternData.Skipped = false
          if (!aActivityPattern.New) {
            if (aActivityPattern.getChangedFields().Count > 0) {
              aActivityPatternData.Updated = true
            } else {
              aActivityPatternData.Unchanged = true
            }
          }
          bundle.commit()
        }, User.util.UnrestrictedUser)
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("Activity Pattern Load: " + aActivityPatternData.PublicID + e.toString())
      }
      aActivityPatternData.Error = true
    }
  }
}