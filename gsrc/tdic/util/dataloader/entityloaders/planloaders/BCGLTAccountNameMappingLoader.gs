package tdic.util.dataloader.entityloaders.planloaders

uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.plandata.GLTAccountNameMappingData
uses java.lang.IllegalArgumentException
uses gw.command.demo.GeneralUtil
uses java.lang.Exception

/**
 * US570 - General Ledger Integration
 * 11/14/2014 Alvin Lee
 *
 * Excel Data Loader Additions for custom GL Integration entities.
 * Class to load the GL T-Account name mappings. It will check the existing mappings prior to creating a new one.
 */
class BCGLTAccountNameMappingLoader extends BCLoader implements EntityLoaderInterface {

  /**
   * Standard constructor
   */
  construct() {
  }

  /**
   * This will create the new GLIntegrationTAccountMapping_TDIC entity and commit the bundle
   * if it does not already exist.
   * 
   * @param applicationData A GLTAccountNameMappingData object containing the data for the GL T-Account Name Mapping
   */
  override function createEntity(applicationData : ApplicationData) {
    var aGLTAccountNameMappingData = applicationData as GLTAccountNameMappingData
    try {
      if (aGLTAccountNameMappingData.TransactionType == null) {
        throw new IllegalArgumentException("Transaction Type is null")
      }
      if (aGLTAccountNameMappingData.LineItemType == null) {
        throw new IllegalArgumentException("Line Item Type is null")
      }
      if (aGLTAccountNameMappingData.OriginalTAccountName == null) {
        throw new IllegalArgumentException("Original T-Account Name is null")
      }
      if (aGLTAccountNameMappingData.MappedTAccountName == null) {
        throw new IllegalArgumentException("Mapped T-Account Name is null")
      }
      var currMapping = GeneralUtil.findGLTAccountMapping(aGLTAccountNameMappingData.TransactionType, aGLTAccountNameMappingData.LineItemType, aGLTAccountNameMappingData.FutureTerm, aGLTAccountNameMappingData.InvoiceStatus, aGLTAccountNameMappingData.OriginalTAccountName)
      gw.transaction.Transaction.runWithNewBundle(\ bundle -> {
        var aGLMapping : GLIntegrationTAccountMapping_TDIC
        if (currMapping == null) { // create new
          aGLMapping = new GLIntegrationTAccountMapping_TDIC()
        } else { // update existing
          aGLMapping = bundle.add(currMapping)
        }
        aGLMapping.TranasctionType = aGLTAccountNameMappingData.TransactionType
        aGLMapping.LineItemType = aGLTAccountNameMappingData.LineItemType
        aGLMapping.FutureTerm = aGLTAccountNameMappingData.FutureTerm
        aGLMapping.InvoiceStatus = aGLTAccountNameMappingData.InvoiceStatus
        aGLMapping.OriginalTAccountName = aGLTAccountNameMappingData.OriginalTAccountName
        aGLMapping.MappedTAccountName = aGLTAccountNameMappingData.MappedTAccountName
        bundle.commit()
        aGLTAccountNameMappingData.Skipped = false
      })
    } catch(e : Exception) {
      LOG.warn("GLTAccountNameMapping Load Failed: " + aGLTAccountNameMappingData.TransactionType + e.toString())
      aGLTAccountNameMappingData.Error = true
    }
  }

}