package tdic.util.dataloader.entityloaders.planloaders

uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.plandata.ReturnPremiumPlanData
uses java.lang.Exception

class ReturnPremiumPlanLoader extends BCLoader implements EntityLoaderInterface {
  override function createEntity(applicationData: ApplicationData) {
  /* For a new Return Premium Plan, first need to add the plan -- default schemes are added automatically.
     Then retrieve the new scheme and update with the spreadsheet values.
   */

    var aReturnPremiumPlanData = applicationData as ReturnPremiumPlanData
    try {
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        var existingPlan = ReturnPremiumPlan.finder.findActivePlanByExactName(ReturnPremiumPlan, aReturnPremiumPlanData.Name) as ReturnPremiumPlan
        if (existingPlan != null) {
          existingPlan = bundle.add(existingPlan)
          existingPlan.ChargeQualification = aReturnPremiumPlanData.Qualifier
          existingPlan.ListBillAccountExcessTreatment = aReturnPremiumPlanData.ListBillExcess
        } else {
          var newPlan = new ReturnPremiumPlan()
          newPlan.PublicID = aReturnPremiumPlanData.PublicID
          newPlan.Name = aReturnPremiumPlanData.Name
          newPlan.Description = aReturnPremiumPlanData.Description
          newPlan.EffectiveDate = aReturnPremiumPlanData.EffectiveDate
          newPlan.ExpirationDate = aReturnPremiumPlanData.ExpirationDate
          newPlan.ChargeQualification = aReturnPremiumPlanData.Qualifier
          newPlan.ListBillAccountExcessTreatment = aReturnPremiumPlanData.ListBillExcess
          //remove all default schemes except 'Other'
          for (newScheme in newPlan.ReturnPremiumHandlingSchemes) {
            if (newScheme.HandlingCondition != ReturnPremiumHandlingCondition.TC_OTHER) {
              newPlan.removeHandlingScheme(newScheme)
            }
          }
          aReturnPremiumPlanData.Skipped = false
        }
        bundle.commit()
      }, User.util.UnrestrictedUser)
    } catch (ex: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("ReturnPremiumLoad Load: " + aReturnPremiumPlanData.PublicID + ex.toString())
      }
      aReturnPremiumPlanData.Error = true
    }
  }
}