package tdic.util.dataloader.entityloaders.planloaders

uses gw.api.database.Query
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.plandata.PlanLocalizationData
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface

class BCPlanLocalizationLoader extends BCLoader implements EntityLoaderInterface {

  override function createEntity(applicationData : ApplicationData) {
    var aLocalizationData = applicationData as PlanLocalizationData
    try {
      if (aLocalizationData.Language != null) {
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          //for each column with data, create (or update) the localization record
          if (aLocalizationData.BillingPlan != null) {
            billPlanLocalization(aLocalizationData)
          }
          if (aLocalizationData.PaymentPlan != null) {
            payPlanLocalization(aLocalizationData)
          }
          if (aLocalizationData.DelinquencyPlan != null) {
            delqPlanLocalization(aLocalizationData)
          }
          if (aLocalizationData.CommissionPlan != null) {
            commPlanLocalization(aLocalizationData)
          }
          if (aLocalizationData.CommissionSubPlan != null) {
            commSubPlanLocalization(aLocalizationData)
          }
          if (aLocalizationData.PayAllocationPlan != null) {
            payAllocationPlanLocalization(aLocalizationData)
          }
          if (aLocalizationData.ReturnPremiumPlan != null) {
            returnPremPlanLocalization(aLocalizationData)
          }
          if (aLocalizationData.ChargePattern != null) {
            chargePatternLocalization(aLocalizationData)
          }
          aLocalizationData.Skipped = false
          bundle.commit()
        })
      } else {
        aLocalizationData.Skipped = true
      }
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("Localization Load: " + aLocalizationData.Language + " " + e.toString())
      }
      aLocalizationData.Error = true
    }
  }

  private function billPlanLocalization(aLocalizationData : PlanLocalizationData) {
    var billingPlan = Query.make(BillingPlan).compare(BillingPlan#Name, Equals, aLocalizationData.BillingPlan).select().first()
    if (billingPlan != null) {
      var billingPlanName = Query.make(Plan_Name_L10N)
          .compare(Plan_Name_L10N#Owner, Equals, billingPlan)
          .compare(Plan_Name_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (billingPlanName == null) {
        billingPlanName = new Plan_Name_L10N()
        billingPlanName.Language = aLocalizationData.Language
        billingPlanName.Owner = billingPlan
      } else {
        gw.transaction.Transaction.Current.add(billingPlanName)
      }
      billingPlanName.Value = aLocalizationData.BillingPlanName

      var billingPlanDesc = Query.make(Plan_Description_L10N)
          .compare(Plan_Description_L10N#Owner, Equals, billingPlan)
          .compare(Plan_Description_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (billingPlanDesc == null) {
        billingPlanDesc = new Plan_Description_L10N()
        billingPlanDesc.Language = aLocalizationData.Language
        billingPlanDesc.Owner = billingPlan
      } else {
        gw.transaction.Transaction.Current.add(billingPlanDesc)
      }
      billingPlanDesc.Value = aLocalizationData.BillingPlanDesc
    }
  }

  private function payPlanLocalization(aLocalizationData : PlanLocalizationData) {
    var payPlan = Query.make(PaymentPlan).compare(PaymentPlan#Name, Equals, aLocalizationData.PaymentPlan).select().first()
    if (payPlan != null) {
      var payPlanName = Query.make(Plan_Name_L10N)
          .compare(Plan_Name_L10N#Owner, Equals, payPlan)
          .compare(Plan_Name_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (payPlanName == null) {
        payPlanName = new Plan_Name_L10N()
        payPlanName.Language = aLocalizationData.Language
        payPlanName.Owner = payPlan
      } else {
        gw.transaction.Transaction.Current.add(payPlanName)
      }
      payPlanName.Value = aLocalizationData.PaymentPlanName

      var payPlanDesc = Query.make(Plan_Description_L10N)
          .compare(Plan_Description_L10N#Owner, Equals, payPlan)
          .compare(Plan_Description_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (payPlanDesc == null) {
        payPlanDesc = new Plan_Description_L10N()
        payPlanDesc.Language = aLocalizationData.Language
        payPlanDesc.Owner = payPlan
      } else {
        gw.transaction.Transaction.Current.add(payPlanDesc)
      }
      payPlanDesc.Value = aLocalizationData.PaymentPlanDesc
    }
  }

  private function delqPlanLocalization(aLocalizationData : PlanLocalizationData) {
    var delqPlan = Query.make(DelinquencyPlan).compare(DelinquencyPlan#Name, Equals, aLocalizationData.DelinquencyPlan).select().first()
    if (delqPlan != null) {
      var delqPlanName = Query.make(Plan_Name_L10N)
          .compare(Plan_Name_L10N#Owner, Equals, delqPlan)
          .compare(Plan_Name_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (delqPlanName == null) {
        delqPlanName = new Plan_Name_L10N()
        delqPlanName.Language = aLocalizationData.Language
        delqPlanName.Owner = delqPlan
      } else {
        gw.transaction.Transaction.Current.add(delqPlanName)
      }
      delqPlanName.Value = aLocalizationData.DelinquencyPlanName
    }
  }

  private function commPlanLocalization(aLocalizationData : PlanLocalizationData) {
    var commPlan = Query.make(CommissionPlan).compare(CommissionPlan#Name, Equals, aLocalizationData.CommissionPlan).select().first()
    if (commPlan != null) {
      var commPlanName = Query.make(Plan_Name_L10N)
          .compare(Plan_Name_L10N#Owner, Equals, commPlan)
          .compare(Plan_Name_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (commPlanName == null) {
        commPlanName = new Plan_Name_L10N()
        commPlanName.Language = aLocalizationData.Language
        commPlanName.Owner = commPlan
      } else {
        gw.transaction.Transaction.Current.add(commPlanName)
      }
      commPlanName.Value = aLocalizationData.CommissionPlanName
    }
  }

  private function commSubPlanLocalization(aLocalizationData : PlanLocalizationData) {
    //subplan name is a combination of commission plan name and subplan name - first we need to separate the two strings
    var splitPosition = aLocalizationData.CommissionSubPlan.indexOf("|")
    if (splitPosition == -1) {
      throw new Exception("Invalid Commission SubPlan" + aLocalizationData.CommissionSubPlan)
    }

    var commPlan = Query.make(CommissionPlan)
        .compare(CommissionPlan#Name, Equals, aLocalizationData.CommissionSubPlan.substring(0,splitPosition))
        .select().first()
    if (commPlan != null) {
      print (aLocalizationData.CommissionSubPlan.substring(splitPosition + 1))
      var commSubPlan = Query.make(CommissionSubPlan)
          .compare(CommissionSubPlan#Name, Equals, aLocalizationData.CommissionSubPlan.substring(splitPosition + 1))
          .compare(CommissionSubPlan#CommissionPlan, Equals, commPlan)
          .select().first()
      if (commSubPlan != null) {
        var commSubPlanName = Query.make(CommissionSubPlan_Name_L10N)
            .compare(CommissionSubPlan_Name_L10N#Owner, Equals, commSubPlan)
            .compare(CommissionSubPlan_Name_L10N#Language, Equals, aLocalizationData.Language)
            .select().first()
        if (commSubPlanName == null) {
          commSubPlanName = new CommissionSubPlan_Name_L10N()
          commSubPlanName.Language = aLocalizationData.Language
          commSubPlanName.Owner = commSubPlan
        } else {
          gw.transaction.Transaction.Current.add(commSubPlanName)
        }
        commSubPlanName.Value = aLocalizationData.CommissionSubPlanName
      }
    }
  }

  private function payAllocationPlanLocalization(aLocalizationData : PlanLocalizationData) {
    var payAllocationPlan = Query.make(PaymentAllocationPlan).compare(PaymentAllocationPlan#Name, Equals, aLocalizationData.PayAllocationPlan).select().first()
    if (payAllocationPlan != null) {
      var payAllocationPlanName = Query.make(Plan_Name_L10N)
          .compare(Plan_Name_L10N#Owner, Equals, payAllocationPlan)
          .compare(Plan_Name_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (payAllocationPlanName == null) {
        payAllocationPlanName = new Plan_Name_L10N()
        payAllocationPlanName.Language = aLocalizationData.Language
        payAllocationPlanName.Owner = payAllocationPlan
      } else {
        gw.transaction.Transaction.Current.add(payAllocationPlanName)
      }
      payAllocationPlanName.Value = aLocalizationData.PayAllocationPlanName

      var payAllocationPlanDesc = Query.make(Plan_Description_L10N)
          .compare(Plan_Description_L10N#Owner, Equals, payAllocationPlan)
          .compare(Plan_Description_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (payAllocationPlanDesc == null) {
        payAllocationPlanDesc = new Plan_Description_L10N()
        payAllocationPlanDesc.Language = aLocalizationData.Language
        payAllocationPlanDesc.Owner = payAllocationPlan
      } else {
        gw.transaction.Transaction.Current.add(payAllocationPlanDesc)
      }
      payAllocationPlanDesc.Value = aLocalizationData.PayAllocationPlanDesc
    }
  }

  private function returnPremPlanLocalization(aLocalizationData : PlanLocalizationData) {
    var returnPremPlan = Query.make(ReturnPremiumPlan).compare(ReturnPremiumPlan#Name, Equals, aLocalizationData.ReturnPremiumPlan).select().first()
    if (returnPremPlan != null) {
      var returnPremPlanName = Query.make(Plan_Name_L10N)
          .compare(Plan_Name_L10N#Owner, Equals, returnPremPlan)
          .compare(Plan_Name_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (returnPremPlanName == null) {
        returnPremPlanName = new Plan_Name_L10N()
        returnPremPlanName.Language = aLocalizationData.Language
        returnPremPlanName.Owner = returnPremPlan
      } else {
        gw.transaction.Transaction.Current.add(returnPremPlanName)
      }
      returnPremPlanName.Value = aLocalizationData.ReturnPremiumPlanName

      var returnPremPlanDesc = Query.make(Plan_Description_L10N)
          .compare(Plan_Description_L10N#Owner, Equals, returnPremPlan)
          .compare(Plan_Description_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (returnPremPlanDesc == null) {
        returnPremPlanDesc = new Plan_Description_L10N()
        returnPremPlanDesc.Language = aLocalizationData.Language
        returnPremPlanDesc.Owner = returnPremPlan
      } else {
        gw.transaction.Transaction.Current.add(returnPremPlanDesc)
      }
      returnPremPlanDesc.Value = aLocalizationData.ReturnPremiumPlanDesc
    }
  }

  private function chargePatternLocalization(aLocalizationData : PlanLocalizationData) {
    var chargePattern = Query.make(ChargePattern).compare(ChargePattern#ChargeCode, Equals, aLocalizationData.ChargePattern).select().first()
    if (chargePattern != null) {
      var chargePatternName = Query.make(ChargePattern_ChargeName_L10N)
          .compare(ChargePattern_ChargeName_L10N#Owner, Equals, chargePattern)
          .compare(ChargePattern_ChargeName_L10N#Language, Equals, aLocalizationData.Language)
          .select().first()
      if (chargePatternName == null) {
        chargePatternName = new ChargePattern_ChargeName_L10N()
        chargePatternName.Language = aLocalizationData.Language
        chargePatternName.Owner = chargePattern
      } else {
        gw.transaction.Transaction.Current.add(chargePatternName)
      }
      chargePatternName.Value = aLocalizationData.ChargePatternName
    }
  }
}