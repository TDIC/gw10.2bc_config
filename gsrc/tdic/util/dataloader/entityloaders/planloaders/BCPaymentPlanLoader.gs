package tdic.util.dataloader.entityloaders.planloaders

uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.plandata.PaymentPlanData
uses gw.command.demo.GeneralUtil
uses java.lang.Exception
uses gw.pl.currency.MonetaryAmount

class BCPaymentPlanLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aPaymentPlanData = applicationData as PaymentPlanData
    try {
      var currPaymentPlan = GeneralUtil.findPaymentPlanByPublicId(aPaymentPlanData.PublicID)
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        var aPaymentPlan: PaymentPlan
        if (currPaymentPlan == null) {
          // create new
          aPaymentPlan = new PaymentPlan()
        } else {
          // update existing
          aPaymentPlan = bundle.add(currPaymentPlan)
        }
        aPaymentPlan.PublicID = aPaymentPlanData.PublicID
        aPaymentPlan.Name = aPaymentPlanData.Name
        aPaymentPlan.Description = aPaymentPlanData.Description
        aPaymentPlan.EffectiveDate = aPaymentPlanData.EffectiveDate
        aPaymentPlan.ExpirationDate = aPaymentPlanData.ExpirationDate
        //aPaymentPlan.Currencies
        aPaymentPlan.Reporting = aPaymentPlanData.Reporting
        aPaymentPlan.DownPaymentPercent = aPaymentPlanData.DownPaymentPercent
        aPaymentPlan.MaximumNumberOfInstallments = aPaymentPlanData.MaximumNumberOfInstallments
        aPaymentPlan.Periodicity = aPaymentPlanData.Periodicity
        //aPaymentPlan.InvoiceItemPlacementCutoffType() = aPaymentPlanData.InvoiceItemPlacementCutoffType
        aPaymentPlan.InvoicingBlackoutType = aPaymentPlanData.InvoicingBlackoutType
        aPaymentPlan.DaysBeforePolicyExpirationForInvoicingBlackout = aPaymentPlanData.DaysBeforePolicyExpirationForInvoicingBlackout
        aPaymentPlan.InstallmentFee = aPaymentPlanData.InstallmentFee == null ? new MonetaryAmount(0.00, Currency.TC_USD) : aPaymentPlanData.InstallmentFee.ofDefaultCurrency()
        aPaymentPlan.SkipFeeForDownPayment = aPaymentPlanData.SkipFeeForDownPayment
        aPaymentPlan.DaysFromReferenceDateToDownPayment = aPaymentPlanData.DaysFromReferenceDateToDownPayment
        aPaymentPlan.DownPaymentAfter = aPaymentPlanData.DownPaymentAfter
        aPaymentPlan.DaysFromReferenceDateToFirstInstallment = aPaymentPlanData.DaysFromReferenceDateToFirstInstallment
        aPaymentPlan.FirstInstallmentAfter = aPaymentPlanData.FirstInstallmentAfter
        aPaymentPlan.DaysFromReferenceDateToOneTimeCharge = aPaymentPlanData.DaysFromReferenceDateToOneTimeCharge
        aPaymentPlan.OneTimeChargeAfter = aPaymentPlanData.OneTimeChargeAfter
        aPaymentPlanData.Skipped = false
        if (!aPaymentPlan.New) {
          if (aPaymentPlan.getChangedFields().Count > 0) {
            aPaymentPlanData.Updated = true
          } else {
            aPaymentPlanData.Unchanged = true
          }
        }
        bundle.commit()
      })
    } catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("PaymentPlan Load: " + aPaymentPlanData.PublicID + e.toString())
      }
      aPaymentPlanData.Error = true
    }
  }
}