package tdic.util.dataloader.entityloaders.sampleloaders

uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.sampledata.BatchData
uses tdic.util.dataloader.util.BCSampleDataLoaderUtil
uses java.lang.Exception

class BCBatchLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aBatchData = applicationData as BatchData
    try {
      if (aBatchData.BatchName == "Default") {
        BCSampleDataLoaderUtil.runBatch()
      }
      else {
        BCSampleDataLoaderUtil.runABatch(aBatchData.BatchName)
      }
      LOG.info("Batch Run Date : " + aBatchData.BatchRunDate + "\n" + " Batch Name: " + aBatchData.BatchName + "\n")
      aBatchData.Skipped = false
    }
        catch (e: Exception) {
          if (LOG.InfoEnabled){
            LOG.info("Batch Run: " + aBatchData.BatchName + e.toString())
          }
          aBatchData.Error = true
        }
  }
}
