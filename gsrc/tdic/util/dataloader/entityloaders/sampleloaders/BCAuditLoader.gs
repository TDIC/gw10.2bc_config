package tdic.util.dataloader.entityloaders.sampleloaders

uses gw.command.demo.GeneralUtil
uses gw.webservice.policycenter.bc1000.entity.types.complex.FinalAuditInfo
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.data.ApplicationData
uses gw.transaction.Transaction
uses java.util.ArrayList
uses tdic.util.dataloader.data.sampledata.AuditData
uses java.lang.Exception
uses gw.api.database.Query
uses gw.api.database.QuerySelectColumns
uses gw.api.path.Paths

class BCAuditLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aAuditData = applicationData as AuditData
    // check for existing PolicyPeriod
    var pp = GeneralUtil.findPolicyPeriod(aAuditData.PolicyNumber)
    if (pp != null) {
      var newFinalAuditInfo = createFinalAuditInfo(aAuditData)
      if (aAuditData.Charges != null) {
        buildCharges(newFinalAuditInfo, aAuditData)
      }
      else {
        if (LOG.DebugEnabled) {
          LOG.debug("null Charges")
        }
      }
      try {
        Transaction.runWithNewBundle(\bundle ->
            {
              //Payment Plan Modifier Support
              if (payPlanModifiersExist(aAuditData)) {
                var payPlanModifiers = buildPayPlanModifierArray(aAuditData)
              }
              if (LOG.DebugEnabled) {
                LOG.debug("about to call BillingAPI.sendBillingInstruction with Final Audit: " + newFinalAuditInfo)
              }
              var bi = newFinalAuditInfo.executeFinalAuditBI()
              LOG.info("Final Audit: " + "\n" + " PolicyNumber: " + newFinalAuditInfo.PolicyNumber + "\n")
              if (LOG.DebugEnabled) {
                LOG.debug("BillingInstruction: " + bi + " returned")
              }
              aAuditData.Skipped = false
            } )
      } catch (e: Exception) {
        if (LOG.InfoEnabled){
          LOG.info("Final Audit Load: " + aAuditData.PolicyNumber + e.toString())
        }
        aAuditData.Error = true
      }
    }
  }

  //Payment Plan Modifier Support
  private function payPlanModifiersExist(aData: AuditData): boolean {
    if (aData.MatchPlannedInstallments) {
      return true
    } else if (aData.SuppressDownPayment) {
      return true
    } else if (aData.DownPaymentOverridePercentage != null) {
      return true
    } else if (aData.MaxNumberOfInstallmentsOverride != null) {
      return true
    } else {
      return false
    }
  }

  //Payment Plan Modifier Support
  private function buildPayPlanModifierArray(aData: AuditData): PaymentPlanModifier[] {
    var payPlanModifierList = new ArrayList <PaymentPlanModifier>()
    if (aData.MatchPlannedInstallments) {
      var payPlanModifier = new MatchPlannedInstallments()
      payPlanModifierList.add(payPlanModifier)
    }
    if (aData.SuppressDownPayment) {
      var payPlanModifier = new SuppressDownPayment()
      payPlanModifierList.add(payPlanModifier)
    }
    if (aData.DownPaymentOverridePercentage != null) {
      var payPlanModifier = new DownPaymentOverride()
      payPlanModifier.DownPaymentPercent = aData.DownPaymentOverridePercentage
      payPlanModifierList.add(payPlanModifier)
    }
    if (aData.MaxNumberOfInstallmentsOverride != null) {
      var payPlanModifier = new MaximumNumberOfInstallmentsOverride ()
      payPlanModifier.MaximumNumberOfInstallments = aData.MaxNumberOfInstallmentsOverride
      payPlanModifierList.add(payPlanModifier)
    }
    if (payPlanModifierList.HasElements) {
      return payPlanModifierList?.toTypedArray()
    }
    else {
      return null
    }
  }

  private function createFinalAuditInfo(aAuditData: AuditData): FinalAuditInfo {
    var aChange = new FinalAuditInfo()
    aChange.PolicyNumber = aAuditData.PolicyNumber
    aChange.TermNumber = aAuditData.TermNumber
    aChange.Description = aAuditData.Description
    aChange.EffectiveDate = DateToXmlDate(aAuditData.AuditDate)
    return aChange
  }

  private function buildCharges(info: FinalAuditInfo, data:AuditData) {
    var d = DateToXmlDate(data.EntryDate)
    var chargesToProcess = data.Charges.where(\elt -> elt.ChargePattern != null and elt.ChargeAmount != null and elt.ChargeCurrency != null and elt.ChargeAmount != 0)
    chargesToProcess.each(\elt -> {
      info.addChargeInfo(ChargeDataToChargeInfo(elt, d))
    })
  }
}
