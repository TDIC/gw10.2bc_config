package tdic.util.dataloader.entityloaders.sampleloaders

uses gw.command.demo.GeneralUtil
uses gw.webservice.policycenter.bc1000.entity.types.complex.CancelPolicyInfo
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.sampledata.CancellationData
uses gw.transaction.Transaction
uses java.lang.Exception
uses gw.api.database.Query
uses gw.api.database.Relop

class BCCancellationLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aCancellationData = applicationData as CancellationData
    // check for existing PolicyPeriod
    var polPeriod = GeneralUtil.findPolicyPeriod(aCancellationData.AssociatedPolicyPeriod)
    if (polPeriod != null) {
      aCancellationData.TermNumber = polPeriod.TermNumber
      var newPolicyCancelInfo = createPolicyCancelInfo(aCancellationData)
      if (aCancellationData.Charges != null) {
        buildCharges(newPolicyCancelInfo, aCancellationData)
      }
      else {
        if (LOG.DebugEnabled) {
          LOG.debug("null Charges")
        }
      }
      try {
        Transaction.runWithNewBundle(\bundle ->
            {
              //Payment Plan Modifier Support
              if (LOG.DebugEnabled) {
                LOG.debug("about to call BillingAPI.sendBillingInstruction with Policy Cancel: " + newPolicyCancelInfo)
              }
              var bi = newPolicyCancelInfo.executeCancellationBI()
              LOG.info("New Policy Cancel: " + "\n" + " PolicyNumber: " + newPolicyCancelInfo.PolicyNumber + "\n")
              if (LOG.DebugEnabled) {
                LOG.debug("BillingInstruction: " + bi + " returned")
              }
              aCancellationData.Skipped = false
            } )
      } catch (e: Exception) {
        if (LOG.InfoEnabled){
          LOG.info("Cancellation Load: " + aCancellationData.AssociatedPolicyPeriod + e.toString())
        }
        e.printStackTrace()
        aCancellationData.Error = true
      }
    }
  }

  private function createPolicyCancelInfo(aCancellationData: CancellationData): CancelPolicyInfo {
    var aCancel = new CancelPolicyInfo()
    aCancel.PolicyNumber = aCancellationData.AssociatedPolicyPeriod
    aCancel.TermNumber = aCancellationData.TermNumber
    aCancel.CancellationReason = aCancellationData.CancellationReason
    aCancel.CancellationType = aCancellationData.CancellationType as String
    aCancel.Description = aCancellationData.Description
    aCancel.EffectiveDate = DateToXmlDate(aCancellationData.ModificationDate)
    return aCancel
  }

  private function buildCharges(info: CancelPolicyInfo, data:CancellationData) {
    var d = DateToXmlDate(data.EntryDate)
    var chargesToProcess = data.Charges.where(\elt -> elt.ChargePattern != null and elt.ChargeAmount != null and elt.ChargeCurrency != null and elt.ChargeAmount != 0)
    chargesToProcess.each(\elt -> {
      info.addChargeInfo(ChargeDataToChargeInfo(elt, d))
    })
  }
}
