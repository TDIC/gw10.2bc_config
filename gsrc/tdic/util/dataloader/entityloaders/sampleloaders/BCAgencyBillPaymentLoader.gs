package tdic.util.dataloader.entityloaders.sampleloaders

uses gw.webservice.bc.bc1000.DistributionItemRecord
uses gw.webservice.bc.bc1000.PaymentAPI
uses gw.webservice.bc.bc1000.PaymentInstrumentRecord
uses gw.webservice.bc.bc1000.PaymentReceiptRecord
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.sampledata.AgencyBillPayData
uses java.lang.Exception
uses gw.api.database.Query

class BCAgencyBillPaymentLoader extends BCLoader implements EntityLoaderInterface {
  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aAgencyBillPayData = applicationData as AgencyBillPayData
    try {
      var newMoneyDetails = new PaymentReceiptRecord()
      var producer = Query.make(Producer).compare(Producer#Name, Equals, aAgencyBillPayData.Producer).select().AtMostOneRow
      if (producer == null) {
        throw new Exception("Producer not found")
      }
      newMoneyDetails.ProducerID = producer.PublicID
      var statementID = aAgencyBillPayData.StatementID
      var statement = Query.make(StatementInvoice).compare(StatementInvoice#InvoiceNumber, Equals, statementID).select().AtMostOneRow

      newMoneyDetails.PaymentInstrumentRecord = new PaymentInstrumentRecord()
      for (PI in producer.PaymentInstruments) {
        if (PI.PaymentMethod == aAgencyBillPayData.PaymentMethod) {
          if ((PI.Token == null and aAgencyBillPayData.PaymentToken == null) or
              (PI.Token == aAgencyBillPayData.PaymentToken))
            newMoneyDetails.PaymentInstrumentRecord.PublicID = PI.PublicID
          newMoneyDetails.PaymentInstrumentRecord.PaymentMethod = PI.getPaymentMethod()
          newMoneyDetails.PaymentInstrumentRecord.OneTime = false
          newMoneyDetails.PaymentInstrumentRecord.Token = PI.Token
        }

      }
      if (newMoneyDetails.PaymentInstrumentRecord.PublicID == null) {
        newMoneyDetails.PaymentInstrumentRecord.PaymentMethod = aAgencyBillPayData.PaymentMethod
        newMoneyDetails.PaymentInstrumentRecord.OneTime = true
        newMoneyDetails.PaymentInstrumentRecord.Token = aAgencyBillPayData.PaymentToken
      }
      newMoneyDetails.ReceivedDate = aAgencyBillPayData.ReceivedDate
      newMoneyDetails.RefNumber = aAgencyBillPayData.ReferenceNumber
      newMoneyDetails.MonetaryAmount = aAgencyBillPayData.Amount.ofDefaultCurrency()
      newMoneyDetails.PaymentReceiptType = AGENCYBILLMONEYRECEIVED
      newMoneyDetails.PaymentDate = aAgencyBillPayData.ReceivedDate

      if (statement == null) {
        //create payment to producer unapplied
        var unappliedPayment = new PaymentAPI().payToProducerUnapplied(newMoneyDetails)
      } else {
        var distributionRecords = new ArrayList<DistributionItemRecord>()
        for (item in statement.InvoiceItems) {
          var newDist = new DistributionItemRecord()
          newDist.InvoiceItemID = item.PublicID
          newDist.GrossAmount = item.GrossUnsettledAmount
          newDist.CommissionAmount = item.getUnsettledCommission()
          newDist.Disposition = DistItemDisposition.TC_AUTOEXCEPTION
          distributionRecords.add(newDist)
        }
        var attemptProducerWriteoff = false
        var dist = distributionRecords.toTypedArray()
        var payment = new PaymentAPI().makeAgencyBillPayment(newMoneyDetails, dist, attemptProducerWriteoff)
      }

      LOG.info("Agency Bill Payment: " + aAgencyBillPayData.ReceivedDate + "\n" + " Account: " + aAgencyBillPayData.Producer + "\n")
      aAgencyBillPayData.Skipped = false
    }
    catch (e: Exception) {
      if (LOG.InfoEnabled){
        LOG.info("Payment Load: " + aAgencyBillPayData.Producer + e.toString())
      }
      aAgencyBillPayData.Error = true
    }
  }
}
