package tdic.util.dataloader.entityloaders.sampleloaders

uses gw.webservice.policycenter.bc1000.entity.anonymous.elements.PolicyChangeInfo_PrimaryNamedInsuredContact
uses gw.webservice.policycenter.bc1000.entity.types.complex.RenewalInfo
uses gw.webservice.policycenter.bc1000.entity.types.complex.RewriteInfo
uses tdic.util.dataloader.data.sampledata.InvoiceStreamData
uses tdic.util.dataloader.data.sampledata.RenewalData
uses tdic.util.dataloader.entityloaders.BCLoader
uses tdic.util.dataloader.entityloaders.EntityLoaderInterface
uses tdic.util.dataloader.data.ApplicationData
uses tdic.util.dataloader.data.sampledata.RewriteData
uses gw.command.demo.GeneralUtil
uses gw.transaction.Transaction
uses java.util.ArrayList
uses gw.api.database.Query
uses java.lang.Exception
uses gw.api.database.Relop

class BCRewriteLoader extends BCLoader implements EntityLoaderInterface {

  var _bcContact: Contact as NewBCContact
  var _possibleInvoiceStreams: ArrayList <InvoiceStreamData> as PossibleInvoiceStreams

  construct() {
  }

  override function createEntity(applicationData: ApplicationData) {
    var aRewriteData = applicationData as RewriteData
    NewBCContact = convertContactToBC(aRewriteData.LookupPolicyPeriod.Contact)
    // check for existing Prior PolicyPeriod; no update if orig Policy is not available
    var ppPriorArray = Query.make(PolicyPeriod).compare(PolicyPeriod#PolicyNumber, Equals, aRewriteData.PriorPolicyPeriod).select()
    var ppPriorFind = ppPriorArray.firstWhere(\p -> p.TermNumber == ppPriorArray.max(\p2 -> p2.TermNumber))
    if (ppPriorFind != null) {
      var newRewriteInfo = createRewriteInfo(aRewriteData)
      if (aRewriteData.Charges != null) {
        buildCharges(newRewriteInfo, aRewriteData)
      }
      else {
        if (LOG.DebugEnabled) {
          LOG.debug("null Charges")
        }
      }
      try {
        Transaction.runWithNewBundle(\bundle ->
            {
              if (LOG.DebugEnabled) {
                LOG.debug("about to call BillingAPI.sendBillingInstruction with Rewrite: " + newRewriteInfo)
              }
              var bi = newRewriteInfo.executeRewriteBI()
              LOG.info("New Policy Rewrite: " + "\n" + " PolicyNumber: " + newRewriteInfo.PolicyNumber + "\n")
              if (LOG.DebugEnabled) {
                LOG.debug("BillingInstruction: " + bi + " returned")
              }
              aRewriteData.Skipped = false
            } )
      }
      catch (e: Exception) {
        if (LOG.InfoEnabled){
          LOG.info("Policy Rewrite Load: " + aRewriteData.LookupPolicyPeriod + e.toString())
        }
        aRewriteData.Error = true
      }
    }
  }

  private function createRewriteInfo(aRewriteData: RewriteData): RewriteInfo {
    var aRewrite = new RewriteInfo()
    var ppData = aRewriteData.LookupPolicyPeriod
    var acct = GeneralUtil.findAccountByName(aRewriteData.LookupPolicyPeriod.AccountName)
    aRewrite.PriorPolicyNumber = aRewriteData.PriorPolicyPeriod
    aRewrite.PriorTermNumber = 1
    aRewrite.Description = aRewriteData.Description
    aRewrite.AccountNumber = acct.AccountNumber
    aRewrite.Currency = (acct.Currency) as String
    aRewrite.PolicyNumber = ppData.PolicyNumber
    aRewrite.TermNumber = 1
    aRewrite.BillingMethodCode = ppData.BillingMethod
    aRewrite.ModelDate = DateToXmlDate(aRewriteData.EntryDate)
    aRewrite.EffectiveDate = DateToXmlDate(ppData.PolicyPerEffDate)
    aRewrite.PeriodStart = DateToXmlDate(ppData.PolicyPerEffDate)
    aRewrite.PeriodEnd = DateToXmlDate(ppData.PolicyPerExpirDate)
    aRewrite.PaymentPlanPublicId = Query.make(entity.PaymentPlan).compare(PaymentPlan#Name, Equals, ppData.PaymentPlan).select().getAtMostOneRow()?.PublicID
    aRewrite.ProductCode = ppData.LOBCode.Code

    if (ppData.ProducerCode != null) {
      var prodCode = Query.make(ProducerCode).compare(ProducerCode#Code, Equals, ppData.ProducerCode).select().FirstResult
      if(prodCode != null) {
        aRewrite.ProducerCodeOfRecordId = prodCode.PublicID
        if (LOG.DebugEnabled) {
          LOG.debug("Account: " + acct.AccountNumber + " added Primary Producer Code: " + prodCode)
        }
      } else {
        LOG.info("No Producer found for Producer Code " + ppData.ProducerCode)
      }
    }
    if (ppData.UnderAudit != null) {
      aRewrite.HasScheduledFinalAudit = ppData.UnderAudit
    }
    if (ppData.AssignedRisk != null) {
      aRewrite.AssignedRisk = ppData.AssignedRisk
    }
    if (ppData.UWCompany != null) {
      aRewrite.UWCompanyCode = ppData.UWCompany.Code
    }
    aRewrite.PrimaryNamedInsuredContact = BCContactToPrimaryNamedInsuredContact(NewBCContact)
    if(ppData.ListBillPayerAccount != null) {
      var lbpAccount = GeneralUtil.findAccountByName(ppData.ListBillPayerAccount)
      if (lbpAccount == null)
        lbpAccount = GeneralUtil.findAccountByName(ppData.ListBillPayerAccount)
      if (lbpAccount != null) {
        aRewrite.AltBillingAccountNumber = lbpAccount.AccountNumber
        if((PolicyPeriodBillingMethod.TC_LISTBILL.Code == ppData.BillingMethod
            or PolicyPeriodBillingMethod.TC_DIRECTBILL.Code == ppData.BillingMethod)
            and ppData.ListBillPayerInvoiceStream != null) {
          aRewrite.InvoiceStreamId = lbpAccount.InvoiceStreams.first()?.PublicID
        }
      }
    }
    return aRewrite
  }

  private function buildCharges(info: RewriteInfo, data:RewriteData) {
    var d = DateToXmlDate(data.EntryDate)
    var chargesToProcess = data.Charges.where(\elt -> elt.ChargePattern != null and elt.ChargeAmount != null and elt.ChargeCurrency != null and elt.ChargeAmount != 0)
    chargesToProcess.each(\elt -> {
      info.addChargeInfo(ChargeDataToChargeInfo(elt, d))
    })
  }

  private function BCContactToPrimaryNamedInsuredContact(c:Contact):PolicyChangeInfo_PrimaryNamedInsuredContact {
    var pcic = new PolicyChangeInfo_PrimaryNamedInsuredContact()
    pcic.$TypeInstance = BCContactToPCContactInfo(c)
    return pcic
  }
}
