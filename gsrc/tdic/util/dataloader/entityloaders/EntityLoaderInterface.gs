package tdic.util.dataloader.entityloaders

uses tdic.util.dataloader.data.ApplicationData

interface EntityLoaderInterface {
  function createEntity(applicationData: ApplicationData)
}