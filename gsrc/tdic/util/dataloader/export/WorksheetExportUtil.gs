package tdic.util.dataloader.export

uses java.util.HashMap
uses tdic.util.dataloader.data.ColumnDescriptor
uses gw.api.util.DisplayableException
uses java.util.Collection
uses java.lang.Exception
uses org.slf4j.LoggerFactory

class WorksheetExportUtil {
  public static final var LOG: org.slf4j.Logger = LoggerFactory.getLogger("DataLoader.Export")
  construct() {
  }

  // helper function to wrap column index for hashMap

  static function colIndexString(inx: int): String {
    var result = "Col:" + String.format("%03d", new Object[] {inx + 1})
    return result
  }

  // helper function to wrap sheet row count for hashMap

  static function rowIndexString(sheetName: String, rowCount: int): String {
    var result = sheetName + ":" + String.format("%03d", new Object[] {rowCount})
    return result
  }

  // helper function to get column header strings from column descriptors

  static function getHeaderNames(cdArray: ColumnDescriptor[]): HashMap {
    var headerMap = new HashMap()
    for (header in cdArray*.ColHeader index i) {
      headerMap.put(colIndexString(i), header)
    }
    return headerMap
  }

  // helper function to get column cell types from column descriptors

  static function getColumnTypes(cdArray: ColumnDescriptor[]): HashMap {
    var columnTypeMap = new HashMap()
    for (colType in cdArray*.ColType index i) {
      columnTypeMap.put(i, colType)
    }
    return columnTypeMap
  }

  public static function getEntityValues<T extends Object>
      (sheet: String,
       headerNames: HashMap,
       entities: Collection <T>,
       getRowFromEntity(bean: T): HashMap): HashMap {
    LOG.info(sheet + " sheet: Retrieving data started ***")
    var count: int = 1
    var rowMap = new HashMap()
    try {
      // get header row
      rowMap.put(rowIndexString(sheet, 0), headerNames
      )
      // get data rows
      for (entity in entities) {
        // Create column values for the excel sheet
        var colMap = getRowFromEntity(entity)
        rowMap.put(rowIndexString(sheet, count), colMap)
        count++
      }
    } catch (exception: Exception) {
      throw new DisplayableException(sheet + " sheet: Exception while retrieving data " + exception)
    }
    LOG.info(sheet + " sheet: Retrieving data completed ***")
    return rowMap
  }
}
