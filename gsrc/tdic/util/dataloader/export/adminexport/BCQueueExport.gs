package tdic.util.dataloader.export.adminexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.data.admindata.QueueData
uses tdic.util.dataloader.export.WorksheetExportUtil
uses java.util.Collection
uses gw.api.database.QuerySelectColumns
uses gw.api.path.Paths

class BCQueueExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return QueueData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getQueueEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(QueueData.CD), queryQueues(), \e -> getRowFromQueue(e))
  }

  // query data

  public static function queryQueues(): Collection <AssignableQueue> {
    var result = Query.make(AssignableQueue).select().orderBy(
      QuerySelectColumns.path(Paths.make(entity.AssignableQueue#PublicID))
    )
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromQueue(queue: AssignableQueue): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(QueueData.COL_DISPLAYNAME), queue.DisplayName)
    colMap.put(colIndexString(QueueData.COL_PUBLIC_ID), queue.PublicID)
    colMap.put(colIndexString(QueueData.COL_NAME), queue.Name)
    colMap.put(colIndexString(QueueData.COL_GROUP), queue.Group.Name)
    colMap.put(colIndexString(QueueData.COL_DESCRIPTION), queue.Description)
    colMap.put(colIndexString(QueueData.COL_SUB_GROUP_VISIBLE), queue.SubGroupVisible)
    colMap.put(colIndexString(QueueData.COL_EXCLUDE), "false")
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getQueueColumnTypes(): HashMap {
    return getColumnTypes(QueueData.CD)
  }
}
