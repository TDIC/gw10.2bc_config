package tdic.util.dataloader.export.adminexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.admindata.AuthorityLimitProfileData
uses java.util.Collection

class BCAuthorityLimitProfileExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return AuthorityLimitProfileData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getAuthLimitProfileEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(AuthorityLimitProfileData.CD), queryAuthorityLimitProfiles(), \e -> getRowFromAuthLimitProfile(e))
  }

  // query data

  public static function queryAuthorityLimitProfiles(): Collection <AuthorityLimitProfile> {
    var result = Query.make(AuthorityLimitProfile).select().where(\a -> a.Custom == false).orderBy(\a -> a.PublicID)
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromAuthLimitProfile(authLimitProfile: AuthorityLimitProfile): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(AuthorityLimitProfileData.COL_NAME), authLimitProfile.Name)
    colMap.put(colIndexString(AuthorityLimitProfileData.COL_PUBLIC_ID), authLimitProfile.PublicID)
    colMap.put(colIndexString(AuthorityLimitProfileData.COL_DESCRIPTION), authLimitProfile.Description)
    colMap.put(colIndexString(AuthorityLimitProfileData.COL_EXCLUDE), "false")
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getAuthLimitProfileColumnTypes(): HashMap {
    return getColumnTypes(AuthorityLimitProfileData.CD)
  }
}
