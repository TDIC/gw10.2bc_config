package tdic.util.dataloader.export.adminexport

uses org.apache.poi.ss.usermodel.Cell
uses java.util.HashMap
uses tdic.util.dataloader.export.WorksheetExportUtil
uses java.util.Collection
uses tdic.util.dataloader.data.admindata.RolePrivilegeData
uses java.util.List

class BCRolePrivilegeExport extends WorksheetExportUtil {
  // constants
  public static final var privileges: List <SystemPermissionType> = SystemPermissionType.getTypeKeys(false).orderBy(\s -> s.Code)
  construct(rCol: Collection <Role>) {
    Roles = rCol
  }
  var _roles: Collection <Role> as Roles
  // get sheet name

  public static function getSheetName(): String {
    return RolePrivilegeData.SHEET
  }

  public function getRolePrivilegeEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getRolePrivilegeHeaderNames(), Roles, \e -> getRolePrivilegeRowFromRole(e))
  }

  // build column map from entity

  public static function getRolePrivilegeRowFromRole(role: Role): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(0), role.Name)
    for (privilege in privileges index i) {
      var roleMatch = role.Privileges.firstWhere(\r -> r.Permission == privilege)
      if (roleMatch != null) {
        colMap.put(colIndexString(i + 1), "X")
      } else {
        colMap.put(colIndexString(i + 1), "")
      }
    }
    return colMap
  }

  /* @return : HashMap containing the permission type codes
     @desc   : Method to fetch the header names and comments for the role privilege sheet
  */

  static function getRolePrivilegeHeaderNames(): HashMap {
    var headerMap = new HashMap()
    // fixed column headers
    headerMap.put(colIndexString(RolePrivilegeData.COL_ROLE), RolePrivilegeData.CD[RolePrivilegeData.COL_ROLE].ColHeader)
    // dynamic column headers
    for (privilege in privileges index i) {
      headerMap.put(colIndexString(i + 1), privilege.Code + ":" + privilege.Description)
      // description will be exported as header cell comment
    }
    return headerMap
  }

  /* @return : HashMap containing the type of each column in role privilege sheet
     @desc   : Method to fetch the type of each column in role privilege sheet. 
               Used to create cell type during excel export  
  */

  static function getRolePrivilegeColumnTypes(): HashMap {
    var columnTypeMap = new HashMap()
    // fixed column types
    columnTypeMap.put(RolePrivilegeData.COL_ROLE, RolePrivilegeData.CD[RolePrivilegeData.COL_ROLE].ColType)
    // dynamic column types
    for (i in 1..privileges.Count) {
      columnTypeMap.put(i, Cell.CELL_TYPE_STRING)
    }
    return columnTypeMap
  }
}
