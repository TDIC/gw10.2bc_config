package tdic.util.dataloader.export.adminexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.data.admindata.AuthorityLimitData
uses tdic.util.dataloader.export.WorksheetExportUtil
uses java.util.Collection

class BCAuthorityLimitExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return AuthorityLimitData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getAuthLimitEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(AuthorityLimitData.CD), queryAuthLimits(), \e -> getRowFromAuthLimit(e))
  }

  // query data

  public static function queryAuthLimits(): Collection <AuthorityLimit> {
    var result = Query.make(AuthorityLimit).compare("Profile", NotEquals, null).select().where(\a -> !a.Profile.Custom).orderBy(\a -> a.Profile.PublicID).thenBy(\a -> a.LimitType.Code)
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromAuthLimit(authLimit: AuthorityLimit): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(AuthorityLimitData.COL_PROFILE), authLimit.Profile)
    colMap.put(colIndexString(AuthorityLimitData.COL_PUBLIC_ID), authLimit.PublicID)
    colMap.put(colIndexString(AuthorityLimitData.COL_EXCLUDE), "false")
    colMap.put(colIndexString(AuthorityLimitData.COL_LIMIT_TYPE), authLimit.LimitType)
    colMap.put(colIndexString(AuthorityLimitData.COL_LIMIT_AMOUNT), authLimit.LimitAmount)
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getAuthLimitColumnTypes(): HashMap {
    return getColumnTypes(AuthorityLimitData.CD)
  }
}
