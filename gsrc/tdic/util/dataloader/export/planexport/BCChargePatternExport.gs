package tdic.util.dataloader.export.planexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.plandata.ChargePatternData
uses java.util.Collection

class BCChargePatternExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return ChargePatternData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getChrgPatternEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(ChargePatternData.CD), queryChrgPatterns(), \e -> getRowFromChrgPattern(e))
  }

  // query data

  public static function queryChrgPatterns(): Collection <ChargePattern> {
    var result = Query.make(ChargePattern).select()
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromChrgPattern(charge: ChargePattern): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(ChargePatternData.COL_PUBLIC_ID), charge.PublicID)
    colMap.put(colIndexString(ChargePatternData.COL_CHARGE_CODE), charge.ChargeCode)
    colMap.put(colIndexString(ChargePatternData.COL_CHARGE_NAME), charge.ChargeName)
    colMap.put(colIndexString(ChargePatternData.COL_CHARGE_TYPE), charge.Subtype)
    if (charge.Subtype == TC_PRORATACHARGE) {
      colMap.put(colIndexString(ChargePatternData.COL_PERIODICITY), (charge as ProRataCharge).Periodicity)
    } else {
      colMap.put(colIndexString(ChargePatternData.COL_PERIODICITY), null)
    }
    colMap.put(colIndexString(ChargePatternData.COL_CATEGORY), charge.Category)
    colMap.put(colIndexString(ChargePatternData.COL_TACCOUNT_OWNER), charge.TAccountOwnerPattern.TAccountOwner)
    colMap.put(colIndexString(ChargePatternData.COL_INVOICING_APPROACH), charge.InvoiceTreatment)
    colMap.put(colIndexString(ChargePatternData.COL_PRIORITY), charge.Priority)
    colMap.put(colIndexString(ChargePatternData.COL_INCLUDE_IN_EQUITY_DATING), charge.IncludedInEquityDating)
    colMap.put(colIndexString(ChargePatternData.COL_EXCLUDE), "false")
    colMap.put(colIndexString(ChargePatternData.COL_PRINTPRIORITY), charge.PrintPriority_TDIC)
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getChrgPatternColumnTypes(): HashMap {
    return getColumnTypes(ChargePatternData.CD)
  }
}
