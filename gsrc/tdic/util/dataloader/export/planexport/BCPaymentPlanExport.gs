package tdic.util.dataloader.export.planexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.plandata.PaymentPlanData
uses java.util.Collection

class BCPaymentPlanExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return PaymentPlanData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getPayPlanEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(PaymentPlanData.CD), queryPayPlans(), \e -> getRowFromPayPlan(e))
  }

  // query data

  public static function queryPayPlans(): Collection <PaymentPlan> {
    var result = Query.make(PaymentPlan).select()
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromPayPlan(plan: PaymentPlan): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(PaymentPlanData.COL_PUBLIC_ID), plan.PublicID)
    colMap.put(colIndexString(PaymentPlanData.COL_NAME), plan.Name)
    colMap.put(colIndexString(PaymentPlanData.COL_DESCRIPTION), plan.Description)
    colMap.put(colIndexString(PaymentPlanData.COL_EFFECTIVE_DATE), plan.EffectiveDate.AsUIStyle)
    colMap.put(colIndexString(PaymentPlanData.COL_EXPIRATION_DATE), plan.ExpirationDate.AsUIStyle)
    colMap.put(colIndexString(PaymentPlanData.COL_REPORTING), plan.Reporting)
    colMap.put(colIndexString(PaymentPlanData.COL_DOWNPAYMENT_PERCENT), plan.DownPaymentPercent)
    colMap.put(colIndexString(PaymentPlanData.COL_MAXIMUM_NUMBER_OF_INSTALLMENTS), plan.MaximumNumberOfInstallments)
    colMap.put(colIndexString(PaymentPlanData.COL_PERIODICITY), plan.Periodicity)
    //colMap.put(colIndexString(PaymentPlanData.COL_INVOICE_ITEM_PLACEMENT_CUTOFF_TYPE), plan.InvoiceItemPlacementCutoffType)
    colMap.put(colIndexString(PaymentPlanData.COL_INVOICING_BLACKOUT_TYPE), plan.InvoicingBlackoutType)
    colMap.put(colIndexString(PaymentPlanData.COL_DAYS_BEFORE_POLICY_EXPIRATION_FOR_INVOICING_BLACKOUT), plan.DaysBeforePolicyExpirationForInvoicingBlackout)
    colMap.put(colIndexString(PaymentPlanData.COL_INSTALLMENT_FEE), plan.InstallmentFee)
    colMap.put(colIndexString(PaymentPlanData.COL_SKIP_FEE_FOR_DOWNPAYMENT), plan.SkipFeeForDownPayment)
    colMap.put(colIndexString(PaymentPlanData.COL_DAYS_FROM_REFERENCE_DATE_TO_DOWNPAYMENT), plan.DaysFromReferenceDateToDownPayment)
    colMap.put(colIndexString(PaymentPlanData.COL_DOWNPAYMENT_AFTER), plan.DownPaymentAfter)
    colMap.put(colIndexString(PaymentPlanData.COL_DAYS_FROM_REFERENCE_DATE_TO_FIRST_INSTALLMENT), plan.DaysFromReferenceDateToFirstInstallment)
    colMap.put(colIndexString(PaymentPlanData.COL_FIRST_INSTALLMENT_AFTER), plan.FirstInstallmentAfter)
    colMap.put(colIndexString(PaymentPlanData.COL_DAYS_FROM_REFERENCE_DATE_TO_ONETIME_CHARGE), plan.DaysFromReferenceDateToOneTimeCharge)
    colMap.put(colIndexString(PaymentPlanData.COL_ONETIME_CHARGE_AFTER), plan.OneTimeChargeAfter)
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getPayPlanColumnTypes(): HashMap {
    return getColumnTypes(PaymentPlanData.CD)
  }
}
