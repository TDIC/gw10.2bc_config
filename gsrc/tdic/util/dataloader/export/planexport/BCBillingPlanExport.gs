package tdic.util.dataloader.export.planexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.plandata.BillingPlanData
uses java.util.Collection

class BCBillingPlanExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return BillingPlanData.SHEET
  }

  // Function to get the billing plan data

  public static function getBillPlanEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(BillingPlanData.CD), queryBillPlans(), \e -> getRowFromBillPlan(e))
  }

  // query data

  public static function queryBillPlans(): Collection <BillingPlan> {
    var result = Query.make(BillingPlan).select()
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromBillPlan(plan: BillingPlan): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(BillingPlanData.COL_PUBLIC_ID), plan.PublicID)
    colMap.put(colIndexString(BillingPlanData.COL_NAME), plan.Name)
    colMap.put(colIndexString(BillingPlanData.COL_DESCRIPTION), plan.Description)
    colMap.put(colIndexString(BillingPlanData.COL_LEADTIME), plan.PaymentDueInterval)
    colMap.put(colIndexString(BillingPlanData.COL_NONRESPONSIVE_LEADTIME), plan.NonResponsivePmntDueInterval)
    colMap.put(colIndexString(BillingPlanData.COL_FIX_PAYMENT_DUEDATE_ON), plan.PaymentDueDayLogic.Code.toString())
//    colMap.put(colIndexString(BillingPlanData.COL_DUNNING_LEADTIME), plan.DunningInterval)
    colMap.put(colIndexString(BillingPlanData.COL_EFFECTIVE_DATE), plan.EffectiveDate.AsUIStyle)
    colMap.put(colIndexString(BillingPlanData.COL_EXPIRATION_DATE), plan.ExpirationDate.AsUIStyle)
    colMap.put(colIndexString(BillingPlanData.COL_INVOICE_FEE), plan.InvoiceFee)
    colMap.put(colIndexString(BillingPlanData.COL_PAYMENT_REVERSAL_FEE), plan.PaymentReversalFee)
    colMap.put(colIndexString(BillingPlanData.COL_SKIP_INSTALLMENT_FEES), plan.SkipInstallmentFees)
    colMap.put(colIndexString(BillingPlanData.COL_LINE_ITEMS_SHOW), plan.Aggregation.Code.toString())
    colMap.put(colIndexString(BillingPlanData.COL_SUPPRESS_LOWBAL_INVOICES), plan.SuppressLowBalInvoices)
    colMap.put(colIndexString(BillingPlanData.COL_LOW_BALANCE_THRESHOLD), plan.LowBalanceThreshold)
    colMap.put(colIndexString(BillingPlanData.COL_LOW_BALANCE_METHOD), plan.LowBalanceMethod.Code.toString())
    colMap.put(colIndexString(BillingPlanData.COL_REVIEW_DISBURSEMENTS_OVER), plan.ReviewDisbursementOver)
    colMap.put(colIndexString(BillingPlanData.COL_DELAY_DISBURSEMENT), plan.DelayDisbursement)
    colMap.put(colIndexString(BillingPlanData.COL_AUTOMATIC_DISBURSEMENT_OVER), plan.DisbursementOver)
    colMap.put(colIndexString(BillingPlanData.COL_AVAILABLE_DISB_AMT_TYPE), plan.AvailableDisbAmtType.Code.toString())
    colMap.put(colIndexString(BillingPlanData.COL_CREATE_APPR_ACT_FOR_AUTO_DISB), plan.CreateApprActForAutoDisb)
    colMap.put(colIndexString(BillingPlanData.COL_SEND_AUTO_DISB_AWAITING_APPROVAL), plan.SendAutoDisbAwaitingApproval)
    colMap.put(colIndexString(BillingPlanData.COL_REQUEST_INTERVAL), plan.RequestInterval)
    colMap.put(colIndexString(BillingPlanData.COL_CHANGE_DEADLINE_INTERVAL), plan.ChangeDeadlineInterval)
    colMap.put(colIndexString(BillingPlanData.COL_DRAFT_INTERVAL), plan.DraftInterval)
    colMap.put(colIndexString(BillingPlanData.COL_DRAFT_DAY_LOGIC), plan.DraftDayLogic.Code.toString())
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getBillPlanColumnTypes(): HashMap {
    return getColumnTypes(BillingPlanData.CD)
  }
}
