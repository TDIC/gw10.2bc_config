package tdic.util.dataloader.export.planexport

uses java.util.HashMap
uses gw.api.database.Query
uses tdic.util.dataloader.export.WorksheetExportUtil
uses tdic.util.dataloader.data.plandata.DelinquencyPlanData
uses java.util.Collection

class BCDelinquencyPlanExport extends WorksheetExportUtil {
  construct() {
  }

  // get sheet name

  public static function getSheetName(): String {
    return DelinquencyPlanData.SHEET
  }

  /* @return : HashMap containing the header name and values of the entity
     @desc   : Method to fetch the entity values
  */

  public static function getDelPlanEntityValues(): HashMap {
    return getEntityValues(getSheetName(), getHeaderNames(DelinquencyPlanData.CD), queryDelPlans(), \e -> getRowFromDelPlan(e))
  }

  // query data

  public static function queryDelPlans(): Collection <DelinquencyPlan> {
    var result = Query.make(DelinquencyPlan).select()
    return result.toCollection()
  }

  // build column map from entity

  public static function getRowFromDelPlan(plan: DelinquencyPlan): HashMap {
    // Create column values for the excel sheet
    var colMap = new HashMap()
    colMap.put(colIndexString(DelinquencyPlanData.COL_PUBLIC_ID), plan.PublicID)
    colMap.put(colIndexString(DelinquencyPlanData.COL_NAME), plan.Name)
    colMap.put(colIndexString(DelinquencyPlanData.COL_EFFECTIVE_DATE), plan.EffectiveDate.AsUIStyle)
    colMap.put(colIndexString(DelinquencyPlanData.COL_EXPIRATION_DATE), plan.ExpirationDate.AsUIStyle)
    colMap.put(colIndexString(DelinquencyPlanData.COL_CANCELLATION_TARGET), plan.CancellationTarget)
    colMap.put(colIndexString(DelinquencyPlanData.COL_HOLD_INVOICING_ON_TARGETED_POLICIES), plan.HoldInvoicingOnDlnqPolicies)
    colMap.put(colIndexString(DelinquencyPlanData.COL_GRACE_PERIOD_DAYS), plan.GracePeriodDays)
    //colMap.put(colIndexString(DelinquencyPlanData.COL_LATE_FEE), plan.LateFeeAmount)
    //colMap.put(colIndexString(DelinquencyPlanData.COL_REINSTATEMENT_FEE), plan.ReinstatementFeeAmount)
    colMap.put(colIndexString(DelinquencyPlanData.COL_WRITE_OFF_THRESHOLD), plan.WriteoffThreshold)
    colMap.put(colIndexString(DelinquencyPlanData.COL_ENTER_DELINQUENCY_THRESHOLD_ACCOUNT), plan.AcctEnterDelinquencyThreshold)
    colMap.put(colIndexString(DelinquencyPlanData.COL_ENTER_DELINQUENCY_THRESHOLD_POLICY), plan.PolEnterDelinquencyThreshold)
    colMap.put(colIndexString(DelinquencyPlanData.COL_CANCEL_POLICY), plan.CancellationThreshold)
    colMap.put(colIndexString(DelinquencyPlanData.COL_EXIT_DELINQUENCY), plan.ExitDelinquencyThreshold)
    colMap.put(colIndexString(DelinquencyPlanData.COL_APPLICABLE_SEGMENTS), plan.ApplicableSegments)
    return colMap
  }

  /* @return : HashMap containing the type of each column in the entity
     @desc   : Method to fetch the type of each column in the entity. 
               Used to create cell type during excel export  
  */

  static function getDelPlanColumnTypes(): HashMap {
    return getColumnTypes(DelinquencyPlanData.CD)
  }
}
