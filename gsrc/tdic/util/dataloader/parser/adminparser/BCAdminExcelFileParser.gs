package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.processor.DataLoaderProcessor
uses tdic.util.dataloader.processor.BCAdminDataLoaderProcessor
uses tdic.util.dataloader.parser.ExcelFileParser
uses java.io.FileInputStream
uses org.apache.poi.ss.usermodel. *
uses java.lang.Exception
uses tdic.util.dataloader.data.admindata.ActivityPatternData
uses tdic.util.dataloader.data.admindata.RoleData
uses tdic.util.dataloader.data.admindata.SecurityZoneData
uses tdic.util.dataloader.data.admindata.AuthorityLimitProfileData
uses tdic.util.dataloader.data.admindata.AuthorityLimitData
uses tdic.util.dataloader.data.admindata.UserAuthorityLimitData
uses tdic.util.dataloader.data.AddressData
uses tdic.util.dataloader.data.admindata.RegionData
uses tdic.util.dataloader.data.admindata.UserData
uses tdic.util.dataloader.data.admindata.GroupData
uses tdic.util.dataloader.data.admindata.UserGroupData
uses tdic.util.dataloader.data.admindata.AttributeData
uses tdic.util.dataloader.data.admindata.QueueData
uses tdic.util.dataloader.data.admindata.HolidayData
uses tdic.util.dataloader.data.admindata.RolePrivilegeData

class BCAdminExcelFileParser extends ExcelFileParser {
  construct() {
  }

  public static function importAdminFile(filePath: String, dataLoaderHelper: DataLoaderProcessor): DataLoaderProcessor {
    try
    {
      var fis = new FileInputStream(filePath)
      var wb = WorkbookFactory.create(fis)
      var roleSheet = getSheet(wb, RoleData.SHEET)
      var rolePrivilegeSheet = getSheet(wb, RolePrivilegeData.SHEET)
      var activityPatternSheet = getSheet(wb, ActivityPatternData.SHEET)
      var securityZoneSheet = getSheet(wb, SecurityZoneData.SHEET)
      var authorityLimitProfileSheet = getSheet(wb, AuthorityLimitProfileData.SHEET)
      var authorityLimitSheet = getSheet(wb, AuthorityLimitData.SHEET)
      var userAuthorityLimitSheet = getSheet(wb, UserAuthorityLimitData.SHEET)
      var addressSheet = getSheet(wb, AddressData.SHEET)
      var regionSheet = getSheet(wb, RegionData.SHEET)
      var userSheet = getSheet(wb, UserData.SHEET)
      var groupSheet = getSheet(wb, GroupData.SHEET)
      var userGroupSheet = getSheet(wb, UserGroupData.SHEET)
      var attributeSheet = getSheet(wb, AttributeData.SHEET)
      var queueSheet = getSheet(wb, QueueData.SHEET)
      var holidaySheet = getSheet(wb, HolidayData.SHEET)
      var bcAdminDataLoaderHelper = dataLoaderHelper as BCAdminDataLoaderProcessor
      /* Parse Roles */
      replaceDataWithParsedArray(bcAdminDataLoaderHelper.RoleArray, new RoleWorksheetParser().parseSheet(roleSheet))
      /* add permissions to existing Role Array */
      new RolePrivilegeWorksheetParser().parseSheetAddRole(rolePrivilegeSheet, bcAdminDataLoaderHelper.RoleArray)
      /* Parse Security Zones */
      replaceDataWithParsedArray(bcAdminDataLoaderHelper.SecurityZoneArray, new SecurityZoneWorksheetParser().parseSheet(securityZoneSheet))
      /* Parse Authority Limit Profiles */
      replaceDataWithParsedArray(bcAdminDataLoaderHelper.AuthorityLimitProfileArray, new AuthorityLimitProfileWorksheetParser().parseSheet(authorityLimitProfileSheet))
      /* Parse Profile Authority Limits */
      replaceDataWithParsedArray(bcAdminDataLoaderHelper.AuthorityLimitArray, new AuthorityLimitWorksheetParser().parseSheetWithProfile(authorityLimitSheet,
          bcAdminDataLoaderHelper.AuthorityLimitProfileArray))
      /* Parse Addresses */
      replaceDataWithParsedArray(bcAdminDataLoaderHelper.AddressArray, new AddressWorksheetParser().parseSheet(addressSheet))
      /* Parse Regions */
      replaceDataWithParsedArray(bcAdminDataLoaderHelper.RegionArray, new RegionWorksheetParser().parseSheet(regionSheet))
      /* Parse Attributes */
      replaceDataWithParsedArray(bcAdminDataLoaderHelper.AttributeArray, new AttributeWorksheetParser().parseSheet(attributeSheet))
      /* Parse Users */
      replaceDataWithParsedArray(bcAdminDataLoaderHelper.UserArray, new UserWorksheetParser().parseUserSheet(userSheet,
          bcAdminDataLoaderHelper.RegionArray, bcAdminDataLoaderHelper.RoleArray,
          bcAdminDataLoaderHelper.AddressArray))
      /* Parse Groups */
      replaceDataWithParsedArray(bcAdminDataLoaderHelper.GroupArray, new GroupWorksheetParser().parseGroupSheet(groupSheet,
          bcAdminDataLoaderHelper.RegionArray, bcAdminDataLoaderHelper.SecurityZoneArray, bcAdminDataLoaderHelper.UserArray))
      /* Parse User Groups */
      replaceDataWithParsedArray(bcAdminDataLoaderHelper.UserGroupArray, new UserGroupWorksheetParser().parseSheet(userGroupSheet))
      /* Parse User Group Columns */
      replaceDataWithParsedArray(bcAdminDataLoaderHelper.UserGroupColumnArray, new UserGroupColumnParser().parseSheet(userGroupSheet))
      /* Parse User Authority Limits */
      replaceDataWithParsedArray(bcAdminDataLoaderHelper.UserAuthorityLimitArray, new UserAuthorityLimitWorksheetParser().parseUserAuthorityLimitSheet(userAuthorityLimitSheet,
          bcAdminDataLoaderHelper.AuthorityLimitProfileArray))
      /* Parse Authority Limit Types */
      replaceDataWithParsedArray(bcAdminDataLoaderHelper.AuthorityLimitTypeArray, new AuthorityLimitTypeWorksheetParser().parseSheet(userAuthorityLimitSheet))
      /* Parse Queues */
      replaceDataWithParsedArray(bcAdminDataLoaderHelper.QueueArray, new QueueWorksheetParser().parseQueueSheet(queueSheet,
          bcAdminDataLoaderHelper.GroupArray))
      /* Parse Activity Patterns */
      replaceDataWithParsedArray(bcAdminDataLoaderHelper.ActivityPatternArray, new ActivityPatternWorksheetParser().parseActivityPatternSheet(activityPatternSheet,
          bcAdminDataLoaderHelper.QueueArray))
      /* Parse Holidays */
      replaceDataWithParsedArray(bcAdminDataLoaderHelper.HolidayArray, new HolidayWorksheetParser().parseHolidaySheet(holidaySheet))
    }
        catch (e: Exception)
        {
          throw "Error occurred while importing the file and parsing : " + e.toString()
        }
    return dataLoaderHelper
  }
}
