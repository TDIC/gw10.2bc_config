package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses tdic.util.dataloader.data.admindata.AttributeData
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses gw.api.upgrade.Coercions

class AttributeWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  override function parseSheet(sheet: Sheet): ArrayList <AttributeData> {
    return parseSheet(sheet, \r -> parseAttributeRow(r), true)
  }

  function parseAttributeRow(row: Row): AttributeData {
    // Populate Attribute Data
    var result = new AttributeData()
    result.Name = convertCellToString(row.getCell(AttributeData.COL_NAME))
    result.PublicID = convertCellToString(row.getCell(AttributeData.COL_PUBLIC_ID))
    result.Description = convertCellToString(row.getCell(AttributeData.COL_DESCRIPTION))
    result.UserAttributeType = typekey.UserAttributeType.get(convertCellToString(row.getCell(AttributeData.COL_USER_ATTRIBUTE_TYPE)))
    result.Active = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AttributeData.COL_ACTIVE)))
    result.Exclude = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AttributeData.COL_EXCLUDE)))
    return result
  }
}
