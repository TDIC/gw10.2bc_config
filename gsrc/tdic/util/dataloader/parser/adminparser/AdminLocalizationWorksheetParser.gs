package tdic.util.dataloader.parser.adminparser

uses org.apache.poi.ss.usermodel.Row
uses org.apache.poi.ss.usermodel.Sheet
uses tdic.util.dataloader.data.admindata.AdminLocalizationData
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses tdic.util.dataloader.parser.WorksheetParserUtil

class AdminLocalizationWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parseRegionRow(row: Row): AdminLocalizationData {
    var result = new AdminLocalizationData()
    // Populate Admin Localization Data
    result.Language = LanguageType.get(convertCellToString(row.getCell(AdminLocalizationData.COL_LANGUAGE)))
    result.Role = convertCellToString(row.getCell(AdminLocalizationData.COL_ROLE))
    result.RoleName = convertCellToString(row.getCell(AdminLocalizationData.COL_ROLE_NAME))
    result.RoleDesc = convertCellToString(row.getCell(AdminLocalizationData.COL_ROLE_DESC))
    result.ActivityPattern = convertCellToString(row.getCell(AdminLocalizationData.COL_ACTIVITY_PATTERN))
    result.ActvPatternSubj = convertCellToString(row.getCell(AdminLocalizationData.COL_ACTV_PATTERN_SUBJ))
    result.ActvPatternDesc = convertCellToString(row.getCell(AdminLocalizationData.COL_ACTV_PATTERN_DESC))
    result.SecZone = convertCellToString(row.getCell(AdminLocalizationData.COL_SEC_ZONE))
    result.SecZoneName = convertCellToString(row.getCell(AdminLocalizationData.COL_SEC_ZONE_NAME))
    result.SecZoneDesc = convertCellToString(row.getCell(AdminLocalizationData.COL_SEC_ZONE_DESC))
    result.AuthLimitProfile = convertCellToString(row.getCell(AdminLocalizationData.COL_AUTH_LIMIT_PROFILE))
    result.AuthLimitProfileName = convertCellToString(row.getCell(AdminLocalizationData.COL_AUTH_LIMIT_PROFILE_NAME))
    result.AuthLimitProfileDesc = convertCellToString(row.getCell(AdminLocalizationData.COL_AUTH_LIMIT_PROFILE_DESC))
    result.Group = convertCellToString(row.getCell(AdminLocalizationData.COL_GROUP))
    result.GroupName = convertCellToString(row.getCell(AdminLocalizationData.COL_GROUP_NAME))
    result.Region = convertCellToString(row.getCell(AdminLocalizationData.COL_REGION))
    result.RegionName = convertCellToString(row.getCell(AdminLocalizationData.COL_REGION_NAME))
    result.Attribute = convertCellToString(row.getCell(AdminLocalizationData.COL_ATTRIBUTE))
    result.AttributeName = convertCellToString(row.getCell(AdminLocalizationData.COL_ATTRIBUTE_NAME))
    result.AttributeDesc = convertCellToString(row.getCell(AdminLocalizationData.COL_ATTRIBUTE_DESC))
    result.Holiday = convertCellToString(row.getCell(AdminLocalizationData.COL_HOLIDAY))
    result.HolidayName = convertCellToString(row.getCell(AdminLocalizationData.COL_HOLIDAY_NAME))
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList <AdminLocalizationData> {
    return super.parseSheet(sheet, \r -> parseRegionRow(r), false)
  }
}