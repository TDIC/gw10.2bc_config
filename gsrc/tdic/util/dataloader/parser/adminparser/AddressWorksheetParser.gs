package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses tdic.util.dataloader.data.AddressData

class AddressWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parseAddressRow(row: Row): AddressData {
    // Define and instantiate variables 
    var result = new AddressData()
    // Populate Address Data
    result.DisplayName = row.getCell(AddressData.COL_DISPLAY_NAME).StringCellValue
    result.AddressLine1 = convertCellToString(row.getCell(AddressData.COL_ADDRESS_LINE1))
    result.AddressLine2 = convertCellToString(row.getCell(AddressData.COL_ADDRESS_LINE2))
    result.AddressLine3 = convertCellToString(row.getCell(AddressData.COL_ADDRESS_LINE3))
    result.City = convertCellToString(row.getCell(AddressData.COL_CITY))
    result.State = convertCellToString(row.getCell(AddressData.COL_STATE))
    result.PostalCode = convertCellToString(row.getCell(AddressData.COL_POSTAL_CODE))
    result.County = convertCellToString(row.getCell(AddressData.COL_COUNTY))
    result.Country = typekey.Country.get(convertCellToString(row.getCell(AddressData.COL_COUNTRY)))
    result.AddressType = typekey.AddressType.get(convertCellToString(row.getCell(AddressData.COL_ADDRESS_TYPE)))
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList <AddressData> {
    return super.parseSheet(sheet, \r -> {
      return validate(parseAddressRow(r))
    }, false);
  }

  function validate(ad: AddressData): AddressData {
    if (ad.DisplayName != null and ad.DisplayName != "")
      return ad
    else
      return null
  }
}
