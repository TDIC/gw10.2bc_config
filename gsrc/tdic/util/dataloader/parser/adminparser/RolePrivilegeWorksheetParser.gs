package tdic.util.dataloader.parser.adminparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.data.admindata.RoleData
uses tdic.util.dataloader.data.admindata.RolePrivilegeData
uses org.apache.poi.ss.usermodel.Sheet
uses java.util.ArrayList
uses java.util.HashMap
uses java.lang.Integer
uses org.apache.poi.ss.usermodel.Cell
uses java.lang.Exception

class RolePrivilegeWorksheetParser extends WorksheetParserUtil {
  construct() {
  }
  var ROLE: int = 0
  var firstRowDone = false
  var currRowNum: int
  var PermissionHM = new HashMap()
  public function parseSheetAddRole(sheet: Sheet, roleArray: ArrayList <RoleData>): ArrayList <RoleData> {
    if (sheet != null) {
      LOG.info(sheet.SheetName + " sheet: Parsing started ***")
      try
      {
        // Iterate through each row in the Role sheet 
        for (var row in sheet.rowIterator())
        {
          // ignore empty rows
          if (!isEmptyRow(row)) {
            currRowNum = row.RowNum
            // get permission codes from header row
            if (!firstRowDone)
            {
              // each cell beginning with column 2 (B) contains a permission code
              var x = 0 as Integer
              var cells = row.cellIterator()
              for (myCell in cells) {
                x = x + 1
                var perm = myCell.toString()
                if (perm != null and perm != "") {
                  // add to permissions Hashmap
                  PermissionHM.put(x, perm)
                }
              }
              firstRowDone = true
              continue
            }
            var data: RoleData
            // Get role data from Role Name in first column
            var roleName = row.getCell(ROLE).StringCellValue
            if (roleName != null and roleName != "" and roleArray != null){
              data = roleArray.firstWhere(\r -> r.Name.equalsIgnoreCase(roleName))
              if (data == null) {
                var msg = sheet.SheetName + " sheet: Value " + roleName
                    + " in column " + RolePrivilegeData.CD[RolePrivilegeData.COL_ROLE].ColHeader
                    + " of row " + currRowNum
                    + " is not a known role name"
                LOG.error(msg)
                throw msg
              }
              var roleRow = roleArray.indexOf(data)
              // step through columns, add permission to array if cell is non-blank
              var col = 0 as Integer
              var cells = row.cellIterator()
              for (myCell in cells) {
                // skip first column
                if (col > 0) {
                  var chk = (myCell.CellType == Cell.CELL_TYPE_FORMULA) ? myCell.RichStringCellValue.toString() : myCell.toString()
                  if (chk != null and chk != "") {
                    // get code from Hashmap and add to permission array
                    data.Permissions.add(PermissionHM.get(col + 1))
                  }
                }
                col = col + 1
              }
              // Save Role back to array in the same array location
              if (data.Permissions != null) {
                roleArray[roleRow] = data
              }
            }
          }
        }
      }
          catch (e: Exception)
          {
            var msg = sheet.SheetName + " sheet: Error parsing Role Privilege sheet at row " + currRowNum + " : "
            LOG.error(msg, e)
            throw msg + e.toString()
          }
      LOG.info(sheet.SheetName + " sheet: Parsing completed ***")
    }
    return roleArray
  }
}
