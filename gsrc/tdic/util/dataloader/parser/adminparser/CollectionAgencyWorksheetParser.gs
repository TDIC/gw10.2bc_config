package tdic.util.dataloader.parser.adminparser

uses gw.api.upgrade.Coercions
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses tdic.util.dataloader.data.AddressData
uses tdic.util.dataloader.data.admindata.CollectionAgencyData
uses tdic.util.dataloader.parser.WorksheetParserUtil

class CollectionAgencyWorksheetParser extends WorksheetParserUtil {
  construct() {  }
  var addressArray= new ArrayList <AddressData>()

  function parseUserRow(row: Row): CollectionAgencyData {
    var result = new CollectionAgencyData()
    result.Name = convertCellToString(row.getCell(CollectionAgencyData.COL_NAME))
    result.Phone = convertCellToString(row.getCell(CollectionAgencyData.COL_PHONE))
    result.Fax = convertCellToString(row.getCell(CollectionAgencyData.COL_FAX))
    result.Email = convertCellToString(row.getCell(CollectionAgencyData.COL_EMAIL_ADDRESS))
    result.Exclude = Coercions.makeBooleanFrom(convertCellToString(row.getCell(CollectionAgencyData.COL_EXCLUDE)))
    result.PublicID = convertCellToString(row.getCell(CollectionAgencyData.COL_PUBLIC_ID))
    var addressID = (convertCellToString(row.getCell(CollectionAgencyData.COL_ADDRESS)))
    if (addressArray != null){
      result.Address = addressArray.firstWhere(\p -> p.DisplayName == addressID)
    }
    return result
  }

  function parseUserSheet(sheet: Sheet, addrArray: ArrayList <AddressData>): ArrayList <CollectionAgencyData> {
    addressArray = addrArray
    return parseSheet(sheet, \r -> parseUserRow(r), true)
  }
}