package tdic.util.dataloader.parser.planparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses java.lang.Integer
uses tdic.util.dataloader.data.plandata.CommissionSubPlanData
uses gw.api.upgrade.Coercions

class CommissionSubPlanWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parseCommissionSubPlanRow(row: Row): CommissionSubPlanData {
    var result = new CommissionSubPlanData()
    // Populate Commission SubPlan Data
    result.PlanName = convertCellToString(row.getCell(CommissionSubPlanData.COL_PLAN_NAME))
    if (result.PlanName == null)
      return null
    result.SubPlanName = convertCellToString(row.getCell(CommissionSubPlanData.COL_SUBPLAN_NAME))
    result.Priority = Coercions.makeIntFrom(convertCellToString(row.getCell(CommissionSubPlanData.COL_PRIORITY)))
    result.AssignedRisk = typekey.AssignedRiskRestriction.get(convertCellToString(row.getCell(CommissionSubPlanData.COL_ASSIGNED_RISK)))
    result.AllEvaluations = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_EVALUATIONS)), true)
    result.AllLOBCodes = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_LOB_CODES)), true)
    result.LOBWorkersComp = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_LOB_CODES + 1)), true)
    result.LOBCommercialProperty = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_LOB_CODES + 2)), true)
    result.LOBInlandMarine = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_LOB_CODES + 3)), true)
    result.LOBGeneralLiability = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_LOB_CODES + 4)), true)
    result.LOBBusinessAuto = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_LOB_CODES + 5)), true)
    result.LOBPersonalAuto = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_LOB_CODES + 6)), true)
    result.LOBBusinessOwners = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_LOB_CODES + 7)), true)
    result.AllSegments = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_SEGMENTS)), true)
    result.SegmentLargeBusiness = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_SEGMENTS + 1)), true)
    result.SegmentMediumBusiness = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_SEGMENTS + 2)), true)
    result.SegmentPersonal = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_SEGMENTS + 3)), true)
    result.SegmentSmallBusiness = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_SEGMENTS + 4)), true)
    result.SegmentSubprime = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_SEGMENTS + 5)), true)
    result.AllJurisdictions = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_JURISDICTIONS)), true)
    result.AllTerms = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_TERMS)), true)
    result.TermsInitialBusiness = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_TERMS + 1)), true)
    result.TermsFirstRenewal = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_TERMS + 2)), true)
    result.TermsSecondRenewal = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_TERMS + 3)), true)
    result.TermsThirdRenewal = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_TERMS + 4)), true)
    result.TermsThereafter = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_TERMS + 5)), true)
    result.AllUWCompanies = setBoolean(convertCellToString(row.getCell(CommissionSubPlanData.COL_ALL_UW_COMPANIES)), true)
    // Get the rest of the SubPlan Data from common function
    if (result.SubPlanName != null) {
      result = CommissionSubPlanParserUtility.populateSubPlanData(result, row, CommissionSubPlanData.COL_PRIMARY_RATE)
    }
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList <CommissionSubPlanData> {
    return parseSheet(sheet, \r -> parseCommissionSubPlanRow(r), false)
  }
}