package tdic.util.dataloader.parser.planparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses java.lang.Integer
uses tdic.util.dataloader.data.plandata.DelinquencyPlanWorkflowData
uses gw.api.upgrade.Coercions

class DelinquencyPlanWorkflowWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parseDelinquencyPlanWorkflowRow(row: Row): DelinquencyPlanWorkflowData {
    var result = new DelinquencyPlanWorkflowData()
    // Populate Delinquency Plan Workflow Data
    result.DelinquencyPlanName = convertCellToString(row.getCell(DelinquencyPlanWorkflowData.COL_DELINQUENCY_PLAN_NAME))
    if (result.DelinquencyPlanName == null)
      return null
    result.DelinquencyReason = typekey.DelinquencyReason.get(convertCellToString(row.getCell(DelinquencyPlanWorkflowData.COL_DELINQUENCY_REASON)))
    result.WorkflowType = convertCellToString(row.getCell(DelinquencyPlanWorkflowData.COL_WORKFLOW_TYPE))
    result.EventName = typekey.DelinquencyEventName.get(convertCellToString(row.getCell(DelinquencyPlanWorkflowData.COL_EVENT_NAME)))
    result.EventTrigger = typekey.DelinquencyTriggerBasis.get(convertCellToString(row.getCell(DelinquencyPlanWorkflowData.COL_EVENT_TRIGGER)))
    result.EventOffset = Coercions.makeIntFrom(convertCellToString(row.getCell(DelinquencyPlanWorkflowData.COL_EVENT_OFFSET)))
    result.EventRelativeOrder = Coercions.makeIntFrom(convertCellToString(row.getCell(DelinquencyPlanWorkflowData.COL_EVENT_RELATIVE_ORDER)))
    result.EventAutomatic = Coercions.makeBooleanFrom(convertCellToString(row.getCell(DelinquencyPlanWorkflowData.COL_EVENT_AUTOMATIC)))
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList <DelinquencyPlanWorkflowData> {
    return parseSheet(sheet, \r -> parseDelinquencyPlanWorkflowRow(r), false)
  }
}