package tdic.util.dataloader.parser.planparser

uses tdic.util.dataloader.parser.WorksheetParserInterface
uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.data.plandata.ReturnPremiumSchemeData
uses java.util.ArrayList
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.lang.Integer
uses gw.api.upgrade.Coercions

class ReturnPremiumSchemeWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parseReturnPremiumSchemeRow(row: Row): ReturnPremiumSchemeData {
    var result = new ReturnPremiumSchemeData ()
    // Populate Return Premium Scheme Data
    result.ReturnPremiumPlanName = convertCellToString(row.getCell(ReturnPremiumSchemeData.COL_RETURN_PREMIUM_PLAN_NAME))
    result.Priority = Coercions.makeIntFrom(convertCellToString(row.getCell(ReturnPremiumSchemeData.COL_PRIORITY)))
    result.HandlingCondition = typekey.ReturnPremiumHandlingCondition.get(convertCellToString(row.getCell(ReturnPremiumSchemeData.COL_CONTEXT)))
    result.StartDateOption = typekey.ReturnPremiumStartDateOption.get(convertCellToString(row.getCell(ReturnPremiumSchemeData.COL_START_DATE_OPTION)))
    result.AllocateTiming = typekey.ReturnPremiumAllocateTiming.get(convertCellToString(row.getCell(ReturnPremiumSchemeData.COL_ALLOCATE_TIMING)))
    result.AllocateMethod = typekey.ReturnPremiumAllocateMethod.get(convertCellToString(row.getCell(ReturnPremiumSchemeData.COL_ALLOCATE_METHOD)))
    result.ExcessTreatment = typekey.ReturnPremiumExcessTreatment.get(convertCellToString(row.getCell(ReturnPremiumSchemeData.COL_EXCESS_TREATMENT)))
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList < ReturnPremiumSchemeData > {
    return parseSheet(sheet, \r -> parseReturnPremiumSchemeRow(r), false)
  }
}