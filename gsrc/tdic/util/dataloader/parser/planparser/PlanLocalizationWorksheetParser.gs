package tdic.util.dataloader.parser.planparser

uses org.apache.poi.ss.usermodel.Row
uses org.apache.poi.ss.usermodel.Sheet
uses tdic.util.dataloader.data.plandata.PlanLocalizationData
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses tdic.util.dataloader.parser.WorksheetParserUtil

class PlanLocalizationWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
    construct() {
    }

  function parseRegionRow(row: Row): PlanLocalizationData {
    var result = new PlanLocalizationData()
    // Populate Admin Localization Data
    result.Language = LanguageType.get(convertCellToString(row.getCell(PlanLocalizationData.COL_LANGUAGE)))
    result.BillingPlan = convertCellToString(row.getCell(PlanLocalizationData.COL_BILLING_PLAN))
    result.BillingPlanName = convertCellToString(row.getCell(PlanLocalizationData.COL_BILLING_PLAN_NAME))
    result.BillingPlanDesc = convertCellToString(row.getCell(PlanLocalizationData.COL_BILLING_PLAN_DESC))
    result.PaymentPlan = convertCellToString(row.getCell(PlanLocalizationData.COL_PAYMENT_PLAN))
    result.PaymentPlanName = convertCellToString(row.getCell(PlanLocalizationData.COL_PAYMENT_PLAN_NAME))
    result.PaymentPlanDesc = convertCellToString(row.getCell(PlanLocalizationData.COL_PAYMENT_PLAN_DESC))
    result.DelinquencyPlan = convertCellToString(row.getCell(PlanLocalizationData.COL_DELINQUENCY_PLAN))
    result.DelinquencyPlanName = convertCellToString(row.getCell(PlanLocalizationData.COL_DELINQUENCY_PLAN_NAME))
    result.CommissionPlan = convertCellToString(row.getCell(PlanLocalizationData.COL_COMMISSION_PLAN))
    result.CommissionPlanName = convertCellToString(row.getCell(PlanLocalizationData.COL_COMMISSION_PLAN_NAME))
    result.CommissionSubPlan = convertCellToString(row.getCell(PlanLocalizationData.COL_COMMISSION_SUBPLAN))
    result.CommissionSubPlanName = convertCellToString(row.getCell(PlanLocalizationData.COL_COMMISSION_SUBPLAN_NAME))
    result.AgencyBillPlan = convertCellToString(row.getCell(PlanLocalizationData.COL_AGENCY_BILL_PLAN))
    result.AgencyBillPlanName = convertCellToString(row.getCell(PlanLocalizationData.COL_AGENCY_BILL_PLAN_NAME))
    result.PayAllocationPlan = convertCellToString(row.getCell(PlanLocalizationData.COL_PAY_ALLOCATION_PLAN))
    result.PayAllocationPlanName = convertCellToString(row.getCell(PlanLocalizationData.COL_PAY_ALLOCATION_PLAN_NAME))
    result.PayAllocationPlanDesc = convertCellToString(row.getCell(PlanLocalizationData.COL_PAY_ALLOCATION_PLAN_DESC))
    result.ReturnPremiumPlan = convertCellToString(row.getCell(PlanLocalizationData.COL_RETURN_PREMIUM_PLAN))
    result.ReturnPremiumPlanName = convertCellToString(row.getCell(PlanLocalizationData.COL_RETURN_PREMIUM_PLAN_NAME))
    result.ReturnPremiumPlanDesc = convertCellToString(row.getCell(PlanLocalizationData.COL_RETURN_PREMIUM_PLAN_DESC))
    result.ChargePattern = convertCellToString(row.getCell(PlanLocalizationData.COL_CHARGE_PATTERN))
    result.ChargePatternName = convertCellToString(row.getCell(PlanLocalizationData.COL_CHARGE_PATTERN_NAME))
    return result
    }

  override function parseSheet(sheet: Sheet): ArrayList <PlanLocalizationData> {
    return super.parseSheet(sheet, \r -> parseRegionRow(r), false)
    }
    }