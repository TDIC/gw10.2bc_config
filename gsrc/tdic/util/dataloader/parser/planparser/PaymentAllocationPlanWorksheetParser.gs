package tdic.util.dataloader.parser.planparser

uses tdic.util.dataloader.parser.WorksheetParserInterface
uses tdic.util.dataloader.parser.WorksheetParserUtil
uses java.util.ArrayList
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses tdic.util.dataloader.data.plandata.PaymentAllocationPlanData

class PaymentAllocationPlanWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parsePaymentAllocationRow(row: Row): PaymentAllocationPlanData {
    var result = new PaymentAllocationPlanData ()
    result.PublicID = convertCellToString(row.getCell(PaymentAllocationPlanData.COL_PUBLIC_ID))
    result.Name = convertCellToString(row.getCell(PaymentAllocationPlanData.COL_NAME))
    result.Description = convertCellToString(row.getCell(PaymentAllocationPlanData.COL_DESCRIPTION))
    result.EffectiveDate = convertCellToDate(row.getCell(PaymentAllocationPlanData.COL_EFFECTIVE_DATE))
    result.ExpirationDate = convertCellToDate(row.getCell(PaymentAllocationPlanData.COL_EXPIRATION_DATE))
    // step through Filter and PayOrder columns
    var filterArray = new ArrayList<DistributionFilterType>()
    for (i in 0..5) {
      filterArray.add(typekey.DistributionFilterType.get(convertCellToString(row.getCell(i+5))))
    }
    var payArray = new ArrayList<InvoiceItemOrderingType>()
    for (i in 0..9) {
      /**
       * 10/23/2014
       * Alvin Lee
       *
       * Fix to Accelerator code; previously reading incorrect cell.
       */
      payArray.add(typekey.InvoiceItemOrderingType.get(convertCellToString(row.getCell(i+11))))
    }

    result.Filters = filterArray
    result.PayOrderTypes = payArray
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList < PaymentAllocationPlanData > {
    return parseSheet(sheet, \r -> parsePaymentAllocationRow(r), true)
  }
}