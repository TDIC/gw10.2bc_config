package tdic.util.dataloader.parser.sampleparser

uses tdic.util.dataloader.parser.WorksheetParserInterface
uses tdic.util.dataloader.parser.WorksheetParserUtil
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses gw.util.GWBaseDateEnhancement
uses java.util.ArrayList
uses tdic.util.dataloader.data.sampledata.AgencyBillPayData
uses gw.api.upgrade.Coercions

class AgencyBillPayWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }
  var RECEIVED_DATE: int = 0
  var PRODUCER_NAME: int = 1
  var STATEMENTID: int = 2
  var DESCRIPTION: int = 3
  var PAYMENT_REF: int = 4
  var PAYMENT_METHOD: int = 5
  var PAYMENT_TOKEN: int = 6
  var AMOUNT: int = 7
  function parseAgencyBillPayRow(row: Row): AgencyBillPayData {
    var agencyBillPayData = new AgencyBillPayData()
    // Populate Agency Bill Payment data
    agencyBillPayData.ReceivedDate = row.getCell(RECEIVED_DATE).DateCellValue
    agencyBillPayData.Producer = convertCellToString(row.getCell(PRODUCER_NAME))
    agencyBillPayData.StatementID = row.getCell(STATEMENTID).NumericCellValue as String
    if (agencyBillPayData.StatementID == null || agencyBillPayData.StatementID == "0")
      return null
    agencyBillPayData.Description = convertCellToString(row.getCell(DESCRIPTION))
    agencyBillPayData.PaymentMethod = typekey.PaymentMethod.get(convertCellToString(row.getCell(PAYMENT_METHOD)))
    agencyBillPayData.PaymentToken = convertCellToString(row.getCell(PAYMENT_TOKEN))
    agencyBillPayData.ReferenceNumber = convertCellToString(row.getCell(PAYMENT_REF))
    agencyBillPayData.Amount = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(AMOUNT)))
    agencyBillPayData.EntryDate = row.getCell(RECEIVED_DATE).DateCellValue
    if (agencyBillPayData.ReceivedDate == null  or agencyBillPayData.ReceivedDate < gw.api.util.DateUtil.currentDate()) {
      agencyBillPayData.ReceivedDate = gw.api.util.DateUtil.currentDate()
      agencyBillPayData.EntryDate = gw.api.util.DateUtil.currentDate()
    }
    return agencyBillPayData
  }

  override function parseSheet(sheet: Sheet): ArrayList <AgencyBillPayData> {
    return parseSheet(sheet, \r -> parseAgencyBillPayRow(r), false)
  }
}