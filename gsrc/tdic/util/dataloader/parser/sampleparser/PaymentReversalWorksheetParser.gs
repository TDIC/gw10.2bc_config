package tdic.util.dataloader.parser.sampleparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses java.util.ArrayList
uses org.apache.poi.ss.usermodel.Row
uses tdic.util.dataloader.data.sampledata.PaymentReversalData
uses gw.util.GWBaseDateEnhancement
uses gw.api.upgrade.Coercions

class PaymentReversalWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }
  var REVERSED_DATE: int = 0
  var PAYMENT_REF: int = 1
  var TRANSNUMBER: int = 2
  var REASON: int = 3
  var AMOUNT: int = 4
  function parsePaymentReversalRow(row: Row): PaymentReversalData {
    var paymentReversalData = new PaymentReversalData()
    // Populate paymentReversalData data
    paymentReversalData.ReferenceNumber = convertCellToString(row.getCell(PAYMENT_REF))
    paymentReversalData.TransactionNumber = convertCellToString(row.getCell(TRANSNUMBER))
    if (paymentReversalData.TransactionNumber.Empty || paymentReversalData.TransactionNumber == null)
      return null
    paymentReversalData.ReversalReason = typekey.PaymentReversalReason.get(convertCellToString(row.getCell(REASON)))
    paymentReversalData.Amount = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(AMOUNT)))
    paymentReversalData.ReversalDate = row.getCell(REVERSED_DATE).DateCellValue
    paymentReversalData.EntryDate = row.getCell(REVERSED_DATE).DateCellValue
    if (paymentReversalData.ReversalDate == null  or paymentReversalData.ReversalDate < gw.api.util.DateUtil.currentDate()) {
      paymentReversalData.ReversalDate = gw.api.util.DateUtil.currentDate()
      paymentReversalData.EntryDate = gw.api.util.DateUtil.currentDate()
    }
    return paymentReversalData
  }

  override function parseSheet(sheet: Sheet): ArrayList <PaymentReversalData> {
    return parseSheet(sheet, \r -> parsePaymentReversalRow(r), false)
  }
}