package tdic.util.dataloader.parser.sampleparser

uses tdic.util.dataloader.parser.WorksheetParserInterface
uses tdic.util.dataloader.parser.WorksheetParserUtil
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses tdic.util.dataloader.data.sampledata.InvoiceStreamData
uses java.util.ArrayList
uses java.lang.Integer
uses java.util.Date
uses gw.api.upgrade.Coercions

class InvoiceStreamWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  var INVOICE_STREAM_NAME: int = 0
  var DESCRIPTION: int = 1
  var DUE_OR_INVOICE_DAY: int = 2
  var DAY_OF_MONTH: int = 3
  var TWICE_MONTH_FIRST: int = 4
  var TWICE_MONTH_SECOND: int = 5
  var DAY_OF_WEEK: int = 6
  var WEEK_ANCHOR: int = 7
  var PAYMENT_METHOD: int = 8
  var TOKEN: int = 9
  var LEAD_TIME: int = 10
  construct() {
  }

  function parseInvoiceStreamRow(row: Row): InvoiceStreamData {
    var invoiceStreamData = new InvoiceStreamData()
    // Populate data
    invoiceStreamData.InvoiceStreamName = convertCellToString(row.getCell(INVOICE_STREAM_NAME))
    invoiceStreamData.Description = convertCellToString(row.getCell(DESCRIPTION))
    invoiceStreamData.PeriodicityString = invoiceStreamData.Description
    switch (invoiceStreamData.Description) {
      case "Monthly":
          invoiceStreamData.Periodicity = typekey.Periodicity.TC_MONTHLY
          break
      case "Quarterly":
          invoiceStreamData.Periodicity = typekey.Periodicity.TC_QUARTERLY
          break
      case "Weekly":
          invoiceStreamData.Periodicity = typekey.Periodicity.TC_EVERYWEEK
          break
      case "Yearly":
          invoiceStreamData.Periodicity = typekey.Periodicity.TC_EVERYYEAR
          break
      case "Every Other Month":
          invoiceStreamData.Periodicity = typekey.Periodicity.TC_EVERYOTHERMONTH
          break
      case "Every Other Week":
          invoiceStreamData.Periodicity = typekey.Periodicity.TC_EVERYOTHERWEEK
          break
      case "Every Other Year":
          invoiceStreamData.Periodicity = typekey.Periodicity.TC_EVERYOTHERYEAR
          break
      case "Twice Per Month":
          invoiceStreamData.Periodicity = typekey.Periodicity.TC_TWICEPERMONTH
          break
      case "Every Six Months":
          invoiceStreamData.Periodicity = typekey.Periodicity.TC_EVERYSIXMONTHS
          break
      case "Every Four Months":
          invoiceStreamData.Periodicity = typekey.Periodicity.TC_EVERYFOURMONTHS
          break
        default:
        //invoiceStreamData.Periodicity = typekey.Periodicity.TC_MONTHLY
        break
    }
    invoiceStreamData.DueOrInvoiceDayString = convertCellToString(row.getCell(DUE_OR_INVOICE_DAY))
    if (invoiceStreamData.DueOrInvoiceDayString == "Due") {
      invoiceStreamData.DueOrInvoiceDay = typekey.BillDateOrDueDateBilling.TC_DUEDATEBILLING
    } else {
      invoiceStreamData.DueOrInvoiceDay = typekey.BillDateOrDueDateBilling.TC_BILLDATEBILLING
    }
    invoiceStreamData.MonthlyDayOfMonth = Coercions.makeIntFrom(convertCellToString(row.getCell(DAY_OF_MONTH)))
    invoiceStreamData.TwiceAMonthFirstDay = Coercions.makeIntFrom(convertCellToString(row.getCell(TWICE_MONTH_FIRST)))
    invoiceStreamData.TwiceAMonthSecondDay = Coercions.makeIntFrom(convertCellToString(row.getCell(TWICE_MONTH_SECOND)))
    invoiceStreamData.WeeklyDayOfWeek = typekey.DayOfWeek.get(convertCellToString(row.getCell(DAY_OF_WEEK)))
    invoiceStreamData.EveryOtherWeekAnchorDate = Coercions.makeDateFrom(convertCellToString(row.getCell(WEEK_ANCHOR)))
    invoiceStreamData.PaymentMethodString = convertCellToString(row.getCell(PAYMENT_METHOD))
    if (invoiceStreamData.PaymentMethodString == null) {
      invoiceStreamData.PaymentMethodString = "cash"
    }
    invoiceStreamData.PaymentMethod = typekey.PaymentMethod.get(invoiceStreamData.PaymentMethodString)
    invoiceStreamData.Token = convertCellToString(row.getCell(TOKEN))
    invoiceStreamData.LeadTime = Coercions.makeIntFrom(convertCellToString(row.getCell(LEAD_TIME)))
    return invoiceStreamData
  }

  override function parseSheet(sheet: Sheet): ArrayList <InvoiceStreamData> {
    return parseSheet(sheet, \r -> parseInvoiceStreamRow(r), false)
  }
}