package tdic.util.dataloader.parser.sampleparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Row
uses tdic.util.dataloader.data.sampledata.ProducerData
uses tdic.util.dataloader.data.sampledata.ContactData
uses tdic.util.dataloader.data.AddressData
uses gw.util.GWBaseDateEnhancement
uses java.lang.Integer
uses org.apache.poi.ss.usermodel.Sheet
uses java.util.ArrayList
uses gw.api.upgrade.Coercions

class ProducerWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }
  var PRODUCER_NAME: int = 0
  var PRODUCER_CODE: int = 1
  var PRODUCER_ADDRESS_LINE1: int = 2
  var PRODUCER_ADDRESS_LINE2: int = 3
  var PRODUCER_CITY: int = 4
  var PRODUCER_STATE: int = 5
  var PRODUCER_ZIP: int = 6
  var PRODUCER_COUNTRY: int = 7
  var ACTIVE: int = 8
  var PRODUCER_CONTACT_FIRST_NAME: int = 9
  var PRODUCER_CONTACT_LAST_NAME: int = 10
  var PRODUCER_CONTACT_WORKPHONE: int = 11
  var PRODUCER_CONTACT_FAXPHONE: int = 12
  var PRODUCER_CONTACT_EMAIL: int = 13
  var PRODUCER_CONTACT_ROLE: int = 14
  var PRODUCER_ACCOUNT_REP: int = 15
  var PRODUCER_AGENCY_BILL_PLAN: int = 16
  var PRODUCER_COMMISSION_PLAN: int = 17
  var PRODUCER_TIER: int = 18
  var PRODUCER_PAY_PERIODICITY: int = 19
  var PRODUCER_PAY_DAY: int = 20
  var CREATE_DATE: int = 21
  function parseProducerRow(row: Row): ProducerData {
    var producerData = new ProducerData()
    var contactData = new ContactData()
    var addressData = new AddressData()
    producerData.ProducerName = convertCellToString(row.getCell(PRODUCER_NAME))
    if (producerData.ProducerName.Empty || producerData.ProducerName == null)
      return null
    /* Populate Producer Address */
    addressData.AddressLine1 = convertCellToString(row.getCell(PRODUCER_ADDRESS_LINE1))
    addressData.AddressLine2 = convertCellToString(row.getCell(PRODUCER_ADDRESS_LINE2))
    addressData.City = convertCellToString(row.getCell(PRODUCER_CITY))
    addressData.State = convertCellToString(row.getCell(PRODUCER_STATE))
    addressData.PostalCode = convertCellToString(row.getCell(PRODUCER_ZIP))
    addressData.Country = typekey.Country.get(convertCellToString(row.getCell(PRODUCER_COUNTRY)))
    /* Populate Producer Contact */
    contactData.FirstName = convertCellToString(row.getCell(PRODUCER_CONTACT_FIRST_NAME))
    contactData.LastName = convertCellToString(row.getCell(PRODUCER_CONTACT_LAST_NAME))
    contactData.Name = contactData.FirstName + " " + contactData.LastName
    contactData.WorkPhone = convertCellToString(row.getCell(PRODUCER_CONTACT_WORKPHONE))
    contactData.FaxPhone = convertCellToString(row.getCell(PRODUCER_CONTACT_FAXPHONE))
    contactData.Email = convertCellToString(row.getCell(PRODUCER_CONTACT_EMAIL))
    contactData.ContactType = typekey.Contact.TC_PERSON
    contactData.PrimaryAddress = addressData
    /* Populate Producer */
    producerData.ProducerCode = convertCellToString(row.getCell(PRODUCER_CODE))
    producerData.Active = Coercions.makeBooleanFrom(convertCellToString(row.getCell(ACTIVE)))
    if (producerData.Active == null) {
      producerData.Active = true
    }
    producerData.AccountRep = convertCellToString(row.getCell(PRODUCER_ACCOUNT_REP))
    producerData.AgencyBillPlan = convertCellToString(row.getCell(PRODUCER_AGENCY_BILL_PLAN))
    producerData.CommissionPlan = convertCellToString(row.getCell(PRODUCER_COMMISSION_PLAN))
    producerData.Tier = convertCellToString(row.getCell(PRODUCER_TIER))
    producerData.Periodicity = convertCellToString(row.getCell(PRODUCER_PAY_PERIODICITY))
    producerData.CommissionDOM = Coercions.makeIntFrom(convertCellToString(row.getCell(PRODUCER_PAY_DAY)))
    producerData.Role = typekey.ProducerRole.get(convertCellToString(row.getCell(PRODUCER_CONTACT_ROLE)))
    producerData.CreateDate = row.getCell(CREATE_DATE).DateCellValue
    producerData.EntryDate = row.getCell(CREATE_DATE).DateCellValue
    if (producerData.CreateDate == null or producerData.CreateDate < gw.api.util.DateUtil.currentDate()) {
      producerData.CreateDate = gw.api.util.DateUtil.currentDate()
      producerData.EntryDate = gw.api.util.DateUtil.currentDate()
    }
    producerData.Contact = contactData
    producerData.PrimaryAddress = addressData
    return producerData
  }

  override function parseSheet(sheet: Sheet): ArrayList <ProducerData> {
    return parseSheet(sheet, \r -> parseProducerRow(r), false)
  }
}