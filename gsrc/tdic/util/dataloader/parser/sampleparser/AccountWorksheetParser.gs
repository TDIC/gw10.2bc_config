package tdic.util.dataloader.parser.sampleparser

uses tdic.util.dataloader.parser.WorksheetParserInterface
uses tdic.util.dataloader.parser.WorksheetParserUtil
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses gw.util.GWBaseDateEnhancement
uses tdic.util.dataloader.data.sampledata.AccountData
uses tdic.util.dataloader.data.sampledata.ContactData
uses tdic.util.dataloader.data.AddressData
uses java.util.ArrayList
uses gw.api.upgrade.Coercions

class AccountWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  function parseAccountRow(row: Row): AccountData {
    var result = new AccountData()
    var contactData = new ContactData()
    var addressData = new AddressData()
    result.AccountName = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_NAME))
    if (result.AccountName.Empty || result.AccountName == null)
      return null
    // Populate Account Address
    addressData.AddressLine1 = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_CONTACT_ADDRESS_LINE1))
    addressData.AddressLine2 = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_CONTACT_ADDRESS_LINE2))
    addressData.City = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_CONTACT_CITY))
    addressData.State = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_CONTACT_STATE))
    addressData.PostalCode = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_CONTACT_ZIP))
    addressData.Country = typekey.Country.get(convertCellToString(row.getCell(AccountData.COL_ACCOUNT_CONTACT_COUNTRY)))
    // Populate Account Contact 
    contactData.Name = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_CONTACT_NAME))
    contactData.FirstName = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_CONTACT_FIRST_NAME))
    contactData.LastName = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_CONTACT_LAST_NAME))
    if (contactData.Name == null and (contactData.FirstName != null or contactData.LastName != null)) {
      contactData.Name = contactData.FirstName + " " + contactData.LastName
    }
    contactData.ContactType = typekey.Contact.get(convertCellToString(row.getCell(AccountData.COL_ACCOUNT_CONTACT_TYPE)))
    contactData.WorkPhone = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_CONTACT_PHONE))
    contactData.FaxPhone = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_CONTACT_FAXPHONE))
    contactData.Email = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_CONTACT_EMAIL))
    contactData.PrimaryAddress = addressData
    // Populate Account
    result.AccountNumber = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_NUMBER))
    result.BillingLevel = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_BILLING_LEVEL))
    result.AccountNumber = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_NUMBER))
    result.FEIN = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_FEIN))
    result.BillPlan = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_BILL_PLAN))
    result.DelinquencyPlan = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_DELINQUENCY))
    result.DueOrInvoiceInd = convertCellToString(row.getCell(AccountData.COL_DUE_OR_INVOICE_DAY))
    result.InvoiceDOM = Coercions.makeIntFrom(convertCellToString(row.getCell(AccountData.COL_DAY_OF_MONTH)))
    result.InvoiceTwiceFirst = Coercions.makeIntFrom(convertCellToString(row.getCell(AccountData.COL_TWICE_MONTH_FIRST)))
    result.InvoiceTwiceSecond = Coercions.makeIntFrom(convertCellToString(row.getCell(AccountData.COL_TWICE_MONTH_SECOND)))
    result.InvoiceDayofWeek = convertCellToString(row.getCell(AccountData.COL_DAY_OF_WEEK))
    result.InvoiceWeekAnchorDate = convertStringToDate(convertCellToString(row.getCell(AccountData.COL_WEEK_ANCHOR)))
    result.InvoiceDelivery = typekey.InvoiceDeliveryMethod.get(convertCellToString(row.getCell(AccountData.COL_ACCOUNT_CONTACT_INVOICE_DELIVERY)))
//    result.DistributionLimitType = convertCellToString(row.getCell(AccountData.COL_DISTRIBUTIONLIMIT_TYPE))    deprecated in BC8.0.1
    result.AccountType = typekey.AccountType.get(convertCellToString(row.getCell(AccountData.COL_ACCOUNT_TYPE)))
    result.PaymentMethod = typekey.PaymentMethod.get(convertCellToString(row.getCell(AccountData.COL_PAYMENTMETHOD)))
    if (result.PaymentMethod == TC_ACH or result.PaymentMethod == TC_CREDITCARD){
      result.Token = convertCellToString(row.getCell(AccountData.COL_TOKEN))
    }
    result.PaymentPlan = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_PAYMENT_PLAN))
    result.InvoiceStream = convertCellToString(row.getCell(AccountData.COL_ACCOUNT_INVOICE_STREAM))
    result.PrimaryPayer = Coercions.makeBooleanFrom(convertCellToString(row.getCell(AccountData.COL_ACCOUNT_CONTACT_PRIMARY_PAYER)))
    if (result.PrimaryPayer == null) {
      result.PrimaryPayer = true
    }
    result.Role = typekey.AccountRole.get(convertCellToString(row.getCell(AccountData.COL_ACCOUNT_CONTACT_ROLE)))
    result.CreateDate = convertStringToDate(convertCellToString(row.getCell(AccountData.COL_CREATE_DATE)))
    result.EntryDate = convertStringToDate(convertCellToString(row.getCell(AccountData.COL_CREATE_DATE)))
    if (result.CreateDate == null or result.CreateDate < gw.api.util.DateUtil.currentDate()) {
      result.CreateDate = gw.api.util.DateUtil.currentDate()
      result.EntryDate = gw.api.util.DateUtil.currentDate()
    }
    result.Contact = contactData
    return result
  }

  override function parseSheet(sheet: Sheet): ArrayList <AccountData> {
    return parseSheet(sheet, \r -> parseAccountRow(r), false)
  }
}