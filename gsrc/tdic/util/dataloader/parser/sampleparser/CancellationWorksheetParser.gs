package tdic.util.dataloader.parser.sampleparser

uses tdic.util.dataloader.parser.WorksheetParserUtil
uses tdic.util.dataloader.parser.WorksheetParserInterface
uses org.apache.poi.ss.usermodel.Sheet
uses org.apache.poi.ss.usermodel.Row
uses java.util.ArrayList
uses tdic.util.dataloader.data.sampledata.ChargeData
uses java.math.BigDecimal
uses gw.util.GWBaseDateEnhancement
uses tdic.util.dataloader.data.sampledata.CancellationData
uses gw.api.upgrade.Coercions

class CancellationWorksheetParser extends WorksheetParserUtil implements WorksheetParserInterface {
  construct() {
  }

  // first column is offset days; not imported
  var CANCELLATION_DATE: int = 1
  var ASSOCIATED_POLICY_PERIOD: int = 2
  var CANCELLATION_TYPE: int = 3
  var CANCELLATION_REASON: int = 4
  var HOLD_UNBILLED_PREMIUM: int = 5
  var DESCRIPTION: int = 6
  function parseCancellationRow(row: Row): CancellationData {
    var policyNumber = convertCellToString(row.getCell(ASSOCIATED_POLICY_PERIOD))
    if (policyNumber.Empty || policyNumber == null)
      return null
    var cancellationData = new CancellationData()
    var charges = new ArrayList <ChargeData>()
    // Populate charges
    var i = 7
    while (i < 14) {
      var chgCell = i
      var amtCell = i + 1
      var amt = Coercions.makeBigDecimalFrom(convertCellToString(row.getCell(amtCell as short)))
      var chg = convertCellToString(row.getCell(chgCell as short))
      populateCharges(charges, amt, chg, null, null)
      i = i + 2
    }
    // Populate Cancellation data
    cancellationData.AssociatedPolicyPeriod = policyNumber
    cancellationData.ModificationDate = row.getCell(CANCELLATION_DATE).DateCellValue
    if (cancellationData.ModificationDate == null  or cancellationData.ModificationDate < gw.api.util.DateUtil.currentDate()) {
      cancellationData.ModificationDate = gw.api.util.DateUtil.currentDate()
    }
    cancellationData.CancellationType = typekey.CancellationType.get(convertCellToString(row.getCell(CANCELLATION_TYPE)))
    cancellationData.CancellationReason = convertCellToString(row.getCell(CANCELLATION_REASON))
    cancellationData.HoldUnbilledPremiumCharges = Coercions.makeBooleanFrom(convertCellToString(row.getCell(HOLD_UNBILLED_PREMIUM)))
    if (cancellationData.HoldUnbilledPremiumCharges == null) {
      cancellationData.HoldUnbilledPremiumCharges = false
    }
    cancellationData.Description = convertCellToString(row.getCell(DESCRIPTION))
    cancellationData.EntryDate = row.getCell(CANCELLATION_DATE).DateCellValue
    cancellationData.Charges = (charges?.toTypedArray())
    return cancellationData
  }

  override function parseSheet(sheet: Sheet): ArrayList <CancellationData> {
    return parseSheet(sheet, \r -> parseCancellationRow(r), false)
  }
}