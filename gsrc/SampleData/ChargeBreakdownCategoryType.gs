package SampleData

uses gw.api.database.Query
uses gw.api.databuilder.ChargeBreakdownCategoryTypeBuilder

@Export
class ChargeBreakdownCategoryType {

  function create(code : String, name : String) : ChargeBreakdownCategoryType {
    return Query.make(ChargeBreakdownCategoryType).compare("Code", Equals, code).select().AtMostOneRow
        ?: new ChargeBreakdownCategoryTypeBuilder().withCode(code).withName(name).createAndCommit()
  }

}