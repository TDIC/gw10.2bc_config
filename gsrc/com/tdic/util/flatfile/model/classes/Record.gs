package com.tdic.util.flatfile.model.classes

class Record {
  private var _recordType: String as RecordType
  private var _recordSection: String as RecordSection
  private var _recordDelimiter: String as RecordDelimiter
  private var _recordFormat: String as RecordFormat
  private var _execute: String as Execute
  private var _source: String as Source
  private var _fileStart: String as FileStart
  private var _fields: Field[] as Fields
  private var _identifier: String as Identifier
  construct() {
  }

  construct(recordType: String, recordSection: String, recordDelimiter: String, recordFormat: String, execute: String, source: String, identifier: String) {
    _recordType = recordType
    _recordSection = recordSection
    _recordDelimiter = recordDelimiter
    _recordFormat = recordFormat
    _execute = execute
    _source = source
    _identifier = identifier
  }

  function encode(): String {
    return ""
  }

  override function toString(): String {
    return "Record Type: ${RecordType}\t Record Section: ${RecordSection}\t RecordDelimiter: ${RecordDelimiter}\t RecordFormat ${RecordFormat}\t Execute: ${Execute}\t Source: ${Source}\t FileStart ${FileStart}\t Identifier: ${Identifier}\t Fields: ${Fields}"
  }
}