package com.tdic.util.flatfile.excel

uses java.lang.Exception

class CsvFormatException extends Exception {
  // Default Constructor

  construct() {
  }

  construct(aMessage: String) {
    super(aMessage)
  }
}


