/**
 * © Copyright 2011, 2013-2014 Hewlett-Packard Development Company, L.P. 
 * © Copyright 2009-2014 Guidewire Software, Inc.
 */
package com.tdic.plugins.hpexstream.core.web

uses gw.pl.logging.LoggerCategory
uses gw.servlet.Servlet
uses javax.servlet.http.HttpServlet
uses javax.servlet.http.HttpServletRequest
uses javax.servlet.http.HttpServletResponse
uses java.lang.Exception
uses org.slf4j.LoggerFactory

/**
 * Class <code>ExstreamStoreReviewCaseServlet</code> is the servlet used to 
 * receive Review Case from Correspond NOW document editor.
 *
 */
@Servlet(\path: String -> path.matches("/RetrieveLiveDocument"))
public class TDIC_ExstreamRetrieveLiveDocumentServlet extends HttpServlet {
  private static var _logger = LoggerFactory.getLogger("EXSTREAM_DOCUMENT_PRODUCTION")
  override public function init() {
    _logger.debug("TDIC_ExstreamRetrieveLiveDocumentServlet#init() - Entering")
    _logger.debug("TDIC_ExstreamRetrieveLiveDocumentServlet#init() - Exiting")
  }

  /**
   * US555
   * 10/13/2014 shanem
   */
  override public function doPost(httpRequest: HttpServletRequest, httpResponse: HttpServletResponse) {
    _logger.debug("TDIC_ExstreamRetrieveLiveDocumentServlet#doPost(HttpServletRequest, HttpServletResponse) - Entering")
    var reviewCaseId = httpRequest.getParameter("reviewCaseId")
    if (reviewCaseId == null) {
      throw new Exception("'reviewCaseId' parameter not specified")
    }

    var userName = httpRequest.getParameter("userName")
    if (userName == null or userName == "null") {
      throw new Exception("'userName' parameter not specified")
    }

    try {
      httpResponse.setContentType("application/dlf")
      httpResponse.setHeader("Content-Disposition", "filename=\"out.dlf\"")

      /*var fileByteArray = LiveContent_Ext(reviewCaseId).Content.Bytes // new File("C:\\java\\ews-tomcat-gw\\exstream\\var\\tmp\\PolicyCenter241990244\\DLFOUT.dlf").readBytes() //  
      httpResponse.setContentLength(fileByteArray.length)
      var os = httpResponse.OutputStream
      os.write(fileByteArray)
      os.flush()
      os.close()*/
    } catch (e: Exception) {
      _logger.error("TDIC_ExstreamRetrieveLiveDocumentServlet#doPost(HttpServletRequest, HttpServletResponse) - Exception in ExstreamStoreReviewCaseServlet.put(): " + e.Message)
      httpResponse.sendError(500, e.getMessage())
    }
    _logger.debug("TDIC_ExstreamRetrieveLiveDocumentServlet#doPost(HttpServletRequest, HttpServletResponse) - Exiting")
  }

  /**
   * US555
   * 10/13/2014 shanem
   */
  override public function doGet(request: HttpServletRequest, response: HttpServletResponse) {
    _logger.debug("TDIC_ExstreamRetrieveLiveDocumentServlet#doGet(HttpServletRequest, HttpServletResponse) - Entering")
    doPost(request, response)
    _logger.debug("TDIC_ExstreamRetrieveLiveDocumentServlet#doGet(HttpServletRequest, HttpServletResponse) - Exiting")
  }

  /**
   * US555
   * 10/13/2014 shanem
   *
   * Clean up resources
   */
  override public function destroy() {
    _logger.debug("TDIC_ExstreamRetrieveLiveDocumentServlet#destroy() - Entering")
    _logger.debug("TDIC_ExstreamRetrieveLiveDocumentServlet#destroy() - Exiting")
  }
}
