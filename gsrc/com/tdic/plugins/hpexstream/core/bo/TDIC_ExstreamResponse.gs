package com.tdic.plugins.hpexstream.core.bo

uses gw.xml.ws.annotation.WsiExportable
uses java.util.List

/**
 * US645
 * 01/14/2015 shanem
 */
@WsiExportable
final class TDIC_ExstreamResponse {
  var documents: List<TDIC_DocumentUpdate> as Documents
}
